//
//  TableViewWithEmptyView.m
//
//
//  Created by Shekhar on 8/31/14.
//  Copyright 2014 Edenred. All rights reserved.
//

#import "TableViewWithEmptyView.h"
#import "Constants.h"
#import <QuartzCore/QuartzCore.h>

@implementation TableViewWithEmptyView

@synthesize emptyView;

- (bool) tableViewHasRows
{
    // TODO: This only supports the first section so far
    return [self numberOfRowsInSection:0] == 0;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.customRefreshControl = [[UIRefreshControl alloc] init];
    self.customRefreshControl.backgroundColor = [UIColor clearColor];
    self.customRefreshControl.tintColor = COLOUR_MIDGREY;
    if(@available(iOS 10, *)){
        self.refreshControl = self.customRefreshControl;
    } else {
        [self insertSubview: self.customRefreshControl atIndex: 0];
    }
}
-(id)init
{
    [super init];
    if (self)
    {
        self.customRefreshControl = [[UIRefreshControl alloc] init];
        self.customRefreshControl.backgroundColor = [UIColor clearColor];
        self.customRefreshControl.tintColor = COLOUR_MIDGREY;
        if(@available(iOS 10, *)){
            self.refreshControl = self.customRefreshControl;
        } else {
            [self insertSubview: self.customRefreshControl atIndex: 0];
        }
    }
    return self;
}

- (void) updateEmptyPage
{
    const CGRect rect = (CGRect){CGPointZero,self.frame.size};
    emptyView.frame  = rect;
//    if (emptyView.fullView) {
//        emptyView.lblPlaceholderTitle.frame = CGRectMake(10, emptyView.center.y, 300, 30);
//    }
//    else emptyView.lblPlaceholderTitle.frame = CGRectMake(0, emptyView.center.y, 300, 30);
    const bool shouldShowEmptyView = self.tableViewHasRows;
    const bool emptyViewShown      = emptyView.superview != nil;

    if (shouldShowEmptyView == emptyViewShown) return;

    CATransition *animation = [CATransition animation];
    [animation setDuration:0.5];
    [animation setType:kCATransitionFade];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [[self layer] addAnimation:animation forKey:kCATransitionReveal];

    if (shouldShowEmptyView)
        [self addSubview:emptyView];
    else
        [emptyView removeFromSuperview];
}

- (void) setEmptyView:(PlaceholderView *)newView
{
    if (newView == emptyView) return;

    UIView *oldView = emptyView;
    emptyView = [newView retain];

    [oldView removeFromSuperview];
    [oldView release];

    [self updateEmptyPage];
}

#pragma mark UIView

- (void) layoutSubviews
{
    [super layoutSubviews];
    [self updateEmptyPage];
}

//- (UIView*) hitTest:(CGPoint)point withEvent:(UIEvent *)event
//{
//    // Prevent any interaction when the empty view is shown
//    const bool emptyViewShown = emptyView.superview != nil;
//    return emptyViewShown ? nil : [super hitTest:point withEvent:event];
//}

#pragma mark UITableView

- (void) reloadData
{
    [super reloadData];
    [self updateEmptyPage];
}

@end
