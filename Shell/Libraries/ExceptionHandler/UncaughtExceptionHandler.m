//
//  UncaughtExceptionHandler.m
//  UncaughtExceptions
//
//  Created by Matt Gallagher on 2010/05/25.
//  Copyright 2010 Matt Gallagher. All rights reserved.
//
//  Permission is given to use this source code file, free of charge, in any
//  project, commercial or otherwise, entirely at your risk, with the condition
//  that any redistribution (in part or whole) of source code must retain
//  this copyright and permission notice. Attribution in compiled projects is
//  appreciated but not required.
//

#import "UncaughtExceptionHandler.h"
#include <libkern/OSAtomic.h>
#include <execinfo.h>
#import "AlertViewManager.h"
#import "Session.h"

#import "WebServiceManager.h"

@interface UncaughtExceptionHandler ()
@property(nonatomic,assign) BOOL dismissed;
@end
NSString * const UncaughtExceptionHandlerSignalExceptionName = @"UncaughtExceptionHandlerSignalExceptionName";

NSString * const UncaughtExceptionHandlerSignalKey = @"UncaughtExceptionHandlerSignalKey";

NSString * const UncaughtExceptionHandlerAddressesKey = @"UncaughtExceptionHandlerAddressesKey";

volatile int32_t UncaughtExceptionCount = 0;

const int32_t UncaughtExceptionMaximum = 10;

const NSInteger UncaughtExceptionHandlerSkipAddressCount = 4;
const NSInteger UncaughtExceptionHandlerReportAddressCount = 5;

@implementation UncaughtExceptionHandler


- (id)init
{
    self = [super init];
    if (self) {

    }
    return self;
}

+ (NSArray *)backtrace
{
	 void* callstack[128];
	 int frames = backtrace(callstack, 128);
	 char **strs = backtrace_symbols(callstack, frames);
	 
	 int i;
	 NSMutableArray *backtrace = [NSMutableArray arrayWithCapacity:frames];
	 for (
	 	i = UncaughtExceptionHandlerSkipAddressCount;
	 	i < UncaughtExceptionHandlerSkipAddressCount +
			UncaughtExceptionHandlerReportAddressCount;
		i++)
	 {
	 	[backtrace addObject:[NSString stringWithUTF8String:strs[i]]];
	 }
	 free(strs);
	 
	 return backtrace;
}

- (void)alertView:(UIAlertView *)anAlertView clickedButtonAtIndex:(NSInteger)anIndex
{
	if (anIndex == 0)
	{
		self.dismissed = YES;
	}
}



+ (void) run
{
	InstallUncaughtExceptionHandler();
}


- (void)handleException:(NSException *)exception
{
    NSLog(@"EXCEPTION ---- %@",[exception reason]);
//    [mAlert showErrorAlertWithMessage:[NSString stringWithFormat:NSLocalizedString(
//                                                                                   @""
//                                                                                   @"%@\n%@", nil),
//                                       [exception reason],
//                                       [[exception userInfo] objectForKey:UncaughtExceptionHandlerAddressesKey]]];
    
    self.errorDescription = [NSString stringWithFormat:NSLocalizedString(
                                                                         @""
                                                                         @"%@\n%@", nil),
                             [exception reason],
                             [[exception userInfo] objectForKey:UncaughtExceptionHandlerAddressesKey]];
    self.logHelper = mLogHelper;
    [[WebServiceManager sharedInstance]logExceptionError:nil withLogInfo:self withDelegate:self];
    
//    UIAlertView *alert =
//        [[[UIAlertView alloc]
//            initWithTitle:NSLocalizedString(@"The application has quit unexpectedly ", nil)
//            message:[NSString stringWithFormat:NSLocalizedString(
//                @""
//                @"%@\n%@", nil),
//                [exception reason],
//                [[exception userInfo] objectForKey:UncaughtExceptionHandlerAddressesKey]]
//            delegate:self
//            cancelButtonTitle:NSLocalizedString(@"Quit", nil)
//            otherButtonTitles:nil, nil]
//        autorelease];
	//[alert show];
//
//    CFRunLoopRef runLoop = CFRunLoopGetCurrent();
//    CFArrayRef allModes = CFRunLoopCopyAllModes(runLoop);
//
//    while (!self.dismissed)
//    {
//        for (NSString *mode in (NSArray *)allModes)
//        {
//            CFRunLoopRunInMode((CFStringRef)mode, 0.001, false);
//        }
//    }
	
//    CFRelease(allModes);

	NSSetUncaughtExceptionHandler(NULL);
	signal(SIGABRT, SIG_DFL);
	signal(SIGILL, SIG_DFL);
	signal(SIGSEGV, SIG_DFL);
	signal(SIGFPE, SIG_DFL);
	signal(SIGBUS, SIG_DFL);
	signal(SIGPIPE, SIG_DFL);
	
	if ([[exception name] isEqual:UncaughtExceptionHandlerSignalExceptionName])
	{
		kill(getpid(), [[[exception userInfo] objectForKey:UncaughtExceptionHandlerSignalKey] intValue]);
	}
	else
	{
		[exception raise];
	}
}

- (void)processCompleted:(WebServiceResponse *)response
{
//    if (response.webserviceCall == kLOGEXCEPTIONERROR)
//    {
//        if (!response.errorInResponse) {
//            NSLOG(@"Exception");
//        }
//        else
//        {
//            [mAlert showWebserviceAlertWithAlert:response.alert onCompletion:nil];
//        }
//    }
    [mSession reset];
    [mLookupManager loadLookupTable];
    [mSession loadLoginView];
}


@end

void HandleException(NSException *exception)
{

	int32_t exceptionCount = OSAtomicIncrement32(&UncaughtExceptionCount);
	if (exceptionCount > UncaughtExceptionMaximum)
	{
		return;
	}
	
	NSArray *callStack = [UncaughtExceptionHandler backtrace];
	NSMutableDictionary *userInfo =
		[NSMutableDictionary dictionaryWithDictionary:[exception userInfo]];
	[userInfo
		setObject:callStack
		forKey:UncaughtExceptionHandlerAddressesKey];
	
	[[[UncaughtExceptionHandler alloc] init]
		performSelectorOnMainThread:@selector(handleException:)
		withObject:
			[NSException
				exceptionWithName:[exception name]
				reason:[exception reason]
				userInfo:userInfo]
		waitUntilDone:YES];
}

void SignalHandler(int signal)
{

	int32_t exceptionCount = OSAtomicIncrement32(&UncaughtExceptionCount);
	if (exceptionCount > UncaughtExceptionMaximum)
	{
		return;
	}
	
	NSMutableDictionary *userInfo =
		[NSMutableDictionary
			dictionaryWithObject:[NSNumber numberWithInt:signal]
			forKey:UncaughtExceptionHandlerSignalKey];

	NSArray *callStack = [UncaughtExceptionHandler backtrace];
	[userInfo
		setObject:callStack
		forKey:UncaughtExceptionHandlerAddressesKey];
	
    
	[[[UncaughtExceptionHandler alloc] init]
		performSelectorOnMainThread:@selector(handleException:)
		withObject:
			[NSException
				exceptionWithName:UncaughtExceptionHandlerSignalExceptionName
				reason:
					[NSString stringWithFormat:
						NSLocalizedString(@"Signal %d was raised.", nil),
						signal]
				userInfo:
					[NSDictionary
						dictionaryWithObject:[NSNumber numberWithInt:signal]
						forKey:UncaughtExceptionHandlerSignalKey]]
		waitUntilDone:YES];
}

void InstallUncaughtExceptionHandler()
{
	NSSetUncaughtExceptionHandler(&HandleException);
	signal(SIGABRT, SignalHandler);
	signal(SIGILL, SignalHandler);
	signal(SIGSEGV, SignalHandler);
	signal(SIGFPE, SignalHandler);
	signal(SIGBUS, SignalHandler);
	signal(SIGPIPE, SignalHandler);
}


