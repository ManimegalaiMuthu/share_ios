//
//  UncaughtExceptionHandler.h
//  UncaughtExceptions
//
//  Created by Matt Gallagher on 2010/05/25.
//  Copyright 2010 Matt Gallagher. All rights reserved.
//
//  Permission is given to use this source code file, free of charge, in any
//  project, commercial or otherwise, entirely at your risk, with the condition
//  that any redistribution (in part or whole) of source code must retain
//  this copyright and permission notice. Attribution in compiled projects is
//  appreciated but not required.
//

#import <UIKit/UIKit.h>
#import "LogHelper.h"

@protocol UncaughtExceptionHandlerDelegate <NSObject>
- (void) restoreApplicationContents;
@end
@interface UncaughtExceptionHandler : NSObject
@property(nonatomic,strong) LogHelper *logHelper;
@property(nonatomic,strong) NSString *errorDescription;
+(void)run;
@end

void InstallUncaughtExceptionHandler();
