//
//  UIView+IBDesignable.m
//  testB
//
//  Created by Will Choy on 2/10/15.
//  Copyright © 2015 WillYue. All rights reserved.
//

#import "UIView+IBDesignable.h"

@implementation UIView (IBDesignable)
@dynamic borderColor,borderWidth,cornerRadius;

-(void)setBorderColor:(UIColor *)borderColor{
    [self.layer setBorderColor:borderColor.CGColor];
}

-(void)setBorderWidth:(CGFloat)borderWidth{
    [self.layer setBorderWidth:borderWidth];
}

-(void)setCornerRadius:(CGFloat)cornerRadius{
    [self.layer setCornerRadius:cornerRadius];
}
@end
