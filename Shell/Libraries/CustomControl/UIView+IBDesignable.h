//
//  UIView+IBDesignable.h
//  testB
//
//  Created by Will Choy on 2/10/15.
//  Copyright © 2015 WillYue. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (IBDesignable)
@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic) IBInspectable CGFloat cornerRadius;
@end
