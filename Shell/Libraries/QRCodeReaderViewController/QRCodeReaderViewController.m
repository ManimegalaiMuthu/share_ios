/*
 * QRCodeReaderViewController
 *
 * Copyright 2014-present Yannick Loriot.
 * http://yannickloriot.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#import "QRCodeReaderViewController.h"
#import "QRCameraSwitchButton.h"
#import "QRCodeReaderView.h"
#import "LocalizationManager.h"

@interface QRCodeReaderViewController () <AVCaptureMetadataOutputObjectsDelegate>
@property (strong, nonatomic) QRCameraSwitchButton *switchCameraButton;
@property (strong, nonatomic) QRCodeReaderView     *cameraView;
@property (strong, nonatomic) UIButton             *cancelButton;

@property (strong, nonatomic) UILabel             *lblCount;
@property (strong, nonatomic) UILabel             *lblScanTitle;

@property (strong, nonatomic) AVCaptureDevice            *defaultDevice;
@property (strong, nonatomic) AVCaptureDeviceInput       *defaultDeviceInput;
@property (strong, nonatomic) AVCaptureDevice            *frontDevice;
@property (strong, nonatomic) AVCaptureDeviceInput       *frontDeviceInput;
@property (strong, nonatomic) AVCaptureMetadataOutput    *metadataOutput;
@property (strong, nonatomic) AVCaptureSession           *session;
@property (strong, nonatomic) AVCaptureVideoPreviewLayer *previewLayer;

@property (copy, nonatomic) void (^completionBlock) (NSString *);

@end

@implementation QRCodeReaderViewController

- (id)init
{
  return [self initWithCancelButtonTitle: LOCALIZATION(C_FORM_CANCEL)];
}

- (id)initWithCancelButtonTitle:(NSString *)cancelTitle
{
  if ((self = [super init])) {
    self.view.backgroundColor = [UIColor blackColor];
    
    [self setupAVComponents];
    [self configureDefaultComponents];
    [self setupUIComponentsWithCancelButtonTitle:cancelTitle];
    [self setupAutoLayoutConstraints];
    
    [_cameraView.layer insertSublayer:self.previewLayer atIndex:0];
  }
  return self;
}

-(void) setScanCountHidden:(BOOL) hidden
{
    self.lblCount.hidden = hidden;
}

-(void) setScanSubtitleHidden:(BOOL) hidden
{
    self.lblScanTitle.hidden = hidden;
}
-(void) setScanSubtitleMessage:(NSString*)string
{
    self.lblScanTitle.text = string;
    [self setScanSubtitleHidden:NO];
}

+ (instancetype)readerWithCancelButtonTitle:(NSString *)cancelTitle
{
  return [[self alloc] initWithCancelButtonTitle:cancelTitle];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [_cancelButton setTitle: LOCALIZATION(C_FORM_CANCEL) forState:UIControlStateNormal];
    
    _lblCount.text = [NSString stringWithFormat: @"%i/50", _scanCount];
    _lblScanTitle.text = LOCALIZATION(C_DECAL_TITLE);
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addSuccessCount)
                                                 name:@"addSuccessCount"
                                               object:nil];
    
    [self startScanning];
}

-(void) addSuccessCount
{
    self.scanCount++;
    _lblCount.text = [NSString stringWithFormat: @"%i/50", _scanCount];
}

- (void)viewWillDisappear:(BOOL)animated
{
  [self stopScanning];
    [[NSNotificationCenter defaultCenter] removeObserver: self];
    
  [super viewWillDisappear:animated];
}

- (void)viewWillLayoutSubviews
{
  [super viewWillLayoutSubviews];
  
  _previewLayer.frame = self.view.bounds;
}

- (BOOL)shouldAutorotate
{
  return YES;
}

#pragma mark - Managing the Orientation

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
  [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
  
  [_cameraView setNeedsDisplay];
  
  if (self.previewLayer.connection.isVideoOrientationSupported) {
    self.previewLayer.connection.videoOrientation = [[self class] videoOrientationFromInterfaceOrientation:toInterfaceOrientation];
  }
}

+ (AVCaptureVideoOrientation)videoOrientationFromInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
  switch (interfaceOrientation) {
    case UIInterfaceOrientationLandscapeLeft:
      return AVCaptureVideoOrientationLandscapeLeft;
    case UIInterfaceOrientationLandscapeRight:
      return AVCaptureVideoOrientationLandscapeRight;
    case UIInterfaceOrientationPortrait:
      return AVCaptureVideoOrientationPortrait;
    default:
      return AVCaptureVideoOrientationPortraitUpsideDown;
  }
}

#pragma mark - Managing the Block

- (void)setCompletionWithBlock:(void (^) (NSString *resultAsString))completionBlock
{
  self.completionBlock = completionBlock;
}

#pragma mark - Initializing the AV Components

- (void)setupUIComponentsWithCancelButtonTitle:(NSString *)cancelButtonTitle
{
  self.cameraView                                       = [[QRCodeReaderView alloc] init];
  _cameraView.translatesAutoresizingMaskIntoConstraints = NO;
  _cameraView.clipsToBounds                             = YES;
  [self.view addSubview:_cameraView];
  
    UIView *test = [[UIView alloc] initWithFrame: CGRectMake(0, 0, SCREEN_WIDTH, 100)];
    test.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:test];
    
//  if (_frontDevice) {
//    _switchCameraButton = [[QRCameraSwitchButton alloc] init];
//    [_switchCameraButton setTranslatesAutoresizingMaskIntoConstraints:false];
//    [_switchCameraButton addTarget:self action:@selector(switchCameraAction:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:_switchCameraButton];
//  }
  
  self.cancelButton                                       = [[UIButton alloc] init];
  _cancelButton.translatesAutoresizingMaskIntoConstraints = NO;
  [_cancelButton setTitle:cancelButtonTitle forState:UIControlStateNormal];
  [_cancelButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
  [_cancelButton addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
  [self.view addSubview:_cancelButton];
    
    _lblCount = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, SCREEN_WIDTH, 100)];
    _lblCount.text = [NSString stringWithFormat: @"%i/50", _scanCount];
    _lblCount.textColor = COLOUR_WHITE;
    _lblCount.textAlignment = NSTextAlignmentCenter;
    _lblCount.font = FONT_B1;
    [_lblCount sizeToFit];
    [_lblCount setTranslatesAutoresizingMaskIntoConstraints:false];
    [self.view addSubview:_lblCount];
    
    _lblScanTitle = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, SCREEN_WIDTH, 100)];
    _lblScanTitle.text = LOCALIZATION(C_DECAL_TITLE);
    _lblScanTitle.textColor = COLOUR_WHITE;
    _lblScanTitle.textAlignment = NSTextAlignmentCenter;
    _lblScanTitle.font = FONT_B1;
    _lblScanTitle.numberOfLines = 0;
    [_lblScanTitle sizeToFit];
    [_lblScanTitle setTranslatesAutoresizingMaskIntoConstraints:false];
    [self.view addSubview:_lblScanTitle];
}

- (void)setupAutoLayoutConstraints
{
  NSDictionary *views = NSDictionaryOfVariableBindings(_lblCount,_lblScanTitle, _cameraView, _cancelButton);
  
  [self.view addConstraints:
   [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_cameraView][_cancelButton(40)]-25-|" options:0 metrics:nil views:views]];
  [self.view addConstraints:
   [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_cameraView]|" options:0 metrics:nil views:views]];
  [self.view addConstraints:
   [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_cancelButton]-|" options:0 metrics:nil views:views]];

    [self.view addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_lblCount]-15-[_lblScanTitle]-20-[_cancelButton]" options:0 metrics:nil views:views]];
    [self.view addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[_lblCount]-20-|" options:0 metrics:nil views:views]];
    [self.view addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[_lblScanTitle]-20-|" options:0 metrics:nil views:views]];
    
  if (_switchCameraButton) {
    NSDictionary *switchViews = NSDictionaryOfVariableBindings(_switchCameraButton);
    
    [self.view addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_switchCameraButton(50)]" options:0 metrics:nil views:switchViews]];
    [self.view addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_switchCameraButton(70)]|" options:0 metrics:nil views:switchViews]];
  }
}

- (void)setupAVComponents
{
  self.defaultDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
  
  if (_defaultDevice) {
    self.defaultDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:_defaultDevice error:nil];
    self.metadataOutput     = [[AVCaptureMetadataOutput alloc] init];
    self.session            = [[AVCaptureSession alloc] init];
    self.previewLayer       = [AVCaptureVideoPreviewLayer layerWithSession:self.session];
    
//    for (AVCaptureDevice *device in [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo]) {
//      if (device.position == AVCaptureDevicePositionFront) {
//        self.frontDevice = device;
//      }
//    }
//    
//    if (_frontDevice) {
//      self.frontDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:_frontDevice error:nil];
//    }
  }
    // to check if camera is available
    AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
       NSError *error = nil;
       AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];

       if(!input) {
           // running on simulator or something else without camera
           NSLog(@"camera is not found");
       }
       else {
            NSLog(@"camera is  found");
           // camera exists, you can alert
       }
    
    
}

- (void)configureDefaultComponents
{
  [_session addOutput:_metadataOutput];
  
  if (_defaultDeviceInput) {
    [_session addInput:_defaultDeviceInput];
  }
  
  [_metadataOutput setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
  if ([[_metadataOutput availableMetadataObjectTypes] containsObject:AVMetadataObjectTypeQRCode]) {
    [_metadataOutput setMetadataObjectTypes:@[ AVMetadataObjectTypeQRCode ]];
  }
  [_previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
  [_previewLayer setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
  
  if ([_previewLayer.connection isVideoOrientationSupported]) {
    _previewLayer.connection.videoOrientation = [[self class] videoOrientationFromInterfaceOrientation:self.interfaceOrientation];
  }
}

- (void)switchDeviceInput
{
  if (_frontDeviceInput) {
    [_session beginConfiguration];
    
    AVCaptureDeviceInput *currentInput = [_session.inputs firstObject];
    [_session removeInput:currentInput];
    
    AVCaptureDeviceInput *newDeviceInput = (currentInput.device.position == AVCaptureDevicePositionFront) ? _defaultDeviceInput : _frontDeviceInput;
    [_session addInput:newDeviceInput];
    
    [_session commitConfiguration];
  }
}

#pragma mark - Catching Button Events

- (void)cancelAction:(UIButton *)button
{
  [self stopScanning];
  
//  if (_completionBlock) {
//    _completionBlock(nil);
//  }
  
  if (_delegate && [_delegate respondsToSelector:@selector(readerDidCancel:)]) {
    [_delegate readerDidCancel:self];
  }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)switchCameraAction:(UIButton *)button
{
  [self switchDeviceInput];
}

#pragma mark - Controlling Reader

- (void)startScanning;
{
  if (![self.session isRunning]) {
    [self.session startRunning];
  }
}

- (void)stopScanning;
{
  if ([self.session isRunning]) {
    [self.session stopRunning];
  }
}

#pragma mark - AVCaptureMetadataOutputObjects Delegate Methods

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
  for(AVMetadataObject *current in metadataObjects) {
    if ([current isKindOfClass:[AVMetadataMachineReadableCodeObject class]]
        && [current.type isEqualToString:AVMetadataObjectTypeQRCode]) {
      NSString *scannedResult = [(AVMetadataMachineReadableCodeObject *) current stringValue];
      
      if (_completionBlock) {
        _completionBlock(scannedResult);
      }
      
      if (_delegate && [_delegate respondsToSelector:@selector(reader:didScanResult:)]) {
        [_delegate reader:self didScanResult:scannedResult];
      }
      
      break;
    }
  }
}

#pragma mark - Checking the Metadata Items Types

+ (BOOL)isAvailable
{
  @autoreleasepool {
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    if (!captureDevice) {
      return NO;
    }
    
    NSError *error;
    AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!deviceInput || error) {
      return NO;
    }
    
    AVCaptureMetadataOutput *output = [[AVCaptureMetadataOutput alloc] init];
    
    if (![output.availableMetadataObjectTypes containsObject:AVMetadataObjectTypeQRCode]) {
      return NO;
    }
    
    return YES;
  }
}

@end
