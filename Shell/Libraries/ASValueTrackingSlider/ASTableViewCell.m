//
//  ASTableViewCell.m
//  Shell
//
//  Created by Jeremy Lua on 10/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "ASTableViewCell.h"

@interface ASTableViewCell () <ASValueTrackingSliderDelegate>
@end

@implementation ASTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    self.slider.delegate = self;
    self.slider.popUpViewCornerRadius = 0;
    [self.slider setMaxFractionDigitsDisplayed:0];
    self.slider.minimumTrackTintColor = COLOUR_RED;
    self.slider.maximumTrackTintColor = COLOUR_LIGHTGREY;
//    self.slider.popUpViewColor = COLOUR_RED;
//    self.slider.textColor = COLOUR_WHITE;
    self.slider.font = FONT_B3;
    self.slider.autoAdjustTrackColor = NO;
    self.slider.popUpViewWidthPaddingFactor = 1.2;
    self.slider.popUpViewHeightPaddingFactor = 1.5;
//    [self.slider showPopUpViewAnimated: NO];
    
    //hide popup
    self.slider.textColor = [UIColor clearColor];
    self.slider.popUpViewColor = [UIColor clearColor];
}

- (void)sliderWillDisplayPopUpView:(ASValueTrackingSlider *)slider;
{
    //do nothing
    [self.superview bringSubviewToFront:self];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
