//
//  ASTableViewCell.h
//  Shell
//
//  Created by Jeremy Lua on 10/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASValueTrackingSlider.h"
#import "Constants.h"

@interface ASTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet ASValueTrackingSlider *slider;
@end
