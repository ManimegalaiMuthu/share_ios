//
//  UICountingLabel+Count.h
//  JTIStarPartners
//
//  Created by Shekhar  on 9/1/15.
//  Copyright (c) 2015 Edenred. All rights reserved.
//

#import "UICountingLabel.h"

@interface UICountingLabel (Count)

- (void)addCountingAnimationwithNumber:(int)number;

@end
