//
//  NSMutableAttributedString+NativeFont.m
//  JTIStarPartners
//
//  Created by Shekhar  on 9/4/15.
//  Copyright (c) 2015 Edenred. All rights reserved.
//

#import "NSString+HTMLNativeFont.h"
#import "Constants.h"

@implementation NSString (HTMLNativeFont)

//- (NSMutableAttributedString *)getHTMLWithNativeFont
//{
//
//   NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithData:[self dataUsingEncoding:NSUTF8StringEncoding]
//                                                                            options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
//                                                                                      NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
//                                                                 documentAttributes:nil error:nil];
//    [attrString beginEditing];
//    [attrString enumerateAttribute:NSFontAttributeName inRange:NSMakeRange(0, attrString.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
//        if (value) {
//            UIFont *oldFont = (UIFont *)value;
//            NSLog(@"%@",oldFont.fontName);
//            
//            /*----- Remove old font attribute -----*/
//            [attrString removeAttribute:NSFontAttributeName range:range];
//            //replace your font with new.
//            /*----- Add new font attribute -----*/
//            if ([oldFont.fontName isEqualToString:@"TimesNewRomanPSMT"])
//                [attrString addAttribute:NSFontAttributeName value:FONTREGULAR(12) range:range];
//            else if([oldFont.fontName isEqualToString:@"TimesNewRomanPS-BoldMT"])
//                [attrString addAttribute:NSFontAttributeName value:FONTBOLD(12) range:range];
//            else if([oldFont.fontName isEqualToString:@"TimesNewRomanPS-ItalicMT"])
//                [attrString addAttribute:NSFontAttributeName value:FONTITALIC(12) range:range];
//            else if([oldFont.fontName isEqualToString:@"TimesNewRomanPS-BoldItalicMT"])
//                [attrString addAttribute:NSFontAttributeName value:FONTBOLDITALIC(12) range:range];
//            else
//                [attrString addAttribute:NSFontAttributeName value:FONTREGULAR(12) range:range];
//        }
//    }];
//    [attrString endEditing];
//    return attrString;
//
//}

@end
