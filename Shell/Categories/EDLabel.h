//
//  EDLabel.h
//  iEdenred
//
//  Created by Shekhar  on 13/8/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDLabel : UILabel

@property (nonatomic, assign) UIEdgeInsets edgeInsets;

@end
