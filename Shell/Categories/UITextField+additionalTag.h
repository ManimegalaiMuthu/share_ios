//
//  UITextField+additionalTag.h
//  JTIStarPartners
//
//  Created by Shekhar  on 27/1/15.
//  Copyright (c) 2015 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <objc/runtime.h>

@interface UITextField (additionalTag)

@property (nonatomic, strong) NSString *additionalTag;

@end
