//
//  UITextField+Extended.h
//  JTIStarPartners
//
//  Created by Shekhar  on 29/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Extended)

@property(retain, nonatomic)UITextField* nextTextField;

@end