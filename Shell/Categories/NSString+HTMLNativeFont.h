//
//  NSMutableAttributedString+NativeFont.h
//  JTIStarPartners
//
//  Created by Shekhar  on 9/4/15.
//  Copyright (c) 2015 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (HTMLNativeFont)

- (NSMutableAttributedString *)getHTMLWithNativeFont;

@end
