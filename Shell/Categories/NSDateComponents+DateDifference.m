//
//  NSDateComponents+DateDifference.m
//  ManpowerManagement
//
//  Created by Shekhar  on 7/4/15.
//  Copyright (c) 2015 Myaango. All rights reserved.
//

#import "NSDateComponents+DateDifference.h"

@implementation NSDateComponents (DateDifference)

- (NSUInteger)getDifferenceInDaysBetweenDate1:(NSString *)date1 andDate2:(NSString *)date2
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *startDate = [formatter dateFromString:date1];
    NSDate *endDate = [formatter dateFromString:date2];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];

    return components.day;
}

+ (NSInteger)getDifferenceInDaysBetweenNSDate1:(NSDate *)date1 andDate2:(NSDate *)date2
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:date1];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:date2];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

@end
