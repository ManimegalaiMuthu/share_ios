//
//  NSString+OccurrenceCount.h
//  JTIStarPartners
//
//  Created by Shekhar  on 25/3/15.
//  Copyright (c) 2015 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (OccurrenceCount)

- (NSUInteger)occurrenceCountOfCharacter:(UniChar)character;

@end
