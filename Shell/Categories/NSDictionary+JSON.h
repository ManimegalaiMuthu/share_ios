//
//  NSDictionary+JSON.h
//  JTIStarPartners
//
//  Created by Shekhar on 20/11/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (JSON)
/**
  Get Json String
 */
-(NSString*)jsonString;

@end
