//
//  NSObject+DataTypeConversion.h
//  JTIStarPartners
//
//  Created by Shekhar  on 10/4/15.
//  Copyright (c) 2015 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    kDouble = 0,
    kFloat = 1
}DataType;

@interface NSString (DataTypeConversion)

+ (NSString *)getStringValueFrom:(id)data andType:(DataType)type;

@end
