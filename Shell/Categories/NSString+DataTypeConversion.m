//
//  NSObject+DataTypeConversion.m
//  JTIStarPartners
//
//  Created by Shekhar  on 10/4/15.
//  Copyright (c) 2015 Edenred. All rights reserved.
//

#import "NSString+DataTypeConversion.h"

@implementation NSString (DataTypeConversion)

+ (NSString *)getStringValueFrom:(id)data andType:(DataType)type
{
    NSString *str = [[NSString alloc]init];
    NSNumber *number = [[NSNumber alloc]init];
    switch (type) {
        case kDouble:
        {
           number = [NSNumber numberWithDouble:[data doubleValue]];
        }
            break;
        case kFloat:
        {
            number = [NSNumber numberWithDouble:[data floatValue]];
        }
            break;
            
        default:
            break;
    }
    str = [number stringValue];
    return str;
}

@end
