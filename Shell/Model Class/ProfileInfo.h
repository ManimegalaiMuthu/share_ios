//
//  ProfileInfo.h
//  Shell
//
//  Created by Ankita Chhikara on 25/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface ProfileInfo : NSObject


@property NSString *companyName;
@property NSString *salutation; //save keycode?
@property NSString *emailAddress;
@property NSInteger bonusPoint;

@property NSString *firstName;
@property NSString *fullName;
@property NSString *lastName;
@property NSString *mobileNumber;

@property CGFloat profileCompleteness;
@property NSString *profileDesc;
@property NSString *profileImage; //url as string
@property NSString *profileType; //bit value
@property NSString *rank; //e.g.string "#3/5"

//FA Flags
@property NSString *faShareCode;
@property NSString *faShareQRCode;
@property BOOL hasCSHasFABadge;
@property BOOL hasCSHasFAPerformance;

@property BOOL hasMyRewards;
@property NSString *shareCode;
@property BOOL redeemOnly;
@property BOOL canInviteTrade;

@property BOOL hasOrdering;
@property BOOL hasRegisterProductCode;
@property BOOL hasRegisterProductCode_V2;


@property BOOL hasCustomerRedemption;
@property BOOL hasCustomerRedemptionDSR;
@property BOOL hasSearchVehicleID;
@property BOOL hasManageMyStaff;

@property BOOL hasParticipatingProducts;
@property BOOL hasParticipatingAllProducts;
@property BOOL hasParticipatingShareProducts;


@property BOOL hasWorkshopPerformance;
@property BOOL hasMYPerformance;
@property BOOL hasMYPoints;
@property BOOL hasAncillaryOffers;

@property BOOL IsPerfOpt1; //old My Performance
@property BOOL IsPerfOpt2; //new Summary

@property BOOL enableCustomerRedemption;

@property BOOL canEditAddress;
@property BOOL hasAddNewWorkshop;

@property BOOL canEditShippingAddress;
@property NSInteger unReadPushMessage;

@property BOOL isEnableBooking;
@property BOOL isEnableSurvey;
@property NSString *surveyId;

@property BOOL showSurveyPp;

@property BOOL hasTOPoint;

@property BOOL hasInviteConsumer;

@property BOOL hasWorkshopProfiling;

@property BOOL hasMyLibrary;

@property BOOL hasOrdering_V2;
@property BOOL hasShowScanSearchVehicle;

//decal
@property BOOL hasTagDecalToWorkshop;
@property BOOL hasDecal;

//Quick Links flag
@property BOOL hasQLHasOrdering_V2;
@property BOOL hasQLHasAsset;
@property BOOL hasQLHasOneplusOne;
@property BOOL hasQLHasSearchVehicleID;
@property BOOL hasQLHasManageWorkshop;
@property BOOL hasQLHasRegisterProductCode;
@property BOOL hasQLHasCustomerRedemption;
@property BOOL hasQLHasScanDecal;
@property BOOL hasQLHasInviteConsumer;
@property BOOL hasQLHasRewards;

//carousel
@property BOOL hasCSHasMyProfile;
@property BOOL hasCSHasPerformance;

//Russia flags
@property BOOL hasDRWorkshopPerformance;
@property BOOL hasPartnersOfShellClub;
@property BOOL hasQLHasPartnersOfShellClub;
@property BOOL hasCSHasMyPoints;
@property BOOL hasCSHasPartnersOfShellClub;
@property BOOL hasCSHasInventorySpeedometer;

//@property BOOL hasCSHasInventorySpeedometer;
@property BOOL hasOnePlusOne;

//ID MCO
//@property BOOL hasDSRManageStaff;

//Thai Retail Flags
@property BOOL hasCSHasOutletPerformance;
@property BOOL isManageStaffOpt1;
@property BOOL isManageStaffOpt2;
@property BOOL isMyPointsOpt1;
@property BOOL isMyPointsOpt2;
@property BOOL isParticipatingProductsOpt1;
@property BOOL isParticipatingProductsOpt2;
@property BOOL isParticipatingProductsOpt3;

//India addition
@property BOOL hasCSHasOutletPerformanceV2;

@property BOOL hasQLHasParticipatingProducts;

@property BOOL hasLeaderBoard;
@property BOOL hasRUPerformance;
@property BOOL hasQLHasRUPerformance;
@property BOOL hasCSHasDSRPartnersOfShellClub;
@property BOOL hasQLHasWorkShopOffer;
@property BOOL hasQLCashIncentive;
@property BOOL hasCashIncentive;

@property BOOL hasMultipleSubmissionOilChange;
@property BOOL isRetail;

//ID Loyalty
//For TO Carousel
@property BOOL hasCSHasLoyaltyProgram;
@property BOOL hasCSHasLoyaltyBD;
@property BOOL hasLoyaltyProgram;
@property BOOL hasQLHasLoyaltyProgram;

//India
@property BOOL hasQLHasMechanicPerformance;
@property BOOL hasQLHasManageStaff;
@property BOOL hasManageMechanicPerformance;
//India Badge
@property BOOL hasCSHasBadge;
@property NSString *shareQRCode;
@property NSString *shareTagCode;

//India Oil Walkthrough
@property BOOL hasOilChangeWalkThrough;


//is account deactivated, TO/TI
@property BOOL isAccountDeActive;

@property BOOL hasWorkshopPerformanceView;
@property BOOL hasCSHasTradePromotion;

//For Customer Redemption Btn
@property BOOL hasCustomerRedemptionBtn;


//BIWS Tiering
@property NSString *companyCertTierImage;
@property NSString *companyCertTierText;

@property BOOL hasQLOilChangeWalkThrough;
@property BOOL hasCSHasCVBooster;
@property BOOL hasCSHasAgri;

//To Skip Delivery address in rewards
@property BOOL isToSkipDeliveryAddress;
// show/hide ‘Current Mileage’ textbox in the Register Oil Change
@property BOOL hasCurrentMileage;
@property BOOL hasCSHasLebaranPromotion;
@property BOOL hasCSHasHX8Promotion;

//SDA
@property BOOL hasDistributorAcademy;
@property BOOL hasQLHasDistributorAcademy;
@property NSString *distributorAcademyURL;

//SWA
@property BOOL hasWorkshopAcademy;
@property BOOL hasQLHasWorkshopAcademy;
@property NSString *workshopAcademyURL;

@property BOOL hasVehicleType; //For VN
@property NSInteger businessType; //For RU

@property BOOL hasAcceptPrivacyPolicy;
@property BOOL hasPendingWorkshopList;
@property BOOL hasBtnUpdate;
@property BOOL hasBtnDeactivate;

@property BOOL hasQLHasDecalActivation;
@property BOOL hasQLHasBottleSale;
@property BOOL hasBottleSale;

@property NSString *nextExpiryDate;
@property NSInteger nextExpiryPoints;

@property BOOL hasShowCoinPopup;
@property BOOL hasShowTaxPopup;


@property BOOL hasLubeMatch;
@property BOOL hasQLHasLubeMatch;

//TR Module
@property BOOL hasCSHasEarnNReward;
@property BOOL hasCSMYHasEarnNReward;
@property BOOL hasCSHasTHIncentive;
@property BOOL hasCSHasTHOrderBreakdown;

@property BOOL hasCSHasGamification;
@property BOOL gamification_HasCheckedInToday;
@property NSInteger gamification_Milestone;

@property BOOL hasCSHasTiering;
@property BOOL hasPCMOGamification;
@property BOOL hasPilotPromotion;

@property NSString *loyaltyTHAsOfDate;

//Moscow
@property BOOL hasAddNewRegWorkshop;
@property BOOL hasInventoryManagement;
@property BOOL hasQLHasInventoryManagement;
// added Sept 2020
@property BOOL HasInventory;

-(id)initWithData:(NSDictionary *)userData;
-(NSDictionary *) getJSON;
@end
