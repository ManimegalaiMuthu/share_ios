//
//  Member.h
//  Shell
//
//  Created by Ankita Chhikara on 26/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Member : NSObject 

@property(nonatomic,strong) NSString *userID;
@property(nonatomic,strong) NSString *DSRCode;
@property(nonatomic,strong) NSString *userPassword;

@property NSInteger salutation;
@property(nonatomic,strong) NSString *firstName;
@property(nonatomic,strong) NSString *lastName; //surname

@property(nonatomic,strong) NSString *countryCode;
@property(nonatomic,strong) NSString *mobileNumber;
@property(nonatomic,strong) NSString *emailAddress;
@property(nonatomic,strong) NSString *gender;
@property(nonatomic,strong) NSString *contactPreference;

@property(nonatomic,strong) NSString *registrationType;
@property(nonatomic,strong) NSString *registrationMode;
@property(nonatomic,strong) NSString *registrationRole;

@property(nonatomic,strong) NSString *dobMonth;
@property(nonatomic,strong) NSString *dobDay;

@property(nonatomic,strong) NSString *leadID;
@property(nonatomic,strong) NSString *tradeID;

@property NSInteger staffType;
@property(nonatomic,strong) NSString *faShareCode;


-(id)initWithData:(NSDictionary *)userData;
-(NSDictionary *) getJSON;
@end
