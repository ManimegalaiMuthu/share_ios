//
//  ProfileInfo.m
//  Shell
//
//  Created by Ankita Chhikara on 25/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "ProfileInfo.h"

@implementation ProfileInfo
#define kCompanyName            @"CompanyName"
#define kSalutation             @"Salutation"
#define kEmailAddress           @"EmailAddress"
#define kBonusPoint             @"BonusPoint"

#define kFirstName              @"FirstName"
#define kFullName               @"FullName"
#define kLastName               @"LastName"
#define kMobileNumber           @"MobileNumber"

//FA Flags
#define kFAShareCode            @"FAShareCode"
#define kFAShareQRCode          @"FAShareQRCode"
#define kCSHasFABadge           @"CSHasFABadge"
#define kCSHasFAPerformance     @"CSHasFAPerformance"

#define kProfileDesc            @"ProfileDesc"
#define kProfileImage           @"ProfileImage"
#define kProfileType            @"ProfileType"
#define kRank                   @"Rank"
#define kProfileCompleteness    @"ProfileCompleteness"

#define kRewardsStatus          @"RewardsStatus"
#define kShareCode              @"ShareCode"
#define kRedeemOnly             @"RedeemOnly"
#define kCanInviteTrade         @"CanInviteTrade"

#define kHasOrdering            @"HasOrdering"
#define kHasRegisterProductCode @"HasRegisterProductCode"
#define kHasRegisterProductCode_V2 @"HasRegisterProductCode_V2"



#define kHasCustomerRedemption   @"HasCustomerRedemption"
#define kHasCustomerRedemptionDSR   @"HasCustomerRedemptionDSR"
#define kHasSearchVehicleID     @"HasSearchVehicleID"
#define kHasManageMyStaff       @"HasManageMyStaff"

#define kHasParticipatingProducts @"HasParticipatingProducts"
#define kHasParticipatingShareProducts @"HasParticipatingShareProducts"
#define kHasParticipatingAllProducts @"HasParticipatingAllProducts"


#define kHasWorkshopPerformance @"HasWorkshopPerformance"
#define kHasMYPerformance     @"HasMYPerformance"
#define kHasMYPoints          @"HasMYPoints"
#define kHasAncillaryOffers    @"HasAncillaryOffers"

#define kEnableCustomerRedemption   @"EnableCustomerRedemption"

#define kCanEditAddress          @"CanEditAddress"
#define kHasAddNewWorkshop      @"HasAddNewWorkshop"

#define kCanEditShippingAddress      @"CanEditShippingAddress"
#define kUnReadPushMessage          @"UnReadPushMessage"


#define kIsEnableSurvey          @"IsEnableSurvey"
#define kIsEnableBooking         @"IsEnableBooking"
#define kSurveyID                @"SurveyID"

#define kShowSurveyPP                @"ShowSurveyPP"

#define kHasTOPoint                @"HasTOPoint"

#define kHasInviteConsumer          @"HasInviteConsumer"

#define kHasWorkshopProfiling          @"HasWorkshopProfiling"

#define kHasAsset           @"HasAsset"

#define kHasTagDecalToWorkshop  @"HasTagDecalToWorkshop" //Appears true only for the trade owner for Country: Thailand

#define kHasDecal               @"HasDecal" //Appears true for both the trade owner and the Mechanic for Country : Thailand

#define kHasOrdering_V2         @"HasOrdering_V2"

#define kShowScanSearchVehicle         @"ShowScanSearchVehicle"

//Quick Links flag
#define kQLHasOrdering_V2 @"QLHasOrdering_V2"
#define kQLHasAsset @"QLHasAsset"
#define kQLHasOnePlusOne @"QLHasOnePlusOne"
#define kQLHasSearchVehicleID @"QLHasSearchVehicleID"
#define kQLHasManageWorkshop @"QLHasManageWorkshop"
#define kQLHasRegisterProductCode @"QLHasRegisterProductCode"
#define kQLHasCustomerRedemption @"QLHasCustomerRedemption"
#define kQLHasScanDecal @"QLHasScanDecal"
#define kQLHasInviteConsumer @"QLHasInviteConsumer"
#define kQLHasRewards @"QLHasRewards"

#define kCSHasPerformance @"CSHasPerformance"

//Russia flags
#define kHasPartnersOfShellClub @"HasPartnersOfShellClub"
#define kHasOnePlusOne @"HasOnePlusOne"
#define kQLHasPartnersOfShellClub @"QLHasPartnersOfShellClub"

#define kCSHasMyPoints @"CSHasMyPoints"
#define kCSHasMyProfile @"CSHasMyProfile"
#define kCSHasPartnersOfShellClub @"CSHasPartnersOfShellClub"
#define kCSHasInventorySpeedometer @"CSHasInventorySpeedometer"
#define kHasDRWorkshopPerformance @"HasDRWorkshopPerformance"
#define kCSHasInventorySpeedometer @"CSHasInventorySpeedometer"

#define kCSHasOutletPerformance @"CSHasOutletPerformance"
#define kCSHasOutletPerformanceV2 @"CSHasOutletPerformanceV2"
#define kIsManageStaffOpt1 @"IsManageStaffOpt1"
#define kIsManageStaffOpt2 @"IsManageStaffOpt2"
#define kIsMyPointsOpt1 @"IsMyPointsOpt1"
#define kIsMyPointsOpt2 @"IsMyPointsOpt2"
#define kIsParticipatingProductsOpt1 @"IsParticipatingProductsOpt1"
#define kIsParticipatingProductsOpt2 @"IsParticipatingProductsOpt2"
#define kIsParticipatingProductsOpt3 @"IsParticipatingProductsOpt3"

#define kQLHasParticipatingProducts @"QLHasParticipatingProducts"

#define kHasLeaderBoard @"HasLeaderBoard"

#define kHasRUPerformance               @"HasRUPerformance"
#define kQLHasRUPerformance             @"QLHasRUPerformance"
#define kCSHasDSRPartnersOfShellClub    @"CSHasDSRPartnersOfShellClub"
#define kQLHasWorkShopOffer             @"QLHasWorkShopOffer"
#define kQLCashIncentive             @"QLCashIncentive"
#define kHasCashIncentive               @"HasCashIncentive"

#define kHasMultipleSubmissionOilChange @"HasMultipleSubmissionOilChange"
#define kIsRetail @"IsRetail"

//#define kHasDSRManageStaff @"HasDSRManageStaff"
//ID Loyalty
//For TO Carousel
//Changing to V2 so that loyalty is not available in versions b4 1.1.9
#define kCSHasLoyaltyProgram @"CSHasLoyaltyProgramV2"
#define kCSHasLoyaltyBD @"CSHasLoyaltyBDV2"
#define kHasLoyaltyProgram @"HasLoyaltyProgramV2"
#define kQLHasLoyaltyPrograms @"QLHasLoyaltyProgramV2"

#define kHasQLHasMechanicPerformance    @"QLHasMechanicPerformance"
#define kHasQLHasManageStaff            @"QLHasManageStaff"
#define kHasManageMechanicPerformance   @"HasManageMechanicPerformance"

#define kCSHasBadge @"CSHasBadge"
#define kShareQRCode @"ShareQRCode"
#define kShareTagCode @"ShareTagCode"

#define kHasOilChangeWalkThrough @"HasOilChangeWalkThrough"

#define kIsAccountDeActive @"IsAccountDeActive"
#define kIsPerfOpt1 @"IsPerfOpt1"
#define kIsPerfOpt2 @"IsPerfOpt2"
#define kHasWorkshopPerformanceView @"HasWorkshopPerformanceView"
#define kCSHasTradePromotion    @"CSHasTradePromotion"

#define kHasCustomerRedemptionBtn   @"HasCustomerRedemptionBtn"

//BIWS Tiering
#define kCompanyCertTierImage @"CompanyCertTierImage"
#define kCompanyCertTierText @"CompanyCertTierText"
#define kQLOilChangeWalkThrough @"QLOilChangeWalkThrough"

#define kCSHasCVBooster @"CSHasCVBooster"
#define kCSHasAgri @"CSHasAgri"

#define kIsToSkipDeliveryAddress @"IsToSkipDeliveryAddress"
#define kHasCurrentMileage @"HasCurrentMileage"
#define kCSHasLebaranPromotion @"CSHasLebaranPromotion"
#define kCSHasHX8Promotion     @"CSHasHX8Promotion"

#define kHasDistributorAcademy     @"HasDistributorAcademy"
#define kQLHasDistributorAcademy   @"QLHasDistributorAcademy"
#define kDistributorAcademyURL     @"DistributorAcademyURL"

#define kHasWorkshopAcademy     @"HasWorkshopAcademy"
#define kQLHasWorkshopAcademy   @"QLHasWorkshopAcademy"
#define kWorkshopAcademyURL     @"WorkshopAcademyURL"

#define kHasVehicleType         @"HasVehicleType"
#define kBusinessType           @"BusinessType"

#define kAcceptPrivacyPolicy    @"AcceptPrivacyPolicy"
#define kHasPendingWorkshopList @"HasPendingWorkshopList"
#define kHasBtnUpdate           @"HasBtnUpdate"
#define kHasBtnDeactivate       @"HasBtnDeactivate"

#define kQLHasDecalActivation   @"QLHasDecalActivation"

#define kQLHasBottleSale        @"QLHasBottleSale"
#define kHasBottleSale          @"HasBottleSale"

//New profile
#define kNextExpiryDate          @"NextExpiryDate"
#define kNextExpiryPoints          @"NextExpiryPoints"

#define kShowCoinPopup          @"ShowCoinPopup"
#define kShowTaxPopup           @"ShowTaxPopup"

#define kHasLubeMatch           @"HasLubeMatch"
#define kQLHasLubeMatch         @"QLHasLubeMatch"

#define kCSHasEarnNReward       @"CSHasEarnNReward"
#define kCSMYHasEarnNReward     @"CSMYHasEarnNReward"
#define kCSHasTHIncentive       @"CSHasTHIncentive"
#define kCSHasTHOrderBreakdown  @"CSHasTHOrderBreakdown"

#define kCSHasGamification      @"CSHasGamification"
#define kHasCheckedInToday      @"HasCheckedInToday"
#define kMilestone              @"Milestone"

#define kCSHasTiering           @"CSHasTiering"
#define kCSHasPCMOGamification  @"CSHasPCMOGamification"
#define kCSHasPilotPromotion    @"CSHasPilotPromotion"
#define kTHAsOfDate             @"THAsOfDate"

//Moscow
#define kHasAddNewRegWorkshop           @"HasAddNewRegWorkshop"
#define kHasInventoryManagement         @"HasInventoryManagement"
#define kQLHasInventoryManagement       @"QLHasInventoryManagement"

// added sept 2020
#define kHasInventory         @"HasInventory"


-(id)init
{
    self = [super init];
    if (self)
    {
        self.companyName = @"";
        self.salutation = @"";
        self.emailAddress = @"";
        self.bonusPoint = 0;
        
        self.firstName = @"";
        self.fullName = @"";
        self.lastName = @"";
        self.mobileNumber = @"";
        
        self.faShareCode = @"";
        self.faShareQRCode = @"";
        self.hasCSHasFABadge = NO;
        
        self.profileDesc = @"";
        self.profileImage = @""; //url as string
        self.profileType = @""; //bit value
        self.rank = @""; //e.g.string "#3/5"
        self.profileCompleteness = 0;
        self.hasMyRewards = NO;
        self.shareCode = @"";
        self.redeemOnly = NO;
        self.canInviteTrade = NO;
        
        self.hasOrdering = NO;
        self.hasRegisterProductCode = NO;
        self.hasRegisterProductCode_V2 = NO;
        self.hasCustomerRedemption = NO;
        self.hasCustomerRedemptionDSR = NO;
        self.hasSearchVehicleID = NO;
        self.hasManageMyStaff = NO;
        
        self.hasParticipatingProducts = NO;
        self.hasParticipatingAllProducts = NO;
        self.hasParticipatingShareProducts = NO;
        
        
        self.hasWorkshopPerformance = NO;
        self.hasMYPerformance = NO;
        self.hasMYPoints = NO;
        
        self.enableCustomerRedemption = NO;
        
        self.canEditAddress = NO;
        self.hasAddNewWorkshop = NO;
        
        self.canEditShippingAddress = NO;
        
        self.unReadPushMessage = 0;
        
        self.isEnableSurvey = NO;
        self.isEnableBooking = NO;
        self.surveyId = @"";
        
        self.showSurveyPp = NO;
        
        self.hasTOPoint = NO;
        
        self.hasInviteConsumer = NO;
        self.hasWorkshopProfiling = NO;
        
        self.hasMyLibrary               = NO;
        
        self.hasTagDecalToWorkshop = NO;
        self.hasDecal = NO;
        self.hasOrdering_V2 = NO;
        self.hasShowScanSearchVehicle = NO;
        
        //Quick Links flag
        self.hasQLHasOrdering_V2 = NO;
        self.hasQLHasAsset = NO;
        self.hasQLHasOneplusOne = NO;
        self.hasQLHasSearchVehicleID = NO;
        self.hasQLHasManageWorkshop = NO;
        self.hasQLHasRegisterProductCode = NO;
        self.hasQLHasCustomerRedemption = NO;
        self.hasQLHasScanDecal = NO;
        self.hasQLHasInviteConsumer = NO;
        self.hasQLHasRewards = NO;
        
        self.hasPartnersOfShellClub = NO;
        self.hasOnePlusOne = NO;
        self.hasQLHasPartnersOfShellClub = NO;
        self.hasCSHasMyPoints = NO;
        self.hasCSHasMyProfile = NO;
        self.hasCSHasPartnersOfShellClub = NO;
        self.hasCSHasInventorySpeedometer = NO;
        self.hasCSHasPerformance = NO;
        self.hasCSHasInventorySpeedometer = NO;

        self.hasDRWorkshopPerformance = NO;
        self.hasCSHasOutletPerformance = NO;
        self.hasCSHasOutletPerformanceV2 = NO;
        
        self.isManageStaffOpt1 = NO;
        self.isManageStaffOpt2 = NO;
        self.isMyPointsOpt1 = NO;
        self.isMyPointsOpt2 = NO;
        
//        self.hasDSRManageStaff = NO;
        self.hasQLHasParticipatingProducts = NO;
        
        self.isParticipatingProductsOpt1 = NO;
        self.isParticipatingProductsOpt2 = NO;
        self.isParticipatingProductsOpt3 = NO;

        self.hasLeaderBoard = NO;
        self.hasRUPerformance = NO;
        self.hasQLHasRUPerformance = NO;
        self.hasCSHasDSRPartnersOfShellClub = NO;
        self.hasQLHasWorkShopOffer = NO;
        self.hasQLCashIncentive = NO;
        self.hasCashIncentive = NO;
        
        self.hasMultipleSubmissionOilChange = NO;
        self.isRetail = NO;
        
        self.hasQLHasMechanicPerformance = NO;
        self.hasQLHasManageStaff = NO;
        self.hasManageMechanicPerformance = NO;
        
        self.hasCSHasBadge = NO;
        self.shareQRCode = @"";
        self.shareTagCode = @"";
        
        self.hasOilChangeWalkThrough = NO;
        
        self.isAccountDeActive = NO;

        self.hasCSHasLoyaltyBD = NO;
        self.hasCSHasLoyaltyProgram = NO;
        self.hasQLHasLoyaltyProgram = NO;
        self.hasLoyaltyProgram = NO;

        self.IsPerfOpt1 = NO;
        self.IsPerfOpt2 = NO;
        
        self.hasWorkshopPerformanceView = NO;
        self.hasCSHasTradePromotion = NO;
        
        self.hasCustomerRedemptionBtn = NO;
        
        self.companyCertTierText = @"";
        self.companyCertTierImage = @"";
        
        self.hasQLOilChangeWalkThrough = NO;
        self.hasCSHasCVBooster = NO;
        self.hasCSHasAgri = NO;
        
        self.isToSkipDeliveryAddress = NO;
        self.hasCurrentMileage = NO;
        self.hasCSHasLebaranPromotion = NO;
        self.hasCSHasHX8Promotion = NO;
        
        self.hasDistributorAcademy = NO;
        self.hasQLHasDistributorAcademy = NO;
        self.distributorAcademyURL = @"";
        
        self.hasWorkshopAcademy = NO;
        self.hasQLHasWorkshopAcademy = NO;
        self.workshopAcademyURL = @"";
        
        self.hasVehicleType = NO;
        self.businessType = 0;
        
        self.hasAcceptPrivacyPolicy = NO;
        self.hasPendingWorkshopList = NO;
        self.hasBtnDeactivate = NO;
        self.hasBtnUpdate = NO;
        
        self.hasQLHasDecalActivation = NO;
        self.hasQLHasBottleSale = NO;
        self.hasBottleSale = NO;
        
        self.nextExpiryDate = @"";
        self.nextExpiryPoints = 0;
        
        self.hasShowTaxPopup = NO;
        self.hasShowCoinPopup = NO;
        
        self.hasLubeMatch = NO;
        self.hasQLHasLubeMatch = NO;

        self.hasCSHasEarnNReward = NO;
        self.hasCSMYHasEarnNReward = NO;

        self.hasAddNewRegWorkshop = NO;
        self.hasInventoryManagement = NO;
        self.hasQLHasInventoryManagement = NO;

        self.HasInventory = NO;

        self.hasCSHasTHIncentive = NO;
        self.hasCSHasTHOrderBreakdown = NO;

        self.hasCSHasGamification = NO;
        self.gamification_HasCheckedInToday = NO;
        self.gamification_Milestone = 0;

        self.hasCSHasTiering = NO;
        self.hasPCMOGamification = NO;
        self.hasPilotPromotion = NO;
        self.loyaltyTHAsOfDate = @"";

    }
    return self;
}

-(id)initWithData:(NSDictionary *)userData
{
    self = [super init];
    if (self) {
        self.companyName =          [userData objectForKey: kCompanyName];
        self.salutation =           [userData objectForKey: kSalutation]; //save keycode?
        self.emailAddress =         [userData objectForKey: kEmailAddress];
        self.bonusPoint =           [[userData objectForKey: kBonusPoint] intValue];
        
        self.firstName =            [userData objectForKey: kFirstName];
        self.fullName =             [userData objectForKey: kFullName];
        self.lastName =             [userData objectForKey: kLastName];
        self.mobileNumber =         [userData objectForKey: kMobileNumber];
        
        self.faShareQRCode =        [userData objectForKey:kFAShareQRCode];
        self.faShareCode =          [userData objectForKey:kFAShareCode];
        self.hasCSHasFABadge =      [[userData objectForKey:kCSHasFABadge]boolValue];
        self.hasCSHasFAPerformance = [[userData objectForKey:kCSHasFAPerformance]boolValue];
        
        
        //@property NSInteger *profileCompleteness = 30;
        self.profileDesc =          [userData objectForKey: kProfileDesc];
        self.profileImage =         [userData objectForKey: kProfileImage]; //url as string
        self.profileType =          [userData objectForKey: kProfileType]; //bit value
        self.rank =                 [userData objectForKey: kRank]; //e.g.string "#3/5"
        self.profileCompleteness =  [[userData objectForKey: kProfileCompleteness] floatValue]; //e.g.string "#3/5"
        
        self.hasMyRewards =         [[userData objectForKey: kRewardsStatus] boolValue];
        self.redeemOnly =           [[userData objectForKey: kRedeemOnly] boolValue];
        self.canInviteTrade =       [[userData objectForKey: kCanInviteTrade] boolValue];
        
        self.hasOrdering =              [[userData objectForKey: kHasOrdering] boolValue];
        self.hasRegisterProductCode =   [[userData objectForKey: kHasRegisterProductCode] boolValue];
        self.hasRegisterProductCode_V2 =   [[userData objectForKey: kHasRegisterProductCode_V2] boolValue];
        self.hasCustomerRedemption =     [[userData objectForKey: kHasCustomerRedemption] boolValue];
        self.hasCustomerRedemptionDSR =     [[userData objectForKey: kHasCustomerRedemptionDSR] boolValue];
        self.hasSearchVehicleID =       [[userData objectForKey: kHasSearchVehicleID] boolValue];
        self.hasManageMyStaff =         [[userData objectForKey: kHasManageMyStaff] boolValue];
        
        self.hasParticipatingProducts =         [[userData objectForKey: kHasParticipatingProducts] boolValue];
        self.hasParticipatingAllProducts =         [[userData objectForKey: kHasParticipatingAllProducts] boolValue];
        self.hasParticipatingShareProducts =         [[userData objectForKey: kHasParticipatingShareProducts] boolValue];
        
        
        self.hasWorkshopPerformance =         [[userData objectForKey: kHasWorkshopPerformance] boolValue];
        self.hasMYPerformance =         [[userData objectForKey: kHasMYPerformance] boolValue];
        self.hasMYPoints =         [[userData objectForKey: kHasMYPoints] boolValue];
        self.hasAncillaryOffers = [[userData objectForKey: kHasAncillaryOffers] boolValue];
        self.enableCustomerRedemption =     [[userData objectForKey: kEnableCustomerRedemption] boolValue];
        
        self.canEditAddress =       [[userData objectForKey: kCanEditAddress] boolValue];
        self.hasAddNewWorkshop =         [[userData objectForKey: kHasAddNewWorkshop] boolValue];
        
        self.canEditShippingAddress =       [[userData objectForKey: kCanEditShippingAddress] boolValue];
        
        self.shareCode = [userData objectForKey: kShareCode];
        
        self.isEnableBooking = [[userData objectForKey:kIsEnableBooking] boolValue];
        self.isEnableSurvey = [[userData objectForKey: kIsEnableSurvey] boolValue];
        self.surveyId = [userData objectForKey: kSurveyID];
        
        self.showSurveyPp = [[userData objectForKey: kShowSurveyPP] boolValue];
        
        self.hasTOPoint = [[userData objectForKey: kHasTOPoint] boolValue];
        
        self.hasInviteConsumer = [[userData objectForKey: kHasInviteConsumer] boolValue];
        
        self.hasTagDecalToWorkshop = [[userData objectForKey: kHasTagDecalToWorkshop] boolValue];
        
        self.hasDecal = [[userData objectForKey: kHasDecal] boolValue];
        
        self.hasMyLibrary = [[userData objectForKey: kHasAsset] boolValue];
        
        self.hasOrdering_V2 = [[userData objectForKey: kHasOrdering_V2] boolValue];
        
        self.hasShowScanSearchVehicle = [[userData objectForKey: kShowScanSearchVehicle] boolValue];
        
        self.hasWorkshopProfiling = [[userData objectForKey: kHasWorkshopProfiling] boolValue];
        
        if ([[userData objectForKey: kUnReadPushMessage] isKindOfClass: [NSNull class]])
            self.unReadPushMessage = 0;
        else
            self.unReadPushMessage = [[userData objectForKey: kUnReadPushMessage] intValue];
        
        if ([self.profileImage isKindOfClass: [NSNull class]])
        {
            self.profileImage = @"";
        }
        if ([self.shareCode isKindOfClass: [NSNull class]])
        {
            self.shareCode = @"";
        }
        
        
        if ([self.surveyId isKindOfClass: [NSNull class]])
        {
            self.surveyId = @"";
        }
        
        //Quick Links flag
        self.hasQLHasOrdering_V2 = [[userData objectForKey: kQLHasOrdering_V2] boolValue];
        self.hasQLHasAsset = [[userData objectForKey: kQLHasAsset] boolValue];
        self.hasQLHasOneplusOne = [[userData objectForKey: kQLHasOnePlusOne] boolValue];
        self.hasQLHasSearchVehicleID = [[userData objectForKey: kQLHasSearchVehicleID] boolValue];
        self.hasQLHasManageWorkshop = [[userData objectForKey: kQLHasManageWorkshop] boolValue];
        self.hasQLHasRegisterProductCode = [[userData objectForKey: kQLHasRegisterProductCode] boolValue];
        self.hasQLHasCustomerRedemption = [[userData objectForKey: kQLHasCustomerRedemption] boolValue];
        self.hasQLHasScanDecal = [[userData objectForKey: kQLHasScanDecal] boolValue];
        self.hasQLHasInviteConsumer = [[userData objectForKey: kQLHasInviteConsumer] boolValue];
        self.hasQLHasRewards = [[userData objectForKey: kQLHasRewards] boolValue];
     //   self.hasPartnersOfShellClub = [[userData objectForKey: kHasPartnersOfShellClub] boolValue];
        self.hasOnePlusOne = [[userData objectForKey:kHasOnePlusOne]boolValue];
    //    self.hasQLHasPartnersOfShellClub = [[userData objectForKey: kQLHasPartnersOfShellClub] boolValue];
        self.hasCSHasMyProfile = [[userData objectForKey: kCSHasMyProfile] boolValue];
        self.hasCSHasPartnersOfShellClub = [[userData objectForKey: kCSHasPartnersOfShellClub] boolValue];
        self.hasCSHasInventorySpeedometer = [[userData objectForKey: kCSHasInventorySpeedometer] boolValue];
        NSLog(@"HASPARTNER SHELL:%hhd",self.hasCSHasPartnersOfShellClub);



       // self.hasCSHasInventorySpeedometer =[[userData objectForKey:kCSHasInventorySpeedometer] boolValue];

        self.hasCSHasPerformance = [[userData objectForKey: kCSHasPerformance] boolValue];
        
        self.hasDRWorkshopPerformance = [[userData objectForKey: kHasDRWorkshopPerformance] boolValue];
        self.hasCSHasOutletPerformance = [[userData objectForKey:kCSHasOutletPerformance]boolValue];
        self.hasCSHasOutletPerformanceV2 = [[userData objectForKey:kCSHasOutletPerformanceV2]boolValue];
        
        self.isManageStaffOpt1 = [[userData objectForKey:kIsManageStaffOpt1] boolValue];
        self.isManageStaffOpt2 = [[userData objectForKey:kIsManageStaffOpt2] boolValue];
        self.isMyPointsOpt1 = [[userData objectForKey:kIsMyPointsOpt1] boolValue];
        self.isMyPointsOpt2 = [[userData objectForKey:kIsMyPointsOpt2] boolValue];
        
//        self.hasDSRManageStaff = [[userData objectForKey: kHasDSRManageStaff] boolValue];
        self.hasQLHasParticipatingProducts = [[userData objectForKey:kQLHasParticipatingProducts]boolValue];
        
        self.isParticipatingProductsOpt1 = [[userData objectForKey:kIsParticipatingProductsOpt1]boolValue];
        self.isParticipatingProductsOpt2 = [[userData objectForKey:kIsParticipatingProductsOpt2]boolValue];
        self.isParticipatingProductsOpt3 = [[userData objectForKey:kIsParticipatingProductsOpt3]boolValue];

        self.hasLeaderBoard = [[userData objectForKey:kHasLeaderBoard]boolValue];
        self.hasRUPerformance = [[userData objectForKey:kHasRUPerformance]boolValue];
        self.hasQLHasRUPerformance = [[userData objectForKey:kQLHasRUPerformance]boolValue];
        self.hasCSHasDSRPartnersOfShellClub = [[userData objectForKey:kCSHasDSRPartnersOfShellClub]boolValue];
        self.hasQLHasWorkShopOffer = [[userData objectForKey:kQLHasWorkShopOffer]boolValue];
        self.hasQLCashIncentive = [[userData objectForKey:kQLCashIncentive]boolValue];
        self.hasCashIncentive = [[userData objectForKey:kHasCashIncentive]boolValue];
        
        self.hasMultipleSubmissionOilChange = [[userData objectForKey:kHasMultipleSubmissionOilChange]boolValue];
        self.isRetail = [[userData objectForKey:kIsRetail]boolValue];
        
        self.hasQLHasMechanicPerformance = [[userData objectForKey: kHasQLHasMechanicPerformance] boolValue];
        self.hasQLHasManageStaff = [[userData objectForKey: kHasManageMyStaff] boolValue];
        self.hasManageMechanicPerformance = [[userData objectForKey: kHasQLHasMechanicPerformance] boolValue];
        
        self.hasCSHasBadge = [[userData objectForKey:kCSHasBadge]boolValue];
        self.shareTagCode = [userData objectForKey:kShareTagCode];
        self.shareQRCode = [userData objectForKey:kShareQRCode];
     
        self.hasOilChangeWalkThrough = [[userData objectForKey:kHasOilChangeWalkThrough] boolValue];
        
        self.isAccountDeActive = [[userData objectForKey:kIsAccountDeActive] boolValue];

        self.hasCSHasLoyaltyProgram = [[userData objectForKey:kCSHasLoyaltyProgram]boolValue];
        self.hasCSHasLoyaltyBD = [[userData objectForKey:kCSHasLoyaltyBD]boolValue];
        
        self.hasQLHasLoyaltyProgram = [[userData objectForKey:kQLHasLoyaltyPrograms]boolValue];
        self.hasLoyaltyProgram = [[userData objectForKey:kHasLoyaltyProgram]boolValue];
        
        self.IsPerfOpt1 = [[userData objectForKey: kIsPerfOpt1] boolValue];
        self.IsPerfOpt2 = [[userData objectForKey: kIsPerfOpt2] boolValue];
        
        self.hasWorkshopPerformanceView = [[userData objectForKey: kHasWorkshopPerformanceView]boolValue];
        self.hasCSHasTradePromotion = [[userData objectForKey: kCSHasTradePromotion] boolValue];
        
        self.hasCustomerRedemptionBtn = [[userData objectForKey: kHasCustomerRedemptionBtn] boolValue];
        
        
        self.companyCertTierText = [userData objectForKey: kCompanyCertTierText];
        self.companyCertTierImage = [userData objectForKey: kCompanyCertTierImage];
        
        self.hasQLOilChangeWalkThrough = [[userData objectForKey: kQLOilChangeWalkThrough]boolValue];
        
        self.hasCSHasCVBooster = [[userData objectForKey: kCSHasCVBooster]boolValue];

       // self.hasCSHasCVBooster = 1;
        self.hasCSHasAgri = [[userData objectForKey: kCSHasAgri]boolValue];
        
        self.isToSkipDeliveryAddress = [[userData objectForKey: kIsToSkipDeliveryAddress]boolValue];
        self.hasCurrentMileage = [[userData objectForKey: kHasCurrentMileage] boolValue];
        
        self.hasCSHasLebaranPromotion = [[userData objectForKey: kCSHasLebaranPromotion] boolValue];
        self.hasCSHasHX8Promotion = [[userData objectForKey: kCSHasHX8Promotion] boolValue];
        
        self.hasDistributorAcademy = [[userData objectForKey: kHasDistributorAcademy] boolValue];
        self.hasQLHasDistributorAcademy = [[userData objectForKey: kQLHasDistributorAcademy] boolValue];
        self.distributorAcademyURL = [userData objectForKey: kDistributorAcademyURL];
        
        self.hasWorkshopAcademy = [[userData objectForKey: kHasWorkshopAcademy] boolValue];
        self.hasQLHasWorkshopAcademy = [[userData objectForKey: kQLHasWorkshopAcademy] boolValue];
        self.workshopAcademyURL = [userData objectForKey: kWorkshopAcademyURL];
        
        self.hasVehicleType = [[userData objectForKey:kHasVehicleType] boolValue];
        if ([[userData objectForKey: kBusinessType] isKindOfClass: [NSNull class]])
            self.businessType = 0;
        else
            self.businessType = [[userData objectForKey: kBusinessType] intValue];
        
        self.hasAcceptPrivacyPolicy = [[userData objectForKey: kAcceptPrivacyPolicy] boolValue];
        self.hasPendingWorkshopList = [[userData objectForKey: kHasPendingWorkshopList] boolValue];
        self.hasBtnDeactivate = [[userData objectForKey: kHasBtnDeactivate] boolValue];
        self.hasBtnUpdate = [[userData objectForKey: kHasBtnUpdate] boolValue];
        
        self.hasQLHasDecalActivation = [[userData objectForKey:kQLHasDecalActivation] boolValue];
        
        self.hasQLHasBottleSale = [[userData objectForKey:kQLHasBottleSale] boolValue];
        self.hasBottleSale = [[userData objectForKey: kHasBottleSale] boolValue];
        
        if ([[userData objectForKey: kNextExpiryPoints] isKindOfClass: [NSNull class]])
            self.nextExpiryPoints = 0;
        else
            self.nextExpiryPoints = [[userData objectForKey: kNextExpiryPoints] intValue];
        
        self.nextExpiryDate = [userData objectForKey: kNextExpiryDate];
        
        self.hasShowCoinPopup = [[userData objectForKey:kShowCoinPopup] boolValue];
        self.hasShowTaxPopup = [[userData objectForKey:kShowTaxPopup] boolValue];
        
        self.hasLubeMatch = [[userData objectForKey:kHasLubeMatch] boolValue];
        self.hasQLHasLubeMatch = [[userData objectForKey:kQLHasLubeMatch] boolValue];

        self.hasCSHasEarnNReward = [[userData objectForKey:kCSHasEarnNReward] boolValue];

        self.hasAddNewRegWorkshop = [[userData objectForKey:kHasAddNewRegWorkshop] boolValue];
        
        self.hasInventoryManagement = [[userData objectForKey:kHasInventoryManagement] boolValue];
        self.hasQLHasInventoryManagement = [[userData objectForKey:kQLHasInventoryManagement] boolValue];
        // HasInventory
         self.HasInventory = [[userData objectForKey:kHasInventory] boolValue];

        self.hasCSMYHasEarnNReward = [[userData objectForKey:kCSMYHasEarnNReward] boolValue];
        self.hasCSHasTHIncentive = [[userData objectForKey:kCSHasTHIncentive] boolValue];
        self.hasCSHasTHOrderBreakdown = [[userData objectForKey:kCSHasTHOrderBreakdown] boolValue];

        self.hasCSHasGamification = [[userData objectForKey:kCSHasGamification] boolValue];
        self.gamification_HasCheckedInToday = [[userData objectForKey: kHasCheckedInToday] boolValue];
        if ([[userData objectForKey: kMilestone] isKindOfClass: [NSNull class]])
                self.gamification_Milestone = 0;
        else
                self.gamification_Milestone = [[userData objectForKey:kMilestone] intValue];

        self.hasCSHasTiering = [[userData objectForKey:kCSHasTiering] boolValue];

        self.hasPCMOGamification = [[userData objectForKey:kCSHasPCMOGamification] boolValue];
        self.hasPilotPromotion = [[userData objectForKey:kCSHasPilotPromotion] boolValue];

        if ([self.loyaltyTHAsOfDate isKindOfClass: [NSNull class]])
        {
            self.loyaltyTHAsOfDate = @"";
        } else {
            self.loyaltyTHAsOfDate = [userData objectForKey:kTHAsOfDate];
        }
    }
    
    return self;
}

-(NSDictionary *) getJSON
{
    return @{kCompanyName:          self.companyName,
             kSalutation:           self.salutation,
             kEmailAddress:         self.emailAddress,
             kBonusPoint:           @(self.bonusPoint),
             
             kFirstName:            self.firstName,
             kFullName:             self.fullName,
             kLastName:             self.lastName,
             kMobileNumber:         self.mobileNumber,
             
             kFAShareCode:          self.faShareCode,
             kFAShareQRCode:        self.faShareQRCode,
             kCSHasFAPerformance:   @(self.hasCSHasFAPerformance),
             kCSHasFABadge:         @(self.hasCSHasFABadge),
             
             kProfileDesc:          self.profileDesc,
             kProfileImage:         self.profileImage,
             kProfileType:          self.profileType,
             kRank:                 self.rank,
             kProfileCompleteness:  @(self.profileCompleteness),
             kRewardsStatus:        @(self.hasMyRewards),
             kRedeemOnly:           @(self.redeemOnly),
             kCanInviteTrade:       @(self.canInviteTrade),
             
             kHasOrdering:                  @(self.hasOrdering),
             kHasRegisterProductCode:       @(self.hasRegisterProductCode),
             kHasRegisterProductCode_V2:       @(self.hasRegisterProductCode_V2),
             kHasCustomerRedemption:        @(self.hasCustomerRedemption),
             kHasCustomerRedemptionDSR:        @(self.hasCustomerRedemptionDSR),
             kHasSearchVehicleID:           @(self.hasSearchVehicleID),
             kHasManageMyStaff:             @(self.hasManageMyStaff),
             
             kHasParticipatingProducts:     @(self.hasParticipatingProducts),
             kHasParticipatingAllProducts:     @(self.hasParticipatingAllProducts),
             kHasParticipatingShareProducts:     @(self.hasParticipatingShareProducts),
             
             kHasWorkshopPerformance:       @(self.hasWorkshopPerformance),
             kHasMYPerformance:              @(self.hasMYPerformance),
             kHasMYPoints:                   @(self.hasMYPoints),
             kHasAncillaryOffers : @(self.hasAncillaryOffers),
             kEnableCustomerRedemption:        @(self.enableCustomerRedemption),
             
             kCanEditAddress:               @(self.canEditAddress),
             kHasAddNewWorkshop:            @(self.hasAddNewWorkshop),
             
             kCanEditShippingAddress:       @(self.canEditShippingAddress),
             
             kUnReadPushMessage:        @(self.unReadPushMessage),
             kIsEnableBooking:           @(self.isEnableBooking),
             kIsEnableSurvey:           @(self.isEnableSurvey),
             kSurveyID:                     self.surveyId,
             
             kShowSurveyPP:             @(self.showSurveyPp),
             
             kHasTOPoint:               @(self.hasTOPoint),
             
             kHasInviteConsumer:        @(self.hasInviteConsumer),
             kHasWorkshopProfiling:     @(self.hasWorkshopProfiling),
             
             kHasAsset:                 @(self.hasMyLibrary),
             
             kHasTagDecalToWorkshop:    @(self.hasTagDecalToWorkshop),
             kHasDecal:                 @(self.hasDecal),
             kHasOrdering_V2:           @(self.hasOrdering_V2),
             kShowScanSearchVehicle:    @(self.hasShowScanSearchVehicle),
             
             //Quick Links flag
             kQLHasOrdering_V2          : @(self.hasQLHasOrdering_V2),
             kQLHasAsset                : @(self.hasQLHasAsset),
             kQLHasOnePlusOne            : @(self.hasQLHasOneplusOne),
             kQLHasSearchVehicleID      : @(self.hasQLHasSearchVehicleID),
             kQLHasManageWorkshop      : @(self.hasQLHasManageWorkshop),
             kQLHasRegisterProductCode  : @(self.hasQLHasRegisterProductCode),
             kQLHasCustomerRedemption   : @(self.hasQLHasCustomerRedemption),
             kQLHasScanDecal            : @(self.hasQLHasScanDecal),
             kQLHasInviteConsumer       : @(self.hasQLHasInviteConsumer),
             kQLHasRewards              : @(self.hasQLHasRewards),
             
             kHasPartnersOfShellClub              : @(self.hasPartnersOfShellClub),
             kHasOnePlusOne             : @(self.hasOnePlusOne),
             kQLHasPartnersOfShellClub              : @(self.hasQLHasPartnersOfShellClub),
             kCSHasMyPoints              : @(self.hasCSHasMyPoints),
             kCSHasMyProfile              : @(self.hasCSHasMyProfile),
             kCSHasPartnersOfShellClub              : @(self.hasCSHasPartnersOfShellClub),
             kCSHasInventorySpeedometer        : @(self.hasCSHasInventorySpeedometer),
             kCSHasPerformance : @(self.hasCSHasPerformance),
             
             kCSHasInventorySpeedometer  : @(self.hasCSHasInventorySpeedometer),

             kHasDRWorkshopPerformance : @(self.hasDRWorkshopPerformance),

             kCSHasOutletPerformance : @(self.hasCSHasOutletPerformance),
             kCSHasOutletPerformanceV2 : @(self.hasCSHasOutletPerformanceV2),
             kIsManageStaffOpt1 : @(self.isManageStaffOpt1),
             kIsManageStaffOpt2 : @(self.isManageStaffOpt2),
             kIsMyPointsOpt1 : @(self.isMyPointsOpt1),
             kIsMyPointsOpt2 : @(self.isMyPointsOpt2),
             
//             kHasDSRManageStaff : @(self.hasDSRManageStaff),
             kQLHasParticipatingProducts : @(self.hasQLHasParticipatingProducts),
             
             kIsParticipatingProductsOpt1 : @(self.isParticipatingProductsOpt1),
             kIsParticipatingProductsOpt2 : @(self.isParticipatingProductsOpt2),
             kIsParticipatingProductsOpt3 : @(self.isParticipatingProductsOpt3),

             kHasLeaderBoard : @(self.hasLeaderBoard),
             kHasRUPerformance : @(self.hasRUPerformance),
             kQLHasRUPerformance : @(self.hasQLHasRUPerformance),
             kCSHasDSRPartnersOfShellClub : @(self.hasCSHasDSRPartnersOfShellClub),
             
             kQLHasWorkShopOffer : @(self.hasQLHasWorkShopOffer),
             kQLCashIncentive : @(self.hasQLCashIncentive),
             kHasCashIncentive : @(self.hasCashIncentive),
             
             kHasMultipleSubmissionOilChange : @(self.hasMultipleSubmissionOilChange),
             
             kHasQLHasMechanicPerformance : @(self.hasQLHasMechanicPerformance),
             kHasQLHasManageStaff : @(self.hasQLHasManageStaff),
             kHasManageMechanicPerformance : @(self.hasManageMechanicPerformance),
             
             kCSHasBadge : @(self.hasCSHasBadge),
             kShareQRCode : self.shareQRCode,
             kShareTagCode : self.shareTagCode,
             
             kHasOilChangeWalkThrough : @(self.hasOilChangeWalkThrough),
             kIsAccountDeActive : @(self.isAccountDeActive),
             
             kIsRetail : @(self.isRetail),
             
             kCSHasLoyaltyBD : @(self.hasCSHasLoyaltyBD),
             kCSHasLoyaltyProgram : @(self.hasCSHasLoyaltyProgram),
             
             kQLHasLoyaltyPrograms: @(self.hasQLHasLoyaltyProgram),
             kHasLoyaltyProgram : @(self.hasLoyaltyProgram),

             kIsPerfOpt1 : @(self.IsPerfOpt1),
             kIsPerfOpt2 : @(self.IsPerfOpt2),
             
             kHasWorkshopPerformanceView : @(self.hasWorkshopPerformanceView),
             kCSHasTradePromotion : @(self.hasCSHasTradePromotion),
             
             kHasCustomerRedemptionBtn : @(self.hasCustomerRedemptionBtn),
             
             kCompanyCertTierText : self.companyCertTierText,
             kCompanyCertTierImage : self.companyCertTierImage,
             
             kQLOilChangeWalkThrough : @(self.hasQLOilChangeWalkThrough),
             
             kCSHasCVBooster : @(self.hasCSHasCVBooster),
             kCSHasAgri : @(self.hasCSHasAgri),
             
             kIsToSkipDeliveryAddress : @(self.isToSkipDeliveryAddress),
             kHasCurrentMileage : @(self.hasCurrentMileage),
             
             kCSHasLebaranPromotion : @(self.hasCSHasLebaranPromotion),
             kCSHasHX8Promotion : @(self.hasCSHasHX8Promotion),
             
             kHasDistributorAcademy : @(self.hasDistributorAcademy),
             kQLHasDistributorAcademy : @(self.hasQLHasDistributorAcademy),
             kDistributorAcademyURL : self.distributorAcademyURL,
             
             kHasWorkshopAcademy : @(self.hasWorkshopAcademy),
             kQLHasWorkshopAcademy : @(self.hasQLHasWorkshopAcademy),
             kWorkshopAcademyURL : self.workshopAcademyURL,
             
             kHasVehicleType : @(self.hasVehicleType),
             kBusinessType : @(self.businessType),
             
             kAcceptPrivacyPolicy : @(self.hasAcceptPrivacyPolicy),
             kHasPendingWorkshopList : @(self.hasPendingWorkshopList),
             kHasBtnDeactivate : @(self.hasBtnDeactivate),
             kHasBtnUpdate : @(self.hasBtnUpdate),
             
             kQLHasDecalActivation : @(self.hasQLHasDecalActivation),
             
             kQLHasBottleSale : @(self.hasQLHasBottleSale),
             kHasBottleSale : @(self.hasBottleSale),
             
             kNextExpiryDate : self.nextExpiryDate,
             kNextExpiryPoints : @(self.nextExpiryPoints),
             
             kShowCoinPopup : @(self.hasShowCoinPopup),
             kShowTaxPopup : @(self.hasShowTaxPopup),

             kHasLubeMatch : @(self.hasLubeMatch),
             kQLHasLubeMatch : @(self.hasQLHasLubeMatch),

             kCSHasEarnNReward : @(self.hasCSHasEarnNReward),
             kCSMYHasEarnNReward : @(self.hasCSMYHasEarnNReward),

             kHasAddNewRegWorkshop : @(self.hasAddNewRegWorkshop),
             kHasInventoryManagement : @(self.hasInventoryManagement),
             kQLHasInventoryManagement : @(self.hasQLHasInventoryManagement),
             // added Sept 2020
             kHasInventory : @(self.HasInventory),

             kCSHasTHIncentive : @(self.hasCSHasTHIncentive),
             kCSHasTHOrderBreakdown : @(self.hasCSHasTHOrderBreakdown),

             kCSHasGamification : @(self.hasCSHasGamification),
             kHasCheckedInToday : @(self.gamification_HasCheckedInToday),
             kMilestone         : @(self.gamification_Milestone),

             kCSHasTiering      : @(self.hasCSHasTiering),
             kCSHasPCMOGamification : @(self.hasPCMOGamification),
             kCSHasPilotPromotion : @(self.hasPilotPromotion),
             kTHAsOfDate        : self.loyaltyTHAsOfDate,

             };
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init]) {
        self.companyName =          [decoder decodeObjectForKey: kCompanyName];
        self.salutation =           [decoder decodeObjectForKey: kSalutation]; //save keycode?
        self.emailAddress =         [decoder decodeObjectForKey: kEmailAddress];
        self.bonusPoint =           [[decoder decodeObjectForKey: kBonusPoint] intValue];
        
        self.firstName =            [decoder decodeObjectForKey: kFirstName];
        self.fullName =             [decoder decodeObjectForKey: kFullName];
        self.lastName =             [decoder decodeObjectForKey: kLastName];
        self.mobileNumber =         [decoder decodeObjectForKey: kMobileNumber];
        
        self.faShareQRCode =        [decoder decodeObjectForKey:kFAShareQRCode];
        self.faShareCode =          [decoder decodeObjectForKey:kFAShareCode];
        self.hasCSHasFABadge =      [[decoder decodeObjectForKey:kCSHasFABadge]boolValue];
        self.hasCSHasFAPerformance =      [[decoder decodeObjectForKey:kCSHasFAPerformance]boolValue];
        
        self.profileDesc =          [decoder decodeObjectForKey: kProfileDesc];
        self.profileImage =         [decoder decodeObjectForKey: kProfileImage]; //url as string
        self.profileType =          [decoder decodeObjectForKey: kProfileType]; //bit value
        self.rank =                 [decoder decodeObjectForKey: kRank]; //e.g.string "#3/5"
        self.profileCompleteness =  [[decoder decodeObjectForKey: kProfileCompleteness] floatValue];
        self.hasMyRewards =         [[decoder decodeObjectForKey: kRewardsStatus] boolValue];
        self.redeemOnly =           [[decoder decodeObjectForKey: kRedeemOnly] boolValue];
        self.canInviteTrade =       [[decoder decodeObjectForKey: kCanInviteTrade] boolValue];
        
        self.hasOrdering =                  [[decoder decodeObjectForKey: kHasOrdering] boolValue];
        self.hasRegisterProductCode =       [[decoder decodeObjectForKey: kHasRegisterProductCode] boolValue];
        self.hasRegisterProductCode_V2 =       [[decoder decodeObjectForKey: kHasRegisterProductCode_V2] boolValue];
        self.hasCustomerRedemption =        [[decoder decodeObjectForKey: kHasCustomerRedemption] boolValue];
        self.hasCustomerRedemptionDSR =        [[decoder decodeObjectForKey: kHasCustomerRedemptionDSR] boolValue];
        self.hasSearchVehicleID =           [[decoder decodeObjectForKey: kHasSearchVehicleID] boolValue];
        self.hasManageMyStaff =             [[decoder decodeObjectForKey: kHasManageMyStaff] boolValue];
        
        self.hasParticipatingProducts =     [[decoder decodeObjectForKey: kHasParticipatingProducts] boolValue];
        self.hasParticipatingAllProducts =     [[decoder decodeObjectForKey: kHasParticipatingAllProducts] boolValue];
        self.hasParticipatingShareProducts =     [[decoder decodeObjectForKey: kHasParticipatingShareProducts] boolValue];
        
        
        self.hasMyLibrary = [[decoder decodeObjectForKey: kHasAsset] boolValue];
        self.hasWorkshopPerformance =       [[decoder decodeObjectForKey: kHasWorkshopPerformance] boolValue];
        self.hasMYPerformance =       [[decoder decodeObjectForKey: kHasMYPerformance] boolValue];
        self.hasMYPoints =       [[decoder decodeObjectForKey: kHasMYPoints] boolValue];
        self.hasAncillaryOffers = [[decoder decodeObjectForKey: kHasAncillaryOffers] boolValue];
        self.enableCustomerRedemption =        [[decoder decodeObjectForKey: kEnableCustomerRedemption] boolValue];
        
        self.canEditAddress =               [[decoder decodeObjectForKey: kCanEditAddress] boolValue];
        self.hasAddNewWorkshop =            [[decoder decodeObjectForKey: kHasAddNewWorkshop] boolValue];
        self.canEditShippingAddress =       [[decoder decodeObjectForKey: kCanEditShippingAddress] boolValue];
        self.unReadPushMessage =            [[decoder decodeObjectForKey: kUnReadPushMessage] intValue];

        self.shareCode =            [decoder decodeObjectForKey: kShareCode];
        
        self.surveyId =            [decoder decodeObjectForKey: kSurveyID];
        self.isEnableBooking = [[decoder decodeObjectForKey: kIsEnableBooking] boolValue];
        self.isEnableSurvey =       [[decoder decodeObjectForKey: kIsEnableSurvey] boolValue];
        
        self.showSurveyPp =       [[decoder decodeObjectForKey: kShowSurveyPP] boolValue];
        
        self.hasTOPoint =       [[decoder decodeObjectForKey: kHasTOPoint] boolValue];
        
        self.hasInviteConsumer =       [[decoder decodeObjectForKey: kHasInviteConsumer] boolValue];
        
        self.hasWorkshopProfiling =       [[decoder decodeObjectForKey: kHasWorkshopProfiling] boolValue];
        
        self.hasTagDecalToWorkshop =       [[decoder decodeObjectForKey: kHasTagDecalToWorkshop] boolValue];
        
        self.hasDecal =       [[decoder decodeObjectForKey: kHasDecal] boolValue];
        self.hasOrdering_V2 =       [[decoder decodeObjectForKey: kHasOrdering_V2] boolValue];
        
        self.hasShowScanSearchVehicle = [[decoder decodeObjectForKey: kShowScanSearchVehicle] boolValue];
        
        
        //Quick Links flag
        self.hasQLHasOrdering_V2 = [[decoder decodeObjectForKey: kQLHasOrdering_V2] boolValue];
        self.hasQLHasAsset = [[decoder decodeObjectForKey: kQLHasAsset] boolValue];
        self.hasQLHasOneplusOne = [[decoder decodeObjectForKey: kQLHasOnePlusOne] boolValue];
        self.hasQLHasSearchVehicleID = [[decoder decodeObjectForKey: kQLHasSearchVehicleID] boolValue];
        self.hasQLHasManageWorkshop = [[decoder decodeObjectForKey: kQLHasManageWorkshop] boolValue];
        self.hasQLHasRegisterProductCode = [[decoder decodeObjectForKey: kQLHasRegisterProductCode] boolValue];
        self.hasQLHasCustomerRedemption = [[decoder decodeObjectForKey: kQLHasCustomerRedemption] boolValue];
        self.hasQLHasScanDecal = [[decoder decodeObjectForKey: kQLHasScanDecal] boolValue];
        self.hasQLHasInviteConsumer = [[decoder decodeObjectForKey: kQLHasInviteConsumer] boolValue];
        self.hasQLHasRewards = [[decoder decodeObjectForKey: kQLHasRewards] boolValue];
        self.hasPartnersOfShellClub = [[decoder decodeObjectForKey: kHasPartnersOfShellClub] boolValue];
        self.hasOnePlusOne = [[decoder decodeObjectForKey:kHasOnePlusOne] boolValue];
        self.hasQLHasPartnersOfShellClub = [[decoder decodeObjectForKey: kQLHasPartnersOfShellClub] boolValue];
        self.hasCSHasMyPoints = [[decoder decodeObjectForKey: kCSHasMyPoints] boolValue];
        self.hasCSHasMyProfile = [[decoder decodeObjectForKey: kCSHasMyProfile] boolValue];
        self.hasCSHasPartnersOfShellClub = [[decoder decodeObjectForKey: kCSHasPartnersOfShellClub] boolValue];
        self.hasCSHasInventorySpeedometer = [[decoder decodeObjectForKey: kCSHasInventorySpeedometer] boolValue];
        self.hasCSHasPerformance = [[decoder decodeObjectForKey: kCSHasPerformance] boolValue];

        self.hasCSHasInventorySpeedometer = [[decoder decodeObjectForKey:kCSHasInventorySpeedometer] boolValue];

        self.hasDRWorkshopPerformance = [[decoder decodeObjectForKey: kHasDRWorkshopPerformance] boolValue];
        self.hasCSHasOutletPerformanceV2 = [[decoder decodeObjectForKey: kCSHasOutletPerformanceV2] boolValue];
        self.isManageStaffOpt1 = [[decoder decodeObjectForKey: kIsManageStaffOpt1] boolValue];
        self.isManageStaffOpt2 = [[decoder decodeObjectForKey: kIsManageStaffOpt2] boolValue];
        self.isMyPointsOpt1 = [[decoder decodeObjectForKey: kIsMyPointsOpt1] boolValue];
        self.isMyPointsOpt2 = [[decoder decodeObjectForKey: kIsMyPointsOpt2] boolValue];
        
//        self.hasDSRManageStaff = [[decoder decodeObjectForKey: kHasDSRManageStaff] boolValue];
        self.hasQLHasParticipatingProducts = [[decoder decodeObjectForKey:kQLHasParticipatingProducts]boolValue];
        
        self.isParticipatingProductsOpt1 = [[decoder decodeObjectForKey:kIsParticipatingProductsOpt1]boolValue];
        self.isParticipatingProductsOpt2 = [[decoder decodeObjectForKey:kIsParticipatingProductsOpt2]boolValue];
        self.isParticipatingProductsOpt3 = [[decoder decodeObjectForKey:kIsParticipatingProductsOpt3]boolValue];

        self.hasLeaderBoard = [[decoder decodeObjectForKey:kHasLeaderBoard]boolValue];
        self.hasRUPerformance = [[decoder decodeObjectForKey:kHasRUPerformance] boolValue];
        self.hasQLHasRUPerformance = [[decoder decodeObjectForKey:kQLHasRUPerformance] boolValue];
        self.hasCSHasDSRPartnersOfShellClub = [[decoder decodeObjectForKey:kCSHasDSRPartnersOfShellClub] boolValue];
        self.hasQLHasWorkShopOffer = [[decoder decodeObjectForKey:kQLHasWorkShopOffer] boolValue];
        self.hasQLCashIncentive = [[decoder decodeObjectForKey:kQLCashIncentive] boolValue];
        self.hasCashIncentive = [[decoder decodeObjectForKey:kHasCashIncentive] boolValue];
        
        self.hasMultipleSubmissionOilChange = [[decoder decodeObjectForKey:kHasMultipleSubmissionOilChange]boolValue];
        
        self.hasQLHasMechanicPerformance = [[decoder decodeObjectForKey:kHasQLHasMechanicPerformance] boolValue];
        self.hasQLHasManageStaff = [[decoder decodeObjectForKey:kHasManageMyStaff] boolValue];
        self.hasManageMechanicPerformance = [[decoder decodeObjectForKey:kHasManageMechanicPerformance] boolValue];
        
        self.hasCSHasBadge = [[decoder decodeObjectForKey:kCSHasBadge]boolValue];
        self.shareTagCode = [decoder decodeObjectForKey:kShareTagCode];
        self.shareQRCode = [decoder decodeObjectForKey:kShareQRCode];
        
        self.hasOilChangeWalkThrough = [[decoder decodeObjectForKey:kHasOilChangeWalkThrough]boolValue];
        self.isAccountDeActive = [[decoder decodeObjectForKey:kIsAccountDeActive]boolValue];

        self.isRetail = [[decoder decodeObjectForKey:kIsRetail]boolValue];
        
        self.hasCSHasLoyaltyProgram = [[decoder decodeObjectForKey:kCSHasLoyaltyProgram]boolValue];
        self.hasCSHasLoyaltyBD = [[decoder decodeObjectForKey:kCSHasLoyaltyBD]boolValue];
        
        self.hasQLHasLoyaltyProgram = [[decoder decodeObjectForKey:kQLHasLoyaltyPrograms]boolValue];
        self.hasLoyaltyProgram = [[decoder decodeObjectForKey:kHasLoyaltyProgram]boolValue];

        self.IsPerfOpt1 =  [[decoder decodeObjectForKey: kIsPerfOpt1] boolValue];
        self.IsPerfOpt2 =  [[decoder decodeObjectForKey: kIsPerfOpt2] boolValue];
        
        self.hasWorkshopPerformanceView = [[decoder decodeObjectForKey: kHasWorkshopPerformanceView] boolValue];
        self.hasCSHasTradePromotion = [[decoder decodeObjectForKey: kCSHasTradePromotion] boolValue];
        
        self.hasCustomerRedemptionBtn = [[decoder decodeObjectForKey: kHasCustomerRedemptionBtn] boolValue];
        
        self.companyCertTierText = [decoder decodeObjectForKey: kCompanyCertTierText];
        self.companyCertTierImage = [decoder decodeObjectForKey: kCompanyCertTierImage];
        
        self.hasQLOilChangeWalkThrough = [[decoder decodeObjectForKey: kQLOilChangeWalkThrough] boolValue];
        
        self.hasCSHasCVBooster = [[decoder decodeObjectForKey: kCSHasCVBooster] boolValue];
        self.hasCSHasAgri = [[decoder decodeObjectForKey: kCSHasAgri] boolValue];
        
        self.isToSkipDeliveryAddress = [[decoder decodeObjectForKey: kIsToSkipDeliveryAddress] boolValue];
        self.hasCurrentMileage = [[decoder decodeObjectForKey: kHasCurrentMileage] boolValue];
        
        self.hasCSHasLebaranPromotion = [[decoder decodeObjectForKey: kCSHasLebaranPromotion] boolValue];
        self.hasCSHasHX8Promotion = [[decoder decodeObjectForKey: kCSHasHX8Promotion] boolValue];
        
        self.hasDistributorAcademy = [[decoder decodeObjectForKey: kHasDistributorAcademy] boolValue];
        self.hasQLHasDistributorAcademy = [[decoder decodeObjectForKey: kQLHasDistributorAcademy] boolValue];
        self.distributorAcademyURL = [decoder decodeObjectForKey: kDistributorAcademyURL];
        
        self.hasWorkshopAcademy = [[decoder decodeObjectForKey: kHasWorkshopAcademy] boolValue];
        self.hasQLHasWorkshopAcademy = [[decoder decodeObjectForKey: kQLHasWorkshopAcademy] boolValue];
        self.workshopAcademyURL = [decoder decodeObjectForKey: kWorkshopAcademyURL];
        
        self.hasVehicleType = [[decoder decodeObjectForKey: kHasVehicleType] boolValue];
        self.businessType =  [[decoder decodeObjectForKey: kBusinessType] intValue];
        
        self.hasAcceptPrivacyPolicy = [[decoder decodeObjectForKey: kAcceptPrivacyPolicy] boolValue];
        self.hasPendingWorkshopList = [[decoder decodeObjectForKey: kHasPendingWorkshopList] boolValue];
        self.hasBtnDeactivate = [[decoder decodeObjectForKey: kHasBtnDeactivate] boolValue];
        self.hasBtnUpdate = [[decoder decodeObjectForKey: kHasBtnUpdate] boolValue];
        
        self.hasQLHasDecalActivation = [[decoder decodeObjectForKey: kQLHasDecalActivation] boolValue];
        self.hasQLHasBottleSale = [[decoder decodeObjectForKey: kQLHasBottleSale] boolValue];
        self.hasBottleSale = [[decoder decodeObjectForKey: kHasBottleSale] boolValue];
        
        self.nextExpiryDate = [decoder decodeObjectForKey: kNextExpiryDate];
        self.nextExpiryPoints = [[decoder decodeObjectForKey: kNextExpiryPoints] intValue];
        
        self.hasShowCoinPopup = [[decoder decodeObjectForKey:kShowCoinPopup]boolValue];
        self.hasShowTaxPopup = [[decoder decodeObjectForKey:kShowTaxPopup]boolValue];
        
        self.hasLubeMatch = [[decoder decodeObjectForKey:kHasLubeMatch]boolValue];
        self.hasQLHasLubeMatch = [[decoder decodeObjectForKey:kQLHasLubeMatch]boolValue];

        self.hasCSHasEarnNReward = [[decoder decodeObjectForKey:kCSHasEarnNReward] boolValue];
        self.hasCSMYHasEarnNReward = [[decoder decodeObjectForKey:kCSMYHasEarnNReward] boolValue];
        self.hasCSHasTHIncentive = [[decoder decodeObjectForKey:kCSHasTHIncentive] boolValue];
        self.hasCSHasTHOrderBreakdown = [[decoder decodeObjectForKey:kCSHasTHOrderBreakdown] boolValue];
        
        self.hasAddNewRegWorkshop = [[decoder decodeObjectForKey:kHasAddNewRegWorkshop] boolValue];
        self.hasInventoryManagement = [[decoder decodeObjectForKey:kHasInventoryManagement] boolValue];
        self.hasQLHasInventoryManagement = [[decoder decodeObjectForKey:kQLHasInventoryManagement] boolValue];
        // added Sept 2020
        self.HasInventory = [[decoder decodeObjectForKey:kHasInventory] boolValue];

        self.hasCSHasGamification = [[decoder decodeObjectForKey: kCSHasGamification] boolValue];
        self.gamification_HasCheckedInToday = [[decoder decodeObjectForKey: kHasCheckedInToday] boolValue];
        self.gamification_Milestone = [[decoder decodeObjectForKey: kMilestone] intValue];

        self.hasCSHasTiering = [[decoder decodeObjectForKey: kCSHasTiering] boolValue];
        self.hasPCMOGamification = [[decoder decodeObjectForKey: kCSHasPCMOGamification] boolValue];
        self.hasPilotPromotion = [[decoder decodeObjectForKey: kCSHasPilotPromotion] boolValue];
        self.loyaltyTHAsOfDate = [decoder decodeObjectForKey: kTHAsOfDate];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject: self.companyName             forKey: kCompanyName];
    [encoder encodeObject: self.salutation              forKey: kSalutation];
    [encoder encodeObject: self.emailAddress            forKey: kEmailAddress];
    [encoder encodeObject: @(self.bonusPoint)           forKey: kBonusPoint];
    
    [encoder encodeObject: self.firstName               forKey: kFirstName];
    [encoder encodeObject: self.fullName                forKey: kFullName];
    [encoder encodeObject: self.lastName                forKey: kLastName];
    [encoder encodeObject: self.mobileNumber            forKey: kMobileNumber];
    
    [encoder encodeObject:self.faShareQRCode            forKey:kFAShareQRCode];
    [encoder encodeObject:self.faShareCode            forKey:kFAShareCode];
    [encoder encodeObject:@(self.hasCSHasFAPerformance)            forKey:kCSHasFAPerformance];
    [encoder encodeObject:@(self.hasCSHasFABadge)          forKey:kCSHasFABadge];
    
    
    [encoder encodeObject: self.profileDesc             forKey: kProfileDesc];
    [encoder encodeObject: self.profileImage            forKey: kProfileImage];
    [encoder encodeObject: self.profileType             forKey: kProfileType];
    [encoder encodeObject: self.rank                    forKey: kRank];
    [encoder encodeObject: @(self.profileCompleteness)  forKey: kProfileCompleteness];
    
    [encoder encodeObject: @(self.hasMyRewards)         forKey: kRewardsStatus];
    [encoder encodeObject: self.shareCode               forKey: kShareCode];
    [encoder encodeObject: @(self.redeemOnly)           forKey: kRedeemOnly];
    [encoder encodeObject: @(self.canInviteTrade)       forKey: kCanInviteTrade];
    
    
    [encoder encodeObject: @(self.hasOrdering)                  forKey: kHasOrdering];
    [encoder encodeObject: @(self.hasRegisterProductCode)       forKey: kHasRegisterProductCode];
    [encoder encodeObject: @(self.hasRegisterProductCode_V2)       forKey: kHasRegisterProductCode_V2];
    [encoder encodeObject: @(self.hasCustomerRedemption)        forKey: kHasCustomerRedemption];
    [encoder encodeObject: @(self.hasCustomerRedemptionDSR)        forKey: kHasCustomerRedemptionDSR];
    [encoder encodeObject: @(self.hasSearchVehicleID)           forKey: kHasSearchVehicleID];
    [encoder encodeObject: @(self.hasManageMyStaff)             forKey: kHasManageMyStaff];
    
    [encoder encodeObject: @(self.hasParticipatingProducts)     forKey: kHasParticipatingProducts];
    [encoder encodeObject: @(self.hasParticipatingAllProducts)     forKey: kHasParticipatingAllProducts];
    [encoder encodeObject: @(self.hasParticipatingShareProducts)     forKey: kHasParticipatingShareProducts];
    
    
    [encoder encodeObject: @(self.hasWorkshopPerformance)       forKey: kHasWorkshopPerformance];
    [encoder encodeObject: @(self.hasMYPerformance)       forKey: kHasMYPerformance];
    [encoder encodeObject: @(self.hasMYPoints)       forKey: kHasMYPoints];
    [encoder encodeObject: @(self.hasAncillaryOffers)       forKey: kHasAncillaryOffers];
    [encoder encodeObject: @(self.enableCustomerRedemption)      forKey: kEnableCustomerRedemption];
    
    [encoder encodeObject: @(self.canEditAddress)               forKey: kCanEditAddress];
    [encoder encodeObject: @(self.hasAddNewWorkshop)            forKey: kHasAddNewWorkshop];
    [encoder encodeObject: @(self.canEditShippingAddress)       forKey: kCanEditShippingAddress];
    
    [encoder encodeObject: @(self.unReadPushMessage)         forKey: kUnReadPushMessage];
    
    [encoder encodeObject: @(self.isEnableBooking)       forKey: kIsEnableBooking];
    [encoder encodeObject: @(self.isEnableSurvey)       forKey: kIsEnableSurvey];
    [encoder encodeObject: self.surveyId       forKey: kSurveyID];
    
    [encoder encodeObject: @(self.showSurveyPp)       forKey: kShowSurveyPP];
    
    [encoder encodeObject: @(self.hasTOPoint)       forKey: kHasTOPoint];
    
    [encoder encodeObject: @(self.hasInviteConsumer)       forKey: kHasInviteConsumer];
    
    [encoder encodeObject: @(self.hasWorkshopProfiling)       forKey: kHasWorkshopProfiling];
    
    [encoder encodeObject: @(self.hasMyLibrary)       forKey: kHasAsset];
    [encoder encodeObject: @(self.hasOrdering_V2)       forKey: kHasOrdering_V2];
    
    
    [encoder encodeObject: @(self.hasDecal)       forKey: kHasDecal];
    [encoder encodeObject: @(self.hasTagDecalToWorkshop)       forKey: kHasTagDecalToWorkshop];
    [encoder encodeObject: @(self.hasShowScanSearchVehicle)       forKey: kShowScanSearchVehicle];
    
    
    //Quick Links flag
    [encoder encodeObject: @(self.hasQLHasOrdering_V2)       forKey: kQLHasOrdering_V2];
    [encoder encodeObject: @(self.hasQLHasOneplusOne)       forKey: kQLHasOnePlusOne];
    [encoder encodeObject: @(self.hasQLHasSearchVehicleID)       forKey: kQLHasSearchVehicleID];
    [encoder encodeObject: @(self.hasQLHasManageWorkshop)       forKey: kQLHasManageWorkshop];
    [encoder encodeObject: @(self.hasQLHasRegisterProductCode)       forKey: kQLHasRegisterProductCode];
    [encoder encodeObject: @(self.hasQLHasCustomerRedemption)       forKey: kQLHasCustomerRedemption];
    [encoder encodeObject: @(self.hasQLHasScanDecal)       forKey: kQLHasScanDecal];
    [encoder encodeObject: @(self.hasQLHasInviteConsumer)       forKey: kQLHasInviteConsumer];
    [encoder encodeObject: @(self.hasQLHasRewards)       forKey: kQLHasRewards];
    
    [encoder encodeObject: @(self.hasPartnersOfShellClub)       forKey: kHasPartnersOfShellClub];
    [encoder encodeObject: @(self.hasQLHasPartnersOfShellClub)       forKey: kQLHasPartnersOfShellClub];
    [encoder encodeObject:@(self.hasOnePlusOne) forKey:kHasOnePlusOne];

    [encoder encodeObject: @(self.hasCSHasMyPoints)       forKey: kCSHasMyPoints];
    [encoder encodeObject: @(self.hasCSHasMyProfile)       forKey: kCSHasMyProfile];
    [encoder encodeObject: @(self.hasCSHasPartnersOfShellClub)       forKey: kCSHasPartnersOfShellClub];
    [encoder encodeObject: @(self.hasCSHasInventorySpeedometer)       forKey: kCSHasInventorySpeedometer];
    [encoder encodeObject:@(self.hasCSHasInventorySpeedometer) forKey:kCSHasInventorySpeedometer];

    [encoder encodeObject: @(self.hasCSHasPerformance)       forKey: kCSHasPerformance];
    
    [encoder encodeObject: @(self.hasDRWorkshopPerformance)       forKey: kHasDRWorkshopPerformance];
    [encoder encodeObject: @(self.hasCSHasOutletPerformanceV2)       forKey: kCSHasOutletPerformanceV2];
    [encoder encodeObject: @(self.isManageStaffOpt1)       forKey: kIsManageStaffOpt1];
    [encoder encodeObject: @(self.isManageStaffOpt2)       forKey: kIsManageStaffOpt2];
    [encoder encodeObject: @(self.isMyPointsOpt1)       forKey: kIsMyPointsOpt1];
    [encoder encodeObject: @(self.isMyPointsOpt2)       forKey: kIsMyPointsOpt2];
    
//    [encoder encodeObject: @(self.hasDSRManageStaff)       forKey: kHasDSRManageStaff];
    [encoder encodeObject: @(self.hasQLHasParticipatingProducts) forKey:kQLHasParticipatingProducts];
    
    [encoder encodeObject: @(self.isParticipatingProductsOpt1) forKey:kIsParticipatingProductsOpt1];
    [encoder encodeObject: @(self.isParticipatingProductsOpt2) forKey:kIsParticipatingProductsOpt2];
    [encoder encodeObject: @(self.isParticipatingProductsOpt3) forKey:kIsParticipatingProductsOpt3];

    [encoder encodeObject: @(self.hasLeaderBoard) forKey:kHasLeaderBoard];
    [encoder encodeObject: @(self.hasRUPerformance) forKey:kHasRUPerformance];
    [encoder encodeObject: @(self.hasQLHasRUPerformance) forKey:kQLHasRUPerformance];
    [encoder encodeObject: @(self.hasCSHasDSRPartnersOfShellClub) forKey:kCSHasDSRPartnersOfShellClub];
    [encoder encodeObject: @(self.hasQLHasWorkShopOffer) forKey: kQLHasWorkShopOffer];
    
    [encoder encodeObject: @(self.hasMultipleSubmissionOilChange) forKey: kHasMultipleSubmissionOilChange];

    [encoder encodeObject: @(self.hasQLHasMechanicPerformance) forKey: kHasQLHasMechanicPerformance];
    [encoder encodeObject: @(self.hasQLHasManageStaff) forKey: kHasManageMyStaff];
    [encoder encodeObject: @(self.hasManageMechanicPerformance) forKey: kHasManageMechanicPerformance];
    
    [encoder encodeObject: @(self.hasCSHasBadge) forKey:kCSHasBadge];
    [encoder encodeObject: self.shareTagCode forKey:kShareTagCode];
    [encoder encodeObject: self.shareQRCode forKey:kShareQRCode];
    
    [encoder encodeObject: @(self.hasOilChangeWalkThrough) forKey:kHasOilChangeWalkThrough];
    
    [encoder encodeObject: @(self.isAccountDeActive) forKey:kIsAccountDeActive];
    
    [encoder encodeObject: @(self.isRetail) forKey: kIsRetail];
    
    [encoder encodeObject: @(self.hasCSHasLoyaltyBD) forKey: kCSHasLoyaltyBD];
    [encoder encodeObject: @(self.hasCSHasLoyaltyProgram) forKey: kCSHasLoyaltyProgram];
    
    [encoder encodeObject: @(self.hasQLHasLoyaltyProgram) forKey:kQLHasLoyaltyPrograms];
    [encoder encodeObject: @(self.hasLoyaltyProgram) forKey:kHasLoyaltyProgram];
    
    [encoder encodeObject: @(self.IsPerfOpt1) forKey:kIsPerfOpt1];
    [encoder encodeObject: @(self.IsPerfOpt2) forKey:kIsPerfOpt2];
    
    [encoder encodeObject: @(self.hasWorkshopPerformanceView) forKey:kHasWorkshopPerformanceView];
    [encoder encodeObject: @(self.hasCSHasTradePromotion) forKey:kCSHasTradePromotion];
    
    [encoder encodeObject: @(self.hasCustomerRedemptionBtn) forKey:kHasCustomerRedemptionBtn];
    
    [encoder encodeObject: self.companyCertTierText forKey:kCompanyCertTierText];
    [encoder encodeObject: self.companyCertTierImage forKey:kCompanyCertTierImage];
    
    [encoder encodeObject: @(self.hasQLOilChangeWalkThrough) forKey:kQLOilChangeWalkThrough];
    
    [encoder encodeObject: @(self.hasCSHasCVBooster) forKey:kCSHasCVBooster];
    [encoder encodeObject: @(self.hasCSHasAgri) forKey:kCSHasAgri];
    
    [encoder encodeObject: @(self.isToSkipDeliveryAddress) forKey:kIsToSkipDeliveryAddress];
    [encoder encodeObject: @(self.hasCurrentMileage) forKey:kHasCurrentMileage];
    
    [encoder encodeObject: @(self.hasCSHasLebaranPromotion) forKey:kCSHasLebaranPromotion];
    [encoder encodeObject: @(self.hasCSHasHX8Promotion) forKey:kCSHasHX8Promotion];
    
    [encoder encodeObject: @(self.hasDistributorAcademy) forKey:kHasDistributorAcademy];
    [encoder encodeObject: @(self.hasQLHasDistributorAcademy) forKey:kQLHasDistributorAcademy];
    [encoder encodeObject:self.distributorAcademyURL forKey:kDistributorAcademyURL];
    
    [encoder encodeObject: @(self.hasWorkshopAcademy) forKey:kHasWorkshopAcademy];
    [encoder encodeObject: @(self.hasQLHasWorkshopAcademy) forKey:kQLHasWorkshopAcademy];
    [encoder encodeObject:self.workshopAcademyURL forKey:kWorkshopAcademyURL];
    
    [encoder encodeObject:@(self.hasVehicleType) forKey:kHasVehicleType];
    [encoder encodeObject:@(self.businessType) forKey:kBusinessType];
    
    [encoder encodeObject:@(self.hasAcceptPrivacyPolicy) forKey:kAcceptPrivacyPolicy];
    [encoder encodeObject:@(self.hasPendingWorkshopList) forKey:kHasPendingWorkshopList];
    [encoder encodeObject:@(self.hasBtnDeactivate) forKey:kHasBtnDeactivate];
    [encoder encodeObject:@(self.hasBtnUpdate) forKey:kHasBtnUpdate];
    
    [encoder encodeObject:@(self.hasQLHasDecalActivation) forKey:kQLHasDecalActivation];
    
    [encoder encodeObject:@(self.hasQLHasBottleSale) forKey:kQLHasBottleSale];
    [encoder encodeObject:@(self.hasBottleSale) forKey:kHasBottleSale];
    
    [encoder encodeObject:self.nextExpiryDate forKey:kNextExpiryDate];
    [encoder encodeObject:@(self.nextExpiryPoints) forKey:kNextExpiryPoints];
    
    [encoder encodeObject:@(self.hasShowCoinPopup) forKey:kShowCoinPopup];
    [encoder encodeObject:@(self.hasShowTaxPopup) forKey:kShowTaxPopup];
    

    [encoder encodeObject:@(self.hasLubeMatch) forKey:kHasLubeMatch];
    [encoder encodeObject:@(self.hasQLHasLubeMatch) forKey:kQLHasLubeMatch];

    [encoder encodeObject:@(self.hasCSHasEarnNReward) forKey:kCSHasEarnNReward];

    [encoder encodeObject:@(self.hasAddNewRegWorkshop) forKey:kHasAddNewRegWorkshop];
    [encoder encodeObject:@(self.hasInventoryManagement) forKey:kHasInventoryManagement];
    [encoder encodeObject:@(self.hasQLHasInventoryManagement) forKey:kQLHasInventoryManagement];

    [encoder encodeObject:@(self.HasInventory) forKey:kHasInventory];

    [encoder encodeObject:@(self.hasCSMYHasEarnNReward) forKey:kCSMYHasEarnNReward];
    [encoder encodeObject:@(self.hasCSHasTHIncentive) forKey:kCSHasTHIncentive];
    [encoder encodeObject:@(self.hasCSHasTHOrderBreakdown) forKey:kCSHasTHOrderBreakdown];

    [encoder encodeObject:@(self.hasCSHasGamification) forKey:kCSHasGamification];
    [encoder encodeObject:@(self.gamification_HasCheckedInToday) forKey:kHasCheckedInToday];
    [encoder encodeObject:@(self.gamification_Milestone) forKey:kMilestone];

    [encoder encodeObject:@(self.hasCSHasTiering) forKey:kCSHasTiering];
    [encoder encodeObject:@(self.hasPCMOGamification) forKey:kCSHasPCMOGamification];
    [encoder encodeObject:@(self.hasPilotPromotion) forKey:kCSHasPilotPromotion];
    [encoder encodeObject:self.loyaltyTHAsOfDate forKey:kTHAsOfDate];
}
@end
