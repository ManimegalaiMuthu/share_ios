//
//  CompanyAddress.h
//  Shell
//
//  Created by Ankita Chhikara on 26/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CompanyAddress : NSObject

@property NSInteger addressType;
@property(nonatomic,strong) NSString *address1;
@property(nonatomic,strong) NSString *address2;
@property(nonatomic,strong) NSString *streetName;
@property(nonatomic,strong) NSString *companyCity;
@property(nonatomic,strong) NSString *companyState;
@property(nonatomic,strong) NSString *postalCode;
@property double longitude;
@property double latitude;

-(id)initWithData:(NSDictionary *)userData;
-(NSDictionary *) getJSON;
@end
