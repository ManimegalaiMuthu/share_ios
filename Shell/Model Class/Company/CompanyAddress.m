//
//  CompanyAddress.m
//  Shell
//
//  Created by Ankita Chhikara on 26/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "CompanyAddress.h"

@implementation CompanyAddress

#define kAddressType    @"AddressType"
#define kAddress1       @"Address1"
#define kAddress2       @"Address2"
#define kStreetName     @"StreetName"
#define kCompanyCity    @"CompanyCity"
#define kCompanyState   @"CompanyState"
#define kPostalCode     @"PostalCode"
#define kLongitude      @"Longitude"
#define kLatitude       @"Latitude"


-(id)init
{
    self = [super init];
    if (self)
    {
        self.addressType    = 0;
        
        self.address1       = @"";
        self.address2       = @"";
        
        self.streetName     = @"";
        self.companyCity    = @"";
        self.companyState   = @"0";
        self.postalCode     = @"";
        
        self.longitude      = 0;
        self.latitude       = 0;
    }
    return self;
}

-(id)initWithData:(NSDictionary *)userData
{
    self = [super init];
    if (self) {
        
        self.addressType    = [[userData objectForKey: kAddressType] intValue];
        
        self.address1       = [userData objectForKey: kAddress1];
        self.address2       = [userData objectForKey: kAddress2];
        
        self.streetName     = [userData objectForKey: kStreetName];
        self.companyCity    = [userData objectForKey: kCompanyCity];
        self.companyState   = [userData objectForKey: kCompanyState];
        self.postalCode     = [userData objectForKey: kPostalCode];
        
        self.longitude      = [[userData objectForKey: kLongitude] doubleValue];
        self.latitude       = [[userData objectForKey: kLatitude] doubleValue];
        
        
        if ([self.address1 isKindOfClass: [NSNull class]] || !self.address1)
            self.address1          = @"";
        if ([self.address2 isKindOfClass: [NSNull class]] || !self.address2)
            self.address2          = @"";
        
        
        if ([self.streetName isKindOfClass: [NSNull class]] || !self.streetName)
            self.streetName          = @"";
        if ([self.companyCity isKindOfClass: [NSNull class]] || !self.companyCity)
            self.companyCity          = @"";
        if ([self.companyState isKindOfClass: [NSNull class]] || !self.companyState)
            self.companyState          = @"";
        if ([self.postalCode isKindOfClass: [NSNull class]] || !self.postalCode)
            self.postalCode          = @"";
        
        
    }
    return self;
}

-(NSDictionary *) getJSON
{
    return @{
             kAddressType:      @(self.addressType),
             
             kAddress1:         self.address1,
             kAddress2:         self.address2,
             
             kStreetName:       self.streetName,
             kCompanyCity:      (!self.companyCity)?@"":self.companyCity,
             kCompanyState:     self.companyState,
             kPostalCode:       self.postalCode,
             
             kLongitude:        @(self.longitude),
             kLatitude:         @(self.latitude),
             @"IsActive": @(YES),
             };
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init]) {
        self.addressType    = [[decoder decodeObjectForKey: kAddressType] intValue];
        
        self.address1       = [decoder decodeObjectForKey: kAddress1];
        self.address2       = [decoder decodeObjectForKey: kAddress2];
        
        self.streetName     = [decoder decodeObjectForKey: kStreetName];
        self.companyCity    = [decoder decodeObjectForKey: kCompanyCity];
        self.companyState   = [decoder decodeObjectForKey: kCompanyState];
        self.postalCode     = [decoder decodeObjectForKey: kPostalCode];
        
        self.longitude      = [[decoder decodeObjectForKey: kLongitude] doubleValue];
        self.latitude       = [[decoder decodeObjectForKey: kLatitude] doubleValue];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject: @(self.addressType)    forKey: kAddressType];
    
    [encoder encodeObject: self.address1       forKey: kAddress1];
    [encoder encodeObject: self.address2       forKey: kAddress2];
    
    [encoder encodeObject: self.streetName     forKey: kStreetName];
    [encoder encodeObject: self.companyCity    forKey: kCompanyCity];
    [encoder encodeObject: self.companyState   forKey: kCompanyState];
    
    [encoder encodeObject: self.postalCode      forKey: kPostalCode];
    [encoder encodeObject: @(self.longitude)       forKey: kLongitude];
    [encoder encodeObject: @(self.latitude)        forKey: kLatitude];
}

@end
