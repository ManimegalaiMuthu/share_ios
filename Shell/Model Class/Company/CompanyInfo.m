//
//  CompanyInfo.m
//  Shell
//
//  Created by Ankita Chhikara on 26/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "CompanyInfo.h"

@implementation CompanyInfo

#define kReferenceCode          @"ReferenceCode"        //Workshop Code
#define kWorkshopQRCode         @"WorkshopQRCode"       //Workshop SHARE Code (for QR Scan)
#define kCompanyType            @"CompanyType"
#define kCompanyCategory        @"CompanyCategory"
#define kCompanyName            @"CompanyName"
#define kCompanyEmailAddress    @"CompanyEmailAddress"
#define kCompanyTier            @"CompanyTier"
#define kContactNumber          @"ContactNumber"
#define kOilChangePerMonth      @"OilChangePerMonth"
#define kOilChangePercentage    @"OilChangePercentage"
#define kHalfSyntheticOilChanges    @"HalfSyntheticOilChanges"
#define kShellHelixQty          @"ShellHelixQty"
#define kStatus                 @"Status"
#define kRemarks                @"Remarks"
#define kApprovalStatus         @"ApprovalStatus"

#define kCompanySource         @"CompanySource"

#define kOutletCodes           @"OutletCodes"
#define kPOSTaxID              @"POSTaxID"
#define kHasDSRManageStaff  @"HasDSRManageStaff"

-(id)init
{
    self = [super init];
    if (self)
    {
        self.referenceCode          = @"";
        self.workshopQRCode         = @"";
        
        self.companyType            = 0;
        self.companyCategory        = 0;
        self.companySource          = @"";
        
        self.companyName            = @"";
        self.companyEmailAddress    = @"";
        self.companyTier            = @"";
        self.contactNumber          = @"";
        
        self.oilChangePerMonth      = @"";
        self.oilChangePercentage    = @"";
        self.halfSyntheticOilChanges = @"";
        self.shellHelixQty          = @"";
        self.status                 = 0;
        self.remarks                = @"";
        self.approvalStatus         = @"";
        
        self.outletCodes            = @"";
        self.posTaxID               = @"";
        
        self.hasDSRManageStaff = NO;
    }
    return self;
}

-(id)initWithData:(NSDictionary *)userData
{
    self = [super init];
    if (self) {
        self.referenceCode          = [userData objectForKey: kReferenceCode];
        self.workshopQRCode         = [userData objectForKey: kWorkshopQRCode];
        
        self.companyType            = [[userData objectForKey: kCompanyType] intValue];
        self.companyCategory            = [[userData objectForKey: kCompanyCategory] intValue];
        
        self.companySource            = [userData objectForKey: kCompanySource];
        self.companyName            = [userData objectForKey: kCompanyName];
        self.companyEmailAddress    = [userData objectForKey: kCompanyEmailAddress];
        self.companyTier            = [userData objectForKey: kCompanyTier];
        self.contactNumber          = [userData objectForKey: kContactNumber];
        
        self.oilChangePerMonth      = [userData objectForKey: kOilChangePerMonth];
        self.oilChangePercentage    = [userData objectForKey: kOilChangePercentage];
        self.halfSyntheticOilChanges    = [userData objectForKey: kHalfSyntheticOilChanges];
        self.shellHelixQty          = [userData objectForKey: kShellHelixQty];
        self.status                 = [[userData objectForKey: kStatus] intValue];
        self.remarks                = [userData objectForKey: kRemarks];
        
        self.outletCodes             = [userData objectForKey: kOutletCodes];
        self.posTaxID                = [userData objectForKey: kPOSTaxID];
        
        self.approvalStatus         = [userData objectForKey: kApprovalStatus];
        
        self.hasDSRManageStaff = [[userData objectForKey: kHasDSRManageStaff] boolValue];
        
        //null handlers
        if ([self.referenceCode isKindOfClass: [NSNull class]] || !self.referenceCode)
            self.referenceCode          = @"";
        if ([self.workshopQRCode isKindOfClass: [NSNull class]] || !self.workshopQRCode)
            self.workshopQRCode         = @"";
        if ([self.companySource isKindOfClass: [NSNull class]] || !self.companySource)
            self.companySource            = @"";
        if ([self.companyName isKindOfClass: [NSNull class]] || !self.companyName)
            self.companyName            = @"";
        if ([self.companyEmailAddress isKindOfClass: [NSNull class]] || !self.companyEmailAddress)
            self.companyEmailAddress    = @"";
        if ([self.companyTier isKindOfClass: [NSNull class]] || !self.companyTier)
            self.companyTier            = @"";
        if ([self.contactNumber isKindOfClass: [NSNull class]] || !self.contactNumber)
            self.contactNumber          = @"";
        if (!self.status)
            self.status                 = 0;
        if ([self.approvalStatus isKindOfClass: [NSNull class]] || !self.approvalStatus)
            self.approvalStatus         = @"";
        if ([self.oilChangePerMonth isKindOfClass: [NSNull class]] || !self.oilChangePerMonth)
            self.oilChangePerMonth = @"";
        if ([self.oilChangePercentage isKindOfClass: [NSNull class]] || !self.oilChangePercentage)
            self.oilChangePercentage = @"";
        if ([self.halfSyntheticOilChanges isKindOfClass: [NSNull class]] || !self.halfSyntheticOilChanges)
            self.halfSyntheticOilChanges = @"";
        if ([self.shellHelixQty isKindOfClass: [NSNull class]] || !self.shellHelixQty)
            self.shellHelixQty = @"";
        if ([self.remarks isKindOfClass: [NSNull class]] || !self.remarks)
            self.remarks = @"";
        if ([self.outletCodes isKindOfClass: [NSNull class]] || !self.outletCodes)
            self.outletCodes = @"";
        if ([self.posTaxID isKindOfClass: [NSNull class]] || !self.posTaxID)
            self.posTaxID = @"";
        
        
        
    }
    
    return self;
}
-(NSDictionary *) getJSON
{
    
    return @{
             kReferenceCode:            self.referenceCode,
             kWorkshopQRCode:           self.workshopQRCode,
             
             kCompanyType:              @(self.companyType),
             kCompanyCategory:              @(self.companyCategory),
             
             kCompanySource:            self.companySource,
             kCompanyName:              self.companyName,
             kCompanyEmailAddress:      self.companyEmailAddress,
             kCompanyTier:              self.companyTier,
             kContactNumber:            self.contactNumber,
             
             kOilChangePerMonth:        self.oilChangePerMonth,
             kOilChangePercentage:      self.oilChangePercentage,
             kHalfSyntheticOilChanges:  self.halfSyntheticOilChanges,
             kShellHelixQty:            self.shellHelixQty,
             kStatus:                   @(self.status),
             kRemarks:                  self.remarks,
             kApprovalStatus:           self.approvalStatus,
             @"ProgramID": @"1",
             kHasDSRManageStaff : @(self.hasDSRManageStaff),
             kOutletCodes      :  self.outletCodes,
             kPOSTaxID          : self.posTaxID,
             
             };
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init]) {
        self.referenceCode          = [decoder decodeObjectForKey: kReferenceCode];
        self.workshopQRCode         = [decoder decodeObjectForKey: kWorkshopQRCode];
        
        self.companySource            = [decoder decodeObjectForKey: kCompanySource];
        self.companyType            = [[decoder decodeObjectForKey: kCompanyType] intValue];
        self.companyCategory        = [[decoder decodeObjectForKey: kCompanyCategory] intValue];
        
        self.companyName            = [decoder decodeObjectForKey: kCompanyName];
        self.companyEmailAddress    = [decoder decodeObjectForKey: kCompanyEmailAddress];
        self.companyTier            = [decoder decodeObjectForKey: kCompanyTier];
        self.contactNumber          = [decoder decodeObjectForKey: kContactNumber];
        
        self.oilChangePerMonth      = [decoder decodeObjectForKey: kOilChangePerMonth];
        self.oilChangePercentage    = [decoder decodeObjectForKey: kOilChangePercentage];
        self.halfSyntheticOilChanges = [decoder decodeObjectForKey: kHalfSyntheticOilChanges];
        self.shellHelixQty          = [decoder decodeObjectForKey: kShellHelixQty];
        
        self.status                =  [[decoder decodeObjectForKey: kStatus] intValue];
        self.remarks                = [decoder decodeObjectForKey: kRemarks];
        self.approvalStatus         =  [decoder decodeObjectForKey: kApprovalStatus];
        
        self.outletCodes                = [decoder decodeObjectForKey: kOutletCodes];
        self.posTaxID                = [decoder decodeObjectForKey: kPOSTaxID];
        
        self.hasDSRManageStaff = [[decoder decodeObjectForKey: kHasDSRManageStaff] boolValue];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject: self.referenceCode       forKey: kReferenceCode];
    [encoder encodeObject: self.workshopQRCode      forKey: kWorkshopQRCode];
    
    [encoder encodeObject: @(self.companyType)      forKey: kCompanyType];
    [encoder encodeObject: @(self.companyCategory)      forKey: kCompanyCategory];
    
    [encoder encodeObject: self.companySource         forKey: kCompanySource];
    [encoder encodeObject: self.companyName         forKey: kCompanyName];
    [encoder encodeObject: self.companyEmailAddress forKey: kCompanyEmailAddress];
    [encoder encodeObject: self.companyTier         forKey: kCompanyTier];
    [encoder encodeObject: self.contactNumber       forKey: kContactNumber];
    
    [encoder encodeObject: self.oilChangePerMonth   forKey: kOilChangePerMonth];
    [encoder encodeObject: self.oilChangePercentage forKey: kOilChangePercentage];
    [encoder encodeObject: self.halfSyntheticOilChanges forKey: kHalfSyntheticOilChanges];
    [encoder encodeObject: self.shellHelixQty       forKey: kShellHelixQty];
    
    [encoder encodeObject: @(self.status)              forKey: kStatus];
    [encoder encodeObject: self.remarks             forKey: kRemarks];
    [encoder encodeObject: self.approvalStatus             forKey: kApprovalStatus];
    
    [encoder encodeObject: self.outletCodes             forKey: kOutletCodes];
    [encoder encodeObject: self.posTaxID             forKey: kPOSTaxID];
    
    [encoder encodeObject: @(self.hasDSRManageStaff)       forKey: kHasDSRManageStaff];
}
@end
