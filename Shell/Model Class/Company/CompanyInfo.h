//
//  CompanyInfo.h
//  Shell
//
//  Created by Ankita Chhikara on 26/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CompanyInfo : NSObject

@property(nonatomic,strong) NSString *referenceCode;
@property(nonatomic,strong) NSString *workshopQRCode;

@property NSInteger companyType;
@property NSInteger companyCategory;

@property(nonatomic,strong) NSString *companyName;
@property(nonatomic,strong) NSString *companyEmailAddress;
@property(nonatomic,strong) NSString *companySource;
@property(nonatomic,strong) NSString *companyTier;

@property(nonatomic,strong) NSString *contactNumber;

@property(nonatomic,strong) NSString *oilChangePerMonth;
@property(nonatomic,strong) NSString *oilChangePercentage;
@property(nonatomic,strong) NSString *halfSyntheticOilChanges;
@property(nonatomic,strong) NSString *shellHelixQty;

@property NSInteger status;
@property(nonatomic,strong) NSString *remarks;
@property(nonatomic,strong) NSString *approvalStatus;

@property(nonatomic,strong) NSString *outletCodes;
@property(nonatomic,strong) NSString *posTaxID;

@property BOOL hasDSRManageStaff;

- (id)initWithData:(NSDictionary *)userData;
- (NSDictionary *) getJSON;
@end
