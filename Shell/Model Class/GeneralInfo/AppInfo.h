//
//  AppInfo.h
//  JTIStarPartners
//
//  Created by Shekhar  on 11/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"


@interface AppInfo : NSObject

@property(nonatomic,strong) NSString *appVersion;
@property(nonatomic,strong) NSString *appBuild;
@property(nonatomic,strong) NSString *appName;
@property(nonatomic,assign) LanguageType appLanguage;

- (id)initWithAppInfo;

- (NSString *)appName;
- (NSString *)appVersion;
- (NSString *)appBuild;
- (NSNumber *)localisedLanguage;

@end
