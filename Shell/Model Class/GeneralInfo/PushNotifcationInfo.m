//
//  PushNotifcationInfo.m
//  JTIStarPartners
//
//  Created by Shekhar  on 12/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import "PushNotifcationInfo.h"
#import "Constants.h"
#import "Session.h"

@implementation PushNotifcationInfo

- (id)initWithPushNotificationInfo
{
    self = [super init];
    if (self) {
        self.pushToken = [self pushNotificationToken];
        self.isSubscribed = [self pushNotificationSubscribed];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init]) {
        self.pushToken = [decoder decodeObjectForKey:kPushToken];
        self.isSubscribed = [decoder decodeBoolForKey:@"isSubscribed"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.pushToken forKey:kPushToken];
    [encoder encodeBool:self.isSubscribed forKey:@"isSubscribed"];
}

- (NSString *)pushNotificationToken
{
    return GET_PUSHTOKEN;
}

- (BOOL)pushNotificationSubscribed
{
    return GET_PUSHNOTIFICATIONENABLED;
}

@end
