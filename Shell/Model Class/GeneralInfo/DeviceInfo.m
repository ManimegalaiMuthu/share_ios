//
//  DeviceInfo.m
//  JTIStarPartners
//
//  Created by Shekhar  on 8/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import "DeviceInfo.h"
#include <sys/sysctl.h>
#import <UIKit/UIKit.h>
#import "Constants.h"
#import "SFHFKeychainUtils.h"
#import "WebServiceManager.h"

#define CC_MD5_DIGEST_LENGTH	16
#define unsigned char *CC_MD5(const void *data, CC_LONG len, unsigned char *md);
static DeviceInfo *instance;

@implementation DeviceInfo

+ (DeviceInfo *) getInstance {
    @synchronized([DeviceInfo class]) {
        if ( instance == nil ) {
            instance = [[DeviceInfo alloc] init];
        }
    }
    return instance;
}

- (id)initWithDeviceInfo
{
    self = [super init];
    if (self) {
        self.deviceModel = [self platformString];
        
        if (EQUALS(self.deviceModel, kSimulator)) {
            self.deviceUUID = SIMULATOR_DEVICE_TOKEN;
        }
        else self.deviceUUID = [DeviceInfo getUniqueUUID];
        NSLOG(self.deviceUUID);
        self.iOSVersion = [self systemVersion];
        self.devicePlatform = [NSNumber numberWithInt:OS_DEVICE];
        if (IS_IPAD) {
            self.deviceType = [NSNumber numberWithInt:TABLET];
        }
        else self.deviceType = [NSNumber numberWithInt:SMART_PHONE];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init]) {
        self.iOSVersion = [decoder decodeObjectForKey:@"iOSVersion"];
        self.devicePlatform = [decoder decodeObjectForKey:kDevicePlatform];
        self.deviceType = [decoder decodeObjectForKey:kDeviceType];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.iOSVersion forKey:@"iOSVersion"];
    [encoder encodeObject:self.devicePlatform forKey:kDevicePlatform];
    [encoder encodeObject:self.deviceType forKey:kDeviceType];
    
}

- (NSString *)systemVersion
{
    return [UIDevice currentDevice].systemVersion;
}

- (NSString *)platform
{
    int mib[2];
    size_t len;
    char *machine;

    mib[0] = CTL_HW;
    mib[1] = HW_MACHINE;
    sysctl(mib, 2, NULL, &len, NULL, 0);
    machine = malloc(len);
    sysctl(mib, 2, machine, &len, NULL, 0);

    NSString *platform = [NSString stringWithCString:machine encoding:NSASCIIStringEncoding];
    free(machine);
    return platform;
}

- (NSString *) platformString{
    
    NSString *platform = [self platform];
    
#pragma mark - iPhone
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,2"])    return @"iPhone 4 GSM Rev A";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6s Plus";
    if ([platform isEqualToString:@"iPhone8,4"])    return @"iPhone SE (GSM)";
    if ([platform isEqualToString:@"iPhone9,1"])    return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,2"])    return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"iPhone9,3"])    return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,4"])    return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"iPhone10,1"])    return @"iPhone 8";
    if ([platform isEqualToString:@"iPhone10,2"])    return @"iPhone 8 Plus";
    if ([platform isEqualToString:@"iPhone10,3"])    return @"iPhone X Global";
    if ([platform isEqualToString:@"iPhone10,4"])    return @"iPhone 8";
    if ([platform isEqualToString:@"iPhone10,5"])    return @"iPhone 8 Plus";
    if ([platform isEqualToString:@"iPhone10,6"])    return @"iPhone X GSM";
    if ([platform isEqualToString:@"iPhone11,2"])    return @"iPhone XS";
    if ([platform isEqualToString:@"iPhone11,4"])    return @"iPhone XS Max";
    if ([platform isEqualToString:@"iPhone11,6"])    return @"iPhone XS Max Global";
    if ([platform isEqualToString:@"iPhone11,8"])    return @"iPhone XR";
    
#pragma mark - iPod Touch
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPod7,1"])    return @"iPod Touch 6G";
    if ([platform isEqualToString:@"iPod9,1"])    return @"iPod Touch 7G";
    

#pragma mark - iPad
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad1,2"])      return @"iPad 3G";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([platform isEqualToString:@"iPad4,3"])      return @"iPad Air (China)";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad Mini Retina (WiFi)";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad Mini Retina (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,6"])      return @"iPad Mini Retina (China)";
    if ([platform isEqualToString:@"iPad4,7"])    return @"iPad Mini 3 (WiFi)";
    if ([platform isEqualToString:@"iPad4,8"])    return @"iPad Mini 3 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,9"])    return @"iPad Mini 3 (China)";
    if ([platform isEqualToString:@"iPad5,1"])    return @"iPad Mini 4 (WiFi)";
    if ([platform isEqualToString:@"iPad5,2"])    return @"4th Gen iPad mini (WiFi+Cellular)";
    if ([platform isEqualToString:@"iPad5,3"])    return @"iPad Air 2 (WiFi)";
    if ([platform isEqualToString:@"iPad5,4"])    return @"iPad Air 2 (Cellular)";
    if ([platform isEqualToString:@"iPad6,3"])    return @"iPad Pro (9.7 inch, WiFi)";
    if ([platform isEqualToString:@"iPad6,4"])    return @"iPad Pro (9.7 inch, WiFi+LTE)";
    if ([platform isEqualToString:@"iPad6,7"])    return @"iPad Pro (12.9 inch, WiFi)";
    if ([platform isEqualToString:@"iPad6,8"])    return @"iPad Pro (12.9 inch, WiFi+LTE)";
    if ([platform isEqualToString:@"iPad6,11"])    return @"iPad (2017)";
    if ([platform isEqualToString:@"iPad6,12"])    return @"iPad (2017)";
    if ([platform isEqualToString:@"iPad7,1"])    return @"iPad Pro 2nd Gen (WiFi)";
    if ([platform isEqualToString:@"iPad7,2"])    return @"iPad Pro 2nd Gen (WiFi+Cellular)";
    if ([platform isEqualToString:@"iPad7,3"])    return @"iPad Pro 10.5-inch";
    if ([platform isEqualToString:@"iPad7,4"])    return @"iPad Pro 10.5-inch";
    if ([platform isEqualToString:@"iPad7,5"])    return @"iPad 6th Gen (WiFi)";
    if ([platform isEqualToString:@"iPad7,6"])    return @"iPad 6th Gen (WiFi+Cellular)";
    if ([platform isEqualToString:@"iPad8,1"])    return @"iPad Pro 3rd Gen (11 inch, WiFi)";
    if ([platform isEqualToString:@"iPad8,2"])    return @"iPad Pro 3rd Gen (11 inch, 1TB, WiFi)";
    if ([platform isEqualToString:@"iPad8,3"])    return @"iPad Pro 3rd Gen (11 inch, WiFi+Cellular)";
    if ([platform isEqualToString:@"iPad8,4"])    return @"iPad Pro 3rd Gen (11 inch, 1TB, WiFi+Cellular)";
    if ([platform isEqualToString:@"iPad8,5"])    return @"iPad Pro 3rd Gen (12.9 inch, WiFi)";
    if ([platform isEqualToString:@"iPad8,6"])    return @"iPad Pro 3rd Gen (12.9 inch, 1TB, WiFi)";
    if ([platform isEqualToString:@"iPad8,7"])    return @"iPad Pro 3rd Gen (12.9 inch, WiFi+Cellular)";
    if ([platform isEqualToString:@"iPad8,8"])    return @"iPad Pro 3rd Gen (12.9 inch, 1TB, WiFi+Cellular)";
    if ([platform isEqualToString:@"iPad11,1"])    return @"iPad Mini 5th Gen (WiFi)";
    if ([platform isEqualToString:@"iPad11,2"])    return @"iPad Mini 5th Gen";
    if ([platform isEqualToString:@"iPad11,3"])    return @"iPad Air 3rd Gen (WiFi)";
    if ([platform isEqualToString:@"iPad11,4"])    return @"iPad Air 3rd Gen";
    
#pragma mark - Apple Watch
    if ([platform isEqualToString:@"Watch1,1"])    return @"Apple Watch 38mm case";
    if ([platform isEqualToString:@"Watch1,2"])    return @"Apple Watch 42mm case";
    if ([platform isEqualToString:@"Watch2,6"])    return @"Apple Watch Series 1 38mm case";
    if ([platform isEqualToString:@"Watch2,7"])    return @"Apple Watch Series 1 42mm case";
    if ([platform isEqualToString:@"Watch2,3"])    return @"Apple Watch Series 2 38mm case";
    if ([platform isEqualToString:@"Watch2,4"])    return @"Apple Watch Series 2 42mm case";
    if ([platform isEqualToString:@"Watch3,1"])    return @"Apple Watch Series 3 38mm case (GPS+Cellular)";
    if ([platform isEqualToString:@"Watch3,2"])    return @"Apple Watch Series 3 42mm case (GPS+Cellular)";
    if ([platform isEqualToString:@"Watch3,3"])    return @"Apple Watch Series 3 38mm case (GPS)";
    if ([platform isEqualToString:@"Watch3,4"])    return @"Apple Watch Series 3 42mm case (GPS)";
    if ([platform isEqualToString:@"Watch4,1"])    return @"Apple Watch Series 4 40mm case (GPS)";
    if ([platform isEqualToString:@"Watch4,2"])    return @"Apple Watch Series 4 44mm case (GPS)";
    if ([platform isEqualToString:@"Watch4,3"])    return @"Apple Watch Series 4 40mm case (GPS+Cellular)";
    if ([platform isEqualToString:@"Watch4,4"])    return @"Apple Watch Series 4 44mm case (GPS+Cellular)";
    
#pragma mark - Others
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return platform;
}

- (int)getDeviceWidth
{
    if (SCREEN_WIDTH == iPHONE5WIDTH) {
        return 1;
    }
    else if (SCREEN_WIDTH == iPHONE6WIDTH) {
        return 2;
    }
    else
    {
        return 3;
    }
}

+ (NSString *) getUniqueUUID {
    NSError * error;
    
    NSString *uuid = [[NSUserDefaults standardUserDefaults] stringForKey:KEYCHAIN_SERVICE_NAME];
    BOOL vendorIDMissingFromUserDefaults = (uuid == nil || uuid.length == 0);
    
    if (vendorIDMissingFromUserDefaults) {
  
    uuid = [SFHFKeychainUtils getPasswordForUsername:SECRET_TOKEN_VALUE andServiceName:KEYCHAIN_SERVICE_NAME error:&error];
    [[NSUserDefaults standardUserDefaults] setValue:uuid forKey:KEYCHAIN_SERVICE_NAME];
    if (error) {
        NSLog(@"Error geting unique UUID for this device! %@", [error localizedDescription]);
        return nil;
    }
    if (!uuid) {
        NSLog(@"No UUID found. Creating a new one.");
        uuid = [self GetUUID];
        //uuid = [self md5String:uuid];
        [SFHFKeychainUtils storeUsername:SECRET_TOKEN_VALUE andPassword:uuid forServiceName:KEYCHAIN_SERVICE_NAME updateExisting:YES error:&error];
        [[NSUserDefaults standardUserDefaults] setValue:uuid forKey:KEYCHAIN_SERVICE_NAME];
        if (error) {
            NSLog(@"Error geting unique UUID for this device! %@", [error localizedDescription]);
            return nil;
        }
    }
    }
    return uuid;
}


//+ (NSString *)md5String:(NSString *)plainText
//{
//    if(plainText == nil || [plainText length] == 0)
//        return nil;
//    
//    const char *value = [plainText UTF8String];
//    unsigned char outputBuffer[CC_MD5_DIGEST_LENGTH];
//    CC_MD5(value, strlen(value), outputBuffer);
//    
//    NSMutableString *outputString = [[NSMutableString alloc] initWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
//    for(NSInteger count = 0; count < CC_MD5_DIGEST_LENGTH; count++){
//        [outputString appendFormat:@"%02x",outputBuffer[count]];
//    }
//    NSString * retString = [NSString stringWithString:outputString];
//    return retString;
//}


+ (NSString *)GetUUID
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge NSString *)string;
}
@end
