//
//  UserCredentials.m
//  JTIStarPartners
//
//  Created by Shekhar  on 11/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import "UserCredentials.h"
#import "Constants.h"
#import "Session.h"

@implementation UserCredentials

@synthesize loginName = _loginName;
@synthesize loginPassword = _loginPassword;
@synthesize changedPassword = _changedPassword;

- (id)initWithUserCredentials
{
    self = [super init];
    
    if(self)
    {
        self.loginPassword = [self userName];
        self.loginName = [self password];
    }
    
    return self;
}

- (id)initWithUsername:(NSString *)userName andPassword:(NSString *)password

{
    self = [super init];
    
    if(self)
    {
        self.loginPassword = password;
        self.loginName = userName;
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init]) {
        self.loginName = [decoder decodeObjectForKey:kLoginName];
        self.loginPassword = [decoder decodeObjectForKey:kPassword];

    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.loginName forKey:kLoginName];
    [encoder encodeObject:self.loginPassword forKey:kPassword];
 
}


- (NSString *)userName
{
    return LOGIN_NAME;
}

- (NSString *)password

{
    return LOGIN_PASSWORD;
}

@end
