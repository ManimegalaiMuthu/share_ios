//
//  LoginInfo.m
//  JTIStarPartners
//
//  Created by Shekhar  on 11/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import "LoginInfo.h"

@implementation LoginInfo

- (id)initWithLoginInfo //: (NSString *) dialingCode
{
    self = [super init];
    if (self) {
        self.credentials = [[UserCredentials alloc]initWithUserCredentials];
        self.deviceInfo = [[DeviceInfo alloc]initWithDeviceInfo];
        self.appInfo = [[AppInfo alloc]initWithAppInfo];
        self.pushNotificationInfo = [[PushNotifcationInfo alloc]initWithPushNotificationInfo];
        
        self.tradeID = @"";
        self.programID = @"";
        self.companyType = @"";
        self.companyCategory = @"";
        
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init]) {
        self.credentials = [decoder decodeObjectForKey:@"Credentials"];
        self.deviceInfo = [decoder decodeObjectForKey:@"DeviceInfo"];
        self.appInfo = [decoder decodeObjectForKey:@"AppInfo"];
        self.pushNotificationInfo = [decoder decodeObjectForKey:@"PushNotifcationInfo"];
        self.countryCode = [decoder decodeObjectForKey:@"CountryCode"];
        self.tradeID = [decoder decodeObjectForKey:@"TradeID"];
        self.programID = [decoder decodeObjectForKey:@"ProgramID"];
        self.companyType = [decoder decodeObjectForKey:@"CompanyType"];
        self.companyCategory = [decoder decodeObjectForKey:@"CompanyCategory"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.credentials forKey:@"Credentials"];
    [encoder encodeObject:self.deviceInfo forKey:@"DeviceInfo"];
    [encoder encodeObject:self.appInfo forKey:@"AppInfo"];
    [encoder encodeObject:self.pushNotificationInfo forKey:@"PushNotifcationInfo"];
    [encoder encodeObject:self.countryCode forKey:@"CountryCode"];
    [encoder encodeObject:self.tradeID forKey:@"TradeID"];
    [encoder encodeObject:self.programID forKey:@"ProgramID"];
    [encoder encodeObject:self.companyType forKey:@"CompanyType"];
    [encoder encodeObject:self.companyCategory forKey:@"CompanyCategory"];
    
}

@end
