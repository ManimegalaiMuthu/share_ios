//
//  AppInfo.m
//  JTIStarPartners
//
//  Created by Shekhar  on 11/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import "AppInfo.h"

@implementation AppInfo

- (id)initWithAppInfo
{
    self = [super init];
    if (self) {
        self.appVersion = [self appVersion];
        self.appBuild = [self appBuild];
        self.appName = [self appName];
        self.appLanguage = [[self localisedLanguage] intValue];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init]) {
        self.appVersion = [decoder decodeObjectForKey:kAppVersion];
        self.appBuild = [decoder decodeObjectForKey:kAppBuild];
        self.appName = [decoder decodeObjectForKey:kAppName];
        self.appLanguage = (LanguageType) [decoder decodeObjectForKey:kLanguageCode];
    
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.appVersion forKey:kAppVersion];
    [encoder encodeObject:self.appBuild forKey:kAppBuild];
    [encoder encodeObject:self.appName forKey:kAppName];
    [encoder encodeObject:[NSNumber numberWithInt:self.appLanguage] forKey:kLanguageCode];
}



- (NSString *)appName
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
}

- (NSString *)appVersion
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

- (NSString *)appBuild
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
}

- (NSNumber *)localisedLanguage
{
    return [NSNumber numberWithInteger:[kAvailableLanguages indexOfObject:GET_LOCALIZATION]];
}

@end
