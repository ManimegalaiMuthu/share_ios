//
//  UserCredentials.h
//  JTIStarPartners
//
//  Created by Shekhar  on 11/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UserCredentials : NSObject

@property(nonatomic,strong) NSString *loginName;
@property(nonatomic,strong) NSString *loginPassword;
@property(nonatomic,strong) NSString *changedPassword;

- (id)initWithUserCredentials;
- (id)initWithUsername:(NSString *)userName andPassword:(NSString *)password;

- (NSString *)userName;
- (NSString *)password;

@end
