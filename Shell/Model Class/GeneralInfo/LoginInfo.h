//
//  LoginInfo.h
//  JTIStarPartners
//
//  Created by Shekhar  on 11/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserCredentials.h"
#import "DeviceInfo.h"
#import "AppInfo.h"
#import "PushNotifcationInfo.h"

@interface LoginInfo : NSObject

@property (nonatomic,strong) UserCredentials *credentials;
@property (nonatomic,strong) DeviceInfo *deviceInfo;
@property (nonatomic,strong) AppInfo *appInfo;
@property (nonatomic,strong) PushNotifcationInfo *pushNotificationInfo;
@property (nonatomic,strong) NSString *countryCode;
@property (nonatomic,strong) NSString *tradeID;
@property (nonatomic,strong) NSString *programID;
@property (nonatomic,strong) NSString *companyType;
@property (nonatomic,strong) NSString *companyCategory;

- (id)initWithLoginInfo;

@end
