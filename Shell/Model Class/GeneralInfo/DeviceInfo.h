//
//  DeviceInfo.h
//  JTIStarPartners
//
//  Created by Shekhar  on 8/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    kiPHONE5     = 1,
    kiPHONE6     = 2,
    kiPHONE6PLUS = 3
} DeviceWidth;


#define mDevice ((DeviceInfo *) [DeviceInfo getInstance])

@interface DeviceInfo : NSObject

@property (nonatomic,strong) NSString *deviceUUID;
@property (nonatomic,strong) NSString *iOSVersion;
@property (nonatomic,strong) NSString *deviceModel;
@property (nonatomic,strong) NSNumber* devicePlatform;
@property (nonatomic,strong) NSNumber* deviceType;

+ (DeviceInfo *)getInstance;
- (id)initWithDeviceInfo;
- (int)getDeviceWidth;

@end
