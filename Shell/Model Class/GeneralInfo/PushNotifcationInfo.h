//
//  PushNotifcationInfo.h
//  JTIStarPartners
//
//  Created by Shekhar  on 12/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PushNotifcationInfo : NSObject

@property (nonatomic,strong) NSString *pushToken;
@property (nonatomic,assign) BOOL isSubscribed;

- (id)initWithPushNotificationInfo;

@end
