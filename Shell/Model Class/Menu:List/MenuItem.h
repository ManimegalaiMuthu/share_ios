//
//  MenuItem.h
//  JTIStarPartners
//
//  Created by Shekhar  on 27/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuItem : NSObject

@property (nonatomic,strong) NSString *icon;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *className;

- (id)initWithData:(NSDictionary *)data;


@end
