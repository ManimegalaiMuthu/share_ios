//
//  MenuItem.m
//  JTIStarPartners
//
//  Created by Shekhar  on 27/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import "MenuItem.h"

@implementation MenuItem

- (id)initWithData:(NSDictionary *)data
{
    self = [super init];
    if (self) {
        self.icon = [data objectForKey:@"icon"];
        self.title = [data objectForKey:@"title"];
        self.className = [data objectForKey:@"viewcontroller"];
    }
    
    return self;
}

@end
