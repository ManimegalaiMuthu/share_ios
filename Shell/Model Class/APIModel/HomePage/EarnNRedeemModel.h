//
//  EarnNRedeemModel.h
//  Shell
//
//  Created by Nach on 24/7/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface EarnNRedeemModel : NSObject

@property NSString *bottles;
@property NSString *pointsToReachMilestone;
@property NSString *productGroup;
@property NSString *productGroupID;
@property NSString *pointsPerBottle;

-(id) initWithData:(NSDictionary *) userData;
-(NSDictionary *)getJSON;

@end

NS_ASSUME_NONNULL_END
