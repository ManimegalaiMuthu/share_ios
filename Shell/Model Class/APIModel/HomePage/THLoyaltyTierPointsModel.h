//
//  THLoyaltyTierPointsModel.h
//  Shell
//
//  Created by Nach on 27/7/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface THLoyaltyTierPointsModel : NSObject

@property NSString *startDate;
@property NSString *endDate;
@property NSString *workshopGroup;
@property NSString *target1;
@property NSString *target2;
@property NSString *target3;
@property NSString *target4;
@property NSString *target5;
@property NSString *workshopCode;
@property NSString *netSales;
@property NSString *tierTarget1;
@property NSString *tierTarget2;
@property NSString *tierTarget3;
@property NSString *tierTarget4;
@property NSString *tierTarget5;
@property NSInteger tier;
@property NSString *tierPoints;
@property NSString *bahtToBeEarned;
@property NSString *premiumValueBaht;
@property NSString *otherLubricantsValueBaht;
@property NSString *totalOrderValueBaht;
@property NSString *targetStepZone;
@property NSString *totalRewardValueBaht;
@property NSString *totalRewardPoints;
@property NSString *pointsAwardedToDate;
@property NSString *balancePointsToAwardThisWeek;

-(id) initWithData:(NSDictionary *) userData;
-(NSDictionary *)getJSON;

@end

NS_ASSUME_NONNULL_END
