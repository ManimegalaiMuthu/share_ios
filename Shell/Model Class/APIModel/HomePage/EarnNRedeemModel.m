//
//  EarnNRedeemModel.m
//  Shell
//
//  Created by Nach on 24/7/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "EarnNRedeemModel.h"

#define kBottles                @"Bottles"
#define kPointsToReachMilestone @"PointsToReachMilestone"
#define kProductGroup           @"ProductGroup"
#define kProductGroupID         @"ProductGroupID"
#define kPointsPerBottle        @"PointsPerBottle"

@implementation EarnNRedeemModel

-(id) init {
    self = [super init];
    if(self) {
        self.bottles                = @"";
        self.pointsToReachMilestone = @"";
        self.productGroup           = @"";
        self.productGroupID         = @"";
        self.pointsPerBottle        = @"";
    }
    return self;
}

-(id) initWithData:(NSDictionary *)userData {
    self = [super init];
    if(self) {
        if([[userData objectForKey:kBottles] isKindOfClass:[NSNull class]]) {
            self.bottles         = @"";
        } else {
            self.bottles         = [userData objectForKey:kBottles];
        }
        
        if([[userData objectForKey:kPointsToReachMilestone] isKindOfClass:[NSNull class]]) {
            self.pointsToReachMilestone         = @"";
        } else {
            self.pointsToReachMilestone         = [userData objectForKey:kPointsToReachMilestone];
        }
        
        if([[userData objectForKey:kProductGroup] isKindOfClass:[NSNull class]]) {
            self.productGroup         = @"";
        } else {
            self.productGroup         = [userData objectForKey:kProductGroup];
        }
        
        if([[userData objectForKey:kProductGroupID] isKindOfClass:[NSNull class]]) {
            self.productGroupID         = @"";
        } else {
            self.productGroupID         = [userData objectForKey:kProductGroupID];
        }
        
        if([[userData objectForKey:kPointsPerBottle] isKindOfClass:[NSNull class]]) {
            self.pointsPerBottle         = @"";
        } else {
            self.pointsPerBottle         = [userData objectForKey:kPointsPerBottle];
        }
    }
    return self;
}

-(NSDictionary *) getJSON {
    return @{
        kBottles                    : self.bottles,
        kPointsToReachMilestone     : self.pointsToReachMilestone,
        kProductGroup               : self.productGroup,
        kProductGroupID             : self.productGroupID,
        kPointsPerBottle            : self.pointsPerBottle,
    };
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init]) {
        self.productGroupID             = [decoder decodeObjectForKey:kProductGroupID];
        self.bottles                    = [decoder decodeObjectForKey:kBottles];
        self.productGroup               = [decoder decodeObjectForKey:kProductGroup];
        self.pointsPerBottle            = [decoder decodeObjectForKey:kPointsPerBottle];
        self.pointsToReachMilestone     = [decoder decodeObjectForKey:kPointsToReachMilestone];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.productGroupID forKey:kProductGroupID];
    [encoder encodeObject:self.bottles forKey:kBottles];
    [encoder encodeObject:self.productGroup forKey:kProductGroup];
    [encoder encodeObject:self.pointsPerBottle forKey:kPointsPerBottle];
    [encoder encodeObject:self.pointsToReachMilestone forKey:kPointsToReachMilestone];
}

@end
