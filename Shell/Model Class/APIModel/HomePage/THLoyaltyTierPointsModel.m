//
//  THLoyaltyTierPointsModel.m
//  Shell
//
//  Created by Nach on 27/7/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "THLoyaltyTierPointsModel.h"

#define kStartdate                          @"Startdate"
#define kEnddate                            @"Enddate"
#define kWorkshopGroup                      @"WorkshopGroup"
#define kTarget1                            @"Target1"
#define kTarget2                            @"Target2"
#define kTarget3                            @"Target3"
#define kTarget4                            @"Target4"
#define kTarget5                            @"Target5"
#define kWorkshopCode                       @"WorkshopCode"
#define kNetSales                           @"NetSales"
#define kTierTarget1                        @"TierTarget1"
#define kTierTarget2                        @"TierTarget2"
#define kTierTarget3                        @"TierTarget3"
#define kTierTarget4                        @"TierTarget4"
#define kTierTarget5                        @"TierTarget5"
#define kTier                               @"Tier"
#define kTierPoints                         @"TierPoints"
#define kBahtTobeEearned                    @"BahtTobeEearned"
#define kPremiumValueBaht                   @"PremiumValueBaht"
#define kOtherLubricantsValueBaht           @"OtherLubricantsValueBaht"
#define kTotalOrderValueBaht                @"TotalOrderValueBaht"
#define kTargetStepZone                     @"TargetStepZone"
#define kTotalRewardValueBaht               @"TotalRewardValueBaht"
#define kTotalRewardPoints                  @"TotalRewardPoints"
#define kPointsAwardedtodate                @"PointsAwardedtodate"
#define kBalancePointstoAwardthisWeek       @"BalancePointstoAwardthisWeek"

@implementation THLoyaltyTierPointsModel

-(id) init {
    self = [super init];
    if(self) {
        self.startDate                      = @"";
        self.endDate                        = @"";
        self.workshopGroup                  = @"";
        self.target1                        = @"";
        self.target2                        = @"";
        self.target3                        = @"";
        self.target4                        = @"";
        self.target5                        = @"";
        self.workshopCode                   = @"";
        self.netSales                       = @"";
        self.tierTarget1                    = @"";
        self.tierTarget2                    = @"";
        self.tierTarget3                    = @"";
        self.tierTarget4                    = @"";
        self.tierTarget5                    = @"";
        self.tier                           = 0;
        self.tierPoints                     = @"";
        self.bahtToBeEarned                 = @"";
        self.premiumValueBaht               = @"";
        self.otherLubricantsValueBaht       = @"";
        self.totalOrderValueBaht            = @"";
        self.targetStepZone                 = @"";
        self.totalRewardValueBaht           = @"";
        self.totalRewardPoints              = @"";
        self.pointsAwardedToDate            = @"";
        self.balancePointsToAwardThisWeek   = @"";
    }
    return self;
}

-(id) initWithData:(NSDictionary *) userData {
   self = [super init];
    if(self) {
        if([[userData objectForKey:kStartdate] isKindOfClass:[NSNull class]]) {
            self.startDate         = @"";
        } else {
            self.startDate         = [userData objectForKey:kStartdate];
        }
        
        if([[userData objectForKey:kEnddate] isKindOfClass:[NSNull class]]) {
            self.endDate         = @"";
        } else {
            self.endDate         = [userData objectForKey:kEnddate];
        }
        
        if([[userData objectForKey:kWorkshopGroup] isKindOfClass:[NSNull class]]) {
            self.workshopGroup         = @"";
        } else {
            self.workshopGroup         = [userData objectForKey:kWorkshopGroup];
        }
        
        if([[userData objectForKey:kTarget1] isKindOfClass:[NSNull class]]) {
            self.target1         = @"";
        } else {
            self.target1         = [userData objectForKey:kTarget1];
        }
        
        if([[userData objectForKey:kTarget2] isKindOfClass:[NSNull class]]) {
            self.target2         = @"";
        } else {
            self.target2         = [userData objectForKey:kTarget2];
        }
        
        if([[userData objectForKey:kTarget3] isKindOfClass:[NSNull class]]) {
            self.target3         = @"";
        } else {
            self.target3         = [userData objectForKey:kTarget3];
        }
        
        if([[userData objectForKey:kTarget4] isKindOfClass:[NSNull class]]) {
            self.target4         = @"";
        } else {
            self.target4         = [userData objectForKey:kTarget4];
        }
        
        if([[userData objectForKey:kTarget5] isKindOfClass:[NSNull class]]) {
            self.target5         = @"";
        } else {
            self.target5         = [userData objectForKey:kTarget5];
        }
        
        if([[userData objectForKey:kWorkshopCode] isKindOfClass:[NSNull class]]) {
            self.workshopCode         = @"";
        } else {
            self.workshopCode         = [userData objectForKey:kWorkshopCode];
        }
        
        if([[userData objectForKey:kNetSales] isKindOfClass:[NSNull class]]) {
            self.netSales         = @"";
        } else {
            self.netSales         = [userData objectForKey:kNetSales];
        }
        
        if([[userData objectForKey:kTierTarget1] isKindOfClass:[NSNull class]]) {
            self.tierTarget1         = @"";
        } else {
            self.tierTarget1         = [userData objectForKey:kTierTarget1];
        }
        
        if([[userData objectForKey:kTierTarget2] isKindOfClass:[NSNull class]]) {
            self.tierTarget2         = @"";
        } else {
            self.tierTarget2         = [userData objectForKey:kTierTarget2];
        }
        
        if([[userData objectForKey:kTierTarget3] isKindOfClass:[NSNull class]]) {
            self.tierTarget3         = @"";
        } else {
            self.tierTarget3         = [userData objectForKey:kTierTarget3];
        }
        
        if([[userData objectForKey:kTierTarget4] isKindOfClass:[NSNull class]]) {
            self.tierTarget4         = @"";
        } else {
            self.tierTarget4         = [userData objectForKey:kTierTarget4];
        }
        
        if([[userData objectForKey:kTierTarget5] isKindOfClass:[NSNull class]]) {
            self.tierTarget5         = @"";
        } else {
            self.tierTarget5         = [userData objectForKey:kTierTarget5];
        }
        
        if([[userData objectForKey:kTier] isKindOfClass:[NSNull class]]) {
            self.tier         = 0;
        } else {
            self.tier         = [[userData objectForKey:kTier] intValue];
        }
        
        if([[userData objectForKey:kTierPoints] isKindOfClass:[NSNull class]]) {
            self.tierPoints         = @"";
        } else {
            self.tierPoints         = [userData objectForKey:kTierPoints];
        }
        
        if([[userData objectForKey:kBahtTobeEearned] isKindOfClass:[NSNull class]]) {
            self.bahtToBeEarned         = @"";
        } else {
            self.bahtToBeEarned         = [userData objectForKey:kBahtTobeEearned];
        }
        
        if([[userData objectForKey:kPremiumValueBaht] isKindOfClass:[NSNull class]]) {
            self.premiumValueBaht         = @"";
        } else {
            self.premiumValueBaht         = [userData objectForKey:kPremiumValueBaht];
        }
        
        if([[userData objectForKey:kOtherLubricantsValueBaht] isKindOfClass:[NSNull class]]) {
            self.otherLubricantsValueBaht         = @"";
        } else {
            self.otherLubricantsValueBaht         = [userData objectForKey:kOtherLubricantsValueBaht];
        }
        
        if([[userData objectForKey:kTotalOrderValueBaht] isKindOfClass:[NSNull class]]) {
            self.totalOrderValueBaht         = @"";
        } else {
            self.totalOrderValueBaht         = [userData objectForKey:kTotalOrderValueBaht];
        }
        
        if([[userData objectForKey:kTargetStepZone] isKindOfClass:[NSNull class]]) {
            self.targetStepZone         = @"";
        } else {
            self.targetStepZone         = [userData objectForKey:kTargetStepZone];
        }
        
        if([[userData objectForKey:kTotalRewardValueBaht] isKindOfClass:[NSNull class]]) {
            self.totalRewardValueBaht         = @"";
        } else {
            self.totalRewardValueBaht         = [userData objectForKey:kTotalRewardValueBaht];
        }
        
        if([[userData objectForKey:kTotalRewardPoints] isKindOfClass:[NSNull class]]) {
            self.totalRewardPoints         = @"";
        } else {
            self.totalRewardPoints         = [userData objectForKey:kTotalRewardPoints];
        }
        
        if([[userData objectForKey:kPointsAwardedtodate] isKindOfClass:[NSNull class]]) {
            self.pointsAwardedToDate         = @"";
        } else {
            self.pointsAwardedToDate         = [userData objectForKey:kPointsAwardedtodate];
        }
        
        if([[userData objectForKey:kBalancePointstoAwardthisWeek] isKindOfClass:[NSNull class]]) {
            self.balancePointsToAwardThisWeek         = @"";
        } else {
            self.balancePointsToAwardThisWeek         = [userData objectForKey:kBalancePointstoAwardthisWeek];
        }
        
    }
    return self;
}

-(NSDictionary *) getJSON {
    return @{
            kStartdate                      : self.startDate,
            kEnddate                        : self.endDate,
            kWorkshopGroup                  : self.workshopGroup,
            kTarget1                        : self.target1,
            kTarget2                        : self.target2,
            kTarget3                        : self.target3,
            kTarget4                        : self.target4,
            kTarget5                        : self.target5,
            kWorkshopCode                   : self.workshopCode,
            kNetSales                       : self.netSales,
            kTierTarget1                    : self.tierTarget1,
            kTierTarget2                    : self.tierTarget2,
            kTierTarget3                    : self.tierTarget3,
            kTierTarget4                    : self.tierTarget4,
            kTierTarget5                    : self.tierTarget5,
            kTier                           : @(self.tier),
            kTierPoints                     : self.tierPoints,
            kBahtTobeEearned                : self.bahtToBeEarned,
            kPremiumValueBaht               : self.premiumValueBaht,
            kOtherLubricantsValueBaht       : self.otherLubricantsValueBaht,
            kTotalOrderValueBaht            : self.totalOrderValueBaht,
            kTargetStepZone                 : self.targetStepZone,
            kTotalRewardValueBaht           : self.totalRewardValueBaht,
            kTotalRewardPoints              : self.totalRewardPoints,
            kPointsAwardedtodate            : self.pointsAwardedToDate,
            kBalancePointstoAwardthisWeek   : self.balancePointsToAwardThisWeek,
    };
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init]) {
        self.startDate                      = [decoder decodeObjectForKey:kStartdate];
        self.endDate                        = [decoder decodeObjectForKey:kEnddate];
        self.workshopGroup                  = [decoder decodeObjectForKey:kWorkshopGroup];
        self.target1                        = [decoder decodeObjectForKey:kTarget1];
        self.target2                        = [decoder decodeObjectForKey:kTarget2];
        self.target3                        = [decoder decodeObjectForKey:kTarget3];
        self.target4                        = [decoder decodeObjectForKey:kTarget4];
        self.target5                        = [decoder decodeObjectForKey:kTarget5];
        self.workshopCode                   = [decoder decodeObjectForKey:kWorkshopCode];
        self.netSales                       = [decoder decodeObjectForKey:kNetSales];
        self.tierTarget1                    = [decoder decodeObjectForKey:kTierTarget1];
        self.tierTarget2                    = [decoder decodeObjectForKey:kTierTarget2];
        self.tierTarget3                    = [decoder decodeObjectForKey:kTierTarget3];
        self.tierTarget4                    = [decoder decodeObjectForKey:kTierTarget4];
        self.tierTarget5                    = [decoder decodeObjectForKey:kTierTarget5];
        self.tier                           = [[decoder decodeObjectForKey:kTier] intValue];
        self.tierPoints                     = [decoder decodeObjectForKey:kTierPoints];
        self.bahtToBeEarned                 = [decoder decodeObjectForKey:kBahtTobeEearned];
        self.premiumValueBaht               = [decoder decodeObjectForKey:kPremiumValueBaht];
        self.otherLubricantsValueBaht       = [decoder decodeObjectForKey:kOtherLubricantsValueBaht];
        self.totalOrderValueBaht            = [decoder decodeObjectForKey:kTotalOrderValueBaht];
        self.targetStepZone                 = [decoder decodeObjectForKey:kTargetStepZone];
        self.totalRewardValueBaht           = [decoder decodeObjectForKey:kTotalRewardValueBaht];
        self.totalRewardPoints              = [decoder decodeObjectForKey:kTotalRewardPoints];
        self.pointsAwardedToDate            = [decoder decodeObjectForKey:kPointsAwardedtodate];
        self.balancePointsToAwardThisWeek   = [decoder decodeObjectForKey:kBalancePointstoAwardthisWeek];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.startDate forKey:kStartdate];
    [encoder encodeObject:self.endDate forKey:kEnddate];
    [encoder encodeObject:self.workshopGroup forKey:kWorkshopGroup];
    [encoder encodeObject:self.target1 forKey:kTarget1];
    [encoder encodeObject:self.target2 forKey:kTarget2];
    [encoder encodeObject:self.target3 forKey:kTarget3];
    [encoder encodeObject:self.target4 forKey:kTarget4];
    [encoder encodeObject:self.target5 forKey:kTarget5];
    [encoder encodeObject:self.workshopCode forKey:kWorkshopCode];
    [encoder encodeObject:self.netSales forKey:kNetSales];
    [encoder encodeObject:self.tierTarget1 forKey:kTierTarget1];
    [encoder encodeObject:self.tierTarget2 forKey:kTierTarget2];
    [encoder encodeObject:self.tierTarget3 forKey:kTierTarget3];
    [encoder encodeObject:self.tierTarget4 forKey:kTierTarget4];
    [encoder encodeObject:self.tierTarget5 forKey:kTierTarget5];
    [encoder encodeObject:@(self.tier) forKey:kTier];
    [encoder encodeObject:self.tierPoints forKey:kTierPoints];
    [encoder encodeObject:self.bahtToBeEarned forKey:kBahtTobeEearned];
    [encoder encodeObject:self.premiumValueBaht forKey:kPremiumValueBaht];
    [encoder encodeObject:self.otherLubricantsValueBaht forKey:kOtherLubricantsValueBaht];
    [encoder encodeObject:self.totalOrderValueBaht forKey:kTotalOrderValueBaht];
    [encoder encodeObject:self.targetStepZone forKey:kTargetStepZone];
    [encoder encodeObject:self.totalRewardValueBaht forKey:kTotalRewardValueBaht];
    [encoder encodeObject:self.totalRewardPoints forKey:kTotalRewardPoints];
    [encoder encodeObject:self.pointsAwardedToDate forKey:kPointsAwardedtodate];
    [encoder encodeObject:self.balancePointsToAwardThisWeek forKey:kBalancePointstoAwardthisWeek];
}

@end
