//
//  ShareParticipatingProduct_V3ListModel.m
//  Shell
//
//  Created by Nach on 17/7/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "ShareParticipatingProduct_V3ListModel.h"

@implementation ShareParticipatingProduct_V3ListModel

#define kProductGroupID             @"ProductGroupID"
#define kProductGroupName           @"ProductGroupName"
#define kProductGroupImage          @"ProductGroupImage"
#define kProductGroupDetails        @"ProductGroupDetails"
#define kProductGroupDescription    @"ProductGroupDescription"
#define kProductGroupLogoID         @"ProductGroupLogoID"

-(id) init {
    self = [super init];
    if(self) {
        self.productGroupID         = @"";
        self.productGroupName       = @"";
        self.productGroupImage      = @"";
        self.productGroupDescription= @"";
        self.productGroupLogo       = @"";
        self.productGroupDetails    = [[NSMutableArray alloc] init];
    }
    return self;
}

-(id)initWithData:(NSDictionary *)userData {
    self = [super init];
    if(self) {
        if([[userData objectForKey:kProductGroupID] isKindOfClass:[NSNull class]]) {
            self.productGroupID         = @"";
        } else {
            self.productGroupID         = [userData objectForKey:kProductGroupID];
        }
        
        if([[userData objectForKey:kProductGroupImage] isKindOfClass:[NSNull class]]) {
            self.productGroupImage         = @"";
        } else {
            self.productGroupImage         = [userData objectForKey:kProductGroupImage];
        }
        
        if([[userData objectForKey:kProductGroupName] isKindOfClass:[NSNull class]]) {
            self.productGroupName         = @"";
        } else {
            self.productGroupName         = [userData objectForKey:kProductGroupName];
        }
        
        if([[userData objectForKey:kProductGroupDescription] isKindOfClass:[NSNull class] ]) {
            self.productGroupDescription         = @"";
        } else {
            self.productGroupDescription         = [userData objectForKey:kProductGroupDescription];
        }
        
        if([[userData objectForKey:kProductGroupLogoID] isKindOfClass:[NSNull class]]) {
            self.productGroupLogo         = @"";
        } else {
            self.productGroupLogo         = [userData objectForKey:kProductGroupLogoID];
        }
        
        if([[userData objectForKey:kProductGroupDetails] isKindOfClass:[NSNull class]]) {
            self.productGroupDetails = [NSMutableArray arrayWithArray:@[]];
        } else {
            NSDictionary *grpDetails  = [userData objectForKey:kProductGroupDetails];
            self.productGroupDetails    = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in grpDetails) {
                [self.productGroupDetails addObject:[[ProductGroupDetailsModel alloc] initWithData:dict]];
            }
        }
        
    }
    
    return self;
}

-(id)initForAllProductsWithData:(NSDictionary *)userData {
    self = [super init];
    if(self) {
        self.productGroupID             = @"";
        self.productGroupName           = @"";
        self.productGroupDescription    = @"";
        self.productGroupLogo           = @"";
        
        if([[userData objectForKey:kProductGroupImage] isKindOfClass:[NSNull class]]) {
            self.productGroupImage         = @"";
        } else {
            self.productGroupImage         = [userData objectForKey:kProductGroupImage];
        }
        
        if([[userData objectForKey:kProductGroupDetails] isKindOfClass:[NSNull class]]) {
            self.productGroupDetails = [NSMutableArray arrayWithArray:@[]];
        } else {
            NSDictionary *grpDetails  = [userData objectForKey:kProductGroupDetails];
            self.productGroupDetails    = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in grpDetails) {
                [self.productGroupDetails addObject:[[ProductGroupDetailsModel alloc] initForAllProductsWithData:dict]];
            }
        }
    }
    return self;
}

-(NSDictionary *) getJSON
{
    return @{
        kProductGroupID                 : self.productGroupID,
        kProductGroupName               : self.productGroupName,
        kProductGroupImage              : self.productGroupImage,
        kProductGroupDescription        : self.productGroupDescription,
        kProductGroupLogoID             : self.productGroupLogo,
        kProductGroupDetails            : self.productGroupDetails,
    };
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init]) {
        self.productGroupID             = [decoder decodeObjectForKey:kProductGroupID];
        self.productGroupName           = [decoder decodeObjectForKey:kProductGroupName];
        self.productGroupImage          = [decoder decodeObjectForKey:kProductGroupImage];
        self.productGroupDescription    = [decoder decodeObjectForKey:kProductGroupDescription];
        self.productGroupLogo           = [decoder decodeObjectForKey:kProductGroupLogoID];
        self.productGroupDetails        = [decoder decodeObjectForKey:kProductGroupDetails];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.productGroupID forKey:kProductGroupID];
    [encoder encodeObject:self.productGroupName forKey:kProductGroupName];
    [encoder encodeObject:self.productGroupImage forKey:kProductGroupImage];
    [encoder encodeObject:self.productGroupDescription forKey:kProductGroupDescription];
    [encoder encodeObject:self.productGroupLogo forKey:kProductGroupLogoID];
    [encoder encodeObject:self.productGroupDetails forKey:kProductGroupDetails];
}
@end
