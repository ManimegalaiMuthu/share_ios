//
//  ShareParticipatingProduct_V3ListModel.h
//  Shell
//
//  Created by Nach on 17/7/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductGroupDetailsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ShareParticipatingProduct_V3ListModel : NSObject

@property NSString *productGroupID;
@property NSString *productGroupName;
@property NSString *productGroupDescription;
@property NSString *productGroupImage;
@property NSString *productGroupLogo;
@property NSMutableArray *productGroupDetails;

-(id)initWithData:(NSDictionary *)userData;
-(id)initForAllProductsWithData:(NSDictionary *)userData;
-(NSDictionary *) getJSON;

@end

NS_ASSUME_NONNULL_END
