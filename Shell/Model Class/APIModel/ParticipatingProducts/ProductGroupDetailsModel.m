//
//  ProductGroupDetailsModel.m
//  Shell
//
//  Created by Nach on 17/7/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "ProductGroupDetailsModel.h"

@implementation ProductGroupDetailsModel

#define kProductName    @"ProductName"
#define kReferalPoints  @"ReferalPoints"
#define kWalkInPoints   @"WalkInPoints"
#define kPoints         @"Points"
#define kCash           @"Cash"
#define kProductGroupName   @"ProductGroupName"
#define kProdDetails        @"ProdDetails"

-(id)init {
    self = [super init];
    if(self) {
        self.productName        = @"";
        self.referalPoints      = 0;
        self.walkInPoints       = 0;
        self.points             = 0;
        self.cash               = 0;
        self.productGroupName   = @"";
        self.prodDetails        = [[NSMutableArray alloc] init];
    }
    return self;
}

-(id)initWithData:(NSDictionary *)userData {
    self = [super init];
    if(self) {
        if([[userData objectForKey:kProductName] isKindOfClass:[NSNull class]]) {
            self.productName         = @"";
        } else {
            self.productName         = [userData objectForKey:kProductName];
        }
        
        if ([[userData objectForKey: kReferalPoints] isKindOfClass: [NSNull class]])
            self.referalPoints = 0;
        else
            self.referalPoints = [[userData objectForKey: kReferalPoints] intValue];
        
        if ([[userData objectForKey: kWalkInPoints] isKindOfClass: [NSNull class]])
            self.walkInPoints = 0;
        else
            self.walkInPoints = [[userData objectForKey: kWalkInPoints] intValue];
        
        if ([[userData objectForKey: kPoints] isKindOfClass: [NSNull class]])
            self.points = 0;
        else
            self.points = [[userData objectForKey: kPoints] intValue];
        
        if ([[userData objectForKey: kCash] isKindOfClass: [NSNull class]])
            self.cash = 0;
        else
            self.cash = [[userData objectForKey: kCash] intValue];
        
        self.productGroupName = @"";
        self.prodDetails = [NSMutableArray arrayWithArray:@[]];
    }
    return self;
}

-(id) initForAllProductsWithData:(NSDictionary *)userData {
    self = [super init];
    if(self) {
        self.productName        = @"";
        self.cash               = 0;
        self.referalPoints      = 0;
        self.walkInPoints       = 0;
        if([[userData objectForKey:kProductGroupName] isKindOfClass:[NSNull class]]) {
            self.productGroupName         = @"";
        } else {
            self.productGroupName         = [userData objectForKey:kProductGroupName];
        }
        
        if([[userData objectForKey:kProdDetails] isKindOfClass:[NSNull class]]) {
            self.prodDetails = [NSMutableArray arrayWithArray:@[]];
        } else {
            NSDictionary *grpDetails  = [userData objectForKey:kProdDetails];
            self.prodDetails    = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in grpDetails) {
                if(![[dict objectForKey:kProductName] isKindOfClass:[NSNull class]]) {
                    [self.prodDetails addObject:@{ kProductName : [dict objectForKey:kProductName]}];
                }
            }
        }
        
    }
    return self;
}

-(NSDictionary *) getJSON
{
    return @{
        kProductName            : self.productName,
        kReferalPoints          : @(self.referalPoints),
        kWalkInPoints           : @(self.walkInPoints),
        kPoints                 : @(self.points),
        kCash                   : @(self.cash),
        kProductGroupName       : self.productGroupName,
        kProdDetails            : self.prodDetails,
    };
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init]) {
        self.productName        = [decoder decodeObjectForKey:kProductName];
        self.referalPoints      = [decoder decodeIntForKey:kReferalPoints];
        self.walkInPoints       = [decoder decodeIntForKey:kWalkInPoints];
        self.points             = [decoder decodeIntForKey:kPoints];
        self.cash               = [decoder decodeIntForKey:kCash];
        self.productGroupName   = [decoder decodeObjectForKey:kProductGroupName];
        self.prodDetails        = [decoder decodeObjectForKey:kProdDetails];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.productName forKey: kProductName];
    [encoder encodeInteger:self.referalPoints forKey:kReferalPoints];
    [encoder encodeInteger:self.walkInPoints forKey:kWalkInPoints];
    [encoder encodeInteger:self.points forKey:kPoints];
    [encoder encodeInteger:self.cash forKey:kCash];
    [encoder encodeObject:self.productGroupName forKey:kProductGroupName];
    [encoder encodeObject:self.prodDetails  forKey:kProdDetails];
}
@end
