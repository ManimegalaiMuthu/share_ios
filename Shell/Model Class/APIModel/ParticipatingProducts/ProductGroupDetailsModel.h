//
//  ProductGroupDetailsModel.h
//  Shell
//
//  Created by Nach on 17/7/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProductGroupDetailsModel : NSObject

@property NSString *productName;
@property NSInteger referalPoints;
@property NSInteger walkInPoints;
@property NSInteger points;
@property NSInteger cash;
//For all Products - V3
@property NSString *productGroupName;
@property NSMutableArray *prodDetails;

-(id)initWithData:(NSDictionary *)userData;
-(id)initForAllProductsWithData:(NSDictionary *)userData;
-(NSDictionary *) getJSON;

@end

NS_ASSUME_NONNULL_END
