//
//  Qrcode.m
//  Syngenta
//
//  Created by Will Choy on 1/12/15.
//  Copyright © 2015 WillChoy. All rights reserved.
//

#import "Qrcode.h"
#import "Constants.h"

@implementation Qrcode

- (id)initWithResults:(FMResultSet *)results
{
    self = [super init];
    
    if(self)
    {
        self.codeID = [results stringForColumn:@"code"];
        self.memberID = [results stringForColumn:@"memberID"];
    }
    return self;
}

- (id)initWithCode:(NSString *)code {
    self = [super init];
    if(self) {
        // formattting here
        self.codeID = code;
        self.memberID = MEMBER_ID;
    }
    return self;
}
@end
