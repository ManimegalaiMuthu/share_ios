//
//  Qrcode.h
//  Syngenta
//
//  Created by Will Choy on 1/12/15.
//  Copyright © 2015 WillChoy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"

@interface Qrcode : NSObject

@property (strong,nonatomic) NSString *memberID;
@property (strong,nonatomic) NSString *codeID;

- (id)initWithResults:(FMResultSet *)results;
- (id)initWithCode:(NSString *)code;
@end

