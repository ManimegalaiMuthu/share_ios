//
//  Member.m
//  Shell
//
//  Created by Ankita Chhikara on 26/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "Member.h"

@implementation Member
#define ISNULLCLASS(obj) [obj isKindOfClass: [NSNull class]]

#define kUserID             @"UserID"
#define kDSRCode            @"DSRCode"
#define kUserPassword       @"UserPassword"
#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kEmailAddress       @"EmailAddress"
#define kGender             @"Gender"
#define kContactPreference  @"ContactPreferences"
#define kRegistrationType   @"RegistrationType"
#define kRegistrationMode   @"RegistrationMode"
#define kRegistrationRole   @"RegistrationRole"
#define kDOBMonth           @"DOBMonth"
#define kDOBDay             @"DOBDay"
#define kCountryCode        @"CountryCode"
#define kLeadID             @"LeadID"
#define kTradeID            @"TradeID"
#define kStaffType          @"StaffType"
#define kFAShareCode        @"FAShareCode"


-(id)init
{
    self = [super init];
    if (self)
    {
        self.userID             = @"";
        self.DSRCode            = @"";
        self.userPassword       = @"";
        
        self.salutation         = 0;
        self.firstName          = @"";
        self.lastName           = @"";
        
        self.countryCode        = @"";
        self.mobileNumber       = @"";
        self.emailAddress       = @"";
        
        self.gender             = @"";
        self.contactPreference  = @"";
        
        self.registrationType   = @"";
        self.registrationMode   = @"";
        self.registrationRole   = @"";
        
        self.dobMonth           = @"";
        self.dobDay             = @"";
        
        self.tradeID   = @"";
        self.leadID    = @"";
        
        self.staffType = 0;
        self.faShareCode = @"";
    }
    return self;
}

-(id)initWithData:(NSDictionary *)userData
{
    self = [super init];
    if (self) {
        
        self.userID             = [userData objectForKey: kUserID];
        self.DSRCode            = [userData objectForKey: kDSRCode];
        self.userPassword       = [userData objectForKey: kUserPassword];
        
        self.salutation         = [[userData objectForKey: kSalutation] intValue];
        self.firstName          = [userData objectForKey: kFirstName];
        self.lastName           = [userData objectForKey: kLastName];
        
        self.countryCode        = [[userData objectForKey: kCountryCode] stringValue];
        self.mobileNumber       = [userData objectForKey: kMobileNumber];
        self.emailAddress       = [userData objectForKey: kEmailAddress];
        
        self.gender             = [userData objectForKey: kGender];
        self.contactPreference  = [userData objectForKey: kContactPreference];
        
        self.registrationType   = [userData objectForKey: kRegistrationType];
        self.registrationMode   = [userData objectForKey: kRegistrationMode];
//        self.registrationRole   = [userData objectForKey: kRegistrationRole];
        
        self.dobMonth           = [userData objectForKey: kDOBMonth];
        self.dobDay             = [userData objectForKey: kDOBDay];
        
        self.leadID             = [userData objectForKey: kLeadID];
        self.tradeID            = [userData objectForKey: kTradeID];
        
        self.staffType          = [[userData objectForKey:kStaffType] intValue];
        self.faShareCode        = [userData objectForKey:kFAShareCode];
        
        [self nullHandler];
    }
    
    return self;
}

-(NSDictionary *) getJSON
{
    [self nullHandler];
    
    return @{
        kUserID:            self.userID,
        kDSRCode:           self.DSRCode,
        kUserPassword:      self.userPassword,
        kSalutation:        @(self.salutation),
        kCountryCode:       self.countryCode,
        kFirstName:         self.firstName,
        kLastName:          self.lastName,
        kMobileNumber:      self.mobileNumber,
        kEmailAddress:      self.emailAddress,
        kGender:            self.gender,
        kContactPreference: self.contactPreference,
        kRegistrationType:  self.registrationType,
        kRegistrationMode:  self.registrationMode,
        kRegistrationRole:  self.registrationRole,
        kDOBMonth:          self.dobMonth,
        kDOBDay:            self.dobDay,
        
        kLeadID:            self.leadID,
        kTradeID:           self.tradeID,
        
        kStaffType:         @(self.staffType),
        kFAShareCode:       self.faShareCode ? self.faShareCode : @"",
    };
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init]) {
        self.userID             = [decoder decodeObjectForKey: kUserID];
        self.DSRCode            = [decoder decodeObjectForKey: kDSRCode];
        self.userPassword       = [decoder decodeObjectForKey: kUserPassword];
        
        self.salutation         = [[decoder decodeObjectForKey: kSalutation] intValue];
        self.firstName          = [decoder decodeObjectForKey: kFirstName];
        self.lastName           = [decoder decodeObjectForKey: kLastName];
        
        self.countryCode        = [[decoder decodeObjectForKey: kCountryCode] stringValue];
        self.mobileNumber       = [decoder decodeObjectForKey: kMobileNumber];
        self.emailAddress       = [decoder decodeObjectForKey: kEmailAddress];
        
        self.gender             = [decoder decodeObjectForKey: kGender];
        self.contactPreference  = [decoder decodeObjectForKey: kContactPreference];
        
        self.registrationType   = [decoder decodeObjectForKey: kRegistrationType];
        self.registrationMode   = [decoder decodeObjectForKey: kRegistrationMode];
        self.registrationRole   = [decoder decodeObjectForKey: kRegistrationRole];
        
        self.dobMonth           = [decoder decodeObjectForKey: kDOBMonth];
        self.dobDay             = [decoder decodeObjectForKey: kDOBDay];
        
        self.leadID             = [decoder decodeObjectForKey: kLeadID];
        self.tradeID            = [decoder decodeObjectForKey: kTradeID];
        
        self.staffType          = [[decoder decodeObjectForKey:kStaffType] intValue];
        self.faShareCode        = [decoder decodeObjectForKey:kFAShareCode];
        
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject: self.userID              forKey: kUserID];
    [encoder encodeObject: self.DSRCode             forKey: kDSRCode];
    [encoder encodeObject: self.userPassword        forKey: kUserPassword];
    
    [encoder encodeObject: @(self.salutation)          forKey: kSalutation];
    [encoder encodeObject: self.firstName           forKey: kFirstName];
    [encoder encodeObject: self.lastName            forKey: kLastName];
    
    [encoder encodeObject: self.countryCode         forKey: kCountryCode];
    [encoder encodeObject: self.mobileNumber        forKey: kMobileNumber];
    [encoder encodeObject: self.emailAddress        forKey: kEmailAddress];
    
    [encoder encodeObject: self.gender              forKey: kGender];
    [encoder encodeObject: self.contactPreference   forKey: kContactPreference];
    
    [encoder encodeObject: self.registrationType    forKey: kRegistrationType];
    [encoder encodeObject: self.registrationMode    forKey: kRegistrationMode];
    [encoder encodeObject: self.registrationRole    forKey: kRegistrationRole];
    
    [encoder encodeObject: self.dobMonth            forKey: kDOBMonth];
    [encoder encodeObject: self.dobDay              forKey: kDOBDay];    
    [encoder encodeObject: self.leadID              forKey: kLeadID];
    [encoder encodeObject: self.tradeID             forKey: kTradeID];
    
    [encoder encodeObject: @(self.staffType)        forKey: kStaffType];
    [encoder encodeObject:self.faShareCode          forKey:kFAShareCode];
}

-(void) nullHandler
{
    if (ISNULLCLASS(self.userID))
        self.userID = @"";
    
    if (ISNULLCLASS(self.DSRCode))
        self.DSRCode = @"";
    
    if (ISNULLCLASS(self.userPassword))
        self.userPassword = @"";
    
    if (ISNULLCLASS(self.userPassword))
        self.userPassword = @"";
    
    if (ISNULLCLASS(@(self.salutation)))
        self.salutation = 0;
    
    if (ISNULLCLASS(self.firstName))
        self.firstName = @"";
    if (ISNULLCLASS(self.lastName))
        self.lastName = @"";
    
    if (ISNULLCLASS(self.countryCode))
        self.countryCode = @"";
    if (ISNULLCLASS(self.mobileNumber))
        self.mobileNumber = @"";
    if (ISNULLCLASS(self.emailAddress))
        self.emailAddress = @"";
    
    if (ISNULLCLASS(self.gender))
        self.gender = @"";
    
    if (ISNULLCLASS(self.contactPreference))
        self.contactPreference = @"";
    
    if (ISNULLCLASS(self.registrationType))
        self.registrationType = @"";
    
    if (ISNULLCLASS(self.registrationMode))
        self.registrationMode = @"";
    
    if (ISNULLCLASS(self.registrationRole) || !self.registrationRole)
        self.registrationRole = @"";
    
    if ([self.dobDay isKindOfClass: [NSNull class]])
        self.dobDay           = @"";
    if ([self.dobMonth isKindOfClass: [NSNull class]])
        self.dobMonth           = @"";
    
    if ([self.tradeID isKindOfClass: [NSNull class]])
        self.tradeID   = @"";
    if ([self.leadID isKindOfClass: [NSNull class]])
        self.leadID    = @"";
    
    if (ISNULLCLASS(@(self.salutation)))
        self.staffType    = 0;
    
    if (ISNULLCLASS(self.faShareCode))
        self.faShareCode = @"";
}

@end
