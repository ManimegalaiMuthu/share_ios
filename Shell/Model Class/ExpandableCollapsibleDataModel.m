//
//  ExpandableCollapsibleDataModel.m
//  Shell
//
//  Created by Nach on 9/3/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "ExpandableCollapsibleDataModel.h"

@implementation ExpandableCollapsibleDataModel


-(instancetype) initWithData:(NSDictionary *) data {
        
    self = [super init];
    
    if(self) {
        if([data objectForKey: kExpandable_TableDataArray])
            self.tableDataArray = [data objectForKey: kExpandable_TableDataArray];
        // removed Sept 2020
       // if([data objectForKey: kExpandable_TableHeadingsArray])
          //  self.tableHeadingsArray = [data objectForKey: kExpandable_TableHeadingsArray];
    }
    
    return self;
}


@end
