//
//  ExpandableCollapsibleDataModel.h
//  Shell
//
//  Created by Nach on 9/3/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExpandableCollapsibleDataModel : NSObject

@property NSMutableArray *tableDataArray;
// removed Sept 2020
//@property NSMutableArray *tableHeadingsArray;

-(instancetype) initWithData:(NSDictionary *) data;

@end

NS_ASSUME_NONNULL_END
