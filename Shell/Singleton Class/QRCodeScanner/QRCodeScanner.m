//
//  QRCodeScanner.m
//  BorneoMotorsClaim
//
//  Created by Shekhar  on 27/4/15.
//  Copyright (c) 2015 Edenred. All rights reserved.
//

#import "QRCodeScanner.h"
#import "UIImageView+AFNetworking.h"
#import "Session.h"
#import "AlertViewManager.h"
#import "LocalizationManager.h"
#import <AVFoundation/AVFoundation.h>
#import "QRCodeReaderViewController.h"

//#import "ScannerViewController.h"

static QRCodeScanner *instance;

@interface QRCodeScanner ()<UIImagePickerControllerDelegate,QRCodeReaderDelegate>//,ScannerViewControllerDelegate>
//@interface QRCodeScanner ()<UIImagePickerControllerDelegate,ZBarReaderDelegate>

@property (nonatomic,strong) UIViewController *vc;
@property (nonatomic,strong) UIImage *image;

#define DELAY_SECONDS 1
@property BOOL delayLock;
@end

@implementation QRCodeScanner

+ (QRCodeScanner*) getInstance {
    @synchronized([QRCodeScanner class]) {
        if ( instance == nil ) {
            instance = [[QRCodeScanner alloc] init];
        }
    }
    return instance;
}

//- (void)showQRCodeScannerOnVC:(UIViewController *)vc
//{
//    self.vc = [[UIViewController alloc]init];
//    self.vc = vc;
//    static QRCodeReaderViewController *reader = nil;
//    static dispatch_once_t onceToken;
//    
//    dispatch_once(&onceToken, ^{
//        reader                        = [QRCodeReaderViewController new];
//        reader.modalPresentationStyle = UIModalPresentationFormSheet;
//    });
//    reader.delegate = self;
//    
//    [reader setCompletionWithBlock:^(NSString *resultAsString) {
//        NSLog(@"Completion with result: %@", resultAsString);
//    }];
//    
//    [vc presentViewController:reader animated:YES completion:NULL];
//
//}
//
//- (void)showBarCodeScannerOnVC:(UIViewController *)vc
//{
//    self.vc = [[UIViewController alloc]init];
//    self.vc = vc;
//    static ScannerViewController *reader = nil;
//    static dispatch_once_t onceToken;
//    
//    dispatch_once(&onceToken, ^{
//        reader                        = [ScannerViewController new];
//        reader.modalPresentationStyle = UIModalPresentationFormSheet;
//    });
//    reader.delegate = self;
//    
////    [reader setCompletionWithBlock:^(NSString *resultAsString) {
////        NSLog(@"Completion with result: %@", resultAsString);
////    }];
//    
//    [vc presentViewController:reader animated:YES completion:NULL];
//    
//}

- (void)showQRCodeScannerOnVC:(UIViewController *)vc withMessage:(NSString*)message
{
    switch ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo])
    {
        case AVAuthorizationStatusAuthorized:
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.vc = [[UIViewController alloc]init];
                self.vc = vc;
                static QRCodeReaderViewController *reader = nil;
                static dispatch_once_t onceToken;
                
                dispatch_once(&onceToken, ^{
                    reader                        = [QRCodeReaderViewController new];
                    reader.modalPresentationStyle = UIModalPresentationFullScreen;
                });
                reader.delegate = self;
                [reader setScanCountHidden: YES];
                [reader setScanSubtitleMessage:message];
                
                [reader setCompletionWithBlock:^(NSString *resultAsString) {
                    [reader dismissViewControllerAnimated:YES completion:^{
                        [self.delegate performSelectorOnMainThread:@selector(qrCodeScanSuccess:) withObject:resultAsString waitUntilDone:YES];
                    }];
                }];
                
                [vc presentViewController:reader animated:YES completion:NULL];
            });
        }
            break;
        default:
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
             {
                 if(granted)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         self.vc = [[UIViewController alloc]init];
                         self.vc = vc;
                         static QRCodeReaderViewController *reader = nil;
                         static dispatch_once_t onceToken;
                         
                         dispatch_once(&onceToken, ^{
                             reader                        = [QRCodeReaderViewController new];
                             reader.modalPresentationStyle = UIModalPresentationFullScreen;
                         });
                         reader.delegate = self;
                         [reader setScanCountHidden: YES];
                         [reader setScanSubtitleMessage:message];
                         
                         [reader setCompletionWithBlock:^(NSString *resultAsString) {
                             [reader dismissViewControllerAnimated:YES completion:^{
                                 [self.delegate performSelectorOnMainThread:@selector(qrCodeScanSuccess:) withObject:resultAsString waitUntilDone:YES];
                             }];
                         }];
                         
                         [vc presentViewController:reader animated:YES completion:NULL];
                     });
                 }
                 else
                 {
                     if (vc)
                     {
                         [self permissionDenied: vc];
                     }
                 }
             }];
            break;
    }
}

- (void)showZBarScannerOnVC:(UIViewController *)vc
{
    [vc.view endEditing:true];
    switch ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo])
    {
        case AVAuthorizationStatusAuthorized:
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.vc = [[UIViewController alloc]init];
                self.vc = vc;
                static QRCodeReaderViewController *reader = nil;
                static dispatch_once_t onceToken;
                
                dispatch_once(&onceToken, ^{
                    reader                        = [QRCodeReaderViewController new];
                    reader.modalPresentationStyle = UIModalPresentationFullScreen;
                });
                reader.delegate = self;
                [reader setScanCountHidden: YES];
                [reader setScanSubtitleHidden: YES];
                
                [reader setCompletionWithBlock:^(NSString *resultAsString) {
                    [reader dismissViewControllerAnimated:YES completion:^{
                        [self.delegate performSelectorOnMainThread:@selector(qrCodeScanSuccess:) withObject:resultAsString waitUntilDone:YES];
                    }];
                }];
                
                [vc presentViewController:reader animated:YES completion:NULL];
            });
        }
            break;
        default:
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
             {
                 if(granted)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         self.vc = [[UIViewController alloc]init];
                         self.vc = vc;
                         static QRCodeReaderViewController *reader = nil;
                         static dispatch_once_t onceToken;
                         
                         dispatch_once(&onceToken, ^{
                             reader                        = [QRCodeReaderViewController new];
                             reader.modalPresentationStyle = UIModalPresentationFullScreen;
                         });
                         reader.delegate = self;
                         [reader setScanCountHidden: YES];
                         [reader setScanSubtitleHidden: YES];
                         
                         [reader setCompletionWithBlock:^(NSString *resultAsString) {
                             [reader dismissViewControllerAnimated:YES completion:^{
                                 [self.delegate performSelectorOnMainThread:@selector(qrCodeScanSuccess:) withObject:resultAsString waitUntilDone:YES];
                             }];
                         }];
                         
                         [vc presentViewController:reader animated:YES completion:NULL];
                     });
                 }
                 else
                 {
                     if (vc)
                         [self permissionDenied: vc];
                 }
             }];
            break;
    }
}

-(void) permissionDenied: (UIViewController *)vc
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_ALERTVIEW_ERROR) message:LOCALIZATION(C_ALERTVIEW_PERMISSIONERRORMSG) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_CANCEL) style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_SETTING) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                         {
                                             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                                                         UIApplicationOpenSettingsURLString]];
                                         }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:settingsAction];
        
        [vc presentViewController:alertController animated:YES completion:nil];
    });
}



- (void)showDecalScanner:(UIViewController *)vc startCount:(NSInteger) startCount
{
    [vc.view endEditing:true];
    switch ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo])
    {
        case AVAuthorizationStatusAuthorized:
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.vc = [[UIViewController alloc]init];
                self.vc = vc;
                static QRCodeReaderViewController *reader = nil;
                static dispatch_once_t onceToken;
                
                dispatch_once(&onceToken, ^{
                    reader                        = [QRCodeReaderViewController new];
                    reader.modalPresentationStyle = UIModalPresentationFullScreen;
                });
                reader.delegate = self;
                reader.showCount = YES;
                reader.scanCount = startCount;
                [reader setScanCountHidden: NO];
                [reader setScanSubtitleHidden: YES];
                
                [reader setCompletionWithBlock:^(NSString *resultAsString) {
                    if (!self.delayLock)
                        [self.delegate qrCodeScanSuccess:resultAsString];
                    else
                        [self performSelectorInBackground: @selector(unlockDelay) withObject:nil];
    //                [reader dismissViewControllerAnimated:YES completion:^{
    //                    [self.delegate qrCodeScanSuccess:resultAsString];
    //                }];
                }];
                
                [vc presentViewController:reader animated:YES completion:NULL];
            });
        }
            break;
        default:
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
             {
                 if(granted)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                          self.vc = [[UIViewController alloc]init];
                          self.vc = vc;
                          static QRCodeReaderViewController *reader = nil;
                          static dispatch_once_t onceToken;
                          
                          dispatch_once(&onceToken, ^{
                              reader                        = [QRCodeReaderViewController new];
                              reader.modalPresentationStyle = UIModalPresentationFullScreen;
                          });
                          reader.delegate = self;
                          reader.showCount = YES;
                          reader.scanCount = startCount;
                          [reader setScanCountHidden: NO];
                          [reader setScanSubtitleHidden: YES];
                          
                          
                          [reader setCompletionWithBlock:^(NSString *resultAsString) {
                              //decal vc will handle error popup
                              if (!self.delayLock)
                                  [self.delegate performSelectorOnMainThread:@selector(qrCodeScanSuccess:) withObject:resultAsString waitUntilDone:YES];
                              else
                                  [self performSelectorInBackground: @selector(unlockDelay) withObject:nil];
     //                         [reader dismissViewControllerAnimated:YES completion:^{
     //                             [self.delegate qrCodeScanSuccess:resultAsString];
     //                         }];
                          }];
                          
                          [vc presentViewController:reader animated:YES completion:NULL];
                     });
                 }
                 else
                 {
                     if (vc)
                         [self permissionDenied: vc];
                 }
             }];
            break;
    }
}


- (void)showSingleDecalScanner:(UIViewController *)vc
{
    [vc.view endEditing:true];
    switch ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo])
    {
        case AVAuthorizationStatusAuthorized:
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.vc = [[UIViewController alloc]init];
                self.vc = vc;
                static QRCodeReaderViewController *reader = nil;
                static dispatch_once_t onceToken;
                
                dispatch_once(&onceToken, ^{
                    reader                        = [QRCodeReaderViewController new];
                    reader.modalPresentationStyle = UIModalPresentationFullScreen;
                });
                reader.delegate = self;
                [reader setScanCountHidden: YES];
                [reader setScanSubtitleHidden: NO];
                
                [reader setCompletionWithBlock:^(NSString *resultAsString) {
                    [reader dismissViewControllerAnimated:YES completion:^{
                        [self.delegate performSelectorOnMainThread:@selector(qrCodeScanSuccess:) withObject:resultAsString waitUntilDone:YES];
                    }];
                }];
                
                [vc presentViewController:reader animated:YES completion:NULL];
            });
        }
            break;
        default:
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
             {
                 if(granted)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         self.vc = [[UIViewController alloc]init];
                         self.vc = vc;
                         static QRCodeReaderViewController *reader = nil;
                         static dispatch_once_t onceToken;
                         
                         dispatch_once(&onceToken, ^{
                             reader                        = [QRCodeReaderViewController new];
                             reader.modalPresentationStyle = UIModalPresentationFullScreen;
                         });
                         reader.delegate = self;
                         [reader setScanCountHidden: YES];
                         [reader setScanSubtitleHidden: NO];
                         
                         
                         [reader setCompletionWithBlock:^(NSString *resultAsString) {
                             [reader dismissViewControllerAnimated:YES completion:^{
                                 [self.delegate performSelectorOnMainThread:@selector(qrCodeScanSuccess:) withObject:resultAsString waitUntilDone:YES];
                             }];
                         }];
                         
                         [vc presentViewController:reader animated:YES completion:NULL];
                     });
                 }
                 else
                 {
                     if (vc)
                         [self permissionDenied: vc];
                 }
             }];
            break;
    }
}


-(void)readerDidCancel:(QRCodeReaderViewController *)reader
{
    if ([self.delegate respondsToSelector:@selector(qrCodeScanCancelled)])
    {
        [self.delegate qrCodeScanCancelled];
    }
}

-(void) unlockDelay
{
    self.delayLock = NO;
}

-(void)resetToolbar:(ZBarReaderViewController*)codeReader {
    for (UIView *codeReaderSubViews in codeReader.view.subviews)
    {
        if ([codeReaderSubViews isKindOfClass:[UIView class]])
        {
            for (UIView *subView in codeReaderSubViews.subviews)
            {
                if ([subView isKindOfClass:[UIToolbar class]])
                {
                    for (UIView *tempView in subView.subviews)
                    {
                        if ([tempView isKindOfClass:[UIButton class]])
                        {
                            //Info button
                            [tempView setHidden:YES];
                        } else
                        {
                            //cancel button
                            UIButton *cancelButton = (UIButton *)tempView;
                            [cancelButton setTintColor:[UIColor whiteColor]];
                        }
                    }
                }
            }
        }
    }
}

//#pragma mark - QRCodeReader Delegate Methods
//
//- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result
//{
//    [self.vc dismissViewControllerAnimated:YES completion:^{
//        [self.delegate qrCodeScanSuccess:result];
//    }];
//}
//
//- (void)readerDidCancel:(QRCodeReaderViewController *)reader
//{
//    [self.vc dismissViewControllerAnimated:YES completion:NULL];
//    if ([self.delegate respondsToSelector:@selector(qrCodeScanCancelled)]) {
//        [self.delegate qrCodeScanCancelled];
//    }
//}
//
//- (void)barCodeScanSuccess:(NSString *)scanString;
//{
//    [self.vc dismissViewControllerAnimated:YES completion:^{
//        [self.delegate qrCodeScanSuccess:scanString];
//    }];
//    
//}
//- (void)barCodeScanFailed
//{
//    
//}
//- (void)barCodeScanCancelled
//{
//    [self.vc dismissViewControllerAnimated:YES completion:NULL];
//    if ([self.delegate respondsToSelector:@selector(qrCodeScanCancelled)]) {
//        [self.delegate qrCodeScanCancelled];
//    }
//}

#pragma mark - ZBar's Delegate method

- (void)imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    //  get the decode results
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // just grab the first barcode
        break;
    
    [self.vc dismissViewControllerAnimated:YES completion:^{
        [self.delegate qrCodeScanSuccess:symbol.data];
    }];
    
    // dismiss the controller
    [reader dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self.vc dismissViewControllerAnimated:YES completion:NULL];
    if ([self.delegate respondsToSelector:@selector(qrCodeScanCancelled)]) {
        [self.delegate qrCodeScanCancelled];
    }
}

@end
