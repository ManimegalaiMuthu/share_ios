//
//  QRCodeScanner.h
//  BorneoMotorsClaim
//
//  Created by Shekhar  on 27/4/15.
//  Copyright (c) 2015 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ZBarSDK.h"

#define mQRCodeScanner	((QRCodeScanner *) [QRCodeScanner getInstance])


@protocol QRCodeScannerDelegate <NSObject>
@optional
- (void)qrCodeScanSuccess:(NSString *)scanString;
- (void)qrCodeScanFailed;
- (void)qrCodeScanCancelled;
- (void)barCodeScanSuccess:(NSString *)scanString;
- (void)barCodeScanFailed;
- (void)barCodeScanCancelled;

@end


@interface QRCodeScanner : NSObject

@property (nonatomic,weak) NSObject <QRCodeScannerDelegate>  *delegate;

+ (QRCodeScanner*) getInstance;
+ (QRCodeScanner*) getBarCodeInstance;
+ (QRCodeScanner*) getIntegratedInstance;

- (void)showQRCodeScannerOnVC:(UIViewController *)vc;
- (void)showQRCodeScannerOnVC:(UIViewController *)vc withMessage:(NSString*)message;
- (void)showBarCodeScannerOnVC:(UIViewController *)vc;
- (void)showZBarScannerOnVC:(UIViewController *)vc;
- (void)showSingleDecalScanner:(UIViewController *)vc;
- (void)showDecalScanner:(UIViewController *)vc startCount:(NSInteger) startCount;
@end
