//
//  LocalizationManager.m

//
//  Created by Shekhar on 26/12/2014.
//

#import <Lokalise/Lokalise.h>
#import "LocalizationManager.h"
#import "Constants.h"

@interface LocalizationManager()

@property NSDictionary * dicoLocalisation;
@property NSUserDefaults * defaults;

@end

@implementation LocalizationManager


#pragma  mark - Singleton Method

+ (LocalizationManager*)sharedInstance
{
    static LocalizationManager *_sharedInstance = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[LocalizationManager alloc] init];
    });
    return _sharedInstance;
}


#pragma mark - Init methods

- (id)init
{
    self = [super init];
    if (self)
    {
        _defaults                       = [NSUserDefaults standardUserDefaults];
        _availableLanguagesArray        = kAvailableLanguages;
        _dicoLocalisation               = nil;
        
        _currentLanguage                = kDeviceLanguage;
        
    
        NSString *languageSaved = [_defaults objectForKey:kSaveLanguageDefaultKey];
        
        if (languageSaved != nil && ![languageSaved isEqualToString:kDeviceLanguage])
        {
            [self loadDictionaryForLanguage:languageSaved];
        }
    }
    return self;
}


#pragma mark - saveInIUserDefaults custom accesser/setter

-(BOOL)saveInUserDefaults
{
    return ([self.defaults objectForKey:kSaveLanguageDefaultKey] != nil);
}

-(void)setSaveInUserDefaults:(BOOL)saveInUserDefaults
{
    if (saveInUserDefaults)
    {
        [self.defaults setObject:_currentLanguage forKey:kSaveLanguageDefaultKey];
    }
    else
    {
        [self.defaults removeObjectForKey:kSaveLanguageDefaultKey];
    }
    [self.defaults synchronize];
}

- (void)setLocalizationLanguage:(NSString *)language
{
    [_defaults setObject:kEnglish forKey:kSaveLanguageDefaultKey];
}

#pragma mark - Private  Instance methods

-(BOOL)loadDictionaryForLanguage:(NSString *)newLanguage
{
    NSURL * urlPath = [[NSBundle bundleForClass:[self class]] URLForResource:@"Localizable" withExtension:@"strings" subdirectory:nil localization:newLanguage];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:urlPath.path])
    {
        
        self.currentLanguage = [newLanguage copy];
        self.dicoLocalisation = [[NSDictionary dictionaryWithContentsOfFile:urlPath.path] copy];
        
        return YES;
    }
    return NO;
}


#pragma mark - Public Instance methods

-(NSString *)localizedStringForKey:(NSString*)key
{
    //NSLog(@"%@",self.dicoLocalisation);
    //NSLog(@"%lu",(unsigned long)[self.dicoLocalisation count]);
    
    NSString *finalValue;
    
    if (self.dicoLocalisation == nil)
    {
//        return NSLocalizedString(key, key);
        finalValue = NSLocalizedString(key, key);
    }
    else
    {
//        NSString * localizedString = self.dicoLocalisation[key];
//        if (localizedString == nil)
//            localizedString = key;
//        return localizedString;
        
        
//        return [[Lokalise sharedObject] localizedStringForKey: key value: nil table: nil];
        finalValue = [[Lokalise sharedObject] localizedStringForKey: key value: nil table: nil];
        
        //TEST MORE 
        if ([Lokalise sharedObject].localizationType == LokaliseLocalizationDebug)
            finalValue = [@"debug_" stringByAppendingString: finalValue];
        else if ([Lokalise sharedObject].localizationType == LokaliseLocalizationLocal)
            finalValue = [@"local_" stringByAppendingString: finalValue];
    }
    
#pragma mark do all the reference replacements here"
    finalValue = [finalValue stringByReplacingOccurrencesOfString: @"&amp;" withString: @"&"];
    finalValue = [finalValue stringByReplacingOccurrencesOfString: @"&#160;" withString: @" "];
    finalValue = [finalValue stringByReplacingOccurrencesOfString: @"<u>" withString: @""]; //remove underline tag
    finalValue = [finalValue stringByReplacingOccurrencesOfString: @"</u>" withString: @""]; //remove underline tag
    finalValue = [finalValue stringByReplacingOccurrencesOfString: @"<b>" withString: @""]; //remove bold tag
    finalValue = [finalValue stringByReplacingOccurrencesOfString: @"</b>" withString: @""]; //remove bold tag
    return finalValue;
}

-(NSString *)localStringForKey:(NSString*) key languageCode:(NSString*) languageCode
{
    NSString * path = [[NSBundle mainBundle] pathForResource: languageCode ofType:@"lproj"];
    NSBundle * bundle = nil;
    if(path == nil){
        bundle = [NSBundle mainBundle];
    }else{
        bundle = [NSBundle bundleWithPath:path];
    }
    return [bundle localizedStringForKey: key value:nil table:nil];
}

-(BOOL)setLanguage:(NSString *)newLanguage
{
    NSLocale *locale;
    if ([newLanguage isEqualToString: kChineseT_HK] ||
        [newLanguage isEqualToString: kEnglishIndia] ||
        [newLanguage isEqualToString: kBengali] ||
        [newLanguage isEqualToString: kHindi] ||
        [newLanguage isEqualToString: kMalayalam] ||
        [newLanguage isEqualToString: kKannada] ||
        [newLanguage isEqualToString: kTelugu] ||
        [newLanguage isEqualToString: kTamil] ||
        [newLanguage isEqualToString: kEnglishIndo])
    {
        locale = [[NSLocale alloc] initWithLocaleIdentifier: [newLanguage stringByReplacingOccurrencesOfString: @"-" withString: @"_"]];
    }
    else
        locale = [[NSLocale alloc] initWithLocaleIdentifier: newLanguage];
    
    [[Lokalise sharedObject] setLocalizationLocale:locale makeDefault:YES completion:^(NSError * _Nullable error) {
//        if (IS_STAGING_VERSION && [error isKindOfClass: [NSNull class]])
//            NSLog(@"lokalise error %@ ------ %@", newLanguage, error);
    }];
    
    {
        NSString * path = [[NSBundle mainBundle] pathForResource:@"pt-PT" ofType:@"lproj"];
        NSBundle * bundle = nil;
        if(path == nil){
            bundle = [NSBundle mainBundle];
        }else{
            bundle = [NSBundle bundleWithPath:path];
        }
        NSString * str = [bundle localizedStringForKey:@"a string" value:@"comment" table:nil];
    }
    NSLog(@"%@", [locale localizedStringForLanguageCode: newLanguage]);
    
    if (newLanguage == nil || [newLanguage isEqualToString:self.currentLanguage] || ![self.availableLanguagesArray containsObject:newLanguage])
        return NO;
    
    if ([newLanguage isEqualToString:kDeviceLanguage])
    {
        self.currentLanguage = [newLanguage copy];
        self.dicoLocalisation = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLanguageChanged
                                                            object:nil];
        return YES;
    }
    else
    {
        BOOL isLoadingOk = [self loadDictionaryForLanguage:newLanguage];
        
        if (isLoadingOk)
        {
            if ([self saveInUserDefaults])
            {
                [self.defaults setObject:_currentLanguage forKey:kSaveLanguageDefaultKey];
                [self.defaults synchronize];
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLanguageChanged
                                                                object:nil];
        }
        return isLoadingOk;
    }
}


@end
