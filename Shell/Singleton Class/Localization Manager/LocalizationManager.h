//
//  LocalizationManager.h

//
//  Created by Shekhar on 26/12/2014.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "LocalizationConstants.h"

#define LOCALIZATION(text) [[LocalizationManager sharedInstance] localizedStringForKey:(text)]
#define LOCALIZATION_EN(text) [[LocalizationManager sharedInstance] localStringForKey: text languageCode: @"en"]
#define LOCALIZATION_WITHCODE(text, code) [[LocalizationManager sharedInstance] localStringForKey: text languageCode: newLanguage]
#define mLocalizationManager 	((LocalizationManager *) [LocalizationManager sharedInstance])

static NSString * const kNotificationLanguageChanged = @"kNotificationLanguageChanged";



@interface LocalizationManager : NSObject

@property (nonatomic, readonly) NSArray* availableLanguagesArray;
@property (nonatomic, assign) BOOL saveInUserDefaults;
@property NSString * currentLanguage;

+ (LocalizationManager*)sharedInstance;

- (void)setLocalizationLanguage:(NSString *)language;
-(NSString *)localizedStringForKey:(NSString*)key;
-(NSString *)localStringForKey:(NSString*) key languageCode:(NSString*) languageCode;
-(BOOL)setLanguage:(NSString*)newLanguage;

@end
