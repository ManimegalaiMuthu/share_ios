//
//  Session.h
//  JTIStarPartners
//
//  Created by Shekhar on 16/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import  <UIKit/UIKit.h>
#import "SecureStoreManager.h"
#import "AlertViewManager.h"
#import "UncaughtExceptionHandler.h"
#import "LoginInfo.h"
#import "DataManager.h"
#import "Member.h"
#import "ProfileInfo.h"
#import "WalkthroughHelper.h"
#import "LookupManager.h"

@class Reachability;

#define mSession 	((Session *) [Session getInstance])
#define LOGIN_TOKEN       @"LoginToken"

@interface Session : NSObject
{
    SecureStoreManager *mSecureStoreManager;
    Reachability *internetReach;
}

@property(nonatomic)BOOL isUserDataChanged;
@property ProfileInfo *profileInfo;
@property(nonatomic, strong)NSDictionary *lookupTable;
@property(nonatomic, strong)NSDictionary *languageTable;
@property(nonatomic, strong) NSString *currentViewControllerString;

@property(nonatomic) CGFloat rewardsCurrentPoints;

@property NSMutableDictionary *workshopOfferDict; //for edit details in workshop  offers

@property BOOL loadPerformance;

+ (Session *)getInstance;
- (void)checkFirstLaunch;
- (SecureStoreManager *) getSecureStoreManager;
- (void)checkSessionAndLaunchApp:(NSDictionary *) userInfo;
-(void) handlePushNotificationWithReferenceKey:(NSString *) referenceKey referenceValue:(NSString *)referenceValue;
-(void) sessionSendOpenNotificationApiWithReferenceValue:(NSString *)referenceValue;

#pragma mark QuickAction
-(void) configureShortcutItems:(BOOL) isLoggedIn;
-(void) handleShortcutItem:(UIApplicationShortcutItem *)shortcutItem;

#pragma mark Google Analytics
-(void) updateAnalyticsUserProperty;
-(void) locationUpdate;
-(BOOL) getReachability;
//Views
- (void)loadSplash:(BOOL) isAfterLogin;
- (void)loadLoginView;
- (void)pushForgetPasswordView: (UIViewController*) parentController;
- (void)pushChangePasswordView: (NSString *)mobNum showBackBtn:(BOOL)showBackBtn vc: (UIViewController*) parentController;


- (void) pushMobileAuthView: (UIViewController*) parentController;
- (void) pushOTPView: (UIViewController*) parentController mobNum:(NSString* ) mobNum countryCode:(NSString *)countryCode regType:(NSInteger) regType;

- (void)pushTradeRegistrationView: (UIViewController*) parentController mobNum: (NSString* ) mobNum countryCode:(NSString *)countryCode;
- (void)pushTradeRegistrationTwoView: (UIViewController*) parentController memberInfo: (Member *) memberInfo;
- (void)pushWorkingHoursView: (UIViewController*) parentController workingHoursData:(NSDictionary *) data;

- (void)pushMechanicRegistrationView: (UIViewController*) parentController mobNum: (NSString* ) mobNum countryCode:(NSString *)countryCode;


- (void)loadContactView;
- (void) pushContactView: (UIViewController*) parentController;
-(void) pushPhotoTakingViewControllerWithParent: (UIViewController *) parentController;
-(void) pushPanCardPhotoTakingWithParent: (UIViewController *) parentController
                                 message: (NSString *) message;
-(void) pushPanCardPhotoTakingWithParent: (UIViewController *) parentController
                              delegateVc: (UIViewController *) delegateVc
                                 message: (NSString *) message
                                     key:(NSString *)key;
-(void) pushCarPlateScanning: (UIViewController *) parentController;

-(void) pushWalkthroughView: (UIViewController*) parentController
                   WithType: (enum WalkthroughType) type;
-(void) loadWalkthroughView :(enum WalkthroughType) type;

- (void)popVC: (UIViewController *) vc;

- (void)loadHomeView;
- (void)loadManageWorkshopView;
- (void)loadWorkshopDetailsTabView;
- (void)pushWorkshopDetailsTabView: (UIViewController*) parentController
               workshopDetailsData:(NSDictionary *) workshopDetailsData;
- (void)pushINWorkshopAddNewView: (UIViewController*) parentController;

- (void)loadSpiraxWorkshopView;
- (void)loadSpiraxWorkshopDetailsTabView;
- (void)pushSpiraxWorkshopDetailsTabView: (UIViewController*) parentController
workshopDetailsData:(NSDictionary *) workshopDetailsData;
- (void)pushINSpiraxWorkshopAddNewView: (UIViewController*) parentController;

- (void)loadPerformanceView;
//- (void)loadRUPerformanceView;
- (void)loadMechPointsView;

- (void)loadQRCodeScanScreen;
- (void)loadAssistedRegistrationView;
- (void)loadInviteTradeView;

//- (void)pushNotificationsDetailView:(NSString*)pushID vc:(UIViewController*) parentController;
- (void)pushNotificationsDetailView:(NSDictionary*)msgData vc:(UIViewController*) parentController;

- (void)loadManageMechanic;
- (void)pushManageMechanic:(NSString *)tradeID
                        vc:(UIViewController*) parentController;
- (void)pushMechanicDetailsView:(NSString *)tradeID
                 registrationID:(NSString *)registrationID
                workshopTradeID:(NSString *)workshopTradeID
                             vc:(UIViewController*) parentController;

- (void)pushStaffDetailsView:(NSString *)tradeID
                             vc:(UIViewController*) parentController;
- (void)loadPromotionsView;

- (void)loadPromoCodeMasterView;
- (void)pushPromoCodeView:(UIViewController*) parentController;
- (void)loadPromoCodeView;
-(void)pushPromoCodeResultView:(NSDictionary *)resultsDict vc:(UIViewController*) parentController;

- (void)loadProfileTabView;
- (void)loadMyProfileView;
- (void)loadMoreInfoInProfile;

- (void)loadOrderView;
- (void)loadProductListView;

-(void) loadTOEbooking;

- (void)loadNotificationsView;
- (void)loadNotificationsViewfromPushWithId: (NSString *) pushId;
- (void)loadInAppBrowserFromPushWithId: (NSString *) pushId urlString: (NSString *) urlString;
- (void)pushNotificationView: (UIViewController*) parentController;

- (void)pushNotificationEditWithListArray:(NSMutableArray *)listArray vc:(UIViewController*) parentController;
- (void)pushInAppBrowserWithAddress: (NSString *) address vc:(UIViewController*) parentController;

- (void)loadScanProductView;
- (void)pushScanProductView:(NSString *)vehicleNumber previousState:(NSString*) previousState vc:(UIViewController*) parentController;
- (void)pushPendingCodeView: (UIViewController*) parentController;
- (void)pushPendingCodeViewForScanBottle:(BOOL)isScanBottle forConsumer:(BOOL)forConsumer onVc:(UIViewController *) parentController;

- (void)loadSearchVehicleView;
- (void)loadCashIncentiveView;
- (void)loadSearchVehicleViewWithDecal: (NSString *) decalString;
- (void)loadDecalView;
- (void)pushDecalResultsWithDictionary:(NSDictionary *)resultsDict parentController:(UIViewController*) parentController;

-(void) pushGalleryView: (NSString*) tradeID
             isApproved:(BOOL) isApproved
                     vc:(UIViewController*) parentController
               delegate:(UIViewController*) delegate;

- (void)loadManageOrder;
- (void)loadParticipatingProduct;
- (void)pushParticipatingProductsWith:(NSArray *) productArray onVc:(UIViewController *) parentController;
- (void)pushPendingOrderView: (UIViewController*) parentController;
- (void)loadOrderConfirmView;

- (void)loadFAQView;
- (void)loadTNCView;
- (void)loadOtherView;
- (void)loadAddView;
- (void)loadViewOrderView;
- (void)loadParticipatingView;
- (void)loadSettingsView;
- (void)loadWorkshopOfferView;
- (void) loadPrivacyView;
- (void) pushGenericWebViewWith:(NSString *) titleToBeDisplayed andURL:(NSString *) urlToBeDisplayed onVC:(UIViewController*) parentVC;
- (void) pushGenericWebViewWith:(NSString *) titleToBeDisplayed andHtmlString:(NSString *) htmlString onVC:(UIViewController*) parentVC;

- (void)loadSurveyView;

- (void) pushTNCView: (UIViewController*) parentController;
- (void) pushTNCView:(BOOL)showAgreeBtn tncType:(NSInteger)tncType vc: (UIViewController*) parentController;
- (void) pushPrivacyView:(BOOL)showAgreeBtn vc: (UIViewController*) parentController;
- (void) pushPrivacyView: (UIViewController*) parentController;
- (void) pushRegisterCodeResult:(NSDictionary *)resultsDict vc:(UIViewController*) parentController;
- (void) pushScanBottleResult:(NSDictionary *)resultsDict isScanBottle:(BOOL)isScanBottle isConsumer:(BOOL) isConsumer vc:(UIViewController*) parentController;

- (void)loadRewardsView;
- (void)loadRewardsViewWithLockFlag:(BOOL) locked;

-(void) pushRewardsCategoryWithCategoryArray: (NSArray *) categoryArray vc:(UIViewController*) parentController;
-(void) pushRewardsItemDetailsView: (NSDictionary *) item vc:(UIViewController*) parentController;
-(void) pushRewardsFavourites:(UIViewController*) parentController;
-(void) pushRewardsCart:(UIViewController*) parentController;
-(void) pushRewardsAddress:(UIViewController*) parentController;
-(void) pushRewardsConfirmation:(UIViewController*) parentController addressData:(NSDictionary *) addressData;
-(void) pushRewardsHistory:(UIViewController*) parentController;
-(void) pushRewardsRedemptionDetailsWithDetails: (NSDictionary *) detailsDict vc:(UIViewController*) parentController;
-(void) loadRewardsCompletionWithDictionary:(NSDictionary *) completionDictionary;
-(void) loadRewardsHistoryFromCompletion;

- (void)loadTOLoyaltySummaryView;
-(void) pushTOLoyaltyDetails:(UIViewController*) parentController addressData:(NSDictionary *) addressData;

- (void)pushOfferDetailsView:(UIViewController*) parentController offerDict:(NSDictionary *)offerDict;
- (void)pushOfferConfirmationView:(UIViewController*) parentController dict:(NSDictionary *)dict;
//- (void)pushCalenderView:(UIViewController*) parentController;

- (void)loadInviteCustomerView;
- (void) pushInviteCustomerView:(NSString *) vehicleNumber vc: (UIViewController*) parentController;

- (void) loadScanBottle;
- (void) pushScanBottleForShareCodeView : (UIViewController *) parentController;
- (void) pushScanBottleForVehicleIDView : (UIViewController *) parentController;
- (void) loadAddNewNetwork;

#pragma mark - In App Asset
-(void)loadMyLibraryView;
-(void)loadPerformanceCTA:(NSString *) categoryId;
-(void) pushInAppAssetCategoryListWithCategoryId:(NSString *)categoryId vc:(UIViewController *) parentController;
-(void) pushInAppAssetPDFView:(NSString *)pdfUrlString vc:(UIViewController *) parentController;

-(void)loadWorkshopProfilingView;
-(void) pushWorkshopProfilingDetails: (UIViewController *) parentController
                 workshopDetailsData:(NSDictionary *) workshopDetailsData;

#pragma mark - RUSSIA
-(void) loadRUEarnPoints;
-(void) pushRUEarnPoints: (UIViewController *) parentController;
-(void)loadRUPartners;
//-(void)loadRUSearchVehicle;
//-(void)loadRURegisterOilChange;
-(void) pushRURegisterOilChangeWithVehicleId: (NSString *) vehicleId vc:(UIViewController *) parentController;
-(void) pushRURegisterOilChangeResults:(NSMutableDictionary *)resultsDict vc:(UIViewController *) parentController;
-(void) pushRUBreakdown: (UIViewController *) parentController fromB2CCampaign:(BOOL) isB2CCampaign;

-(void)loadRUCustomerRedemption;
//-(void) pushRUCustomerRedemption: (UIViewController *) parentController;
-(void) pushRUCustomerRedemptionSuccess:(NSDictionary *)successParams vc: (UIViewController *) parentController;

-(void) pushRUChooseProductBrand: (UIViewController *) parentController;
-(void) pushRUChooseProductWithBrand: (NSDictionary* )brandDict vc: (UIViewController *) parentController;
-(void) loadRURegisterCustomer: (NSString *) vehicleId;
-(void) pushRUSearchProductWithAllItems:(NSMutableArray *)allItemsArray vc: (UIViewController *) parentController;
-(void) pushRUCustomerSignature: (UIViewController *) parentController;
-(void) pushRUCustomerSignatureWithDelegate: (UIViewController *) parentController delegate:(UIViewController *) delegate;

- (void) pushRegisterPanOTPViewWithMobileNo:(NSString *)mobileNo dictBankDetail:(NSDictionary*)dictBank vc: (UIViewController*) parentController;
- (void) pushRURegisterConsumerOTPViewWithDict:(NSMutableDictionary *)submitDict vc: (UIViewController*) parentController;
-(void) pushValueCalculatorWithData:(NSDictionary *)calculatorDict vc:(UIViewController *)vc;
- (void)pushRUOfferDetailsView:(UIViewController*) parentController offerDict:(NSDictionary *)offerDict;

- (void)pushRUAddNewOfferStepOne:(UIViewController*) parentController;
//- (void)pushRUAddNewOfferStepOneWithOffer:(NSMutableDictionary *) offerDict vc:(UIViewController*) parentController; //for edit or reorder

- (void)pushRUAddNewOfferStepTwoWithOffer:(NSMutableDictionary *) offerDict vc:(UIViewController*) parentController;
- (void)pushRUAddNewOfferStepThreeWithOffer:(NSMutableDictionary *) offerDict vc:(UIViewController*) parentController;
- (void)pushRUAddNewOfferStepFourWithOffer:(NSMutableDictionary *) offerDict vc:(UIViewController*) parentController;
//- (void)pushRUAddNewOfferSuccessWithOffer:(NSMutableDictionary *) offerDict vc:(UIViewController*) parentController;
-(void) loadRUAddNewOfferSuccessWithOffer:(NSMutableDictionary *) offerDict;
- (void)pushRUAddNewOfferSelectCarBrand:(NSArray *)selectedCustomers vc:(UIViewController *) parentController;
- (void)pushRUAddNewOfferSelectOilBrand:(NSDictionary *)submitParams vc:(UIViewController *) parentController;

#pragma mark - Russia V2
-(void) pushActivateRegistration:(NSDictionary *)values vc:(UIViewController *)parentController delegate:(UIViewController *)delgate;

#pragma mark RU Performance
-(void) loadRUWorkshopPerformance;
-(void) loadRUDSROrderBreakdown;
-(void) loadRUWorkshopPerformanceFromCarousel: (NSString *) pushNavKey;

-(void) pushOrderBreakdown:(UIViewController *)vc;
-(void) pushPointsBreakdown:(UIViewController *)vc;
-(void) pushStaffBreakdown:(UIViewController *)vc;
-(void) pushOilChangeBreakdown:(UIViewController *)vc;
-(void) pushCustomerBreakdown:(UIViewController *)vc;
-(void) pushOfferRedemptionBreakdown:(UIViewController *)vc;
-(void) pushDSRBreakdown:(BOOL)isOrderBreakdown vc:(UIViewController *)vc;
-(void) pushDSRCustomerBreakdown:(UIViewController *)vc;
-(void) loadApprovedWorkshop;

#pragma mark - IN Performance
-(void) loadINPerformance;
-(void) pushINBreakdown:(BOOL)isProductBreakdown vc:(UIViewController *)vc;
-(void) pushINCustomerBreakdown:(UIViewController *)vc;
-(void) pushINMechBreakdown:(UIViewController *)vc;
-(void) pushPerformanceDetails:(NSArray *) categoryArray
                       vcTitle:(NSString *) sectionName
              summaryDataArray:(NSArray *) summaryDataArray
               originalTradeId:(NSString*) originalTradeId
                 selectedIndex:(NSUInteger)selectedIndex
                            vc:(UIViewController *) vc;


-(void) pushImagePreview: (UIViewController *) vc image: (UIImage *) image title:(NSString *) title;
-(void) pushImagePreview: (UIViewController *) vc image: (UIImage *) image;

#pragma mark - Register Consumer
-(void) loadRegisterCustomerView:(NSDictionary *) promoDict;
- (void) pushRegisterConsumer:(NSDictionary *)registerCustomerDict vc: (UIViewController*) parentController;
- (void) pushRegisterConsumerOTPViewWithDict:(NSDictionary *)promoDict vc: (UIViewController*) parentController;
- (void) pushRegisterConsumerRegisterFormViewWithOTPDict:(NSDictionary *)otpDict vc:(UIViewController*) parentController;
- (void) pushRegisterConsumerRegisterCompleteWithDict:(NSDictionary *)promoDict vc:(UIViewController*) parentController;
-(void) loadRegsumerRegisterCompleteWithDict:(NSDictionary *) completionDictionary;
-(void) loadRURegisterConsumerSuccess;

#pragma mark - Loyalty Contract
-(void) loadLoyaltyContract;
-(void) loadLoyaltyContractProgramPerformance;
-(void) pushFilterBy:(NSMutableDictionary *)filterStatusArray delegate:(UIViewController*)delegate vc: (UIViewController*) parentController;
-(void) pushAddNewContract:(UIViewController *)parentController;
-(void) pushFinalViewInNewContract:(UIViewController *)parentController withPreferedDict:(NSDictionary*)responseData forTradeId:(NSString *) tradeID withCatalogue:(NSArray *)catalogueList;
-(void) loadContractConfirmView;
-(void) pushContractDetails:(NSDictionary *)contractDetails vc:(UIViewController *)parentController;
-(void) pushLoyaltyBreakdown:(NSDictionary *)workshopDetails withWorkshopList:(NSArray *) workshopList andContract:(NSString *) contractRef vc:(UIViewController *)parentController;
-(void) pushLoyaltyCartonBreakdown:(NSDictionary *)breakdownDetails vc:(UIViewController *)parentViewController;
-(void) loadLoyaltyContractDetails:(NSDictionary*)contractDetails;
-(void) pushPendingContractDetails:(NSDictionary *)breakdownDetails vc:(UIViewController *)parentViewController;
-(void) pushContractDetailsForOwner:(NSDictionary *)breakdownDetails vc:(UIViewController *)parentViewController;

#pragma mark - From Carousel
-(void) loadRecentActivity;
-(void) pushScanNRewardReport:(NSArray *)productNameList vc:(UIViewController *)parentController;

#pragma mark - SDA
-(void) loadSDA:(UIViewController *)vc;

#pragma mark - SWA
-(void) loadSWA:(UIViewController *)vc;

#pragma mark - HomeV2
-(void) pushHomePromotionsWithSelectedIndex:(NSInteger)selectedIndex onVc:(UIViewController *)vc;
-(void) pushPromoDetailsWith:(NSDictionary*)promoDetails andIsPromotion:(BOOL)isPromotion onVc:(UIViewController *)vc;
-(void) showCoachMark:(NSArray *)coachMarkData withDelegate:(UIViewController*)delegate onVc:(UIViewController *)vc;

#pragma mark - Inventory Management/Moscow
-(void) loadDSRInventoryManagement;
-(void) loadTOInventoryManagement;
-(void) pushInventoryScanResultWith:(NSArray *)data onVc:(UIViewController *)vc;
-(void)  loadTotalEarnings;
-(void) pushInventoryForSelectedWorkshopon:(UIViewController *)vc;
-(void) pushDSRInventoryScanOn:(UIViewController *)vc;

#pragma mark - lubeMatch
-(void) loadLubeMatch;
-(void) pushLubematchPopupWith:(NSArray *)lubematchArray forVehicleId:(NSString *)vehicleID onVc:(UIViewController *)vc;
-(void) pushRegisterOilChangeFor:(NSString *)vehicleID onVc:(UIViewController *)vc;

#pragma mark - Lebaran Promotion
-(void) presentScratchViewOn:(UIViewController *)vc forGamification:(BOOL) isGamification;
-(void) pushPromotionViewDetailsWithData:(NSDictionary *)data onVc:(UIViewController *)vc;

-(void) pushCheckInTodayPopuponVc:(UIViewController *)vc;

#pragma mark - AI
-(void) popupAIPage:(UIViewController *)vc;

#pragma mark - lookup tables methods
- (NSArray *) getCountryDialingCodes;
- (NSArray *) getCountryNames;
- (NSString *) convertDialCodeToCountryCode: (NSString*) dialingCode;
- (NSString *) convertDialCodeToCountryName: (NSString*) dialingCode;
- (NSString *) convertCountryNameToCountryCode: (NSString*) countryName;
- (NSString *) convertCountryCodeToCountryName: (NSString*) countryName;
- (NSString *) convertCountryCodeToCountryDialingCode: (NSString*) countryCode;
- (NSString *) convertCountryCodeToCountryDisplayName: (NSString*) countryCode;
- (NSString *) convertCountryDisplayNameToCountryCode: (NSString*) countryName;

- (NSArray *) getSalutations;
- (NSString *) convertToSalutation: (NSInteger) salutationKeyCode;
- (NSString *) convertToSalutationKeyCode: (NSString *) salutation;

- (NSArray *) getStaffAccountType;
- (NSString *) convertToStaffAccountType: (NSInteger) staffAccountTypeCode;
- (NSString *) convertToStaffAccountTypeCode: (NSString *) staffAccountType;

- (NSArray *) getCompanyTypes;
- (NSString *) convertToCompanyTypes: (NSInteger) companyTypeKeyCode;
- (NSString *) convertToCompanyTypesKeyCode: (NSString *) companyType;

- (NSArray *) getCompanyCategory;
- (NSString *) convertToCompanyCategory: (NSInteger) companyCategoryKeyCode;
- (NSString *) convertToCompanyCategoryKeyCode: (NSString *) companyCategory;

- (NSArray *) getGender;
- (NSString *) convertToGenderKeyCode: (NSString *) gender;

- (NSArray *) getContactPreference;
- (NSString *) convertToContactPreferenceKeyCode: (NSString *) contactPref;

- (NSArray *) getEnquiryType;
- (NSString *) convertToEnquiryTypeKeyCode: (NSString *) enquiryTypeString;

- (NSArray *) getCompanyStatus; //Approved Workshop Status
- (NSString *) convertToCompanyStatus: (NSInteger) statusKeyCode;
- (NSString *) convertToCompanyStatusKeyCode: (NSString *) companyStatus;

- (NSArray *) getLeadStatus; //Pending Workshop Status
- (NSString *) convertToLeadStatus: (NSInteger) statusKeyCode;
- (NSString *) convertToLeadStatusKeyCode: (NSString *) companyStatus;

- (NSArray *) getTransactionTypes;
- (NSString *) convertToTransactionTypes: (NSInteger) transactionTypeKeyCode;
- (NSString *) convertToTransactionTypesKeyCode: (NSString *) transactionType;

- (NSArray *) getWorkshopStatus;    //workshop status - active, lapsed, inactive
- (NSString *) convertToWorkshopStatus: (NSInteger) workshopStatusKeyCode;
- (NSString *) convertToWorkshopStatusKeyCode: (NSString *) workshopStatus;

- (NSArray *) getWorkshopProfileTypes;
- (NSString *) convertToWorkshopProfileTypes: (NSInteger) workshopProfileTypeKeyCode;
- (NSString *) convertToWorkshopProfileTypesKeyCode: (NSString *) workshopProfileType;


- (NSArray *) getCompanySource;
- (NSString *) convertToCompanySource: (NSInteger) companySourceKeyCode;
- (NSString *) convertToCompanySourceKeyCode: (NSString *) companySource;

- (NSArray *) getRewardsPoints;
- (NSString *) convertToRewardsPoints: (NSInteger) rewardsPointsKeyCode;
- (NSString *) convertToRewardsPointsKeyCode: (NSString *) rewardsPoints;

- (NSArray *) getRedemptionType;
- (NSString *) convertToRedemptionType: (NSInteger) redemptionTypeKeyCode;
- (NSString *) convertToRedemptionTypeKeyCode: (NSString *) redemptionType;

- (NSArray *) getRewardsCategory;
- (NSString *) convertToRewardsCategory: (NSInteger) rewardsCategoryKeyCode;
- (NSString *) convertToRewardsCategoryKeyCode: (NSString *) rewardsCategory;

//rewards v2
- (NSArray *) getRedeemStatus;
- (NSString *) convertRedeemStatus: (NSInteger) redeemStatusCode;
- (NSString *) convertToRedeemStatusKeyCode: (NSString *) redeemStatus;

- (NSArray *) getRedeemSort;
- (NSString *) convertRedeemSort: (NSInteger) redeemSortCode;
- (NSString *) convertToRedeemSortKeyCode: (NSString *) redeemSort;

- (NSArray *) getRedeemDate;
- (NSString *) convertRedeemDate: (NSInteger) redeemDateCode;
- (NSString *) convertToRedeemDateKeyCode: (NSString *) redeemDate;

-(void) setDefaultLanguage;
- (NSArray *) getLanguageList;
- (NSString *) convertLanguageNameToSortCode: (NSString*) languageName;
- (NSString *) convertLanguageNameToLanguageKeyCode: (NSString *) languageString;
- (NSString *) convertLanguageKeyCodeToLanguageName: (NSString*) languageCode;
- (NSString *) convertLanguageSortCodeToKeyCode: (NSString*) sortCode;
- (NSString *) convertLanguageKeyCodeToSortCode: (NSString*) languageCode;

- (NSArray *) getStateNames: (NSArray *) stateDetailedListArray;

- (NSString *) convertToStateName: (NSString *) stateKeyCode stateDetailedListArray:(NSArray *) stateDetailedListArray;
- (NSString *) convertToStateKeyCode: (NSString *) state stateDetailedListArray:(NSArray *) stateDetailedListArray;

//Workshop offer
- (NSArray *) getWorkshopOfferArray;
- (NSString *) convertWorkshopOffer: (NSInteger) workshopOfferCode;
- (NSString *) convertToWorkshopOfferKeyCode: (NSString *) workshopOfferType;

-(NSArray *)getWorkshopOfferProduct;
- (NSString *) convertToWorkshopProductKeyCode:(NSString *)workshopOfferProduct;
- (NSString *) convertToWorkshopProduct: (NSInteger) workshopOfferProductKeyCode;

-(NSArray *)getWorkshopOfferService;
- (NSString *) convertToWorkshopService: (NSInteger) workshopOfferServiceKeyCode;
- (NSString *) convertToWorkshopServiceKeyCode:(NSString *)workshopOfferService;

-(NSArray *)getAncilliaryOfferType;
- (NSString *) convertToAncilliaryOfferTypeKeyCode:(NSString *)ancilliaryOfferType;

-(NSArray *)getAncilliaryOfferStatus;
- (NSString *) convertToAncilliaryOfferStatusKeyCode:(NSString *)ancilliaryOfferStatus;
- (NSString *) convertToAncilliaryOfferStatus: (NSInteger) workshopOfferStatusKeyCode;

-(NSArray *)getOfferPeriod;
- (NSString *) convertToOfferPeriodTypeKeyCode:(NSString *)offerPeriod;

//manage order
- (NSArray *) getOrderStatus;
- (NSString *) convertToOrderStatus: (NSInteger) orderStatusKeyCode;
- (NSString *) convertToOrderStatusKeyCode: (NSString *) orderStatusType;

- (NSArray *) getOrderDate;
- (NSString *) convertToOrderDateType: (NSInteger) orderDateKeyCode;
- (NSString *) convertToOrderDateKeyCode: (NSString *) orderStatusType;


//get workshop profiling uk
- (NSArray *) getCompanyProfService;
- (NSString *) convertToCompanyProfService: (NSInteger) companyProfServiceKeyCode;
- (NSString *) convertToCompanyProfServiceKeyCode: (NSString *) companyProfService;

- (NSArray *) getCompanyProfPackSize;
- (NSString *) convertToCompanyProfPackSize: (NSInteger) companyProfPackSizeKeyCode;
- (NSString *) convertToCompanyProfPackSizeKeyCode: (NSString *) companyProfPackSize;

- (NSArray *) getCompanyProfMainBrand;
- (NSString *) convertToCompanyProfMainBrand: (NSInteger) companyProfMainBrandKeyCode;
- (NSString *) convertToCompanyProfMainBrandKeyCode: (NSString *) companyProfMainBrand;

- (NSArray *) getCompanyProfBuy;
- (NSString *) convertToCompanyProfBuy: (NSInteger) companyProfBuyKeyCode;
- (NSString *) convertToCompanyProfBuyKeyCode: (NSString *) companyProfBuy;

- (NSArray *) getDateRange;
- (NSString *) convertToDateRange: (NSInteger) dateRange;
- (NSString *) convertToDateRangeKeyCode: (NSString *) dateRange;


#pragma mark - Register Consumer Lookup
- (NSArray *) getVehicleUsage;
- (NSString *) convertToVehicleUsage: (NSInteger) vehicleUsage;
- (NSString *) convertToVehicleUsageKeyCode: (NSString *) vehicleUsage;

- (NSArray *) getYearOfManufacture;
- (NSString *) convertToYearOfManufacture: (NSInteger) yearOfManufacture;
- (NSString *) convertToYearOfManufactureKeyCode: (NSString *) yearOfManufacture;

#pragma mark Russia Vehicle Make
- (NSArray *) getVehicleMake;
- (NSString *) convertToVehicleMake: (NSInteger) vehicleMake;
- (NSString *) convertToVehicleMakeKeyCode: (NSString *) vehicleMake;

#pragma mark India
- (NSArray *) getBanksDetailsList;
- (NSString *) convertToBankName: (NSInteger) bankName;
- (NSString *) convertToBankNameKeyCode: (NSString *) bankName;

#pragma mark Cash Incentive
- (NSArray *) getCashStatus;
- (NSString *) convertToCashStatus: (NSInteger) status;
- (NSString *) convertToCashStatusKeyCode: (NSString *) status;

- (NSArray *) getCashServiceType;
- (NSString *) convertToCashServiceType: (NSInteger) serviceType;
- (NSString *) convertToCashServiceTypeKeyCode: (NSString *) serviceType;

#pragma mark-Loyalty
- (NSArray *) getContractStatus;
- (NSString *) convertToContractStatus: (NSInteger) contractStatus;
- (NSString *) convertToContractStatusKeyCode: (NSString *) contractStatus;

- (NSArray *) getProgramPeriod;
- (NSString *) convertToProgramPeriod: (NSInteger) programPeriod;
- (NSString *) convertToProgramPeriodKeyCode: (NSString *) programPeriod;

#pragma mark - Vehicle Type Lookup (VN Rimula)
- (NSArray *) getVehicleType;
- (NSString *) convertToVehicleType: (NSInteger) vehicleType;
- (NSString *) convertToVehicleTypeKeyCode: (NSString *) vehicleType;

//Order details
-(void) pushOrderDetail: (UIViewController *) parentController orderID:(NSString *)orderID isShareOrder:(BOOL) isShareOrder;
//-(void) pushCategoryView: (UIViewController *) parentController productID:(NSString *)productID categoryName:(NSString *)categoryName;
-(void) pushCategoryView: (UIViewController *) parentController productID:(NSString *)productID categoryName:(NSString *)categoryName categoryArray:(NSArray *)categoryArray;
-(void) pushMyCartView: (UIViewController *) parentController;

-(NSString *)getDisclaimerMsgForMyCart;

//login functions
- (void)saveUser:(LoginInfo *)userInfo;
- (LoginInfo *)loadUserProfile;
-(void) loadVCWithSidePanelWithVc:(UIViewController *) vc;

- (void)saveReports:(NSArray *)reports withCacheKey:(NSString *)cacheKey;
- (NSArray *)loadReportswithCacheKey:(NSString *)cacheKey;

- (void)saveProfilePic:(UIImage *)image;
- (void)saveImage:(UIImage *)image withFileName:(NSString *)fileName;

- (void)savePic:(UIImage *)image withCacheKey:(NSString *)cacheKey;
- (UIImage *)loadImageWithCacheKey:(NSString *)cacheKey withPlaceholder:(NSString *)fileName;

- (UIImage *)loadImageWithFileName:(NSString *)fileName;
//- (UIImage *)loadImage:(ImagePickerMode)mode;

- (NSString *)getDatabasePath;


- (void)reset;
- (UIWindow *)getMainWindow;

-(BOOL)isTOAccount;


@end
