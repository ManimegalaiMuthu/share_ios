
//
//  Session.m
//  JTIStarPartners
//
//  Created by Shekhar on 16/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import "Session.h"
#import "UncaughtExceptionHandler.h"
#import "AppDelegate.h"
#import "WebServiceManager.h"
#import "LocalizationManager.h"
#import "LoginViewController.h"
#import "Reachability.h"
#import "SplashScreenVC.h"
#import "Constants.h"
#import "FileManager.h"
#import "LookupManager.h"
#import <MapKit/MapKit.h>
#import "RegisterPanOTPViewController.h"
#import "OTPViewController.h"
#import "TradeRegistrationViewController.h"
#import "TradeRegistrationTwoViewController.h"
#import "MechanicRegistrationViewController.h"
#import "WorkingHoursViewController.h"
#import "WorkshopDetailsTabViewController.h"
#import "TermsAndConditionsViewController.h"
#import "ChangePasswordViewController.h"
#import "GalleryUploadViewController.h"

#import "NotificationDetailViewController.h"
#import "NotificationDeleteViewController.h"
#import "InAppBrowserViewController.h"

#import "WalkthroughViewController.h"
#import "PhotoTakingViewController.h"
#import "ScanProductCodeViewController.h"
#import "ManageMechanicViewController.h"
#import "MechanicDetailsViewController.h"
#import "MechanicDetails_NewViewController.h"

#import "RegisterCodeResultViewController.h"
#import "JASidePanelController.h"
#import "SideMenuViewController.h"

#import "RewardsItemDetailsViewController.h"
#import "RewardsConfirmationViewController.h"
#import "RewardsCategoryViewController.h"
#import "RewardsRedemptionDetailsViewController.h"
#import "RewardsRedemptionCompleteViewController.h"

#import "SearchVehicleViewController.h"

#import "PromoCodeResultViewController.h"

#import "TOLoyaltyDetailsViewController.h"

#import "InviteCustomerViewController.h"
#import "OrderConfirmationViewController.h"
#import "RDVTabBarController.h"
#import "RDVTabBarItem.h"
#import "OfferDetailsViewController.h"
#import "OfferConfirmationViewController.h"
#import "OrderDetailsViewController.h"
#import "AddNewCategoryViewController.h"

#import "InAppAssetCategoryViewController.h"
#import "InAppAssetPDFViewController.h"

#import "WorkshopProfilingDetailsViewController.h"
#import "IN_WorkshopAddNewViewController.h"

#import "ScanCode_DecalResultViewController.h"

#import "RegisterCustomerOTPViewController.h"
#import "RegisterCustomerFormViewController.h"
#import "RegisterCustomerCompleteViewController.h"

#import "RegisterCustomerViewController.h"

#import "RUChooseProductBrandViewController.h"
#import "RUChooseProductViewController.h"

#import "RURegisterOilChangeViewController.h"
#import "RURegisterOilChangeResultViewController.h"
#import "RUProductSearchViewController.h"
#import "RURegisterCustomerViewController.h"
#import "RUCustomerSignatureViewController.h"
#import "RURegisterCustomerOTPViewController.h"
#import "RURegisterCustomerSuccessViewController.h"
#import "RUValueCalculatorViewController.h"

//Russia Ancilliary Offer
#import "RUWorkshopOfferDetailsViewController.h"
#import "RUWorkshopOfferSelectCarBrandViewController.h"
#import "RUWorkshopOfferSelectOilBrandViewController.h"
#import "RUWorkshopOfferAddNew_StepOneViewController.h"
#import "RUWorkshopOfferAddNew_StepTwoViewController.h"
#import "RUWorkshopOfferAddNew_StepThreeViewController.h"
#import "RUWorkshopOfferAddNew_StepFourViewController.h"
#import "RUWorkshopOfferAddNew_SuccessViewController.h"
#import "RUCustomerRedemptionSuccessViewController.h"
#import "RUDSRBreakdownMainViewController.h"
#import "ManageWorkshopTabViewController.h"

#import "ManageStaffDetailViewController.h"

//India
#import "PendingWorkshopDetails_NewViewController.h"
#import "RUDSRPerformanceTabViewController.h"
#import "INBreakdownMainViewController.h"
#import "PerformanceDetails_NewViewController.h"
#import "RUTOPerformanceTabViewController.h"

#import "ImagePreviewViewController.h"

#import "FilterByViewController.h"
#import "AddNewContract_FirstStepViewController.h"
#import "AddNewContract_SecondStepViewController.h"
#import "LoyaltyContractDetailsViewController.h"
#import "LoyaltyWorkshopBreakdownViewController.h"
#import "LoyaltyCartonsBreakdownViewController.h"
#import "LoyaltyRewardsTabViewController.h"
#import "OwnerPendingContractDetailViewController.h"
#import "OwnerContractDetailsViewController.h"

#import "INPerformanceTabViewController.h"
#import "RUBreakdown_V2ViewController.h"

#import "AddNewWorkshop_NewViewController.h"
#import "RUEarnPointsTable_V2ViewController.h"
#import "PrivacyPolicyViewController.h"

#import "ScanBottleForShareCodeViewController.h"
#import "ScanBottleForConsumerViewController.h"
#import "PendingCodeViewController.h"

#import "HomePromotionViewController.h"
#import "HomePromotionsDetailViewController.h"
#import "GenericOnlyWebViewController.h"
#import "ProfileMasterViewController.h"
#import "MyProfile_NewViewController.h"
#import "CoachMarkViewController.h"

#import "InventoryScanResultViewController.h"
#import "InventoryListViewController.h"
#import "InventoryScanViewController.h"

#import "RegisterOilChangeLubematchViewController.h"
#import "RegisterOilChangeScanViewController.h"

#import "PromotionScratchViewController.h"
#import "PromotionDetailsViewController.h"
#import "CheckInViewController.h"
#import "ProductsTableViewController.h"
#import "ScanForRewardsHistoryViewController.h"

#import "AIHomeViewController.h"

#define k_LOGINALERT 111
#define k_FAILURE_ALERT 112
static Session* instance;

@interface Session()<WebServiceManagerDelegate, RDVTabBarControllerDelegate,CLLocationManagerDelegate>
{
    BOOL isReachable;
    UIWindow *mainWindow;
    JASidePanelController *sidePanel;
    dispatch_queue_t backgroundQueue;
    dispatch_queue_t cacheClear;
    //PullableView *pullLeftView;
    //ShoppingCartVC *shoppingCartVC;
    
    NSDictionary *notificationDict;
    
}

@property(nonatomic,strong) NSString *databasePath;

@property CLLocationManager *locationManager;

@end

@implementation Session
@synthesize profileInfo;
@synthesize isUserDataChanged;

#pragma mark -
#pragma mark INIT

- (id) init {
	if (self = [super init])
	{
        mainWindow = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).window;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged) name:kReachabilityChangedNotification object:nil];
        
        [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init]
                                          forBarPosition:UIBarPositionAny
                                              barMetrics:UIBarMetricsDefault];
        
        [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
        
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;

        if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)
        {
            isReachable = NO;
        }
        else
        {
            isReachable = YES;
        }
        internetReach = [Reachability reachabilityForInternetConnection];
        [internetReach startNotifier];
        backgroundQueue = dispatch_queue_create("bgqueue", NULL);
        cacheClear = dispatch_queue_create("bgclearcache", NULL);
        
        sidePanel = [[JASidePanelController alloc] init];
        _loadPerformance = NO;
        return self;
	}
    return nil;
}


+ (Session*) getInstance {
	@synchronized([Session class]) {
        
		if ( instance == nil ) {
			instance = [[Session alloc] init];
		}
	}
	return instance;
}

#pragma mark -
#pragma mark Unique instance create methods

- (SecureStoreManager *) getSecureStoreManager{
	if(!mSecureStoreManager){
		mSecureStoreManager = [[SecureStoreManager alloc] initWithServiceName:@"com.edenred.jtistarpartners"];
	}
	return mSecureStoreManager;
}

// NSLog(@"PROFILE----- %i",GET_PROFILETYPE);kPROFILETYPE_TRADEOWNER
- (BOOL) isTOAccount
{
    if(GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER){
        return true;
    }
    return false;
}

#pragma mark  Global Exception Handler

- (void) catchExceptions
{
    [UncaughtExceptionHandler run];
}

#pragma mark -
#pragma mark UI Controls
- (void)checkFirstLaunch
{
//    first launch of the app
    if (FIRSTLAUNCH)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kHasLaunchedOnce];
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:ENGLISH] forKey:USERDEFAULTS_LANGUAGETYPE];
        SET_VERSIONNUMBER_LATEST(VERSION_NUMBER);

        //For Coachmark
        if (!GET_COACHMARK_HASSHOWN)
        {
            //if coachmark data don't exist, initialise it
            NSMutableDictionary *coachmarkData = [[NSMutableDictionary alloc] init];
            SET_COACHMARK_HASSHOWN(coachmarkData);
        }
        
        SET_PUSHNOTIFICATIONENABLED(YES);
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:REMEMBER_ME];
        
        //clear login credentials
        SET_AUTHENTICATIONTOKEN(BLANK);
        [[mSession getSecureStoreManager] setKeyValue:BLANK forKey:USER_PASSWORD];
        SET_LOGIN_NAME(BLANK);
        SET_PUSHTOKEN(BLANK);
        [self createAndCheckDatabase];
        
//        for (NSString *languageString in kAvailableLanguages)
//        {
//            if (EQUALS(SYSTEMLANGUAGE, languageString))
//                SET_LOCALIZATION(languageString);
//        }
        if (EQUALS(SYSTEMLANGUAGE, kThai))
        {
            SET_LOCALIZATION(kThai);
            SET_COUNTRY_CODE(COUNTRYCODE_THAILAND);
        }
        else if (EQUALS(SYSTEMLANGUAGE, kRussian))
        {
            SET_LOCALIZATION(kRussian);
            SET_COUNTRY_CODE(COUNTRYCODE_RUSSIA);
        }
//        else if (EQUALS(SYSTEMLANGUAGE, kVietnamese))
//        {
//            SET_LOCALIZATION(kVietnamese);
//            SET_COUNTRY_CODE(COUNTRYCODE_VIETNAM);
//        }
//        else if (EQUALS(SYSTEMLANGUAGE, kIndo))
//            SET_LOCALIZATION(kIndo);
//        else if (EQUALS(SYSTEMLANGUAGE, kChinese))
//            SET_LOCALIZATION(kChinese);
//        else if(EQUALS(SYSTEMLANGUAGE, kMalay))
//            SET_LOCALIZATION(kMalay);
        else
            SET_LOCALIZATION(kEnglish);
        
        [[NSUserDefaults standardUserDefaults] synchronize];

        [self updateAnalyticsUserProperty];
    }
}

- (void)checkSessionAndLaunchApp:(NSDictionary *) userInfo
{
    [self loadSplash: NO];
    [self catchExceptions];

    //set alignment based on current language
    if ([GET_LOCALIZATION isEqualToString: kArabic] ||
        [GET_LOCALIZATION isEqualToString: kUrdu])
    {
        [UIView.appearance setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
    }
    else
    {
        [UIView.appearance setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
    }

    [self reloadSidePanel];
    
    if (!GET_WALKTHROUGH_TABLE)
    {
        SET_WALKTHROUGH_TABLE([[NSMutableDictionary alloc] init]);
    }
    
    if(![GET_VERSIONNUMBER_LATEST isEqualToString:VERSION_NUMBER]) {
        CLEAR_COACHMARK_HASSHOWN;
        SET_VERSIONNUMBER_LATEST(VERSION_NUMBER);
        NSMutableDictionary *coachmarkData = [[NSMutableDictionary alloc] init];
        SET_COACHMARK_HASSHOWN(coachmarkData);
    }

    if(GET_KEEPLOGIN)
    { //had login before and keep login
        if (userInfo)
        {
            notificationDict = [userInfo objectForKey:@"aps"];
//            [self performSelector: @selector(delayHandlePushNotification:) withObject:notificationDict afterDelay: 3.0];
            
            [self sessionSendOpenNotificationApiWithReferenceValue: [notificationDict objectForKey:@"ReferenceValue"]];
            [[WebServiceManager sharedInstance] fetchHomeSession];
        }
        else
        {
            if (isReachable)
            {
                //is online, check for auth token
                [[WebServiceManager sharedInstance] isValidAuthToken:self];
            }
            else
            {
                //offline
                [self configureShortcutItems:YES];
                [self performSelector:@selector(loadHomeView) withObject:nil afterDelay:3.0];
            }
        }
    }else{
        [mSession reset]; //reset all login fields if keep log in is not enabled
        [mLookupManager loadLookupTable];
        [self performSelector:@selector(loadLoginView) withObject:nil afterDelay:3.0];
        [self removeFile:USER_PROFILE_ARCHIVE_NAME];
    }
}

-(void) updateAnalyticsUserProperty
{
    LoginInfo *loginInfo = [mSession loadUserProfile];
    
    if ([loginInfo.tradeID length] > 0)
    {
        //Setup GA
        [FIRAnalytics setUserPropertyString: loginInfo.tradeID forName:@"TradeID"];
    }
    else
        [FIRAnalytics setUserPropertyString: @"0" forName:@"TradeID"];
    
    NSString *languageCodeString  = [NSString stringWithFormat: @"%lu", (unsigned long)[kAvailableLanguages indexOfObject: GET_LOCALIZATION]];
    [FIRAnalytics setUserPropertyString: languageCodeString forName:@"LanguageCode"];
    
    NSString *languageNameString = [self convertLanguageKeyCodeToLanguageName: languageCodeString];
    [FIRAnalytics setUserPropertyString: languageNameString forName:@"LanguageName"];
    
    NSString *countryNameString  =  [self convertCountryCodeToCountryName:GET_COUNTRY_CODE];
    [FIRAnalytics setUserPropertyString: countryNameString forName:@"CountryName"];
    [FIRAnalytics setUserPropertyString: GET_COUNTRY_CODE forName:@"CountryCode"];
    
    NSString *profileTypeString  = [NSString stringWithFormat: @"%lu", (unsigned long) GET_PROFILETYPE];
    [FIRAnalytics setUserPropertyString: profileTypeString forName:@"ProfileType"];
    
    
//    [FIRAnalytics logEventWithName: @"UserPropertyChange" parameters: nil];
}

-(void) delayHandlePushNotification
{
    if (notificationDict)
    {
        [mSession handlePushNotificationWithReferenceKey: [notificationDict objectForKey:@"ReferenceKey"]
                                          referenceValue: [notificationDict objectForKey:@"ReferenceValue"]];
        notificationDict = nil;
    }
    else
        [self checkSessionAndLaunchApp: nil];
}

enum kReferenceKey_TYPE
{
    kReferenceKey_NOTIFICATIONLIST = 0,
    kReferenceKey_REWARDS,
    kReferenceKey_ORDERS,
    kReferenceKey_LEADS,
    kReferenceKey_WORKSHOP,
    kReferenceKey_PERFORMANCE,
    kReferenceKey_SCANCODE,
    kReferenceKey_INAPPBROWSER, //from notification details
    
};

-(void) sessionSendOpenNotificationApiWithReferenceValue:(NSString *)referenceValue
{
    [[WebServiceManager sharedInstance] openPushMessageInSession:referenceValue];
}

#pragma mark QuickAction
-(void) configureShortcutItems:(BOOL) isLoggedIn {
    NSString *bundleString = [[NSBundle mainBundle] bundleIdentifier];
    NSMutableArray *items = [[NSMutableArray alloc] initWithCapacity:4];
    if(isLoggedIn) {
        if(self.profileInfo == nil) {
            self.profileInfo = [[ProfileInfo alloc] initWithData: [GET_HOMEDATA objectForKey: @"MyProfile"]];
            self.lookupTable = GET_LOOKUP_TABLE;
        }
        if([self.profileInfo hasOrdering_V2]) {
            UIApplicationShortcutItem *onlineOrderShortcut = [[UIApplicationShortcutItem alloc]
                                                           initWithType:[NSString stringWithFormat:@"%@.OnlineOrder",bundleString]
                                                           localizedTitle:LOCALIZATION(C_QL_MANAGEORDERS)
                                                           localizedSubtitle:nil
                                                           icon:[UIApplicationShortcutIcon iconWithTemplateImageName:@"quicklinks_manageorder_midgrey.png"]
                                                           userInfo:nil];
            [items addObject:onlineOrderShortcut];
        }
        if([self.profileInfo hasRegisterProductCode_V2] || [self.profileInfo hasLubeMatch]) {
            UIApplicationShortcutItem *oilChangeShortcut = [[UIApplicationShortcutItem alloc]
                                                           initWithType:[NSString stringWithFormat:@"%@.OilChange",bundleString]
                                                           localizedTitle:LOCALIZATION(C_QL_REGOILCHANGE)
                                                           localizedSubtitle:nil
                                                           icon:[UIApplicationShortcutIcon iconWithTemplateImageName:@"quicklinks_registerproductcode_midgrey.png"]
                                                           userInfo:nil];
            [items addObject:oilChangeShortcut];
        }
        if([self.profileInfo hasCashIncentive]) {
            UIApplicationShortcutItem *cashIncentiveShortcut = [[UIApplicationShortcutItem alloc]
                                                           initWithType:[NSString stringWithFormat:@"%@.CashIncentive",bundleString]
                                                           localizedTitle:LOCALIZATION(C_DRAWER_CASHINCENTIVE)
                                                           localizedSubtitle:nil
                                                           icon:[UIApplicationShortcutIcon iconWithTemplateImageName:@"quicklinks_cash_midgrey.png"]
                                                           userInfo:nil];
            [items addObject:cashIncentiveShortcut];
        }
        if([self.profileInfo hasInviteConsumer]) {
            UIApplicationShortcutItem *inviteCustomerShortcut = [[UIApplicationShortcutItem alloc]
                                                           initWithType:[NSString stringWithFormat:@"%@.InviteCustomer",bundleString]
                                                           localizedTitle:LOCALIZATION(C_QL_INVITECUSTOMER)
                                                           localizedSubtitle:nil
                                                           icon:[UIApplicationShortcutIcon iconWithTemplateImageName:@"quicklinks_invite_midgrey.png"]
                                                           userInfo:nil];
            [items addObject:inviteCustomerShortcut];
        }
        if([self.profileInfo hasLoyaltyProgram]) {
            UIApplicationShortcutItem *loyaltyShortcut = [[UIApplicationShortcutItem alloc]
                                                           initWithType:[NSString stringWithFormat:@"%@.Loyalty",bundleString]
                                                           localizedTitle:LOCALIZATION(C_LOYALTY_LOYALTYPROGRAM)
                                                           localizedSubtitle:nil
                                                           icon:[UIApplicationShortcutIcon iconWithTemplateImageName:@"quicklinks_partnersofshell_midgrey.png"]
                                                           userInfo:nil];
            [items addObject:loyaltyShortcut];
        }
        if([self.profileInfo hasWorkshopAcademy]) {
            UIApplicationShortcutItem *sdaSwaShortcut = [[UIApplicationShortcutItem alloc]
                                                           initWithType:[NSString stringWithFormat:@"%@.SWA",bundleString]
                                                           localizedTitle:LOCALIZATION(@"SWA")
                                                           localizedSubtitle:nil
                                                           icon:[UIApplicationShortcutIcon iconWithTemplateImageName:@"quicklinks_swa_midgray.png"]
                                                           userInfo:nil];
            [items addObject:sdaSwaShortcut];
        }
        if([self.profileInfo hasDistributorAcademy]) {
            UIApplicationShortcutItem *sdaSwaShortcut = [[UIApplicationShortcutItem alloc]
                                                           initWithType:[NSString stringWithFormat:@"%@.SDA",bundleString]
                                                           localizedTitle:LOCALIZATION(@"SDA")
                                                           localizedSubtitle:nil
                                                           icon:[UIApplicationShortcutIcon iconWithTemplateImageName:@"quicklinks_swa_midgray.png"]
                                                           userInfo:nil];
            [items addObject:sdaSwaShortcut];
        }
        if([self.profileInfo hasMyRewards] || self.profileInfo == nil) {
            UIApplicationShortcutItem *rewardsShortcut = [[UIApplicationShortcutItem alloc]
                                                           initWithType:[NSString stringWithFormat:@"%@.Rewards",bundleString]
                                                           localizedTitle:LOCALIZATION(C_QL_MYREWARDS)
                                                           localizedSubtitle:nil
                                                           icon:[UIApplicationShortcutIcon iconWithTemplateImageName:@"quicklinks_myrewards_midgrey.png"]
                                                           userInfo:nil];
            [items addObject:rewardsShortcut];
        }
        if([self.profileInfo hasRUPerformance] || [self.profileInfo hasPartnersOfShellClub] || [self.profileInfo hasOnePlusOne] || [self.profileInfo hasDRWorkshopPerformance] || [self.profileInfo hasManageMechanicPerformance] || [self.profileInfo hasMYPerformance]) {
            UIApplicationShortcutItem *analyticsShortcut = [[UIApplicationShortcutItem alloc]
                                                           initWithType:[NSString stringWithFormat:@"%@.Analytics",bundleString]
                                                           localizedTitle:LOCALIZATION(C_SIDEMENU_ANALYTICS)
                                                           localizedSubtitle:nil
                                                           icon:[UIApplicationShortcutIcon iconWithTemplateImageName:@"sideicon_performance_midgrey.png"]
                                                           userInfo:nil];
            [items addObject:analyticsShortcut];
        }

    } else {
        UIApplicationShortcutItem *loginShortCut = [[UIApplicationShortcutItem alloc]
                                                       initWithType:[NSString stringWithFormat:@"%@.Login",bundleString]
                                                       localizedTitle:LOCALIZATION_EN(C_TITLE_LOGIN)
                                                       localizedSubtitle:nil
                                                       icon:[UIApplicationShortcutIcon iconWithType:UIApplicationShortcutIconTypeContact]
                                                       userInfo:nil];
        [items addObject:loginShortCut];
    }
    // add the array to our app
    [UIApplication sharedApplication].shortcutItems = items;
}

-(void) handleShortcutItem:(UIApplicationShortcutItem *)shortcutItem {
    NSString *bundleString = [[NSBundle mainBundle] bundleIdentifier];

    SET_LAUNCH_SDASWA(NO);

    if([shortcutItem.type isEqualToString:[NSString stringWithFormat:@"%@.Login",bundleString]]) {
        [self loadLoginView];
    } else if([shortcutItem.type isEqualToString:[NSString stringWithFormat:@"%@.OnlineOrder",bundleString]]) {
        [self loadManageOrder];
    } else if([shortcutItem.type isEqualToString:[NSString stringWithFormat:@"%@.OilChange",bundleString]]) {
        if(self.profileInfo == nil) {
            self.profileInfo = [profileInfo initWithData:[GET_HOMEDATA objectForKey:@"MyProfile"]];
        }
        if([self.profileInfo hasRegisterProductCode_V2]) {
            [self loadScanProductView];
        } else if([self.profileInfo hasLubeMatch]) {
            [self loadLubeMatch];
        }
    } else if([shortcutItem.type isEqualToString:[NSString stringWithFormat:@"%@.InviteCustomer",bundleString]]) {
        [self loadRegisterCustomerView: @{@"PromoCode" : @"",
                                              @"VehicleNumber": @"",
                                              @"VehicleStateID": @"",
        }];
    } else if([shortcutItem.type isEqualToString:[NSString stringWithFormat:@"%@.SWA",bundleString]] || [shortcutItem.type isEqualToString:[NSString stringWithFormat:@"%@.SDA",bundleString]]) {
        [self loadHomeView];
        SET_LAUNCH_SDASWA(YES);
    } else if([shortcutItem.type isEqualToString:[NSString stringWithFormat:@"%@.Rewards",bundleString]]) {
        [self loadRewardsView];
    } else if([shortcutItem.type isEqualToString:[NSString stringWithFormat:@"%@.Analytics",bundleString]]) {
        if([self.profileInfo hasRUPerformance]) {
            [self loadRUWorkshopPerformance];
        } else if([self.profileInfo hasPartnersOfShellClub]) {
            [self loadRUPartners];
        }
        else {
            [self loadINPerformance];
        }
    } else if([shortcutItem.type isEqualToString:[NSString stringWithFormat:@"%@.Loyalty",bundleString]]) {
        [self loadLoyaltyContract];
    } else if([shortcutItem.type isEqualToString:[NSString stringWithFormat:@"%@.CashIncentive",bundleString]]) {
        [self loadCashIncentiveView];
    }
}

-(void) handlePushNotificationWithReferenceKey:(NSString *) referenceKey referenceValue:(NSString *)referenceValue
{
    if ([referenceKey isEqualToString: BLANK])
    {
        [self loadNotificationsViewfromPushWithId: referenceValue];
    }
    else
    {
        NSArray *kReferenceKeyType = @[@"NL",
                                       @"RW",
                                       @"OD",
                                       @"LE",
                                       @"WS",
                                       @"PF",
                                       @"SC",
                                       @"IAB",
                                       /*@"NA"*/
                                       ];
        
        switch ([kReferenceKeyType indexOfObject: [referenceKey uppercaseString]])
        {
            case kReferenceKey_NOTIFICATIONLIST:
                if ([referenceValue length] > 0)
                {
                    [self loadNotificationsViewfromPushWithId: referenceValue];
                }
                else
                {
                    [self loadNotificationsView];
                }
                break;
            case kReferenceKey_REWARDS:
                if ([[self profileInfo] hasMyRewards]) //proceed if flag = 1
                    [self loadRewardsView];
                else
                    [self checkSessionAndLaunchApp: nil];
                break;
            case kReferenceKey_ORDERS:
                if ([[self profileInfo] hasOrdering_V2])
                    [self loadManageOrder];
                else
                    [self checkSessionAndLaunchApp: nil];
                break;
            case kReferenceKey_LEADS:
                if (GET_PROFILETYPE == kPROFILETYPE_DSR)
                    [self loadManageWorkshopView];
                else
                    [self checkSessionAndLaunchApp: nil];
                break;
            case kReferenceKey_WORKSHOP:
                if (GET_PROFILETYPE == kPROFILETYPE_DSR)
                    [self loadManageWorkshopView];
                else
                    [self checkSessionAndLaunchApp: nil];
                break;
            case kReferenceKey_PERFORMANCE:
                if ([[self profileInfo] hasMYPerformance] || [[self profileInfo] hasMYPoints]) //proceed if flag = 1
//                if ([[self profileInfo] hasWorkshopPerformance]) //proceed if flag = 1
                {
//                    if (GET_PROFILETYPE == kPROFILETYPE_MECHANIC)
//                        [self loadMechPointsView];
//                    else
                        [self loadPerformanceView];
                }
                else
                    [self checkSessionAndLaunchApp: nil];
                break;
            case kReferenceKey_SCANCODE:
                //            if ([[self profileInfo]  hasRegisterProductCode_V2])
                [self checkSessionAndLaunchApp: nil];
                break;
            case kReferenceKey_INAPPBROWSER:
                if ([referenceValue length] > 0)
                {
//                    [mSession loadInAppBrowserFromPushWithId: @"16273" urlString: @"http://shell-dev.ersg-staging.com/services/Pages/SurveyForm.aspx?sid=SV00000002"];11
                    
                    NSArray *referenceValueArray = [referenceValue componentsSeparatedByString: @","];
                    [self loadInAppBrowserFromPushWithId: [referenceValueArray firstObject] urlString: [referenceValueArray lastObject]];
                }
                else
                {
                    [self loadNotificationsView];
                }
                
                break;
            default:
                [self checkSessionAndLaunchApp: nil];
                break;
        }
    }
}

//-(void) checkLookupTable
//{
//    NSLog((GET_LOOKUP_TABLE)? @"GET_LOOKUP_TABLE OK": @"GET_LANGUAGE_TABLE null-----");
//    if ([[NSUserDefaults standardUserDefaults]objectForKey:LOOKUP_KEY])
//    {
//        self.lookupTable = GET_LOOKUP_TABLE;
//        self.languageTable = [self.lookupTable objectForKey: @"LanguageList"];
//    }
//    
//    NSDictionary *requestData;
//    if (!self.lookupTable)
//    {
//        //certain languages will not have default countries
//        //set country code if not set
//        if (!GET_COUNTRY_CODE)
//            SET_COUNTRY_CODE(DEFAULT_COUNTRY_CODE);
//        
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        requestData = @{@"CountryCode":     DEFAULT_COUNTRY_CODE,
//                        @"LanguageCode":    ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])? @"5" : @"1", //use system language
//                        @"LookUpVersion":   @"1501065350"};
//        
//    }
//    else
//    {
//        requestData = @{@"CountryCode": GET_COUNTRY_CODE,
//                        @"LanguageCode": @([kAvailableLanguages indexOfObject: GET_LOCALIZATION]).stringValue,
////                        @"LanguageCode": [self.lookupTable objectForKey: @"LanguageCode"],
//                        @"LookUpVersion": [self.lookupTable objectForKey: @"LookUpVersion"]};
//    }
//
//    [[WebServiceManager sharedInstance] requestLookupTable: requestData
//                                                      view:nil];
//}
//
//-(void) reloadLookupTable:(UIView *)view
//{
//    NSString *languageCodeString  = [NSString stringWithFormat: @"%lu", (unsigned long)[kAvailableLanguages indexOfObject: GET_LOCALIZATION]];
//    NSDictionary *requestData = @{@"CountryCode": GET_COUNTRY_CODE,
//                                  @"LanguageCode": languageCodeString,
//                                  @"LookUpVersion": [self.lookupTable objectForKey: @"LookUpVersion"] ? [self.lookupTable objectForKey: @"LookUpVersion"] : @"1501065350"};
//    [[WebServiceManager sharedInstance] requestLookupTable: requestData
//                                                      view:view];
//}


#pragma mark View Controller shared functions
-(UIViewController *) loadVC:(NSString *)identifier storyboard:(UIStoryboard *) storyboard
{
    [UpdateHUD removeSpotifyHUD:CURRENT_VIEW];
    
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:identifier];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    mainWindow.rootViewController = nav;
    [mainWindow makeKeyAndVisible];
    CATransition *animation = [CATransition animation];
    animation.type = kCATransitionFade;
    animation.duration = 0.4;
    [mainWindow.layer addAnimation:animation forKey:nil];
    
    return vc;
}

- (void)pushVC: (UIViewController *) parentController newVC: (UIViewController *) newVC
{
    [UpdateHUD removeSpotifyHUD:CURRENT_VIEW];
    
    CATransition *animation = [CATransition animation];
    animation.type = kCATransitionFade;
    animation.duration = 0.4;
    [parentController.navigationController.view.layer addAnimation:animation forKey:nil];
    
    [parentController.navigationController pushViewController: newVC animated:NO];
}

-(UIViewController *) loadVCWithSidePanel:(NSString *)identifier storyboard:(UIStoryboard *) storyboard vc:(UIViewController *) vc
{
    [UpdateHUD removeSpotifyHUD:CURRENT_VIEW];
    
    if ([identifier isEqualToString: VIEW_PROFILE]) //disable iqkeyboard for selected views
    {
        [[IQKeyboardManager sharedManager] setEnable: NO];
    }
    else
    {
        [[IQKeyboardManager sharedManager] setEnable: YES];
    }
    
    if (kIsRightToLeft) {
        if(!sidePanel.rightPanel) {
            [self reloadSidePanel];
        }
    } else {
        if(!sidePanel.leftPanel) {
            //reload panel again.
            //panel should only be released on logout
            [self reloadSidePanel];
        }
    }
//    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:identifier];
    sidePanel.centerPanel = [[UINavigationController alloc] initWithRootViewController:vc];
    mainWindow.rootViewController = sidePanel;
    [mainWindow makeKeyAndVisible];
    CATransition *animation = [CATransition animation];
    animation.type = kCATransitionFade;
    animation.duration = 0.4;
    [mainWindow.layer addAnimation:animation forKey:nil];
    
//    SideMenuViewController *sideMenuVC = [[[sidePanel leftPanel] childViewControllers] lastObject];
//    [sideMenuVC selectQuickLinkMenu: identifier];
    //see reload side panel for hierachy.
    //sidepanel.leftPanel = navController
    //leftPanel.child[0] = SideMenuVC
    
    //    [sideMenuVC updateProfile]; //update to ProfileData
    [[NSNotificationCenter defaultCenter] postNotificationName: @"UpdateProfileInfo" object: nil];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"Unread Icon Changed" object: nil];
    
    return vc;
}

-(UIViewController *) loadVCWithSidePanel:(NSString *)identifier storyboard:(UIStoryboard *) storyboard
{
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:identifier];

    [self loadVCWithSidePanel:identifier storyboard:storyboard vc: vc];
    return vc;
}

-(void) loadVCWithSidePanelWithVc:(UIViewController *) vc
{
    [self loadVCWithSidePanel: @"" storyboard: nil vc: vc];
}

-(void) reloadSidePanel
{
    CGRect bounds=[[UIScreen mainScreen] bounds];
    sidePanel.shouldDelegateAutorotateToVisiblePanel = NO;
    sidePanel.bouncePercentage = 0.0;
    
    SideMenuViewController *sideMenuVC = [SideMenuViewController new];
    sideMenuVC.tableviewOffset = 57;
    
    if (kIsRightToLeft) {
        sidePanel.leftPanel = nil;
        sidePanel.rightFixedWidth = bounds.size.width - 57;
        sidePanel.rightPanel = [[UINavigationController alloc] initWithRootViewController:sideMenuVC];
    } else {
        sidePanel.rightPanel = nil;
        sidePanel.leftFixedWidth = bounds.size.width - 57;
        sidePanel.leftPanel = [[UINavigationController alloc] initWithRootViewController:sideMenuVC];
    }
}

#pragma mark - View Controllers
#pragma mark - Pre Login
- (void)loadSplash:(BOOL) isAfterLogin
{
    SplashScreenVC *splashVc = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier:VIEW_SPLASH];
    [splashVc view];
    splashVc.lblVersion.hidden = isAfterLogin;
    
    mainWindow.rootViewController = splashVc;
    [mainWindow makeKeyAndVisible];
}

- (void)loadLoginView
{
    sidePanel.leftPanel = nil;
    sidePanel.centerPanel = nil;
    sidePanel.rightPanel = nil;

    if(!GET_ISLOCATIONDETERMINED) {
        [self locationUpdate];
    }
    [self loadVC:VIEW_LOGIN storyboard:STORYBOARD_MAIN];   //LoginViewController
}

- (void)pushForgetPasswordView: (UIViewController*) parentController
{
    UIViewController *newVc = [STORYBOARD_PASSWORD instantiateViewControllerWithIdentifier:VIEW_FORGETPASSWORD]; //ForgetPasswordViewController
    [self pushVC: parentController newVC: newVc];
}

- (void)pushChangePasswordView: (NSString *)mobNum showBackBtn:(BOOL)showBackBtn vc: (UIViewController*) parentController
{
    ChangePasswordViewController *newVc = [STORYBOARD_PASSWORD instantiateViewControllerWithIdentifier:VIEW_CHANGEPASSWORD]; //ChangePasswordViewController
    newVc.mobNum = mobNum;
    newVc.showBackBtn = showBackBtn;
    
    [self pushVC: parentController newVC: newVc];
}
/**
 * Registration storyboard
 * Push from Login screen
 **/
-(void) pushMobileAuthView: (UIViewController*) parentController
{
    UIViewController *newvc = [STORYBOARD_REGISTRATION instantiateViewControllerWithIdentifier: VIEW_MOBILEAUTH];
    
    [self pushVC: parentController  newVC:newvc];
}

/**
 * Registration storyboard
 * Push from Mobile Auth screen.
 * requires <Mobile Number>, <Dialing code>, <Selected registration type> (Trade or Mechanic)
 **/
- (void) pushOTPView: (UIViewController*) parentController mobNum:(NSString* ) mobNum countryCode:(NSString *)countryCode regType:(NSInteger) regType;
{
    OTPViewController *otpView = (OTPViewController *) [STORYBOARD_REGISTRATION instantiateViewControllerWithIdentifier: VIEW_OTP]; //OTPViewController
    otpView.mobNum = mobNum;
    otpView.countryCode = countryCode;
    otpView.regType = regType;
    
    [self pushVC: parentController  newVC:otpView];
}

/**
 * Registration storyboard
 * Push from OTP screen.
 * requires <Mobile Number>, <Dialing Code>
 **/
- (void)pushTradeRegistrationView: (UIViewController*) parentController mobNum: (NSString* ) mobNum countryCode:(NSString *)countryCode
{
    TradeRegistrationViewController *regView = (TradeRegistrationViewController *)[STORYBOARD_REGISTRATION instantiateViewControllerWithIdentifier: VIEW_TRADEREGISTRATION]; //TradeRegistrationViewController
    regView.mobNum = mobNum;
    regView.countryCode = countryCode;
    
    [self pushVC: parentController  newVC:regView];
}

/**
 * Registration storyboard
 * Push from Trade Registration screeb
 * requires <Member> class from Trade Registration.
 **/
- (void)pushTradeRegistrationTwoView: (UIViewController*) parentController memberInfo: (Member *) memberInfo
{
    TradeRegistrationTwoViewController *regView = (TradeRegistrationTwoViewController *) [STORYBOARD_REGISTRATION instantiateViewControllerWithIdentifier: VIEW_TRADEREGISTRATIONTWO]; //TradeRegistrationViewController
    regView.memberInfo = memberInfo;
    
    [self pushVC: parentController  newVC:regView];
}

/**
 * Registration storyboard
 * Push from registration screens with working hour field
 **/
- (void)pushWorkingHoursView: (UIViewController*) parentController workingHoursData:(NSDictionary *) data
{
    WorkingHoursViewController *workingHoursView = (WorkingHoursViewController *) [STORYBOARD_REGISTRATION instantiateViewControllerWithIdentifier: VIEW_WORKINGHOURS]; //WorkingHoursViewController
    
    workingHoursView.workingHoursDict = data;
    workingHoursView.delegate = parentController;
    
    [self pushVC: parentController newVC:workingHoursView];
}

/**
 * Registration storyboard
 * Push from OTP Screen
 * requires <Mobile Number> & <Dialing Code> fields
 **/
- (void)pushMechanicRegistrationView: (UIViewController*) parentController mobNum: (NSString* ) mobNum countryCode:(NSString *)countryCode
{
    MechanicRegistrationViewController *regView = (MechanicRegistrationViewController *)[STORYBOARD_REGISTRATION instantiateViewControllerWithIdentifier: VIEW_MECHANICREGISTRATION]; //TradeRegistrationViewController
    regView.mobNum = mobNum;
    regView.countryCode = countryCode;
    
    [self pushVC: parentController  newVC:regView];
}

/**
 * Load from Side screen
 **/
- (void)loadContactView
{
    [self loadVCWithSidePanel: VIEW_CONTACTUS storyboard:STORYBOARD_MAIN]; //ContactUsViewController
}

/**
 * Push from Login screen
 **/
-(void) pushContactView: (UIViewController*) parentController
{
    UIViewController *newvc = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_CONTACTUS];
    
    [self pushVC: parentController  newVC:newvc];
}

-(void) pushPhotoTakingViewControllerWithParent: (UIViewController<PhotoTakingVCDelegate> *) parentController
{
    PhotoTakingViewController *newvc = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_PHOTO_TAKING];
    newvc.delegate = parentController;
    
    [self pushVC:parentController newVC:newvc];
}

//-(void) pushPhotoTakingViewControllerWithParent: (UIViewController<PhotoTakingVCDelegate> *) parentController
//                                        message:(NSString *) message
//{
//    PhotoTakingViewController *newvc = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_PHOTO_TAKING];
//    newvc.delegate = parentController;
//    newvc.message = message;
//
//    [self pushVC:parentController newVC:newvc];
//}

-(void) pushPanCardPhotoTakingWithParent: (UIViewController<PhotoTakingVCDelegate> *) parentController
                                        message:(NSString *) message
{
    [self pushPanCardPhotoTakingWithParent: parentController delegateVc:parentController message:message key: @""];
}

-(void) pushPanCardPhotoTakingWithParent: (UIViewController <PhotoTakingVCDelegate> *) parentController
                              delegateVc: (UIViewController <PhotoTakingVCDelegate> *) delegateVc
                                 message: (NSString *) message
                                     key:(NSString *)key
{
    PhotoTakingViewController *newvc = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_PHOTO_TAKING];
    newvc.keyString = key;
    newvc.delegate = delegateVc;
    newvc.message = message;
    newvc.showBorderOverlay = YES;
    newvc.allowRetake = YES;
    
    [self pushVC:parentController newVC:newvc];
}
-(void) pushCarPlateScanning: (UIViewController<PhotoTakingVCDelegate> *) parentController
{
    PhotoTakingViewController *newvc = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_PHOTO_TAKING];
    newvc.delegate = parentController;
    newvc.showBorderOverlay = NO;
    newvc.allowRetake = NO;
    
    [self pushVC:parentController newVC:newvc];
}

-(void) pushWalkthroughView: (UIViewController*) parentController
                   WithType: (enum WalkthroughType) type
{
    WalkthroughViewController* newvc = [STORYBOARD_RU_WALKTHROUGH instantiateViewControllerWithIdentifier: VIEW_WALKTHROUGH];
    [newvc setupWalkthroughArray:[WalkthroughHelper getArrayForType:type] withType:type];
    [self pushVC:parentController newVC:newvc];
}

-(void) loadWalkthroughView :(enum WalkthroughType) type {
    WalkthroughViewController* newvc = [STORYBOARD_RU_WALKTHROUGH instantiateViewControllerWithIdentifier: VIEW_WALKTHROUGH];
    [newvc setupWalkthroughArray:[WalkthroughHelper getArrayForType:type] withType:type];
    [self loadVCWithSidePanel:VIEW_WALKTHROUGH storyboard:STORYBOARD_RU_WALKTHROUGH vc:newvc];
}

-(void) popVC:(UIViewController *)vc {
    [vc.navigationController popViewControllerAnimated:true];
}

#pragma mark - Post Login
- (void)loadHomeView
{
//    [self loadVCWithSidePanel: VIEW_HOME storyboard:STORYBOARD_MAIN]; //HomeViewController
    [self loadVCWithSidePanel: VIEW_HOME_NEW storyboard:STORYBOARD_MAINV2];
}



-(void) pushGalleryView: (NSString*) tradeID
             isApproved:(BOOL) isApproved
                     vc:(UIViewController*) parentController
               delegate:(UIViewController*) delegate
{
    GalleryUploadViewController *newvc = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_GAllERY];
    if (!tradeID)
        newvc.delegate = delegate; //delayed upload, upload at delegate level
    else
        newvc.tradeID = tradeID; //to allow direct upload
    
    newvc.isApproved = isApproved;
    
    [self pushVC: parentController  newVC:newvc];
}




#pragma mark Workshop
- (void)loadManageWorkshopView
{
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        if(![[mSession profileInfo] hasAddNewWorkshop])
            [self loadVCWithSidePanel: VIEW_WORKSHOPLIST storyboard:STORYBOARD_MANAGEWORKSHOP];
        else
             [self loadVCWithSidePanel: VIEW_MANAGE storyboard:STORYBOARD_MANAGEWORKSHOP];
    } else {
        if([[mSession profileInfo] hasPendingWorkshopList] || [[mSession profileInfo] hasAddNewWorkshop]) {
            [self loadVCWithSidePanel: VIEW_MANAGE storyboard:STORYBOARD_MANAGEWORKSHOP]; //ManageWorkshopViewController
        } else {
            [self loadVCWithSidePanel: VIEW_WORKSHOPLIST storyboard:STORYBOARD_MANAGEWORKSHOP];
        }
    }
}

- (void)loadWorkshopDetailsTabView
{
    [self loadTabBarVCWithSidePanel: VIEW_WORKSHOPDETAILS_TAB storyboard:STORYBOARD_MANAGEWORKSHOP]; //WorkshopDetailsTabViewController
}

-(void) pushWorkshopDetailsTabView: (UIViewController *) parentController
            workshopDetailsData:(NSDictionary *) workshopDetailsData
{
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        Member *memberInfo = [[Member alloc] initWithData: [workshopDetailsData objectForKey: @"REG_Member"]];
        
        //PENDING workshop Settings (Pending contains only lead ID)
        if (ISNULL(memberInfo.tradeID) || [memberInfo.tradeID isEqualToString: @""])
        {
            
            PendingWorkshopDetails_NewViewController *newvc = (PendingWorkshopDetails_NewViewController *)[STORYBOARD_MANAGEWORKSHOP instantiateViewControllerWithIdentifier: VIEW_PENDINGWORKSHOPDETAILS_NEW]; //WorkshopDetailsTabViewController
            newvc.workshopData = workshopDetailsData;
            [self pushVC: parentController  newVC:newvc];
        }
        //change to new approved workshop list later
        else
        {
            WorkshopDetailsTabViewController *newvc = (WorkshopDetailsTabViewController *)[STORYBOARD_MANAGEWORKSHOP instantiateViewControllerWithIdentifier: VIEW_WORKSHOPDETAILS_TAB]; //WorkshopDetailsTabViewController
            newvc.workshopDetailsData = workshopDetailsData;
            [self pushVC: parentController  newVC:newvc];
        }
    }
    else
    {
        WorkshopDetailsTabViewController *newvc = (WorkshopDetailsTabViewController *)[STORYBOARD_MANAGEWORKSHOP instantiateViewControllerWithIdentifier: VIEW_WORKSHOPDETAILS_TAB]; //WorkshopDetailsTabViewController
        newvc.workshopDetailsData = workshopDetailsData;
        [self pushVC: parentController  newVC:newvc];
    }
}

-(void) pushINWorkshopAddNewView: (UIViewController*) parentController
{
    IN_WorkshopAddNewViewController* newvc = [STORYBOARD_MANAGEWORKSHOP instantiateViewControllerWithIdentifier: VIEW_INWORKSHOPADDNEW];
    [self pushVC:parentController newVC:newvc];
}

#pragma mark SpiraxAndGadus

- (void)loadSpiraxWorkshopView
{
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        if(![[mSession profileInfo] hasAddNewWorkshop])
            [self loadVCWithSidePanel: VIEW_SPIRAXWORKSHOPLIST storyboard:STORYBOARD_SPIRAXANDGADUS];
        else
             [self loadVCWithSidePanel: VIEW_SPIRAX storyboard:STORYBOARD_SPIRAXANDGADUS];
    } else {
        if([[mSession profileInfo] hasPendingWorkshopList] || [[mSession profileInfo] hasAddNewWorkshop]) {
            [self loadVCWithSidePanel: VIEW_SPIRAX storyboard:STORYBOARD_SPIRAXANDGADUS]; //ManageWorkshopViewController
        } else {
            [self loadVCWithSidePanel: VIEW_SPIRAXWORKSHOPLIST storyboard:STORYBOARD_SPIRAXANDGADUS];
        }
    }
}

- (void)loadSpiraxWorkshopDetailsTabView
{
    [self loadTabBarVCWithSidePanel: VIEW_WORKSHOPDETAILS_TAB storyboard:STORYBOARD_SPIRAXANDGADUS]; //WorkshopDetailsTabViewController
}

-(void) pushSpiraxWorkshopDetailsTabView: (UIViewController *) parentController
            workshopDetailsData:(NSDictionary *) workshopDetailsData
{
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        Member *memberInfo = [[Member alloc] initWithData: [workshopDetailsData objectForKey: @"REG_Member"]];

        //PENDING workshop Settings (Pending contains only lead ID)
        if (ISNULL(memberInfo.tradeID) || [memberInfo.tradeID isEqualToString: @""])
        {

            PendingWorkshopDetails_NewViewController *newvc = (PendingWorkshopDetails_NewViewController *)[STORYBOARD_SPIRAXANDGADUS instantiateViewControllerWithIdentifier: VIEW_PENDINGWORKSHOPDETAILS_NEW]; //WorkshopDetailsTabViewController
            newvc.workshopData = workshopDetailsData;
            [self pushVC: parentController  newVC:newvc];
        }
        //change to new approved workshop list later
        else
        {
            WorkshopDetailsTabViewController *newvc = (WorkshopDetailsTabViewController *)[STORYBOARD_SPIRAXANDGADUS instantiateViewControllerWithIdentifier: VIEW_WORKSHOPDETAILS_TAB]; //WorkshopDetailsTabViewController
            newvc.workshopDetailsData = workshopDetailsData;
            [self pushVC: parentController  newVC:newvc];
        }
    }
    else
    {
        WorkshopDetailsTabViewController *newvc = (WorkshopDetailsTabViewController *)[STORYBOARD_SPIRAXANDGADUS instantiateViewControllerWithIdentifier: VIEW_WORKSHOPDETAILS_TAB]; //WorkshopDetailsTabViewController
        newvc.workshopDetailsData = workshopDetailsData;
        [self pushVC: parentController  newVC:newvc];
    }
}

-(void) pushINSpiraxWorkshopAddNewView: (UIViewController*) parentController
{
    IN_WorkshopAddNewViewController* newvc = [STORYBOARD_SPIRAXANDGADUS instantiateViewControllerWithIdentifier: VIEW_INWORKSHOPADDNEW];
    [self pushVC:parentController newVC:newvc];
}

#pragma mark Performance
- (void)loadPerformanceView
{
    
    if (self.profileInfo.hasMYPerformance && self.profileInfo.hasMYPoints){
        [self loadTabBarVCWithSidePanel: VIEW_PERFORMANCE_TAB storyboard:STORYBOARD_PERFORMANCE]; //PerformanceTabViewController
    }
    else if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND] && self.profileInfo.hasMYPerformance) {
        [self loadVCWithSidePanel:VIEW_TH_MYPERFORMANCE storyboard:STORYBOARD_THRetail];
    }
       
    else if (self.profileInfo.hasMYPerformance)
    {
        if (GET_PROFILETYPE == kPROFILETYPE_DSR)
        {
            //DSR (My performance is DSR performance
            [self loadVCWithSidePanel:VIEW_MYPERFORMANCE storyboard:STORYBOARD_PERFORMANCE];
        }
        else
        {
            //owner/mech (workshopperformanceview)
            [self loadVCWithSidePanel:VIEW_PERFORMANCEWORKSHOP storyboard:STORYBOARD_PERFORMANCE];
        }
    }
    else if (self.profileInfo.hasMYPoints)
    {
    //Owner/Mech
    [self loadVCWithSidePanel:VIEW_PERFORMANCEPOINTS storyboard:STORYBOARD_PERFORMANCE];

    }
}

//- (void)loadRUPerformanceView
//{
//    
//}

- (void)loadMechPointsView
{
    [self loadTabBarVCWithSidePanel: VIEW_PERFORMANCEPOINTS storyboard:STORYBOARD_PERFORMANCE]; //PerformanceTabViewController
}


#pragma mark Profile
-(void) loadProfileTabView
{
    [self loadVCWithSidePanel: VIEW_PROFILE storyboard:STORYBOARD_PROFILE];
}

- (void)loadMyProfileView
{
    [self loadVCWithSidePanel: VIEW_MYPROFILE_NEW storyboard:STORYBOARD_PROFILE];
}

-(void)loadMoreInfoInProfile
{
    ProfileMasterViewController *vc = (ProfileMasterViewController * ) [self loadVCWithSidePanel:VIEW_PROFILE storyboard:STORYBOARD_PROFILE];
    [vc view];
    vc.selectedIndex = 1;
}

#pragma mark ???


- (void)loadPromotionsView
{
    [self reloadSidePanel];
    [self loadVCWithSidePanel: VIEW_PROMOTION storyboard:STORYBOARD_PROMOCODE]; //PromotionViewController
}

#pragma mark Customer Redemption (3+1)
- (void)loadPromoCodeView
{
    [self loadVCWithSidePanel: VIEW_PROMOTIONCODE storyboard:STORYBOARD_PROMOCODE]; //PromoCodeViewController
}

- (void)pushPromoCodeView:(UIViewController*) parentController
{
    UIViewController *newvc = [STORYBOARD_PROMOCODE instantiateViewControllerWithIdentifier: VIEW_PROMOTIONMASTER]; //PromoCodeViewController
    [self pushVC: parentController  newVC:newvc];
}

- (void)pushOfferDetailsView:(UIViewController*) parentController offerDict:(NSDictionary *)offerDict
{
    OfferDetailsViewController *newvc = [STORYBOARD_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_WORKSHOPOFFER_OFFERDETAILVIEW]; //OfferDetailViewController
    newvc.offerDict = offerDict;
    [self pushVC: parentController  newVC:newvc];
}



- (void)pushOfferConfirmationView:(UIViewController*) parentController dict:(NSDictionary *)dict
{
    OfferConfirmationViewController *newvc = [STORYBOARD_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_WORKSHOPOFFER_OFFERCONFIRMATIONVIEW]; //OfferConfirmationViewControllerb
    newvc.submitDict = [[NSMutableDictionary alloc]initWithDictionary:dict];
    [self pushVC: parentController  newVC:newvc];
}

//- (void)pushCalenderView:(UIViewController*) parentController
//{
//    UIViewController *newvc = [STORYBOARD_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_WORKSHOPOFFER_CALENDERVIEW]; //Calendar view
//    [self pushVC: parentController  newVC:newvc];
//}

- (void)loadPromoCodeMasterView
{
    [self loadVCWithSidePanel: VIEW_PROMOTIONMASTER storyboard:STORYBOARD_PROMOCODE]; //PromoCodeMasterViewController
}

-(void)pushPromoCodeResultView:(NSDictionary *)resultsDict vc:(UIViewController*) parentController

{
    PromoCodeResultViewController *newVc = [STORYBOARD_PROMOCODE instantiateViewControllerWithIdentifier: VIEW_PROMOTIONRESULT]; //PromoCodeResultViewController
    newVc.resultsDict = resultsDict;
    [self pushVC: parentController  newVC:newVc];
}

- (void)loadOrderView
{
    [self loadVCWithSidePanel: VIEW_ORDERSYSTEM storyboard:STORYBOARD_MAIN]; //OrderingSystemViewController
}

- (void)loadProductListView
{
    [self loadVCWithSidePanel: VIEW_PRODUCTLIST storyboard:STORYBOARD_MAIN]; //ProductListViewController
}

- (void)loadManageOrder
{
     [self loadVCWithSidePanel: VIEW_MANAGEORDER storyboard:STORYBOARD_MANAGEORDERS]; //ManageOrdersViewController
}

- (void)loadParticipatingProduct
{

    if ([self.profileInfo hasParticipatingAllProducts] && [self.profileInfo hasParticipatingShareProducts])
    {
         if([[mSession profileInfo] isParticipatingProductsOpt3])
            [self loadVCWithSidePanel:VIEW_PARTICIPATINGPRODUCTS_V3_TAB storyboard:STORYBOARD_MAINV2];
    
        else
            [self loadVCWithSidePanel: VIEW_PARTICIPATINGPRODUCT storyboard:STORYBOARD_WEBVIEW]; //ParticipatingWebViewController
    }
    else if ([self.profileInfo hasParticipatingAllProducts])
    {

            if([[mSession profileInfo] isParticipatingProductsOpt3])
            [self loadVCWithSidePanel:VIEW_ALLPARTICIPATINGPRODUCTS_V3 storyboard:STORYBOARD_MAINV2];
        else
            [self loadVCWithSidePanel: VIEW_PARTICIPATEPRODUCT storyboard:STORYBOARD_WEBVIEW];
    }
    else if([self.profileInfo hasParticipatingShareProducts])
    {
        if([[mSession profileInfo] isParticipatingProductsOpt3])
                   [self loadVCWithSidePanel:VIEW_SHAREPARTICIPATINGPRODUCTS_V3 storyboard:STORYBOARD_MAINV2];
        else
            [self loadVCWithSidePanel: VIEW_OTHERPRODUCT storyboard:STORYBOARD_WEBVIEW];
    }

}

- (void)pushParticipatingProductsWith:(NSArray *) productArray onVc:(UIViewController *) parentController {

    ProductsTableViewController *productsTableView = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier:VIEW_PRODUCTTABLEVIEW];
    productsTableView.fetchShareProducts = NO;
    productsTableView.selectedProductArray = productArray;

    [self pushVC:parentController newVC:productsTableView];

}

- (void)loadScanProductView
{
    [self loadVCWithSidePanel: VIEW_SCANPRODUCTCODE storyboard:STORYBOARD_MAIN]; //ScanProductCodeViewController
}

- (void)pushScanProductView:(NSString *)vehicleNumber previousState:(NSString*) previousState vc:(UIViewController*) parentController
{
    ScanProductCodeViewController *newvc = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_SCANPRODUCTCODE]; //ScanProductCodeViewController
    
    newvc.previousVehicleNumber = vehicleNumber;
    newvc.previousState = previousState;
    
    [self pushVC: parentController  newVC:newvc];
}

- (void)pushPendingCodeView: (UIViewController*) parentController
{
    PendingCodeViewController *newvc = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_PENDINGCODE]; //PendingCodeViewController
    
    newvc.isScanBottle = NO;
    newvc.forConsumer = NO;
    
    [self pushVC: parentController  newVC:newvc];
}

- (void)pushPendingCodeViewForScanBottle:(BOOL)isScanBottle forConsumer:(BOOL)forConsumer onVc:(UIViewController *) parentController {
    PendingCodeViewController *newvc = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_PENDINGCODE]; //PendingCodeViewController
    
    newvc.isScanBottle = isScanBottle;
    newvc.forConsumer = forConsumer;
    
    [self pushVC: parentController  newVC:newvc];
}

- (void)loadSearchVehicleViewWithDecal: (NSString *) decalString
{
    SearchVehicleViewController *vc = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier:VIEW_SEARCHVEHICLE];
    
    vc.decalString = decalString;
    
    //identifier for special cases
    [self loadVCWithSidePanel:VIEW_SEARCHVEHICLE storyboard:STORYBOARD_MAIN vc:vc]; //ScanProductCodeViewController
    
//    [vc tagDecal];
    [vc performSelector: @selector(tagDecal) withObject:nil afterDelay:0.4f];
}

- (void)loadDecalView
{
    [self loadVCWithSidePanel: VIEW_DECALMAIN storyboard:STORYBOARD_SCANCODE]; //DecalMainView
}

- (void)pushDecalResultsWithDictionary:(NSDictionary *)resultsDict parentController:(UIViewController*) parentController
{
    ScanCode_DecalResultViewController *newvc = [STORYBOARD_SCANCODE instantiateViewControllerWithIdentifier: VIEW_DECALRESULT]; //ScanCode_DecalResultViewController
    
    newvc.resultsDict = resultsDict;
    
    [self pushVC: parentController  newVC:newvc];
}

- (void)loadSearchVehicleView
{
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
        [self loadVCWithSidePanel: VIEW_RU_SEARCHVEHICLE storyboard:STORYBOARD_RUSSIA]; //russia
    else if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDIA] ||
            [GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_HONGKONG] ||
            [GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_VIETNAM] ||
            [GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_SAUDI])
        [self loadVCWithSidePanel: VIEW_IN_SEARCHVEHICLE storyboard:STORYBBOARD_IN];
    else if (GET_ISADVANCE)
        [self loadVCWithSidePanel: VIEW_IN_SEARCHVEHICLE storyboard:STORYBBOARD_IN];
    else
        [self loadVCWithSidePanel: VIEW_SEARCHVEHICLE storyboard:STORYBOARD_MAIN]; //ScanProductCodeViewController
}

-(void)loadCashIncentiveView
{
    [self loadVCWithSidePanel:VIEW_IN_CASHINCENTIVE storyboard:STORYBBOARD_IN];
}


- (void)loadFAQView
{
    [self loadVCWithSidePanel: VIEW_FAQ storyboard:STORYBOARD_MAIN]; //FAQViewController
}

- (void) pushGenericWebViewWith:(NSString *) titleToBeDisplayed andURL:(NSString *) urlToBeDisplayed onVC:(UIViewController*) parentVC {
    GenericOnlyWebViewController *newWebView = [STORYBOARD_WEBVIEW instantiateViewControllerWithIdentifier:VIEW_WEBVIEW_GENERIC];
    newWebView.titleToBeDisplayed = titleToBeDisplayed;
    newWebView.urlToBeDisplayed = urlToBeDisplayed;
    newWebView.isHtmlString = NO;
    [self pushVC:parentVC newVC:newWebView];
}
- (void) pushGenericWebViewWith:(NSString *) titleToBeDisplayed andHtmlString:(NSString *) htmlString onVC:(UIViewController*) parentVC {
    GenericOnlyWebViewController *newWebView = [STORYBOARD_WEBVIEW instantiateViewControllerWithIdentifier:VIEW_WEBVIEW_GENERIC];
    newWebView.titleToBeDisplayed = titleToBeDisplayed;
    newWebView.urlToBeDisplayed = htmlString;
    newWebView.isHtmlString = YES;
    [self pushVC:parentVC newVC:newWebView];
}

//- (void)pushNotificationsDetailView:(NSString*)pushId vc:(UIViewController*) parentController
- (void)pushNotificationsDetailView:(NSDictionary*)msgData vc:(UIViewController*) parentController
{
    NotificationDetailViewController *newvc = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_NOTIFICATIONDETAIL]; //NotificationDetailViewController
//    newvc.pushId = pushId;
    newvc.msgData = @{@"PushID" : msgData};
    [self pushVC: parentController  newVC:newvc];
}

- (void)pushNotificationEditWithListArray:(NSMutableArray *)listArray vc:(UIViewController*) parentController
{
    NotificationDeleteViewController *newvc = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_NOTIFICATIONDELETEVIEW]; //nofic delete
    newvc.notificationArray = [[NSMutableArray alloc] initWithArray: listArray];
    [self pushVC: parentController  newVC:newvc];
}

- (void)pushInAppBrowserWithAddress: (NSString *) address vc:(UIViewController*) parentController
{
    InAppBrowserViewController *newvc = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_INAPPBROWSER];
    newvc.urlAddressString = address;

    [self pushVC: parentController  newVC:newvc];
}



- (void)loadWorkshopOfferView
{
//    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
//    {
        UIViewController *parentVc = [self loadVCWithSidePanel: VIEW_RU_WORKSHOPOFFER storyboard:STORYBOARD_RU_WORKSHOPOFFER]; //WorkshopOffer
        
        //walkthrough available
      if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_RUSSIA]) {
        NSMutableDictionary *walkthroughData = GET_WALKTHROUGH_TABLE;
        if (![walkthroughData objectForKey: VIEW_RU_WORKSHOPOFFER]) //if key exists, don't show walk through
        {
            //show walkthrough
            UIViewController *vc = [STORYBOARD_RU_WALKTHROUGH instantiateViewControllerWithIdentifier: VIEW_RU_WALKTHROUGH];
            
            [self pushVC: parentVc newVC: vc];
        } 
    }
//    }
    
}


- (void)pushManageMechanic:(NSString *)tradeID
                        vc:(UIViewController*) parentController
{
    ManageMechanicViewController *newvc = [STORYBOARD_MANAGEMECHANIC instantiateViewControllerWithIdentifier: VIEW_MANAGEMECHANIC]; //ManageMechanicViewController
    newvc.tradeID = tradeID;
    [self pushVC: parentController  newVC:newvc];
}
- (void)pushMechanicDetailsView:(NSString *)tradeID
                 registrationID:(NSString *)registrationID
                workshopTradeID:(NSString *)workshopTradeID
                             vc:(UIViewController*) parentController
{
//    if (IS_COUNTRY(COUNTRYCODE_INDONESIA))
//    {
        MechanicDetails_NewViewController *newvc = [STORYBOARD_MANAGEMECHANIC instantiateViewControllerWithIdentifier: VIEW_MECHANICDETAILS_NEW]; //MechanicDetailViewController
        newvc.tradeID = tradeID;
        newvc.registrationID = registrationID;
        newvc.workshopTradeID = workshopTradeID;
        [self pushVC: parentController  newVC:newvc];
//    }
//    else
//    {
//        MechanicDetailsViewController *newvc = [STORYBOARD_MANAGEMECHANIC instantiateViewControllerWithIdentifier: VIEW_MECHANICDETAILS]; //MechanicDetailViewController
//        newvc.tradeID = tradeID;
//        newvc.registrationID = registrationID;
//        newvc.workshopTradeID = workshopTradeID;
//        [self pushVC: parentController  newVC:newvc];
//    }
}

- (void)pushStaffDetailsView:(NSString *)tradeID vc:(UIViewController*) parentController {
    ManageStaffDetailViewController *newVC = [STORYBOARD_THRetail instantiateViewControllerWithIdentifier: VIEW_STAFFDETAILS];
    newVC.tradeID = tradeID;
    [self pushVC:parentController newVC:newVC];
}

- (void)loadManageMechanic
{
    [self loadVCWithSidePanel: VIEW_MANAGEMECHANIC storyboard:STORYBOARD_MANAGEMECHANIC]; //ManageMechanicViewController
}


- (void)loadOtherView
{
    [self loadVCWithSidePanel: VIEW_OTHERPRODUCT storyboard:STORYBOARD_MAIN]; //OtherWebViewController
}
- (void)loadAddView
{
    [self loadVCWithSidePanel: VIEW_ADDORDER storyboard:STORYBOARD_MAIN]; //AddOrderWebViewController
}
- (void)loadViewOrderView
{
    [self loadVCWithSidePanel: VIEW_VIEWORDER storyboard:STORYBOARD_MAIN]; //ViewOrderWebViewController
}
- (void)loadParticipatingView
{
    [self loadVCWithSidePanel: VIEW_PARTICIPATEPRODUCT storyboard:STORYBOARD_MAIN]; //ParticipatingWebViewController
}
- (void)loadNotificationsView
{
    [self loadVCWithSidePanel: VIEW_NOTIFICATIONS storyboard:STORYBOARD_MAIN];
}

- (void)pushPendingOrderView: (UIViewController*) parentController
{
    UIViewController *newvc = [STORYBOARD_MANAGEORDERS instantiateViewControllerWithIdentifier: VIEW_PENDINGORDER]; //PendingOrderViewController
    
    [self pushVC: parentController  newVC:newvc];
}

- (void)loadNotificationsViewfromPushWithId: (NSString *) pushId
{
    UIViewController *vc = [self loadVCWithSidePanel: VIEW_NOTIFICATIONS storyboard:STORYBOARD_MAIN]; //Notification View
    
    [self pushNotificationsDetailView:pushId vc: vc];
}

- (void) loadInAppBrowserFromPushWithId: (NSString *) pushId urlString: (NSString *) urlString
{
    UIViewController *vc = [self loadVCWithSidePanel: VIEW_NOTIFICATIONS storyboard:STORYBOARD_MAIN]; //Notification View
    
    [self pushNotificationsDetailView:pushId vc: vc];
    
    [self pushInAppBrowserWithAddress: urlString vc: vc];
}

- (void)pushNotificationView: (UIViewController*) parentController {
    UIViewController *vc = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_NOTIFICATIONS];
    
    [self pushVC:parentController newVC:vc];
}

- (void)loadTNCView
{
    [self loadVCWithSidePanel: VIEW_TNC storyboard:STORYBOARD_WEBVIEW]; //TermsAndConditionsViewController
}

- (void) loadPrivacyView {
    [self loadVCWithSidePanel: VIEW_PRIVACY storyboard:STORYBOARD_WEBVIEW];
}

-(void)pushRegisterCodeResult:(NSDictionary *)resultsDict vc:(UIViewController*) parentController {
    RegisterCodeResultViewController *newVc = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_REGISTERCODERESULT]; //RegisterCodeResultViewController
    newVc.resultsDict = resultsDict;
    newVc.isScanBottle = NO;
    newVc.isConsumer = NO;
    [self pushVC: parentController  newVC:newVc];
}

-(void) pushScanBottleResult:(NSDictionary *)resultsDict isScanBottle:(BOOL)isScanBottle isConsumer:(BOOL) isConsumer vc:(UIViewController*) parentController {
    RegisterCodeResultViewController *newVc = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_REGISTERCODERESULT]; //RegisterCodeResultViewController
    newVc.resultsDict = resultsDict;
    newVc.isScanBottle = isScanBottle;
    newVc.isConsumer = isConsumer;
    [self pushVC: parentController  newVC:newVc];
}

#pragma mark Rewards Views
-(void)loadRewardsView
{
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
    {
        [self loadRewardsViewWithLockFlag: YES]; //redirect to locked
    }
    else
    {
        [self loadVCWithSidePanel: VIEW_REWARDS storyboard:STORYBOARD_REWARDS]; //RewardsViewController
    }
}
- (void)loadRewardsViewWithLockFlag:(BOOL) locked
{
    if (locked)
    {
        [self loadVCWithSidePanel: VIEW_RU_LOCKEDREWARDS storyboard:STORYBOARD_RUSSIA]; //RULockedRewardsViewController
    }
    else
    {
        [self loadVCWithSidePanel: VIEW_REWARDS storyboard:STORYBOARD_REWARDS]; //RewardsViewController
    }
}

-(void) pushRewardsCategoryWithCategoryArray: (NSArray *) categoryArray vc:(UIViewController*) parentController
{
    RewardsCategoryViewController *newvc = [STORYBOARD_REWARDS instantiateViewControllerWithIdentifier: VIEW_REWARDS_CATEGORY];
    
    newvc.mainCategoryArray = categoryArray;
    
    [self pushVC: parentController  newVC:newvc];
}

-(void) pushRewardsItemDetailsView: (NSDictionary *) item vc:(UIViewController*) parentController
{
    RewardsItemDetailsViewController *newvc = [STORYBOARD_REWARDS instantiateViewControllerWithIdentifier: VIEW_REWARDS_ITEMS];
    
    newvc.itemData = item;
    
    [self pushVC: parentController  newVC:newvc];
}

-(void) pushRewardsFavourites:(UIViewController*) parentController
{
    UIViewController *newvc = [STORYBOARD_REWARDS instantiateViewControllerWithIdentifier: VIEW_REWARDS_FAVOURITES];
    
    [self pushVC: parentController  newVC:newvc];
}

-(void) pushRewardsCart:(UIViewController*) parentController
{
    UIViewController *newvc = [STORYBOARD_REWARDS instantiateViewControllerWithIdentifier: VIEW_REWARDS_CART];
    
    [self pushVC: parentController  newVC:newvc];
}

-(void) pushRewardsHistory:(UIViewController*) parentController
{
    UIViewController *newvc = [STORYBOARD_REWARDS instantiateViewControllerWithIdentifier: VIEW_REWARDS_HISTORY];
    
    [self pushVC: parentController  newVC:newvc];
}

-(void) pushRewardsAddress:(UIViewController*) parentController
{
    //check out
    UIViewController *newvc = [STORYBOARD_REWARDS instantiateViewControllerWithIdentifier: VIEW_REWARDS_ADDRESS];
    
    [self pushVC: parentController  newVC:newvc];
}
-(void) pushRewardsConfirmation:(UIViewController*) parentController addressData:(NSDictionary *) addressData
{
    //check out
    RewardsConfirmationViewController *newvc = [STORYBOARD_REWARDS instantiateViewControllerWithIdentifier: VIEW_REWARDS_CONFIRMATION];
    
    newvc.addressData = addressData;
    
    [self pushVC: parentController  newVC:newvc];
}

-(void) pushRewardsRedemptionDetailsWithDetails: (NSDictionary *) detailsDict vc:(UIViewController*) parentController
{
    //check out
    RewardsRedemptionDetailsViewController *newvc = [STORYBOARD_REWARDS instantiateViewControllerWithIdentifier: VIEW_REWARDS_REDEMPTIONDETAILS];
    
    newvc.redemptionDetailsDict = detailsDict;
    
    [self pushVC: parentController  newVC:newvc];
}

-(void) loadRewardsCompletionWithDictionary:(NSDictionary *) completionDictionary
{
    //checkout complete
    RewardsRedemptionCompleteViewController *newvc = [STORYBOARD_REWARDS instantiateViewControllerWithIdentifier: VIEW_REWARDS_REDEMPTIONCOMPLETE];
    
    newvc.msgDictionary = completionDictionary;
    
    [self loadVCWithSidePanel:VIEW_REWARDS_CONFIRMATION storyboard:STORYBOARD_REWARDS vc: newvc];
}

-(void)loadRewardsHistoryFromCompletion
{
    [self pushRewardsHistory: [self loadVCWithSidePanel: VIEW_REWARDS storyboard:STORYBOARD_REWARDS]]; //RewardsViewController
}

#pragma mark TO Loyalty

- (void)loadTOLoyaltySummaryView
{
    [self loadVCWithSidePanel: VIEW_TOLOYALTY_SUMMARY storyboard:STORYBOARD_TOLOYALTY]; //TermsAndConditionsViewController
}

-(void) pushTOLoyaltyDetails:(UIViewController*) parentController addressData:(NSDictionary *) addressData
{
    TOLoyaltyDetailsViewController *newvc = [STORYBOARD_TOLOYALTY instantiateViewControllerWithIdentifier: VIEW_TOLOYALTY_DETAILS];

    newvc.addressData = addressData;
    [self pushVC: parentController  newVC:newvc];
}

#pragma mark -
-(void)loadSurveyView
{
    [self loadVCWithSidePanel: VIEW_SURVEY storyboard:STORYBOARD_MAIN]; //SurveyWebViewController
}

-(void)pushTNCView:(UIViewController *)parentController
{
    TermsAndConditionsViewController *newvc = [STORYBOARD_WEBVIEW instantiateViewControllerWithIdentifier: VIEW_TNC]; //TermsAndConditionsViewController
    [newvc view];
    
    newvc.tncType = 0;
    
    [self pushVC: parentController  newVC:newvc];
}

- (void) pushTNCView:(BOOL)showAgreeBtn tncType:(NSInteger)tncType vc: (UIViewController*) parentController
{
    TermsAndConditionsViewController *newvc = [STORYBOARD_WEBVIEW instantiateViewControllerWithIdentifier: VIEW_TNC]; //TermsAndConditionsViewController
    [newvc view];
    
    newvc.tncType = tncType;
    if (showAgreeBtn)
        [newvc showAgreeBtn];
    
    [self pushVC: parentController  newVC:newvc];
}

- (void) pushPrivacyView:(BOOL)showAgreeBtn vc: (UIViewController*) parentController
{
    PrivacyPolicyViewController *newvc = [STORYBOARD_WEBVIEW instantiateViewControllerWithIdentifier: VIEW_PRIVACY];
    [newvc view];
    
    if (showAgreeBtn)
        [newvc showAgreeBtn];
    
    [self pushVC: parentController  newVC:newvc];
}


- (void) pushPrivacyView: (UIViewController*) parentController;
{
    PrivacyPolicyViewController *newvc = [STORYBOARD_WEBVIEW instantiateViewControllerWithIdentifier: VIEW_PRIVACY];
    [newvc view]; 
    
    [self pushVC: parentController  newVC:newvc];
}

- (void)loadSettingsView
{
    [self loadVCWithSidePanel: VIEW_SETTINGS storyboard:STORYBOARD_MAIN]; //SettingsViewController
}

- (void)loadContactPostLoginView
{
    [self loadVCWithSidePanel: VIEW_CONTACTUS storyboard:STORYBOARD_MAIN]; //ContactUsViewController
}

#pragma manage order
-(void) pushOrderDetail: (UIViewController *) parentController orderID:(NSString *)orderID isShareOrder:(BOOL) isShareOrder
{
    OrderDetailsViewController *newvc = [STORYBOARD_MANAGEORDERS instantiateViewControllerWithIdentifier: VIEW_ORDERDETAILS];
    newvc.orderID = orderID;
    newvc.isShareOrder = isShareOrder;
    [self pushVC: parentController  newVC:newvc];
}

//-(void) pushCategoryView: (UIViewController *) parentController productID:(NSString *)productID categoryName:(NSString *)categoryName

-(void) pushCategoryView: (UIViewController *) parentController productID:(NSString *)productID categoryName:(NSString *)categoryName categoryArray:(NSArray *)categoryArray
{
    AddNewCategoryViewController *newvc = [STORYBOARD_MANAGEORDERS instantiateViewControllerWithIdentifier: VIEW_CATEGORYVIEW];
    newvc.productID = productID;
    newvc.categoryName = categoryName;
    newvc.mainCategoryArray = categoryArray;
    [self pushVC: parentController  newVC:newvc];
}

-(void) pushMyCartView: (UIViewController *) parentController
{
    UIViewController *newvc = [STORYBOARD_MANAGEORDERS instantiateViewControllerWithIdentifier: VIEW_MYCARTVIEW];
    [self pushVC: parentController  newVC:newvc];
}

-(void)loadOrderConfirmView
{
    [self loadVCWithSidePanel: VIEW_ORDERCONFIRMVIEW storyboard:STORYBOARD_MANAGEORDERS]; //Order Confirmation
}

#pragma mark DSR only
- (void)loadAssistedRegistrationView
{
    [self loadVCWithSidePanel: VIEW_TRADEREGISTRATION storyboard:STORYBOARD_MAIN];
}

- (void)loadInviteTradeView
{
    [self loadVCWithSidePanel: VIEW_INVITETRADE storyboard:STORYBOARD_MAIN];
}

#pragma mark Workshop only
#pragma mark Mechanic only


#pragma mark Invite Customer
//- (void)loadInviteCustomerView
//{
////    [self loadVCWithSidePanel: VIEW_INVITECUSTOMER storyboard:STORYBOARD_MAIN];
//}

- (void) pushInviteCustomerView:(NSString *) vehicleNumber vc: (UIViewController*) parentController
{
    InviteCustomerViewController *newvc = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_INVITECUSTOMER]; //InviteCustomerViewController
    [newvc view];
    newvc.isPush = YES;
    newvc.txtVehicleNumber.text = vehicleNumber;
    
    [self pushVC: parentController  newVC:newvc];
}

#pragma mark - Register Consumer
-(void) loadRegisterCustomerView:(NSDictionary *) promoDict
{
    //checkout complete
    RegisterCustomerViewController *newvc = [STORYBOARD_REGISTERCUSTOMER instantiateViewControllerWithIdentifier: VIEW_REGISTERCUSTOMER];
    
    newvc.registerCustomerDict = promoDict;

    //identifier for conditions only. not for loading
    [self loadVCWithSidePanel:VIEW_REGISTERCUSTOMER storyboard:STORYBOARD_REGISTERCUSTOMER vc: newvc];
}

- (void) pushRegisterConsumer:(NSDictionary *)registerCustomerDict vc: (UIViewController*) parentController
{
    RegisterCustomerViewController *newvc = [STORYBOARD_REGISTERCUSTOMER instantiateViewControllerWithIdentifier: VIEW_REGISTERCUSTOMER];
    newvc.registerCustomerDict = registerCustomerDict;
    [newvc view];
    
    [self pushVC: parentController  newVC:newvc];
}

- (void) pushRegisterConsumerOTPViewWithDict:(NSDictionary *)promoDict vc: (UIViewController*) parentController
{
    RegisterCustomerOTPViewController *newvc = [STORYBOARD_REGISTERCUSTOMER instantiateViewControllerWithIdentifier: VIEW_REGISTERCUSTOMER_OTP];
    newvc.promoDict = promoDict;
    [newvc view];
    
    [self pushVC: parentController  newVC:newvc];
}

- (void) pushRegisterConsumerRegisterFormViewWithOTPDict:(NSDictionary *)otpDict vc:(UIViewController*) parentController
{
    RegisterCustomerFormViewController *newvc = [STORYBOARD_REGISTERCUSTOMER instantiateViewControllerWithIdentifier: VIEW_REGISTERCUSTOMER_FORM];
    
    newvc.otpDict = otpDict;
    
    [newvc view];
    
    [self pushVC: parentController  newVC:newvc];
}

- (void) pushRegisterConsumerRegisterCompleteWithDict:(NSDictionary *)promoDict vc:(UIViewController*) parentController
{
    RegisterCustomerCompleteViewController *newvc = [STORYBOARD_REGISTERCUSTOMER instantiateViewControllerWithIdentifier: VIEW_REGISTERCUSTOMER_COMPLETE]; //InviteCustomerViewController
    newvc.promoDict = promoDict;
    [newvc view];
    
    [self pushVC: parentController  newVC:newvc];
}

-(void) loadRegisterConsumerRegisterCompleteWithDict:(NSDictionary *) promoDict
{
    //checkout complete
    RegisterCustomerCompleteViewController *newvc = [STORYBOARD_REGISTERCUSTOMER instantiateViewControllerWithIdentifier: VIEW_REGISTERCUSTOMER_COMPLETE];
    
    newvc.promoDict = promoDict;
    
    //identifier for conditions only. not for loading
    [self loadVCWithSidePanel:VIEW_REGISTERCUSTOMER_COMPLETE storyboard:STORYBOARD_REGISTERCUSTOMER vc: newvc];
}

#pragma mark - Scan Bottle
-(void) loadScanBottle {
    [self loadVCWithSidePanel: VIEW_SCANBOTTLE_MAIN storyboard: STORYBBOARD_IN];
}

- (void) pushScanBottleForShareCodeView : (UIViewController *) parentController {
    
    ScanBottleForShareCodeViewController *newVc = [STORYBBOARD_IN instantiateViewControllerWithIdentifier: VIEW_SCANBOTTLE_FOR_SHARECODE];
    
    [self pushVC:parentController newVC:newVc];
    
}

- (void) pushScanBottleForVehicleIDView : (UIViewController *) parentController {
    
    ScanBottleForConsumerViewController *newVc = [STORYBBOARD_IN instantiateViewControllerWithIdentifier: VIEW_SCANBOTTLE_FOR_CONSUMER];
    
    [self pushVC:parentController newVC:newVc];
    
}

- (void) loadAddNewNetwork {
    ManageWorkshopTabViewController *vc = (ManageWorkshopTabViewController * ) [self loadVCWithSidePanel:VIEW_MANAGE storyboard:STORYBOARD_MANAGEWORKSHOP];
    [vc view];
    if([[mSession profileInfo] hasPendingWorkshopList]) {
         [vc setSelectedIndex: 2];
    } else {
         [vc setSelectedIndex: 1];
    }
}

#pragma mark - Loyalty Contract
-(void)  loadLoyaltyContract {
     [self loadVCWithSidePanel: VIEW_LOYALTY_TAB storyboard:STORYBOARD_LOYALTYPROGRAM]; //Loyalty Contract
}

-(void) loadLoyaltyContractProgramPerformance {
    LoyaltyRewardsTabViewController *vc = (LoyaltyRewardsTabViewController *) [self loadVCWithSidePanel:VIEW_LOYALTY_TAB storyboard:STORYBOARD_LOYALTYPROGRAM];
    [vc view];
    
    //    [vc performSelector: @selector(setSelectedIndex:) withObject: @(1) afterDelay: 0.1f];
    //    [vc setSelectedViewController: [[vc viewControllers] objectAtIndex: 1]];
    [vc setSelectedIndex: 1];
}

-(void) pushFilterBy:(NSMutableDictionary *)filterStatusArray delegate:(UIViewController*)delegate vc: (UIViewController*) parentController
{
    FilterByViewController *filterByViewController = [STORYBOARD_LOYALTYPROGRAM instantiateViewControllerWithIdentifier:VIEW_LOYALTY_FILTERBY];
    filterByViewController.filterStatusArray = filterStatusArray;
    filterByViewController.delegate = (UIViewController<FilterByDelegate> *)delegate;
    
    [self pushVC:parentController newVC:filterByViewController];
}

-(void) pushAddNewContract:(UIViewController *)parentController {
    AddNewContract_FirstStepViewController *newVC = [STORYBOARD_LOYALTYPROGRAM instantiateViewControllerWithIdentifier:VIEW_LOYALTY_ADDNEWCONTRACT_FIRSTSTEP];
    
    [self pushVC:parentController newVC:newVC];
}

-(void) pushFinalViewInNewContract:(UIViewController *)parentController withPreferedDict:(NSDictionary*)responseData forTradeId:(NSString *) tradeID withCatalogue:(NSArray *)catalogueList{
    AddNewContract_SecondStepViewController *newVC = [STORYBOARD_LOYALTYPROGRAM instantiateViewControllerWithIdentifier:VIEW_LOYALTY_ADDNEWCONTRACT_SECONDSTEP];
    newVC.responseData = responseData;
    newVC.companyTradeID = tradeID;
    newVC.catalogueListArray = catalogueList;
    
    [self pushVC:parentController newVC:newVC];
}

-(void) loadContractConfirmView {
    [self loadVCWithSidePanel: VIEW_LOYALTY_CONTRACTCONFIRMATION storyboard:STORYBOARD_LOYALTYPROGRAM]; 
}

-(void) pushContractDetails:(NSDictionary *)contractDetails vc:(UIViewController *)parentController {
    LoyaltyContractDetailsViewController *newVC = [STORYBOARD_LOYALTYPROGRAM instantiateViewControllerWithIdentifier:VIEW_LOYALTY_CONTRACTDETAILSVIEW];
    newVC.contractDetails = contractDetails;
    newVC.isLoaded = false;
    
    [self pushVC:parentController newVC:newVC];
}

-(void) pushLoyaltyBreakdown:(NSDictionary *)workshopDetails withWorkshopList:(NSArray *) workshopList andContract:(NSString *) contractRef vc:(UIViewController *)parentController {
    
    LoyaltyWorkshopBreakdownViewController *newVC = [STORYBOARD_LOYALTYPROGRAM instantiateViewControllerWithIdentifier:VIEW_LOYALTY_WORKSHOPBREAKDOWN];
    newVC.workshopList = workshopList;
    newVC.contractRef = contractRef;
    newVC.breakdownDetails = workshopDetails;
    
    [self pushVC:parentController newVC:newVC];
}

-(void) pushLoyaltyCartonBreakdown:(NSDictionary *)breakdownDetails vc:(UIViewController *)parentViewController {
    LoyaltyCartonsBreakdownViewController *newVC = [STORYBOARD_LOYALTYPROGRAM instantiateViewControllerWithIdentifier:VIEW_LOYALTY_CARTONBREAKDOWN];
    
    newVC.breakdownDetails = breakdownDetails;
    
    [self pushVC:parentViewController newVC:newVC];
}

-(void) loadLoyaltyContractDetails:(NSDictionary*)contractDetails {
    if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
        LoyaltyContractDetailsViewController *newVC = [STORYBOARD_LOYALTYPROGRAM instantiateViewControllerWithIdentifier:VIEW_LOYALTY_CONTRACTDETAILSVIEW];
        newVC.contractDetails = contractDetails;
        newVC.isLoaded = true;

        //identifier for conditions only. not for loading
        [self loadVCWithSidePanel: VIEW_LOYALTY_CONTRACTDETAILSVIEW storyboard:STORYBOARD_LOYALTYPROGRAM vc:newVC];
    } else {
        OwnerContractDetailsViewController *newVC = [STORYBOARD_LOYALTYPROGRAM instantiateViewControllerWithIdentifier:VIEW_LOYALTY_TO_CONTRACTDETAIL];
        newVC.contractDetails = contractDetails;
        newVC.isLoaded = true;

        //identifier for conditions only. not for loading
        [self loadVCWithSidePanel: VIEW_LOYALTY_TO_CONTRACTDETAIL storyboard:STORYBOARD_LOYALTYPROGRAM vc:newVC];
    }
}

-(void) pushPendingContractDetails:(NSDictionary *)breakdownDetails vc:(UIViewController *)parentViewController {
    OwnerPendingContractDetailViewController *newVC = [STORYBOARD_LOYALTYPROGRAM instantiateViewControllerWithIdentifier:VIEW_LOYALTY_TO_PENDINGCONTRACTDETAIL];

    newVC.contractDetails = breakdownDetails;

    [self pushVC:parentViewController newVC:newVC];
}

-(void) pushContractDetailsForOwner:(NSDictionary *)breakdownDetails vc:(UIViewController *)parentViewController {
    OwnerContractDetailsViewController *newVC = [STORYBOARD_LOYALTYPROGRAM instantiateViewControllerWithIdentifier:VIEW_LOYALTY_TO_CONTRACTDETAIL];

    newVC.contractDetails = breakdownDetails;

    [self pushVC:parentViewController newVC:newVC];
}

#pragma mark - From Carousel
-(void) loadRecentActivity {
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
            RUDSRPerformanceTabViewController *vc = (RUDSRPerformanceTabViewController *) [self loadVCWithSidePanel: VIEW_RU_DSRPERFORMANCE_TAB storyboard: STORYBOARD_RU_DSR_WORKSHOPPERFORMANCE];
            [vc view];
            [vc setSelectedIndex:1];
        } else if(GET_PROFILETYPE == kPROFILETYPE_DSM) {
             [self loadVCWithSidePanel:VIEW_PARTNERSOFSHELL_POINTSEARNED_V2 storyboard:STORYBOARD_RUSSIA_V2];
        } else {
            RUTOPerformanceTabViewController *vc = (RUTOPerformanceTabViewController *) [self loadVCWithSidePanel: VIEW_RU_TOPERFORMANCE_TAB storyboard: STORYBOARD_RU_WORKSHOPPERFORMANCE];
            [vc view];
            [vc setSelectedIndex:1];
        }
    } else {
        if(GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER || GET_PROFILETYPE == kPROFILETYPE_MECHANIC || GET_PROFILETYPE == kPROFILETYPE_FORECOURTATTENDANT) {
                if ([[self profileInfo] hasMYPerformance] && [[self profileInfo] hasMYPoints]) {
                    INPerformanceTabViewController *vc = (INPerformanceTabViewController *) [self loadTabBarVCWithSidePanel: VIEW_IN_PERFORMANCE_TAB  storyboard:STORYBOARD_IN_PERFORMANCE];
                    [vc view];

                    [vc setSelectedIndex:1];
                } else if([[self profileInfo] hasMYPerformance]) {

                } else {
                     [self loadTabBarVCWithSidePanel: VIEW_PERFORMANCEPOINTS  storyboard:STORYBOARD_PERFORMANCE];
                }
        } else {
            if ([[self profileInfo] hasWorkshopPerformanceView] || [[self profileInfo] hasLeaderBoard]) {
                INPerformanceTabViewController *vc = (INPerformanceTabViewController *) [self loadTabBarVCWithSidePanel: VIEW_IN_PERFORMANCE_TAB storyboard:STORYBOARD_IN_PERFORMANCE];
                [vc view];

                [vc setSelectedIndex:1];
            }
        }
    }
}

-(void) pushScanNRewardReport:(NSArray *)productNameList vc:(UIViewController *)parentController {
    ScanForRewardsHistoryViewController *newVC = [STORYBOARD_MAINV2 instantiateViewControllerWithIdentifier: VIEW_SCANFORREWARDS_HISTORY];
    newVC.productNameList = productNameList;
    [self pushVC:parentController newVC:newVC];
}

#pragma mark - In App Asset
-(void)loadMyLibraryView
{
    [self loadVCWithSidePanel: VIEW_INAPPASSETVIEW storyboard:STORYBOARD_INAPPASSET]; //In App Library
}

-(void)loadPerformanceCTA:(NSString *) categoryId
{
    UIViewController *vc = [self loadVCWithSidePanel: VIEW_INAPPASSETVIEW storyboard:STORYBOARD_INAPPASSET]; //In App Library
    [self pushInAppAssetCategoryListWithCategoryId: categoryId vc: vc];
}

-(void) pushInAppAssetCategoryListWithCategoryId:(NSString *)categoryId vc:(UIViewController *) parentController
{
    InAppAssetCategoryViewController *newvc = [STORYBOARD_INAPPASSET instantiateViewControllerWithIdentifier: VIEW_INAPPASSETCATEGORYVIEW];
    
    newvc.categoryId = categoryId;
    
    [self pushVC: parentController  newVC:newvc];
}

-(void) pushInAppAssetPDFView:(NSString *)pdfUrlString vc:(UIViewController *) parentController
{
    InAppAssetPDFViewController *newvc = [STORYBOARD_INAPPASSET instantiateViewControllerWithIdentifier: VIEW_INAPPASSETPDFVIEW];
    newvc.urlString = pdfUrlString;
    [self pushVC: parentController  newVC:newvc];
}

#pragma mark - In App Asset
-(void)loadWorkshopProfilingView
{
    [self loadVCWithSidePanel: VIEW_WORKSHOPPROFILING storyboard:STORYBOARD_WORKSHOPPROFILING]; //Workshop Profiling
}

-(void) pushWorkshopProfilingDetails: (UIViewController *) parentController
               workshopDetailsData:(NSDictionary *) workshopDetailsData
{
    WorkshopProfilingDetailsViewController *newvc = [STORYBOARD_WORKSHOPPROFILING instantiateViewControllerWithIdentifier: VIEW_WORKSHOPPROFILINGDETAILS]; //WorkshopProfilingDetailsViewController
    
    newvc.workshopDetailsData = workshopDetailsData;
    [self pushVC: parentController  newVC:newvc];
}

#pragma mark - RUSSIA METHODS
-(void)loadRUEarnPoints
{
    switch([[mSession profileInfo] businessType]) {
        case 1:
        {
            RUEarnPointsTable_V2ViewController *b2bCampaignView = [STORYBOARD_RUSSIA_V2 instantiateViewControllerWithIdentifier: VIEW_EARNPOINTS_TABLE_V2];
            b2bCampaignView.isB2BCampaign = YES;
            [self loadVCWithSidePanel:VIEW_EARNPOINTS_TABLE_V2 storyboard:STORYBOARD_RUSSIA_V2 vc:b2bCampaignView];
        }
            break;
        case 2:
        {
            RUEarnPointsTable_V2ViewController *b2bCampaignView = [STORYBOARD_RUSSIA_V2 instantiateViewControllerWithIdentifier: VIEW_EARNPOINTS_TABLE_V2];
            b2bCampaignView.isB2BCampaign = NO;
            [self loadVCWithSidePanel:VIEW_EARNPOINTS_TABLE_V2 storyboard:STORYBOARD_RUSSIA_V2 vc:b2bCampaignView];
            
        }
            break;
        case 3:
        {
            [self loadVCWithSidePanel: VIEW_EARNPOINTS_TAB_V2 storyboard:STORYBOARD_RUSSIA_V2];
        }
            break;
        default:
            break;
    }
    
}

-(void) pushRUEarnPoints: (UIViewController *) parentController
{
    switch([[mSession profileInfo] businessType]) {
        case 1:
        {
            RUEarnPointsTable_V2ViewController *b2bCampaignView = [STORYBOARD_RUSSIA_V2 instantiateViewControllerWithIdentifier: VIEW_EARNPOINTS_TABLE_V2];
            b2bCampaignView.isB2BCampaign = YES;
            [self pushVC:parentController newVC:b2bCampaignView];
        }
            break;
        case 2:
        {
            RUEarnPointsTable_V2ViewController *b2bCampaignView = [STORYBOARD_RUSSIA_V2 instantiateViewControllerWithIdentifier: VIEW_EARNPOINTS_TABLE_V2];
            b2bCampaignView.isB2BCampaign = NO;
            [self pushVC:parentController newVC:b2bCampaignView];
            
        }
            break;
        case 3:
        {
            UIViewController *newvc = [STORYBOARD_RUSSIA_V2 instantiateViewControllerWithIdentifier: VIEW_EARNPOINTS_TAB_V2];
            [self pushVC: parentController  newVC:newvc];
        }
            break;
        default:
            break;
    }
    
}

-(void)loadRUPartners
{
    if(GET_PROFILETYPE == kPROFILETYPE_DSM) {
        [self loadVCWithSidePanel: VIEW_PARTNERSOFSHELL_POINTSEARNED_V2 storyboard:STORYBOARD_RUSSIA_V2];
    } else {
        [self loadVCWithSidePanel: VIEW_PARTNERSOFSHELL_TAB_V2 storyboard:STORYBOARD_RUSSIA_V2];
    }
}

-(void) pushRUBreakdown: (UIViewController *) parentController fromB2CCampaign:(BOOL) isB2CCampaign {
    RUBreakdown_V2ViewController *newvc = [STORYBOARD_RUSSIA_V2 instantiateViewControllerWithIdentifier: VIEW_RU_BREAKDOWN_V2];
    newvc.isB2CBreakdown = isB2CCampaign;
    
    [self pushVC: parentController  newVC:newvc];
}
//-(void)loadRUSearchVehicle
//{
//    [self loadVCWithSidePanel: VIEW_RU_SEARCHVEHICLE storyboard:STORYBOARD_RUSSIA];
//}
//
//-(void)loadRURegisterOilChange
//{
//    [self loadVCWithSidePanel: VIEW_RU_REGISTEROILCHANGE storyboard:STORYBOARD_RUSSIA];
//}

-(void)pushRURegisterOilChangeWithVehicleId: (NSString *) vehicleId vc:(UIViewController *) parentController
{
    RURegisterOilChangeViewController *newvc = [STORYBOARD_RUSSIA instantiateViewControllerWithIdentifier: VIEW_RU_REGISTEROILCHANGE];
    
    newvc.vehicleNumberString = vehicleId;
    
    [self pushVC: parentController  newVC:newvc];
}

-(void)loadRUCustomerRedemption
{
    [self loadVCWithSidePanel: VIEW_RU_REDEMPTION storyboard:STORYBOARD_RUSSIA];
}

//-(void) pushRUCustomerRedemption: (UIViewController *) parentController
//{
//    UIViewController *newvc = [STORYBOARD_RUSSIA instantiateViewControllerWithIdentifier: VIEW_RU_REDEMPTION];
//
//    [self pushVC: parentController  newVC:newvc];
//}

-(void) pushRUCustomerRedemptionSuccess:(NSDictionary *)successParams vc: (UIViewController *) parentController
{
    RUCustomerRedemptionSuccessViewController *newvc = [STORYBOARD_RUSSIA instantiateViewControllerWithIdentifier: VIEW_RU_REDEMPTIONSUCCESS]; //RUCustomerRedemptionSuccessViewController
    
    newvc.successDict = successParams;
    
    [self pushVC: parentController  newVC:newvc];
}

-(void) pushRURegisterOilChangeResults:(NSMutableDictionary *)resultsDict vc:(UIViewController *) parentController
{
    RURegisterOilChangeResultViewController *newvc = [STORYBOARD_RUSSIA instantiateViewControllerWithIdentifier: VIEW_RU_REGISTEROILCHANGERESULT];
    
    newvc.resultsDict = resultsDict;
    
    [self pushVC: parentController  newVC:newvc];
}

-(void) pushRUChooseProductBrand: (UIViewController *) parentController
{
    RUChooseProductBrandViewController *newvc = [STORYBOARD_RUSSIA instantiateViewControllerWithIdentifier: VIEW_RU_CHOOSEPRODUCTBRAND];

    newvc.delegate = (UIViewController<RUChooseProductBrandDelegate> *) parentController;
    
    [self pushVC: parentController  newVC:newvc];
}

-(void) pushRUChooseProductWithBrand: (NSDictionary* )brandDict vc: (UIViewController *) parentController
{
    RUChooseProductViewController *newvc = [STORYBOARD_RUSSIA instantiateViewControllerWithIdentifier: VIEW_RU_CHOOSEPRODUCT];
    
    RUChooseProductBrandViewController *parentVc = (RUChooseProductBrandViewController *) parentController;
    
    newvc.delegate = parentVc.delegate;
    newvc.brandDict = brandDict;
    
    [self pushVC: parentVc  newVC:newvc];
}

-(void) pushRUSearchProductWithAllItems:(NSMutableArray *)allItemsArray vc: (UIViewController *) parentController
{
    RUProductSearchViewController *newvc = [STORYBOARD_RUSSIA instantiateViewControllerWithIdentifier: VIEW_RU_PRODUCTSEARCH];
    
    RUChooseProductBrandViewController *parentVc = (RUChooseProductBrandViewController *) parentController;
    newvc.delegate = parentVc.delegate;
    newvc.allItemsArray = allItemsArray;
    
    [self pushVC: parentVc  newVC:newvc];
}

-(void) pushValueCalculatorWithData:(NSDictionary *)calculatorDict vc:(UIViewController *)vc
{
    RUValueCalculatorViewController *newvc = [STORYBOARD_RUSSIA instantiateViewControllerWithIdentifier: VIEW_RU_VALUECALCULATOR];
    
//    newvc.calculatorDict = calculatorDict;
    
    [self pushVC: vc  newVC:newvc];
    
}

-(void) loadRURegisterCustomer: (NSString *) vehicleId
{
    RURegisterCustomerViewController *newVC = [STORYBOARD_RUSSIA instantiateViewControllerWithIdentifier: VIEW_RU_REGISTERCUSTOMER];
    
    newVC.vehicleId = vehicleId;
    
    //identifier for conditions only. not for loading
    [self loadVCWithSidePanel: VIEW_RU_REGISTERCUSTOMER storyboard:STORYBOARD_RUSSIA vc:newVC];
}

-(void) pushRUCustomerSignature: (UIViewController<RUCustomerSignatureDelegate> *) parentController
{
    RUCustomerSignatureViewController *newvc = [STORYBOARD_RUSSIA instantiateViewControllerWithIdentifier: VIEW_RU_CUSTOMERSIGNATURE];
    
    newvc.delegate = parentController;
    
    [self pushVC: parentController  newVC:newvc];
}
-(void) pushRUCustomerSignatureWithDelegate: (UIViewController<RUCustomerSignatureDelegate> *) parentController delegate:(UIViewController<RUCustomerSignatureDelegate> *) delegate
{
    RUCustomerSignatureViewController *newvc = [STORYBOARD_RUSSIA instantiateViewControllerWithIdentifier: VIEW_RU_CUSTOMERSIGNATURE];
    
    newvc.delegate = delegate;
    
    [self pushVC: parentController  newVC:newvc];
}

- (void) pushRURegisterConsumerOTPViewWithDict:(NSMutableDictionary *)submitDict vc: (UIViewController*) parentController
{
    RURegisterCustomerOTPViewController *newvc = [STORYBOARD_RUSSIA instantiateViewControllerWithIdentifier: VIEW_RU_REGISTERCUSTOMEROTP];
    newvc.submitDict = submitDict;
    [newvc view];
    
    [self pushVC: parentController  newVC:newvc];
}

- (void) pushRegisterPanOTPViewWithMobileNo:(NSString *)mobileNo dictBankDetail:(NSDictionary*)dictBank vc: (UIViewController*) parentController
{
    RegisterPanOTPViewController *newvc = [STORYBOARD_REGISTEROTP instantiateViewControllerWithIdentifier: VIEW_REGISTER_PANOTP];
    newvc.bankDetailDict = dictBank;
    newvc.strMobileNo = mobileNo;
    [self pushVC: parentController  newVC:newvc];
}

-(void) loadRURegisterConsumerSuccess
{
    //identifier for conditions only. not for loading
    [self loadVCWithSidePanel:VIEW_RU_REGISTERCUSTOMERSUCCESS storyboard: STORYBOARD_RUSSIA];
}

- (void)pushRUOfferDetailsView:(UIViewController*) parentController offerDict:(NSDictionary *)offerDict
{
    RUWorkshopOfferDetailsViewController *newvc = [STORYBOARD_RU_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_RU_OFFERDETAILS]; //RUWorkshopOfferDetailsViewController
    newvc.offerDict = offerDict;
    [self pushVC: parentController  newVC:newvc];
}

#pragma mark Russia Workshop Ancilliary Offers
- (void)pushRUAddNewOfferStepOne:(UIViewController*) parentController
{
    UIViewController *newvc = [STORYBOARD_RU_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_RU_ADDWORKSHOPOFFER_STEPONE];
    
    [self pushVC: parentController  newVC:newvc];
}

//- (void)pushRUAddNewOfferStepOneWithOffer:(NSMutableDictionary *) offerDict vc:(UIViewController*) parentController
//{
//    RUWorkshopOfferAddNew_StepOneViewController *newvc = [STORYBOARD_RU_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_RU_ADDWORKSHOPOFFER_STEPTWO];
//    
//    newvc.submitParams = offerDict;
//    
//    [self pushVC: parentController  newVC:newvc];
//}

- (void)pushRUAddNewOfferStepTwoWithOffer:(NSMutableDictionary *) offerDict vc:(UIViewController*) parentController
{
    RUWorkshopOfferAddNew_StepTwoViewController *newvc = [STORYBOARD_RU_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_RU_ADDWORKSHOPOFFER_STEPTWO];
    
    newvc.submitParams = offerDict;
    
    [self pushVC: parentController  newVC:newvc];
}

- (void)pushRUAddNewOfferStepThreeWithOffer:(NSMutableDictionary *) offerDict vc:(UIViewController*) parentController
{
    RUWorkshopOfferAddNew_StepThreeViewController *newvc = [STORYBOARD_RU_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_RU_ADDWORKSHOPOFFER_STEPTHREE];
    
    newvc.submitParams = offerDict;
    
    [self pushVC: parentController  newVC:newvc];
}
- (void)pushRUAddNewOfferStepFourWithOffer:(NSMutableDictionary *) offerDict vc:(UIViewController*) parentController
{
    RUWorkshopOfferAddNew_StepFourViewController *newvc = [STORYBOARD_RU_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_RU_ADDWORKSHOPOFFER_STEPFOUR];
    
    newvc.submitParams = offerDict;
    
    [self pushVC: parentController  newVC:newvc];
}

//- (void)pushRUAddNewOfferSuccessWithOffer:(NSMutableDictionary *) offerDict vc:(UIViewController*) parentController
//{
//    RUWorkshopOfferAddNew_SuccessViewController *newvc = [STORYBOARD_RU_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_RU_ADDWORKSHOPOFFER_SUCCESS];
//    
//    [self pushVC: parentController  newVC:newvc];
//}

-(void) loadRUAddNewOfferSuccessWithOffer:(NSMutableDictionary *) offerDict
{
    //checkout complete
    RUWorkshopOfferAddNew_SuccessViewController *newvc = [STORYBOARD_RU_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_RU_ADDWORKSHOPOFFER_SUCCESS];
    
    newvc.submitParams = offerDict;
    
    [self loadVCWithSidePanel:VIEW_RU_ADDWORKSHOPOFFER_SUCCESS storyboard:STORYBOARD_RUSSIA vc: newvc];
}

- (void)pushRUAddNewOfferSelectCarBrand:(NSDictionary *)submitParams vc:(RUWorkshopOfferAddNew_StepTwoViewController *) parentController
{
    RUWorkshopOfferSelectCarBrandViewController *newvc = [STORYBOARD_RU_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_RU_ADDWORKSHOPOFFER_SELECTCARBRAND];
    
    newvc.carBrandSubmitParams = submitParams;
    newvc.delegate = parentController;
    
    [self pushVC: parentController  newVC:newvc];
}

- (void)pushRUAddNewOfferSelectOilBrand:(NSDictionary *)submitParams vc:(RUWorkshopOfferSelectCarBrandViewController*) parentController
{
    RUWorkshopOfferSelectOilBrandViewController *newvc = [STORYBOARD_RU_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_RU_ADDWORKSHOPOFFER_SELECTOILBRAND];
    
    newvc.oilBrandSubmitParams = submitParams;
    newvc.delegate = parentController;
    
//    newvc.delegate = parentController.delegate;
    
    [self pushVC: parentController  newVC:newvc];
}

#pragma mark - Russia V2
-(void) pushActivateRegistration:(NSDictionary *)values vc:(UIViewController *)parentController delegate:(UIViewController *)delegate{
    
    AddNewWorkshop_NewViewController *preRegisterWorkshopView = [STORYBOARD_MANAGEWORKSHOP instantiateViewControllerWithIdentifier:VIEW_ADDNEWWORKSHOP_NEW];
    preRegisterWorkshopView.linkedWorkshopArray = [values objectForKey:@"LinkedWorkshops"];
    preRegisterWorkshopView.primaryOutletID = [values objectForKey:@"PrimaryOutletID"];
    preRegisterWorkshopView.registeredName = [values objectForKey:@"RegisteredName"];
    preRegisterWorkshopView.delegate = delegate;
    
    [self pushVC:parentController newVC:preRegisterWorkshopView];
    
}

#pragma mark RU Performance
-(void) loadRUWorkshopPerformance
{
    if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
       //  [self loadVCWithSidePanel: VIEW_RU_DSRPERFORMANCE_TAB storyboard: STORYBOARD_RU_DSR_WORKSHOPPERFORMANCE];
    
        [self loadVCWithSidePanel:VIEW_PARTNERSOFSHELL_POINTSEARNED_V2 storyboard: STORYBOARD_RUSSIA_V2];
    }
   /* else if (GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER) {
        [self loadVCWithSidePanel: VIEW_RU_DSRPERFORMANCE_TAB storyboard: STORYBOARD_RU_DSR_WORKSHOPPERFORMANCE];
    }*/
    else {
         [self loadVCWithSidePanel: VIEW_RU_TOPERFORMANCE_TAB storyboard: STORYBOARD_RU_WORKSHOPPERFORMANCE];
    }
}

-(void) loadRUWorkshopPerformanceFromCarousel: (NSString *) pushNavKey
{
    RUDSRPerformanceTabViewController *vc = (RUDSRPerformanceTabViewController *) [self loadVCWithSidePanel: VIEW_RU_DSRPERFORMANCE_TAB storyboard: STORYBOARD_RU_DSR_WORKSHOPPERFORMANCE];
    vc.pushNavKey = pushNavKey;
}


-(void) pushOrderBreakdown:(UIViewController *)vc
{
    UIViewController *newvc = [STORYBOARD_RU_WORKSHOPPERFORMANCE instantiateViewControllerWithIdentifier: VIEW_RU_BREAKDOWN_ORDER_TAB];
    
    [self pushVC: vc  newVC:newvc];
}

-(void) pushPointsBreakdown:(UIViewController *)vc {
    UIViewController *newvc = [STORYBOARD_RU_WORKSHOPPERFORMANCE instantiateViewControllerWithIdentifier: VIEW_RU_BREAKDOWN_POINTS_TAB];
    
    [self pushVC: vc  newVC:newvc];
}

-(void) pushStaffBreakdown:(UIViewController *)vc {
    UIViewController *newvc = [STORYBOARD_RU_WORKSHOPPERFORMANCE instantiateViewControllerWithIdentifier: VIEW_RU_BREAKDOWN_STAFF];
    
    [self pushVC:vc  newVC:newvc];
}

-(void) pushOilChangeBreakdown:(UIViewController *)vc {
    UIViewController *newvc = [STORYBOARD_RU_WORKSHOPPERFORMANCE instantiateViewControllerWithIdentifier: VIEW_RU_BREAKDOWN_OILCHANGE_TAB];
    
    [self pushVC:vc  newVC:newvc];
}

-(void) pushCustomerBreakdown:(UIViewController *)vc {
    UIViewController *newvc = [STORYBOARD_RU_WORKSHOPPERFORMANCE instantiateViewControllerWithIdentifier: VIEW_RU_BREAKDOWN_CUSTOMER];
    
    [self pushVC:vc  newVC:newvc];
}

-(void) pushOfferRedemptionBreakdown:(UIViewController *)vc {
    UIViewController *newvc = [STORYBOARD_RU_WORKSHOPPERFORMANCE instantiateViewControllerWithIdentifier: VIEW_RU_BREAKDOWN_OFFERREDEMPTIONS];
    
    [self pushVC:vc  newVC:newvc];
}

-(void) pushDSRBreakdown:(BOOL)isOrderBreakdown vc:(UIViewController *)vc {
    RUDSRBreakdownMainViewController *newvc = [STORYBOARD_RU_DSR_WORKSHOPPERFORMANCE instantiateViewControllerWithIdentifier: VIEW_RU_DSR_BREAKDOWN_TAB];
    
    newvc.isOrderBreakdown = isOrderBreakdown;
    
    [self pushVC:vc  newVC:newvc];
}

-(void) pushDSRCustomerBreakdown:(UIViewController *)vc {
    UIViewController *newvc = [STORYBOARD_RU_DSR_WORKSHOPPERFORMANCE instantiateViewControllerWithIdentifier: VIEW_RU_DSR_BREAKDOWN_CUSTOMER];
    
    
    [self pushVC:vc  newVC:newvc];
}

-(void) loadApprovedWorkshop {
//    ManageWorkshopTabViewController *vc = [STORYBOARD_MANAGEWORKSHOP instantiateViewControllerWithIdentifier:VIEW_MANAGE];
//
//    vc.tabToBeDisplayed = @"ApprovedWorkshop";
    
    ManageWorkshopTabViewController *vc = (ManageWorkshopTabViewController * ) [self loadVCWithSidePanel:VIEW_MANAGE storyboard:STORYBOARD_MANAGEWORKSHOP];
    [vc view];
    
//    [vc performSelector: @selector(setSelectedIndex:) withObject: @(1) afterDelay: 0.1f];
//    [vc setSelectedViewController: [[vc viewControllers] objectAtIndex: 1]];
    [vc setSelectedIndex: 1];
}

#pragma mark - IN Performance
-(void) loadINPerformance
{
    if(GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER || GET_PROFILETYPE == kPROFILETYPE_MECHANIC || GET_PROFILETYPE == kPROFILETYPE_FORECOURTATTENDANT) {
         if ([[self profileInfo] hasMYPerformance] && [[self profileInfo] hasMYPoints]) {
             [self loadTabBarVCWithSidePanel: VIEW_IN_PERFORMANCE_TAB  storyboard:STORYBOARD_IN_PERFORMANCE];
         } else if([[self profileInfo] hasMYPerformance]) {
             [self loadTabBarVCWithSidePanel: VIEW_PERFORMANCE_DASHBOARD_NEW  storyboard:STORYBOARD_PERFORMANCE];
         } else {
             [self loadTabBarVCWithSidePanel: VIEW_PERFORMANCEPOINTS  storyboard:STORYBOARD_PERFORMANCE];
         }
    } else if(GET_PROFILETYPE == kPROFILETYPE_DSM && IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        [self loadVCWithSidePanel:VIEW_PARTNERSOFSHELL_POINTSEARNED_V2 storyboard:STORYBOARD_RUSSIA_V2];
    } else {
        if ([[self profileInfo] hasWorkshopPerformanceView] || [[self profileInfo] hasLeaderBoard])
            [self loadTabBarVCWithSidePanel: VIEW_IN_PERFORMANCE_TAB  storyboard:STORYBOARD_IN_PERFORMANCE];
        else
            [self loadTabBarVCWithSidePanel: VIEW_PERFORMANCE_DASHBOARD_NEW  storyboard:STORYBOARD_PERFORMANCE];
    }
}

-(void) pushINBreakdown:(BOOL)isProductBreakdown vc:(UIViewController *)vc{
    INBreakdownMainViewController *newvc = [STORYBOARD_IN_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_IN_PERFORMANCE_BREAKDOWN_MAIN];
    
    newvc.isProductBreakdown = isProductBreakdown;
    
    [self pushVC:vc  newVC:newvc];
}

-(void) pushINCustomerBreakdown:(UIViewController *)vc {
    UIViewController *newvc = [STORYBOARD_IN_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_IN_PERFORMANCE_CUSTOMERBREAKDOWN];
    
    [self pushVC:vc  newVC:newvc];
}

-(void) pushINMechBreakdown:(UIViewController *)vc {
    UIViewController *newvc = [STORYBOARD_IN_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_IN_PERFORMANCE_MECHBREAKDOWN];
    
    [self pushVC:vc  newVC:newvc];
}

-(void) pushPerformanceDetails:(NSArray *) categoryArray
                       vcTitle:(NSString *) sectionName
              summaryDataArray:(NSArray *) summaryDataArray
               originalTradeId:(NSString*) originalTradeId
                 selectedIndex:(NSUInteger)selectedIndex
                            vc:(UIViewController *) vc;
{
    PerformanceDetails_NewViewController *newvc = [STORYBOARD_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_PERFORMANCE_DETAILS_NEW];
    
    newvc.mainCategoryArray = categoryArray;
    newvc.selectedIndex = selectedIndex;
    newvc.vcTitle = sectionName;
    newvc.summaryDataArray = summaryDataArray;
    newvc.originalTradeId = originalTradeId;
    
    [self pushVC:vc  newVC:newvc];
}

#pragma mark Image Preview VC
-(void) pushImagePreview: (UIViewController *) vc image: (UIImage *) image title:(NSString *) title
{
    ImagePreviewViewController *newVc = [STORYBOARD_MANAGEWORKSHOP instantiateViewControllerWithIdentifier: VIEW_IMAGEPREVIEW];
    newVc.title = title;
    newVc.previewImage = image;
    
    [self pushVC: vc newVC: newVc];
}
-(void) pushImagePreview: (UIViewController *) vc image: (UIImage *) image
{
    [self pushImagePreview:vc image:image title:@""];
}
#pragma mark - SDA
-(void) loadSDA:(UIViewController *)vc
{
    BaseVC *presentVC = (BaseVC *)vc;
    NSString *contentString = LOCALIZATION(C_POPUP_SDA_CONTENT);
        
    [presentVC popupInfoVCWithTitle:LOCALIZATION(C_POPUP_SWA_TITLE) content: contentString btnTitle:LOCALIZATION(C_POPUP_SWA_BTN) onYesPressed:^(BOOL finished) {
        
        if([[self.profileInfo distributorAcademyURL] isKindOfClass:[NSNull class]] || [[self.profileInfo distributorAcademyURL] isEqualToString:@""])
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString:@"http://shell-stg.ersg-staging.com/ShareSSOTH"]];
        else
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[self.profileInfo distributorAcademyURL]]];

    }];
}

#pragma mark - SWA
-(void) loadSWA:(UIViewController *)vc
{
    BaseVC *presentVC = (BaseVC *)vc;
    NSString *contentString = LOCALIZATION(C_POPUP_SWA_CONTENT);
        
    [presentVC popupInfoVCWithTitle:LOCALIZATION(C_POPUP_SWA_TITLE) content: contentString btnTitle:LOCALIZATION(C_POPUP_SWA_BTN) onYesPressed:^(BOOL finished) {
        
        if([[self.profileInfo workshopAcademyURL] isKindOfClass:[NSNull class]] || [[self.profileInfo workshopAcademyURL] isEqualToString:@""])
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString:@"http://shell-stg.ersg-staging.com/ShareSSO"]];
        else
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[self.profileInfo workshopAcademyURL]]];

    }];
}

#pragma mark - HomeV2
-(void) pushHomePromotionsWithSelectedIndex:(NSInteger)selectedIndex onVc:(UIViewController *)vc {
    
    HomePromotionViewController *newVc = [STORYBOARD_MAINV2 instantiateViewControllerWithIdentifier: VIEW_HOMEPROMOTION];
    [newVc setModalPresentationStyle:UIModalPresentationFullScreen];
    newVc.selectedIndex = selectedIndex;
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:newVc];
    [vc presentViewController:navController animated:YES completion:nil];
    
}

-(void) pushPromoDetailsWith:(NSDictionary*) promoDetails andIsPromotion:(BOOL)isPromotion onVc:(UIViewController *)vc {
    
    HomePromotionsDetailViewController *newVC = [STORYBOARD_MAINV2 instantiateViewControllerWithIdentifier:VIEW_HOMEPROMOTION_DETAIL];
    newVC.promoDetails = promoDetails;
    newVC.isPromotion = isPromotion;
    [self pushVC:vc newVC:newVC];
    
}

#pragma mark - lubeMatch
-(void) loadLubeMatch {
    [self loadVCWithSidePanel:VIEW_REGISTEROILCHANGE_SEARCHVIEW storyboard:STORYBOARD_REGISTEROILCHANGE];
}

-(void) pushLubematchPopupWith:(NSArray *)lubematchArray forVehicleId:(NSString *)vehicleID onVc:(UIViewController *)vc {
    RegisterOilChangeLubematchViewController *newVC = [STORYBOARD_REGISTEROILCHANGE instantiateViewControllerWithIdentifier:VIEW_REGISTEROILCHANGE_LUBEMATCH_POPUP];
//    newVC.vehicleLubeArray = lubematchArray;
    newVC.vehicleID = vehicleID;
    newVC.parentVC = vc;
    
    [vc addChildViewController: newVC];
    [vc.view addSubview:newVC.view];
    [newVC didMoveToParentViewController: vc];
    
    newVC.view.accessibilityViewIsModal = YES;
    newVC.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
}

-(void) pushRegisterOilChangeFor:(NSString *)vehicleID onVc:(UIViewController *)vc {
    RegisterOilChangeScanViewController *newVC = [STORYBOARD_REGISTEROILCHANGE instantiateViewControllerWithIdentifier:VIEW_REGISTEROILCHANGE_SCANVIEW];
    newVC.vehicleID = vehicleID;
    [self pushVC:vc newVC:newVC];
}

-(void) showCoachMark:(NSArray *)coachMarkData withDelegate:(UIViewController<CoachMarkDelegate> *)delegate onVc:(UIViewController *)vc {
    
    CoachMarkViewController *newVc = [STORYBOARD_MAINV2 instantiateViewControllerWithIdentifier:VIEW_COACHMARK];
    newVc.coachMarkData = coachMarkData;
    newVc.delegate = delegate;
    newVc.view.opaque = NO;
    [newVc.view setBackgroundColor: [UIColor clearColor]];
    newVc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [newVc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [vc presentViewController:newVc animated:YES completion:nil];
    
}

#pragma mark - Lebaran Promotion
-(void) presentScratchViewOn:(UIViewController *)vc forGamification:(BOOL) isGamification {
    PromotionScratchViewController *newVc = [STORYBOARD_PROMOTIONS instantiateViewControllerWithIdentifier:VIEW_PROMOTIONSCRATCH];
    newVc.delegate = vc;
    newVc.isGamification = isGamification;
    newVc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [newVc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [vc presentViewController:newVc animated:YES completion:nil];
}

-(void) pushPromotionViewDetailsWithData:(NSDictionary *)data onVc:(UIViewController *)vc {
    PromotionDetailsViewController *newVc = [STORYBOARD_PROMOTIONS instantiateViewControllerWithIdentifier:VIEW_PROMOTIONDETAILS];
    newVc.data = data;
    [self pushVC:vc newVC:newVc];
}

-(void) pushCheckInTodayPopuponVc:(UIViewController *)vc {
    CheckInViewController *newVc = [STORYBOARD_MAINV2 instantiateViewControllerWithIdentifier:VIEW_CHECKIN];
    newVc.delegate = vc;
    newVc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [newVc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [vc presentViewController:newVc animated:YES completion:nil];
}

#pragma mark - AI
-(void) popupAIPage:(UIViewController *)vc {
    AIHomeViewController *newVc = [STORYBOARD_AI instantiateViewControllerWithIdentifier: VIEW_AIHOME];
    [newVc setModalPresentationStyle:UIModalPresentationFullScreen];

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:newVc];
    [vc presentViewController:navController animated:YES completion:nil];
}

#pragma mark - Ebooking

-(void) loadTOEbooking {
    [self loadVCWithSidePanel:VIEW_ALLBOOKING_LIST storyboard: STORYBOARD_IN_ALLBOOKLIST];
}



#pragma mark - Inventory Management/Moscow
-(void) loadDSRInventoryManagement {
    [self loadVCWithSidePanel:VIEW_INVENTORY_WORKSHOPLIST storyboard: STORYBOARD_RUSSIA_V2];
}

-(void) loadTOInventoryManagement {
    [self loadVCWithSidePanel:VIEW_INVENTORY_TABVIEW storyboard: STORYBOARD_RUSSIA_V2];
}

-(void) pushInventoryScanResultWith:(NSArray *)data onVc:(UIViewController *)vc {
    InventoryScanResultViewController *newVC = [STORYBOARD_RUSSIA_V2 instantiateViewControllerWithIdentifier:VIEW_INVENTORY_SCANRESULT_VIEW];
    newVC.inventoryManagementTabView = (InventoryManagementTabViewController *)vc;
    newVC.resultData = data;
    [self pushVC:vc newVC:newVC];
}

-(void) pushInventoryForSelectedWorkshopon:(UIViewController *)vc {
    InventoryListViewController *newVC = [STORYBOARD_RUSSIA_V2 instantiateViewControllerWithIdentifier:VIEW_INVENTORY_LISTVIEW];
    newVC.isPushed = true;
    [self pushVC:vc newVC:newVC];
}

-(void) pushDSRInventoryScanOn:(UIViewController *)vc {
    InventoryScanViewController *newVC = [STORYBOARD_RUSSIA_V2 instantiateViewControllerWithIdentifier:VIEW_INVENTORY_SCANVIEW];
    newVC.isPushed = true;
    [self pushVC:vc newVC:newVC];
}

#pragma mark - Total Earnings/Moscow
-(void) loadTotalEarnings {
    [self loadVCWithSidePanel:VIEW_PARTNERSOFSHELL_POINTSEARNED_V2 storyboard: STORYBOARD_RUSSIA_V2];
}


#pragma mark CustomMethods
- (void)saveLoginToken:(NSString *)token showAlert:(BOOL)show
{
    if (token)
    {
        [[self getSecureStoreManager] setKeyValue:token forKey:LOGIN_TOKEN];
    }
    else
    {
        [[self getSecureStoreManager]  deleteKeyValue:LOGIN_TOKEN];
    }
}

#pragma mark - Look up Table Methods
- (NSArray *) getCountryDialingCodes
{
    NSMutableArray *countryDialingCodeArray = [[NSMutableArray alloc] init];
    for (NSDictionary *countryDict in [[mSession lookupTable] objectForKey: @"CountryDialCode"])
    {
        [countryDialingCodeArray addObject: [countryDict objectForKey: @"CountryDialCode"]];
    }
    return countryDialingCodeArray;
}

- (NSArray *) getCountryNames
{
    NSMutableArray *countryNameArray = [[NSMutableArray alloc] init];
    for (NSDictionary *countryDict in [[mSession lookupTable] objectForKey: @"CountryDialCode"])
    {
        [countryNameArray addObject: [countryDict objectForKey: @"CountryName"]];
    }
    return countryNameArray;
}

- (NSString *) convertDialCodeToCountryCode: (NSString*) dialingCode
{
    //dialingCode = "+65", "+60", etc. from @"CountryDialCode" values
    for (NSDictionary *countryDict in [[mSession lookupTable] objectForKey: @"CountryDialCode"])
    {
        
        if ([[countryDict objectForKey: @"CountryDialCode"] isEqualToString: dialingCode])
        {
            return [countryDict objectForKey: @"CountryCode"];
        }
    }
    return @"";
}

- (NSString *) convertDialCodeToCountryName: (NSString*) dialingCode
{
    //dialingCode = "+65", "+60", etc. from @"CountryDialCode" values
    for (NSDictionary *countryDict in [[mSession lookupTable] objectForKey: @"CountryDialCode"])
    {
        if ([[countryDict objectForKey: @"CountryDialCode"] isEqualToString: dialingCode])
        {
            return [countryDict objectForKey: @"CountryName"];
        }
    }
    return @"";
}

- (NSString *) convertCountryNameToCountryCode: (NSString*) countryName
{
    for (NSDictionary *countryDict in [[mSession lookupTable] objectForKey: @"CountryDialCode"])
    {
        if ([[countryDict objectForKey: @"CountryName"] isEqualToString: countryName])
        {
            return [countryDict objectForKey: @"CountryCode"];
        }
    }
    return @"";
}

- (NSString *) convertCountryCodeToCountryName: (NSString*) countryCode
{
    for (NSDictionary *countryDict in [[mSession lookupTable] objectForKey: @"CountryDialCode"])
    {
        if ([[countryDict objectForKey: @"CountryCode"]  isEqualToString: countryCode])
        {
            return [countryDict objectForKey: @"CountryName"];
        }
    }
    return @"";
}


- (NSString *) convertCountryCodeToCountryDialingCode: (NSString*) countryCode
{
    for (NSDictionary *countryDict in [[mSession lookupTable] objectForKey: @"CountryDialCode"])
    {
        if ([[countryDict objectForKey: @"CountryCode"]  isEqualToString: countryCode])
        {
            return [countryDict objectForKey: @"CountryDialCode"];
        }
    }
    return @"";
}

- (NSString *) convertCountryCodeToCountryDisplayName: (NSString*) countryCode
{
    for (NSDictionary *countryDict in [[mSession lookupTable] objectForKey: @"CountryDialCode"])
    {
        if ([[countryDict objectForKey: @"CountryCode"]  isEqualToString: countryCode])
        {
            return [countryDict objectForKey: @"CountryDisplayName"];
        }
    }
    return @"";
}

- (NSString *) convertCountryDisplayNameToCountryCode: (NSString*) countryName
{
    for (NSDictionary *countryDict in [[mSession lookupTable] objectForKey: @"CountryDialCode"])
    {
        if ([[countryDict objectForKey: @"CountryDisplayName"] isEqualToString: countryName])
        {
            return [countryDict objectForKey: @"CountryCode"];
        }
    }
    return @"";
}

#pragma Workshop offer Look up method
//- (NSString *) convertOfferCodeToWorkshopOfferCode: (NSString*) offerCode
//{
//    for (NSDictionary *offerDict in [[mSession lookupTable] objectForKey: @"AncillaryOffer"])
//    {
//        if ([[offerDict objectForKey: @"KeyValue"]  isEqualToString: offerCode])
//        {
//            return [offerDict objectForKey: @"KeyCode"];
//        }
//    }
//    return @"";
//}

- (NSArray *) getWorkshopOfferArray
{
    return [self getKeyValues: @"AncillaryOffer"];
}

- (NSString *) convertWorkshopOffer: (NSInteger) workshopOfferCode
{
    return [self convertToKeyValue: workshopOfferCode lookupTableKey: @"AncillaryOffer"];
}

- (NSString *) convertToWorkshopOfferKeyCode: (NSString *) workshopOfferType
{
    return [self convertKeyValueToKeyCode: workshopOfferType lookupTableKey: @"AncillaryOffer"];
}

-(NSArray *)getWorkshopOfferProduct
{
    return [self getKeyValues: @"AncilliaryProductOffer"];
}

- (NSString *) convertToWorkshopProduct: (NSInteger) workshopOfferProductKeyCode
{
    return [self convertToKeyValue: workshopOfferProductKeyCode lookupTableKey: @"AncilliaryProductOffer"];
}

- (NSString *) convertToWorkshopProductKeyCode:(NSString *)workshopOfferProduct
{
     return [self convertKeyValueToKeyCode: workshopOfferProduct lookupTableKey: @"AncilliaryProductOffer"];
}

-(NSArray *)getWorkshopOfferService
{
    return [self getKeyValues: @"AncillaryService"];
}

- (NSString *) convertToWorkshopService: (NSInteger) workshopOfferServiceKeyCode
{
    return [self convertToKeyValue: workshopOfferServiceKeyCode lookupTableKey: @"AncillaryService"];
}

- (NSString *) convertToWorkshopServiceKeyCode:(NSString *)workshopOfferService
{
    return [self convertKeyValueToKeyCode: workshopOfferService lookupTableKey: @"AncillaryService"];
}

-(NSArray *)getAncilliaryOfferType
{
    return [self getKeyValues: @"AncilliaryOfferType"];
}

- (NSString *) convertToAncilliaryOfferTypeKeyCode:(NSString *)ancilliaryOfferType
{
    return [self convertKeyValueToKeyCode: ancilliaryOfferType lookupTableKey: @"AncilliaryOfferType"];
}

-(NSArray *)getAncilliaryOfferStatus
{
    return [self getKeyValues: @"AncilliaryOfferStatus"];
}

- (NSString *) convertToAncilliaryOfferStatusKeyCode:(NSString *)ancilliaryOfferStatus
{
    return [self convertKeyValueToKeyCode: ancilliaryOfferStatus lookupTableKey: @"AncilliaryOfferStatus"];
}

- (NSString *) convertToAncilliaryOfferStatus: (NSInteger) workshopOfferStatusKeyCode
{
    return [self convertToKeyValue: workshopOfferStatusKeyCode lookupTableKey: @"AncilliaryOfferStatus"];
}


-(NSArray *)getOfferPeriod
{
    return [self getKeyValues: @"OfferPeriod"];
}

- (NSString *) convertToOfferPeriodTypeKeyCode:(NSString *)offerPeriod
{
    return [self convertKeyValueToKeyCode: offerPeriod lookupTableKey: @"OfferPeriod"];
}


#pragma mark Salutations Lookup methods
- (NSArray *) getSalutations
{
    return [self getKeyValues: @"Salutation"];
}

- (NSString *) convertToSalutation: (NSInteger) salutationKeyCode
{
    return [self convertToKeyValue: salutationKeyCode lookupTableKey: @"Salutation"];
}

- (NSString *) convertToSalutationKeyCode: (NSString *) salutation
{
    return [self convertKeyValueToKeyCode: salutation lookupTableKey: @"Salutation"];
}

#pragma mark Staff Account Type Lookup methods
- (NSArray *) getStaffAccountType
{
    return [self getKeyValues: @"StaffAccountType"];
}

- (NSString *) convertToStaffAccountType:(NSInteger)staffAccountTypeCode
{
    return [self convertToKeyValue: staffAccountTypeCode lookupTableKey: @"StaffAccountType"];
}

- (NSString *) convertToStaffAccountTypeCode:(NSString *)staffAccountType
{
    return [self convertKeyValueToKeyCode: staffAccountType lookupTableKey: @"StaffAccountType"];
}

#pragma mark Company Types Lookup methods
- (NSArray *) getCompanyTypes
{
    return [self getKeyValues: @"CompanyType"];
}
- (NSString *) convertToCompanyTypes: (NSInteger) companyTypeKeyCode
{
    return [self convertToKeyValue: companyTypeKeyCode lookupTableKey: @"CompanyType"];
}
- (NSString *) convertToCompanyTypesKeyCode: (NSString *) companyType
{
    return [self convertKeyValueToKeyCode: companyType lookupTableKey: @"CompanyType"];
}

- (NSArray *) getCompanyCategory
{
    return [self getKeyValues: @"CompanyCategory"];
}
- (NSString *) convertToCompanyCategory: (NSInteger) companyCategoryKeyCode
{
    return [self convertToKeyValue: companyCategoryKeyCode lookupTableKey: @"CompanyCategory"];
}
- (NSString *) convertToCompanyCategoryKeyCode: (NSString *) companyCategory
{
    return [self convertKeyValueToKeyCode: companyCategory lookupTableKey: @"CompanyCategory"];
}

#pragma mark Gender Lookup methods
- (NSArray *) getGender
{
    return [self getKeyValues: @"Gender"];
}
- (NSString *) convertToGenderKeyCode: (NSString *) gender
{
    return [self convertKeyValueToKeyCode: gender lookupTableKey: @"Gender"];
}


#pragma mark Contact Preferences Lookup methods
- (NSArray *) getContactPreference
{
    return [self getKeyValues: @"ContactPreference"];
}
- (NSString *) convertToContactPreference: (NSInteger) contactPrefKeyCode
{
    return [self convertToKeyValue: contactPrefKeyCode lookupTableKey: @"ContactPreference"];
}
- (NSString *) convertToContactPreferenceKeyCode: (NSString *) contactPref
{
    return [self convertKeyValueToKeyCode: contactPref lookupTableKey: @"ContactPreference"];
}

#pragma mark Enquiry Type Lookup methods
- (NSArray *) getEnquiryType
{
    return [self getKeyValues: @"Contact_Enquiry"];
}
- (NSString *) convertToEnquiryType: (NSInteger) enquiryTypeKeyCode
{
    return [self convertToKeyValue: enquiryTypeKeyCode lookupTableKey: @"Contact_Enquiry"];
}
- (NSString *) convertToEnquiryTypeKeyCode: (NSString *) enquiryTypeString
{
    return [self convertKeyValueToKeyCode: enquiryTypeString lookupTableKey: @"Contact_Enquiry"];
}

#pragma mark Company Status Lookup methods (for Pending Workshops)
- (NSArray *) getCompanyStatus
{
    return [self getKeyValues: @"CompanyStatus"];
}

- (NSString *) convertToCompanyStatus: (NSInteger) statusKeyCode
{
    return [self convertToKeyValue: statusKeyCode lookupTableKey: @"CompanyStatus"];
}

- (NSString *) convertToCompanyStatusKeyCode: (NSString *) companyStatus
{
    return [self convertKeyValueToKeyCode: companyStatus lookupTableKey: @"CompanyStatus"];
}

- (NSArray *) getLeadStatus
{
    return [self getKeyValues: @"LeadStatus"];
}

- (NSString *) convertToLeadStatus: (NSInteger) statusKeyCode
{
    return [self convertToKeyValue: statusKeyCode lookupTableKey: @"LeadStatus"];
}

- (NSString *) convertToLeadStatusKeyCode: (NSString *) leadStatus
{
    return [self convertKeyValueToKeyCode: leadStatus lookupTableKey: @"LeadStatus"];
}

#pragma mark Mechanic Status Lookup methods (for Mechanic - not approved)
- (NSArray *) getMechanicStatus
{
    return [self getKeyValues: @"MechanicStatus"];
}

- (NSString *) convertToMechanicStatus: (NSInteger) statusKeyCode
{
    return [self convertToKeyValue: statusKeyCode lookupTableKey: @"MechanicStatus"];
}

- (NSString *) convertToMechanicStatusKeyCode: (NSString *) mechanicStatus
{
    return [self convertKeyValueToKeyCode: mechanicStatus lookupTableKey: @"MechanicStatus"];
}

#pragma mark Trade Status Lookup methods (for Mechanic - approved)
- (NSArray *) getTradeStatus
{
    return [self getKeyValues: @"TradeStatus"];
}

- (NSString *) convertToTradeStatus: (NSInteger) statusKeyCode
{
    return [self convertToKeyValue: statusKeyCode lookupTableKey: @"TradeStatus"];
}

- (NSString *) convertToTradeStatusKeyCode: (NSString *) tradeStatus
{
    return [self convertKeyValueToKeyCode: tradeStatus lookupTableKey: @"TradeStatus"];
}

#pragma mark Transaction Types Lookup methods
- (NSArray *) getTransactionTypes
{
    return [self getKeyValues: @"TransactionType"];
}
- (NSString *) convertToTransactionTypes: (NSInteger) transactionTypeKeyCode
{
    return [self convertToKeyValue: transactionTypeKeyCode lookupTableKey: @"TransactionType"];
}
- (NSString *) convertToTransactionTypesKeyCode: (NSString *) transactionType
{
    return [self convertKeyValueToKeyCode: transactionType lookupTableKey: @"TransactionType"];
}

#pragma mark Workshop Status Lookup methods (active,lasped,inactive)
- (NSArray *) getWorkshopStatus
{
    return [self getKeyValues: @"WorkshopStatus"];
}
- (NSString *) convertToWorkshopStatus: (NSInteger) workshopStatusKeyCode
{
    return [self convertToKeyValue: workshopStatusKeyCode lookupTableKey: @"WorkshopStatus"];
}
- (NSString *) convertToWorkshopStatusKeyCode: (NSString *) workshopStatus
{
    return [self convertKeyValueToKeyCode: workshopStatus lookupTableKey: @"WorkshopStatus"];
}

#pragma mark Workshop Profile Type in Workshop List
- (NSArray *) getWorkshopProfileTypes
{
    return [self getKeyValues: @"ProfileTypeFilter"];
}
- (NSString *) convertToWorkshopProfileTypes: (NSInteger) workshopProfileTypeKeyCode
{
    return [self convertToKeyValue: workshopProfileTypeKeyCode lookupTableKey: @"ProfileTypeFilter"];
}
- (NSString *) convertToWorkshopProfileTypesKeyCode: (NSString *) workshopProfileType
{
    return [self convertKeyValueToKeyCode: workshopProfileType lookupTableKey: @"ProfileTypeFilter"];
}

#pragma mark Company Source Lookup methods
- (NSArray *) getCompanySource
{
    return [self getKeyValues: @"CompanySource"];
}
- (NSString *) convertToCompanySource: (NSInteger) companySourceKeyCode
{
    return [self convertToKeyValue: companySourceKeyCode lookupTableKey: @"CompanySource"];
}
- (NSString *) convertToCompanySourceKeyCode: (NSString *) companySource
{
    return [self convertKeyValueToKeyCode: companySource lookupTableKey: @"CompanySource"];
}

#pragma mark Rewards Points Lookup methods
- (NSArray *) getRewardsPoints
{
    return [self getKeyValues: @"RewardsPoints"];
}
- (NSString *) convertToRewardsPoints: (NSInteger) rewardsPointsKeyCode
{
    return [self convertToKeyValue: rewardsPointsKeyCode lookupTableKey: @"RewardsPoints"];
}
- (NSString *) convertToRewardsPointsKeyCode: (NSString *) rewardsPoints
{
    return [self convertKeyValueToKeyCode: rewardsPoints lookupTableKey: @"RewardsPoints"];
}

#pragma mark Rewards Points Lookup methods
- (NSArray *) getRedemptionType
{
    return [self getKeyValues: @"RedemptionOfferType"];
}
- (NSString *) convertToRedemptionType: (NSInteger) redemptionTypeKeyCode
{
    return [self convertToKeyValue: redemptionTypeKeyCode lookupTableKey: @"RedemptionOfferType"];
}
- (NSString *) convertToRedemptionTypeKeyCode: (NSString *) redemptionType
{
    return [self convertKeyValueToKeyCode: redemptionType lookupTableKey: @"RedemptionOfferType"];
}

#pragma mark Rewards Category Lookup methods
- (NSArray *) getRewardsCategory
{
    return [self getKeyValues: @"RewardsCategory"];
}
- (NSString *) convertToRewardsCategory: (NSInteger) rewardsCategoryKeyCode
{
    return [self convertToKeyValue: rewardsCategoryKeyCode lookupTableKey: @"RewardsCategory"];
}
- (NSString *) convertToRewardsCategoryKeyCode: (NSString *) rewardsCategory
{
    return [self convertKeyValueToKeyCode: rewardsCategory lookupTableKey: @"RewardsCategory"];
}

#pragma Rewards V2 Lookup methods
- (NSArray *) getRedeemStatus
{
    return [self getKeyValues: @"RedemStatus"];
}

- (NSString *) convertRedeemStatus: (NSInteger) redeemStatusCode
{
    return [self convertToKeyValue: redeemStatusCode lookupTableKey: @"RedemStatus"];
}

- (NSString *) convertToRedeemStatusKeyCode: (NSString *) redeemStatus
{
    return [self convertKeyValueToKeyCode: redeemStatus lookupTableKey: @"RedemStatus"];
}

- (NSArray *) getRedeemSort
{
    return [self getKeyValues: @"RedemSort"];
}

- (NSString *) convertRedeemSort: (NSInteger) redeemSortCode
{
    return [self convertToKeyValue: redeemSortCode lookupTableKey: @"RedemSort"];
}

- (NSString *) convertToRedeemSortKeyCode: (NSString *) redeemSort
{
    return [self convertKeyValueToKeyCode: redeemSort lookupTableKey: @"RedemSort"];
}

- (NSArray *) getRedeemDate
{
    return [self getKeyValues: @"RedemDate"];
}

- (NSString *) convertRedeemDate: (NSInteger) redeemDateCode
{
    return [self convertToKeyValue: redeemDateCode lookupTableKey: @"RedemDate"];
}

- (NSString *) convertToRedeemDateKeyCode: (NSString *) redeemDate
{
    return [self convertKeyValueToKeyCode: redeemDate lookupTableKey: @"RedemDate"];
}

#pragma mark manage order Lookup methods
- (NSArray *) getOrderStatus
{
    return [self getKeyValues: @"OrderStatus"];
}
- (NSString *) convertToOrderStatus: (NSInteger) orderStatusKeyCode
{
    return [self convertToKeyValue: orderStatusKeyCode lookupTableKey: @"OrderStatus"];
}
- (NSString *) convertToOrderStatusKeyCode: (NSString *) orderStatusType
{
    return [self convertKeyValueToKeyCode: orderStatusType lookupTableKey: @"OrderStatus"];
}


- (NSArray *) getOrderDate
{
    return [self getKeyValues: @"OrderDate"];
}
- (NSString *) convertToOrderDateType: (NSInteger) orderDateKeyCode
{
    return [self convertToKeyValue: orderDateKeyCode lookupTableKey: @"OrderDate"];
}
- (NSString *) convertToOrderDateKeyCode: (NSString *) orderStatusType
{
    return [self convertKeyValueToKeyCode: orderStatusType lookupTableKey: @"OrderDate"];
}

-(NSString *)getDisclaimerMsgForMyCart
{
    return [self getMSG: @"OrderDisclaimerText"];
}

#pragma mark - Company Profiling lookups

- (NSArray *) getCompanyProfService
{
    return [self getKeyValues: @"CompanyProfService"];
}
- (NSString *) convertToCompanyProfService: (NSInteger) companyProfServiceKeyCode
{
    return [self convertToKeyValue: companyProfServiceKeyCode lookupTableKey: @"CompanyProfService"];
}
- (NSString *) convertToCompanyProfServiceKeyCode: (NSString *) companyProfService
{
    return [self convertKeyValueToKeyCode: companyProfService lookupTableKey: @"CompanyProfService"];
}

- (NSArray *) getCompanyProfPackSize
{
    return [self getKeyValues: @"CompanyProfPackSize"];
}
- (NSString *) convertToCompanyProfPackSize: (NSInteger) companyProfPackSizeKeyCode
{
    return [self convertToKeyValue: companyProfPackSizeKeyCode lookupTableKey: @"CompanyProfPackSize"];
}
- (NSString *) convertToCompanyProfPackSizeKeyCode: (NSString *) companyProfPackSize
{
    return [self convertKeyValueToKeyCode: companyProfPackSize lookupTableKey: @"CompanyProfPackSize"];
}

- (NSArray *) getCompanyProfMainBrand
{
    return [self getKeyValues: @"CompanyProfMainBrand"];
}
- (NSString *) convertToCompanyProfMainBrand: (NSInteger) companyProfMainBrandKeyCode
{
    return [self convertToKeyValue: companyProfMainBrandKeyCode lookupTableKey: @"CompanyProfMainBrand"];
}
- (NSString *) convertToCompanyProfMainBrandKeyCode: (NSString *) companyProfMainBrand
{
    return [self convertKeyValueToKeyCode: companyProfMainBrand lookupTableKey: @"CompanyProfMainBrand"];
}

- (NSArray *) getCompanyProfBuy
{
    return [self getKeyValues: @"CompanyProfBuy"];
}
- (NSString *) convertToCompanyProfBuy: (NSInteger) companyProfBuyKeyCode
{
    return [self convertToKeyValue: companyProfBuyKeyCode lookupTableKey: @"CompanyProfBuy"];
}
- (NSString *) convertToCompanyProfBuyKeyCode: (NSString *) companyProfBuy
{
    return [self convertKeyValueToKeyCode: companyProfBuy lookupTableKey: @"CompanyProfBuy"];
}

#pragma mark - Library filter
- (NSArray *) getDateRange
{
    return [self getKeyValues: @"DateRange"];
}
- (NSString *) convertToDateRange: (NSInteger) dateRange
{
    return [self convertToKeyValue: dateRange lookupTableKey: @"DateRange"];
}
- (NSString *) convertToDateRangeKeyCode: (NSString *) dateRange
{
    return [self convertKeyValueToKeyCode: dateRange lookupTableKey: @"DateRange"];
}

#pragma mark - Vehicle Type Lookup (VN Rimula)
- (NSArray *) getVehicleType
{
    return [self getKeyValues: @"VehicleType"];
}
- (NSString *) convertToVehicleType: (NSInteger) vehicleType
{
    return [self convertToKeyValue: vehicleType lookupTableKey: @"VehicleType"];
}
- (NSString *) convertToVehicleTypeKeyCode: (NSString *) vehicleType
{
    return [self convertKeyValueToKeyCode: vehicleType lookupTableKey: @"VehicleType"];
}


#pragma mark - Register Consumer Lookup
- (NSArray *) getVehicleUsage
{
    return [self getKeyValues: @"VehicleUsage"];
}
- (NSString *) convertToVehicleUsage: (NSInteger) vehicleUsage
{
    return [self convertToKeyValue: vehicleUsage lookupTableKey: @"VehicleUsage"];
}
- (NSString *) convertToVehicleUsageKeyCode: (NSString *) vehicleUsage
{
    return [self convertKeyValueToKeyCode: vehicleUsage lookupTableKey: @"VehicleUsage"];
}

- (NSArray *) getYearOfManufacture
{
    return [self getKeyValues: @"YearOfManufacture"];
}
- (NSString *) convertToYearOfManufacture: (NSInteger) yearOfManufacture
{
    return [self convertToKeyValue: yearOfManufacture lookupTableKey: @"YearOfManufacture"];
}
- (NSString *) convertToYearOfManufactureKeyCode: (NSString *) yearOfManufacture
{
    return [self convertKeyValueToKeyCode: yearOfManufacture lookupTableKey: @"YearOfManufacture"];
}

#pragma mark Russia Vehicle Make
- (NSArray *) getVehicleMake
{
    return [self getKeyValues: @"VehicleMake"];
}
- (NSString *) convertToVehicleMake: (NSInteger) vehicleMake
{
    return [self convertToKeyValue: vehicleMake lookupTableKey: @"VehicleMake"];
}
- (NSString *) convertToVehicleMakeKeyCode: (NSString *) vehicleMake
{
    return [self convertKeyValueToKeyCode: vehicleMake lookupTableKey: @"VehicleMake"];
}

#pragma mark India
- (NSArray *) getBanksDetailsList
{
    return [self getKeyValues: @"BanksDetailsList"];
}
- (NSString *) convertToBankName: (NSInteger) bankName
{
    return [self convertToKeyValue: bankName lookupTableKey: @"BanksDetailsList"];
}
- (NSString *) convertToBankNameKeyCode: (NSString *) bankName
{
    return [self convertKeyValueToKeyCode: bankName lookupTableKey: @"BanksDetailsList"];
}

#pragma mark Cash Incentive
- (NSArray *) getCashStatus
{
    return [self getKeyValues:@"CashStatus"];
}
- (NSString *) convertToCashStatus: (NSInteger) status
{
    return [self convertToKeyValue:status lookupTableKey:@"CashStatus"];
}
- (NSString *) convertToCashStatusKeyCode: (NSString *) status
{
    return [self convertKeyValueToKeyCode:status lookupTableKey: @"CashStatus"];
}

- (NSArray *) getCashServiceType
{
    return [self getKeyValues:@"CashServiceType"];
}
- (NSString *) convertToCashServiceType: (NSInteger) serviceType
{
    return [self convertToKeyValue:serviceType lookupTableKey:@"CashServiceType"];
}
- (NSString *) convertToCashServiceTypeKeyCode: (NSString *) serviceType
{
    return [self convertKeyValueToKeyCode:serviceType lookupTableKey: @"CashServiceType"];
}

#pragma mark - Loyalty
- (NSArray *) getContractStatus {
    return [self getKeyValues: @"ContractStatus"];
}

- (NSString *) convertToContractStatus: (NSInteger) contractStatus{
    return [self convertToKeyValue: contractStatus lookupTableKey: @"ContractStatus"];
}

- (NSString *) convertToContractStatusKeyCode: (NSString *) contractStatus{
    return [self convertKeyValueToKeyCode: contractStatus lookupTableKey: @"ContractStatus"];
}


- (NSArray *) getProgramPeriod{
    return [self getKeyValues: @"ProgramPeriod"];
}

- (NSString *) convertToProgramPeriod: (NSInteger) programPeriod{
     return [self convertToKeyValue: programPeriod lookupTableKey: @"ProgramPeriod"];
}

- (NSString *) convertToProgramPeriodKeyCode: (NSString *) programPeriod{
    return [self convertKeyValueToKeyCode: programPeriod lookupTableKey: @"ProgramPeriod"];
}

#pragma mark Language List Lookup methods
-(void) setDefaultLanguage //use localization manager default.
{
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
    {
        [[LocalizationManager sharedInstance] setLanguage: [kThai lowercaseString]];
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDONESIA])
    {
        [[LocalizationManager sharedInstance] setLanguage: [kIndo lowercaseString]];
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
    {
        [[LocalizationManager sharedInstance] setLanguage: [kRussian lowercaseString]];
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_VIETNAM])
    {
        [[LocalizationManager sharedInstance] setLanguage: [kVietnamese lowercaseString]];
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_HONGKONG])
    {
        [[LocalizationManager sharedInstance] setLanguage: kChineseT_HK];
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_SAUDI])
    {
        [[LocalizationManager sharedInstance] setLanguage: kArabic];
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDIA])
    {
        [[LocalizationManager sharedInstance] setLanguage: kEnglishIndia];
    }
    else
        [[LocalizationManager sharedInstance] setLanguage: [kEnglish lowercaseString]];
}

- (NSArray *) getLanguageList
{
    NSMutableArray *languageArray = [[NSMutableArray alloc] init];
    for (NSDictionary *languageDict in self.languageTable)
    {
        [languageArray addObject: [languageDict objectForKey: @"LaguageName"]];
    }
    return languageArray;
}

- (NSString *) convertLanguageNameToSortCode: (NSString*) languageName
{
    for (NSDictionary *languageDict in self.languageTable)
    {
        NSString *languageString = [languageDict objectForKey: @"LaguageName"];
        
        if ([languageString isEqualToString: languageName])
        {
            return [languageDict objectForKey:@"SortCode"];
        }
    }
    return @"";
}

- (NSString *) convertLanguageNameToLanguageKeyCode: (NSString*) languageName
{
    for (NSDictionary *languageDict in self.languageTable)
    {
        NSString *languageString = [languageDict objectForKey: @"LaguageName"];
        
        if ([languageString isEqualToString: languageName])
        {
            return [languageDict objectForKey: @"LanguageCode"];
        }
    }
    return @"";
}

- (NSString *) convertLanguageKeyCodeToLanguageName: (NSString*) languageCode
{
    for (NSDictionary *languageDict in self.languageTable)
    {
        NSString *languageString = [languageDict objectForKey: @"LanguageCode"];
        
        if ([languageString isEqualToString: languageCode])
        {
            return [languageDict objectForKey: @"LaguageName"];
        }
    }
    return @"";
}

- (NSString *) convertLanguageSortCodeToKeyCode: (NSString*) sortCode
{
    for (NSDictionary *languageDict in self.languageTable)
    {
        NSString *languageString = [languageDict objectForKey: @"SortCode"];
        
        if ([languageString isEqualToString: sortCode])
        {
            return [languageDict objectForKey: @"LanguageCode"];
        }
    }
    return @"";
}

- (NSString *) convertLanguageKeyCodeToSortCode: (NSString*) languageCode
{
    for (NSDictionary *languageDict in self.languageTable)
    {
        NSString *languageString = [languageDict objectForKey: @"LanguageCode"];
        
        if ([languageString isEqualToString: languageCode])
        {
            return [languageDict objectForKey: @"SortCode"];
        }
    }
    return @"";
}

#pragma mark State Lookup methods
- (NSArray *) getStateNames: (NSArray *) stateDetailedListArray    //send stateData array here
{
    NSMutableArray *stateArray = [[NSMutableArray alloc] init];
    for (NSDictionary *stateDict in stateDetailedListArray)
    {
        [stateArray addObject: [stateDict objectForKey: @"KeyValue"]];
    }
    return stateArray;
}
- (NSString *) convertToStateName: (NSString *) stateKeyCode stateDetailedListArray:(NSArray *) stateDetailedListArray
{
    for (NSDictionary *dict in stateDetailedListArray)
    {
        if ([stateKeyCode isEqualToString: [dict objectForKey: @"KeyCode"]])
        {
            return [dict objectForKey: @"KeyValue"];
        }
    }
    return @"";
}
- (NSString *) convertToStateKeyCode: (NSString *) state stateDetailedListArray:(NSArray *) stateDetailedListArray
{
    for (NSDictionary *dict in stateDetailedListArray)
    {
        if ([state isEqualToString: [dict objectForKey: @"KeyValue"]])
        {
            return [dict objectForKey: @"KeyCode"];
        }
    }
    return @"";
}
//- (NSString *) convertKeyValueToKeyCode: (NSString *)keyValue lookupTableKey:(NSString*) lookupTableKey
//{
//    for (NSDictionary *dict in [[mSession lookupTable] objectForKey: lookupTableKey])
//    {
//        if ([keyValue isEqualToString: [dict objectForKey: @"KeyValue"]])
//        {
//            return [dict objectForKey: @"KeyCode"];
//        }
//    }
//    return @"";
//}
//
//- (NSString *) convertToKeyValue:(NSInteger) keyCode lookupTableKey:(NSString*) lookupTableKey
//{
//    for (NSDictionary *dict in [[mSession lookupTable] objectForKey: lookupTableKey])
//    {
//        if (keyCode == [[dict objectForKey: @"KeyCode"] intValue])
//        {
//            return [dict objectForKey: @"KeyValue"];
//        }
//    }
//    NSDictionary *defaultDict = [[[mSession lookupTable] objectForKey: lookupTableKey] objectAtIndex: 0];
//    
//    return [defaultDict objectForKey: @"KeyValue"];
//}

- (NSArray *) getKeyValues:(NSString*) lookupTableKey
{
    NSMutableArray *keyValueArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in [[mSession lookupTable] objectForKey: lookupTableKey])
    {
        [keyValueArray addObject: [dict objectForKey: @"KeyValue"]];
    }
    return keyValueArray;
}

-(NSString *)getMSG:(NSString *) lookupTableKey
{
    if([[mSession lookupTable] objectForKey: lookupTableKey] != nil){
        return [[mSession lookupTable] objectForKey: lookupTableKey];
    }else{
        return @"";
    }
}

- (NSString *) convertKeyValueToKeyCode: (NSString *)keyValue lookupTableKey:(NSString*) lookupTableKey
{
    for (NSDictionary *dict in [[mSession lookupTable] objectForKey: lookupTableKey])
    {
        if ([keyValue isEqualToString: [dict objectForKey: @"KeyValue"]])
        {
            return [dict objectForKey: @"KeyCode"];
        }
    }
    return @"";
}

- (NSString *) convertToKeyValue:(NSInteger) keyCode lookupTableKey:(NSString*) lookupTableKey
{
    for (NSDictionary *dict in [[mSession lookupTable] objectForKey: lookupTableKey])
    {
        if (keyCode == [[dict objectForKey: @"KeyCode"] intValue])
        {
            return [dict objectForKey: @"KeyValue"];
        }
    }
//    NSDictionary *defaultDict = [[[mSession lookupTable] objectForKey: lookupTableKey] objectAtIndex: 0];
    
//    return [defaultDict objectForKey: @"KeyValue"];
    return @"";
}

#pragma mark -
#pragma mark Network check handlers
- (void)reachabilityChanged
{
    isReachable = YES;
    
    if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)
    {
        isReachable = NO;
    }
}

-(BOOL) getReachability
{
    return isReachable;
}

#pragma mark -
#pragma mark Archive/UnArchive

- (NSString*) getFullFilePath:(NSString*)name
{
   // [MGLog debug:@"Limo-Func" class:NSStringFromClass([self class]) selector:NSStringFromSelector(_cmd) message:@""];
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *savePath = [paths objectAtIndex:0];
    NSString *theFileName = [NSString stringWithFormat:@"%@.setting", name];
    return [savePath stringByAppendingPathComponent: theFileName];
}

- (void)saveUser:(LoginInfo *)userInfo
{
    [mDataManager saveUser: userInfo];
}

- (LoginInfo *)loadUserProfile
{
    return [mDataManager loadUserProfile];
}

#pragma mark Tab Bar controllers
-(RDVTabBarController *) loadTabBarVCWithSidePanel:(NSString *)identifier storyboard:(UIStoryboard *) storyboard
{
    [UpdateHUD removeSpotifyHUD:CURRENT_VIEW];
    
    RDVTabBarController *vc = [storyboard instantiateViewControllerWithIdentifier:identifier];
    //    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    //    mainWindow.rootViewController = nav;
    sidePanel.centerPanel = [[UINavigationController alloc] initWithRootViewController:vc];
    mainWindow.rootViewController = sidePanel;
    [mainWindow makeKeyAndVisible];
    CATransition *animation = [CATransition animation];
    animation.type = kCATransitionFade;
    animation.duration = 0.4;
    [mainWindow.layer addAnimation:animation forKey:nil];
    
//    SideMenuViewController *sideMenuVC = [[[sidePanel leftPanel] childViewControllers] lastObject];
//    [sideMenuVC selectQuickLinkMenu: identifier];
    
    return vc;
}

//-(void) loadPromotions
//{
//    RDVTabBarController *vc = [self loadTabBarVCWithSidePanel: VIEW_PROMOTION];
//    vc.delegate = self;
//    
//    NSMutableArray *navControlArray = [[NSMutableArray alloc] init];
//    
//    //child VC of promotions //testing
//    UIViewController *homeNavigationController = [[UINavigationController alloc] initWithRootViewController: [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_PROMOTIONCODE]];
//    [navControlArray addObject: homeNavigationController];
//    UIViewController *vc2 = [[UINavigationController alloc] initWithRootViewController: [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_TNC]];
//    [navControlArray addObject: vc2];
//    UIViewController *vc3 = [[UINavigationController alloc] initWithRootViewController: [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_TNC]];
//    [navControlArray addObject: vc3];
//    //end test
//    
//    [vc setViewControllers: navControlArray];
//    NSArray *title = @[@"title", @"meow", @"woof"];
//    [self customizeTabBarForController: vc titleArray: title];
//}
//
//- (void)customizeTabBarForController:(RDVTabBarController *)tabBarController titleArray:(NSArray *) titleArray {
//
//    for (RDVTabBarItem *item in [[tabBarController tabBar] items]) {
//        [item setTitle: @"title"];
//        [item setBackgroundColor: COLOUR_RED];
//    }
//}

#pragma mark Tab Bar Delegates
-(void)tabBarController:(RDVTabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    NSLog(@"selected tabbar");
}

- (void)removeFile:(NSString *)name
{
    //[MGLog debug:@"Limo-Func" class:NSStringFromClass([self class]) selector:NSStringFromSelector(_cmd) message:@""];
    NSString *filePath = [self getFullFilePath:name];
    NSFileManager *manager = [NSFileManager defaultManager];
    [manager removeItemAtPath:filePath error:nil];
}

#pragma mark -
#pragma mark getters
- (UIWindow *)getMainWindow
{
    return mainWindow;
}



#pragma mark -
#pragma mark Reset App Data

- (void)reset
{
    [FileManager emptySandbox];
    isUserDataChanged = NO;
    
    [self removeFile:USER_PROFILE_ARCHIVE_NAME];
    SET_KEEPLOGIN(@(NO));
    SET_AUTHENTICATIONTOKEN(BLANK);
    SET_ISIMPROFILETYPE(NO);
    SET_ISADVANCE(NO);
    SET_DATESTORED_LATEST(@"");
    NSMutableDictionary *coachmarkData = [[NSMutableDictionary alloc] init];
    SET_COACHMARK_HASSHOWN(coachmarkData);
//    SET_COUNTRY_CODE(DEFAULT_COUNTRY_CODE);
    CLEAR_REWARDSCART;
    CLEAR_LOADSTATE;
    CLEAR_SCANNEDCODES;
    CLEAR_HOMEDATA;
    CLEAR_PROGRAMBREAKDOWN;
    [CrashlyticsKit setObjectValue: @"" forKey: @"Profile Type"]; //username as usertype
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //Reset Shortcut
    [self configureShortcutItems:NO];

    //Reset GA user properties
//    [self updateAnalyticsUserProperty];
}

#pragma mark -
#pragma mark Webservice delegates
- (void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_LOOKUPTABLE:
        {
#warning process lookup table
            NSLog(@"success");
            if (IS_DEVELOPMENT_VERSION || IS_STAGING_VERSION)
                NSLog(@"success lookup ------ %@", [response getJSON]);
            NSDictionary *lookupDict = [response getGenericResponse];
            
            if ([[lookupDict objectForKey: @"LoadData"] boolValue])
            {
                SET_LOOKUP_TABLE(lookupDict);
                self.lookupTable = lookupDict;
                SET_LANGUAGE_TABLE([self.lookupTable objectForKey: @"LanguageList"]);
                self.languageTable = [self.lookupTable objectForKey: @"LanguageList"];
                
                [[NSNotificationCenter defaultCenter] postNotificationName: @"Reloaded Lookup Table" object: self];
            }
        }
            break;
        case kWEBSERVICE_LANGUAGE:
        {
            NSDictionary *languageDict = [response getGenericResponse];
            
            if ([[languageDict objectForKey: @"LoadData"] boolValue])
            {
                SET_LANGUAGE_TABLE([self.lookupTable objectForKey: @"LanguageList"]);
                self.languageTable = [languageDict objectForKey: @"LanguageList"];
            }
        }
            break;
            
            
        case kWEBSERVICE_ISVALIDAUTH:
            if ([response getResponseCode] == kSuccessCode) {
                [mLookupManager loadLookupTable];
                [self configureShortcutItems:YES];
                [self performSelector:@selector(loadHomeView) withObject:nil afterDelay:3.0];
            }
            else if ([response getResponseCode] == kAppVersionOldVersionCode)
            {
                
            }
            else
            {
                [mSession reset]; //reset all login fields if keep log in is not enabled
                [mLookupManager loadLookupTable];
                [self performSelector:@selector(loadLoginView) withObject:nil afterDelay:3.0];
                [self removeFile:USER_PROFILE_ARCHIVE_NAME];
            }
            break;
        case kWEBSERVICE_FETCHHOME:

            [self setProfileInfo: [[ProfileInfo alloc] initWithData: [[response getGenericResponse] objectForKey: @"MyProfile"]]];
            [self configureShortcutItems:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"UpdateProfileInfo" object: nil];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"Unread Icon Changed" object: nil];
            [self delayHandlePushNotification];
            break;
        case kWEBSERVICE_LOOKUPTNC_CONSUMER_RU:
            if (![[[response getGenericResponse] objectForKey: @"TncText"] isKindOfClass: [NSNull class]]) //replace if not null
                SET_CONSUMER_TNC([response getGenericResponse]);
            break;
        case kWEBSERVICE_LOOKUPTNC_WORKSHOP_RU:
            if (![[[response getGenericResponse] objectForKey: @"TncText"] isKindOfClass: [NSNull class]]) //replace if not null
                SET_WORKSHOP_TNC([response getGenericResponse]);
            break;
        case kWEBSERVICE_LOOKUPTNC_DSR_RU:
            if (![[[response getGenericResponse] objectForKey: @"TncText"] isKindOfClass: [NSNull class]]) //replace if not null
                SET_DSR_TNC([response getGenericResponse]);
            break;
        default:
            break;
    }
}

- (void)processFailed:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_LOOKUPTABLE:
            if (IS_STAGING_VERSION)
                NSLog(@"no need to renew or fail to get lookup table");
            break;
        case kWEBSERVICE_LANGUAGE:
            if (IS_STAGING_VERSION)
                NSLog(@"no need to renew or fail to get language table");
            break;
        case kWEBSERVICE_ISVALIDAUTH:
            if (response.getResponseCode != kAppVersionOldVersionCode)
            {
                [mSession reset]; //reset all login fields if keep log in is not enabled
                [mLookupManager loadLookupTable];
                [self performSelector:@selector(loadLoginView) withObject:nil afterDelay:1.0];
                [self removeFile:USER_PROFILE_ARCHIVE_NAME];
            }
            break;
        case kWEBSERVICE_FETCHHOME:
//            notificationDict = nil;
//            [self delayHandlePushNotification];
            break;
        default:
            break;
            
    }
}

- (void)saveReports:(NSArray *)reports withCacheKey:(NSString *)cacheKey;
{
    if (reports == (id)[NSNull null]) {
        return;
    }

    NSString *filePath = [self getFullFilePath:cacheKey];
    NSData *theData = [NSKeyedArchiver archivedDataWithRootObject:reports];
    [NSKeyedArchiver archiveRootObject:theData toFile:filePath];
}

- (NSArray *)loadReportswithCacheKey:(NSString *)cacheKey;
{
    NSArray *reports = [NSArray array];
    NSString *filePath = [self getFullFilePath:cacheKey];
    NSData *theData = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    reports = [NSKeyedUnarchiver unarchiveObjectWithData:theData];
    return reports;

}

#pragma mark Image Handling

- (void)saveProfilePic:(UIImage *)image
{
    NSData *data = [NSData dataWithData:UIImageJPEGRepresentation(image, 0.6f)];
    NSString *fileName;
    fileName = @"ProfilePic.jpg";
    [FileManager saveFileWithName:fileName andData:data];
}

- (void)savePic:(UIImage *)image withCacheKey:(NSString *)cacheKey
{
    NSData *data = [NSData dataWithData:UIImageJPEGRepresentation(image, 0.6f)];
    NSString *fileName;
    fileName = [NSString stringWithFormat:@"%@.jpg",cacheKey];
    [FileManager saveFileWithName:fileName andData:data];
}

- (UIImage *)loadImageWithCacheKey:(NSString *)cacheKey withPlaceholder:(NSString *)fileName
{
    UIImage *placeholderImage = [[UIImage alloc]init];
    placeholderImage = [UIImage imageNamed:fileName];
    
    NSData *data = [FileManager readDataFromFileWithName:[NSString stringWithFormat:@"%@.jpg",cacheKey]];
    UIImage *image = [[UIImage alloc]initWithData:data];
    if (!image) {
        return placeholderImage;
    }
    return image;
}

- (void)saveImage:(UIImage *)image withFileName:(NSString *)fileName
{
    NSData *data = [NSData dataWithData:UIImageJPEGRepresentation(image, 0.6f)];
    [FileManager saveFileWithName:fileName andData:data];
}

- (UIImage *)loadImageWithFileName:(NSString *)fileName
{
    UIImage *placeholderImage = [[UIImage alloc]init];
    placeholderImage = [UIImage imageNamed:kPlaceholderimage];

    NSData *data = [FileManager readDataFromFileWithName:fileName];
    UIImage *image = [[UIImage alloc]initWithData:data];
    if (!image) {
        return placeholderImage;
    }
    return image;
}

#pragma mark Database utilities methods

- (NSString *)getDatabasePath
{
    [self createAndCheckDatabase];
    return self.databasePath;
}

- (void)createAndCheckDatabase
{
    BOOL success; 
    
    NSString *dbPath = [FileManager getFilePathWithName:kDatabaseName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    success = [fileManager fileExistsAtPath:dbPath];
    
    if(success)
    {
        self.databasePath = dbPath;
        return;
    }
    
    NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:kDatabaseName];
    
    if([fileManager copyItemAtPath:databasePathFromApp toPath:dbPath error:nil]){
        [self addSkipBackupAttributeToItemAtURL:dbPath];
        self.databasePath = dbPath;
        // Prevent iCloud sync for the db file
    }
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSString *)path
{
    NSURL *URL = [[NSURL alloc] initFileURLWithPath:path];
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

#pragma mark - Location Methods
-(void) locationUpdate {
    switch([CLLocationManager authorizationStatus]) {
        case kCLAuthorizationStatusNotDetermined:
            [self.locationManager requestWhenInUseAuthorization];
            break;
        case kCLAuthorizationStatusAuthorizedAlways:
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.locationManager startUpdatingLocation];
            break;
        default:
            break;
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    if(!GET_ISLOCATIONDETERMINED) {
        CLLocation *location = [locations firstObject];
        [self.locationManager stopUpdatingLocation];

        CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
        [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error){
            if(!(error)) {
                CLPlacemark *placemark = [placemarks objectAtIndex:0];
                NSString *countryCode = [NSString stringWithFormat:@"%@",placemark.ISOcountryCode];
                if([ISOCOUNTRYCODES_DICT objectForKey:countryCode]) {
                    SET_COUNTRY_CODE([ISOCOUNTRYCODES_DICT objectForKey:countryCode]);
                } else {
                    SET_COUNTRY_CODE(DEFAULT_COUNTRY_CODE);
                }
                [self setDefaultLanguage];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [mLookupManager reloadLookup];
                SET_ISLOCATIONDETERMINED(YES);
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }];
    }
}


@end
