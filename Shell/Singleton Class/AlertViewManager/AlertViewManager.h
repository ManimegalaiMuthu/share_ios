//
//  AlertViewManager.h
//  JTIStarPartners
//
//  Created by Shekhar  on 30/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCLAlertView.h"
#import "BannerAlert.h"
#import "WebServiceResponse.h"


#define mAlert	((AlertViewManager *) [AlertViewManager getInstance])
typedef void (^CompletionBlock)(BOOL finished);

@interface AlertViewManager : NSObject

+ (AlertViewManager*) getInstance;

@property(nonatomic,assign)BOOL isAlertViewActive;
//- (void)showLogoutAlert:(CompletionBlock)compBlock;
- (void)showNetworkErrorAlert:(NSString *)message onCompletion:(CompletionBlock)compBlock;
- (void)showSuccessAlertWithMessage:(NSString *)message onCompletion:(CompletionBlock)compBlock;
- (void)showSuccessAlertWithTitle:(NSString *)title withMessage:(NSString *)message withCloseButtonTitle:(NSString *)closeTitle onCompletion:(CompletionBlock)compBlock;
- (void)showErrorAlertWithMessage:(NSString *)message;
- (void)showErrorAlertWithMessage:(NSString *)message onCompletion:(CompletionBlock)compBlock;
- (void)showErrorAlertWithMessage:(NSString *)message title:(NSString *)title closeButtonTitle:(NSString *)closeButtonTitle;
- (void)showWebserviceAlertWithAlert:(WebserviceAlert *)webServiceAlert onCompletion:(CompletionBlock)compBlock;
- (void)showWebserviceSuccessWithAlert:(WebserviceAlert *)webServiceAlert onCompletion:(CompletionBlock)compBlock;
- (void)showAlertWithMessageAndTitle:(NSString *)message title:(NSString *)title onCompletion:(CompletionBlock)compBlock;
- (void)showBannerAlertWithTitle:(NSString *)title andSubTitle:(NSString *)subTitle;

@end
