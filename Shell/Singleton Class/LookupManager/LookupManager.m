//
//  LookupManager.m
//  Shell
//
//  Created by Nach on 4/3/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "LookupManager.h"
#import "Constants.h"
#import "Session.h"
#import "WebServiceManager.h"

#define DEFAULT_LOOKUP_VERSION  @"1501065350"
static LookupManager *instance;

@interface LookupManager() <WebServiceManagerDelegate>

@end

@implementation LookupManager

- (id) init {
    if (self = [super init])
    {
        self.lookupTable = [[NSDictionary alloc] init];
        return self;
    }
    return nil;
}

+ (LookupManager*) sharedInstance
{
    static LookupManager *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[LookupManager alloc] init];
    });
    
    return _sharedInstance;
}

-(void) loadLookupTable {
    NSDictionary *requestData;
    self.lookupTable = GET_LOOKUP_TABLE;
    self.languageTable = GET_LANGUAGE_TABLE;
    if ([self.lookupTable isEqualToDictionary:@{}] || self.lookupTable == nil)
    {
        //certain languages will not have default countries
        //set country code if not set
        if (!GET_COUNTRY_CODE)
            SET_COUNTRY_CODE(DEFAULT_COUNTRY_CODE);
            
        [[NSUserDefaults standardUserDefaults] synchronize];
        requestData = @{@"CountryCode"  :   DEFAULT_COUNTRY_CODE,
                        @"LanguageCode" :   ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])? @"5" : @"1", //use system language
                        @"LookUpVersion":   DEFAULT_LOOKUP_VERSION,
                    };
            
    }
    else
    {
        requestData = @{@"CountryCode"  :   GET_COUNTRY_CODE,
                        @"LanguageCode" :   @([kAvailableLanguages indexOfObject: GET_LOCALIZATION]).stringValue,
                        @"LookUpVersion":   [self.lookupTable objectForKey: @"LookUpVersion"]
                    };
    }

    [[WebServiceManager sharedInstance] requestLookupTable: requestData view:nil];
}

-(void) reloadLookup {
    NSString *languageCodeString  = [NSString stringWithFormat: @"%lu", (unsigned long)[kAvailableLanguages indexOfObject: GET_LOCALIZATION]];
    NSDictionary *requestData = @{@"CountryCode"    :   GET_COUNTRY_CODE,
                                  @"LanguageCode"   :   languageCodeString,
                                  @"LookUpVersion"  :   [self.lookupTable objectForKey: @"LookUpVersion"] ? [self.lookupTable objectForKey: @"LookUpVersion"] : DEFAULT_LOOKUP_VERSION
                            };
    [[WebServiceManager sharedInstance] requestLookupTable: requestData view:nil];
}

#pragma mark - Webservice Manager
- (void)processCompleted:(WebServiceResponse *)response {
    
    switch (response.webserviceCall) {
        case kWEBSERVICE_LOOKUPTABLE:
        {
            if (IS_DEVELOPMENT_VERSION || IS_STAGING_VERSION)
                NSLog(@"success lookup ------ %@", [response getJSON]);
            NSDictionary *lookupDict = [response getGenericResponse];
            
            if ([[lookupDict objectForKey: @"LoadData"] boolValue])
            {
                SET_LOOKUP_TABLE(lookupDict);
                self.lookupTable = lookupDict;
            
                SET_LANGUAGE_TABLE([self.lookupTable objectForKey: @"LanguageList"]);
                self.languageTable = [self.lookupTable objectForKey: @"LanguageList"];
                
                [mSession setLookupTable:self.lookupTable];
                [mSession setLanguageTable:self.languageTable];
                
                [[NSNotificationCenter defaultCenter] postNotificationName: @"Reloaded Lookup Table" object: self];
            }
        }
            break;
            
        default:
            break;
    }
    
}


@end
