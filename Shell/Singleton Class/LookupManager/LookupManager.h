//
//  LookupManager.h
//  Shell
//
//  Created by Nach on 4/3/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>

#define mLookupManager ((LookupManager *) [LookupManager sharedInstance])

NS_ASSUME_NONNULL_BEGIN

@interface LookupManager : NSObject

@property(nonatomic, strong)NSDictionary *lookupTable;
@property(nonatomic, strong)NSDictionary *languageTable;
+(LookupManager *) sharedInstance;

-(void) loadLookupTable;
-(void) reloadLookup;

@end

NS_ASSUME_NONNULL_END
