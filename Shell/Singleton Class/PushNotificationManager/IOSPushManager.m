//
//  IOSPushManager.m
//  JTIStarPartners
//
//  Created by Shekhar on 14/8/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import "IOSPushManager.h"
#include <sys/sysctl.h>
#include "Session.h"



@interface IOSPushManager ()
@property (nonatomic,strong) NSString *deviceToken;
@end

static NSString *kPushNotificationPromptMessage=@"PushNotificationPromptMessage";

@implementation IOSPushManager

/**
 Create Shared Instance
 */
+ (id) shared {
    static IOSPushManager *sharedMyManager = nil;
    @synchronized(self) {
        if (sharedMyManager == nil)
            sharedMyManager = [[self alloc] init];
      
    }
    return sharedMyManager;
}

- (void) registerAPNS {
    if(@available(iOS 10, *)){

        
    }
    else
    {

    }
}

#pragma mark PushNotification Delegate Methods
- (void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {

}

- (void)didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kPushNotificationPromptMessage object:userInfo];
    NSLog(@"Alert: %@", userInfo);
    NSString *message = [NSString stringWithFormat:@"%@",userInfo];
    NSLog(@"message---%@", message);
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"\n\n\nNew Message" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

- (void) didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    
    NSLog(@"error %@", error);
}




- (NSString *) getDeviceToken
{
    return self.deviceToken;
}


@end
