//
//  IOSPushManager.h
//  JTIStarPartners
//
//  Created by Shekhar on 14/8/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#include <UserNotifications/UserNotifications.h>

@protocol IOSPushManagerDelegate <NSObject>
@optional
- (void) receivedFeedbackWithReportID:(NSString *)aID;
- (void) receivedBroadCastMessage:(NSString *)aMessage;
@end


@interface IOSPushManager : NSObject
/**
 Create Shared Instance
*/
 @property(nonatomic,strong) id <IOSPushManagerDelegate, UNUserNotificationCenterDelegate> delegate;

+ (id) shared;
- (void) registerAPNS;
- (void) didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken;
- (void) didFailToRegisterForRemoteNotificationsWithError:(NSError*)error;
- (void) didReceiveRemoteNotification:(NSDictionary *)userInfo;
- (NSString *) getDeviceToken;
@end
