//
//  DatabaseManager.m
//  Edenred
//
//  Created by Shekhar on 15/08/14.

//

#import "DatabaseManager.h"
#import "FMDatabaseQueue.h"
#import "SQL.h"
#import "Db.h"
//#import "Cart.h"

//#import "Transactions.h"
//#import "Case.h"
//#import "UserProfile.h"

#define CREATE_SEMAPHORE dispatch_semaphore_t databaseOptSema = dispatch_semaphore_create(0);
#define WAIT_FOR_ACTION dispatch_semaphore_wait(databaseOptSema, DISPATCH_TIME_FOREVER);
#define SIGNAL_COMPLETE_ACTION dispatch_semaphore_signal(databaseOptSema);


@interface DatabaseManager ()
@property  (nonatomic ,strong) Db *db;
//@property  (nonatomic ,strong) Cart *cart;
@property  (nonatomic ,strong) FMDatabaseQueue *queue;
@property (nonatomic , strong) NSString *databasePath;

- (BOOL) openDatabase;
- (BOOL) closeDatabase;
@end

@implementation DatabaseManager

@synthesize databasePath;
@synthesize db;

#pragma mark DatabaseManager <Private Methods>
 static DatabaseManager *sharedInstance = nil;
/**
 Create Shared Instance
 */
+ (DatabaseManager *)sharedInstance {
    
    // 1
    static DatabaseManager *sharedInstance = nil;
    
    // 2
    static dispatch_once_t oncePredicate;
    
    // 3
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [[DatabaseManager alloc] init];
    });
    return sharedInstance;
}

/**
 Initialize Class
 */
- (id) init {
    self = [super init];
    if (self) {
        db = [[Db alloc]init];
        self.queue = [FMDatabaseQueue databaseQueueWithPath:[Db getDatabasePath]];
    }
    return self;
}

+ (DatabaseManager *) getInstance {
    return [[DatabaseManager alloc] init];
}


- (void)listTableName {
    
    [db openDatabase];
    db.results = [db.database executeQuery:@"SELECT name FROM sqlite_master WHERE type='table'"];
    while([db.results next])
    {
          NSLog(@"TABLE NAME %@",[db.results stringForColumn:@"name"]);
    }
    [db closeDatabase];

}

#pragma mark - QR CODEs
- (BOOL)addItem:(Qrcode *)qrCode {
    
    BOOL success = NO;
    NSString *dbInsertQuery = add_qr_code_to_sql;
    
    [db openDatabase];
    
    @try
    {
        success = [db.database executeUpdate:[NSString stringWithFormat:dbInsertQuery, qrCode.codeID, qrCode.memberID]];
        if (success) {
            
        }else {
            NSLog(@"Error inserting %@",qrCode);
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Error: %@", [exception reason]);
    }
    @finally
    {
        [db.database close];
    }
    
    return success;
}

- (BOOL)checkExist:(Qrcode *)qrCode  {
    NSString *dbQuery = check_qr_code_from_sql;
    [db openDatabase];
    NSLog(@"%@",[NSString stringWithFormat:dbQuery,qrCode.memberID,qrCode.codeID]);
    
    db.results = [db.database executeQuery:[NSString stringWithFormat:dbQuery,qrCode.memberID,qrCode.codeID]];
    
    if(![db.results next]){
        [db closeDatabase];
        return false;
    }
    [db closeDatabase];
    return true;
}

- (BOOL)removeAllItemBy:(NSString *)memberID {
    BOOL success = NO;
    NSString *dbDeleteQuery = delete_all_qr_code_from_cart_sql;
    
    [db openDatabase];
    @try
    {
        success = [db.database executeUpdate:[NSString stringWithFormat:dbDeleteQuery,memberID]];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Error: %@", [exception reason]);
    }
    @finally
    {
        [db closeDatabase];
    }
    
    return success;
}

- (BOOL)removeItem:(Qrcode *)qrCode {
    BOOL success = NO;
    NSString *dbDeleteQuery = delete_qr_code_from_cart_sql;
    
    [db openDatabase];
    @try
    {
        success = [db.database executeUpdate:[NSString stringWithFormat:dbDeleteQuery, qrCode.codeID, qrCode.memberID]];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Error: %@", [exception reason]);
    }
    @finally
    {
        [db closeDatabase];
    }

    return success;
}

- (NSArray *)getQrCodesForMember:(NSString *)memberID {
    
    NSString *dbQuery = get_qr_code_from_sql;
    
    [db openDatabase];
    db.results = [db.database executeQuery:[NSString stringWithFormat:dbQuery,memberID]];
    
    NSMutableArray *qrcodeArray = [[NSMutableArray alloc]init];
    while([db.results next])
    {
        Qrcode *qrcode = [[Qrcode alloc] initWithResults:db.results];
        [qrcodeArray addObject:qrcode];
    }
    
    [db closeDatabase];
    
    return qrcodeArray;
    
}



#pragma mark - Product SKUs
- (BOOL)addSKUArray:(NSArray *)sku forType:(NSString *)type {
    BOOL success = NO;
    NSString *dbInsertQuery = add_product_sku_to_sql;
    
    [db openDatabase];
    
    NSData *revert = [NSJSONSerialization dataWithJSONObject:sku options:NSJSONWritingPrettyPrinted error:nil];
    NSString* jsonData = [[[NSString alloc] initWithData:revert encoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
    
    @try
    {
        success = [db.database executeUpdate:[NSString stringWithFormat:dbInsertQuery, type,jsonData]];
        if (success) {
            
        }else {
            NSLog(@"Error inserting %@",jsonData);
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Error: %@", [exception reason]);
    }
    @finally
    {
        [db.database close];
    }
    
    return success;

}

- (BOOL)checkSKUExist:(NSString *)type {
    NSString *dbQuery = get_product_sku_from_sql;
    [db openDatabase];
    
    db.results = [db.database executeQuery:[NSString stringWithFormat:dbQuery,type]];
    
    if(![db.results next]){
        [db closeDatabase];
        return false;
    }
    [db closeDatabase];
    return true;

}

- (BOOL)updateSKUArray:(NSArray *)sku forType:(NSString *)type {
    BOOL success = NO;
    NSString *dbUpdateQuery = update_product_sku_from_sql;
    
    [db openDatabase];
    
    NSData *revert = [NSJSONSerialization dataWithJSONObject:sku options:NSJSONWritingPrettyPrinted error:nil];
    NSString* jsonData = [[[NSString alloc] initWithData:revert encoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
    
    @try
    {
        success = [db.database executeUpdate:[NSString stringWithFormat:dbUpdateQuery, jsonData, type]];
        if (success) {
            
        }else {
            NSLog(@"Error updating %@",jsonData);
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Error: %@", [exception reason]);
    }
    @finally
    {
        [db.database close];
    }
    
    return success;

}

- (NSArray *)getSKUForType:(NSString *)type {
    NSString *dbQuery = get_product_sku_from_sql;
    
    [db openDatabase];
    db.results = [db.database executeQuery:[NSString stringWithFormat:dbQuery,type]];
    
    NSError *err;
    NSData *data = [[db.results stringForColumn:@"json"] dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *moduleData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
    
//    NSMutableArray *qrcodeArray = [[NSMutableArray alloc]init];
//    while([db.results next])
//    {
//        Qrcode *qrcode = [[Qrcode alloc] initWithResults:db.results];
//        [qrcodeArray addObject:qrcode];
//    }
    
    [db closeDatabase];
    
    return moduleData;
}


//#pragma mark - Shopping Cart
//
//- (void)listRewardsInCatalogueWithDelegate:(id<DatabaseManagerDelegate>)delegate
//
//{
//    [db.databasequeue inDatabase:^(FMDatabase *dbase) {
//        db.results = [dbase executeQuery:@"SELECT * FROM products"];
//        while([db.results next])
//        {
//            Reward *reward = [[Reward alloc] initWithResults:db.results];
//            [delegate updateReward:reward];
//        }
//    }];
//}
//
//- (NSMutableArray *)getRewardsInCatalogue
//{
//
//    NSMutableArray *rewards = [[NSMutableArray alloc] init];
//    [db openDatabase];
//    db.results = [db.database executeQuery:@"SELECT * FROM products"];
//    while([db.results next])
//    {
//        Reward *reward = [[Reward alloc] initWithResults:db.results];
//
//        [rewards addObject:reward];
//    }
//    [db closeDatabase];
//
//    return rewards;
//}
//
//- (void)getContentsfromDB:(DatabaseType)dbType withDelegate:(id<DatabaseManagerDelegate>)delegate
//
//{
//    NSMutableArray *content = [[NSMutableArray alloc] init];
//    NSString *dbQuery = nil;
//
//    switch (dbType) {
//        case 1:
//            dbQuery = get_products_from_cart_sql;
//            break;
//
//        case 2:
//            dbQuery = get_products_from_wishlist_sql;
//            break;
//
//        default:
//            break;
//    }
//    [db.databasequeue inDatabase:^(FMDatabase *dbase) {
//        db.results = [dbase executeQuery:dbQuery];
//        while([db.results next])
//        {
//            Reward *reward = [[Reward alloc]initWithResults:db.results];
//            RewardItem *item = [[RewardItem alloc] initWithReward:reward andQuantity:[db.results intForColumn:@"quantity"]];
//            [content addObject:item];
//            [delegate updateCart:content];
//            
//        }
//        
//    }];
//
//}
//
//- (NSMutableArray *)getContentsfromDB:(DatabaseType)dbType
//{
//    NSMutableArray *content = [[NSMutableArray alloc] init];
//    NSString *dbQuery = nil;
//    
//    switch (dbType) {
//        case 1:
//            dbQuery = get_products_from_cart_sql;
//            break;
//            
//        case 2:
//            dbQuery = get_products_from_wishlist_sql;
//            break;
//            
//        default:
//            break;
//    }
//    [db openDatabase];
//    db.results = [db.database executeQuery:dbQuery];
//    content = [self contentsFromResults:db.results];
//    [db closeDatabase];
//    
//    return content;
//}
//
//- (int)totalRewardsInDB:(DatabaseType)dbType
//
//{
//    NSMutableArray *contents = [self getContentsfromDB:dbType];
//    return [self totalRewardsInDB:dbType withRewards:contents];
//}
//
//- (int)totalPointsInDB:(DatabaseType)dbType
//
//{
//    NSMutableArray *contents = [self getContentsfromDB:dbType];
//    return [self totalPointsInDB:dbType withRewards:contents];
//}
//
//- (BOOL)isDBEmpty:(DatabaseType)dbType
//{
//    return [self totalRewardsInDB:dbType] == 0;
//}
//
//- (BOOL)clearDB:(DatabaseType)dbType
//{
//    BOOL success = NO;
//    
//    NSString *dbQuery = nil;
//    
//    switch (dbType) {
//        case 1:
//            dbQuery = delete_cart_sql;
//            break;
//            
//        case 2:
//            dbQuery = delete_wishlist_sql;
//            break;
//            
//        default:
//            break;
//    }
//
//    [db openDatabase];
//    
//    @try
//    {
//        success = [db.database executeUpdate:dbQuery];
//    }
//    @catch (NSException *exception)
//    {
//        NSLog(@"Error: %@", [exception reason]);
//    }
//    @finally
//    {
//        [db closeDatabase];
//    }
//    
//    return success;
//}
//
//- (BOOL)exists:(Reward *)reward inDB:(DatabaseType)dbType
//{
//    RewardItem *item = [self getReward:reward.itemCode fromDB:dbType];
//    return item != nil;
//}
//
//- (BOOL)existsInRewardsDB:(Reward *)reward
//{
//    RewardItem *item = [self getReward:reward.itemCode fromDB:kProducts];
//    return item != nil;
//}
//
//- (RewardItem *)getReward:(NSString *)rewardID fromDB:(DatabaseType)dbType
//{
//    RewardItem *item = nil;
//    NSString *dbQuery = nil;
//    
//    switch (dbType) {
//        case kCart:
//            dbQuery = get_productid_from_cart_sql;
//            db.results = [db.database executeQuery:[NSString stringWithFormat:dbQuery,rewardID]];
//            break;
//            
//        case kWishlist:
//            dbQuery = get_productid_from_wishlist_sql;
//            db.results = [db.database executeQuery:[NSString stringWithFormat:dbQuery,rewardID]];
//            break;
//        case kProducts:
//            dbQuery = get_productid_from_products_sql;
//            db.results = [db.database executeQuery:[NSString stringWithFormat:dbQuery,rewardID]];
//            break;
//            
//        default:
//            break;
//    }
//    //db.results = [db.database executeQuery:[NSString stringWithFormat:dbQuery, rewardID]];
//    item = [self getProductFromResults:db.results];
//    return item;
//}
//
//- (int)totalPointsInDB:(DatabaseType)dbType withRewards:(NSMutableArray *)contents
//{
//    int total = 0;
//    for(RewardItem *item in contents)
//    {
//        total += (item.reward.pointsNeeded * item.quantity);
//    }
//    
//    return total;
//}
//
//- (int)totalRewardsInDB:(DatabaseType)dbType withRewards:(NSMutableArray *)contents
//{
//    int total = 0;
//    
//    for(RewardItem *item in contents)
//    {
//        total += item.quantity;
//    }
//    
//    return total;
//}
//
//- (NSMutableArray *)contentsFromResults:(FMResultSet *)results
//{
//    
//    NSMutableArray *content = [[NSMutableArray alloc] init];
//    
//    while([results next])
//    {
//        Reward *reward = [[Reward alloc]initWithResults:results];
//        RewardItem *item = [[RewardItem alloc] initWithReward:reward andQuantity:[results intForColumn:@"quantity"]];
//        
//        [content addObject:item];
//    }
//    return content;
//}
//
//- (RewardItem *)getProductFromResults:(FMResultSet *)results
//{
//    RewardItem *item = nil;
//    while([results next])
//    {
//        Reward *reward = [[Reward alloc] initWithResults:results];
//        item = [[RewardItem alloc] initWithReward:reward andQuantity:[results intForColumn:@"quantity"]];
//    }
//    
//    return item;
//}
//
//- (BOOL)addItemToDB:(DatabaseType)dbType withReward:(Reward *)reward
//{
//    return [self addItemToDB:dbType withReward:reward quantity:1];
//}
//
//
//- (BOOL)addItemToDB:(DatabaseType)dbType withReward:(Reward *)reward quantity:(int)quantity
//{
//    reward.name = [reward.name stringByReplacingOccurrencesOfString:@"'"
//                                                      withString:@" "];
//    reward.desc = [reward.desc stringByReplacingOccurrencesOfString:@"'"
//                                                         withString:@" "];
//    BOOL success = NO;
//    NSString *dbUpdateQuery = nil;
//    NSString *dbInsertQuery = nil;
//    
//    switch (dbType) {
//        case kCart:
//        {
//            dbUpdateQuery = update_quantity_to_cart_sql;
//            dbInsertQuery = insert_product_to_cart_sql;
//        }
//            break;
//            
//        case kWishlist:
//        {
//            dbUpdateQuery = update_quantity_to_wishlist_sql;
//            dbInsertQuery = insert_product_to_wishlist_sql;
//        }
//            break;
//            
//        default:
//            break;
//    }
//
//    
//    [db openDatabase];
//    
//    @try
//    {
//        if ([self getReward:reward.itemCode fromDB:kProducts]) {
//            RewardItem *item = [self getReward:reward.itemCode fromDB:dbType];
//            if (dbType == kCart) {
//                success = item ?
//                [db.database executeUpdate:[NSString stringWithFormat:dbUpdateQuery, quantity + item.quantity, reward.itemCode]] :
//                [db.database executeUpdate:[NSString stringWithFormat:dbInsertQuery, reward.itemCode, quantity]];
//            }
//            else
//            {
//                success = item ?
//                [db.database executeUpdate:[NSString stringWithFormat:dbUpdateQuery, quantity + item.quantity, reward.itemCode]] :
//                [db.database executeUpdate:[NSString stringWithFormat:dbInsertQuery, reward.itemCode, quantity,reward.wishID]];
//            }
//            
//        }
//        else
//        {
//            success = [db.database executeUpdate:[NSString stringWithFormat:insert_product_to_products__sql,reward.itemCode,reward.name,reward.image,reward.pointsNeeded,reward.desc,reward.categoryCode]];
//            if (success) {
//             
//                if (dbType == kCart) {
//                    success = [db.database executeUpdate:[NSString stringWithFormat:dbInsertQuery, reward.itemCode, quantity]];
//                }
//                else
//                {
//                   success = [db.database executeUpdate:[NSString stringWithFormat:dbInsertQuery, reward.itemCode, quantity,reward.wishID]];
//                }
//            }
//            else
//            {
//                 NSLog(@"Error");
//            }
//            
//        }
//    }
//    @catch (NSException *exception)
//    {
//        NSLog(@"Error: %@", [exception reason]);
//    }
//    @finally
//    {
//        [db.database close];
//    }
//    
//    return success;
//
//}
//
//- (BOOL)updateWishListDBWithReward:(Reward *)reward quantity:(int)quantity
//{
//    BOOL success = NO;
//    NSString *dbUpdateQuery = nil;
//    NSString *dbInsertQuery = nil;
//    
// 
//    dbUpdateQuery = update_quantity_to_wishlist_sql;
//    dbInsertQuery = insert_product_to_wishlist_sql;
//    
//    //_databasePath	NSPathStore2 *	@"/var/mobile/Containers/Data/Application/B886EB05-3CE7-4937-9ED7-FA4FB24AFDBF/Documents/localDB.db"	0x1c179500
//    
//    
//    
//    [db openDatabase];
//    
//    @try
//    {
//        if ([self getReward:reward.itemCode fromDB:kProducts]) {
//            RewardItem *item = [self getReward:reward.itemCode fromDB:kWishlist];
//            if (!(EQUALS(item.reward.itemCode, reward.itemCode) && item.quantity == quantity)) {
//                success = item ?
//                [db.database executeUpdate:[NSString stringWithFormat:dbUpdateQuery, quantity + item.quantity, reward.itemCode]] :
//                [db.database executeUpdate:[NSString stringWithFormat:dbInsertQuery, reward.itemCode, quantity,reward.wishID]];
//            }
//            
//          
//            
//        }
//        else
//        {
//            success = [db.database executeUpdate:[NSString stringWithFormat:insert_product_to_products__sql,reward.itemCode,reward.name,reward.image,reward.pointsNeeded,reward.desc,reward.categoryCode]];
//            
//            if (success) {
//            success = [db.database executeUpdate:[NSString stringWithFormat:dbInsertQuery, reward.itemCode, quantity,reward.wishID]];
//             
//            }
//            else
//            {
//                NSLog(@"Error");
//            }
//            
//        }
//    }
//    @catch (NSException *exception)
//    {
//        NSLog(@"Error: %@", [exception reason]);
//    }
//    @finally
//    {
//        [db.database close];
//    }
//    
//    return success;
//    
//}
//
//
//- (BOOL)removeItemFromDB:(DatabaseType)dbType withReward:(Reward *)reward quantity:(int)quantity
//{
//  
//    BOOL success = NO;
//    
//    NSString *dbUpdateQuery = nil;
//    NSString *dbInsertQuery = nil;
//    
//    switch (dbType) {
//        case 1:
//        {
//            dbUpdateQuery = update_quantity_to_cart_sql;
//            dbInsertQuery = insert_product_to_cart_sql;
//        }
//            break;
//            
//        case 2:
//        {
//            dbUpdateQuery = update_quantity_to_wishlist_sql;
//            dbInsertQuery = insert_product_to_wishlist_sql;
//        }
//            break;
//            
//        default:
//            break;
//    }
//
//    
//    [db openDatabase];
//    
//    @try
//    {
//        RewardItem *item = [self getReward:reward.itemCode fromDB:dbType];
//        success = item ?
//        [db.database executeUpdate:[NSString stringWithFormat:dbUpdateQuery, item.quantity - quantity, reward.itemCode]] :
//        [db.database executeUpdate:[NSString stringWithFormat:dbInsertQuery, reward.itemCode, quantity]];
//    }
//    @catch (NSException *exception)
//    {
//        NSLog(@"Error: %@", [exception reason]);
//    }
//    @finally
//    {
//        [db.database close];
//    }
//    
//    return success;
//}
//
//- (BOOL)removeWholeItemFromDB:(DatabaseType)dbType withReward:(Reward *)reward
//{
//    BOOL success = NO;
//    NSString *dbDeleteQuery = nil;
//
//    
//    switch (dbType) {
//        case 1:
//        {
//            dbDeleteQuery = delete_whole_from_cart_sql;
//        }
//            break;
//            
//        case 2:
//        {
//            dbDeleteQuery = delete_whole_from_wishlist_sql;
//        }
//            break;
//            
//        default:
//            break;
//    }
//
//    
//    [db openDatabase];
//    
//    @try
//    {
//        success = [db.database executeUpdate:[NSString stringWithFormat:dbDeleteQuery, reward.itemCode]];
//    }
//    @catch (NSException *exception)
//    {
//        NSLog(@"Error: %@", [exception reason]);
//    }
//    @finally
//    {
//        [db closeDatabase];
//    }
//    
//    return success;
//}


@end
