//
//  SQL.h
//  Edenred
//
//  Created by Shekhar on 16/1/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#ifndef Edenred_SQL_h
#define Edenred_SQL_h

static NSString *add_app_document_sql=@"INSERT INTO app_documents(documentName,documentDirectoryPath,documentFullPath) VALUES(?,?,?)";

static NSString *fetch_app_document_sql=@"SELECT *FROM app_documents where documentDirectoryPath='%@'";


static NSString *add_new_conference_sql=@"INSERT INTO conference_info(userId,conferenceTitle,conferenceDate,conferenceDescription,conferenceStatus) VALUES(?,?,?,?,?)";

static NSString *fetch_running_conference=@"SELECT * FROM conference_info where conferenceStatus='1'";

static NSString *end_running_conference_sql=@"UPDATE conference_info SET conferenceStatus='0' WHERE id=?";

static NSString *add_user_profile_sql=@"INSERT INTO user_profile(userId,machineId,hostName,port,userName,company,position,email,profileImagePath) VALUES(?,?,?,?,?,?,?,?,?)";

static NSString *fetch_last_color_code = @"select max(colorCode) from conference_users where conferenceId=%d";


static NSString *fetch_user_color_code = @"select *from conference_users where  conferenceId=%d and peerUserId='%@'";


static NSString *fetch_color_code = @"Select backgroundColor from Colors where  id=%d";

static NSString *fetch_comment_text_color_code = @"Select textColor from Colors where  id=%d";


static NSString *fetch_users_info = @"Select *from conference_users where  conferenceId=%d and conferenceOwnerId='%@'";


static NSString *fetch_color = @"SELECT * FROM Colors where id in (select colorCode from conference_users where peerUserId='%@')";




static NSString *update_user_profile_sql=@"UPDATE user_profile SET machineId=?,hostName=?,port=?,userName=?,company=?,position=?,email=?,profileImagePath=? WHERE userId=?";

static NSString *fetch_user_profile=@"SELECT * FROM user_profile";

static NSString *add_conference_users_sql=@"INSERT INTO conference_users(conferenceId,conferenceOwnerId,peerMachineId,peerUserId,peerHostName,peerPort,peerName,peerProfileImagePath,role,colorCode) VALUES(?,?,?,?,?,?,?,?,?,?)";

static NSString *add_conference_doc_sql=@"INSERT INTO conference_doc(conferenceOwnerId,conferenceId,documentId,documentName,documentFilePath,documentStatus) VALUES(?,?,?,?,?,?)";

static NSString *fetch_conference_doc=@"SELECT *FROM conference_doc where conferenceId=%d AND conferenceOwnerId='%@' AND documentStatus=%d";

static NSString *fetch_all_conference_info=@"SELECT *FROM conference_info";

static NSString *fetch_conference_doc_exist_sql=@"SELECT * FROM conference_doc where conferenceId=%d AND conferenceOwnerId='%@' AND documentId=%d";

static NSString *update_conference_doc_sql=@"UPDATE conference_doc SET conferenceOwnerId=?,documentName=?,documentFilePath=?,documentStatus=? WHERE documentId=? AND conferenceId=?";

static NSString *add_chat_message_sql=@"INSERT INTO conference_chat(conferenceId,conferenceOwnerId,userId,userName,date,message,role) VALUES(?,?,?,?,?,?,?)";

static NSString *add_conference_bookmark_sql=@"INSERT INTO conference_doc_bookmark(conferenceId,documentId,pageNo) VALUES(?,?,?)";

static NSString *fetch_conference_bookmark_sql=@"SELECT *FROM conference_doc_bookmark where conferenceId=%d AND documentId=%d AND pageNo=%d";

static NSString *fetch_conference_all_bookmarks_sql=@"SELECT *FROM conference_doc_bookmark where conferenceId=%d AND documentId=%d";

static NSString *delete_conference_bookmark_sql=@"DELETE FROM conference_doc_bookmark where conferenceId=%d AND documentId=%d";

static NSString *add_conference_doc_comments_sql=@"INSERT INTO conference_doc_comments(conferenceId,documentId,pageNo,cord_x,cord_y,text,userId) VALUES(?,?,?,?,?,?,?)";


static NSString *fetch_conference_doc_in_page_comments_sql=@"SELECT *FROM conference_doc_comments where conferenceId=%d AND documentId=%d AND pageNo=%d";

static NSString *fetch_conference_doc_in_page_with_userid_comments_sql=@"SELECT *FROM conference_doc_comments where conferenceId=%d AND documentId=%d AND pageNo=%d AND userId='%@'";

static NSString *fetch_conference_doc_comment_sql=@"SELECT *FROM conference_doc_comments where id=%d";


static NSString *fetch_all_conference_doc_comments_sql = @"SELECT *FROM conference_doc_comments where conferenceId=%d AND documentId=%d";

static NSString *delete_conference_doc_comment_sql=@"DELETE FROM conference_doc_comments where id=%d";

static NSString *delete_conference_doc_of_user_comment_sql=@"DELETE FROM conference_doc_comments where conferenceId=%d AND documentId=%d AND pageNo=%d AND userId='%@'";


// Qrcode
static NSString *add_qr_code_to_sql =@"INSERT INTO qrcode(code,memberID) VALUES('%@','%@')";

static NSString *get_qr_code_from_sql =@"SELECT * FROM qrcode WHERE memberID = '%@'";

static NSString *check_qr_code_from_sql =@"SELECT * FROM qrcode WHERE memberID = '%@' AND code = '%@'";

static NSString *delete_all_qr_code_from_cart_sql=@"DELETE FROM qrcode WHERE memberID = '%@'";

static NSString *delete_qr_code_from_cart_sql=@"DELETE FROM qrcode WHERE code = '%@' AND memberID = '%@'";

// ProductSku
static NSString *add_product_sku_to_sql =@"INSERT INTO ProductSKU(type,json) VALUES('%@','%@')";

static NSString *get_product_sku_from_sql =@"SELECT * FROM qrcode WHERE type = '%@'";

static NSString *update_product_sku_from_sql =@"UPDATE qrcode SET json = '%@' WHERE type = '%@'";

//CSModule

static NSString *insert_message_to_feedbacks_sql=@"INSERT INTO feedbacks (date,message,image,caseid,messagetype) VALUES ('%@','%@','%@','%@','0')";

static NSString *insert_newcase_to_cases_sql=@"INSERT INTO cases (caseid,casetype,casestatus,username,date,image,description) VALUES ('%@','%@','0','%@','%@','%@','%@')";

static NSString *get_feedbacks_with_caseid_sql=@"SELECT f.* FROM feedbacks f JOIN cases c ON f.caseid = c.caseid where c.caseid = '%@'";

static NSString *get_allcases_from_cases_sql=@"SELECT c.* FROM cases c where c.username = '%@'";

static NSString *get_casestatus_from_cases_sql=@"SELECT c.* FROM cases c where c.casestatus = %d and c.username = '%@'";

static NSString *get_casetype_from_cases_sql=@"SELECT c.* FROM cases c where c.casetype = '%@' and c.username = '%@'";

//Shopping Cart

static NSString *insert_product_to_products__sql=@"INSERT INTO products(itemcode,name,image,price,description,categorycode) VALUES ('%@','%@','%@','%d','%@','%@')";

static NSString *delete_whole_from_cart_sql=@"DELETE FROM cart where productitemcode = %@";

static NSString *insert_product_to_cart_sql=@"INSERT INTO cart (productitemcode,quantity) VALUES (%@,%d)";

static NSString *update_quantity_to_cart_sql=@"UPDATE cart set quantity = %d where productitemcode = %@";

static NSString *get_productid_from_cart_sql=@"SELECT p.*, c.quantity FROM products p JOIN cart c ON p.itemcode = c.productitemcode where p.itemcode = %@";

static NSString *get_productid_from_products_sql=@"SELECT * FROM products where itemcode = %@";

static NSString *delete_cart_sql=@"DELETE FROM cart";

static NSString *get_products_from_cart_sql=@"SELECT p.*, c.quantity FROM products p JOIN cart c ON p.itemcode = c.productitemcode";

//Wishlist

static NSString *delete_whole_from_wishlist_sql=@"DELETE FROM wishlist where productitemcode = %@";

static NSString *insert_product_to_wishlist_sql=@"INSERT INTO wishlist (productitemcode,quantity,wishid) VALUES (%@,%d,%@)";

static NSString *update_quantity_to_wishlist_sql=@"UPDATE wishlist set quantity = %d where productitemcode = %@";

static NSString *get_productid_from_wishlist_sql=@"SELECT p.*, c.quantity FROM products p JOIN wishlist c ON p.itemcode = c.productitemcode where p.itemcode = %@";

static NSString *delete_wishlist_sql=@"DELETE FROM wishlist";

static NSString *get_products_from_wishlist_sql=@"SELECT p.*, c.quantity,c.wishid FROM products p JOIN wishlist c ON p.itemcode = c.productitemcode";

//Rewards

static NSString *get_earned_transactions_sql=@"SELECT t.* FROM transactions t where t.transactiontype = '0' AND t.username = '%@'";
static NSString *get_redeemed_transactions_sql=@"SELECT t.* FROM transactions t where t.transactiontype = '1' AND t.username = '%@'";
static NSString *get_all_transactions_sql=@"SELECT t.* FROM transactions t where t.username = '%@'";
static NSString *get_expiring_points_sql=@"SELECT t.points,t.expiringdate FROM transactions t where t.transactiontype = '0' AND t.username = '%@'";

#endif

// Table Column name
static NSString *kHostName=@"hostName";
static NSString *kPort=@"port";
static NSString *kId=@"id";
static NSString *kConferenceOwnerId=@"conferenceOwnerId";
static NSString *kDocumentId=@"documentId";
static NSString *kDocumentName=@"documentName";
static NSString *kDocumentFilePath=@"documentFilePath";
static NSString *kDocumentStatus=@"documentStatus";
static NSString *kConferenceId=@"conferenceId";

static NSString *kUserId=@"userId";
static NSString *kMachineId=@"machineId";
static NSString *kUserName=@"userName";

//app_documents
static NSString *kDocumentDirectoryPath=@"documentDirectoryPath";
static NSString *kDocumentFullPath=@"documentFullPath";


// user_profile table
static NSString *kCompany=@"company";
static NSString *kPosition=@"position";
//static NSString *kEmail=@"email";
static NSString *kProfileImagePath=@"profileImagePath";

// conference_info table
static NSString *kConterenceId=@"id";
static NSString *kConterenceTitle=@"conferenceTitle";
static NSString *kConterenceDate=@"conferenceDate";
static NSString *kConterenceDescription=@"conferenceDescription";
static NSString *kConterenceStatus=@"conferenceStatus";

// conference_info table
static NSString *kPeerMachineId=@"peerMachineId";
static NSString *kPeerHostName=@"peerHostName";
static NSString *kPeerPort=@"peerPort";
static NSString *kPeerName=@"peerName";
static NSString *kPeerUserId=@"peerUserId";
static NSString *kPeerProfileImagePath=@"peerProfileImagePath";
static NSString *kRole=@"role";
static NSString *kColorCode=@"colorCode";
static NSString *kMaxColorCode=@"max(colorCode)";

static NSString *kBackgroundColor=@"backgroundColor";
static NSString *kTextColor=@"textColor";


// conference_chat table
static NSString *kChatDate=@"date";
static NSString *kChatMessage=@"message";
static NSString *kConferenceUserId=@"conferenceUserId";

//Bookmark
static NSString *kPageNo=@"pageNo";

//Comments
static NSString *kCord_x=@"cord_x";
static NSString *kCord_y=@"cord_y";
static NSString *kText =@"text";

//Annotation Events
static NSString *kAnnotationData=@"annotationData";

//Comments Events
static NSString *kCommentsData=@"commentsData";


