//
// Created by Shekhar on 5/25/14.
// Copyright (c) 2014 Shekhar. All rights reserved.
//

#import "FMDatabase.h"
#import "FMDatabaseQueue.h"

@interface Db : NSObject

+(NSString *) getDatabasePath;

@property  (nonatomic ,strong) FMDatabase *database;
@property  (nonatomic ,strong) FMDatabaseQueue *databasequeue;
@property  (nonatomic ,strong) FMResultSet *results;

- (BOOL) openDatabase;
- (BOOL) closeDatabase;
- (void) resetDatabase;
- (void) beginTransaction;
- (void) endTransaction;
- (FMResultSet *)executeQuery:(NSString *)sqlStatement;
- (BOOL)executeUpdate:(NSString *)sqlStatement;

@end