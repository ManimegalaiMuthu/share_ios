//
// Created by Shekhar on 5/25/14.


#import "Db.h"
#import "DataManager.h"

@interface Db ()

@end

@implementation Db

- (id) init {
    self = [super init];
    if (self) {
        self.database = [FMDatabase databaseWithPath:[Db getDatabasePath]];
        self.results = [[FMResultSet alloc] init];
        self.databasequeue = [FMDatabaseQueue databaseQueueWithPath:[Db getDatabasePath]];
    }
    return self;
}

+ (NSString *)getDatabasePath
{
    NSString *databasePath = [mDataManager getDatabasePath];
    return databasePath;
    
    /*
     /var/mobile/Containers/Data/Application/630A3F9F-BD95-4A98-9E58-A10F8CE19091/Documents/localDB.db
     Printing description of databasePathFromApp:
     /var/mobile/Containers/Bundle/Application/351C18D1-97C8-4701-89A1-8346712FFE63/Syngenta.app/localDB.db

     */
}

- (BOOL) openDatabase
{
     return [self.database open];
}
- (BOOL) closeDatabase
{
     return [self.database close];
}

- (void) resetDatabase {
    [self.database open];
    [self.database executeUpdate:@"DELETE FROM user_profile"];
    [self.database executeUpdate:@"DELETE FROM application_documents"];
    [self.database executeUpdate:@"DELETE FROM conference_users"];
    [self.database executeUpdate:@"DELETE FROM conference_doc"];
    [self.database executeUpdate:@"DELETE FROM conference_info"];
    [self.database executeUpdate:@"DELETE FROM conference_chat"];
    [self.database close];
}

- (FMResultSet *)executeQuery:(NSString *)sqlStatement
{
    
    [self.database open];
    self.results = [self.database executeQuery:sqlStatement];
    [self.database close];
    
    return self.results;
}

- (BOOL)executeUpdate:(NSString *)sqlStatement
{
    BOOL update = NO;
    
    [self.database open];
    update = [self.database executeUpdate:sqlStatement];
    [self.database close];
    
    return update;
}


- (void) beginTransaction {
    [self.database open];
    [self.database beginTransaction];
}

- (void) endTransaction {
    [self.database commit];
    [self.database close];
}



@end