//
//  DatabaseManager.h
//  Edenred
//
//  Created by Shekhar on 15/08/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "DatabaseManagerInterface.h"

typedef enum {
    kCart                          = 1,
    kWishlist                      = 2,
    kProducts                      = 3
}DatabaseType;


@protocol DatabaseManagerDelegate

//- (void)updateReward:(Reward *)reward;
//- (void)updateCart:(NSMutableArray *)content;
//- (void)updatePointsHistory:(NSMutableArray *)transactions;
//- (void)updateCasesList:(NSMutableArray *)cases;
//
@end

@interface DatabaseManager : NSObject <DatabaseManagerInterface>

+ (DatabaseManager *)sharedInstance;
@property (assign) id<DatabaseManagerDelegate> delegate;

- (void)listTableName;


// QR Codes
- (BOOL) addItem:(Qrcode *)qrCode;
- (BOOL) checkExist:(Qrcode *)qrCode;
- (NSArray*) getQrCodesForMember:(NSString *)memberID;
- (BOOL) removeAllItemBy:(NSString *)memberID;
- (BOOL) removeItem:(Qrcode *)qrCode;

// Product SKU
- (BOOL) addSKUArray:(NSArray *)sku forType:(NSString *)type;
- (BOOL) checkSKUExist:(NSString *)type;
- (BOOL) updateSKUArray:(NSArray *)sku forType:(NSString *)type;
- (NSArray*) getSKUForType:(NSString *)type;


//Catalogue

//- (void)listRewardsInCatalogueWithDelegate:(id<DatabaseManagerDelegate>)delegate;
//- (NSMutableArray *)getRewardsInCatalogue;
//
////Rewards
//- (void)getContentsfromDB:(DatabaseType)dbType withDelegate:(id<DatabaseManagerDelegate>)delegate;
//- (NSMutableArray *)getContentsfromDB:(DatabaseType)dbType;
//- (int)totalRewardsInDB:(DatabaseType)dbType;
//- (int)totalPointsInDB:(DatabaseType)dbType;
//- (BOOL)isDBEmpty:(DatabaseType)dbType;
//- (BOOL)clearDB:(DatabaseType)dbType;
//- (BOOL)exists:(Reward *)reward inDB:(DatabaseType)dbType;
//- (BOOL)addItemToDB:(DatabaseType)dbType withReward:(Reward *)reward;
//- (BOOL)addItemToDB:(DatabaseType)dbType withReward:(Reward *)reward quantity:(int)quantity;
//- (BOOL)updateWishListDBWithReward:(Reward *)reward quantity:(int)quantity;
//- (BOOL)removeItemFromDB:(DatabaseType)dbType withReward:(Reward *)reward quantity:(int) quantity;
//- (BOOL)removeWholeItemFromDB:(DatabaseType)dbType withReward:(Reward *)reward;
//
////Rewards
//
//- (NSMutableArray *)getAllTransactions;
//- (void)getAllTransactionsWithDelegate:(id<DatabaseManagerDelegate>)delegate;
//- (NSMutableArray *)getEarnedTransactions;
//- (NSMutableArray *)getRedeemedTransactions;
//
//- (UserPoints *)getUserPoints;
//- (int)totalPointsExpiring;
//- (int)totalPointsEarned;
//- (int)totalPointsRedeemed;
//
////CSModule
//
//- (NSString *)getNewCaseNumber;
//- (void)getAllCasesWithDelegate:(id<DatabaseManagerDelegate>)delegate;
//- (NSMutableArray *)getAllCases;
//- (int)totalUserCases;
//- (NSMutableArray *)getCasesWithType:(NSString *)caseType;
//- (NSMutableArray *)getCasesWithStatus:(int)caseStatus;
//- (NSMutableArray *)getFeedbacksWithCaseID:(NSString *)caseID;
//
//- (BOOL)addNewCase:(CaseItem *)caseitem andFeedback:(Feedback *)feedback;
//- (BOOL)addNewFeedbackToCase:(Feedback *)feedback;

//User Profile


@end
