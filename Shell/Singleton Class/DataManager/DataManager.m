//
//  DataManager.m
//  JTIStarPartners
//
//  Created by Shekhar  on 10/6/15.
//  Copyright (c) 2015 Edenred. All rights reserved.
//

#import "DataManager.h"
#import "AppDelegate.h"
#import "LocalizationManager.h"
#import "Constants.h"
#import "MenuItem.h"
#import "Helper.h"
#import "Headers.h"
#import "FileManager.h"

#import "DatabaseManager.h"

static DataManager* instance;

@interface DataManager()<WebServiceManagerDelegate>

{
    dispatch_queue_t backgroundQueue;
}

@property(nonatomic,strong) NSString *dbPath;

@end

@implementation DataManager


#pragma mark -
#pragma mark INIT

- (id) init {
    if (self = [super init])
    {
        backgroundQueue = dispatch_queue_create("bgqueue", NULL);
//        self.userPoints = [[UserPoints alloc]init];
        return self;
    }
    return nil;
}

+ (DataManager*) getInstance {
    @synchronized([DataManager class]) {
        
        if ( instance == nil ) {
            instance = [[DataManager alloc] init];
        }
    }
    return instance;
}

//- (void)saveCookies
//{
//    NSData         *cookiesData = [NSKeyedArchiver archivedDataWithRootObject: [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]];
//    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
//    [defaults setObject: cookiesData forKey: LOGIN_COOKIES];
//    [defaults synchronize];
//}
//
//- (void)loadCookies
//{
//    NSArray             *cookies       = [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults] objectForKey: LOGIN_COOKIES]];
//    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
//    
//    for (NSHTTPCookie *cookie in cookies)
//    {
//        [cookieStorage setCookie: cookie];
//    }
//}

#pragma mark -
#pragma mark Archive/Unarchive data

- (void)saveUser:(LoginInfo *)loginInfo
{
    NSString *filePath = [FileManager getFullFilePath:USER_PROFILE_ARCHIVE_NAME];
    NSData *theData = [NSKeyedArchiver archivedDataWithRootObject:loginInfo];
    [NSKeyedArchiver archiveRootObject:theData toFile:filePath];
}

- (LoginInfo *)loadUserProfile
{
    NSString *filePath = [FileManager getFullFilePath:USER_PROFILE_ARCHIVE_NAME];
    
    NSData *theData = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    LoginInfo *loginInfo = [NSKeyedUnarchiver unarchiveObjectWithData:theData];
    
    if (!loginInfo) {
        [mSession reset];
        [mSession loadLoginView];
    }
    
    return loginInfo;
}

#pragma mark Database utilities methods

- (NSString *)getDatabasePath
{
    [self createAndCheckDatabase];
    return self.dbPath;
}

- (void)createAndCheckDatabase
{
    BOOL success;
    
    NSString *dbPath = [FileManager getFilePathWithName:kDatabaseName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    success = [fileManager fileExistsAtPath:dbPath];
    
    if(success)
    {
        self.dbPath = dbPath;
        return;
    }
//    dbPath	NSPathStore2 *	@"/var/mobile/Containers/Data/Application/582862AC-2AB7-4D65-B828-F6B84AB6E7A2/Documents/localDB.db"	0x192a51a0
    NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:kDatabaseName];
    
    if([fileManager copyItemAtPath:databasePathFromApp toPath:dbPath error:nil]){
        [self addSkipBackupAttributeToItemAtURL:dbPath];
        self.dbPath = dbPath;
        // Prevent iCloud sync for the db file
    }
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSString *)path
{
    NSURL *URL = [[NSURL alloc] initFileURLWithPath:path];
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

@end

