//
//  DataManager.h
//  JTIStarPartners
//
//  Created by Shekhar  on 10/6/15.
//  Copyright (c) 2015 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import  <UIKit/UIKit.h>
#import "AlertViewManager.h"
#import "Session.h"
#import "UncaughtExceptionHandler.h"

@class Reachability;
@class UserInfo;

#define mDataManager	((DataManager *) [DataManager getInstance])

@interface DataManager : NSObject
{
}

@property(nonatomic)BOOL isUserDataChanged;
@property(nonatomic, strong)UserInfo *userInfo;
@property(nonatomic, strong)UserInfo *user;

//Session API's
+ (DataManager *)getInstance;

//Get Data from Plists in mainbundle
- (NSMutableArray *)getMenuItems:(NSString *)tier; //setup side menu
- (NSMutableArray *)getHomeMenuItems:(NSString *)tier; //setup dashboard menu
- (NSMutableArray *)getMyProfileItems; //setup my profile
- (NSMutableArray *)getSideMenuItems;
- (NSMutableArray *)getPriceCalculatorItems; //dummy data
- (NSMutableArray *)getInboxItems; //dummy data

- (NSMutableArray *)getPromoItems; //dummy data
- (NSMutableArray *)getEventItems; //dummy data
- (NSMutableArray *)getSurveyItems; //dummy data
- (NSMutableArray *)getPollItems; //dummy data

- (NSMutableArray *)getInboxItems; //dummy data

// Cache data API's
-(void)saveCookies;
-(void)loadCookies;

- (void)saveUser:(LoginInfo *)userInfo;
- (LoginInfo *)loadUserProfile;

- (void)saveProfilePic:(UIImage *)image;
- (void)saveImage:(UIImage *)image withFileName:(NSString *)fileName;
- (void)savePic:(UIImage *)image withCacheKey:(NSString *)cacheKey;
- (UIImage *)loadImageWithCacheKey:(NSString *)cacheKey withPlaceholder:(NSString *)fileName;
- (UIImage *)loadImageWithFileName:(NSString *)fileName;

//Database
- (NSString *)getDatabasePath;
- (void)createAndCheckDatabase;

@end
