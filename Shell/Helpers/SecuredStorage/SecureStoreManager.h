//
//  SecureStoreManager.h
//  Seurat
//
//  Created by ramkumar on 6/16/11.
//  Copyright 2011 Logitech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SecureStoreManager : NSObject {

	NSString	*mServiceName;
}

@property (nonatomic, retain) NSString *mServiceName;


- (id)initWithServiceName:(NSString *)serviceName;
- (BOOL)setKeyValue:(NSString *)value forKey:(NSString *)key;
- (BOOL)deleteKeyValue:(NSString *)key;
- (NSString *)getValueForKey:(NSString *)key;


@end
