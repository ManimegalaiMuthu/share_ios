//
//  SecureStoreManager.m
//  Seurat
//
//  Created by ramkumar on 6/16/11.
//  Copyright 2011 Logitech. All rights reserved.
//

#import "SecureStoreManager.h"


//private methods
@interface SecureStoreManager (private)

- (NSMutableDictionary *)getDictionaryWithIdentifier:(NSString *)identifier;

@end



@implementation SecureStoreManager


@synthesize mServiceName;


// Initialization
- (id) initWithServiceName:(NSString *) serviceName {
	
    if ((self = [super init])) {
        self.mServiceName = serviceName;
    }
		
	return self;
}

#pragma mark -
#pragma mark keychain methods starts


#pragma mark -
#pragma mark keychain method setKeyValue:forKey

- (BOOL)setKeyValue:(NSString *)value forKey:(NSString *)key {
    
    //delete the value if already exists for key
    [self deleteKeyValue:key];
    
    NSMutableDictionary *dictionary = [self getDictionaryWithIdentifier:key];
	
    NSData *passwordData = [value dataUsingEncoding:NSUTF8StringEncoding];
    [dictionary setObject:passwordData forKey:(id)kSecValueData];
	
    OSStatus status = SecItemAdd((CFDictionaryRef)dictionary, NULL);
	
    if (status == errSecSuccess) {
        return YES;
    }
    return NO;
}



#pragma mark -
#pragma mark keychain method getValueForKey

- (NSString *)getValueForKey:(NSString *)key {
    
    NSMutableDictionary *searchDictionary = [self getDictionaryWithIdentifier:key];
	
    // Add search attributes
    [searchDictionary setObject:(id)kSecMatchLimitOne forKey:(id)kSecMatchLimit];
	
    // Add search return types
    [searchDictionary setObject:(id)kCFBooleanTrue forKey:(id)kSecReturnData];
	
    NSData *result = nil;
    OSStatus status = SecItemCopyMatching((CFDictionaryRef)searchDictionary,
                                          (CFTypeRef *)&result);
	
	NSString *value = nil;
	
	if (status == errSecSuccess && result){
		value = [[[NSString alloc] initWithData:result   encoding:NSUTF8StringEncoding] autorelease];
	}
	
	//[searchDictionary release];
	[result release];
	
	return value;
}


#pragma mark -
#pragma mark keychain method deleteKeyValue

- (BOOL)deleteKeyValue:(NSString *)key {
	
    NSMutableDictionary *searchDictionary = [self getDictionaryWithIdentifier:key];
    OSStatus status = SecItemDelete((CFDictionaryRef)searchDictionary);
    //[searchDictionary release];
    
    if(status == errSecSuccess){
        return YES;
    }
    
    return NO;
}


#pragma mark -
#pragma mark keychain methods ends


#pragma mark -
#pragma mark keychain support methods starts

- (NSMutableDictionary *)getDictionaryWithIdentifier:(NSString *)identifier {
    
    
    NSMutableDictionary *searchDictionary = [[[NSMutableDictionary alloc] init] autorelease];
    [searchDictionary setObject:(id)kSecClassGenericPassword forKey:(id)kSecClass];
	
    NSData *encodedIdentifier = [identifier dataUsingEncoding:NSUTF8StringEncoding];
    [searchDictionary setObject:encodedIdentifier forKey:(id)kSecAttrGeneric];
    [searchDictionary setObject:encodedIdentifier forKey:(id)kSecAttrAccount];
    [searchDictionary setObject:self.mServiceName forKey:(id)kSecAttrService];
	
    return searchDictionary; 
}






@end
