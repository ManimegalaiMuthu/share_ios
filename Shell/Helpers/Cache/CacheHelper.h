//
//  Helper.h
//  JTIStarPartners
//
//  Created by Shekhar on 20/11/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CacheHelper : NSObject

+ (NSArray *)getSavedSearches;
+ (void)saveSuccessfulSearch:(NSString *) strSearch;
+ (void)saveWeatherDataWithSearchString:(NSString *)location data:(NSMutableDictionary *)data;
+ (NSDictionary *)getWeatherDetailsOfCity:(NSString *)city;

@end
