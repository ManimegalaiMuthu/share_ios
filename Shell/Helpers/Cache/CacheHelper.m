//
//  Helper.m
//  JTIStarPartners
//
//  Created by Shekhar on 20/11/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import "CacheHelper.h"
#import "NSArray+Search.h"

@implementation CacheHelper

+ (void) saveSuccessfulSearch:(NSString *) strSearch {
    NSMutableArray *locations = [NSMutableArray array];
    NSArray *loc=[self getSavedSearches];
    if (loc) {
        locations=[[self getSavedSearches]mutableCopy];
        if ([locations count]==10) {
            [locations removeObjectAtIndex:0];
        }
    }
    if (![locations checkIfExistsInCache:strSearch]) {
        [locations addObject:strSearch];
        [locations writeToFile:[self getSavedSearchesFilePath] atomically:YES];
    }
}

+ (NSArray *) getSavedSearches {
    NSArray * values=[[NSArray alloc] initWithContentsOfFile:[self getSavedSearchesFilePath]];
    return values;
}

+ (NSDictionary *) getWeatherDetailsOfCity:(NSString *)city {
    NSDictionary * values=[[NSDictionary alloc] initWithContentsOfFile:[self getCityWeatherDataFilePath:city]];
    return values;
}

+ (void) saveWeatherDataWithSearchString:(NSString *)location data:(NSMutableDictionary *)data {
    
    [data writeToFile:[self getCityWeatherDataFilePath:location] atomically:YES];
}

+ (NSString *)getSavedSearchesFilePath {
    NSString *docsPath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docsPath stringByAppendingPathComponent:@"savedsearches.plist"];
    return filePath;
}

+ (NSString *)getCityWeatherDataFilePath:(NSString *)location {
    NSString *docsPath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",location]];
    return filePath;
}


@end
