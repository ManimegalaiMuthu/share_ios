//
//  UIColorExtension.swift
//  Shell
//
//  Created by MUTHUPANDIYAN Manimegalai on 14/10/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
  
  convenience init?(hexString: String) {
    
    let chars = Array(hexString.hasPrefix("#") ? hexString.dropFirst() : hexString[...])
    
    if chars.count == 6 { //no alpha
      
      let r = CGFloat(Int(String(chars[0...1]), radix: 16) ?? 0) / 255
      let g = CGFloat(Int(String(chars[2...3]), radix: 16) ?? 0) / 255
      let b = CGFloat(Int(String(chars[4...5]), radix: 16) ?? 0) / 255
      
      self.init(red: r, green: g, blue: b, alpha: 1)
      return
    }
    
    if chars.count == 8 { //with alpha
      
      let r = CGFloat(Int(String(chars[0...1]), radix: 16) ?? 0) / 255
      let g = CGFloat(Int(String(chars[2...3]), radix: 16) ?? 0) / 255
      let b = CGFloat(Int(String(chars[4...5]), radix: 16) ?? 0) / 255
      let a = CGFloat(Int(String(chars[6...7]), radix: 16) ?? 0) / 255
      
      self.init(red: r, green: g, blue: b, alpha: a)
      return
    }
    
    return nil
  }

  
  //Primary plus white
  static let COLOUR_DARKBLUE:UIColor = UIColor(red: 0/255, green: 60/255, blue: 136/255, alpha: 1)
  static let COLOUR_SHELLRED:UIColor = UIColor(red: 221/255, green: 29/255, blue: 33/255, alpha: 1)
  static let COLOUR_YELLOW:UIColor = UIColor(red: 251/255, green: 206/255, blue: 7/255, alpha: 1)
  
  //Secondary
  static let COLOUR_WHITE:UIColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
  static let COLOUR_LIGHTBLUE:UIColor = UIColor(red: 137/255, green: 207/255, blue: 220/255, alpha: 1)
  static let COLOUR_LIGHTGREEN:UIColor = UIColor(red: 190/255, green: 213/255, blue: 15/255, alpha: 1)
    

    
    
  static let COLOUR_VERYDARKGREY:UIColor = UIColor(red: 64/255, green: 64/255, blue: 64/255, alpha: 1)
  static let COLOUR_DARKGREY:UIColor = UIColor(red: 89/255, green: 89/255, blue: 89/255, alpha: 1)
  static let COLOUR_MIDGREY:UIColor = UIColor(red: 127/255, green: 127/255, blue: 127/255, alpha: 1)
    
    
  
    
    
    
  static let COLOUR_LIGHTGREY:UIColor = UIColor(red: 166/255, green: 166/255, blue: 166/255, alpha: 1)
  static let COLOUR_PALEGREY:UIColor = UIColor(red: 217/255, green: 217/255, blue: 217/255, alpha: 1)
  
  //Neutral plus black
  static let COLOUR_VERYPALEGREY:UIColor = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)
  static let COLOUR_BACKGROUNDGREY:UIColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)

  static let COLOUR_DARKGREEN:UIColor = UIColor(red: 0/255, green: 132/255, blue: 67/255, alpha: 1)
  
  //Graph Colors
  static let COLOUR_BROWN:UIColor = UIColor(red: 116/255, green: 52/255, blue: 16/255, alpha: 1)
  static let COLOUR_PURPLE:UIColor = UIColor(red: 100/255, green: 25/255, blue: 100/255, alpha: 1)
  static let GraphYellow:UIColor = UIColor(red: 245/255, green: 208/255, blue: 15/255, alpha: 1)
  static let GraphLightGreen:UIColor = UIColor(red: 117/255, green: 238/255, blue: 88/255, alpha: 1)
  static let GraphGreen:UIColor = UIColor(red: 50/255, green: 170/255, blue: 78/255, alpha: 1)
  
  //Unused
  static let PieTierFirst:UIColor = UIColor(red: 117/255, green: 242/255, blue: 88/255, alpha: 1)
  static let PieShadowFirst:UIColor = UIColor(red: 185/255, green: 246/255, blue: 171/255, alpha: 1)
  
  static let PieTierSecond:UIColor = UIColor(red: 121/255, green: 203/255, blue: 233/255, alpha: 1)
  static let PieShadowSecond:UIColor = UIColor(red: 187/255, green: 229/255, blue: 244/255, alpha: 1)
  
  static let PieTierThird:UIColor = UIColor(red: 248/255, green: 163/255, blue: 60/255, alpha: 1)
  static let PieShadowThird:UIColor = UIColor(red: 251/255, green: 209/255, blue: 157/255, alpha: 1)
  
  static let PieTierFourth:UIColor = UIColor(red: 245/255, green: 208/255, blue: 16/255, alpha: 1)
  static let PieShadowFourth:UIColor = UIColor(red: 250/255, green: 231/255, blue: 135/255, alpha: 1)
  
  static let PieTierFifth:UIColor = UIColor(red: 150/255, green: 114/255, blue: 117/255, alpha: 1)
  static let PieShadowFifth:UIColor = UIColor(red: 202/255, green: 184/255, blue: 185/255, alpha: 1)
  
  static let PieTierSixth:UIColor = UIColor(red: 80/255, green: 80/255, blue: 80/255, alpha: 1)
  static let PieShadowSixth:UIColor = UIColor(red: 167/255, green: 167/255, blue: 167/255, alpha: 1)
  
  static let PieTierSeventh:UIColor = UIColor(red: 2/255, green: 171/255, blue: 78/255, alpha: 1)
  static let PieShadowSeventh:UIColor = UIColor(red: 128/255, green: 213/255, blue: 166/255, alpha: 1)
  
}
