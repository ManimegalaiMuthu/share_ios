//
//  Utils.swift
//  Shell
//
//  Created by MUTHUPANDIYAN Manimegalai on 13/10/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

import Foundation
import UIKit
import iOSDropDown


class Utils{
    static var loading:UIActivityIndicatorView!
    
    var btn:UIButton?
    init() {}
    
    static func getViewController(status: Int,storyBoard: UIStoryboard) -> UIViewController {
        var id = ""
        var vc = UIViewController()
        switch status {
        case Constants.PENDING_WORKSHOP_STATUS:
            id = Constants.VControllerIdentifier.ACCEPT_DECLINE_MODIFY_ID
            vc = storyBoard.instantiateViewController(withIdentifier: id) as! AcceptDeclineModifyViewController
            break
        case Constants.PENDING_CUSTOMER_STATUS:
            id = Constants.VControllerIdentifier.ACCEPT_DECLINE_MODIFY_ID
            vc = storyBoard.instantiateViewController(withIdentifier: id) as! AcceptDeclineModifyViewController
            break
        case Constants.CONFIRMED_STATUS:
            id = Constants.VControllerIdentifier.BOOKING_DETAILS_ID
            vc = storyBoard.instantiateViewController(withIdentifier: id) as! BookingDetailsViewController
            break
        case Constants.CANCELLED_STATUS:
            id = Constants.VControllerIdentifier.BOOKING_DETAILS_ID
            vc = storyBoard.instantiateViewController(withIdentifier: id) as! BookingDetailsViewController
            break
        case Constants.DECLINED_STATUS:
            id = Constants.VControllerIdentifier.BOOKING_DETAILS_ID
            vc = storyBoard.instantiateViewController(withIdentifier: id) as! BookingDetailsViewController
            break
        case Constants.SERVICE_STARTS_STATUS:
            id = Constants.VControllerIdentifier.BOOKING_DETAILS_ID
            vc = storyBoard.instantiateViewController(withIdentifier: id) as! BookingDetailsViewController
            break
        default:
            id = Constants.VControllerIdentifier.BOOKING_DETAILS_ID
            vc = storyBoard.instantiateViewController(withIdentifier: id) as! BookingDetailsViewController
        }
        return vc
        
    }
    
    static func displayToastMessage(_ message : String,view : UIView) {
        let toastView = UILabel()
        toastView.backgroundColor = UIColor.systemYellow//UIColor.black.withAlphaComponent(0.7)
        toastView.textColor = UIColor.COLOUR_SHELLRED
        toastView.textAlignment = .center
        toastView.font = UIFont.preferredFont(forTextStyle: .caption1)
        toastView.layer.cornerRadius = 25
        toastView.layer.masksToBounds = true
        toastView.text = message
        toastView.numberOfLines = 0
        toastView.alpha = 0
        toastView.translatesAutoresizingMaskIntoConstraints = false

        //let window = UIApplication.shared.delegate?.window!
        view.addSubview(toastView)

        let horizontalCenterContraint: NSLayoutConstraint = NSLayoutConstraint(item: toastView, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)

        let widthContraint: NSLayoutConstraint = NSLayoutConstraint(item: toastView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 275)

        let verticalContraint: [NSLayoutConstraint] = NSLayoutConstraint.constraints(withVisualFormat: "V:|-(>=200)-[loginView(==50)]-68-|", options: [.alignAllCenterX, .alignAllCenterY], metrics: nil, views: ["loginView": toastView])

        NSLayoutConstraint.activate([horizontalCenterContraint, widthContraint])
        NSLayoutConstraint.activate(verticalContraint)

        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
            toastView.alpha = 1
        }, completion: nil)

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(2 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC), execute: {
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
                toastView.alpha = 0
            }, completion: { finished in
                toastView.removeFromSuperview()
            })
        })
    }
   static func setUpNavigationBar(navigationController: UINavigationController,
                            navigationItem:UINavigationItem,
                            title:String){
    navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.FONT_BOLD(ofSize: 18),NSAttributedString.Key.foregroundColor : UIColor.COLOUR_SHELLRED]
            navigationItem.title = title//
            navigationItem.leftBarButtonItem?.tintColor = UIColor.COLOUR_SHELLRED
    }
    
    static func showLoading(view: UIView){
        loading = UIActivityIndicatorView(frame: CGRect(x: 50, y: 50, width: 100, height: 100))
        let container: UIView = UIView()
        container.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        container.backgroundColor = .clear
        loading.center = view.center
        loading.activityIndicatorViewStyle = UIActivityIndicatorView.Style.large
        loading.color = UIColor.COLOUR_SHELLRED
        container.addSubview(loading)
        view.addSubview(container)
        view.isUserInteractionEnabled = false
        loading.startAnimating()
        
    }
    static func stopLoading(view: UIView){
        if(loading.isAnimating){
            view.isUserInteractionEnabled = true
            loading.stopAnimating()
        }
    }
    static func isAllInputsValid(textFields: [UITextField])-> Bool{
        for input in textFields{
            if(input.text!.isEmpty && input.text!.count < 3){
                input.isError(baseColor: UIColor.COLOUR_SHELLRED.cgColor, numberOfShakes: 3, revert: true)
                return false
            }
        }
        return true
    }
    static func isAllBTNInputsValid(textFields: [UIButton])-> Bool{
        for input in textFields{
            if(input.titleLabel!.text!.isEmpty){
                input.isError(baseColor: UIColor.COLOUR_SHELLRED.cgColor, numberOfShakes: 3, revert: true)
                return false
            }
        }
        return true
    }
    static func statusObjects()-> [StatusObject]{
        
        var status = [StatusObject]()
        status.append(StatusObject(id: Constants.PENDING_WORKSHOP_STATUS,statusDescription: Constants.PENDING_WORKSHOP))
        status.append(StatusObject(id: Constants.PENDING_CUSTOMER_STATUS,statusDescription: Constants.PENDING_CUSTOMER))
        status.append(StatusObject(id: Constants.CONFIRMED_STATUS,statusDescription: Constants.CONFIRMED))
        status.append(StatusObject(id: Constants.CANCELLED_STATUS,statusDescription: Constants.CANCELLED))
        status.append(StatusObject(id: Constants.DECLINED_STATUS,statusDescription: Constants.DECLINED))
        status.append(StatusObject(id: Constants.SERVICE_STARTS_STATUS,statusDescription: Constants.SERVICE_STARTS))
        status.append(StatusObject(id: Constants.IN_PROGRESS_STATUS,statusDescription: Constants.IN_PROGRESS))
        status.append(StatusObject(id: Constants.SERVICE_ENDS_STATUS,statusDescription: Constants.SERVICE_ENDS))
        status.append(StatusObject(id: Constants.NO_SHOW_STATUS,statusDescription: Constants.NO_SHOW))
        status.append(StatusObject(id: Constants.COMPLETED_STATUS,statusDescription: Constants.COMPLETED))
        return status
    }
    static func getStatusDescription(object: [StatusObject]) -> [String]{
        var status = [String]()
        for n in 0...object.count - 1 {
            status.append(object[n].statusDescription!)
        }
        return status
        
    }
    static func getStatusID(object: [StatusObject]) -> [Int]{
        var status = [Int]()
        for n in 0...object.count - 1 {
            status.append(object[n].id!)
        }
        return status
    }
    
    static func addPadding(textField: UITextField){
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 20))
        textField.leftView = paddingView
        textField.leftViewMode = .always
    }
   static func getCurrentDate(date:Date) -> String
    {
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "EEEE, dd MMMM, yyyy"
      return dateFormatter.string(from: date)
    }
    static func getBookingStatus(status: Int)->String{
        var bookingStatus = ""
        switch status {
        case Constants.pendingStatus:
            bookingStatus = "Pending Confirmation"
        case Constants.readyStatus:
            bookingStatus = "Ready"
        case Constants.inProgressStatus:
            bookingStatus = "In Progress"
        default:
            bookingStatus = "Confirmed"
        }
        return bookingStatus
    }
    
    var toolBar = UIToolbar()
    var datePicker  = UIDatePicker()
    
    func setDateTimePicker(view: UIView){
       
        datePicker.frame = CGRect(x: 0.0, y: UIScreen.main.bounds.size.height - 500, width: UIScreen.main.bounds.size.width, height: 500)
        datePicker.backgroundColor = UIColor.COLOUR_WHITE
        view.addSubview(datePicker)
        toolBar = UIToolbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.size.height - 500, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = UIBarStyle.black
        toolBar.barTintColor = UIColor.COLOUR_YELLOW
        
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(onDoneButtonClick))
        doneButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.COLOUR_SHELLRED], for: .normal)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(onDoneButtonClick))
        cancelButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.COLOUR_SHELLRED], for: .normal)
        toolBar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        toolBar.sizeToFit()
        view.addSubview(toolBar)
        
       }
    func showDatePicker(view: UIView,btn: UIButton){
         self.btn = btn
        //let yesterday = -1
         let today = 0
        //let tommorow = 1
         datePicker = UIDatePicker.init()
         datePicker.autoresizingMask = .flexibleWidth
         datePicker.minimumDate = Calendar.current.date(byAdding: .day, value: today, to: Date())
         datePicker.maximumDate = Calendar.current.date(byAdding: .day, value: 30, to: Date())
         datePicker.datePickerMode = .date
         datePicker.preferredDatePickerStyle = .inline
            
        let selector =  #selector(dateChanged(_:))
        datePicker.addTarget(self, action: selector, for: .valueChanged)
        setDateTimePicker(view: view)
        
    }
        
    func showTimePicker(view: UIView,btn: UIButton) {
         self.btn = btn
        datePicker = UIDatePicker.init()
         datePicker.backgroundColor = UIColor.white
         datePicker.autoresizingMask = .flexibleWidth
        
        let calendar = Calendar.current
        var minDateComponent = calendar.dateComponents([.hour], from: Date())
        minDateComponent.hour = 09 // Start time


        let formatter = DateFormatter()
        formatter.dateFormat = "h:mma"
        let minDate = calendar.date(from: minDateComponent)
        print(" min date : \(formatter.string(from: minDate!))")

        var maxDateComponent = calendar.dateComponents([.hour], from: Date())
        maxDateComponent.hour = 17 //EndTime
        let maxDate = calendar.date(from: maxDateComponent)
        
        datePicker.minimumDate = minDate //Calendar.current.date(byAdding: .hour, value: 8, to: Date())
        datePicker.maximumDate = maxDate //Calendar.current.date(byAdding: .hour, value: 17, to: Date())
        datePicker.datePickerMode = .time
        datePicker.preferredDatePickerStyle = .wheels
          let selector = #selector(timeChanged(_:))
          datePicker.addTarget(self, action: selector, for: .valueChanged)
        setDateTimePicker(view: view)
    }
    
    @objc func dateChanged(_ sender: UIDatePicker?) {
           let dateFormatter = DateFormatter()
           dateFormatter.dateStyle = .long
           dateFormatter.timeStyle = .none
           dateFormatter.dateFormat = "dd-M-yyyy"
           if let date = sender?.date {
               print("Picked the date \(dateFormatter.string(from: date))")
               let dateSelected = dateFormatter.string(from: date)
            self.btn?.setTitle(dateSelected, for: .normal)
           }
       }
       @objc func timeChanged(_ sender: UIDatePicker?) {
           let dateFormatter = DateFormatter()
           dateFormatter.dateStyle = .none
           dateFormatter.timeStyle = .long
           dateFormatter.dateFormat = "h:mm"
           if let date = sender?.date {
               print("Picked the time \(dateFormatter.string(from: date))")
               let dateSelected = dateFormatter.string(from: date)
                self.btn?.setTitle(dateSelected, for: .normal)
           }
       }

    @objc func onDoneButtonClick() {
           toolBar.removeFromSuperview()
           datePicker.removeFromSuperview()
       }

    
    
    
    
}
