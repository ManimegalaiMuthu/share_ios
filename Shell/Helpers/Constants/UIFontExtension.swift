//
//  UIFontExtension.swift
//  ersg Base
//
//  Created by Ben on 5/11/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

import Foundation
import UIKit

/*extension UIFont {
  
  static func FONT_HEAD(ofSize size:CGFloat) -> UIFont {

    return UIFont(name: "ShellFutura-Book", size: size) ?? UIFont.systemFont(ofSize: size)
  }
  static func FONT_BOLD(ofSize size:CGFloat) -> UIFont {
    return UIFont(name: "ShellFutura-Bold", size: size) ?? UIFont.systemFont(ofSize: size)
  }
  static func NuFarmMainFontItalic(ofSize size:CGFloat) -> UIFont {
    return UIFont(name: "Arial-ItalicMT", size: size) ?? UIFont.systemFont(ofSize: size)
  }
  static func NuFarmMainFontBoldItalic(ofSize size:CGFloat) -> UIFont {
    return UIFont(name: "Arial-BoldItalicMT", size: size) ?? UIFont.systemFont(ofSize: size)
  }
}*/

extension UIFont {
    
  
  static func FONT_HEAD(ofSize size:CGFloat) -> UIFont {
    
    if (UserDefaults.standard.string(forKey: Constants.UserDefault.LanguageCode) == "th"){
        return UIFont(name: "Kittithada Roman 55", size: size) ?? UIFont.systemFont(ofSize: size)
        
    }else if(UserDefaults.standard.string(forKey: Constants.UserDefault.LanguageCode) == "vi"){
        return UIFont(name: "ArialMT", size: size) ?? UIFont.systemFont(ofSize: size)
        
    }else if(UserDefaults.standard.string(forKey: Constants.UserDefault.LanguageCode) == "ar"){
        return UIFont(name: "ShellARB-Book", size: size) ?? UIFont.systemFont(ofSize: size)
    }else if(UserDefaults.standard.string(forKey: Constants.UserDefault.LanguageCode) == "ur"){
        return UIFont(name: "ArialMT", size: size) ?? UIFont.systemFont(ofSize: size)
    }else{
        print(UserDefaults.standard.string(forKey: Constants.UserDefault.LanguageCode) ?? "1")
        return UIFont(name: "ShellFutura-Book", size: size) ?? UIFont.systemFont(ofSize: size)
    }
  }
    

    static func FONT_BOLD(ofSize size:CGFloat) -> UIFont {
        if (UserDefaults.standard.string(forKey: Constants.UserDefault.LanguageCode) == "th"){
            
            return UIFont(name: "Kittithada Bold 75", size: size) ?? UIFont.systemFont(ofSize: size)
            
        }else if(UserDefaults.standard.string(forKey: Constants.UserDefault.LanguageCode) == "vi"){
            return UIFont(name: "Arial-BoldMT", size: size) ?? UIFont.systemFont(ofSize: size)
            
        }else if(UserDefaults.standard.string(forKey: Constants.UserDefault.LanguageCode) == "ar"){
            return UIFont(name: "ShellARB-Bold", size: size) ?? UIFont.systemFont(ofSize: size)
            
        }else if(UserDefaults.standard.string(forKey: Constants.UserDefault.LanguageCode) == "ur"){
            return UIFont(name: "Arial-BoldMT", size: size) ?? UIFont.systemFont(ofSize: size)
        }else{
            return UIFont(name: "ShellFutura-Bold", size: size) ?? UIFont.systemFont(ofSize: size)
        }
    }
}
