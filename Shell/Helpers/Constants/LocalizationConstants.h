//Global

#define C_GLOBAL_LOADING         @"misc_pleasewait"
#define C_GLOBAL_SIGNIN         @"misc_signingin"
#define C_GLOBAL_SUBMIT         @"misc_submitting"
#define C_GLOBAL_UPDATE         @"misc_updating"
#define C_GLOBAL_ERROR         @"popup_error"
#define C_GLOBAL_SIGNOUT         @"misc_signout"
#define C_GLOBAL_YES         @"btn_yes"
#define C_GLOBAL_NO         @"btn_no"
#define C_GLOBAL_OK         @"btn_ok"

#define C_GLOBAL_NONETWORK         @"mic_error_offline"
#define C_GLOBAL_TIMEOUT         @"mic_request_timeout"
#define C_GLOBAL_NETWORK         @"popup_error"
#define C_GLOBAL_SUCCESS         @"misc_success"
#define C_GLOBAL_VERSION         @"misc_version"

#define C_GLOBAL_SESSION_EXPIREDLOGINAGAIN         @"misc_sessionexpired"

//NAVIGATIONTITLES
#define C_TITLE_LOGIN         @"title_login"

#define C_TITLE_REGISTRATION         @"title_registration"
#define C_TITLE_FORGETPASS         @"title_forgetpassword"
#define C_TITLE_WORKINGHOURS         @"title_workinghours"
#define C_TITLE_HOME         @"title_home"
#define C_TITLE_MANAGEWORKSHOP         @"title_manageworkshops"
#define C_TITLE_MANAGEMECHANIC         @"title_managestaff"
#define C_TITLE_MECHANICDETAILS         @"title_staffdetails"
#define C_TITLE_PERFORMANCE         @"title_performance"
#define C_TITLE_MANAGEORDER         @"title_manageorders"
#define C_TITLE_PARTICIPATINGPRODUCT         @"title_participating_products"

#define C_TITLE_SCANPRODUCTCODE         @"title_registeroilchange"
#define C_TITLE_PENDINGCODE         @"title_pendingcode"
#define C_TITLE_MYPROFILE         @"home_myprofile_header"
#define C_TITLE_SEARCHVEHICLE         @"title_searchvehicle"
#define C_TITLE_CASHINCENTIVE       @"title_cashincentive"
#define C_TITLE_CHANGEPASS         @"profile_title_changepassword"
#define C_TITLE_TNC         @"title_tnc"
#define C_TITLE_FAQ         @"title_faq"
#define C_TITLE_CONTACTUS         @"title_contact"
#define C_TITLE_SETTINGS         @"title_settings"
#define C_TITLE_NOTIFICATION         @"title_notification"
#define C_TITLE_WORKSHOPDETAIL         @"form_title_workshopdetails"
#define C_TITLE_WORKSHOPPROFILING         @"form_profiling"
#define C_TITLE_WORKSHOPDETAILSPERFORMANCE         @"manageworkshops_performance"
#define C_TITLE_ALLPRODUCT         @"title_all_products"
#define C_TITLE_SHAREPRODUCT         @"title_share_products"
#define C_TITLE_MYPOINTS         @"title_mypoints"
#define C_TITLE_WORKSHOPPERFORMANCE         @"title_performance_workshopperformance"
#define C_TITLE_REWARDS         @"title_myrewards"
#define C_TITLE_REGISTERCODERESULT         @"title_registeroilchange"
#define C_TITLE_PRIVACY         @"title_privacy"

#define C_TITLE_CONTACT_PREFERENCE         @"form_contactpreference"

//Alerthead
#define C_ALERTHEADER_PROMOOFFER         @"popup_entitledoffer_header"
#define C_PLACEHOLDER_PROMO_OFFER         @"searchvehicle_promocode_placeholder"
#define C_ALERTHEADER_INVITECONSUMER         @"popup_inviteconsumer_header"
#define C_INVITE_CUSTOMER         @"searchvehicle_invitecustomer"
#define C_ALERTVIEW_ERROR         @"popup_error"
#define C_ALERTVIEW_PERMISSIONERRORMSG         @"popup_enablePermission"
#define C_ALERTVIEW_CANCEL         @"btn_cancel"
#define C_ALERTVIEW_SETTING         @"btn_settings"

//LOGIN
//Username
#define C_LOGIN_USERNAME_PLACEHOLDER         @"form_mobilenumber_placeholder"

#define C_LOGIN_PASSWORD_PLACEHOLDER         @"form_enterpassword_placeholder"
#define C_LOGIN_ENTER         @"btn_login"
#define C_LOGIN_KEEPLOGIN         @"login_keeploggedin"
#define C_LOGIN_FORGET         @"login_forgetpw"

#define C_LOGIN_REGISTER         @"login_register"
#define C_LOGIN_CONTACT         @"login_contactus"

#define C_LOGIN_FORGETHEADER         @"forgetpassword_headercontent"

#define C_LOGIN_AGREETNCPRIVACY         @"login_disclaimer"
#define C_LOGIN_TNC         @"login_tnc_link"
#define C_LOGIN_PRIVACY         @"login_privacy_link"

#define C_OTP_PLACEHOLDER         @"form_otp_placeholder"

//Formbutton
#define C_FORM_VIEWPROFILE         @"btn_viewprofile"
#define C_FORM_VIEWDETAIL         @"btn_viewdetails"
#define C_FORM_UPDATEPROFILE         @"btn_updateprofile"
#define C_FORM_MANAGEMECHANIC         @"btnManageStaff"
#define C_FORM_UPLOADWORKSHOPIMAGE         @"btn_uploadworkshopimages"
#define C_FORM_UPDATE         @"btn_update"
#define C_FORM_REGISTER         @"btnRegister"
#define C_FORM_REGISTERNEWSTAFF         @"btn_registernewstaff"
#define C_FORM_SCAN         @"btn_scanproductcode"
#define C_FORM_MANUAL         @"btn_manualproductcode"
#define C_FORM_YES         @"btn_yes"
#define C_FORM_NO         @"btn_no"
#define C_FORM_OFFLINESCAN         @"registeroilchange_offlinescan"
#define C_FORM_SUBMIT         @"btn_submit"
#define C_FORM_CANCEL         @"btn_cancel"
#define C_FORM_CLEAR         @"btn_clear"
#define C_FORM_UPDATEWORKSHOPPROFILE         @"btn_updateworkshop"

#define C_FORM_CHANGEPASSWORD         @"profile_title_changepassword"


//REGISTRATION
#define C_REGISTER_TYPE         @"dropdown_choose_profile"
#define C_REGISTER_WORKSHOP         @"registration_workshop_owner"
#define C_REGISTER_MECHANIC         @"registration_workshop_staff"

#define C_REGISTER_SUBMIT         @"btn_continue"
#define C_REGISTER_RESENDOTP         @"form_resentotp"
#define C_REGISTER_CONTINUE         @"btn_continue_registration"
#define C_REGISTER_SUBMITOTP         @"btn_submitotp"


//Performance
#define C_PERFORMANCE_PROGRAMPERFORMANCE         @"performance_programperformance"
#define C_PERFORMANCE_TOP5WORKSHOP         @"performance_topfiveworkshops"      //Nt Needed, to remove later
#define C_PERFORMANCE_WORKSHOP         @"title_performance_workshop"

//SIDEMENU
#define C_SIDEMENU_DSRMANAGEWORKSHOP         @"drawer_managemyworkshops"
#define C_SIDEMENU_DSRMANAGEORDER         @"drawer_manageorders"
#define C_SIDEMENU_INVITETRADE         @"drawer_invitetrade"
#define C_TABMENU_WORKSHOPPERFORMANCE         @"tabmenu_workshopperformance"

//CONTACTUS
#define C_CONTACTUS_SALUTATION         @"form_salutation"
#define C_CONTACTUS_FIRSTNAME         @"form_firstname"
#define C_CONTACTUS_LASTNAME         @"form_lastname"
#define C_CONTACTUS_MOBILE         @"form_mobilenumber"
#define C_CONTACTUS_EMAIL         @"form_emailaddress"
#define C_CONTACTUS_ENQUIRY         @"form_enquiry"
#define C_CONTACTUS_MESSAGE         @"form_message"
#define C_CONTACTUS_HEADER         @"contact_headercontent"

//TRADE
#define C_SIDEMENU_SEARCHVEHICLEID         @"drawer_searchvehicleid"
#define C_SIDEMENU_SCANPRODUCTCODE         @"drawer_regoilchange"
#define C_SIDEMENU_SCANCUSTCODE         @"drawer_customerredemption"
#define C_SIDEMENU_MYREWARDS         @"drawer_myrewards"

//MECHANIC
#define C_SIDEMENU_PERFORMANCEPOINTS         @"performance_workshopperformance"          //Nt Needed, to remove later


//COMMON
#define C_SIDEMENU_HOME         @"drawer_home"
#define C_SIDEMENU_MYPROFILE         @"home_myprofile_header"
#define C_SIDEMENU_PARTICIPATINGPRODUCTS         @"drawer_participatingproducts"
#define C_SIDEMENU_FAQ         @"drawer_faq"
#define C_SIDEMENU_CONTACTUS         @"drawer_contactus"
#define C_SIDEMENU_TNC         @"drawer_tnc"
#define C_SIDEMENU_SETTINGS         @"drawer_settings"


//Manageorder
#define C_MANAGEORDER_PENDING         @"title_manageorders_pending"
#define C_MANAGEORDER_ADDNEWORDER         @"title_manageorders_addnew"

//Manageworkshop
#define C_MANAGEWORKSHOP_PENDING         @"title_manageworkshops_pending"
#define C_MANAGEWORKSHOP_APPROVE         @"title_manageworkshops_approved"
#define C_MANAGEWORKSHOP_ADDNEW         @"title_manageworkshops_addnew"

//PROFILE(REGISTRATION,PROFILE)

#define C_HEADER_MYPROFILE         @"home_myprofile_header"
#define C_HEADER_MYWORKSHOP         @"registration_my_workshop"
#define C_HEADER_WORKSHOPPROFILE         @"form_title_workshopprofile"
#define C_HEADER_OWNERPROFILE         @"form_title_ownerprofile"
#define C_HEADER_PROFILE         @"title_profile"

#define C_PROFILE_SALUTATION         @"form_salutation"
#define C_PROFILE_SALUTATION_PLACEHOLDER         @"dropdown_salutation"
#define C_PROFILE_FIRSTNAME         @"form_firstname"
#define C_PROFILE_LASTNAME         @"form_lastname"
#define C_PROFILE_MOBILENUM         @"form_mobilenumber"
#define C_PROFILE_EMAILADDRESS         @"form_emailaddress"
#define C_PROFILE_WORKSHOPEMAILADDRESS         @"form_workshop_email"
#define C_PROFILE_WORKSHOPEMAILADDRESSOPT         @"form_workshop_email"
#define C_PROFILE_EMAILADDRESSOPT         @"PROFILE_EMAILADDRESSOPT"
#define C_PROFILE_OWNEREMAILADDRESSOPT         @"form_owneremailaddress"
#define C_PROFILE_EMAILADDRESS_PLACEHOLDER         @"form_workshop_email_placeholder"
#define C_PROFILE_DOB         @"form_dateofbirth"
#define C_PROFILE_DOB_PLACEHOLDER         @"dropdown_birthday"
#define C_PROFILE_DSRCODE         @"registration_dsrcode_placeholder"
#define C_PROFILE_WORKSHOPSHARECODE         @"form_workshopsharecode"
#define C_PROFILE_LOCATION         @"form_gpslocation"

#define C_PROFILE_STATUS         @"form_workshopsharestatus"
#define C_PROFILE_APPROVALSTATUS         @"form_distributorapproval"
#define C_PROFILE_ACTIVATIONSTATUS         @"form_workshopsharestatus"
#define C_PROFILE_ACTIVATIONREMARKS         @"form_workshopshareremark"
#define C_PROFILE_ACTIVATIONREMARKS_PLACEHOLDER         @"form_workshopshareremark_placeholder"

#define C_PROFILE_CREATEPASS         @"form_createpassword_placeholder"
#define C_PROFILE_CONFPASS         @"form_confirm_password"

#define C_PROFILE_WORKSHOPTYPE         @"form_workshoptype"
#define C_PROFILE_WORKSHOPTYPE_PLACEHOLDER         @"dropdown_workshoptype"
#define C_PROFILE_COMPANYNAME         @"form_registeredworkshopname"
#define C_PROFILE_CONTACTNUMBER         @"form_landlinenumber"
#define C_PROFILE_APPROVEDCONTACTNUMBER         @"form_landlinenumber"
#define C_PROFILE_CONTACTNUMBER_PLACEHOLDER         @"form_landlinenumber_placeholder"

#define C_PROFILE_ADDRESSLINE1         @"form_addressline1"
#define C_PROFILE_ADDRESSLINE2         @"form_addressline2"
#define C_PROFILE_TOWN         @"form_town"
#define C_PROFILE_STATE         @"form_state"
#define C_PROFILE_STATE_PLACEHOLDER         @"form_state_placeholder"
#define C_PROFILE_POSTALCODE         @"form_postalcode"
#define C_PROFILE_OILCHANGEPERDAY         @"form_oilchangeperday"
#define C_PROFILE_OILCHANGEPERCENT         @"form_fullsynthetic_oilchangeperday"
#define C_PROFILE_SEMIOILCHANGEPERCENT         @"form_semisynthetic_oilchangeperday"
#define C_PROFILE_SHELLHELIX         @"form_shellhelix_oilchangeperday"
#define C_PROFILE_ANSWER         @"form_answer_placeholder"
#define C_PROFILE_WORKINGHOURS         @"form_working_hours"

#define C_PROFILE_AGREE         @"registration_tnc"
#define C_PROFILE_TNC         @"login_tnc_link"
#define C_PROFILE_PRIVACY         @"registration_privacy_link"
#define C_PROFILE_CONTACT_PREFERENCE         @"form_contactpreference"

#define C_MECHANICDETAILS_REMARKSPLACEHOLDER         @"btn_remarks_placeholder"
#define C_MECHANICDETAILS_APPROVE         @"btn_approve"
#define C_MECHANICDETAILS_REJECT         @"btn_reject"
#define C_MECHANICDETAILS_DEACTIVATE         @"btn_deactivate"

//DROPDOWN
#define C_DROPDOWN_SALUTATION         @"dropdown_salutation"
#define C_DROPDOWN_BIRTHDAY         @"dropdown_birthday"
#define C_DROPDOWN_COUNTRY         @"dropdown_countryname"
#define C_DROPDOWN_DIALINGCODE         @"dropdown_dialingcode"
#define C_DROPDOWN_STATE         @"dropdown_state"
#define C_DROPDOWN_LANGUAGE         @"dropdown_language"
#define C_DROPDOWN_WORKSHOPTYPE         @"dropdown_workshoptype"
#define C_DROPDOWN_ACTIVATIONSTATUS         @"dropdown_selectactivationstatus"
#define C_DROPDOWN_ENQUIRY         @"dropdown_enquiry"

//HOME
#define C_HOME_QUICKLINKS         @"home_quicklinks_header"

//SETTINGS
#define C_SETTINGS_PUSH_NOTIFICATION         @"settings_pushnotif"
#define C_SETTINGS_LANGUAGE         @"settings_selectlang"

//etc
#define C_COMPLETE_YOUR_PROFILE         @"profile_header_content"
#define C_KEYWORD_SEARCH         @"form_keywordsearch_placeholder"

#define C_SEARCHFIELD_VEHICLEID         @"searchvehicle_vehicleid_placeholder"
#define C_SEARCHFIELD_MILEAGE         @"registeroilchange_mileage_placeholder"
#define C_SCANCODE_SELECTFOLLOWING         @"registeroilchange_selectoption"

#define C_FULLDAY         @"form_fullday"
#define C_HALFDAY         @"form_halfday"
#define C_STARTTIME         @"form_starttime"
#define C_ENDTIME         @"form_endtime"
#define C_DELETE         @"btn_delete"
#define C_STATUS         @"popup_status"

#define C_POINTS_SEARCHBYTYPE         @"points_searchbytype"
#define C_POINTS_STARTDATE         @"popup_fromdate"
#define C_POINTS_ENDDATE         @"popup_enddate"
#define C_POINTS_TO         @"misc_to"
#define C_POINTS_GO         @"btn_enter"
#define C_POINTS_DATE         @"points_date"
#define C_POINTS_DESCRIPTION         @"points_description"
#define C_POINTS_POINTS         @"points_points"
#define C_ACCEPTTNC         @"btn_agreetnc"

//week
#define C_MON         @"form_monday"
#define C_TUE         @"form_tuesday"
#define C_WED         @"form_wednesday"
#define C_THU         @"form_thursday"
#define C_FRI         @"form_friday"
#define C_SAT         @"form_saturday"
#define C_SUN         @"form_sunday"

//Placeholder
#define C_PROFILE_CURRENTPASSWORD         @"profile_currentpassword"
#define C_PROFILE_NEWPASSWORD         @"profile_newpassword"
#define C_PROFILE_RE_NEWPASSWORD         @"profile_reenterpassword"

#define C_PLACEHOLDER_CURRENTPASSWORD         @"profile_currentpassword_placeholder"
#define C_PLACEHOLDER_NEWPASSWORD         @"profile_newpassword_placeholder"
#define C_PLACEHOLDER_RE_NEWPASSWORD         @"profile_reenterpassword_placeholder"
#define C_CHANGEPASSWORD_MISMATCH_ERRORMSG         @"form_popup_password_not_match"

#define C_FORM_DONE         @"btn_done"
#define C_POPUPMSG_SUBMITAGAIN         @"btn_submitagain"

#define C_REWARDS_POINTSREQUIRED         @"rewards_point_required"

#define C_SEARCHVEHICLE_VEHICLEID         @"searchvehicle_vehicleid"
#define C_SEARCHVEHICLE_SHEWSTATUS         @"searchvehicle_shewstatus"
#define C_SEARCHVEHICLE_SHEWVALIDITY         @"searchvehicle_shewvalidity"
#define C_SEARCHVEHICLE_SERVICEDATE         @"searchvehicle_servicedate"
#define C_SEARCHVEHICLE_PRODUCTUSED         @"searchvehicle_productused"
#define C_SEARCHVEHICLE_MILEAGE         @"searchvehicle_mileage"
#define C_SEARCHVEHICLE_SERVICEBY         @"searchvehicle_servicedby"

#define C_SEARCHVEHICLE_REGISTERPRODUCTCODE         @"btn_registeroilchange"

#define C_SCANPRODUCTCODE_VEHICLENUM_ERROR         @"popup_error_vehicleid"
#define C_SCANPRODUCTCODE_MILEAGE_ERROR         @"popup_error_mileage"
#define C_SCANPRODUCTCODE_CODE_ERROR         @"popup_error_code"
#define C_SCANPRODUCTCODE_CODE_EXIST         @"popup_codeexist"

#define C_SCANPRODUCTCODE_SUBMIT         @"btn_submitoilchange"

#define C_DROPDOWN_COMPANYSOURCE         @"form_workshopsource_placeholder"
#define C_PROFILE_COMPANYSOURCE         @"form_workshop_source"
#define C_PROFILE_COMPANYSOURCE_PLACEHOLDER         @"form_workshopsource_placeholder"
#define C_PROFILE_STAFF_HEADER         @"form_title_staffprofile"

#define C_TABLE_NO         @"registeroilchange_table_number"
#define C_TABLE_SCANNEDCODE         @"registeroilchange_table_scannedcode"
#define C_FORM_ADDCODE         @"btn_addcode"
#define C_FORM_ENTERCODETITLE         @"form_entercode"
#define C_PROFILE_ADDNEWREMARKS         @"form_remarks"

#define C_RESULT_VEHICLEID         @"registeroilchange_result_vehicleid"
#define C_RESULT_CODEDESCRIPTION         @"registeroilchange_result_description"
#define C_RESULT_STATUS         @"registeroilchange_result_status"

#define C_PERFORMANCE_LEADERBOARD         @"performance_leaderboard"

#define C_TITLE_MECHPERFORMANCE         @"title_mech_performance"
#define C_SCANCODE_STATUS_PENDING         @"form_pending"

#define C_REWARDS_CURRENTPOINTS         @"rewards_youhave"
#define C_REWARDS_FILTERCATEGORY         @"dropdown_filterbycategory"

#define C_REWARDS_FAVOURITESBTN         @"rewards_favourites"
#define C_REWARDS_POINTS         @"rewards_pts"
#define C_REWARDS_PTS         @"rewards_pts"
#define C_REWARDS_ADDTOCART         @"btn_addtocart"
#define C_REWARDS_ADDTOFAVOURITES         @"btn_addtofavourites"
#define C_REWARDS_CHECKOUT         @"btn_proceedtoredeem"
#define C_REWARDS_DELIVERTOADDRESS         @"btn_delivertothisaddress"
#define C_REWARDS_BACK         @"btn_back"
#define C_REWARDS_QUANTITY         @"rewards_quantity"
#define C_REWARDS_TOTALITEMS         @"rewards_total_items"
#define C_REWARDS_CARTTITLE         @"title_redemptioncart"
#define C_REWARDS_ADDRESSTITLE         @"title_deliveryaddress"
#define C_REWARDS_CONFIRMATIONTITLE         @"title_confirmation"
#define C_REWARDS_FAVOURITESTITLE         @"rewards_favourites"
#define C_REWARDS_INSUFFICIENTPOPUP         @"rewards_insufficient_points_content"
#define C_REWARDS_SMALL_QTY                 @"rewards_qty_small"

//month
#define C_MONTH_JAN         @"dropdown_month_jan"
#define C_MONTH_FEB         @"dropdown_month_feb"
#define C_MONTH_MAR         @"dropdown_month_mar"
#define C_MONTH_APR         @"dropdown_month_apr"
#define C_MONTH_MAY         @"dropdown_month_may"
#define C_MONTH_JUN         @"dropdown_month_jun"
#define C_MONTH_JUL         @"dropdown_month_jul"
#define C_MONTH_AUG         @"dropdown_month_aug"
#define C_MONTH_SEP         @"dropdown_month_sep"
#define C_MONTH_OCT         @"dropdown_month_oct"
#define C_MONTH_NOV         @"dropdown_month_nov"
#define C_MONTH_DEC         @"dropdown_month_dec"

//Keyword Search Range
#define C_KEYWORD_ALL         @"manageworkshops_all_filter"
#define C_REWARDS_ADDEDTOCART         @"popup_rewards_additemtocart_msg"

#define C_TITLE_APPROVEDWORKSHOPPROFILING         @"form_title_workshop_profiling"

#define C_PROFILE_WORKSHOPCODE         @"form_workshop_code"
#define C_TABLE_NODATAFOUND         @"table_nodatafound"

#define C_PROMO_REDEEMOFFERTAB         @"customerredemption_submit_title"
#define C_PROMO_REDEMPTIONHISTORYTAB         @"customerredemption_history_title"
#define C_PROMO_ENTERPROMOCODE         @"customerredemption_enterpromocode_placeholder"
#define C_PROMO_ENTER         @"btn_enter"
#define C_PROMO_VEHICLEID         @"registeroilchange_redemption_vehicleid"
#define C_PROMO_SELECTTYPE         @"dropdown_selecttype"
#define C_PROMO_FROMDATE         @"popup_fromdate"
#define C_PROMO_TO         @"misc_to"
#define C_PROMO_ENDDATE         @"popup_enddate"
#define C_PROMO_COMPLETEREDEMPTION         @"btn_submitredemption"

#define C_PROMO_QTY         @"registeroilchange_redemption_qty"
#define C_PROMO_REDEMPTIONITEM         @"registeroilchange_redemption_item"
#define C_TITLE_PROMOCODE         @"drawer_customerredemption"
#define C_PROMO_VALIDITY         @"registeroilchange_redemption_validity"

#define C_KEYWORD_SECONDRANGE         @"manageworkshops_af_filter"
#define C_KEYWORD_THIRDRANGE         @"manageworkshops_gl_filter"
#define C_KEYWORD_FOURTHRANGE         @"manageworkshops_mz_filter"

#define C_MANAGEMECHANIC_ADDNEW         @"managestaff_addnew"

#define C_DROPDOWN_SEARCHPLACEHOLDER         @"dropdown_search"

#define C_PROMO_REDEEMEDDATE         @"customerredemption_date"
#define C_PROMO_PROMOTYPE         @"customerredemption_promotype"
#define C_PROMO_REWARDDETAILS         @"customerredemption_rewarddetails"
#define C_PROMO_REDEMPTIONDETAILS         @"popup_redemptiondetails"

#define C_PROMO_RESULTS_VEHICLEID         @"registeroilchange_redemption_vehicleid"
#define C_PROMO_RESULTS_OFFERDETAILS         @"popup_redemptiondetails"
#define C_PROMO_RESULTS_REMARKS         @"customerredemption_resultsverificationnum"
#define C_PROMO_RESULTS_NO         @"registeroilchange_table_number"
#define C_PROMO_RESULTS_CODE         @"registeroilchange_result_description"

#define C_PROMO_SELECT_OFFERTYPE         @"dropdown_searchbytype"
#define C_PROMO_RESULTS_COMPLETEDHEADER         @"customerredemption_redeemcompleted"
#define C_PROMO_RESULTS_CONFIRMATIONHEADER         @"customerredemption_confirmationdetails"

#define C_PROMO_DETAIL_VEHICLEID         @"registeroilchange_redemption_vehicleid"
#define C_PROMO_DETAIL_REDEMPTIONDETAILS         @"popup_redemptiondetails"
#define C_PROMO_DETAIL_PRODUCTNAME         @"popup_productname"
#define C_PROMO_DETAIL_PRODUCTCODES         @"customerredemption_productcodes"
#define C_PROMO_DETAIL_QTY         @"registeroilchange_redemption_qty"
#define C_PROMO_DETAIL_VERIFICATIONNO         @"customerredemption_verificationnum"
#define C_PROMO_DETAIL_APPROVEDBY         @"customerredemption_approvedby"
#define C_PROMO_DETAIL_HEADER         @"popup_redemptiondetails"

#define C_PROMO_SCAN_CODE         @"btn_scanproductcode"
#define C_PROMO_ENTER_CODE         @"btn_manualproductcode"
#define C_PROMO_POPUP_HEADER         @"popup_productcode"
#define C_PROMO_POPUP_PLACEHOLDER         @"popup_productcode"
#define C_MECH_REDEEMFLAG         @"managestaff_authorizemechanic"
#define C_MECH_UPDATEDETAILS         @"btn_updatedetails"

#define C_PROMO_SCANNEDCODE         @"registeroilchange_table_scannedcode"

#define C_SEARCHVEHICLE_OFFERVALIDITY         @"searchvehicle_validity"
#define C_SEARCHVEHICLE_OFFERREDEEM         @"btn_redeem"
#define C_SEARCHVEHICLE_ENTITLEDOFFER         @"searchvehicle_entitledoffer"
#define C_SEARCHVEHICLE_OFFERDETAILS         @"searchvehicle_offerdetails"

#define C_SIDEMENU_SURVEY         @"drawer_survey"
#define C_TITLE_SURVEY         @"drawer_survey"


#define C_TOLOYALTY_SUMMARY_TITLE         @"toloyalty_summary_title"
#define C_TOLOYALTY_SUMMARY_HEADER         @"toloyalty_summary_header"
#define C_TOLOYALTY_SUMMARY_MONTH         @"toloyalty_summary_month"
#define C_TOLOYALTY_SUMMARY_VOLUME         @"partnersofshell_vol"
#define C_TOLOYALTY_SUMMARY_POINTS         @"rewards_points"
#define C_TOLOYALTY_SUMMARY_FROMMONTH         @"toloyalty_summary_frommonth"
#define C_TOLOYALTY_SUMMARY_TOMONTH         @"toloyalty_summary_tomonth"
#define C_TOLOYALTY_SUMMARY_VIEWCATALOGUE         @"toloyalty_summary_viewcatalogue"

#define C_TOLOYALTY_DETAILS_TITLE         @"toloyalty_summary_title"
#define C_TOLOYALTY_DETAILS_PRODUCTNAME         @"toloyalty_details_productname"
#define C_TOLOYALTY_DETAILS_VOLUME         @" partnersofshell_vol"
#define C_TOLOYALTY_DETAILS_POINTS         @"rewards_points"

#define C_DROPDOWN_MONTHYEAR         @"performance_selectdate"     //Used in TO Loyalty which is hidden, use "Select Date" temporarily

#define C_PROMO_SUBMITOILCHANGE         @"registeroilchange_redemption_submitoilchange"

#define C_SETTINGS_REFRESHDATA         @"settings_refresh"

#define C_TITLE_INVITECUSTOMER         @"title_invitecustomer"

#define C_TITLE_WORKSHOPOFFER         @"title_workshopoffer"
#define C_SIDEMENU_WORKSHOPOFFER         @"drawer_workshopoffers"
#define C_WORKSHOPOFFER_ADDNEW         @"title_manageworkshops_addnew"

#define C_CONTACTUS_UPLOADIMAGE         @"form_uploadimages"
#define C_CONTACTUS_ADDPHOTO         @"form_addphoto"

#define C_LIBRARY_TITLE         @"title_mylibrary"
#define C_LIBRARY_BROWSECATEGORY         @"mylibrary_browsebyalbum"
#define C_LIBRARY_FILTERDATERANGE         @"popup_filterbydaterange"

#define C_DECAL_TITLE         @"decal_msg_scandecal"
#define C_DECAL_SCAN         @"drawer_scandecal"
#define C_DECAL_TAGNEW         @"btn_tagnew"
#define C_DECAL_TAGDECALTOVEHICLE         @"btn_tagdecalvehicle"
#define C_DECAL_TAGTOVEHICLE         @"btn_tagtovehicle"
#define C_DECAL_REGISTERDECAL         @"searchvehicle_decal_header"
#define C_DECAL_ACTIVATEDECAL         @"title_activatedecal"
#define C_DECAL_TAGTOWORKSHOP         @"registeroilchange_decal_content"
#define C_DECAL_SCANDECAL         @"btn_scandecal"
#define C_DECAL_ENTERDECAL         @"btn_enterdecal"
#define C_DECAL_SUBMITDECAL         @"btn_submitdecal"


#define C_ORDER_DATERANGE         @"popup_daterange"
#define C_ORDER_ORDERDETAILS         @"title_orderdetails"
#define C_ORDER_ORDERID         @"manageorders_orderdetails_orderid"
#define C_ORDER_ADDRESS         @"manageorders_orderdetails_address"
#define C_ORDER_PLACEDON         @"manageorders_orderdetails_placedon"
#define C_ORDER_TOTAL         @"manageorders_orderdetails_ordertotal"
#define C_ORDER_APPROVEDON         @"ORDER_APPROVEDON"
#define C_ORDER_REORDER         @"btn_reorder"
#define C_ORDER_ITEMSADDED         @"popup_itemadded_header"
#define C_ORDER_ADDEDTOCART         @"popup_itemadded_content"
#define C_ORDER_CANCELORDER         @"btn_cancelorder"
#define C_ORDER_CANCELORDERCONFIRMATION         @"popup_cancelorder_header"
#define C_ORDER_CANCELLATIONWARNING         @"popup_cancelorder_content"
#define C_ORDER_VIEWCART         @"btn_viewcart"
#define C_ORDER_ADD         @"btn_add"
#define C_ORDER_SUBTOTAL         @"manageorders_orderdetails_subtotal"
#define C_ORDER_REMARKS         @"manageorders_orderdetails_remarks"
#define C_ORDER_APPROVEDQUANTITY         @"manageorders_orderdetails_approvedquantity﻿"
#define C_ORDER_UNITPRICE         @"manageorders_orderdetails_unitprice"
#define C_ORDER_PLACEORDER         @"btn_placeorder"
#define C_ORDER_VIEWORDERS         @"btn_vieworders"
#define C_ORDER_ENTERREMARKS         @"manageorders_orderdetails_remarks_placeholder"
#define C_ORDER_MYCART         @"title_mycart"
#define C_ORDER_CARTEMPTY         @"manageorders_cartempty"
#define C_ORDER_STARTADDINGITEM         @"btn_startadding"
#define C_ORDER_MYORDER         @"title_manageorders_myorders"

#define C_DECAL_SHAREDECAL         @"searchvehicle_sharedecal"

#define C_ORDER_STARTDATE         @"btn_startdate"
#define C_ORDER_ENDDATE         @"btn_enddate"
#define C_ORDER_ENTER         @"btn_enter"

#define C_NOTIFICATION_EDIT         @"btn_edit"
#define C_NOTIFICATION_SELECTALL         @"btn_selectall"

#define C_ORDER_CONFIRMATION_TITLE         @"title_orderconfirmation"
#define C_ORDER_QUANTITY         @"manageorders_orderdetails_quantity"
#define C_ORDER_ORDEREDBY         @"manageorders_orderdeatils_orderedby"
#define C_ORDER_APPROVEDORDER         @"btn_approveorder"
#define C_ORDER_REJECTORDER         @"btn_rejectorder"
#define C_ORDER_REJECTCONF_TITLE         @"title_rejectconfirmation"
#define C_ORDER_APPROVALCONF_TITLE         @"title_approvalconfirmation"

#define C_CONSUMER_ONEMORESTEP         @"invitecustomer_onemorestep_header"
#define C_CONSUMER_ONEMORESTEPCONTENT         @"invitecustomer_header"
#define C_CONSUMER_OPTIONONE         @"invitecustomer_option1_header"
#define C_CONSUMER_INVITEVIASMS         @"btn_inviteviasms"
#define C_CONSUMER_OR         @"invitecustomer_or"
#define C_CONSUMER_OPTIONTWO         @"invitecustomer_option2_header"
#define C_CONSUMER_REGISTERCUSTOMER         @"btn_registercustomer"
#define C_CONSUMER_SUCCESSFULREGISTRATION         @"invitecustomer_successful_full_content"

#define C_CONSUMER_CONTINUE         @"btn_continue"
#define C_CONSUMER_SUCCESSFUL         @"invitecustomer_successful_header"
#define C_CONSUMER_CUSTOMERMOBILE_PLACEHOLDER         @"invitecustomer_mobile_placeholder"
#define C_CONSUMER_CUSTOMERVEHICLE_PLACEHOLDER         @"invitecustomer_vehicleid_placeholder"
#define C_CONSUMER_VERIFYMOBNUM         @"invitecustomer_verifynumber_header"
#define C_CONSUMER_OTPSENT         @"form_msg_otpsent"
#define C_CONSUMER_REQUESTOTP         @"btn_requestotp"
#define C_CONSUMER_OTP         @"form_otp"
#define C_CONSUMER_ENTERDETAILS         @"invitecustomer_customerdetails_header"
#define C_CONSUMER_VEHICLEREGPLATE         @"form_vehicleregistrationplate"
#define C_CONSUMER_YEAROFMANUFACTURE         @"form_yearofmanufacture"
#define C_CONSUMER_VEHICLEUSAGE         @"form_vehicleusage"
#define C_CONSUMER_MALAYSIAVEHICLE         @"invitecustomer_vehicleregistration_my﻿"
#define C_CONSUMER_SUBMITREGISTRATION         @"btn_submitregistration"
#define C_CONSUMER_TWOOPTIONS         @"invitecustomer_consumer_redemption_header"
#define C_CONSUMER_THAILANDVEHICLE         @"invitecustomer_vehicleregistration_th"
#define C_CONSUMER_INDONESIAVEHICLE         @"invitecustomer_vehicleregistration_id"
#define C_CONSUMER_PHILLIPPINESVEHICLE         @"invitecustomer_vehicleregistration_ph"

#define C_CONSUMER_AUTHMSG         @"form_msg_avoidmultipleotp"

#define C_REWARDS_BROWSEBYCATEGORY         @"form_browsebycategory_header"
#define C_REWARDS_HISTORY         @"rewards_history"
#define C_REWARDS_VIEWALLREWARDS         @"btn_viewallrewards"
#define C_REWARDS_FILTERBY         @"dropdown_filterby"
#define C_REWARDS_SORTBY         @"dropdown_sortby"
#define C_REWARDS_ONLYSHOWREDEEM         @"popup_rewards_chk_showcanredeemrewards"

#define C_REWARDS_ITEMSINCART         @"rewards_items_in_cart"
#define C_REWARDS_EMPTYCART         @"rewards_empty_cart"
#define C_REWARDS_DELIVERYADDRESS         @"rewards_delivery_address"
#define C_REWARDS_RECIPIENT         @"rewards_recipient"
#define C_REWARDS_DELIVERYTOTHISADDRESS         @"btn_delivertothisaddress"
#define C_REWARDS_DELIVERYDETAILS         @"rewards_delivery_details"
#define C_REWARDS_REVIEWREDEMPTION         @"rewards_review_redemption"

#define C_REWARDS_CONFIRMREDEEM         @"btn_confirmandredeem"
#define C_REWARDS_THANKYOU         @"rewards_thankyou"
#define C_REWARDS_REDEMPTIONID         @"rewards_your_ref_id_is"
#define C_REWARDS_VIEWHISTORY         @"btn_viewhistory"
#define C_REWARDS_PENDING         @"form_pending"
#define C_REWARDS_PROCESSING         @"form_processing"
#define C_REWARDS_DELIVERED         @"form_delivered"
#define C_REWARDS_DELIVERTODIST         @"form_deliveringtodist"
#define C_REWARDS_ITEMRECEIVED         @"form_itemreceived"

#define C_REWARDS_PLACEDON         @"misc_placed_on"
#define C_REWARDS_POINTSUSED         @"rewards_points_used"
#define C_REWARDS_REDEMPTIONDETAILS_TITLE         @"title_redemption_details"
#define C_REWARDS_REDEMPTIONSUMMARY         @"rewards_redemption_summary"
#define C_REWARDS_CANCELLED         @"form_cancelled"
#define C_REWARDS_REASON         @"rewards_reason"
#define C_REWARDS_CONTACTINFO         @"rewards_contact_info"
#define C_REWARDS_CONTACTCONTENT         @"rewards_contact_info_content"
#define C_REWARDS_CONTACTUS         @"btn_contactus"
#define C_REWARDS_FEATUREDREWARDS         @"rewards_feature_rewards"

#define C_CONSUMER_MARKETING         @"invitecustomer_marketing"

#define C_NOTIFICATION_READMORE         @"home_notifreadmore"
#define C_NOTIFICATION_CLOSE         @"home_notifclose"

#define C_CONSUMER_BIRTHDAYMONTH         @"form_birthdaymonth"
#define C_CONSUMER_FULLNAME         @"form_fullname"
#define C_CONSUMER_YEAROFMANUFACTURE_OPT         @"dropdown_yearofmanufacture"

#define C_POPUP_LEGEND         @"form_legends"
#define C_REWARDS_ITEMSREDEEMED         @"rewards_item_redeemed"
#define C_REWARDS_REFID         @"form_refid"
#define C_REWARDS_QTY         @"rewards_quantity"
#define C_REWARDS_EMPTYCONFIRMATION         @"popup_rewards_remove_items_from_cart_content"

#define C_PROFILE_REQUIREDFIELDS         @"form_requiredfield_header"

#define C_SIDEMENU_MYWORKSHOPMODULES         @"drawer_workshopmodules_sub"
#define C_SIDEMENU_HELPANDSUPPORT         @"drawer_helpsupport_sub"
#define C_SIDEMENU_MYCUSTOMERS         @"drawer_mycustomer_sub"

#define C_TOTAL_EARNED @"total_earned"

#define C_HOMECAROUSEL_AVAILABLEPOINTS         @"home_availablepoints_header"
#define C_HOMECAROUSEL_EXPIRINGPOINTS         @"home_expiringpoints_header"
#define C_HOMECAROUSEL_REDEEMPOINTS         @"btn_redeempoints"

#define C_THAI_VEHICLE_STATE         @"btn_selectstate"
#define C_REGISTER_RESULT_NOTCONNECTED         @"registeroilchange_registercustomer_header"

#define C_RUSSIA_CAROUSEL_FINALSCORE         @"partnersofshell_finalscore_notes"
#define C_RUSSIA_CAROUSEL_EARNPOINTBTN         @"btn_hottoearnpoints"
#define C_RUSSIA_TITLE_EARNPOINTS         @"title_howtoearnpoints"
#define C_RUSSIA_EARNPOINTS_CONTENT         @"partnersofshell_howtoearnpoints_notes"

#define TITLE_SPIRAX_GADUS_ONE_PLUS_ONE @"spirax_gadus"

#define C_RUSSIA_POINTS_EARNED         @"partnersofshell_pointearned"
#define C_RUSSIA_VOLUME         @"partnersofshell_vol"
#define C_RUSSIA_THANKYOUPARTICIPATION         @"partnersofshell_thankyouforparticipation"
#define C_RUSSIA_MOREINTERESTINGCAMPAIGN         @"partnersofshell_thankyouforparticipation_content"

#define C_RUSSIA_OILCHANGEREGISTRATION         @"ql_oilchangeregistration"
#define C_RUSSIA_NONOTIFICATIONS         @"notifications_nonotif"
#define C_RUSSIA_REGISTEROILCHANGE         @"btn_registeroilchange"
#define C_RUSSIA_SEARCHVEHICLEHEADER         @"searchvehicle_ru_header"

#define C_RUSSIA_DATEFORMAT         @"searchvehicle_ru_dateformat"
#define C_RUSSIA_ADDPRODUCTUSED         @"btn_addproductused"
#define C_RUSSIA_CURRENTMILEAGE_PLACEHOLDER         @"registeroilchange_ru_currentmileage_placeholder"

#define C_RUSSIA_CHOOSEPRODUCT_TITLE         @"title_registeroilchange_chooseproduct"
#define C_RUSSIA_CHOOSEPRODUCT_PLACEHOLDER         @"registeroilchange_ru_productbrand_placeholder"
#define C_RUSSIA_CHOOSEPRODUCT_SEARCH         @"btn_search_chooseproduct"
#define C_RUSSIA_CHOOSEPRODUCT_MOREBRAND         @"registeroilchange_ru_morebrands﻿"

#define C_RUSSIA_OILCHANGERESULT_NOTCONNECTED         @"registeroilchange_result_notconnected"
#define C_RUSSIA_OILCHANGERESULT_REGISTERCUSTOMER         @"registeroilchange_result_registercustomer"
#define C_RUSSIA_OILCHANGERESULT_INVITEBTN         @"btn_registercustomer_result"

#define C_RUSSIA_VEHICLEBRAND         @"form_vehiclebrand"
#define C_RUSSIA_CUSTOMERSIGNATURE         @"form_customer_signature"

#define C_RUSSIA_CLEARSIGNATURE         @"btnClearSignature"
#define C_RUSSIA_CONFIRM         @"btn_confirm"
#define C_RUSSIA_DRAWSIGNATURE         @"form_msg_signature"

#define C_RUSSIA_EARNPOINTS_PACKSIZE         @"partnersofshell_products_notes"
#define C_RUSSIA_VEHICLE_EXAMPLE         @"searchvehicle_ru_vehicleexample"

#define C_RUSSIA_TOTAL_POINT         @"partnersofshell_totalpoints"
#define C_RUSSIA_VALUE_CALCULATOR         @"btn_pointcalculator"

#define C_FILTER_BY_TYPE         @"filter_by_type"
#define C_FILTER_BY_STATUS         @"manageworkshops_filterbystatus_placeholder"
#define C_FILTER_BY_TRANSACTION     @"cashincentive_filterbytransaction_placeholder"
#define C_RUSSIA_BACKTOSEARCHVEH         @"btn_backtosearchvehicle"
#define C_RUSSIA_REGISTERSUCCESS         @"title_registersuccess"

#define C_RUSSIA_OFFERS_CALENDAR         @"workshopoffer_calendar"
#define C_RUSSIA_OFFERS_LIST         @"workshopoffer_list"
#define C_RUSSIA_OFFERS_SELECTOFFERTYPE         @"dropdown_offertype"
#define C_RUSSIA_OFFERS_SELECTOFFERSTATUS         @"dropdown_offerstatus"

#define C_RUSSIA_OFFERS_OFFERDETAILS         @"title_offerdetails"
#define C_RUSSIA_OFFERS_PROMONAME         @"workshopoffer_offername"
#define C_RUSSIA_OFFERS_PROMOPERIOD         @"workshopoffer_offerperiod"
#define C_RUSSIA_OFFERS_PROMOELIGIBLE         @"workshopoffer_numcusteligible"
#define C_RUSSIA_OFFERS_PROMOTYPE         @"workshopoffer_offertype"

#define C_RUSSIA_EDITDETAILS         @"btn_editdetails"
#define C_RUSSIA_CANCELOFFER         @"btn_canceloffer"
#define C_RUSSIA_CREATEOFFERAGAIN         @"btn_createofferagain"

#define C_RUSSIA_REDEMPTIONOVERVIEW         @"workshopoffer_redemptionoverview_header"
#define C_RUSSIA_REDEMPTION_VERIFICATIONNUM         @"workshopoffer_verificationnum"
#define C_RUSSIA_REDEMPTION_DETAILS         @"workshopoffer_redemptiondetails"

#define C_RUSSIA_DATEOFREDEMPTION         @"workshopoffer_redemptiondate"
#define C_RUSSIA_OFFERREDEEMED         @"workshopoffer_offerredeemed"
#define C_RUSSIA_PRODUCTUSED         @"workshopoffer_productused"
#define C_RUSSIA_REDEEMED         @"workshopoffer_redeemed"
#define C_RUSSIA_SENT         @"workshopoffer_sent"

#define C_RUSSIA_CANCELOFFERHEADER         @"popup_canceloffer_header"
#define C_RUSSIA_CANCELOFFERBODY         @"popup_canceloffer"
#define C_RUSSIA_YESCANCEL         @"btn_yescancel"

#define C_RUSSIA_ADDNEWOFFER         @"title_addnewoffer"


#define C_RUSSIA_NEXT_BTN         @"btn_next"
#define C_RUSSIA_EDIT_BTN         @"btn_edit"

#define C_RU_STEPONE_CHOOSEPROMOTYPE         @"workshopoffer_setupoffer_header"
#define C_RU_STEPONE_RUNWORKSHOP         @"workshopoffer_setupoffer"
#define C_RU_STEPONE_PROMOPERIOD         @"workshopoffer_startenddate"

#define C_RU_STEPONE_TO         @"misc_to"
#define C_RU_STEPONE_ENTER         @"btn_enter"
#define C_RU_STEPONE_SELECTNEEDOILCHANGE         @"workshopoffer_oilchange_header"
#define C_RU_STEPONE_WITHOILCHANGE         @"workshopoffer_withoilchange"
#define C_RU_STEPONE_WITHOUTOILCHANGE         @"workshopoffer_withoutoilchange"

#define C_RU_STEPTWO_CHOOSEAUDIENCE         @"workshopoffer_chooseaudience_header"
#define C_RU_STEPTWO_SELECTCUSTOMERFGROUP         @"workshopoffer_customergroup_header"
#define C_RU_STEPTWO_ALLCUSTOMERS         @"workshopoffer_allcustomers"
#define C_RU_STEPTWO_SELECTEDCUSTOMERS         @"workshopoffer_selectedcustomers"
#define C_RU_STEPTWO_CARBRANDS         @"workshopoffer_carbrands"
#define C_RU_STEPTWO_OILBRANDS         @"workshopoffer_oilbrands"

#define C_RU_SELECTCAR_HEADER         @"workshopoffer_bracketcustomergroup"
#define C_RU_SELECTCAR_ALLCARBRANDS         @"workshopoffer_allcarbrands"

#define C_RU_SELECTOIL_TITLE         @"workshopoffer_selectoilbrand"
#define C_RU_SELECTOIL_ALLBRANDS         @"workshop_allbrands"

#define C_RU_STEPTHREE_CHOOSEOFFERTYPE         @"workshopoffer_chooseoffertype"
#define C_RU_STEPTHREE_SERVICE         @"workshopoffer_service"
#define C_RU_STEPTHREE_PRODUCT         @"workshopoffer_product"
#define C_RU_STEPTHREE_SELECTED         @"workshopoffer_selected"
#define C_RU_STEPTHREE_SELECTOFFEROPTIONS         @"workshopoffer_selectofferoptions"
#define C_RU_STEPTHREE_PERCENTDISCOUNT         @"workshopoffer_percentdiscount"
#define C_RU_STEPTHREE_DOLLARDISCOUNT         @"workshopoffer_discount"
#define C_RU_STEPTHREE_FREE         @"workshopoffer_free"
#define C_RU_STEPTHREE_ENTERPERCENT         @"workshopoffer_percentagediscount_placeholder"
#define C_RU_STEPTHREE_ENTERAMOUNT         @"workshopffer_discountamount_placeholder"

#define C_RU_STEPFOUR_TNC         @"forms_ru_tandc"
#define C_RU_STEPFOUR_TNCLINK         @"form_termsandconditions_link"
#define C_RU_STEPFOUR_CONFIRMPUBLISH         @"btn_confirmpublish"
#define C_RU_STEPFOUR_MESSAGEPREVIEW         @"workshopoffer_previewheader"

#define C_RU_OFFERSUCCESS_TITLE         @"workshopoffer_publishsuccess_header"
#define C_RU_OFFERSUCCESS_VIEWALLOFFERS         @"btn_viewalloffers"

#define C_RU_WHATISTHIS         @"popup_whatisthis"
#define C_RU_ADDOFFER_HELP         @"popup_workshopoffer"
#define C_RU_STEPTWO_HELP         @"popup_choosecustomers_header"
#define C_RU_STEPTWO_HELP_ALLCUST_BODY         @"popup_allcustomers_content"
#define C_RU_STEPTWO_HELP_SELCUST_BODY         @"popup_selectcustomers_content"

#define C_RU_STEPTHREE_HELP         @"workshopoffer_listeligibleoffer"
#define C_RU_STEPTHREE_SELECTSERVICE         @"workshopoffer_selectservicesoffer"
#define C_RU_STEPTHREE_SELECTPRODUCT         @"workshopoffer_selectproductoffer"

#define C_RU_STEPFOUR_HELP         @"popup_smspreview_content"

#define C_RU_LOCKEDREWARDS_REDEMPTIONSTART         @"rewards_redemptionstart"
#define C_RU_LOCKEDREWARDS_DAYS         @"rewards_days"
#define C_RU_LOCKEDREWARDS_HOURS         @"rewards_hours"
#define C_RU_LOCKEDREWARDS_MINUTES         @"rewards_minutes"

#define C_RU_VALUECALC_HEADER         @"partnersofshell_pointcalculator_notes"
#define C_RU_VALUECALC_RESET         @"btn_reset"
#define C_RU_VALUECALC_SAVE         @"btn_save"
#define C_RU_VALUECALC_POPUP_RESET_HEADER         @"popup_resetpointcalculator_header"
#define C_RU_VALUECALC_POPUP_RESET_BODY         @"popup_resetpointcalculator_content"
#define C_RU_VALUECALC_POPUP_SAVED_BODY         @"partnersofshell_calc_saved_popup"
#define C_RU_VALUECALC_MAXIMUM_UNITS         @"partnersofshell_maxreached_popup"

#define C_RU_STEPTHREE_SELECTOFFER         @"workshopoffer_selectoffer"

#define C_RU_REDEMPTION_HEADER         @"registeroilchange_redemption_header"
#define C_RU_REDEMPTION_PLACEHOLDER         @"registeroilchange_redemption_promocode_placeholder"
#define C_RU_REDEMPTION_CHECKBOXTEXT         @"registeroilchange_redemption_submitoilchange"
#define C_RU_REDEMPTION_SUCCESSTITLE         @"title_redemptionsuccess"
#define C_RU_REDEMPTION_SUCCESSHEADER         @"registeroilchange_redemption_success_header"
#define C_RU_REDEMPTION_VERIFICATIONID         @"registeroilchange_redemption_success_verificationid"

#define C_RU_REWARDS_FEATUREDBRANDSTWO         @"rewards_feature_rewards_two"
#define C_RU_MILEAGE_SUFFIX         @"registeroilchange_ru_mileage_suffix"

#define C_RU_STEPFOUR_PRIVACYLINK         @"registration_privacy_link"

#define C_TITLE_PRODUCTSTITLE         @"title_productstitle"
#define C_TITLE_PRODUCTSTITLE_REFPTS         @"title_productstitle_refpts"

#define C_HOMECAROUSEL_NAME         @"home_faname"

#define C_HOMECAROUSEL_SHAREBADGEID         @"home_sharebadgeid"
#define C_HOMECAROUSEL_BADGEFOOTER         @"home_badgefooter"
#define C_HOMECAROUSEL_BADGEFOOTER_GENERAL  @"home_badgefooter_general"
#define C_HOMECAROUSEL_BADGEFOOTER_GENERAL_LINK     @"home_badgefooter_general_link"

#define C_HEADER_STAFFTYPE         @"form_staffaccounttype"
#define C_PROFILE_STAFFTYPE_PLACEHOLDER         @"dropdown_selectstaffaccounttype"
#define C_PROFILE_FASHAREBADGECODE         @"managestaff_fasharebadge"

#define C_HOMECAROUSEL_UPDATEDON         @"home_updateon_carousel"

#define C_WALKTHROUGH_WORKSHOPOFFER_HOWITWORKS         @"walkthrough_howitworks_header"
#define C_WALKTHROUGH_WORKSHOPOFFER_CONTENT_INTRO         @"walkthrough_workshopoffer"
#define C_WALKTHROUGH_WORKSHOPOFFER_STEP_ONE         @"walkthrough_stepone_header"
#define C_WALKTHROUGH_WORKSHOPOFFER_CONTENT_ONE         @"walkthrough_workshopoffer_one"
#define C_WALKTHROUGH_WORKSHOPOFFER_STEP_TWO         @"walkthrough_steptwo_header"
#define C_WALKTHROUGH_WORKSHOPOFFER_CONTENT_TWO         @"walkthrough_workshopoffer_two"
#define C_WALKTHROUGH_WORKSHOPOFFER_STEP_THREE         @"walkthrough_stepthree_header"
#define C_WALKTHROUGH_WORKSHOPOFFER_CONTENT_THREE         @"walkthrough_workshopoffer_three"
#define C_WALKTHROUGH_WORKSHOPOFFER_STEP_LAST         @"walkthrough_allset_header"
#define C_WALKTHROUGH_WORKSHOPOFFER_CONTENT_LAST         @"walkthrough_workshopoffer_final"
#define C_WALKTHROUGH_WORKSHOPOFFER_LASTBUTTON         @"btn_addnew"

#define C_TITLE_RETAILPERFORMANCE         @"title_retailperformance"                            //To Be Removed

#define C_VEHICLESEARCH_MEMBERSINCE         @"searchvehicle_customer_joinedsince"

#define C_RU_VEHICLESEARCH_MEMBERSINCE         @"RU_VEHICLESEARCH_MEMBERSINCE"

#define C_RU_PERFORMANCE_ORDERVOLUME         @"performance_ordervolume"


#define C_RU_WORKSHOPPERFORMANCE         @"performance_workshopperformance"

#define C_RU_LEADERBOARD_ORDERVOLUME         @"leaderboard_ordervolume"
#define C_RU_LEADERBOARD_RANK         @"leaderboard_rank"
#define C_RU_LEADERBOARD_DAILY         @"leaderboard_daily"
#define C_RU_LEADERBOARD_DSRFOOTER         @"leaderboard_dsr_footer"

#define C_RU_BREAKDOWN_OVERVIEW         @"breakdown_overview"
#define C_RU_BREAKDOWN_DETAILS         @"breakdown_details"
#define C_RU_BREAKDOWN_BYWORKSHOP         @"breakdown_byworkshop"
#define C_RU_BREAKDOWN_ORDER_FOOTER         @"breakdown_order_footer"
#define C_RU_BREAKDOWN_BYPRODUCTTIER         @"breakdown_byproducttier"
#define C_RU_BREAKDOWN_PRODUCTTIER         @"breakdown_producttier"
#define C_RU_BREAKDOWN_TOTALLITRE         @"breakdown_totallitre"
#define C_RU_BREAKDOWN_TOTALPERCENT         @"breakdown_totalpercent"
#define C_RU_BREAKDOWN_BYPRODUCTCATEGORY         @"breakdown_byproductcategory"
#define C_RU_BREAKDOWN_PRODUCTCATEGORY         @"breakdown_productcategory"

#define C_RU_BREAKDOWN_OILCHANGEBYID         @"breakdown_oilchangebyid"
#define C_RU_BREAKDOWN_OILCHANGE_FOOTER         @"breakdown_oilchange_footer"
#define C_RU_BREAKDOWN_OILCHANGE_BRANDFOOTER         @"breakdown_oilchange_brandfooter"
#define C_RU_BREAKDOWN_BYPRODUCTBRAND         @"breakdown_byproductbrand"
#define C_RU_BREAKDOWN_PRODUCTBRAND         @"breakdown_productbrand"

#define C_RU_BREAKDOWN_NEWCUSTOMERSIGNUPS         @"breakdown_newcustomersignups"
#define C_RU_BREAKDOWN_CUSTOMERTREND         @"breakdown_customertrend"
#define C_RU_BREAKDOWN_TOTAL         @"breakdown_total"

#define C_RU_BREAKDOWN_CHOOSEWORKSHOPS         @"breakdown_chooseworkshops"
#define C_RU_BREAKDOWN_ALLWORKSHOPS         @"performance_allworkshops"

#define C_RU_PERFORMANCE_DASHBOARD         @"performance_dashboard"
#define C_RU_PERFORMANCE_STAFFLEADERBOARD         @"performance_staffleaderboard"
#define C_RU_PERFORMANCE_DASHBOARD_HEADER         @"performance_dashboard_header"
#define C_RU_PERFORMANCE_DASHBOARD_PARTNER         @"performance_dashboard_partner"
#define C_RU_PERFORMANCE_DASHBOARD_SUBHEADER         @"performance_dashboard_subheader"
#define C_RU_PERFORMANCE_DASHBOARD_FOOTER         @"performance_dashboard_footer"

#define C_RU_BREAKDOWN_ORDER_TITLE         @"title_order_breakdown"

#define C_RU_BREAKDOWN_POINT_TITLE          @"breakdown_point_title"
#define C_RU_BREAKDOWN_POINT_HEADER         @"breakdown_point_header"
#define C_RU_BREAKDOWN_POINT_FOOTER         @"breakdown_point_footer"

#define C_RU_BREAKDOWN_OFFER_TITLE         @"title_breakdown_offer"
#define C_RU_BREAKDOWN_OFFER_TOTALSMS         @"breakdown_offer_totalsms"
#define C_RU_BREAKDOWN_OFFER_AVERAGERATE         @"breakdown_offer_averagerate"
#define C_RU_BREAKDOWN_OFFER_REDEEMED         @"breakdown_offer_redeemed"
#define C_RU_BREAKDOWN_OFFER_UNREDEEMED         @"breakdown_offer_unredeemed"
#define C_RU_BREAKDOWN_OFFER_TOTALPERCENT         @"breakdown_offer_totalpercent"
#define C_RU_BREAKDOWN_OFFER_TOTAL         @"breakdown_offer_total"
#define C_RU_BREAKDOWN_OFFER_BYOFFER         @"breakdown_offer_byoffer"
#define C_RU_BREAKDOWN_OFFER_OFFERNAME         @"breakdown_offer_offername"
#define C_RU_BREAKDOWN_OFFER_SENT         @"breakdown_offer_sent"
#define C_RU_BREAKDOWN_OFFER_NO         @"breakdown_offer_no"
#define C_RU_BREAKDOWN_OFFER_REDEEMEDNO         @"breakdown_offer_redeemedno"

#define C_RU_BREAKDOWN_STAFF_TITLE         @"title_breakdown_staff"
#define C_RU_BREAKDOWN_STAFF_HEADER         @"breakdown_staff_header"
#define C_RU_BREAKDOWN_STAFF_UPDATED         @"RU_BREAKDOWN_STAFF_UPDATED"
#define C_RU_BREAKDOWN_STAFF_OILCHANGE         @"breakdown_oilchange_staff"
#define C_RU_BREAKDOWN_STAFF_STAFFNAME         @"breakdown_staffname_staff"
#define C_RU_BREAKDOWN_STAFF_NEWCUSTOMER         @"breakdown_newcustomersignups"

#define C_RU_STAFFLEADERBOARD_MECHANIC         @"leaderboard_mechanic_staff"

#define C_RU_PERFORMANCE_VIEWSUMMARY         @"performance_viewsummary"
#define C_RU_PERFORMANCE_WORKSHOP         @"performance_workshop"
#define C_RU_BREAKDOWN_STAFF_FOOTER         @"breakdown_staff_footer"

#define C_RU_PERFORMANCE_SUMMARY         @"performance_summary"
#define C_RU_BREAKDOWN_OILCHANGE_TITLE         @"title_breakdown_oilchange"
#define C_RU_BREAKDOWN_CUSTOMER_TITLE         @"title_breakdown_customer"

#define C_TITLE_ALLORDERS         @"title_manageorders_allorders"
#define C_RU_CAROUSEL_VIEWPERFORMANCE         @"btn_viewperformance"

#define C_CONTACTUS_PHOTOSELECTION_TITLE         @"popup_select"
#define C_CONTACTUS_PHOTOSELECTION_LIBRARY         @"btn_library"
#define C_CONTACTUS_PHOTOSELECTION_CAMERA         @"btn_camera"
#define C_CONTACTUS_PHOTOSELECTION_CLOSE         @"btn_close"

#define C_PROFILE_OWNEREMAILADDRESS         @"form_owneremailaddress"
#define C_HOME_ACCOUNTDEACTIVATED         @"home_acctdeactivated"

#define C_TITLE_ANALYTICS         @"title_performance_analytics"
#define C_TITLE_IN_MECHPERFORMANCE         @"title_performance_mechanicperformance"
#define C_TITLE_MYNETWORK         @"title_mynetwork"


#define C_TITLE_RECENTACTIVITY         @"title_recentactivity"
#define C_PERFORMANCE_SUMMARY         @"performance_summary"
#define C_SUMMARY_VIEWBY         @"summary_viewby"
#define C_SUMMARY_HEADERTEXT         @"summary_headtext"
#define C_BUTTON_VIEWVALUESTATEMENT         @"button_viewvaluestatement"

#define C_PERFORMANCE_FILTERBYDATERANGE         @"performance_filterbydaterange"
#define C_PERFORMANCE_ALLSTAFF         @"performance_allstaff"
#define C_PERFORMANCE_SELECTDATE         @"performance_selectdate"
#define C_PROFILE_ADDRESS         @"form_title_address"
#define C_CTA_SELECTFOLLOWING         @"form_msg_update"
#define C_CTA_SAVEALLCHANGES         @"btnSaveAllChanges"
#define C_CTA_DELETEACCOUNT         @"btnDeleteAccount"
#define C_CTA_REASONDELETE         @"form_popup_msg_delete"
#define C_PROFILE_WORKSHOPCATEGORY_PLACEHOLDER         @"form_selectworkshopcategory_dropdown"
#define C_PROFILE_WORKSHOPCATEGORY         @"form_workshopcategory"
#define C_HEADER_MECHANICPROFILE         @"form_mechanicprofile"

#define C_DROPDOWN_SELECTWORKSHOPSTATUS         @"dropdown_workshopstatus"

#define C_LOYALTY_LOYALTYPROGRAM         @"title_loyaltyprogram"
#define C_LOYALTY_PERIOD         @"loyalty_period_carousel"
#define C_LOYALTY_ORDERNOW         @"btn_ordernow"
#define C_LOYALTY_LOYALTYPROGRAMBREAKDOWN         @"loyalty_loyaltyprogrambreakdown"
#define C_LOYALTY_VIEWBREAKDOWN         @"btn_viewbreakdown"
#define C_LOYALTY_MONTHLYOVERVIEW         @"loyalty_monthlyoverview_header"
#define C_LOYALTY_CARTONS         @"loyalty_cartons"
#define C_LOYALTY_TOTAL         @"loyalty_total"
#define C_LOYALTY_WORKSHOPBREAKDOWN         @"title_workshopperformance"
#define C_LOYALTY_PROGRAMPROGRESS         @"loyalty_programprogress_header"
#define C_LOYALTY_DETAILEDBREAKDOWN         @"loyalty_detailedbreakdown_header"
#define C_LOYALTY_BYPRODUCTGROUP         @"loyalty_breakdown_productgroup"
#define C_LOYALTY_BYPRODUCTCATEGORY         @"loyalty_breakdown_productcategory﻿"
#define C_LOYALTY_BYPRODUCTTIER         @"loyalty_breakdown_producttier"
#define C_LOYALTY_UPDATECONTRACT         @"btn_updatecontract"
#define C_LOYALTY_CONTINUECONTRACT         @"btn_continuecontract"
#define C_LOYALTY_TERMINATECONTRACT         @"btn_terminatecontract"
#define C_LOYALTY_EXTENDCONTRACT         @"btn_extendcontract"
#define C_LOYALTY_COMPLETECONTRACT         @"btn_completecontract"
#define C_LOYALTY_ALERT         @"popup_alert_header"
#define C_LOYALTY_THANKYOU         @"loyalty_thankyou_header"
#define C_LOYALTY_PROGRAMDETAILS_TITLE         @"title_programdetails"
#define C_LOYALTY_PROGRAMID         @"loyalty_programid"
#define C_LOYALTY_ATTN         @"loyalty_programdetails_attn"
#define C_LOYALTY_MOBILE         @"loyalty_programdetails_mobile"
#define C_LOYALTY_PROGRAMDETAILS         @"loyalty_programdetails_header"
#define C_LOYALTY_PROGRAMPERIOD         @"loyalty_programdetails_period_header"
#define C_LOYALTY_MINORDER         @"loyalty_programdetails_minpremium_header"
#define C_LOYALTY_PROGRAMREWARDS         @"loyalty_programdetails_rewards_header"
#define C_LOYALTY_GOLD         @"loyalty_gold"
#define C_LOYALTY_VOUCHER         @"loyalty_voucher"
#define C_LOYALTY_VIEWCONTRACTS         @"btn_viewcontracts"
#define C_LOYALTY_BACKTODETAILS         @"btn_backtoprgdetails"
#define C_LOYALTY_ADDNEW_TITLE         @"title_addnewprogram"
#define C_LOYALTY_CHOOSEREWARD         @"loyalty_chooserewards_header"
#define C_LOYALTY_RECOMMENDED_HEADER         @"loyalty_addnew_headerview_content"
#define C_LOYALTY_SELECTWORKSHOP         @"loyalty_selectworkshop_placeholder"
#define C_LOYALTY_NEXT         @"btn_next"
#define C_LOYALTY_SELECTREWARD         @"loyalty_addnew_selectreward"
#define C_LOYALTY_PREFERREDVALUE         @"loyalty_addnew_preferredvalue"
#define C_LOYALTY_NOWORKSHOPAVAILABLE         @"popup_noworkshop_content"
#define C_LOYALTY_GIFTVOUCHER         @"loyalty_giftvouchers"
#define C_LOYALTY_NOPACKAGES         @"popup_nopackages_content"
#define C_LOYALTY_ALERT_MULTIPLE         @"popup_wrongvalue_content"
#define C_LOYALTY_CONFIRMATION         @"title_loyalty_confirmation"
#define C_LOYALTY_CONTRACTUPDATED         @"loyalty_updated_header"
#define C_LOYALTY_UPDATEPROGRAM_TITLE         @"title_updateprogram"
#define C_LOYALTY_EXTENDPROGRAM_TITLE         @"title_extendprogram﻿"
#define C_LOYALTY_CONFIRMPRGDETAILS         @"title_confirmprogramdetails"
#define C_LOYALTY_ADDNEW_HEADER         @"loyalty_addnew_details_headerview"
#define C_LOYALTY_UPDATE_HEADER         @"loyalty_update_details_headerview"
#define C_LOYALTY_WORKSHOPNAME         @"loyalty_workshopname"
#define C_LOYALTY_RECOMMENDEDPACKAGES         @"loyalty_recommendedpackages"
#define C_LOYALTY_MINIMUMORDER         @"loyalty_minimumpremium_header"
#define C_LOYALTY_REWARDSDELIVERY         @"loyalty_deliveryaddress_header"
#define C_LOYALTY_SUBMIT         @"btn_loyalty_submit"
#define C_LOYALTY_UPDATE         @"btn_loyalty_update﻿"
#define C_LOYALTY_EXTEND         @"btn_loyalty_extend﻿"
#define C_LOYALTY_OWNERNAME         @"form_ownername"
#define C_LOYALTY_ADDRESSLINE1         @"form_loyalty_addressline1"
#define C_LOYALTY_ADDRESSLINE2         @"form_loyalty_addressline2"
#define C_LOYALTY_FILTERBY_TITLE         @"title_filterby"
#define C_LOYALTY_CLEARALL         @"btn_clearall"
#define C_LOYALTY_FILTERBY         @"btn_filterby"
#define C_LOYALTY_FILTER_SHOWRESULTS         @"btn_showresults"
#define C_LOYALTY_FILTER_STATUS         @"loyalty_filterby_status"
#define C_LOYALTY_FILTER_POROGRAMPERIOD         @"loyalty_filterby_programperiod"
#define C_LOYALTY_FILTER_WORKSHOPCATEGORY         @"loyalty_filterby_workshopcategory"
#define C_LOYALTY_LEGEND         @"loyalty_legend"
#define C_LOYALTY_RECOMMENDED         @"loyalty_recommended"
#define C_LOYALTY_CTNS         @"loyalty_ctns"
#define C_LOYALTY_PROGRAMPERFORAMNCE         @"title_programperformance"
#define C_LOYALTY_VIEW         @"btn_view"
#define C_LOYALTY_SEARCHBYWORKSHOPNAME         @"loyalty_searchbyworkshop_placeholder"
#define C_LOYALTY_PROGRAMBREAKDOWN_TITLE         @"title_programbreakdown"
#define C_LOYALTY_WITHINRANGE         @"popup_errorforrange_content"
#define C_LOYALTY_PROGRAM_TITLE         @"title_programlist"
#define C_LOYALTY_LEGEND_TOTAL         @"popup_legend_totalcartons"
#define C_LOYALTY_LEGEND_PERMONTH         @"popup_legend_minimum"
#define C_LOYALTY_CHOOSEMULTIPLIER         @"loyalty_choosemultiplier"
#define C_LOYALTY_TNC         @"popup_tnc_content"


#define C_PERFORMANCE_ALLWORKSHOPS         @"performance_allworkshops"
#define C_TITLE_IN_MANAGEMYMECHANICS         @"title_managemymechanics"
#define C_PERFORMANCE_FILTERBY         @"performance_filterbydaterange"

#define C_POPUP_PLEASE_ENTER_REMARKS         @"form_popup_required_remarks_content"
#define C_PERFORMANCE_MYRANKING         @"performance_myranking"


#define C_HOMECAROUSEL_MECHID         @"home_famechid"

#define C_QL_MYLIBRARY         @"ql_mylibrary"
#define C_QL_MYREWARDS         @"ql_myrewards"
#define C_QL_REGOILCHANGE         @"ql_regoilchange"

#define C_QL_MANAGEMYMECH         @"ql_managemymech"

#define C_QL_CUSTREDEMPTION         @"ql_customerredemption"

#define C_QL_PARTICIPATINGPRODUCTS         @"ql_participatingproducts"
#define C_QL_INVITECUSTOMER         @"ql_invitecustomer"

#define C_QL_SCANDECAL         @"ql_scandecal"
#define C_QL_SEARCHVEHICLEID         @"ql_searchvehicleid"
#define C_QL_MANAGEMYWORKSHOP         @"ql_managemyworkshop"
#define C_QL_MANAGEORDERS         @"ql_manageorders"
#define C_QL_MYNETWORK         @"ql_mynetwork"
#define C_QL_MYPERFORMANCE         @"ql_myperf"

#define C_SETTINGS_LOGOUT         @"settings_logout"
#define C_SIDEMENU_PARTNERS         @"drawer_partners"
#define C_SIDEMENU_ANALYTICS         @"drawer_analytics"
#define C_SIDEMENU_MYNETWORK         @"drawer_mynetwork"
#define C_SIDEMENU_MANAGESTAFF         @"drawer_managestaff"

#define C_SIDEMENU_MANAGEORDER         @"drawer_manageorders"

#define C_SIDEMENU_MYPERFORMANCE         @"drawer_myperf"
#define C_SIDEMENU_WORKSHOPPERFORMANCE         @"drawer_workshopperf"
#define C_SIDEMENU_MECHPERFORMANCE         @"drawer_mechperf"

#define C_SIDEMENU_MANAGEMYWORKSHOP     @"drawer_managemyworkshops"
#define C_SIDEMENU_MANAGEMYMECH            @"drawer_managemymechanics"
#define C_SIDEMENU_MYLIBRARY            @"drawer_mylibrary"


#define C_SIDEMENU_BACK           @"drawer_back"

#define C_SIDEMENU_REGOILCHANGE           @"drawer_regoilchange"
#define C_SIDEMENU_INVITECUSTOMER           @"drawer_invitecustomer"
#define C_SIDEMENU_SCANDECAL           @"drawer_scandecal"
#define C_SIDEMENU_ACTIVATEDECAL           @"drawer_activatedecal"
#define C_SEARCHVEHICLE_SERVICINGHISTORY    @"searchvehicle_in_servicinghistory"
#define C_REGISTEROILCHANGE_IN_INFO         @"registeroilchange_in_info"
#define C_ALERT_SAVEDCODE                   @"popup_error_codestored"
#define C_REGISTEROILCHANGE_IN_CLICKON      @"registeroilchange_result_in_clickon"
#define C_REGISTEROILCHANGE_IN_STATUSINFO      @"registeroilchange_result_in_statusinfo"
#define C_FILTER_BY_PROFILETYPE             @"popup_filterbyprofiletype"
#define C_FORM_PUBLICHOLIDAYS            @"form_public_holiday_content"

#define C_HEADER_BANKDETAILS         @"form_title_bankdetails"
#define C_FORM_PANCARDID            @"form_pancardid"
#define C_FORM_BENEFICIARYNAME        @"form_beneficiaryname"
#define C_FORM_BENEFICIARYNAME_PLACEHOLDER @"form_beneficiaryname_placeholder"
#define C_FORM_BANKNAME             @"form_bankname"
#define C_DROPDOWN_BANKNAME @"dropdown_selectbank"
#define C_FORM_BENEFICIARYACCTNUM        @"form_beneficiary_accountno"
#define C_FORM_BENEFICIARYIFSC        @"form_beneficiary_ifsccode"
#define C_FORM_EXAMPLE        @"form_example"
#define C_FORM_BANKINFO        @"form_bankinfotandc"
#define C_FORM_PANCARDIMAGE     @"form_pancardimage"
#define C_FORM_PANCARDIMAGE_PLACEHOLDER     @"form_uploadphotoofpanid"
#define C_FORM_TNC_IN           @"form_tnc_india_ios"

#define C_REWARDS_UP        @"rewards_up"
#define C_BUTTON_OK         @"btn_ok"

#define C_REGISTEROILCHANGE_SUBTITLE    @"registeroilchange_in_info"
#define C_CAROUSEL_PROMOTION_DAYSLEFT @"home_promo_expiredate"

#define C_WALKTHROUGH_REGISTEROILCHANGE_HEADER @"walkthrough_registeroilchange_header"
#define C_WALKTHROUGH_REGISTEROILCHANGE @"walkthrough_registeroilchange"
#define C_WALKTHROUGH_REGISTEROILCHANGE_CONTENT_ONE @"walkthrough_registeroilchange_one"
#define C_WALKTHROUGH_REGISTEROILCHANGE_CONTENT_TWO @"walkthrough_registeroilchange_two"
#define C_WALKTHROUGH_REGISTEROILCHANGE_CONTENT_LAST @"walkthrough_registeroilchange_final"

#define C_WALKTHROUGH_REWARDS_CONTENT_ONE   @"walkthrough_rewards_one"
#define C_WALKTHROUGH_REWARDS_CONTENT_TWO   @"walkthrough_rewards_two"
#define C_WALKTHROUGH_REWARDS_CONTENT_LAST  @"walkthrough_rewards_final"

#define C_DROPDOWN_WORKSHOPCATEGORY @"form_selectworkshopcategory_dropdown"

#define C_FORM_MECHSOURCE @"form_mechsource"
#define C_FORM_MECHCATEGORY @"form_mechcategory"
#define C_FORM_MECHTYPE @"form_mechtype"
#define C_DROPDOWN_MECHSOURCE @"dropdown_mechsource"
#define C_DROPDOWN_MECHCATEGORY @"dropdown_mechcategory"
#define C_DROPDOWN_MECHTYPE @"dropdown_mechtype"

#define C_FORM_MECHSHARECODE @"form_mechsharecode"

#define C_FORM_BANKAGREE @"form_bankinfotandc"
#define C_CTA_ADDWORKSHOP @"btn_addworkshop"

#define C_POPUP_DEACTIVATEMECH      @"popup_deactivatemech"
#define C_POPUP_DEACTIVATEOWNER     @"popup_deactivatetradeowner"

#define C_FORM_CUSTOMERCATEGORY @"form_customercategory"
#define C_DROPDOWN_CUSTOMERCATEGORY @"dropdown_customercategory"

#define C_FORM_RETAILERIDBADGE @"form_retaileridbadge"

#define C_PROFILE_MECHANICSHARECODE  @"form_mechanicsharecode"
#define C_MANAGEWORKSHOP_IN_DETAIL_TITLE   @"manageworkshops_details"
#define C_DROPDOWN_SELECTCUSTOMERTYPE   @"popup_selectcustomertype"
#define C_MANAGEWORKSHOP_IN_WORKSHOP    @"manageworkshop_in_network_workshop"
#define C_MANAGEWORKSHOP_IN_RETAILER    @"manageworkshop_in_network_workshop"

//For Registration
#define C_FORM_MSG_OTPSENT              @"form_msg_otpsent"
#define C_FORM_MSG_AVOIDMULTIPLEOTP     @"form_msg_avoidmultipleotp"

#define C_FORM_LEADSOURCE                               @"form_leadsource"
#define C_MANAGEORDER_CANCELLATION_TITLE                @"title_cancelconfirmation"
#define C_INVITECUSTOMER_VEHICLEID_PLACEHOLDER          @"invitecustomer_vehicleno_placeholder"
#define C_SIDEMENU_MECHMODULES                          @"drawer_mymechanicmodules"

#define C_WORKSHOPOFFER_ALLTYPES                @"workshopoffer_alltypes"
#define C_WORKSHOPOFFER_ALLSTATUS               @"workshopoffer_allstatus"

#define C_FORM_CONFIRMBENEFICIARYNUM        @"form_confirmbeneficiaryno"
#define C_FORM_BENEFICIARYNUM_NOTMATCH      @"form_popup_beneficiaryno_not_match"
#define C_RU_SELECTBYPRODUCTUSED            @"workshopoffer_selectbyproduct"

#define C_GOGOVANREGISTERED                @"invitecustomer_gogovanregistered"
#define C_GOGOVANDRIVERID                  @"invitecustomer_gogovandriverid"
#define C_HOME_CUSTOMERSERVICE             @"home_customerservice"
#define C_ERROR_CANNOTRECOGNIZE            @"registeroilchange_carplatescan_cannotrecognize"
#define C_ERROR_UNKNOWNERROR               @"registeroilchange_carplatescan_unknownerror"
#define C_POPUP_SCANCARPLATE               @"registeroilchange_popup_scancarplate"
#define C_POPUP_SCANDECAL                  @"registeroilchange_popup_scandecal"

//Promotion Localization
#define C_BTN_SCANNOW                       @"btn_scannow"
#define C_BTN_VIEWRECENTACTIVITY            @"btn_viewrecentactivity"
#define C_CAROUSEL_EVOUCHER                 @"carousel_evoucher"
#define C_CAROUSEL_TOTALOILCHANGE           @"carousel_totaloilchanges"

#define C_FORM_PANCARD_PHOTOFORMAT         @"form_pancardidformat_content"

#define C_FORM_DRIVINGLICENSEID            @"form_drivinglicense"
#define C_FORM_DRIVINGLICENSEIMAGE               @"form_drivinglicenseimage"
#define C_FORM_DRIVINGLICENSE_PHOTOFORMAT   @"form_drivinglicenseformat_content"

#define C_FORM_VOTERID                       @"form_voterid"
#define C_FORM_VOTERIDIMAGE               @"form_voteridimage"
#define C_FORM_VOTERID_PHOTOFORMAT         @"form_voteridformat_content"
#define C_FORM_PANCARD                      @"form_pancard"

#define C_FORM_UPLOADED                     @"form_uploaded"
#define C_FORM_UPLOAD                       @"pendingtasks_upload_btn"

//popup error messages
#define C_POPUP_ERROR_PANFORMAT                         @"popup_error_pan_format"
#define C_POPUP_ERROR_DRIVINGLICENSEFORMAT              @"popup_error_drivinglicense_format"
#define C_POPUP_ERROR_VOTERIDFORMAT                     @"popup_error_voterid_format"
#define C_POPUP_ERROR_PAN_REQUIRED                      @"popup_required_pancard"
#define C_POPUP_ERROR_DRIVINGLICENSE_REQUIRED           @"popup_required_drivinglicense"
#define C_POPUP_ERROR_VOTERID_REQUIRED                  @"popup_required_voterid"
#define C_POPUP_ERROR_PANIMAGE_REQUIRED                 @"popup_required_pancardimage"
#define C_POPUP_ERROR_DRIVINGLICENSEIMAGE_REQUIRED      @"popup_required_drivinglicenseimage"
#define C_POPUP_ERROR_VOTERIDIMAGE_REQUIRED             @"popup_required_voteridimage"


//Cash Incentive
#define C_UNTIL_NEXT_REMIT          @"cashincentive_untilnextremit"
#define C_RUPEE                     @"cashIncentive_rupee"
#define C_CURRENT_EARNINGS          @"cashincentive_currentearnings"
#define C_TOTAL_EARNINGS            @"cashincentive_totalearnings"
#define C_TOTAL_REMITTED            @"cashincentive_totalremitted"
#define C_UPDATED_AS_OF             @"cashincentive_updatedasof"
#define C_CASHINCENTIVE_DISCLAIMER1 @"cashincentive_disclaimer1"
#define C_CASHINCENTIVE_DISCLAIMER2 @"cashincentive_disclaimer2"
#define C_DRAWER_CASHINCENTIVE      @"drawer_cashincentive"

//SWA/SDA/Shell Masterclass
#define C_DRAWER_SWA                @"drawer_swa"
#define C_DRAWER_SDA                @"drawer_sda"
#define C_POPUP_SWA_TITLE           @"popup_swa_title"
#define C_POPUP_SWA_CONTENT         @"popup_swa_content"
#define C_POPUP_SDA_CONTENT         @"popup_sda_content"
#define C_POPUP_SWA_BTN             @"popup_swa_btn"

#define C_DRAWER_SMC                @"drawer_smc"
#define C_POPUP_SMC_CONTENT         @"popup_smc_content"


//Vehicle Type
#define C_FORM_VEHICLETYPE                      @"form_vehicletype"
#define C_DROPDOWN_VEHICLETYPE_HEADER           @"dropdown_vehicletype_header"


//Partners Of Shell V2
#define C_PARTNERSOFSHELL_HEADER                @"partnersofshell_header"
#define C_PARTNERSOFSHELL_PERFORMANCE_HEADER    @"partnersofshell_performance_header"
#define C_RU_B2C_CAMPAIGN                       @"ru_btoc_campaign"
#define C_RU_B2B_CAMPAIGN                       @"ru_btob_campaign"
#define C_PARTNERSOFSHELL_TOTALORDERED          @"partnersofshell_totalordered"
#define C_PARTNERSOFSHELL_EARNEDPOINTS          @"partnersofshell_earnedpoints"
#define C_RU_B2C_BREAKDOWN_TITLE                @"ru_btoc_breakdown_title"
#define C_RU_B2B_BREAKDOWN_TITLE                @"ru_btob_breakdown_title"
#define C_PARTNERSOFSHELL_HEADER_DSR            @"partnersofshell_header_dsr"
#define C_PARTNERSOFSHELL_HEADER_DSM            @"partnersofshell_header_dsm"

#define C_RU_REGISTRATION_WORKSHOPSELECT_PLACEHOLDER         @"ru_manageworkshop_registration_workshopselect_placeholder"
#define C_RU_REGISTRATION_NOWORKSHOP                         @"ru_manageworkshop_registration_noworkshop"
#define C_RU_REGISTRATION_TABLE_PRIMARY                      @"ru_manageworkshop_registration_table_primary"
#define C_RU_REGISTRATION_TABLE_WORKSHOPNAME                 @"ru_manageworkshop_registration_table_workshopname"
#define C_RU_REGISTRATION_PARTICIPANTS_PLACEHOLDER           @"ru_manageworkshop_registration_participants_placeholder"
#define C_RU_REGISTRATION_PARTICIPANTS                       @"ru_manageworkshop_registration_participants"
#define C_RU_REGISTRATION_REGISTEREDNAME_HINT                @"ru_manageworkshop_registration_registeredname_hint"
#define C_RU_REGISTRATION_REGISTEREDNAME_PLACEHOLDER         @"ru_manageworkshop_registration_registeredname_placeholder"
#define C_RU_REGISTRATION_REGISTEREDNAME                     @"ru_manageworkshop_registration_registeredname"
#define C_RU_REGISTRATION_TITLE                              @"ru_manageworkshop_registration_title"
#define C_RU_REGISTRATION_HEADER                             @"ru_manageworkshop_registration_header"
#define C_FORM_LINKEDWORKSHOP                                @"form_linkedworkshops"
#define C_RU_ACTIVATE_PREREGISTERED_TITLE                    @"ru_activate_preregistered_title"
#define C_FORM_POSTAXID                                      @"form_postaxid"
#define C_RU_LITRE_SYMBOL                                    @"partnersofshell_litre_symbol"
#define C_RU_HOWTOEARNPOINTS_B2B_PERIOD                      @"ru_howtoearnpoints_b2b_period"
#define C_RU_HOWTOEARNPOINTS_B2C_PERIOD                      @"ru_howtoearnpoints_b2c_period"
#define C_RU_HOWTOEARNPOINTS_B2B_HEADER                      @"ru_howtoearnpoints_b2c_header"
#define C_RU_HOWTOEARNPOINTS_B2C_HEADER                      @"ru_howtoearnpoints_b2b_header"
#define C_POPUP_B2B_SUPPORT                                  @"popup_b2bsupport"
#define C_POPUP_B2C_SUPPORT                                  @"popup_b2csupport"

//TH
#define C_PARTICIPATINGPRODUCTS_TH_FOOTER                    @"participatingproducts_th_footer"
#define C_BTN_CONTINUE_TAGGING                               @"btn_continue_tagging"
#define C_POPUP_DECAL_HEADER                                 @"popup_decal_header"

#define C_REGISTEROILCHANGE_INPUTERROR                       @"registeroilchange_inputerror"

//IN RETAILER SCAN BOTTLE
#define C_QL_SCANBOTTLE                                      @"ql_scanbottle"
#define C_SCANBOTTLE_CUSTOMER                                @"scanbottle_customer"
#define C_SCANBOTTLE_SHARECODE                               @"scanbottle_sharecode"
#define C_SCANBOTTLE_SHARECODE_PLACEHOLDER                   @"scanbottle_sharecode_placeholder"
#define C_SCANBOTTLE_NOTREGISTERED                           @"scanbottle_notregistered"
#define C_SCANBOTTLE_BOTTLECODES_HEADER                      @"scanbottle_bottlecodes_header"
#define C_SCANBOTTLE_WORKSHOPORMECHANIC                      @"scanbottle_workshopormechanic"
#define C_SCANBOTTLE_QR_MESSAGE                              @"scanbottle_qr_message"
#define C_SCANBOTTLE_IDORCODE                                @"scanbottle_idorcode"
#define C_SCANBOTTLE_VEHICLEID                               @"scanbottle_vehicleid"
#define C_SCANBOTTLE_RESULT_SHARECODE                        @"scanbottle_result_sharecode"

#define C_HOME_GREET_MORNING                                 @"home_greet_morning"
#define C_HOME_GREET_AFTERNOON                               @"home_greet_afternoon"
#define C_HOME_GREET_EVENING                                 @"home_greet_evening"
#define C_HEADER_ACCOUNTINFO                                 @"header_accountinfo"

#define C_POPUP_INVITECUSTOMER_MAXVEHICLE                    @"popup_invitecustomer_maxvehicle"
#define C_POPUP_INVITECUSTOMER_ALREADYMEMBER                 @"popup_invitecustomer_alreadymember"

#define C_COINS_PROFILE_HEADER @"profile_coins_header"
#define C_TITLE_PHOTOID @"form_title_photoid"
#define C_TAX_TNC @"form_taxtnc"
#define C_TAX_TNC_LINK @"form_taxtnc_link"

#define C_TITLE_TAXID @"form_title_taxid"
#define C_FORM_ACCTVERIFY @"form_acctverification"

#define C_FORM_NAME @"form_name"
#define C_FORM_NAME_PLACEHOLDER @"form_name_placeholder"
#define C_FORM_ADDRESS @"form_address"
#define C_FORM_ADDRESS_PLACEHOLDER @"form_address_placeholder"

#define C_FORM_PHOTOIDFRONT @"form_photoid_front"

#define C_FORM_TAXID @"form_taxid"
#define C_FORM_TAXID_PLACEHOLDER @"form_taxid_placeholder"
#define C_POPUP_TAXNUMBER_VALIDATION    @"popup_taxnumber_validation"

#define C_FORM_TAXPAYERNAME @"form_taxpayername"
#define C_FORM_TAXPAYERNAME_PLACEHOLDER @"form_taxpayername_placeholder"

#define C_FORM_TAXPAYERADDRESS @"form_taxpayeraddress"
#define C_FORM_TAXPAYERADDRESS_PLACEHOLDER @"form_taxpayeraddress_placeholder"
#define C_FORM_TAXIDFORMAT_CONTENT         @"form_taxidformat_content"
#define C_FORM_PHOTOID_CONTENT             @"form_photoid_content"

#define C_FORM_TAXIDFRONT @"form_taxid_front"

#define C_TITLE_MOREINFO @"title_moreinfo"

#define C_TITLE_MYCOINS @"title_mycoins"
#define C_COINS_AVAILABLEBALANCE @"coins_availablebalance"
#define C_COINS_STARTEARNING @"coins_startearning"

#define C_TITLE_COINBACKPROGRAM             @"title_coinbackprogram"
#define C_POPUP_COINBACK_SUCCESS_CONTENT    @"popup_coinback_success_content"
#define C_STATUS_PENDINGVERIFICATION        @"status_pendingverification"
#define C_STATUS_VERIFIED                   @"status_verified"
#define C_STATUS_NOTVERIFIED                @"status_notverified"
#define C_FORM_COINAGREE                    @"form_coinagree"
#define C_BTN_LATER                         @"btn_later"
#define C_BTN_UPDATENOW                     @"btn_updatenow"
#define C_BTN_JOINNOW                       @"btn_joinnow"
#define C_HOME_PROMOTIONS                   @"home_promotions"
#define C_HOME_NEWS                         @"home_news"
#define C_HOME_ALL                          @"home_all"
#define C_HOME_VIEWALL_BTN                  @"home_viewall_btn"
#define C_HOME_NEWS_TAB                     @"home_news_tab"
#define C_HOME_INBOX_TAB                    @"home_inbox_tab"
#define C_HOME_EMPTYSECTION                 @"home_emptysection"
#define C_HOME_BADGEFOOTER_GENERAL_FULLTEXT @"home_badgefooter_general_fulltext"

#define C_BTN_ACCEPT                        @"btn_accept"
#define C_BTN_RETAKE                        @"btn_retake"

#define C_LOYALTY_COINS                     @"loyalty_coins"
#define C_FORM_PHOTOID                      @"form_photoid"

//Coachmark
#define C_BTN_SKIPALL                       @"btn_skipall"
#define C_BTN_OKAYGOTIT                     @"btn_okaygotit"
#define C_COACHMARK_HOME_STEP1              @"coachmark_home_step1"
#define C_COACHMARK_HOME_STEP2              @"coachmark_home_step2"
#define C_COACHMARK_HOME_STEP3              @"coachmark_home_step3"
#define C_COACHMARK_HOME_STEP4              @"coachmark_home_step4"
#define C_COACHMARK_HOME_STEP5              @"coachmark_home_step5"
#define C_COACHMARK_COINS_STEP1             @"coachmark_coins_step1"

//TR Changes
#define C_HOME_EARNREDEEM_TITLE             @"home_earnredeem_title"
#define C_HOME_EARNREDEEM_CONTENT           @"home_earnredeem_content"
#define C_HOME_EARNREDEEM_FREEBOTTLE        @"home_earnredeem_freebottle"
#define C_HOME_EARNREDEEM_FOOTER            @"home_earnredeem_footer"

//TH Incentive Carousels
#define C_HOME_THINCENTIVE_TITLE                  @"home_thincentive_title"
#define C_HOME_THINCENTIVE_DISCLAIMER             @"home_thincentive_disclaimer"
#define C_HOME_THINCENTIVE_FOOTER                 @"home_thincentive_footer"
#define C_HOME_THINCENTIVE_UNLOCKLABEL            @"home_thincentive_unlocklabel"
#define C_HOME_THINCENTIVE_BAHT                   @"home_thincentive_baht"
#define C_HOME_THINCENTIVE_EARNEDPOINTS           @"home_thincentive_earnedpoints"
#define C_HOME_THINCENTIVE_TIERUNLOCKED           @"home_thincentive_tierunlocked"
#define C_HOME_THINCENTIVE_ORDERBREAKDOWN_TITLE   @"home_thincentive_orderbreakdown_title"
#define C_HOME_THINCENTIVE_PREMIUMTIER            @"home_thincentive_premiumtier"
#define C_HOME_THINCENTIVE_OTHERPRODUCTS          @"home_thincentive_otherproducts"
#define C_REWARDS_POINTS_SMALL                    @"rewards_point_small"

#define C_BTN_HOWTOREDEEM                   @"btn_howtoredeem"

#define C_REGISTEROILCHANGE_EMPTY_CONTENT   @"registeroilchange_empty_content"
#define C_LUBEMATCH_MAKE                    @"lubematch_make"
#define C_LUBEMATCH_MODEL                   @"lubematch_model"
#define C_LUBEMATCH_YEAR                    @"lubematch_year"
#define C_LUBEMATCH_MAINTENANCE             @"lubematch_maintenance"
#define C_LUBEMATCH_MEMBERSINCE             @"lubematch_membersince"
#define C_LUBEMATCH_RECOMMENDATION_ENGINEOIL    @"lubematch_recommendation_engineoil"
#define C_LUBEMATCH_SERVICEHISTORY          @"lubematch_servicehistory"
#define C_LUBEMATCH_MILEAGE                 @"customerredemption_mileage"
#define C_LUBEMATCH_NORECOMMENDATION_CONTENT    @"lubematch_norecomeendation_content"
#define C_BTN_GETLUBEMATCH                  @"btn_getlubematch"
#define C_LUBEMATCH_NOSERVICEHISTORY        @"lubematch_noservicehistory"
#define C_LUBEMATCH_SERVICEDBY              @"lubematch_servicedby"
#define C_LUBEMATCH_MONTHS                  @"lubematch_months"
#define C_LUBEMATCH_CUSTOMERINFO            @"lubematch_customerinfo"
#define C_POPUP_LUBEMATCH_RECOMMENDATION_TITLE  @"popup_lubematch_recommendation_title"
#define C_POPUP_LUBEMATCH_RECOMMENDATION_HEADER @"popup_lubematch_recommendation_header"
#define C_LUBEMATCH_FORM_VEHICLEMAKE            @"lubematch_form_vehiclemake"
#define C_LUBEMATCH_FORM_VEHICLEMODEL           @"lubematch_form_vehiclemodel"
#define C_LUBEMATCH_FORM_VEHICLEYEAR            @"lubematch_form_vehicleyear"
#define C_POPUP_PLEASESELECT                    @"popup_pleaseselect"
#define C_LUBEMATCH_TRANSMISSION_HEADER         @"lubematch_transmission_header"
#define C_LUBEMATCH_REAR_HEADER                 @"lubematch_rear_header"
#define C_LUBEMATCH_COOLANT_HEADER              @"lubematch_coolant_header"
#define C_LUBEMATCH_BRAKEFLUID_HEADER           @"lubematch_brakefluid_header"
#define C_LUBEMATCH_TRANSFERBOX_HEADER          @"lubematch_transferbox_header"
#define C_LUBEMATCH_TAG_PREMIUM                 @"lubematch_tag_premium"
#define C_LUBEMATCH_TAG_STANDARD                @"lubematch_tag_standard"
#define C_LUBEMATCH_RECOMMENDEDFOR              @"lubematch_recommendedfor"
#define C_LUBEMATCH_SCANEMPTY_CONTENT           @"lubematch_scanempty_content"
#define C_LUBEMATCH_RECOMMENDATION_OTHER        @"lubematch_recommendation_other"
#define C_LUBEMATCH_VALIDTILL                   @"lubematch_validtill"
#define C_TITLE_INVENTORY                       @"title_inventorymanagement"
#define C_INVENTORY_MYINVENTORY_TITLE           @"inventory_myinventory_title"
#define C_INVENTORY_SCANIN_TITLE                @"inventory_scanin_title"
#define C_INVENTORY_CURRENTINSTOCK              @"inventory_currentinstock"
#define C_INVENTORY_BOTTLES                     @"inventory_bottles"
#define C_INVENTORY_PRODUCTSOLD                 @"inventory_productsold"
#define C_INVENTORY_HEADER_TO                   @"inventory_header_to"
#define C_INVENTORY_SCANIN_HEADER               @"inventory_scanin_header"
#define C_INVENTORY_BTN_SUBMITPRODUCTCODES      @"btn_submitproductcodes"
#define C_INVENTORY_BTN_SEEINVENTORY            @"btn_seeinventory"
#define C_INVENTORY_POINTHISTORY_TITLE          @"performance_pointshistory_title"
#define C_INVENTORY_SELECTWORKSHOP_HEADER       @"inventory_selectworkshop_header"
#define C_INVENTORY_SEARCHWORKSHOP_PLACEHOLDER  @"inventory_searchworkshop_placeholder"
#define C_INVENTORY_BTN_STARTINVENTORY          @"btn_startinventory"
#define C_INVENTORY_EARNPOINTS_BTN @"partnersofshell_pts"
#define C_INVENTORY_EARNINGS_LBL @"inventory_earnings"
#define C_INVENTORY_BTN_CONTINUEINVENTORY       @"btn_continueinventory"
#define C_INVENTORY_SCAIN_RESULT_TITLE          @"inventory_scanin_result_title"
#define C_INVENTORY_MAXREACHED_POPUP            @"inventory_maxreached_popup"
#define C_INVENTORY_POINTHISTORY_HEADER         @"inventory_pointshistory_header"
// inventory_earnings_header
#define C_INVENTORY_HEADER_DISCLAIMER        @"inventory_earnings_header"

//SADIX
#define C_MANAGEORDERS_FILTERBYSOURCE_PLACEHOLDER       @"manageorders_filterbysource_placeholder"
#define C_POPUP_ORDERSOURCE                             @"popup_ordersource"
#define C_MANAGEORDERS_ORDERSOURCE_OTHER                @"manageorders_ordersource_other"
#define C_MANAGEORDERS_ORDERSOURCE_SHARE                @"manageorders_ordersource_share"
#define C_POPUP_ORDERSOURCE_SHARE_REMARKS               @"popup_ordersource_share_remarks"
#define C_POPUP_ORDERSOURCE_OTHER_REMARKS               @"popup_ordersource_other_remarks"

#define C_LOYALTY_BREAKDOWN_TITLE               @"loyalty_breakdown_title"
#define C_LOYALTY_CONTRACTHISTORY_TITLE         @"loyalty_contracthistory_title"
#define C_LOYALTY_STARTTRACKING_CONTENT         @"loyalty_starttracking_content"
#define C_BTN_ACTIVATE                          @"btn_activate"
#define C_TITLE_ADDNEWCONTRACT                  @"title_addnewcontract"
#define C_LOYALTY_CONTRACTDETAILS_TITLE         @"loyalty_contractdetails_title"
#define C_LOYALTY_CONTRACTID                    @"loyalty_contractid"
#define C_LOYALTY_TNCCHECKBOX_CONTENT           @"loyalty_tnccheckbox_content"
#define C_LOYALTY_SIGNATURE                     @"loyalty_signature"
#define C_LOYALTY_CONTRACTACHIEVEMENTS          @"loyalty_contractachievements"
#define C_LOYALTY_EMPTYSTATE_CONTENT            @"loyalty_emptystate_content"
#define C_LOYALTY_CHOOSEWORKSHOP                @"loyalty_chooseworkshop"
#define C_LOYALTY_CHOOSEWORKSHOP_CONTENT        @"loyalty_chooseworkshop_content"
#define C_LOYALTY_CONFIRMDETAILS_CONTENT        @"loyalty_confirmdetails_content"
#define C_LOYALTY_ADDNEW_REWARD                 @"loyalty_addnew_reward"
#define C_LOYALTY_CONTRACTS_TITLE               @"loyalty_contracts_title"
#define C_LOYALTY_PERIOD_NOTAVAILABLE           @"loyalty_period_notavailable"
#define C_LOYALTY_PERIOD_NOTACCEPTED            @"loyalty_period_notaccepted"

//Lebaran Promotion
#define C_POPUP_LEBARAN_SCRATCH                         @"popup_lebaran_scratch"
#define C_POPUP_BTN_SKIP                                @"popup_btn_skip"

#define C_CONTACTUS_REDEMPTION_HOTLINE_HEADER           @"contactus_redemption_hotline_header"
#define C_POPUP_MOBILENUMBER_VALIDATION                 @"popup_mobilenumber_validation"

#define C_POPUP_SELECTBYPRODUCTGRP                      @"popup_selectbyproductgrp"
#define C_PARTICIPATINGPRODUCTS_HEADER                  @"participatingproducts_header"
#define C_PARTICIPATINGPRODUCTS_CASH                    @"participatingproducts_cash"

#define C_COACHMARK_LUBEMATCH_STEP1                     @"coachmark_lubematch_step1"
#define C_COACHMARK_LUBEMATCH_STEP2                     @"coachmark_lubematch_step2"
#define C_COACHMARK_LUBEMATCH_STEP3                     @"coachmark_lubematch_step3"
#define C_COACHMARK_LUBEMATCH_STEP4                     @"coachmark_lubematch_step4"
#define C_COACHMARK_LUBEMATCH_STEP5                     @"coachmark_lubematch_step5"

//Gamification
#define C_HOME_CAROUSEL_CHECKIN_PTS                     @"home_carousel_checkin_pts"
#define C_HOME_CAROUSEL_CHECKIN_TODAY_BTN               @"home_carousel_checkin_today_btn"
#define C_HOME_CAROUSEL_CHECKIN_TMRW_BTN                @"home_carousel_checkin_tmrw_btn"
#define C_HOME_CAROUSEL_CHECKIN_GRANDPRIZE              @"home_carousel_checkin_grandprize"
#define C_HOME_CAROUSEL_CHECKIN_DAYSLEFT                @"home_carousel_checkin_daysleft"
#define C_HOME_CAROUSEL_CHECKIN_OPENNOW                 @"home_carousel_checkin_opennow"
#define C_HOME_CAROUSEL_CHECKIN_REDEEMED                @"home_carousel_checkin_redeemed"

#define C_COACHMARK_CHECKIN_STEP1                       @"coachmark_checkin_step1"
#define C_COACHMARK_CHECKIN_STEP2                       @"coachmark_checkin_step2"
#define C_COACHMARK_CHECKIN_STEP3                       @"coachmark_checkin_step3"
#define C_COACHMARK_CHECKIN_STEP4                       @"coachmark_checkin_step4"
#define C_COACHMARK_CHECKIN_STEP5                       @"coachmark_checkin_step5"

#define C_HOME_CAROUSEL_SCANFORREWARDS_HEADER           @"home_carousel_scanforrewards_header"
#define C_HOME_CAROUSEL_SCANFORREWARDS_CONTENT          @"home_carousel_scanforrewards_content"
#define C_HOME_CAROUSEL_SCANFORREWARDS_VIEWMORE_BTN     @"home_carousel_scanforrewards_viewmore_btn"
#define C_SCANFORREWARDS_TOTALDISBURSED                 @"scanforrewards_totaldisbursed"
#define C_SCANFORREWARDS_HEADER_CONTENT                 @"scanforrewards_header_content"
#define C_SCANFORREWARDS_FILTERBYPRODUCT_PLACEHOLDER    @"scanforrewards_filterbyproduct_placeholder"

#define C_INVENTORY_BOTTLES                             @"inventory_bottles"
#define C_HOME_LEARNMORE                                @"home_learnmore"
#define C_SHAREPARTNERSCLUB_TITLE                       @"sharepartnersclub_title"

#define C_AI_WEEKATGLANCE_HEADER                        @"ai_weekatglance_header"
#define C_AI_ACHIEVED                                   @"ai_achieved"
#define C_AI_OILCHANGES_THISWEEK                        @"ai_oilchanges_thisweek"
#define C_AI_OILCHNAGES_LASTWEEK                        @"ai_oilchanges_lastweek"
#define C_AI_SWIPETOCLOSE_BTN                           @"ai_swipetoclose_btn"
#define C_AI_HOME_MYSHARESUMMARY                        @"ai_home_mysharesummary"

#define C_COACHMARK_SCANFORREWARDS_STEP1                @"coachmark_scanforrewards_step1"
#define C_COACHMARK_SCANFORREWARDS_STEP2                @"coachmark_scanforrewards_step2"
#define C_COACHMARK_SCANFORREWARDS_STEP3                @"coachmark_scanforrewards_step3"
#define C_COACHMARK_SCANFORREWARDS_STEP4                @"coachmark_scanforrewards_step4"
#define C_COACHMARK_SCANFORREWARDS_STEP5                @"coachmark_scanforrewards_step5"

#define C_BOOKING_STATUS_COMPLETED                      @"booking_status_completed"

