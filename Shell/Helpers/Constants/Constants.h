//
//  Constants.h
//  Syngenta
//
//  Created by Will Choy on 6/10/15.
//  Copyright © 2015 WillChoy. All rights reserved.
//

#import "Helper.h"


#ifndef Constants_h
#define Constants_h

//First Launch
#define kHasLaunchedOnce @"HasLaunchedOnce"
#define FIRSTLAUNCH ![[NSUserDefaults standardUserDefaults] boolForKey:kHasLaunchedOnce]

#define kIsRightToLeft UIView.appearance.semanticContentAttribute == UISemanticContentAttributeForceRightToLeft

//LOCALIZATION
static NSString * const kSaveLanguageDefaultKey = @"SaveLanguageDefaultKey";


typedef enum
{
    kREGISTRATION_TEXTFIELD = 1,        //First Name, etc.
    kREGISTRATION_STATUSVEIFICATION,
    kREGISTRATION_DROPDOWN,             //salutation, country, etc.
    kREGISTRATION_TEXTFIELDWITHBUTTON,  //DSR Code, location
    kREGISTRATION_BUTTON,               //working hours
    kREGISTRATION_MOBNUM,               //mobile number
    kREGISTRATION_BIRTHDATE,            //use DROPDOWN, but handle with dateView
    kREGISTRATION_LABELWITHTEXTFIELD,   //for 3 questions (helix, etc.)
    kREGISTRATION_TEXTVIEW,             //contact us, activation remarks,
    kREGISTRATION_HEADER,               //headers for combined tableviews
    kREGISTRATION_WORKINGHOURS,         //working hours (eg. Mon  - 0:00 - 23:59)
    kREGISTRATION_IMAGEUPLOAD,          //image
    kPROFILING_REARRANGE,             //rearrange
    kPROFILING_BOOLEANQUESTION,           //YES/NO
    kPROFILING_LABELWITHTEXTBOX,                 //Textbox
    kPROFILING_CHECKBOX,                 //Checkboxes
    kPROFILING_TEXTVIEW,                 //Boxed textview
    kREGISTRATION_SIGNATURE,
    kREGISTRATION_CONTACTPREF,
    kREGISTRATION_HEADERWITHREQUIRED,
    kREGISTRATION_BUTTONCELL,
    kREGISTRATION_TNCCHECKBOX,
    kFORM_PROFILE_TOPSECTION,
    kFORM_PROFILE_IMAGE,
    kFORM_PANCARD_ID,
    kFORM_PANCARD_IMAGE,
    kFORM_MULTICHECKBOX,
    
    kFORM_PANCARD_ID_NEW,
    kFORM_PANCARD_ID_NEWSTATUS,
    kFORM_DRIVERLICENSE,
    kFORM_VOTERID,
    
    kFORM_LINKEDWORKSHOP,
    kFORM_LABELONLY,
    kFORM_STATUS,
    
} kREGISTRATION_TYPE;

enum kOrderStatus_Type
{
    kOrderStatus_Pending = 1,
    kOrderStatus_Approved = 2,
    kOrderStatus_Rejected = 4,
    kOrderStatus_Completed = 8,
    kOrderStatus_DSM_Rejected = 16,
    kOrderStatus_AllStatus = 31,
    kOrderStatus_Cancel = 32,
};

typedef enum
{
    kPROFILETYPE_DMM           = 1 << 1,
    kPROFILETYPE_DSM           = 1 << 2,
    kPROFILETYPE_DSR           = 1 << 3,
    kPROFILETYPE_TRADEOWNER    = 1 << 4,
    kPROFILETYPE_MECHANIC      = 1 << 5,
    kPROFILETYPE_FORECOURTATTENDANT     = 1 << 7,
    kPROFILETYPE_DMR     = 1 << 8,
} kPROFILETYPE_TYPE;

typedef enum {
    ListSelectionTypeSingle = 0,
    ListSelectionTypeMultiple,
    ListSelectionTypeReorder
} CommonListSelectionType;

typedef enum {
    ENGLISH   = 1,
    INDO   = 2,
} LanguageType;

typedef enum {
    kCOACHMARK_UP_LEFTSIDE = 1,
    kCOACHMARK_UP_RIGHTSIDE,
    kCOACHMARK_DOWN_LEFTSIDE,
    kCOACHMARK_DOWN_RIGHTSIDE,
    kCOACHMARK_LEFT_UPSIDE,
    kCOACHMARK_LEFT_DOWNSIDE,
    kCOACHMARK_RIGHT_UPSIDE,
    kCOACHMARK_RIGHT_DOWNSIDE,
    
    kCOACHMARK_CENTER,
} kCOACHMARK_SIDE;

typedef enum {
    kCONTRACTSTATUS_INPROGRESS  = 1         ,
    kCONTRACTSTATUS_COMPLETED   = 1 << 1    ,
    kCONTRACTSTATUS_UNDERREVIEW = 1 << 2    ,
    kCONTRACTSTATUS_INCOMPLETE  = 1 << 3    ,
    kCONTRACTSTATUS_PENDING     = 1 << 4    ,
    kCONTRACTSTATUS_UNACCEPTED  = 1 << 5    ,
} kCONTRACTSTATUS_TYPE;

typedef enum {
    kTIERING_STANDARD   = 10,
    kTIERING_BRONZE     = 20,
    kTIERING_SILVER     = 30,
    kTIERING_GOLD       = 40,
    kTIERING_PLATINUM   = 50,
} kTIERING_CODE;


#define kDeviceLanguage @"DeviceLanguage" //0
#define kEnglish    @"en" //default - 1
#define kPlaceholder   @"en"
#define kThai       @"th" //4, previously 5
#define kIndo       @"id"   //5, previously 2
#define kChinese    @"zh-Hans" //8, previously 3
#define kMalay      @"ms" //9, previously 4
//#define kTChinese   @"zh-Hant"
#define kChineseT_HK @"zh-Hant-HK" //10
#define kRussian     @"ru"  //11
#define kVietnamese @"vi" //12
#define kArabic     @"ar"  //20
#define kUrdu        @"ur" //21
#define kFilipino @"fil" //22
#define kEnglishIndia @"en-IN" //23

#define kBengali    @"bn-IN" //24
#define kHindi      @"hi-IN" //25
#define kKannada    @"kn-IN" //26
#define kMalayalam  @"ml-IN" //27
#define kTamil      @"ta-IN" //28
#define kTelugu     @"te-IN" //29
#define kTurkish     @"tr" //30
#define kEnglishIndo @"en-ID" //31

static NSString * const LanguageType_toString[] = {
    [ENGLISH] = kEnglish,
    [INDO] = kIndo,
};

//separate to max 5 languages per line. 0-4, 5-9, 10-14,15-19,20-23
#define kAvailableLanguages [NSArray arrayWithObjects: \
kDeviceLanguage, kEnglish, kDeviceLanguage, kDeviceLanguage, kThai, \
kIndo, kPlaceholder, kPlaceholder, kChinese, kMalay, \
kChineseT_HK, kRussian, kVietnamese, kPlaceholder, kPlaceholder, \
kPlaceholder, kPlaceholder, kPlaceholder, kPlaceholder, kPlaceholder, \
kArabic, kUrdu, kFilipino, kEnglishIndia,\
kBengali, kHindi, kKannada, kMalayalam, kTamil, kTelugu, \
kTurkish, kEnglishIndo,  \
nil]


//NSUSERDEFAULTS

#define LOOKUP_KEY @"SHELL_LOOKUP_TABLE"
#define GET_LOOKUP_TABLE                [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults]objectForKey:LOOKUP_KEY]]
#define SET_LOOKUP_TABLE(lookupData)    [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject: lookupData] forKey: LOOKUP_KEY]

#define CONSUMER_TNC_KEY @"LOOKUP_CONSUMER_TNC"
#define GET_CONSUMER_TNC                [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults]objectForKey:CONSUMER_TNC_KEY]]
#define SET_CONSUMER_TNC(lookupData)    [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject: lookupData] forKey: CONSUMER_TNC_KEY]

#define WORKSHOP_TNC_KEY @"LOOKUP_WORKSHOP_TNC"
#define GET_WORKSHOP_TNC                [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults]objectForKey:WORKSHOP_TNC_KEY]]
#define SET_WORKSHOP_TNC(lookupData)    [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject: lookupData] forKey: WORKSHOP_TNC_KEY]

#define DSR_TNC_KEY @"LOOKUP_DSR_TNC"
#define GET_DSR_TNC                [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults]objectForKey:DSR_TNC_KEY]]
#define SET_DSR_TNC(lookupData)    [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject: lookupData] forKey: DSR_TNC_KEY]

//set walkthrough dictionary as mutable dictionary.
//on push to vc with walkthrough, assign key as name of VC.
//if key not found, show walkthrough then update walkthrough dictionary with a @(NO).
#define WALKTHROUGH_KEY @"WALKTHROUGH_KEY"
#define GET_WALKTHROUGH_TABLE                [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults]objectForKey:WALKTHROUGH_KEY]]
#define SET_WALKTHROUGH_TABLE(walkthroughData)    [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject: walkthroughData] forKey: WALKTHROUGH_KEY]

#define DEFAULT_COUNTRY_CODE @"458"
#define GET_COUNTRY_CODE [[NSUserDefaults standardUserDefaults]objectForKey: COUNTRYCODE_KEY]
#define SET_COUNTRY_CODE(countryCode) [[NSUserDefaults standardUserDefaults] setObject:countryCode forKey: COUNTRYCODE_KEY]
#define IS_COUNTRY(countryCode) [GET_COUNTRY_CODE isEqualToString: countryCode]
#define COUNTRYCODE_MALAYSIA @"458"
#define COUNTRYCODE_INDONESIA @"360"
#define COUNTRYCODE_THAILAND @"764"
#define COUNTRYCODE_PHILLIPPINES @"608"
#define COUNTRYCODE_HONGKONG @"344"
#define COUNTRYCODE_UNITEDKINGDOM @"826"
#define COUNTRYCODE_RUSSIA @"643"
#define COUNTRYCODE_VIETNAM @"704"
#define COUNTRYCODE_INDIA @"356"
#define COUNTRYCODE_SAUDI @"682"
#define COUNTRYCODE_TURKEY @"792"

#define ISOCOUNTRYCODES_DICT  [NSDictionary dictionaryWithObjectsAndKeys:\
                                                            @"458" , @"MY" , \
                                                            @"360" , @"ID" , \
                                                            @"764" , @"TH" , \
                                                            @"608" , @"PH" , \
                                                            @"344" , @"HK" , \
                                                            @"826" , @"GB" , \
                                                            @"643" , @"RU" , \
                                                            @"704" , @"VN" , \
                                                            @"356" , @"IN" , \
                                                            @"682" , @"SA" , \
                                                            @"792" , @"TR" , \
                                                        nil]


#define LANGUAGE_KEY @"SHELL_LANGUAGE_TABLE"
#define GET_LANGUAGE_TABLE                  [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults]objectForKey:LANGUAGE_KEY]]
#define SET_LANGUAGE_TABLE(languageData)    [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject: languageData] forKey: LANGUAGE_KEY]


#define SET_PUSHTOKEN(pushtoken) [[NSUserDefaults standardUserDefaults]setObject:pushtoken forKey:@"PUSH_NOTIFICATION_TOKEN"]
#define GET_PUSHTOKEN [[NSUserDefaults standardUserDefaults]objectForKey:@"PUSH_NOTIFICATION_TOKEN"]

#define SET_LOCALIZATION(language) [[NSUserDefaults standardUserDefaults]setObject:language forKey:kSaveLanguageDefaultKey]
#define GET_LOCALIZATION [[NSUserDefaults standardUserDefaults]objectForKey:kSaveLanguageDefaultKey]
#define IS_THAI_LANGUAGE [GET_LOCALIZATION isEqualToString: kThai]
#define IS_VIET_LANGUAGE [GET_LOCALIZATION isEqualToString: kVietnamese]


#define SET_PUSHNOTIFICATIONENABLED(choice) [[NSUserDefaults standardUserDefaults]setBool:choice forKey:PUSHNOTIFICATION_ENABLED]
#define GET_PUSHNOTIFICATIONENABLED [[NSUserDefaults standardUserDefaults]boolForKey:PUSHNOTIFICATION_ENABLED]

#define GET_AUTHENTICATIONTOKEN [[NSUserDefaults standardUserDefaults]objectForKey:SESSION_TOKEN]
#define SET_AUTHENTICATIONTOKEN(authtoken) [[NSUserDefaults standardUserDefaults]setObject:authtoken forKey:SESSION_TOKEN]

#define PROFILETYPE_KEY @"PROFILETYPE_KEY"
#define GET_PROFILETYPE [[[NSUserDefaults standardUserDefaults]objectForKey: PROFILETYPE_KEY] intValue]
#define SET_PROFILETYPE(profileType) [[NSUserDefaults standardUserDefaults]setObject:profileType forKey:PROFILETYPE_KEY]

#define ISIMPROFILETYPE_KEY @"ISIMPROFILETYPE_KEY"
#define SET_ISIMPROFILETYPE(isIMProfileType) [[NSUserDefaults standardUserDefaults]setBool:isIMProfileType forKey:ISIMPROFILETYPE_KEY]
#define GET_ISIMPROFILETYPE [[NSUserDefaults standardUserDefaults]boolForKey:ISIMPROFILETYPE_KEY]

#define ISADVANCE_KEY @"ISADVANCE_KEY"
#define SET_ISADVANCE(isAdvance) [[NSUserDefaults standardUserDefaults]setBool:isAdvance forKey:ISADVANCE_KEY]
#define GET_ISADVANCE [[NSUserDefaults standardUserDefaults]boolForKey:ISADVANCE_KEY]

#define ISLOCATIONDETERMINED @"ISLOCATIONDETERMINED"
#define SET_ISLOCATIONDETERMINED(isLocationDetermined) [[NSUserDefaults standardUserDefaults]setBool:isLocationDetermined forKey:ISLOCATIONDETERMINED]
#define GET_ISLOCATIONDETERMINED [[NSUserDefaults standardUserDefaults]boolForKey:ISLOCATIONDETERMINED]

//#define COMPANYTYPE_KEY @"COMPANYTYPE_KEY"
//#define GET_COMPANYTYPE [[[NSUserDefaults standardUserDefaults]objectForKey: COMPANYTYPE_KEY] intValue]
//#define SET_COMPANYTYPE(companyType) [[NSUserDefaults standardUserDefaults]setObject:companyType forKey:COMPANYTYPE_KEY]

#define HOMEDATA_KEY @"HOMEDATA_KEY"
//#define GET_HOMEDATA [[NSUserDefaults standardUserDefaults]objectForKey: HOMEDATA_KEY]
//#define SET_HOMEDATA(homeData) [[NSUserDefaults standardUserDefaults]setObject: homeData forKey: HOMEDATA_KEY]
//#define CLEAR_HOMEDATA [[NSUserDefaults standardUserDefaults] removeObjectForKey: HOMEDATA_KEY]
#define GET_HOMEDATA                [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults]objectForKey:HOMEDATA_KEY]]
#define SET_HOMEDATA(homeData)    [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject: homeData] forKey: HOMEDATA_KEY]
#define CLEAR_HOMEDATA [[NSUserDefaults standardUserDefaults] removeObjectForKey: HOMEDATA_KEY]


#define VALUECALC_KEY @"VALUECALC_KEY"
#define GET_VALUECALC_DATA               [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults]objectForKey: VALUECALC_KEY]]
#define SET_VALUECALC_DATA(valueCalcData)    [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject: valueCalcData] forKey: VALUECALC_KEY]

//scanned codes array, packaged to be submitted
#define SCANNEDCODES_KEY @"SCANNEDCODES_KEY"
#define GET_SCANNEDCODES [[NSUserDefaults standardUserDefaults]objectForKey: SCANNEDCODES_KEY]
#define SET_SCANNEDCODES(scannedCodes) [[NSUserDefaults standardUserDefaults]setObject:scannedCodes forKey:SCANNEDCODES_KEY]
#define CLEAR_SCANNEDCODES [[NSUserDefaults standardUserDefaults] removeObjectForKey: SCANNEDCODES_KEY]

//scanned codes array, packaged to be submitted for IN retailer
#define SCANNEDCODES_IN_KEY @"SCANNEDCODES_IN_KEY"
#define GET_SCANNEDCODES_IN [[NSUserDefaults standardUserDefaults]objectForKey: SCANNEDCODES_IN_KEY]
#define SET_SCANNEDCODES_IN(scannedCodes) [[NSUserDefaults standardUserDefaults]setObject:scannedCodes forKey:SCANNEDCODES_IN_KEY]
#define CLEAR_SCANNEDCODES_IN [[NSUserDefaults standardUserDefaults] removeObjectForKey: SCANNEDCODES_IN_KEY]

//rewards cart table
#define REWARDSCART_KEY @"REWARDSCART_KEY"
#define GET_REWARDSCART [[NSUserDefaults standardUserDefaults]objectForKey: REWARDSCART_KEY]
#define SET_REWARDSCART(rewardsCart) [[NSUserDefaults standardUserDefaults]setObject:rewardsCart forKey:REWARDSCART_KEY]
#define CLEAR_REWARDSCART [[NSUserDefaults standardUserDefaults] removeObjectForKey: REWARDSCART_KEY]


//state table, refreshed every loadState API call
#define LOADSTATE_KEY @"LOADSTATE_KEY"
#define GET_LOADSTATE [[NSUserDefaults standardUserDefaults]objectForKey: LOADSTATE_KEY]
#define SET_LOADSTATE(loadState) [[NSUserDefaults standardUserDefaults]setObject:loadState forKey:LOADSTATE_KEY]
#define CLEAR_LOADSTATE [[NSUserDefaults standardUserDefaults] removeObjectForKey: LOADSTATE_KEY]


#define KEEPLOGIN_KEY @"keepLogin"
#define SET_KEEPLOGIN(keepLogin) [[NSUserDefaults standardUserDefaults]setObject: keepLogin forKey:KEEPLOGIN_KEY]
#define GET_KEEPLOGIN [[NSUserDefaults standardUserDefaults]boolForKey: KEEPLOGIN_KEY]

#define SET_LOGIN_NAME(loginname)  [[NSUserDefaults standardUserDefaults]setObject:loginname forKey:USER_NAME]
#define GET_LOGIN_NAME  [[NSUserDefaults standardUserDefaults]objectForKey: USER_NAME]

#define SET_MEMBER_ID(memberid)  [[NSUserDefaults standardUserDefaults]setObject:memberid forKey:MEMBERID_KEY]

#define PUSH_COUNTER_KEY @"Push_Counter"
#define SET_PUSH_COUNT(pushcounter)  [[NSUserDefaults standardUserDefaults]setInteger:pushcounter forKey:PUSH_COUNTER_KEY]
#define GET_PUSH_COUNT  [[NSUserDefaults standardUserDefaults]integerForKey: PUSH_COUNTER_KEY]


#define ORDER_KEY @"ORDERKEY"
#define GET_ORDERCART                [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults]objectForKey:ORDER_KEY]]
#define SET_ORDERCART(orderData)    [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject: orderData] forKey: ORDER_KEY]

#define CONFIRMKEY @"CONFIRMKEY"
#define GET_CONFIRMORDERKEYS                [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults]objectForKey:CONFIRMKEY]]
#define SET_CONFIRMORDERKEYS(keys)    [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject: keys] forKey: CONFIRMKEY]

#define LOYALTYCONFIRMKEY @"LOYALTYCONFIRMKEY"
#define GET_LOYALTYCONFIRMORDERKEYS                [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults]objectForKey:LOYALTYCONFIRMKEY]]
#define SET_LOYALTYCONFIRMORDERKEYS(keys)    [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject: keys] forKey: LOYALTYCONFIRMKEY]

#define LOYALTYTYPE @"LOYALTYTYPE"
#define GET_LOYALTYTYPE               [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults]objectForKey:LOYALTYTYPE]]
#define SET_LOYALTYTYPE(keys)    [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject: keys] forKey: LOYALTYTYPE]

#define LOYALTY_PROGRAMBREAKDOWN_KEY @"LOYALTY_PROGRAMBREAKDOWN"
#define GET_PROGRAMBREAKDOWN               [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults]objectForKey:LOYALTY_PROGRAMBREAKDOWN_KEY]]
#define SET_PROGRAMBREAKDOWN(keys)    [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject: keys] forKey: LOYALTY_PROGRAMBREAKDOWN_KEY]
#define CLEAR_PROGRAMBREAKDOWN [[NSUserDefaults standardUserDefaults] removeObjectForKey: LOYALTY_PROGRAMBREAKDOWN_KEY]

#define REWARDS_CANREDEEM @"REWARDS_CANREDEEM"
#define GET_REWARDS_CANREDEEM                    [[NSUserDefaults standardUserDefaults]boolForKey: REWARDS_CANREDEEM]
#define SET_REWARDS_CANREDEEM(canRedeem)         [[NSUserDefaults standardUserDefaults]setBool:canRedeem forKey:REWARDS_CANREDEEM]


#define LUBEMATCH_RECOMMENDED @"LUBEMATCH_RECOMMENDED"
#define GET_LUBEMATCH_RECOMMENDED                 [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults]objectForKey:LUBEMATCH_RECOMMENDED]]
#define SET_LUBEMATCH_RECOMMENDED(keys)        [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject: keys] forKey: LUBEMATCH_RECOMMENDED]

//For coachmark
#define COACHMARK_HASSHOWN @"COACHMARK_HASSHOWN"
#define GET_COACHMARK_HASSHOWN               [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults]objectForKey: COACHMARK_HASSHOWN]]
#define SET_COACHMARK_HASSHOWN(data)    [[NSUserDefaults standardUserDefaults]setObject:[NSKeyedArchiver archivedDataWithRootObject: data] forKey: COACHMARK_HASSHOWN]
#define CLEAR_COACHMARK_HASSHOWN  [[NSUserDefaults standardUserDefaults] removeObjectForKey: COACHMARK_HASSHOWN]

#define HOME_COACHMARK @"HOME_COACHMARK"
#define COIN_COACHMARK @"COIN_COACHMARK"
#define LUBEMATCH_COACHMARK                 @"LUBEMATCH_COACHMARK"
#define LUBEMATCH_FIRSTSEARCH_COACHMARK     @"LUBEMATCH_FIRSTSEARCH_COACHMARK"
#define CHECKIN_COACHMARK                   @"CHECKIN_COACHMARK"
#define MY_SCANFORREWARDS_COACHMARK         @"MY_SCANFORREWARDS_COACHMARK"

//For Storing version number
#define VERSIONNUMBER_LATEST @"VERSIONNUMBER_LATEST"
#define SET_VERSIONNUMBER_LATEST(version)  [[NSUserDefaults standardUserDefaults]setObject:version forKey:VERSIONNUMBER_LATEST]
#define GET_VERSIONNUMBER_LATEST  [[NSUserDefaults standardUserDefaults]objectForKey: VERSIONNUMBER_LATEST]

//For Date latest
#define DATESTORED_LATEST @"DATESTORED_LATEST"
#define SET_DATESTORED_LATEST(date)  [[NSUserDefaults standardUserDefaults] setObject:date forKey:DATESTORED_LATEST]
#define GET_DATESTORED_LATEST  [[NSUserDefaults standardUserDefaults]objectForKey: DATESTORED_LATEST]

#define LAUNCH_SDASWA                       @"LAUNCH_SDASWA"
#define GET_LAUNCH_SDASWA                   [[NSUserDefaults standardUserDefaults]boolForKey: LAUNCH_SDASWA]
#define SET_LAUNCH_SDASWA(shouldLaunch)     [[NSUserDefaults standardUserDefaults]setBool:shouldLaunch forKey:LAUNCH_SDASWA]


//#define GET_COIN_COACHMARK_SHOW                   [[NSUserDefaults standardUserDefaults]boolForKey: COIN_COACHMARK_SHOW]
//#define SET_COIN_COACHMARK_SHOW(canShow)         [[NSUserDefaults standardUserDefaults]setBool:canShow forKey:COIN_COACHMARK_SHOW]
#define INVENTORY_SELECTEDWORKSHOP      @"INVENTORY_SELECTEDWORKSHOP"
#define SET_INVENTORY_SELECTEDWORKSHOP(workshopName)  [[NSUserDefaults standardUserDefaults] setValue:workshopName forKey:INVENTORY_SELECTEDWORKSHOP]
#define GET_INVENTORY_SELECTEDWORKSHOP  [[NSUserDefaults standardUserDefaults]stringForKey: INVENTORY_SELECTEDWORKSHOP]

#define INVENTORY_SELECTEDWORKSHOP_TRADEID      @"INVENTORY_SELECTEDWORKSHOP_TRADEID"
#define SET_INVENTORY_SELECTEDWORKSHOP_TRADEID(tradeID)  [[NSUserDefaults standardUserDefaults] setValue:tradeID forKey:INVENTORY_SELECTEDWORKSHOP_TRADEID]
#define GET_INVENTORY_SELECTEDWORKSHOP_TRADEID  [[NSUserDefaults standardUserDefaults]stringForKey: INVENTORY_SELECTEDWORKSHOP_TRADEID]

#define SCANNED_INVENTORY_KEY @"SCANNED_INVENTORY_KEY"
#define GET_SCANNED_INVENTORY [[NSUserDefaults standardUserDefaults]objectForKey: SCANNED_INVENTORY_KEY]
#define SET_SCANNED_INVENTORY(scannedCodes) [[NSUserDefaults standardUserDefaults]setObject:scannedCodes forKey:SCANNED_INVENTORY_KEY]


//APP INFO
static NSString *kAppVersion = @"AppVersion";
static NSString *kAppBuild = @"AppBuild";
static NSString *kAppName = @"AppName";
#define REMEMBER_ME_ENABLED [[NSUserDefaults standardUserDefaults] boolForKey:REMEMBER_ME]

#define VERSION_NUMBER [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]

//Webservice Response Schema
static NSString *RESPONSE_CODE_NODE = @"ResponseCode";
static NSString *RESPONSE_MESSAGE_NODE = @"ResponseMessage";
static NSString *kAuthToken = @"AuthToken";
static NSString *kMemberID = @"MemberID";
static NSString *kError = @"Error";
static NSString *kData = @"Data";

//DEVICE WEBSERVICE KEYS
static NSString *kDeviceUUID = @"DeviceToken";
static NSString *kPushToken = @"PushToken";
static NSString *kPushStatus = @"PushStatus";
static NSString *kDeviceType = @"DeviceType";
static NSString *kDevicePlatform = @"DevicePlatform";
static NSString *devicePlatform = @"iOS";
static NSString *kDeviceInformation = @"DeviceInformation";
static NSString *kTestOutletCode = @"TEST-ER-001";

//USERINFO WEBSERVICE KEYS
static NSString *kLanguageCode = @"LanguageCode";
static NSString *kCountryCode = @"CountryCode";
static NSString *kLoginName = @"LoginName";
static NSString *kPassword = @"Password";
static NSString *kChangeOldPassword = @"OldPassword";
static NSString *kChangeNewPassword = @"NewPassword";
static NSString *kEmailAddress = @"EmailAddress";
static NSString *kOutletCode = @"OutletCode";
static NSString *kFirstName = @"FirstName";
static NSString *kLastName = @"LastName";
static NSString *kCompanyName = @"CompanyName";
static NSString *kPartnerName = @"PartnerName";
static NSString *kMobileNumber = @"MobileNumber";

//static NSString *kSalutation = @"Salutation";
//static NSString *kGPSLocation = @"GPSLocation";
//static NSString *kPostalCode = @"PostalCode";
//static NSString *kState = @"State";
//static NSString *kTown = @"Town";
//static NSString *kAddress1 = @"Address1";
//static NSString *kAddress2 = @"Address2";
//
//static NSString *kWorkshopSource = @"WorkshopSource";
//static NSString *kWorkshopType = @"WorkshopType";
//static NSString *kWorkshopCategory = @"WorkshopCategory";

//REGISTRATION
enum REGISTRATION_TYPE
{
    kREGISTRATION_WORKSHOP,
    kREGISTRATION_MECHANIC,
};

//static NSString *kPANCardId = @"PANCardID";
//static NSString *kPANCardImage = @"PANCardImage";
//static NSString *kBeneficiaryName = @"BeneficiaryName";
//static NSString *kBankName = @"BankName";
//static NSString *kIFSCCode = @"IFSCCode";
//static NSString *kBeneficiaryImage = @"BeneficiaryImage";
//static NSString *kBankCheckBox = @"BankCheckBox";
//static NSString *kTncCheckBox = @"TNCCheckBox";
//static NSString *kRegisterCTA = @"RegisterCTA";

static NSString *kAccountNo = @"Account No";
static NSString *kOutletID = @"Outlet ID";
static NSString *kUserID =  @"User ID";
static NSString *kGivenName = @"Given Name";
static NSString *kSurname = @"Surname";
static NSString *kMobile =  @"Mobile";
static NSString *kEmail = @"Email";
static NSString *kReenterPassword = @"Re-enter Password";


//EXCEPTION ERROR
static NSString *kErrorDesc = @"ErrorDesc";
static NSString *kModule = @"Module";

//SIMULATOR
static NSString *kSimulator = @"Simulator";
#define SIMULATOR_DEVICE_TOKEN @"ECF028C6-BEFB-424B-A4B2-D5A7D7AD6BCC"


static NSString * const kNavigation = @"Navigation";
static NSString * const kUpdatedQR = @"UpdatedQR";

//PLACE HOLDER IMAGES
static NSString *kPlaceholderProfileimage = @"";
static NSString *kPlaceholderimage = @"";
static NSString *kPlaceholderMailImage = @"ic_mailbox";


//APP AUTHENTICATION
#define CONTENT_TYPE_JSON @"application/json"
#define CONTENT_TYPE_FORMDATA @"multipart/form-data"
#define CONTENT_TYPE_KEY @"Content-Type"
#define AUTH_TOKEN_KEY @"AUTH-TOKEN"
#define SECRET_TOKEN_KEY @"ER-TOKEN"
//live
#define LIVE_SECRET_TOKEN_VALUE @"SHARE-395952CD-74CE-415C-B7D1-B1805A79F398"
//UAT/staging
#define STAGING_SECRET_TOKEN_VALUE @"SHARE-297992CD-74CE-415C-B7D1-B1805A79F398"
#define BETA_SECRET_TOKEN_VALUE @"SHARE-395952CD-74CE-415C-B7D1-B1905A79F398"

//#define SECRET_TOKEN_VALUE (IS_STAGING_VERSION || IS_DEVELOPMENT_VERSION) ? STAGING_SECRET_TOKEN_VALUE : LIVE_SECRET_TOKEN_VALUE

#define IS_DEVELOPMENT_VERSION ([VERSION_NUMBER rangeOfString:@"d"].length > 0) ? YES : NO
#define IS_STAGING_VERSION ([VERSION_NUMBER rangeOfString:@"t"].length > 0) ? YES : NO
#define IS_BETA_VERSION ([VERSION_NUMBER isEqualToString:@"tp1.0.14"]) ? YES : NO

#define SECRET_TOKEN_VALUE [WebServiceManager secret_token]


#define APPID_KEY @"APPID"
#define APPID_VALUE @"20170727"
#define KEYCHAIN_SERVICE_NAME @"come.keychain.JTIStarPartners"
#define APP_LANGUAGE_CODE @"LanguageCode" 
#define SIMULATOR_PUSHNOTIFICATION_TOKEN @"123456"
static NSString *kSessionToken = @"SessionToken";

#define TRADEID_KEY @"TradeID"
#define PROGRAMID_KEY @"ProgramID"
#define COUNTRYCODE_KEY @"CountryCode"

//TEXTFIELD VALIDATION RULES
#define REGEX_USER_NAME_LIMIT @"^.{6,30}$"
#define REGEX_USER_NAME @"[A-Za-z0-9]{6,30}"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT @"^.{8,20}$"
#define REGEX_PASSWORD @"[A-Za-z0-9]{8,20}"
#define REGEX_PHONE_DEFAULT @"[0-9]{8}"
#define REGEX_SHOPID_DEFAULT @"[0-9]{8}"
#define REGEX_SHOPID_LIMIT @"^.{8}$"
#define REGEX_PANCARD @"[A-Za-z]{5}+[0-9]{4}+[A-Za-z]{1}"
#define REGEX_DRIVINGLICENSE @"[A-Za-z]{2}+[0-9]{2}+[0-9]{7,13}"
#define REGEX_VOTERID @"[A-Za-z]{3}+[0-9]{7}"

//HUD
#define DEFAULT_HUD LOCALIZATION(C_GLOBAL_DEFAULT_HUD)
#define SIGNIN_HUD LOCALIZATION(C_GLOBAL_SIGNIN)
#define REGISTRATION_HUD LOCALIZATION(C_GLOBAL_SUBMIT)
#define UPDATING_HUD LOCALIZATION(C_GLOBAL_UPDATE)
#define SIGNOUT_HUD LOCALIZATION(C_GLOBAL_SIGNOUT)
#define AUTHENTICATION_HUD @"Authenticating.."
#define LOADING_HUD LOCALIZATION(C_GLOBAL_LOADING)
#define SENDING_HUD @"Sending.."
#define SPLASHSCREEN_HUD @"Splash"
#define MYPOINTS_HUD @"MyPoints"
//userinfo

static NSString *kLoginInfo = @"LoginInfo";
static NSString *kPushNotificationInfo = @"PushNotificationInfo";

#define PUSH_TOKEN @"pushnotificationToken"
#define SESSION_TOKEN @"sessiontoken"
#define USER_PASSWORD @"password"
#define USER_NAME @"loginname"
#define ADMIN_NAME @"AdminName"
#define REMEMBER_ME @"loginremember"
#define MEMBERID_KEY  @"MemberID"
#define LOGIN_NAME  [[NSUserDefaults standardUserDefaults]objectForKey:USER_NAME]
#define MEMBER_ID  [[NSUserDefaults standardUserDefaults]objectForKey:MEMBERID_KEY]
#define USER_TOKEN [[NSUserDefaults standardUserDefaults]objectForKey:SESSION_TOKEN]
#define LOGIN_PASSWORD  [[mSession getSecureStoreManager] getValueForKey:USER_PASSWORD]

#define OS_DEVICE 1 //iOS
#define SMART_PHONE 1 //SmartPhone
#define TABLET 1 //Tablet

//NSUSERDEFAULTS
#define PUSHNOTIFICATION_ENABLED @"pushNotificationEnabled"
#define USERDEFAULTS_PASSWORD @"password"
#define USERDEFAULTS_LOGINID @"loginid"
#define USERDEFAULTS_LANGUAGETYPE @"languageType"
#define USERDEFAULTS_SESSIONID @"sessionid"
#define USERDEFAULTS_PUSHNOTIFICATION_TOKEN @"pushNotification"
#define USER_PROFILE_ARCHIVE_NAME @"UserInfo"
#define HOME_FOOTER_ARCHIVE_NAME @"HomeFooter"

//SQL
#define kDatabaseName @"localDB.db"

//DocumentsPath
#define DOC_PATH [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]

//ALERTVIEW
#define  Alert(TITLE,MSG) [[[UIAlertView alloc] initWithTitle:(TITLE) \
message:(MSG) \
delegate:nil \
cancelButtonTitle:@"OK" \
otherButtonTitles:nil] show]

//GENERAL
//#define SYSTEMLANGUAGE [[NSLocale preferredLanguages] objectAtIndex:0]
#define SYSTEMLANGUAGE [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] //update in iOS 9
#define EQUALS(text1,text2) [text1 isEqualToString:text2]
#define ISNULL(obj) obj == [NSNull null]
#define ISNULLCLASS(obj) [obj isKindOfClass: [NSNull class]]
#define BLANK @""

#define NSLOGFRAME(view) NSLog(@"FRAME -- %f,%f,%f,%f",view.frame.origin.x,view.frame.origin.y,view.frame.size.width,view.frame.size.height);
#define NSLOG(id) NSLog(@"%@",id);

//BORDER
#define  BORDER(view,color,width) [view addAllBorderWithColor:color andWidth:width]

//ROUNDCORNER
#define  MASKCORNER(view,rad) [view maskRoundCorners:UIRectCornerAllCorners radius:rad]

#define PLACEHOLDERIMG(image) [UIImage imageNamed:image]
#define SETIMAGE(image) [UIImage imageNamed:image]


//macro for colours
#define COLOUR_MAXBYTE 255.0

//Primary Colours
#define COLOUR_DARKBLUE     [UIColor colorWithRed:0/COLOUR_MAXBYTE green:60/COLOUR_MAXBYTE blue:136/COLOUR_MAXBYTE alpha: 1.0f]
#define COLOUR_SHELLRED     [UIColor colorWithRed:221/COLOUR_MAXBYTE green:29/COLOUR_MAXBYTE blue:33/COLOUR_MAXBYTE alpha: 1.0f]

#define COLOUR_YELLOW       [UIColor colorWithRed:251/COLOUR_MAXBYTE green:206/COLOUR_MAXBYTE blue:7/COLOUR_MAXBYTE alpha: 1.0f]
#define COLOUR_RED          GET_ISADVANCE ? COLOUR_DARKBLUE : COLOUR_SHELLRED
#define COLOUR_WHITE        [UIColor colorWithRed:255/COLOUR_MAXBYTE green:255/COLOUR_MAXBYTE blue:255/COLOUR_MAXBYTE alpha: 1.0f]

//Supporting Colours
#define COLOUR_LIGHTBLUE    [UIColor colorWithRed:137/COLOUR_MAXBYTE green:207/COLOUR_MAXBYTE blue:220/COLOUR_MAXBYTE alpha: 1.0f]
#define COLOUR_LIGHTGREEN   [UIColor colorWithRed:190/COLOUR_MAXBYTE green:213/COLOUR_MAXBYTE blue:15/COLOUR_MAXBYTE alpha: 1.0f]

#define COLOUR_DARKGREEN    [UIColor colorWithRed:0/COLOUR_MAXBYTE green:132/COLOUR_MAXBYTE blue:67/COLOUR_MAXBYTE alpha: 1.0f]
#define COLOUR_PURPLE       [UIColor colorWithRed:100/COLOUR_MAXBYTE green:25/COLOUR_MAXBYTE blue:100/COLOUR_MAXBYTE alpha: 1.0f]
#define COLOUR_BROWN        [UIColor colorWithRed:116/COLOUR_MAXBYTE green:52/COLOUR_MAXBYTE blue:16/COLOUR_MAXBYTE alpha: 1.0f]

//Neutral Colours
#define COLOUR_VERYDARKGREY [UIColor colorWithRed:64/COLOUR_MAXBYTE green:64/COLOUR_MAXBYTE blue:64/COLOUR_MAXBYTE alpha: 1.0f]
#define COLOUR_DARKGREY     [UIColor colorWithRed:89/COLOUR_MAXBYTE green:89/COLOUR_MAXBYTE blue:89/COLOUR_MAXBYTE alpha: 1.0f]
#define COLOUR_MIDGREY      [UIColor colorWithRed:127/COLOUR_MAXBYTE green:127/COLOUR_MAXBYTE blue:127/COLOUR_MAXBYTE alpha: 1.0f]

#define COLOUR_LIGHTGREY    [UIColor colorWithRed:166/COLOUR_MAXBYTE green:166/COLOUR_MAXBYTE blue:166/COLOUR_MAXBYTE alpha: 1.0f]
#define COLOUR_PALEGREY    [UIColor colorWithRed:217/COLOUR_MAXBYTE green:217/COLOUR_MAXBYTE blue:217/COLOUR_MAXBYTE alpha: 1.0f]
#define COLOUR_VERYPALEGREY [UIColor colorWithRed:242/COLOUR_MAXBYTE green:242/COLOUR_MAXBYTE blue:242/COLOUR_MAXBYTE alpha: 1.0f]
#define COLOUR_BACKGROUNDGREY [UIColor colorWithRed:247/COLOUR_MAXBYTE green:247/COLOUR_MAXBYTE blue:247/COLOUR_MAXBYTE alpha: 1.0f]


//FONT
/*
 FuturaStd-Bold,
 FuturaStd-Book,
 FuturaStd-Heavy,
 FuturaStd-ExtraBold,
 FuturaStd-Medium,
 FuturaStd-Light
 
 Kittithada Bold 75.otf
 Kittithada Light 45.otf
 Kittithada Medium 65.otf
 Kittithada Roman 55.otf.
 Kittithada Thin 35.otf
 
 *** Arial ***
 Arial-BoldItalicMT
 Arial-BoldMT
 Arial-ItalicMT
 ArialMT
 */


#define FONT_BUTTON     FONT_H1 //standard font for  buttons


#define THAI_FONTOFFSET (IS_THAI_LANGUAGE ? 5 : 0)

//BOLD Body, FONT_H(x)
//#define FONT_BOLD IS_THAI_LANGUAGE ? @"Kittithada Bold 75" :\
//                    IS_VIET_LANGUAGE ? @"Arial-BoldMT" :\
//                    @"FuturaStd-Bold"

//Normal Body, FONT_B(x)
//#define FONT_BODY IS_THAI_LANGUAGE ? @"Kittithada Roman 55" :\
//                    IS_VIET_LANGUAGE ? @"ArialMT" :\
//                    @"FuturaStd-Book"
#define FONT_BODY [Helper getFontBodyString]
#define FONT_BOLD [Helper getFontBoldString]

#define FONT_H1 [UIFont fontWithName: FONT_BOLD size: 15 + THAI_FONTOFFSET]
#define FONT_H2 [UIFont fontWithName: FONT_BOLD size: 13 + THAI_FONTOFFSET]
#define FONT_H3 [UIFont fontWithName: FONT_BOLD size: 10 + THAI_FONTOFFSET]
#define FONT_B1    [UIFont fontWithName: FONT_BODY size: 15 + THAI_FONTOFFSET]
#define FONT_B2    [UIFont fontWithName: FONT_BODY size: 13 + THAI_FONTOFFSET]
#define FONT_B3    [UIFont fontWithName: FONT_BODY size: 10 + THAI_FONTOFFSET]

#define FONT_H(x) [UIFont fontWithName: FONT_BOLD size: x + THAI_FONTOFFSET]
#define FONT_B(x)    [UIFont fontWithName: FONT_BODY size: x + THAI_FONTOFFSET]


//#define FONT_H(x) [UIFont fontWithName: ((IS_THAI_LANGUAGE) ? @"Kittithada Bold 75" : @"FuturaStd-Bold") size: (IS_THAI_LANGUAGE) ?  x + THAI_FONTOFFSET : x]
//#define FONT_B(x)  [UIFont fontWithName: ((IS_THAI_LANGUAGE) ? @"Kittithada Medium 65" : @"FuturaStd-Medium") size: (IS_THAI_LANGUAGE) ?  x + THAI_FONTOFFSET : x]
//#define FONT_BOOK(x)    [UIFont fontWithName: ((IS_THAI_LANGUAGE) ? @"Kittithada Roman 55" : @"FuturaStd-Book") size: (IS_THAI_LANGUAGE) ?  x + THAI_FONTOFFSET : x]
//#define FONT_LIGHT(x)   [UIFont fontWithName: ((IS_THAI_LANGUAGE) ? @"Kittithada Light 45" : @"FuturaStd-Light") size: (IS_THAI_LANGUAGE) ?  x + THAI_FONTOFFSET : x]
//#define FONT_HEADER(x)  [UIFont fontWithName: ((IS_THAI_LANGUAGE) ? @"Kittithada Bold 75" : @"FuturaStd-Bold") size: (IS_THAI_LANGUAGE) ?  x + THAI_FONTOFFSET : x] //CAPS
//#define FONT_CONTENTTITLE(x) [UIFont fontWithName:@"HelveticaNeue-Medium" size:x]

//#define FONT_CONTENTTITLE(x, fontName) [UIFont fontWithName: fontName size:x]


////TO CHANGE LATER
#define FONTBOLD(x) [UIFont boldSystemFontOfSize:x]
#define FONTREGULAR(x) [UIFont systemFontOfSize:x]

//SCREEN CONTROLS
#define HAMBURGERBTN_COLOR COLOUR_MIDGREY
#define SCREEN_HEIGHT  [[UIScreen mainScreen] bounds].size.height
#define SCREEN_WIDTH  [[UIScreen mainScreen] bounds].size.width
#define KEY_WINDOW [[UIApplication sharedApplication] keyWindow]

//CURRENT VIEW CONTROLS
#define CURRENT_VIEWCONTROLLER [[UIApplication sharedApplication] keyWindow].rootViewController
#define CURRENT_VIEW   [[[[UIApplication sharedApplication] keyWindow] subviews] lastObject]

//STORYBOARD
#define kSTORYBOARD(name) [UIStoryboard storyboardWithName:name bundle: nil]
#define STORYBOARD_MAIN             kSTORYBOARD(@"Main")
#define STORYBOARD_SCANCODE             kSTORYBOARD(@"ScanCode")
#define STORYBOARD_REGISTRATION     kSTORYBOARD(@"Registration")
#define STORYBOARD_MANAGEWORKSHOP   kSTORYBOARD(@"ManageWorkshop")
#define STORYBOARD_SPIRAXANDGADUS   kSTORYBOARD(@"SpiraxandGadus")
#define STORYBOARD_PASSWORD         kSTORYBOARD(@"Password")
#define STORYBOARD_PROFILE          kSTORYBOARD(@"Profile")
#define STORYBOARD_PERFORMANCE      kSTORYBOARD(@"Performance")
#define STORYBOARD_MANAGEMECHANIC   kSTORYBOARD(@"ManageMechanics")
#define STORYBOARD_PROMOCODE        kSTORYBOARD(@"PromoCode")
#define STORYBOARD_WEBVIEW          kSTORYBOARD(@"WebView")
#define STORYBOARD_MANAGEORDERS     kSTORYBOARD(@"ManageOrders")
#define STORYBOARD_REWARDS          kSTORYBOARD(@"Rewards")
#define STORYBOARD_TOLOYALTY        kSTORYBOARD(@"TOLoyaltyPoints")
#define STORYBOARD_WORKSHOPOFFER    kSTORYBOARD(@"WorkshopOffer")
#define STORYBOARD_INAPPASSET       kSTORYBOARD(@"InAppAsset")
#define STORYBOARD_POPUPS           kSTORYBOARD(@"Popups")
#define STORYBOARD_WORKSHOPPROFILING    kSTORYBOARD(@"WorkshopProfiling")
#define STORYBOARD_REGISTERCUSTOMER               kSTORYBOARD(@"RegisterCustomer")
#define STORYBOARD_MAINV2           kSTORYBOARD(@"MainV2")
#define STORYBOARD_PROMOTIONS       kSTORYBOARD(@"Promotions")
#define STORYBOARD_AI               kSTORYBOARD(@"AI")
#define STORYBOARD_REGISTEROTP      kSTORYBOARD(@"RegisterPanOTP")
#define STORYBOARD_RUSSIA               kSTORYBOARD(@"Russia")
#define STORYBOARD_RUSSIA_V2            kSTORYBOARD(@"RussiaV2")
#define STORYBOARD_RU_WORKSHOPOFFER    kSTORYBOARD(@"RUWorkshopOffer")
#define STORYBOARD_THRetail              kSTORYBOARD(@"THRetail")
#define STORYBOARD_RU_WALKTHROUGH    kSTORYBOARD(@"Walkthrough")
#define STORYBOARD_RU_WORKSHOPPERFORMANCE    kSTORYBOARD(@"RUWorkshopPerformance")
#define STORYBOARD_RU_DSR_WORKSHOPPERFORMANCE kSTORYBOARD(@"RUDSRWorkshopPerformance")

//Ebook

#define STORYBOARD_IN_ALLBOOKLIST kSTORYBOARD(@"MainStoryBoard")

//IN Performance
#define STORYBOARD_IN_PERFORMANCE kSTORYBOARD(@"INPerformance")

#define STORYBBOARD_IN kSTORYBOARD(@"India")
//Forms
#define STORYBOARD_FORM_CONTROLLERS kSTORYBOARD(@"FormControllers")

//Loyalty Storyboard
#define STORYBOARD_LOYALTYPROGRAM kSTORYBOARD(@"LoyaltyProgramStoryboard")
#define STORYBOARD_REGISTEROILCHANGE    kSTORYBOARD(@"RegisterOilChange")

//STORYBOARD ID
#define VIEW_SPLASH                 @"SplashView"
#define VIEW_LOGIN                  @"LoginView"
#define VIEW_FORGETPASSWORD         @"ForgetPasswordView"
#define VIEW_CHANGEPASSWORD         @"ChangePasswordView"
#define VIEW_MOBILEAUTH             @"MobileNumberAuthView"
#define VIEW_OTP                    @"OTPView"
#define VIEW_GAllERY                @"GalleryUpload"

#define VIEW_TRADEREGISTRATION      @"TradeRegistrationView"
#define VIEW_TRADEREGISTRATIONTWO   @"TradeRegistrationTwoView"
#define VIEW_WORKINGHOURS           @"WorkingHoursView"
#define VIEW_MECHANICREGISTRATION   @"MechanicRegistrationView"
#define VIEW_SCANPRODUCTCODE        @"ScanProductCodeView"
#define VIEW_PENDINGCODE            @"PendingCodeView"
#define VIEW_SEARCHVEHICLE          @"SearchVehicleView"

#define VIEW_DECALMAIN              @"ScanCode_DecalMainView"
#define VIEW_DECALRESULT            @"ScanCode_DecalResultView"

#define VIEW_INVITETRADE            @"InviteTradeView"
#define VIEW_INVITECUSTOMER            @"InviteCustomerView"

#define VIEW_HOME                   @"HomeView"

#define VIEW_SPIRAX                 @"SpiraxManageWorkshopView"
#define VIEW_SPIRAXWORKSHOPLIST           @"SpiraxWorkshopListView"
#define VIEW_SPIRAXWORKSHOPADDNEW         @"SpiraxWorkshopAddNewView"


#define VIEW_MANAGE                 @"ManageWorkshopView"
#define VIEW_WORKSHOPLIST           @"WorkshopListView"
#define VIEW_WORKSHOPADDNEW         @"WorkshopAddNewView"

#define VIEW_ADDNEWWORKSHOP_NEW         @"AddNewWorkshop_NewViewController"

#define VIEW_MANAGEMECHANIC             @"ManageMechanicView"
#define VIEW_MECHANICLIST               @"MechanicListView"
#define VIEW_MECHANICADDNEW             @"MechanicAddNewView"
#define VIEW_MECHANICDETAILS            @"MechanicDetailsView"
#define VIEW_MECHANICDETAILS_NEW            @"MechanicDetails_NewViewController"
#define VIEW_ADDSTAFF                   @"AddNewStaffView"
#define VIEW_STAFFDETAILS               @"ManageStaffDetailView"

#define VIEW_ADDNEWMECHANIC_NEW             @"AddNewMechanic_NewViewController"

#define VIEW_NOTIFICATIONDETAIL         @"NotificationDetail"
#define VIEW_NOTIFICATIONDELETEVIEW     @"NotificationDeleteView"
#define VIEW_INAPPBROWSER               @"InAppBrowserViewController"
#define VIEW_REGISTERCODERESULT         @"RegisterCodeResult"

#define VIEW_INWORKSHOPADDNEW           @"INWorkshopAddNew"
#define VIEW_WORKSHOPDETAILS_TAB        @"WorkshopDetailsTabView"
#define VIEW_WORKSHOPDETAILS            @"WorkshopDetailsView"
#define VIEW_WORKSHOPAPPROVEDDETAILS    @"WorkshopApprovedDetailsView"
#define VIEW_WORKSHOPPERFORMANCE        @"WorkshopPerformanceView"
#define VIEW_WORKSHOPPROFILING          @"WorkshopProfilingView"
#define VIEW_PENDINGWORKSHOPDETAILS_NEW            @"PendingWorkshopDetails_NewViewController"
#define VIEW_APPROVEDWORKSHOPDETAILS_NEW            @"ApprovedWorkshopDetails_NewViewController"
#define VIEW_INMECHANIC_ADDNEW @"INMechanicAddNewViewController"


#define VIEW_RU_WORKSHOP_REGISTRATION            @"RUWorkshopRegistrationViewController"

#define VIEW_RU_SPIRAX_REGISTRATION            @"RUSpiraxRegistrationViewController"



#define VIEW_PROFILE_TAB            @"ProfileTabView"
#define VIEW_PROFILE_TAB_NEW        @"ProfileTab_NewViewController"
#define VIEW_PROFILE                @"ProfileView"
#define VIEW_MYPROFILE              @"MyProfileView"
#define VIEW_MYPROFILE_NEW          @"MyProfile_NewViewController"
#define VIEW_WORKSHOPPROFILE        @"WorkshopProfileView"

#define VIEW_PERFORMANCE_TAB        @"PerformanceTabView"
#define VIEW_MYPERFORMANCE          @"MyPerformanceView"
#define VIEW_PERFORMANCEPOINTS      @"PerformancePointsView"
#define VIEW_PERFORMANCEWORKSHOP    @"PerformanceWorkshopView"
#define VIEW_LEADERBOARD            @"LeaderboardView"
#define VIEW_MYCOIN_PERFORMANCE     @"MyCoinsPerformanceViewController"

#define VIEW_PROMOTION @"PromotionView"

#define VIEW_REGISTEROILCHANGE_SEARCHVIEW       @"RegisterOilChangeSearchViewController"
#define VIEW_REGISTEROILCHANGE_LUBEMATCH_POPUP  @"RegisterOilChangeLubematchViewController"
#define VIEW_REGISTEROILCHANGE_SCANVIEW         @"RegisterOilChangeScanViewController"

#define VIEW_PROMOTIONCODE @"PromoCodeView"
#define VIEW_PROMOTIONHISTORY @"PromoCodeHistoryView"
#define VIEW_PROMOTIONMASTER @"PromoCodeMaster"
#define VIEW_PROMOTIONRESULT @"PromoCodeResultView"

#define VIEW_ORDERSYSTEM @"OrderSystemView"
#define VIEW_PRODUCTLIST @"ProductListView"

#define VIEW_WALKTHROUGH @"WalkthroughBaseViewController"
#define VIEW_PHOTO_TAKING @"PhotoTaking"
#define VIEW_CONTACTUS @"ContactUsView"
#define VIEW_FAQ @"FAQView"
#define VIEW_TNC @"TNCView"
#define VIEW_PRIVACY @"PrivacyView"
#define VIEW_OTHERPRODUCT           @"OtherView"
#define VIEW_VIEWORDER              @"OrderVIew"
#define VIEW_ADDORDER               @"AddView"
#define VIEW_PARTICIPATEPRODUCT     @"ParticipateView"
#define VIEW_MANAGEORDER            @"ManageOrder"
#define VIEW_PARTICIPATINGPRODUCT   @"ParticipatingProduct"
#define VIEW_PENDINGORDER           @"PendingView"
#define VIEW_PRODUCTTABVIEW         @"ProductsTabView"
#define VIEW_PRODUCTTABLEVIEW       @"ProductsTableView"

#define VIEW_REWARDS                @"RewardsView"
#define VIEW_REWARDS_ITEMS          @"RewardsItemDetailsView"
#define VIEW_REWARDS_HISTORY        @"RewardsHistoryView"
#define VIEW_REWARDS_CART           @"RewardsCartView"
#define VIEW_REWARDS_FAVOURITES     @"RewardsFavouritesView"
#define VIEW_REWARDS_ADDRESS        @"RewardsAddressView"
#define VIEW_REWARDS_CONFIRMATION   @"RewardsConfirmationView"
#define VIEW_REWARDS_CATEGORY        @"RewardsCategoryView"
#define VIEW_REWARDS_HOME            @"RewardsHomeView"
#define VIEW_REWARDS_ITEMLIST       @"RewardsItemListView"
#define VIEW_REWARDS_REDEMPTIONDETAILS       @"RewardsRedemptionDetailsView"
#define VIEW_REWARDS_REDEMPTIONCOMPLETE       @"RewardsRedemptionCompleteView"

#define VIEW_TOLOYALTY_SUMMARY      @"TOLoyaltySummaryView"
#define VIEW_TOLOYALTY_DETAILS      @"TOLoyaltyDetailsView"

#define VIEW_WORKSHOPOFFERTAB      @"WorkshopOfferTabView"
#define VIEW_WORKSHOPOFFERADDNEW      @"AddNewWorkshopOfferView"
#define VIEW_WORKSHOPOFFER_CURRENTOFFER      @"CurrentOfferView"
#define VIEW_WORKSHOPOFFER_PASTOFFERSVIEW      @"PastOffersView"
#define VIEW_WORKSHOPOFFER_OFFERDETAILVIEW      @"OfferDetailsView"
#define VIEW_WORKSHOPOFFER_OFFERCONFIRMATIONVIEW      @"OfferConfirmationView"
#define VIEW_WORKSHOPOFFER_CALENDERVIEW      @"CalenderView"
#define VIEW_WORKSHOPOFFER_ALLOFFER      @"WorkshopOfferAllOfferView"
#define VIEW_WORKSHOPOFFER_ALLLISTOFFER      @"WorkshopOfferAllOfferListView"

#define VIEW_ORDERDETAILS @"OrderDetailView"
#define VIEW_CATEGORYVIEW @"OrdersCategoryView"
#define VIEW_MYCARTVIEW @"MyCartView"
#define VIEW_ORDERCONFIRMVIEW @"OrderConfirmView"
#define VIEW_PENDINGORDERSVIEW @"PendingOrdersView"

#define VIEW_INAPPASSETVIEW         @"InAppAssetView"
#define VIEW_INAPPASSETCATEGORYVIEW @"InAppAssetCategoryView"
//#define VIEW_INAPPASSETVIDEOVIEW    @"InAppAssetVideoView"
#define VIEW_INAPPASSETVIDEOVIEW    @"InAppAssetVideoPlayerView"
#define VIEW_INAPPASSETPDFVIEW      @"InAppAssetPDFView"

#define VIEW_POPUPVIEW             @"PopupView"
#define VIEW_POPUPINFOVIEW         @"PopupInfoView"
#define VIEW_POPUPINFOTABLEVIEW         @"PopupInfoTableView"
#define VIEW_POPUPYESNOVIEW             @"PopupYesNoView"
#define VIEW_POPUPTEXTVIEW              @"PopupTextView"
#define VIEW_POPUPTAGDECALVIEW             @"PopupTagDecalView"
#define VIEW_POPUPTAGDECALTOVEHICLEVIEW             @"PopupTagDecalToVehicleView"
#define VIEW_POPUPREGISTERCUSTOMERVIEW             @"PopupRegisterCustomerView"
#define VIEW_POPUPREWARDFILTERVIEW             @"PopupRewardFilterView"
#define VIEW_POPUPPRIVACYPOLICYVIEW             @"PopupPrivacyPolicyViewController"
#define VIEW_POPUPWEBVIEW             @"PopupWebviewViewController"
#define VIEW_POPUPATTRIBUTEDLABEL       @"PopupAttributedLabelViewController"
#define VIEW_POPUPCAMERADECAL      @"PopupCameraDecalViewController"
#define VIEW_POPUPMULTIPLESELECT    @"PopupSelectMultipleViewController"
#define VIEW_POPUPCUSTOMERSERVICE   @"PopupCustomerServiceViewController"
#define VIEW_POPUPCHANGEPASSWORD    @"PopupChangePasswordViewController"
#define VIEW_POPUPVALUEENTRY        @"PopupValueEntryViewController"
#define VIEW_WEBVIEW_GENERIC        @"GenericOnlyWebViewController"
#define VIEW_POPUP_WITH2BUTTONS_NEW @"PopupWithTwoButtons_NewViewController"

#define VIEW_REGISTERCUSTOMER         @"RegisterCustomerView"
#define VIEW_REGISTERCUSTOMER_OTP      @"RegisterCustomerOTPView"
#define VIEW_REGISTERCUSTOMER_FORM      @"RegisterCustomerFormView"
#define VIEW_REGISTERCUSTOMER_COMPLETE      @"RegisterCustomerCompleteView"

#define VIEW_NOTIFICATIONS      @"NotificationsView"
#define VIEW_SETTINGS           @"SettingsView"

#define VIEW_SURVEY                @"SurveyView"

//#define VIEW_WORKSHOPPROFILING           @"WorkshopProfilingView" //defined above, can reuse for storyboard
#define VIEW_WORKSHOPPROFILINGDETAILS     @"WorkshopProfilingDetailsView"

//Russia
#define VIEW_RU_HOME @"RUHomeViewController"
#define VIEW_RU_LOCKEDREWARDS @"RULockedRewardsViewController"

#define VIEW_RU_REGISTEROILCHANGE @"RURegisterOilChangeViewController"
#define VIEW_RU_REGISTEROILCHANGERESULT @"RURegisterOilChangeResultViewController"
#define VIEW_RU_CHOOSEPRODUCTBRAND @"RUChooseProductBrandViewController"
#define VIEW_RU_CHOOSEPRODUCT @"RUChooseProductViewController"
#define VIEW_RU_PRODUCTSEARCH @"RUProductSearchViewController"

#define VIEW_RU_SEARCHVEHICLE @"RUSearchVehicleViewController"
#define VIEW_RU_VALUECALCULATOR @"RUValueCalculatorViewController"

#define VIEW_RU_REGISTERCUSTOMER @"RURegisterCustomerViewController"
#define VIEW_RU_CUSTOMERSIGNATURE @"RUCustomerSignatureViewController"
#define VIEW_RU_REGISTERCUSTOMEROTP @"RURegisterCustomerOTPViewController"
#define VIEW_RU_REGISTERCUSTOMERSUCCESS @"RURegisterCustomerSuccessViewController"

#define VIEW_RU_EARNPOINTS @"RUEarnPointsViewController"
//#define VIEW_RU_POINTSCALCULATOR @"RUPointsCalculatorViewController"

#define VIEW_RU_PARTNERTAB @"RUPartnerTabViewController"
#define VIEW_RU_PARTNERPOINTSEARNED @"RUPartnerPointsEarnedViewController"
#define VIEW_RU_PARTNERPERFORMANCE @"RUPartnerPerformanceViewController"
#define VIEW_RU_REGISTERCUSTOMERSUCCESS @"RURegisterCustomerSuccessViewController"
#define VIEW_RU_REDEMPTION @"RUCustomerRedemptionViewController"
#define VIEW_RU_REDEMPTIONSUCCESS @"RUCustomerRedemptionSuccessViewController"


//RU Workshop Offer
#define VIEW_RU_WORKSHOPOFFER @"RUWorkshopOfferListViewController"
#define VIEW_RU_ADDWORKSHOPOFFER_STEPONE @"RUWorkshopOfferAddNew_StepOneViewController"
#define VIEW_RU_ADDWORKSHOPOFFER_STEPTWO @"RUWorkshopOfferAddNew_StepTwoViewController"
#define VIEW_RU_ADDWORKSHOPOFFER_STEPTHREE @"RUWorkshopOfferAddNew_StepThreeViewController"
#define VIEW_RU_ADDWORKSHOPOFFER_STEPFOUR @"RUWorkshopOfferAddNew_StepFourViewController"
#define VIEW_RU_ADDWORKSHOPOFFER_SUCCESS @"RUWorkshopOfferAddNew_SuccessViewController"
#define VIEW_RU_ADDWORKSHOPOFFER_SELECTOILBRAND @"RUWorkshopOfferSelectOilBrandViewController"
#define VIEW_RU_ADDWORKSHOPOFFER_SELECTCARBRAND @"RUWorkshopOfferSelectCarBrandViewController"
#define VIEW_RU_OFFERDETAILS @"RUWorkshopOfferDetailsViewController"

#define VIEW_RU_TOPERFORMANCE_TAB @"RUTOPerformanceTabViewController"
#define VIEW_RU_TOPERFORMANCE_DASHBOARD @"RUTOPerformanceDashboardViewController"
#define VIEW_RU_TOPERFORMANCE_LEADERBOARD @"RUTOLeaderboardViewController"

#define VIEW_RU_BREAKDOWN_ORDER_TAB @"RUOrdersBreakdownMasterViewController"
#define VIEW_RU_BREAKDOWN_ORDER_OVERVIEW @"RUOrdersBreakdownOverviewViewController"
#define VIEW_RU_BREAKDOWN_ORDER_DETAILS @"RUOrdersBreakdownDetailsViewController"

#define VIEW_RU_BREAKDOWN_POINTS_TAB @"RUPointsBreakdownMasterViewController"
#define VIEW_RU_BREAKDOWN_POINTS_OVERVIEW @"RUPointsBreakdownOverviewViewController"
#define VIEW_RU_BREAKDOWN_POINTS_DETAILS @"RUPointsBreakdownDetailsViewController"

#define VIEW_RU_BREAKDOWN_OILCHANGE_TAB @"RUOilChangeBreakdownMasterViewController"
#define VIEW_RU_BREAKDOWN_OILCHANGE_OVERVIEW @"RUOilChangeBreakdownOverviewViewController"
#define VIEW_RU_BREAKDOWN_OILCHANGE_DETAILS @"RUOilChangeBreakdownDetailsViewController"

#define VIEW_RU_BREAKDOWN_CUSTOMER @"RUCustomersBreakdownViewController"
#define VIEW_RU_BREAKDOWN_STAFF @"RUStaffBreakdownViewController"
#define VIEW_RU_BREAKDOWN_OFFERREDEMPTIONS @"RUOfferRedemptionsBreakdownViewController"

#define VIEW_RU_DSRPERFORMANCE_TAB @"RUDSRPerformanceTabViewController"
#define VIEW_RU_DSRPERFORMANCE_PERFORMANCEVIEW @"RUDSRPartnersWorkshopPerformanceViewController"
#define VIEW_RU_DSRPERFORMANCE_LEADERBOARD @"RUDSRPartnersLeaderboardViewController"

#define VIEW_RU_DSR_BREAKDOWN_TAB @"RUDSRBreakdownMainViewController"
#define VIEW_RU_DSR_BREAKDOWN_OVERVIEW @"RUDSRBreakdownOverviewController"
#define VIEW_RU_DSR_BREAKDOWN_DETAIL @"RUDSRBreakdownDetailViewController"
#define VIEW_RU_DSR_BREAKDOWN_CUSTOMER @"RUDSRCustomerBreakdownViewController"
#define VIEW_RU_DSR_BREAKDOWN_OIL_OVERVIEW @"RUDSROilBreakdownOverviewViewController"
#define VIEW_RU_DSR_BREAKDOWN_OIL_DETAIL @"RUDSROilBreakdownDetailViewController"

//Walkthrough
#define VIEW_RU_WALKTHROUGH @"WalkthroughViewController"

#define VIEW_TH_PERFORMANCEPOINT @"THPerformancePointView"
#define VIEW_TH_MYPERFORMANCE @"THRetailMyPerformanceView"

//India  
//Performance
#define VIEW_IN_PERFORMANCE_TAB @"INPerformanceTabViewController"
#define VIEW_IN_PERFORMANCE_DASHBOARD @"INPerformanceDashbordViewController"
#define VIEW_IN_PERFORMANCE_BREAKDOWN_MAIN @"INBreakdownMainViewController"
#define VIEW_IN_PERFORMANCE_BREAKDOWN_OVERVIEW @"INBreakdownOverviewViewController"
#define VIEW_IN_PERFORMANCE_BREAKDOWN_DETAIL @"INBreakdownDetailViewController"
#define VIEW_IN_PERFORMANCE_CUSTOMERBREAKDOWN @"INCustomerBreakdownViewController"
#define VIEW_IN_PERFORMANCE_MECHBREAKDOWN @"INMechanicBreakdownViewController"
#define VIEW_IN_PERFORMANCE_LEADERBOARD @"INLeaderboardPerformanceViewController"

//New Performance
#define VIEW_PERFORMANCE_DASHBOARD_NEW @"PerformanceDashboard_NewViewController"
#define VIEW_PERFORMANCE_DETAILS_NEW @"PerformanceDetails_NewViewController"
#define VIEW_PERFORMANCE_CHART_NEW @"PerformanceChart_NewViewController"

#define VIEW_IMAGEPREVIEW @"ImagePreviewViewController"

#define VIEW_MANAGE_STAFF @"StaffAdd_NewViewController"

//Forms
#define VIEW_STAFFFORM @"StaffFormViewController"

#define VIEW_IN_SEARCHVEHICLE @"INSearchVehicleViewController"
#define VIEW_IN_CASHINCENTIVE @"INCashIncentiveViewController"
#define VIEW_SCANBOTTLE_MAIN            @"ScanBottleMainViewController"
#define VIEW_SCANBOTTLE_FOR_SHARECODE   @"ScanBottleForShareCodeViewController"
#define VIEW_SCANBOTTLE_FOR_CONSUMER    @"ScanBottleForConsumerViewController"

#define VIEW_REGISTER_PANOTP    @"RegisterPanOTPView"

//LoyaltyProgram
#define VIEW_LOYALTY_TAB @"LoyaltyRewardsTabViewController"
#define VIEW_LOYALTY_CONTRACTLIST @"ContractListViewController"
#define VIEW_LOYALTY_PROGRAMPERFORMANCE @"LoyaltyProgramPerformanceViewController"
#define VIEW_LOYALTY_FILTERBY @"FilterByViewController"
#define VIEW_LOYALTY_ADDNEWCONTRACT_FIRSTSTEP @"AddNewContract_FirstStepViewController"
#define VIEW_LOYALTY_ADDNEWCONTRACT_SECONDSTEP @"AddNewContract_SecondStepViewController"
#define VIEW_LOYALTY_CONTRACTCONFIRMATION  @"ContractConfirmationViewController"
#define VIEW_LOYALTY_CONTRACTDETAILSVIEW    @"LoyaltyContractDetailsViewController"
#define VIEW_LOYALTY_WORKSHOPBREAKDOWN @"LoyaltyWorkshopBreakdownViewController"
#define VIEW_LOYALTY_CARTONBREAKDOWN @"LoyaltyCartonsBreakdownViewController"
#define VIEW_LOYALTY_TO_PENDINGCONTRACTDETAIL  @"OwnerPendingContractDetailViewController"
#define VIEW_LOYALTY_TO_CONTRACTDETAIL  @"OwnerContractDetailsViewController"

//Russia V2
#define VIEW_PARTNERSOFSHELL_TAB_V2             @"RUPartnersOfShell_V2TabViewController"
#define VIEW_PARTNERSOFSHELL_PERFORMANCE_V2     @"RUPartnersPerformance_V2ViewController"
#define VIEW_PARTNERSOFSHELL_POINTSEARNED_V2    @"RUPartnersPointsEarned_V2ViewController"
#define VIEW_EARNPOINTS_TAB_V2                  @"RUEarnPoints_V2TabViewController"
#define VIEW_EARNPOINTS_TABLE_V2                @"RUEarnPointsTable_V2ViewController"
#define VIEW_RU_BREAKDOWN_V2                    @"RUBreakdown_V2ViewController"
#define VIEW_INVENTORY_TABVIEW                  @"InventoryManagementTabViewController"
#define VIEW_INVENTORY_LISTVIEW                 @"InventoryListViewController"
#define VIEW_INVENTORY_SCANVIEW                 @"InventoryScanViewController"
#define VIEW_INVENTORY_SCANRESULT_VIEW          @"InventoryScanResultViewController"
#define VIEW_INVENTORY_WORKSHOPLIST             @"InventoryWorkshopListViewController"

//Ebooking

#define VIEW_ALLBOOKING_LIST                           @"CustomTabController"

//Main V2
#define VIEW_HOME_NEW                           @"Home_NewViewController"
#define VIEW_HOMEPROMOTION                      @"HomePromotionViewController"
#define VIEW_HOMEPROMOTION_DETAIL               @"HomePromotionsDetailViewController"
#define VIEW_HOMENOTIFICATIONS                  @"HomeNotificationViewController"
#define VIEW_HOMEPROMOTION_LIST                 @"HomePromotionListViewController"
#define VIEW_COACHMARK                          @"CoachMarkViewController"
#define VIEW_PARTICIPATINGPRODUCTS_V3_TAB       @"ParticipatingProducts_V3TabViewController"
#define VIEW_SHAREPARTICIPATINGPRODUCTS_V3      @"ShareParticipatingProducts_V3ViewController"
#define VIEW_ALLPARTICIPATINGPRODUCTS_V3        @"AllParticipatingProducts_V3ViewController"
#define VIEW_SCANFORREWARDS_HISTORY             @"ScanForRewardsHistoryViewController"

//Promotions
#define VIEW_PROMOTIONSCRATCH                   @"PromotionScratchViewController"
#define VIEW_PROMOTIONDETAILS                   @"PromotionDetailsViewController"
#define VIEW_CHECKIN                            @"CheckInViewController"

//AI
#define VIEW_AIHOME                             @"AIHomeViewController"

// SYSTEM VERSION
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

//DEVICE
#define iPHONE6 @"iPhone 6"
#define iPHONE6PLUS @"iPhone 6 Plus"
#define iPHONE6WIDTH 375
#define iPHONE6PLUSWIDTH 414
#define iPHONE5WIDTH 320

#define IS_IPHONE5 (SCREEN_WIDTH == iPHONE5WIDTH)

#define DEVICE_VERSION_EQUAL_TO(devicewidth,versionwidth) (devicewidth == versionwidth)
#define IS_IPAD ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)


#define COLVIEW_CELLHEIGHT      50
#define COLVIEW_LINESPACING     10


//FORMS KEYS
#define kForm_Type          @"Type"
#define kForm_Title         @"Title"
#define kForm_Key           @"Key"
#define kForm_TextColour    @"TextColour"
#define kForm_BackgroundColour    @"BackgroundColour"
#define kForm_SecureEntry   @"SecureEntry"
#define kForm_StatusVerify  @"StatusVerify"
#define kForm_StatusIcon @"StatusIcon"
#define kForm_Value         @"Value"
#define kForm_Enabled       @"Enabled"
#define kForm_Optional      @"Optional"
#define kForm_Placeholder   @"Placeholder"
#define kForm_Keyboard      @"Keyboard"
#define kForm_TextfieldEnabled @"TextfieldEnabled"
#define kForm_Icon              @"Icon"
#define kForm_CountryCode              @"CountryCode"
#define kForm_MobileNumber              @"MobileNumber"
#define kForm_WorkingHours              @"WorkingHours"

#define kForm_Hyperlinks              @"Hyperlinks" //send as dictionary. @{"text" : "url"}

#define kForm_CheckboxSelected              @"CheckboxSelected"

#define kForm_HasDelegate              @"HasDelegate"
#define kForm_IsHidden                @"IsHidden"
#define kForm_HasDelegate              @"HasDelegate"
#define kForm_CheckboxLabels              @"CheckboxLabels" //array of checkbox labels
#define kForm_LinkedCellKeys              @"LinkedCellKeys" //key array of other cells to be affected by current cell

#define kForm_LinkedWorkshopDict             @"LinkedWorkshopDict"

//ExpandsableCollapsible Data constants
#define kExpandable_TableDataArray               @"TableDataArray"
#define kExpandable_TableHeadingsArray           @"TableHeadingsArray"
// modified Sept 2020
#define kExpandable_ProductName                  @"ProductName"
#define kExpandable_ProductTotal                 @"InStock"

#endif /* Constants_h */
