//
//  Log.m

//
//  Created by Shekhar on 1/6/12.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import "LogHelper.h"
#import "UIDevice+Machine.h"
#import "AppInfo.h"
#import "DeviceInfo.h"

static int level;
static NSString *logFile;
NSFileHandle *myHandle;

static LogHelper* instance;

@implementation LogHelper

+ (void)initialize
{
    level = [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"LogLevel"] intValue];
}

- (id) init {
    if (self = [super init])
    {
        level = [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"LogLevel"] intValue];
        return self;
    }
    return nil;
}

+ (LogHelper*) getInstance {
    @synchronized([LogHelper class]) {
        
        if ( instance == nil ) {
            instance = [[LogHelper alloc] init];
        }
    }
    return instance;
}

- (void)setExceptionErrorInfo:(NSString *)module
{
    self.appInfo = [[AppInfo alloc]initWithAppInfo];
    self.deviceInfo = [[DeviceInfo alloc]initWithDeviceInfo];
    self.currentModule = module;
}

+ (void)Heading
{
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [infoDict objectForKey:@"CFBundleShortVersionString"];
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    NSString *countryName = [usLocale displayNameForKey: NSLocaleCountryCode value: countryCode];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSString *devName = [NSString stringWithFormat:@"%@", [[UIDevice currentDevice] machine]];
    
    /*NSString *message = [NSString stringWithFormat:@"Date, Type, ClassName, MethodName, Message\n%@, Debug, DeviceInfo, DeviceInfo, App Version: %@; Device: %@; DeviceName: %@; OS Version: %@; model: %@; Locale: %@; TimeZone: %@;\n", [Log getCurrentTime], version, [devName stringByReplacingOccurrencesOfString:@"," withString:@"."], [[UIDevice currentDevice] name], [[UIDevice currentDevice] systemVersion], [[UIDevice currentDevice] model], countryName, [destinationTimeZone name]];*/
    NSString *message = [NSString stringWithFormat:@"Date, Type, Category, ClassName, MethodName, Message\n"];
    
    NSLog(@"%@", message);
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); 
    NSString *documentsDirectory = [documentPaths objectAtIndex:0];
    NSString *documentTXTPath = [documentsDirectory stringByAppendingPathComponent:logFile];
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    if ([fileManager fileExistsAtPath:documentTXTPath])
    {
        myHandle = [NSFileHandle fileHandleForUpdatingAtPath:documentTXTPath];
        [myHandle seekToEndOfFile];
        [myHandle writeData:  [message dataUsingEncoding:NSUTF8StringEncoding]];
        [myHandle seekToEndOfFile];
    }
   
    
    [LogHelper debug:@"Device-Info" class:NSStringFromClass([self class]) selector:NSStringFromSelector(_cmd)  message:[NSString stringWithFormat:@"%@ App Version: %@; Device: %@; DeviceName: %@; OS Version: %@; model: %@; Locale: %@; TimeZone: %@;\n", [LogHelper getCurrentTime], version, [devName stringByReplacingOccurrencesOfString:@"," withString:@"."], [[UIDevice currentDevice] name], [[UIDevice currentDevice] systemVersion], [[UIDevice currentDevice] model], countryName, [destinationTimeZone name]]];
}


+ (void)createNewLogFile
{
    AppInfo *appInfo = [[AppInfo alloc]initWithAppInfo];
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMdd_hh:mm:ss"];
    logFile = [[NSString alloc] initWithString:[NSString stringWithFormat:@"%@%@",appInfo.appName,appInfo.appVersion]];
    logFile = [logFile stringByAppendingString:[dateFormatter stringFromDate:today]];
    //logFile = [[NSString alloc] initWithString:[dateFormatter stringFromDate:today]];
    logFile = [logFile stringByAppendingString:@".txt"];
    NSLog(@"date:%@",logFile);
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); 
    NSString *documentsDirectory = [documentPaths objectAtIndex:0];
    NSString *documentTXTPath = [documentsDirectory stringByAppendingPathComponent:logFile];
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    
    if (![fileManager fileExistsAtPath:documentTXTPath]){
    
        NSString *str = @"";
        [str writeToFile:documentTXTPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
    [LogHelper Heading];
}


+ (void)info:(NSString *)type class:(NSString *)className selector:(NSString *)methodName message:(NSString *)messageString
{
    if (level < 2)
    {
        NSString *message = [NSString stringWithFormat:@"%@, Info, %@, %@, %@, %@\n", [LogHelper getCurrentTime], type, className, methodName, [LogHelper formatMessage:messageString]];
        NSLog(@"%@", message);
        [myHandle writeData:  [message dataUsingEncoding:NSUTF8StringEncoding]];
        [myHandle seekToEndOfFile];
    }
}


+ (void)debug:(NSString *)type class:(NSString *)className selector:(NSString *)methodName message:(NSString *)messageString
{
    if (level < 3)
    {
        NSString *message = [NSString stringWithFormat:@"%@, Debug, %@, %@, %@, %@\n", [LogHelper getCurrentTime], type, className, methodName, [LogHelper formatMessage:messageString]];
        NSLog(@"%@", message);
        [myHandle writeData: [message dataUsingEncoding:NSUTF8StringEncoding]];
        [myHandle seekToEndOfFile];
    }
}


+ (void)error:(NSString *)type class:(NSString *)className selector:(NSString *)methodName message:(NSString *)messageString
{
    if (level < 4)
    {
    NSString *message = [NSString stringWithFormat:@"%@, Error, %@, %@, %@, %@\n", [LogHelper getCurrentTime], type, className, methodName, [LogHelper formatMessage:messageString]];
    NSLog(@"%@", message);
    [myHandle writeData: [message dataUsingEncoding:NSUTF8StringEncoding]];
    [myHandle seekToEndOfFile];
    }
}

+ (void)deleteExistingLogFile
{
    int logFileCount = 0;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectoryPath = [paths objectAtIndex:0];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *fileList = [fileManager contentsOfDirectoryAtPath:documentsDirectoryPath error:nil];
    for (NSString *fileName in fileList){
        NSLog(@"File:%@",fileName);
        if ([fileName length] > 0 && [[fileName substringFromIndex:[fileName length] - 4] isEqualToString:@".txt"])
            logFileCount++;
    }
    NSLog(@"LogFile Count:%d",logFileCount);
    if(logFileCount > 3)
    {
        for (NSString *fileName in fileList )
        {
                NSLog(@"Reverse File:%@",fileName);
                if ([fileName length] > 0 && [[fileName substringFromIndex:[fileName length] - 4] isEqualToString:@".txt"])                {
                    NSString *documentTXTPath = [documentsDirectoryPath stringByAppendingPathComponent:fileName];
                    
                    if ([fileManager fileExistsAtPath:documentTXTPath] )
                    {
                        [fileManager removeItemAtPath:documentTXTPath error:nil];
                        logFileCount--;
                    }
                }
                if(logFileCount <=3 )
                    return;
        }
    }
}

+ (NSString *)formatMessage:(NSString *)message
{
    if ([message rangeOfString:@"{"].location != NSNotFound)
    {
        message = [message stringByReplacingOccurrencesOfString:@"{" withString:@""];
    }
    if ([message rangeOfString:@"}"].location != NSNotFound)
    {
        message = [message stringByReplacingOccurrencesOfString:@"}" withString:@""];
    }

    if ([message rangeOfString:@"\n"].location != NSNotFound)
    {
        message = [message stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    }
    if ([message rangeOfString:@"\r"].location != NSNotFound)
    {
        message = [message stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    }
    return message;
}

+ (NSString *)getCurrentTime
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy  hh:mm:ss.SSS"];
    NSString *currentTime = [dateFormatter stringFromDate:today];
    return currentTime;
}

+ (void)destroy
{
    //[myHandle release];
}

@end
