//
//  Log.h

//
//  Created by Shekhar on 1/6/12.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AppInfo;
@class DeviceInfo;

#define mLogHelper 	((LogHelper *) [LogHelper getInstance])

@interface LogHelper : NSObject

+ (LogHelper *)getInstance;

@property(nonatomic,strong) AppInfo *appInfo;
@property(nonatomic,strong) DeviceInfo *deviceInfo;
@property(nonatomic,strong) NSString *currentModule;

+ (void)error:(NSString *)type class:(NSString *)className selector:(NSString *)methodName message:(NSString *)messageString;
+ (void)info:(NSString *)type class:(NSString *)className selector:(NSString *)methodName message:(NSString *)messageString;
+ (void)debug:(NSString *)type class:(NSString *)className selector:(NSString *)methodName message:(NSString *)messageString;
+ (void)createNewLogFile;
+ (void)deleteExistingLogFile;
+ (void)Heading;
+ (NSString *)formatMessage:(NSString *)message;
+ (NSString *)getCurrentTime;
+ (void)destroy;
- (void)setExceptionErrorInfo:(NSString *)module;

@end
