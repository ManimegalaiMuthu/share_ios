//
//  Helper.h
//  JTIStarPartners
//
//  Created by Shekhar  on 22/11/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CountLabel.h"
#import "TTTAttributedLabel.h"
#import "Constants.h"

@interface Helper : NSObject

//UI
+ (void)showAnimationOnView:(UIView *)view withDuration:(NSTimeInterval)duration withType:(NSString*)type;
+ (void)showAnimationOnView:(UIView *)view withDuration:(NSTimeInterval)duration;
+ (void)hideAllSubviewsinView:(UIView *)view;
+ (void)showAnimationOnViewWillAppear:(UIView *)view withDuration:(NSTimeInterval)duration;
+ (void)showViewWithAnimation:(UIView *)view withDuration:(NSTimeInterval)duration;

//Label
+ (void)showCountingAnimationOnLabels:(NSMutableArray *)countLabels;
+ (float)expectedHeightOfLabelOfWidth:(CGFloat)width withString:(NSString *)text andFont:(UIFont *)font;

//Time/Date
+ (NSInteger)compareTime:(NSDate *)observationTime atTimeZone:(NSTimeZone *)timeZone;
+ (NSString *)formatDate:(NSDate *)date withFormat:(NSString *)format;
+ (NSDate *)parseDate:(NSString *)str withFormat:(NSString *)format;


//Localization
+ (int)getLocalization:(NSString *)localization;


+ (void) setHeaderFont: (UILabel *) label;
+ (void) setHeaderSubtitleFont: (UILabel *) label;

+ (void) setNavigationBarAppearanceTitleSize:(CGFloat) size;
+ (void) setNavigationBarAppearanceTitleLabelInTwoLines:(UIViewController *)view;
+ (void) setNavigationBarTitle:(UIViewController *)view title:(NSString *)title subtitle:(NSString *) subtitle;
+ (void) setNavigationBarTitle:(UIViewController *)view title:(NSString *)title subtitle:(NSString *) subtitle size:(CGFloat)size subtitleSize:(CGFloat)subtitleSize;

//UI adjustments
+ (void) setCustomFontButtonContentModes: (UIButton *) btn;
+ (void) setButtonFontWithAttributedString: (UIButton *) btn font:(UIFont *)font colour:(UIColor *)colour underline:(BOOL)underline;

//TTAttributedLabel helper
+ (void) setHyperlinkLabel: (TTTAttributedLabel *) label hyperlinkText:(NSString *) hyperlinkText bodyText:(NSString *) bodyText urlString:(NSString *) urlString;
+ (void) setHyperlinkLabel: (TTTAttributedLabel *) label hyperlinkText:(NSString *) hyperlinkText hyperlinkFont:(UIFont *)hyperlinkFont bodyText:(NSString *) bodyText bodyFont:(UIFont *)bodyFont urlString:(NSString *) urlString;
+ (void) addHyperlink: (TTTAttributedLabel *) label hyperlinkText:(NSString *) hyperlinkText hyperlinkFont:(UIFont *)hyperlinkFont urlString:(NSString *) urlString bodyText:(NSString *) bodyText;
+ (void) addHyperlink: (TTTAttributedLabel *) label hyperlinkText:(NSString *) hyperlinkText urlString:(NSString *) urlString bodyText:(NSString *) bodyText;
+ (void) setHyperlinkLabel: (TTTAttributedLabel *) label hyperlinkText:(NSString *) hyperlinkText hyperlinkFont:(UIFont *)hyperlinkFont bodyText:(NSString *) bodyText bodyFont:(UIFont *)bodyFont urlString:(NSString *) urlString bodyColor :(UIColor *)bodyColor;
+ (UIColor*)colorFromHex:(NSString *)hexString;
+ (UIColor*)colorFromHex:(NSString *)hexString withAlpha:(CGFloat)alpha;

+ (NSString *) getFontBodyString;
+ (NSString *) getFontBoldString;

//For Coachmark
+ (NSDictionary *) createCoachmarkFor:(CGRect) rect withCaption:(NSString *) caption andOnSide:(NSNumber *)side;
@end
