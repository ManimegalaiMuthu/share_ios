//
//  Helper.m
//  JTIStarPartners
//
//  Created by Shekhar  on 22/11/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import "Helper.h"
#import <UIKit/UIKit.h>
#import "Constants.h"
#import "NSDateComponents+DateDifference.h"

@implementation Helper



+ (void)showAnimationOnViewWillAppear:(UIView *)view withDuration:(NSTimeInterval)duration
{
    for (UIView *subView in view.subviews)
    {
        subView.hidden = YES;
    }
    
    dispatch_time_t showTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC));
    dispatch_after(showTime, dispatch_get_main_queue(), ^(void)
                   {
                       [Helper showAnimationOnView:view withDuration:duration];
                       for (UIView *subView in view.subviews)
                       {
                           subView.hidden = NO;
                       }
                   });
}

+ (void)hideAllSubviewsinView:(UIView *)view
{
    for (UIView *subView in view.subviews)
    {
        subView.hidden = YES;
    }
    
}

+ (void)showViewWithAnimation:(UIView *)view withDuration:(NSTimeInterval)duration
{
    view.hidden = YES;
    
    dispatch_time_t showTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC));
    dispatch_after(showTime, dispatch_get_main_queue(), ^(void)
                   {
                       [Helper showAnimationOnView:view withDuration:duration];
                           view.hidden = NO;
                   });
}

+ (void)showAnimationOnView:(UIView *)view withDuration:(NSTimeInterval)duration
{
    CATransition *animation = [CATransition animation];
    animation.type = kCATransitionFade;
    animation.duration = duration;
    [view.layer addAnimation:animation forKey:nil];
}

+ (void)showAnimationOnView:(UIView *)view withDuration:(NSTimeInterval)duration withType:(NSString*)type
{
    CATransition *animation = [CATransition animation];
    animation.type = type;
    animation.duration = duration;
    [view.layer addAnimation:animation forKey:nil];
}

+ (void)showCountingAnimationOnLabels:(NSMutableArray *)countLabels
{
    for (CountLabel *countLabel in countLabels) {
        [countLabel.label addCountingAnimationwithNumber:[countLabel.count doubleValue]];
        
    }
}

+ (float)expectedHeightOfLabelOfWidth:(CGFloat)width withString:(NSString *)text andFont:(UIFont *)font
{
    CGSize maximumLabelSize = CGSizeMake(width,9999);
    CGSize expectedLabelSize = [text sizeWithFont:font
                                constrainedToSize:maximumLabelSize
                                    lineBreakMode:NSLineBreakByWordWrapping];
    return expectedLabelSize.height;
}

+ (NSInteger)compareTime:(NSDate *)observationTime atTimeZone:(NSTimeZone *)timeZone
{
    NSDate *systemTime = [NSDate date];

    NSTimeInterval distanceBetweenTimes = [systemTime timeIntervalSinceDate:observationTime];
    double secondsInAnHour = 3600;
    NSInteger minutesBetweenTimes = distanceBetweenTimes * 60 / secondsInAnHour;

    return abs(minutesBetweenTimes);
}

+ (NSString *)formatDate:(NSDate *)date withFormat:(NSString *)format
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    NSString *formatted = [dateFormatter stringFromDate:date];
    //    [dateFormatter release];
    return formatted;
}

+ (NSDate *)parseDate:(NSString *)str withFormat:(NSString *)format
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    NSDate *date = [dateFormatter dateFromString:str];
    
    return date;
}

+ (int)getLocalization:(NSString *)localization
{
//    if (EQUALS(localization, kIndo)) {
//        return INDO;
//    }
    return ENGLISH;
    
}



+ (void) setHeaderFont: (UILabel *) label
{
    label.font = FONT_H(18);
    label.textColor = COLOUR_RED;
}

+ (void) setHeaderSubtitleFont: (UILabel *) label
{
    label.font = FONT_H(12);
    label.textColor = COLOUR_VERYDARKGREY;
}

//+ (void) setContentTitle: (UILabel *) label;
//{
//
//}

/**
 * Set this before loading new View Controller
 **/
+ (void) setNavigationBarAppearanceTitleSize:(CGFloat) size
{
    //default setting
    UINavigationBar *navigationBarAppearance = [UINavigationBar appearance];
    [navigationBarAppearance setTitleTextAttributes:@{NSFontAttributeName: FONT_H(size), NSForegroundColorAttributeName:COLOUR_RED}];
}

+ (void) setNavigationBarAppearanceTitleLabelInTwoLines:(UIViewController *)view
{
    UILabel *label = [[UILabel alloc]init];
    label = view.navigationItem.titleView;
    label.numberOfLines = 0;
    view.navigationItem.titleView = label;
}

+ (void) setNavigationBarTitle:(UIViewController *)view title:(NSString *)title subtitle:(NSString *) subtitle
{
    //default size
    [self setNavigationBarTitle:view title:title subtitle:subtitle size:18 subtitleSize:12];
}

+ (void) setNavigationBarTitle:(UIViewController *)view title:(NSString *)title subtitle:(NSString *) subtitle size:(CGFloat)size subtitleSize:(CGFloat)subtitleSize
{
    NSMutableAttributedString *attrString;
    title = [title uppercaseString];
    
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 36)];
    
    if([subtitle length] > 0)
    {
        NSString *stringName = [NSString stringWithFormat: @"%@\r%@", title, subtitle] ;
        attrString = [[NSMutableAttributedString alloc] initWithString:stringName];
        
        //header attributes
        [attrString addAttribute:NSFontAttributeName value:FONT_H(size) range: [stringName rangeOfString: title ]];
        [attrString addAttribute:NSForegroundColorAttributeName value:COLOUR_RED range: [stringName rangeOfString: [title uppercaseString]]];
        
        //subtitle attributes
        [attrString addAttribute:NSFontAttributeName value:FONT_B(subtitleSize) range: [stringName rangeOfString: subtitle]];
        [attrString addAttribute:NSForegroundColorAttributeName value:COLOUR_VERYDARKGREY range: [stringName rangeOfString: subtitle]];
        label.numberOfLines = 0;
    }
    else
    {
        NSString *stringName = [NSString stringWithFormat: @"%@", title];
        attrString = [[NSMutableAttributedString alloc] initWithString:stringName];
        
        //header attributes
        [attrString addAttribute:NSFontAttributeName value:FONT_H(size) range: [stringName rangeOfString: title]];
        [attrString addAttribute:NSForegroundColorAttributeName value:COLOUR_RED range: [stringName rangeOfString: [title uppercaseString]]];
        label.numberOfLines = 1;
    }
    
    label.textAlignment = NSTextAlignmentCenter;
    label.attributedText = attrString;
    view.navigationItem.titleView = label;
}

+ (void) setCustomFontButtonContentModes: (UIButton *) btn
{
    //UIButton's content vertical alignment & content mode set in XIB
    [btn setContentVerticalAlignment: UIControlContentVerticalAlignmentFill];
    [btn setContentMode: UIViewContentModeCenter];
    [btn.imageView setContentMode: UIViewContentModeScaleAspectFit];
}

+ (void) setButtonFontWithAttributedString: (UIButton *) btn font:(UIFont *)font colour:(UIColor *)colour underline:(BOOL)underline;
{
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString: btn.titleLabel.text attributes:
                                            @{NSFontAttributeName: font,
                                              NSUnderlineStyleAttributeName: underline? @(NSUnderlineStyleSingle): @(NSUnderlineStyleNone)}];
    
    [btn setAttributedTitle: attributedString forState: UIControlStateNormal];
    [btn setTintColor: colour];
}

+ (void) setHyperlinkLabel: (TTTAttributedLabel *) label hyperlinkText:(NSString *) hyperlinkText bodyText:(NSString *) bodyText urlString:(NSString *) urlString
{
    [self setHyperlinkLabel: label hyperlinkText:hyperlinkText hyperlinkFont: FONT_B2 bodyText:bodyText bodyFont:FONT_B2 urlString: urlString];
}

+ (void) setHyperlinkLabel: (TTTAttributedLabel *) label hyperlinkText:(NSString *) hyperlinkText hyperlinkFont:(UIFont *)hyperlinkFont bodyText:(NSString *) bodyText bodyFont:(UIFont *)bodyFont urlString:(NSString *) urlString
{
    label.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    label.linkAttributes = @{NSFontAttributeName: hyperlinkFont,
                              NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,
                             NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)
                             };
    
    NSMutableAttributedString *bodyAttrString = [[NSMutableAttributedString alloc] initWithString: bodyText
                                                                                       attributes: @{NSFontAttributeName: bodyFont,
                                                                                                     NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,
                                                                                                     }];
    
    NSRange rangeOfString = [bodyText rangeOfString: hyperlinkText];
    //    [bodyAttrString setAttributes: hyperlinkAttributes range:rangeOfString];
    [label setText: bodyAttrString];
    [label addLinkToURL: [NSURL URLWithString: urlString] withRange: rangeOfString];

}

+ (void) setHyperlinkLabel: (TTTAttributedLabel *) label hyperlinkText:(NSString *) hyperlinkText hyperlinkFont:(UIFont *)hyperlinkFont bodyText:(NSString *) bodyText bodyFont:(UIFont *)bodyFont urlString:(NSString *) urlString bodyColor :(UIColor *)bodyColor
{
    label.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    label.linkAttributes = @{NSFontAttributeName: hyperlinkFont,
                             NSForegroundColorAttributeName: bodyColor,
                             NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)
                             };
    
    NSMutableAttributedString *bodyAttrString = [[NSMutableAttributedString alloc] initWithString: bodyText
                                                                                       attributes: @{NSFontAttributeName: bodyFont,
                                                                                                     NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,
                                                                                                     }];
    
    NSRange rangeOfString = [bodyText rangeOfString: hyperlinkText];
    //    [bodyAttrString setAttributes: hyperlinkAttributes range:rangeOfString];
    [label setText: bodyAttrString];
    
    [label addLinkToURL: [NSURL URLWithString: urlString] withRange: rangeOfString];
}

+ (void) addHyperlink: (TTTAttributedLabel *) label hyperlinkText:(NSString *) hyperlinkText urlString:(NSString *) urlString bodyText:(NSString *) bodyText
{
    [self addHyperlink: label hyperlinkText:hyperlinkText hyperlinkFont:FONT_B1 urlString:urlString bodyText:bodyText];
}

+ (void) addHyperlink: (TTTAttributedLabel *) label hyperlinkText:(NSString *) hyperlinkText hyperlinkFont:(UIFont *)hyperlinkFont urlString:(NSString *) urlString bodyText:(NSString *) bodyText
{
    label.linkAttributes = @{NSFontAttributeName: hyperlinkFont,
                             //                             NSForegroundColorAttributeName: COLOUR_RED,
                             NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)
                             };
    
    NSRange rangeOfString = [bodyText rangeOfString: hyperlinkText];
    [label addLinkToURL: [NSURL URLWithString: urlString] withRange: rangeOfString];
    
    
}
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define UIColorFromRGBWithAlpha(rgbValue,alpha) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:alpha]

+ (UIColor*)colorFromHex:(NSString *)hexString
{
    unsigned result = 0;
    hexString = [hexString stringByReplacingOccurrencesOfString: @"#" withString: @""];
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    
    [scanner setScanLocation:0];
    [scanner scanHexInt:&result];
    
    return UIColorFromRGB(result);
}

+ (UIColor*)colorFromHex:(NSString *)hexString withAlpha:(CGFloat)alpha
{
    unsigned result = 0;
    hexString = [hexString stringByReplacingOccurrencesOfString: @"#" withString: @""];
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    
    [scanner setScanLocation:0];
    [scanner scanHexInt:&result];
    
    return UIColorFromRGBWithAlpha(result,alpha);
    
}

+ (NSString *) getFontBodyString
{
    if ([GET_LOCALIZATION isEqualToString: kThai])
        return @"Kittithada Roman 55";
    else if ([GET_LOCALIZATION isEqualToString: kVietnamese])
        return @"ArialMT";
    else if ([GET_LOCALIZATION isEqualToString: kArabic])
        return @"ShellARB-Book";
    else if ([GET_LOCALIZATION isEqualToString: kUrdu])
        return @"ArialMT";
    else
        return @"ShellFutura-Book";  //default string
}

+ (NSString *) getFontBoldString
{
    if ([GET_LOCALIZATION isEqualToString: kThai])
        return @"Kittithada Bold 75";
    else if ([GET_LOCALIZATION isEqualToString: kVietnamese])
        return @"Arial-BoldMT";
    else if ([GET_LOCALIZATION isEqualToString: kArabic])
        return @"ShellARB-Bold";
    else if ([GET_LOCALIZATION isEqualToString: kUrdu])
        return @"Arial-BoldMT";
    else
        return @"ShellFutura-Bold"; //default string
}

//For Coachmark
+ (NSDictionary *) createCoachmarkFor:(CGRect) rect withCaption:(NSString *) caption andOnSide:(NSNumber *)side {
    return @{
        @"rectangle" : [NSValue valueWithCGRect:rect],
        @"caption"   : caption,
        @"side"      : side,
    };
}

@end
