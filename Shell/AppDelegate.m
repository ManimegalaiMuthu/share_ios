//
//  AppDelegate.m
//  Shell
//
//  Created by Ankita Chhikara on 17/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "AppDelegate.h"
#import "Constants.h"
#import "IOSPushManager.h"
#import "Session.h"
#import "DeviceInfo.h"
#import "IQKeyboardManager.h"
//#import "GAI.h"
#import <Firebase.h>
#import <MapKit/MapKit.h>

#import <UserNotifications/UserNotifications.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <Lokalise/Lokalise.h>
#import "InAppAssetVideoPlayerViewController.h"
#import "Shell-Swift.h"
#import "ObjCBridgeHeader.h"


@interface AppDelegate ()<IOSPushManagerDelegate, UNUserNotificationCenterDelegate,CLLocationManagerDelegate>
{
    NSTimer *idleTimer;
    NSTimeInterval maxIdleTime;
    
    BOOL restrictPushLaunch;
    BOOL pushNotificationLock; //for iOS 9
}


@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFontExtension FONT_MEDIUMOfSize:13]} forState:UIControlStateNormal];

    
    maxIdleTime = 10;
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window setBackgroundColor:[UIColor clearColor]];
    
    [mSession checkFirstLaunch];
    
    if (!GET_PUSHTOKEN)
    {
        SET_PUSHTOKEN(@"Not Available");
    }
    
    [self setupPushNotifications];
    [self setupLocationAccess];
        
    NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (userInfo) {
        restrictPushLaunch = YES;
    }
    
    [mSession checkSessionAndLaunchApp: userInfo];
    
    UIApplicationShortcutItem *shortcutItem = [launchOptions objectForKeyedSubscript:UIApplicationLaunchOptionsShortcutItemKey];
           
    if(shortcutItem)
    {
        // When the app launch at first time, this block can not called.
        [mSession handleShortcutItem:shortcutItem];
    }
    
    [self setupNavigationBar];
    
    [self setupKeyBoard];
    
    NSArray *windows = [[UIApplication sharedApplication] windows];
    for(UIWindow *window in windows) {
        if(window.rootViewController == nil){
            UIViewController* vc = [[UIViewController alloc]initWithNibName:nil bundle:nil];
            window.rootViewController = vc;
        }
    }
    
    
//    GAI *gai = [GAI sharedInstance];
//    [gai trackerWithTrackingId:@"UA-105355603-1"];
////    [gai trackerWithTrackingId:@"UA-109125498-1"];
//    gai.trackUncaughtExceptions = YES;
//    gai.logger.logLevel = kGAILogLevelVerbose;
    
    // Load a named file.
    NSString *filePath;
    
    if (IS_DEVELOPMENT_VERSION || IS_STAGING_VERSION)
    {
        filePath = [[NSBundle mainBundle] pathForResource: @"GoogleService-Info-Test" ofType: @"plist"];
    }
    else
    {
        filePath = [[NSBundle mainBundle] pathForResource: @"GoogleService-Info-Production" ofType: @"plist"];
    }
    FIROptions *firebaseOptions = [[FIROptions alloc] initWithContentsOfFile: filePath];
    [FIRApp configureWithOptions: firebaseOptions];
//    [FIRApp configure];
    
    [mSession updateAnalyticsUserProperty];
    
//    [[GAI sharedInstance] setDryRun: IS_STAGING_VERSION]; //disable test data sending in staging
//    [GAI sharedInstance].dispatchInterval = 1;
    
    // Provide SDK Token and Project ID
    [[Lokalise sharedObject] setAPIToken:@"0a2e35bb61bc8b7c128e28262795e7841e907e20" projectID:@"537805875bac7d706a51a4.16989954"];
    // Enable swizzling
    [[Lokalise sharedObject] swizzleMainBundle];
    
    if (IS_DEVELOPMENT_VERSION)
    {
//        // Returns keys instead of localized string.
//        [Lokalise sharedObject].localizationType = LokaliseLocalizationDebug;

        // Loads prerelease localization.
        [Lokalise sharedObject].localizationType = LokaliseLocalizationPrerelease;
        
//        // Uses localization included in local app bundle.
//        [Lokalise sharedObject].localizationType = LokaliseLocalizationLocal;
        
    }
    else if (IS_STAGING_VERSION)
    {
        // Loads prerelease localization.
        [Lokalise sharedObject].localizationType = LokaliseLocalizationPrerelease;
    }
    else
    {
        // Loads production localization (Default type).
        [Lokalise sharedObject].localizationType = LokaliseLocalizationRelease;
    }
    
    //Crashlytics must be last
    [Fabric with:@[[Crashlytics class]]];
    [CrashlyticsKit setObjectValue: GET_COUNTRY_CODE forKey: @"Country Code"]; //identifier as country code
    // Override point for customization after application launch.
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    NSInteger counter = 0;
    SET_PUSH_COUNT(counter);
    
    [[Lokalise sharedObject] checkForUpdatesWithCompletion:^(BOOL updated, NSError * _Nullable error) {
        NSLog(@"Updated %d\nError: %@", updated, error);
    }];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: counter];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler {
    [mSession handleShortcutItem:shortcutItem];
    SET_ISLOCATIONDETERMINED(NO);
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"Shell"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

#pragma mark - UI

- (void)setupNavigationBar
{
    UINavigationBar *navigationBarAppearance = [UINavigationBar appearance];
    navigationBarAppearance.translucent = NO;
    [navigationBarAppearance setBarTintColor: COLOUR_YELLOW];
    [navigationBarAppearance setTintColor: COLOUR_VERYDARKGREY];
    [navigationBarAppearance setTitleTextAttributes:@{NSFontAttributeName: FONT_H(18), NSForegroundColorAttributeName:COLOUR_RED}];
    
//    UIImage *backgroundImage = [UIImage imageNamed:@"shell"];
//    [navigationBarAppearance setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
}

- (void)setupKeyBoard
{
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
}

#pragma mark Push notifications
-(void)setupPushNotifications
{
    DeviceInfo *deviceInfo = [[DeviceInfo alloc]initWithDeviceInfo];
    
    if ([deviceInfo.deviceModel isEqualToString:kSimulator]) {
        
        SET_PUSHTOKEN(SIMULATOR_PUSHNOTIFICATION_TOKEN);
    }
    else
    {
//        [[IOSPushManager shared] registerAPNS];
        
//        [[IOSPushManager shared] setDelegate:self];
        if(@available(iOS 10, *)){
            //do nothing if iOS 10, handled in register APNS
            UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
            center.delegate = self;
            [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
                if(!error){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[UIApplication sharedApplication] registerForRemoteNotifications];
                    });
                }
            }];
        }
        else {
            //set delegate to self. iOS 10 uses UNUserNotification delegate
            [[UIApplication sharedApplication]registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
    }
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    NSCharacterSet *removeSet =
    [NSCharacterSet characterSetWithCharactersInString:@"<> "];
    NSString *deviceTokenString = [self hexStringFromData:deviceToken];
    deviceTokenString  = [[deviceTokenString  componentsSeparatedByCharactersInSet:removeSet]
                         componentsJoinedByString:@""];
    NSLog(@"Device Token---%@", deviceTokenString);
    
    SET_PUSHTOKEN(deviceTokenString);
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
//    [[IOSPushManager shared] didFailToRegisterForRemoteNotificationsWithError:error];
    
    SET_PUSHTOKEN(@"Not Available");
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSInteger counter = GET_PUSH_COUNT+1;
    SET_PUSH_COUNT(counter);
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: counter];
    
    [[IOSPushManager shared] didReceiveRemoteNotification:userInfo];
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo  fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))handler
{
    //Check for openURL [optional]
    //Track notification only if the application opened from Background by clicking on the notification.
    
    NSInteger counter = GET_PUSH_COUNT+1;
    SET_PUSH_COUNT(counter);
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: counter];
    
    if (@available(iOS 10, *)){
        
    }
    else
    {
        NSDictionary *notificationDict = [userInfo objectForKey:@"aps"];
        [mSession sessionSendOpenNotificationApiWithReferenceValue: [notificationDict objectForKey:@"ReferenceValue"]];
        //not logged in, don't proceed
        if (![GET_AUTHENTICATIONTOKEN isEqualToString: BLANK])
        {
#pragma mark iOS 9 handler
            if (application.applicationState == UIApplicationStateInactive)
            {
                if (restrictPushLaunch)
                {
                    restrictPushLaunch = NO;
                }
                else
                {
                    [self alertViewHandlerWithTitle:@"" subTitle:[notificationDict objectForKey:@"alert"]  okHandler:^{
                        [mSession handlePushNotificationWithReferenceKey:[notificationDict objectForKey:@"ReferenceKey"]
                                                          referenceValue:[notificationDict objectForKey:@"ReferenceValue"]];
                    }];
                }
            }
        
            //The application was already active when the user got the notification, just show an alert.
            //That should *not* be considered open from Push.
            if (application.applicationState == UIApplicationStateActive)
            {
                pushNotificationLock = YES;
                [self alertViewHandlerWithTitle:@"" subTitle:[notificationDict objectForKey:@"alert"]  okHandler:^{
                    [mSession handlePushNotificationWithReferenceKey:[notificationDict objectForKey:@"ReferenceKey"]
                                                  referenceValue:[notificationDict objectForKey:@"ReferenceValue"]];
                    [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(unlockPushNotification) userInfo:nil repeats:NO];
                }];
            }
        }
    }
    
    // .. Process notification data
    
    handler(UIBackgroundFetchResultNewData);
}
-(void) unlockPushNotification
{
    pushNotificationLock = NO;
}


#pragma mark iOS 10 Push notification handler
//Called when a notification is delivered to a foreground app.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
    
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}

//Called to let your app know which action was selected by the user for a given notification.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler {
    
    if(response.actionIdentifier == UNNotificationDismissActionIdentifier){
        
    }else{
        
    }
        
    completionHandler();
    
    [self handleRemoteNotification:[UIApplication sharedApplication] userInfo:response.notification.request.content.userInfo];
    
}

-(void) handleRemoteNotification:(UIApplication *) application   userInfo:(NSDictionary *) remoteNotif {
    // Handle Click of the Push Notification From Here…
    // You can write a code to redirect user to specific screen of the app here….
    
    if (restrictPushLaunch)
    {
        //don't process push notification methods here if launching from termination
        restrictPushLaunch = NO;
        return;
    }
    
    NSDictionary *notificationDict = [remoteNotif objectForKey:@"aps"];
    [mSession sessionSendOpenNotificationApiWithReferenceValue: [notificationDict objectForKey:@"ReferenceValue"]];
    
    //not logged in, don't proceed
    if ([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK])
        return;
    
    if (application.applicationState == UIApplicationStateInactive) {
        [self alertViewHandlerWithTitle:@"" subTitle:[notificationDict objectForKey:@"alert"]  okHandler:^{
            [mSession handlePushNotificationWithReferenceKey:[notificationDict objectForKey:@"ReferenceKey"]
                                              referenceValue:[notificationDict objectForKey:@"ReferenceValue"]];
        }];
    }
    if (application.applicationState == UIApplicationStateActive) {
        [self alertViewHandlerWithTitle:@"" subTitle:[notificationDict objectForKey:@"alert"]  okHandler:^{
            [mSession handlePushNotificationWithReferenceKey:[notificationDict objectForKey:@"ReferenceKey"]
                                              referenceValue:[notificationDict objectForKey:@"ReferenceValue"]];
        }];
    }
}

-(void) alertViewHandlerWithTitle:(NSString *) title subTitle:(NSString *)subtitle okHandler:(void (^)())okHandler
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle: title
                                                                   message: subtitle
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style: UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        okHandler();
    }];
    [alert addAction:defaultAction];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
    [alert addAction:cancelAction];
    
    [((AppDelegate *)[[UIApplication sharedApplication] delegate]).window.rootViewController presentViewController:alert animated:YES completion:nil];
}

- (NSString *)hexStringFromData:(NSData *)data
{
    NSUInteger dataLength = [data length];
    NSMutableString *stringBuffer =
    [NSMutableString stringWithCapacity:dataLength * 2];
    const unsigned char *dataBuffer = [data bytes];
    for (int i=0; i<dataLength; i++) {
        [stringBuffer appendFormat:@"%02x", (NSUInteger)dataBuffer[i]];
    }
    return stringBuffer;
}

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    UIViewController *topMostVC = [[CURRENT_VIEWCONTROLLER childViewControllers] lastObject];
    
    if ([topMostVC isKindOfClass: [InAppAssetVideoPlayerViewController class]])
    {
//        InAppAssetVideoViewController *secondController = (InAppAssetVideoViewController *) self.window.rootViewController.presentedViewController;
//
//        if (InAppAssetVideoViewController.isPresented)
//        {
//            return UIInterfaceOrientationMaskAll;
//        }
//        else return UIInterfaceOrientationMaskPortrait;
        return UIInterfaceOrientationMaskLandscape;
    }
    else
        return UIInterfaceOrientationMaskPortrait;
}

-(void) setupLocationAccess {
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    [locationManager requestWhenInUseAuthorization];
}
@end
