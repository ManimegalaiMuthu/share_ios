//
//  SideMenuTableViewCell.h
//  Shell
//
//  Created by Ankita Chhikara on 25/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgMenuIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgDropdownIcon;
//@property (weak, nonatomic) IBOutlet UIView *numberBackgroundView;
//@property (weak, nonatomic) IBOutlet UILabel *lblNumber;
@property (weak, nonatomic) IBOutlet UIImageView *imgSubMenuArrow;

-(void) reloadFont;

@end
