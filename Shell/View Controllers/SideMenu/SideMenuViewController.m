//
//  SideMenuViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 19/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "SideMenuViewController.h"
#import "SideMenuTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "ProfileInfo.h"


#define ANIMATION_SPEED 0.4f
typedef enum
{
    //DSR
    kSIDEMENU_DSRMANAGEWORKSHOP     = 100,
    kSIDEMENU_DSRWORKSHOPPERFORMANCE     ,
    kSIDEMENU_DSRSALESKIT           ,
    kSIDEMENU_DSRMANAGEORDER        ,
    kSIDEMENU_DSRVIEWPROMOTION      ,
    kSIDEMENU_WORKSHOPPROFILING     ,
    kSIDEMENU_WORKSHOPMODULES       ,       //SUB MENU BUTTON
    kSIDEMENU_PARTNERSOFSHELLCLUB,  //russia
    kSIDEMENU_SPIRAXGADUSONEPLUSONE,//russia
    kSIDEMENU_WORKSHOPOFFER         ,       //ancillary
    kSIDEMENU_RU_DSRWORKSHOPPERFORMANCE     ,
    kSIDEMENU_MYNETWORK             ,   //India
    kSIDEMENU_LOYALTYREWARDS,
    kSIDEMENU_DSR_INVENTORYMANAGEMENT,
    kSIDEMENU_DSR_HasInventory,
    kSIDEMENU_EBOOKING,

    //TRADE
    kSIDEMENU_TRADEMANAGESTAFF   = 200,
    kSIDEMENU_TRADEMANAGEORDER      ,
    kSIDEMENU_TRADEVIEWPROMOTION    ,
    kSIDEMENU_TRADESPIRAXGADUSONEPLUSONE,//russia
    kSIDEMENU_TRADEMYCUSTOMERS,
    kSIDEMENU_CASHINCENTIVE,    //India
    kSIDEMENU_MYREWARDS,
    kSIDEMENU_PARTICIPATINGPRODUCTS,
    kSIDEMENU_TO_INVENTORYMANAGEMENT,
    kSIDEMENU_TO_HasInventory, // added sept 2020
    kSIDEMENU_TO_EBOOKING,

    //MECHANIC
    kSIDEMENU_MECHSALESKIT          = 300,
    kSIDEMENU_TRADEWORKSHOPPERFORMANCE     ,
    kSIDEMENU_MECHSPIRAXGADUSONEPLUSONE,//russia
    kSIDEMENU_MECHPERFORMANCE       ,
    kSIDEMENU_MECHVIEWPROMOTION     ,
    kSIDEMENU_MECHMYCUSTOMERS,
    kSIDEMENU_RU_TRADEWORKSHOPPERFORMANCE,
    
    //FA
    kSIDEMENU_FAREGISTERPRODUCTCODE = 400,
    kSIDEMENU_FASPIRAXGADUSONEPLUSONE,//russia
    
    //DMR
    kSIDEMENU_IN_MECHPERFORMANCE = 500,
    kSIDEMENU_IN_MANAGEMECHANICS,
    kSIDEMENU_IN_MYMECHMODULES,
    kSIDEMENU_IN_MECHSPIRAXGADUSONEPLUSONE,//russia

    //COMMON
    kSIDEMENU_HOME                  = 900,
    kSIDEMENU_MYPROFILE             ,
    kSIDEMENU_COMMONSPIRAXGADUSONEPLUSONE,

    kSIDEMENU_MYLIBRARY           ,
    kSIDEMENU_NOTIFICATION          ,
    kSIDEMENU_SURVEY                   = 80,
    kSIDEMENU_SETTINGS              ,
    kSIDEMENU_HELPANDSUPPORT        ,   //submenu button
    kSIDEMENU_SWA                   ,
    kSIDEMENU_SDA                   ,
    
} kSIDEMENU_TYPES;

enum
{
    kSUBMENU_BACK = 9999, //default to return to main side menu
    
    //DSR
    
    //my workshop modules
    kSUBMENU_SEARCHVEHICLEID       = 200,
    kSUBMENU_INVITETRADE                ,
    kSUBMENU_REGISTERPRODUCTCODE        ,
    kSUBMENU_MYREWARDS                  ,
    kSUBMENU_CUSTOMERREDEMPTION         ,
    kSUBMENU_PARTICIPATINGPRODUCTS      ,
    kSUBMENU_OILCHANGEWALKTHROUGH       ,
    kSUBMENU_MYNETWORK                  , //IN Retailer
    kSUBMENU_LUBEMATCH                  ,
    
    //Help & Support
    kSUBMENU_FAQ                   = 300,
    kSUBMENU_CONTACTUS             ,
    kSUBMENU_TNC                   ,
    kSUBMENU_PRIVACYPOLICY         ,
    
    //My Customers (with some methods from above)
    //search vehicle ID
    //register product code
    kSUBMENU_INVITECUSTOMER        = 400,
    kSUBMENU_SCANDECAL             ,
    kSUBMENU_ACTIVATEDECAL,
    //customer redemption
    
    kSUBMENU_MANAGEWORKSHOP_TAG             = 1000,
    kSUBMENU_MANAGEWORKSHOP_PROFILE         ,
    kSUBMENU_MANAGEWORKSHOP_ADDNEW          ,
    
    kSUBMENU_PARTICIPATINGPRODUCTS_ALL      ,
    kSUBMENU_PARTICIPATINGPRODUCTS_APPOLO   ,
    
    kSUBMENU_SALESKIT_VALUE                 ,
    kSUBMENU_SALESKIT_POINTS                ,
    kSUBMENU_SALESKIT_SALESMARKETING        ,
    
    kSUBMENU_MANAGEORDERS_NEW               ,
    kSUBMENU_MANAGEORDERS_EDIT              ,
    kSUBMENU_MANAGEORDERS_APPROVE           ,
    
    kSUBMENU_VIEWPROMOTION_CONSUMER         ,
    kSUBMENU_VIEWPROMOTION_TRADE            ,
    
    kSUBMENU_SCANBOTTLE                     ,
    
    
//    kSUBMENU_,
    
    
    kSUBMENU_WORKSHOP_SEARCH = 0,
} kSUBMENU_TYPES;


@interface SideMenuViewController () <WebServiceManagerDelegate, UITableViewDataSource, UITableViewDelegate>


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *numberBackgroundConstraint;
@property (weak, nonatomic) IBOutlet UIView *numberBackgroundView;
@property (weak, nonatomic) IBOutlet UILabel *lblNumber;

@property (weak, nonatomic) IBOutlet UITableView *sideMenuTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainTableviewTrailingConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *notificationButtonOffset;
@property (weak, nonatomic) IBOutlet UIButton *btnNotification;


@property (weak, nonatomic) IBOutlet UITableView *subMenuTableView;
@property NSMutableArray *menuArray;
@property NSMutableArray *subMenuArray;

@property NSIndexPath *previousRowSelected;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property UIButton *lastButtonPressed;

@property (weak, nonatomic) IBOutlet UILabel *lblUserFullName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserCompany;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserProfilePhoto;

@property NSInteger currentMenuIndex;
@end

@implementation SideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.scrollView layoutIfNeeded];
    
    //SIDE PANEL HAS TO HIDE NAV BAR
    self.previousRowSelected = 0;
    
    [self.sideMenuTableView registerNib:[UINib nibWithNibName:@"SideMenuTableViewCell" bundle:nil] forCellReuseIdentifier:@"SideMenuTableViewCell"];
    
    [self.subMenuTableView registerNib:[UINib nibWithNibName:@"SideMenuTableViewCell" bundle:nil] forCellReuseIdentifier:@"SideMenuTableViewCell"];
    
    self.sideMenuTableView.tableFooterView = [UIView new];
    self.subMenuTableView.tableFooterView = [UIView new];
    
    self.mainTableviewTrailingConstraint.constant = self.tableviewOffset;
    self.notificationButtonOffset.constant = self.mainTableviewTrailingConstraint.constant;
    [self setupInterface];
    
//    [self setupMenu];
    
    [self updateProfile];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveLanguageChangedNotification:)
                                                 name:kNotificationLanguageChanged
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateProfile)
                                                 name: @"UpdateProfileInfo"
                                               object:nil];
}

- (void) receiveLanguageChangedNotification:(NSNotification *) notification
{
    if ([notification.name isEqualToString:kNotificationLanguageChanged])
    {
        [self.sideMenuTableView reloadData];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void) updateProfile
{
    //Session will have a reference to side menu.
    //update from ProfileInfo Model class.
    self.lblUserFullName.text = [[mSession profileInfo] fullName];
    self.lblUserCompany.text = [[mSession profileInfo] companyName];

//    NSString *urlString = [[[mSession profileInfo] profileImage] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
//    [self.imgUserProfilePhoto sd_setImageWithURL:[NSURL URLWithString: urlString]
//                                placeholderImage:[UIImage imageNamed:@"icon-profile"]
//                                         options:SDWebImageRefreshCached];
//    [self setupMenu];
    
    //setup notification
//    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_UNITEDKINGDOM])
//    {
//        //only UK is hiding notification
//        [self.numberBackgroundView setHidden: YES];
//        [self.lblNumber setHidden: YES];
//        self.btnNotification.hidden = YES;
//    }
//    else
//    {
////        self.btnNotification.hidden = NO;
//        
//        if ([[mSession profileInfo] unReadPushMessage] > 0)
//        {
//            [self.lblNumber setHidden: NO];
//            
//            self.lblNumber.font = FONT_B3;
//            
//            if ([[mSession profileInfo] unReadPushMessage] > 99)
//                [self.lblNumber setText: @"99+"];
//            else
//                [self.lblNumber setText: @([[mSession profileInfo] unReadPushMessage]).stringValue];
//            [self.lblNumber sizeToFit];
//            [self.view layoutIfNeeded];
//            [self.view setNeedsLayout];
//            
//            if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
//                self.numberBackgroundConstraint.constant = 18;
//            else
//                self.numberBackgroundConstraint.constant = 16;
//            
//            self.numberBackgroundView.layer.cornerRadius = self.numberBackgroundView.frame.size
//            .height / 2.0;
//            self.numberBackgroundView.backgroundColor = COLOUR_RED;
//        }
//        else
//        {
//            [self.lblNumber setHidden: YES];
//            [self.numberBackgroundView setHidden: YES];
//        }
//    }
    
    [self setupNewMenu];
}

-(void) setupNewMenu
{
    self.menuArray = [[NSMutableArray alloc] init];
    self.subMenuArray = [[NSMutableArray alloc] init];
    switch (GET_PROFILETYPE) {
        case kPROFILETYPE_DSR:
            if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_UNITEDKINGDOM])
                [self setupDSRMenuUK];
            else
                [self setupDSRMenu];
            break;
        case kPROFILETYPE_TRADEOWNER:
            [self setupTradeOwnerMenu];
            break;
        case kPROFILETYPE_MECHANIC:
            [self setupMechanicMenu];
            break;
        case kPROFILETYPE_FORECOURTATTENDANT:
            [self setupFAMenu];
            break;
        case kPROFILETYPE_DMR:
            [self setupDMRMenu];
            break;
        case kPROFILETYPE_DSM:
            [self setupDSMMenu];
            break;
        default:
            [self setupDefaultMenu];
            break;
    }
    
    [self.sideMenuTableView reloadData];
}

-(void) setupDSRMenuUK
{
    [self.menuArray addObject: @(kSIDEMENU_HOME)];
    [self.menuArray addObject: @(kSIDEMENU_HELPANDSUPPORT)];
    [self.menuArray addObject: @(kSIDEMENU_WORKSHOPPROFILING)];
    [self.menuArray addObject: @(kSIDEMENU_SETTINGS)];
}

- (void) setupInterface
{
    self.lblUserFullName.font = FONT_H1;
    self.lblUserFullName.textColor = COLOUR_DARKGREY;
    
    self.lblUserCompany.font = FONT_B2;
    self.lblUserCompany.textColor = COLOUR_MIDGREY;
    
//    [self.imgUserProfilePhoto setBackgroundColor: COLOUR_YELLOW];
    self.imgUserProfilePhoto.layer.cornerRadius = self.imgUserProfilePhoto.frame.size.width / 2;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setSelectedMenu: (UIButton *) btn
{
    self.lastButtonPressed.enabled = YES;
    self.lastButtonPressed.selected = NO;
    self.lastButtonPressed.backgroundColor = COLOUR_WHITE;
    
    self.lastButtonPressed = btn;
    self.lastButtonPressed.backgroundColor = COLOUR_YELLOW;
    self.lastButtonPressed.enabled = NO;
    self.lastButtonPressed.selected = YES;
}

-(void) setButtonsFont: (UIButton *) btn
{
    btn.titleLabel.font = FONT_B(20);
    [btn setTintColor: COLOUR_DARKGREY];
}

//enum
//{
//    kUSER_DMM           = 1 << 1,
//    kUSER_DSM           = 1 << 2,
//    kUSER_DSR           = 1 << 3,
//    kUSER_TRADEOWNER    = 1 << 4,
//    kUSER_MECHANIC      = 1 << 5,
//
//} kUSER_TYPE;
-(BOOL) checkMyWorkshopModulesSubMenu
{
    if (![[mSession profileInfo] hasSearchVehicleID] &&
        ![[mSession profileInfo] canInviteTrade] &&
        ![[mSession profileInfo] hasRegisterProductCode_V2] &&
        ![[mSession profileInfo] hasMyRewards] &&
        ![[mSession profileInfo] hasCustomerRedemption] &&
        ![[mSession profileInfo] hasParticipatingProducts])
        return NO; //if all flag fails
    else
        return YES;
}

-(void) setupMyWorkshopModulesSubMenu
{
    //include in all submenu setups
    [self.subMenuArray removeAllObjects];
    [self.subMenuArray addObject: @(kSUBMENU_BACK)];
    
    if ([[mSession profileInfo] hasSearchVehicleID])
        [self.subMenuArray addObject: @(kSUBMENU_SEARCHVEHICLEID)];
    if ([[mSession profileInfo] canInviteTrade])
        [self.subMenuArray addObject: @(kSUBMENU_INVITETRADE)];
    if([[mSession profileInfo] hasOilChangeWalkThrough])
        [self.subMenuArray addObject: @(kSUBMENU_OILCHANGEWALKTHROUGH)];
    if ([[mSession profileInfo] hasRegisterProductCode_V2])
        [self.subMenuArray addObject: @(kSUBMENU_REGISTERPRODUCTCODE)];
    if ([[mSession profileInfo] hasMyRewards] && !IS_COUNTRY(COUNTRYCODE_RUSSIA))
        [self.subMenuArray addObject: @(kSUBMENU_MYREWARDS)];
    if ([[mSession profileInfo] hasCustomerRedemptionDSR])
        [self.subMenuArray addObject: @(kSUBMENU_CUSTOMERREDEMPTION)];
    if ([[mSession profileInfo] hasParticipatingProducts])
        [self.subMenuArray addObject: @(kSUBMENU_PARTICIPATINGPRODUCTS)];
    [self.subMenuTableView reloadData];
    [self showSubmenu];
}

-(void) setupMyMechanicModulesSubMenu
{
    //include in all submenu setups
    [self.subMenuArray removeAllObjects];
    [self.subMenuArray addObject: @(kSUBMENU_BACK)];
    
    if([[mSession profileInfo] hasOilChangeWalkThrough]) {
        [self.subMenuArray addObject:@(kSUBMENU_OILCHANGEWALKTHROUGH)];
    }
    
    if ([[mSession profileInfo] hasRegisterProductCode_V2])
    {
        [self.subMenuArray addObject: @(kSUBMENU_REGISTERPRODUCTCODE)];
    }
    
    if ([[mSession profileInfo] hasMyRewards])
        [self.subMenuArray addObject: @(kSUBMENU_MYREWARDS)];
    if ([[mSession profileInfo] hasParticipatingProducts])
        [self.subMenuArray addObject: @(kSUBMENU_PARTICIPATINGPRODUCTS)];
    [self.subMenuTableView reloadData];
    [self showSubmenu];
}

-(void) setupHelpAndSupportSubMenu
{
    //include in all submenu setups
    [self.subMenuArray removeAllObjects];
    [self.subMenuArray addObject: @(kSUBMENU_BACK)];
    
    
    if (![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_UNITEDKINGDOM] &&
        ![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA] && ![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_TURKEY])
    {
        [self.subMenuArray addObject: @(kSUBMENU_FAQ)];
    }
    
    [self.subMenuArray addObject: @(kSUBMENU_TNC)];
    [self.subMenuArray addObject: @(kSUBMENU_PRIVACYPOLICY)];
    
    if (![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_UNITEDKINGDOM])
    {
        [self.subMenuArray addObject: @(kSUBMENU_CONTACTUS)];
    }
    
    [self.subMenuTableView reloadData];
    [self showSubmenu];
}

-(BOOL) checkMyCustomerSubMenu
{
    if (![[mSession profileInfo] redeemOnly])
    {
        if (![[mSession profileInfo] hasSearchVehicleID] &&
            ![[mSession profileInfo] hasRegisterProductCode_V2] &&
            ![[mSession profileInfo] hasCustomerRedemption] &&
            ![[mSession profileInfo] hasDecal] &&
            ![[mSession profileInfo] hasTagDecalToWorkshop] &&
            ![[mSession profileInfo] hasInviteConsumer] &&
            ![[mSession profileInfo] hasLubeMatch])
            return NO; //if all flag fails
        else
            return YES;
    }
    else
    {
        if (![[mSession profileInfo] hasCustomerRedemption] &&
            ![[mSession profileInfo] hasDecal] &&
            ![[mSession profileInfo] hasTagDecalToWorkshop] &&
            ![[mSession profileInfo] hasInviteConsumer] &&
            ![[mSession profileInfo] hasLubeMatch])
            return NO; //if all flag fails
        else
            return YES;
    }
  
}

-(void) setupTradeMyCustomer
{
    //include in all submenu setups
    [self.subMenuArray removeAllObjects];
    [self.subMenuArray addObject: @(kSUBMENU_BACK)];
    
    if(IS_COUNTRY(COUNTRYCODE_INDIA)) {
        if([[[mSession loadUserProfile] companyType] isEqualToString:@"16"]) {
            [self.subMenuArray addObject:@(kSUBMENU_MYNETWORK)];
        }
    }
    
    if ([[mSession profileInfo] hasSearchVehicleID])
        [self.subMenuArray addObject: @(kSUBMENU_SEARCHVEHICLEID)];

    if ([[mSession profileInfo] hasRegisterProductCode_V2])
    {
        [self.subMenuArray addObject: @(kSUBMENU_REGISTERPRODUCTCODE)];
    }
    
    if ([[mSession profileInfo] hasLubeMatch])
    {
        [self.subMenuArray addObject: @(kSUBMENU_LUBEMATCH)];
    }
    
    if([[mSession profileInfo] hasBottleSale]) {
        [self.subMenuArray addObject: @(kSUBMENU_SCANBOTTLE)];
    }
    
    if ([[mSession profileInfo] hasCustomerRedemption])
        [self.subMenuArray addObject: @(kSUBMENU_CUSTOMERREDEMPTION)];
    if ([[mSession profileInfo] hasDecal])
        [self.subMenuArray addObject: @(kSUBMENU_SCANDECAL)];
    
    if ([[mSession profileInfo] hasTagDecalToWorkshop])
        [self.subMenuArray addObject: @(kSUBMENU_ACTIVATEDECAL)];
    if ([[mSession profileInfo] hasInviteConsumer])
        [self.subMenuArray addObject: @(kSUBMENU_INVITECUSTOMER)];
    
    
//    if ([[mSession profileInfo] hasAncillaryOffers])
//        [self.subMenuArray addObject: @(kSIDEMENU_WORKSHOPOFFER)]; //shifted to main menu
    
    [self.subMenuTableView reloadData];
    [self showSubmenu];
}

-(void) setupMechMyCustomer
{
    //include in all submenu setups
    [self.subMenuArray removeAllObjects];
    [self.subMenuArray addObject: @(kSUBMENU_BACK)];
    
    if (![[mSession profileInfo] redeemOnly])
    {
        if ([[mSession profileInfo] hasSearchVehicleID])
            [self.subMenuArray addObject: @(kSUBMENU_SEARCHVEHICLEID)];
        
        if ([[mSession profileInfo] hasRegisterProductCode_V2])
        {
            [self.subMenuArray addObject: @(kSUBMENU_REGISTERPRODUCTCODE)];
        }
        
        if ([[mSession profileInfo] hasLubeMatch])
        {
            [self.subMenuArray addObject: @(kSUBMENU_LUBEMATCH)];
        }
    }
    
    
    if ([[mSession profileInfo] hasDecal])
         [self.subMenuArray addObject: @(kSUBMENU_SCANDECAL)];    
    if ([[mSession profileInfo] hasTagDecalToWorkshop])
        [self.subMenuArray addObject: @(kSUBMENU_ACTIVATEDECAL)];

    if ([[mSession profileInfo] hasInviteConsumer])
        [self.subMenuArray addObject: @(kSUBMENU_INVITECUSTOMER)];
    if ([[mSession profileInfo] hasCustomerRedemption])
        [self.subMenuArray addObject: @(kSUBMENU_CUSTOMERREDEMPTION)];
    [self.subMenuTableView reloadData];
    [self showSubmenu];
}

#pragma mark setup Menu
-(void) setupDSRMenu
{
      NSLog(@"newsArray ----- setupDSRMenu %@",[[mSession lookupTable] objectForKey: @"ContactUSCSNumber"]);
    [self.menuArray addObject: @(kSIDEMENU_HOME)];
    
    if(IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [self.menuArray addObject: @(kSIDEMENU_MYNETWORK)];
    }
    else
    {
        if (!IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
            [self.menuArray addObject: @(kSIDEMENU_DSRMANAGEWORKSHOP)];
        }
    }
    
    if([[mSession profileInfo] hasInventoryManagement])
        [self.menuArray addObject:@(kSIDEMENU_DSR_INVENTORYMANAGEMENT)];

    if([[mSession profileInfo] HasInventory])
       [self.menuArray addObject:@(kSIDEMENU_DSR_HasInventory)];

    if ([[mSession profileInfo] hasCashIncentive])
        [self.menuArray addObject:@(kSIDEMENU_CASHINCENTIVE)];
    
    if ([[mSession profileInfo] hasPartnersOfShellClub])
        [self.menuArray addObject: @(kSIDEMENU_PARTNERSOFSHELLCLUB)];
    
    if ([[mSession profileInfo] hasOnePlusOne])
        [self.menuArray addObject:@(kSIDEMENU_SPIRAXGADUSONEPLUSONE)];


    if ([[mSession profileInfo] hasLoyaltyProgram])
        [self.menuArray addObject: @(kSIDEMENU_LOYALTYREWARDS)];
    
    if ([[mSession profileInfo] hasDRWorkshopPerformance])
        [self.menuArray addObject: @(kSIDEMENU_DSRWORKSHOPPERFORMANCE)];
    
    if ([[mSession profileInfo] hasManageMechanicPerformance])
        [self.menuArray addObject: @(kSIDEMENU_IN_MECHPERFORMANCE)];
    
    if ([[mSession profileInfo]  hasRUPerformance])
        [self.menuArray addObject: @(kSIDEMENU_RU_DSRWORKSHOPPERFORMANCE)];
    
    if ([[mSession profileInfo] hasMyRewards] && IS_COUNTRY(COUNTRYCODE_RUSSIA))
        [self.menuArray addObject: @(kSIDEMENU_MYREWARDS)];
    
    
    if ([[mSession profileInfo] hasOrdering_V2])
        [self.menuArray addObject: @(kSIDEMENU_DSRMANAGEORDER)];
    
    
    if ([[mSession profileInfo] hasMyLibrary])
        [self.menuArray addObject: @(kSIDEMENU_MYLIBRARY)];
    
    //check if sub-modules available, don't add if none
//    if ([self checkMyWorkshopModulesSubMenu])
//        [self.menuArray addObject: @(kSIDEMENU_WORKSHOPMODULES)];
    
    if ([[mSession profileInfo] isEnableSurvey])
        [self.menuArray addObject: @(kSIDEMENU_SURVEY)];
    
    if([[mSession profileInfo] hasDistributorAcademy])
        [self.menuArray addObject: @(kSIDEMENU_SDA)];
    
    if([[mSession profileInfo] hasWorkshopAcademy])
        [self.menuArray addObject: @(kSIDEMENU_SWA)];
    
    if([[mSession profileInfo] isEnableBooking])
    [self.menuArray addObject: @(kSIDEMENU_EBOOKING)];
    
    [self.menuArray addObject: @(kSIDEMENU_HELPANDSUPPORT)];
    //add to help and support later
    
    [self.menuArray addObject: @(kSIDEMENU_SETTINGS)];
    
    NSLog(@"ARRAY VALUE FIND:%@",self.menuArray);
}
-(void) setupTradeOwnerMenu
{
    [self.menuArray addObject: @(kSIDEMENU_HOME)];
    
    if ([self checkMyCustomerSubMenu])
        [self.menuArray addObject: @(kSIDEMENU_TRADEMYCUSTOMERS)];
    
    if ([[mSession profileInfo] hasLoyaltyProgram])
        [self.menuArray addObject: @(kSIDEMENU_LOYALTYREWARDS)];

    if ([[mSession profileInfo] hasOnePlusOne])
        [self.menuArray addObject:@(kSIDEMENU_TRADESPIRAXGADUSONEPLUSONE)];

    if([[mSession profileInfo] hasInventoryManagement] )
        [self.menuArray addObject:@(kSIDEMENU_TO_INVENTORYMANAGEMENT)];

    if([[mSession profileInfo] HasInventory])
        [self.menuArray addObject:@(kSIDEMENU_TO_HasInventory)];

    if ([[mSession profileInfo] hasCashIncentive])
        [self.menuArray addObject:@(kSIDEMENU_CASHINCENTIVE)];
    
    if ([[mSession profileInfo] hasMyRewards])
        [self.menuArray addObject: @(kSIDEMENU_MYREWARDS)];
    
    if ([[mSession profileInfo] hasManageMyStaff])
        [self.menuArray addObject: @(kSIDEMENU_TRADEMANAGESTAFF)];
    
    if (([[mSession profileInfo]  hasMYPerformance] ||
        [[mSession profileInfo]  hasMYPoints]) && !(IS_COUNTRY(COUNTRYCODE_RUSSIA)))
        [self.menuArray addObject: @(kSIDEMENU_TRADEWORKSHOPPERFORMANCE)];
    
    if ([[mSession profileInfo]  hasRUPerformance])
        [self.menuArray addObject: @(kSIDEMENU_RU_TRADEWORKSHOPPERFORMANCE)];
    
    if ([[mSession profileInfo] hasPartnersOfShellClub])
        [self.menuArray addObject: @(kSIDEMENU_PARTNERSOFSHELLCLUB)];
    
    if ([[mSession profileInfo] hasAncillaryOffers])
        [self.menuArray addObject: @(kSIDEMENU_WORKSHOPOFFER)];
    
    if ([[mSession profileInfo] hasMyLibrary])
        [self.menuArray addObject: @(kSIDEMENU_MYLIBRARY)];
    
    if ([[mSession profileInfo] hasOrdering_V2])
        [self.menuArray addObject: @(kSIDEMENU_TRADEMANAGEORDER)];
    
    if ([[mSession profileInfo] hasParticipatingProducts])
        [self.menuArray addObject: @(kSIDEMENU_PARTICIPATINGPRODUCTS)];
    
    if ([[mSession profileInfo] isEnableSurvey])
        [self.menuArray addObject: @(kSIDEMENU_SURVEY)];
    
    if([[mSession profileInfo] hasWorkshopAcademy])
        [self.menuArray addObject: @(kSIDEMENU_SWA)];
    
    if([[mSession profileInfo] isEnableBooking])
    [self.menuArray addObject:@(kSIDEMENU_TO_EBOOKING)];
    
    [self.menuArray addObject: @(kSIDEMENU_HELPANDSUPPORT)];
    [self.menuArray addObject: @(kSIDEMENU_SETTINGS)];
}

-(void) setupMechanicMenu
{
    [self.menuArray addObject: @(kSIDEMENU_HOME)];
    
    if ([[mSession profileInfo] hasCashIncentive])
        [self.menuArray addObject:@(kSIDEMENU_CASHINCENTIVE)];
    
    if ([[mSession profileInfo] hasOnePlusOne])
        [self.menuArray addObject:@(kSIDEMENU_MECHSPIRAXGADUSONEPLUSONE)];

    if([[mSession profileInfo] HasInventory])
        [self.menuArray addObject:@(kSIDEMENU_TO_HasInventory)];

    if ([[mSession profileInfo] hasMyRewards])
        [self.menuArray addObject: @(kSIDEMENU_MYREWARDS)];
    
    if ([[mSession profileInfo] hasMyLibrary])
        [self.menuArray addObject: @(kSIDEMENU_MYLIBRARY)];
    
    if ([[mSession profileInfo]  hasMYPerformance] ||
        [[mSession profileInfo]  hasMYPoints])
        [self.menuArray addObject: @(kSIDEMENU_MECHPERFORMANCE)];
    
    if ([self checkMyCustomerSubMenu])
        [self.menuArray addObject: @(kSIDEMENU_MECHMYCUSTOMERS)];
    
    if ([[mSession profileInfo] hasParticipatingProducts])
        [self.menuArray addObject: @(kSIDEMENU_PARTICIPATINGPRODUCTS)];
    
    if ([[mSession profileInfo] isEnableSurvey])
        [self.menuArray addObject: @(kSIDEMENU_SURVEY)];
    
    if([[mSession profileInfo] hasWorkshopAcademy])
        [self.menuArray addObject: @(kSIDEMENU_SWA)];
    
    [self.menuArray addObject: @(kSIDEMENU_HELPANDSUPPORT)];
    [self.menuArray addObject: @(kSIDEMENU_SETTINGS)];
}

-(void) setupFAMenu {
    [self.menuArray addObject: @(kSIDEMENU_HOME)];
    
    if ([[mSession profileInfo] hasMyRewards])
        [self.menuArray addObject: @(kSIDEMENU_MYREWARDS)];
    
    if ([[mSession profileInfo] hasOnePlusOne])
        [self.menuArray addObject:@(kSIDEMENU_FASPIRAXGADUSONEPLUSONE)];

    if ([[mSession profileInfo] hasMyLibrary])
        [self.menuArray addObject: @(kSIDEMENU_MYLIBRARY)];
    
    if ([[mSession profileInfo]  hasMYPerformance] ||
        [[mSession profileInfo]  hasMYPoints])
        [self.menuArray addObject: @(kSIDEMENU_MECHPERFORMANCE)];
    
    if ([self checkMyCustomerSubMenu])
        [self.menuArray addObject: @(kSIDEMENU_MECHMYCUSTOMERS)];
    
    if ([[mSession profileInfo] hasParticipatingProducts])
        [self.menuArray addObject: @(kSIDEMENU_PARTICIPATINGPRODUCTS)];
    
    if ([[mSession profileInfo] isEnableSurvey])
        [self.menuArray addObject: @(kSIDEMENU_SURVEY)];
    
    if([[mSession profileInfo] hasWorkshopAcademy])
        [self.menuArray addObject: @(kSIDEMENU_SWA)];
    
    [self.menuArray addObject: @(kSIDEMENU_HELPANDSUPPORT)];
    [self.menuArray addObject: @(kSIDEMENU_SETTINGS)];
    
}

-(void) setupDMRMenu
{
    [self.menuArray addObject: @(kSIDEMENU_HOME)];
    
    if ([[mSession profileInfo] hasManageMechanicPerformance])
        [self.menuArray addObject: @(kSIDEMENU_IN_MECHPERFORMANCE)];
    
    if ([[mSession profileInfo] hasOnePlusOne])
        [self.menuArray addObject:@(kSIDEMENU_IN_MECHSPIRAXGADUSONEPLUSONE)];

    if ([[mSession profileInfo] hasManageMyStaff])
        [self.menuArray addObject: @(kSIDEMENU_IN_MANAGEMECHANICS)];
    
    if ([[mSession profileInfo] hasOrdering_V2])
          [self.menuArray addObject: @(kSIDEMENU_DSRMANAGEORDER)];
            
    if ([[mSession profileInfo] hasMyLibrary])
        [self.menuArray addObject: @(kSIDEMENU_MYLIBRARY)];
    
    [self.menuArray addObject: @(kSIDEMENU_IN_MYMECHMODULES)];
    
    if ([[mSession profileInfo] hasCashIncentive])
        [self.menuArray addObject:@(kSIDEMENU_CASHINCENTIVE)];
        
    if([[mSession profileInfo] hasDistributorAcademy])
        [self.menuArray addObject: @(kSIDEMENU_SDA)];
    
    if([[mSession profileInfo] hasWorkshopAcademy])
        [self.menuArray addObject: @(kSIDEMENU_SWA)];
    
    [self.menuArray addObject: @(kSIDEMENU_HELPANDSUPPORT)];
    [self.menuArray addObject: @(kSIDEMENU_SETTINGS)];
}

-(void) setupDSMMenu
{
    [self.menuArray addObject: @(kSIDEMENU_HOME)];
    
    if ([[mSession profileInfo] hasPartnersOfShellClub])
        [self.menuArray addObject: @(kSIDEMENU_PARTNERSOFSHELLCLUB)];
    
    if ([[mSession profileInfo] hasOnePlusOne])
    [self.menuArray addObject:@(kSIDEMENU_SPIRAXGADUSONEPLUSONE)];

    if (([[mSession profileInfo]  hasRUPerformance] ||
        [[mSession profileInfo]  hasMYPoints]))
        [self.menuArray addObject: @(kSIDEMENU_TRADEWORKSHOPPERFORMANCE)];

    if ([[mSession profileInfo] hasMyRewards])
        [self.menuArray addObject: @(kSIDEMENU_MYREWARDS)];
            
    if ([[mSession profileInfo] hasMyLibrary])
        [self.menuArray addObject: @(kSIDEMENU_MYLIBRARY)];
        
    if([[mSession profileInfo] hasDistributorAcademy])
        [self.menuArray addObject: @(kSIDEMENU_SDA)];
    
    if([[mSession profileInfo] hasWorkshopAcademy])
        [self.menuArray addObject: @(kSIDEMENU_SWA)];
    
    [self.menuArray addObject: @(kSIDEMENU_HELPANDSUPPORT)];
    [self.menuArray addObject: @(kSIDEMENU_SETTINGS)];
}


-(void) setupDefaultMenu
{
    if ([[mSession profileInfo] hasOnePlusOne])
    [self.menuArray addObject:@(kSIDEMENU_COMMONSPIRAXGADUSONEPLUSONE)];

    [self.menuArray addObject: @(kSIDEMENU_HOME)];
    [self.menuArray addObject: @(kSIDEMENU_HELPANDSUPPORT)];
    [self.menuArray addObject: @(kSIDEMENU_SETTINGS)];

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.sideMenuTableView)
        return [self.menuArray count];
    else
        return [self.subMenuArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SideMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"SideMenuTableViewCell"];

    cell.imgSubMenuArrow.hidden = YES;
    
//    [cell.numberBackgroundView setHidden: YES];
    [cell reloadFont];
    
#pragma mark Main Side Menu UI Setup
    if (tableView == self.sideMenuTableView)
    {
        
        if (kIsRightToLeft) {
            cell.imgSubMenuArrow.image = [UIImage imageNamed:@"icon-arrsingleL.png"];
        } else {
            cell.imgSubMenuArrow.image = [UIImage imageNamed:@"icon-arrsingleR.png"];
        }
        
        //get enum value assigned to button
        NSInteger menuIndex = [[self.menuArray objectAtIndex: indexPath.row] intValue];
        switch (menuIndex)
        {
#pragma mark DSR MENUS
            case kSIDEMENU_DSR_INVENTORYMANAGEMENT:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_inventory_midgrey.png"]];
                [cell.lblTitle setText: LOCALIZATION(C_TITLE_INVENTORY)];
                break;
            case kSIDEMENU_DSR_HasInventory:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_inventory_midgrey.png"]];
                [cell.lblTitle setText: LOCALIZATION(C_TITLE_INVENTORY)];
                break;
            case kSIDEMENU_PARTNERSOFSHELLCLUB:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon__partnersofshell_midgrey.png"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_PARTNERS)]; //lokalise 17 jan
                break;
            case kSIDEMENU_SPIRAXGADUSONEPLUSONE:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_performance_midgrey.png"]];
                [cell.lblTitle setText: LOCALIZATION(TITLE_SPIRAX_GADUS_ONE_PLUS_ONE)]; //lokalise 17 jan
                break;
            case kSIDEMENU_WORKSHOPOFFER:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_workshopoffers_midgrey.png"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_WORKSHOPOFFER)];  //lokalise 17 jan
                break;
//            case kSIDEMENU_WORKSHOPMODULES:
//                cell.imgSubMenuArrow.hidden = NO;
//                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_myworkshopmodules_midgrey.png"]];
//                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_MYWORKSHOPMODULES)];    //lokalise 17 jan
//                break;
            case kSIDEMENU_HELPANDSUPPORT:
                cell.imgSubMenuArrow.hidden = NO;
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_helpsupportfaq_midgrey.png"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_HELPANDSUPPORT)];       //lokalise 17 jan
                break;
            case kSIDEMENU_DSRMANAGEWORKSHOP:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_manageworkshops_midgrey.png"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_MANAGEMYWORKSHOP)];   //lokalise 17 jan
                break;
                
            case kSIDEMENU_DSRWORKSHOPPERFORMANCE:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_performance_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_ANALYTICS)];   //lokalise 17 jan
                break;
            case kSIDEMENU_RU_DSRWORKSHOPPERFORMANCE:
//                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon__partnersofshell_midgrey.png"]];
//                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_PARTNERS)];   //lokalise 17 jan
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_performance_midgrey.png"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_PARTNERS)];
                break;
                
            case kSIDEMENU_TRADEMYCUSTOMERS:
                cell.imgSubMenuArrow.hidden = NO;
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_mycustomer_midgrey.png"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_MYCUSTOMERS)];   //lokalise 17 jan
                break;
            case kSIDEMENU_MECHMYCUSTOMERS:
                cell.imgSubMenuArrow.hidden = NO;
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_mycustomer_midgrey.png"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_MYCUSTOMERS )];   //lokalise 17 jan
                break;
            case kSIDEMENU_DSRMANAGEORDER:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_manageorder_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_MANAGEORDER)];   //lokalise 17 jan
                break;
            case kSIDEMENU_WORKSHOPPROFILING:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_manageworkshops_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_TITLE_APPROVEDWORKSHOPPROFILING)];       //lokalised
//                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_PROFILING)];
                break;
            case kSIDEMENU_MYNETWORK:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_managestaff_midgrey"]];
                [cell.lblTitle setText:LOCALIZATION(C_SIDEMENU_MYNETWORK)];        //lokalise 17 jan
                break;
            case kSIDEMENU_LOYALTYREWARDS:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon__partnersofshell_midgrey.png"]];
                [cell.lblTitle setText: LOCALIZATION(C_LOYALTY_LOYALTYPROGRAM)];    //llokalised
                break;
            case kSIDEMENU_EBOOKING:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon__partnersofshell_midgrey.png"]];
                [cell.lblTitle setText: @"EBooking"];    //llokalised
                break;
        
#pragma mark OWNER MENUS
                
            case kSIDEMENU_TO_EBOOKING:
                
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon__partnersofshell_midgrey.png"]];
                [cell.lblTitle setText: @"EBooking"];    //llokalised
                break;
                
            case kSIDEMENU_TO_INVENTORYMANAGEMENT:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_inventory_midgrey.png"]];
                [cell.lblTitle setText: LOCALIZATION(C_TITLE_INVENTORY)];
                break;
            case kSIDEMENU_TO_HasInventory:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_inventory_midgrey.png"]];
                [cell.lblTitle setText: LOCALIZATION(C_TITLE_INVENTORY)];
                 break;
            case kSIDEMENU_CASHINCENTIVE:
                //TODO: set to correct values
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_cash_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_DRAWER_CASHINCENTIVE)];
                break;
                
            case kSIDEMENU_TRADESPIRAXGADUSONEPLUSONE:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon__partnersofshell_midgrey.png"]];
                [cell.lblTitle setText: LOCALIZATION(TITLE_SPIRAX_GADUS_ONE_PLUS_ONE)]; //lokalise 17 jan
                break;

            case kSIDEMENU_TRADEMANAGESTAFF:
            {
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_managestaff_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_MANAGESTAFF)];  //lokalise 17 jan
            }
                break;
            case kSIDEMENU_TRADEMANAGEORDER:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_manageorder_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_MANAGEORDER) ];  //lokalise 17 jan
                break;

            case kSIDEMENU_TRADEWORKSHOPPERFORMANCE:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_performance_midgrey"]];
                if (GET_ISIMPROFILETYPE) //TO profile and company type 2048 is India iM
                    [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_MYPERFORMANCE)]; //lokalise 17 jan
                else
                    [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_WORKSHOPPERFORMANCE)]; //lokalise 17 jan
                
//                }
                break;
            case kSIDEMENU_RU_TRADEWORKSHOPPERFORMANCE:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_performance_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_WORKSHOPPERFORMANCE) ]; //lokalise 17 jan
                break;
                
            case kSIDEMENU_MECHPERFORMANCE:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_performance_midgrey.png"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_MYPERFORMANCE)]; //lokalise 17 jan
                break;
                
            case kSIDEMENU_FAREGISTERPRODUCTCODE:
                [cell.imgMenuIcon setImage:[UIImage imageNamed:@"sideicon_registerproductcode_midgrey.png"]];
                [cell.lblTitle setText:LOCALIZATION(C_SIDEMENU_SCANPRODUCTCODE)]; //lokalise 17 jan
                break;
                
            
            
                
#pragma mark DMR MENUS
            case kSIDEMENU_IN_MECHPERFORMANCE:
                [cell.imgMenuIcon setImage:[UIImage imageNamed:@"sideicon_performance_midgrey.png"]];
                if (GET_PROFILETYPE == kPROFILETYPE_DMR)
                    [cell.lblTitle setText:LOCALIZATION(C_SIDEMENU_MECHPERFORMANCE)];  //lokalise 17 jan
                else
                    [cell.lblTitle setText:LOCALIZATION(C_SIDEMENU_ANALYTICS)];  //lokalise 17 jan
                break;

            case kSIDEMENU_IN_MECHSPIRAXGADUSONEPLUSONE:
                    [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon__partnersofshell_midgrey.png"]];
                    [cell.lblTitle setText: LOCALIZATION(TITLE_SPIRAX_GADUS_ONE_PLUS_ONE)]; //lokalise 17 jan
                    break;

            case kSIDEMENU_IN_MANAGEMECHANICS:
                [cell.imgMenuIcon setImage:[UIImage imageNamed:@"sideicon_managestaff_midgrey.png"]];
                [cell.lblTitle setText:LOCALIZATION(C_SIDEMENU_MANAGEMYMECH)];  //lokalise 17 jan
                break;
            case kSIDEMENU_IN_MYMECHMODULES:
                cell.imgSubMenuArrow.hidden = NO;
                [cell.imgMenuIcon setImage:[UIImage imageNamed:@"sideicon_myworkshopmodules_midgrey.png"]];
                [cell.lblTitle setText:LOCALIZATION(C_SIDEMENU_MECHMODULES)];   //lokalised
                break;
                
                
#pragma mark COMMON MENUS
            case kSIDEMENU_HOME:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_home_midgrey.png"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_HOME) ];        //lokalise 17 jan
                break;
            case kSIDEMENU_MYLIBRARY:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_mylibrary_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_MYLIBRARY) ];   //lokalise 17 jan
                break;
            case kSIDEMENU_SURVEY:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_survey_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_SURVEY)];       //lokalise 17 jan
                break;
            case kSIDEMENU_SETTINGS:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_settings_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_SETTINGS)];    //lokalise 17 jan
                break;
            case kSIDEMENU_MYREWARDS:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_myrewards_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_MYREWARDS) ];    //lokalise 17 jan
                break;
            case kSIDEMENU_PARTICIPATINGPRODUCTS:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_participatingproducts_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_PARTICIPATINGPRODUCTS) ];   //lokalise 17 jan
                break;
            case kSIDEMENU_SDA:
                [cell.lblTitle setText: LOCALIZATION(C_DRAWER_SDA)];
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_swa_midgrey"]];
                break;
            case kSIDEMENU_SWA:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_swa_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_DRAWER_SWA)];
                break;
            case kSIDEMENU_COMMONSPIRAXGADUSONEPLUSONE:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon__partnersofshell_midgrey.png"]];
                [cell.lblTitle setText: LOCALIZATION(TITLE_SPIRAX_GADUS_ONE_PLUS_ONE)]; //lokalise 17 jan
                break;
        }
    }
#pragma mark Sub Menu UI Setup
    else if (tableView == self.subMenuTableView)
    {
        cell.imgMenuIcon.transform = CGAffineTransformIdentity;
        //get enum value assigned to button
        NSInteger menuIndex = [[self.subMenuArray objectAtIndex: indexPath.row] intValue];
        switch (menuIndex)
        {
            case kSUBMENU_BACK:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_back_midgrey"]];
                if (kIsRightToLeft) {
                    cell.imgMenuIcon.transform = CGAffineTransformMakeScale(-1, 1);
                }
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_BACK)];     //lokalise 17 jan
                break;
                
#pragma mark My Workshop Modules
            case kSUBMENU_SEARCHVEHICLEID:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_searchvehicleid_midgrey"]];
                if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
                    [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_REGOILCHANGE)];   //lokalise 17 jan
                else
                    [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_SEARCHVEHICLEID)];   //lokalise 17 jan
                break;
            case kSUBMENU_INVITETRADE:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_invite_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_INVITETRADE)];   //lokalise 17 jan
                break;
            case kSUBMENU_REGISTERPRODUCTCODE:
               [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_registerproductcode_midgrey.png"]];
                if(IS_COUNTRY(COUNTRYCODE_INDIA)) {
                    [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_REGOILCHANGE) ];   //lokalise 17 jan
                } else {
                    [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_SCANPRODUCTCODE) ];  //lokalise 17 jan
                }
                
                break;
            case kSUBMENU_LUBEMATCH:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_registerproductcode_midgrey.png"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_REGOILCHANGE) ];
                 
                break;
            case kSUBMENU_SCANBOTTLE:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_scanbottle_midgray"]];
                [cell.lblTitle setText: LOCALIZATION(C_QL_SCANBOTTLE)];
                break;
                
            case kSUBMENU_MYREWARDS:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_myrewards_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_MYREWARDS) ];  //lokalise 17 jan
                break;
            case kSUBMENU_CUSTOMERREDEMPTION:    //custmer redemption
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_customerredemption_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_SCANCUSTCODE) ];  //lokalise 17 jan
                break;
            case kSUBMENU_PARTICIPATINGPRODUCTS:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_participatingproducts_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_PARTICIPATINGPRODUCTS) ];  //lokalise 17 jan
                break;
            case kSUBMENU_OILCHANGEWALKTHROUGH:
                [cell.imgMenuIcon setImage:[UIImage imageNamed: @"sideicon_registerproductcode_walkthru_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_REGOILCHANGE)];  //lokalise 17 jan
                break;
                
#pragma mark My Customers
            case kSUBMENU_INVITECUSTOMER:  //share with mechanic
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_invite_midgrey"]];
                //                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_INVITETRADE)];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_INVITECUSTOMER)];  //lokalise 17 jan
                break;
            case kSUBMENU_SCANDECAL:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_scandecal_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_SCANDECAL) ];  //lokalise 17 jan
                break;
            case kSUBMENU_ACTIVATEDECAL:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_activatedecal_midgrey.png"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_ACTIVATEDECAL) ];  //lokalise 17 jan
                break;
            case kSUBMENU_MYNETWORK:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_managestaff_midgrey"]];
                [cell.lblTitle setText:LOCALIZATION(C_SIDEMENU_MYNETWORK)];        //lokalise 17 jan
                break;
#pragma mark Help & Support
            case kSUBMENU_FAQ:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_helpsupportfaq_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_FAQ) ];  //lokalise 17 jan
                break;
            case kSUBMENU_CONTACTUS:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_contactus_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_CONTACTUS)];  //lokalise 17 jan
                break;
            case kSUBMENU_TNC:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_tc_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_SIDEMENU_TNC)];  //lokalise 17 jan
                break;
            case kSUBMENU_PRIVACYPOLICY:
                [cell.imgMenuIcon setImage: [UIImage imageNamed: @"sideicon_privacy_midgrey"]];
                [cell.lblTitle setText: LOCALIZATION(C_TITLE_PRIVACY)];  //lokalise 17 jan
                break;
            default:
                break;
        }
    }
    
    
    return cell;
}

#pragma mark Menu interactions
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //don't do anything if same menu
//    if (indexPath.row == self.currentMenuIndex)
//        return;
//    else
        self.currentMenuIndex = indexPath.row;
    
    if (tableView == self.sideMenuTableView)
    {
        NSInteger menuIndex = [[self.menuArray objectAtIndex: indexPath.row] intValue];
        switch (menuIndex)
        {
            case kSIDEMENU_DSR_INVENTORYMANAGEMENT:
                [mSession loadDSRInventoryManagement];
                break;
            case kSIDEMENU_DSR_HasInventory:
                [mSession loadDSRInventoryManagement];
                break;
            case kSIDEMENU_TO_HasInventory:
                [mSession loadTOInventoryManagement];
               break;
            case kSIDEMENU_PARTNERSOFSHELLCLUB:
                [mSession loadRUPartners];
                break;


            case kSIDEMENU_WORKSHOPOFFER:
                [mSession loadWorkshopOfferView];
                break;
                //Customer redemption
//            case kSIDEMENU_WORKSHOPMODULES:
//                [self setupMyWorkshopModulesSubMenu];
//                break;
            case kSIDEMENU_HELPANDSUPPORT:
                [self setupHelpAndSupportSubMenu];
                break;
            case kSIDEMENU_TRADEMYCUSTOMERS:
                [self setupTradeMyCustomer];
                break;
            case kSIDEMENU_MECHMYCUSTOMERS:
                [self setupMechMyCustomer];
                break;
                
            case kSIDEMENU_MYREWARDS:
                [mSession loadRewardsView];
                break;
                
//            case kSIDEMENU_SUBMENUEXAMPLE:
//                self.subMenuArray = @[@(kSUBMENU_BACK),
//                                      @(kSUBMENU_WORKSHOP_SEARCH)];
//                [self showSubmenu];
//                break;
            case kSIDEMENU_DSRMANAGEWORKSHOP:
                [mSession loadManageWorkshopView];
                break;

            case kSIDEMENU_SPIRAXGADUSONEPLUSONE:
                [mSession loadSpiraxWorkshopView];
                break;
            case kSIDEMENU_DSRWORKSHOPPERFORMANCE:
                [mSession loadINPerformance];
//                [mSession loadPerformanceView];
                break;
            case kSIDEMENU_RU_DSRWORKSHOPPERFORMANCE:
                [mSession loadRUWorkshopPerformance];
                break;
//            case kSIDEMENU_DSRSALESKIT:
//#warning phase 2
//                break;
            case kSIDEMENU_DSRMANAGEORDER:
                [mSession loadManageOrder];
                break;
            case kSIDEMENU_DSRVIEWPROMOTION:
                
                break;
                
            case kSIDEMENU_WORKSHOPPROFILING:
                [mSession loadWorkshopProfilingView];
                break;
            case kSIDEMENU_MYNETWORK:
                [mSession loadManageWorkshopView];
//                [mSession loadMyNetworkView];
                break;
            case kSIDEMENU_LOYALTYREWARDS:
                [mSession loadLoyaltyContract];
                break;
                
            case kSIDEMENU_EBOOKING:
                [mSession loadTOEbooking];
                break;

#pragma mark OWNER MENUS
                
            case kSIDEMENU_TO_EBOOKING:
                [mSession loadTOEbooking];
                break;
            case kSIDEMENU_TO_INVENTORYMANAGEMENT:
                [mSession loadTOInventoryManagement];
                break;
            case kSIDEMENU_CASHINCENTIVE:
                [mSession loadCashIncentiveView];
                break;
            case kSIDEMENU_TRADEMANAGESTAFF:
                [mSession loadManageMechanic];
                break;
            case kSIDEMENU_TRADEMANAGEORDER:
                [mSession loadManageOrder];
                break;
//#pragma mark MECHANIC MENUS
//            case kSIDEMENU_MECHSALESKIT:
                break;
            case kSIDEMENU_TRADEWORKSHOPPERFORMANCE:
//                [mSession loadPerformanceView];
                [mSession loadINPerformance];
                break;
            case kSIDEMENU_RU_TRADEWORKSHOPPERFORMANCE:
                [mSession loadRUWorkshopPerformance];
                break;
//            case kSIDEMENU_MECHVIEWPROMOTION:
//#warning phase 2
//                break;
            case kSIDEMENU_MECHPERFORMANCE:
//                [mSession loadMechPointsView];
//                [mSession loadPerformanceView];
                [mSession loadINPerformance];
                break;
            case kSIDEMENU_FAREGISTERPRODUCTCODE:
                [mSession loadScanProductView];
                break;

            case kSIDEMENU_TRADESPIRAXGADUSONEPLUSONE:
                [mSession loadSpiraxWorkshopView];
                break;

                
#pragma mark DMR MENUS
            case kSIDEMENU_IN_MECHPERFORMANCE:
                [mSession loadINPerformance];
                break;
            case kSIDEMENU_IN_MANAGEMECHANICS:
                [mSession loadManageWorkshopView];
                break;
            case kSIDEMENU_IN_MYMECHMODULES:
                [self setupMyMechanicModulesSubMenu];
                break;
                
            case kSIDEMENU_IN_MECHSPIRAXGADUSONEPLUSONE:
                [mSession loadSpiraxWorkshopView];
                break;

#pragma mark COMMON MENU
            case kSIDEMENU_HOME:
                [mSession loadHomeView];
                break;
            case kSIDEMENU_MYPROFILE:
            {
                [self goToProfile:nil];
            }
                break;
                
            case kSIDEMENU_MYLIBRARY:
                [mSession loadMyLibraryView];
                break;
                
            case kSIDEMENU_NOTIFICATION:
                [mSession loadNotificationsView];
                break;
            case kSIDEMENU_SURVEY:
                [mSession loadSurveyView];
                break;
            case kSIDEMENU_SETTINGS:
                [mSession loadSettingsView];
                break;
                
            case kSIDEMENU_PARTICIPATINGPRODUCTS:
                [mSession loadParticipatingProduct];
                break;
            case kSIDEMENU_SDA:
                [mSession loadSDA:self];
                break;
            case kSIDEMENU_SWA:
                [mSession loadSWA:self];
                break;

            case kSIDEMENU_COMMONSPIRAXGADUSONEPLUSONE:
                [mSession loadSpiraxWorkshopView];
                break;
        }
    }
    else if (tableView == self.subMenuTableView)
    {
        
        //get enum value assigned to button
        NSInteger menuIndex = [[self.subMenuArray objectAtIndex: indexPath.row] intValue];
        switch (menuIndex)
        {
            case kSUBMENU_BACK:
                [self.sideMenuTableView selectRowAtIndexPath: nil
                                                    animated:NO
                                              scrollPosition:UITableViewScrollPositionTop];
                [self hideSubmenu];
                break;
            case kSUBMENU_WORKSHOP_SEARCH:
                
                break;
                
#pragma mark My workshop Modules
            case kSUBMENU_SEARCHVEHICLEID:
                [mSession loadSearchVehicleView];
                break;
            case kSUBMENU_INVITETRADE:
                [mSession loadInviteTradeView];
                break;
            case kSUBMENU_REGISTERPRODUCTCODE:
                [mSession loadScanProductView];
                break;
            case kSUBMENU_LUBEMATCH:
                [mSession loadLubeMatch];
                break;
            case kSUBMENU_MYREWARDS:
                [mSession loadRewardsView];
                break;
            case kSUBMENU_CUSTOMERREDEMPTION:    //customer redemption
                [mSession loadPromoCodeMasterView];
                break;
            case kSUBMENU_PARTICIPATINGPRODUCTS:
                [mSession loadParticipatingProduct];
                break;
            case kSUBMENU_OILCHANGEWALKTHROUGH:
                [mSession loadWalkthroughView: WALKTHROUGH_OilChange];
                break;
                
#pragma mark My Customers
                //search vehicle id
                //register product code
                
            case kSUBMENU_SCANDECAL:    //scan decal
                [mSession loadSearchVehicleViewWithDecal: nil];
                break;
            case kSUBMENU_ACTIVATEDECAL:    //activate decal
                [mSession loadDecalView];
                break;
            case kSUBMENU_INVITECUSTOMER:
                //                [mSession loadInviteCustomerView];
                [mSession loadRegisterCustomerView: @{@"PromoCode" : @"",
                                                      @"VehicleNumber": @"",
                                                      @"VehicleStateID": @"",
                                                      
                                                      }];
                break;
            case kSUBMENU_MYNETWORK:
                [mSession loadManageWorkshopView];
                break;
            case kSUBMENU_SCANBOTTLE:
                [mSession loadScanBottle];
                break;
                
#pragma mark Help & Support
            case kSUBMENU_FAQ:
                [mSession loadFAQView];
                break;
            case kSUBMENU_CONTACTUS:
                [mSession loadContactView];
                break;
            case kSUBMENU_TNC:
                [mSession loadTNCView];
                break;
            case kSUBMENU_PRIVACYPOLICY:
                [mSession loadPrivacyView];
                break;
            default:
                break;
        }
    }
    self.previousRowSelected = indexPath;
}

-(void) showSubmenu
{
    //reload submenu data to populate
    [self.subMenuTableView reloadData];
    
    if (kIsRightToLeft) {
        [UIView animateWithDuration: ANIMATION_SPEED animations:^{
            self.sideMenuTableView.transform = CGAffineTransformMakeTranslation(-self.sideMenuTableView.frame.size.width, 0);
            self.subMenuTableView.transform = CGAffineTransformMakeTranslation(-self.sideMenuTableView.frame.size.width, 0);
        }];
        
    } else {
        [UIView animateWithDuration: ANIMATION_SPEED animations:^{
            self.sideMenuTableView.transform = CGAffineTransformMakeTranslation(self.sideMenuTableView.frame.size.width, 0);
            self.subMenuTableView.transform = CGAffineTransformMakeTranslation(self.sideMenuTableView.frame.size.width, 0);
        }];
    }
}

-(void) resetMenu
{
    self.sideMenuTableView.transform = CGAffineTransformIdentity;
    self.subMenuTableView.transform = CGAffineTransformIdentity;
}

-(void) hideSubmenu
{
    [UIView animateWithDuration: ANIMATION_SPEED animations:^{
        self.subMenuTableView.transform = CGAffineTransformIdentity;
        self.sideMenuTableView.transform = CGAffineTransformIdentity;
    }];
}

- (IBAction)notificationPressed:(id)sender
{
    [mSession loadNotificationsView];
}

- (IBAction)goToProfile:(id)sender {
    //if currently in menu, profile button does nothing
    if (self.currentMenuIndex == kSIDEMENU_MYPROFILE)
        return;
    else
    {
        //deselect row
//        NSInteger selectedIndex = [self.subMenuArray indexOfObject: @(self.currentMenuIndex)];
        [self.sideMenuTableView deselectRowAtIndexPath: [NSIndexPath indexPathForRow:self.previousRowSelected.row inSection:0] animated: NO];
        //self.currentMenuIndex = kSIDEMENU_MYPROFILE;
        
    }
    
    
    //only india TO/iM/DSR/DMR profile due to Bank Details
   if (IS_COUNTRY(COUNTRYCODE_INDIA)) {
        if(GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER || GET_PROFILETYPE == kPROFILETYPE_DSR || GET_PROFILETYPE == kPROFILETYPE_DMR) {
            [mSession loadProfileTabView];
        } else {
            [mSession loadMyProfileView];
        }
   } else if(IS_COUNTRY(COUNTRYCODE_INDONESIA)) {
       if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
           [mSession loadMyProfileView];
       } else {
            [mSession loadProfileTabView];
       }
   }
    else
        [mSession loadMyProfileView];
    
    
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
//        case kWEBSERVICE_LOGOUT:
//            [mSession reset];
//            [mSession loadLoginView];
//            break;
        default:
            break;
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
