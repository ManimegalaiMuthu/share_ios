//
//  SideMenuTableViewCell.m
//  Shell
//
//  Created by Ankita Chhikara on 25/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "SideMenuTableViewCell.h"
#import "Helper.h"
#import "Constants.h"

@implementation SideMenuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    self.lblTitle
    [self reloadFont];
//    self.selectedBackgroundView = [UIView new];
//    self.selectedBackgroundView.backgroundColor = COLOUR_YELLOW;
}
-(void) reloadFont
{
    [self.lblTitle setFont: (IS_THAI_LANGUAGE) ? FONT_B2 : FONT_B1];
    [self.lblTitle setTextColor: COLOUR_DARKGREY];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
