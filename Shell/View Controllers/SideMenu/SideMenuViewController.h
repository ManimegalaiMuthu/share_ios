//
//  SideMenuViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 19/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface SideMenuViewController : BaseVC
@property CGFloat tableviewOffset;

//-(void)selectQuickLinkMenu: (NSString *) identifier;
-(void) updateProfile;
-(void) resetMenu;
@end
