//
//  ExpandableCollapsibleTableViewController.m
//  Shell
//
//  Created by Nach on 9/3/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "ExpandableCollapsibleTableViewController.h"
#import "ShareProductTableViewCell.h"
#import "ProductTableViewCell.h"

#define CELL_HEIGHT 48

static int const kHeaderSectionTag = 6900;

@interface ExpandableCollapsibleTableViewController ()

@property ExpandableCollapsibleDataModel *dataModel;
@property (assign) NSInteger expandedSectionHeaderNumber;
@property (assign) UITableViewHeaderFooterView *expandedSectionHeader;

@property BOOL hasPoints;

@end

@implementation ExpandableCollapsibleTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataModel = [[ExpandableCollapsibleDataModel alloc] init];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
     self.expandedSectionHeaderNumber = -1;
    [self.tableView registerNib:[UINib nibWithNibName:@"ProductTableViewCell" bundle:nil] forCellReuseIdentifier:@"ProductTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ShareProductTableViewCell" bundle:nil] forCellReuseIdentifier:@"ShareProductTableViewCell"];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.view.backgroundColor = [Helper colorFromHex:@"#F7F7F7"];
}


-(void) initialiseTableWith:(ExpandableCollapsibleDataModel *)dataModel hasPoints:(BOOL)hasPoints{
     
    self.dataModel = dataModel;
    self.hasPoints = hasPoints;
    [self.tableView reloadData];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.dataModel.tableDataArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.expandedSectionHeaderNumber == section) {
        return 0;
    } else {
        NSArray *numberOfProducts = [self.dataModel.tableDataArray objectAtIndex:section];
        return numberOfProducts.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *productsAtSection = [self.dataModel.tableDataArray objectAtIndex:indexPath.section];
    NSDictionary *productDetails = [productsAtSection objectAtIndex:indexPath.row];
    
    if(self.hasPoints) {
        ShareProductTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ShareProductTableViewCell" ];
        cell.productnameLbl.font = FONT_H2;//FONT_B1;
        cell.productnameLbl.textColor = COLOUR_VERYDARKGREY;
        cell.productnameLbl.text = [NSString stringWithFormat:@"%@",[productDetails objectForKey:kExpandable_ProductName]];
        cell.RefPtsLbl.hidden = YES;
        cell.WalkinPtsLbl.text = [NSString stringWithFormat:@"%@",[productDetails objectForKey:kExpandable_ProductTotal]];
        UIView *boxBottomBorderView = [cell viewWithTag:300];
        if (indexPath.row == productsAtSection.count - 1) {
            boxBottomBorderView.hidden = NO;
        } else {
            boxBottomBorderView.hidden = YES;
        }
        cell.RefPtsLbl.font = cell.productnameLbl.font;
        cell.WalkinPtsLbl.font = cell.productnameLbl.font;
        cell.RefPtsLbl.textColor = cell.productnameLbl.textColor;
        cell.WalkinPtsLbl.textColor = cell.productnameLbl.textColor;
        
        return cell;
    } else {
        ProductTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ProductTableViewCell"];
        
        cell.productNameLbl.text = [productDetails objectForKey:@"ProductName"];
        cell.productNameLbl.textColor = COLOUR_VERYDARKGREY;
        cell.productNameLbl.font = FONT_H2;//FONT_B1;
        UIView *boxBottomBorderView = [cell viewWithTag:300];
        if (indexPath.row == (productsAtSection.count - 1)) {
            boxBottomBorderView.hidden = NO;
        } else {
            boxBottomBorderView.hidden = YES;
        }
         return cell;
    }
    
}
// removed sept 2020
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    return [self.dataModel.tableHeadingsArray objectAtIndex:section];
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section; {
    return CELL_HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return CELL_HEIGHT;
}


- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.contentView.backgroundColor = COLOUR_WHITE;
    header.textLabel.textColor = COLOUR_VERYDARKGREY;
    header.textLabel.font = FONT_H2;//FONT_B1;//FONT_B1;
    header.layer.borderColor = COLOUR_PALEGREY.CGColor;
    header.layer.borderWidth = 1.0;
    
    UIImageView *viewWithTag = [self.view viewWithTag:kHeaderSectionTag + section];
    if (viewWithTag) {
        [viewWithTag removeFromSuperview];
    }
    // add the arrow image
    CGSize headerFrame = self.view.frame.size;
    UIImageView *theImageView;
    if(kIsRightToLeft) {
        theImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 13, 18, 18)];
    } else {
        theImageView = [[UIImageView alloc] initWithFrame:CGRectMake(headerFrame.width - 35, 13, 18, 18)];
    }
    theImageView.image = GET_ISADVANCE ? [UIImage imageNamed:@"btn_plus_blue"] : [UIImage imageNamed:@"btn_plus_red"];
    theImageView.tag = kHeaderSectionTag + section;
    [header addSubview:theImageView];
    [UIView animateWithDuration:0.4 animations:^{
        theImageView.transform = CGAffineTransformMakeRotation(M_PI_4);
        [theImageView layoutIfNeeded];
    }];
    
    // make headers touchable
    header.tag = section;
    UITapGestureRecognizer *headerTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderWasTouched:)];
    [header addGestureRecognizer:headerTapGesture];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *viewForFooter = [[UIView alloc]init];
    viewForFooter.backgroundColor = [UIColor colorWithRed:247/COLOUR_MAXBYTE green:247/COLOUR_MAXBYTE blue:247/COLOUR_MAXBYTE alpha: 1.0f];
    return viewForFooter;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)updateTableViewRowDisplay:(NSArray *)arrayOfIndexPaths {
    [self.tableView beginUpdates];
    [self.tableView deleteRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation: UITableViewRowAnimationFade];
    [self.tableView endUpdates];
}

- (void)sectionHeaderWasTouched:(UITapGestureRecognizer *)sender {
    UITableViewHeaderFooterView *headerView = (UITableViewHeaderFooterView *)sender.view;
    NSInteger section = headerView.tag;
    UIImageView *eImageView = (UIImageView *)[headerView viewWithTag:kHeaderSectionTag + section];
    self.expandedSectionHeader = headerView;
    
    if (self.expandedSectionHeaderNumber == -1) {
        self.expandedSectionHeaderNumber = section;
        [self tableViewCollapeSection:section withImage: eImageView];
    } else {
        if (self.expandedSectionHeaderNumber == section) {
            [self tableViewExpandSection:section withImage: eImageView];
            self.expandedSectionHeader = nil;
        } else {
            UIImageView *cImageView  = (UIImageView *)[self.view viewWithTag:kHeaderSectionTag + self.expandedSectionHeaderNumber];
            [self tableViewExpandSection:self.expandedSectionHeaderNumber withImage: cImageView];
            [self tableViewCollapeSection:section withImage: eImageView];
        }
    }
}

- (void)tableViewCollapeSection:(NSInteger)section withImage:(UIImageView *)imageView {
    NSArray *sectionData = [self.dataModel.tableDataArray objectAtIndex:section];
    
    self.expandedSectionHeaderNumber = section;
    if (sectionData.count == 0) {
        return;
    } else {
        [UIView animateWithDuration:0.4 animations:^{
            imageView.transform = CGAffineTransformMakeRotation(M_PI_2);
        }];
        NSMutableArray *arrayOfIndexPaths = [NSMutableArray array];
        for (int i=0; i< sectionData.count; i++) {
            NSIndexPath *index = [NSIndexPath indexPathForRow:i inSection:section];
            [arrayOfIndexPaths addObject:index];
        }
        
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation: UITableViewRowAnimationFade];
        [self.tableView endUpdates];
        
    }
}

- (void)tableViewExpandSection:(NSInteger)section withImage:(UIImageView *)imageView {
    NSArray *sectionData = [self.dataModel.tableDataArray objectAtIndex:section];
    
    [UIView animateWithDuration:0.4 animations:^{
        imageView.transform = CGAffineTransformMakeRotation(M_PI_4);
        [imageView layoutIfNeeded];
    }];
    NSMutableArray *arrayOfIndexPaths = [NSMutableArray array];
    for (int i=0; i< sectionData.count; i++) {
        NSIndexPath *index = [NSIndexPath indexPathForRow:i inSection:section];
        [arrayOfIndexPaths addObject:index];
    }
    
    self.expandedSectionHeaderNumber = -1;
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation: UITableViewRowAnimationFade];
    [self.tableView endUpdates];
    
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}




/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
