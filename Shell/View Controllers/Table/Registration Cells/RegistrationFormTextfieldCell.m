//
//  RegistrationFormTextfieldCell.m
//  Shell
//
//  Created by Admin on 25/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "RegistrationFormTextfieldCell.h"

@interface RegistrationFormTextfieldCell()
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UITextField *textfield;
@end

@implementation RegistrationFormTextfieldCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.textfield setFont: FONT_B1];
    [self.topLabel setFont: FONT_B1];
    
    if (kIsRightToLeft)
    {
        [self.textfield setTextAlignment: NSTextAlignmentRight];
    }
    else
    {
        [self.textfield setTextAlignment: NSTextAlignmentLeft];
    }
}
-(void) setTextfieldText: (NSString *) textfieldString
{
    self.textfield.text = textfieldString;
}

-(void) setPlaceholder: (NSString *) title
{
    self.textfield.placeholder = [NSString stringWithFormat: @"%@", title];
}

-(void) setSecureTextEntry: (BOOL)isSecureTextEntry
{
    self.textfield.secureTextEntry = isSecureTextEntry;
}
-(void) setKeyboard:(UIKeyboardType) keyboardType
{
    self.textfield.keyboardType = keyboardType;
}

-(void) setTitle: (NSString *) title
{
    if ([self.topLabel isHidden])
        self.textfield.placeholder = title;
    else
//        self.textfield.placeholder = [NSString stringWithFormat: @"%@ %@",  LOCALIZATION(C_TEXTFIELD_PLEASEENTER) , title];
        
        self.textfield.placeholder = [NSString stringWithFormat: @"%@", title];
    self.topLabel.text = title;
}

-(void) setRequiredTitle: (NSAttributedString *) title placeholder:(NSString *)placeholderString
{
        //        self.textfield.placeholder = [NSString stringWithFormat: @"%@ %@",  LOCALIZATION(C_TEXTFIELD_PLEASEENTER) , title];
    
    self.topLabel.attributedText = title;
    self.textfield.placeholder = [NSString stringWithFormat: @"%@", placeholderString];
    
}

-(void) hideTopLabel: (BOOL) hidden;
{
    self.topLabel.hidden = hidden;
}

-(void)enableCell:(BOOL)userInteractionEnabled{
    [self setUserInteractionEnabled: userInteractionEnabled];
    [self.textfield setUserInteractionEnabled: userInteractionEnabled];
    if (![self isUserInteractionEnabled])
    {
        self.textfield.textColor = COLOUR_LIGHTGREY;
    }
}

-(NSString *) getJsonValue
{
    if ([self.textfield.text isKindOfClass: [NSNull class]])
        return @"";
    return self.textfield.text;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) toggleProfilingQuestion:(BOOL) toEnable
{
    if (toEnable)
    {
        //enabled UI
        self.topLabel.textColor = COLOUR_VERYDARKGREY;
        self.userInteractionEnabled = YES;
    }
    else
    {
        //disabled UI
        self.topLabel.textColor = COLOUR_LIGHTGREY;
        self.textfield.text = @"";
        self.userInteractionEnabled = NO;
    }
}

-(void) setTextfieldDelegate:(id<UITextFieldDelegate>) delegate
{
    self.textfield.delegate = delegate;
}

-(void) setTextFieldTag:(NSInteger) tag {
    self.textfield.tag = tag;
}

@end
