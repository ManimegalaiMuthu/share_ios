//
//  RegistrationHeaderTableViewCell.m
//  Shell
//
//  Created by Ankita Chhikara on 27/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "RegistrationHeaderTableViewCell.h"
#import "Constants.h"

@interface RegistrationHeaderTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation RegistrationHeaderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.lblTitle setFont: FONT_H1];
    self.userInteractionEnabled = NO;
}

-(void) setTitle: (NSString *) title
{
    self.lblTitle.text = title;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
