//
//  RegistrationButtonCell.h
//  Shell
//
//  Created by Admin on 25/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Constants.h"

@interface RegistrationButtonCell : UITableViewCell

-(void) hideTopLabel: (BOOL) hidden;
-(void) setTitle: (NSString *) title; //top label
-(void) setSelectionTitle: (NSString *) selectionTitle;
-(void) setIconImage: (NSString *) imageName;
-(NSString *) getJsonValue;
@end
