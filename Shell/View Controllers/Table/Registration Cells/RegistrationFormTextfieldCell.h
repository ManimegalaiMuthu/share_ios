//
//  RegistrationFormTextfieldCell.h
//  Shell
//
//  Created by Admin on 25/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "LocalizationManager.h"
#import <Foundation/Foundation.h>

@interface RegistrationFormTextfieldCell : UITableViewCell

-(void) setTextfieldText: (NSString *) textfieldString;
-(void) setTitle: (NSString *) title;
-(void) setPlaceholder: (NSString *) title;
-(void) hideTopLabel: (BOOL) hidden;
-(void) setSecureTextEntry: (BOOL)isSecureTextEntry;
-(void) setKeyboard:(UIKeyboardType) keyboardType;
-(void) enableCell:(BOOL)userInteractionEnabled;
-(NSString *) getJsonValue;

-(void) setTextfieldDelegate:(id<UITextFieldDelegate>) delegate;
-(void) setRequiredTitle: (NSAttributedString *) title placeholder:(NSString *)placeholderString;

-(void) toggleProfilingQuestion:(BOOL) toEnable;

-(void) setTextFieldTag:(NSInteger) tag;

@end
