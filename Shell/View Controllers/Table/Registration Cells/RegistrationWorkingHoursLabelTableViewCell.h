//
//  RegistrationWorkingHoursLabelTableViewCell.h
//  Shell
//
//  Created by Ankita Chhikara on 30/1/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationWorkingHoursLabelTableViewCell : UITableViewCell

-(void) setLeftLabel: (NSString *) labelString;
-(void) setRightLabel: (NSString *) labelString;
@end
