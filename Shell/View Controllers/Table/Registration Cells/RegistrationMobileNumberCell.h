//
//  RegistrationMobileNumberCell.h
//  Shell
//
//  Created by Admin on 25/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Constants.h"

@interface RegistrationMobileNumberCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *mobileButton;

-(void) setMobileCodeEnabled: (BOOL) enabled;
-(void) setTextfieldEnabled: (BOOL) enabled;
-(void) hideTopLabel: (BOOL) hidden;
-(void) setTitle: (NSString *) title;
-(void) setMobileNumber: (NSString *) mobileNumberString;
-(void) setCountryCode: (NSString *) codeString;
-(NSString *) getMobileCodeJsonValue;
-(NSString *) getMobileNumberJsonValue;
-(void) setTextfieldDelegate:(id<UITextFieldDelegate>) delegate;

-(void) setRequiredTitle: (NSAttributedString *) title placeholder:(NSString *)placeholderString;

-(void) setTextFieldTag:(NSInteger) tag;

@end
