//
//  ContactUsImageCollectionViewCell.h
//  Shell
//
//  Created by Admin on 6/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ContactUsImageCellDelegate <NSObject>
-(void)deleteImageBtnPressed:(UIButton *) button;
-(void)newImageAdded:(UIButton *) button;
@end

@interface ContactUsImageCollectionViewCell : UICollectionViewCell
@property id<ContactUsImageCellDelegate> delegate;

-(NSInteger)getTag;
-(NSString *)getImageByte;
-(void)setTagValueForClose:(NSInteger)tagValue;
@end
