//
//  ContactUsImageTableViewCell.m
//  Shell
//
//  Created by Admin on 6/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "ContactUsImageTableViewCell.h"
#import "ContactUsImageCollectionViewCell.h"
#import "Constants.h"
#import "LocalizationManager.h"

enum kImage_Count
{
    kImage_1 = 1,
    kImage_2,
    kImage_3,
};

#define PHOTO_ALLOW_TO_UPLOAD 3
#define LEADINGANDTRAILING 32
@interface ContactUsImageTableViewCell()<UICollectionViewDelegate,UICollectionViewDataSource,ContactUsImageCellDelegate>
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *collectionCellLayout;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property NSMutableDictionary *referenceDict;
@end
@implementation ContactUsImageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.referenceDict = [[NSMutableDictionary alloc]init];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ContactUsImageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ContactUsImageCollectionViewCell"];
    
    NSInteger cellLength = (SCREEN_WIDTH- LEADINGANDTRAILING -25)/3;
    self.collectionCellLayout.itemSize = CGSizeMake(cellLength, cellLength);
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    self.lblTitle.font = FONT_B1;
    self.lblTitle.text = LOCALIZATION(C_CONTACTUS_UPLOADIMAGE);  //lokalise 17 Jan
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return PHOTO_ALLOW_TO_UPLOAD;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ContactUsImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ContactUsImageCollectionViewCell" forIndexPath:indexPath];
    [cell setTagValueForClose:indexPath.row];
    [self.referenceDict setObject:cell forKey:[NSString stringWithFormat:@"%ld",indexPath.row]];
    cell.delegate = self;
    return cell;
}

-(NSArray *)imageArray
{
    ContactUsImageCollectionViewCell *cell1 = [self.referenceDict objectForKey:@"0"];
    ContactUsImageCollectionViewCell *cell2 = [self.referenceDict objectForKey:@"1"];
    ContactUsImageCollectionViewCell *cell3 = [self.referenceDict objectForKey:@"2"];
    
    
    NSArray *allCell = @[cell1,cell2,cell3];
    NSMutableArray *submitArray = [[NSMutableArray alloc]init];
    
    for(int i= 0;i<allCell.count;i++){
        if(![[[allCell objectAtIndex:i] getImageByte] isEqualToString:@" "]){
            NSDictionary *cellDict = @{
                                       @"ImageArray" : [[allCell objectAtIndex:i] getImageByte],
                                       @"FileName"   : [NSString stringWithFormat:@"%d.jpg",i]
                                       };
            [submitArray addObject:cellDict];
        }
    }
    
    return submitArray;
}

-(void)newImageAdded :(UIButton *) button
{
    NSInteger reference = button.tag;
    ContactUsImageCollectionViewCell *cell0 = [self.referenceDict objectForKey:@"0"];
    ContactUsImageCollectionViewCell *cell1 = [self.referenceDict objectForKey:@"1"];
    ContactUsImageCollectionViewCell *cell2 = [self.referenceDict objectForKey:@"2"];
    
    if(reference == 1){
        if([[cell0 getImageByte] isEqualToString:@" "]){
            [self.collectionView moveItemAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] toIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            [cell1 setTagValueForClose:0];
            [cell0 setTagValueForClose:1];
            
            [self.referenceDict setObject:cell1 forKey:@"0"];
            [self.referenceDict setObject:cell0 forKey:@"1"];
        }
    }else if(reference == 2){
        if([[cell0 getImageByte] isEqualToString:@" "]){
            [self.collectionView moveItemAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0] toIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            [cell2 setTagValueForClose:0];
            [cell0 setTagValueForClose:1];
            [cell1 setTagValueForClose:2];
            
            [self.referenceDict setObject:cell2 forKey:@"0"];
            [self.referenceDict setObject:cell0 forKey:@"1"];
            [self.referenceDict setObject:cell1 forKey:@"2"];
            
        }else if([[cell1 getImageByte] isEqualToString:@" "]){
            [self.collectionView moveItemAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0] toIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
            [cell2 setTagValueForClose:1];
            [cell1 setTagValueForClose:2];
            
            [self.referenceDict setObject:cell2 forKey:@"1"];
            [self.referenceDict setObject:cell1 forKey:@"2"];
        }
    }
    
    NSLog(@"%ld : %ld : %ld",[cell0 getTag], [cell1 getTag],[cell2 getTag]);
    
    ContactUsImageCollectionViewCell *newCell1 = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    ContactUsImageCollectionViewCell *newCell2 = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    ContactUsImageCollectionViewCell *newCell3 = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    
    NSLog(@"%ld : %ld : %ld",[newCell1 getTag], [newCell2 getTag],[newCell3 getTag]);
    
}

-(void)deleteImageBtnPressed:(UIButton *) button
{
    ContactUsImageCollectionViewCell *cell0 = [self.referenceDict objectForKey:@"0"];
    ContactUsImageCollectionViewCell *cell1 = [self.referenceDict objectForKey:@"1"];
    ContactUsImageCollectionViewCell *cell2 = [self.referenceDict objectForKey:@"2"];
    
    switch (button.tag) {
        case 0:{
            if(![[cell1 getImageByte] isEqualToString:@" "]){
                if(![[cell2 getImageByte] isEqualToString:@" "]){
                    [self.collectionView moveItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] toIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
                    [self.collectionView moveItemAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] toIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
                    
                    [cell1 setTagValueForClose:0];
                    [cell2 setTagValueForClose:1];
                    [cell0 setTagValueForClose:2];
                    
                    [self.referenceDict setObject:cell1 forKey:@"0"];
                    [self.referenceDict setObject:cell2 forKey:@"1"];
                    [self.referenceDict setObject:cell0 forKey:@"2"];
                }else{
                    [self.collectionView moveItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] toIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
                    
                    [cell1 setTagValueForClose:0];
                    [cell0 setTagValueForClose:1];
                    
                    [self.referenceDict setObject:cell1 forKey:@"0"];
                    [self.referenceDict setObject:cell0 forKey:@"1"];
                }
            }
        }
            break;
        case 1:{
            if(![[cell2 getImageByte] isEqualToString:@" "]){
                [self.collectionView moveItemAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] toIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
                [cell2 setTagValueForClose:1];
                [cell1 setTagValueForClose:2];
                
                [self.referenceDict setObject:cell2 forKey:@"1"];
                [self.referenceDict setObject:cell1 forKey:@"2"];
            }
        }
            break;
            
        default:
            break;
    }
    
    ContactUsImageCollectionViewCell *newCell1 = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    ContactUsImageCollectionViewCell *newCell2 = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    ContactUsImageCollectionViewCell *newCell3 = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    
    NSLog(@"%ld : %ld : %ld",[newCell1 getTag], [newCell2 getTag],[newCell3 getTag]);
    
}

// This method is for deleting the selected images from the data source array
-(void)deleteItemsFromDataSourceAtIndexPaths:(NSArray  *)itemPaths
{
    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
    for (NSIndexPath *itemPath  in itemPaths) {
        [indexSet addIndex:itemPath.row];
    }
//    [self.images removeObjectsAtIndexes:indexSet]; // self.images is my data source
    
}

@end
