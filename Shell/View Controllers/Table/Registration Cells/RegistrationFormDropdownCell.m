//
//  RegistrationFormDropdownCell.m
//  Shell
//
//  Created by Admin on 25/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "RegistrationFormDropdownCell.h"

@interface RegistrationFormDropdownCell()

@property (weak, nonatomic) IBOutlet UIImageView *imgDownIcon;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@end

@implementation RegistrationFormDropdownCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.button.titleLabel setFont: FONT_B1];
    self.button.titleLabel.text = @"";
    [self.button setTitle:@"" forState:UIControlStateNormal];
    [self.topLabel setFont: FONT_B1];
}

-(void)setButtonContentHorizontalAlignment: (UIControlContentHorizontalAlignment) alignment
{
    [self.button setContentHorizontalAlignment:alignment];
}

-(void) setSelectionTitle:(NSString *) selectionTitle
{
    [self.button setTitle: selectionTitle forState: UIControlStateNormal];
}

-(void) setRequiredTitle: (NSAttributedString *) title placeholder:(NSString *)placeholderString
{
    //        self.textfield.placeholder = [NSString stringWithFormat: @"%@ %@",  LOCALIZATION(C_TEXTFIELD_PLEASEENTER) , title];
    
    self.topLabel.attributedText = title;
    
}

-(void) hideTopLabel: (BOOL) hidden
{
    self.topLabel.hidden = hidden;
}


-(void) setTitle: (NSString *) title
{
    self.topLabel.text = title;
}

-(NSString *) getJsonValue
{
    if ([self.button.titleLabel.text isKindOfClass: [NSNull class]])
        return @"";
    return self.button.titleLabel.text;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setUserInteractionEnabled:(BOOL)userInteractionEnabled
{
    [super setUserInteractionEnabled: userInteractionEnabled];
    //self.imgDownIcon.hidden = !userInteractionEnabled; //hide down arrow if not enabled
    if (![self isUserInteractionEnabled])
    {
        [self.button setTitleColor: COLOUR_LIGHTGREY forState: UIControlStateNormal];
    }
}

@end
