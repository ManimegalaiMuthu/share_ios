//
//  ContactPreferencesCollectionViewCell.h
//  Shell
//
//  Created by Ankita Chhikara on 26/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ContactPreferencesCollectionViewCellDelegate <NSObject>
-(void)didSelectCell:(NSInteger) index isSelected:(BOOL) isSelected;
@end

@interface ContactPreferencesCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) id<ContactPreferencesCollectionViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *checkboxBtn;
@property NSInteger bitValue;
@end
