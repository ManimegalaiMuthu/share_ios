//
//  RegistrationTextViewTableViewCell.h
//  Shell
//
//  Created by Ankita Chhikara on 27/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Constants.h"

@interface RegistrationTextViewTableViewCell : UITableViewCell
-(void) setTextviewDelegate:(id<UITextViewDelegate>) delegate;
-(void)didBeginEditing;
-(void)didEndEditing;

-(void) setTextviewText: (NSString *) textviewString;
-(void) setTitle: (NSString *) title;
-(void) setPlaceholder: (NSString *) title;
-(void) hideTopLabel: (BOOL) hidden;
-(void) setSecureTextEntry: (BOOL)isSecureTextEntry;
-(void) setKeyboard:(UIKeyboardType) keyboardType;
-(void) setRequiredTitle: (NSAttributedString *) title placeholder:(NSString *)placeholderString;
-(NSString *) getJsonValue;
@end
