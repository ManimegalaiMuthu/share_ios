//
//  RegistrationFormDropdownCell.h
//  Shell
//
//  Created by Admin on 25/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Constants.h"

@interface RegistrationFormDropdownCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *button;

-(void) hideTopLabel: (BOOL) hidden;
-(void) setTitle: (NSString *) title;
-(void) setSelectionTitle:(NSString *) selectionTitle;
-(void) setRequiredTitle: (NSAttributedString *) title placeholder:(NSString *)placeholderString;
-(void)setButtonContentHorizontalAlignment: (UIControlContentHorizontalAlignment) alignment;
-(NSString *) getJsonValue;
//-(void) setIconImage: (NSString *) imageName;
@end
