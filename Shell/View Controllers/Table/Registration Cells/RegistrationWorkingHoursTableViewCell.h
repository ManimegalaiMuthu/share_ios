//
//  RegistrationWorkingHoursTableViewCell.h
//  Shell
//
//  Created by Ankita Chhikara on 31/1/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationWorkingHoursTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *button;
@property NSDictionary *workingHoursDict;

-(void) reloadCellData;
@end
