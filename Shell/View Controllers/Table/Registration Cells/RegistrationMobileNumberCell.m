//
//  RegistrationMobileNumberCell.m
//  Shell
//
//  Created by Admin on 25/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "RegistrationMobileNumberCell.h"

@interface RegistrationMobileNumberCell()
@property (weak, nonatomic) IBOutlet UIImageView *imgDownIcon;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UITextField *mobileTextfield;
@end

@implementation RegistrationMobileNumberCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.mobileButton.titleLabel setFont: FONT_B1];
    [self.mobileTextfield setFont: FONT_B1];
    [self.topLabel setFont: FONT_B1];
    
    if (kIsRightToLeft)
    {
        [self.mobileTextfield setTextAlignment: NSTextAlignmentRight];
        [self.mobileButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    }
    else
    {
        [self.mobileTextfield setTextAlignment: NSTextAlignmentLeft];
        [self.mobileButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    }
}

-(void) hideTopLabel: (BOOL) hidden
{
    self.topLabel.hidden = hidden;
}

-(void) setMobileNumber: (NSString *) mobileNumberString
{
    self.mobileTextfield.text = mobileNumberString;
}

-(void) setCountryCode: (NSString *) codeString
{
    [self.mobileButton setTitle: codeString forState: UIControlStateNormal];
}

-(void) setTitle: (NSString *) title
{
    self.topLabel.text = title;
    self.mobileTextfield.placeholder = title;
}

-(NSString *) getMobileCodeJsonValue
{
    return self.mobileButton.titleLabel.text;
}

-(NSString *) getMobileNumberJsonValue
{
    return self.mobileTextfield.text;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setMobileCodeEnabled: (BOOL) enabled
{
    self.mobileButton.enabled = enabled;
    self.imgDownIcon.hidden = !enabled;
    if (!enabled)
    {
        [self.mobileButton setTitleColor: COLOUR_LIGHTGREY forState: UIControlStateNormal];
    }
}


-(void) setRequiredTitle: (NSAttributedString *) title placeholder:(NSString *)placeholderString
{
    //        self.textfield.placeholder = [NSString stringWithFormat: @"%@ %@",  LOCALIZATION(C_TEXTFIELD_PLEASEENTER) , title];
    
    self.topLabel.attributedText = title;
    self.mobileTextfield.placeholder = [NSString stringWithFormat: @"%@", placeholderString];
    
}

-(void) setTextfieldEnabled: (BOOL) enabled
{
    self.mobileTextfield.enabled = enabled;
    if (!enabled)
    {
        self.mobileTextfield.textColor = COLOUR_LIGHTGREY;
    }
}

-(void)setUserInteractionEnabled:(BOOL)userInteractionEnabled
{
    [super setUserInteractionEnabled: userInteractionEnabled];
    self.mobileTextfield.userInteractionEnabled = userInteractionEnabled;
    self.imgDownIcon.hidden = !userInteractionEnabled; //hide down arrow if not enabled
}

-(void) setTextfieldDelegate:(id<UITextFieldDelegate>) delegate
{
    self.mobileTextfield.delegate = delegate;
}

-(void) setTextFieldTag:(NSInteger) tag {
    self.mobileTextfield.tag = tag;
}

@end
