//
//  ContactUsImageTableViewCell.h
//  Shell
//
//  Created by Admin on 6/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactUsImageTableViewCell : UITableViewCell
-(NSArray *)imageArray;
@end
