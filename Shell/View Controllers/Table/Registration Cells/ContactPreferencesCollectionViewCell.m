//
//  ContactPreferencesCollectionViewCell.m
//  Shell
//
//  Created by Ankita Chhikara on 26/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "ContactPreferencesCollectionViewCell.h"

@interface ContactPreferencesCollectionViewCell ()
@property BOOL isHeightCalculated;

@end

@implementation ContactPreferencesCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    if (!self.isHeightCalculated)
    {
        [self setNeedsLayout];
        [self layoutIfNeeded];
        CGSize size = [self.contentView systemLayoutSizeFittingSize: layoutAttributes.size];
        CGRect frame = layoutAttributes.frame;
        frame.size.width = ceilf(size.width);
        layoutAttributes.frame = frame;
        self.isHeightCalculated = YES;
    }
    
    return layoutAttributes;
}

- (IBAction)checkboxPressed:(UIButton *)sender {
    
    if ([sender isSelected])
    {
        sender.selected = NO;
    }
    else
    {
        sender.selected = YES;
    }
    
    if ([self.delegate respondsToSelector: @selector(didSelectCell:isSelected:)])
        [self.delegate didSelectCell: self.bitValue isSelected: sender.selected];
}

@end
