//
//  ContactUsImageCollectionViewCell.m
//  Shell
//
//  Created by Admin on 6/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "ContactUsImageCollectionViewCell.h"
#import "Constants.h"
#import "LocalizationManager.h"
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>
#import <QuartzCore/QuartzCore.h>

@interface ContactUsImageCollectionViewCell()<UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *mainImageButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UILabel *lblImagePlaceHolder;

@end
@implementation ContactUsImageCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.closeButton.hidden = YES;
    self.lblImagePlaceHolder.font = FONT_B2;
    self.lblImagePlaceHolder.text = LOCALIZATION(C_CONTACTUS_ADDPHOTO);
    
}

- (IBAction)mainImageButtonPressed:(id)sender {
    if(!self.mainImageButton.selected){
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:LOCALIZATION(C_CONTACTUS_PHOTOSELECTION_TITLE)  //lokalise 17 Jan
                                                                      message:@""
                                                               preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* closeBtn = [UIAlertAction actionWithTitle:LOCALIZATION(C_CONTACTUS_PHOTOSELECTION_CLOSE)  //lokalise 17 Jan
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action){
                                                             
                                                         }];
        
        UIAlertAction* libBtn = [UIAlertAction actionWithTitle:LOCALIZATION(C_CONTACTUS_PHOTOSELECTION_LIBRARY)  //lokalise 17 Jan
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action){
                                                           [self permissionLibrary];
                                                       }];
        
        UIAlertAction* camBtn = [UIAlertAction actionWithTitle:LOCALIZATION(C_CONTACTUS_PHOTOSELECTION_CAMERA)  //lokalise 17 Jan
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action){
                                                           [self permissionCamera];
                                                       }];
        
        [alert addAction:libBtn];
        [alert addAction:camBtn];
        [alert addAction:closeBtn];
        
        
        [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
        
    }
}
-(void)permissionLibrary
{
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        switch (status) {
            case PHAuthorizationStatusAuthorized:{
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.allowsEditing = YES;
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                
                [self.window.rootViewController presentViewController:picker animated:YES completion:NULL];
            }
                break;
            case PHAuthorizationStatusDenied:{
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_ALERTVIEW_ERROR) message:LOCALIZATION(C_ALERTVIEW_PERMISSIONERRORMSG) preferredStyle:UIAlertControllerStyleAlert];  //lokalise 17 Jan
                
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_CANCEL) style:UIAlertActionStyleCancel handler:nil];
                UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_SETTING) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                                UIApplicationOpenSettingsURLString]];  //lokalise 17 Jan
                }];
                
                [alertController addAction:cancelAction];
                [alertController addAction:settingsAction];
                [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
            }
                
                break;
            default:
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_ALERTVIEW_ERROR) message:LOCALIZATION(C_ALERTVIEW_PERMISSIONERRORMSG) preferredStyle:UIAlertControllerStyleAlert];  //lokalise 17 Jan
                
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_CANCEL) style:UIAlertActionStyleCancel handler:nil];
                UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_SETTING) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                                UIApplicationOpenSettingsURLString]];  //lokalise 17 Jan
                }];
                
                [alertController addAction:cancelAction];
                [alertController addAction:settingsAction];
                [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
            }
                break;
        }
    }];
}
-(void)permissionCamera
{
    switch ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo])
    {
        case AVAuthorizationStatusAuthorized:
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            picker.showsCameraControls = YES;
            
            [self.window.rootViewController presentViewController:picker animated:YES completion:NULL];
        }
            break;
        default:
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
             {
                 if(granted)
                 {
                     UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                     picker.delegate = self;
                     picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                     picker.showsCameraControls = YES;
                     
                     [self.window.rootViewController presentViewController:picker animated:YES completion:NULL];
                     
                 }
                 else
                 {
                     UIAlertController *alertController = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_ALERTVIEW_ERROR) message:LOCALIZATION(C_ALERTVIEW_PERMISSIONERRORMSG) preferredStyle:UIAlertControllerStyleAlert];  //lokalise 17 Jan
                     
                     UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_CANCEL) style:UIAlertActionStyleCancel handler:nil];
                     UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_SETTING) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                                     UIApplicationOpenSettingsURLString]];
                     }];  //lokalise 17 Jan
                     
                     [alertController addAction:cancelAction];
                     [alertController addAction:settingsAction];
                     [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
                 }
             }];
            break;
    }
}
-(void)setTagValueForClose:(NSInteger)tagValue
{
    [self.closeButton setTag:tagValue];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    [self.mainImageButton setSelected:YES];
    self.closeButton.hidden = NO;
    
    UIImage *chosenImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    [self.mainImageButton setImage:chosenImage forState:UIControlStateSelected];
    self.mainImageButton.clipsToBounds = YES;
    
    [self.delegate newImageAdded:self.closeButton];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(NSInteger)getTag
{
    return self.closeButton.tag;
}

-(NSString *)getImageByte
{
    if(self.mainImageButton.selected){
        NSData *imageData = UIImageJPEGRepresentation(self.mainImageButton.currentImage,0.0);
        NSString *byteArray = [imageData base64EncodedStringWithOptions:0];
        return byteArray;
    }else{
        return @" ";
    }
}

- (IBAction)closeBtnPressed:(id)sender {
    [self.mainImageButton setSelected:NO];
    self.closeButton.hidden = YES;
    [self.delegate deleteImageBtnPressed:self.closeButton];
}

@end
