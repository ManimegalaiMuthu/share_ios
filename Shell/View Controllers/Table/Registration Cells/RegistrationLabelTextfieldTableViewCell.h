//
//  RegistrationLabelTextfieldTableViewCell.h
//  Shell
//
//  Created by Ankita Chhikara on 26/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Constants.h"

@interface RegistrationLabelTextfieldTableViewCell : UITableViewCell
-(void) setPlaceholder: (NSString *) title;
-(void) setTitle: (NSString *) title;
-(void) setLeftTitle: (NSString *) leftTitle;
-(void) hideTopLabel: (BOOL) hidden;
-(void) setSecureTextEntry: (BOOL)isSecureTextEntry;
-(void) setKeyboard:(UIKeyboardType) keyboardType;
-(void) setTextfieldText: (NSString *) textfieldString;
-(void) setTextfieldDelegate:(id<UITextFieldDelegate>) delegate;
@end
