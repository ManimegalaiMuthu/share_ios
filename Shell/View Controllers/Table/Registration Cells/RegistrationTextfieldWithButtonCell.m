//
//  RegistrationTextfieldWithButtonCell.m
//  Shell
//
//  Created by Admin on 25/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "RegistrationTextfieldWithButtonCell.h"

@interface RegistrationTextfieldWithButtonCell()

@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UITextField *textfield;

@end

@implementation RegistrationTextfieldWithButtonCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.textfield setFont: FONT_B1];
    [self.topLabel setFont: FONT_B1];
    
    if(kIsRightToLeft) {
        [self.textfield setTextAlignment:NSTextAlignmentRight];
    } else {
        [self.textfield setTextAlignment:NSTextAlignmentLeft];
    }
}

-(void) hideTopLabel: (BOOL) hidden
{
    self.topLabel.hidden = hidden;
}

-(void) setTextfieldEnabled: (BOOL) enabled
{
    self.textfield.enabled = enabled;
}

-(void) setTextfieldText: (NSString *) textString
{
    self.textfield.text = textString;
}

-(void) setTitle: (NSString *) title
{
    self.topLabel.text = title;
    self.textfield.placeholder = title;
}

-(void) setIconImage: (NSString *) imageName
{
    [self.button setImage: [UIImage imageNamed: imageName] forState: UIControlStateNormal];
}

-(NSString *) getJsonValue
{
    if ([self.textfield.text isKindOfClass: [NSNull class]])
        return @"";
    return self.textfield.text;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setTextfieldDelegate:(id<UITextFieldDelegate>) delegate
{
    self.textfield.delegate = delegate;
}

-(void) setTextFieldTag:(NSInteger) tag {
    self.textfield.tag = tag;
}

-(void) setRequiredTitle: (NSAttributedString *) title placeholder:(NSString *)placeholderString
{
    //        self.textfield.placeholder = [NSString stringWithFormat: @"%@ %@",  LOCALIZATION(C_TEXTFIELD_PLEASEENTER) , title];
    
    self.topLabel.attributedText = title;
    self.textfield.placeholder = [NSString stringWithFormat: @"%@", placeholderString];
    
}

@end
