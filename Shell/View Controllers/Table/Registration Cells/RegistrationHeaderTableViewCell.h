//
//  RegistrationHeaderTableViewCell.h
//  Shell
//
//  Created by Ankita Chhikara on 27/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Constants.h"

@interface RegistrationHeaderTableViewCell : UITableViewCell
-(void) setTitle: (NSString *) title;
@end
