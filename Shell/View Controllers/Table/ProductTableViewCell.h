//
//  ProductTableViewCell.h
//  Shell
//
//  Created by Edenred on 27/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProductTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *productNameLbl;

@end

NS_ASSUME_NONNULL_END
