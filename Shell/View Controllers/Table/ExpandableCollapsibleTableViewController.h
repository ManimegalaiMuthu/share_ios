//
//  ExpandableCollapsibleTableViewController.h
//  Shell
//
//  Created by Nach on 9/3/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExpandableCollapsibleDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExpandableCollapsibleTableViewController : UITableViewController

-(void) initialiseTableWith:(ExpandableCollapsibleDataModel *)dataModel hasPoints:(BOOL)hasPoints;

@end

NS_ASSUME_NONNULL_END
