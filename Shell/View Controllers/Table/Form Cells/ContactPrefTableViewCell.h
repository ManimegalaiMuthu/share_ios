//
//  ContactPrefTableViewCell.h
//  Shell
//
//  Created by Nach on 2/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@interface ContactPrefTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblContactPrefLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *contactPrefColView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *contactPrefColViewLayout;

@property NSMutableArray *contactPrefArray;
@property NSString *contactPrefString;

-(NSString *) getJsonValue;
-(void) setOptional:(BOOL)isOptional;

@end

NS_ASSUME_NONNULL_END
