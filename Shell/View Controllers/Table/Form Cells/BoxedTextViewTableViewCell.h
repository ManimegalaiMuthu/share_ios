//
//  BoxedTextViewTableViewCell.h
//  Shell
//
//  Created by Jeremy Lua on 13/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BoxedTextViewTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextView *textview;
-(void) setTextviewValue: (NSString *) value;
-(NSString *) getJsonValue;
-(void) setTextviewDelegate:(id<UITextViewDelegate>) delegate;
@end
