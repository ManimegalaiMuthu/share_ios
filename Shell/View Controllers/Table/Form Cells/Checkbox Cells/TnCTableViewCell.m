//
//  TnCTableViewCell.m
//  Shell
//
//  Created by Nach on 8/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "TnCTableViewCell.h"
#import "Helper.h"
#import "Headers.h"
#import "PopupWebviewViewController.h"

@implementation TnCTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lblTnc.text = LOCALIZATION(C_RU_STEPFOUR_TNC);     //lokalised
    [Helper setHyperlinkLabel:self.lblTnc hyperlinkText: LOCALIZATION(C_RU_STEPFOUR_TNCLINK) hyperlinkFont: FONT_B2 bodyText: LOCALIZATION(C_RU_STEPFOUR_TNC) bodyFont:FONT_B2 urlString: @"MechTNC"];      //lokalised
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url absoluteString] isEqualToString: @"MechTNC"])
    {
        NSDictionary *data = GET_WORKSHOP_TNC;
        
        PopupWebviewViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPWEBVIEW];
        
        [popupVc popupInitWithHTMLBody: [data objectForKey: @"TncText"] headerTitle: LOCALIZATION(C_SIDEMENU_TNC) buttonTitle: LOCALIZATION(C_GLOBAL_OK)];  //lokalised
    }

}


@end
