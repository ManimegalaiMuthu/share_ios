//
//  CheckboxTableViewCell.m
//  Shell
//
//  Created by Jeremy Lua on 12/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "CheckboxTableViewCell.h"
#import "CheckboxCell.h"
#import "OtherCheckboxCell.h"

#import "Constants.h"
#import "Session.h"

@interface CheckboxTableViewCell ()  <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
@property NSMutableArray *cellArray;
@end
@implementation CheckboxTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lblDescription.font = FONT_B1;
    self.tableview.delegate = self;
    self.tableview.dataSource = self;

    [self.tableview registerNib:[UINib nibWithNibName:@"CheckboxCell" bundle:nil] forCellReuseIdentifier:@"CheckboxCell"];
    
    [self.tableview registerNib:[UINib nibWithNibName:@"OtherCheckboxCell" bundle:nil] forCellReuseIdentifier:@"OtherCheckboxCell"];
    self.tableview.tableFooterView = [UIView new];
    
    self.cellArray = [[NSMutableArray alloc] init];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.hasOther && indexPath.row == [self.dataArray count] - 1) //for others. always last
        return 72;
    else
        return 50;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //others will always be last.
    if (self.hasOther && indexPath.row == [self.dataArray count] - 1)
    {
        OtherCheckboxCell *cell = [tableView dequeueReusableCellWithIdentifier: @"OtherCheckboxCell"];
        
        
        cell.lblTitle.text = [[self.dataArray objectAtIndex: indexPath.row] objectForKey: @"KeyValue"];
        cell.lblTitle.font = FONT_B1;
        
        cell.textfield.placeholder = @"Please State";
        cell.textfield.font = FONT_B1;
        cell.textfield.delegate = self;
        
        if ([self.stringArray containsObject: [[self.dataArray objectAtIndex: indexPath.row] objectForKey: @"KeyCode"]])
        {
            cell.btnCheckbox.selected = YES;
            cell.textfield.text = self.otherString;
        }
        
        [self.cellArray addObject: cell];
        return cell;
    }
    else
    {
        CheckboxCell *cell = [tableView dequeueReusableCellWithIdentifier: @"CheckboxCell"];
        
        cell.lblCheckbox.text = [[self.dataArray objectAtIndex: indexPath.row] objectForKey: @"KeyValue"];
//        cell.lblCheckbox.text = [self.dataArray objectAtIndex: indexPath.row];
        cell.lblCheckbox.font = FONT_B1;
        
//        NSInteger bitValue = 1 << indexPath.row;
        if ([self.stringArray containsObject: [[self.dataArray objectAtIndex: indexPath.row] objectForKey: @"KeyCode"]])
        {
            cell.btnCheckbox.selected = YES;
        }
        
        [self.cellArray addObject: cell];
        return cell;
    }
    return [UITableViewCell new];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.delegate didEndEditing];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataArray count];
}


//-(NSString *)getJsonValue
-(NSString *)getJsonValue
{
    NSString *jsonString = @"";
    for (id cell in self.cellArray)
    {
        if ([cell isKindOfClass: [CheckboxCell class]])
        {
            CheckboxCell *checkboxCell = cell;
            if (checkboxCell.btnCheckbox.selected)
            {
                if ([jsonString isEqualToString: @""])
                    jsonString = checkboxCell.lblCheckbox.text;
                else
                    jsonString = [jsonString stringByAppendingString: [NSString stringWithFormat: @",%@", checkboxCell.lblCheckbox.text]];
            }
        }
        else if ([cell isKindOfClass: [OtherCheckboxCell class]])
        {
            OtherCheckboxCell *otherCell = cell;
            if (otherCell.btnCheckbox.selected)
            {
                if ([jsonString isEqualToString: @""])
                    jsonString = otherCell.lblTitle.text;
                else
                    jsonString = [jsonString stringByAppendingString: [NSString stringWithFormat: @",Others:%@:%@", otherCell.lblTitle.text, otherCell.textfield.text]];
            }
        }
    }
    
    return jsonString;
}

-(NSArray *)getJsonArrayToConvert
{
    NSMutableArray *jsonArray = [[NSMutableArray alloc] init];
    
    for (id cell in self.cellArray)
    {
        if ([cell isKindOfClass: [CheckboxCell class]])
        {
            CheckboxCell *checkboxCell = cell;
            if (checkboxCell.btnCheckbox.selected)
            {
                [jsonArray addObject: checkboxCell.lblCheckbox.text];
            }
        }
        else if ([cell isKindOfClass: [OtherCheckboxCell class]])
        {
            OtherCheckboxCell *otherCell = cell;
            if (otherCell.btnCheckbox.selected)
            {
                [jsonArray addObject: [NSString stringWithFormat: @"Others:%@:%@", otherCell.lblTitle.text, otherCell.textfield.text]];
            }
        }
    }
    return jsonArray;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
