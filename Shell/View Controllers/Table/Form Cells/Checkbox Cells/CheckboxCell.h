//
//  CheckboxCell.h
//  Shell
//
//  Created by Jeremy Lua on 13/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckboxCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnCheckbox;
@property (weak, nonatomic) IBOutlet UILabel *lblCheckbox;

@end
