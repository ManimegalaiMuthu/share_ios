//
//  OtherCheckboxCell.m
//  Shell
//
//  Created by Jeremy Lua on 13/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "OtherCheckboxCell.h"

@implementation OtherCheckboxCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void) setTextfieldDelegate:(id<UITextFieldDelegate>) delegate
{
    self.textfield.delegate = delegate;
}

- (IBAction)checkBoxPressed:(id)sender {
    self.btnCheckbox.selected = !self.btnCheckbox.isSelected;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
