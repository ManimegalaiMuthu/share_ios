//
//  TnCTableViewCell.h
//  Shell
//
//  Created by Nach on 8/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TnCTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnTncCheckBox;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblTnc;

@end

NS_ASSUME_NONNULL_END
