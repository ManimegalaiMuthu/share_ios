//
//  CheckboxTableViewCell.h
//  Shell
//
//  Created by Jeremy Lua on 12/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CheckboxTableViewCellDelegate
-(void) didEndEditing;
@end

@interface CheckboxTableViewCell : UITableViewCell
@property id<CheckboxTableViewCellDelegate> delegate;
@property NSArray *dataArray;
@property NSMutableArray *stringArray; //input from response
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property BOOL hasOther;
@property NSString *otherString;

-(NSArray *)getJsonArrayToConvert;
-(NSString *)getJsonValue;
@end
