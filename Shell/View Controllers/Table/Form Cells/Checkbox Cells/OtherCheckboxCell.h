//
//  OtherCheckboxCell.h
//  Shell
//
//  Created by Jeremy Lua on 13/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtherCheckboxCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *textfield;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckbox;

@end
