//
//  ButtonTableViewCell.h
//  Shell
//
//  Created by Nach on 2/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ButtonTableViewCell : UITableViewCell

//Add onTouchInside event on the viewcontroller that creates the TableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btn;

-(void) setButtonTitle:(NSString *)title;

@end

NS_ASSUME_NONNULL_END
