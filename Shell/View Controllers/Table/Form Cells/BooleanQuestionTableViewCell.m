//
//  BooleanQuestionTableViewCell.m
//  Shell
//
//  Created by Jeremy Lua on 12/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BooleanQuestionTableViewCell.h"
#import "Constants.h"

@interface BooleanQuestionTableViewCell()
{
    __weak IBOutlet UIButton *yesBtn;
    __weak IBOutlet UIButton *noBtn;
    
}
@end

@implementation BooleanQuestionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.lblQuestion.font = FONT_B1;
    self.lblQuestion.textColor = COLOUR_VERYDARKGREY;
    
    [yesBtn.titleLabel setFont: FONT_B1];
    yesBtn.titleLabel.textColor = COLOUR_VERYDARKGREY;
    
    [noBtn.titleLabel setFont: yesBtn.titleLabel.font];
    noBtn.titleLabel.textColor = yesBtn.titleLabel.textColor;
}

-(void) setJsonValue:(BOOL) isYes
{
    if (isYes)
        [self yesBtnPressed: nil];
    else
        [self noBtnPressed: nil];
}

- (IBAction)yesBtnPressed:(id)sender {
    yesBtn.selected = YES;
    [yesBtn setBackgroundColor: COLOUR_YELLOW];
    
    noBtn.selected = NO;
    [noBtn setBackgroundColor: COLOUR_LIGHTGREY];
    
    [self.delegate toggleQuestion: YES];
}


- (IBAction)noBtnPressed:(id)sender {
    noBtn.selected = YES;
    [noBtn setBackgroundColor: COLOUR_YELLOW];
    
    yesBtn.selected = NO;
    [yesBtn setBackgroundColor: COLOUR_LIGHTGREY];
    
    [self.delegate toggleQuestion: NO];
    
}
-(NSString *) getJsonValue
{
    if (yesBtn.selected)
        return @"1";
    else
        return @"0";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
