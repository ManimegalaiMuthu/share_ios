//
//  LabelWithTextBoxTableViewCell.m
//  Shell
//
//  Created by Jeremy Lua on 13/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "LabelWithTextBoxTableViewCell.h"
#import "Constants.h"

@interface LabelWithTextBoxTableViewCell ()
@property (weak, nonatomic) IBOutlet UITextField *textfield;

@end
@implementation LabelWithTextBoxTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.lblTitle setFont: FONT_B1];
    [self.textfield setFont: FONT_B1];
}
-(void) setSecureTextEntry: (BOOL)isSecureTextEntry
{
    self.textfield.secureTextEntry = isSecureTextEntry;
}
-(void) setKeyboard:(UIKeyboardType) keyboardType
{
    self.textfield.keyboardType = keyboardType;
}

-(void) setJsonValue: (NSString *) value
{
    self.textfield.text = value;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(NSString *) getJsonValue
{
    if ([self.textfield.text isKindOfClass: [NSNull class]])
        return @"";
    return self.textfield.text;
}
-(void) setTextfieldDelegate:(id<UITextFieldDelegate>) delegate
{
    self.textfield.delegate = delegate;
}
@end
