//
//  ButtonTableViewCell.m
//  Shell
//
//  Created by Nach on 2/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "ButtonTableViewCell.h"
#import "Constants.h"
#import "Helper.h"

@implementation ButtonTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.btn.titleLabel setFont:FONT_BUTTON];
    [Helper setCustomFontButtonContentModes: self.btn];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setButtonTitle:(NSString *)title {
    [self.btn setTitle:title forState:UIControlStateNormal];
}

@end
