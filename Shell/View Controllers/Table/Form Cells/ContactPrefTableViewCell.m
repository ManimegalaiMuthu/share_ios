//
//  ContactPrefTableViewCell.m
//  Shell
//
//  Created by Nach on 2/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "ContactPrefTableViewCell.h"
#import "ContactPreferencesCollectionViewCell.h"
#import "Session.h"
#import "Headers.h"

@interface ContactPrefTableViewCell ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *colViewHeight;

@end

@implementation ContactPrefTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.lblContactPrefLabel setFont: FONT_B1];
    
    [self.contactPrefColView registerNib:[UINib nibWithNibName: @"ContactPreferencesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier: @"ContactPrefCell"];
    
    self.colViewHeight.constant = 50 * ceil([[mSession getContactPreference] count] / 2.0f);
    self.contactPrefColView.backgroundColor = [UIColor clearColor];
    if(@available(iOS 10, *))
    {
        self.contactPrefColViewLayout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize;
    }
    else
    {
        self.contactPrefColViewLayout.estimatedItemSize = CGSizeMake(130,50);
    }

}

-(void) setOptional:(BOOL) isOptional {
    if(isOptional)
        self.lblContactPrefLabel.text = LOCALIZATION(C_TITLE_CONTACT_PREFERENCE); //lokalise 30 jan
    else
        self.lblContactPrefLabel.attributedText = [self addAsteriskForRequiredField:LOCALIZATION(C_TITLE_CONTACT_PREFERENCE) enabled:YES];
}

-(NSAttributedString *) addAsteriskForRequiredField: (NSString *) string enabled:(BOOL) enabled
{
    NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @"* " attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
    
    NSMutableAttributedString *titleAttrString;
    if (enabled)
        titleAttrString = [[NSMutableAttributedString alloc] initWithString: string attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
    else
        titleAttrString = [[NSMutableAttributedString alloc] initWithString: string attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
    
    [titleAttrString appendAttributedString: asteriskAttrString];
    
    return titleAttrString;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


#pragma mark - CollectionView DataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[mSession getContactPreference] count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //dont need to reload, cell is persistent in FormTableViewDelegate
    ContactPreferencesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"ContactPrefCell" forIndexPath: indexPath];
    
    UIButton *checkboxBtn = [cell viewWithTag: 1]; //tagged in storyboard
//    checkboxBtn.selected  = YES;
    
    UILabel *contactPref = [cell viewWithTag: 2]; //tagged in storyboard
    contactPref.text = [[mSession getContactPreference] objectAtIndex: [indexPath row]];
    contactPref.font = FONT_B2;
    
    cell.bitValue = [[mSession convertToContactPreferenceKeyCode: contactPref.text] intValue];
    
    if ([[self.contactPrefString componentsSeparatedByString: @","] containsObject: @(cell.bitValue).stringValue])
        checkboxBtn.selected = YES;
    else
        checkboxBtn.selected = NO;


    if (!self.contactPrefArray)
        self.contactPrefArray = [[NSMutableArray alloc] init];
    [self.contactPrefArray addObject: cell];
    
    return cell;
}

-(NSString *) getJsonValue
{
    NSString *contactPrefString = @"";
    for (ContactPreferencesCollectionViewCell* cell in self.contactPrefArray)
    {
        if ([cell.checkboxBtn isSelected])
        {
            if (contactPrefString.length == 0)
                contactPrefString = @(cell.bitValue).stringValue;
            else
                contactPrefString = [contactPrefString stringByAppendingString: [NSString stringWithFormat: @",%@", @(cell.bitValue).stringValue]];
        }
    }
    return contactPrefString;
}


@end
