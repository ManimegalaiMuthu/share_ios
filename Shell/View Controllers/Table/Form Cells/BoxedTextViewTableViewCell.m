//
//  BoxedTextViewTableViewCell.m
//  Shell
//
//  Created by Jeremy Lua on 13/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BoxedTextViewTableViewCell.h"
#import "Constants.h"

@implementation BoxedTextViewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.lblTitle setFont: FONT_B1];
    [self.textview setFont: FONT_B1];
}

-(void) setTextviewValue: (NSString *) value
{
    self.textview.text = value;
}

-(void) setTextviewDelegate:(id<UITextViewDelegate>) delegate
{
    self.textview.delegate = delegate;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(NSString *) getJsonValue
{
    if ([self.textview.text isKindOfClass: [NSNull class]])
        return @"";
    return self.textview.text;
}
@end
