//
//  HeaderWithReqTableViewCell.m
//  Shell
//
//  Created by Nach on 2/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "HeaderWithReqTableViewCell.h"
#import "Constants.h"

@interface HeaderWithReqTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblRequiredFields;

@end
@implementation HeaderWithReqTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.lblTitle setFont: FONT_H1];
    self.userInteractionEnabled = NO;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setTitle:(NSString *)title {
    [self.lblTitle setText:title];
}

-(void) setRequiredLabelText:(NSString *)requiredText {
    NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @"*" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B3, }];
    
    NSMutableAttributedString *requiredFieldsAttrString = [[NSMutableAttributedString alloc] initWithString: requiredText attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B3, }];
    
    [asteriskAttrString appendAttributedString:  requiredFieldsAttrString];
    
    self.lblRequiredFields.attributedText = asteriskAttrString;
}

@end
