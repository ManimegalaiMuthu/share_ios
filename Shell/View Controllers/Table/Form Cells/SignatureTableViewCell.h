//
//  SignatureTableViewCell.h
//  Shell
//
//  Created by Nach on 2/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SignatureTableViewCellDelegate
-(void) signaturePressed: (NSString *) signatureKey;
@end

@interface SignatureTableViewCell : UITableViewCell
@property id<SignatureTableViewCellDelegate> delegate;
@property NSString *signatureKey;
@property (weak, nonatomic) IBOutlet UIImageView *signatureImageView;
@property (weak, nonatomic) IBOutlet UIButton *btnSignatureCell;
@property (weak, nonatomic) IBOutlet UILabel *lblSignature;

-(NSObject *)getSignature;

@end

NS_ASSUME_NONNULL_END
