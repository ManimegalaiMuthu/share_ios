//
//  RearrangeTableViewCell.m
//  Shell
//
//  Created by Jeremy Lua on 12/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RearrangeCell.h"
#import "RearrangeTableViewCell.h"
#import "Constants.h"
#import "Session.h"


@interface RearrangeTableViewCell () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation RearrangeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.lblDescription.font = FONT_B1;
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    [self.tableview setEditing: YES];
    
    [self.tableview registerNib:[UINib nibWithNibName:@"RearrangeCell" bundle:nil] forCellReuseIdentifier:@"RearrangeCell"];
    self.tableview.tableFooterView = [UIView new];
}

-(NSArray *) getJsonArrayValue
{
    return self.dataArray;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataArray count];
}
-(void) tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    
    NSDictionary *objectToMove = [self.dataArray objectAtIndex:sourceIndexPath.row];
    [self.dataArray removeObjectAtIndex:sourceIndexPath.row];
    [self.dataArray insertObject:objectToMove atIndex:destinationIndexPath.row];
    
    //if we have multiple sections then we should also check the section,
    //sourceIndexPath.section to make sure we insert into the correct section
    
    //also if we are moving within the same section we could just use the exchange METHOD
//    [self.dataArray  exchangeObjectAtIndex:destinationIndexPath.row withObjectAtIndex:sourceIndexPath.row];
    
    [self.tableview reloadData];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *cellData = [self.dataArray objectAtIndex: indexPath.row];
    
    RearrangeCell *cell = [tableView dequeueReusableCellWithIdentifier: @"RearrangeCell"];
    cell.lblCount.text = @(indexPath.row + 1).stringValue;
    cell.lblCount.font = FONT_H1;
    
    cell.lblDescription.text = [cellData objectForKey: @"Text"];
    cell.lblDescription.font = FONT_B1;
    
    cell.shouldIndentWhileEditing = NO;
    cell.accessoryType = UITableViewCellAccessoryNone;
    //    cell.imgIcon.image =

    [cellData setValue: @(indexPath.row + 1) forKey: @"Order"];
    [self.dataArray replaceObjectAtIndex: indexPath.row withObject: cellData];
    return cell;
}

-(BOOL) tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  UITableViewCellEditingStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
