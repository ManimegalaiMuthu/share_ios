//
//  RearrangeTableViewCell.h
//  Shell
//
//  Created by Jeremy Lua on 12/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RearrangeTableViewCell : UITableViewCell
@property NSMutableArray *dataArray;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UITableView *tableview;

-(NSArray *) getJsonArrayValue;
@end
