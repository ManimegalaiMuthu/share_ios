//
//  BooleanQuestionTableViewCell.h
//  Shell
//
//  Created by Jeremy Lua on 12/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BooleanQuestionDelegate
-(void) toggleQuestion:(BOOL) toEnable;
@end

@interface BooleanQuestionTableViewCell : UITableViewCell
@property id<BooleanQuestionDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion;

-(void) setJsonValue:(BOOL) isYes;
-(NSString *) getJsonValue;
@end
