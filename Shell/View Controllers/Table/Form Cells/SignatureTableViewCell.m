//
//  SignatureTableViewCell.m
//  Shell
//
//  Created by Nach on 2/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "SignatureTableViewCell.h"
#import "RUCustomerSignatureViewController.h"

@interface SignatureTableViewCell() <RUCustomerSignatureDelegate>

@end

@implementation SignatureTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lblSignature.text = LOCALIZATION(C_RUSSIA_CUSTOMERSIGNATURE);      //lokalised
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)signaturePressed:(id)sender
{
    [self.delegate signaturePressed: self.signatureKey];
}

-(NSObject *)getSignature {
    return [UIImageJPEGRepresentation(self.signatureImageView.image, 0.5f) base64EncodedStringWithOptions:0];
}

-(NSString *) getJsonValue
{
    if (self.signatureImageView.image)
        return [UIImageJPEGRepresentation(self.signatureImageView.image, 0.5f) base64EncodedStringWithOptions:0];
    else
        return @"";
}

@end
