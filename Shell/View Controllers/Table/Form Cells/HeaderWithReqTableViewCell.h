//
//  HeaderWithReqTableViewCell.h
//  Shell
//
//  Created by Nach on 2/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HeaderWithReqTableViewCell : UITableViewCell

-(void) setTitle:(NSString *)title;
-(void) setRequiredLabelText:(NSString *)requiredText;

@end

NS_ASSUME_NONNULL_END
