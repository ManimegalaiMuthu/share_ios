//
//  LabelWithTextBoxTableViewCell.h
//  Shell
//
//  Created by Jeremy Lua on 13/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LabelWithTextBoxTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
-(void) setTextfieldDelegate:(id<UITextFieldDelegate>) delegate;
-(NSString *) getJsonValue;
-(void) setSecureTextEntry: (BOOL)isSecureTextEntry;
-(void) setKeyboard:(UIKeyboardType) keyboardType;
-(void) setJsonValue: (NSString *) value;
@end
