//
//  StaffFormViewController.m
//  Shell
//
//  Created by Nach on 2/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "StaffFormViewController.h"
#import "CASDatePickerView.h"
#import "PopupWebviewViewController.h"
#import "RUCustomerSignatureViewController.h"

#define CELL_HEIGHT 72

enum kForm_ManageMechanicAddNew_TYPE
{
    kManageMechanicAddNew_HEADER = 0,
    kManageMechanicAddNew_SALUTATION,
    kManageMechanicAddNew_FIRSTNAME,
    kManageMechanicAddNew_LASTNAME,
    kManageMechanicAddNew_MOBNUM,
    kManageMechanicAddNew_EMAIL,
    kManageMechanicAddNew_DOB,
};

enum {
    kStaffFormTable_Data,
    kStaffFormTable_SubmitButton,
}kStaffFormTable_TAG;


#define kUserID             @"UserID"
#define kDSRCode            @"DSRCode"
#define kUserPassword       @"UserPassword"
#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kEmailAddress       @"EmailAddress"
#define kDOB                @"DOB"
#define kDOBMonth           @"DOBMonth"
#define kDOBDay             @"DOBDay"
#define kRedeem             @"Redeem"
#define kSignature          @"Signature"
#define kTnC                @"TnC"

@interface StaffFormViewController () <UITableViewDelegate,UITableViewDataSource,CommonListDelegate,DatePickerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;

@property Member *member;
@property CASDatePickerView *dateView;

@end

@implementation StaffFormViewController
@synthesize listView;
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupGoogleAnalytics];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationFormTextfieldCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormTextfieldCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationButtonCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationFormDropdownCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormDropdownCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationHeaderTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationLabelTextfieldTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationLabelTextfieldTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationMobileNumberCell" bundle:nil] forCellReuseIdentifier:@"RegistrationMobileNumberCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationTextfieldWithButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextfieldWithButtonCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationTextViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextViewTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"CheckboxCell" bundle:nil] forCellReuseIdentifier:@"CheckboxCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"SignatureTableViewCell" bundle:nil] forCellReuseIdentifier:@"SignatureTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ButtonTableViewCell" bundle:nil] forCellReuseIdentifier:@"ButtonTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"TnCTableViewCell" bundle:nil] forCellReuseIdentifier:@"TnCTableViewCell"];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

-(void) setupGoogleAnalytics
{
//    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//    [tracker set:kGAIScreenName value:@"Mechanic Add New"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


-(void) setupInterface {
    
    self.tableView.tableFooterView = [UIView new];
    [self.tableView reloadData];
    
    self.regSubmissionDict = [[NSMutableDictionary alloc] init];
    
    self.member = [[Member alloc] init];
    
    
    //Dropdown and date setup
    listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_DROPDOWN_SALUTATION)
                                                   list: [mSession getSalutations]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];       //lokalised
    listView.delegate = self;
    
    
    dateView = [[CASDatePickerView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_BIRTHDAY) previouslySelected:nil withYearHidden:YES];   //lokalised
    
    dateView.delegate = self;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - TableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //Data + ButtonCell
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch(section) {
        case kStaffFormTable_Data:
            return [self.registrationFormArray count];
        case kStaffFormTable_SubmitButton:
            return 1;
        default:
            return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == kStaffFormTable_SubmitButton) {
        ButtonTableViewCell *buttonCell = [tableView dequeueReusableCellWithIdentifier:@"ButtonTableViewCell"];
        [buttonCell setButtonTitle:LOCALIZATION(C_FORM_REGISTERNEWSTAFF)];      //loklaised
        [buttonCell.btn addTarget: self action: @selector(submitButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        return buttonCell;
    }
    
    NSDictionary *regFieldData = [self.registrationFormArray objectAtIndex: indexPath.row];
    switch ([[regFieldData objectForKey: @"Type"] intValue])
    {
        case kREGISTRATION_TEXTFIELD:
        {
            RegistrationFormTextfieldCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormTextfieldCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [textfieldCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [textfieldCell hideTopLabel: NO];
//            [textfieldCell setTextfieldDelegate:self];
//            [textfieldCell setTextFieldTag:indexPath.row];
            
            if ([regFieldData objectForKey: @"Value"])
                [textfieldCell setTextfieldText: [regFieldData objectForKey: @"Value"]];
            if ([regFieldData objectForKey: @"Placeholder"])
                [textfieldCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_DROPDOWN:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [dropdownCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [dropdownCell hideTopLabel: NO];
            dropdownCell.button.tag = indexPath.row;
            
            [dropdownCell.button addTarget: self action: @selector(dropdownPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            //set initial value
            if ([regFieldData objectForKey: @"Value"])
                [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            
            //keep a reference of cell to submit json value later
            [self.regSubmissionDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        case kREGISTRATION_TEXTFIELDWITHBUTTON:
        {
            RegistrationTextfieldWithButtonCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationTextfieldWithButtonCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            if ([regFieldData objectForKey: @"TextfieldEnabled"])
                [textfieldCell setTextfieldEnabled: [[regFieldData objectForKey: @"TextfieldEnabled"] boolValue]];
            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            [textfieldCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [textfieldCell hideTopLabel: NO];
//            [textfieldCell setTextfieldDelegate:self];
//            [textfieldCell setTextFieldTag:indexPath.row];
            
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_BUTTON:
        {
            RegistrationButtonCell *buttonCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationButtonCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [buttonCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [buttonCell setTitle: [regFieldData objectForKey: @"Title"]];
            [buttonCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [buttonCell hideTopLabel: NO];
            
            [self.regSubmissionDict setObject: buttonCell forKey: [regFieldData objectForKey: @"Key"]];
            return buttonCell;
        }
            break;
        case kREGISTRATION_MOBNUM:
        {
            RegistrationMobileNumberCell *mobNumCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationMobileNumberCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [mobNumCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            if ([regFieldData objectForKey: @"TextfieldEnabled"])
                [mobNumCell setTextfieldEnabled: [[regFieldData objectForKey: @"TextfieldEnabled"] boolValue]];
            [mobNumCell setMobileCodeEnabled: NO];
            [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [mobNumCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [mobNumCell hideTopLabel: NO];
//            [mobNumCell setTextfieldDelegate:self];
//            [mobNumCell setTextFieldTag:indexPath.row];
            
            [mobNumCell setCountryCode: [regFieldData objectForKey: @"CountryCode"]];
            [mobNumCell setMobileNumber: [regFieldData objectForKey: @"MobileNumber"]];
            
            [self.regSubmissionDict setObject: mobNumCell forKey: [regFieldData objectForKey: @"Key"]];
            return mobNumCell;
        }
            break;
        case kREGISTRATION_LABELWITHTEXTFIELD:
        {
            RegistrationLabelTextfieldTableViewCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationLabelTextfieldTableViewCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [textfieldCell setLeftTitle: [regFieldData objectForKey: @"Title"]];
            [textfieldCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
//            [textfieldCell hideTopLabel: YES];
            
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_BIRTHDATE:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            //Salutation
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            [dropdownCell hideTopLabel: NO];
            dropdownCell.button.tag = indexPath.row;
            
            [dropdownCell.button addTarget: self action: @selector(birthdayPressed:) forControlEvents:UIControlEventTouchUpInside];
            [self.regSubmissionDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        case kREGISTRATION_HEADER:
        {
            RegistrationHeaderTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationHeaderTableViewCell"];
            [headerCell setTitle: [regFieldData objectForKey: @"Title"]];
            return headerCell;
        }
            break;
        case kPROFILING_CHECKBOX:
        {
            CheckboxCell *checkboxCell = [tableView dequeueReusableCellWithIdentifier:@"CheckboxCell"];
            [checkboxCell.lblCheckbox setFont: FONT_B2];
            checkboxCell.lblCheckbox.text = LOCALIZATION(C_MECH_REDEEMFLAG);        //lokalised
            return checkboxCell;
        }
            break;
        case kREGISTRATION_SIGNATURE:
        {
            SignatureTableViewCell *signatureCell = [tableView dequeueReusableCellWithIdentifier:@"SignatureTableViewCell"];
            [signatureCell.btnSignatureCell addTarget:self action:@selector(signaturePressed:) forControlEvents:UIControlEventTouchUpInside];
            return signatureCell;
        }
            break;
        case kREGISTRATION_BUTTONCELL:
        {
            ButtonTableViewCell *buttonCell = [tableView dequeueReusableCellWithIdentifier:@"ButtonTableViewCell"];
            [buttonCell setButtonTitle:LOCALIZATION(C_FORM_REGISTERNEWSTAFF)];      //lokalised
            return buttonCell;
        }
            break;
        case kREGISTRATION_TNCCHECKBOX:
        {
            TnCTableViewCell *tncCell = [tableView dequeueReusableCellWithIdentifier:@"TnCTableViewCell"];
            return tncCell;
        }
            break;
        default:
            NSLog(@"UNKNOWN CELL TYPE");
            return [UITableViewCell new];
            break;
    }
    return [UITableViewCell new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[[_registrationFormArray objectAtIndex:indexPath.row]objectForKey:@"Type"]intValue] == kPROFILING_CHECKBOX || [[[_registrationFormArray objectAtIndex:indexPath.row]objectForKey:@"Type"]intValue] == kREGISTRATION_TNCCHECKBOX) {
        return 80.0;
    } else if([[[_registrationFormArray objectAtIndex:indexPath.row]objectForKey:@"Type"]intValue] == kREGISTRATION_SIGNATURE){
        return 220.0;
    } else if (indexPath.section == kStaffFormTable_SubmitButton) {
        return 50;
    }
    return CELL_HEIGHT;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(section == kStaffFormTable_SubmitButton) {
        return 20.0;
    }
    return 0;
}

#pragma mark - Submit Button
-(void)submitButtonPressed:(UIButton *)sender {
    self.member.firstName =   [[self.regSubmissionDict objectForKey: kFirstName] getJsonValue];
    self.member.lastName =    [[self.regSubmissionDict objectForKey: kLastName] getJsonValue];
    self.member.mobileNumber = [[self.regSubmissionDict objectForKey: kMobileNumber] getMobileNumberJsonValue];
    self.member.emailAddress = [[self.regSubmissionDict objectForKey: kEmailAddress] getJsonValue];
    self.member.countryCode  = GET_COUNTRY_CODE;
    self.member.salutation   = [[mSession convertToSalutationKeyCode:[[self.regSubmissionDict objectForKey: kSalutation] getJsonValue]] intValue];
    
    CheckboxCell *checkboxcell;
    
    NSMutableDictionary *registerParams = [[NSMutableDictionary alloc] init];
    
    [registerParams setObject: @(self.member.salutation) forKey:@"Salutation"];
    [registerParams setObject:self.member.firstName forKey:@"FirstName"];
    [registerParams setObject:self.member.lastName forKey:@"LastName"];
    [registerParams setObject:self.member.mobileNumber forKey:@"MobileNumber"];
    [registerParams setObject:self.member.emailAddress forKey: @"EmailAddress"];
    [registerParams setObject:self.member.dobMonth forKey: @"DOBMonth"];
    [registerParams setObject:self.member.dobDay forKey:@"DOBDay"];
    checkboxcell = [self.regSubmissionDict objectForKey:kRedeem];
    [registerParams setObject:@(checkboxcell.btnCheckbox.selected) forKey:@"EnableCustomerRedemption"];
    
    if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_RUSSIA]) {
        SignatureTableViewCell *signatureTableViewCell = [self.regSubmissionDict objectForKey:kSignature];
        [registerParams setObject:signatureTableViewCell.getSignature  forKey: @"DigitalSignature"];
        
        TnCTableViewCell *tncTableViewCell = [self.regSubmissionDict objectForKey:kTnC];
        [registerParams setObject:@(tncTableViewCell.btnTncCheckBox.selected).stringValue forKey: @"TermCondition"];
    }
    
    [self.delegate registerStaff:registerParams];
}

#pragma mark - dropdown handlers
-(void) dropdownPressed:(UIButton *)sender
{
    [self.view endEditing: YES];
    listView.tag = sender.tag;
    
    switch (listView.tag)
    {
        case kManageMechanicAddNew_SALUTATION:
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO]; //lokalised
            break;
        default:
            break;
    }
    [self popUpView];
}

- (IBAction)birthdayPressed:(UIButton *)sender {
    [self.view endEditing: YES];
    
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}

- (void)selectedDate:(NSDate *)selectedDate
{
    //do smething
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //   for API
    [formatter setDateFormat:@"dd"];
    self.member.dobDay = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"MM"];
    self.member.dobMonth = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"dd MMMM"];
    
    if ([GET_LOCALIZATION isEqualToString: kThai])
        [formatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
    else
        [formatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    NSString *selectedDateString = [formatter stringFromDate:selectedDate];
    
    
    RegistrationFormDropdownCell *cell = [self.regSubmissionDict objectForKey: kDOB];
    [cell setSelectionTitle: selectedDateString];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        RegistrationFormDropdownCell *cell;
        switch (listView.tag)
        {
            case kManageMechanicAddNew_SALUTATION:
                cell = [self.regSubmissionDict objectForKey: kSalutation];
                break;
            default:
                return;
                break;
        }
        [cell.button setTitle:selection.firstObject forState:UIControlStateNormal];
    }
}
-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

#pragma mark - signature handler
-(IBAction) signaturePressed:(UIButton *)sender {
    [mSession pushRUCustomerSignatureWithDelegate:self delegate:self];
}

- (void)didFinishSignature:(UIImage *)image {
    SignatureTableViewCell *signatureTableViewCell = [self.regSubmissionDict objectForKey:kSignature];
    signatureTableViewCell.signatureImageView.image = image;
}

@end
