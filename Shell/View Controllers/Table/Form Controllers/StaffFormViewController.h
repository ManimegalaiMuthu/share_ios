//
//  StaffFormViewController.h
//  Shell
//
//  Created by Nach on 2/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@protocol StaffRegistrationDelegate
-(void)registerStaff:(NSDictionary *)registrationDict;
@end

@interface StaffFormViewController : BaseVC

@property NSMutableArray *registrationFormArray; //ui only
@property NSMutableDictionary *regSubmissionDict;  //key as json key, value = cell. retrieve value by using getJson method
@property id<StaffRegistrationDelegate> delegate;

-(void) setupInterface;

@end

NS_ASSUME_NONNULL_END
