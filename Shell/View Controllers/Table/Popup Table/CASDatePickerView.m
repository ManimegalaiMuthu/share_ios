//
//  CASDatePickerView.m
//  Casio
//
//  Created by Ankita Chhikara on 13/9/16.
//  Copyright © 2016 Edenred. All rights reserved.
//

#import "CASDatePickerView.h"
#import "Constants.h"
#import "LocalizationManager.h"
#import "LocalizationConstants.h"

typedef enum {
    kDateComponent = 0,
    kMonthComponent,
    kYearComponent
} CalanderComponent;

#define kLeapYear @"2016"

@interface CASDatePickerView () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (nonatomic, weak) IBOutlet UIPickerView *datePicker;
@property (nonatomic, strong) NSArray *dateArray;
@property (nonatomic, strong) NSArray *monthArray;
@property (nonatomic, strong) NSMutableArray *yearArray;
@property (nonatomic, assign) BOOL hideYear;
@property (weak, nonatomic) IBOutlet UIView *selectionTopLine;
@property (weak, nonatomic) IBOutlet UIView *selectionBtmLine;

@end

@implementation CASDatePickerView

- (id)initWithTitle:(NSString*)title previouslySelected:(NSDate*)previouslySelected withYearHidden:(BOOL)hideYear {
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CASDatePickerView" owner:self options:nil];
    self = nib[0];
    
    [self.backgroundView addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(cancelButtonAction:)]];
    
    self.titleLabel.text = title;
    
    self.titleLabel.font = FONT_H1;
    self.titleLabel.textColor = COLOUR_VERYDARKGREY;
    if(kIsRightToLeft) {
        [self.titleLabel setTextAlignment:NSTextAlignmentRight];
    } else {
        [self.titleLabel setTextAlignment:NSTextAlignmentLeft];
    }
    
    [self.doneButton setTitle: LOCALIZATION(C_FORM_DONE) forState: UIControlStateNormal];       //lokalised
    [self.doneButton.titleLabel setFont: FONT_BUTTON];
    [self.doneButton setTintColor: COLOUR_WHITE];
    [self.doneButton setBackgroundColor: COLOUR_RED];
    
    [self.cancelButton setTitle: LOCALIZATION(C_FORM_CANCEL) forState: UIControlStateNormal];   //loklaised
    [self.cancelButton.titleLabel setFont: self.doneButton.titleLabel.font];
    [self.cancelButton setTintColor:       COLOUR_VERYDARKGREY];
    [self.cancelButton setBackgroundColor: COLOUR_YELLOW];
//    [self.cancelButton setTintColor:       self.doneButton.tintColor];
//    [self.cancelButton setBackgroundColor: self.doneButton.backgroundColor];
    
    [self.selectionTopLine setBackgroundColor: COLOUR_LIGHTGREY];
    [self.selectionBtmLine setBackgroundColor: self.selectionTopLine.backgroundColor];
    
    self.hideYear = hideYear;

    self.dateArray = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10",
                       @"11", @"12", @"13", @"14", @"15", @"16", @"17", @"18", @"19", @"20",
                       @"21", @"22", @"23", @"24", @"25", @"26", @"27", @"28", @"29", @"30", @"31"];
//    self.monthArray = @[@"January", @"February", @"March", @"April", @"May", @"June", @"July", @"August", @"September", @"October", @"November", @"December"];
    
    self.monthArray = @[LOCALIZATION(C_MONTH_JAN),
                        LOCALIZATION(C_MONTH_FEB),
                        LOCALIZATION(C_MONTH_MAR),
                        LOCALIZATION(C_MONTH_APR),
                        LOCALIZATION(C_MONTH_MAY),
                        LOCALIZATION(C_MONTH_JUN),
                        LOCALIZATION(C_MONTH_JUL),
                        LOCALIZATION(C_MONTH_AUG),
                        LOCALIZATION(C_MONTH_SEP),
                        LOCALIZATION(C_MONTH_OCT),
                        LOCALIZATION(C_MONTH_NOV),
                        LOCALIZATION(C_MONTH_DEC),
                        ];      //lokalised
    self.yearArray = [NSMutableArray new];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:[NSDate date]];
    NSInteger year = [components year];
    
    for (int loop = 1950; loop <= year; loop++) {
        [self.yearArray addObject:[NSString stringWithFormat:@"%d",loop]];
    }
    
    self.datePicker.showsSelectionIndicator = NO;
    
//    NSDate *preselectDate = [NSDate date];
//    if (previouslySelected != nil) {
//        preselectDate = previouslySelected;
//    }
//    components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:preselectDate];
//    year = [components year];
//    NSInteger month = [components month];
//    NSInteger day = [components day];
//    
//    NSInteger yearIndex = [self.yearArray indexOfObject:[NSString stringWithFormat:@"%d",(int)year]];
    
//    [self.datePicker selectRow:month-1 inComponent:kMonthComponent animated:NO];
//    [self.datePicker selectRow:day-1 inComponent:kDateComponent animated:NO];
//    if (!self.hideYear) {
//        [self.datePicker selectRow:yearIndex inComponent:kYearComponent animated:NO];
//    }
    
    return self;
}

#pragma mark - Picker view delegates

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    if (self.hideYear) {
        return 2;
    } else {
        return 3;
    }
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    CalanderComponent currentComponent = (int)component;
    NSInteger rows = 0;
    switch (currentComponent) {
        case kDateComponent:
            rows = self.dateArray.count;
            break;
            
        case kMonthComponent:
            rows = self.monthArray.count;
            break;
            
        case kYearComponent:
            rows = self.yearArray.count;
            break;
    }
    return rows;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 50;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    //Hide selection indicator of picker
    for (UIView *tempView in pickerView.subviews) {
        tempView.backgroundColor = [UIColor clearColor];
    }
    
    UILabel *rowLabel = [UILabel new];
//    rowLabel.font = [UIFont fontWithName:@"DINPro-Medium" size:14.0f];
    rowLabel.font = FONT_B1;
    rowLabel.textAlignment = NSTextAlignmentCenter;
    
    CalanderComponent currentComponent = (int)component;
    switch (currentComponent) {
        case kDateComponent:
            rowLabel.text = self.dateArray[row];
            break;
            
        case kMonthComponent:
            rowLabel.text = self.monthArray[row];
            break;
            
        case kYearComponent:
            rowLabel.text = self.yearArray[row];
            break;
    }
    
    return rowLabel;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:[NSDate date]];
    NSInteger year = [components year];
    NSInteger month = [components month];
    NSInteger day = [components day];
    NSString *selectedYear;
    if (self.hideYear) {
        selectedYear = kLeapYear;
    } else{
        selectedYear = self.yearArray[[self.datePicker selectedRowInComponent:kYearComponent]];
    }
    NSString *selectedMonth = [NSString stringWithFormat:@"%d",(int)[self.datePicker selectedRowInComponent:kMonthComponent] + 1];
    NSString *selectedDate = self.dateArray[[pickerView selectedRowInComponent:kDateComponent]];
    
    
    
    if (!self.hideYear) {
        //Check for current date -  Do Not allow future date
        if (year == [selectedYear integerValue]) {
            if (month == [selectedMonth integerValue]) {
                if (day < [selectedDate integerValue]) {
                    [pickerView selectRow:day-1 inComponent:kDateComponent animated:YES];
                }
            } else if (month < [selectedMonth integerValue]) {
                [pickerView selectRow:month-1 inComponent:kMonthComponent animated:YES];
                if (day < [selectedDate integerValue]) {
                    [pickerView selectRow:day-1 inComponent:kDateComponent animated:YES];
                }
            }
        }
        if ([selectedMonth intValue] == 2) {
            if ([self isLeapYear:[selectedYear integerValue]] && [selectedDate integerValue] > 29) {
                [pickerView selectRow:28 inComponent:kDateComponent animated:YES];
            } else if ([selectedDate integerValue] > 28) {
                [pickerView selectRow:27 inComponent:kDateComponent animated:YES];
            }
        }
        //Check for days month with 30 days
        if (![self is31DaysMonth:[selectedMonth integerValue]] && [selectedDate integerValue] > 30) {
            [pickerView selectRow:29 inComponent:kDateComponent animated:YES];
        }
    } else {
        //Check for february
        if ([selectedMonth intValue] == 2) {
            if ([selectedDate integerValue] > 29) {
                [pickerView selectRow:28 inComponent:kDateComponent animated:YES];
            }
        }
        //Check for days month with 30 days
        else if (![self is31DaysMonth:[selectedMonth integerValue]] && [selectedDate integerValue] > 30) {
            [pickerView selectRow:29 inComponent:kDateComponent animated:YES];
        }
    }
    
}

- (BOOL)isLeapYear:(NSInteger)year {
    if (year%4 == 0) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)is31DaysMonth:(NSInteger)month {
    NSArray *longerMonthArray = @[@"1",@"3",@"5",@"7",@"8",@"10",@"12"];
    if ([longerMonthArray containsObject:[NSString stringWithFormat:@"%d",(int)month]]) {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - Delegate Action
- (IBAction)doneButtonAction:(id)sender {
    NSString *selectedYear;
    if (self.hideYear) {
        selectedYear = kLeapYear;
    } else{
        selectedYear = self.yearArray[[self.datePicker selectedRowInComponent:kYearComponent]];
    }
    NSString *selectedMonth = [NSString stringWithFormat:@"%d",(int)[self.datePicker selectedRowInComponent:kMonthComponent] + 1];
    NSString *selectedDate = self.dateArray[[self.datePicker selectedRowInComponent:kDateComponent]];
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:[selectedDate integerValue]];
    [comps setMonth:[selectedMonth integerValue]];
    [comps setYear:[selectedYear integerValue]];
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:comps];
    [self.delegate selectedDate:date];
    [self removeFromSuperview];
}

- (IBAction)cancelButtonAction:(id)sender {
    [self removeFromSuperview];
}

@end
