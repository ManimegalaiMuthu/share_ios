//
//  CASCommonListCell.h
//  Casio
//
//  Created by Ankita Chhikara on 12/8/16.
//  Copyright © 2016 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CASCommonListCellDelegate <NSObject>
@end


@interface CASCommonListCell : UITableViewCell
@property id<CASCommonListCellDelegate> delegate;

- (void)setTableText:(NSString*)cellText;
- (void)rowIsSelected:(BOOL)isSelected;
- (void)setCellImage:(NSString*)imageName;

@end
