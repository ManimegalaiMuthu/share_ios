//
//  NativeDatePicker.h
//  Shell
//
//  Created by Admin on 16/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  NativeDatePickerDelegate<NSObject>
@optional
-(void)getDateAsString:(NSString *)dateString;
-(void)getDateAsDate:(NSDate *)date;
@end
@interface NativeDatePicker : UIView
@property id<NativeDatePickerDelegate> delegate;
- (id)initWithDatepickerTitle:(NSString*)title maximumDate:(NSDate *)maxiDate minimumDate:(NSDate *)miniDate inputDateFormatter:(NSDateFormatter *)inputFormat outputDateFormatter:(NSDateFormatter *)outputFormat;
@end
