//
//  CASCommonListView.h
//  Casio
//
//  Created by Ankita Chhikara on 10/8/16.
//  Copyright © 2016 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@protocol CommonListDelegate <NSObject>

- (void)dropDownSelection:(NSArray *)selection;

@end

@interface CASCommonListView : UIView
@property NSString *formKey;
@property (nonatomic, weak) id<CommonListDelegate> delegate;

- (id)initWithTitle:(NSString*)title list:(NSArray*)list selectionType:(CommonListSelectionType)selectionType previouslySelected:(NSArray*)previouslySelected;
- (id)initWithTitle:(NSString*)title list:(NSArray*)list selectionType:(CommonListSelectionType)selectionType previouslySelected:(NSArray*)previouslySelected withMinimumLimit:(int)minLimit andMaxLimit:(int)maxLimit disableDismissal:(BOOL)dismissal;

- (id)initWithTitle:(NSString*)title list:(NSArray*)list selectionType:(CommonListSelectionType)selectionType previouslySelected:(NSArray*)previouslySelected disableDismiss:(BOOL)disableDismiss;

-(void) setTitle:(NSString *)title;
-(void) setList:(NSArray *)list;
-(void) setTitleAndList:(NSString *)title list:(NSArray *)list hasSearchField:(BOOL) hasSearchField;
-(void) setTitleAndListWithAccessbilityTimer:(NSString *)title list:(NSArray *)list hasSearchField:(BOOL) hasSearchField accessibilityTimer:(NSTimeInterval) timeInterval;
-(void) setHasSearchField:(BOOL) hasSearchField;
@end
