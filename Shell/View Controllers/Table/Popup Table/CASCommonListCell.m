//
//  CASCommonListCell.m
//  Casio
//
//  Created by Ankita Chhikara on 12/8/16.
//  Copyright © 2016 Edenred. All rights reserved.
//

#import "CASCommonListCell.h"
#import "Constants.h"

@interface CASCommonListCell()

@property (nonatomic, weak) IBOutlet UILabel *tableCellLabel;
@property (nonatomic, weak) IBOutlet UIImageView *selectedImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectedImageWidth;

@end

@implementation CASCommonListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectedImage.hidden = YES;
    self.selectedImageWidth.constant = 0.0;
    self.tableCellLabel.font = FONT_B1;
    
    if(kIsRightToLeft) {
        self.tableCellLabel.textAlignment = NSTextAlignmentRight;
    } else {
        self.tableCellLabel.textAlignment =  NSTextAlignmentLeft;
    }
    
    self.tag = 0;
}

- (void)setTableText:(NSString*)cellText {
    self.tableCellLabel.text = cellText;
}

- (void)rowIsSelected:(BOOL)isSelected {
    self.selectedImage.hidden = NO;
    self.selectedImageWidth.constant = 40.0;
    if (isSelected) {
        self.selectedImage.image = [UIImage imageNamed: @"checkbox_tick"];
    } else {
        self.selectedImage.image = [UIImage imageNamed: @"checkbox"];
    }
}

- (void)setCellImage:(NSString*)imageName {
    self.selectedImage.hidden = NO;
    self.selectedImageWidth.constant = 40.0;
    self.selectedImage.image = [UIImage imageNamed:imageName];
}

//- (void)setSelected:(BOOL)selected animated:(BOOL)animated
//{
//    [super setSelected:selected animated:animated];
//    if (selected)
//    {
//        self.selectedImage.image = [UIImage imageNamed:bullet_selected_image];
//    } else {
//        self.selectedImage.image = [UIImage imageNamed:bullet_image];
//    }
//}

@end
