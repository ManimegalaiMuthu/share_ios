//
//  CASMonthYearPicker.h
//  Casio
//
//  Created by Ankita Chhikara on 13/9/16.
//  Copyright © 2016 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MonthYearPickerDelegate <NSObject>

- (void)selectedMonthYear:(NSDate *)selection;

@end

@interface CASMonthYearPicker : UIView

@property (nonatomic, weak) id<MonthYearPickerDelegate> delegate;

- (id)initWithTitle:(NSString*)title previouslySelected:(NSDate*)previouslySelected withYearHidden:(BOOL)hideYear;

@end
