//
//  CASCommonListView.m
//  Casio
//
//  Created by Ankita Chhikara on 10/8/16.
//  Copyright © 2016 Edenred. All rights reserved.
//

#import "LocalizationManager.h"
#import "CASCommonListView.h"
#import "CASCommonListCell.h"
#import "IQKeyboardManager.h"
//#import "ProfileList.h"

#define COMMON_LIST_VIEW @"CASCommonListView"
#define COMMON_LIST_CELL @"CASCommonListCell"


@interface CASCommonListView () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, CASCommonListCellDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIButton *doneButton;
@property (nonatomic, weak) IBOutlet UIButton *cancelButton;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *tableHeight;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *buttonHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *dividerHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *cancelButtonHeightConstraint;
@property (nonatomic, strong) NSMutableArray *tableList;
@property (nonatomic, strong) NSMutableArray *resultArray;
@property (nonatomic, strong) NSArray *rankImagesArray;
@property (nonatomic, assign) CommonListSelectionType selectionType;
@property (nonatomic, assign) int minLimit;
@property (nonatomic, assign) int maxLimit;

@property BOOL disableDismissal;
@property (weak, nonatomic) IBOutlet UITextField *searchTxtField;
@property NSMutableArray *defaultListArray;

@property NSMutableArray *accessibleMutableElements;
@property BOOL accessbilityFirstRead; //flag to read all contents of table. to cancel timer when false
@property NSTimer *accessbilityTimer;
@property int accessbilityCurrentIndex; //count of current index
@end

@implementation CASCommonListView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithTitle:(NSString*)title list:(NSArray*)list selectionType:(CommonListSelectionType)selectionType previouslySelected:(NSArray*)previouslySelected disableDismiss:(BOOL)disableDismiss
{
    return [self initWithTitle:title list:list selectionType:selectionType previouslySelected:previouslySelected withMinimumLimit:0 andMaxLimit:0 disableDismissal:disableDismiss];
}
- (id)initWithTitle:(NSString*)title list:(NSArray*)list selectionType:(CommonListSelectionType)selectionType previouslySelected:(NSArray*)previouslySelected withMinimumLimit:(int)minLimit andMaxLimit:(int)maxLimit disableDismissal:(BOOL)dismissal {
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:COMMON_LIST_VIEW owner:self options:nil];
    self = nib[0];
    
    self.titleLabel.text = title;
    self.resultArray = [NSMutableArray arrayWithArray:previouslySelected];
    self.minLimit = minLimit;
    self.maxLimit = maxLimit;
    self.selectionType = selectionType;
    
    if (self.selectionType == ListSelectionTypeSingle) {
        self.doneButton.hidden = YES;
        self.cancelButton.hidden = YES;
        self.buttonHeightConstraint.constant = 0;
        self.dividerHeightConstraint.constant = 0;
        self.cancelButtonHeightConstraint.constant = 0;
    }
    
    if (self.selectionType == ListSelectionTypeReorder) {
        [self.tableView setEditing:YES];
        self.rankImagesArray = @[@"rank1", @"rank2", @"rank3", @"rank4", @"rank5"];
    }
    
    if (self.resultArray.count < self.minLimit) {
        self.doneButton.enabled = NO;
        self.doneButton.alpha = 0.3f;
    }
    
    [self setHasSearchField:NO];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 55;
    self.tableList = [NSMutableArray arrayWithArray:list];
    self.defaultListArray = [NSMutableArray arrayWithArray:list];
    [self.tableView reloadData];
    [self viewDidLayoutSubviews];
    
    if (self.resultArray.count > 0) {
        NSInteger index = [self.tableList indexOfObject:self.resultArray[0]];
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexpath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    }
    
    self.disableDismissal = dismissal;
    
    [self setupInterface];
    
    return self;
}

-(void) setupInterface
{
    self.titleLabel.font = FONT_H1;
    self.titleLabel.textColor = COLOUR_VERYDARKGREY;
    
    [self.doneButton.titleLabel setFont: FONT_B1];
    [self.doneButton setTintColor: COLOUR_DARKGREY];
    [self.doneButton setBackgroundColor: COLOUR_YELLOW];
    [self.doneButton.titleLabel setText: LOCALIZATION(C_FORM_DONE)];        //lokalised
    
    [self.cancelButton.titleLabel setFont: FONT_B1];
    [self.cancelButton setTintColor: COLOUR_DARKGREY];
    [self.cancelButton setBackgroundColor: COLOUR_YELLOW];
    [self.cancelButton.titleLabel setText: LOCALIZATION(C_FORM_CANCEL)];    //lokalised
    
    self.searchTxtField.font = FONT_B1;
    
    self.searchTxtField.placeholder = LOCALIZATION(C_DROPDOWN_SEARCHPLACEHOLDER);   //lokalised
    
    
    if (kIsRightToLeft)
    {
        [self.titleLabel setTextAlignment: NSTextAlignmentRight];
        [self.searchTxtField setTextAlignment: NSTextAlignmentRight];
    }
    else
    {
        [self.titleLabel setTextAlignment: NSTextAlignmentLeft];
        [self.searchTxtField setTextAlignment: NSTextAlignmentLeft];
    }
}

-(void) setTitle:(NSString *)title
{
    self.titleLabel.text = title;
}

-(void) setList:(NSArray *)list
{
    self.tableList = [NSMutableArray arrayWithArray:list];
    self.defaultListArray = [NSMutableArray arrayWithArray:list];
    [self.tableView reloadData];
    [self viewDidLayoutSubviews];
}

-(void) setHasSearchField:(BOOL) hasSearchField
{
    self.searchTxtField.hidden = !hasSearchField;
    if(self.searchTxtField.hidden) {
        [NSLayoutConstraint constraintWithItem:self.searchTxtField attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
    } else {
        [NSLayoutConstraint constraintWithItem:self.searchTxtField attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:30].active = YES;
    }
}

-(void) setTitleAndList:(NSString *)title list:(NSArray *)list hasSearchField:(BOOL) hasSearchField
{
    [self setupInterface];
    
    self.titleLabel.text = title;
    self.accessibleMutableElements = [[NSMutableArray alloc] initWithObjects: self.titleLabel, nil];
    [self.accessbilityTimer invalidate];
    self.accessbilityTimer = nil;
    self.accessbilityCurrentIndex = -1;
    self.tableList = [NSMutableArray arrayWithArray:list];
    [self setHasSearchField: hasSearchField]; //pass the same parameter in.
    self.defaultListArray = [NSMutableArray arrayWithArray:list];
    [self.tableView reloadData];
    [self viewDidLayoutSubviews];
}

-(void) setTitleAndListWithAccessbilityTimer:(NSString *)title list:(NSArray *)list hasSearchField:(BOOL) hasSearchField accessibilityTimer:(NSTimeInterval) timeInterval
{
    [self setTitleAndList: title list: list hasSearchField: hasSearchField];

    if (UIAccessibilityIsVoiceOverRunning())
    {
        UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, [self.accessibleMutableElements objectAtIndex: 0]);
        self.accessbilityTimer = [NSTimer scheduledTimerWithTimeInterval: timeInterval target: self selector:@selector(readAccessibleList) userInfo:nil repeats:YES];
        [self.accessbilityTimer fire];
    }
}

-(void)accessibilityDidSelect:(NSInteger)index hasBeenAutoAnnounced:(BOOL)hasBeenAutoAnnounced
{
    NSLog(@"did select, %@,  %i / %i", hasBeenAutoAnnounced ? @"YES" : @"NO", index, [self.accessibleMutableElements count]);
    if (hasBeenAutoAnnounced)
    {
        NSLog(@"stopping timer for auto announced");
        [self.accessbilityTimer invalidate];
        self.accessbilityTimer = nil;
        self.accessbilityCurrentIndex = -1;
    }
    else if (index > self.accessbilityCurrentIndex)
    {
        NSLog(@"stopping timer for index more than current");
        [self.accessbilityTimer invalidate];
        self.accessbilityTimer = nil;
        self.accessbilityCurrentIndex = -1;
    }
}

-(void) readAccessibleList //:(id) obj
{
    if (UIAccessibilityIsVoiceOverRunning())
    {
        self.accessbilityCurrentIndex++;
        
        NSLog(@"reading... %i / %i", self.accessbilityCurrentIndex, [self.accessibleMutableElements count]);
        if (self.accessbilityCurrentIndex >= [self.accessibleMutableElements count])
        {
            [self.accessbilityTimer invalidate];
            self.accessbilityTimer = nil;
            self.accessbilityCurrentIndex = -1;
            
            NSLog(@"cancelling... readjusting %i / %i", self.accessbilityCurrentIndex, [self.accessibleMutableElements count]);
        }
        else
        {
            UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, [self.accessibleMutableElements objectAtIndex: self.accessbilityCurrentIndex]);
        }
    }
    else
    {
        NSLog(@"reading... %i / %i", self.accessbilityCurrentIndex, [self.accessibleMutableElements count]);
        [self.accessbilityTimer invalidate];
        self.accessbilityTimer = nil;
        self.accessbilityCurrentIndex = -1;
    }
//    [obj accessibilityElementIsFocused];
//     UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, obj);
}

- (id)initWithTitle:(NSString*)title list:(NSArray*)list selectionType:(CommonListSelectionType)selectionType previouslySelected:(NSArray*)previouslySelected {
    
    return [self initWithTitle:title list:list selectionType:selectionType previouslySelected:previouslySelected withMinimumLimit:0 andMaxLimit:0 disableDismissal:NO];
}

-(void)viewDidLayoutSubviews
{
    CGFloat height = MIN(self.bounds.size.height - 71, self.tableView.contentSize.height);
    self.tableHeight.constant = height;
    [self layoutIfNeeded];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tableList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CASCommonListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"listItem"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:COMMON_LIST_CELL owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    
    [cell setTableText:self.tableList[indexPath.row]];
    
    
    switch (self.selectionType) {
        case ListSelectionTypeSingle:
        {
            if ([self.resultArray containsObject:[self.tableList objectAtIndex:indexPath.row]]) {
                cell.contentView.backgroundColor = [UIColor colorWithRed:237.0f/255.0f green:237.0f/255.0f blue:237.0f/255.0f alpha:1.0f];
            }
            break;
        }
            
        case ListSelectionTypeMultiple:
        {
            if ([self.resultArray containsObject:[self.tableList objectAtIndex:indexPath.row]]) {
                cell.contentView.backgroundColor = [UIColor colorWithRed:237.0f/255.0f green:237.0f/255.0f blue:237.0f/255.0f alpha:1.0f];

                [cell rowIsSelected:YES];
            } else {
                [cell rowIsSelected:NO];
            }
            break;
        }
            
        case ListSelectionTypeReorder:
        {
            NSString *imageName = [self.rankImagesArray objectAtIndex:indexPath.row];
            [cell setCellImage:imageName];
            [cell setShowsReorderControl:NO];
            break;
        }
    }
    cell.accessibilityLabel = self.tableList[indexPath.row];
    cell.isAccessibilityElement = YES;
    cell.delegate = self;
    cell.tag = indexPath.row + 1;
    [self.accessibleMutableElements addObject: cell];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.selectionType) {
        case ListSelectionTypeSingle:
        {
            [self.resultArray removeAllObjects];
            [self.resultArray addObject:[self.tableList objectAtIndex:indexPath.row]];
            [self doneButtonAction:nil];
            break;
        }
            
        case ListSelectionTypeMultiple:
        {
            if ([self.resultArray containsObject:[self.tableList objectAtIndex:indexPath.row]]) {
                [self.resultArray removeObject:[self.tableList objectAtIndex:indexPath.row]];
            } else if ((self.maxLimit > 0 && self.resultArray.count < self.maxLimit) || (self.minLimit == 0 && self.maxLimit == 0)) {
                    [self.resultArray addObject:[self.tableList objectAtIndex:indexPath.row]];
            }
            if (self.resultArray.count < self.minLimit) {
                [self.doneButton setEnabled:NO];
                self.doneButton.alpha = 0.3f;
            } else {
                [self.doneButton setEnabled:YES];
                self.doneButton.alpha = 1.0f;
            }
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
        }
            
        case ListSelectionTypeReorder:
            break;
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectionType == ListSelectionTypeReorder) {
        return YES;
    } else {
        return NO;
    }
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    NSString *stringToMove = [self.tableList objectAtIndex:sourceIndexPath.row];
    [self.tableList removeObjectAtIndex:sourceIndexPath.row];
    [self.tableList insertObject:stringToMove atIndex:destinationIndexPath.row];
    [tableView reloadData];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (IBAction)doneButtonAction:(id)sender
{
    if (self.selectionType == ListSelectionTypeReorder) {
        self.resultArray = self.tableList;
    }
    [self.delegate dropDownSelection:self.resultArray];
    
    [self removeFromSuperview];
}

- (IBAction)cancelButtonAction:(id)sender {
    
    if(!self.disableDismissal)
        [self removeFromSuperview];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSRange textFieldRange = NSMakeRange(0, [textField.text length]);
    if (NSEqualRanges(range, textFieldRange) && [string length] == 0)
    {
        self.tableList = [NSMutableArray arrayWithArray: self.defaultListArray];
    }
    else
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat: @"SELF CONTAINS[cd] %@", [NSString stringWithFormat: @"%@%@", self.searchTxtField.text, string]];
        
        self.tableList = [self.defaultListArray filteredArrayUsingPredicate:predicate];
        
    }
    [self.tableView reloadData];
    return YES;
}

@end
