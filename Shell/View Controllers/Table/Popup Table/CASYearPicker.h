//
//  CASYearPicker.h
//  Casio
//
//  Created by Ankita Chhikara on 13/9/16.
//  Copyright © 2016 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YearPickerDelegate <NSObject>

- (void)didSelectYear:(NSDate *)selection;

@end

@interface CASYearPicker : UIView

@property (nonatomic, weak) id<YearPickerDelegate> delegate;

- (id)initWithTitle:(NSString*)title;
@end
