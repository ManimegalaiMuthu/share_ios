//
//  NativeDatePicker.m
//  Shell
//
//  Created by Admin on 16/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "NativeDatePicker.h" //have to be first

#import "Constants.h"
#import "LocalizationManager.h"
#import "LocalizationConstants.h"

@interface NativeDatePicker()
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;
@property NSDateFormatter *inputFormatter;
@property NSDateFormatter *outputFormatter;
@end
@implementation NativeDatePicker

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithDatepickerTitle:(NSString*)title maximumDate:(NSDate *)maxiDate minimumDate:(NSDate *)miniDate inputDateFormatter:(NSDateFormatter *)inputFormat outputDateFormatter:(NSDateFormatter *)outputFormat{
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NativeDatePicker" owner:self options:nil];
    self = nib[0];
    
    self.lblTitle.text = title;
    self.lblTitle.font = FONT_H1;
    self.lblTitle.textColor = COLOUR_VERYDARKGREY;
    
    [self.btnDone setTitle: LOCALIZATION (C_FORM_DONE) forState: UIControlStateNormal];     //lokalised
    
    self.datePicker.maximumDate = maxiDate;
    self.datePicker.minimumDate = miniDate;
    
    self.inputFormatter = inputFormat;
    self.outputFormatter = outputFormat;
    
    return self;
}

- (IBAction)doneBtnPressed:(id)sender
{
    [self.delegate getDateAsString:[self.outputFormatter stringFromDate:self.datePicker.date]];
    [self removeFromSuperview];
}

@end
