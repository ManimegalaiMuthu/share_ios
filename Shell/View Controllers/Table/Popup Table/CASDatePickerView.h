//
//  CASDatePickerView.h
//  Casio
//
//  Created by Ankita Chhikara on 13/9/16.
//  Copyright © 2016 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DatePickerDelegate <NSObject>

- (void)selectedDate:(NSDate *)selection;

@end

@interface CASDatePickerView : UIView

@property (nonatomic, weak) id<DatePickerDelegate> delegate;

- (id)initWithTitle:(NSString*)title previouslySelected:(NSDate*)previouslySelected withYearHidden:(BOOL)hideYear;

@end
