//
//  CASYearPicker.m
//  Casio
//
//  Created by Ankita Chhikara on 13/9/16.
//  Copyright © 2016 Edenred. All rights reserved.
//

#import "CASYearPicker.h"
#import "Constants.h"
#import "LocalizationManager.h"
#import "LocalizationConstants.h"

//typedef enum {
//    kDateComponent = 0,
//    kMonthComponent = 0,
//    kYearComponent
//} CalanderComponent;

@interface CASYearPicker () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (nonatomic, weak) IBOutlet UIPickerView *datePicker;

//@property (nonatomic, strong) NSArray *monthArray;
@property (nonatomic, strong) NSMutableArray *yearArray;
@property (weak, nonatomic) IBOutlet UIView *selectionTopLine;
@property (weak, nonatomic) IBOutlet UIView *selectionBtmLine;

@end

@implementation CASYearPicker

- (id)initWithTitle:(NSString*)title {
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CASYearPicker" owner:self options:nil];
    self = nib[0];
    
    [self.backgroundView addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(cancelButtonAction:)]];
    
    self.titleLabel.text = title;
    
    self.titleLabel.font = FONT_H1;
    self.titleLabel.textColor = COLOUR_VERYDARKGREY;
    
    if (kIsRightToLeft) {
        [self.titleLabel setTextAlignment:NSTextAlignmentRight];
    } else {
        [self.titleLabel setTextAlignment:NSTextAlignmentLeft];
    }
    
    
    [self.doneButton setTitle: LOCALIZATION(C_FORM_DONE) forState: UIControlStateNormal];       //lokalised
    [self.doneButton.titleLabel setFont: FONT_BUTTON];
    [self.doneButton setTintColor: COLOUR_WHITE];
    [self.doneButton setBackgroundColor: COLOUR_RED];
    
    [self.cancelButton setTitle: LOCALIZATION(C_FORM_CANCEL) forState: UIControlStateNormal];   //lokalised
    [self.cancelButton.titleLabel setFont: self.doneButton.titleLabel.font];
    [self.cancelButton setTintColor:       COLOUR_VERYDARKGREY];
    [self.cancelButton setBackgroundColor: COLOUR_YELLOW];
//    [self.cancelButton setTintColor:       self.doneButton.tintColor];
//    [self.cancelButton setBackgroundColor: self.doneButton.backgroundColor];
    
    [self.selectionTopLine setBackgroundColor: COLOUR_LIGHTGREY];
    [self.selectionBtmLine setBackgroundColor: self.selectionTopLine.backgroundColor];

    self.yearArray = [NSMutableArray new];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:[NSDate date]];
    NSInteger year = [components year];
    
    for (int loop = 1950; loop <= year; loop++) {
        [self.yearArray addObject:[NSString stringWithFormat:@"%d",loop]];
    }
    
    self.datePicker.showsSelectionIndicator = NO;
    
    NSInteger yearIndex = [self.yearArray indexOfObject:[NSString stringWithFormat:@"%d",(int)year]];

    [self.datePicker selectRow:yearIndex inComponent:0 animated:NO];
    
    return self;
}

#pragma mark - Picker view delegates

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.yearArray.count;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 50;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    //Hide selection indicator of picker
    for (UIView *tempView in pickerView.subviews) {
        tempView.backgroundColor = [UIColor clearColor];
    }
    
    UILabel *rowLabel = [UILabel new];
//    rowLabel.font = [UIFont fontWithName:@"DINPro-Medium" size:14.0f];
    rowLabel.font = FONT_B1;
    rowLabel.textAlignment = NSTextAlignmentCenter;
    
    rowLabel.text = self.yearArray[row];
    
    return rowLabel;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
}

#pragma mark - Delegate Action
- (IBAction)doneButtonAction:(id)sender {
    NSString *selectedYear;
    
    selectedYear = self.yearArray[[self.datePicker selectedRowInComponent: 0]];
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay: 1];
    [comps setMonth:1];
    [comps setYear:[selectedYear integerValue]];
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:comps];
    [self.delegate didSelectYear:date];
    [self removeFromSuperview];
}

- (IBAction)cancelButtonAction:(id)sender {
    [self removeFromSuperview];
}

@end
