//
//  ShareProductTableViewCell.h
//  Shell
//
//  Created by Edenred on 27/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShareProductTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *productnameLbl;
@property (weak, nonatomic) IBOutlet UILabel *RefPtsLbl;
@property (weak, nonatomic) IBOutlet UILabel *WalkinPtsLbl;

@end

NS_ASSUME_NONNULL_END
