//
//  EbookingUtils.swift
//  ShareEbooking
//
//  Created by Admin on 8/31/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import UIKit
import iOSDropDown


class EbookingUtils{
    static var loading:UIActivityIndicatorView!
    // test 
    var txtField:UITextField?
    init() {}
    static  func statusTintColor(status: Int)->UIColor{
        switch status {
        case EbookingConstants.PENDING_WORKSHOP_STATUS,EbookingConstants.PENDING_CUSTOMER_STATUS,EbookingConstants.CANCELLED_STATUS,EbookingConstants.DECLINED_STATUS:
            return UIColor.COLOUR_SHELLRED
        case EbookingConstants.CONFIRMED_STATUS,EbookingConstants.COMPLETED_STATUS:
            return UIColor.COLOUR_DARKGREEN
        case EbookingConstants.SERVICE_STARTS_STATUS,EbookingConstants.SERVICE_ENDS_STATUS,EbookingConstants.IN_PROGRESS_STATUS:
            return UIColor.COLOUR_DARKBLUE
        default:
            return UIColor.COLOUR_DARKGREY
        }
    }
    static  func statusDotTintColor(status: Int)->UIColor{
        switch status {
        case EbookingConstants.PENDING_WORKSHOP_STATUS,EbookingConstants.PENDING_CUSTOMER_STATUS:
            return UIColor.COLOUR_YELLOW
        case EbookingConstants.DECLINED_STATUS:
            return UIColor.COLOUR_SHELLRED
        case EbookingConstants.CANCELLED_STATUS:
            return UIColor.COLOUR_DARKGREY
        case EbookingConstants.COMPLETED_STATUS:
            return UIColor.COLOUR_DARKGREEN
        case EbookingConstants.CONFIRMED_STATUS:
            return UIColor.COLOUR_DARKBLUE
        case EbookingConstants.SERVICE_STARTS_STATUS,EbookingConstants.SERVICE_ENDS_STATUS,EbookingConstants.IN_PROGRESS_STATUS:
            return UIColor.COLOUR_DARKBLUE
        default:
            return UIColor.COLOUR_MIDGREY
        }
    }
    static func getViewController(status: Int,storyBoard: UIStoryboard,data:ResponseData) -> UIViewController {
        var id = ""
        var vc = UIViewController()
        switch status {
        case EbookingConstants.PENDING_WORKSHOP_STATUS:
            id = EbookingConstants.VControllerIdentifier.ACCEPT_DECLINE_MODIFY_ID
            let vC = storyBoard.instantiateViewController(withIdentifier: id) as! AcceptDeclineModifyViewController
            vC.bookingData = data
            vc = vC
             break
        case EbookingConstants.PENDING_CUSTOMER_STATUS:
            id = EbookingConstants.VControllerIdentifier.ACCEPT_DECLINE_MODIFY_ID
            let vC = storyBoard.instantiateViewController(withIdentifier: id) as! AcceptDeclineModifyViewController
            vC.bookingData = data
            vc = vC
            break
        case EbookingConstants.CONFIRMED_STATUS:
            id = EbookingConstants.VControllerIdentifier.BOOKING_DETAILS_ID
            let vC = storyBoard.instantiateViewController(withIdentifier: id) as! BookingDetailsViewController
            vC.bookingData = data
            vc = vC
            break
        case EbookingConstants.CANCELLED_STATUS:
            id = EbookingConstants.VControllerIdentifier.BOOKING_DETAILS_ID
            let vC = storyBoard.instantiateViewController(withIdentifier: id) as! BookingDetailsViewController
            vC.bookingData = data
            vc = vC
            break
        case EbookingConstants.DECLINED_STATUS:
            id = EbookingConstants.VControllerIdentifier.BOOKING_DETAILS_ID
            let vC = storyBoard.instantiateViewController(withIdentifier: id) as! BookingDetailsViewController
            vC.bookingData = data
            vc = vC
            break
        case EbookingConstants.SERVICE_STARTS_STATUS:
            id = EbookingConstants.VControllerIdentifier.BOOKING_DETAILS_ID
            let vC = storyBoard.instantiateViewController(withIdentifier: id) as! BookingDetailsViewController
            vC.bookingData = data
            vc = vC
            break
        default:
            id = EbookingConstants.VControllerIdentifier.BOOKING_DETAILS_ID
            let vC = storyBoard.instantiateViewController(withIdentifier: id) as! BookingDetailsViewController
            vC.bookingData = data
            vc = vC
        }
        return vc
        
    }
    
    static func displayToastMessage(_ message : String,view : UIView) {
        let toastView = UILabel()
        toastView.backgroundColor = UIColor.systemYellow//UIColor.black.withAlphaComponent(0.7)
        toastView.textColor = UIColor.COLOUR_SHELLRED
        toastView.textAlignment = .center
        toastView.font = UIFont.preferredFont(forTextStyle: .caption1)
        toastView.layer.cornerRadius = 5
        toastView.layer.masksToBounds = true
        toastView.text = message
        toastView.numberOfLines = 5
        toastView.alpha = 0
        toastView.translatesAutoresizingMaskIntoConstraints = false

        //let window = UIApplication.shared.delegate?.window!
        view.addSubview(toastView)

        let horizontalCenterContraint: NSLayoutConstraint = NSLayoutConstraint(item: toastView, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)

        let widthContraint: NSLayoutConstraint = NSLayoutConstraint(item: toastView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 275)

        let verticalContraint: [NSLayoutConstraint] = NSLayoutConstraint.constraints(withVisualFormat: "V:|-(>=200)-[loginView(==50)]-68-|", options: [.alignAllCenterX, .alignAllCenterY], metrics: nil, views: ["loginView": toastView])

        NSLayoutConstraint.activate([horizontalCenterContraint, widthContraint])
        NSLayoutConstraint.activate(verticalContraint)

        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
            toastView.alpha = 1
        }, completion: nil)

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(2 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC), execute: {
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
                toastView.alpha = 0
            }, completion: { finished in
                toastView.removeFromSuperview()
            })
        })
    }
   static func setUpNavigationBar(navigationController: UINavigationController,
                            navigationItem:UINavigationItem,
                            title:String){
    let titleLabel = UILabel()
    let font = UIFontExtension.FONT_BOLD(ofSize: 18)
    print("Font djghs\(font)")
    navigationController.title = title
    //attributes for the first part of the string
    let firstAttr: [NSAttributedString.Key: Any] =
        [.font: font,.foregroundColor: UIColor.COLOUR_SHELLRED]
    let attrString = NSMutableAttributedString(string: title, attributes: firstAttr)
    titleLabel.attributedText = attrString
    titleLabel.sizeToFit()
    navigationItem.titleView = titleLabel
    navigationItem.leftBarButtonItem?.title = ""
//            navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: font,NSAttributedString.Key.foregroundColor : UIColor.COLOUR_SHELLRED]
//            navigationItem.leftBarButtonItems = CustomBackButton.createWithImage(UIImage(named: "yourImageName")!, color: UIColor.yourColor(), target: weakSelf, action: #selector(self.backToInitial(sender:)))

        
    }
    static func showLoading(view: UIView){
        loading = UIActivityIndicatorView(frame: CGRect(x: 50, y: 50, width: 150, height: 150))
        let container: UIView = UIView()
        container.frame = CGRect(x: 0, y: 0, width: 110, height: 100)
        container.backgroundColor = .clear
        loading.style = UIActivityIndicatorView.Style.whiteLarge
        loading.center = view.center
        loading.color = .red
        container.addSubview(loading)
        view.addSubview(container)
        view.isUserInteractionEnabled = false
        loading.startAnimating()
        
    }
    static func stopLoading(view: UIView){
        if(loading.isAnimating){
            view.isUserInteractionEnabled = true
            loading.stopAnimating()
        }
    }
    static func removeInputErrortextFields(textFields: [UITextField]){
        for input in textFields{
            input.setError()
        }
    }
    static func isAllInputsValid(textFields: [UITextField])-> Bool{
        for input in textFields{
//            if(input.text!.count > 0 && input.text!.count < 3){
//                input.setError(" Invalid legnth of input ", show: true)
//                return false
//            }else
            if(input.text!.isEmpty){
                input.setError(" Invalid empty input ", show: true)
                return false
            }
        }
        return true
    }
    static func isAllBTNInputsValid(btns: [UIButton])-> Bool{
        for input in btns{
            if(input.titleLabel!.text == "" || input.titleLabel!.text == nil){
                input.isError(baseColor: UIColor.COLOUR_SHELLRED.cgColor, numberOfShakes: 3, revert: true)
                return false
            }
        }
        return true
    }
    static func statusObjects()-> [StatusObject]{
        
        var status = [StatusObject]()
        status.append(StatusObject(id: EbookingConstants.PENDING_WORKSHOP_STATUS,statusDescription: EbookingConstants.PENDING_WORKSHOP))
        status.append(StatusObject(id: EbookingConstants.PENDING_CUSTOMER_STATUS,statusDescription: EbookingConstants.PENDING_CUSTOMER))
        status.append(StatusObject(id: EbookingConstants.CONFIRMED_STATUS,statusDescription: EbookingConstants.CONFIRMED))
        status.append(StatusObject(id: EbookingConstants.CANCELLED_STATUS,statusDescription: EbookingConstants.CANCELLED))
        status.append(StatusObject(id: EbookingConstants.DECLINED_STATUS,statusDescription: EbookingConstants.DECLINED))
        status.append(StatusObject(id: EbookingConstants.SERVICE_STARTS_STATUS,statusDescription: EbookingConstants.SERVICE_STARTS))
        status.append(StatusObject(id: EbookingConstants.IN_PROGRESS_STATUS,statusDescription: EbookingConstants.IN_PROGRESS))
        status.append(StatusObject(id: EbookingConstants.SERVICE_ENDS_STATUS,statusDescription: EbookingConstants.SERVICE_ENDS))
        status.append(StatusObject(id: EbookingConstants.NO_SHOW_STATUS,statusDescription: EbookingConstants.NO_SHOW))
        status.append(StatusObject(id: EbookingConstants.COMPLETED_STATUS,statusDescription: EbookingConstants.COMPLETED))
        return status
    }
    static func getStatusDescription(object: [StatusObject]) -> [String]{
        var status = [String]()
        for n in 0...object.count - 1 {
            status.append(object[n].statusDescription!)
        }
        return status
        
    }
    static func getStatusID(object: [StatusObject]) -> [Int]{
        var status = [Int]()
        for n in 0...object.count - 1 {
            status.append(object[n].id!)
        }
        return status
    }
    
    static func addPadding(textField: UITextField){
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 20))
        textField.leftView = paddingView
        textField.leftViewMode = .always
    }
   static func getCurrentDate(date:Date) -> String
    {
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "EEEE, dd MMMM, yyyy"
      return dateFormatter.string(from: date)
    }
    
    
    var toolBar = UIToolbar()
    var datePicker  = UIDatePicker()
    
    func setDateTimePicker(view:UIView){
      //  let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: 320, height: 200)
        datePicker.frame = CGRect(x: 0.0, y: view.frame.height - 200, width: view.frame.width, height: 200)
        datePicker.backgroundColor = UIColor.white
        view.addSubview(datePicker)
        toolBar = UIToolbar(frame: CGRect(x: 0, y: view.frame.height - 200, width: view.frame.width, height: 50))
        toolBar.barStyle = UIBarStyle.black
        toolBar.barTintColor = UIColor.systemYellow
        let font = UIFontExtension.FONT_HEAD(ofSize: 16)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(onDoneButtonClick))
        doneButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.COLOUR_SHELLRED], for: .normal)
        doneButton.setTitleTextAttributes([NSAttributedString.Key.font: font,NSAttributedString.Key.foregroundColor : UIColor.COLOUR_SHELLRED], for: .normal)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(onDoneButtonClick))
        cancelButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.COLOUR_SHELLRED], for: .normal)
        cancelButton.setTitleTextAttributes([NSAttributedString.Key.font: font,NSAttributedString.Key.foregroundColor : UIColor.COLOUR_SHELLRED], for: .normal)
        
        toolBar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        toolBar.sizeToFit()
        view.addSubview(toolBar)
        
       }
    func showDatePicker(view: UIView,txtField: UITextField){
        if(datePicker != nil){
            onDoneButtonClick()
        }
         self.txtField = txtField
        //let yesterday = -1
         let today = 0
        //let tommorow = 1
         datePicker = UIDatePicker.init()
         datePicker.autoresizingMask = .flexibleWidth
         datePicker.minimumDate = Calendar.current.date(byAdding: .day, value: today, to: Date())
         datePicker.maximumDate = Calendar.current.date(byAdding: .day, value: 30, to: Date())
         datePicker.datePickerMode = .date
        // supports IOS 13.6 only
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .compact
        }
        let selector =  #selector(dateChanged(_:))
        datePicker.addTarget(self, action: selector, for: .valueChanged)
        setDateTimePicker(view: view)
        
    }
        
    func showTimePicker(view: UIView,txtField: UITextField) {
        if(datePicker != nil){
            onDoneButtonClick()
        }
         self.txtField = txtField
         datePicker = UIDatePicker.init()
         datePicker.backgroundColor = UIColor.white
         datePicker.autoresizingMask = .flexibleWidth
        
        let calendar = Calendar.current
        var minDateComponent = calendar.dateComponents([.hour], from: Date())
        minDateComponent.hour = 09 // Start time


        let formatter = DateFormatter()
        formatter.dateFormat = "h:mma"
        let minDate = calendar.date(from: minDateComponent)
        print(" min date : \(formatter.string(from: minDate!))")

        var maxDateComponent = calendar.dateComponents([.hour], from: Date())
        maxDateComponent.hour = 17 //EndTime
        let maxDate = calendar.date(from: maxDateComponent)
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        datePicker.datePickerMode = .time
        
        // supports IOS 13.6 only
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
          let selector = #selector(timeChanged(_:))
          datePicker.addTarget(self, action: selector, for: .valueChanged)
        setDateTimePicker(view: view)
    }
    
    @objc func dateChanged(_ sender: UIDatePicker?) {
           let dateFormatter = DateFormatter()
           dateFormatter.dateStyle = .long
           dateFormatter.timeStyle = .none
           dateFormatter.dateFormat = "dd-M-yyyy"
           if let date = sender?.date {
               print("Picked the date \(dateFormatter.string(from: date))")
               let dateSelected = dateFormatter.string(from: date)
            self.txtField?.text = dateSelected //setTitle(dateSelected, for: .normal)
           }
       }
       @objc func timeChanged(_ sender: UIDatePicker?) {
           let dateFormatter = DateFormatter()
           dateFormatter.dateStyle = .none
           dateFormatter.timeStyle = .long
           dateFormatter.dateFormat = "h:mm"
           if let date = sender?.date {
               print("Picked the time \(dateFormatter.string(from: date))")
               let dateSelected = dateFormatter.string(from: date)
                self.txtField?.text = dateSelected //setTitle(dateSelected, for: .normal)
           }
       }

    @objc func onDoneButtonClick() {
           toolBar.removeFromSuperview()
           datePicker.removeFromSuperview()
       }

    
    
    
    
}
