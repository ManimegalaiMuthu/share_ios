//
//  EbookingConstants.swift
//  TestApp
//
//  Created by Admin on 8/21/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import Alamofire

public struct EbookingConstants {
    
    struct StoryBoards {
        static let MAIN_STORYBOARD = "MainStoryBoard"
        static let ADD_NEW_BOOKING_STORYBOARD = "AddNewBooking"
        static let BOOKING_STATUS = "BookingStatus"
        static let REG_OIL_CHANGE = "RegisterOilChange"
    }
    
    struct VControllerIdentifier{
        static let MAIN_NAVIGATION_ID = "MainNavigationController"
        static let ALL_BOOKING_LIST_ID = "AllBookingListViewController"
        static let CALENDAR_VIEW_ID = "CalendarViewController"
        static let ADD_NEW_BOOKING_ID = "AddNewBookingController"
        static let CONFIMED_BOOKING_ID = "BookingConfirmedViewController"
        static let UPDATE_BOOKING_ID = "UpdateBookingInfoViewController"
        static let ACCEPT_DECLINE_MODIFY_ID = "AcceptDeclineModifyViewController"
        static let BOOKING_DETAILS_ID = "BookingDetailsViewController"
        static let REG_OIL_CHANGE_ID = "RegisterOilChangeSearchViewController"
    }
    
    struct PopUpIdentifier{
        static let POP_ACCEPTED_BOOKING = "popup_accepted_booking"
        static let POP_DECLINE_MODIFY_BOOKING = "popup_decline_modify"
        static let POP_DECLINE_REASON = "popup_decline_reason"
        static let POP_COMMON = "popup_common"
        static let POP_CALENDAR = "DatePickerPopupController"
        static let POP_TIME_PICKER = "TimePickerPopUpViewController"
        static let POP_ERROR = "ErrorPopUp"
    }

    
    public static let PENDING_WORKSHOP_STATUS: Int = 0
    public static let PENDING_CUSTOMER_STATUS: Int = 1
    public static let CONFIRMED_STATUS: Int  = 2
    public static let CANCELLED_STATUS: Int = 8
    public static let DECLINED_STATUS: Int = 4
    public static let SERVICE_STARTS_STATUS: Int = 16
    public static let IN_PROGRESS_STATUS: Int = 32
    public static let SERVICE_ENDS_STATUS: Int = 64
    public static let NO_SHOW_STATUS: Int = 128
    public static let COMPLETED_STATUS: Int = 256

    public static let PENDING_WORKSHOP: String = "Pending Confirmation(Workshop)"
    public static let PENDING_CUSTOMER: String = "Pending Confirmation(Customer)"
    public static let CONFIRMED: String = "Confirmed"
    public static let CANCELLED: String  = "Cancelled"
    public static let DECLINED: String  = "Declined"
    public static let SERVICE_STARTS: String  = "Service Starts"
    public static let IN_PROGRESS: String  = "In Progress"
    public static let SERVICE_ENDS: String  = "Service Ends"
    public static let NO_SHOW: String = "No Show"
    public static let COMPLETED: String = "Completed"

    
     static let ADD_NEW_BOOKING = "ADD NEW BOOKING"
     static let MY_BOOKINGS = "MY BOOKINGS"
     static let BOOKING_DETAILS = "BOOKING DETAILS"
     static let EDIT_BOOKING = "EDIT BOOKING"
     static let CONFIRMATION = "CONFIRMATION"
    
    static let NO_RESULT_FOUND = "No results found."
    static let FAILED_TO_GET_BOOKINGS = "Failed to get bookings."
    static let FAILED_TO_ADD_NEW_BOOKING = "Failed to Add new booking."
    static let FAILED_TO_GET_PRODUCTS = "Failed to get products."
    static let FAILED_TO_GET_SERVICES = "Failed to get services."
    static let INVALID_INPUT = "Invalid input(s)."
    
    public static let VEHICLE_ID: String = "Vehicle ID"
    public static let MINOR_MAJOR_SERVICE = "Oil Change Service Type"
    public static let MOBILE_NUMBER = "Mobile Number"
    public static let EMAIL_ADD = "EmailL Adress"
    public static let PREF_OIL_CHANGE_PROD = "Preferred Oil Change Product"
    public static let DATE = "Date"
    public static let TIME = "Time"
    public static let FIRST_NAME = "First Name"
    public static let LAST_NAME = "Last Name"
    
    static let OTHER_REASON = "Others: Please specify reason."
    static let OUT_OF_STOCK = "Product selected is out of stock"
    static let UNAVAILABLE_SLOT = "Unavailable slot"
    static let TO_REGISTER_PRODUCT = "Click here to register product for oil change"
    
    public static let requiredTxtAddBooking = [VEHICLE_ID,MINOR_MAJOR_SERVICE,MOBILE_NUMBER,DATE,TIME,FIRST_NAME,LAST_NAME]
    
    public static let requiredTxtUpdateBooking = [MINOR_MAJOR_SERVICE,DATE,TIME]
       
   struct Api {
        static let base = "https://share.shell.com/services/"
//       static let base = "https://shell-stg.ersg-staging.com/services/"
       static let fetchBookingUrl = base + "API/Booking/FetchBookingList"
       static let getProducts = base + "API/Trade/LoadProduct"
       static let getRuleMatrix = base + "API/Booking/FetchBookingRuleMatrix"
       static let addNewBooking = base + "API/Booking/AddNewBooking"
       static let updateBooking = base + "API/Booking/ModifyBooking"
       static let updateBookingStatus = base + "API/Booking/UpdateBookingStatus"
   
    }
    
    public struct UserDefault {
       public static let ERToken:String = "ERToken"
       public static let LanguageCode:String = "LanguageCode"
       public static let AppVersion:String = "AppVersion"
       public static let AppID:String = "APPID"
       public static let AuthToken:String = "AuthToken"
       public static let CountryCode:String = "CountryCode"
       public static let UserID:String = "UserID"
       public static let PushToken:String = "PushToken"
       public static let TradeID:String = "TradeID"
        public static let ProgramID:String = "ProgramID"
     }
     ///
    public struct HeaderObjects: Decodable {
       var ERToken:String = "ER-TOKEN"
        var LanguageCode:String
        var AppVersion:String
        var APPID:String
        var AuthToken:String = "AUTH-TOKEN"
        var CountryCode:String
        var UserID:String
        var PushToken:String
        var TradeID:String
        var ProgramID:String
     }
    
    struct TradeID{
        public static func getTradeID() -> String{
            let headers = WebserviceHelper.prepPostLoginHeader() as NSDictionary
            return headers.object(forKey: "TradeID") as! String
        }
    }
    public struct Headers {
     public static func apiHeader() -> HTTPHeaders {
        let headers = WebserviceHelper.prepPostLoginHeader() as NSDictionary
          return [
           "ER-TOKEN":headers.object(forKey: "ER-TOKEN") as! String,
            "Content-Type":headers.object(forKey: "Content-Type") as! String,
            "LanguageCode":headers.object(forKey: "LanguageCode") as! String,
            "AppVersion":headers.object(forKey: "AppVersion") as! String,
            "APPID":headers.object(forKey: "APPID") as! String,
            "AUTH-TOKEN":headers.object(forKey: "AUTH-TOKEN") as! String,
            "CountryCode":headers.object(forKey: "CountryCode") as! String,
            "TradeID":headers.object(forKey: "TradeID") as! String,
            "ProgramID":headers.object(forKey: "ProgramID") as! String
          ]
  
     }
    }

}
