//
//  AlamofireExtension.swift
//  ersgProjectBase
//
//  Created by Ben on 26/2/19.
//  Copyright © 2019 Ben. All rights reserved.
//

import Foundation
import Alamofire

extension Alamofire.Session {
  
  @discardableResult
  open func requestWithoutCache(
    _ url: URLConvertible,
    method: HTTPMethod = .get,
    parameters: Parameters? = nil,
    encoding: ParameterEncoding = URLEncoding.default,
    headers: HTTPHeaders? = nil)// also you can add URLRequest.CachePolicy here as parameter
    -> DataRequest
  {
    do {
      var urlRequest = try URLRequest(url: url, method: method, headers: headers)
      urlRequest.cachePolicy = .reloadIgnoringCacheData
      let encodedURLRequest = try encoding.encode(urlRequest, with: parameters)
      return request(encodedURLRequest)
    } catch {
      // TODO: find a better way to handle error
      print(error)
      return request(URLRequest(url: URL(string: "http://example.com/wrong_request")!))
    }
  }
}
