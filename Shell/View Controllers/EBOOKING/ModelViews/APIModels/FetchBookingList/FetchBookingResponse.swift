//
//  FetchBookingResponse.swift
//  ShareEbooking
//
//  Created by Admin on 10/11/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation



 struct APIResponse:Decodable{
    var Data:[ResponseData]
    var Error:String?
    var Msg:String?
    var ResponseCode:Int
    var ResponseMessage:Message
}

struct Message:Decodable {
        var Header:String?
        var Message:String?
        var BtnText1:String?
        var BtnText2:String?
}
struct ResponseData:Decodable {
    var BookedFrom:String
    var BookedTo:String
    var BookingDate:String
    var BookingDetailsID:String
    var BookingStatusID:Int
    var ConsumerName:String
    var EditCount:Int
    var KeyValue:String
    var MobileNumber:String
    var ModifiedBy:String
    var ProductID:Int
    var ProductName:String
    var ProductQuality:Int
    var PromoCode:String
    var ServiceTypeDescription:String
    var Status:Int
    var StatusDisplay:String
    var VehicleNumber:String
}
//"Data": [
//{
//    "BookingDetailsID": "2A8F217299",
//    "MobileNumber": "23555566",
//    "VehicleNumber": "veh3344",
//    "ConsumerName": "API BOOKING 20 Test 20",
//    "StatusDisplay": "Declined",
//    "Status": 4,
//    "KeyValue": "EBOOKING_STATUS",
//    "BookedFrom": "07:00:00",
//    "BookedTo": "09:00:00",
//    "ServiceTypeDescription": "Minor Service: Interval between every 20,000KM",
//    "PromoCode": "",
//    "ProductID": 1020,
//    "ProductName": "",
//    "ProductQuality": 2,
//    "BookingDate": "28/10/2020",
//    "BookingStatusID": 612,
//    "ModifiedBy": "T170000133",
//    "EditCount": 0
