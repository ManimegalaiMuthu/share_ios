//
//  GetProductsResponse.swift
//  ShareEbooking
//
//  Created by Admin on 10/12/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

struct GetProductsAPIResponse:Decodable{
    
   var Data:ProductDetails
   var Error:String?
   var Msg:String?
   var ResponseCode:Int?
   var ResponseMessage:Message
}

struct ProductDetails:Decodable {
    var ProductDetails:[Products]
}
struct Products:Decodable {
   var ProductID:String?
   var ProductName:String?
}

// Data[{
// ProductDetails:
// {ProductID,ProductName}
//}]
class ShellProducts{
    static func getProductName(object: ProductDetails) -> [String]{
        var name = [String]()
        for n in 0...object.ProductDetails.count - 1 {
            name.append(object.ProductDetails[n].ProductName!)
        }
        return name
    }
    static func getProductID(object: ProductDetails) -> [Int]{
        var id = [Int]()
        for n in 0...object.ProductDetails.count - 1 {
            let productID = object.ProductDetails[n].ProductID!
            id.append((productID as NSString).integerValue)
        }
        return id
    }
    
}

