//
//  GetRuleMatrixResponse.swift
//  ShareEbooking
//
//  Created by Admin on 10/12/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

struct GetRuleMatrixAPIResponse:Decodable{
   var Data:[RuleMatrixData]
   var Error:String?
   var Msg:String?
   var ResponseCode:Int?
   var ResponseMessage:Message
}
struct RuleMatrixData:Decodable {
   var BookingMatrixID:Int?
   var BookingServiceTypeID:Int?
   var ServiceTypeDurationHour:Int?
   var ServiceTypeDescription:String?
   var ServiceTypeDescriptionValue:String?
   var BookingServiceModeID:Int?
   var BookingModeDescriptionValue:String
   var ISRequiredPickup:Bool?
   var MaxOpsCount:Int?
   var OpsPanaltyDays:Int?
   var MaxEditableCount:Int?
}

class RuleMatrix{
    static func getServiceTypeDescriptionValue(object: GetRuleMatrixAPIResponse) -> [String]{
        var name = [String]()
        for n in 0...object.Data.count - 1 {
            name.append(object.Data[n].ServiceTypeDescriptionValue!)
        }
        return name
    }
}

