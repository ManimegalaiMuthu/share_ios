//
//  CustomTabController.swift
//  ShareEbooking
//
//  Created by Admin on 8/24/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import UIKit
class CustomTabController : UITabBarController,UITabBarControllerDelegate{
    
    override func viewDidLayoutSubviews() {
        let spaceToTop = 0
        tabBar.frame = CGRect(x: 0, y: CGFloat(spaceToTop), width: tabBar.frame.size.width, height: tabBar.frame.size.height)
     tabBar.unselectedItemTintColor = .black
     tabBar.tintColor = .red
     tabBar.selectionIndicatorImage = UIImage().createSelectionIndicator(color: .red, size: CGSize(width: tabBar.frame.width/CGFloat(tabBar.items!.count), height:  tabBar.frame.height), lineWidth: 2.0)
        super.viewDidLayoutSubviews()
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        var tabFrame:CGRect = self.tabBar.frame
        tabFrame.origin.y = self.view.frame.origin.y
        self.tabBar.frame = tabFrame
    }
//    override func viewDidLayoutSubviews() {
//        let height = navigationController?.navigationBar.frame.maxY
//        tabBar.frame = CGRect(x: 0, y: height ?? 0, width: tabBar.frame.size.width, height: tabBar.frame.size.height)
//        super.viewDidLayoutSubviews()
//      }
//    override func viewDidLayoutSubviews() {
//            tabBar.frame = CGRect(x: 0, y: 0, width: tabBar.frame.size.width, height: tabBar.frame.size.height)
////            super.viewDidLayoutSubviews()
////            delegate = self
//            selectedViewController?.view.frame.origin = CGPoint(x: 0, y: tabBar.frame.size.height)
////        tabBar.frame = CGRect(x: 0, y: 0, width: tabBar.frame.size.width, height: tabBar.frame.size.height)
//        super.viewDidLayoutSubviews()
//
//        }

        func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
              selectedViewController?.view.frame.origin = CGPoint(x: 0, y: tabBar.frame.size.height)
        }
}
