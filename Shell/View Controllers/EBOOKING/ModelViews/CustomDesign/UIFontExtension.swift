//
//  UIFontExtension.swift
//  Shell
//
//  Created by Admin on 10/16/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

import Foundation
import UIKit
import Pods_Shell

@objc class UIFontExtension:NSObject {

    override init() {
        
    }
    @objc static func FONT_HEAD(ofSize size:CGFloat) -> UIFont {
    
    if (UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) == "th"){
        return UIFont(name: "Kittithada Roman 55", size: size) ?? UIFont.systemFont(ofSize: size)
        
    }else if(UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) == "vi"){
        return UIFont(name: "ArialMT", size: size) ?? UIFont.systemFont(ofSize: size)
        
    }else if(UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) == "ar"){
        return UIFont(name: "ShellARB-Book", size: size) ?? UIFont.systemFont(ofSize: size)
    }else if(UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) == "ur"){
        return UIFont(name: "ArialMT", size: size) ?? UIFont.systemFont(ofSize: size)
    }else{
        print(UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) ?? "1")
        return UIFont(name: "FuturaStd-Book", size: size) ?? UIFont.systemFont(ofSize: size)
    }
  }
    
    @objc static func FONT_MEDIUM(ofSize size:CGFloat) -> UIFont {
    if (UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) == "th"){
        return UIFont(name: "Kittithada Medium 65", size: size) ?? UIFont.systemFont(ofSize: size)
    }else if(UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) == "vi"){
        return UIFont(name: "ArialMT", size: size) ?? UIFont.systemFont(ofSize: size)
    }else if(UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) == "ar"){
        return UIFont(name: "ShellARB-Medium", size: size) ?? UIFont.systemFont(ofSize: size)
    }else if(UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) == "ur"){
        return UIFont(name: "ArialMT", size: size) ?? UIFont.systemFont(ofSize: size)
    }else{
        print(UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) ?? "1")
        return UIFont(name: "FuturaStd-Book", size: size) ?? UIFont.systemFont(ofSize: size)
    }
  }
    

    static func FONT_BOLD(ofSize size:CGFloat) -> UIFont {
        if (UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) == "th"){
            return UIFont(name: "Kittithada Bold 75", size: size) ?? UIFont.systemFont(ofSize: size)
            
        }else if(UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) == "vi"){
            return UIFont(name: "Arial-BoldMT", size: size) ?? UIFont.systemFont(ofSize: size)
            
        }else if(UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) == "ar"){
            return UIFont(name: "ShellARB-Bold", size: size) ?? UIFont.systemFont(ofSize: size)
            
        }else if(UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) == "ur"){
            return UIFont(name: "Arial-BoldMT", size: size) ?? UIFont.systemFont(ofSize: size)
        }else{
            return UIFont(name: "FuturaStd-Heavy", size: size) ?? UIFont.systemFont(ofSize: size)
        }
    }
}

class CustomLabel: UILabel {
    override func layoutSubviews() {
        var fontName = ""
        let traits = CTFontGetSymbolicTraits(self.font);
       // self.font = UIFont.init(name: "Kittithada Roman 55", size: self.font.pointSize)
        if (UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) == "th"){
            if(traits == CTFontSymbolicTraits.boldTrait){
                fontName = "Kittithada Bold 75"
            }else{
                fontName = "Kittithada Roman 55"
            }
          self.font = UIFont.init(name: fontName, size: self.font.pointSize)
        }else if(UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) == "vi"){
            if(traits == CTFontSymbolicTraits.boldTrait){
                fontName = "Arial-BoldMT"
            }else{
                fontName = "ArialMT"
            }
            self.font = UIFont.init(name: fontName, size: self.font.pointSize)
        }else if(UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) == "ar"){
            if(traits == CTFontSymbolicTraits.boldTrait){
                fontName = "ShellARB-Bold"
            }else{
                fontName = "ShellARB-Book"
            }
            self.font = UIFont.init(name: fontName, size: self.font.pointSize)
        }else if(UserDefaults.standard.string(forKey: EbookingConstants.UserDefault.LanguageCode) == "ur"){
            if(traits == CTFontSymbolicTraits.boldTrait){
                fontName = "Arial-BoldMT"
            }else{
                fontName = "ArialMT"
            }
            self.font = UIFont.init(name: fontName, size: self.font.pointSize)
        }else{
            if(traits == CTFontSymbolicTraits.boldTrait){
                fontName = "FuturaStd-Heavy"
                self.textColor = UIColor.COLOUR_VERYDARKGREY
            }else{
                fontName = "FuturaStd-Book"
                self.textColor = UIColor.COLOUR_DARKGREY
            }
            print("MYFONT","------ \(fontName) %",self.font.pointSize)
            self.font = UIFont.init(name: fontName, size: self.font.pointSize)
        }
    }
}
