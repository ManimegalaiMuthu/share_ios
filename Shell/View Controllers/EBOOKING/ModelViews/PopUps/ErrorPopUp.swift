//
//  ErrorPopUp.swift
//  Shell
//
//  Created by Admin on 10/22/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

import UIKit

class ErrorPopUp: UIViewController {

    static let instance = ErrorPopUp()
    @IBOutlet weak var PARENT_VIEW: UIView!
    @IBOutlet weak var LBL_MESSAGE: UILabel!
    @IBOutlet weak var BTN_OK: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func showPopUpMessage(message:String){
        Bundle.main.loadNibNamed(EbookingConstants.PopUpIdentifier.POP_ERROR, owner: self, options: nil)
        PARENT_VIEW.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        PARENT_VIEW.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        LBL_MESSAGE.text = message
        BTN_OK.titleLabel?.font = UIFontExtension.FONT_BOLD(ofSize: 18)
        UIApplication.shared.windows.first { $0.isKeyWindow }?.addSubview(PARENT_VIEW)
    }
    
    @IBAction func BtnOk(_ sender: Any) {
        PARENT_VIEW.removeFromSuperview()
       
    }

}
