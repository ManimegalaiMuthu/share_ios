//
//  DatePickerPopupController.swift
//  Shell
//
//  Created by Admin on 10/22/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

import UIKit
import FSCalendar

class DatePickerPopupController: UIViewController, FSCalendarDelegate,FSCalendarDataSource {
    var mydate:String?
    var completion: ((_ selectedDate:String) -> Void)?
    static let instance = DatePickerPopupController()
    
    @IBOutlet weak var CALENDAR_HEADER: FSCalendarHeaderView!
    
    @IBOutlet weak var PARENT_VIEW: UIView!
    
    @IBOutlet weak var LBL_YEAR: UILabel!
    @IBOutlet weak var LBL_SELECTED_DATE: CustomLabel!
    @IBOutlet weak var BTN_CANCEL: UIButton!
    @IBOutlet weak var BTN_OK: UIButton!
    @IBOutlet weak var fsCalendar: FSCalendar!
    
    var viewController: UIViewController!
    var stringDate: String = ""
    var selectedDate = ""
    
      
    override func viewDidLoad() {
        super.viewDidLoad()
        //fsCalendar.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func showCalendarPopUp(completionHandler: @escaping(_ selectedDate:String) -> Void){
        completion = completionHandler
        selectedDate = ""
        Bundle.main.loadNibNamed(EbookingConstants.PopUpIdentifier.POP_CALENDAR, owner: self, options: nil)
        PARENT_VIEW.frame = CGRect(x: 0, y: 0, width:UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        PARENT_VIEW.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        BTN_OK.titleLabel?.font = UIFontExtension.FONT_BOLD(ofSize: 15)
        BTN_OK.tintColor = UIColor.COLOUR_DARKGREY
        BTN_CANCEL.titleLabel?.font = UIFontExtension.FONT_BOLD(ofSize: 15)
        BTN_CANCEL.tintColor = UIColor.COLOUR_DARKGREY
        UIApplication.shared.windows.first { $0.isKeyWindow }?.addSubview(PARENT_VIEW)
        fsCalendar.delegate = self
        
        CALENDAR_HEADER.backgroundColor = UIColor.COLOUR_SHELLRED
        let dateFormater = DateFormatter()
        
        dateFormater.dateFormat = "EE, dd MMMM"
        stringDate = dateFormater.string(from: Date())
        LBL_SELECTED_DATE.text = stringDate
        LBL_SELECTED_DATE.font = UIFontExtension.FONT_HEAD(ofSize: 30)
        dateFormater.dateFormat = "yyyy"
        stringDate = dateFormater.string(from: Date())
        LBL_YEAR.text = stringDate
        LBL_YEAR.font = UIFontExtension.FONT_HEAD(ofSize: 13)
    
        
    }
    

    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let dateFormater = DateFormatter()
        
        dateFormater.dateFormat = "EE, dd MMMM"
        stringDate = dateFormater.string(from: date)
        LBL_SELECTED_DATE.text = stringDate
        LBL_SELECTED_DATE.font = UIFontExtension.FONT_HEAD(ofSize: 30)
        
        dateFormater.dateFormat = "yyyy"
        stringDate = dateFormater.string(from: date)
        LBL_YEAR.text = stringDate
        LBL_YEAR.font = UIFontExtension.FONT_HEAD(ofSize: 13)
        
        dateFormater.dateFormat = "dd-M-yyyy"
        selectedDate = dateFormater.string(from: date)
        
    }
    
    public func minimumDate(for calendar: FSCalendar) -> Date {
            return Date() // crash!
        }
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        return Calendar.current.date(byAdding: .day, value: 30, to: Date())!
    }
    
    @IBAction func OKBTNSelected(_ sender: Any) {
        PARENT_VIEW.removeFromSuperview()
        if(selectedDate != ""){
            completion!(selectedDate)
        }
    }
    @IBAction func CancelBTN(_ sender: Any) {
        PARENT_VIEW.removeFromSuperview()
    }
    
    
}
