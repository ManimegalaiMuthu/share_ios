//
//  TimePickerPopUpViewController.swift
//  Shell
//
//  Created by Admin on 10/22/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

import UIKit

class TimePickerPopUpViewController: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource {
    var completion: ((_ selectedTime:String) -> Void)?
    public static let instance = TimePickerPopUpViewController()
    
    @IBOutlet weak var TIME_PICKER: UIPickerView!
    @IBOutlet weak var PARENT_VIEW: UIView!
    @IBOutlet weak var BTN_CANCEL: UIButton!
    @IBOutlet weak var BTN_OK: UIButton!
    
    var selectedTime:String = ""
    let timeData = [9,10,11,12,13,14,15,16,17]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func showTimePickerPopUp(completionHandler: @escaping(_ selectedTime:String) -> Void){
        completion = completionHandler
        selectedTime = ""
        Bundle.main.loadNibNamed(EbookingConstants.PopUpIdentifier.POP_TIME_PICKER, owner: self, options: nil)
        PARENT_VIEW.frame = CGRect(x: 0, y: 0, width:UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        PARENT_VIEW.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        BTN_OK.titleLabel?.font = UIFontExtension.FONT_BOLD(ofSize: 18)
        BTN_CANCEL.titleLabel?.font = UIFontExtension.FONT_BOLD(ofSize: 18)
        UIApplication.shared.windows.first { $0.isKeyWindow }?.addSubview(PARENT_VIEW)
        self.TIME_PICKER.delegate = self
        self.TIME_PICKER.dataSource = self
    }
    
    @IBAction func BTNOk(_ sender: Any) {
        PARENT_VIEW.removeFromSuperview()
        if(selectedTime != ""){
        completion!(selectedTime)
        }
    }
    
    @IBAction func BTNCancel(_ sender: Any) {
        PARENT_VIEW.removeFromSuperview()
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return timeData.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
           return "\(timeData[row])"
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedTime = "\(timeData[row])"
    }
}
