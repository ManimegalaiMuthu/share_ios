//
//  PopUpAcceptedBooking.swift
//  ShareEbooking
//
//  Created by Admin on 10/13/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import UIKit

class PopUpAcceptedBooking:UIView{
    
    static let instance = PopUpAcceptedBooking()
    @IBOutlet weak var PARENT_VIEW: UIView!
    @IBOutlet weak var LBL_MESSAGE: UILabel!
    @IBOutlet weak var LBL_BOOKING_ID: UILabel!
    @IBOutlet weak var BTN_OK: UIButton!
    @IBOutlet weak var LBL_THANKYOU: UILabel!
    
    
    
    
    var navigationController = UINavigationController()

    
    override init(frame: CGRect)  {
        super.init(frame:frame)
     }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showPopUp(message:String,bookingId:String,nav: UINavigationController){
        navigationController = nav
        Bundle.main.loadNibNamed(EbookingConstants.PopUpIdentifier.POP_ACCEPTED_BOOKING, owner: self, options: nil)
        PARENT_VIEW.frame = CGRect(x: 0, y: 0, width:UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        PARENT_VIEW.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        
        LBL_MESSAGE.text = message
        LBL_BOOKING_ID.text = bookingId
        BTN_OK.titleLabel?.font = UIFontExtension.FONT_BOLD(ofSize: 18)
        BTN_OK.backgroundColor = UIColor.COLOUR_SHELLRED
        LBL_THANKYOU.font = UIFontExtension.FONT_BOLD(ofSize: 16)
        LBL_THANKYOU.textColor = UIColor.COLOUR_SHELLRED
        UIApplication.shared.windows.first { $0.isKeyWindow }?.addSubview(PARENT_VIEW)

    }
    @IBAction func OkButton(_ sender: Any) {
        PARENT_VIEW.removeFromSuperview()
        self.navigationController.popToRootViewController(animated: true)
    }
}
