//
//  EBookingInfo.swift
//  ShareEbooking
//
//  Created by Admin on 8/27/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import  UIKit
class EBookingInfo{
    var bookedFrom:String = ""
    var bookedTo:String = ""
    var bookingDate:String = ""
    var bookingDetailsID:String = ""
    var bookingStatusID:Int = 0
    var consumerName:String = ""
    var editCount:Int = 0
    var keyValue:String = ""
    var mobileNumber:String = ""
    var modifiedBy:String = ""
    var productID:Int = 0
    var productName:String = ""
    var productQuality:Int = 0
    var promoCode:String = ""
    var serviceTypeDescription:String = ""
    var status:Int = 0
    var statusDisplay:String = ""
    var vehicleNumber:String = ""
}


