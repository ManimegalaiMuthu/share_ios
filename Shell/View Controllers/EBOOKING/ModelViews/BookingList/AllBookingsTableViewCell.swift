//
//  AllBookingsTableViewCell.swift
//  ShareEbooking
//
//  Created by Admin on 8/27/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class AllBookingsTableViewCell: UITableViewCell {

    @IBOutlet weak var IV_DOT: UIImageView!
    @IBOutlet var LBL_ID: UILabel!
     @IBOutlet var LBL_STATUS: UILabel!
     @IBOutlet var LBL_VEHICLEID_AND_NAME: UILabel!
     @IBOutlet var LBL_TIME: UILabel!
     @IBOutlet var LBL_PRODUCT_VALUE: UILabel!
     @IBOutlet var BTN_SHOW_INFO: UIButton!
    
    var actionBlock: (() -> Void)? = nil
    func updateTable(bookings: ResponseData){
        LBL_ID.text = bookings.BookingDetailsID
        LBL_STATUS.text = bookings.StatusDisplay
        LBL_STATUS.sizeToFit()
        LBL_STATUS.font = UIFontExtension.FONT_HEAD(ofSize: 11)
        LBL_STATUS.textColor = UIColor.COLOUR_SHELLRED
        LBL_VEHICLEID_AND_NAME.text = bookings.VehicleNumber + " (" + bookings.ConsumerName + ")"
        LBL_TIME.text = bookings.BookingDate + " , " + bookings.BookedFrom + " - " + bookings.BookedTo
        LBL_PRODUCT_VALUE.text = bookings.ProductName
        IV_DOT.tintColor = EbookingUtils.statusDotTintColor(status: bookings.Status)
        
    }
   
    func selectedRow(){
        print("click click  \(LBL_ID.text ?? "empty--")")
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    @IBAction func OnClickShowBookingInfo(_ sender: Any) {
         actionBlock?()
    }
    
    
    
}
