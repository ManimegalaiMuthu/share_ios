//
//  BookingConfirmedViewController.swift
//  Shell
//
//  Created by MUTHUPANDIYAN Manimegalai on 13/10/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

import UIKit

class BookingConfirmedViewController: UIViewController {

    @IBOutlet weak var LBL_MESSAGE: UILabel!
    @IBOutlet weak var LBL_BOOKING_ID: UILabel!
    var message:String?
    var bookingID:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        LBL_MESSAGE.text = message
        LBL_BOOKING_ID.text = bookingID
        EbookingUtils.setUpNavigationBar(navigationController: self.navigationController!, navigationItem: navigationItem, title: EbookingConstants.CONFIRMATION)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: nil)
        
    }
    @objc func backToInitial(sender: AnyObject) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func BackToBookingList(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
