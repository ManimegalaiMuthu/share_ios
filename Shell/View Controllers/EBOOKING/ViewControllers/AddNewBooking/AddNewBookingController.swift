//
//  AddNewBookingController.swift
//  Shell
//
//  Created by MUTHUPANDIYAN Manimegalai on 13/10/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

import UIKit
import iOSDropDown
import SwiftyJSON
import Alamofire


class AddNewBookingController: UIViewController {
    
    @IBOutlet weak var LBL_VEHICLE_ID: UILabel!
    @IBOutlet weak var LBL_SERVICE_TYPE: UILabel!
    @IBOutlet weak var LBL_MOBILE_NUMBER: UILabel!
    @IBOutlet weak var LBL_EMAIL_ADD: UILabel!
    @IBOutlet weak var LBL_PREPARED_OIL: UILabel!
    @IBOutlet weak var LBL_DATE: UILabel!
    @IBOutlet weak var LBL_TIME: UILabel!
    @IBOutlet weak var LBL_FIRST_NAME: UILabel!
    @IBOutlet weak var LBL_LAST_NAME: UILabel!
    
    @IBOutlet weak var TXT_VEHICLE_ID: UITextField!
    @IBOutlet weak var TXT_MOBILE_NUM: UITextField!
    @IBOutlet weak var TXT_FIRST_NAME: UITextField!
    @IBOutlet weak var TXT_LAST_NAME: UITextField!
    @IBOutlet weak var TXT_COUNTRY_CODE: UITextField!
    @IBOutlet weak var TXT_EMAIL: UITextField!
    
    @IBOutlet weak var TXT_PROMO_CODE: UITextField!
    @IBOutlet weak var DATE: UITextField!
    @IBOutlet weak var TIME: UITextField!
    
    
    @IBOutlet weak var OIL_CHANGE_PRODUCT_DROPDOWN: DropDown!
    @IBOutlet weak var MAJOR_MINOR_SERVICE_DROPDOWN: DropDown!
    
    
    @IBOutlet weak var DATE_PARENT_VIEW: UIView!
    @IBOutlet weak var TIME_PARENT_VIEW: UIView!
    @IBOutlet weak var OIL_CHANGE_PARENT_VIEW: UIView!
    @IBOutlet weak var MAJOR_MINOR_PARENT_VIEW: UIView!
    
    @IBOutlet weak var btnConfirmBook: UIButton!
    var toolBar = UIToolbar()
    var datePicker  = UIDatePicker()
    var uiWidgets : CustomUIWidgets! = nil
    let dateTimeTableView = UITableView()
    var customUI: CustomUIWidgets! = nil
    let ebookingUtils = EbookingUtils()
    var currentButton = UIButton()
    var source:[String] = []
    var apiRequest = APIRequest()
    var productsArray = [String]()
    var serviceArray = [String]()
    var textFields = [UITextField]()
    var btnInputs = [UIButton]()
    var ruleMatrixData:GetRuleMatrixAPIResponse?
    
    var selectedProduct:String?
    var selectedProductID:Int?
    var newBookingID:String?
    var bookingMessage:String?
    
    var bookingMatrixID :Int?
    var bookingServiceTypeID :Int?
    var serviceTypeDurationHour :Int?
    var serviceTypeDescription :String?
    var serviceTypeDescriptionValue :String?
    var bookingServiceModeID :Int?
    var bookingModeDescriptionValue :String?
    var iSRequiredPickup :Bool?
    var maxOpsCount :Int?
    var opsPanaltyDays :Int?
    var maxEditableCount :Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        uiWidgets = CustomUIWidgets()
        self.title = EbookingConstants.ADD_NEW_BOOKING
        EbookingUtils.setUpNavigationBar(navigationController: self.navigationController!, navigationItem: navigationItem, title: EbookingConstants.ADD_NEW_BOOKING)
        getProducts()
        TXT_VEHICLE_ID.setBottomBorderOnlyWith(color: UIColor.gray.cgColor)
        btnConfirmBook.titleLabel?.font = UIFontExtension.FONT_BOLD(ofSize: 16)
        btnConfirmBook.backgroundColor = UIColor.COLOUR_SHELLRED
        TXT_VEHICLE_ID.font = UIFontExtension.FONT_HEAD(ofSize: 16)
        MAJOR_MINOR_SERVICE_DROPDOWN.font = UIFontExtension.FONT_HEAD(ofSize: 13)
        TXT_MOBILE_NUM.font = UIFontExtension.FONT_HEAD(ofSize: 16)
        TXT_FIRST_NAME.font = UIFontExtension.FONT_HEAD(ofSize: 16)
        TXT_LAST_NAME.font = UIFontExtension.FONT_HEAD(ofSize: 16)
        OIL_CHANGE_PRODUCT_DROPDOWN.font = UIFontExtension.FONT_HEAD(ofSize: 13)
        DATE.font = UIFontExtension.FONT_HEAD(ofSize: 16)
        TIME.font = UIFontExtension.FONT_HEAD(ofSize: 16)
        let countryCode = UserDefaults.standard.string(forKey: "CountryCode")!
        let session = Session()
        let cName = session.convertCountryCode(toCountryDisplayName: countryCode)
        TXT_COUNTRY_CODE.text = cName! as String
        setUpRequiredInputLabels()
    }
   
    override func viewWillDisappear(_ animated: Bool) {
        EbookingUtils.removeInputErrortextFields(textFields: textFields)
    }

    
    
    // TradeID
    func getProducts(){
        EbookingUtils.showLoading(view: self.view)
        let tradeId = EbookingConstants.TradeID.getTradeID()
        apiRequest.request(url:URL(string: EbookingConstants.Api.getProducts)!,parameters: ["TradeID":tradeId]){responseCode,responseMessage,json,response in
                if(responseCode == .Success){
                    let decoder = JSONDecoder()
                    do{
                        let responseData = try decoder.decode(GetProductsAPIResponse.self, from: response.data!)
                        let data = responseData.Data
                        let objects = data
                        self.productsArray = ShellProducts.getProductName(object: objects)
                        self.OIL_CHANGE_PRODUCT_DROPDOWN.optionArray = self.productsArray
                        self.OIL_CHANGE_PRODUCT_DROPDOWN.optionIds = ShellProducts.getProductID(object: objects)
                        self.getRulematrix()
                     }catch{
                        EbookingUtils.stopLoading(view: self.view)
                        EbookingUtils.displayToastMessage(EbookingConstants.FAILED_TO_GET_PRODUCTS,view: self.view)
                    }
                }else{
                    EbookingUtils.stopLoading(view: self.view)
                    EbookingUtils.displayToastMessage(responseMessage.message,view: self.view)
                }
            }
        }
    
    func getRulematrix(){
        apiRequest = APIRequest()
        apiRequest.request(url:URL(string: EbookingConstants.Api.getRuleMatrix)!,parameters: [:]){responseCode,responseMessage,json,response in
                if(responseCode == .Success){
                    let decoder = JSONDecoder()
                    do{
                        let responseData = try decoder.decode(GetRuleMatrixAPIResponse.self, from: response.data!)
                        self.ruleMatrixData = responseData
                        self.serviceArray = RuleMatrix.getServiceTypeDescriptionValue(object: responseData)
                        self.MAJOR_MINOR_SERVICE_DROPDOWN.optionArray = self.serviceArray
                        self.MAJOR_MINOR_SERVICE_DROPDOWN.text = self.ruleMatrixData?.Data[0].ServiceTypeDescriptionValue
                        self.bookingMatrixID = self.ruleMatrixData?.Data[0].BookingMatrixID
                        self.initServiceTypeDropDown()
                        self.initProductsDropDown()
                     }catch{
                        ErrorPopUp.instance.showPopUpMessage(message: EbookingConstants.FAILED_TO_GET_SERVICES)
                    }
                }else{
                    ErrorPopUp.instance.showPopUpMessage(message: responseMessage.message)
                }
            EbookingUtils.stopLoading(view: self.view)
            }
        
    }
    
    func addNewBooking(param: Parameters = [:]){
        print("ADDBOOKING param ---",param)
        EbookingUtils.showLoading(view: self.view)
        apiRequest.request(url:URL(string: EbookingConstants.Api.addNewBooking)!,parameters: param){responseCode,responseMessage,json,response in
                if(responseCode == .Success){
                    let decoder = JSONDecoder()
                    do{
                        let responseData = try decoder.decode(AddNewBookingAPIResponse.self, from: response.data!)
                        self.newBookingID = responseData.Data
                        self.bookingMessage = responseMessage.message
                        print("ADDBOOKING",responseData.Data! + " -- " + responseMessage.message)
                        self.showBookingConformation() // data
                     }catch{
                        ErrorPopUp.instance.showPopUpMessage(message: EbookingConstants.FAILED_TO_ADD_NEW_BOOKING)
                    }
                    
                }else{
                    // show popup instead
                    ErrorPopUp.instance.showPopUpMessage(message: responseMessage.message)
                }
            EbookingUtils.stopLoading(view: self.view)
            }
        
    }
    
    @IBAction func AddNewBooking(_ sender: Any) {
        
        textFields.append(TXT_VEHICLE_ID)
        //textFields.append(MAJOR_MINOR_SERVICE_DROPDOWN)
        textFields.append(TXT_MOBILE_NUM)
        //textFields.append(TXT_EMAIL)
        textFields.append(TXT_FIRST_NAME)
        textFields.append(TXT_LAST_NAME)
       // textFields.append(OIL_CHANGE_PRODUCT_DROPDOWN)
        textFields.append(DATE)
        textFields.append(TIME)
       
    
        if(EbookingUtils.isAllInputsValid(textFields: textFields)){
            let date = DATE.text! as String?
            let time = TIME.text! as String?
            let bookingMatrixId: String = "\(self.bookingMatrixID ?? 0)"
            let param = ["MobileNumber":TXT_MOBILE_NUM.text ?? "",
                         "VehicleNumber":TXT_VEHICLE_ID.text ?? "",
                         "EmailAddress":TXT_EMAIL.text ?? "",
                         "StreetName":"","BuildingName":"","UnitNumber":"","PostalCode":"",
                         "BookingMatrixID":bookingMatrixId ,
                         "BookingDate":date!,
                         "BookingTime":time!,
                         "ProductID":"\(self.selectedProductID!)",
                         "ProductQuality":"2",
                         "FirstName":TXT_FIRST_NAME.text!,
                         "LastName":TXT_LAST_NAME.text!,
                         "PromoCode":TXT_PROMO_CODE.text ?? ""] as [String : Any]?
            
            self.addNewBooking(param: param!)
        }
    }
   
    @objc func showServiceDate(textField: UITextField) {
        DATE.endEditing(true)
        DATE.resignFirstResponder()
        DATE.tintColor = UIColor.clear
        DATE.inputAccessoryView = UIView()
        DATE.inputView = UIView()
        DATE.allowsEditingTextAttributes = false
        
    }
    

    @IBAction func BTN_DATEPICKER(_ sender: Any) {
        DatePickerPopupController.instance.showCalendarPopUp() {
            selectedDate in
            self.DATE.text = selectedDate
        }
    }
    
    @IBAction func BTN_TIMEPICKER(_ sender: Any) {
        TimePickerPopUpViewController.instance.showTimePickerPopUp() {
            selectedTime in
            if(selectedTime != ""){
                self.TIME.text = "\(selectedTime):00"
            }
         }
    }
    @objc func showServiceTime(textField: UITextField) {
        TIME.endEditing(true)
        TIME.resignFirstResponder()
        TIME.tintColor = UIColor.clear
        
     }
    
    func initServiceTypeDropDown(){
            MAJOR_MINOR_SERVICE_DROPDOWN.didSelect{(selectedText , index ,id) in
                self.bookingMatrixID = self.ruleMatrixData?.Data[index].BookingMatrixID
                self.bookingServiceTypeID = self.ruleMatrixData?.Data[index].BookingServiceTypeID
                self.serviceTypeDurationHour = self.ruleMatrixData?.Data[index].ServiceTypeDurationHour
                self.serviceTypeDescription = self.ruleMatrixData?.Data[index].ServiceTypeDescription
                self.serviceTypeDescriptionValue = self.ruleMatrixData?.Data[index].ServiceTypeDescriptionValue
                self.bookingServiceModeID = self.ruleMatrixData?.Data[index].BookingServiceModeID
                self.bookingModeDescriptionValue = self.ruleMatrixData?.Data[index].BookingModeDescriptionValue
                self.iSRequiredPickup = self.ruleMatrixData?.Data[index].ISRequiredPickup
                self.maxOpsCount = self.ruleMatrixData?.Data[index].MaxOpsCount
                self.opsPanaltyDays = self.ruleMatrixData?.Data[index].OpsPanaltyDays
                self.maxEditableCount = self.ruleMatrixData?.Data[index].MaxEditableCount
               
          }
      }
    func initProductsDropDown(){
        OIL_CHANGE_PRODUCT_DROPDOWN.didSelect{(selectedText , index ,id) in
            self.selectedProduct = selectedText
            self.selectedProductID = id
        }
        
    }
    func showBookingConformation(){
        let storyBoard: UIStoryboard = UIStoryboard(name: EbookingConstants.StoryBoards.ADD_NEW_BOOKING_STORYBOARD, bundle: nil)
        let vController = storyBoard.instantiateViewController(withIdentifier: EbookingConstants.VControllerIdentifier.CONFIMED_BOOKING_ID) as! BookingConfirmedViewController
        vController.message = self.bookingMessage
        vController.bookingID = self.newBookingID
        print("ADDBOOKING",self.newBookingID! + " --xx-- " + self.bookingMessage!)
               self.navigationController?.pushViewController(vController, animated: true)
    }
   
    func setUpRequiredInputLabels(){
        TXT_VEHICLE_ID.delegate = self
        MAJOR_MINOR_SERVICE_DROPDOWN.delegate = self
        TXT_MOBILE_NUM.delegate = self
        TXT_FIRST_NAME.delegate = self
        TXT_LAST_NAME.delegate = self
        OIL_CHANGE_PRODUCT_DROPDOWN.delegate = self
        DATE.delegate = self
        TIME.delegate = self
        let labels:[UILabel] =
            [LBL_VEHICLE_ID,LBL_SERVICE_TYPE,LBL_MOBILE_NUMBER,LBL_DATE,LBL_TIME,LBL_FIRST_NAME,LBL_LAST_NAME]
        uiWidgets.reQuiredInputLabel(value: EbookingConstants.requiredTxtAddBooking, label:labels)
     
    }

}


// Validation textFields
extension AddNewBookingController: UITextFieldDelegate {
    // Remove error message after start editing
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        textField.setError()
        return true
    }
    // Check error
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.setError()
    }
    // Check error
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
}
