//
//  AcceptDeclineModifyViewController.swift
//  ShareEbooking
//
//  Created by Admin on 10/13/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class AcceptDeclineModifyViewController: UIViewController {

    @IBOutlet weak var LBL_STATUS_DISPLAY: UILabel!
    
    @IBOutlet weak var LBL_BOOKING_ID: UILabel!
    @IBOutlet weak var LBL_VEHICLE_CUSTOMER: UILabel!
    @IBOutlet weak var LBL_DATE: UILabel!
    @IBOutlet weak var LBL_TIME: UILabel!
    @IBOutlet weak var LBL_SERVICE_TYPE: UILabel!
    @IBOutlet weak var LBL_PRODUCT: UILabel!
    @IBOutlet weak var LBL_PROMO_CODE: UILabel!
    @IBOutlet weak var BTN_ACCEPT: UIButton!
    @IBOutlet weak var BTN_DECLINEMODIFY: CustomButton!
    
    var apiRequest = APIRequest()
    var bookingData : ResponseData!
    var bookingID:String?
    var mobileNumber = ""
    var editCount: Int?
    var modifiedBy: String?
    var maxEditCount:Int?
    var tradeID:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        EbookingUtils.setUpNavigationBar(navigationController: self.navigationController!, navigationItem: navigationItem, title: EbookingConstants.BOOKING_DETAILS)
        
        LBL_BOOKING_ID.text = bookingData.BookingDetailsID
        LBL_STATUS_DISPLAY.text = bookingData.StatusDisplay
        LBL_STATUS_DISPLAY.font = UIFontExtension.FONT_HEAD(ofSize: 13)
        LBL_STATUS_DISPLAY.textColor = UIColor.COLOUR_SHELLRED
        LBL_VEHICLE_CUSTOMER.text = bookingData.VehicleNumber + " (\(bookingData.ConsumerName))"
        LBL_DATE.text = bookingData.BookingDate
        LBL_TIME.text = bookingData.BookedFrom + " - " + bookingData.BookedTo
        LBL_SERVICE_TYPE.text = bookingData.ServiceTypeDescription
        LBL_PRODUCT.text = bookingData.ProductName
        LBL_PROMO_CODE.text = bookingData.PromoCode
        BTN_ACCEPT.titleLabel?.font = UIFontExtension.FONT_BOLD(ofSize: 18)
        BTN_DECLINEMODIFY.titleLabel?.font = UIFontExtension.FONT_BOLD(ofSize: 18)
        editCount = bookingData.EditCount
        modifiedBy = bookingData.ModifiedBy
        tradeID = EbookingConstants.TradeID.getTradeID()
        let session = Session()
        if(session.isTOAccount()){
            getRulematrix()
        }
        
    }
    

    @IBAction func BTNCall(_ sender: Any) {
        mobileNumber = bookingData.MobileNumber
        let url:NSURL = URL(string: "TEL://\(mobileNumber)")! as NSURL
        UIApplication.shared.open(url as URL, options:[:], completionHandler: nil)
        
    }
    @IBAction func AcceptBooking(_ sender: Any) {
        // BookingDetailsID BookingStatusID Reason Remarks
        let param = ["BookingDetailsID":bookingData.BookingDetailsID,
                     "BookingStatusID":EbookingConstants.CONFIRMED_STATUS,
                      "Reason":"",
                      "Remarks":""] as [String : Any]?
        EbookingUtils.showLoading(view: self.view)
        
        apiRequest.request(url:URL(string: EbookingConstants.Api.updateBookingStatus)!,parameters: param!){responseCode,responseMessage,json,response in
                if(responseCode == .Success){
                        self.bookingID = self.bookingData.BookingDetailsID
                        print("ADDBOOKING json ---",json!)
                        self.showAcceptedBooking(msg:responseMessage.message,id:self.bookingID!) // data
                }else{
                    // show popup instead
                    EbookingUtils.displayToastMessage(responseMessage.message,view: self.view)
                }
            EbookingUtils.stopLoading(view: self.view)
            }
    }
    @IBAction func DeclineBooking(_ sender: Any) {
        PopUpDeclineModify.instance.showPopUp(nav: self.navigationController!,bookingResponse: bookingData)
    }
    
    
    func showAcceptedBooking(msg:String,id:String){
        PopUpAcceptedBooking.instance.showPopUp(message: msg, bookingId: id, nav: self.navigationController!)
        
    }
    
    func getRulematrix(){
        EbookingUtils.showLoading(view: self.view)
        apiRequest = APIRequest()
        apiRequest.request(url:URL(string: EbookingConstants.Api.getRuleMatrix)!,parameters: [:]){responseCode,responseMessage,json,response in
                if(responseCode == .Success){
                    let decoder = JSONDecoder()
                    do{
                        let responseData = try decoder.decode(GetRuleMatrixAPIResponse.self, from: response.data!)
                           self.maxEditCount =  responseData.Data[0].MaxEditableCount!
                        if(self.editCount! > self.maxEditCount! && self.modifiedBy == self.tradeID){
                            self.BTN_DECLINEMODIFY.isHidden = true
                        }
                    }catch{
                        EbookingUtils.displayToastMessage(EbookingConstants.FAILED_TO_GET_SERVICES,view: self.view)
                    }
                }else{
                    EbookingUtils.displayToastMessage(responseMessage.message,view: self.view)
                }
            EbookingUtils.stopLoading(view: self.view)
            }
        
    }

}
