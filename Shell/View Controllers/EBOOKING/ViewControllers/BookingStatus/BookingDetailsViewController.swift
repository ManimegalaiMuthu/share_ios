//
//  BookingDetailsViewController.swift
//  ShareEbooking
//
//  Created by Admin on 10/13/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class BookingDetailsViewController: UIViewController {
    @IBOutlet weak var LBL_BOOKING_ID: UILabel!
    @IBOutlet weak var LBL_STATUS_DISPLAY: UILabel!
    @IBOutlet weak var LBL_VEHICLE_CUSTOMER: UILabel!
    @IBOutlet weak var LBL_DATE: UILabel!
    @IBOutlet weak var LBL_TIME: UILabel!
    @IBOutlet weak var LBL_SERVICE_TYPE: UILabel!
    @IBOutlet weak var LBL_PRODUCT: UILabel!
    @IBOutlet weak var LBL_PROMO_CODE: UILabel!
    var mobileNumber = ""
    @IBOutlet weak var BTN_ACTION_CLICK: UIButton!
    @IBOutlet weak var BTN_COMPLETE_SERVICING: UIButton!
    
    var bookingData : ResponseData!
    var apiRequest = APIRequest()
    override func viewDidLoad() {
        super.viewDidLoad()
        EbookingUtils.setUpNavigationBar(navigationController: self.navigationController!, navigationItem: navigationItem, title: EbookingConstants.BOOKING_DETAILS)
        LBL_BOOKING_ID.text = bookingData.BookingDetailsID
        LBL_STATUS_DISPLAY.text = bookingData.StatusDisplay
        LBL_STATUS_DISPLAY.font = UIFontExtension.FONT_HEAD(ofSize: 13)
        LBL_STATUS_DISPLAY.textColor = EbookingUtils.statusDotTintColor(status: bookingData.Status)
        LBL_VEHICLE_CUSTOMER.text = bookingData.VehicleNumber +
            " (\(bookingData.ConsumerName))"
        LBL_DATE.text = bookingData.BookingDate
        LBL_TIME.text = bookingData.BookedFrom + " - " + bookingData.BookedTo
        LBL_SERVICE_TYPE.text = bookingData.ServiceTypeDescription
        LBL_PRODUCT.text = bookingData.ProductName
        LBL_PROMO_CODE.text = bookingData.PromoCode
        BTN_ACTION_CLICK.isHidden = true
        BTN_COMPLETE_SERVICING.isHidden = true
        BTN_ACTION_CLICK.titleLabel?.font = UIFontExtension.FONT_HEAD(ofSize: 16)
        if(bookingData.Status == EbookingConstants.CONFIRMED_STATUS){
            BTN_ACTION_CLICK.isHidden = false
            let session = Session()
            if(session.isTOAccount()){
                BTN_ACTION_CLICK.isHidden = true
            }
        }
        
        if(bookingData.Status == EbookingConstants.SERVICE_STARTS_STATUS){
           setServiceStartButton()
        }
        
        if(bookingData.Status == EbookingConstants.IN_PROGRESS_STATUS){
            setServiceStartButton()
            BTN_COMPLETE_SERVICING.isHidden = false
        }
    }
    
    func setServiceStartButton(){
        BTN_ACTION_CLICK.isHidden = false
        BTN_ACTION_CLICK.setTitle(EbookingConstants.TO_REGISTER_PRODUCT, for: .normal)
        BTN_ACTION_CLICK.backgroundColor = nil
        BTN_ACTION_CLICK.setTitleColor(.COLOUR_DARKGREY, for: UIControl.State.normal)
        BTN_ACTION_CLICK.titleLabel?.font = UIFontExtension.FONT_HEAD(ofSize: 16)
        if let title = BTN_ACTION_CLICK.titleLabel?.text, title != "" {
            let attributeString = NSMutableAttributedString(string: title)
            attributeString.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range: NSMakeRange(0, title.count))
            BTN_ACTION_CLICK.titleLabel?.attributedText = attributeString
        }
    }
    
    @IBAction func CallBtn(_ sender: Any) {
        mobileNumber = bookingData.MobileNumber
        let url:NSURL = URL(string: "TEL://0\(mobileNumber)")! as NSURL
        UIApplication.shared.open(url as URL, options:[:], completionHandler: nil)
    }
    @IBAction func CompleteServicing(_ sender: Any) {
        self.submitUpdateBookingStatus(status: EbookingConstants.COMPLETED_STATUS)
    }
    
    // CANCELL BOOKING or To register product
    @IBAction func BtnActionClick(_ sender: Any) {
        if(bookingData.Status == EbookingConstants.SERVICE_STARTS_STATUS){
            let storyBoard: UIStoryboard = UIStoryboard(name: EbookingConstants.StoryBoards.REG_OIL_CHANGE, bundle: nil)
            let vController = storyBoard.instantiateViewController(withIdentifier: EbookingConstants.VControllerIdentifier.REG_OIL_CHANGE_ID) as! RegisterOilChangeSearchViewController
            self.navigationController?.pushViewController(vController, animated: true)
        }else{
            self.submitUpdateBookingStatus(status: EbookingConstants.CANCELLED_STATUS)
        }
    }
    
    private func submitUpdateBookingStatus(status:Int){
        let param = ["BookingDetailsID":self.bookingData.BookingDetailsID,
                     "BookingStatusID":"\(status)",
                      "Reason":"",
                      "Remarks":""] as [String : Any]?
        
        EbookingUtils.showLoading(view: self.view)
        apiRequest.request(url:URL(string: EbookingConstants.Api.updateBookingStatus)!,parameters: param!){responseCode,responseMessage,json,response in
                if(responseCode == .Success){
                    PopUpBookingUpdate.instance.showPopUp(message: responseMessage.message, nav: self.navigationController!, shouldDismissToRoot: false)
                    self.LBL_STATUS_DISPLAY.text = LocalizationManager.sharedInstance()?.localizedString(forKey: C_BOOKING_STATUS_COMPLETED)
                    self.LBL_STATUS_DISPLAY.textColor = EbookingUtils.statusDotTintColor(status: EbookingConstants.COMPLETED_STATUS)
                    self.BTN_ACTION_CLICK.isHidden = true;
                    self.BTN_COMPLETE_SERVICING.isHidden = true;
                    
                }else{
                    // show popup instead
                    EbookingUtils.displayToastMessage(responseMessage.message,view: self.view)
                }
            EbookingUtils.stopLoading(view: self.view)
            }
        
    }
    

}
