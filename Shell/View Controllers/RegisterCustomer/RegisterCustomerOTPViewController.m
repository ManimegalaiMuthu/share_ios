//
//  RegisterCustomerOTPViewController.m
//  Shell
//
//  Created by Jeremy Lua on 7/6/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RegisterCustomerOTPViewController.h"

@interface RegisterCustomerOTPViewController () <WebServiceManagerDelegate, TTTAttributedLabelDelegate>
@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@property (strong, nonatomic) IBOutlet UILabel *lblVerifyHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblMobileNum;

@property (strong, nonatomic) IBOutlet UIButton *btnCountryCode;
@property (strong, nonatomic) IBOutlet UITextField *txtMobileNum;
@property (weak, nonatomic) IBOutlet UILabel *lblOTPSent;

@property (strong, nonatomic) IBOutlet UIButton *btnRequestOTP;

@property (weak, nonatomic) IBOutlet UIView *requestOTPView;
@property (strong, nonatomic) IBOutlet UILabel *lblOTP;
@property (strong, nonatomic) IBOutlet UITextField *txtOTP;


@property (strong, nonatomic) IBOutlet UILabel *lblOTPContent;
@property (strong, nonatomic) IBOutlet TTTAttributedLabel *lblOTPResend;

@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;

@property (weak, nonatomic) NSTimer *resendTimer;
@property NSInteger timerCount;

//Progress Outlets
@property (weak, nonatomic) IBOutlet UIImageView *progressOneImageView;
@property (weak, nonatomic) IBOutlet UIView *progressBar_progressView;

@property BOOL isAlreadyMember;

@end

@implementation RegisterCustomerOTPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_INVITECUSTOMER) subtitle: @""];          //lokalise 23 Jan
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    [self setupInterface];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [self.resendTimer invalidate];
    self.resendTimer = nil;
}

- (void)viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_INVITECUSTOMER) screenClass:nil];
}

-(void) setupInterface
{
    self.lblVerifyHeader.font = FONT_H1;
    self.lblVerifyHeader.textColor = COLOUR_DARKGREY;
    self.lblVerifyHeader.text = LOCALIZATION(C_CONSUMER_VERIFYMOBNUM);              //lokalise 23 Jan
    
    self.lblMobileNum.font = FONT_B1;
    self.lblMobileNum.textColor = COLOUR_DARKGREY;
    self.lblMobileNum.text = LOCALIZATION(C_PROFILE_MOBILENUM);                     //lokalised
    
    self.txtMobileNum.font = FONT_B1;
    self.txtMobileNum.textColor = COLOUR_DARKGREY;
    self.txtMobileNum.placeholder = LOCALIZATION(C_PROFILE_MOBILENUM);              //lokalised
    
    self.lblOTPSent.font = FONT_B2;
    self.lblOTPSent.textColor = COLOUR_DARKGREY;
    self.lblOTPSent.text = LOCALIZATION(C_CONSUMER_OTPSENT);                        //lokalise 23 Jan
    
    [self.btnCountryCode.titleLabel setFont: FONT_B1];
    [self.btnCountryCode setTitleColor: COLOUR_DARKGREY forState: UIControlStateNormal];
    [self.btnCountryCode setTitle: [mSession convertCountryCodeToCountryDisplayName: GET_COUNTRY_CODE] forState: UIControlStateNormal];
    
    [self.btnRequestOTP.titleLabel setFont: FONT_BUTTON];
    [self.btnRequestOTP setTitle: LOCALIZATION(C_CONSUMER_REQUESTOTP) forState:UIControlStateNormal];       //lokalise 23 Jan
    [self.btnRequestOTP setBackgroundColor:COLOUR_RED];
    
    self.lblOTP.font = FONT_B1;
    self.lblOTP.textColor = COLOUR_DARKGREY;
    self.lblOTP.text = LOCALIZATION(C_CONSUMER_OTP);                                //lokalise 23 Jan
    
    self.txtOTP.font = FONT_B1;
    self.txtOTP.textColor = COLOUR_DARKGREY;
    self.txtOTP.placeholder = LOCALIZATION(C_OTP_PLACEHOLDER);                      //lokalise 23 Jan
    
    self.lblOTPContent.font = FONT_B2;
    self.lblOTPContent.textColor = COLOUR_DARKGREY;
    
    self.lblOTPContent.text = LOCALIZATION(C_CONSUMER_AUTHMSG);                     //lokalise 23 Jan
    
    self.lblOTPResend.font = FONT_B2;
    self.lblOTPResend.textColor = COLOUR_DARKGREY;
    if (kIsRightToLeft) {
        [self.lblOTPResend setTextAlignment:NSTextAlignmentRight];
    } else {
        [self.lblOTPResend setTextAlignment:NSTextAlignmentLeft];
    }
    [Helper setHyperlinkLabel: self.lblOTPResend hyperlinkText:self.lblOTPResend.text hyperlinkFont:FONT_B1 bodyText:self.lblOTPResend.text bodyFont:FONT_B1 urlString:@"OTP" bodyColor:COLOUR_VERYDARKGREY];
    
    [self.btnSubmit.titleLabel setFont: FONT_BUTTON];
    [self.btnSubmit setTitle: LOCALIZATION(C_REGISTER_SUBMITOTP) forState:UIControlStateNormal];        //lokalise 23 Jan
    [self.btnSubmit setBackgroundColor:COLOUR_RED];
    
    
    self.timerCount = [[[mSession lookupTable] objectForKey: @"OTPTimer"] intValue];
    
    [self setResendOtpEnable:NO timer:self.timerCount];
//    self.resendTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(startTimer) userInfo:nil repeats:YES];
    //must be after setresendotp method
    self.lblOTPSent.hidden = YES;
    
    if(![[self.promoDict objectForKey:@"MobileNumber"]isKindOfClass:[NSNull class]] && [GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND]) {
        self.txtMobileNum.text = [self.promoDict objectForKey:@"MobileNumber"];
    }
    
    if(GET_ISADVANCE) {
        [self.progressOneImageView setImage: [UIImage imageNamed:@"progressnum-blue-1"]];
    } else {
        [self.progressOneImageView setImage: [UIImage imageNamed:@"progressnum-red-1"]];
    }
    self.progressBar_progressView.backgroundColor = COLOUR_RED;
    
    if (kIsRightToLeft) {
        [self.txtMobileNum setTextAlignment:NSTextAlignmentRight];
        [self.txtOTP setTextAlignment:NSTextAlignmentRight];
        [self.btnCountryCode setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.txtMobileNum setTextAlignment:NSTextAlignmentLeft];
        [self.txtOTP setTextAlignment:NSTextAlignmentLeft];
        [self.btnCountryCode setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
    self.isAlreadyMember = NO;
}

- (IBAction)dialingCodePressed:(id)sender {
//    [self popUpListViewWithTitle: LOCALIZATION(C_DROPDOWN_COUNTRY) listArray: [mSession getCountryDialingCodes]];
}


//-(void)dropDownSelection:(NSArray *)selection
//{
//    if (selection.count > 0) {
//        NSString *countryName = [mSession convertDialCodeToCountryName: selection.firstObject];
//        [self.btnCountryCode setTitle: countryName forState:UIControlStateNormal];
//    }
//}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url absoluteString] isEqualToString: @"OTP"])
    {
        [self requestOTPPressed: nil];
    }
    return;
}

- (IBAction)requestOTPPressed:(id)sender {
//    "MobileNumber":"123454321", /*string*/
//    "CountryDial":"458" /*int*/
    
    NSString *countryCodeString = [mSession convertCountryDisplayNameToCountryCode: self.btnCountryCode.titleLabel.text];
    
    NSDictionary *submitParams = @{@"MobileNumber": self.txtMobileNum.text,
                                   @"CountryDial":  @(countryCodeString.intValue),
                                   @"PromoCode": [self.promoDict objectForKey:@"PromoCode"],
                                   @"isConfirmed" : @(0),
                                   };
    [[WebServiceManager sharedInstance] submitMemberRegistrationGetOTP: submitParams view: self];
}

- (IBAction)submitPressed:(id)sender {
    
    NSString *countryCodeString = [mSession convertCountryDisplayNameToCountryCode: self.btnCountryCode.titleLabel.text];
    
    NSDictionary *submitParams = @{@"MobileNumber": self.txtMobileNum.text,
                                   @"CountryDial":  @(countryCodeString.intValue),
                                   @"OTP": self.txtOTP.text,
                                   @"PromoCode": [self.promoDict objectForKey:@"PromoCode"],
                                   };
    
//    if (IS_STAGING_VERSION)
//    {
//        NSDictionary *otpDict = @{@"MobileNumber": self.txtMobileNum.text,
//                                  @"CountryDial":  @(countryCodeString.intValue),
//                                  @"OTP": self.txtOTP.text,
//                                  @"PromoCode": [self.promoDict objectForKey:@"PromoCode"],
//                                  @"VehicleNumber": [self.promoDict objectForKey: @"VehicleNumber"],
//                                  @"VehicleStateID": [self.promoDict objectForKey: @"VehicleStateID"],
//                                  };
//
//        [mSession pushRegisterConsumerRegisterFormViewWithOTPDict: otpDict vc:self];
//    }
//    else
        [[WebServiceManager sharedInstance] submitMemberRegistrationStepOne: submitParams view: self];
}


-(void) setResendOtpEnable :(BOOL) isEnable timer:(NSInteger)timerCounter
{
    self.lblOTPSent.hidden = NO;
    NSString *resendString = LOCALIZATION(C_REGISTER_RESENDOTP);                    //lokalise 23 Jan
    if(isEnable){
        [Helper setHyperlinkLabel: self.lblOTPResend hyperlinkText:resendString hyperlinkFont:FONT_B2 bodyText:resendString bodyFont:FONT_B2 urlString:@"OTP" bodyColor: COLOUR_VERYDARKGREY];
        self.lblOTPResend.delegate = self;
    }else{
        NSString *stringCountDown = [NSString stringWithFormat:@"%@ (%d)",resendString,timerCounter];
        [Helper setHyperlinkLabel: self.lblOTPResend hyperlinkText:stringCountDown hyperlinkFont:FONT_B2 bodyText:stringCountDown bodyFont:FONT_B2 urlString:@"OTP" bodyColor:COLOUR_LIGHTGREY];
        self.lblOTPResend.delegate = nil;
    }
}

-(void)startTimer{
    self.timerCount -= 1;
    [self setResendOtpEnable:NO timer:self.timerCount];
    
    if(self.timerCount == 0){
        [self setResendOtpEnable:YES timer:self.timerCount];
        //        self.timerCount = MAXTIMEBEFORERESEND;
        self.timerCount = [[[mSession lookupTable] objectForKey: @"OTPTimer"] intValue];
        [self.resendTimer invalidate];
        self.resendTimer = nil;
    }
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_SUBMITMEMBER_REQUESTOTP:
            {
                self.requestOTPView.hidden = NO;
                if ([response getResponseMessage])
                {
                    [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                    }];
                }
                self.resendTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(startTimer) userInfo:nil repeats:YES];
                [self setResendOtpEnable:NO timer:self.timerCount];
            }
            break;
        case kWEBSERVICE_SUBMITMEMBER_SUBMITOTP:
        {
            
            NSString *countryCodeString = [mSession convertCountryDisplayNameToCountryCode: self.btnCountryCode.titleLabel.text];
            
            NSDictionary *otpDict = @{@"MobileNumber": self.txtMobileNum.text,
                                      @"CountryDial":  @(countryCodeString.intValue),
                                      @"OTP": self.txtOTP.text,
                                      @"PromoCode": [self.promoDict objectForKey:@"PromoCode"],
                                      @"VehicleNumber": [self.promoDict objectForKey: @"VehicleNumber"],
                                      @"VehicleStateID": [self.promoDict objectForKey: @"VehicleStateID"],
                                      @"isAlreadyMember" : @(self.isAlreadyMember),
                                    };
            
            [mSession pushRegisterConsumerRegisterFormViewWithOTPDict: otpDict vc:self];
        }
            break;
        default:
            break;
    }
}

- (void)processFailed:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
       case kWEBSERVICE_SUBMITMEMBER_REQUESTOTP:
            {
                if([response getResponseCode] == 8113) {
                    [self popUpYesNoWithTitle: @"" content: LOCALIZATION(C_POPUP_INVITECUSTOMER_ALREADYMEMBER) yesBtnTitle: LOCALIZATION(C_FORM_YES) noBtnTitle: LOCALIZATION(C_FORM_NO) onYesPressed:^(BOOL finished) {
                         self.isAlreadyMember = YES;
                         NSString *countryCodeString = [mSession convertCountryDisplayNameToCountryCode: self.btnCountryCode.titleLabel.text];
                         
                         NSDictionary *submitParams = @{@"MobileNumber": self.txtMobileNum.text,
                                                        @"CountryDial":  @(countryCodeString.intValue),
                                                        @"PromoCode": [self.promoDict objectForKey:@"PromoCode"],
                                                        @"isConfirmed" : @(1),
                                                        };
                         [[WebServiceManager sharedInstance] submitMemberRegistrationGetOTP: submitParams view: self];
                    } onNoPressed:^(BOOL finished) {
                        self.txtMobileNum.text = @"";
                    }];
                } else if([response getResponseCode] == 8115) {
                    [self popUpYesNoWithTitle: @"" content: LOCALIZATION(C_POPUP_INVITECUSTOMER_ALREADYMEMBER) yesBtnTitle: LOCALIZATION(C_FORM_YES) noBtnTitle: LOCALIZATION(C_FORM_NO)
                                 onYesPressed:^(BOOL finished) {
                                    [self popupInfoVCWithTitle:@"" content:LOCALIZATION(C_POPUP_INVITECUSTOMER_MAXVEHICLE) btnTitle:LOCALIZATION(C_GLOBAL_OK) onYesPressed:^(BOOL finished){
                                                                                [self popSelf];
                                    }];
                        } onNoPressed:^(BOOL finished) {
                            self.txtMobileNum.text = @"";
                    }];
                    
                }
                
            }
            break;
        default:
            break;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
