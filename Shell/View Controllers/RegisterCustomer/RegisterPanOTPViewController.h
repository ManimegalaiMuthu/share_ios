//
//  RegisterPanOTPViewController.h
//  Shell
//
//  Created by MUTHUPANDIYAN Manimegalai on 13/10/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface RegisterPanOTPViewController : BaseVC

@property NSDictionary *promoDict;
@property NSString *strMobileNo;
@property NSDictionary *bankDetailDict;

@end

NS_ASSUME_NONNULL_END
