//
//  RegisterCustomerViewController.h
//  Shell
//
//  Created by Jeremy Lua on 4/7/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface RegisterCustomerViewController : BaseVC

@property NSDictionary *registerCustomerDict;
@end
