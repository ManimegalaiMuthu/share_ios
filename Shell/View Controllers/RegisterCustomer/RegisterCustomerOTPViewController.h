//
//  RegisterCustomerOTPViewController.h
//  Shell
//
//  Created by Jeremy Lua on 7/6/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface RegisterCustomerOTPViewController : BaseVC
@property NSDictionary *promoDict;
@end
