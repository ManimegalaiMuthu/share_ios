//
//  RegisterCustomerStepOneViewController.h
//  Shell
//
//  Created by Jeremy Lua on 7/6/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface RegisterCustomerFormViewController : BaseVC
@property NSDictionary *otpDict; //contain mobile number, otp, country code
@end
