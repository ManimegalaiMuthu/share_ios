//
//  RegisterCustomerCompleteViewController.m
//  Shell
//
//  Created by Jeremy Lua on 7/6/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RegisterCustomerCompleteViewController.h"
#import "PromoCodeViewController.h"

@interface RegisterCustomerCompleteViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UIButton *btnContinue;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@end

@implementation RegisterCustomerCompleteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.hidesBackButton = YES;
    
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_INVITECUSTOMER) subtitle: @""];          //lokalise 23 Jan
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    
    
    self.lblHeader.font = FONT_H1;
    self.lblHeader.text = LOCALIZATION(C_CONSUMER_SUCCESSFUL);                                              //lokalise 23 Jan
    
    self.lblContent.font = FONT_B1;
//    self.lblContent.text = [NSString stringWithFormat: @"%@\n\n%@\n\n%@",  LOCALIZATION(C_CONSUMER_SUCCESSFULREGISTRATION),  LOCALIZATION(C_CONSUMER_SUCCESSFULREGISTRATIONTWO),  LOCALIZATION(C_CONSUMER_SUCCESSFULREGISTRATIONTHREE)];
    self.lblContent.text = [NSString stringWithFormat: @"%@",  LOCALIZATION(C_CONSUMER_SUCCESSFULREGISTRATION)];    //lokalise 23 Jan
    
    [self.btnContinue.titleLabel setFont: FONT_BUTTON];
//    [self.btnContinue setTitle:LOCALIZATION(C_CONSUMER_CONTINUE) forState:UIControlStateNormal];
    [self.btnContinue setTitle:LOCALIZATION(C_GLOBAL_OK) forState:UIControlStateNormal];                        //lokalise 23 Jan
    [self.btnContinue setBackgroundColor:COLOUR_RED];
    
    if (kIsRightToLeft) {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_INVITECUSTOMER),LOCALIZATION_EN(C_CONSUMER_SUCCESSFUL)] screenClass:nil];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    //hardcoded
    UIViewController *promoMasterVC = [[self.navigationController childViewControllers] firstObject];
    UINavigationController *navController = [[promoMasterVC childViewControllers] firstObject];
    
    if (navController)
    {
        //from promo code
        PromoCodeViewController *promoCodeVC = [[navController childViewControllers] firstObject];
        
        self.delegate = promoCodeVC;
    }
    else
    {
        //assign to search vehicle vc
        self.delegate = promoMasterVC;
    }
    
}

- (IBAction)continuePressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(completeRegister)])
        [self.delegate completeRegister];
    
    [self popToIndex: 0]; //go back to first controller, promocode
    
}


- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
