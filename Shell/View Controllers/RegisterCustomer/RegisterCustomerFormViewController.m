//
//  RegisterCustomerStepOneViewController.m
//  Shell
//
//  Created by Jeremy Lua on 7/6/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RegisterCustomerFormViewController.h"
#import "CASMonthYearPicker.h"

#define CELL_HEIGHT 72

//enum kTradeRegForm_TYPE
//{
//    kTradeRegForm_FIRSTNAME,
//    kTradeRegForm_LASTNAME,
//    kTradeRegForm_VEHICLENUMBER,
//    kTradeRegForm_YEAROFMANUFACTURE,
//    kTradeRegForm_VEHICLEUSAGE,
//};

//"MobileNumber": "46457474322",/*string*/
//"CountryCode": "458",/*int*/
//"OTP": "66617666607",/*string*/
//"FirstName": "Test",/*string*/
//"LastName": "Testing",/*string*/
//"VehicleNumber": "testtttsa",/*string*/
//"YearOfManufacture": "2000",/*string*/
//"VehicleUsage": "1",/*int*/
//"IsRegistered": "1"/*int*/

#define kMobileNumber             @"MobileNumber"
#define kCountryCode             @"CountryCode"
#define kOTP                     @"OTP"
#define kFirstName             @"FirstName"
#define kLastName             @"LastName"
#define kVehicleNumber            @"VehicleNumber"
#define kYearOfManufacture             @"YearOfManufacture"
#define kVehicleUsage             @"VehicleUsage"
#define kIsRegistered             @"IsRegistered"
#define kVehicleStateID             @"VehicleStateID" //province for thai
#define kFullName           @"FullName"
#define kBirthdayMonth      @"BirthdayMonth"
#define kHasGoGoVan         @"HasGoGoVan"
#define kGGVDriverNumb      @"GGVDriverNumb"

//VN Rimula Phase 2
#define kVehicleType      @"VehicleType"

#define kIsSubscribeContent             @"IsSubscribeContent"

#define kDOB                @"DOB"
#define kDOBMonth           @"DOBMonth"
#define kDOBDay             @"DOBDay"
enum kPopupSelection_Type
{
    kPopupSelection_VehicleUsage,
    kPopupSelection_YearOfManufacture,
    kPopupSelection_BirthdayMonth,
    kPopupSelection_State,
    kPopupSelection_VehicleType,
};

@interface RegisterCustomerFormViewController () <WebServiceManagerDelegate, UITableViewDelegate, MonthYearPickerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblProfileHeader;

@property (weak, nonatomic) IBOutlet UILabel *lblRequiredFields;

@property NSMutableArray *registrationFormArray; //ui only
@property NSMutableDictionary *regSubmissionDict;  //key as json key, value = cell. retrieve value by using getJson method

@property (weak, nonatomic) IBOutlet UITableView *regFormTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight; //to adjust according number of cells

@property (weak, nonatomic) IBOutlet UIButton *btnRegister;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckbox;
@property (weak, nonatomic) IBOutlet UILabel *lblCheckbox;

@property (weak, nonatomic) IBOutlet UIButton *btnMarketingCheckbox;
@property (weak, nonatomic) IBOutlet UILabel *lblMarketingCheckbox;
@property (weak, nonatomic) IBOutlet UIView *registeredVehicleCheckboxView;

@property (weak, nonatomic) IBOutlet UIView *gogoVanView;
@property (weak, nonatomic) IBOutlet UIButton *btnGogoVanSelection;
@property (weak, nonatomic) IBOutlet UILabel *lblGogoVanRegistered;
@property (weak, nonatomic) IBOutlet UILabel *lblGogoVanDriverNumber;
@property (weak, nonatomic) IBOutlet UITextField *textFieldGogoVanNumber;

//ProgressNUmber Images
@property (weak, nonatomic) IBOutlet UIImageView *progressOneImageView;
@property (weak, nonatomic) IBOutlet UIImageView *progressTwoImageView;
@property (weak, nonatomic) IBOutlet UIView *progressBar_Progress;


@property NSInteger popupTag;
@property NSArray *stateTable;
@property BOOL isAlreadyMember;


@property Member *member;
@property CASMonthYearPicker *dateView;
@end

@implementation RegisterCustomerFormViewController
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_INVITECUSTOMER) subtitle: @""];              //lokalise 23 Jan
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    
    self.member = [[Member alloc] init];
    
    self.regSubmissionDict = [[NSMutableDictionary alloc] init];
    [self setupRegistrationForm];
    
    self.lblProfileHeader.font = FONT_H1;
    self.lblProfileHeader.text = LOCALIZATION(C_CONSUMER_ENTERDETAILS);                                         //lokalise 23 Jan
    
    self.lblCheckbox.font = FONT_B1;
    self.lblMarketingCheckbox.font = FONT_B1;
    self.lblMarketingCheckbox.text = LOCALIZATION(C_CONSUMER_MARKETING);                                        //lokalise 23 Jan
    
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDONESIA])
    {
        self.lblCheckbox.text = LOCALIZATION(C_CONSUMER_INDONESIAVEHICLE);
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_PHILLIPPINES])                                      //lokalise 23 Jan
    {
        self.lblCheckbox.text = LOCALIZATION(C_CONSUMER_PHILLIPPINESVEHICLE);
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
    {
        self.lblCheckbox.text = LOCALIZATION(C_CONSUMER_THAILANDVEHICLE);                                       //lokalise 23 Jan
    }
    else if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_MALAYSIA]) {
        self.lblCheckbox.text = LOCALIZATION(C_CONSUMER_MALAYSIAVEHICLE);
    }
    else if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_INDIA]) {
        self.registeredVehicleCheckboxView.hidden = YES;
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.registeredVehicleCheckboxView
            attribute:NSLayoutAttributeHeight
            relatedBy:NSLayoutRelationEqual
            toItem:nil attribute: NSLayoutAttributeNotAnAttribute multiplier:1 constant:0]];
    }
    
    
    
    [self.btnRegister.titleLabel setFont: FONT_BUTTON];
    [self.btnRegister setTitle:LOCALIZATION(C_CONSUMER_SUBMITREGISTRATION) forState:UIControlStateNormal];          //lokalise 23 Jan
    [self.btnRegister setBackgroundColor:COLOUR_RED];
    
    
    NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @"*" attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B3, }];
    
    NSMutableAttributedString *requiredFieldsAttrString = [[NSMutableAttributedString alloc] initWithString: LOCALIZATION(C_PROFILE_REQUIREDFIELDS) attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B3, }];           //lokalised
    
    [asteriskAttrString appendAttributedString:  requiredFieldsAttrString];
    
    //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
    self.lblRequiredFields.attributedText = asteriskAttrString;

    
    if(GET_ISADVANCE) {
        [self.progressOneImageView setImage: [UIImage imageNamed:@"progressnum-blue-tick"]];
        [self.progressTwoImageView setImage: [UIImage imageNamed:@"progressnum-blue-2"]];
    } else {
        [self.progressOneImageView setImage: [UIImage imageNamed:@"progressnum-red-tick"]];
        [self.progressTwoImageView setImage: [UIImage imageNamed:@"progressnum-red-2"]];
    }
    self.progressBar_Progress.backgroundColor = COLOUR_RED;
    
    if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_HONGKONG]) {
        [self.gogoVanView setHidden:NO];
        [self.btnGogoVanSelection setSelected:false];

        self.lblGogoVanRegistered.font = FONT_B1;
        self.lblGogoVanRegistered.textColor = COLOUR_DARKGREY;
        self.lblGogoVanRegistered.text = LOCALIZATION(C_GOGOVANREGISTERED);

        self.lblGogoVanDriverNumber.font = FONT_B1;
        self.lblGogoVanDriverNumber.textColor = COLOUR_DARKGREY;
        self.lblGogoVanDriverNumber.text = LOCALIZATION(C_GOGOVANDRIVERID);

        self.textFieldGogoVanNumber.font = FONT_B1;
        self.textFieldGogoVanNumber.textColor = COLOUR_DARKGREY;
        self.textFieldGogoVanNumber.placeholder = @"i.e., #1234";

    } else {
        [self.gogoVanView setHidden:YES];
        [NSLayoutConstraint constraintWithItem:self.gogoVanView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
    }
    
    if (kIsRightToLeft) {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_INVITECUSTOMER) screenClass:nil];
}

-(void) setupRegistrationForm
{
    dateView = [[CASMonthYearPicker alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_BIRTHDAY) previouslySelected:nil withYearHidden:YES];      //lokalise 23 Jan
    
    dateView.delegate = self;
    
    self.isAlreadyMember = [[self.otpDict objectForKey:@"isAlreadyMember"] boolValue];
    
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormTextfieldCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormTextfieldCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormDropdownCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormDropdownCell"];
    
    self.registrationFormArray = [[NSMutableArray alloc] init];
    

    if(!self.isAlreadyMember) {
        if ([[[mSession lookupTable] objectForKey: @"ConsumerSignUpOption"] intValue] == 2) //2 = in/th, 1 = my/ph
        {
            [self.registrationFormArray addObject:@{@"Title": LOCALIZATION(C_CONSUMER_FULLNAME),                //lokalised
                                                    @"Type": @(kREGISTRATION_TEXTFIELD),
                                                    @"Key": kFullName,
                                                    @"Value": @"",
                                                    }]; //first name
        }
        else
        {
            [self.registrationFormArray addObject:@{@"Title": LOCALIZATION(C_PROFILE_FIRSTNAME),                    //lokalised
                                                    @"Type": @(kREGISTRATION_TEXTFIELD),
                                                    @"Key": kFirstName,
                                                    @"Value": @"",
                                                    }]; //first name
            [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_LASTNAME),                    //lokalised
                                                     @"Type": @(kREGISTRATION_TEXTFIELD),
                                                     @"Key": kLastName,
                                                     @"Value": @"",
                                                     }]; //last name
        }
        
        if ([[[mSession lookupTable] objectForKey: @"ConsumerSignUpOption"] intValue] != 1) //2 = in/th, 1 = my/ph,3 = HK
        {
            self.member.dobDay = @"01";
            self.member.dobMonth = @"01";
            [self.registrationFormArray addObject:@{@"Title": LOCALIZATION(C_CONSUMER_BIRTHDAYMONTH),               //lokalise on 23 Jan
                                                    @"Type": @(kREGISTRATION_BIRTHDATE),
                                                    @"Placeholder": C_CONSUMER_BIRTHDAYMONTH,                       //lokalise 23 Jan
                                                    @"Key": kBirthdayMonth,
                                                    @"Value": LOCALIZATION(C_MONTH_JAN),                            //lokalise 23 Jan
                                                    @"Optional": @(YES),
                                                    }]; //Birthday month
        }
    }
    
    if ([[mSession profileInfo] hasVehicleType])
    {
        [self.registrationFormArray addObject:@{@"Title": LOCALIZATION(C_FORM_VEHICLETYPE),
                                                @"Type": @(kREGISTRATION_DROPDOWN),
                                                @"Key": kVehicleType,
                                                @"Value": LOCALIZATION(C_DROPDOWN_VEHICLETYPE_HEADER),
                                                }]; //button for vehicle type (VN Rimula Phase 2)
    }

    
    [self.registrationFormArray addObject:@{@"Title": LOCALIZATION(C_CONSUMER_VEHICLEREGPLATE),                 //lokalise 23 Jan
                                            @"Type": @(kREGISTRATION_TEXTFIELD),
                                            @"Placeholder": LOCALIZATION(C_INVITECUSTOMER_VEHICLEID_PLACEHOLDER),
                                            @"Key": kVehicleNumber,
                                            @"Value": [self.otpDict objectForKey: @"VehicleNumber"],
                                            }]; //Vehicle registration number
    
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
    {
        self.stateTable = GET_LOADSTATE;
        
        NSString *stateID = [self.otpDict objectForKey: @"VehicleStateID"];
        NSString *stateString;
        if (stateID.length > 0)
            stateString = [mSession convertToStateName: stateID stateDetailedListArray: self.stateTable];
        else
            stateString =  LOCALIZATION(C_PROFILE_STATE);
        [self.registrationFormArray addObject:@{@"Title": LOCALIZATION(C_PROFILE_STATE),                        //lokalised
                                                @"Type": @(kREGISTRATION_DROPDOWN),
                                                @"Key": kVehicleStateID,
                                                @"Value": stateString,
                                                }]; //button for Province for Thai
    }
    
    if([[[mSession lookupTable] objectForKey: @"ConsumerSignUpOption"] intValue] != 3) {
        NSString *manufactureString;
        BOOL isManufactureOptional;
        if ([[[mSession lookupTable] objectForKey: @"ConsumerSignUpOption"] intValue] == 1) //2 = in/th, 1 = my/ph , 3 = HK
        {
            manufactureString = LOCALIZATION(C_CONSUMER_YEAROFMANUFACTURE);                                         //lokalise 23 Jan
            isManufactureOptional = NO;
        }
        else
        {
            manufactureString = LOCALIZATION(C_CONSUMER_YEAROFMANUFACTURE_OPT);                                     //lokalise 23 Jan
            isManufactureOptional = YES;
        }
        
        [self.registrationFormArray addObject:@{@"Title": LOCALIZATION(C_CONSUMER_YEAROFMANUFACTURE),               //lokalise 23 Jan
                                                @"Type": @(kREGISTRATION_DROPDOWN),
                                                @"Key": kYearOfManufacture,
                                                @"Value": manufactureString,
                                                @"Optional": @(isManufactureOptional),
                                                }]; //button for year of manufacture
        
        
        //    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND] || [GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDONESIA] )
    }
    
    if ([[[mSession lookupTable] objectForKey: @"ConsumerSignUpOption"] intValue] != 1 ) //2 = in/th, 1 = my/ph, 3 = HK
    {
        
    }
    else
    {
        if (!IS_COUNTRY(COUNTRYCODE_INDIA))
        {
            [self.registrationFormArray addObject:@{@"Title": LOCALIZATION(C_CONSUMER_VEHICLEUSAGE),            //lokalise 23 Jan
                                                    @"Type": @(kREGISTRATION_DROPDOWN),
                                                    @"Key": kVehicleUsage,
                                                    @"Value": LOCALIZATION(C_CONSUMER_VEHICLEUSAGE),                //lokalise 23 Jan
                                                    }]; //button for vehicle Usage
        }
    }
    
    //    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND] || [GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDONESIA] )
    if ([[[mSession lookupTable] objectForKey: @"ConsumerSignUpOption"] intValue] != 1 ) //2 = in/th, 1 = my/ph, 3 = HK
    {
        self.registeredVehicleCheckboxView.hidden = YES;
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.registeredVehicleCheckboxView
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil attribute: NSLayoutAttributeNotAnAttribute multiplier:1 constant:0]];
    }
    
//    [self.registrationFormArray addObject:<#(nonnull id)#>];
//    [self.registrationFormArray addObject:<#(nonnull id)#>];
//    [self.registrationFormArray addObject:<#(nonnull id)#>];

    
    //set tableview height.
    self.tableViewHeight.constant = CELL_HEIGHT * [self.registrationFormArray count];
    self.regFormTableView.tableFooterView = [UIView new];
    [self.regFormTableView reloadData];
}

#pragma mark - tableview delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.registrationFormArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *regFieldData = [self.registrationFormArray objectAtIndex: indexPath.row];
    switch ([[regFieldData objectForKey: @"Type"] intValue])
    {
        case kREGISTRATION_TEXTFIELD:
        {
            RegistrationFormTextfieldCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormTextfieldCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [textfieldCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [textfieldCell hideTopLabel: NO];
            
            
            
            if ([regFieldData objectForKey: @"Placeholder"])
                [textfieldCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
            
            if ([regFieldData objectForKey: @"Value"])
                [textfieldCell setTextfieldText: [regFieldData objectForKey: @"Value"]];
            
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_BIRTHDATE:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            [dropdownCell hideTopLabel: NO];
            dropdownCell.button.tag = indexPath.row;
            
            if (kIsRightToLeft) {
                [dropdownCell setButtonContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
            } else {
                [dropdownCell setButtonContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            }
            
            [dropdownCell.button addTarget: self action: @selector(birthdayPressed:) forControlEvents:UIControlEventTouchUpInside];
            [self.regSubmissionDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        case kREGISTRATION_DROPDOWN:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            
            dropdownCell.button.tag = indexPath.row;
            
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [dropdownCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [dropdownCell hideTopLabel: NO];
            
//            switch (indexPath.row) {
//                case kTradeRegForm_YEAROFMANUFACTURE:
//                    break;
//                case kTradeRegForm_VEHICLEUSAGE:
//                    break;
//            }
            
            if ([[regFieldData objectForKey: @"Key"] isEqualToString: kYearOfManufacture])
            {
                [dropdownCell.button addTarget: self action: @selector(yearOfManufacturePressed) forControlEvents:UIControlEventTouchUpInside];
            }
            else if ([[regFieldData objectForKey: @"Key"] isEqualToString: kVehicleUsage])
            {
                [dropdownCell.button addTarget: self action: @selector(vehicleUsagePressed) forControlEvents:UIControlEventTouchUpInside];
            }
            //Vietname Rimula V2
            else if ([[regFieldData objectForKey: @"Key"] isEqualToString: kVehicleType])
            {
                [dropdownCell.button addTarget: self action: @selector(vehicleTypePressed) forControlEvents:UIControlEventTouchUpInside];
            }
//            else if ([[regFieldData objectForKey: @"Key"] isEqualToString: kBirthdayMonth])
//            {
//                [dropdownCell.button addTarget: self action: @selector(birthdayMonthPressed) forControlEvents:UIControlEventTouchUpInside];
//            }
            else if ([[regFieldData objectForKey: @"Key"] isEqualToString: kVehicleStateID])
            {
                [dropdownCell.button addTarget: self action: @selector(statePressed) forControlEvents:UIControlEventTouchUpInside];
            }
            
            if (kIsRightToLeft) {
                [dropdownCell setButtonContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
            } else {
                [dropdownCell setButtonContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            }
            
            //set initial value
            if ([regFieldData objectForKey: @"Value"])
                [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            
            //keep a reference of cell to submit json value later
            [self.regSubmissionDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        default:
            NSLog(@"UNKNOWN CELL TYPE");
            return [UITableViewCell new];
            break;
    }
    return [UITableViewCell new];
}

-(void) vehicleTypePressed
{
    //Vietnam Rimula Phase 2
    self.popupTag = kPopupSelection_VehicleType;
    [self popUpListViewWithTitle: LOCALIZATION(C_DROPDOWN_VEHICLETYPE_HEADER) listArray:[mSession getVehicleType]];
}

-(void) yearOfManufacturePressed
{
    self.popupTag = kPopupSelection_YearOfManufacture;
    [self popUpListViewWithTitle: LOCALIZATION(C_CONSUMER_YEAROFMANUFACTURE) listArray:[mSession getYearOfManufacture]];            //lokalise 23 Jan
}

-(void) vehicleUsagePressed
{
    self.popupTag = kPopupSelection_VehicleUsage;
    [self popUpListViewWithTitle: LOCALIZATION(C_CONSUMER_VEHICLEUSAGE) listArray:[mSession getVehicleUsage]];                      //lokalise 23 Jan
}

-(void) statePressed
{
    self.popupTag = kPopupSelection_State;
    [self popUpListViewWithTitle: LOCALIZATION(C_PROFILE_STATE_PLACEHOLDER) listArray:[mSession getStateNames: self.stateTable]];       //lokalised
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        RegistrationFormDropdownCell *cell;
        switch (self.popupTag)
        {
            case kPopupSelection_VehicleUsage:
            {
                cell = [self.regSubmissionDict objectForKey: kVehicleUsage];
            }
                break;
                
            case kPopupSelection_YearOfManufacture:
            {
                cell = [self.regSubmissionDict objectForKey: kYearOfManufacture];
            }
                break;
            case kPopupSelection_State:
            {
                cell = [self.regSubmissionDict objectForKey: kVehicleStateID];
            }
                break;
            case kPopupSelection_VehicleType:
            {
                cell = [self.regSubmissionDict objectForKey: kVehicleType];
            }
                break;
//            case kPopupSelection_BirthdayMonth:
//            {
//                cell = [self.regSubmissionDict objectForKey: kBirthdayMonth];
//            }
//                break;
            default:
                break;
        }
        [cell.button setTitle:selection.firstObject forState:UIControlStateNormal];
    }
}
- (IBAction)registerPressed:(UIButton *)sender {
    
    NSMutableDictionary *submitParams = [[NSMutableDictionary alloc] initWithDictionary: self.otpDict];
    
    if(!self.isAlreadyMember) {
        if ([[[mSession lookupTable] objectForKey: @"ConsumerSignUpOption"] intValue] == 2) //2 = in/th, 1 = my/ph
           {
               [submitParams setObject: [[self.regSubmissionDict objectForKey: kFullName] getJsonValue] forKey: kFirstName]; //full name to pass as FirstName
               [submitParams setObject: @"" forKey: kLastName];
           }
           else
           {
               [submitParams setObject: [[self.regSubmissionDict objectForKey: kFirstName] getJsonValue] forKey: kFirstName];
               [submitParams setObject: [[self.regSubmissionDict objectForKey: kLastName] getJsonValue] forKey: kLastName];
           }

           [submitParams setObject: self.member.dobDay forKey: kDOBDay];
           [submitParams setObject: self.member.dobMonth forKey: kDOBMonth];
    }
    
    [submitParams setObject: [[self.regSubmissionDict objectForKey: kVehicleNumber] getJsonValue] forKey: kVehicleNumber];
    
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
    {
        NSString *stateString = [[self.regSubmissionDict objectForKey: kVehicleStateID] getJsonValue];
        [submitParams setObject: [mSession convertToStateKeyCode: stateString stateDetailedListArray: self.stateTable] forKey: kVehicleStateID];
    }
    
    if(![GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_HONGKONG]) {
        [submitParams setObject: @([[mSession convertToYearOfManufactureKeyCode: [[self.regSubmissionDict objectForKey: kYearOfManufacture] getJsonValue]] intValue]) forKey: kYearOfManufacture];
        
        [submitParams setObject: @([[mSession convertToVehicleUsageKeyCode: [[self.regSubmissionDict objectForKey: kVehicleUsage] getJsonValue]] intValue]) forKey: kVehicleUsage];
    }
    

    //    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND] || [GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDONESIA] )
    if ([[[mSession lookupTable] objectForKey: @"ConsumerSignUpOption"] intValue] == 1) //2 = in/th, 1 = my/ph
    {
        [submitParams setObject: @(self.btnCheckbox.selected) forKey: kIsRegistered];    
    }
    else
    {
        [submitParams setObject: @(YES) forKey: kIsRegistered];
    }
    
    [submitParams setObject: @(self.btnMarketingCheckbox.selected) forKey: kIsSubscribeContent];
    
    if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_HONGKONG]) {
        [submitParams setObject: @(self.btnGogoVanSelection.selected) forKey: kHasGoGoVan];
        [submitParams setObject: self.textFieldGogoVanNumber.text forKey:kGGVDriverNumb];
    }
    
    //VN Rimula P2.
    if ([[mSession profileInfo] hasVehicleType]) {
        NSString *vehicleTypeKeyCode = [mSession convertToVehicleTypeKeyCode: [[self.regSubmissionDict objectForKey: kVehicleType] getJsonValue]];
        [submitParams setObject:vehicleTypeKeyCode forKey: kVehicleType];
    }
    
    //"FirstName": "Test",/*string*/
    //"LastName": "Testing",/*string*/
    //"VehicleNumber": "testtttsa",/*string*/
    //"YearOfManufacture": "2000",/*string*/
    //"VehicleUsage": "1",/*int*/
    
//    [mSession pushRegisterConsumerRegisterComplete: self];
    if(!self.isAlreadyMember) {
        [[WebServiceManager sharedInstance] submitMemberRegistrationStepTwo: submitParams view:self];
    } else {
        [[WebServiceManager sharedInstance] submitMemberVehicles:submitParams view:self];
    }
}

- (IBAction)checkBoxSelected:(UIButton *)sender {
    if ([sender isSelected])
        sender.selected = NO;
    else
        sender.selected = YES;
}

#pragma mark - WebServiceManagerDelegate Methods
- (void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_SUBMITMEMBERVEHICLES:
        case kWEBSERVICE_SUBMITMEMBER_FORM:
        {
            [mSession pushRegisterConsumerRegisterCompleteWithDict:self.otpDict vc: self];
//            [mSession loadRegisterConsumerRegisterCompleteWithDict: self.otpDict vc:self];
        }
            break;
        default:
            break;
    }
}


- (IBAction)backPressed:(id)sender {
    [self popSelf];
}


- (IBAction)birthdayPressed:(UIButton *)sender {
    [self.view endEditing: YES];
    
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}

#pragma birthday methods
- (void)selectedMonthYear:(NSDate *)selectedDate
{
    //do smething
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //   for API
//    [formatter setDateFormat:@"dd"];
    self.member.dobDay = @"01";
    
    [formatter setDateFormat:@"MM"];
    self.member.dobMonth = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"MMMM"];
    
    [formatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    NSString *selectedDateString = [formatter stringFromDate:selectedDate];
    
    
    RegistrationFormDropdownCell *cell = [self.regSubmissionDict objectForKey: kBirthdayMonth];
    [cell setSelectionTitle: selectedDateString];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
