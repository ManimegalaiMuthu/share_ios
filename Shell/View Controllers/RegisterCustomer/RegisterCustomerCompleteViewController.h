//
//  RegisterCustomerCompleteViewController.h
//  Shell
//
//  Created by Jeremy Lua on 7/6/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"
@protocol RegisterCustomerCompleteViewDelegate <NSObject>
-(void) completeRegister;
@end

@interface RegisterCustomerCompleteViewController : BaseVC
@property id<RegisterCustomerCompleteViewDelegate> delegate;
@property NSDictionary *promoDict;
@end
