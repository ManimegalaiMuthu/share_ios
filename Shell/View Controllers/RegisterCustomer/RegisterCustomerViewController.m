//
//  RegisterCustomerViewController.m
//  Shell
//
//  Created by Jeremy Lua on 4/7/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RegisterCustomerViewController.h"

@interface RegisterCustomerViewController ()<UITextFieldDelegate, WebServiceManagerDelegate>

enum kPopupSelection_Type
{
    kPopupSelection_State,
};

@property (strong, nonatomic) IBOutlet UIButton *backBtn;

//@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;

@property (weak, nonatomic) IBOutlet UILabel *lblOptionOne;
@property (weak, nonatomic) IBOutlet UIButton *btnRegisterCustomer;


@property (weak, nonatomic) IBOutlet UILabel *lblOr;

@property (weak, nonatomic) IBOutlet UILabel *lblOptionTwo;
@property (weak, nonatomic) IBOutlet UITextField *txtVehicleNum;
@property (weak, nonatomic) IBOutlet UITextField *txtMobileNum;
@property (weak, nonatomic) IBOutlet UIButton *btnInviteSMS;

@property (weak, nonatomic) IBOutlet UIButton *btnCountryCode;  //Disabled in storyboard

@property (weak, nonatomic) IBOutlet UIView *stateView;
@property (weak, nonatomic) IBOutlet UIButton *btnState; //Thai only
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stateViewHeight;
@property NSArray *stateTable;
@property BOOL isAlreadyMember;
@end

@implementation RegisterCustomerViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_INVITECUSTOMER) subtitle: @""];      //lokalise 23 Jan
    
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
    {
        [[WebServiceManager sharedInstance] loadState: [[mSession lookupTable] objectForKey: @"LookUpVersion"]
                                          countryCode: GET_COUNTRY_CODE
                                                   vc: self];
    }
    [self setupInterface];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self.parentViewController.childViewControllers count] > 1)
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    }
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_INVITECUSTOMER) screenClass:nil];
    self.isAlreadyMember = NO;
}

-(void) setupInterface
{
//    self.lblHeader.font = FONT_H1;
//    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
//    self.lblHeader.text = LOCALIZATION(C_CONSUMER_ONEMORESTEP);
    
    self.lblContent.font = FONT_B1;
    self.lblContent.textColor = COLOUR_VERYDARKGREY;
    self.lblContent.text = LOCALIZATION(C_CONSUMER_ONEMORESTEPCONTENT);     //lokalise 23 Jan
    
    self.lblOptionOne.font = FONT_H1;
    self.lblOptionOne.textColor = COLOUR_VERYDARKGREY;
    self.lblOptionOne.text = LOCALIZATION(C_CONSUMER_OPTIONONE);            //lokalise 23 Jan
    
    self.lblOr.font = FONT_B1;
    self.lblOr.textColor = COLOUR_VERYDARKGREY;
    self.lblOr.text = LOCALIZATION(C_CONSUMER_OR);                          //lokalise 23 Jan
    
    self.lblOptionTwo.font = FONT_H1;
    self.lblOptionTwo.textColor = COLOUR_VERYDARKGREY;
    self.lblOptionTwo.text = LOCALIZATION(C_CONSUMER_OPTIONTWO);            //lokalise 23 Jan
    
    self.txtVehicleNum.font = FONT_B1;
    self.txtVehicleNum.textColor = COLOUR_VERYDARKGREY;
    self.txtVehicleNum.placeholder = LOCALIZATION(C_CONSUMER_CUSTOMERVEHICLE_PLACEHOLDER);      //lokalise 23 Jan
    self.txtVehicleNum.text = [self.registerCustomerDict objectForKey: @"VehicleNumber"];
    
    
    self.txtMobileNum.font = FONT_B1;
    self.txtMobileNum.textColor = COLOUR_VERYDARKGREY;
    self.txtMobileNum.placeholder = LOCALIZATION(C_CONSUMER_CUSTOMERMOBILE_PLACEHOLDER);        //lokalise 23 Jan
    
    [self.btnCountryCode.titleLabel setFont: FONT_B1];
    [self.btnCountryCode setTitle: [mSession convertCountryCodeToCountryDisplayName: GET_COUNTRY_CODE] forState: UIControlStateNormal];
    
    [self.btnRegisterCustomer.titleLabel setFont: FONT_BUTTON];
    [self.btnRegisterCustomer setTitle:LOCALIZATION(C_CONSUMER_REGISTERCUSTOMER) forState:UIControlStateNormal];    //lokalise 23 Jan
    [self.btnRegisterCustomer setBackgroundColor:COLOUR_RED];
    
    [self.btnInviteSMS.titleLabel setFont: FONT_BUTTON];
    [self.btnInviteSMS setTitle:LOCALIZATION(C_CONSUMER_INVITEVIASMS) forState:UIControlStateNormal];               //lokalise 23 Jan
    [self.btnInviteSMS setBackgroundColor:COLOUR_RED];
    
    
    if (![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
    {
        self.stateViewHeight.constant = 0;
        self.stateView.hidden = YES;
    }
    else
    {
        
        [self.btnState.titleLabel setFont: FONT_B1];
        
        self.txtVehicleNum.text = [self.registerCustomerDict objectForKey: @"VehicleNumber"];
    }
    
    if (kIsRightToLeft) {
        [self.txtMobileNum setTextAlignment:NSTextAlignmentRight];
        [self.txtVehicleNum setTextAlignment:NSTextAlignmentRight];
        [self.btnCountryCode setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.txtMobileNum setTextAlignment:NSTextAlignmentLeft];
        [self.txtVehicleNum setTextAlignment:NSTextAlignmentLeft];
        [self.btnCountryCode setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
}

- (IBAction)statePressed:(id)sender {
    
//    self.popupTag = kPopupSelection_State;
    [self popUpListViewWithTitle: LOCALIZATION(C_DROPDOWN_STATE) listArray:[mSession getStateNames: self.stateTable]];      //lokalise 23 Jan
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        RegistrationFormDropdownCell *cell;
//        switch (self.popupTag)
//        {
//                
//            default:
//                break;
//        }
        [self.btnState setTitle:selection.firstObject forState:UIControlStateNormal];
    }
}


- (IBAction)registerCustomerPressed:(id)sender {
    //have to cancel popup
//    [self cancelButtonAction: nil];
    
    //push to customer register view
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"CustomerRegister" object: self.registerCustomerDict];
    [mSession pushRegisterConsumerOTPViewWithDict:self.registerCustomerDict vc: self];

}

- (IBAction)editingChanged:(id)sender {
    if ([self.txtMobileNum.text length] > 0 &&
        [self.txtVehicleNum.text length] > 0)
    {
        self.btnInviteSMS.alpha = 1.0f;
    }
    else
    {
        self.btnInviteSMS.alpha = 0.75f;
    }
}

- (IBAction)invitePressed:(id)sender {
    if ([self.txtMobileNum.text length] > 0 &&
        [self.txtVehicleNum.text length] > 0)
    {
        NSString *stateID = [mSession convertToStateKeyCode:self.btnState.titleLabel.text stateDetailedListArray: self.stateTable];
        
        NSDictionary *submitParams = @{@"Name": @"",
                                       @"MobileNumber": self.txtMobileNum.text,
                                       @"EmailAddress": @"",
                                       @"VehicleNumber": self.txtVehicleNum.text,
                                       @"PromoCode": [self.registerCustomerDict objectForKey: @"PromoCode"],
                                       @"VehicleStateID" : stateID,
                                       @"IsConfirmed" : @(NO),
                                       };
        
        [[WebServiceManager sharedInstance] inviteConsumer: submitParams vc:self];
    }
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_INVITECONSUMER:
        {
//            [self cancelButtonAction: nil];
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                [self popSelf];
            }];
            
            //                [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
            //                    [self.delegate didPressedRegisterCustomer];
            //                }];
        }
            break;
            
        case kWEBSERVICE_STATE:
        {
            SET_LOADSTATE([[response getGenericResponse] objectForKey: @"StateDetailsList"]);
            self.stateTable = [[response getGenericResponse] objectForKey: @"StateDetailsList"];
            
            //if thailand
            NSString *stateID = [self.registerCustomerDict objectForKey: @"VehicleStateID"];
            
            if (stateID.length > 0)
                [self.btnState setTitle: [mSession convertToStateName: stateID stateDetailedListArray:self.stateTable] forState:UIControlStateNormal];
            else
                
                [self.btnState setTitle: LOCALIZATION(C_PROFILE_STATE_PLACEHOLDER) forState:UIControlStateNormal];      //lokalise 23 Jan
            
            [self setupInterface];
        }
            break;
        default:
            break;
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_INVITECONSUMER:
        {
//            [self cancelButtonAction: nil];
            if ([response getResponseCode] == 8111)
            {
                [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion: nil];
                
                //                [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                //                                    [self.delegate didPressedRegisterCustomer];
                //                }];
            }
            else if ([response getResponseCode] == 8112)
            {
                [mAlert showSuccessAlertWithTitle: LOCALIZATION(C_GLOBAL_SUCCESS) withMessage: [response getResponseMessage] withCloseButtonTitle: LOCALIZATION(C_CONSUMER_CONTINUE) onCompletion:^(BOOL finished) {                //lokalise 23 Jan
//                    [self.delegate didPressedRegisterCustomer];
                }];
            } else if([response getResponseCode] == 8113) {
                [self popUpYesNoWithTitle: @"" content: LOCALIZATION(C_POPUP_INVITECUSTOMER_ALREADYMEMBER) yesBtnTitle: LOCALIZATION(C_FORM_YES) noBtnTitle: LOCALIZATION(C_FORM_NO) onYesPressed:^(BOOL finished) {
                     self.isAlreadyMember = YES;
                     if ([self.txtMobileNum.text length] > 0 &&
                         [self.txtVehicleNum.text length] > 0)
                     {
                         NSString *stateID = [mSession convertToStateKeyCode:self.btnState.titleLabel.text stateDetailedListArray: self.stateTable];
                         
                         NSDictionary *submitParams = @{@"Name": @"",
                                                        @"MobileNumber": self.txtMobileNum.text,
                                                        @"EmailAddress": @"",
                                                        @"VehicleNumber": self.txtVehicleNum.text,
                                                        @"PromoCode": [self.registerCustomerDict objectForKey: @"PromoCode"],
                                                        @"VehicleStateID" : stateID,
                                                        @"IsConfirmed" : @(YES),                                                        };
                         
                         [[WebServiceManager sharedInstance] inviteConsumer: submitParams vc:self];
                     }
                } onNoPressed:^(BOOL finished) {
                    self.txtMobileNum.text = @"";
                }];
            } else if([response getResponseCode] == 8115) {
                [self popUpYesNoWithTitle: @"" content: LOCALIZATION(C_POPUP_INVITECUSTOMER_ALREADYMEMBER) yesBtnTitle: LOCALIZATION(C_FORM_YES) noBtnTitle: LOCALIZATION(C_FORM_NO)
                             onYesPressed:^(BOOL finished) {
                                [self popupInfoVCWithTitle:@"" content:LOCALIZATION(C_POPUP_INVITECUSTOMER_MAXVEHICLE) btnTitle:LOCALIZATION(C_GLOBAL_OK) onYesPressed:^(BOOL finished){
                                                                            [self popSelf];
                                }];
                    } onNoPressed:^(BOOL finished) {
                        self.txtMobileNum.text = @"";
                }];
            }
        }
            break;
            
        case kWEBSERVICE_STATE:
            self.stateTable = GET_LOADSTATE;
        default:
            break;
    }
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
