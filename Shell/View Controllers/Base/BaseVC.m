//
//  BaseVC.m
//  JTIStarPartners
//
//  Created by Shekhar  on 17/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import "BaseVC.h"
#import "LogHelper.h"

#import "PopupViewController.h"
#import "PopupInfoViewController.h"
#import "PopupInfoTableViewController.h"
#import "PopUpYesNoViewController.h"
#import "PopupTagDecalViewController.h"
#import "PopupTagDecalToVehicleViewController.h"
#import "PopupRegisterCustomerViewController.h"
#import "PopupRewardFilterViewController.h"
#import "PopupTextViewController.h"

#import "PopupPrivacyPolicyViewController.h"
#import "PopupWebviewViewController.h"
#import "PopupAttributedLabelViewController.h"

#import "PopupCameraDecalViewController.h"
#import "PopupSelectMultipleViewController.h"
#import "PopupCustomerServiceViewController.h"
#import "PopupValueEntryViewController.h"

#import "PopupChangePasswordViewController.h"
#import "PopupWithTwoButtons_NewViewController.h"

@interface BaseVC () <WebServiceManagerDelegate>



@end

@implementation BaseVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//  [MGLog info:@"Limo-UI" class:NSStringFromClass([self class]) selector:NSStringFromSelector(_cmd) message:@""];
}

-(void) setupGoogleAnalytics
{
    
}

-(void)keyboardOnScreen:(NSNotification *)notification
{
    NSDictionary *info  = notification.userInfo;
    NSValue      *value = info[UIKeyboardFrameEndUserInfoKey];
    
    CGRect rawFrame      = [value CGRectValue];
    self.keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [CrashlyticsKit setObjectValue: NSStringFromClass([self class]) forKey: @"Current View Controller"]; //send current loaded VC
    [mSession setCurrentViewControllerString: NSStringFromClass([self class])];
    
    [mSession setCurrentViewControllerString: NSStringFromClass([self class])];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardOnScreen:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardOnScreen:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
//    [self.navigationController setValue:[[UINavigationBar alloc]init] forKeyPath:@"navigationBar"];
//    [self.navigationController.navigationBar setTranslucent:NO];
//    self.edgesForExtendedLayout = YES;
//    NSLog(@"%@",NSStringFromClass([self class]));
//    [mLogHelper setExceptionErrorInfo:NSStringFromClass([self class])];
    //[MGLog info:@"Limo-UI" class:NSStringFromClass([self class]) selector:NSStringFromSelector(_cmd) message:@""];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"Unread Icon Changed" object: nil];
    
    self.isPositionRequired = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    //[MGLog info:@"Limo-UI" class:NSStringFromClass([self class]) selector:NSStringFromSelector(_cmd) message:@""];
    [self.view endEditing: YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
//    [[NSNotificationCenter defaultCenter] removeObserver: self
//                                                    name: kNotificationLanguageChanged
//                                                  object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver: self
//                                                    name:@"Reloaded Lookup Table"
//                                                  object:nil];
}

-(void) setNavBackBtn
{
    UIButton *btnBack = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30, 30)];
    if (kIsRightToLeft) {
        [btnBack setImage: [UIImage imageNamed:@"icon-arrsingleR.png"] forState: UIControlStateNormal];
    } else {
        [btnBack setImage: [UIImage imageNamed: @"icon-arrsingleL.png"] forState: UIControlStateNormal];
    }
    [btnBack addTarget:self action: @selector(popSelf) forControlEvents: UIControlEventTouchUpInside];
    if(@available(iOS 11, *)){
        [btnBack.widthAnchor constraintEqualToConstant: btnBack.frame.size.width].active = YES;
        [btnBack.heightAnchor constraintEqualToConstant: btnBack.frame.size.height].active = YES;
    }
    
    UIBarButtonItem *backBarBtn = [[UIBarButtonItem alloc] initWithCustomView: btnBack];
    self.navigationItem.leftBarButtonItem = backBarBtn;
}


- (void)popSelf
{
    [UpdateHUD removeSpotifyHUD:CURRENT_VIEW];
    
    CATransition *animation = [CATransition animation];
    animation.type = kCATransitionFade;
    animation.duration = 0.4;
    [self.navigationController.view.layer addAnimation:animation forKey:nil];
    
#warning test for popping the only controller
    //    if ([[vc.navigationController childViewControllers] count] > 1)
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)popToIndex:(NSInteger) index
{
    [UpdateHUD removeSpotifyHUD:CURRENT_VIEW];
    
    CATransition *animation = [CATransition animation];
    animation.type = kCATransitionFade;
    animation.duration = 0.4;
    [self.navigationController.view.layer addAnimation:animation forKey:nil];
    
#warning out of bounds handling
    //    if ([[vc.navigationController childViewControllers] count] > 1)
    UIViewController *targetChildVC = [[self.navigationController childViewControllers] objectAtIndex: index];
    [self.navigationController popToViewController: targetChildVC animated:NO];
}

-(void) popUpViewWithPopupVC:(UIViewController *)popupVc
{
    [CURRENT_VIEWCONTROLLER addChildViewController: popupVc];
    [CURRENT_VIEWCONTROLLER.view addSubview:popupVc.view];
    [popupVc didMoveToParentViewController: CURRENT_VIEWCONTROLLER];
    
    popupVc.view.accessibilityViewIsModal = YES;
    popupVc.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
}

-(void) popUpViewWithTitle:(NSString *) title content:(NSString *)content
{
    [self.view endEditing: YES];
    PopupInfoViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPINFOVIEW];
    [self popUpViewWithPopupVC: popupVc];
    
    [popupVc initPopupInfoVCWithTitle: title content:content];
}

-(void) popUpListViewWithTitle:(NSString *) title listArray:(NSArray *)listArray
{
    [self.view endEditing: YES];
    PopupViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPVIEW];
    
    [self popUpViewWithPopupVC: popupVc];
    
    popupVc.delegate = self;
    [popupVc initSingleSelectionListWithTitle:title listArray: listArray hasSearchField:NO];
}
-(void) popUpListViewWithTitle:(NSString *) title listArray:(NSArray *)listArray withSearchField:(BOOL) withSearchField
{
    [self.view endEditing: YES];
    PopupViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPVIEW];
    
    [self popUpViewWithPopupVC: popupVc];
    
    popupVc.isPositionRequired = self.isPositionRequired;
    popupVc.delegate = self;
    [popupVc initSingleSelectionListWithTitle:title listArray: listArray hasSearchField:withSearchField];
}

-(void) popUpMultipleSelectListViewWithTitle:(NSString *) title listArray:(NSArray *)listArray withSearchField:(BOOL) withSearchField withPreselect:(NSArray *)selectedArray withVc:(BOOL)isSelectedVc
{
    [self.view endEditing: YES];
    PopupSelectMultipleViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPMULTIPLESELECT];
    NSLog(@"select vc:%d",isSelectedVc);
    [self popUpViewWithPopupVC: popupVc];
    
    popupVc.delegate = self;
    [popupVc initMultipleSelectionListWithTitle:title listArray:listArray hasSearchField:withSearchField withPreselect:selectedArray isSelectVc:isSelectedVc];
}

-(void) popUpYesNoWithTitle:(NSString *)title content:(NSString *)contentString yesBtnTitle:(NSString *)yesBtnTitle noBtnTitle:(NSString *)noBtnTitle onYesPressed:(CompletionBlock)yesBlock onNoPressed:(CompletionBlock)noBlock
{
    [self.view endEditing: YES];
    PopUpYesNoViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPYESNOVIEW];
    
    [self popUpViewWithPopupVC: popupVc];
    
    popupVc = [popupVc initWithTitle:title content:contentString yesBtnTitle:yesBtnTitle noBtnTitle:noBtnTitle onYesPressed: yesBlock onNoPressed:noBlock];
}

-(void) popUpYesNoWithTitle:(NSString *)title contentString:(NSString *)contentString yesBtnTitle:(NSString *)yesBtnTitle noBtnTitle:(NSString *)noBtnTitle
{
    [self.view endEditing: YES];
    PopUpYesNoViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPYESNOVIEW];
    
    [self popUpViewWithPopupVC: popupVc];
    
    popupVc = [popupVc initWithTitle:title content:contentString yesBtnTitle:yesBtnTitle noBtnTitle:noBtnTitle];
}

-(void) popUpTextViewWithTitle:(NSString*)title yesButtonTitle:(NSString*) yesTitle noButtonTitle:(NSString*) noTitle placeHolder:(NSString*)placeHolder completionHandler:(void(^)(BOOL isYes,NSString* remarks)) completionHandler
{
    [self.view endEditing: YES];
    PopupTextViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPTEXTVIEW];

    [self popUpViewWithPopupVC: popupVc];

    [popupVc initWithTitle:title yesButtonTitle:yesTitle noButtonTitle:noTitle placeHolder:placeHolder completionHandler:completionHandler];
}

-(void) initDecalPopupWithDecalCode:(NSString *) decalCode
{
    [self.view endEditing: YES];
    PopupTagDecalViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPTAGDECALVIEW];
    popupVc.delegate = self;
    
    [self popUpViewWithPopupVC: popupVc];
    
    popupVc.delegate = self;
    [popupVc initDecalPopupWithDecalCode: decalCode];
}

-(void) initTagDecalToVehicleWithVehicleId:(NSString *)vehicleId stateName:(NSString *)stateName
{
    [self.view endEditing: YES];
    PopupTagDecalToVehicleViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPTAGDECALTOVEHICLEVIEW];
    
    
    popupVc.vehicleId = vehicleId;
    
    NSString *stateId = [mSession convertToStateKeyCode: stateName stateDetailedListArray: GET_LOADSTATE];
    if ([stateId isEqualToString: @""])
        popupVc.stateId = -1;
    else
        popupVc.stateId = [stateId intValue];
    
    [self popUpViewWithPopupVC: popupVc];
    
    popupVc.delegate = self;
}

-(void) initFilterRewardPopup:(NSString *)title leftValue:(NSInteger)leftValue rightValue:(NSInteger)rightValue isChecked:(BOOL)isChecked
{
    [self.view endEditing: YES];
    PopupRewardFilterViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPREWARDFILTERVIEW];

    popupVc.delegate = self;
    
    [self popUpViewWithPopupVC: popupVc];
    
    [popupVc initFilterRewardPopup: title leftValue:leftValue rightValue:rightValue isChecked: isChecked];
}

-(void) initRegisterCustomerPopup:(NSDictionary *)registerCustomerDict
{
    [self.view endEditing: YES];
    PopupRegisterCustomerViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPREGISTERCUSTOMERVIEW];
    popupVc.registerCustomerDict = registerCustomerDict;

    popupVc.delegate = self;
    
    [self popUpViewWithPopupVC: popupVc];
}

-(void) initPrivacyPolicyPopup
{
    [self.view endEditing: YES];
    PopupPrivacyPolicyViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPPRIVACYPOLICYVIEW];
    
    popupVc.delegate = self;
    
    [self popUpViewWithPopupVC: popupVc];
}

-(void) popupInitWithHTMLBody: (NSString *) htmlBody headerTitle: (NSString *)headerTitle buttonTitle: (NSString *) buttonTitle
{
    [self.view endEditing: YES];
    PopupWebviewViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPWEBVIEW];
    
    [self popUpViewWithPopupVC: popupVc];
    
    [popupVc initWithHTMLBody:htmlBody headerTitle:headerTitle buttonTitle:buttonTitle];
}

-(void) popupInitWithURL: (NSString *) urlAddress headerTitle: (NSString *)headerTitle buttonTitle: (NSString *) buttonTitle
{
    [self.view endEditing: YES];
    PopupWebviewViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPWEBVIEW];
    
    [self popUpViewWithPopupVC: popupVc];
    
    [popupVc initWithUrl:urlAddress headerTitle:headerTitle buttonTitle:buttonTitle];
}

-(void) popupAttributedInfoWithTitle: (NSString *) title content:(NSString *)content {
    [self.view endEditing: YES];
    PopupAttributedLabelViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPATTRIBUTEDLABEL];
    [self popUpViewWithPopupVC: popupVc];
    
    [popupVc initPopupAttributedLabelVCWithTitle:title content:content];
}

-(void) popupInfoVCWithTitle:(NSString *)title content:(NSString *)content btnTitle:(NSString *)btnTitle onYesPressed:(CompletionBlock)yesBlock
{
    [self.view endEditing: YES];
    PopupInfoViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPINFOVIEW];
    [self popUpViewWithPopupVC: popupVc];
    
    [popupVc initPopupInfoVCWithTitle:title content:content btnTitle:btnTitle onYesPressed:yesBlock];
}

-(void) popupCameraDecal
{
    PopupCameraDecalViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPCAMERADECAL];
    popupVc.delegate = self;
    [self popUpViewWithPopupVC: popupVc];
}

-(void) popupCustomerService {
    PopupCustomerServiceViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPCUSTOMERSERVICE];
    [self popUpViewWithPopupVC: popupVc];
}

-(void) popupChangePassword {
    PopupChangePasswordViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPCHANGEPASSWORD];
    [self popUpViewWithPopupVC: popupVc];
}

-(void) popupValueEntryVCWithTitle:(NSString *)title submitTitle:(NSString *)submitTitle cancelTitle:(NSString *)cancelTitle placeHolder:(NSString *)placeHolder {
    [self.view endEditing: YES];
    
    PopupValueEntryViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPVALUEENTRY];
    popupVc.delegate = self;
    [self popUpViewWithPopupVC: popupVc];
    
    [popupVc initWithTitle:title submitButtonTitle:submitTitle cancelButtonTitle:cancelTitle placeHolder:placeHolder];
}

-(void) popupTwoButtonWithUrl:(NSString *)urlString withLaterBTnTitle:(NSString *)laterBtnTitle andActionTitle:(NSString *)actionBtnTitle onLaterPressed:(CompletionBlock)laterBlock onActionPressed:(CompletionBlock)actionBlock {
    
    PopupWithTwoButtons_NewViewController *popupVC = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUP_WITH2BUTTONS_NEW];
    [self popUpViewWithPopupVC:popupVC];
    
    [popupVC initWithUrl:urlString withLaterBTnTitle:laterBtnTitle andActionTitle:actionBtnTitle onLaterPressed:laterBlock onActionPressed:actionBlock];
    
}

#pragma mark - for zooming image
- (void)setAnchorPoint:(CGPoint)anchorPoint forView:(UIView *)view {
    // sanity check - x and y MUST be between 0 and 1
    if (anchorPoint.x < 0 || anchorPoint.x > 1 ||
        anchorPoint.y < 0 || anchorPoint.y > 1) {
        return;
    }

    CGPoint newPoint = CGPointMake(view.bounds.size.width * anchorPoint.x,
                                   view.bounds.size.height * anchorPoint.y);
    CGPoint oldPoint = CGPointMake(view.bounds.size.width * view.layer.anchorPoint.x,
                                   view.bounds.size.height * view.layer.anchorPoint.y);

    newPoint = CGPointApplyAffineTransform(newPoint, view.transform);
    oldPoint = CGPointApplyAffineTransform(oldPoint, view.transform);

    CGPoint position = view.layer.position;

    position.x -= oldPoint.x;
    position.x += newPoint.x;

    position.y -= oldPoint.y;
    position.y += newPoint.y;

    view.layer.position = position;
    view.layer.anchorPoint = anchorPoint;
}


- (IBAction)zoomingImage:(UIPinchGestureRecognizer *)sender {
      static CGAffineTransform initialTransform;
      static CGPoint initialAnchor;

      if (sender.state == UIGestureRecognizerStateBegan) {
          // save these for later states
          initialTransform = sender.view.transform;
          initialAnchor = sender.view.layer.anchorPoint;

          // get the center point of the pinch
          CGPoint touch = [sender locationInView:sender.view];

          // anchor point is relative to the view bounds:  0 ... up to 1.0, for both x and y
          CGFloat anchorX = touch.x / self.view.bounds.size.width;
          CGFloat anchorY = touch.y / self.view.bounds.size.height;

          // set the layer anchor point AND position, to where the view was initially pinched
          [self setAnchorPoint:CGPointMake(anchorX,anchorY) forView:self.view];

      } else if (sender.state == UIGestureRecognizerStateChanged) {
          // perform the pinch zoom
          sender.view.transform = CGAffineTransformScale(initialTransform,sender.scale,sender.scale);

      } else if (sender.state == UIGestureRecognizerStateEnded) {
          // reset the scale when it's done
          sender.scale = 1;

          // restore the original anchor point
          [self setAnchorPoint:initialAnchor forView:self.view];
      }
}


-(void)processFailed:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress: KEY_WINDOW];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    //[MGLog error:@"Limo-UI" class:NSStringFromClass([self class]) selector:NSStringFromSelector(_cmd) message:@""];
    
    // Dispose of any resources that can be recreated.
}


@end
