//
//  BaseVC.h
//  JTIStarPartners
//
//  Created by Shekhar  on 17/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IQKeyboardManager.h"
#import "WebServiceManager.h"
#import "Session.h"
#import "AppDelegate.h"
#import "Helper.h"
#import "Constants.h"
#import "LocalizationManager.h"
#import "AlertViewManager.h"
#import "UpdateHUD.h"
#import "TableViewWithEmptyView.h"
#import "FileManager.h"
#import "IQKeyboardReturnKeyHandler.h"
#import "UIViewController+JASidePanel.h"
#import "CASCommonListView.h"
#import "TTTAttributedLabel.h"
#import "UIImageView+WebCache.h"

#import "RegistrationButtonCell.h"
#import "RegistrationMobileNumberCell.h"
#import "RegistrationFormTextfieldCell.h"
#import "RegistrationFormDropdownCell.h"
#import "RegistrationTextfieldWithButtonCell.h"
#import "RegistrationLabelTextfieldTableViewCell.h"
#import "RegistrationTextViewTableViewCell.h"
#import "RegistrationHeaderTableViewCell.h"
#import "RegistrationWorkingHoursTableViewCell.h"
#import "RegistrationWorkingHoursLabelTableViewCell.h"
#import "ContactUsImageTableViewCell.h"
#import "NativeDatePicker.h"
#import "OrderDetailsPopup.h"

#import "ContactPreferencesCollectionViewCell.h"
#import "ScanProductTableViewCell.h"
#import <Firebase.h>
//#import "GAITracker.h"
//#import "GAI.h"
//#import "GAIDictionaryBuilder.h"
//#import "GAIFields.h"

#import "CheckboxCell.h"
#import "SignatureTableViewCell.h"
#import "HeaderWithReqTableViewCell.h"
#import "ContactPrefTableViewCell.h"
#import "ButtonTableViewCell.h"
#import "TnCTableViewCell.h"

#import "WebViewHelper.h"

#import <Crashlytics/Crashlytics.h>

@interface BaseVC : UIViewController

@property CASCommonListView *listView;
@property NativeDatePicker *nativeDatePickerView;
@property CGRect keyboardFrame;

@property BOOL isPositionRequired;
@property NSString *navigationBarTitle;

- (void)updateLocalisation;

-(void) setNavBackBtn;

- (void)popSelf;
- (void)popToIndex:(NSInteger) index;
-(void)keyboardOnScreen:(NSNotification *)notification;


-(void) popUpViewWithPopupVC:(UIViewController *)popupVc;
-(void) popUpViewWithTitle:(NSString *) title content:(NSString *)content;
-(void) popUpListViewWithTitle:(NSString *) title listArray:(NSArray *)listArray;
-(void) popUpListViewWithTitle:(NSString *) title listArray:(NSArray *)listArray withSearchField:(BOOL) withSearchField;
-(void) popUpMultipleSelectListViewWithTitle:(NSString *) title listArray:(NSArray *)listArray withSearchField:(BOOL) withSearchField withPreselect:(NSArray *)selectedArray withVc:(BOOL) isSelectedVc;


-(void) popUpYesNoWithTitle:(NSString *)title contentString:(NSString *)contentString yesBtnTitle:(NSString *)yesBtnTitle noBtnTitle:(NSString *)noBtnTitle;
-(void) popUpYesNoWithTitle:(NSString *)title content:(NSString *)contentString yesBtnTitle:(NSString *)yesBtnTitle noBtnTitle:(NSString *)noBtnTitle onYesPressed:(CompletionBlock)yesBlock onNoPressed:(CompletionBlock)noBlock;
-(void) popUpTextViewWithTitle:(NSString*)title yesButtonTitle:(NSString*) yesTitle noButtonTitle:(NSString*) noTitle placeHolder:(NSString*)placeHolder completionHandler:(void(^)(BOOL isYes,NSString* remarks)) completionHandler;


-(void) initDecalPopupWithDecalCode:(NSString*) decalCode;
-(void) initTagDecalToVehicleWithVehicleId:(NSString *)vehicleId stateName:(NSString *)stateName;

-(void) initRegisterCustomerPopup:(NSDictionary *)registerCustomerDict;

-(void) initFilterRewardPopup:(NSString *)title leftValue:(NSInteger)leftValue rightValue:(NSInteger)rightValue isChecked:(BOOL)isChecked;

-(void) initPrivacyPolicyPopup;

-(void) popupInitWithHTMLBody: (NSString *) htmlBody headerTitle: (NSString *)headerTitle buttonTitle: (NSString *) buttonTitle;
-(void) popupInitWithURL: (NSString *) urlAddress headerTitle: (NSString *)headerTitle buttonTitle: (NSString *) buttonTitle;
-(void) popupAttributedInfoWithTitle: (NSString *) title content:(NSString *)content;
-(void) popupInfoVCWithTitle:(NSString *)title content:(NSString *)content btnTitle:(NSString *)btnTitle onYesPressed:(CompletionBlock)yesBlock;

-(void) popupCameraDecal;
-(void) popupCustomerService;

-(void) popupChangePassword;
-(void) popupValueEntryVCWithTitle:(NSString *)title submitTitle:(NSString *)submitTitle cancelTitle:(NSString *)cancelTitle placeHolder:(NSString *)placeHolder;

-(void) popupTwoButtonWithUrl:(NSString *)urlString withLaterBTnTitle:(NSString *)laterBtnTitle andActionTitle:(NSString *)actionBtnTitle onLaterPressed:(CompletionBlock)laterBlock onActionPressed:(CompletionBlock)actionBlock;

- (void)setAnchorPoint:(CGPoint)anchorPoint forView:(UIView *)view;
- (IBAction)zoomingImage:(UIPinchGestureRecognizer *)sender;

@end
