//
//  SplashScreenVC.m
//  JTIStarPartners
//
//  Created by Shekhar  on 17/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import "SplashScreenVC.h"
#import "Constants.h"
#import "UpdateHUD.h"
#import "AppInfo.h"
#import "LocalizationManager.h"
#import "LogHelper.h"

@interface SplashScreenVC ()
{
    
}

@property (weak, nonatomic) IBOutlet UIImageView *imgSplash;
@property (strong, nonatomic) IBOutlet UIView *loadingHUD;

@end

@implementation SplashScreenVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    if(self.lblVersion.isHidden)
        [mLookupManager loadLookupTable];
    [UpdateHUD addSpotifyHUDSplash:self.loadingHUD];
    AppInfo *appinfo = [[AppInfo alloc]initWithAppInfo];
    
    if (IS_DEVELOPMENT_VERSION || IS_STAGING_VERSION)
        self.lblVersion.text = [NSString stringWithFormat:@"%@ %@ (%@)",LOCALIZATION(C_GLOBAL_VERSION), appinfo.appVersion, appinfo.appBuild ];   //lokalised
    else
        self.lblVersion.text = [NSString stringWithFormat:@"%@ %@",LOCALIZATION(C_GLOBAL_VERSION), appinfo.appVersion];       //lokalised
    
    if ([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK]) //pre-login splash
    {
        [self.imgSplash setImage: [UIImage imageNamed: @"prelogin_splash.jpg"]];
        self.lblVersion.textColor = COLOUR_VERYDARKGREY;
    }
    else
    {
        if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
        {
            [self.imgSplash setImage: [UIImage imageNamed: @"ru_splash.jpg"]];
            self.lblVersion.textColor = COLOUR_WHITE;
        }
        else if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
        {
            
            [self.imgSplash setImage: [UIImage imageNamed: @"th_splash.jpg"]];
            self.lblVersion.textColor = COLOUR_WHITE;
        }
        else if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_INDONESIA])
        {
            [self.imgSplash setImage: [UIImage imageNamed: @"ID_mco.jpg"]];
            self.lblVersion.textColor = COLOUR_WHITE;
        }
        else if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_MALAYSIA])
        {
            [self.imgSplash setImage: [UIImage imageNamed: @"my_splash_Aug2020.png"]];
            self.lblVersion.textColor = COLOUR_VERYDARKGREY;
        }
        else if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_INDIA])
        {
            [self.imgSplash setImage: [UIImage imageNamed: @"in_splash.jpg"]];
            self.lblVersion.textColor = COLOUR_WHITE;
        }
        else if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_HONGKONG])
        {
            [self.imgSplash setImage:[UIImage imageNamed:@"hk_newsplashscreen.jpg"]];
            self.lblVersion.textColor = COLOUR_WHITE;
        }
        else if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_VIETNAM])
        {
            [self.imgSplash setImage:[UIImage imageNamed:@"vn_splash.png"]];
            
        }
        else if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_SAUDI])
        {
            [self.imgSplash setImage:[UIImage imageNamed:@"SA_splash_2020.png"]];
            self.lblVersion.textColor = COLOUR_WHITE;
        }
        else if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_PHILLIPPINES])
        {
            if([[[mSession loadUserProfile] companyCategory] isEqualToString:@"2"]) {
                [self.imgSplash setImage:[UIImage imageNamed:@"ph_splash_MCO.png"]];
            }
            else {
                [self.imgSplash setImage:[UIImage imageNamed:@"ph_splash.png"]];
            }
            self.lblVersion.textColor = COLOUR_WHITE;
        } else if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_TURKEY]) {
            [self.imgSplash setImage:[UIImage imageNamed:@"tr_splash.png"]];
            self.lblVersion.textColor = COLOUR_VERYDARKGREY;
        }
        else
        {
            [self.imgSplash setImage: [UIImage imageNamed: @"splash.jpg"]];
            self.lblVersion.textColor = COLOUR_WHITE;
        }
    }
    
    [mLogHelper setExceptionErrorInfo:NSStringFromClass([self class])];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
