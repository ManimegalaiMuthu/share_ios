//
//  SplashScreenVC.h
//  JTIStarPartners
//
//  Created by Shekhar  on 17/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Session.h"
#import "Constants.h"

@interface SplashScreenVC : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblVersion;

@end
