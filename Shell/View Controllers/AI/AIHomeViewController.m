//
//  AIHomeViewController.m
//  Shell
//
//  Created by Nach on 5/6/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "AIHomeViewController.h"
#define BAR_WIDTH                   20.0

@interface AIHomeViewController () <WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblGreetings;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UIImageView *rewardImageView;

@property (weak, nonatomic) IBOutlet UIView *pieChartView;
@property (weak, nonatomic) IBOutlet UIView *oilChangeDetailsView;
@property (weak, nonatomic) IBOutlet UILabel *lblOilChangeDetailsFooter;
@property (weak, nonatomic) IBOutlet UILabel *lblAchievedLabel;

@property (weak, nonatomic) IBOutlet UIView *thisWeekOilChangeView;
@property (weak, nonatomic) IBOutlet UILabel *lblOilChangeThisWeek_Data;
@property (weak, nonatomic) IBOutlet UILabel *lblOilChangeThisWeek_title;

@property (weak, nonatomic) IBOutlet UIView *lastWeekOilChangeView;
@property (weak, nonatomic) IBOutlet UILabel *lblOilChangeLastWeek_Data;
@property (weak, nonatomic) IBOutlet UILabel *lblOilChangeLastWeek_title;

@property (weak, nonatomic) IBOutlet UIImageView *conversationIconImageView;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblConversation;

@property (weak, nonatomic) IBOutlet UILabel *lblSwipeToClose;

@property NSMutableDictionary *oilChangesData;
@property NSMutableDictionary *conversationsData;

@end

@implementation AIHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    self.lblGreetings.font = FONT_B(18);
    self.lblGreetings.textColor = COLOUR_VERYDARKGREY;
    
    self.lblName.font = FONT_H(18);
    self.lblName.textColor = COLOUR_VERYDARKGREY;
    
    self.lblHeader.font = FONT_H(16);
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    self.lblHeader.text = LOCALIZATION(C_AI_WEEKATGLANCE_HEADER);
    
    self.lblSwipeToClose.font = FONT_B1;
    self.lblSwipeToClose.textColor = COLOUR_LIGHTGREY;
    self.lblSwipeToClose.text = LOCALIZATION(C_AI_SWIPETOCLOSE_BTN);
    
    self.lblConversation.font = FONT_B1;
    self.lblConversation.textColor = COLOUR_VERYDARKGREY;
    
    self.lblOilChangeLastWeek_Data.text = @"0";
    self.lblOilChangeLastWeek_Data.textColor = COLOUR_VERYDARKGREY;
    self.lblOilChangeLastWeek_Data.font = FONT_H(24);
    
    self.lblOilChangeThisWeek_Data.text = @"0";
    self.lblOilChangeThisWeek_Data.textColor = COLOUR_RED;
    self.lblOilChangeThisWeek_Data.font = FONT_H(24);
    
    self.lblOilChangeLastWeek_title.text = LOCALIZATION(C_AI_OILCHNAGES_LASTWEEK);
    self.lblOilChangeLastWeek_title.textColor = COLOUR_VERYDARKGREY;
    self.lblOilChangeLastWeek_title.font = FONT_B1;
    
    self.lblOilChangeThisWeek_title.text = LOCALIZATION(C_AI_OILCHANGES_THISWEEK);
    self.lblOilChangeThisWeek_title.textColor = COLOUR_RED;
    self.lblOilChangeThisWeek_title.font = FONT_B1;
    
    self.lblOilChangeDetailsFooter.font = FONT_B1;
    self.lblOilChangeDetailsFooter.textColor = COLOUR_VERYDARKGREY;
    
    [[WebServiceManager sharedInstance] fetchAIOilChangeCount:self];
    [[WebServiceManager sharedInstance] fetchAIConverstaion:self];
    
    //Add swipe gesture if ios < 13
    if(@available(iOS 13, *)) {
        
    } else {
        UISwipeGestureRecognizer *swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRecognizer:)];
        [swipeRecognizer setDirection:UISwipeGestureRecognizerDirectionDown];
        [self.view addGestureRecognizer:swipeRecognizer];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    NSDateComponents * comps = [gregorian components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:[NSDate date]];
    self.lblGreetings.backgroundColor = [UIColor clearColor];
    self.lblName.backgroundColor = [UIColor clearColor];
    if([comps hour] >= 00 && [comps hour] <= 11) {
        self.lblGreetings.text = [NSString stringWithFormat:@"%@,",LOCALIZATION(C_HOME_GREET_MORNING)];
        [self.topImageView setImage: [UIImage imageNamed:@"bg_morning.png"]];
    } else if([comps hour] >= 12 && [comps hour] <= 17) {
        self.lblGreetings.text = [NSString stringWithFormat:@"%@,",LOCALIZATION(C_HOME_GREET_AFTERNOON)];
        [self.topImageView setImage: [UIImage imageNamed:@"bg_afternoon.png"]];
    } else {
        self.lblGreetings.text = [NSString stringWithFormat:@"%@,",LOCALIZATION(C_HOME_GREET_EVENING)];
        [self.topImageView setImage: [UIImage imageNamed:@"bg_evening.png"]];
        self.lblGreetings.textColor = COLOUR_WHITE;
        self.lblName.textColor = COLOUR_WHITE;
    }
    self.lblName.text = [NSString stringWithFormat:@"%@ !",[[mSession profileInfo] fullName]];
   
    if(kIsRightToLeft) {
        [self.lblOilChangeDetailsFooter setTextAlignment:NSTextAlignmentRight];
        [self.lblConversation setTextAlignment: NSTextAlignmentRight];
        [NSLayoutConstraint constraintWithItem:self.rewardImageView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.pieChartView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:(self.pieChartView.frame.size.width - 40)].active = YES;
    } else {
        [self.lblOilChangeDetailsFooter setTextAlignment:NSTextAlignmentLeft];
        [self.lblConversation setTextAlignment: NSTextAlignmentLeft];
        [NSLayoutConstraint constraintWithItem:self.rewardImageView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.pieChartView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:10].active = YES;
    }
}

-(void) setConversationUI {
    
    NSString *messageDescription = [self.conversationsData objectForKey:@"MessageDescription"];
    if([messageDescription isEqualToString:@"Customers"]) {
        [self.conversationIconImageView setImage:[UIImage imageNamed:@"ai-icn_customer.png"]];
    } else if([messageDescription isEqualToString:@"Oil Change"]) {
        [self.conversationIconImageView setImage:[UIImage imageNamed:@"ai-icn_scans.png"]];
    } else if([messageDescription isEqualToString:@"Rewards"]) {
        [self.conversationIconImageView setImage:[UIImage imageNamed:@"ai-icn_promo.png"]];
    } else if([messageDescription isEqualToString:@"Orders"]) {
        [self.conversationIconImageView setImage:[UIImage imageNamed:@"ai-icn_orders.png"]];
    } else {
        [self.conversationIconImageView setImage:[UIImage imageNamed:@"ai-icn_notif.png"]];
    }
    
    
    self.lblConversation.delegate = self;
    [Helper setHyperlinkLabel:self.lblConversation hyperlinkText:[self.conversationsData objectForKey:@"LinkText"] hyperlinkFont:FONT_H1 bodyText:[self.conversationsData objectForKey:@"MessageText"] bodyFont:FONT_B1 urlString:@"linkPressed"];

}

-(void) setOilChangeUI {
    self.lblOilChangeDetailsFooter.text = [NSString stringWithFormat:@"%@",[self.oilChangesData objectForKey:@"PerformanceAnalyzation"]];
    
    self.lblOilChangeThisWeek_Data.text = [NSString stringWithFormat:@"%@",[self.oilChangesData objectForKey:@"CurrentWeekOilChangeCount"]];
    
    self.lblOilChangeLastWeek_Data.text = [NSString stringWithFormat:@"%@",[self.oilChangesData objectForKey:@"LastWeekOilChangeCount"]];
    
    [self constructCircle];
    
    CGFloat percentage = [[self.oilChangesData objectForKey:@"CurrentWeekOilChangeCount"] floatValue] / [[self.oilChangesData objectForKey:@"LastWeekOilChangeCount"] floatValue] * 100.0;
    
    NSMutableAttributedString *textAchieved = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.0f%%",percentage] attributes:@{
                                                        NSFontAttributeName : FONT_H(18) ,
                                                        NSForegroundColorAttributeName : COLOUR_VERYDARKGREY,
                                                    }];
    [textAchieved appendAttributedString: [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@",LOCALIZATION(C_AI_ACHIEVED)] attributes:@{
                                                        NSFontAttributeName : FONT_B1,
                                                        NSForegroundColorAttributeName : COLOUR_VERYDARKGREY,
                                                    }]];
    self.lblAchievedLabel.attributedText = textAchieved;
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([[url absoluteString] isEqualToString: @"linkPressed"]) {
        NSString *linkToModule = [self.conversationsData objectForKey:@"LinkToModule"];
        if([linkToModule isEqualToString:@"Analytics"] || [linkToModule isEqualToString:@"Workshop Performance"] || [linkToModule isEqualToString:@"My Performance"])  {
            [mSession loadINPerformance];
        } else if([linkToModule isEqualToString:@"Register Oil Change"]) {
            if ([[mSession profileInfo] hasRegisterProductCode_V2]) {
                [mSession loadScanProductView];
            }
            if ([[mSession profileInfo] hasLubeMatch]) {
                 [mSession loadLubeMatch];
            }
        } else if([linkToModule isEqualToString:@"Invite Customers"]) {
            if ([[mSession profileInfo] hasInviteConsumer]) {
                [mSession loadRegisterCustomerView: @{@"PromoCode" : @"",
                                                      @"VehicleNumber": @"",
                                                      @"VehicleStateID": @"",
                }];
            }
        } else if([linkToModule isEqualToString:@"Manage Orders"]) {
            if ([[mSession profileInfo] hasOrdering_V2]) {
                 [mSession loadManageOrder];
            }
        } else if([linkToModule isEqualToString:@"Manage Workshops"]) {
            [mSession loadManageWorkshopView];
        } else if([linkToModule isEqualToString:@"Participating Products"]) {
            [mSession loadParticipatingProduct];
        } else if([linkToModule isEqualToString:@"Workshop Offer"]) {
            if ([[mSession profileInfo] hasAncillaryOffers]) {
                [mSession loadWorkshopOfferView];
            }
        } else if([linkToModule isEqualToString:@"Scan Decal"]) {
            if ([[mSession profileInfo] hasDecal]) {
                [mSession loadSearchVehicleViewWithDecal: nil];
            }
        } else if([linkToModule isEqualToString:@"Loyalty Contract"]) {
            [mSession loadLoyaltyContract];
        }
    }
}

#pragma mark - WebServiceManager
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_AI_FETCHCONVERSATIONS: {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                self.conversationsData = [response getGenericResponse];
                if([[self.conversationsData objectForKey:@"MessageText"] isKindOfClass:[NSNull class]]) {
                    self.conversationsData[@"MessageText"] = @"";
                }
                if([[self.conversationsData objectForKey:@"MessageDescription"] isKindOfClass:[NSNull class]]) {
                    self.conversationsData[@"MessageDescription"] = @"";
                }
                if([[self.conversationsData objectForKey:@"LinkText"] isKindOfClass:[NSNull class]]) {
                    self.conversationsData[@"LinkText"] = @"";
                }
                if([[self.conversationsData objectForKey:@"LinkToModule"] isKindOfClass:[NSNull class]]) {
                    self.conversationsData[@"LinkToModule"] = @"";
                }
                [self setConversationUI];
            }
        }
            break;
        case kWWEBSERVICE_AI_FETCHOILCHANGECOUNT :{
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                self.oilChangesData = [response getGenericResponse];
                [self setOilChangeUI];
            }
        }
            break;
        default:
            break;
    }
}

#define START_ANGLE -180
#define degreesToRadians(degrees)((-M_PI * degrees)/180)
-(void) constructCircle {
    
    CGFloat targetValue = [[self.oilChangesData objectForKey:@"LastWeekOilChangeCount"] floatValue] * 6 / 5;
    
    CGFloat lastAngleInRadians = degreesToRadians(START_ANGLE);
    CGFloat targetAngle = 360.0 / targetValue;
    
    CGFloat nextAngleInRadians;
    CGFloat previousValue = 0.0;
    CGFloat nextValue = 0.0;
    NSString *colorName;
    BOOL toBeDrawn = YES;
    
    CGFloat radius = self.pieChartView.frame.size.width / 2;
    
    for(int i = 0 ; i < 3; i ++) {
        switch(i) {
            case 0:
            {
                nextValue = [[self.oilChangesData objectForKey:@"CurrentWeekOilChangeCount"] floatValue];
                colorName = @"#DD1D21";
            }
                break;
            case 1:
            {
                previousValue = [[self.oilChangesData objectForKey:@"CurrentWeekOilChangeCount"] floatValue];
                nextValue = [[self.oilChangesData objectForKey:@"LastWeekOilChangeCount"] floatValue];
                colorName = @"#A6A6A6";
            }
                break;
            case 2: {
                previousValue = [[self.oilChangesData objectForKey:@"LastWeekOilChangeCount"] floatValue];
                nextValue = targetValue;
                colorName = @"#F7F7F7";
            }
                break;
        }
        
        if(previousValue >= nextValue) {
            nextValue = previousValue;
            toBeDrawn = NO;
        } else {
            nextValue -= previousValue;
            toBeDrawn = YES;
        }
        
        if(toBeDrawn) {
            nextAngleInRadians = lastAngleInRadians + (degreesToRadians(targetAngle * nextValue));

            UIBezierPath *bezierPath = [UIBezierPath bezierPath];
            [bezierPath addArcWithCenter: CGPointMake(radius, radius)
                                  radius: radius - BAR_WIDTH
                              startAngle: lastAngleInRadians
                                endAngle: nextAngleInRadians
                               clockwise: NO];

            CAShapeLayer *progressLayer = [[CAShapeLayer alloc] init];
            [progressLayer setPath:bezierPath.CGPath];

            UIColor *color = [Helper colorFromHex: colorName];
            [progressLayer setStrokeColor: [color CGColor]];

            [progressLayer setFillColor:[UIColor clearColor].CGColor];
            [progressLayer setLineWidth: BAR_WIDTH];
            [self.pieChartView.layer addSublayer:progressLayer];

            //get ready for the next arc draw
            lastAngleInRadians = nextAngleInRadians;
        }
    }
    
    if([[self.oilChangesData objectForKey:@"LastWeekOilChangeCount"] intValue] == 0) {
        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
        [bezierPath addArcWithCenter: CGPointMake(radius, radius)
                              radius: radius - BAR_WIDTH
                          startAngle: degreesToRadians(START_ANGLE)
                            endAngle: degreesToRadians(START_ANGLE)
                           clockwise: NO];

        CAShapeLayer *progressLayer = [[CAShapeLayer alloc] init];
        [progressLayer setPath:bezierPath.CGPath];

        UIColor *color = [Helper colorFromHex: @"#F7F7F7"];
        [progressLayer setStrokeColor: [color CGColor]];

        [progressLayer setFillColor:[UIColor clearColor].CGColor];
        [progressLayer setLineWidth: BAR_WIDTH];
        [self.pieChartView.layer addSublayer:progressLayer];
    }
    
    //For Marking
    CGFloat markingAngle = -120;
    
    UILabel *lblMarking = [UILabel new];
    lblMarking.backgroundColor = COLOUR_VERYDARKGREY;
    
    UIView *lblMarkingExtension = [UIView new];
    lblMarkingExtension.backgroundColor = COLOUR_VERYDARKGREY;
    [lblMarking addSubview:lblMarkingExtension];
    lblMarkingExtension.frame = CGRectMake(0, BAR_WIDTH, 1, BAR_WIDTH/2);
    
    int x = radius + (radius - (BAR_WIDTH / 2)) * cos(degreesToRadians(markingAngle));
    int y = radius - (radius - (BAR_WIDTH / 2)) * sin(degreesToRadians(markingAngle));
    lblMarking.frame = CGRectMake(x, y, 1, BAR_WIDTH);
    [lblMarking setCenter:CGPointMake(x, y)];
    lblMarking.transform =  CGAffineTransformRotate(CGAffineTransformIdentity, -degreesToRadians(markingAngle)+M_PI/2);
    
    UILabel *lblMarkingValue = [UILabel new];
    lblMarkingValue.backgroundColor = [UIColor clearColor];
    lblMarkingValue.font = FONT_B3;
    lblMarkingValue.textColor = COLOUR_VERYDARKGREY;
    lblMarkingValue.numberOfLines = 0;
    
    lblMarkingValue.text = [NSString stringWithFormat:@"%@",[self.oilChangesData objectForKey:@"LastWeekOilChangeCount"]];
    
    [lblMarkingValue sizeToFit];
    x = radius + (radius + (BAR_WIDTH) ) * cos(degreesToRadians(markingAngle));
    y = radius - (radius + (BAR_WIDTH) ) * sin(degreesToRadians(markingAngle));
    lblMarkingValue.frame = CGRectMake(x, y, lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
    [lblMarkingValue setCenter:CGPointMake(x, y)];
    
    [self.pieChartView addSubview:lblMarking];
    [self.pieChartView addSubview:lblMarkingValue];
}

#pragma mark - Swipe Gesture
-(void) swipeRecognizer:(UISwipeGestureRecognizer *) swipeRecognizer {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
