//
//  PopupRegisterCustomerViewController.h
//  Shell
//
//  Created by Jeremy Lua on 7/6/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupBaseVC.h"
@protocol PopupRegisterCustomerViewDelegate <NSObject>
-(void)didPressedRegisterCustomer;
@end

@interface PopupRegisterCustomerViewController : PopupBaseVC
@property id<PopupRegisterCustomerViewDelegate> delegate;

@property NSDictionary *registerCustomerDict;
@end
