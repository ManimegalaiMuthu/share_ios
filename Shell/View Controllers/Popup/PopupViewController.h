//
//  x.h
//  Shell
//
//  Created by Jeremy Lua on 9/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupBaseVC.h"

@protocol PopupListDelegate <NSObject>
- (void)dropDownSelection:(NSArray *)selection;
@end

@interface PopupViewController : PopupBaseVC
@property (nonatomic, weak) id<PopupListDelegate> delegate;
@property BOOL isPositionTracked;

-(void) initSingleSelectionListWithTitle:(NSString *) title listArray:(NSArray *)listArray hasSearchField: (BOOL) hasSearchField;
@end
