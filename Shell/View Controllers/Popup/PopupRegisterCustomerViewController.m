//
//  PopupRegisterCustomerViewController.m
//  Shell
//
//  Created by Jeremy Lua on 7/6/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupRegisterCustomerViewController.h"

@interface PopupRegisterCustomerViewController () <UITextFieldDelegate, WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;

@property (weak, nonatomic) IBOutlet UILabel *lblOptionOne;
@property (weak, nonatomic) IBOutlet UIButton *btnRegisterCustomer;


@property (weak, nonatomic) IBOutlet UILabel *lblOr;

@property (weak, nonatomic) IBOutlet UILabel *lblOptionTwo;
@property (weak, nonatomic) IBOutlet UITextField *txtVehicleNum;
@property (weak, nonatomic) IBOutlet UITextField *txtMobileNum;
@property (weak, nonatomic) IBOutlet UIButton *btnInviteSMS;

@property (weak, nonatomic) IBOutlet UIView *stateView;
@property (weak, nonatomic) IBOutlet UIButton *stateBtn;

@property NSArray *stateTable;

@end

@implementation PopupRegisterCustomerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupInterface];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND]) {
        self.stateView.hidden = NO;
        [self.stateBtn.titleLabel setFont:FONT_B1];
        self.txtVehicleNum.text = [self.registerCustomerDict objectForKey:@"VehicleNumber"];
        self.txtMobileNum.text = [self.registerCustomerDict objectForKey:@"MobileNumber"];
        self.btnInviteSMS.alpha = 1.0f;
        
        [[WebServiceManager sharedInstance] loadState: [[mSession lookupTable] objectForKey: @"LookUpVersion"]
                                          countryCode: GET_COUNTRY_CODE
                                                   vc: self];
        
    } else {
        [self.stateView removeFromSuperview];
        [NSLayoutConstraint constraintWithItem:self.txtVehicleNum attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.lblOptionTwo attribute:NSLayoutAttributeBottom multiplier:1.0 constant:20].active = YES;
    }
}

-(void) setupInterface
{
    self.lblHeader.font = FONT_H1;
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    self.lblHeader.text = LOCALIZATION(C_CONSUMER_ONEMORESTEP);         //lokalised
    
    self.lblContent.font = FONT_B1;
    self.lblContent.textColor = COLOUR_VERYDARKGREY;
    self.lblContent.text = LOCALIZATION(C_CONSUMER_ONEMORESTEPCONTENT); //lokalised
    
    self.lblOptionOne.font = FONT_H1;
    self.lblOptionOne.textColor = COLOUR_VERYDARKGREY;
    self.lblOptionOne.text = LOCALIZATION(C_CONSUMER_OPTIONONE);        //lokalised
    
    self.lblOr.font = FONT_B1;
    self.lblOr.textColor = COLOUR_VERYDARKGREY;
    self.lblOr.text = LOCALIZATION(C_CONSUMER_OR);                      //lokalised
    
    self.lblOptionTwo.font = FONT_H1;
    self.lblOptionTwo.textColor = COLOUR_VERYDARKGREY;
    self.lblOptionTwo.text = LOCALIZATION(C_CONSUMER_OPTIONTWO);        //loklaised
    
    self.txtVehicleNum.font = FONT_B1;
    self.txtVehicleNum.textColor = COLOUR_VERYDARKGREY;
    self.txtVehicleNum.placeholder = LOCALIZATION(C_CONSUMER_CUSTOMERVEHICLE_PLACEHOLDER);      //lokalised
    
    self.txtMobileNum.font = FONT_B1;
    self.txtMobileNum.textColor = COLOUR_VERYDARKGREY;
    self.txtMobileNum.placeholder = LOCALIZATION(C_CONSUMER_CUSTOMERMOBILE_PLACEHOLDER);        //lokalised
    
    [self.btnRegisterCustomer.titleLabel setFont: FONT_BUTTON];
    [self.btnRegisterCustomer setTitle:LOCALIZATION(C_CONSUMER_REGISTERCUSTOMER) forState:UIControlStateNormal]; //lokalised
    [self.btnRegisterCustomer setBackgroundColor:COLOUR_RED];
    
    [self.btnInviteSMS.titleLabel setFont: FONT_BUTTON];
    [self.btnInviteSMS setTitle:LOCALIZATION(C_CONSUMER_INVITEVIASMS) forState:UIControlStateNormal];   //lokalised
    [self.btnInviteSMS setBackgroundColor:COLOUR_RED];
    
}

- (IBAction)closeBtnPressed:(id)sender {
    [self cancelButtonAction: nil];
}

- (IBAction)registerCustomerPressed:(id)sender {
    //have to cancel popup
    [self cancelButtonAction: nil];
    
    //push to customer register view
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CustomerRegister" object:self.registerCustomerDict];
}

- (IBAction)editingChanged:(id)sender {
    if ([self.txtMobileNum.text length] > 0 &&
        [self.txtVehicleNum.text length] > 0)
    {
        self.btnInviteSMS.alpha = 1.0f;
    }
    else
    {
        self.btnInviteSMS.alpha = 0.75f;
    }
}

- (IBAction)invitePressed:(id)sender {
    if ([self.txtMobileNum.text length] > 0 &&
        [self.txtVehicleNum.text length] > 0)
    {
        NSDictionary *submitParams = @{@"Name": @"",
                                       @"MobileNumber": self.txtMobileNum.text,
                                       @"EmailAddress": @"",
                                       @"VehicleNumber": self.txtVehicleNum.text,
                                       @"PromoCode": [self.registerCustomerDict objectForKey: @"PromoCode"]
                                       };
        
        [[WebServiceManager sharedInstance] inviteConsumer: submitParams vc:self];
    }
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_INVITECONSUMER:
            {
                [self cancelButtonAction: nil];
                [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion: nil];
                
//                [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
//                    [self.delegate didPressedRegisterCustomer];
//                }];
            }
            break;
        case kWEBSERVICE_STATE:
        {
            SET_LOADSTATE([[response getGenericResponse] objectForKey: @"StateDetailsList"]);
            self.stateTable = [[response getGenericResponse] objectForKey: @"StateDetailsList"];
            
            //if thailand
            NSString *stateID = [self.registerCustomerDict objectForKey: @"VehicleStateID"];
            
            if (stateID.length > 0)
                [self.stateBtn setTitle: [mSession convertToStateName: stateID stateDetailedListArray:self.stateTable] forState:UIControlStateNormal];
            else
                
                [self.stateBtn setTitle: LOCALIZATION(C_PROFILE_STATE_PLACEHOLDER) forState:UIControlStateNormal];  //lokalised
            
        }
            break;

        default:
            break;
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_INVITECONSUMER:
        {
            [self cancelButtonAction: nil];
            if ([response getResponseCode] == 8111)
            {
                [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion: nil];
                
                //                [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
//                                    [self.delegate didPressedRegisterCustomer];
                //                }];
            }
            else if ([response getResponseCode] == 8112)
            {
                [mAlert showSuccessAlertWithTitle: LOCALIZATION(C_GLOBAL_SUCCESS) withMessage: [response getResponseMessage] withCloseButtonTitle: LOCALIZATION(C_CONSUMER_CONTINUE) onCompletion:^(BOOL finished) {
                    [self.delegate didPressedRegisterCustomer];
                }];     //lokalised
            }
        }
            break;
        case kWEBSERVICE_STATE:
            self.stateTable = GET_LOADSTATE;
            break;
        default:
            break;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)stateBtnPressed:(UIButton *)sender {
   
    [self popUpListViewWithTitle: LOCALIZATION(C_PROFILE_STATE_PLACEHOLDER) listArray:[mSession getStateNames: self.stateTable]];       //lokalised
}


-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        RegistrationFormDropdownCell *cell;
        [self.stateBtn setTitle:selection.firstObject forState:UIControlStateNormal];
    }
}
@end
