//
//  PopupBaseVC.m
//  Shell
//
//  Created by Jeremy Lua on 24/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupBaseVC.h"

@interface PopupBaseVC ()
@property (weak, nonatomic) IBOutlet UIView *blackView; //background

@end

@implementation PopupBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan) {
        if ((self.keyboardFrame.origin.y == 0 || self.keyboardFrame.origin.y == SCREEN_HEIGHT) &&
            touch.view == self.blackView)
            [self cancelButtonAction: nil];
        else
            [self.view endEditing: YES];
            
    }
}

- (IBAction)cancelButtonAction:(id)sender {
    
    //dismiss
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
