//
//  PopupSelectMultipleViewController.m
//  Shell
//
//  Created by Nach on 19/9/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "PopupSelectMultipleViewController.h"
#import "CASCommonListCell.h"

@interface PopupSelectMultipleViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *searchTxtField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;

@property (nonatomic, strong) NSMutableArray *selectedListArray;

@property (nonatomic, strong) NSMutableArray *tableList;
@property (nonatomic, strong) NSMutableArray *resultArray;
@property (nonatomic, strong) NSMutableArray *responseData;
@property (nonatomic, strong) NSArray *ctoArray;
@property (nonatomic, assign) BOOL myOneplusOneBool;

@property (nonatomic, strong) WebServiceResponse *response;

@property NSMutableArray *defaultListArray;

@end

@implementation PopupSelectMultipleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.lblTitle.font = FONT_H1;
    self.lblTitle.textColor = COLOUR_VERYDARKGREY;
    
    self.tableView.tableFooterView = [UIView new];
    
    self.searchTxtField.font = FONT_B1;
    self.searchTxtField.delegate = self;
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [self.searchTxtField setLeftViewMode:UITextFieldViewModeAlways];
    [self.searchTxtField setLeftView:spacerView];
    
    self.searchTxtField.placeholder = LOCALIZATION(C_RU_REGISTRATION_WORKSHOPSELECT_PLACEHOLDER);
    
    [self.btnAdd setTitle:LOCALIZATION(C_ORDER_ADD) forState:UIControlStateNormal];
    [self.btnAdd setBackgroundColor:COLOUR_RED];
    [self.btnAdd.titleLabel setFont:FONT_BUTTON];
    //[[WebServiceManager sharedInstance] fetchCTODetailsFor:@"" onVc:self];
    self.resultArray = [[NSMutableArray alloc] init];
    self.selectedListArray = [[NSMutableArray alloc] init];
}

-(void) initMultipleSelectionListWithTitle:(NSString *) title listArray:(NSArray *)listArray hasSearchField: (BOOL) hasSearchField withPreselect:(NSArray *)selectedArray isSelectVc:(BOOL)isVC
{
    if (isVC){
        NSLog(@"true");
        [[WebServiceManager sharedInstance] fetchCTODetailsOnePlusOneFor:@"" onVc:self];
        self.myOneplusOneBool = YES;
    }else{
        NSLog(@"false");
        [[WebServiceManager sharedInstance] fetchCTODetailsFor:@"" onVc:self];
        self.myOneplusOneBool = NO;
    }
    self.lblTitle.text = title;
    self.tableList = [NSMutableArray arrayWithArray:listArray];
    self.defaultListArray = [NSMutableArray arrayWithArray:listArray];
    self.selectedListArray = [NSMutableArray arrayWithArray:selectedArray];
    
    [self setHasSearchField: hasSearchField]; //pass the same parameter in.
    
    [self.view layoutIfNeeded];
}

-(void) setHasSearchField:(BOOL) hasSearchField
{
    self.searchTxtField.hidden = !hasSearchField;
    if(self.searchTxtField.isHidden) {
        [NSLayoutConstraint constraintWithItem:self.searchTxtField attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
    } else{
        [NSLayoutConstraint constraintWithItem:self.searchTxtField attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:30].active = YES;
    }
}

-(void) setList:(NSArray *)list
{
    self.tableList = [NSMutableArray arrayWithArray:list];
    self.defaultListArray = [NSMutableArray arrayWithArray:list];
    [self.tableView reloadData];
    [self viewDidLayoutSubviews];
}

-(BOOL) selectedListContains:(NSString *) nameConsidered {
    for (NSString *element in self.selectedListArray) {
        if([nameConsidered isEqualToString:element]) {
            return YES;
        }
    }
    return NO;
}

-(void) selectWorkshop:(UIButton *)sender
{
    UITableViewCell *cell = (UITableViewCell *) sender.superview.superview;
    UIButton *btnCheck = [cell viewWithTag:1000];
    UILabel *lblName = [cell viewWithTag:1001];
    UIImageView *selectedImage = [cell viewWithTag:1003];
    if(btnCheck.isSelected) {
        btnCheck.selected = NO;
        selectedImage.hidden = YES;
        [self.selectedListArray removeObject:lblName.text];
    } else {
        btnCheck.selected = YES;
        selectedImage.hidden = NO;
        [self.selectedListArray addObject:lblName.text];
    }
    
    NSLog(@"selected array:%@",self.selectedListArray);
    
    [self.tableView reloadData];
}

- (IBAction)addPressed:(UIButton *)sender {
    NSMutableArray *selectedCompleteDataArray = [[NSMutableArray alloc]init];
    for(NSDictionary *dict in self.responseData) {
        if([self.selectedListArray containsObject:[dict objectForKey:@"CompanyNameWithReference"]]) {
            [selectedCompleteDataArray addObject:dict];
        }
    }
    
    NSLog(@"selected aded array:%@",selectedCompleteDataArray);
    [self.delegate dropDownMultipleSelection:self.selectedListArray withFullData:selectedCompleteDataArray];
    
    //dismiss
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (IBAction)dismissGesture:(UITapGestureRecognizer *)sender {
    //dismiss
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}


#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tableList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MultipleSelectionTableViewCell" forIndexPath:indexPath];
    
    UILabel *lblName = [cell viewWithTag:1001];
    lblName.textColor = COLOUR_VERYDARKGREY;
    lblName.font = FONT_B1;
    lblName.text = [NSString stringWithFormat:@"%@",[self.tableList objectAtIndex:indexPath.row]];
    
    UIButton *btnCheck = [cell viewWithTag:1000];
    [btnCheck addTarget:self action: @selector(selectWorkshop:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *selectedImage = [cell viewWithTag:1003];
    
    if([self.selectedListArray count] == 0) {
        btnCheck.selected = NO;
        selectedImage.hidden = YES;
    } else if([self selectedListContains:lblName.text]) {
        selectedImage.hidden = NO;
        btnCheck.selected = YES;
    } else {
        btnCheck.selected = NO;
        selectedImage.hidden = YES;
    }
    return cell;
}

#pragma mark - TextViewDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([self.defaultListArray count] > 0) {
        NSRange textFieldRange = NSMakeRange(0, [textField.text length]);
        if (NSEqualRanges(range, textFieldRange) && [string length] == 0)
        {
            self.tableList = [NSMutableArray arrayWithArray: self.defaultListArray];
        }
        else
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat: @"SELF CONTAINS[cd] %@", [NSString stringWithFormat: @"%@%@", self.searchTxtField.text, string]];
            
            self.tableList = [self.defaultListArray filteredArrayUsingPredicate:predicate];
            
        }
        self.resultArray = self.tableList;
        [self.tableView reloadData];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if([textField.text length] >= 1) {
        if (self.myOneplusOneBool) {
            [[WebServiceManager sharedInstance] fetchCTODetailsOnePlusOneFor:textField.text onVc:self];
        }else{
            [[WebServiceManager sharedInstance] fetchCTODetailsFor:textField.text onVc:self];
        }
        
    }
}

#pragma mark - WebService
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_FETCHCTO:
            self.ctoArray = [response getGenericResponse];
            if(self.responseData == nil || [self.responseData count] == 0) {
                self.responseData = self.ctoArray;
            } else {
                [self.responseData addObjectsFromArray: self.ctoArray];
            }
            
            [self.defaultListArray removeAllObjects];
            
            for (NSDictionary *dict in self.ctoArray) {
                [self.defaultListArray addObject: [dict objectForKey:@"CompanyNameWithReference"]];
            }
            
            self.tableList = self.defaultListArray;
            [self.tableView reloadData];
            break;
            
        case kWEBSERVICE_FETCHCTO_ONEPLUSONE:
        self.ctoArray = [response getGenericResponse];
        if(self.responseData == nil || [self.responseData count] == 0) {
            self.responseData = self.ctoArray;
        } else {
            [self.responseData addObjectsFromArray: self.ctoArray];
        }
        
        [self.defaultListArray removeAllObjects];
        
        for (NSDictionary *dict in self.ctoArray) {
            [self.defaultListArray addObject: [dict objectForKey:@"CompanyNameWithReference"]];
        }
        
        self.tableList = self.defaultListArray;
            NSLog(@"Default list array:%@",self.defaultListArray);
        [self.tableView reloadData];
        break;
        default:
            break;
    }
}

@end
