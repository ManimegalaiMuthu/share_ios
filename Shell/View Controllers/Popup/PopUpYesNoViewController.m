//
//  PopUpYesNoViewController.m
//  Shell
//
//  Created by Jeremy Lua on 15/5/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopUpYesNoViewController.h"

@interface PopUpYesNoViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UIButton *noBtn;
@property (weak, nonatomic) IBOutlet UIButton *yesBtn;
@property CompletionBlock yesBlock;
@property CompletionBlock noBlock;
@end

@implementation PopUpYesNoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.lblHeader.font = FONT_H1;
    self.lblContent.font = FONT_B1;
    
    [self.yesBtn.titleLabel setFont: FONT_BUTTON];
    [self.noBtn.titleLabel setFont: FONT_BUTTON];
    
    [self.yesBtn setBackgroundColor:COLOUR_RED];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(id) init
{
    self = [super init];
    self.yesBtn.titleLabel.adjustsFontSizeToFitWidth = true;
    self.yesBtn.titleLabel.numberOfLines = 1;
    self.yesBtn.titleLabel.lineBreakMode = NSLineBreakByClipping;
    self.noBtn.titleLabel.adjustsFontSizeToFitWidth = true;
    self.noBtn.titleLabel.numberOfLines = 1;
    self.noBtn.titleLabel.lineBreakMode = NSLineBreakByClipping;
    return self;
}

-(id) initWithTitle:(NSString *)title content:(NSString *)contentString yesBtnTitle:(NSString *)yesBtnTitle noBtnTitle:(NSString *)noBtnTitle
{
    self = [self init];
    self.lblHeader.text = title;
    
    self.lblContent.text = contentString;
    
    [self.yesBtn setTitle: yesBtnTitle forState: UIControlStateNormal];
    [self.noBtn setTitle: noBtnTitle forState: UIControlStateNormal];
    return self;
}
-(id) initWithTitle:(NSString *)title content:(NSString *)contentString yesBtnTitle:(NSString *)yesBtnTitle noBtnTitle:(NSString *)noBtnTitle onYesPressed:(CompletionBlock)yesBlock onNoPressed:(CompletionBlock)noBlock
{
    self = [self initWithTitle:title content:contentString yesBtnTitle:yesBtnTitle noBtnTitle:noBtnTitle];
    
    self.yesBlock = yesBlock;
    self.noBlock = noBlock;
    return self;
}
-(id) initWithAttributedTitle:(NSAttributedString*)title content:(NSAttributedString *)contentString yesBtnTitle:(NSAttributedString *)yesBtnTitle noBtnTitle:(NSAttributedString *)noBtnTitle
{
    self = [self init];
    self.lblHeader.attributedText = title;
    self.lblContent.attributedText = contentString;
    
    [self.yesBtn setAttributedTitle: yesBtnTitle forState: UIControlStateNormal];
    [self.noBtn setAttributedTitle: noBtnTitle forState: UIControlStateNormal];
    return self;
}
-(id) initWithAttributedTitle:(NSAttributedString *)title content:(NSAttributedString *)contentString yesBtnTitle:(NSAttributedString *)yesBtnTitle noBtnTitle:(NSAttributedString *)noBtnTitle onYesPressed:(CompletionBlock)yesBlock onNoPressed:(CompletionBlock)noBlock
{
    self = [self initWithAttributedTitle:title content:contentString yesBtnTitle:yesBtnTitle noBtnTitle:noBtnTitle];
    
    self.yesBlock = yesBlock;
    self.noBlock = noBlock;
    return self;
}

- (IBAction)yesPressed:(id)sender {
//    [self.delegate yesPressed];
    self.yesBlock(YES);
    self.yesBlock = nil;
    self.noBlock = nil;
    
    //dismiss
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (IBAction)noPressed:(id)sender {
//    [self.delegate noPressed];
    
    self.noBlock(YES);
    self.yesBlock = nil;
    self.noBlock = nil;
    
    //dismiss
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
