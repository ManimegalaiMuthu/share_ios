//
//  PopupViewController.m
//  Shell
//
//  Created by Jeremy Lua on 9/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupViewController.h"
#import "Constants.h"
#import "LocalizationManager.h"
#import "CASCommonListCell.h"

//#define COMMON_LIST_VIEW @"CASCommonListView"
#define COMMON_LIST_CELL @"CASCommonListCell"

#define CELL_HEIGHT 55

@interface PopupViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, CASCommonListCellDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *tableList;
@property (nonatomic, strong) NSMutableArray *resultArray;


@property (weak, nonatomic) IBOutlet UITextField *searchTxtField;
@property NSMutableArray *defaultListArray;

@end

@implementation PopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.lblHeader.font = FONT_H1;
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    
    self.tableView.tableFooterView = [UIView new];
    
    self.doneButton.hidden = YES;
    self.cancelButton.hidden = YES;

    self.searchTxtField.font = FONT_B1;

    self.searchTxtField.placeholder = LOCALIZATION(C_DROPDOWN_SEARCHPLACEHOLDER);       //lokalised
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = CELL_HEIGHT;
    
    self.resultArray = [[NSMutableArray alloc] init];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
}

-(void) initSingleSelectionListWithTitle:(NSString *) title listArray:(NSArray *)listArray hasSearchField: (BOOL) hasSearchField
{
    self.lblHeader.text = title;
//    self.lblHeader.accessibilityLabel = @"Accessibility Label"; //replace label
//    self.lblHeader.accessibilityValue = @"Accessibility Value"; //2nd voice
    self.lblHeader.accessibilityValue = LOCALIZATION(@"Dropdown Menu Header");
//    self.lblHeader.accessibilityHint = LOCALIZATION(@"Dropdown Menu Header"); //3rd voice
    
    self.tableList = [NSMutableArray arrayWithArray:listArray];
    self.defaultListArray = [NSMutableArray arrayWithArray:listArray];

    [self setHasSearchField: hasSearchField]; //pass the same parameter in.
    
    self.tableViewHeightConstraint.constant = MIN(SCREEN_HEIGHT - 178, CELL_HEIGHT * [listArray count]);
    
    [self.view layoutIfNeeded];
    UIAccessibilityPostNotification(UIAccessibilityScreenChangedNotification, self.lblHeader);
}

-(void) setHasSearchField:(BOOL) hasSearchField
{
    self.searchTxtField.hidden = !hasSearchField;
    if(self.searchTxtField.isHidden) {
        [NSLayoutConstraint constraintWithItem:self.searchTxtField attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
    } else{
        [NSLayoutConstraint constraintWithItem:self.searchTxtField attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:30].active = YES;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tableList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CASCommonListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"listItem"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:COMMON_LIST_CELL owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    
    [cell setTableText:self.tableList[indexPath.row]];
    
    cell.accessibilityLabel = self.tableList[indexPath.row];
    cell.isAccessibilityElement = YES;
    cell.delegate = self;
    cell.tag = indexPath.row + 1;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.resultArray removeAllObjects];
    if(self.isPositionRequired) {
        if([self.tableList count] != [self.defaultListArray count]) {
            int defaultCount = 0; //For position in default list array
            NSString *selectedText =  [self.tableList objectAtIndex:indexPath.row];
            for(NSString *rowText in self.defaultListArray) {
                if([selectedText isEqualToString:rowText]) {
                    [self.resultArray addObject:@{@"Name" : selectedText,
                                                 @"IndexPath"  : [NSIndexPath indexPathForRow:defaultCount inSection:indexPath.section],
                                                 }
                    ];
                }
                defaultCount += 1;
            }
            
            if([self.resultArray count] > 1) {
                NSMutableArray *theExactResultArray = [[NSMutableArray alloc] init];
                int tableCount = 0;
                int positionedAt = -1;
                for(NSString *rowText in self.tableList) {
                    if([rowText isEqualToString:selectedText]) {
                        positionedAt += 1;
                        if(tableCount == indexPath.row) {
                            [theExactResultArray addObject:[self.resultArray objectAtIndex:positionedAt]];
                            [self.resultArray removeAllObjects];
                            self.resultArray = theExactResultArray;
                        }
                    }
                    tableCount += 1;
                }
            }
            
        } else {
            [self.resultArray addObject:@{@"Name" : [self.tableList objectAtIndex:indexPath.row],
                                         @"IndexPath"  : indexPath,
                                         }
            ];
        }
    } else {
         [self.resultArray addObject:[self.tableList objectAtIndex:indexPath.row]];
    }
    [self doneButtonAction:nil];
}

- (IBAction)doneButtonAction:(id)sender
{
    [self.delegate dropDownSelection: self.resultArray];
    
    //dismiss
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

-(void) setList:(NSArray *)list
{
    self.tableList = [NSMutableArray arrayWithArray:list];
    self.defaultListArray = [NSMutableArray arrayWithArray:list];
    [self.tableView reloadData];
    [self viewDidLayoutSubviews];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSRange textFieldRange = NSMakeRange(0, [textField.text length]);
    if (NSEqualRanges(range, textFieldRange) && [string length] == 0)
    {
        self.tableList = [NSMutableArray arrayWithArray: self.defaultListArray];
    }
    else
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat: @"SELF CONTAINS[cd] %@", [NSString stringWithFormat: @"%@%@", self.searchTxtField.text, string]];
        
        self.tableList = [self.defaultListArray filteredArrayUsingPredicate:predicate];
        
    }
    [self.tableView reloadData];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
