//
//  PopupPrivacyPolicyViewController.m
//  Shell
//
//  Created by Jeremy Lua on 2/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupPrivacyPolicyViewController.h"
#import <WebKit/WebKit.h>

@interface PopupPrivacyPolicyViewController () <UIScrollViewDelegate, WebServiceManagerDelegate,WKNavigationDelegate>

@property (weak, nonatomic) IBOutlet UIView *webViewContainer;

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UIButton *okBtn;

@property (weak, nonatomic) IBOutlet UIButton *checkboxBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblAgree;

@property BOOL haveScrolledToEnd;
@end

@implementation PopupPrivacyPolicyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.lblHeader.font = FONT_H1;
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    
    if ([GET_LOCALIZATION isEqualToString: kRussian])
        self.lblHeader.text = LOCALIZATION(C_SIDEMENU_TNC);     //lokalised
    else
        self.lblHeader.text = LOCALIZATION(C_TITLE_PRIVACY);    //lokalised
    
    [self.okBtn.titleLabel setFont: FONT_BUTTON];
    [self.okBtn setBackgroundColor:COLOUR_RED];
    [self.okBtn setTitle:LOCALIZATION(C_RUSSIA_CONFIRM) forState:UIControlStateNormal];     //lokalised
    self.okBtn.enabled = NO;    //disable until scroll reach end.
    self.okBtn.alpha = 0.5f;
    
    [self.registrationDict setObject: @(2) forKey: @"Step"];
    
    self.lblAgree.text = LOCALIZATION(C_PROFILE_AGREE);         //lokalised
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];

#pragma mark - Adding WKWebView
    NSString *urlAddress = [NSString stringWithFormat:@"%@%@%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_RU_PRIVACYPOPUP_URL];

    WebViewHelper *webViewHelper = [[WebViewHelper alloc] init];
    [webViewHelper setWebViewWithParentView: self.webViewContainer
                         navigationDelegate: self
                                 urlAddress: urlAddress];
    
    webViewHelper.webView.scrollView.delegate = self;
//    NSDictionary *data = GET_CONSUMER_TNC;
//    NSString *htmlString = [data objectForKey: @"TncText"];
//
//    if (![htmlString isKindOfClass: [NSNull class]])
//    {
//        htmlString = [NSString stringWithFormat:@"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>%@", htmlString];
//    }
//    else
//    {
//        htmlString = @"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>Privacy policy not loaded";
//    }
//
//    WebViewHelper *webViewHelper = [[WebViewHelper alloc] init];
//    [webViewHelper setWebViewWithParentView: self.webViewContainer
//                         navigationDelegate: self
//                                   htmlBody: htmlString];
//    webViewHelper.webView.scrollView.delegate = self;
//    [webViewHelper setWebConfiguration: WKDataDetectorTypeLink | WKDataDetectorTypePhoneNumber];  //default
}


-(void) scrollViewDidScroll:(UIScrollView *)scrollView
{
//    if (self.contentScrollView.contentOffset.y + self.contentScrollView.frame.size.height == self.contentScrollView.contentSize.height && !self.haveScrolledToEnd)
    if (scrollView.contentOffset.y + scrollView.frame.size.height == scrollView.contentSize.height &&
        !self.haveScrolledToEnd)
    {
        self.haveScrolledToEnd = YES;
        
        if (self.checkboxBtn.selected)
        {
            self.okBtn.enabled = YES;
            self.okBtn.alpha = 1.0f;
        }
    }
}

- (IBAction)checkBoxSelected:(UIButton *)sender {
    if ([sender isSelected])
        sender.selected = NO;
    else
        sender.selected = YES;
    
    if (self.haveScrolledToEnd && self.checkboxBtn.selected)
    {
        self.okBtn.enabled = YES;
        self.okBtn.alpha = 1.0f;
    }
    else
    {
        self.okBtn.enabled = NO;
        self.okBtn.alpha = 0.5f;
    }
}

- (IBAction)confirmPressed:(id)sender
{
//    [self.registrationDict setObject: @(self.checkboxBtn.selected) forKey: @"IsAcceptTnC"];
    
    [self.delegate didAgreePrivacyPolicy: @(self.checkboxBtn.selected)];
    
    [self cancelButtonAction: nil];
}

#pragma WKWebView delegate methods
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    [UpdateHUD addMBProgress: webView withText:LOADING_HUD];
}

-(void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    NSLog(@"failed to load navigation");
    [UpdateHUD removeMBProgress: self.view];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [UpdateHUD removeMBProgress: webView];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    if(navigationAction.navigationType ==  WKNavigationTypeLinkActivated) {
        [[UIApplication sharedApplication] openURL: navigationAction.request.URL];
        
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
    return;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
