//
//  PopupTextViewController.h
//  Shell
//
//  Created by Ben on 28/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupBaseVC.h"

@interface PopupTextViewController : PopupBaseVC

-(void)initWithTitle:(NSString*)title yesButtonTitle:(NSString*) yesTitle noButtonTitle:(NSString*) noTitle placeHolder:(NSString*)placeHolder completionHandler:(void(^)(BOOL isYes,NSString* remarks)) completionHandler;
-(void)initWithTitle:(NSString*)title yesButtonTitle:(NSString*) yesTitle noButtonTitle:(NSString*) noTitle placeHolder:(NSString*)placeHolder;

@end
