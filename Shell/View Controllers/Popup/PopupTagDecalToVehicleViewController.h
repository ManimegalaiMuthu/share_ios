//
//  PopupTagDecalToVehicleViewController.h
//  Shell
//
//  Created by Jeremy Lua on 30/5/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupBaseVC.h"
@protocol PopupTagDecalToVehicleDelegate<NSObject>
-(void) didTagVehicle;
@end

@interface PopupTagDecalToVehicleViewController : PopupBaseVC
@property id<PopupTagDecalToVehicleDelegate> delegate;

@property NSString *vehicleId;
@property NSInteger stateId;

@end
