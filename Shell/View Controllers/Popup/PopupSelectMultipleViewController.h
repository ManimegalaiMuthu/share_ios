//
//  PopupSelectMultipleViewController.h
//  Shell
//
//  Created by Nach on 19/9/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "PopupBaseVC.h"

@protocol PopupSelectMultipleDelegate <NSObject>
- (void)dropDownMultipleSelection:(NSArray *)selection withFullData:(NSArray *)completeDataArray;
@end

NS_ASSUME_NONNULL_BEGIN

@interface PopupSelectMultipleViewController : PopupBaseVC


@property (nonatomic, weak) id<PopupSelectMultipleDelegate> delegate;
-(void) initMultipleSelectionListWithTitle:(NSString *) title listArray:(NSArray *)listArray hasSearchField: (BOOL) hasSearchField withPreselect:(NSArray *)selectedArray isSelectVc:(BOOL)isVC;

@end

NS_ASSUME_NONNULL_END
