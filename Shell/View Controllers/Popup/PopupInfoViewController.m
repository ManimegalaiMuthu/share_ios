//
//  PopupInfoViewController.m
//  Shell
//
//  Created by Jeremy Lua on 24/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupInfoViewController.h"

@interface PopupInfoViewController () <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UIScrollView *contentScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentHeight;

@property (weak, nonatomic) IBOutlet UIButton *okBtn;
@property CompletionBlock completionBlock;
@end

@implementation PopupInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.lblHeader.font = FONT_H1;
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    
    self.lblContent.font = FONT_B1;
    self.lblContent.textColor = COLOUR_VERYDARKGREY;
    
    [self.okBtn.titleLabel setFont: FONT_BUTTON];
    [self.okBtn setTitle:LOCALIZATION(C_GLOBAL_OK) forState:UIControlStateNormal];      //lokalised
    [self.okBtn setBackgroundColor:COLOUR_RED];
}

-(void) initPopupInfoVCWithTitle:(NSString *)title content:(NSString *)content
{
    self.lblHeader.text = title;
    self.lblContent.text = content;
    
}

-(void) initPopupInfoVCWithTitle:(NSString *)title content:(NSString *)content btnTitle:(NSString *)btnTitle onYesPressed:(CompletionBlock)yesBlock
{
    self.lblHeader.text = title;
    self.lblContent.text = content;
    
    [self.okBtn setTitle:btnTitle forState:UIControlStateNormal];
    self.completionBlock = yesBlock;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    CGSize size = [self.lblContent.text sizeWithFont:self.lblContent.font
                      constrainedToSize:CGSizeMake(self.contentScrollView.frame.size.width, MAXFLOAT)
                          lineBreakMode:UILineBreakModeWordWrap];
    self.lblContent.frame = CGRectMake(0, 0, size.width, size.height);
    self.contentScrollView.contentSize = size;
}

-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    return;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btnAction:(UIButton *)sender {
    if(self.completionBlock)
        self.completionBlock(YES);
    
    self.completionBlock = nil;
    //dismiss
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

@end
