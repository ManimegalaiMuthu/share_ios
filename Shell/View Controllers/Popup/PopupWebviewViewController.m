//
//  PopupWebviewViewController.m
//  Shell
//
//  Created by Jeremy Lua on 24/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupWebviewViewController.h"
#import <WebKit/WebKit.h>

@interface PopupWebviewViewController () <WKNavigationDelegate>

@property (weak, nonatomic) IBOutlet UIView *webViewContainer;

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UIButton *okBtn;

@property NSString *htmlBody;
@property NSString *urlString;
@property BOOL isURL;

@end

@implementation PopupWebviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.lblHeader.font = FONT_H1;
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    
    [self.okBtn.titleLabel setFont: FONT_BUTTON];
    [self.okBtn setBackgroundColor:COLOUR_RED];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if(self.isURL) {
        #pragma mark - Adding WKWebView
        WebViewHelper *webViewHelper = [[WebViewHelper alloc] init];
        [webViewHelper setWebViewWithParentView: self.webViewContainer
                             navigationDelegate: self
                                     urlAddress: self.urlString];
        [webViewHelper setWebConfiguration: WKDataDetectorTypeLink];
    } else {
        #pragma mark - Adding WKWebView
        WebViewHelper *webViewHelper = [[WebViewHelper alloc] init];
        [webViewHelper setWebViewWithParentView: self.webViewContainer
                             navigationDelegate: self
                                       htmlBody: self.htmlBody];
        [webViewHelper setWebConfiguration: WKDataDetectorTypeLink];
    }
}

-(void) initWithHTMLBody: (NSString *) htmlBody headerTitle: (NSString *)headerTitle buttonTitle: (NSString *) buttonTitle
{
    if (![htmlBody isKindOfClass: [NSNull class]])
    {
        self.htmlBody = [NSString stringWithFormat:@"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>%@", htmlBody];
    }
    else
    {
        self.htmlBody = @"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>Not loaded";
    }
    
    self.lblHeader.text = headerTitle;
    
    [self.okBtn setTitle: buttonTitle forState:UIControlStateNormal];
    
    self.isURL = NO;
}

-(void) initWithUrl: (NSString *) urlString headerTitle: (NSString *)headerTitle buttonTitle: (NSString *) buttonTitle
{
    self.urlString = urlString;
    self.lblHeader.text = headerTitle;
    if([headerTitle isEqualToString:@""]) {
        [NSLayoutConstraint constraintWithItem:self.lblHeader attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
    }
    
    self.isURL = YES;
    [self.okBtn setTitle: buttonTitle forState:UIControlStateNormal];
}

- (IBAction)confirmPressed:(id)sender
{    
    [self cancelButtonAction: nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
