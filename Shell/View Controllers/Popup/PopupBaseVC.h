//
//  PopupBaseVC.h
//  Shell
//
//  Created by Jeremy Lua on 24/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface PopupBaseVC : BaseVC

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
- (IBAction)cancelButtonAction:(id)sender;
@end

