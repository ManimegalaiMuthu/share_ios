//
//  PopupRewardFilterViewController.h
//  Shell
//
//  Created by Jeremy Lua on 4/7/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupBaseVC.h"
@protocol PopupRewardFilterDelegate <NSObject>
-(void)didFinishRewardFilter:(NSMutableDictionary *) rewardFilter;
-(void)didPressCheckbox:(NSMutableDictionary *) rewardFilter;
@end

@interface PopupRewardFilterViewController : PopupBaseVC
@property id<PopupRewardFilterDelegate> delegate;
-(void) initFilterRewardPopup:(NSString *)title leftValue:(NSInteger)leftValue rightValue:(NSInteger)rightValue isChecked:(BOOL)isChecked;
@end
