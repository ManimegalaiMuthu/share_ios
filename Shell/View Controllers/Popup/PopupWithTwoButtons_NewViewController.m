//
//  PopupWithTwoButtons_NewViewController.m
//  Shell
//
//  Created by Nach on 2/1/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "PopupWithTwoButtons_NewViewController.h"
#import <WebKit/WebKit.h>

@interface PopupWithTwoButtons_NewViewController () <WKNavigationDelegate>

@property (weak, nonatomic) IBOutlet UIView *webViewContainer;
@property (weak, nonatomic) IBOutlet UIButton *btnLater;
@property (weak, nonatomic) IBOutlet UIButton *btnAction;

@property CompletionBlock laterBlock;
@property CompletionBlock actionBlock;

@property NSString *urlString;

@end

@implementation PopupWithTwoButtons_NewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.btnLater setBackgroundColor:COLOUR_WHITE];
    self.btnLater.layer.borderColor = [COLOUR_RED CGColor];
    self.btnLater.layer.borderWidth = 1.0;
    [self.btnLater setTitleColor:COLOUR_RED forState:UIControlStateNormal];
    self.btnLater.titleLabel.font = FONT_H1;
    
    [self.btnAction setBackgroundColor:COLOUR_RED];
    [self.btnAction.titleLabel setTextColor:COLOUR_WHITE];
    [self.btnAction setTitleColor:COLOUR_WHITE forState:UIControlStateNormal];
    self.btnAction.titleLabel.font = FONT_H1;
}

-(void) initWithUrl:(NSString *)urlString withLaterBTnTitle:(NSString *)laterBtnTitle andActionTitle:(NSString *)actionBtnTitle onLaterPressed:(CompletionBlock)laterBlock onActionPressed:(CompletionBlock)actionBlock {
    
    [self.btnLater setTitle:laterBtnTitle forState:UIControlStateNormal];
    [self.btnAction setTitle:actionBtnTitle forState:UIControlStateNormal];
    
    self.laterBlock = laterBlock;
    self.actionBlock = actionBlock;
    
    self.urlString = urlString;
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    #pragma mark - Adding WKWebView
    WebViewHelper *webViewHelper = [[WebViewHelper alloc] init];
    [webViewHelper setWebViewWithParentView: self.webViewContainer
                                   navigationDelegate: self
                                           urlAddress: self.urlString];
}


- (IBAction)laterPressed:(UIButton *)sender {
    
    self.laterBlock = nil;
    self.actionBlock = nil;
    
    //dismiss
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    
}

- (IBAction)actionPressed:(UIButton *)sender {
    
    self.actionBlock(YES);
    self.actionBlock = nil;
    self.laterBlock = nil;
    
    //dismiss
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    
}


#pragma WKWebView delegate methods
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    [UpdateHUD addMBProgress: webView withText:LOADING_HUD];
}

-(void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    NSLog(@"failed to load navigation");
    [UpdateHUD removeMBProgress: self.view];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [UpdateHUD removeMBProgress: webView];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
