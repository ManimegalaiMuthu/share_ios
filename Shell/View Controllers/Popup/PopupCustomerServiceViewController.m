//
//  :opupCustomerServiceViewController.m
//  Shell
//
//  Created by Nach on 23/9/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "PopupCustomerServiceViewController.h"

@interface PopupCustomerServiceViewController () <UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;

@end

@implementation PopupCustomerServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.lblTitle.font = FONT_H1;
    self.lblTitle.textColor = COLOUR_VERYDARKGREY;
    
    [self.btnOk setTitle:LOCALIZATION(C_BUTTON_OK) forState:UIControlStateNormal];
    [self.btnOk setBackgroundColor:COLOUR_RED];
    [self.btnOk.titleLabel setFont:FONT_BUTTON];
}

- (IBAction)okPressed:(UIButton *)sender {
     [self cancelButtonAction: nil];
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CallMultipleSelectionTableViewCell" forIndexPath:indexPath];
    
    UILabel *lblName = [cell viewWithTag:1001];
    lblName.textColor = COLOUR_VERYDARKGREY;
    lblName.font = FONT_B1;
    
    if(indexPath.row == 0) {
        lblName.text = [NSString stringWithFormat: @"%@ %@",LOCALIZATION(C_POPUP_B2B_SUPPORT),[[mSession lookupTable] objectForKey: @"ContactUSB2BCSNumber"]];
    } else {
        lblName.text = [NSString stringWithFormat: @"%@ %@",LOCALIZATION(C_POPUP_B2C_SUPPORT),[[mSession lookupTable] objectForKey: @"ContactUSB2CCSNumber"]];
    }
   
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *csNumberString ;
    if(indexPath.row == 0) {
       csNumberString =  [[mSession lookupTable] objectForKey: @"ContactUSB2BCSNumber"];
    } else {
        csNumberString =  [[mSession lookupTable] objectForKey: @"ContactUSB2CCSNumber"];
    }
    NSString *callUrlString = [NSString stringWithFormat: @"tel:%@",
                               [csNumberString stringByReplacingOccurrencesOfString: @" " withString: @""]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: callUrlString]];
    [self cancelButtonAction: nil];
}

@end
