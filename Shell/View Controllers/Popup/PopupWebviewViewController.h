//
//  PopupWebviewViewController.h
//  Shell
//
//  Created by Jeremy Lua on 24/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupBaseVC.h"

@interface PopupWebviewViewController : PopupBaseVC
-(void) initWithHTMLBody: (NSString *) htmlBody headerTitle: (NSString *)headerTitle buttonTitle: (NSString *) buttonTitle;
-(void) initWithUrl: (NSString *) urlString headerTitle: (NSString *)headerTitle buttonTitle: (NSString *)NSString;
@end
