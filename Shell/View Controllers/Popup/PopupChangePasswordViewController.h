//
//  PopupChangePasswordViewController.h
//  Shell
//
//  Created by Jeremy Lua on 4/11/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "PopupBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface PopupChangePasswordViewController : PopupBaseVC

@end

NS_ASSUME_NONNULL_END
