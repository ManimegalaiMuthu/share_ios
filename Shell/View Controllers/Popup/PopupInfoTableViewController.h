//
//  PopupInfoTableViewController.h
//  Shell
//
//  Created by Jeremy Lua on 24/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupBaseVC.h"

@interface PopupInfoTableViewController : PopupBaseVC
-(void) initLegendsPopup;
-(void) initRedemptionHistoryLegendsPopup;
-(void) initRegisterProductCodeLegendsPopup;
-(void) initLoyaltyLegendsPopup;
-(void) initPackagesLegend:(NSArray *)packagesDetail;
-(void) initCashIncentiveLegend;
-(void) initPointsLegendPopup;
-(void) initOrdersSadixLegendsPopup;
@end
