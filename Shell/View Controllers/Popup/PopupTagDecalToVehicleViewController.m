//
//  PopupTagDecalToVehicleViewController.m
//  Shell
//
//  Created by Jeremy Lua on 30/5/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupTagDecalToVehicleViewController.h"
#import "QRCodeScanner.h"

@interface PopupTagDecalToVehicleViewController ()  < QRCodeScannerDelegate, UITextFieldDelegate, WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;


@property (weak, nonatomic) IBOutlet UITextField *txtDecalCode;

@property (weak, nonatomic) IBOutlet UIButton *btnScan;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

@end

@implementation PopupTagDecalToVehicleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [mQRCodeScanner setDelegate: self];

    self.lblHeader.font = FONT_H1;
    self.lblHeader.text = LOCALIZATION(C_DECAL_TAGDECALTOVEHICLE);      //lokalised
    
    [self.btnSubmit setTitle: LOCALIZATION(C_FORM_SUBMIT) forState: UIControlStateNormal];      //lokalised
    [self.btnSubmit.titleLabel setFont: FONT_BUTTON];
    [self.btnSubmit setBackgroundColor:COLOUR_RED];
    
    [self.btnCancel setTitle: LOCALIZATION(C_FORM_CANCEL) forState: UIControlStateNormal];      //lokalised
    [self.btnCancel.titleLabel setFont: FONT_BUTTON];
    
    self.txtDecalCode.placeholder = LOCALIZATION(C_DECAL_ENTERDECAL);           //lokalised
    
    if(kIsRightToLeft) {
        [self.txtDecalCode setTextAlignment:NSTextAlignmentRight];
    } else {
        [self.txtDecalCode setTextAlignment:NSTextAlignmentLeft];
    }
}

- (IBAction)cancelPressed:(id)sender {
    [self cancelButtonAction: nil];
}

- (IBAction)scanPressed:(id)sender {
    [mQRCodeScanner showZBarScannerOnVC: self];
}

-(void)qrCodeScanSuccess:(NSString *)scanString
{
    NSString *scannedCodeString;
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDONESIA])
    {
        scannedCodeString = scanString;
    }
    else
    {
        
        scannedCodeString = [[scanString componentsSeparatedByString:@"="] lastObject];
    }
    
    self.txtDecalCode.text = scannedCodeString;
}
- (IBAction)submitPressed:(id)sender {
    
    NSDictionary *params = @{@"Codes": self.txtDecalCode.text,
                             @"VehicleNumber": self.vehicleId,
                             @"StateId": @(self.stateId)};
    
    [[WebServiceManager sharedInstance] tagVehicleWithDecalCode: params vc:self];
}

- (void)processCompleted:(WebServiceResponse *)response
{
    switch(response.webserviceCall)
    {
        case kWEBSERVICE_TAGVEHICLETODECAL:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                
                [self cancelButtonAction: nil];
                [self.delegate didTagVehicle];
            }];
        }
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
