//
//  PopupValueEntryViewController.h
//  Shell
//
//  Created by Nach on 13/11/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "PopupBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PopupValueEntryDelegate<NSObject>
-(void) valueSubmitted :(NSString *) value;
@end

@interface PopupValueEntryViewController : PopupBaseVC

@property (nonatomic, weak) id<PopupValueEntryDelegate> delegate;

-(void)initWithTitle:(NSString*)title submitButtonTitle:(NSString*) submitTitle cancelButtonTitle:(NSString*) cancelTitle placeHolder:(NSString*) placeHolder;

@end

NS_ASSUME_NONNULL_END
