//
//  PopupCameraDecalViewController.h
//  Shell
//
//  Created by Jeremy Lua on 10/4/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "PopupBaseVC.h"

NS_ASSUME_NONNULL_BEGIN
@protocol  PopupCameraDecalDelegate <NSObject>
-(void)didSelectCarPlate;
-(void)didSelectDecal;
@end


@interface PopupCameraDecalViewController : PopupBaseVC
@property id<PopupCameraDecalDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
