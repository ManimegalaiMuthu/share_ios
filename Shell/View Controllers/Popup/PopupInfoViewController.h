//
//  PopupInfoViewController.h
//  Shell
//
//  Created by Jeremy Lua on 24/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupBaseVC.h"

@interface PopupInfoViewController : PopupBaseVC
-(void) initPopupInfoVCWithTitle:(NSString *)title content:(NSString *)content;
-(void) initPopupInfoVCWithTitle:(NSString *)title content:(NSString *)content btnTitle:(NSString *)btnTitle onYesPressed:(CompletionBlock)yesBlock;
@end
