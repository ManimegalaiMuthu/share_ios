//
//  PopupTagDecalViewController.h
//  Shell
//
//  Created by Jeremy Lua on 30/5/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupBaseVC.h"

@protocol PopupTagDecalDelegate <NSObject>
//- (void)dropDownSelection:(NSArray *)selection;

- (void)tagDecalWithVehicleId:(NSString *)vehicleId stateName:(NSString *)stateName;
@end

@interface PopupTagDecalViewController : PopupBaseVC

@property (nonatomic, weak) id<PopupTagDecalDelegate> delegate;
-(void) initDecalPopupWithDecalCode:(NSString*)decalCode;
@end
