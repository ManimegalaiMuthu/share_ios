//
//  PopupInfoTableViewController.m
//  Shell
//
//  Created by Jeremy Lua on 24/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupInfoTableViewController.h"
#import "Constants.h"
#import "LocalizationManager.h"
#import "CASCommonListCell.h"

#define COMMON_LIST_CELL @"CASCommonListCell"

#define CELL_HEIGHT 72

enum kPopupInfoTable_TYPE
{
    kPopupInfoTable_LEGENDS = 1,
    kPopupInfoTable_REDEMPTIONHISTORY,
    kPopupInfoTable_REGISTERPRODUCTCODERESULTS,
    kPopupInfoTable_LOYALTYLEGENDS,
    kPopupInfoTable_PACKAGESLEGND,
    kPopupInfoTable_CASHINCENTIVELEGEND,
    kPopupInfoTable_RUPOINTSLEGEND,
    kPopupInfoTable_ORDERLEGEND_SADIX,
};

@interface PopupInfoTableViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;

@property (weak, nonatomic) IBOutlet UIButton *okBtn;

@end

@implementation PopupInfoTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.lblHeader.font = FONT_H1;
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    self.tableView.tableFooterView = [UIView new];
    
    
    [self.okBtn.titleLabel setFont: FONT_BUTTON];
    [self.okBtn setTitle:LOCALIZATION(C_GLOBAL_OK) forState:UIControlStateNormal];  //lokalised 4 feb
    [self.okBtn setBackgroundColor:COLOUR_RED];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = CELL_HEIGHT;
    self.dataArray = [[NSMutableArray alloc] init];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    CGFloat totalCellHeight = 0;
    for (UITableViewCell *cell in self.tableView.subviews)
    {
        totalCellHeight += cell.frame.size.height;
        if (totalCellHeight > SCREEN_HEIGHT - self.lblHeader.frame.size.height - 40 - 30 - 80)
            //minus 40 - header height + space betw edge of window and table view
            //minus 30 - window margin to device height
            //minus 80 - button height + space betw edge of window and table view
            break;
    }
    
    self.tableViewHeightConstraint.constant = MIN(SCREEN_HEIGHT - self.lblHeader.frame.size.height - 40 - 30 - 80, totalCellHeight);
    
    [self.view layoutIfNeeded];
}

enum kManageWorkshop_WorkshopStatus
{
    kManageWorkshop_WorkshopStatus_ACTIVE = 1,
    kManageWorkshop_WorkshopStatus_LAPSED = 1 << 1,
    kManageWorkshop_WorkshopStatus_INACTIVE = 1 << 2,
    //    kManageWorkshop_WorkshopStatus_ALL,
};

-(void) initLegendsPopup
{
    self.lblHeader.text = LOCALIZATION(C_POPUP_LEGEND);    //lokalise 4 feb
    //    self.lblHeader.accessibilityLabel = @"Accessibility Label"; //replace label
    //    self.lblHeader.accessibilityValue = @"Accessibility Value"; //2nd voice
//    self.lblHeader.accessibilityValue = LOCALIZATION(@"");
    //    self.lblHeader.accessibilityHint = LOCALIZATION(@"Dropdown Menu Header"); //3rd voice
    
    [self.okBtn setTitle:LOCALIZATION(C_GLOBAL_OK) forState:UIControlStateNormal];      //lokalised
    
    self.dataArray = [[mSession lookupTable] objectForKey: @"WorkshopStatus"];
    self.tableView.tag = kPopupInfoTable_LEGENDS;
    
    self.tableView.estimatedRowHeight = CELL_HEIGHT;
    
//    self.tableViewHeightConstraint.constant = SCREEN_HEIGHT - 165;

    [self.view layoutIfNeeded];
}

-(void) initLoyaltyLegendsPopup
{
    self.lblHeader.text = LOCALIZATION(C_POPUP_LEGEND);     //lokalise 4 feb
    [self.okBtn setTitle:LOCALIZATION(C_GLOBAL_OK) forState:UIControlStateNormal];      //lokalised
    
    self.dataArray = [[mSession lookupTable] objectForKey:@"ContractStatus"];
    self.tableView.tag = kPopupInfoTable_LOYALTYLEGENDS;
    
    self.tableView.estimatedRowHeight = CELL_HEIGHT;
    [self.view layoutIfNeeded];
    
}

-(void) initPackagesLegend:(NSArray *)packagesDetail {
    self.lblHeader.text = LOCALIZATION(C_LOYALTY_RECOMMENDEDPACKAGES);              //lokalised
    [self.okBtn setTitle:LOCALIZATION(C_GLOBAL_OK) forState:UIControlStateNormal];  //lokalised
    
    self.dataArray = packagesDetail;
    self.tableView.tag = kPopupInfoTable_PACKAGESLEGND;
    
    self.tableView.estimatedRowHeight = 90;
    [self.view layoutIfNeeded];
}

-(void) initCashIncentiveLegend {
    
    self.lblHeader.text = LOCALIZATION(C_POPUP_LEGEND);     //lokalise 4 feb
    [self.okBtn setTitle:LOCALIZATION(C_GLOBAL_OK) forState:UIControlStateNormal];      //lokalised
    
    NSMutableArray *statusArray = [mSession.lookupTable objectForKey:@"CashStatus"];
    for(NSDictionary *dict in statusArray) {
        switch([[dict objectForKey:@"KeyCode"] intValue]) {
            case 17:
            case 34:
                [self.dataArray addObject:dict];
                break;
            default:
                break;
        }
    }
    self.tableView.tag = kPopupInfoTable_CASHINCENTIVELEGEND;
    
    [self.tableView reloadData];
    [self.view layoutIfNeeded];
}

-(void) initPointsLegendPopup
{
    self.lblHeader.text = LOCALIZATION(C_POPUP_LEGEND);
    self.lblHeader.accessibilityValue = LOCALIZATION(@"");
    
    [self.okBtn setTitle:LOCALIZATION(C_GLOBAL_OK) forState:UIControlStateNormal];      //lokalise 4 feb
    
    self.dataArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in [[mSession lookupTable] objectForKey: @"TransactionType"])
    {
        [self.dataArray addObject: dict];
    }
    
    //    self.dataArray = [[mSession lookupTable] objectForKey: @"OrderStatus"];
    self.tableView.tag = kPopupInfoTable_RUPOINTSLEGEND;
    
    self.tableView.estimatedRowHeight = CELL_HEIGHT;
    
    //    self.tableViewHeightConstraint.constant = SCREEN_HEIGHT - 165;
    
    [self.view layoutIfNeeded];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataArray count];
}

enum kManageWorkshop_RedemptionHistory
{
    kManageWorkshop_RedemptionHistory_PENDING = 1,
    kManageWorkshop_RedemptionHistory_INPROGRESS = 1 << 1,
    kManageWorkshop_RedemptionHistory_COMPLETED = 1 << 2,
    kManageWorkshop_RedemptionHistory_CANCELLED = 104,
    //    kManageWorkshop_WorkshopStatus_ALL,
};
-(void) initRedemptionHistoryLegendsPopup
{
    self.lblHeader.text = LOCALIZATION(C_POPUP_LEGEND);     //lokalise 4 feb
    //    self.lblHeader.accessibilityLabel = @"Accessibility Label"; //replace label
    //    self.lblHeader.accessibilityValue = @"Accessibility Value"; //2nd voice
    self.lblHeader.accessibilityValue = LOCALIZATION(@"");
    //    self.lblHeader.accessibilityHint = LOCALIZATION(@"Dropdown Menu Header"); //3rd voice
    
    [self.okBtn setTitle:LOCALIZATION(C_GLOBAL_OK) forState:UIControlStateNormal];      //lokalise 4 feb
    
    self.dataArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in [[mSession lookupTable] objectForKey: @"RedemStatus"])
    {
        //skip all status
        if (![[dict objectForKey: @"KeyCode"] isEqualToString: @"128"])
        {
            [self.dataArray addObject: dict];
        }
    }
    
    //    self.dataArray = [[mSession lookupTable] objectForKey: @"OrderStatus"];
    self.tableView.tag = kPopupInfoTable_REDEMPTIONHISTORY;
    
    self.tableView.estimatedRowHeight = CELL_HEIGHT;
    
    //    self.tableViewHeightConstraint.constant = SCREEN_HEIGHT - 165;
    
    [self.view layoutIfNeeded];
}



enum kProductCodeResults_TYPES
{
    kProductCodeResults_SUCCESS     = 1,
    kProductCodeResults_USED        = 1 << 1,
    kProductCodeResults_INVALID     = 1 << 2,
    kProductCodeResults_UNAUTH      = 1 << 3,
    //    kManageWorkshop_WorkshopStatus_ALL,
};

-(void) initRegisterProductCodeLegendsPopup
{
    self.lblHeader.text = LOCALIZATION(C_POPUP_LEGEND);         //lokalise 4 feb
    //    self.lblHeader.accessibilityLabel = @"Accessibility Label"; //replace label
    //    self.lblHeader.accessibilityValue = @"Accessibility Value"; //2nd voice
    self.lblHeader.accessibilityValue = LOCALIZATION(@"");
    //    self.lblHeader.accessibilityHint = LOCALIZATION(@"Dropdown Menu Header"); //3rd voice
    
    [self.okBtn setTitle:LOCALIZATION(C_GLOBAL_OK) forState:UIControlStateNormal];      //lokalise 4 feb
    
    self.dataArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in [[mSession lookupTable] objectForKey: @"VehicleTransStatus"])
    {
        [self.dataArray addObject: dict];
    }
    
    //    self.dataArray = [[mSession lookupTable] objectForKey: @"OrderStatus"];
    self.tableView.tag = kPopupInfoTable_REGISTERPRODUCTCODERESULTS;
    
    self.tableView.estimatedRowHeight = CELL_HEIGHT;
    
    //    self.tableViewHeightConstraint.constant = SCREEN_HEIGHT - 165;
    
    [self.view layoutIfNeeded];
}

enum kSadixOrderLegend_TYPES
{
    kSadixOrderLegend_SHARE         = 1,
    kSadixOrderLegend_OTHERS        = 1 << 1,
};

-(void) initOrdersSadixLegendsPopup {
    self.lblHeader.text = LOCALIZATION(C_POPUP_LEGEND);
    
    [self.okBtn setTitle:LOCALIZATION(C_GLOBAL_OK) forState:UIControlStateNormal];
    
    self.dataArray = [[NSMutableArray alloc] init];
    
    [self.dataArray addObject:@{
        @"KeyCode"  :   @"1",
        @"KeyValue" :   LOCALIZATION(C_MANAGEORDERS_ORDERSOURCE_SHARE),
        @"Remarks"  :   LOCALIZATION(C_POPUP_ORDERSOURCE_SHARE_REMARKS),
    }];
    
    [self.dataArray addObject:@{
        @"KeyCode"  :   @"2",
        @"KeyValue" :   LOCALIZATION(C_MANAGEORDERS_ORDERSOURCE_OTHER),
        @"Remarks"  :   LOCALIZATION(C_POPUP_ORDERSOURCE_OTHER_REMARKS),
    }];
    
    self.tableView.tag = kPopupInfoTable_ORDERLEGEND_SADIX;
    
    self.tableView.estimatedRowHeight = CELL_HEIGHT;
    
    [self.view layoutIfNeeded];
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (tableView.tag)
    {
        case kPopupInfoTable_REGISTERPRODUCTCODERESULTS:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"LegendsCell"];
            
            NSDictionary *cellData = [self.dataArray objectAtIndex: indexPath.row];
            //        @"Status": @(kManageWorkshop_WorkshopStatus_INACTIVE),
            //        @"StatusTitle": @"Inactive Workshops:",
            //        @"StatusDesc": @"Have not registered any product codes since joined",
            
            UIImageView *imgStatus = [cell viewWithTag: 1];
            
            switch ([[cellData objectForKey: @"KeyCode"] intValue]) {
                case kProductCodeResults_SUCCESS:
                    [imgStatus setImage: [UIImage imageNamed: @"icn_success_small"]];
                    break;
                case kProductCodeResults_USED:
                    [imgStatus setImage: [UIImage imageNamed: @"icn_used_small"]];
                    break;
                case kProductCodeResults_INVALID:
                    [imgStatus setImage: [UIImage imageNamed: @"icn_invalid_small"]];
                    break;
                case kProductCodeResults_UNAUTH:
                    [imgStatus setImage: [UIImage imageNamed: @"icn_unathorized_small"]];
                    break;
                default:
                    [imgStatus setImage: nil];
                    break;
            }
            
            UILabel *statusTitle = [cell viewWithTag: 2];
            statusTitle.font = FONT_B1;
            statusTitle.text = [cellData objectForKey: @"KeyValue"];
            
            UILabel *statusDesc = [cell viewWithTag: 3];
            statusDesc.font = FONT_B1;
            statusDesc.text = [cellData objectForKey: @"Remarks"];
            return cell;
        }
            break;
        case kPopupInfoTable_REDEMPTIONHISTORY:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"LegendsCell"];
            
            NSDictionary *cellData = [self.dataArray objectAtIndex: indexPath.row];
            //        @"Status": @(kManageWorkshop_WorkshopStatus_INACTIVE),
            //        @"StatusTitle": @"Inactive Workshops:",
            //        @"StatusDesc": @"Have not registered any product codes since joined",
            
            UIImageView *imgStatus = [cell viewWithTag: 1];
            
            switch ([[cellData objectForKey: @"KeyCode"] intValue]) {
                case kManageWorkshop_RedemptionHistory_PENDING:
                    [imgStatus setImage: [UIImage imageNamed: @"icn_pending_small_on"]];
                    break;
                case kManageWorkshop_RedemptionHistory_INPROGRESS:
                    [imgStatus setImage: [UIImage imageNamed: @"icn_processing_small_on"]];
                    break;
                case kManageWorkshop_RedemptionHistory_COMPLETED:
                    [imgStatus setImage: [UIImage imageNamed: @"icn_delivered_small_on"]];
                    break;
                case kManageWorkshop_RedemptionHistory_CANCELLED:
                    [imgStatus setImage: [UIImage imageNamed: @"icn_cancelled"]];
                    break;
                default:
                    [imgStatus setImage: nil];
                    break;
            }
            
            UILabel *statusTitle = [cell viewWithTag: 2];
            statusTitle.font = FONT_B1;
            statusTitle.text = [cellData objectForKey: @"KeyValue"];
            
            UILabel *statusDesc = [cell viewWithTag: 3];
            statusDesc.text = @"";
            return cell;
        }
            break;
        case kPopupInfoTable_LEGENDS:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"LegendsCell"];
            
            NSDictionary *cellData = [self.dataArray objectAtIndex: indexPath.row];
//            {
//                KeyCode = 1;
//                KeyValue = Pending;
//            },
//            {
//                KeyCode = 2;
//                KeyValue = "In Progress";
//            },
//            {
//                KeyCode = 4;
//                KeyValue = Completed;
            
            UIImageView *imgStatus = [cell viewWithTag: 1];
            
            switch ([[cellData objectForKey: @"KeyCode"] intValue]) {
                case kManageWorkshop_WorkshopStatus_ACTIVE:
                    [imgStatus setImage: [UIImage imageNamed: @"icon-active"]];
                    break;
                case kManageWorkshop_WorkshopStatus_LAPSED:
                    [imgStatus setImage: [UIImage imageNamed: @"icon-lapsed"]];
                    break;
                case kManageWorkshop_WorkshopStatus_INACTIVE:
                    [imgStatus setImage: [UIImage imageNamed: @"icon-inactive"]];
                    break;
                default:
                    [imgStatus setImage: nil];
                    break;
            }
            
            UILabel *statusTitle = [cell viewWithTag: 2];
            statusTitle.font = FONT_H1;
            statusTitle.text = [cellData objectForKey: @"KeyValue"];
            
            UILabel *statusDesc = [cell viewWithTag: 3];
            statusDesc.font = FONT_B2;
            statusDesc.text = [cellData objectForKey: @"Remarks"];
            return cell;
        }
            break;
        case kPopupInfoTable_LOYALTYLEGENDS:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"LegendsCell"];
            
            NSDictionary *cellData = [self.dataArray objectAtIndex: indexPath.row];
            //        @"Status": @(kManageWorkshop_WorkshopStatus_INACTIVE),
            //        @"StatusTitle": @"Inactive Workshops:",
            //        @"StatusDesc": @"Have not registered any product codes since joined",
            
            UIImageView *imgStatus = [cell viewWithTag: 1];
            
            switch ([[cellData objectForKey: @"KeyCode"] intValue]) {
                case kCONTRACTSTATUS_PENDING:
                    [imgStatus setImage: [UIImage imageNamed: @"icn_pending_small_on"]];
                    break;
                case kCONTRACTSTATUS_COMPLETED:
                    [imgStatus setImage: [UIImage imageNamed: @"icn_delivered_small_on"]];
                    break;
                case kCONTRACTSTATUS_UNDERREVIEW:
                    [imgStatus setImage: [UIImage imageNamed: @"icn_underreview_big"]];
                    break;
                case kCONTRACTSTATUS_INCOMPLETE:
                case kCONTRACTSTATUS_UNACCEPTED:
                    [imgStatus setImage: [UIImage imageNamed: @"icn_cancelled"]];
                    break;
                case kCONTRACTSTATUS_INPROGRESS:
                    [imgStatus setImage: [UIImage imageNamed: @"icn_processing_small_on"]];
                    break;
                default:
                    [imgStatus setImage: nil];
                    break;
            }
            
            UILabel *statusTitle = [cell viewWithTag: 2];
            statusTitle.font = FONT_H1;
            statusTitle.text = [cellData objectForKey: @"KeyValue"];
            
            UILabel *statusDesc = [cell viewWithTag: 3];
            statusDesc.font = FONT_B2;
            statusDesc.text =[cellData objectForKey: @"Remarks"];
            return cell;
        }
            break;
        case kPopupInfoTable_PACKAGESLEGND: {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"LegendsCell"];
            
            NSDictionary *cellData = [self.dataArray objectAtIndex: indexPath.row];
            //        @"Status": @(kManageWorkshop_WorkshopStatus_INACTIVE),
            //        @"StatusTitle": @"Inactive Workshops:",
            //        @"StatusDesc": @"Have not registered any product codes since joined",
            
            UIImageView *imgStatus = [cell viewWithTag: 1];
            imgStatus.hidden = YES;
            

            UILabel *statusTitle = [cell viewWithTag: 2];
            statusTitle.font = FONT_H1;
            statusTitle.text = [cellData objectForKey: @"PackageName"];
            
            UILabel *statusDesc = [cell viewWithTag: 3];
            statusDesc.font = FONT_B2;
            statusDesc.text = [NSString stringWithFormat:@"%@ : %@ \n%@  %@ \n%@ : %@",LOCALIZATION(C_LOYALTY_LEGEND_TOTAL),[cellData objectForKey:@"TargetQty"],LOCALIZATION(C_LOYALTY_MINORDER),[cellData objectForKey:@"PackageDetails"],LOCALIZATION(C_LOYALTY_LEGEND_PERMONTH),[cellData objectForKey:@"MonthlyMinTarget"]];       //lokalise 8 Feb
            return cell;
        }
            break;
        case kPopupInfoTable_CASHINCENTIVELEGEND: {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"LegendsCell"];
            
            NSDictionary *cellData = [self.dataArray objectAtIndex: indexPath.row];
            
            UIImageView *imgStatus = [cell viewWithTag: 1];
            switch ([[cellData objectForKey:@"KeyCode"]intValue]) {
                case 17:
                    [imgStatus setImage: [UIImage imageNamed: @"icn_add_small"]];
                    break;
                case 34:
                    [imgStatus setImage: [UIImage imageNamed: @"icn_minus_small"]];
                    break;
                default:
                    [imgStatus setImage: nil];
                    break;
            }
            
            UILabel *statusTitle = [cell viewWithTag: 2];
            statusTitle.font = FONT_H1;
            statusTitle.text = [cellData objectForKey: @"KeyValue"];
            
            UILabel *statusDesc = [cell viewWithTag: 3];
            statusDesc.font = FONT_B2;
            statusDesc.text =[cellData objectForKey: @"Remarks"];
            return cell;
        }
            break;
        case kPopupInfoTable_RUPOINTSLEGEND: {
               UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"LegendsCell"];
               
               NSDictionary *cellData = [self.dataArray objectAtIndex: indexPath.row];
               //        @"Status": @(kManageWorkshop_WorkshopStatus_INACTIVE),
               //        @"StatusTitle": @"Inactive Workshops:",
               //        @"StatusDesc": @"Have not registered any product codes since joined",
               
               UIImageView *imgStatus = [cell viewWithTag: 1];
               
               switch (indexPath.row) {
                   case 0:
                   case 1:
                        [imgStatus setImage: [UIImage imageNamed: @"icn_add_small.png"]];
                        break;
                   case 2:
                        [imgStatus setImage: [UIImage imageNamed: @"icn_minus_small.png"]];
                        break;
                   case 3:
                       [imgStatus setImage: [UIImage imageNamed: @"icn_pending_small.png"]];
                       break;
                    default:
                       [imgStatus setImage: [UIImage imageNamed: @"icn_add_small.png"]];
                       break;
                }

               
               UILabel *statusTitle = [cell viewWithTag: 2];
               statusTitle.font = FONT_B1;
               statusTitle.text = [cellData objectForKey: @"KeyValue"];
               
               UILabel *statusDesc = [cell viewWithTag: 3];
               statusDesc.text = @"";
               return cell;
           }
            break;
        case kPopupInfoTable_ORDERLEGEND_SADIX:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"LegendsCell"];
                       
            NSDictionary *cellData = [self.dataArray objectAtIndex: indexPath.row];
                       
            UIImageView *imgStatus = [cell viewWithTag: 1];
            switch ([[cellData objectForKey:@"KeyCode"]intValue]) {
                case 1:
                    [imgStatus setImage: [UIImage imageNamed: @"icn_share-orders.png"]];
                    break;
                case 2:
                    [imgStatus setImage: [UIImage imageNamed: @"icn_other-orders.png"]];
                    break;
                default:
                    [imgStatus setImage: nil];
                    break;
            }
            [NSLayoutConstraint constraintWithItem:imgStatus attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:30].active = YES;
                       
            UILabel *statusTitle = [cell viewWithTag: 2];
            statusTitle.font = FONT_H1;
            statusTitle.text = [cellData objectForKey: @"KeyValue"];
            
            UILabel *statusDesc = [cell viewWithTag: 3];
            statusDesc.font = FONT_B1;
            statusDesc.text =[cellData objectForKey: @"Remarks"];
            return cell;
        }
            break;
        default:
            break;
    }
    return [UITableViewCell new];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
