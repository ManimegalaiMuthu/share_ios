//
//  PopupRewardFilterViewController.m
//  Shell
//
//  Created by Jeremy Lua on 4/7/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupRewardFilterViewController.h"
#import "MARKRangeSlider.h"

@interface PopupRewardFilterViewController ()
@property (weak, nonatomic) IBOutlet MARKRangeSlider *rangeSlider;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblFilterPoints;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckbox;
@property (weak, nonatomic) IBOutlet UILabel *lblCheckbox;

@property (weak, nonatomic) IBOutlet UIButton *okBtn;

@property NSInteger minValue;
@property NSInteger maxValue;
@end

@implementation PopupRewardFilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
}
-(void) initFilterRewardPopup:(NSString *)title leftValue:(NSInteger)leftValue rightValue:(NSInteger)rightValue isChecked:(BOOL)isChecked
{
    NSArray *redeemLookupArray = [[mSession lookupTable] objectForKey: @"RedemPointRange"];
    NSString *redeemRangeString = [[redeemLookupArray firstObject] objectForKey: @"KeyValue"]; //RedemPointRange is array
    NSArray *redeemRangeStringArray = [redeemRangeString componentsSeparatedByString: @"-"]; //if slider reach first value, convert to second value
    
    NSInteger minValue = 0;
    NSInteger maxValue = [[redeemRangeStringArray firstObject] intValue];
    
    self.lblHeader.text = title;
    self.lblHeader.font = FONT_H1;
    
    
//    self.lblFilterPoints.text = [NSString stringWithFormat: @"%i points - %i points", leftValue, rightValue];
    self.lblFilterPoints.text = [NSString stringWithFormat: @"%i %@ - %i %@", leftValue, LOCALIZATION(C_REWARDS_POINTS), rightValue, LOCALIZATION(C_REWARDS_POINTS)];  //lokalised 31 jan
    if (rightValue >= [[redeemRangeStringArray firstObject] intValue])
    {
//        self.lblFilterPoints.text = [NSString stringWithFormat: @"%i points - %@ points+", leftValue, [redeemRangeStringArray firstObject]];
        self.lblFilterPoints.text = [NSString stringWithFormat: @"%i %@ - %i %@", leftValue, LOCALIZATION(C_REWARDS_POINTS), [[redeemRangeStringArray firstObject] intValue], LOCALIZATION(C_REWARDS_POINTS)];  //lokalised 31 jan
    }
    
    
    self.lblFilterPoints.font = FONT_B1;

    [self.rangeSlider addTarget:self
                         action:@selector(rangeSliderValueDidChange:)
               forControlEvents:UIControlEventValueChanged];
    
    [self.rangeSlider setMinValue:minValue maxValue:maxValue];
    [self.rangeSlider setLeftValue:leftValue rightValue:rightValue];
    
    self.rangeSlider.minimumDistance = 0.2;
    
    
    self.lblCheckbox.text = LOCALIZATION(C_REWARDS_ONLYSHOWREDEEM);     //lokalised 31 jan
    self.lblCheckbox.font = FONT_B1;
    self.btnCheckbox.selected = isChecked;
    
    self.okBtn.titleLabel.font = FONT_BUTTON;
    [self.okBtn setTitle: LOCALIZATION(C_BUTTON_OK) forState:UIControlStateNormal];  //lokalised 31 jan
    [self.okBtn setBackgroundColor:COLOUR_RED];
}

- (IBAction)okPressed:(id)sender {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:@(self.rangeSlider.leftValue) forKey: @"LeftValue"];
    if (self.btnCheckbox.selected)
        [params setObject: @([mSession rewardsCurrentPoints]) forKey: @"RightValue"];
    else
        [params setObject:@(self.rangeSlider.rightValue) forKey: @"RightValue"];
    [params setObject:@(self.btnCheckbox.selected) forKey: @"ShowRewardsCanRedem"];

    [self.delegate didFinishRewardFilter: params];
    [self cancelButtonAction: nil];
}

- (IBAction)checkboxPressed:(id)sender {
    NSArray *redeemLookupArray = [[mSession lookupTable] objectForKey: @"RedemPointRange"];
    NSString *redeemRangeString = [[redeemLookupArray firstObject] objectForKey: @"KeyValue"]; //RedemPointRange is array
    NSArray *redeemRangeStringArray = [redeemRangeString componentsSeparatedByString: @"-"]; //if slider reach first value, convert to second value
    
    NSString *pointsRangeString;
    
    self.btnCheckbox.selected = !self.btnCheckbox.selected;
    
    if (self.btnCheckbox.selected)
    {
        //filter to own current points
        [self.rangeSlider setLeftValue: 0 rightValue: [mSession rewardsCurrentPoints]];
        
        if ([mSession rewardsCurrentPoints] > [[redeemRangeStringArray firstObject] doubleValue])
        {
            //current points > max points, filter to max
            self.lblFilterPoints.text = [NSString stringWithFormat: @"%i %@ - %i %@+", 0, LOCALIZATION(C_REWARDS_POINTS), (int) self.rangeSlider.rightValue, LOCALIZATION(C_REWARDS_POINTS)];   //lokalised 31 jan
        }
        else
        {
            self.lblFilterPoints.text = [NSString stringWithFormat: @"%i %@ - %i %@", 0, LOCALIZATION(C_REWARDS_POINTS), (int) self.rangeSlider.rightValue, LOCALIZATION(C_REWARDS_POINTS)];   //lokalised 31 jan
        }
    }
    else
    {
        //filter to max points
        [self.rangeSlider setLeftValue: 0 rightValue: [[redeemRangeStringArray firstObject] doubleValue]];
        self.lblFilterPoints.text = [NSString stringWithFormat: @"%i %@ - %i %@+", 0, LOCALIZATION(C_REWARDS_POINTS), (int) self.rangeSlider.rightValue, LOCALIZATION(C_REWARDS_POINTS)];  //lokalised 31 jan
    }
}

- (IBAction)rangeSliderValueDidChange:(MARKRangeSlider *)slider {
    self.btnCheckbox.selected = NO; //reset
    
    NSArray *redeemLookupArray = [[mSession lookupTable] objectForKey: @"RedemPointRange"];
    NSString *redeemRangeString = [[redeemLookupArray firstObject] objectForKey: @"KeyValue"]; //RedemPointRange is array
    NSArray *redeemRangeStringArray = [redeemRangeString componentsSeparatedByString: @"-"]; //if slider reach first value, convert to second value
    
    
//    self.lblFilterPoints.text = [NSString stringWithFormat: @"%i points - %i points", (int) slider.leftValue, (int) slider.rightValue];
    self.lblFilterPoints.text = [NSString stringWithFormat: @"%i %@ - %i %@", (int) slider.leftValue, LOCALIZATION(C_REWARDS_POINTS), (int) slider.rightValue, LOCALIZATION(C_REWARDS_POINTS)];  //lokalised 31 jan
    
    if ((int) slider.rightValue >= [[redeemRangeStringArray firstObject] intValue])
    {
//        self.lblFilterPoints.text = [NSString stringWithFormat: @"%i points - %i points+", (int) slider.leftValue, (int) slider.rightValue];
        self.lblFilterPoints.text = [NSString stringWithFormat: @"%i %@ - %i %@+", (int) slider.leftValue, LOCALIZATION(C_REWARDS_POINTS), (int) slider.rightValue, LOCALIZATION(C_REWARDS_POINTS)];  //lokalised 31 jan
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
