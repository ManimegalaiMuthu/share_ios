//
//  PopupPrivacyPolicyViewController.h
//  Shell
//
//  Created by Jeremy Lua on 2/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupBaseVC.h"

@protocol PopupPrivacyPolicyDelegate <NSObject>
-(void) didAgreePrivacyPolicy:(BOOL) tncSelected;
@end

@interface PopupPrivacyPolicyViewController : PopupBaseVC
@property id<PopupPrivacyPolicyDelegate> delegate;
@property NSMutableDictionary *registrationDict;
@end
