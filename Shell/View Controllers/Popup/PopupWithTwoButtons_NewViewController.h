//
//  PopupWithTwoButtons_NewViewController.h
//  Shell
//
//  Created by Nach on 2/1/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "PopupBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface PopupWithTwoButtons_NewViewController : PopupBaseVC

-(void) initWithUrl:(NSString *)urlString withLaterBTnTitle:(NSString *)laterBtnTitle andActionTitle:(NSString *)actionBtnTitle onLaterPressed:(CompletionBlock)laterBlock onActionPressed:(CompletionBlock)actionBlock;

@end

NS_ASSUME_NONNULL_END
