//
//  PopupTextViewController.m
//  Shell
//
//  Created by Ben on 28/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PopupTextViewController.h"

@interface PopupTextViewController () <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel* titleLabel;
@property (weak, nonatomic) IBOutlet UIButton* yesButton;
@property (weak, nonatomic) IBOutlet UIButton* noButton;
@property (weak, nonatomic) IBOutlet UITextView* remarksTextView;
@property (weak, nonatomic) IBOutlet UILabel* noRemarksLabel;
@property void(^completionHandler)(BOOL isYes,NSString* remarks);
@property NSString* placeHolder;

@end


@implementation PopupTextViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self.remarksTextView setFont:FONT_B1];
    [self.titleLabel setFont:FONT_H1];
    [self.noRemarksLabel setFont:FONT_B2];
    
    [self.yesButton.titleLabel setFont:FONT_BUTTON];
    [self.noButton.titleLabel setFont:FONT_BUTTON];
    self.remarksTextView.text = self.placeHolder;
    
    [self.yesButton setBackgroundColor:COLOUR_RED];
}

-(void)initWithTitle:(NSString*)title yesButtonTitle:(NSString*) yesTitle noButtonTitle:(NSString*) noTitle placeHolder:(NSString*)placeHolder completionHandler:(void(^)(BOOL isYes,NSString* remarks)) completionHandler
{
    self.titleLabel.text = title;
    [self.yesButton setTitle:yesTitle forState:UIControlStateNormal];
    [self.noButton setTitle:noTitle forState:UIControlStateNormal];
    self.placeHolder = placeHolder;
    self.remarksTextView.text = placeHolder;
    self.completionHandler = completionHandler;
}

-(void)initWithTitle:(NSString*)title yesButtonTitle:(NSString*) yesTitle noButtonTitle:(NSString*) noTitle placeHolder:(NSString*)placeHolder
{
    self.titleLabel.text = title;
    [self.yesButton setTitle:yesTitle forState:UIControlStateNormal];
    [self.noButton setTitle:noTitle forState:UIControlStateNormal];
    self.placeHolder = placeHolder;
    self.remarksTextView.text = placeHolder;
    self.completionHandler = nil;
}

-(IBAction)yesPressed:(id)sender
{
    if([self.remarksTextView.text isEqualToString:self.placeHolder] || self.remarksTextView.text.length == 0)
    {
        self.noRemarksLabel.text = LOCALIZATION(C_POPUP_PLEASE_ENTER_REMARKS);  //lokalise 4 feb
    }
    else
    {
        if (self.completionHandler != nil) {
            self.completionHandler(YES, self.remarksTextView.text);
        }
        [self cancelButtonAction:nil];
    }
}
-(IBAction)noPressed:(id)sender
{
    if (self.completionHandler != nil) {
        self.completionHandler(NO, self.remarksTextView.text);
    }
    
    [self cancelButtonAction:nil];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if([textView.text isEqualToString:self.placeHolder])
        textView.text = @"";
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView.text.length == 0)
        textView.text = self.placeHolder;
}

@end
