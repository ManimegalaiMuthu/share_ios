//
//  PopupValueEntryViewController.m
//  Shell
//
//  Created by Nach on 13/11/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "PopupValueEntryViewController.h"

@interface PopupValueEntryViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *textValue;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@property NSString *placeHolder;

@end

@implementation PopupValueEntryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.textValue setFont:FONT_B1];
    [self.lblTitle setFont:FONT_H1];
    [self.btnCancel.titleLabel setFont:FONT_BUTTON];
    [self.btnSubmit.titleLabel setFont:FONT_BUTTON];
    [self.textValue setPlaceholder:self.placeHolder];
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [self.textValue setLeftViewMode:UITextFieldViewModeAlways];
    [self.textValue setLeftView:spacerView];
    
    self.lblTitle.textColor = COLOUR_VERYDARKGREY;
    self.textValue.textColor = COLOUR_VERYDARKGREY;
    [self.btnCancel setBackgroundColor:COLOUR_YELLOW];
    [self.btnSubmit setBackgroundColor:COLOUR_RED];
}

-(void)initWithTitle:(NSString*)title submitButtonTitle:(NSString*) submitTitle cancelButtonTitle:(NSString*) cancelTitle placeHolder:(NSString*) placeHolder {
    self.lblTitle.text = title;
    self.placeHolder = placeHolder;
    [self.textValue setPlaceholder:self.placeHolder];
    [self.textValue setDelegate:self.delegate];
    
    [self.btnSubmit setTitle:submitTitle forState:UIControlStateNormal];
    [self.btnCancel setTitle:cancelTitle forState:UIControlStateNormal];
}

- (IBAction)cancelBtnPressed:(UIButton *)sender {
    [self cancelButtonAction:nil];
}

- (IBAction)submitBtnPressed:(UIButton *)sender {
    
    if([self.textValue.text length] > 0) {
         [self.delegate valueSubmitted: self.textValue.text];
    }
    
    [self cancelButtonAction:nil];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
