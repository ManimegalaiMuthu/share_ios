//
//  PopupAttributedLabelViewController.m
//  Shell
//
//  Created by Nach on 25/1/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "PopupAttributedLabelViewController.h"

@interface PopupAttributedLabelViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblContent;
@property (weak, nonatomic) IBOutlet UIScrollView *contentScrollView;

@property (weak, nonatomic) IBOutlet UIButton *okButton;


@end

@implementation PopupAttributedLabelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.lblHeader.font = FONT_H1;
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    
    self.lblContent.font = FONT_B1;
    self.lblContent.textColor = COLOUR_VERYDARKGREY;
    
    [self.okButton.titleLabel setFont: FONT_BUTTON];
    [self.okButton setBackgroundColor: COLOUR_RED];
    [self.okButton setTitle:LOCALIZATION(C_GLOBAL_OK) forState:UIControlStateNormal];
}

-(void) initPopupAttributedLabelVCWithTitle:(NSString *)title content:(NSString *)content
{
    if([title isEqualToString:@""]) {
        self.lblHeader.hidden = YES;
        [NSLayoutConstraint constraintWithItem:self.lblHeader attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
    } else {
        self.lblHeader.hidden = NO;
        self.lblHeader.text = title;
    }
    NSString *contentString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:15; color: #404040;}</style>%@",content];
    self.lblContent.attributedText = [[NSAttributedString alloc] initWithData: [contentString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    CGSize size = [self.lblContent.text sizeWithFont:self.lblContent.font
                                   constrainedToSize:CGSizeMake(self.contentScrollView.frame.size.width, MAXFLOAT)
                                       lineBreakMode:UILineBreakModeWordWrap];
    self.lblContent.frame = CGRectMake(0, 0, size.width, size.height);
    self.contentScrollView.contentSize = size;
}

-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    return;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
