//
//  PopupAttributedLabelViewController.h
//  Shell
//
//  Created by Nach on 25/1/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "PopupBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface PopupAttributedLabelViewController : PopupBaseVC

-(void) initPopupAttributedLabelVCWithTitle:(NSString *)title content:(NSString *)content;

@end

NS_ASSUME_NONNULL_END
