//
//  PopupCameraDecalViewController.m
//  Shell
//
//  Created by Jeremy Lua on 10/4/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "PopupCameraDecalViewController.h"

@interface PopupCameraDecalViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblScanDecal;
@property (weak, nonatomic) IBOutlet UILabel *lblScanCarPlate;
@end

@implementation PopupCameraDecalViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.lblScanDecal.text = LOCALIZATION(C_POPUP_SCANDECAL);
    self.lblScanCarPlate.text = LOCALIZATION(C_POPUP_SCANCARPLATE);
    
    self.lblScanDecal.font = FONT_H1;
    self.lblScanCarPlate.font = self.lblScanDecal.font;
}

- (IBAction)decalPressed:(id)sender
{
    [self.delegate didSelectDecal];
    

    //dismiss
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (IBAction)carPlatePressed:(id)sender
{
    [self.delegate didSelectCarPlate];
    

    //dismiss
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
