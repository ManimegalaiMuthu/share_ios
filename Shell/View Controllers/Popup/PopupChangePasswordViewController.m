//
//  PopupChangePasswordViewController.m
//  Shell
//
//  Created by Jeremy Lua on 4/11/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "PopupChangePasswordViewController.h"

@interface PopupChangePasswordViewController () <WebServiceManagerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UILabel *lblChangePassword;
@property (weak, nonatomic) IBOutlet UILabel *lblOldPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtOldPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblConfirmPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;

@end

@implementation PopupChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupInterface];
}


- (void) setupInterface
{
    self.lblChangePassword.font = FONT_H1;
    self.lblChangePassword.text = LOCALIZATION(C_FORM_CHANGEPASSWORD);  //lokalise 30 jan
    
    self.txtOldPassword.font = FONT_B2;
    self.txtOldPassword.placeholder = LOCALIZATION(C_PLACEHOLDER_CURRENTPASSWORD);  //lokalise 30 jan
    self.lblOldPassword.text = LOCALIZATION(C_PROFILE_CURRENTPASSWORD);  //lokalise
    self.lblOldPassword.font = self.txtOldPassword.font;
    
    self.txtNewPassword.font = self.txtOldPassword.font;
    self.txtNewPassword.placeholder = LOCALIZATION(C_PLACEHOLDER_NEWPASSWORD);  //lokalise 30 jan
    
    self.lblNewPassword.text = LOCALIZATION(C_PROFILE_NEWPASSWORD);  //lokalise
    self.lblNewPassword.font = self.txtNewPassword.font;

    
    self.txtConfirmPassword.font = self.txtOldPassword.font;
    self.txtConfirmPassword.placeholder = LOCALIZATION(C_PLACEHOLDER_RE_NEWPASSWORD);  //lokalise 30 jan
    
    self.lblConfirmPassword.font = self.txtOldPassword.font;
    self.lblConfirmPassword.text = LOCALIZATION(C_PROFILE_RE_NEWPASSWORD);  //lokalise
    
    if (kIsRightToLeft)
    {
        [self.lblChangePassword setTextAlignment: NSTextAlignmentRight];
        [self.txtOldPassword setTextAlignment: NSTextAlignmentRight];
        [self.lblOldPassword setTextAlignment: NSTextAlignmentRight];
        [self.txtNewPassword setTextAlignment: NSTextAlignmentRight];
        [self.lblNewPassword setTextAlignment: NSTextAlignmentRight];
        [self.txtConfirmPassword setTextAlignment: NSTextAlignmentRight];
        [self.lblConfirmPassword setTextAlignment: NSTextAlignmentRight];
    }
    else
    {
        [self.lblChangePassword setTextAlignment: NSTextAlignmentLeft];
        [self.txtOldPassword setTextAlignment: NSTextAlignmentLeft];
        [self.lblOldPassword setTextAlignment: NSTextAlignmentLeft];
        [self.txtNewPassword setTextAlignment: NSTextAlignmentLeft];
        [self.lblNewPassword setTextAlignment: NSTextAlignmentLeft];
        [self.txtConfirmPassword setTextAlignment: NSTextAlignmentLeft];
        [self.lblConfirmPassword setTextAlignment: NSTextAlignmentLeft];
    }
    
    
    [self.btnSubmit.titleLabel setFont: FONT_BUTTON];
    [self.btnSubmit setTitle:LOCALIZATION(C_FORM_SUBMIT) forState:UIControlStateNormal];  //lokalise 30 jan
    [Helper setCustomFontButtonContentModes: self.btnSubmit];
    [self.btnSubmit setBackgroundColor:COLOUR_RED];
    
    [self.btnCancel.titleLabel setFont: FONT_BUTTON];
    [self.btnCancel setTitle:LOCALIZATION(C_FORM_CANCEL) forState:UIControlStateNormal];  //lokalise 30 jan
    [Helper setCustomFontButtonContentModes: self.btnCancel];

    
}

- (IBAction)submitChangePasswordPressed:(id)sender {
    if (![self.txtNewPassword.text isEqualToString: self.txtConfirmPassword.text])
    {
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_CHANGEPASSWORD_MISMATCH_ERRORMSG) title:LOCALIZATION(C_GLOBAL_ERROR) closeButtonTitle: LOCALIZATION(C_POPUPMSG_SUBMITAGAIN)];  //lokalise 30 jan
        
        return;
    }
    [[WebServiceManager sharedInstance] changePasswordPostLogin:self.txtOldPassword.text
                                                    newPassword:self.txtNewPassword.text
                                                             vc: self];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_CHANGEPASSWORD:
        {
            [mAlert showSuccessAlertWithMessage:[response getResponseMessage] onCompletion:^(BOOL finished) {
                [self cancelButtonAction: nil];
            }];
        }
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
