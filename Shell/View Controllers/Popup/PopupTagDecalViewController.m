//
//  PopupTagDecalViewController.m
//  Shell
//
//  Created by Jeremy Lua on 30/5/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupTagDecalViewController.h"
#import "Constants.h"
#import "LocalizationManager.h"
#import "CASCommonListCell.h"

#define CELL_HEIGHT 55
#define COMMON_LIST_CELL @"CASCommonListCell"

@interface PopupTagDecalViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, CASCommonListCellDelegate, WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblSubheader;

@property (weak, nonatomic) IBOutlet UIButton *btnTag;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *tableList;
@property (nonatomic, strong) NSMutableArray *resultArray;

@property (weak, nonatomic) IBOutlet UITextField *txtVehicle;

@property (weak, nonatomic) IBOutlet UITextField *searchTxtField;
@property NSMutableArray *defaultListArray;

@property (weak, nonatomic) IBOutlet UIView *popupView;

@property NSString *decalCode;
@end

@implementation PopupTagDecalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.lblHeader.font = FONT_H1;
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    self.lblHeader.text = LOCALIZATION(C_POPUP_DECAL_HEADER);       //lokalise 11 Feb
    
    self.lblSubheader.font = FONT_B1;
    self.lblSubheader.textColor = COLOUR_VERYDARKGREY;
    self.lblSubheader.text = LOCALIZATION(C_DECAL_REGISTERDECAL);   //lokalise 11 Feb
    
    self.txtVehicle.font = FONT_B1;
    self.txtVehicle.textColor = COLOUR_VERYDARKGREY;
    self.txtVehicle.placeholder = LOCALIZATION(C_SEARCHFIELD_VEHICLEID);    //lokalised
    
    self.tableView.tableFooterView = [UIView new];
    
    self.searchTxtField.font = FONT_B1;
    self.searchTxtField.textColor = COLOUR_VERYDARKGREY;
    self.searchTxtField.placeholder = LOCALIZATION(C_DROPDOWN_SEARCHPLACEHOLDER);  //lokalised
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = CELL_HEIGHT;
    
    self.resultArray = [[NSMutableArray alloc] init];
    
    
    [self.btnTag setTitle: LOCALIZATION(C_DECAL_TAGTOVEHICLE) forState:UIControlStateNormal];       //lokalise 11 Feb
    [self.btnTag.titleLabel setFont: FONT_BUTTON];
    [self.btnTag setBackgroundColor:COLOUR_RED];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
}


-(void) initDecalPopupWithDecalCode:(NSString*)decalCode
{
    self.decalCode = decalCode;
    //    self.lblHeader.accessibilityLabel = @"Accessibility Label"; //replace label
    //    self.lblHeader.accessibilityValue = @"Accessibility Value"; //2nd voice
    self.lblHeader.accessibilityValue = LOCALIZATION(@"Dropdown Menu Header");
    //    self.lblHeader.accessibilityHint = LOCALIZATION(@"Dropdown Menu Header"); //3rd voice
    
    NSArray *listArray =  [mSession getStateNames: GET_LOADSTATE];
    
    self.tableList = [NSMutableArray arrayWithArray:listArray];
    self.defaultListArray = [NSMutableArray arrayWithArray:listArray];
    
//    [self setHasSearchField: hasSearchField]; //pass the same parameter in.
    
    self.tableViewHeightConstraint.constant = MIN(SCREEN_HEIGHT - 178, CELL_HEIGHT * [listArray count]);
    
    [self.view layoutIfNeeded];
    UIAccessibilityPostNotification(UIAccessibilityScreenChangedNotification, self.lblHeader);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tableList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CASCommonListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"listItem"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:COMMON_LIST_CELL owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    
    [cell setTableText:self.tableList[indexPath.row]];
    
    cell.accessibilityLabel = self.tableList[indexPath.row];
    cell.isAccessibilityElement = YES;
    cell.delegate = self;
    cell.tag = indexPath.row + 1;
    
    return cell;
}

- (IBAction)doneButtonAction:(id)sender
{
    NSString *stateIdString = [mSession convertToStateKeyCode:
                         [self.resultArray firstObject]
                                 stateDetailedListArray: GET_LOADSTATE];
    
    NSInteger stateId = - 1;
    if (![stateIdString isEqualToString: @""])
        stateId = [stateIdString intValue];
    
    
    NSDictionary *params = @{@"Codes": self.decalCode,
                             @"VehicleNumber": self.txtVehicle.text,
                             @"StateId": @(stateId)};

    [[WebServiceManager sharedInstance] tagVehicleWithDecalCode:params vc:self];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_TAGVEHICLETODECAL:
            
            [self.delegate tagDecalWithVehicleId: self.txtVehicle.text stateName: [self.resultArray firstObject]];
            //    [self.delegate dropDownSelection: self.resultArray];
            
            //dismiss
            [self willMoveToParentViewController:nil];
            [self.view removeFromSuperview];
            [self removeFromParentViewController];
            break;
            
        default:
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.resultArray removeAllObjects];
    [self.resultArray addObject:[self.tableList objectAtIndex:indexPath.row]];
    
    self.searchTxtField.text = [self.tableList objectAtIndex:indexPath.row];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"SELF CONTAINS[cd] %@", [NSString stringWithFormat: @"%@", self.searchTxtField.text]];
    
    self.tableList = [self.defaultListArray filteredArrayUsingPredicate:predicate];
    
    [tableView reloadData];
    
    [self.view endEditing: YES];
//    [self doneButtonAction:nil];
}

-(void) setList:(NSArray *)list
{
    self.tableList = [NSMutableArray arrayWithArray:list];
    self.defaultListArray = [NSMutableArray arrayWithArray:list];
    [self.tableView reloadData];
    [self viewDidLayoutSubviews];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.searchTxtField) //state
    {
        self.popupView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -100);
    }
    else if (textField == self.txtVehicle)
    {
        self.popupView.transform = CGAffineTransformIdentity;
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.popupView.transform = CGAffineTransformIdentity;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.searchTxtField) //state
    {
        NSRange textFieldRange = NSMakeRange(0, [textField.text length]);
        if (NSEqualRanges(range, textFieldRange) && [string length] == 0)
        {
            self.tableList = [NSMutableArray arrayWithArray: self.defaultListArray];
        }
        else
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat: @"SELF CONTAINS[cd] %@", [NSString stringWithFormat: @"%@%@", self.searchTxtField.text, string]];
            
            self.tableList = [self.defaultListArray filteredArrayUsingPredicate:predicate];
            
        }
        [self.tableView reloadData];
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
