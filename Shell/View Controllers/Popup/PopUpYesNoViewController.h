//
//  PopUpYesNoViewController.h
//  Shell
//
//  Created by Jeremy Lua on 15/5/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PopupBaseVC.h"

//@protocol PopUpYesNoDelegate <NSObject>
//- (void)yesPressed;
//- (void)noPressed;
//@end

@interface PopUpYesNoViewController : PopupBaseVC
//@property id<PopUpYesNoDelegate> delegate;

-(id) initWithTitle:(NSString *)title content:(NSString *)contentString yesBtnTitle:(NSString *)yesBtnTitle noBtnTitle:(NSString *)noBtnTitle;
-(id) initWithTitle:(NSString *)title content:(NSString *)contentString yesBtnTitle:(NSString *)yesBtnTitle noBtnTitle:(NSString *)noBtnTitle onYesPressed:(CompletionBlock)yesBlock onNoPressed:(CompletionBlock)noBlock;
-(id) initWithAttributedTitle:(NSAttributedString*)title content:(NSAttributedString *)contentString yesBtnTitle:(NSAttributedString *)yesBtnTitle noBtnTitle:(NSAttributedString *)noBtnTitle;
-(id) initWithAttributedTitle:(NSAttributedString *)title content:(NSAttributedString *)contentString yesBtnTitle:(NSAttributedString *)yesBtnTitle noBtnTitle:(NSAttributedString *)noBtnTitle onYesPressed:(CompletionBlock)yesBlock onNoPressed:(CompletionBlock)noBlock;
@end
