//
//  OfferDetailsViewController.m
//  Shell
//
//  Created by Admin on 27/2/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "OfferDetailsViewController.h"
#import "WorkshopOfferDetailRedemptionPopup.h"

#define START_ANGLE -90 //12 o clock
#define CELL_HEIGHT 60

@interface OfferDetailsViewController ()<UITableViewDataSource,UITableViewDelegate,WebServiceManagerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblPeriodHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblPeriod;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfCustomerHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfCustomer;
@property (weak, nonatomic) IBOutlet UILabel *lblOfferTypeHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblOfferType;
@property (weak, nonatomic) IBOutlet UILabel *lblRedemptionOverviewHeader;
@property (weak, nonatomic) IBOutlet TableViewWithEmptyView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lblDateHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleIdHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblRedemptionHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblRedeemPercentage;
@property (weak, nonatomic) IBOutlet UILabel *lblRedeemText;
@property (weak, nonatomic) IBOutlet UIView *redeemptionInnerCicle;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberRedeemed;
@property (weak, nonatomic) IBOutlet UILabel *lblRedeem2;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberSent;
@property (weak, nonatomic) IBOutlet UILabel *lblRedeem3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeight;
@property (weak, nonatomic) IBOutlet UIView *innerCircleView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *innerCircleHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *innerCicleWidth;
@property (weak, nonatomic) IBOutlet UILabel *lblPromotionTypeHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblPromotionType;
@property (weak, nonatomic) IBOutlet UILabel *lblPromotionNameHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblPromotionName;


@property WorkshopOfferDetailRedemptionPopup *popupBox;
@property NSMutableArray *offerDetailsArray;
@end

@implementation OfferDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
//    self.title = LOCALIZATION(C_RUSSIA_OFFERS_OFFERDETAILS);    //lokalised 11 Feb
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_RUSSIA_OFFERS_OFFERDETAILS) subtitle: @"" size:15 subtitleSize:0]; //lokalised 11 Feb
    
    [self setupInterface];
    
    [[WebServiceManager sharedInstance]fetchOfferDetailVc:self offerDict:self.offerDict];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_RUSSIA_OFFERS_OFFERDETAILS) screenClass:nil];
}

#define degreesToRadians(degrees)((M_PI * degrees)/180)

-(void)setupCircleView : (CGFloat)percent
{
    
    NSInteger innerLength = SCREEN_WIDTH/2.5;
    self.innerCicleWidth.constant = innerLength;
    self.innerCircleHeight.constant = innerLength;
    
    [self.innerCircleView.layer setCornerRadius: innerLength/2];
    
    CGFloat percentage = percent;
    
    CGFloat endAngleDegrees = START_ANGLE + (360 * percentage * 0.01);
    
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    [bezierPath addArcWithCenter: CGPointMake(self.innerCicleWidth.constant / 2, self.innerCicleWidth.constant / 2)
                          radius: (self.innerCicleWidth.constant / 2) + 10
                      startAngle: degreesToRadians(START_ANGLE)
                        endAngle: degreesToRadians(endAngleDegrees)
                       clockwise:YES];
    
    CAShapeLayer *progressLayer = [[CAShapeLayer alloc] init];
    [progressLayer setPath:bezierPath.CGPath];
    [progressLayer setStrokeColor: [COLOUR_RED CGColor]];
    [progressLayer setFillColor:[UIColor clearColor].CGColor];
    [progressLayer setLineWidth:10];
    [self.innerCircleView.layer addSublayer:progressLayer];
    
    self.lblRedeemPercentage.font = FONTBOLD(SCREEN_WIDTH/7);
    self.redeemptionInnerCicle.backgroundColor = COLOUR_PALEGREY;
    self.lblRedeemText.font = FONT_B2;
    
    self.lblRedeemText.text = LOCALIZATION(C_RUSSIA_REDEEMED);  //lokalised 11 Feb
    
    self.lblNumberRedeemed.font = self.lblRedeemPercentage.font;
    self.lblNumberSent.font = self.lblRedeemPercentage.font;
    self.lblRedeem2.font = FONT_B2;
    self.lblRedeem3.font = FONT_B2;
    
    self.lblNumberRedeemed.textColor = COLOUR_RED;
    self.lblRedeem2.textColor = COLOUR_RED;
    
    self.lblRedeem2.text = LOCALIZATION(C_RUSSIA_REDEEMED); //lokalised 11 Feb
    self.lblRedeem3.text = LOCALIZATION(C_RUSSIA_SENT);     //lokalised 11 Feb
}


-(void)setupInterface
{
     self.lblPeriodHeader.text = LOCALIZATION(C_RUSSIA_OFFERS_PROMOPERIOD);             //lokalised 11 Feb
    self.lblNumberOfCustomerHeader.text = LOCALIZATION(C_RUSSIA_OFFERS_PROMOELIGIBLE);  //lokalised 11 Feb
    self.lblOfferTypeHeader.text = LOCALIZATION(C_RUSSIA_OFFERS_PROMOTYPE);             //lokalised 11 Feb
    self.lblPromotionTypeHeader.text = LOCALIZATION(C_RUSSIA_OFFERS_PROMOTYPE);         //lokalised 11 Feb
    self.lblPromotionNameHeader.text = LOCALIZATION(C_RUSSIA_OFFERS_PROMONAME);         //lokalised 11 Feb
    
    self.lblPeriod.text = @" ";
    self.lblNumberOfCustomer.text = @" ";
    self.lblOfferType.text = @" ";
    self.lblPromotionName.text = @" ";
    self.lblPromotionType.text = @" ";
    
    self.lblDateHeader.font = FONT_H2;
    self.lblVehicleIdHeader.font = self.lblDateHeader.font;
    self.lblRedemptionHeader.font = self.lblDateHeader.font;
    
    self.lblRedemptionOverviewHeader.text = LOCALIZATION(C_RUSSIA_REDEMPTIONOVERVIEW);  //lokalised 11 Feb
    
    self.lblDateHeader.text = LOCALIZATION(C_PROMO_REDEEMEDDATE);                       //lokalised 11 Feb
    self.lblVehicleIdHeader.text = LOCALIZATION(C_SEARCHVEHICLE_VEHICLEID);             //lokalised 11 Feb
    self.lblRedemptionHeader.text = LOCALIZATION(C_RUSSIA_REDEMPTION_VERIFICATIONNUM);  //lokalised 11 Feb
    
    //dont need refresh
    if(@available(iOS 10, *))
        self.tableView.refreshControl = nil;
    [self.tableView.customRefreshControl removeFromSuperview];
    self.tableView.customRefreshControl = nil;
    
//    PlaceholderView *vwPlaceholder = [[PlaceholderView alloc]initWithView:self.tableView withOffset: CELL_HEIGHT];
    PlaceholderView *vwPlaceholder = [[PlaceholderView alloc]initWithView:self.tableView withOffset: CELL_HEIGHT];
    [vwPlaceholder setBackgroundColor:[UIColor clearColor]];
    self.tableView.emptyView = vwPlaceholder;

    self.tableView.tableFooterView = [UIView new];
    self.tableHeight.constant = self.offerDetailsArray.count * CELL_HEIGHT + CELL_HEIGHT;
    
    if (kIsRightToLeft) {
        [self.btnBack setImage: [UIImage imageNamed:@"icon-arrsingleR.png"] forState: UIControlStateNormal];
    } else {
        [self.btnBack setImage: [UIImage imageNamed: @"icon-arrsingleL.png"] forState: UIControlStateNormal];
    }
}

-(void)setupInterfaceWithData :(NSDictionary *)responseDict
{
    if (ISNULL(responseDict))
    {
        [self setupCircleView: 0];
        return;
    }
    
//    self.lblPeriod.text = [NSString stringWithFormat:@"%@ %@ %@",[self nullChecker:[responseDict objectForKey:@"StartDate"]],LOCALIZATION(@"-"),[self nullChecker:[responseDict objectForKey:@"EndDate"]]];
    self.lblPeriod.text = [NSString stringWithFormat:@"%@",[self nullChecker:[responseDict objectForKey:@"OfferPeriod"]]];
    self.lblNumberOfCustomer.text = [self nullChecker:[NSString stringWithFormat:@"%@",[responseDict objectForKey:@"TargetCustomer"]]];
    self.lblOfferType.text = [self nullChecker:[responseDict objectForKey:@"OfferCategoryTypeDesc"]];
  
    self.lblPromotionName.text = [self nullChecker:[responseDict objectForKey:@"OfferTitle"]];
    self.lblPromotionType.text = [self nullChecker:[responseDict objectForKey:@"OfferTypeDescription"]];
    
    self.lblRedeemPercentage.text = [self nullChecker:[NSString stringWithFormat:@"%@",[responseDict objectForKey:@"RedemptionPercentage"]]];
    self.lblNumberRedeemed.text = [self nullChecker:[NSString stringWithFormat:@"%@",[responseDict objectForKey:@"Redemptions"]]];
    self.lblNumberSent.text = [self nullChecker:[NSString stringWithFormat:@"%@",[responseDict objectForKey:@"TotalSent"]]];
    
    NSInteger percent = ([[responseDict objectForKey:@"RedemptionPercentage"] integerValue] == nil)?0:[[responseDict objectForKey:@"RedemptionPercentage"] integerValue];
    [self setupCircleView: percent];
}

-(NSString *)nullChecker:(NSString *)stringValue
{
    if([stringValue isEqual:[NSNull null]]|| stringValue == nil){
        return @" ";
    }else
        return stringValue;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnPressed:(id)sender {
    [self popSelf];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.offerDetailsArray.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CELL_HEIGHT;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"HeaderCell"];
    
    UILabel *lblNo = [cell viewWithTag: 1];
    lblNo.text = LOCALIZATION(C_POINTS_DATE);   //lokalised 11 Feb
    lblNo.font = FONT_B1;
    
    UILabel *lblProductUsed = [cell viewWithTag: 2];
    lblProductUsed.text = LOCALIZATION(C_SEARCHVEHICLE_VEHICLEID);  //lokalised 11 Feb
    lblProductUsed.font = FONT_B1;
    
    UILabel *lblVerificationId = [cell viewWithTag: 3];
    lblVerificationId.text = LOCALIZATION(C_RUSSIA_REDEMPTION_VERIFICATIONNUM); //lokalised 11 Feb
    lblVerificationId.font = FONT_B1;
    
    return cell.contentView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"OfferDetailsCell"];
    
    NSDictionary *cellData = [self.offerDetailsArray objectAtIndex: indexPath.row];
    
    UILabel *lblDate = [cell viewWithTag: 1];
    lblDate.text = [cellData objectForKey:@"RedeemDate"];
    
    UILabel *lblVehicleId = [cell viewWithTag: 2];
    lblVehicleId.text = [cellData objectForKey:@"VehicleNumber"];
    
    UILabel *lblRedemptionVerfication = [cell viewWithTag: 3];
    lblRedemptionVerfication.text = [cellData objectForKey:@"RedemptionOrderReference"];
    lblRedemptionVerfication.textColor = COLOUR_RED;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.popupBox = [[WorkshopOfferDetailRedemptionPopup alloc] initOfferDetailRedemptionDetail:[self.offerDetailsArray objectAtIndex:indexPath.row]];
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: _popupBox];
    _popupBox.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_popupBox]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (_popupBox)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_popupBox]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (_popupBox)]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

- (void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_OFFERDETAILS:{
            [self setupInterfaceWithData:[response getGenericResponse]];
            self.offerDict = [response getGenericResponse];
            if (![[response getGenericResponse] isEqual:[NSNull null]])
            {
                if([[[response getGenericResponse] objectForKey:@"RedeemedCustomersList"] isEqual:[NSNull null]] || [[response getGenericResponse] objectForKey:@"RedeemedCustomersList"] == nil)
                    return;
                
                
                self.offerDetailsArray = [[NSMutableArray alloc]initWithArray: [[response getGenericResponse] objectForKey:@"RedeemedCustomersList"]];
                
                if (self.offerDetailsArray.count > 0)
                    self.tableHeight.constant = self.offerDetailsArray.count * CELL_HEIGHT + CELL_HEIGHT;
                else
                    self.tableHeight.constant = CELL_HEIGHT + CELL_HEIGHT; //header + space for placeholder
                [self.tableView reloadData];
            }
        }
            break;
            
        case kWEBSERVICE_CANCELANCILLARY:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                //refresh self
                [mSession pushRUOfferDetailsView: self offerDict: self.offerDict];
            }];
        }
            break;
        default:
            break;
    }
}

-(void) popSelf
{
    //0 index will always be offer list
    [self popToIndex: 0];
}
@end
