//
//  PastOffersViewController.m
//  Shell
//
//  Created by Admin on 26/2/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PastOffersViewController.h"
#import "CurrentOfferHeaderTableViewCell.h"
#import "CurrentOffersTableViewCell.h"

@interface PastOffersViewController ()<UITableViewDelegate,UITableViewDataSource,CommonListDelegate,WebServiceManagerDelegate,NativeDatePickerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *pastOfferTableview;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property (weak, nonatomic) IBOutlet UIButton *btnStartDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEndDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEnter;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UILabel *lblDatePicker;

@property NSMutableArray *pastOffersArray;
@end

@implementation PastOffersViewController
@synthesize listView,nativeDatePickerView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupInterface];
    [[WebServiceManager sharedInstance] fetchPastOfferVc:self startDate:@"" endDate:@"" offerType:@""];
}

-(void)setupInterface
{
    [self.pastOfferTableview registerNib:[UINib nibWithNibName:@"CurrentOffersTableViewCell" bundle:nil] forCellReuseIdentifier:@"CurrentOffersTableViewCell"];
    [self.pastOfferTableview registerNib:[UINib nibWithNibName:@"CurrentOfferHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"CurrentOfferHeaderTableViewCell"];

    self.pastOfferTableview.tableFooterView = [UIView new];
    
    self.btnSearch.titleLabel.font = FONT_B1;
    
    self.btnStartDate.titleLabel.font = FONT_B2;
    self.btnEndDate.titleLabel.font = self.btnStartDate.titleLabel.font;
    self.lblTo.font = FONT_B2;
    self.lblTo.text = LOCALIZATION(C_POINTS_TO);  //lokalised 11 Feb
    
    [self.btnStartDate setTitle:LOCALIZATION(C_POINTS_STARTDATE) forState:UIControlStateNormal];    //lokalised 11 Feb
    [self.btnEndDate setTitle:LOCALIZATION(C_POINTS_ENDDATE) forState:UIControlStateNormal];        //lokalised 11 Feb
    
    self.btnEnter.backgroundColor = COLOUR_RED;
    self.btnEnter.titleLabel.font = FONT_H2;
    [self.btnEnter setTitle:LOCALIZATION(C_POINTS_GO) forState:UIControlStateNormal];   //lokalised 11 Feb
    
    self.datePickerView.hidden = YES;
    
}

-(void)setupInterfaceWithResponse:(NSArray *)responseArray
{
    self.pastOffersArray = [[NSMutableArray alloc]initWithArray:responseArray];
    [self.pastOfferTableview reloadData];
}

- (IBAction)searchBtnPressed:(id)sender {
    listView = [[CASCommonListView alloc]initWithTitle:LOCALIZATION(@"Search By") list:[mSession getRedemptionType] selectionType:ListSelectionTypeSingle previouslySelected:nil];  //later lokalise
    listView.delegate = self;
    
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        [self.btnSearch setTitle:[NSString stringWithFormat:@"Search By: %@",selection.firstObject] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.pastOffersArray.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        CurrentOfferHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CurrentOfferHeaderTableViewCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        CurrentOffersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CurrentOffersTableViewCell"];
        [cell setupInterface:[self.pastOffersArray objectAtIndex:indexPath.row-1]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row>=1){
        NSString *offerID = [[self.pastOffersArray objectAtIndex:indexPath.row-1] objectForKey:@"OfferID"];
        NSString *offerType = [[self.pastOffersArray objectAtIndex:indexPath.row-1] objectForKey:@"OfferType"];
        NSString *startDate = [[self.pastOffersArray objectAtIndex:indexPath.row-1] objectForKey:@"StartDate"];
        NSString *endDate = [[self.pastOffersArray objectAtIndex:indexPath.row-1] objectForKey:@"EndDate"];
        NSDictionary *offerDict = @{@"offerID" : offerID,
                                    @"offerType" : offerType,
                                    @"startDate" : startDate,
                                    @"endDate" : endDate
                                    };
        [mSession pushOfferDetailsView:self.navigationController.parentViewController offerDict:offerDict];
    }
}
- (IBAction)startDateBtnPressed:(id)sender {
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];
    self.datePicker.minimumDate = nil;
    
    //date cannot be greater than end date
    NSDate *endMaxDate = [outputFormatter dateFromString: self.btnEndDate.titleLabel.text];
    if (endMaxDate){
         self.nativeDatePickerView = [[NativeDatePicker alloc]initWithDatepickerTitle:LOCALIZATION(C_ORDER_STARTDATE) maximumDate:endMaxDate minimumDate:nil inputDateFormatter:outputFormatter outputDateFormatter:outputFormatter];   //lokalised 11 Feb
        self.nativeDatePickerView.delegate = self;
    }
    else{
        self.nativeDatePickerView = [[NativeDatePicker alloc]initWithDatepickerTitle:LOCALIZATION(C_ORDER_ENDDATE) maximumDate:[NSDate date] minimumDate:nil inputDateFormatter:outputFormatter outputDateFormatter:outputFormatter];   //lokalised 11 Feb
        self.nativeDatePickerView.delegate = self;
    }
    self.nativeDatePickerView.tag = 1;
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: nativeDatePickerView];
    nativeDatePickerView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[nativeDatePickerView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (nativeDatePickerView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[nativeDatePickerView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (nativeDatePickerView)]];
    
}
- (IBAction)endDateBtnPressed:(id)sender {
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];
    
    //date cannot be lesser than start date
    NSDate *startMinDate = [outputFormatter dateFromString: self.btnStartDate.titleLabel.text];
    if (startMinDate){
        self.nativeDatePickerView = [[NativeDatePicker alloc]initWithDatepickerTitle: LOCALIZATION(C_POINTS_ENDDATE) maximumDate:[NSDate date] minimumDate:startMinDate inputDateFormatter:outputFormatter outputDateFormatter:outputFormatter];    //lokalised 11 Feb
        self.nativeDatePickerView.delegate = self;
        
    }else{
        self.nativeDatePickerView = [[NativeDatePicker alloc]initWithDatepickerTitle:LOCALIZATION(C_POINTS_ENDDATE) maximumDate:[NSDate date] minimumDate:nil inputDateFormatter:outputFormatter outputDateFormatter:outputFormatter];  //lokalised 11 Feb
        self.nativeDatePickerView.delegate = self;
    }
    
    self.nativeDatePickerView.tag = 2;
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: nativeDatePickerView];
    nativeDatePickerView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[nativeDatePickerView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (nativeDatePickerView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[nativeDatePickerView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (nativeDatePickerView)]];
}

-(void)getDateAsString:(NSString *)dateString
{
    switch (self.nativeDatePickerView.tag) {
        case 1:{
            [self.btnStartDate setTitle:dateString forState:UIControlStateNormal];
        }
            break;
        
        case 2:{
            [self.btnEndDate setTitle:dateString forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
}

- (IBAction)datePickerDoneBtnPressed:(id)sender {
    self.datePickerView.hidden = YES;
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];

    if(self.datePicker.tag == 1)
        [self.btnStartDate setTitle:[outputFormatter stringFromDate:self.datePicker.date] forState:UIControlStateNormal];
    else
        [self.btnEndDate setTitle:[outputFormatter stringFromDate:self.datePicker.date] forState:UIControlStateNormal];
}

- (IBAction)enterBtnPressed:(id)sender {
    self.datePickerView.hidden = YES;

    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat:@"dd MMM yyyy"];

    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy/MM/dd"];

    NSDate *startDate = [inputFormatter dateFromString: self.btnStartDate.titleLabel.text];
    NSString *startDateString = @"";
    if (startDate) 
        startDateString = [outputFormatter stringFromDate: startDate];

    NSDate *endDate = [inputFormatter dateFromString: self.btnEndDate.titleLabel.text];
    NSString *endDateString = @"";
    if (endDate)
        endDateString = [outputFormatter stringFromDate: endDate];

    NSString *typeOffer = self.btnSearch.titleLabel.text;
    NSString *typeCode = [typeOffer stringByReplacingOccurrencesOfString:@"Search By: " withString:@""];
    NSString *offerType = [mSession convertToRedemptionTypeKeyCode:typeCode];

    [[WebServiceManager sharedInstance] fetchPastOfferVc:self startDate:startDateString endDate:endDateString offerType:offerType];
//    [mSession pushCalanderView:self.navigationController.parentViewController];
}

- (void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_PASTOFFER:{
            if([[response getGenericResponse] isEqual:[NSNull null]] || [response getGenericResponse] == nil)
                return;
            
            [self setupInterfaceWithResponse:[[[response getGenericResponse] objectForKey:@"PastOffer"] isEqual:[NSNull null]]?@[]:[[response getGenericResponse] objectForKey:@"PastOffer"]];
        }
            break;
            
        default:
            break;
    }
}

- (void)processFailed:(WebServiceResponse *)response
{
    
}

@end
