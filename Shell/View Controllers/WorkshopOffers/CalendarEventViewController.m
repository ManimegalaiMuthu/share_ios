//
//  CalendarEventViewController.m
//  Shell
//
//  Created by Admin on 22/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "CalendarEventViewController.h"
#import "FSCalendar.h"
#import "FSCalendarAppearance.h"
#import "DIYCalendarCell.h"

#define SHELL_OFFERTYPE_INDEX @"4"

@interface CalendarEventViewController () <FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIScrollView *calendarScrollView;
@property (weak, nonatomic) IBOutlet UIView *legendView;
@property (weak, nonatomic) IBOutlet UIView *dateView;


@property (weak, nonatomic) IBOutlet FSCalendar *calendar;

@property NSMutableArray *shellPromotionsArray;
@property NSMutableArray *workshopPromotionsArray;

@property (weak, nonatomic) IBOutlet UILabel *lblShellPromotions;
@property (weak, nonatomic) IBOutlet UILabel *lblWorkshopPromotions;



// The start date of the range
@property (strong, nonatomic) NSDate *date1;
// The end date of the range
@property (strong, nonatomic) NSDate *date2;
@property (strong, nonatomic) NSCalendar *gregorian;

@property NSMutableDictionary *dateDict; // [date]: @[data].

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *promotionListTableViewHeight;
@property (weak, nonatomic) IBOutlet UITableView *promotionListTableView;
@property (weak, nonatomic) IBOutlet UILabel *lblDayDateHeader;
@property NSMutableArray *selectedDateArray;
@end

@implementation CalendarEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupCalendarInterface];
    
    self.lblDayDateHeader.font = FONT_B2;
    self.lblDayDateHeader.text = @"";
    
    
    self.lblShellPromotions.font = FONT_B2;
    self.lblShellPromotions.text = LOCALIZATION(@"Shell Promotions");    //later lokalise
    self.lblWorkshopPromotions.text = LOCALIZATION(@"Workshop Promotions"); //later lokalise
    self.lblWorkshopPromotions.font = self.lblShellPromotions.font;
    
    self.promotionListTableView.estimatedRowHeight = 70;

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    self.promotionListTableViewHeight.constant = 0;
    
//    for (UITableViewCell *cell in self.promotionListTableView.subviews)
//    {
//        self.promotionListTableViewHeight.constant += cell.frame.size.height;
//    }
//
//    self.calendarScrollView.contentSize = CGSizeMake(self.calendarScrollView.contentSize.width, self.calendar.frame.size.height +  self.legendView.frame.size.height + self.dateView.frame.size.height + self.promotionListTableViewHeight.constant);
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_WORKSHOPOFFER),LOCALIZATION_EN(C_RUSSIA_OFFERS_CALENDAR)] screenClass:nil];
}

-(void) setupCalendarInterface
{
    self.calendar.appearance.headerTitleFont = FONT_B1; //month header text
    self.calendar.appearance.headerTitleColor = COLOUR_VERYDARKGREY; //month header color
    
    self.calendar.appearance.weekdayFont = FONT_B2; //day text (Sun, Mon, etc.)
    self.calendar.appearance.weekdayTextColor = COLOUR_RED;
    
    self.calendar.appearance.titleFont = FONT_B2;   //date text (1-31)
    
    self.calendar.appearance.eventDefaultColor = COLOUR_RED;
    self.calendar.appearance.titleSelectionColor = COLOUR_VERYDARKGREY;
    self.calendar.appearance.todayColor = COLOUR_VERYDARKGREY;
    self.calendar.appearance.titleTodayColor = COLOUR_VERYDARKGREY;
    self.calendar.appearance.selectionColor = COLOUR_PALEGREY;
    
    self.calendar.delegate = self;
    self.calendar.dataSource = self;
    self.calendar.appearance.caseOptions = FSCalendarCaseOptionsWeekdayUsesSingleUpperCase;
    self.calendar.swipeToChooseGesture.enabled = NO;
    self.calendar.allowsMultipleSelection = NO;
    
    [self.calendar setLocale: [[NSLocale alloc] initWithLocaleIdentifier: GET_LOCALIZATION]];
    
//    [self.calendar registerClass:[DIYCalendarCell class] forCellReuseIdentifier:@"cell"];
    
    self.gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    self.shellPromotionsArray = [[NSMutableArray alloc] init];
    self.dateDict = [[NSMutableDictionary alloc] init];
}

-(void) reloadCalendar
{
    if (ISNULL(self.offersDataArray))
    {
        [self.dateDict removeAllObjects];
        [self.selectedDateArray removeAllObjects];
        
        [self.calendar reloadData];
        return;
    }
    for (NSDictionary *dict in self.offersDataArray)
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US_POSIX"];
        [formatter setDateFormat:@"yyyy/MM/dd"];
        NSDateComponents *oneDay = [NSDateComponents new];
        oneDay.day= 1;

        if ([dict objectForKey: @"StartDate"] && [dict objectForKey: @"EndDate"])
        {
            NSDate *loopDate = [formatter dateFromString: [dict objectForKey: @"StartDate"]];

            NSDate *endDate = [formatter dateFromString: [dict objectForKey: @"EndDate"]];
            
            while (loopDate != endDate)
            {
                NSDictionary *data = @{@"OfferType": [dict objectForKey: @"OfferType"],
                                       @"OfferTitle": [dict objectForKey: @"OfferTitle"],
                                       @"OfferID": [dict objectForKey: @"OfferID"],
                                       @"StartDate": [dict objectForKey: @"StartDate"],
                                       @"EndDate": [dict objectForKey: @"EndDate"],
                                       };
//                if ([[formatter stringFromDate: loopDate] isEqualToString: @"26/04/18"])
//                {
//                    YES;
//                }
                if ([self.dateDict objectForKey: [formatter stringFromDate: loopDate]])
                {
                    NSMutableArray *dateArray = [self.dateDict objectForKey: [formatter stringFromDate: loopDate]];
                    [dateArray addObject: data];
                }
                else
                {
                    NSMutableArray *dateArray =  [[NSMutableArray alloc] initWithObjects: data, nil];
                    [self.dateDict setObject: dateArray forKey: [formatter stringFromDate: loopDate]];
                }
                
                loopDate = [self.gregorian dateByAddingComponents: oneDay toDate:loopDate options:0];
            }
        }
    }
    
    if (!self.calendar.selectedDate)
    {
        [self.calendar selectDate: [NSDate date]];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy/MM/dd"];
        
        if ([[self.dateDict allKeys] containsObject: [formatter stringFromDate: self.calendar.selectedDate]])
        {
            self.selectedDateArray = [self.dateDict objectForKey: [formatter stringFromDate: self.calendar.selectedDate]];
        }
        else
        {
            [self.selectedDateArray removeAllObjects];
        }
        
        [UIView animateWithDuration:0 animations:^{
            [self.promotionListTableView reloadData];
            [self.promotionListTableView layoutIfNeeded];
        } completion:^(BOOL finished) {
            self.promotionListTableViewHeight.constant = 0;
            
            for (UITableViewCell *cell in self.promotionListTableView.subviews)
            {
                self.promotionListTableViewHeight.constant += cell.frame.size.height;
            }
            
            self.calendarScrollView.contentSize = CGSizeMake(self.calendarScrollView.contentSize.width, self.calendar.frame.size.height +  self.legendView.frame.size.height + self.dateView.frame.size.height + self.promotionListTableViewHeight.constant);
        }];
        
        NSDateFormatter *headerFormatter = [[NSDateFormatter alloc] init];
        [headerFormatter setDateFormat:@"EEEE, d MMM"];
        
        self.lblDayDateHeader.text = [headerFormatter stringFromDate: self.calendar.selectedDate];
    }
    
    [self.calendar reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.selectedDateArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PromotionOfferCell"];
    
    NSDictionary *cellData = [self.selectedDateArray objectAtIndex: indexPath.row];
    
    UIView *indicatorDotView = [cell viewWithTag: 1];
    if ([[cellData objectForKey: @"OfferType"] isEqualToString: SHELL_OFFERTYPE_INDEX])
        indicatorDotView.backgroundColor = COLOUR_YELLOW;
    else
        indicatorDotView.backgroundColor = COLOUR_VERYDARKGREY;
    
    UILabel *lblPromotion = [cell viewWithTag: 2];
    lblPromotion.font = FONT_B2;
    lblPromotion.text = [cellData objectForKey: @"OfferTitle"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cellData = [self.selectedDateArray objectAtIndex: indexPath.row];
    
    NSString *offerID = [cellData objectForKey:@"OfferID"];
    NSString *offerType = [cellData objectForKey:@"OfferType"];
    NSString *startDate = [cellData objectForKey:@"StartDate"];
    NSString *endDate = [cellData objectForKey:@"EndDate"];
    NSDictionary *offerDict = @{@"offerID" : offerID,
                                @"offerType" : offerType,
                                @"startDate" : startDate,
                                @"endDate" : endDate
                                };
    
    
//    [mSession pushOfferDetailsView:self.parentViewController offerDict:offerDict];
    
    [mSession pushRUOfferDetailsView: self.parentViewController offerDict:offerDict];
}

#pragma mark Calendar configuration
- (UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance fillSelectionColorForDate:(NSDate *)date
{
    return COLOUR_PALEGREY;
}

- (UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance fillDefaultColorForDate:(NSDate *)date
{
    return COLOUR_WHITE;
}

//radius of circle
- (CGFloat)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance borderRadiusForDate:(nonnull NSDate *)date
{
    return 0.0;
}

//default colour
- (NSArray *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance eventDefaultColorsForDate:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    
    if ([[self.dateDict allKeys] containsObject: [formatter stringFromDate: date]])
    {
        NSArray *data = [self.dateDict objectForKey: [formatter stringFromDate: date]];
        NSMutableArray *colour = [[NSMutableArray alloc] init];
        
        //check in order
        for (NSDictionary *dict in data)
        {
            if ([[dict objectForKey: @"OfferType"] isEqualToString: SHELL_OFFERTYPE_INDEX] && ![colour containsObject: COLOUR_YELLOW])
            {
                [colour addObject:COLOUR_YELLOW];
            }
        }
        for (NSDictionary *dict in data)
        {
            if ([[dict objectForKey: @"OfferType"] isEqualToString: @"2"] && ![colour containsObject: COLOUR_DARKGREY])
            {
                [colour addObject:COLOUR_DARKGREY];
                
            }
        }
        return colour;
    }
    else
        return nil;
}

- (NSArray<UIColor *> *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance eventSelectionColorsForDate:(nonnull NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    
    if ([[self.dateDict allKeys] containsObject: [formatter stringFromDate: date]])
    {
        NSArray *data = [self.dateDict objectForKey: [formatter stringFromDate: date]];
        NSMutableArray *colour = [[NSMutableArray alloc] init];
        
        //check in order
        for (NSDictionary *dict in data)
        {
            if ([[dict objectForKey: @"OfferType"] isEqualToString: SHELL_OFFERTYPE_INDEX] && ![colour containsObject: COLOUR_YELLOW])
            {
                [colour addObject:COLOUR_YELLOW];
            }
        }
        for (NSDictionary *dict in data)
        {
            if ([[dict objectForKey: @"OfferType"] isEqualToString: @"2"] && ![colour containsObject: COLOUR_DARKGREY])
            {
                [colour addObject:COLOUR_DARKGREY];
                
            }
        }
        return colour;
    }
    else
        return nil;
}

- (UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance eventColorForDate:(NSDate *)date
{
    return nil;
}

//configure date with dots here
- (NSInteger)calendar:(FSCalendar *)calendar numberOfEventsForDate:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    
    if ([[self.dateDict allKeys] containsObject: [formatter stringFromDate: date]])
    {
        NSArray *data = [self.dateDict objectForKey: [formatter stringFromDate: date]];
        NSMutableArray *colour = [[NSMutableArray alloc] init];
        
        //check in order
        for (NSDictionary *dict in data)
        {
            if ([[dict objectForKey: @"OfferType"] isEqualToString: SHELL_OFFERTYPE_INDEX] && ![colour containsObject: COLOUR_YELLOW])
            {
                [colour addObject:COLOUR_YELLOW];
            }
        }
        for (NSDictionary *dict in data)
        {
            if ([[dict objectForKey: @"OfferType"] isEqualToString: @"2"] && ![colour containsObject: COLOUR_DARKGREY])
            {
                [colour addObject:COLOUR_DARKGREY];
                
            }
        }
        return [colour count];
    }
    else
        return 0;
}
-(void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    
    if ([[self.dateDict allKeys] containsObject: [formatter stringFromDate: date]])
    {
        self.selectedDateArray = [self.dateDict objectForKey: [formatter stringFromDate: date]];
    }
    else
    {
        [self.selectedDateArray removeAllObjects];
    }
    
    [UIView animateWithDuration:0 animations:^{
        [self.promotionListTableView reloadData];
        [self.promotionListTableView layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.promotionListTableViewHeight.constant = 0;
        
        for (UITableViewCell *cell in self.promotionListTableView.subviews)
        {
            self.promotionListTableViewHeight.constant += cell.frame.size.height;
        }
        
        self.calendarScrollView.contentSize = CGSizeMake(self.calendarScrollView.contentSize.width, self.calendar.frame.size.height +  self.legendView.frame.size.height + self.dateView.frame.size.height + self.promotionListTableViewHeight.constant);
    }];
    
    NSDateFormatter *headerFormatter = [[NSDateFormatter alloc] init];
    [headerFormatter setDateFormat:@"EEEE, d MMM"];
    
    self.lblDayDateHeader.text = [headerFormatter stringFromDate: date];
    
}
//custom cell
//- (FSCalendarCell *)calendar:(FSCalendar *)calendar cellForDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
//{
//    DIYCalendarCell *cell = [calendar dequeueReusableCellWithIdentifier:@"cell" forDate:date atMonthPosition:monthPosition];
//    return cell;
//}

//- (FSCalendarCell *)calendar:(FSCalendar *)calendar cellForDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
//{
//    RangePickerCell *cell = [calendar dequeueReusableCellWithIdentifier:@"cell" forDate:date atMonthPosition:monthPosition];
//    return cell;
//}

//- (void)configureCell:(__kindof FSCalendarCell *)cell forDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)position
//{
//    RangePickerCell *rangeCell = cell;
//    if (position != FSCalendarMonthPositionCurrent) {
//        rangeCell.middleLayer.hidden = YES;
//        rangeCell.selectionLayer.hidden = YES;
//        return;
//    }
//    if (self.date1 && self.date2) {
//        // The date is in the middle of the range
//        BOOL isMiddle = [date compare:self.date1] != [date compare:self.date2];
//        rangeCell.middleLayer.hidden = !isMiddle;
//    } else {
//        rangeCell.middleLayer.hidden = YES;
//    }
//    BOOL isSelected = NO;
//    isSelected |= self.date1 && [self.gregorian isDate:date inSameDayAsDate:self.date1];
//    isSelected |= self.date2 && [self.gregorian isDate:date inSameDayAsDate:self.date2];
//    rangeCell.selectionLayer.hidden = !isSelected;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//FSCalendar doesn't update frame by itself, Please implement
//- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated
//{
//    self.calendarHeightConstraint.constant = CGRectGetHeight(bounds);
//    // Do other updates here
//    [self.view layoutIfNeeded];
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
