//
//  WorkshopOfferAllOfferListViewController.h
//  Shell
//
//  Created by Jeremy Lua on 26/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@protocol WorkshopOfferAllOfferDelegate <NSObject>
-(void) reloadAllOffersList:(NSArray *) data;
@end

@interface WorkshopOfferAllOfferListViewController : BaseVC
@property id<WorkshopOfferAllOfferDelegate> delegate;
@end
