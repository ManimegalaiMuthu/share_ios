//
//  WorkshopOfferTabViewController.m
//  Shell
//
//  Created by Admin on 20/2/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "WorkshopOfferTabViewController.h"
#import "AddNewWorkshopOfferViewController.h"
//#import "CurrentOfferViewController.h"
//#import "PastOffersViewController.h"
#import "RDVTabBarItem.h"
#import "PopupInfoViewController.h"
#import "WorkshopOfferAllOfferViewController.h"

@interface WorkshopOfferTabViewController (){
    AddNewWorkshopOfferViewController *addNewWorkshopOfferVc;
//    CurrentOfferViewController *currentOffersVc;
//    PastOffersViewController *pastOfferVc;
    
    
    WorkshopOfferAllOfferViewController *allOfferVc;
    
}
@property (strong, nonatomic) IBOutlet UIButton *btnIcon;

@end

@implementation WorkshopOfferTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupInterface];
    
    UIBarButtonItem *setting = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"icn-info_inactive.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(infoPressed)];
    self.navigationItem.rightBarButtonItem = setting;
}


- (void)infoPressed {
    if ([[mSession lookupTable] objectForKey: @"WorkShopOfferDesc"])
        [self popUpViewWithTitle:LOCALIZATION(C_RU_WHATISTHIS) content:[[mSession lookupTable] objectForKey: @"WorkShopOfferDesc"]];    //lokalised 11 Feb
    else
        [self popUpViewWithTitle:LOCALIZATION(C_RU_WHATISTHIS) content:LOCALIZATION(@"You can publish personalised offers to your customers, such as free or discounted services like car washes or gifts with every Shell Helix Oil Change.")];    //later lokalise
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setupInterface
{
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_WORKSHOPOFFER) subtitle: @"" size:15 subtitleSize:0]; //lokalised 11 Feb
    
    addNewWorkshopOfferVc  = [STORYBOARD_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_WORKSHOPOFFERADDNEW];
    UINavigationController *addNewNav = [[UINavigationController alloc] initWithRootViewController: addNewWorkshopOfferVc];
    [addNewNav setNavigationBarHidden: YES];
    [addNewWorkshopOfferVc view];
    
    allOfferVc  = [STORYBOARD_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_WORKSHOPOFFER_ALLOFFER];
    UINavigationController *allOfferNav = [[UINavigationController alloc] initWithRootViewController: allOfferVc];
    [allOfferNav setNavigationBarHidden: YES];
    [allOfferVc view];
    
//    currentOffersVc  = [STORYBOARD_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_WORKSHOPOFFER_CURRENTOFFER];
//    UINavigationController *currentOfferNav = [[UINavigationController alloc] initWithRootViewController: currentOffersVc];
//    [currentOfferNav setNavigationBarHidden: YES];
//    [currentOffersVc view];
//    
//    pastOfferVc  = [STORYBOARD_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_WORKSHOPOFFER_PASTOFFERSVIEW];
//    UINavigationController *pastOfferNav = [[UINavigationController alloc] initWithRootViewController: pastOfferVc];
//    [pastOfferNav setNavigationBarHidden: YES];
//    [pastOfferNav view];
    //addOrderViewController.delegate = self;
    
    //same VC but different data.
    //selecttab function have been tweaked locally for this case
    
    
    NSArray *title = @[
//                       LOCALIZATION(@"Current Offers"),
//                       LOCALIZATION(@"Offer History"),
                       LOCALIZATION(@"All Offers"),     //later lokalise
                       LOCALIZATION(C_WORKSHOPOFFER_ADDNEW),    //lokalised 11 Feb
                       ];
    [self setViewControllers: @[
//                                currentOfferNav,
//                                pastOfferNav,
                                allOfferNav,
                                addNewNav
                                ]];
    
    UIImage *selected = GET_ISADVANCE ? [UIImage imageNamed:@"tab-bg_blue"] : [UIImage imageNamed:@"tab-bg"];
    UIImage *unselected = [UIImage imageNamed:@"tab-bg_grey.png"];
    if (kIsRightToLeft) {
        title = title.reverseObjectEnumerator.allObjects.mutableCopy;
    }
    for (int i = 0; i < [self.tabBar.items count]; i++)
    {
        RDVTabBarItem *item = [self.tabBar.items objectAtIndex: i];
        [item setTitle: [title objectAtIndex: i]];
        
        
        [item setBackgroundColor: COLOUR_WHITE];
        [item setTitleUnselectedAttributes:FONT_H1 colour:COLOUR_VERYDARKGREY];
        [item setTitleSelectedAttributes:FONT_H1 colour:COLOUR_RED];
        
        [item setBackgroundSelectedImage: selected withUnselectedImage: unselected];
        
        
    }
    if (kIsRightToLeft) {
        [self tabBar:self.tabBar didSelectItemAtIndex:self.viewControllers.count-1];
    }
}

#pragma mark RDVTabBarDelegate
- (BOOL)tabBar:(RDVTabBar *)tabBar shouldSelectItemAtIndex:(NSInteger)index
{
    for (RDVTabBarItem *item in [tabBar items])
    {
        
    }
    return ([tabBar selectedItem] != [[tabBar items] objectAtIndex: index]);
}

- (void)tabBar:(RDVTabBar *)tabBar didSelectItemAtIndex:(NSInteger)index
{
    //super method MUST BE PRESENT
    [super tabBar:tabBar didSelectItemAtIndex: index];
    
    
    switch (index)
    {
            //        case 0:
            //            if (GET_PROFILETYPE == kPROFILETYPE_DSR)
            //            {
            //                [viewOrderViewController reloadWebview];
            //            }
            //            else
            //            {
            //                [addOrderViewController reloadWebview];
            //            }
            //            break;
            //        case 1:
            //            if (GET_PROFILETYPE == kPROFILETYPE_DSR)
            //            {
            //                [addOrderViewController reloadWebview];
            //            }
            //            else
            //            {
            //                [viewOrderViewController reloadWebview];
            //            }
            //            break;
    }
    
}

-(void) popUpViewWithPopupVC:(UIViewController *)popupVc
{
    [CURRENT_VIEWCONTROLLER addChildViewController: popupVc];
    [CURRENT_VIEWCONTROLLER.view addSubview:popupVc.view];
    [popupVc didMoveToParentViewController: CURRENT_VIEWCONTROLLER];
    
    popupVc.view.accessibilityViewIsModal = YES;
    popupVc.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
}

-(void) popUpViewWithTitle:(NSString *) title content:(NSString *)content
{
    [self.view endEditing: YES];
    PopupInfoViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPINFOVIEW];
    [self popUpViewWithPopupVC: popupVc];
    
    [popupVc initPopupInfoVCWithTitle: title content:content];
}
@end
