//
//  WorkshopOfferAllOfferViewController.m
//  Shell
//
//  Created by Jeremy Lua on 26/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "WorkshopOfferAllOfferViewController.h"
#import "WorkshopOfferAllOfferListViewController.h"

#import "CalendarEventViewController.h"

//#import "CurrentOfferViewController.h"

@interface WorkshopOfferAllOfferViewController () <WorkshopOfferAllOfferDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnCalendarView;
@property (weak, nonatomic) IBOutlet UIButton *btnListView;
@property (weak, nonatomic) IBOutlet UIView *vcView;
@property (weak, nonatomic) CalendarEventViewController *calendarVc;
//@property (weak, nonatomic) ListView *calendarVc;
@property (weak, nonatomic) WorkshopOfferAllOfferListViewController *offerListVc;
@end

@implementation WorkshopOfferAllOfferViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.btnCalendarView.titleLabel setFont: FONT_B2];
    [self.btnListView.titleLabel setFont: FONT_B2];
    
    self.calendarVc = [STORYBOARD_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_WORKSHOPOFFER_CALENDERVIEW];
    [self addChildViewController: self.calendarVc];
    
    self.offerListVc = [STORYBOARD_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_WORKSHOPOFFER_ALLLISTOFFER];
    self.offerListVc.delegate = self;
    [self addChildViewController: self.offerListVc];
    
    [self btnPressed: self.btnCalendarView]; //select calendar by default
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
}

- (IBAction)btnPressed:(UIButton *)sender
{
    if (sender == self.btnCalendarView && !self.btnCalendarView.selected)
    {
        self.btnCalendarView.selected = YES;
        [self.btnCalendarView setTitleColor: COLOUR_WHITE forState: UIControlStateNormal];
        self.btnCalendarView.backgroundColor = COLOUR_RED;
        
        self.btnListView.selected = NO;
        [self.btnListView setTitleColor: COLOUR_VERYDARKGREY forState: UIControlStateNormal];
        self.btnListView.backgroundColor = COLOUR_WHITE;
        
        [self.offerListVc.view removeFromSuperview];
        [self.vcView addSubview: self.calendarVc.view];
        [self.calendarVc.view setFrame: CGRectMake(0, 0, self.vcView.frame.size.width, self.vcView.frame.size.height)];
        
    }
    else if (sender == self.btnListView && !self.btnListView.selected)
    {
        self.btnListView.selected = YES;
        [self.btnListView setTitleColor: COLOUR_WHITE forState: UIControlStateNormal];
        self.btnListView.backgroundColor = COLOUR_RED;
        
        self.btnCalendarView.selected = NO;
        [self.btnCalendarView
         setTitleColor: COLOUR_VERYDARKGREY forState: UIControlStateNormal];
        self.btnCalendarView.backgroundColor = COLOUR_WHITE;
        
        [self.calendarVc.view removeFromSuperview];
        [self.vcView addSubview: self.offerListVc.view];
        [self.offerListVc.view setFrame: CGRectMake(0, 0, self.vcView.frame.size.width, self.vcView.frame.size.height)];
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) reloadAllOffersList:(NSArray *)data
{
    self.calendarVc.offersDataArray = data;
    [self.calendarVc reloadCalendar];
}

@end
