//
//  OfferConfirmationViewController.m
//  Shell
//
//  Created by Admin on 28/2/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "OfferConfirmationViewController.h"

@interface OfferConfirmationViewController ()<WebServiceManagerDelegate>
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lblPromotionNameHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblPromotionPeriodHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfCustomersHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblOfferTypeHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblPromotionName;
@property (weak, nonatomic) IBOutlet UILabel *lblPromotionPeriod;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfCustomer;
@property (weak, nonatomic) IBOutlet UILabel *lblOfferType;
@property (weak, nonatomic) IBOutlet UILabel *lblSMSMessageHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblSMSContent;
@property (weak, nonatomic) IBOutlet UILabel *lblConfirmAndPublish;
@property (weak, nonatomic) IBOutlet UIView *messagePreviewView;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;

@end

@implementation OfferConfirmationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    self.title = LOCALIZATION(C_RU_OFFERSUCCESS_TITLE); //lokalised 11 Feb
    [self setupInterface];
}

-(void)setupInterface
{
    self.lblPromotionNameHeader.font = FONT_H1;
    self.lblPromotionPeriodHeader.font = self.lblPromotionNameHeader.font;
    self.lblNumberOfCustomersHeader.font = self.lblPromotionNameHeader.font;
    self.lblOfferTypeHeader.font = self.lblPromotionNameHeader.font;
    
    self.lblPromotionNameHeader.text = LOCALIZATION(@"Promotion Name:");     //later lokalise
    self.lblPromotionPeriodHeader.text = LOCALIZATION(@"Promotion Period:");     //later lokalise
    self.lblNumberOfCustomersHeader.text = LOCALIZATION(@"Number of customers selected that will be contacted during the promotion period:");    //later lokalise
    self.lblOfferTypeHeader.text = LOCALIZATION(@"Offer Type:");     //later lokalise
    
    self.lblPromotionName.font = FONT_B1;
    self.lblPromotionPeriod.font = self.lblPromotionName.font;
    self.lblNumberOfCustomer.font = self.lblPromotionName.font;
    self.lblOfferType.font = self.lblPromotionName.font;
    
    
    self.lblPromotionName.text = [self.submitDict objectForKey:@"OfferTitle"];
    self.lblPromotionPeriod.text = [NSString stringWithFormat:@"%@ %@ %@",[self.submitDict objectForKey:@"StartDate"],LOCALIZATION(C_POINTS_TO),[self.submitDict objectForKey:@"EndDate"]]; //lokalised 11 Feb
    self.lblNumberOfCustomer.text = [NSString stringWithFormat:@"%@",[self.submitDict objectForKey:@"count"]];
    self.lblOfferType.text = [self.submitDict objectForKey:@"SelectedOfferTypeName"];
    
    self.lblSMSMessageHeader.font = FONT_H1;
    self.lblSMSContent.font = FONT_B2;
    
    self.lblSMSMessageHeader.text = LOCALIZATION(@"SMS Message Preview");    //later lokalise
    self.lblSMSContent.text = [self.submitDict objectForKey:@"msg"];
    
    self.lblConfirmAndPublish.font = FONT_B1;
    self.lblConfirmAndPublish.text = LOCALIZATION(@"Upon clicking Confirm and Publish, I acknowledge that the promotion SMS will be sent out to the selected group of customers and the offer cannot be changed.");  //later lokalise
    
    self.messagePreviewView.backgroundColor = COLOUR_PALEGREY;
    
    self.btnSubmit.backgroundColor = COLOUR_RED;
    self.btnSubmit.titleLabel.font = FONT_H1;
    [self.btnSubmit setTitle:LOCALIZATION(@"Confirm and Publish") forState:UIControlStateNormal];    //later lokalise
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_RU_OFFERSUCCESS_TITLE) screenClass:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)checkBoxBtnPressed:(id)sender {
    if(self.btnCheckBox.selected){
        [self.btnCheckBox setSelected:NO];
    }else{
        [self.btnCheckBox setSelected:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)backBtnPressed:(id)sender {
    [self popSelf];
}

- (IBAction)submitPressed:(id)sender {
    
    //    self.nextPageDict = @{@"EndDate" : endDate,
    //                          @"isFinalSubmit" : @(NO),
    //                          @"OfferInformation"     : rangeArray,
    //                          @"OfferOption" : @(offerOptions),
    //                          @"OfferCategoryCode" : offerCategoryCode,
    //                          @"OfferType" : @(offerType),
    //                          @"OfferTitle" : promoName,
    //                          @"StartDate" : startDate,
    //                          @"IsAcceptTerms" : @(NO),
    //                          @"OfferValue" : offerValue,
    //                          @"AllUser" : @(count),
    //                          @"SelectedOfferTypeName" : offerCategory,
    //                          };
    [self.submitDict setValue:@(self.btnCheckBox.selected) forKey:@"IsAcceptTerms"];
    [self.submitDict setValue:@(YES) forKey:@"isFinalSubmit"];
    [[WebServiceManager sharedInstance]submitToGetAncillaryOffer:self submitDict:self.submitDict];
}

- (IBAction)smsPreviewInfoPressed:(id)sender {
    if ([[mSession lookupTable] objectForKey: @"SmsPreviewDesc"])
        [self popUpViewWithTitle: LOCALIZATION(C_RU_WHATISTHIS) content: [[mSession lookupTable] objectForKey: @"SmsPreviewDesc"]]; //lokalised 11 Feb
    else
        [self popUpViewWithTitle:LOCALIZATION(C_RU_WHATISTHIS) content:LOCALIZATION(@"This is a sample SMS that your customers will receive when you publish the offer.")]; //later lokalise
    
}

- (void)processCompleted:(WebServiceResponse *)response
{
    [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion: nil];
}

- (void)processFailed:(WebServiceResponse *)response
{
    
}

@end
