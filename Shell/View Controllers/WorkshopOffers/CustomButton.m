//
//  CustomButton.m
//  Shell
//
//  Created by Admin on 28/2/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "CustomButton.h"
#import "Constants.h"

@implementation CustomButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)layoutSubviews
{
    [super layoutSubviews];
    if(self.imageView != nil)
    {
        if(SCREEN_WIDTH == iPHONE5WIDTH)
            self.imageEdgeInsets = UIEdgeInsetsMake(5, self.bounds.size.width/2+ 10, 5, 5);
        else if(SCREEN_WIDTH == iPHONE6PLUSWIDTH)
            self.imageEdgeInsets = UIEdgeInsetsMake(5, self.bounds.size.width/2 + 40, 5, 5);
        else
            self.imageEdgeInsets = UIEdgeInsetsMake(5, self.bounds.size.width/2 + 20, 5, 5);
//        self.imageEdgeInsets = UIEdgeInsetsMake(<#CGFloat top#>, <#CGFloat left#>, <#CGFloat bottom#>, <#CGFloat right#>)
        
    }
    
}
@end
