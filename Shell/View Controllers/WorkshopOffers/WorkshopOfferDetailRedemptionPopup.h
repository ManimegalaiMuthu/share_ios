//
//  WorkshopOfferDetailRedemptionPopup.h
//  Shell
//
//  Created by Admin on 1/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorkshopOfferDetailRedemptionPopup : UIView
- (id)initOfferDetailRedemptionDetail:(NSDictionary *)infoDict;
@end
