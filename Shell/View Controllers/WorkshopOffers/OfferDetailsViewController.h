//
//  OfferDetailsViewController.h
//  Shell
//
//  Created by Admin on 27/2/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface OfferDetailsViewController : BaseVC
@property NSDictionary *offerDict;

-(void)setupInterfaceWithData :(NSDictionary *)responseDict;
@end
