//
//  CurrentOffersTableViewCell.h
//  Shell
//
//  Created by Admin on 26/2/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurrentOffersTableViewCell : UITableViewCell
-(void) setupInterface :(NSDictionary *)dictValue;
@end
