//
//  CurrentOfferHeaderTableViewCell.m
//  Shell
//
//  Created by Admin on 26/2/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "CurrentOfferHeaderTableViewCell.h"
#import "Constants.h"
#import "LocalizationManager.h"

@interface CurrentOfferHeaderTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *lblPromoPeriod;
@property (weak, nonatomic) IBOutlet UILabel *lblPromoNameAndType;
@property (weak, nonatomic) IBOutlet UILabel *lblRedemptions;

@end
@implementation CurrentOfferHeaderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lblPromoPeriod.font = FONT_H2;
    self.lblPromoNameAndType.font = self.lblPromoPeriod.font;
    self.lblRedemptions.font = self.lblPromoPeriod.font;
    
    self.lblPromoPeriod.text = LOCALIZATION(@"Promo Period");           //later lokalised
    self.lblPromoNameAndType.text = LOCALIZATION(@"Promo Name & Type"); //later lokalised
    self.lblRedemptions.text = LOCALIZATION(@"Redemptions");            //later lokalised
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
