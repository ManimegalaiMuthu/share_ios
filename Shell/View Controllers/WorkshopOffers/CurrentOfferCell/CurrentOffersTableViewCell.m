//
//  CurrentOffersTableViewCell.m
//  Shell
//
//  Created by Admin on 26/2/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "CurrentOffersTableViewCell.h"
#import "Constants.h"
#import "LocalizationManager.h"

@interface CurrentOffersTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *lblEndDate;
@property (weak, nonatomic) IBOutlet UILabel *lblStartDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property (weak, nonatomic) IBOutlet UILabel *lblPromonameAndType;
@property (weak, nonatomic) IBOutlet UILabel *lblRedemptions;
@property (weak, nonatomic) IBOutlet UILabel *lblColorTag;


@end
@implementation CurrentOffersTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setupInterface :(NSDictionary *)dictValue
{
    self.lblTo.font = FONT_B2;
    self.lblPromonameAndType.font = self.lblTo.font;
    self.lblRedemptions.font = self.lblTo.font;
    self.lblStartDate.font = self.lblTo.font;
    self.lblEndDate.font = self.lblTo.font;
    self.lblColorTag.font = FONT_H2;
    
    self.lblTo.text = LOCALIZATION(@"to");  //later lokalised
    self.lblPromonameAndType.text = [dictValue objectForKey:@"OfferTitle"];
    self.lblStartDate.text = [dictValue objectForKey:@"StartDate"];
    self.lblEndDate.text = [dictValue objectForKey:@"EndDate"];
    self.lblRedemptions.text = [NSString stringWithFormat:@"%@",[dictValue objectForKey:@"Redemptions"]];
    
    NSString *index = [NSString stringWithFormat:@"%@",[dictValue objectForKey:@"promotionTypeIndex"]];
    if([index isEqualToString:@"1"]){// 1 = shell
        self.lblColorTag.backgroundColor = COLOUR_YELLOW;
        self.lblColorTag.textColor = COLOUR_RED;
        self.lblColorTag.text = [dictValue objectForKey:@"OfferTypeDescription"];
    }else{
        self.lblColorTag.backgroundColor = COLOUR_DARKGREY;
        self.lblColorTag.textColor = COLOUR_WHITE;
        self.lblColorTag.text = [dictValue objectForKey:@"OfferTypeDescription"];
    }
}

@end
