//
//  AddNewWorkshopOfferViewController.m
//  Shell
//
//  Created by Admin on 20/2/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "AddNewWorkshopOfferViewController.h"
#import "CustomerRangeTableViewCell.h"
#import "CustomerRangeHeaderTableViewCell.h"
#import "PopUpYesNoViewController.h"

//#define TABLEVIEW_ROW_OFFSET 2
#define TABLEVIEW_ROW_OFFSET 1
#define HEADERHEIGHT 70
#define CELLHEIGHT 80
#define PADDINGHEIGHT 15  //top and bottom padding space inside custom range view

//#define OFFEROPTION_TEXTFIELDHEIGHT 60
#define OFFEROPTION_TEXTFIELDHEIGHT 40
@interface AddNewWorkshopOfferViewController ()<UITableViewDelegate,UITableViewDataSource,CommonListDelegate,WebServiceManagerDelegate,AncillaryOfferAll, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *offerOptionTextFieldView;
@property (weak, nonatomic) IBOutlet UITextField *txtOfferOptionCash;
@property (weak, nonatomic) IBOutlet UITextField *txtOfferOptionPercentage;
@property (weak, nonatomic) IBOutlet UIImageView *imgDollar;
@property (weak, nonatomic) IBOutlet UIImageView *imgPercentage;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableviewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *offerOptionTextfieldHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblStep1;
@property (weak, nonatomic) IBOutlet UILabel *lblStep1Title;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property (weak, nonatomic) IBOutlet UILabel *lblStep2;
@property (weak, nonatomic) IBOutlet UILabel *lblStep2Title;
@property (weak, nonatomic) IBOutlet UILabel *lblStep3;
@property (weak, nonatomic) IBOutlet UILabel *lblStep3Title;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *offerTypeHiddenViewHeight;
@property (weak, nonatomic) IBOutlet UIView *offerTypeHiddenView;

@property (weak, nonatomic) IBOutlet UILabel *lblOfferType;
@property (weak, nonatomic) IBOutlet UILabel *lblOfferOptions;
@property (weak, nonatomic) IBOutlet UIButton *btnService;
@property (weak, nonatomic) IBOutlet UIButton *btnProduct;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectOfferType;
@property (weak, nonatomic) IBOutlet UIButton *btnDiscountPercentage;
@property (weak, nonatomic) IBOutlet UIButton *btnDiscountInCash;
@property (weak, nonatomic) IBOutlet UIButton *btnFree;
@property (weak, nonatomic) IBOutlet UIButton *btnPublishMyOffer;
@property (weak, nonatomic) IBOutlet UIButton *btnStartDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEndDate;
@property (weak, nonatomic) IBOutlet UITextField *txtPromotionName;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UILabel *lblDatePicker;

@property BOOL isShow;
@property NSMutableArray *customerRangeArray;
@property NSMutableDictionary *referenceCustomerRangeDict;

@property NSMutableDictionary *nextPageDict;

@property (weak, nonatomic) IBOutlet UILabel *lblSmsPreview;
@property (weak, nonatomic) IBOutlet UILabel *lblSmsPreviewContent;
@property NSString *selectedServiceString;
@property (weak, nonatomic) IBOutlet UIButton *btnDateEnter;
@property NSString *smsAncillaryOfferString;

@property NSInteger numberOfTargetMembers;
@end

@implementation AddNewWorkshopOfferViewController
//@synthesize listView;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.numberOfTargetMembers = 0;
    [self.tableView registerNib:[UINib nibWithNibName:@"CustomerRangeTableViewCell" bundle:nil] forCellReuseIdentifier:@"CustomerRangeTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"CustomerRangeHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"CustomerRangeHeaderTableViewCell"];
    self.tableView.tableFooterView = [UIView new];
    
    [self setupInterface];
    self.nextPageDict = [[NSMutableDictionary alloc]init];
    [[WebServiceManager sharedInstance]fetchWorkshopOilDetailVc:self input:@{@"startDate":@"", @"endDate" : @"", @"isAllOffers": @(1)}];
    
}


-(void)setupInterfaceWithResponse:(NSArray *)responseDict
{
    self.referenceCustomerRangeDict = [[NSMutableDictionary alloc]init];
    
    if (![responseDict isKindOfClass: [NSNull class]])
        self.customerRangeArray = [[NSMutableArray alloc]initWithArray:responseDict];
    else
        self.customerRangeArray = [[NSMutableArray alloc] init];
    
//    self.tableviewHeight.constant = (self.customerRangeArray.count + 1)*CELLHEIGHT + HEADERHEIGHT + PADDINGHEIGHT*2;
    
    self.tableviewHeight.constant = (self.customerRangeArray.count)*CELLHEIGHT + HEADERHEIGHT + PADDINGHEIGHT*2;
    
    [self.tableView reloadData];
}

-(void)setupInterface
{
    self.btnStartDate.titleLabel.font = FONT_B2;
    self.btnEndDate.titleLabel.font = self.btnStartDate.titleLabel.font;
    
    self.lblStep1.font = FONT_H2;
    self.lblStep2.font = self.lblStep1.font;
    self.lblStep3.font = self.lblStep1.font;
    
    self.lblStep1.text = LOCALIZATION(@"STEP 1:");  //later lokalised
    self.lblStep2.text = LOCALIZATION(@"STEP 2:");  //later lokalised
    self.lblStep3.text = LOCALIZATION(@"STEP 3:");  //later lokalised
    
    self.lblStep1Title.font = FONT_B2;
    self.lblStep2Title.font = self.lblStep1Title.font;
    self.lblStep3Title.font = self.lblStep1Title.font;
    
    self.lblStep1Title.text = LOCALIZATION(@"ENTER PROMOTION DETAILS"); //later lokalised
    self.lblStep2Title.text = LOCALIZATION(@"CHOOSE CUSTOMER RANGE");   //later lokalised
    self.lblStep3Title.text = LOCALIZATION(@"CHOOSE OFFER TYPE");       //later lokalised
    self.txtPromotionName.font = FONT_B2;
    self.txtPromotionName.placeholder = LOCALIZATION(@"Enter Promotion Name Here"); //later lokalised
    self.lblTo.text = LOCALIZATION(@"To");  //later lokalised
    
    self.lblOfferType.font = FONT_B2;
    self.lblTo.font = FONT_B2;
    self.lblOfferOptions.font = FONT_B2;
    
    self.txtOfferOptionPercentage.placeholder = LOCALIZATION(@"Enter Discount % Percentage Here");  //later lokalised
    self.txtOfferOptionPercentage.delegate = self;
    self.txtOfferOptionPercentage.keyboardType = UIKeyboardTypeDecimalPad;
    
    self.txtOfferOptionCash.placeholder = LOCALIZATION(@"Enter Discount Amount Here");  //later lokalised
    self.txtOfferOptionCash.delegate = self;
    self.txtOfferOptionCash.keyboardType = UIKeyboardTypeDecimalPad;
    
    self.tableView.backgroundColor = COLOUR_VERYPALEGREY;
    
    self.btnService.backgroundColor = COLOUR_PALEGREY;
    self.btnProduct.backgroundColor = COLOUR_PALEGREY;
    self.btnDiscountPercentage.backgroundColor = COLOUR_PALEGREY;
    self.btnDiscountInCash.backgroundColor = COLOUR_PALEGREY;
    self.btnFree.backgroundColor = COLOUR_PALEGREY;
    
    self.offerTypeHiddenViewHeight.constant = 0;
    self.offerTypeHiddenView.hidden = YES;
    self.btnSelectOfferType.enabled = NO;
    self.btnSelectOfferType.backgroundColor = COLOUR_VERYPALEGREY;
    self.btnSelectOfferType.titleLabel.font = FONT_B2;
    self.btnSelectOfferType.titleLabel.textColor = COLOUR_PALEGREY;
    
    self.btnService.titleLabel.font = FONT_H3;
    self.btnProduct.titleLabel.font = self.btnService.titleLabel.font;
    self.btnDiscountPercentage.titleLabel.font = FONT_H3;
    self.btnDiscountInCash.titleLabel.font = self.btnDiscountPercentage.titleLabel.font;
    self.btnFree.titleLabel.font = self.btnDiscountPercentage.titleLabel.font;
    
    self.btnPublishMyOffer.backgroundColor = COLOUR_RED;
    self.btnPublishMyOffer.titleLabel.font = FONT_BUTTON;
    [self.btnPublishMyOffer setTitle:LOCALIZATION(@"Publish My Offer") forState:UIControlStateNormal];  //later lokalised
    
    self.txtOfferOptionCash.font = FONT_H2;
    self.txtOfferOptionPercentage.font = self.txtOfferOptionCash.font;
    self.offerOptionTextFieldView.hidden = YES;
    self.offerOptionTextfieldHeight.constant = 0;
    
    self.lblSmsPreview.text = LOCALIZATION(@"SMS Message Preview"); //later lokalised
    self.lblSmsPreview.font = FONT_H1;
    self.lblSmsPreviewContent.font = FONT_B2;
    self.selectedServiceString = @"";
    
    self.datePickerView.hidden = YES;
    [self.btnDateEnter.titleLabel setFont: FONT_H2];
    [self.btnDateEnter setBackgroundColor:COLOUR_RED];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)startDatePressed:(id)sender {
    [self.view endEditing:YES];
    
    self.datePicker.tag = 1;
    self.lblDatePicker.text = LOCALIZATION(@"From Date");   //later lokalised
    self.datePickerView.hidden = NO;
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];
    
    //start date is always future
    self.datePicker.minimumDate = [NSDate date];
    
    
    //date cannot be greater than end date
    NSDate *endMaxDate = [outputFormatter dateFromString: self.btnEndDate.titleLabel.text];
    if (endMaxDate)
        self.datePicker.maximumDate = endMaxDate;
    else
        self.datePicker.maximumDate = nil;
    
    
    NSDate *startDate = [outputFormatter dateFromString: self.btnStartDate.titleLabel.text];
    if (startDate){
        self.datePicker.date = startDate;
    }
    
}
- (IBAction)endDatePressed:(id)sender {
    [self.view endEditing:YES];
    
    self.datePicker.tag = 2;
    self.lblDatePicker.text = LOCALIZATION(C_POINTS_ENDDATE);   //lokalised 11 Feb
    self.datePickerView.hidden = NO;
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];
    self.datePicker.maximumDate = nil;
    
    //date cannot be lesser than start date
    NSDate *startMinDate = [outputFormatter dateFromString: self.btnStartDate.titleLabel.text];
    if (startMinDate)
        self.datePicker.minimumDate = startMinDate;
    else
        self.datePicker.minimumDate = nil;
    
    NSDate *endDate = [outputFormatter dateFromString: self.btnEndDate.titleLabel.text];
    if (endDate){
        self.datePicker.date = endDate;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.customerRangeArray.count + TABLEVIEW_ROW_OFFSET;
}

- (IBAction)checkboxPressed:(UIButton *)sender {
    sender.selected = !sender.selected;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0:
        {
            CustomerRangeHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomerRangeHeaderTableViewCell"];
            cell.lblCustomerRangeHeader.text = @"Number of your customers that will be contacted in the selected promotion period:";
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.delegate = self;
            return cell;
        }
            break;
//        case 1:
//        {
//            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomerRangeIncludeNotTaggedCell"];
//
//            NSDictionary *lightDict = [NSDictionary dictionaryWithObject: FONT_B2 forKey:NSFontAttributeName];
//            NSMutableAttributedString *aAttrString1 = [[NSMutableAttributedString alloc] initWithString:    LOCALIZATION(@"Include all my workshop's customers not tagged to an oil change") attributes: lightDict];
//
//            NSDictionary *boldDict = [NSDictionary dictionaryWithObject: FONT_H2 forKey:NSFontAttributeName];
////            NSString *counterString = [NSString stringWithFormat:@" (%@)",[customRangeDict objectForKey:@"TargetNumber"]];
//            NSString *counterString = [NSString stringWithFormat:@" (%@)", @(self.numberOfTargetMembers).stringValue];
//            NSMutableAttributedString *aAttrString2 = [[NSMutableAttributedString alloc] initWithString:counterString attributes: boldDict];
//            [aAttrString1 appendAttributedString:aAttrString2];
//
//            UILabel *lblText = [cell viewWithTag: 1];
//            lblText.attributedText = aAttrString1;
//
//            return cell;
////
//        }
//            break;
        default:
        {
            CustomerRangeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomerRangeTableViewCell"];
            NSDictionary *individualDict = [self.customerRangeArray objectAtIndex: indexPath.row - TABLEVIEW_ROW_OFFSET];
            [cell setupInterface: individualDict];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if(indexPath.row == TABLEVIEW_ROW_OFFSET)
                cell.delegate = self; //delegate method to select all cells
            [self.referenceCustomerRangeDict setObject:cell forKey:[individualDict objectForKey:@"ProductGroupID"]];
            return cell;
        }
            break;
    }
    return [UITableViewCell new];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row >= TABLEVIEW_ROW_OFFSET){
        NSString * title = [[self.customerRangeArray objectAtIndex:indexPath.row - TABLEVIEW_ROW_OFFSET] objectForKey:@"ProductGroupID"];
        CustomerRangeTableViewCell *cell = [self.referenceCustomerRangeDict objectForKey:title];
        [cell setCheckboxSelected];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row < TABLEVIEW_ROW_OFFSET)
//        return HEADERHEIGHT;
        return UITableViewAutomaticDimension;
//    else if (indexPath.row == 1) //TABLEVIEW_ROW_OFFSET
//        return UITableViewAutomaticDimension;
    else
        return CELLHEIGHT;
}

- (IBAction)serviceBtnPressed:(id)sender {
    [self selectedButton:self.btnService];
//    [self showPopup];
}
- (IBAction)productBtnPressed:(id)sender {
    [self selectedButton:self.btnProduct];
//    [self showPopup];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        self.selectedServiceString = selection.firstObject;
        [self.btnSelectOfferType setTitle:[NSString stringWithFormat:@"Selected Service: %@",selection.firstObject] forState:UIControlStateNormal];
        self.offerTypeHiddenView.hidden = NO;
        [self.offerTypeHiddenView removeConstraint:self.offerTypeHiddenViewHeight];
        
        if (self.btnDiscountPercentage.isSelected)
        {
            [self reloadSmsPreviewMessage: selectedOfferOptions_PERCENTAGE];
//            NSString *smsAncillaryOfferString = [[mSession lookupTable] objectForKey: @"SMSAncillaryOffer"];
//            smsAncillaryOfferString = [smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#discount#" withString: LOCALIZATION(@"XX% Discount off")];
//            smsAncillaryOfferString = [smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#service#" withString: self.selectedServiceString];
//            self.lblSmsPreviewContent.text = smsAncillaryOfferString;
        } else if (self.btnDiscountInCash.isSelected)
        {
            [self reloadSmsPreviewMessage: selectedOfferOptions_CASH];
            
//            NSString *smsAncillaryOfferString = [[mSession lookupTable] objectForKey: @"SMSAncillaryOffer"];
//            smsAncillaryOfferString = [smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#discount#" withString: LOCALIZATION(@"$XX Discount off")];
//            smsAncillaryOfferString = [smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#service#" withString: self.selectedServiceString];
//            self.lblSmsPreviewContent.text = smsAncillaryOfferString;
        } else if (self.btnFree.isSelected)
        {
            [self reloadSmsPreviewMessage: selectedOfferOptions_FREE];
//            NSString *smsAncillaryOfferString = [[mSession lookupTable] objectForKey: @"SMSAncillaryOffer"];
//            smsAncillaryOfferString = [smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#discount#" withString: LOCALIZATION(@"Free")];
//            smsAncillaryOfferString = [smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#service#" withString: self.selectedServiceString];
//            self.lblSmsPreviewContent.text = smsAncillaryOfferString;
            
        }
    }
}

-(void)selectedButton:(UIButton *)buttonClicked
{
    [self.view endEditing: YES];
    if(buttonClicked == self.btnService){
        [self.btnProduct setSelected:NO];
        [self.btnService setSelected:YES];
        
        self.btnService.backgroundColor = COLOUR_YELLOW;
        self.btnProduct.backgroundColor = COLOUR_PALEGREY;
        
        [self popUpListViewWithTitle:LOCALIZATION(@"Select Services to Offer") listArray:[mSession getWorkshopOfferService]];   //later lokalised
        
        
    }else if(buttonClicked == self.btnProduct){
        [self.btnProduct setSelected:YES];
        [self.btnService setSelected:NO];
        
        self.btnService.backgroundColor = COLOUR_PALEGREY;
        self.btnProduct.backgroundColor = COLOUR_YELLOW;
        
        [self popUpListViewWithTitle:LOCALIZATION(@"Select Products to Offer") listArray:[mSession getWorkshopOfferProduct]];   //later lokalised
        
    }else if(buttonClicked == self.btnDiscountPercentage){
        self.offerOptionTextFieldView.hidden = NO;
        self.offerOptionTextfieldHeight.constant = OFFEROPTION_TEXTFIELDHEIGHT;
        
        self.txtOfferOptionCash.hidden = YES;
        self.txtOfferOptionCash.text = @"";
        self.imgDollar.hidden = YES;
        
        self.txtOfferOptionPercentage.hidden = NO;
        self.imgPercentage.hidden = NO;
        
        [self.btnDiscountPercentage setSelected:YES];
        [self.btnDiscountInCash setSelected:NO];
        [self.btnFree setSelected:NO];
        
        self.btnDiscountPercentage.backgroundColor = COLOUR_YELLOW;
        self.btnDiscountInCash.backgroundColor = COLOUR_PALEGREY;
        self.btnFree.backgroundColor = COLOUR_PALEGREY;
//        NSString *smsAncillaryOfferString = [[mSession lookupTable] objectForKey: @"SMSAncillaryOffer"];
//        smsAncillaryOfferString = [smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#discount#" withString: LOCALIZATION(@"XX% Discount off")];
//        smsAncillaryOfferString = [smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#service#" withString: self.selectedServiceString];
//        self.lblSmsPreviewContent.text = smsAncillaryOfferString;

        [self reloadSmsPreviewMessage: selectedOfferOptions_PERCENTAGE];
        
    }else if(buttonClicked == self.btnDiscountInCash){
        self.offerOptionTextFieldView.hidden = NO;
        self.offerOptionTextfieldHeight.constant = OFFEROPTION_TEXTFIELDHEIGHT;
        
        self.txtOfferOptionCash.hidden = NO;
        self.imgDollar.hidden = NO;
        
        self.txtOfferOptionPercentage.text = @"";
        self.txtOfferOptionPercentage.hidden = YES;
        self.imgPercentage.hidden = YES;
        
        [self.btnDiscountPercentage setSelected:NO];
        [self.btnDiscountInCash setSelected:YES];
        [self.btnFree setSelected:NO];
        
        self.btnDiscountPercentage.backgroundColor = COLOUR_PALEGREY;
        self.btnDiscountInCash.backgroundColor = COLOUR_YELLOW;
        self.btnFree.backgroundColor = COLOUR_PALEGREY;
        
        [self reloadSmsPreviewMessage: selectedOfferOptions_CASH];
//        NSString *smsAncillaryOfferString = [[mSession lookupTable] objectForKey: @"SMSAncillaryOffer"];
//        smsAncillaryOfferString = [smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#discount#" withString: LOCALIZATION(@"$XX Discount off")];
//        smsAncillaryOfferString = [smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#service#" withString: self.selectedServiceString];
//        self.lblSmsPreviewContent.text = smsAncillaryOfferString;
        
    }else{
        self.offerOptionTextFieldView.hidden = YES;
        self.txtOfferOptionCash.text = @"";
        self.txtOfferOptionPercentage.text = @"";
        self.offerOptionTextfieldHeight.constant = 0;
        
        [self.btnDiscountPercentage setSelected:NO];
        [self.btnDiscountInCash setSelected:NO];
        [self.btnFree setSelected:YES];
        
        self.btnDiscountPercentage.backgroundColor = COLOUR_PALEGREY;
        self.btnDiscountInCash.backgroundColor = COLOUR_PALEGREY;
        self.btnFree.backgroundColor = COLOUR_YELLOW;
        
        [self reloadSmsPreviewMessage: selectedOfferOptions_FREE];
//        NSString *smsAncillaryOfferString = [[mSession lookupTable] objectForKey: @"SMSAncillaryOffer"];
//        smsAncillaryOfferString = [smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#discount#" withString: LOCALIZATION(@"Free")];
//        smsAncillaryOfferString = [smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#service#" withString: self.selectedServiceString];
//        smsAncillaryOfferString = [smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#startDate#" withString: self.btnStartDate.titleLabel.text];
//        smsAncillaryOfferString = [smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#endDate#" withString: self.btnEndDate.titleLabel.text];
//        self.lblSmsPreviewContent.text = smsAncillaryOfferString;
    }
}

enum selectedOfferOptions_TYPES
{
    selectedOfferOptions_UNKNOWN = 0,
    selectedOfferOptions_CASH = 1,
    selectedOfferOptions_PERCENTAGE,
    selectedOfferOptions_FREE,
};

-(void) reloadSmsPreviewMessage: (NSInteger) selectedOfferType
{
    self.smsAncillaryOfferString = [[mSession lookupTable] objectForKey: @"SMSAncillaryOffer"];
    switch (selectedOfferType)
    {
        case 0:
            if (self.btnDiscountInCash.selected)
                [self reloadSmsPreviewMessage: selectedOfferOptions_CASH];
            else if (self.btnDiscountPercentage.selected)
                [self reloadSmsPreviewMessage: selectedOfferOptions_PERCENTAGE];
            else if (self.btnFree.selected)
                [self reloadSmsPreviewMessage: selectedOfferOptions_FREE];
            break;
        case selectedOfferOptions_CASH:
            self.smsAncillaryOfferString = [self.smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#discount#" withString: [NSString stringWithFormat: @"$%@ %@", self.txtOfferOptionCash.text, LOCALIZATION(@"Discount off")]];  //later lokalised
            break;
        case selectedOfferOptions_PERCENTAGE:
            self.smsAncillaryOfferString = [self.smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#discount#" withString: [NSString stringWithFormat: @"%@%% %@", self.txtOfferOptionPercentage.text, LOCALIZATION(@"Discount off")]];   //later lokalised
            break;
        case selectedOfferOptions_FREE:
            self.smsAncillaryOfferString = [self.smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#discount#" withString: LOCALIZATION(@"Free")];    //later lokalised
            break;
        default:
            break;
    }
    self.smsAncillaryOfferString = [self.smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#service#" withString: self.selectedServiceString];
    self.smsAncillaryOfferString = [self.smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#startDate#" withString: self.btnStartDate.titleLabel.text];
    self.smsAncillaryOfferString = [self.smsAncillaryOfferString stringByReplacingOccurrencesOfString: @"#endDate#" withString: self.btnEndDate.titleLabel.text];
    
    self.lblSmsPreviewContent.text = self.smsAncillaryOfferString;
}

- (IBAction)textFieldDidChange:(id)sender {
    if (sender == self.txtOfferOptionCash)
    {
        [self reloadSmsPreviewMessage: selectedOfferOptions_CASH];
    }
    else if (sender == self.txtOfferOptionPercentage)
    {
        [self reloadSmsPreviewMessage: selectedOfferOptions_PERCENTAGE];
    }
}

- (IBAction)freeBtnPressed:(id)sender {
    [self selectedButton:self.btnFree];
}

- (IBAction)discountInCashBtnPressed:(id)sender {
    [self selectedButton:self.btnDiscountInCash];
}

- (IBAction)discountInPercentageBtnPressed:(id)sender {
    [self selectedButton:self.btnDiscountPercentage];
}

- (IBAction)publishBtnPressed:(id)sender {
    NSMutableArray *rangeArray = [[NSMutableArray alloc]init];
    NSInteger count = 0;
    for(int i =0;i<self.customerRangeArray.count;i++){
        NSLog(@"%@",[[self.referenceCustomerRangeDict objectForKey:[[self.customerRangeArray objectAtIndex:i] objectForKey:@"ProductGroupID"]] getGroupDict]);
        NSLog(@"%d",[[self.referenceCustomerRangeDict objectForKey:[[self.customerRangeArray objectAtIndex:i] objectForKey:@"ProductGroupID"]] isCheckboxSelected]);
        if ([[self.referenceCustomerRangeDict objectForKey:[[self.customerRangeArray objectAtIndex:i] objectForKey:@"ProductGroupID"]] isCheckboxSelected]) {
            NSDictionary *dict = [[self.referenceCustomerRangeDict objectForKey:[[self.customerRangeArray objectAtIndex:i] objectForKey:@"ProductGroupID"]] getGroupDict];
            [rangeArray addObject:dict];
            
            count += [[dict objectForKey:@"TargetMembers"] integerValue];
            
        }
    }
    
    NSInteger offerType = 0;
    if(self.btnService.selected)
        offerType = 1;
    else if(self.btnProduct.selected)
        offerType = 2;
    
    NSInteger offerOptions = 0;
    if(self.btnDiscountPercentage.selected)
        offerOptions = 1;
    else if(self.btnDiscountInCash.selected)
        offerOptions = 2;
    else if(self.btnFree.selected)
        offerOptions = 3;
    
    NSString *offerCategoryCode = @"0";
    NSString *offerCategory = [self.btnSelectOfferType.titleLabel.text stringByReplacingOccurrencesOfString:@"Selected Service: " withString:@""];
    if(offerType == 1){
        offerCategoryCode = [mSession convertToWorkshopServiceKeyCode:offerCategory];
    }else if(offerType == 2){
        offerCategoryCode = [mSession convertToWorkshopProductKeyCode:offerCategory];
    }
    
    NSString *offerValue = @"";
    if(offerOptions == 1){
        offerValue = [self.txtOfferOptionPercentage.text isEqualToString:@""]?@"0":self.txtOfferOptionPercentage.text;
    }else if(offerOptions == 2){
        offerValue = [self.txtOfferOptionCash.text isEqualToString:@""]?@"0":self.txtOfferOptionCash.text;
    }
    
    NSString *promoName = self.txtPromotionName.text == nil?@"":self.txtPromotionName.text;
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];
    
    NSDate *start = [outputFormatter dateFromString:self.btnStartDate.titleLabel.text];
    NSDate *end = [outputFormatter dateFromString:self.btnEndDate.titleLabel.text];
    
    NSString *startDate = start == nil?@"":self.btnStartDate.titleLabel.text;
    NSString *endDate = end== nil?@"":self.btnEndDate.titleLabel.text;
    
    NSDictionary *submitDict = @{@"EndDate" : endDate,
                                 @"isFinalSubmit" : @(NO),
                                 @"OfferInformation"     : rangeArray,
                                 @"OfferOption" : @(offerOptions),
                                 @"OfferCategoryCode" : offerCategoryCode,
                                 @"OfferType" : @(offerType),
                                 @"OfferTitle" : promoName,
                                 @"StartDate" : startDate,
                                 @"IsAcceptTerms" : @(NO),
                                 @"OfferValue" : offerValue
                                 };
    
    self.nextPageDict =[[NSMutableDictionary alloc] initWithDictionary: @{@"EndDate" : endDate,
                                                                          @"isFinalSubmit" : @(NO),
                                                                          @"OfferInformation"     : rangeArray,
                                                                          @"OfferOption" : @(offerOptions),
                                                                          @"OfferCategoryCode" : offerCategoryCode,
                                                                          @"OfferType" : @(offerType),
                                                                          @"OfferTitle" : promoName,
                                                                          @"StartDate" : startDate,
                                                                          @"IsAcceptTerms" : @(NO),
                                                                          @"OfferValue" : offerValue,
                                                                          @"AllUser" : @(count),
                                                                          @"SelectedOfferTypeName" : offerCategory,
                                                                          }];
    
    [[WebServiceManager sharedInstance] submitToGetAncillaryOffer:self submitDict:submitDict];
//    [mSession pushOfferConfirmationView:self.navigationController.parentViewController dict:self.nextPageDict];
}

- (IBAction)pickerDonePressed:(id)sender {
    self.datePickerView.hidden = YES;
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];
    
    if(self.datePicker.tag == 1){
        [self.btnStartDate setTitle:[outputFormatter stringFromDate:self.datePicker.date] forState:UIControlStateNormal];
        NSDate *endD = [outputFormatter dateFromString:self.btnEndDate.titleLabel.text];
        if(endD != nil){
//            [[WebServiceManager sharedInstance]fetchWorkshopOilDetailVc:self input:@{@"startDate":[outputFormatter stringFromDate:self.datePicker.date],
//                                                                                     @"endDate" : self.btnEndDate.titleLabel.text
//                                                                                     }];
        }
    }else{
        [self.btnEndDate setTitle:[outputFormatter stringFromDate:self.datePicker.date] forState:UIControlStateNormal];
        NSDate *startD = [outputFormatter dateFromString:self.btnStartDate.titleLabel.text];
        if(startD != nil){
        }
    }
}

- (IBAction)btnDateEnter:(id)sender {
    //end pickerview if showing
    if (!self.datePickerView.hidden)
        [self pickerDonePressed: nil];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];
    if (self.btnDiscountPercentage.selected || self.btnDiscountInCash.selected || self.btnFree.selected)
        [self reloadSmsPreviewMessage: 0];
    
    
    //to send empty string if date is invalid.
    NSDate *startDateFromString = [outputFormatter dateFromString:self.btnStartDate.titleLabel.text];
    NSString *startDateString = @"";
    if (startDateFromString)
        startDateString = self.btnStartDate.titleLabel.text;

    //to send empty string if date is invalid.
    NSDate *endDateFromString = [outputFormatter dateFromString:self.btnEndDate.titleLabel.text];
    NSString *endDateString = @"";
    if (endDateFromString)
        endDateString = self.btnEndDate.titleLabel.text;
    
    [[WebServiceManager sharedInstance]fetchWorkshopOilDetailVc:self input:@{@"startDate": startDateString, @"endDate" :  endDateString,    @"isAllOffers": @(0) }];
}

-(void)ancillaryOfferSelectAll
{
    for(int i =0;i<self.customerRangeArray.count;i++){
        if (![[self.referenceCustomerRangeDict objectForKey:[[self.customerRangeArray objectAtIndex:i] objectForKey:@"ProductGroupID"]] isCheckboxSelected]) {
            [[self.referenceCustomerRangeDict objectForKey:[[self.customerRangeArray objectAtIndex:i] objectForKey:@"ProductGroupID"]] setCheckboxSelected];
        }
    }
}

-(void)ancillaryOfferDeselectAll
{
    for(int i =0;i<self.customerRangeArray.count;i++){
        if ([[self.referenceCustomerRangeDict objectForKey:[[self.customerRangeArray objectAtIndex:i] objectForKey:@"ProductGroupID"]] isCheckboxSelected]) {
            [[self.referenceCustomerRangeDict objectForKey:[[self.customerRangeArray objectAtIndex:i] objectForKey:@"ProductGroupID"]] setCheckboxSelected];
        }
    }
}

//- (void)yesPressed
//{
//
//}
//
//-(void)noPressed
//{
//
//}

- (void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_WORKSHOPOILDETAIL:{
            
//            [self popUpYesNoWithTitle: LOCALIZATION(@"Title") contentString:@"" yesBtnTitle: LOCALIZATION(@"YES") noBtnTitle: LOCALIZATION(@"NO")];
            
            [self setupInterfaceWithResponse:[[response getGenericResponse] objectForKey:@"ProductGroup"]];
            
            if ([[[response getGenericResponse] objectForKey: @"TargetMembers"] isKindOfClass: [NSNull class]])
                self.numberOfTargetMembers = 0;
            else
                self.numberOfTargetMembers = [[[[response getGenericResponse] objectForKey: @"TargetMembers"] objectAtIndex: 0] objectForKey: @"NoOfTargetMembers"];
        }
            
            break;
        case kWEBSERVICE_SUBMITANCILLARY:{
//            [self.nextPageDict setObject:self.lblSmsPreviewContent.text forKey:@"msg"];
            [self.nextPageDict setObject: [[response getGenericResponse] objectForKey: @"SMSPreview"] forKey:@"msg"];

            [self.nextPageDict setObject:[[response getGenericResponse] objectForKey:@"TotalCount"] forKey:@"count"];
            [mSession pushOfferConfirmationView:self.navigationController.parentViewController dict:self.nextPageDict];
            
        }
            break;
        default:
            break;
    }
}
- (void)processFailed:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_WORKSHOPOILDETAIL:{
            if ([response getResponseCode] == 8010) //other error code will show default error popup
            {
                [self popUpYesNoWithTitle: @"" content: [response getResponseMessage] yesBtnTitle:LOCALIZATION(C_FORM_YES)  noBtnTitle:LOCALIZATION(C_FORM_NO) onYesPressed:^(BOOL finished) {  //lokalised 11 Feb
                    [self setupInterfaceWithResponse:[[response getGenericResponse] objectForKey:@"ProductGroup"]];
                    self.numberOfTargetMembers = [[[[response getGenericResponse] objectForKey: @"TargetMembers"] objectAtIndex: 0] objectForKey: @"NoOfTargetMembers"];
                } onNoPressed:^(BOOL finished) {
//                    [self setupInterfaceWithResponse: nil];
//                    self.numberOfTargetMembers = 0;
                    [[WebServiceManager sharedInstance]fetchWorkshopOilDetailVc:self input:@{@"startDate":@"", @"endDate" : @"", @"isAllOffers": @(1)}];
                }];
            }
        }
            break;
        default:
            break;
    }
}

- (IBAction)offerInfoPressed:(id)sender {
    if ([[mSession lookupTable] objectForKey: @"OfferTypeDesc"])
        [self popUpViewWithTitle: LOCALIZATION(C_RU_WHATISTHIS) content: [[mSession lookupTable] objectForKey: @"OfferTypeDesc"]];  //lokalised 11 Feb
    else
    {
        [self popUpViewWithTitle:LOCALIZATION(C_RU_WHATISTHIS) content:LOCALIZATION(@"You can click on the ‘Service’ or ‘Product’ button to view available services and products to offer.")];  //later lokalised
    }
    
}

- (IBAction)smsPreviewInfoPressed:(id)sender {
    if ([[mSession lookupTable] objectForKey: @"SmsPreviewDesc"])
        [self popUpViewWithTitle: LOCALIZATION(C_RU_WHATISTHIS) content: [[mSession lookupTable] objectForKey: @"SmsPreviewDesc"]]; //lokalised 11 Feb
    else
        [self popUpViewWithTitle:LOCALIZATION(C_RU_WHATISTHIS) content:LOCALIZATION(@"This is a sample SMS that your customers will receive when you publish the offer.")]; //later lokalised
    
}

-(void)customerRangeInfoPressed
{
    if ([[mSession lookupTable] objectForKey: @"CustomerRangeDesc"])
        [self popUpViewWithTitle: LOCALIZATION(C_RU_WHATISTHIS) content: [[mSession lookupTable] objectForKey: @"CustomerRangeDesc"]];  //lokalised 11 Feb
    else
        [self popUpViewWithTitle:LOCALIZATION(C_RU_WHATISTHIS) content:LOCALIZATION(@"You can choose one or more groups of customers to receive this offer. You can also include customers who have not had an oil change but are registered with your workshop.")];    //later lokalised
}

@end
