//
//  WorkshopOfferDetailRedemptionPopup.m
//  Shell
//
//  Created by Admin on 1/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "WorkshopOfferDetailRedemptionPopup.h"
#import "Constants.h"
#import "RedemptionTableViewCell.h"
#import "LocalizationManager.h"

//#define ROWCOUNT 7
#define CELL_HEIGHT 70

enum kRedemption_TYPE
{
    kRedemption_DateOfRedemption,
    kRedemption_VehicleID,
    kRedemption_OfferRedeemed,
//    kRedemption_ProductName,
    kRedemption_ProductUsed,
//    kRedemption_ProductCode,
//    kRedemption_Quantity,
    kRedemption_RedemptionVerification,
    kRedemption_Approver,
};
@interface WorkshopOfferDetailRedemptionPopup()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property NSDictionary *detailDict;
@end
@implementation WorkshopOfferDetailRedemptionPopup

- (id)initOfferDetailRedemptionDetail:(NSDictionary *)infoDict {
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"WorkshopOfferDetailRedemptionPopup" owner:self options:nil];
    self = nib[0];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"RedemptionTableViewCell" bundle:nil] forCellReuseIdentifier:@"RedemptionTableViewCell"];
    self.detailDict = [[NSDictionary alloc]initWithDictionary:infoDict];
    self.tableView.tableFooterView = [UIView new];
    [self.tableView reloadData];
    
    self.lblTitle.font = FONT_H1;
    self.lblTitle.text = LOCALIZATION(C_RUSSIA_REDEMPTION_DETAILS); //lokalised 11 Feb
    
    self.btnOk.backgroundColor = COLOUR_RED;
    self.btnOk.titleLabel.font = FONT_BUTTON;
    
    [self.btnOk setTitle:LOCALIZATION(C_GLOBAL_OK) forState:UIControlStateNormal];   //lokalised 11 Feb
    return self;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return ROWCOUNT;
    return kRedemption_Approver;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RedemptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RedemptionTableViewCell"];
//    cell.lblTitle.font = FONT_H1;
//    cell.lblValue.font = FONT_B1;

    switch (indexPath.row) {
        case kRedemption_DateOfRedemption:{
            cell.lblTitle.text = LOCALIZATION(C_RUSSIA_DATEOFREDEMPTION);    //lokalised 11 Feb
            cell.lblValue.text = [self.detailDict objectForKey:@"RedeemDate"];
        }
            break;
        case kRedemption_VehicleID:{
            cell.lblTitle.text = LOCALIZATION(C_PROMO_VEHICLEID);    //lokalised 11 Feb
            cell.lblValue.text = [self.detailDict objectForKey:@"VehicleNumber"];
        }
            break;
        case kRedemption_OfferRedeemed:{
            cell.lblTitle.text = LOCALIZATION(C_RUSSIA_OFFERREDEEMED);   //lokalised 11 Feb
            cell.lblValue.text = [self.detailDict objectForKey:@"OfferDetails"];
        }
            break;
        case kRedemption_ProductUsed:{
            cell.lblTitle.text = LOCALIZATION(C_RUSSIA_PRODUCTUSED);     //lokalised 11 Feb
            cell.lblValue.text = [self.detailDict objectForKey:@"productName"];
        }
            break;
//        case kRedemption_ProductCode:{
//            cell.lblTitle.text = LOCALIZATION(@"Product Code");
//            cell.lblValue.text = [self.detailDict objectForKey:@"ProductCodes"];
//        }
//            break;
//        case kRedemption_Quantity:{
//            cell.lblTitle.text = LOCALIZATION(@"Qty");
//            cell.lblValue.text = [self.detailDict objectForKey:@"MaxScan"];
//        }
//            break;
        case kRedemption_RedemptionVerification:{
            cell.lblTitle.text = LOCALIZATION(@"Redemption Verification #");    //later lokalise
            cell.lblValue.text = [self.detailDict objectForKey:@"RedemptionOrderReference"];
            cell.lblValue.textColor = COLOUR_RED;
        }
            break;
        case kRedemption_Approver:{
            cell.lblTitle.text = LOCALIZATION(@"Approved By");
            cell.lblValue.text = [self.detailDict objectForKey:@"ApprovedBy"];  //later lokalise
        }
            break;
            
        default:
            break;
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    return CELL_HEIGHT;
    return UITableViewAutomaticDimension;
}

- (IBAction)okBtnPressed:(id)sender {
    [self removeFromSuperview];
}

@end
