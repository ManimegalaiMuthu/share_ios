//
//  CustomerRangeHeaderTableViewCell.h
//  Shell
//
//  Created by Admin on 22/2/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CustomerRangeHeaderTableViewCellDelegate<NSObject>
-(void) customerRangeInfoPressed;
@end

@interface CustomerRangeHeaderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerRangeHeader;
@property id<CustomerRangeHeaderTableViewCellDelegate> delegate;
@end
