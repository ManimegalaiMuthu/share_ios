//
//  CustomerRangeHeaderTableViewCell.m
//  Shell
//
//  Created by Admin on 22/2/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "CustomerRangeHeaderTableViewCell.h"
#import "AlertViewManager.h"
#import "Constants.h"

@implementation CustomerRangeHeaderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lblCustomerRangeHeader.font = FONT_H1;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)iconBtnPressed:(id)sender {
//    [mAlert showSuccessAlertWithMessage:@"Icon Pressed" onCompletion:nil];
    [self.delegate customerRangeInfoPressed];
}

@end
