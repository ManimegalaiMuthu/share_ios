//
//  CustomerRangeTableViewCell.m
//  Shell
//
//  Created by Admin on 21/2/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "CustomerRangeTableViewCell.h"
#import "Constants.h"
#import "UIImageView+WebCache.h"

@interface CustomerRangeTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;
@property NSMutableDictionary *productGroupDict;
@end

@implementation CustomerRangeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lblTitle.font = FONT_B1;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setupInterface:(NSDictionary *)customRangeDict
{
//    self.imgView.image = [UIImage imageNamed:[customRangeDict objectForKey:@"image"]];
    NSString *urlString = [[customRangeDict objectForKey:@"ProductGroupImage"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"bottle"] options:SDWebImageRefreshCached];
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"bottle"]];
    
    NSDictionary *lightDict = [NSDictionary dictionaryWithObject: FONT_B2 forKey:NSFontAttributeName];
    NSMutableAttributedString *aAttrString1 = [[NSMutableAttributedString alloc] initWithString:[customRangeDict objectForKey:@"ProductGroupName"] attributes: lightDict];
    
    NSDictionary *boldDict = [NSDictionary dictionaryWithObject: FONT_H2 forKey:NSFontAttributeName];
    NSString *counterString = [NSString stringWithFormat:@" (%@)",[customRangeDict objectForKey:@"TargetNumber"]];
    NSMutableAttributedString *aAttrString2 = [[NSMutableAttributedString alloc] initWithString:counterString attributes: boldDict];
    
    
    [aAttrString1 appendAttributedString:aAttrString2];
    self.lblTitle.attributedText = aAttrString1;
    
    self.productGroupDict = [[NSMutableDictionary alloc]initWithDictionary:@{@"ProductGroupID" : [customRangeDict objectForKey:@"ProductGroupID"],
                                                                             @"TargetMembers" : [customRangeDict objectForKey:@"TargetNumber"],
                                                                             }];
}

-(BOOL)isCheckboxSelected
{
    if(self.btnCheckBox.selected)
        return YES;
    else
        return NO;
}

-(NSDictionary*)getGroupDict
{
    return self.productGroupDict;
}

- (IBAction)checkboxBtnPressed:(id)sender {
    if(self.btnCheckBox.selected){
        [self.btnCheckBox setSelected:NO];
        [self.delegate ancillaryOfferDeselectAll];
    }else{
        [self.btnCheckBox setSelected:YES];
        [self.delegate ancillaryOfferSelectAll];
    }
}

-(void)setCheckboxSelected
{
    [self checkboxBtnPressed:self];
}
@end
