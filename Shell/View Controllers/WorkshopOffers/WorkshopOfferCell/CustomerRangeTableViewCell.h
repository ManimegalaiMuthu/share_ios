//
//  CustomerRangeTableViewCell.h
//  Shell
//
//  Created by Admin on 21/2/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol AncillaryOfferAll <NSObject>
-(void)ancillaryOfferSelectAll;
-(void)ancillaryOfferDeselectAll;
@end

@interface CustomerRangeTableViewCell : UITableViewCell
@property id<AncillaryOfferAll> delegate;

-(void)setupInterface:(NSDictionary *)customRangeDict;
-(BOOL)isCheckboxSelected;
-(NSDictionary*)getGroupDict;
-(void)setCheckboxSelected;
@end
