//
//  WorkshopOfferAllOfferListViewController.m
//  Shell
//
//  Created by Jeremy Lua on 26/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "WorkshopOfferAllOfferListViewController.h"

@interface WorkshopOfferAllOfferListViewController () <WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *btnOfferStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnOfferType;
@property (weak, nonatomic) IBOutlet UITableView *offerTableView;
@property NSMutableArray *offerDataArray;
@property NSInteger popupTag;
@end

enum kPopupSelection_Type
{
    kPopupSelection_OfferStatus,
    kPopupSelection_OfferType,
};

@implementation WorkshopOfferAllOfferListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.btnOfferStatus.titleLabel setFont: FONT_B2];
    [self.btnOfferStatus setTitleColor: COLOUR_VERYDARKGREY forState: UIControlStateNormal];
    [self.btnOfferStatus setTitle: LOCALIZATION(C_WORKSHOPOFFER_ALLSTATUS) forState: UIControlStateNormal];
    
    [self.btnOfferType.titleLabel setFont: FONT_B2];
    [self.btnOfferType setTitleColor: COLOUR_VERYDARKGREY forState: UIControlStateNormal];
    [self.btnOfferType setTitle: LOCALIZATION(C_WORKSHOPOFFER_ALLTYPES) forState: UIControlStateNormal];
    
    self.offerDataArray = [[NSMutableArray alloc] init];
    self.offerTableView.estimatedRowHeight = 100;
    self.offerTableView.tableFooterView = [UIView new];
    
    //first load for calendar
    {
//        NSString *offerStatusString = [mSession convertToAncilliaryOfferStatusKeyCode: self.btnOfferStatus.titleLabel.text];
//        NSString *offerTypeString = [mSession convertToAncilliaryOfferTypeKeyCode: self.btnOfferType.titleLabel.text];
        
        [[WebServiceManager sharedInstance]fetchAncillaryOfferVcWithOfferStatus: @"" offerType: @"" vc: self];
    }
    
    if (kIsRightToLeft) {
        [self.btnOfferType setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.btnOfferStatus setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    } else {
        [self.btnOfferType setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.btnOfferStatus setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSString *offerStatusString = [mSession convertToAncilliaryOfferStatusKeyCode: self.btnOfferStatus.titleLabel.text];
    NSString *offerTypeString = [mSession convertToAncilliaryOfferTypeKeyCode: self.btnOfferType.titleLabel.text];
    
    [[WebServiceManager sharedInstance]fetchAncillaryOfferVcWithOfferStatus: offerStatusString offerType:offerTypeString vc: self];
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_WORKSHOPOFFER),LOCALIZATION_EN(C_RUSSIA_OFFERS_LIST)] screenClass:nil];
}

- (IBAction)offerStatusPressed:(id)sender {
    [self popUpListViewWithTitle: LOCALIZATION(C_RUSSIA_OFFERS_SELECTOFFERSTATUS) listArray: [mSession getAncilliaryOfferStatus]];  //lokalised 11 Feb
    
    self.popupTag = kPopupSelection_OfferStatus;
}

- (IBAction)offerTypePressed:(id)sender {
    [self popUpListViewWithTitle: LOCALIZATION(C_RUSSIA_OFFERS_SELECTOFFERTYPE) listArray: [mSession getAncilliaryOfferType]];  //lokalised 11 Feb
    
    self.popupTag = kPopupSelection_OfferType;
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        switch (self.popupTag)
        {
            case kPopupSelection_OfferStatus:
            {
                
                NSString *offerStatusString = [mSession convertToAncilliaryOfferStatusKeyCode: selection.firstObject];
                NSString *offerTypeString = [mSession convertToAncilliaryOfferTypeKeyCode: self.btnOfferType.titleLabel.text];
                
                [self.btnOfferStatus setTitle:selection.firstObject forState:UIControlStateNormal];
                
                [[WebServiceManager sharedInstance]fetchAncillaryOfferVcWithOfferStatus: offerStatusString offerType:offerTypeString vc: self];
            }
                break;
            case kPopupSelection_OfferType:
            {
                NSString *offerStatusString = [mSession convertToAncilliaryOfferStatusKeyCode: self.btnOfferStatus.titleLabel.text];
                NSString *offerTypeString = [mSession convertToAncilliaryOfferTypeKeyCode: selection.firstObject];
                
                [self.btnOfferType setTitle:selection.firstObject forState:UIControlStateNormal];
                
                [[WebServiceManager sharedInstance]fetchAncillaryOfferVcWithOfferStatus: offerStatusString offerType:offerTypeString vc: self];
            }
                break;
            default:
                break;
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cellData = [self.offerDataArray objectAtIndex: indexPath.row];
    
//    NSString *offerID = [cellData objectForKey:@"OfferID"];
//    NSString *offerType = [cellData objectForKey:@"OfferType"];
//    NSString *startDate = [cellData objectForKey:@"StartDate"];
//    NSString *endDate = [cellData objectForKey:@"EndDate"];
//    NSDictionary *offerDict = @{@"OfferID" : offerID,
//                                @"OfferType" : offerType,
//                                @"StartDate" : startDate,
//                                @"EndDate" : endDate
//                                };
    
//    //WorkshopOfferAllOfferVC in tab view.
//    [mSession pushOfferDetailsView:self.parentViewController offerDict:offerDict];
    
    //fetch ancilliary details here
    [mSession pushRUOfferDetailsView:self.parentViewController offerDict:cellData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.offerDataArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"ListViewCell"];
    
    NSDictionary *cellData = [self.offerDataArray objectAtIndex: indexPath.row];
    
    UIImageView *imgThumbnail = [cell viewWithTag: 1];
    [imgThumbnail sd_setImageWithURL: [NSURL URLWithString: [cellData objectForKey:@"OfferTypeImageUrl"]] placeholderImage: [UIImage imageNamed: @"icn_offer-workshop_darkgrey"] options: (SDWebImageRefreshCached | SDWebImageRetryFailed)];

    UILabel *lblOfferTitle = [cell viewWithTag: 2];
    lblOfferTitle.font = FONT_H2;
    lblOfferTitle.text = [cellData objectForKey: @"OfferTitle"];
    
    UILabel *lblOfferDateRange = [cell viewWithTag: 3];
    lblOfferDateRange.font = FONT_B2;
//    lblOfferDateRange.text = [NSString stringWithFormat: @"%@ - %@", [cellData objectForKey: @"StartDate"], [cellData objectForKey: @"EndDate"]];
    lblOfferDateRange.text = [NSString stringWithFormat: @"%@", [cellData objectForKey: @"OfferPeriod"]];
    
    UIImageView *imgArrow = [cell viewWithTag:5];
    if (kIsRightToLeft) {
        imgArrow.image = [UIImage imageNamed:@"icon-arrsingleL.png"];
    } else {
        imgArrow.image = [UIImage imageNamed:@"icon-arrsingleR.png"];
    }

    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch(response.webserviceCall)
    {
        case kWEBSERVICE_FETCHANCILLARY:
        {   
            if (ISNULL([[response getGenericResponse] objectForKey: @"AncilliaryOffers"]))
                self.offerDataArray = @[];
            else
                self.offerDataArray = [[response getGenericResponse] objectForKey: @"AncilliaryOffers"];
            
            [self.offerTableView reloadData];
            
            //reload calendar
            [self.delegate reloadAllOffersList: [[response getGenericResponse] objectForKey: @"AncilliaryOffers"]];
        }
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
