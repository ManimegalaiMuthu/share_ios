//
//  CurrentOfferViewController.m
//  Shell
//
//  Created by Admin on 2/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "CurrentOfferViewController.h"
#import "CurrentOffersTableViewCell.h"
#import "CurrentOfferHeaderTableViewCell.h"
#import "CurrentOfferSectionHeaderTableViewCell.h"

enum kFromSection_Type
{
    kFromSection_ONGOING = 0,
    kFromSection_UPCOMING = 1,
};

#define SECTION_HEADER_HEIGHT 50

@interface CurrentOfferViewController ()<UITableViewDelegate,UITableViewDataSource,WebServiceManagerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSMutableArray *ongoingArray;
@property NSMutableArray *upcomingArray;
@property BOOL isOngingClicked;
@property BOOL isUpcomingClicked;
@property NSMutableDictionary *referenceHeaderDict;
@end

@implementation CurrentOfferViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupTable];
    [[WebServiceManager sharedInstance]fetchCurrentOfferVc:self];
}

-(void)setupTable
{
    self.referenceHeaderDict = [[NSMutableDictionary alloc]init];
    [self.tableView registerNib:[UINib nibWithNibName:@"CurrentOfferSectionHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"CurrentOfferSectionHeaderTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"CurrentOffersTableViewCell" bundle:nil] forCellReuseIdentifier:@"CurrentOffersTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"CurrentOfferHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"CurrentOfferHeaderTableViewCell"];
    
    self.tableView.tableFooterView = [UIView new];
    self.isOngingClicked = NO;
    self.isUpcomingClicked = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0){
        return self.ongoingArray.count+2;
    }else{
        return self.upcomingArray.count+2;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        if(indexPath.row == 0){
            CurrentOfferSectionHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CurrentOfferSectionHeaderTableViewCell"];
            cell.lblTitle.font = FONT_B1;
            cell.lblTitle.text = LOCALIZATION(@"ONGOING");  //later lokalise
            [self.referenceHeaderDict setObject:cell forKey:@"ongoing"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }else if(self.isOngingClicked == NO){
            CurrentOfferHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CurrentOfferHeaderTableViewCell"];
            cell.hidden = YES;
            return cell;
        }else if(indexPath.row == 1){
            CurrentOfferHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CurrentOfferHeaderTableViewCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.hidden = NO;
            return cell;
        }else{
            CurrentOffersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CurrentOffersTableViewCell"];
            NSDictionary *individualDict = [self.ongoingArray objectAtIndex:indexPath.row-2];
            [cell setupInterface:individualDict];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.hidden = NO;
            return cell;
        }
    }else{
        if(indexPath.row == 0){
            CurrentOfferSectionHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CurrentOfferSectionHeaderTableViewCell"];
            cell.lblTitle.font = FONT_B1;
            cell.lblTitle.text = LOCALIZATION(@"UPCOMING"); //later lokalise
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [self.referenceHeaderDict setObject:cell forKey:@"upcoming"];
            return cell;
        }else if(self.isUpcomingClicked == NO){
            CurrentOfferHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CurrentOfferHeaderTableViewCell"];
            cell.hidden = YES;
            return cell;
        }else if(indexPath.row == 1){
            CurrentOfferHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CurrentOfferHeaderTableViewCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.hidden = NO;
            return cell;
        }else{
            CurrentOffersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CurrentOffersTableViewCell"];
            NSDictionary *individualDict = [self.upcomingArray objectAtIndex:indexPath.row-2];
            [cell setupInterface:individualDict];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.hidden = NO;
            return cell;
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0 && indexPath.row == 0){
        CurrentOfferSectionHeaderTableViewCell *cell = [self.referenceHeaderDict objectForKey:@"ongoing"];
        if(self.isOngingClicked){
            cell.imageIcon.image = [UIImage imageNamed:@"icon-expand"];
            self.isOngingClicked = NO;
        }else{
            cell.imageIcon.image = [UIImage imageNamed:@"icon-collapse"];
            self.isOngingClicked = YES;
        }
        
        [tableView beginUpdates];
        [tableView endUpdates];
        [self.tableView reloadData];
    }else if(indexPath.section == 1 && indexPath.row == 0){
        CurrentOfferSectionHeaderTableViewCell *cell = [self.referenceHeaderDict objectForKey:@"upcoming"];
        if(self.isUpcomingClicked){
            cell.imageIcon.image = [UIImage imageNamed:@"icon-expand"];
            self.isUpcomingClicked = NO;
        }else{
            cell.imageIcon.image = [UIImage imageNamed:@"icon-collapse"];
            self.isUpcomingClicked = YES;
        }
        
        [tableView beginUpdates];
        [tableView endUpdates];
        [self.tableView reloadData];
    }else if(indexPath.row>=2){
        
        switch (indexPath.section) {
            case kFromSection_ONGOING:{
                NSString *offerID = [[self.ongoingArray objectAtIndex:indexPath.row-2] objectForKey:@"OfferID"];
                NSString *offerType = [[self.ongoingArray objectAtIndex:indexPath.row-2] objectForKey:@"OfferType"];
                NSString *startDate = [[self.ongoingArray objectAtIndex:indexPath.row-2] objectForKey:@"StartDate"];
                NSString *endDate = [[self.ongoingArray objectAtIndex:indexPath.row-2] objectForKey:@"EndDate"];
                NSDictionary *offerDict = @{@"offerID" : offerID,
                                            @"offerType" : offerType,
                                            @"startDate" : startDate,
                                            @"endDate" : endDate
                                            };
                [mSession pushOfferDetailsView:self.navigationController.parentViewController offerDict:offerDict];
            }
                break;
            case kFromSection_UPCOMING:{
                NSString *offerID = [[self.upcomingArray objectAtIndex:indexPath.row-2] objectForKey:@"OfferID"];
                NSString *offerType = [[self.upcomingArray objectAtIndex:indexPath.row-2] objectForKey:@"OfferType"];
                NSString *startDate = [[self.upcomingArray objectAtIndex:indexPath.row-2] objectForKey:@"StartDate"];
                NSString *endDate = [[self.upcomingArray objectAtIndex:indexPath.row-2] objectForKey:@"EndDate"];
                NSDictionary *offerDict = @{@"offerID" : offerID,
                                            @"offerType" : offerType,
                                            @"startDate" : startDate,
                                            @"endDate" : endDate
                                            };
                [mSession pushOfferDetailsView:self.navigationController.parentViewController offerDict:offerDict];
            }
                break;
            default:
                break;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        if(self.isOngingClicked){
            return UITableViewAutomaticDimension;
        }else{
            if(indexPath.row == 0)
                return SECTION_HEADER_HEIGHT;
            else
                return 0;
        }
    }else{
        if(self.isUpcomingClicked){
            return UITableViewAutomaticDimension;
        }else{
            if(indexPath.row == 0)
                return SECTION_HEADER_HEIGHT;
            else
                return 0;
        }
    }
}
-(void)addData:(NSDictionary *)responseDict
{
    
    if([[responseDict objectForKey:@"ongoingOffers"] isEqual:[NSNull null]]){
        self.ongoingArray = [[NSMutableArray alloc]initWithArray:@[]];
    }else{
        self.ongoingArray = [[NSMutableArray alloc]initWithArray:[responseDict objectForKey:@"ongoingOffers"]];
    }
    
    if([[responseDict objectForKey:@"upComingOffers"] isEqual:[NSNull null]]){
        self.upcomingArray = [[NSMutableArray alloc]initWithArray:@[]];
    }else{
        self.upcomingArray = [[NSMutableArray alloc]initWithArray:[responseDict objectForKey:@"upComingOffers"]];
    }
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.tableView setNeedsLayout];
    [self.tableView layoutIfNeeded];
    
    [self.tableView reloadData];
}

- (void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_CURRENTOFFER:{
            if([[response getGenericResponse] isEqual:[NSNull null]] || [response getGenericResponse] == nil){
                return;
            }
            [self addData:[response getGenericResponse]];
            
        }
            break;
            
        default:
            break;
    }
}

- (void)processFailed:(WebServiceResponse *)response
{
    
}


@end
