//
//  CurrentOfferSectionHeaderTableViewCell.h
//  Shell
//
//  Created by Admin on 2/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurrentOfferSectionHeaderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;

@end
