//
//  CalendarEventViewController.h
//  Shell
//
//  Created by Admin on 22/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface CalendarEventViewController : BaseVC
@property NSArray* offersDataArray;
-(void) reloadCalendar;
@end
