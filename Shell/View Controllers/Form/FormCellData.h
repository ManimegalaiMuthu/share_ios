//
//  FormCellData.h
//  Shell
//
//  Created by Jeremy Lua on 8/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Constants.h"

NS_ASSUME_NONNULL_BEGIN

@interface FormCellData : NSObject
@property kREGISTRATION_TYPE type;  //Type of Cell
@property NSString *key;            //Key tagged to cell

@property NSString *title;
@property NSString *value;  //value coming from dictionary needs to be converted to stirng first.
@property NSString *placeholder;
@property BOOL secureEntry;
@property BOOL enabled;
@property BOOL optional;
@property UIKeyboardType keyboard;
@property BOOL textfieldEnabled;
@property NSString *icon;
@property NSString *countryCode;
@property NSString *mobileNumber;
@property NSString *statusVerify;
@property NSString *iconStatus;
@property NSDictionary *workingHoursDict;
@property BOOL checkBoxSelected;

@property UIColor *textColour;
@property UIColor *backgroundColour;

@property NSDictionary *hyperlinks;

@property NSArray *checkboxLabelsArray;
@property NSArray *linkedCellKeysArray;

@property NSArray *linkedWorkshopDict;
@property BOOL hasDelegate;
@property BOOL hidden; //0 height if YES

-(instancetype) initWithData:(NSDictionary *) data;

@end



NS_ASSUME_NONNULL_END
