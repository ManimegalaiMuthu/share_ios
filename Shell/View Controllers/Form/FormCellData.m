//
//  FormCellData.m
//  Shell
//
//  Created by Jeremy Lua on 8/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "FormCellData.h"

/*
 Model class to hold data only
 */
@implementation FormCellData

//#define kForm_Type          @"Type"
//#define kForm_Title         @"Key"
//#define kForm_Key           @"Title"
//#define kForm_SecureEntry   @"SecureEntry"
//#define kForm_Value         @"Value"
//#define kForm_Enabled       @"Enabled"
//#define kForm_Optional      @"Optional"
//#define kForm_Placeholder   @"Placeholder"
//#define kForm_Keyboard      @"Keyboard"
//#define kForm_TextfieldEnabled @"TextfieldEnabled"
//#define kForm_Icon              @"Icon"
//#define kForm_CountryCode              @"CountryCode"
//#define kForm_MobileNumber              @"MobileNumber"
//#define kForm_WorkingHours              @"WorkingHours"
//#define kForm_ @""

-(instancetype) initWithData:(NSDictionary *) data
{
    self = [super init];
    
    if (self)
    {
        if ([data objectForKey: kForm_Title])
            self.title = [data objectForKey: kForm_Title];
        
        if ([data objectForKey: kForm_Key])
            self.key = [data objectForKey: kForm_Key];
        
        if ([data objectForKey: kForm_SecureEntry])
            self.secureEntry = [[data objectForKey: kForm_SecureEntry] boolValue];
        
        self.type = (kREGISTRATION_TYPE) [[data objectForKey: kForm_Type] intValue];
        
        if ([data objectForKey: kForm_Value])
            self.value = [data objectForKey: kForm_Value];
        
        if ([data objectForKey: kForm_Enabled])
            self.enabled = [[data objectForKey: kForm_Enabled] boolValue];
        else
            self.enabled = YES;
        
        if ([data objectForKey: kForm_Optional])
            self.optional = [[data objectForKey: kForm_Optional] boolValue];
        
        if ([data objectForKey: kForm_Keyboard])
            self.keyboard = (UIKeyboardType) [[data objectForKey: kForm_Keyboard] intValue];
        
        if ([data objectForKey: kForm_TextfieldEnabled])
            self.textfieldEnabled = [[data objectForKey: kForm_TextfieldEnabled] boolValue];
        
        if ([data objectForKey: kForm_Icon])
            self.icon = [data objectForKey: kForm_Icon];
        
        if ([data objectForKey: kForm_StatusIcon]) {
            self.iconStatus = [data objectForKey:kForm_StatusIcon];
        }
        
        if ([data objectForKey: kForm_StatusVerify]) {
            self.statusVerify = [data objectForKey:kForm_StatusVerify];
        }
        
        if ([data objectForKey: kForm_CountryCode])
            self.countryCode = [data objectForKey: kForm_CountryCode];
        
        if ([data objectForKey: kForm_MobileNumber])
            self.mobileNumber = [data objectForKey: kForm_MobileNumber];
        
        if ([data objectForKey: kForm_WorkingHours])
            self.workingHoursDict = [data objectForKey: kForm_WorkingHours];
        
        if ([data objectForKey: kForm_TextColour])
            self.textColour = [data objectForKey: kForm_TextColour];
        
        if ([data objectForKey: kForm_BackgroundColour])
            self.backgroundColour = [data objectForKey: kForm_BackgroundColour];
        
        if ([data objectForKey: kForm_Hyperlinks])
            self.hyperlinks = [data objectForKey: kForm_Hyperlinks];
        
        if ([data objectForKey: kForm_CheckboxSelected])
            self.checkBoxSelected = [[data objectForKey: kForm_CheckboxSelected] boolValue];
        
        if ([data objectForKey: kForm_Placeholder])
            self.placeholder = [data objectForKey: kForm_Placeholder];
        
        if ([data objectForKey: kForm_HasDelegate])
            self.hasDelegate = [[data objectForKey: kForm_HasDelegate] boolValue];
        
        if ([data objectForKey: kForm_IsHidden])
            self.hidden = [[data objectForKey: kForm_IsHidden] boolValue];
        
        if ([data objectForKey: kForm_CheckboxLabels])
            self.checkboxLabelsArray = [data objectForKey: kForm_CheckboxLabels];
        
        if ([data objectForKey: kForm_LinkedCellKeys])
            self.linkedCellKeysArray = [data objectForKey: kForm_LinkedCellKeys];
        
        if([data objectForKey: kForm_LinkedWorkshopDict])
            self.linkedWorkshopDict = [data objectForKey: kForm_LinkedWorkshopDict];
        
    }
    return self;
}

@end
