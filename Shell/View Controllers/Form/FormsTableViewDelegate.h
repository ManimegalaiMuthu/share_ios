//
//  FormsTableViewDelegate.h
//  Shell
//
//  Created by Jeremy Lua on 8/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"


NS_ASSUME_NONNULL_BEGIN
@protocol FormsTableViewDelegate
//-(void) dropdownPressedWithKey:(NSString *) formKey;
-(void) dropdownPressedWithKey:(NSString *) formKey;
-(void) birthdatePressedWithKey:(NSString *) formKey;
-(void) ctaPressedWithKey:(NSString *) ctaKey;
-(void) textfieldWithButtonPressedWithKey:(NSString *) buttonKey;
-(void) hyperlinkPressed:(NSString *)link;
-(void) didPressWorkingHours;
-(void) showPhoto:(UIImage *) image;
-(void) takePhoto: (NSString *) cellKey;
-(void) tableViewDidScroll:(UIScrollView*)tableView;

-(void) textfieldCellDidBeginEditingWithKey: (NSString *) key;
-(void) textfieldCellDidEndEditingWithKey: (NSString *) key;
-(BOOL) textFieldCell:(NSString *) key shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
-(void) signaturePressed: (NSString *) key;
-(void)contactPrefCellDidSelectCell:(NSInteger) index isSelected:(BOOL) isSelected;
-(void) attributedLabeldidSelectLinkWithURL:(NSURL *)url;
@end

@interface FormsTableViewDelegate : NSObject
@property id<UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate, FormsTableViewDelegate> delegate;
@property id<UITableViewDataSource> dataSource;
@property id<UITableViewDataSourcePrefetching> prefetchDataSource;

/* Table Data Handler
 Element count = section count;
 Each element should be another array of dictionaries.
 Each dictionaries of child array should contain the necessary form data (keys, value, cell type, etc.)
 E.g: [ [{"Title": "this is a title",
            "Key": "myKey"},
         {"Title": "title two",
            "Key": "myKeyTwo"},
        ]
        [{"Title": "this is section two",
         "Key": "mySectionTwoKey"},
         {"Title": "section two title two",
         "Key": "mySectionTwoKeyTwo"},
        ]
 
 */
@property NSMutableArray *initializingArray;
@property NSMutableArray *headersArray;

//@property NSMutableArray *headerTitleArray;
-(void) registerNib: (UITableView *) regFormTableView;

-(void) editCell: (NSString *) formKey toSecureField:(BOOL) secured;
-(void) editCell: (NSString *) formKey withTextfieldString: (NSString *) text;
-(void) editCell: (NSString *) formKey withDropdownSelection: (NSString *) selectionString;
-(void) editCell: (NSString *) formKey withTextfieldButtonText: (NSString *) textfieldString;
-(void) editCell: (NSString *) formKey withImage: (UIImage *) image;
-(void) editCell: (NSString *) formKey withSignatureImage:(nonnull UIImage *)image;

-(void) updateWorkingHours:(NSDictionary *) workingHoursDict;

-(NSMutableDictionary *) getJsonValues;

@end

NS_ASSUME_NONNULL_END
