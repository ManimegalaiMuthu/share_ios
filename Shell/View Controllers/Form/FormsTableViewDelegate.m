//
//  FormsTableViewDelegate.m
//  Shell
//
//  Created by Jeremy Lua on 8/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "FormsTableViewDelegate.h"
#import "FormCellData.h"
#import "BaseVC.h"

#import "FormButtonCell.h"
#import "FormMobileNumberCell.h"
#import "FormTextfieldCell.h"
#import "FormDropdownCell.h"
#import "FormTextfieldWithButtonCell.h"
#import "FormLabelTextfieldTableViewCell.h"
#import "FormTextViewTableViewCell.h"
#import "FormHeaderTableViewCell.h"
#import "FormWorkingHoursTableViewCell.h"
#import "FormWorkingHoursLabelTableViewCell.h"
#import "ContactPrefTableViewCell.h"
#import "FormCTAButtonCell.h"
#import "FormCheckboxCell.h"
#import "FormPANCardIDCell.h"
#import "FormPANCardImageCell.h"
#import "FormImageCell.h"
#import "FormMultiCheckboxCell.h"
#import "FormPanCardIDCell_New.h"
#import "FormDriverLicenseCell.h"
#import "FormVoterIDCell.h"
#import "FormLinkedWorkshopViewCell.h"
#import "FormOnlyLabelTableViewCell.h"
#import "FormStatusTableViewCell.h"
#import "FormBankDetailStatusViewCell.h"
#import "FormPanCardIDCell_NewStatus.h"


@interface FormsTableViewDelegate() <UITableViewDelegate, UITableViewDataSource, FormDropdownCellDelegate, FormCTACellDelegate, FormTextfieldWithButtonCellDelegate, FormCheckboxCellDelegate, FormPANCardIDCellDatasource, FormTextfieldDelegate, UITextFieldDelegate, UITextViewDelegate, FormPANCardImageCellDelegate, SignatureTableViewCellDelegate, FormMultiCheckboxCellDelegate,FormOnlyLabelTableViewCellDelegate,FormBankDetailStatusViewCellDelegate>

/* Cell Reference
 Each element is a reference to the cell by a key.
 Element and key is unique and should not be repeated or stored in sections
 */
@property NSMutableDictionary *cellReferenceDict;

#define kFormTextfieldCell              @"FormTextfieldCell"
#define kFormButtonCell                  @"FormButtonCell"
#define kFormDropdownCell               @"FormDropdownCell"
#define kFormHeaderTableViewCell         @"FormHeaderTableViewCell"
#define kFormLabelTextfieldTableViewCell @"FormLabelTextfieldTableViewCell"
#define kFormMobileNumberCell            @"FormMobileNumberCell"
#define kFormTextfieldWithButtonCell     @"FormTextfieldWithButtonCell"
#define kFormTextViewTableViewCell       @"FormTextViewTableViewCell"
#define kFormWorkingHoursTableViewCell   @"FormWorkingHoursTableViewCell"
#define kContactPrefTableViewCell       @"ContactPrefTableViewCell"
#define kWorkingHours                   @"WorkingHours"
#define kSignatureTableViewCell         @"SignatureTableViewCell"

#define kFormCTAButtonCell              @"FormCTAButtonCell"
#define kFormCheckboxCell               @"FormCheckboxCell"
#define kFormPANCardIDCell              @"FormPANCardIDCell"
#define kFormPANCardImageCell           @"FormPANCardImageCell"
#define kFormIDInputCell                @"FormIDInputCell"
#define kFormImageCell                  @"FormImageCell"
#define kFormMultiCheckboxCell          @"FormMultiCheckboxCell"
#define kFormPanCardIDCell_New              @"FormPanCardIDCell_New"
#define kFormPanCardIDCell_NewStatus              @"FormPanCardIDCell_NewStatus"
#define kFormDriverLicenseCell              @"FormDriverLicenseCell"
#define kFormVoterIDCell              @"FormVoterIDCell"
#define kFormLinkedWorkshopViewCell     @"FormLinkedWorkshopViewCell"
#define kFormOnlyLabelTableViewCell     @"FormOnlyLabelTableViewCell"
#define kFormStatusTableViewCell        @"FormStatusTableViewCell"
#define kFormBankDetailStatusViewCell   @"FormBankDetailStatusViewCell"

//customised cells
#define kValidateCheckboxes @"ValidateCheckboxes"
#define kPANCardId              @"PANCardId"
#define kPANCardImage           @"PANCardImage"
#define kDrivingLicense         @"DrivingLicenseID"
#define kDrivingLicenseImage    @"DrivingLicenseImage"
#define kVoterID                @"VoterID"
#define kVoterIDImage           @"VoterIDImage"
@end

enum
{
    TextFieldPostalCode = 100,
};

@implementation FormsTableViewDelegate

-(instancetype) init
{
    self = [super init];
    if (self)
    {
        self.initializingArray = [[NSMutableArray alloc] init];
        self.headersArray = [[NSMutableArray alloc] init];
        self.cellReferenceDict = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

//-(instancetype) initWithArray: (nonnull NSMutableArray *) initArray
//{
//    self = [super init];
//    if (self)
//    {
//        self.initializingArray = initArray;
//        self.cellReferenceDict = [[NSMutableDictionary alloc] init];
//    }
//    return self;
//}


-(void) registerNib: (UITableView *) regFormTableView
{
    [regFormTableView registerNib:[UINib nibWithNibName:kFormHeaderTableViewCell bundle:nil] forCellReuseIdentifier:kFormHeaderTableViewCell];
    
    [regFormTableView registerNib:[UINib nibWithNibName:kFormTextfieldCell bundle:nil] forCellReuseIdentifier:kFormTextfieldCell];
    [regFormTableView registerNib:[UINib nibWithNibName:kFormButtonCell bundle:nil] forCellReuseIdentifier:kFormButtonCell];
    [regFormTableView registerNib:[UINib nibWithNibName:kFormDropdownCell bundle:nil] forCellReuseIdentifier:kFormDropdownCell];
    [regFormTableView registerNib:[UINib nibWithNibName:kFormLabelTextfieldTableViewCell bundle:nil] forCellReuseIdentifier:kFormLabelTextfieldTableViewCell];
    [regFormTableView registerNib:[UINib nibWithNibName:kFormMobileNumberCell bundle:nil] forCellReuseIdentifier:kFormMobileNumberCell];
    [regFormTableView registerNib:[UINib nibWithNibName:kFormTextfieldWithButtonCell bundle:nil] forCellReuseIdentifier:kFormTextfieldWithButtonCell];
    [regFormTableView registerNib:[UINib nibWithNibName:kFormTextViewTableViewCell bundle:nil] forCellReuseIdentifier:kFormTextViewTableViewCell];
    [regFormTableView registerNib:[UINib nibWithNibName:kFormWorkingHoursTableViewCell bundle:nil] forCellReuseIdentifier:kFormWorkingHoursTableViewCell];
    [regFormTableView registerNib:[UINib nibWithNibName:kFormPANCardImageCell bundle:nil] forCellReuseIdentifier:kFormPANCardImageCell];
    
    [regFormTableView registerNib:[UINib nibWithNibName:kContactPrefTableViewCell bundle:nil] forCellReuseIdentifier:kContactPrefTableViewCell];
    
    [regFormTableView registerNib:[UINib nibWithNibName: kFormCTAButtonCell bundle:nil] forCellReuseIdentifier:kFormCTAButtonCell];
    [regFormTableView registerNib:[UINib nibWithNibName: kFormMultiCheckboxCell bundle:nil] forCellReuseIdentifier:kFormMultiCheckboxCell];
    [regFormTableView registerNib:[UINib nibWithNibName: kFormPanCardIDCell_New bundle:nil] forCellReuseIdentifier:kFormPanCardIDCell_New];
    [regFormTableView registerNib:[UINib nibWithNibName: kFormPanCardIDCell_NewStatus bundle:nil] forCellReuseIdentifier:kFormPanCardIDCell_NewStatus];
    [regFormTableView registerNib:[UINib nibWithNibName: kFormDriverLicenseCell bundle:nil] forCellReuseIdentifier:kFormDriverLicenseCell];
    [regFormTableView registerNib:[UINib nibWithNibName: kFormVoterIDCell bundle:nil] forCellReuseIdentifier:kFormVoterIDCell];
    
    [regFormTableView registerNib:[UINib nibWithNibName:kFormLinkedWorkshopViewCell bundle:nil] forCellReuseIdentifier:kFormLinkedWorkshopViewCell];
    [regFormTableView registerNib:[UINib nibWithNibName:kFormOnlyLabelTableViewCell bundle:nil] forCellReuseIdentifier:kFormOnlyLabelTableViewCell];
    [regFormTableView registerNib:[UINib nibWithNibName:kFormStatusTableViewCell bundle:nil] forCellReuseIdentifier:kFormStatusTableViewCell];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {

    NSArray *sectionArray = [self.initializingArray objectAtIndex: indexPath.section];
    FormCellData *cellData = [[FormCellData alloc] initWithData: [sectionArray objectAtIndex: indexPath.row]];
    
    //already have cell reference, stop here
    if ([self.cellReferenceDict objectForKey: cellData.key])
    {
        return [self.cellReferenceDict objectForKey: cellData.key];
    }

    switch (cellData.type)
    {
        case kREGISTRATION_TEXTFIELD:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormTextfieldCell owner:self options:nil];
            FormTextfieldCell *textfieldCell = [nib objectAtIndex:0];
            
            if (cellData.keyboard)
                [textfieldCell setKeyboard: cellData.keyboard];
            if (cellData.secureEntry)
                [textfieldCell setSecureTextEntry: cellData.secureEntry];

            //if Enabled Key is enabled
            [textfieldCell enableCell: cellData.enabled];
            
            [textfieldCell setTitle: cellData.title];

            if (cellData.optional)
            {
                [textfieldCell setTitle: cellData.title];
            }
            else
            {
                [textfieldCell setRequiredTitle: [self addAsteriskForRequiredField: cellData.title enabled: cellData.enabled] placeholder: cellData.title];
            }
            
            [textfieldCell hideTopLabel: NO];
            if (cellData.hasDelegate)
                [textfieldCell setTextfieldDelegate: self];
            [textfieldCell setCellKey: cellData.key];
            
            if (cellData.value)
                [textfieldCell setTextfieldText: cellData.value];
            
            if (cellData.placeholder)
                [textfieldCell setPlaceholder: cellData.placeholder];
            
            [self.cellReferenceDict setObject: textfieldCell forKey: cellData.key];
            return textfieldCell;
        }
            break;
            
        case kREGISTRATION_STATUSVEIFICATION:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormBankDetailStatusViewCell owner:self options:nil];
            FormBankDetailStatusViewCell *textfieldCell = [nib objectAtIndex:0];
            
            if (cellData.keyboard)
                [textfieldCell setKeyboard: cellData.keyboard];
            if (cellData.secureEntry)
                [textfieldCell setSecureTextEntry: cellData.secureEntry];

            //if Enabled Key is enabled
            [textfieldCell enableCell: cellData.enabled];
            
            [textfieldCell setTitle: cellData.title];
            
          //  [textfieldCell setIconImage:cellData.iconStatus];
            [textfieldCell setStatusTitle:cellData.statusVerify];
            
         //   [textfieldCell setTitle:cellData.statusVerify];

            if (cellData.optional)
            {
                [textfieldCell setTitle: cellData.title];
            }
            else
            {
                [textfieldCell setRequiredTitle: [self addAsteriskForRequiredField: cellData.title enabled: cellData.enabled] placeholder: cellData.title];
            }
            
            [textfieldCell hideTopLabel: NO];
            if (cellData.hasDelegate)
                [textfieldCell setTextfieldDelegate: self];
            [textfieldCell setCellKey: cellData.key];
            
            if (cellData.value)
                [textfieldCell setTextfieldText: cellData.value];
            
            if (cellData.placeholder)
                [textfieldCell setPlaceholder: cellData.placeholder];
            
            [self.cellReferenceDict setObject: textfieldCell forKey: cellData.key];
            return textfieldCell;
        }
           break;
        case kREGISTRATION_DROPDOWN:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormDropdownCell owner:self options:nil];
            FormDropdownCell *dropdownCell = [nib objectAtIndex:0];
            
            [dropdownCell setUserInteractionEnabled: cellData.enabled];
            
            [dropdownCell setTitle: cellData.title];
            
            if (!cellData.value || [cellData.value isEqualToString: @""])
            {
               //dropdown cannot be blank
               //set initial value, blank dropdown defaulted from conversion
               [dropdownCell setSelectionTitle: cellData.placeholder textColor:COLOUR_LIGHTGREY];
            }
            else
            {
               [dropdownCell setSelectionTitle: cellData.value textColor:COLOUR_DARKGREY];
            }
            
            if (cellData.optional)
            {
                [dropdownCell setTitle: cellData.title];
            }
            else
            {
                [dropdownCell setRequiredTitle: [self addAsteriskForRequiredField: cellData.title enabled: cellData.enabled] placeholder: cellData.title];
            }
            [dropdownCell hideTopLabel: NO];
            
            //keep a reference of cell to submit json value later
            [self.cellReferenceDict setObject: dropdownCell forKey: cellData.key];
            [dropdownCell setDropdownKey: cellData.key];
            [dropdownCell setDropdownDelegate: self];
            
            if (kIsRightToLeft) {
                [dropdownCell setButtonContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
            } else {
                [dropdownCell setButtonContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            }
            
            return dropdownCell;
        }
            break;
        case kREGISTRATION_TEXTFIELDWITHBUTTON:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormTextfieldWithButtonCell owner:self options:nil];
            FormTextfieldWithButtonCell *textfieldCell = [nib objectAtIndex:0];
            
            //see if Enabled key is available, if not see if pending or approved
            [textfieldCell setUserInteractionEnabled: cellData.enabled];
            
            [textfieldCell setTextfieldEnabled: cellData.textfieldEnabled];
            
            [textfieldCell setTitle: cellData.title];
            [textfieldCell setIconImage: cellData.icon];
            
            if (cellData.optional)
            {
                [textfieldCell setTitle: cellData.title];
            }
            else
            {
                [textfieldCell setRequiredTitle: [self addAsteriskForRequiredField: cellData.title enabled: cellData.enabled] placeholder: cellData.title];
            }
            [textfieldCell hideTopLabel: NO];
            [textfieldCell setTextfieldDelegate: self];
            
            if (cellData.enabled)
            {
                [textfieldCell setButtonDelegate: self];
                [textfieldCell setButtonKey: cellData.key];
            }
            
            if (cellData.value)
                [textfieldCell setTextfieldText: cellData.value];
            [self.cellReferenceDict setObject: textfieldCell forKey: cellData.key];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_BUTTON:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormButtonCell owner:self options:nil];
            FormButtonCell *buttonCell = [nib objectAtIndex:0];
            
            [buttonCell setUserInteractionEnabled: cellData.enabled];
            
            [buttonCell setTitle: cellData.title];
            [buttonCell setIconImage: cellData.icon];
            [buttonCell hideTopLabel: NO];
            
            [self.cellReferenceDict setObject: buttonCell forKey: cellData.key];
            
            
            return buttonCell;
        }
            break;
        case kREGISTRATION_MOBNUM:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormMobileNumberCell owner:self options:nil];
            FormMobileNumberCell *mobNumCell = [nib objectAtIndex:0];
            
            [mobNumCell setTitle: cellData.title];
            
            //see if Enabled key is available, if not see if pending or approved
            [mobNumCell setUserInteractionEnabled: cellData.enabled];
            [mobNumCell hideTopLabel: NO];
            [mobNumCell  setTextfieldDelegate: self];
            
            [mobNumCell setCountryCode: cellData.countryCode];
            [mobNumCell setMobileNumber: cellData.mobileNumber];
            
            [mobNumCell setTextfieldEnabled: cellData.textfieldEnabled];
            [mobNumCell setMobileCodeEnabled: NO];
            
            if (cellData.optional)
            {
                [mobNumCell setTitle: cellData.title];
            }
            else
            {
                [mobNumCell setRequiredTitle: [self addAsteriskForRequiredField: cellData.title enabled: cellData.enabled] placeholder:cellData.title];
            }
            
            [self.cellReferenceDict setObject: mobNumCell forKey: cellData.key];
            return mobNumCell;
        }
            break;
        case kREGISTRATION_LABELWITHTEXTFIELD:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormLabelTextfieldTableViewCell owner:self options:nil];
            FormLabelTextfieldTableViewCell *textfieldCell = [nib objectAtIndex:0];
            
            if (cellData.keyboard)
                [textfieldCell setKeyboard: cellData.keyboard];
            
            if (cellData.secureEntry)
                [textfieldCell setSecureTextEntry: cellData.secureEntry];
            //see if Enabled key is available, if not see if pending or approved
            [textfieldCell setUserInteractionEnabled: cellData.enabled];
            
            [textfieldCell setLeftTitle: cellData.title];
            [textfieldCell setPlaceholder: cellData.placeholder];
//            [textfieldCell hideTopLabel: YES];
            if (cellData.hasDelegate)
                [textfieldCell  setTextfieldDelegate: self];
            
            if (cellData.value)
                [textfieldCell setTextfieldText: cellData.value];
            
            [self.cellReferenceDict setObject: textfieldCell forKey: cellData.key];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_BIRTHDATE:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormDropdownCell owner:self options:nil];
            FormDropdownCell *dropdownCell = [nib objectAtIndex:0];

            //see if Enabled key is available, if not see if pending or approved
            [dropdownCell setUserInteractionEnabled: cellData.enabled];
            
            //Salutation
            [dropdownCell setTitle: cellData.title];
            [dropdownCell setSelectionTitle: cellData.value textColor:UIColor.lightGrayColor];
            
            [dropdownCell hideTopLabel: NO];
            [dropdownCell setButtonTag: indexPath.row];
            
            [dropdownCell setDropdownKey: cellData.key];
            [dropdownCell setBirthdateDelegate: self];
            [self.cellReferenceDict setObject: dropdownCell forKey: cellData.key];
            
            if (kIsRightToLeft) {
                [dropdownCell setButtonContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
            } else {
                [dropdownCell setButtonContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            }
            
            return dropdownCell;
        }
            break;
        case kREGISTRATION_TEXTVIEW:
        {
//            FormTextViewTableViewCell *textviewCell = [tableView dequeueReusableCellWithIdentifier: kFormTextViewTableViewCell forIndexPath: indexPath];
            
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormTextViewTableViewCell owner:self options:nil];
            FormTextViewTableViewCell *textviewCell = [nib objectAtIndex:0];

            if (cellData.keyboard)
                [textviewCell setKeyboard: cellData.keyboard];
            if (cellData.secureEntry)
                [textviewCell setSecureTextEntry: cellData.secureEntry];
            
            //see if Enabled key is available, if not see if pending or approved
            [textviewCell setUserInteractionEnabled: cellData.enabled];
            
            [textviewCell setTitle: cellData.title];
            [textviewCell hideTopLabel: NO];
//            [textviewCell setTextviewDelegate: self];
            
            if (cellData.value)
                [textviewCell setTextviewText: cellData.value];
            
            if (cellData.placeholder)
                [textviewCell setPlaceholder: cellData.placeholder];
            
            [self.cellReferenceDict setObject: textviewCell forKey: cellData.key];
            return textviewCell;
        }
            break;
        case kREGISTRATION_WORKINGHOURS:
        {
//            FormWorkingHoursTableViewCell *workingHoursCell = [tableView dequeueReusableCellWithIdentifier: kFormWorkingHoursTableViewCell forIndexPath: indexPath];
            
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormWorkingHoursTableViewCell owner:self options:nil];
            FormWorkingHoursTableViewCell *workingHoursCell = [nib objectAtIndex:0];

//            if (!self.isApproved) //allow working hours to be modified if pending
            if (cellData.enabled)
                [workingHoursCell.button addTarget: self action: @selector(workingHoursPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            if (!workingHoursCell.workingHoursDict)
                workingHoursCell.workingHoursDict = cellData.workingHoursDict;
            
            [workingHoursCell reloadCellData];
            [self.cellReferenceDict setObject: workingHoursCell forKey: cellData.key];
            return workingHoursCell;
        }
            break;
        case kREGISTRATION_SIGNATURE:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kSignatureTableViewCell owner:self options:nil];
            SignatureTableViewCell *signatureCell = [nib objectAtIndex:0];
            
            if ([cellData.value length] > 0)
            {
                NSData *data = [[NSData alloc]  initWithBase64EncodedString: cellData.value options: NSDataBase64DecodingIgnoreUnknownCharacters];
                signatureCell.signatureImageView.image = [[UIImage alloc] initWithData: data];
            }
            
            signatureCell.delegate = self;
            [self.cellReferenceDict setObject: signatureCell forKey: cellData.key];
            
            
            return signatureCell;
        }
            break;
        case kREGISTRATION_CONTACTPREF:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kContactPrefTableViewCell owner:self options:nil];
            ContactPrefTableViewCell *contactPrefCell = [nib objectAtIndex:0];
            contactPrefCell.contactPrefString = cellData.value;
            
            [contactPrefCell setOptional:cellData.optional];
            
            [self.cellReferenceDict setObject: contactPrefCell forKey: cellData.key];
            
            if (cellData.backgroundColour)
                contactPrefCell.backgroundColor = cellData.backgroundColour;
            else
                contactPrefCell.backgroundColor = [UIColor clearColor];
            
            UIView *backView = [[UIView alloc]initWithFrame:CGRectZero];
            backView.backgroundColor = [UIColor clearColor];
            contactPrefCell.backgroundView = backView;
            
            return contactPrefCell;
        }
            break;
        case kFORM_MULTICHECKBOX:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormMultiCheckboxCell owner:self options:nil];
            FormMultiCheckboxCell *multiCheckboxCell = [nib objectAtIndex:0];
            
            multiCheckboxCell.lblTitle.text = cellData.title;
            
            multiCheckboxCell.checkboxValueString = cellData.value;
            
            multiCheckboxCell.checkboxLabelArray = cellData.checkboxLabelsArray;
            multiCheckboxCell.linkedCellKeysArray = cellData.linkedCellKeysArray;
            
            [self.cellReferenceDict setObject: multiCheckboxCell forKey: cellData.key];
            
            if (cellData.hasDelegate)
                [multiCheckboxCell setDelegate: self];
            
            return multiCheckboxCell;
        }
            break;
            
        case kFORM_PANCARD_ID_NEW:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormPanCardIDCell_New owner:self options:nil];
            FormPanCardIDCell_New *cell = [nib objectAtIndex:0];
            [cell hideRow: YES];
            
            if (cellData.optional)
            {
                [cell setTitle: cellData.title];
            }
            else
            {
                [cell setRequiredTitle: [self addAsteriskForRequiredField: cellData.title enabled: cellData.enabled] placeholder: cellData.title];
            }
            
            if (cellData.value && ![cellData.value isKindOfClass: [NSNull class]]) {
                 [cell setPancardValuesTo: cellData.value];
                 [cell hideRow:NO];
            }
               
            
            [cell setTitle: cellData.title];

//            [cell setTextfieldText: @"" textfieldIndex: 0];
//            [cell setTextfieldText: @"" textfieldIndex: 1];
//            [cell setTextfieldText: @"" textfieldIndex: 2];
            
            [cell setPlaceholder: @"ABCDE" textfieldIndex: 0];
            [cell setKeyboard:UIKeyboardTypeAlphabet textfieldIndex:0];
            [cell setPlaceholder: @"1234" textfieldIndex: 1];
            [cell setKeyboard:UIKeyboardTypeNumberPad textfieldIndex:1];
            [cell setPlaceholder: @"A" textfieldIndex: 2];
            [cell setKeyboard:UIKeyboardTypeAlphabet textfieldIndex:2];
            
//            [cell setupCellWithDatasource: self];
//
//            if (cellData.value && ![cellData.value isKindOfClass: [NSNull class]])
//                [cell setIndividualFields: cellData.value];
            
            [self.cellReferenceDict setObject:cell forKey:cellData.key];
            return cell;
        }
        break;
            
        case kFORM_PANCARD_ID_NEWSTATUS:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormPanCardIDCell_NewStatus owner:self options:nil];
            FormPanCardIDCell_NewStatus *cell = [nib objectAtIndex:0];
            [cell hideRow: YES];
                        
            if (cellData.optional)
            {
                    [cell setTitle: cellData.title];
            }
            else
            {
                [cell setRequiredTitle: [self addAsteriskForRequiredField: cellData.title enabled: cellData.enabled] placeholder: cellData.title];
            }
                        
            if (cellData.value && ![cellData.value isKindOfClass: [NSNull class]]) {
                [cell setPancardValuesTo: cellData.value];
                [cell hideRow:NO];
            }
                           
                        
            [cell setTitle: cellData.title];
            [cell setStatusTitle:cellData.statusVerify];

                        
            [cell setPlaceholder: @"ABCDE" textfieldIndex: 0];
            [cell setKeyboard:UIKeyboardTypeAlphabet textfieldIndex:0];
            [cell setPlaceholder: @"1234" textfieldIndex: 1];
            [cell setKeyboard:UIKeyboardTypeNumberPad textfieldIndex:1];
            [cell setPlaceholder: @"A" textfieldIndex: 2];
            [cell setKeyboard:UIKeyboardTypeAlphabet textfieldIndex:2];
                        
            //            [cell setupCellWithDatasource: self];
            //
            //            if (cellData.value && ![cellData.value isKindOfClass: [NSNull class]])
            //                [cell setIndividualFields: cellData.value];
                        
            [self.cellReferenceDict setObject:cell forKey:cellData.key];
            return cell;
        }
        break;
            
        case kFORM_DRIVERLICENSE:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormDriverLicenseCell owner:self options:nil];
            FormDriverLicenseCell *cell = [nib objectAtIndex:0];
            [cell hideRow: YES];
            
            if (cellData.optional)
            {
                [cell setTitle: cellData.title];
            }
            else
            {
                [cell setRequiredTitle: [self addAsteriskForRequiredField: cellData.title enabled: cellData.enabled] placeholder: cellData.title];
            }
            
            [cell setTitle: cellData.title];
            
            //            [cell setTextfieldText: @"" textfieldIndex: 0];
            //            [cell setTextfieldText: @"" textfieldIndex: 1];
            //            [cell setTextfieldText: @"" textfieldIndex: 2];
            if (cellData.value && ![cellData.value isKindOfClass: [NSNull class]]) {
                [cell setPancardValuesTo: cellData.value];
                [cell hideRow:NO];
            }
            
            [cell setPlaceholder: @"MH" textfieldIndex: 0];
            [cell setKeyboard:UIKeyboardTypeAlphabet textfieldIndex:0];
            [cell setPlaceholder: @"14" textfieldIndex: 1];
            [cell setKeyboard:UIKeyboardTypeNumberPad textfieldIndex:1];
            [cell setPlaceholder: @"0123456789123" textfieldIndex: 2];
            [cell setKeyboard:UIKeyboardTypeNumberPad textfieldIndex:2];
            
            //            [cell setupCellWithDatasource: self];
            //
            //            if (cellData.value && ![cellData.value isKindOfClass: [NSNull class]])
            //                [cell setIndividualFields: cellData.value];
            
            [self.cellReferenceDict setObject:cell forKey:cellData.key];
            return cell;
        }
            break;
        case kFORM_VOTERID:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormVoterIDCell owner:self options:nil];
            FormVoterIDCell *cell = [nib objectAtIndex:0];
            [cell hideRow: YES];
            
            if (cellData.optional)
            {
                [cell setTitle: cellData.title];
            }
            else
            {
                [cell setRequiredTitle: [self addAsteriskForRequiredField: cellData.title enabled: cellData.enabled] placeholder: cellData.title];
            }
            
            [cell setTitle: cellData.title];
            
            //            [cell setTextfieldText: @"" textfieldIndex: 0];
            //            [cell setTextfieldText: @"" textfieldIndex: 1];
            if (cellData.value && ![cellData.value isKindOfClass: [NSNull class]]) {
                [cell setPancardValuesTo: cellData.value];
                [cell hideRow:NO];
            }
            
            [cell setPlaceholder: @"ABC" textfieldIndex: 0];
            [cell setKeyboard:UIKeyboardTypeAlphabet textfieldIndex:0];
            [cell setPlaceholder: @"1234567" textfieldIndex: 1];
            [cell setKeyboard:UIKeyboardTypeNumberPad textfieldIndex:1];
            
            //            [cell setupCellWithDatasource: self];
            //
            //            if (cellData.value && ![cellData.value isKindOfClass: [NSNull class]])
            //                [cell setIndividualFields: cellData.value];
            
            [self.cellReferenceDict setObject:cell forKey:cellData.key];
            return cell;
        }
            break;
            
//        case kREGISTRATION_HEADERWITHREQUIRED:
//            
//            break;
        case kREGISTRATION_BUTTONCELL:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormCTAButtonCell owner:self options:nil];
            FormCTAButtonCell *buttonCell = [nib objectAtIndex:0];
                
            //see if Enabled key is available, if not see if pending or approved
            [buttonCell setUserInteractionEnabled: cellData.enabled];
            
            //Salutation
            [buttonCell setButtonTitle: cellData.title];
            [buttonCell setColourWithTextColour: cellData.textColour backgroundColour: cellData.backgroundColour];
            [buttonCell setCTADelegate: self];
            [buttonCell setCtaKey: cellData.key]; //for target
            
            [self.cellReferenceDict setObject: buttonCell forKey: cellData.key];
            
            buttonCell.backgroundColor = [UIColor clearColor];
            UIView *backView = [[UIView alloc]initWithFrame:CGRectZero];
            backView.backgroundColor = [UIColor clearColor];
            buttonCell.backgroundView = backView;
            
            return buttonCell;
        }
            break;
        case kFORM_PROFILE_TOPSECTION:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"TopSectionCell"];
            
            
            UILabel *lblFullName = [cell viewWithTag: 10];
            lblFullName.text = [[mSession profileInfo] fullName];
            lblFullName.font = FONT_H(25);
            
            UILabel *lblDSRCode = [cell viewWithTag: 11];
            lblDSRCode.text = [[mSession profileInfo] shareCode];
            lblDSRCode.font = FONT_B1;
            
            UILabel *lblCompleteYourProfile = [cell viewWithTag:699];
            lblCompleteYourProfile.text = LOCALIZATION(C_COMPLETE_YOUR_PROFILE);    //lokalise 30 jan
            lblCompleteYourProfile.font = FONT_H1;
            
            UIView *yellowBar = [cell viewWithTag: 999];
            
            CGFloat sliderFillAmt = [[mSession profileInfo] profileCompleteness];
            CGFloat widthConstant = (SCREEN_WIDTH - 60) * (sliderFillAmt / 100);
            
            

            for (NSLayoutConstraint *constraint in yellowBar.constraints)
            {
                if ([constraint.identifier isEqualToString:@"yellowBarConstraint"])
                {
                    constraint.constant = widthConstant;
                    break;
                }
            }

            UIImageView *progressBubble = [cell viewWithTag: 9998];
            UIImage *bubbleImage = GET_ISADVANCE ? [UIImage imageNamed:@"icon-bubble_blue"] : [UIImage imageNamed:@"icon-bubble"];
            [progressBubble setImage: bubbleImage];
            UILabel *lblProgress = [cell viewWithTag: 9999];

            lblProgress.text = [NSString stringWithFormat: @"%3.0f%%", sliderFillAmt];
            return cell;
        }
            break;
        case kFORM_PANCARD_ID:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormPANCardIDCell owner:self options:nil];
            FormPANCardIDCell *cell = [nib objectAtIndex:0];
            
            //            if (cellData.optional)
            [cell setTitle: cellData.title];
            //            else
            //                [cell setAttributedTitle: [self addAsteriskForRequiredField: cellData.title enabled: cellData.enabled]];
            
//            [cell setupCellWithDatasource: self];
            
            if (cellData.value && ![cellData.value isKindOfClass: [NSNull class]])
                [cell setIndividualFields: cellData.value];
            
            [self.cellReferenceDict setObject:cell forKey:cellData.key];
            return cell;
        }
            break;
        case kFORM_PROFILE_IMAGE:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormImageCell  owner:self options:nil];
            FormImageCell *imageCell = [nib objectAtIndex:0];
            
            if (cellData.value && ![cellData.value isKindOfClass: [NSNull class]])
            {
                [imageCell setImage: [UIImage imageNamed: cellData.value]];
            }
            
            return imageCell;
        }
            break;
        case kFORM_PANCARD_IMAGE:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormPANCardImageCell owner:self options:nil];
            FormPANCardImageCell *imageCell = [nib objectAtIndex:0];
            imageCell.delegate = self;
            imageCell.userInteractionEnabled = cellData.enabled;
            
            imageCell.keyString = cellData.key;
            [imageCell hideRow: YES];
            
            if (cellData.optional)
                [imageCell setTitle: cellData.title];
            else
                [imageCell setAttributedTitle: [self addAsteriskForRequiredField: cellData.title enabled: cellData.enabled]];
            
            if (cellData.value && ![cellData.value isKindOfClass: [NSNull class]])
            {
                if ([cellData.value length] > 0)
                {
                    NSData *data = [[NSData alloc]  initWithBase64EncodedString: cellData.value options: NSDataBase64DecodingIgnoreUnknownCharacters];
                    [imageCell setPhoto: [[UIImage alloc] initWithData: data]];
                    [imageCell hideRow:NO];
                } else {
                    [imageCell hideRow:NO];
                }
            }
            
            [self.cellReferenceDict setObject: imageCell forKey: cellData.key];
            
            return imageCell;
        }
            break;
        case kREGISTRATION_TNCCHECKBOX:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormCheckboxCell owner:self options:nil];
            FormCheckboxCell *cell = [nib objectAtIndex:0];
            
            if (cellData.optional)
                [cell setTitle: cellData.title];
            else
                [cell setAttributedTitle: [self addAsteriskForRequiredField: cellData.title enabled: cellData.enabled]];
            
            [cell setCheckboxSelected: cellData.checkBoxSelected];
            [cell setUserInteractionEnabled:cellData.enabled];
            
            cell.delegate = self;
            
            if (cellData.hyperlinks)
            {
                for (NSString *key in cellData.hyperlinks)
                {
                    NSString *hyperlinkKey = [cellData.hyperlinks objectForKey: key];
                    [cell setHyperlinkWithBodyText: key hyperlinkKey: hyperlinkKey];
                }
            }
            
            [self.cellReferenceDict setObject: cell forKey: cellData.key];
            
            cell.backgroundColor = [UIColor clearColor];
            UIView *backView = [[UIView alloc]initWithFrame:CGRectZero];
            backView.backgroundColor = [UIColor clearColor];
            cell.backgroundView = backView;
            return cell;
        }
            break;
        case kFORM_LINKEDWORKSHOP:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormLinkedWorkshopViewCell owner:self options:nil];
            FormLinkedWorkshopViewCell *cell = [nib objectAtIndex:0];
            
            [cell setTitle: cellData.title];
            [cell setUserInteractionEnabled:cellData.enabled];
            [cell setLinkedWorkshops:[NSMutableArray arrayWithArray:cellData.linkedWorkshopDict]];
            [self.cellReferenceDict setObject: cell forKey: cellData.key];
            
            return cell;
        }
            break;
        case kFORM_LABELONLY:
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormOnlyLabelTableViewCell owner:self options:nil];
            FormOnlyLabelTableViewCell *cell = [nib objectAtIndex:0];
            cell.userInteractionEnabled = YES;
            if(cellData.hyperlinks) {
                for (NSString *key in cellData.hyperlinks)
                {
                    NSString *hyperlinkKey = [cellData.hyperlinks objectForKey: key];
                    [cell setLabelText: cellData.title];
                    [cell setHyperlinkWithBodyText: key hyperlinkKey: hyperlinkKey];
                    cell.delegate = self;
                }
            } else {
                [cell setLabelText: cellData.title];
            }
            [self.cellReferenceDict setObject: cell forKey: cellData.key];
            
            return cell;
        }
            break;
        case kFORM_STATUS: {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed: kFormStatusTableViewCell owner:self options:nil];
            FormStatusTableViewCell *cell = [nib objectAtIndex:0];
            [cell setCellStatus:[cellData.value intValue]];
            
            return cell;
        }
            break;
        default:
            NSLog(@"UNKNOWN CELL TYPE");
            return [UITableViewCell new];
            break;
    }
    
    return [UITableViewCell new];
}

- (void) workingHoursPressed:(id)sender
{
    [self.delegate didPressWorkingHours];
}

-(void)textfieldCellDidBeginEditingWithKey:(NSString *)key
{
    if ([self.delegate respondsToSelector: @selector(textfieldCellDidBeginEditingWithKey:)])
        [self.delegate textfieldCellDidBeginEditingWithKey: key];
}

-(void)textfieldCellDidEndEditingWithKey:(NSString *)key
{
    if ([self.delegate respondsToSelector: @selector(textfieldCellDidEndEditingWithKey:)])
        [self.delegate textfieldCellDidEndEditingWithKey: key];
}

-(BOOL) textFieldCell:(NSString *) key shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([self.delegate respondsToSelector:@selector(textFieldCell:shouldChangeCharactersInRange:replacementString:)])
           return [self.delegate textFieldCell:key shouldChangeCharactersInRange:range replacementString:string];
       else
           return YES;
}

-(void) updateWorkingHours:(NSDictionary *) workingHoursDict
{
    FormWorkingHoursTableViewCell *workingHoursCell = [self.cellReferenceDict objectForKey: kWorkingHours];
    workingHoursCell.workingHoursDict = workingHoursDict;
    [workingHoursCell reloadCellData];
    
    
    //possibly need to reload whole table instead
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark Dropdown Cell Delegates

-(void)cellPressedWithKey:(NSString *)key
{
    //find the key and pass to delegate to handle popup
    [self.delegate dropdownPressedWithKey: key];
}

-(void)birthdatePressedWithKey:(NSString *)key
{
    //find the key and pass to delegate to handle birthdate popup
    [self.delegate birthdatePressedWithKey: key];
}

#pragma mark CTA Cell Delegates

-(void) ctaPressedWithKey:(NSString *) ctaKey;
{
    //find the key and pass to delegate to handle CTA
    [self.delegate ctaPressedWithKey: ctaKey];
}

#pragma mark PAN Card image cell
-(void) showPhoto:(UIImage *) image
{
    [self.delegate showPhoto: image];
}

-(void) takePhoto: (NSString *) cellKey
{
    [self.delegate takePhoto: cellKey];
}

#pragma mark PAN Card ID Cell Delegate and Datasource

- (void)inputRecieved:(NSString *)input :(BOOL)isAccepted :(NSString *)finalString
{
    
}
- (int)numberOfInputForCell:(FormPANCardIDCell *)cell
{
    return 10;
}

- (enum FormIDAcceptedInput)inputAcceptedForFieldAtIndex:(int)index
{
    if(index < 5 || index > 8)
    {
        return FormIDAlphabet;
    }
    else
    {
        return FormIDNumbers;
    }
}
- (NSString *)placeHolderForFieldAtIndex:(int)index
{
    switch (index) {
        case 0:
            return @"A";
            break;
        case 1:
            return @"B";
            break;
        case 2:
            return @"C";
            break;
        case 3:
            return @"D";
            break;
        case 4:
            return @"E";
            break;
        case 5:
            return @"1";
            break;
        case 6:
            return @"2";
            break;
        case 7:
            return @"3";
            break;
        case 8:
            return @"4";
            break;
        case 9:
            return @"A";
            break;
            
        default:
            return @"-";
            break;
    };
}

#pragma mark Textfield with Button Cell Delegates
-(void)hyperlinkPressed:(NSString *)link
{
    [self.delegate hyperlinkPressed: link];
}

#pragma mark Textfield with Button Cell Delegates
-(void)buttonPressedWithKey:(NSString *)key
{
    [self.delegate textfieldWithButtonPressedWithKey: key];
}

#pragma mark Signature delegate
-(void)signaturePressed:(NSString *)signatureKey
{
    [self.delegate signaturePressed: signatureKey];
}

#pragma mark HyperlinkLabel Delegate
-(void) attributedLabeldidSelectLinkWithURL:(NSURL *)url {
    [self.delegate attributedLabeldidSelectLinkWithURL:url];
}
//-(void) dropdownPressed:(UIButton *)sender
//{
//    //find the key and pass to delegate
//
//    /* UIButton's superview = Cell Content View
//     Cell ContentView's superview = Cell */
//    NSArray *cellArray = [self.cellReferenceDict allKeysForObject: sender.superview.superview];
//    if ([cellArray count] > 0)
//    {
//        NSString *firstKey = [cellArray firstObject];
//        [self.delegate dropdownPressedWithKey: firstKey];
//    }
//
//    //once delegate is done, pass the selected data and key back to process the table
////    [self.delegate dropdownPressedWithKey: @""];
//}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *sectionArray = [self.initializingArray objectAtIndex: indexPath.section];
    FormCellData *cellData = [[FormCellData alloc] initWithData: [sectionArray objectAtIndex: indexPath.row]];
    
    switch (cellData.type)
    {
        case kFORM_PROFILE_TOPSECTION:
            return 200;
            
        case kFORM_PANCARD_ID:
            return 80;
            
        default:
        {
            if ([cellData.key isEqualToString: kPANCardId] || [cellData.key isEqualToString: kPANCardImage])
            {
                FormPanCardIDCell_New *cell = [self.cellReferenceDict objectForKey: kPANCardId];
                if (cell.isHidden)
                    return 0;
            }
            else if ([cellData.key isEqualToString: kDrivingLicense] || [cellData.key isEqualToString: kDrivingLicenseImage])
            {
                FormPanCardIDCell_New *cell = [self.cellReferenceDict objectForKey: kDrivingLicense];
                if (cell.isHidden)
                    return 0;
            }
            else if ([cellData.key isEqualToString: kVoterID] || [cellData.key isEqualToString: kVoterIDImage])
            {
                FormPanCardIDCell_New *cell = [self.cellReferenceDict objectForKey: kVoterID];
                if (cell.isHidden)
                    return 0;
            }
            if (cellData.hidden)
            {
                return 0;
            }
            
            //default case
            return UITableViewAutomaticDimension;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if ([self.headersArray objectAtIndex: section])
    {
        NSString *title = [self.headersArray objectAtIndex: section];
        if (title.length == 0)
            return 0;
    }
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([self.headersArray objectAtIndex: section])
    {
        NSString *title = [self.headersArray objectAtIndex: section];
        if (title.length == 0)
            return 0;
    }
    return UITableViewAutomaticDimension;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    FormHeaderTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:kFormHeaderTableViewCell];
    
    if ([self.headersArray objectAtIndex: section])
    {
        NSString *title = [self.headersArray objectAtIndex: section];
        if ([title length] > 0)
        {
            [headerCell setTitle: title];
            if([title hasPrefix:LOCALIZATION(C_FORM_LEADSOURCE)])           //lokalise 11 Feb
            {
                NSArray* components = [title componentsSeparatedByString:@"+"];
                [headerCell setTitle: components[0]];
                [headerCell setLabel: (components.count > 1) ? components[1] : @""];
            }
        }
        else
            return [UIView new];
    }
    else
        return [UIView new];
//    switch (section)
//    {
//        case 0:
//            [headerCell setTitle: LOCALIZATION(C_HEADER_WORKSHOPPROFILE)];
//            break;
//        case 1:
//            [headerCell setTitle: LOCALIZATION(C_HEADER_OWNERPROFILE)];
//            break;
//        default:
//            return [UIView new];
//            break;
//    }
    
    //add title
    return headerCell.contentView;
}

-(void) editCell: (NSString *) formKey toSecureField:(BOOL) secured
{
    FormTextfieldCell *textfieldCell = [self.cellReferenceDict objectForKey: formKey];
    [textfieldCell setSecureTextEntry: secured];
}

-(void) editCell: (NSString *) formKey withTextfieldString: (NSString *) text
{
    FormTextfieldCell *textfieldCell = [self.cellReferenceDict objectForKey: formKey];
    [textfieldCell setTextfieldText: text];
}

-(void) editCell: (NSString *) formKey withDropdownSelection: (NSString *) selectionString
{
    FormDropdownCell *dropdownCell = [self.cellReferenceDict objectForKey: formKey];
    [dropdownCell setSelectionTitle: selectionString];
}

-(void) editCell: (NSString *) formKey withTextfieldButtonText: (NSString *) textfieldString
{
    FormTextfieldWithButtonCell *textfieldCell = [self.cellReferenceDict objectForKey: formKey];
    [textfieldCell setTextfieldText: textfieldString];
}

-(void) editCell: (NSString *) formKey withImage:(nonnull UIImage *)image
{
    FormPANCardImageCell *imageCell = [self.cellReferenceDict objectForKey: formKey];
    [imageCell setPhoto: image];
}

-(void) editCell: (NSString *) formKey withSignatureImage:(nonnull UIImage *)image
{
    SignatureTableViewCell *signatureCell = [self.cellReferenceDict objectForKey: formKey];
    signatureCell.signatureImageView.image = image;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.initializingArray count];
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *sectionArray = [self.initializingArray objectAtIndex: section];
    
    return [sectionArray count];
}

    
-(NSAttributedString *) addAsteriskForRequiredField: (NSString *) string enabled:(BOOL) enabled
{
    NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @"* " attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
    
    NSMutableAttributedString *titleAttrString;
    if (enabled)
         titleAttrString = [[NSMutableAttributedString alloc] initWithString: string attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
    else
        titleAttrString = [[NSMutableAttributedString alloc] initWithString: string attributes: @{NSForegroundColorAttributeName: COLOUR_LIGHTGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
    
    [titleAttrString appendAttributedString: asteriskAttrString];
    
    return titleAttrString;
}

-(NSMutableDictionary *) getJsonValues
{
    NSMutableDictionary *jsonValuesDict = [[NSMutableDictionary alloc] init];
    if (IS_COUNTRY(COUNTRYCODE_RUSSIA)){
        [jsonValuesDict setObject:@"" forKey:@"OilChangePerMonth"];
        [jsonValuesDict setObject:@"" forKey:@"OilChangePercentage"];
        [jsonValuesDict setObject:@"" forKey:@"HalfSyntheticOilChanges"];
        [jsonValuesDict setObject:@"" forKey:@"ShellHelixQty"];
    }
    
    
    for (NSString *key in self.cellReferenceDict)
    {
        FormBaseCell *cell = [self.cellReferenceDict objectForKey: key];
        
        if ([cell isKindOfClass: [NSNull class]])
            break;
        if ([cell respondsToSelector: @selector(getJsonValue)])
        {
            if ([cell getJsonValue])
                [jsonValuesDict setObject: [cell getJsonValue] forKey: key];
        }
    }
    return jsonValuesDict;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    FormBaseCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    
//    if ([cell respondsToSelector: @selector(getJsonValue)])
//        NSLog(@"%@", [cell getJsonValue]);
//}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSArray *sectionArray = [self.initializingArray objectAtIndex: indexPath.section];
//    FormCellData *cellData = [[FormCellData alloc] initWithData: [sectionArray objectAtIndex: indexPath.row]];
//
//    switch (cellData.type)
//    {
//        case kFORM_PANCARD_ID:
//        {
//            FormPANCardIDCell* panCell = (FormPANCardIDCell*)cell;
//            [panCell setupCellWithDatasource:self];
//        }
//
//        default:
//            return;
//    }
}
- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *sectionArray = [self.initializingArray objectAtIndex: indexPath.section];
    if(indexPath.row < sectionArray.count) {
        FormCellData *cellData = [[FormCellData alloc] initWithData: [sectionArray objectAtIndex: indexPath.row]];
        
        switch (cellData.type)
        {
            case kFORM_PANCARD_ID:
            {
                FormPANCardIDCell* panCell = (FormPANCardIDCell*)cell;
                cellData.value = [panCell getJsonValue];
            }
                
            default:
                return;
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([self.delegate respondsToSelector: @selector(tableViewDidScroll:)])
    {
        [self.delegate tableViewDidScroll:scrollView];
    }
}

-(void)contactPrefCellDidSelectCell:(NSInteger) index isSelected:(BOOL) isSelected sender:(id)sender
{
    if ([self.delegate respondsToSelector: @selector(contactPrefCellDidSelectCell:isSelected:)])
        [self.delegate contactPrefCellDidSelectCell: index isSelected: isSelected];
    
    NSString *cellKey = [[self.cellReferenceDict allKeysForObject: sender] firstObject];
    UITableView *tableView = (UITableView *) [[self.cellReferenceDict objectForKey: cellKey] superview];
    NSIndexPath *indexPath = [tableView indexPathForCell: [self.cellReferenceDict objectForKey: cellKey]];
    
    if ([cellKey isEqualToString: kValidateCheckboxes])
    {
        FormMultiCheckboxCell *cell = [self.cellReferenceDict objectForKey: cellKey];
        NSString *selectedLinkedCellKey = [cell.linkedCellKeysArray objectAtIndex: index];
        
        if ([selectedLinkedCellKey isEqualToString: kPANCardId])
        {
            FormPanCardIDCell_New *panCardCell = [self.cellReferenceDict objectForKey: kPANCardId];
            [panCardCell hideRow: !panCardCell.isHidden];
            
            FormPANCardImageCell *imageCell = [self.cellReferenceDict objectForKey: kPANCardImage];
            [imageCell hideRow: panCardCell.isHidden];  //textfield cell hidden changed in hideRow function
            
        }
        else if ([selectedLinkedCellKey isEqualToString: kDrivingLicense])
        {
            FormPanCardIDCell_New *panCardCell = [self.cellReferenceDict objectForKey: kDrivingLicense];
            [panCardCell hideRow: !panCardCell.isHidden];
            
            FormPANCardImageCell *imageCell = [self.cellReferenceDict objectForKey: kDrivingLicenseImage];
            [imageCell hideRow: panCardCell.isHidden];  //textfield cell hidden changed in hideRow function
            
        }
        else if ([selectedLinkedCellKey isEqualToString: kVoterID])
        {
            FormPanCardIDCell_New *panCardCell = [self.cellReferenceDict objectForKey: kVoterID];
            [panCardCell hideRow: !panCardCell.isHidden];
            
            FormPANCardImageCell *imageCell = [self.cellReferenceDict objectForKey: kVoterIDImage];
            [imageCell hideRow: panCardCell.isHidden];  //textfield cell hidden changed in hideRow function
            
        }
        
        cell.bottomLine.hidden = [[self.cellReferenceDict objectForKey: kPANCardId] isHidden] && [[self.cellReferenceDict objectForKey: kDrivingLicense] isHidden] && [[self.cellReferenceDict objectForKey: kVoterID] isHidden];
        
        if (isSelected)
        {
            //show cell
            
//            -(void) editCell: (NSString *) formKey withTextfieldButtonText: (NSString *) textfieldString
//            {
//                FormTextfieldWithButtonCell *textfieldCell = [self.cellReferenceDict objectForKey: formKey];
//                [textfieldCell setTextfieldText: textfieldString];
//            }
        }
        else
        {
            //hide cell
        }
        
        [tableView reloadData];
//        [tableView reloadRowsAtIndexPaths: @[indexPath] withRowAnimation: UITableViewRowAnimationNone];
    }
}

@end
