//
//  FormOnlyLabelTableViewCell.m
//  Shell
//
//  Created by Nach on 30/12/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "FormOnlyLabelTableViewCell.h"
#import "TTTAttributedLabel.h"

@interface FormOnlyLabelTableViewCell()<TTTAttributedLabelDelegate>

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblText;

@end

@implementation FormOnlyLabelTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.userInteractionEnabled = YES;
    
    self.lblText.text = @"";
    self.lblText.textColor = COLOUR_VERYDARKGREY;
    self.lblText.font = FONT_B1;
    self.lblText.delegate = self;
    
    self.userInteractionEnabled = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setLabelText:(NSString *) text {
    self.lblText.text = text;
}

-(void) setHyperlinkWithBodyText:(NSString *)bodyText hyperlinkKey:(NSString *)hyperlinkKey
{
   [Helper addHyperlink:self.lblText hyperlinkText:bodyText urlString:hyperlinkKey bodyText:self.lblText.text];
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    //send hyperlink key to delegate to process
    [self.delegate attributedLabeldidSelectLinkWithURL: url];
}
@end
