//
//  FormLinkedWorkshopViewCell.m
//  Shell
//
//  Created by Nach on 20/9/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "FormLinkedWorkshopViewCell.h"
#import "LinkedWorkshopDetailsTableViewCell.h"

@interface FormLinkedWorkshopViewCell() <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;

@end

@implementation FormLinkedWorkshopViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.topLabel setFont: FONT_B1];
    self.topLabel.textColor = COLOUR_VERYDARKGREY;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"LinkedWorkshopDetailsTableViewCell" bundle:nil] forCellReuseIdentifier:@"LinkedWorkshopDetailsTableViewCell"];
    
    self.tableViewHeight.constant = [self.displayArray count] * 38.0;
    [self.tableView reloadData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setUserInteractionEnabled:(BOOL)userInteractionEnabled {
    [super setUserInteractionEnabled:userInteractionEnabled];
}

-(void) setTitle: (NSString *) title {
    self.topLabel.text = title;
}

-(void) setLinkedWorkshops:(NSMutableArray *)linkedArray {
    self.displayArray = linkedArray;
    self.tableViewHeight.constant = [self.displayArray count] * 35.0;
    [self.tableView reloadData];
}

-(NSString *) getJsonValue {
    return [self.displayArray componentsJoinedByString:@","];
}

- (void) deleteDataAt:(NSInteger)row {
    [self.displayArray removeObjectAtIndex:row];
    
    self.tableViewHeight.constant = [self.displayArray count] * 35.0;
    [self.tableView reloadData];
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.displayArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LinkedWorkshopDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"LinkedWorkshopDetailsTableViewCell"];
    cell.delegate = self;
    [cell setText:self.displayArray[indexPath.row]];
    [cell setTag:indexPath.row];
    
    if(indexPath.row == 0 && self.userInteractionEnabled) {
        [cell setNoDelete];
    } else if(!self.userInteractionEnabled){
        [cell setNoDelete];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 35.0;
}

@end
