//
//  FormWorkingHoursLabelTableViewCell.m
//  Shell
//
//  Created by Ankita Chhikara on 30/1/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "FormWorkingHoursLabelTableViewCell.h"
#import "Constants.h"

@interface FormWorkingHoursLabelTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *lblLeft;
@property (weak, nonatomic) IBOutlet UILabel *lblRight;
@end


@implementation FormWorkingHoursLabelTableViewCell
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.lblLeft setFont: FONT_B1];
    [self.lblRight setFont: FONT_B1];
}

-(void)setLeftLabel:(NSString *)labelString
{
    self.lblLeft.text = labelString;
}

- (void)setRightLabel:(NSString *)labelString
{
    self.lblRight.text = labelString;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
