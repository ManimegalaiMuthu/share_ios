//
//  FormMultiCheckboxCell.h
//  Shell
//
//  Created by Jeremy Lua on 29/5/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "FormBaseCell.h"
#import "ContactPrefTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FormMultiCheckboxCellDelegate <NSObject>
-(void)contactPrefCellDidSelectCell:(NSInteger) index isSelected:(BOOL) isSelected sender:(id)sender;
@end

@interface FormMultiCheckboxCell : FormBaseCell
@property (weak, nonatomic) id<FormMultiCheckboxCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *bottomLine;
@property (weak, nonatomic) IBOutlet UICollectionView *multiCheckboxColView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *colViewLayout;

@property NSMutableArray *checkboxCellArray;    // to hold cell references

@property NSString *checkboxValueString;    //existing value to preselect checkboxes
@property NSArray *checkboxLabelArray;    // to hold checkbox titles/labels
@property NSArray *linkedCellKeysArray;    // to hold key strings to link to other cells.

@end

NS_ASSUME_NONNULL_END
