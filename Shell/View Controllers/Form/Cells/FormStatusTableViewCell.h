//
//  FormStatusTableViewCell.h
//  Shell
//
//  Created by Nach on 3/1/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "FormBaseCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface FormStatusTableViewCell : FormBaseCell

@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UIImageView *statusImage;
@property (weak, nonatomic) IBOutlet UILabel *lblStatusDescription;

-(void) setCellStatus:(NSInteger)status;

@end

NS_ASSUME_NONNULL_END
