//
//  FormImageCell.m
//  Shell
//
//  Created by Jeremy Lua on 27/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "FormImageCell.h"

@interface FormImageCell ()
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@end

@implementation FormImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void) setImage:(UIImage *)image
{
    self.imgView.image = image;
    [self resizeImage];
}

-(void) resizeImage
{
    CGFloat width = self.frame.size.width - 30; //width of cell - 15 left - 15 right
    CGFloat heightFactor = width / self.imgView.image.size.width; //cell width / actual image width
    CGFloat height = self.imgView.image.size.height * heightFactor;
    self.imgView.frame = CGRectMake(15, 0, width, height);
    [self layoutIfNeeded];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
