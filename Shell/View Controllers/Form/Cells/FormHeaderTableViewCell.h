//
//  FormHeaderTableViewCell.h
//  Shell
//
//  Created by Ankita Chhikara on 27/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Constants.h"
#import "FormBaseCell.h"

@interface FormHeaderTableViewCell : FormBaseCell
-(void) setTitle: (NSString *) title;
-(void) setLabel: (NSString *) text;
@end
