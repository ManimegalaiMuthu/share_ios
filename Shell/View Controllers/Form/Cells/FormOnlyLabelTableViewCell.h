//
//  FormOnlyLabelTableViewCell.h
//  Shell
//
//  Created by Nach on 30/12/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "FormBaseCell.h"

NS_ASSUME_NONNULL_BEGIN
@protocol FormOnlyLabelTableViewCellDelegate
-(void)attributedLabeldidSelectLinkWithURL: (NSURL *) url;
@end

@interface FormOnlyLabelTableViewCell : FormBaseCell

@property id<FormOnlyLabelTableViewCellDelegate> delegate;
-(void) setLabelText:(NSString *) text;
-(void) setHyperlinkWithBodyText:(NSString *) bodyText hyperlinkKey:(NSString*) hyperlinkKey;

@end

NS_ASSUME_NONNULL_END
