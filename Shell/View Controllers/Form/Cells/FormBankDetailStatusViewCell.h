//
//  FormBankDetailStatusViewCell.h
//  Shell
//
//  Created by MUTHUPANDIYAN Manimegalai on 10/10/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "LocalizationManager.h"
#import <Foundation/Foundation.h>
#import "FormBaseCell.h"

@protocol FormBankDetailStatusViewCellDelegate <NSObject>

-(void) textfieldCellDidBeginEditingWithKey:(NSString *) key;
-(void) textfieldCellDidEndEditingWithKey:(NSString *) key;
-(BOOL) textFieldCell:(NSString *) key shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

@end

NS_ASSUME_NONNULL_BEGIN

@interface FormBankDetailStatusViewCell : FormBaseCell

@property id<FormBankDetailStatusViewCellDelegate> delegate;

-(void) setCellKey: (NSString *) key;
-(void) setTextfieldText: (NSString *) textfieldString;
-(void) setTitle: (NSString *) title;
-(void) setStatusTitle: (NSString *) statusString;
-(void) setPlaceholder: (NSString *) title;
-(void) hideTopLabel: (BOOL) hidden;
-(void) setSecureTextEntry: (BOOL)isSecureTextEntry;
-(void) setKeyboard:(UIKeyboardType) keyboardType;
-(void) enableCell:(BOOL)userInteractionEnabled;
-(void) setIconImage: (NSString *) imageName;
-(NSString *) getJsonValue;

-(void) setTextfieldDelegate:(id<UITextFieldDelegate>) delegate;
-(void) setRequiredTitle: (NSAttributedString *) title placeholder:(NSString *)placeholderString;

-(void) toggleProfilingQuestion:(BOOL) toEnable;

-(void) setTextFieldTag:(NSInteger) tag;


@end

NS_ASSUME_NONNULL_END
