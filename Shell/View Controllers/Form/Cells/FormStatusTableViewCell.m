//
//  FormStatusTableViewCell.m
//  Shell
//
//  Created by Nach on 3/1/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "FormStatusTableViewCell.h"
#import "LocalizationManager.h"

@implementation FormStatusTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lblStatus.text = LOCALIZATION(C_LOYALTY_FILTER_STATUS);
    self.lblStatus.textColor = COLOUR_VERYDARKGREY;
    self.lblStatus.font = FONT_H1;
    
    self.lblStatusDescription.textColor = COLOUR_VERYDARKGREY;
    self.lblStatusDescription.font = FONT_B1;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setCellStatus:(NSInteger)status {
    switch (status) {
        case 1:
        {
            [self.statusImage setImage: [UIImage imageNamed: @"icn_pending_small_on"]];
            self.lblStatusDescription.text = LOCALIZATION(C_STATUS_PENDINGVERIFICATION);
            
        }
            break;
        case 2:
        {
            [self.statusImage  setImage: [UIImage imageNamed: @"icn_success_small"]];
            self.lblStatusDescription.text = LOCALIZATION(C_STATUS_VERIFIED);
        }
            break;
        case 4:
        {
            [self.statusImage  setImage: [UIImage imageNamed: @"icn_rejected"]];
            self.lblStatusDescription.text = LOCALIZATION(C_STATUS_NOTVERIFIED);
        }
            break;
        default:
            break;
    }
}

@end
