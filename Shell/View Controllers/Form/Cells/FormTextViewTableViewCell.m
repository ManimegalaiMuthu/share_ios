//
//  FormTextViewTableViewCell.m
//  Shell

//
//  Created by Ankita Chhikara on 27/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "FormTextViewTableViewCell.h"

@interface FormTextViewTableViewCell() <UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UITextView *textview;

@property (weak, nonatomic) IBOutlet UILabel *lblPlaceholder;

@end

@implementation FormTextViewTableViewCell
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.topLabel setFont: FONT_B1];
    [self.textview setFont: FONT_B1];
    [self.lblPlaceholder setFont: FONT_B1];
    
    self.textview.delegate = self;
    
    if (kIsRightToLeft) {
        [self.textview setTextAlignment:NSTextAlignmentLeft];
    } else {
        [self.textview setTextAlignment:NSTextAlignmentRight];
    }
}

//-(void) setTextviewDelegate:(id<UITextViewDelegate>) delegate
//{
//    self.textview.delegate = delegate;
//}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    self.lblPlaceholder.hidden = YES;
}

-(void) textViewDidEndEditing:(UITextView *)textView
{
    if(self.textview.text.length > 0)
        self.lblPlaceholder.hidden = YES;
    else
        self.lblPlaceholder.hidden = NO;
}

//-(void)didBeginEditing
//{
//}

//-(void)didEndEditing
//{
//    if(self.textview.text.length > 0)
//        self.lblPlaceholder.hidden = YES;
//    else
//        self.lblPlaceholder.hidden = NO;
//}

-(void) setTextviewText: (NSString *) textviewString
{
    self.textview.text = textviewString;
    if(self.textview.text.length > 0)
        self.lblPlaceholder.hidden = YES;
    else
        self.lblPlaceholder.hidden = NO;
}

-(void) setPlaceholder: (NSString *) title
{
    self.lblPlaceholder.text = title;
}

-(void) setTitle: (NSString *) title
{
    self.lblPlaceholder.text = title;
    self.topLabel.text = title;
}
-(void) setRequiredTitle: (NSAttributedString *) title placeholder:(NSString *)placeholderString
{
    //        self.textfield.placeholder = [NSString stringWithFormat: @"%@ %@",  LOCALIZATION(C_TEXTFIELD_PLEASEENTER) , title];
    
    self.topLabel.attributedText = title;
    self.lblPlaceholder.text = [NSString stringWithFormat: @"%@", placeholderString];
    
}

-(void) hideTopLabel: (BOOL) hidden;
{
    self.topLabel.hidden = hidden;
}
-(void) setSecureTextEntry: (BOOL)isSecureTextEntry
{
    self.textview.secureTextEntry = isSecureTextEntry;
}
-(void) setKeyboard:(UIKeyboardType) keyboardType
{
    self.textview.keyboardType = keyboardType;
}

-(NSString *) getJsonValue
{
    if ([self.textview.text isKindOfClass: [NSNull class]])
        return @"";
    return self.textview.text;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
