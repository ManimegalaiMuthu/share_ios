//
//  LinkedWorkshopDetailsTableViewCell.h
//  Shell
//
//  Created by Nach on 20/9/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol LinkedWorkshopDeleteDelegate <NSObject>
- (void) deleteDataAt:(NSInteger)row;
@end

@interface LinkedWorkshopDetailsTableViewCell : UITableViewCell

@property (nonatomic, weak) id<LinkedWorkshopDeleteDelegate> delegate;

-(void) setText:(NSString *)workshop;
-(void) setTag:(NSInteger) row;
-(void) setNoDelete;

@end

NS_ASSUME_NONNULL_END
