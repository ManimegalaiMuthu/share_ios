//
//  FormCTAButtonCell.m
//  Shell
//
//  Created by Ankita Chhikara on 27/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "FormCTAButtonCell.h"
#import "Constants.h"
#import "LocalizationManager.h"

@interface FormCTAButtonCell ()
@property NSString *ctaKey;
@property (weak, nonatomic) IBOutlet UIButton *button;

@end

@implementation FormCTAButtonCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.userInteractionEnabled = NO;
    
    [self.button.titleLabel setFont: FONT_BUTTON];
}

-(void) setButtonTitle:(NSString *) buttonTitle
{
    [self.button setTitle: buttonTitle forState: UIControlStateNormal];
}

-(void) setColourWithTextColour:(UIColor *) textColour backgroundColour:(UIColor *) backgroundColour;
{
    [self.button setTitleColor: textColour forState: UIControlStateNormal];
    self.button.backgroundColor = backgroundColour;
}

-(void)setCTADelegate:(id<FormCTACellDelegate>)delegate
{
    self.delegate = delegate;
    
    [self.button addTarget: self action: @selector(buttonPressed) forControlEvents: UIControlEventTouchUpInside];
}

-(void) buttonPressed
{
    [self.delegate ctaPressedWithKey: self.ctaKey];
}

-(void)setUserInteractionEnabled:(BOOL)userInteractionEnabled
{
    [super setUserInteractionEnabled: userInteractionEnabled];
    //self.imgDownIcon.hidden = !userInteractionEnabled; //hide down arrow if not enabled
    if ([self isUserInteractionEnabled])
    {
        self.button.alpha = 1.0f;
    }
    else
    {
        self.button.alpha = 0.5f;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(NSAttributedString *) addAsteriskForRequiredField: (NSString *) string
{
    NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @"* " attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B3, }];
    
    NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: string attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B3, }];
    
    [asteriskAttrString appendAttributedString: titleAttrString];
    
    return asteriskAttrString;
}

@end
