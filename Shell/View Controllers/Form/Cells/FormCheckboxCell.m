//
//  FormCheckboxCell.m
//  Shell
//
//  Created by Jeremy Lua on 27/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "FormCheckboxCell.h"
#import "TTTAttributedLabel.h"
#import "Helper.h"

@interface FormCheckboxCell () <TTTAttributedLabelDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnCheckbox;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblTnc;

@end

@implementation FormCheckboxCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.lblTnc.delegate = self;
    self.lblTnc.font = FONT_B1;
    
    if(kIsRightToLeft) {
        [self.lblTnc setTextAlignment:NSTextAlignmentRight];
    } else {
        [self.lblTnc setTextAlignment:NSTextAlignmentLeft];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//-(void)setCheckboxDelegate:(id<FormCheckboxCellDelegate>)delegate
//{
//    self.delegate = delegate;
//
//}
-(void) setCheckboxSelected:(BOOL) selected
{
    [self.btnCheckbox setSelected: selected];
}

- (IBAction)checkboxPressed:(id)sender
{
    self.btnCheckbox.selected =  !self.btnCheckbox.selected ;
}

-(void) setHyperlinkWithBodyText:(NSString *)bodyText hyperlinkKey:(NSString *)hyperlinkKey
{
    [Helper addHyperlink:self.lblTnc hyperlinkText:bodyText urlString:hyperlinkKey bodyText:self.lblTnc.text];
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    //send hyperlink key to delegate to process
    [self.delegate hyperlinkPressed: [url absoluteString]];
}

-(void) setTitle: (NSString *) title
{
    self.lblTnc.text = title;
}

-(void) setAttributedTitle:(NSAttributedString*)title
{
    self.lblTnc.attributedText = title;
}

-(NSString *) getJsonValue
{
    if (self.btnCheckbox.selected)
        return @"1";
    else
        return @"0";
}
@end
