//
//  FormPanCardIDCell_New.h
//  Shell
//
//  Created by Jeremy Lua on 30/5/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "FormTextfieldCell.h"

NS_ASSUME_NONNULL_BEGIN
//@protocol FormPanCardIDDelegate <NSObject>
//-(void) textfieldCellDidBeginEditingWithKey:(NSString *) key;
//-(void) textfieldCellDidEndEditingWithKey:(NSString *) key;
//@end

@interface FormPanCardIDCell_New : FormTextfieldCell

@property (weak, nonatomic) IBOutlet UITextField *textfield;
@property (weak, nonatomic) IBOutlet UITextField *textfield2;
@property (weak, nonatomic) IBOutlet UITextField *textfield3;

@property BOOL isHidden;
-(void) hideRow:(BOOL) isHidden;
-(void) setTextfieldText: (NSString *) textfieldString textfieldIndex:(NSInteger) index;
-(void) setPlaceholder: (NSString *) title textfieldIndex:(NSInteger) index;
-(void) setKeyboard:(UIKeyboardType) keyboardType textfieldIndex:(NSInteger) index;
-(void) setPancardValuesTo: (NSString *) pancardID;
@end

NS_ASSUME_NONNULL_END
