//
//  FormPANCardIDCell.h
//  Shell
//
//  Created by Jeremy Lua on 27/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "FormBaseCell.h"

enum FormIDAcceptedInput
{
    FormIDAlphabet = 0,
    FormIDNumbers,
};

@interface FormPANCardIDCell : FormBaseCell
@end


@protocol FormPANCardIDCellDatasource
@required
-(int)numberOfInputForCell:(FormPANCardIDCell*) cell;
-(enum FormIDAcceptedInput)inputAcceptedForFieldAtIndex:(int) index;
-(NSString*)placeHolderForFieldAtIndex:(int) index;
@end

@interface FormPANCardIDCell ()
@property (weak, nonatomic) id<FormPANCardIDCellDatasource> datasource;
@property BOOL isNeedSetup;
-(void) setIndividualFields:(NSString *) string;
-(void) setTitle:(NSString*)title;
-(void) setAttributedTitle:(NSAttributedString*)title;
-(void) setupCellWithDatasource:(id<FormPANCardIDCellDatasource>)datasource;
@end
