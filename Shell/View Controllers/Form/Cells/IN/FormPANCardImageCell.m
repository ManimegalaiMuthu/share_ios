//
//  FormPANCardImageCell.m
//  Shell
//
//  Created by Jeremy Lua on 27/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "FormPANCardImageCell.h"
#import "LocalizationManager.h"

@interface FormPANCardImageCell ()
@property (weak, nonatomic) IBOutlet UILabel *topLabel;

@property (weak, nonatomic) IBOutlet UIView *uploadContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *imgCameraIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblUploadBtn;
@property (weak, nonatomic) IBOutlet UIButton *btnUpload;


@property (weak, nonatomic) IBOutlet UIButton *btnPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblUploaded;
@property (weak, nonatomic) IBOutlet UIView *greenLine;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;


@end

@implementation FormPANCardImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
//    self.topLabel.text = LOCALIZATION(C_FORM_PANCARDIMAGE);     //lokalised
    self.topLabel.font = FONT_B1;
    self.topLabel.textColor = COLOUR_VERYDARKGREY;
    
    self.btnDelete.hidden = YES;
    self.lblUploaded.hidden = self.btnDelete.hidden;
    self.greenLine.hidden = self.btnDelete.hidden;
    
    self.uploadContainerView.hidden = NO;
    [self.uploadContainerView setBackgroundColor:COLOUR_RED];
    
    self.lblUploadBtn.text = LOCALIZATION(C_FORM_UPLOAD);
    self.lblUploaded.text = LOCALIZATION(C_FORM_UPLOADED);       //lokalised
}

-(void)hideRow:(BOOL)isHidden
{
    self.isHidden = isHidden;
    self.topLabel.hidden = isHidden;
    self.uploadContainerView.hidden = !self.btnDelete.hidden;   //if delete button showing means have data, don't show upload
    self.clipsToBounds = isHidden;
}

-(void) setTitle:(NSString*)title
{
    self.topLabel.text = title;
}
-(void) setAttributedTitle:(NSAttributedString*)title
{
    self.topLabel.attributedText = title;
}

-(void) setPhoto:(UIImage *) image
{
    [self.btnPhoto.imageView setContentMode: UIViewContentModeScaleAspectFit];
    [self.btnPhoto setImage: image forState: UIControlStateNormal];
    
    self.btnDelete.hidden = NO;
    self.lblUploaded.hidden = self.btnDelete.hidden;
    self.greenLine.hidden = self.btnDelete.hidden;
    
    self.uploadContainerView.hidden = YES;
    self.panImage = image;
}

- (IBAction)uploadPressed:(id)sender
{
    [self.delegate takePhoto: self.keyString];
}


- (IBAction)photoPressed:(id)sender
{
//    if (self.btnDelete.hidden)
//        [self.delegate takePhoto]; //if delete button is not showing, no photo yet
//    else
        [self.delegate showPhoto: self.panImage]; //if delete button is showing, photo to upload
}

- (IBAction)deletePressed:(id)sender
{
    [self.btnPhoto setImage: nil forState: UIControlStateNormal];
    [self.btnPhoto.imageView setContentMode: UIViewContentModeScaleToFill];
    
    self.btnDelete.hidden = YES;
    self.lblUploaded.hidden = self.btnDelete.hidden;
    self.greenLine.hidden = self.btnDelete.hidden;
    
    self.uploadContainerView.hidden = NO;
}

-(NSString *) getJsonValue
{
    
    if (self.btnDelete.hidden)
        return @""; //if delete button is not showing, no photo yet
    else
        return [UIImageJPEGRepresentation(self.btnPhoto.currentImage, 0.5f) base64EncodedStringWithOptions:0];
    //if delete button is showing, photo to upload, return base64 encoded string
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
