//
//  FormDriverLicenseCell.m
//  Shell
//
//  Created by Jeremy Lua on 31/5/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "FormDriverLicenseCell.h"

@implementation FormDriverLicenseCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger maxLength = 0;
    if (textField == self.textfield)
    {
        if (NSNotFound != [string rangeOfCharacterFromSet:NSCharacterSet.decimalDigitCharacterSet].location)
        {
            //do not change if not letters
            return NO;
        }
        maxLength = 2;
    }
    else if (textField == self.textfield2)
    {
        if (NSNotFound != [string rangeOfCharacterFromSet:NSCharacterSet.letterCharacterSet].location)
        {
            //do not change if not numbers
            return NO;
        }
        maxLength = 2;
    }
    else if (textField == self.textfield3)
    {
        if (NSNotFound != [string rangeOfCharacterFromSet:NSCharacterSet.letterCharacterSet].location)
        {
            //do not change if not numbers
            return NO;
        }
        maxLength = 13;
    }
    
    if (textField.text.length >= maxLength && range.length == 0)
    {
        return NO; // return NO to not change text
    }
    else
    {
        return YES;
    }
}

-(void) setPancardValuesTo: (NSString *) pancardID {
    if([pancardID length] >= 11) {
        [self setTextfieldText: [pancardID substringWithRange: NSMakeRange(0, 2)] textfieldIndex:0];
        [self setTextfieldText: [pancardID substringWithRange: NSMakeRange(2, 2)] textfieldIndex:1];
        [self setTextfieldText: [pancardID substringWithRange: NSMakeRange(4, [pancardID length] - 4)] textfieldIndex:2];
    }
}

@end
