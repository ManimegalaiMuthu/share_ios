//
//  FormDriverLicenseCell.h
//  Shell
//
//  Created by Jeremy Lua on 31/5/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "FormPanCardIDCell_New.h"

NS_ASSUME_NONNULL_BEGIN

@interface FormDriverLicenseCell : FormPanCardIDCell_New

@end

NS_ASSUME_NONNULL_END
