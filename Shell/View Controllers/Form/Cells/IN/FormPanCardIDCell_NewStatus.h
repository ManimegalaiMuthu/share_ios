//
//  FormPanCardIDCell_NewStatus.h
//  Shell
//
//  Created by MUTHUPANDIYAN Manimegalai on 11/10/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FormTextfieldCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface FormPanCardIDCell_NewStatus : FormTextfieldCell
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgStatus;

@property (weak, nonatomic) IBOutlet UITextField *textfield;
@property (weak, nonatomic) IBOutlet UITextField *textfield2;
@property (weak, nonatomic) IBOutlet UITextField *textfield3;

@property BOOL isHidden;
-(void) hideRow:(BOOL) isHidden;
-(void) setTextfieldText: (NSString *) textfieldString textfieldIndex:(NSInteger) index;
-(void) setPlaceholder: (NSString *) title textfieldIndex:(NSInteger) index;
-(void) setKeyboard:(UIKeyboardType) keyboardType textfieldIndex:(NSInteger) index;
-(void) setPancardValuesTo: (NSString *) pancardID;
-(void) setStatusTitle: (NSString *) statusString;

@end

NS_ASSUME_NONNULL_END
