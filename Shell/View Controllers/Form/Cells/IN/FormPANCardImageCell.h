//
//  FormPANCardImageCell.h
//  Shell
//
//  Created by Jeremy Lua on 27/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "FormBaseCell.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FormPANCardImageCellDelegate <NSObject>
-(void) showPhoto:(UIImage *) image;
-(void) takePhoto: (NSString *) cellKey; //key of cell to assign photo to
@end


@interface FormPANCardImageCell : FormBaseCell
@property NSString *keyString;
@property BOOL isHidden;
@property UIImage *panImage;

-(void) hideRow:(BOOL) isHidden;

@property id<FormPANCardImageCellDelegate> delegate;

-(void) setPhoto:(UIImage *) image;
-(void) setTitle:(NSString*)title;
-(void) setAttributedTitle:(NSAttributedString*)title;
@end


NS_ASSUME_NONNULL_END
