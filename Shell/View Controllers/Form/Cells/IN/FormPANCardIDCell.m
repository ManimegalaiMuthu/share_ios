//
//  FormPANCardIDCell.m
//  Shell
//
//  Created by Jeremy Lua on 27/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "FormPANCardIDCell.h"
#import <Foundation/Foundation.h>

@interface FormPANCardIDCell () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel* titleLabel;
@property (weak, nonatomic) IBOutlet UIView* inputStack;
@property NSMutableArray<UITextField*>* textFields;

@end

@implementation FormPANCardIDCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.titleLabel setFont:FONT_B1];
    self.titleLabel.textColor = COLOUR_VERYDARKGREY;
    self.textFields = [NSMutableArray new];
    self.isNeedSetup = true;
}

-(void) setTitle:(NSString*)title
{
    self.titleLabel.text = title;
}
-(void) setAttributedTitle:(NSAttributedString*)title
{
    self.titleLabel.attributedText = title;
}

-(void) setupCellWithDatasource:(id<FormPANCardIDCellDatasource>)datasource
{
    self.datasource = datasource;
    
    if(self.isNeedSetup)
    {
        self.isNeedSetup = false;
        [self setNeedsLayout];
        [self layoutIfNeeded];
        
        [self setupInputStackFor: [self.datasource numberOfInputForCell:self]];
    }
}

-(void) setupInputStackFor:(int) stacks
{
    for(UIView* view in self.inputStack.subviews)
    {
        [view removeFromSuperview];
    }
    [self.textFields removeAllObjects];
//    CGFloat totalWidth = self.inputStack.frame.size.width;
    CGFloat totalWidth = SCREEN_WIDTH - 30 - 30;
    for(int i = 0; i < stacks; i++){
        UITextField* newField = [[UITextField alloc] initWithFrame:CGRectMake(totalWidth/(stacks)*i, 0,
                                                                              totalWidth/(stacks), 30)];
        newField.placeholder = [self.datasource placeHolderForFieldAtIndex:i];
        newField.textColor = COLOUR_VERYDARKGREY;
        newField.textAlignment = NSTextAlignmentCenter;
        newField.layer.borderWidth = 1;
        newField.layer.borderColor = [[UIColor alloc] initWithWhite:0.85 alpha:1].CGColor;
        newField.delegate = self;
        [newField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        
        enum FormIDAcceptedInput acceptedInput = [self.datasource inputAcceptedForFieldAtIndex:i];
        newField.tag = acceptedInput;
        switch (acceptedInput) {
            case FormIDAlphabet:
                [newField setKeyboardType:UIKeyboardTypeAlphabet];
                break;
                
            case FormIDNumbers:
                [newField setKeyboardType:UIKeyboardTypeNumberPad];
                break;
                
            default:
                break;
        }
        
        [self.inputStack addSubview:newField];
        [self.textFields addObject:newField];
    }
}

-(void) setIndividualFields:(NSString *) string
{
    if ([string isKindOfClass: [NSNull class]])
        string = @"";
    [self setupInputStackFor: [string length]];
    int i = 0;
    for(UITextField* textField in self.textFields)
    {
        if (i >= [string length])
            return;
        textField.text = [string substringWithRange: NSMakeRange(i, 1)];
        i++;
    }
}

-(NSString *) getJsonValue
{
    NSString* returnString = @"";
    
    
    for(UITextField* textField in self.textFields)
    {
        returnString = [returnString stringByAppendingString:textField.text];
    }
    
    return returnString;
}

-(void) textFieldDidChange:(UITextField*)sender
{
    int pos = [self.textFields indexOfObject:sender];
    if(sender.text.length == 1)
    {
        sender.text = sender.text.uppercaseString;
        if(self.textFields.count > pos+1)
            [self.textFields[pos+1] becomeFirstResponder];
    }
    else
    {
        if(pos != 0)
            [self.textFields[[self.textFields indexOfObject:sender]-1] becomeFirstResponder];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //Backspace
    if([string isEqualToString: @""] && range.length == 1)
        return true;
    
    if(textField.text.length == 1)
        return false;
    
    NSCharacterSet* setToCheck;
    switch (textField.tag) {
        case FormIDAlphabet:
            setToCheck = [NSCharacterSet letterCharacterSet].invertedSet;
            break;
            
        case FormIDNumbers:
            setToCheck = [NSCharacterSet decimalDigitCharacterSet].invertedSet;
            break;
            
        default:
            break;
    }
    
    if([string rangeOfCharacterFromSet:setToCheck].location != NSNotFound)
        return false;
    else
        return true;
}



@end
