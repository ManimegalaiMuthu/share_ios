//
//  FormPanCardIDCell_New.m
//  Shell
//
//  Created by Jeremy Lua on 30/5/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "FormPanCardIDCell_New.h"

@interface FormPanCardIDCell_New() <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *topLabel;

@property (nonatomic) NSString *textfieldKey;

@end


@implementation FormPanCardIDCell_New

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.textfield setFont: FONT_B1];
    [self.textfield2 setFont: FONT_B1];
    [self.textfield3 setFont: FONT_B1];
    
    self.textfield.delegate = self;
    self.textfield2.delegate = self;
    self.textfield3.delegate = self;
    
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [self.textfield setLeftViewMode:UITextFieldViewModeAlways];
    [self.textfield setLeftView:spacerView];
    UIView *spacerView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [self.textfield2 setLeftViewMode:UITextFieldViewModeAlways];
    [self.textfield2 setLeftView:spacerView2];
    UIView *spacerView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [self.textfield3 setLeftViewMode:UITextFieldViewModeAlways];
    [self.textfield3 setLeftView:spacerView3];
}

-(void)hideRow:(BOOL)isHidden
{
    self.isHidden = isHidden;
    self.topLabel.hidden = isHidden;
    self.clipsToBounds = isHidden;
}

-(void) setTextfieldText: (NSString *) textfieldString textfieldIndex:(NSInteger) index
{
    switch (index) {
        case 0:
            self.textfield.text = textfieldString;
            break;
        case 1:
            self.textfield2.text = textfieldString;
            break;
        case 2:
            self.textfield3.text = textfieldString;
            break;
        default:
            break;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger maxLength = 0;
    if (textField == self.textfield)
    {
        if (NSNotFound != [string rangeOfCharacterFromSet:NSCharacterSet.decimalDigitCharacterSet].location)
        {
            //do not change if not letters
            return NO;
        }
        maxLength = 5;
    }
    else if (textField == self.textfield2)
    {
        if (NSNotFound != [string rangeOfCharacterFromSet:NSCharacterSet.letterCharacterSet].location)
        {
            //do not change if not numbers
            return NO;
        }
        maxLength = 4;
    }
    else if (textField == self.textfield3)
    {
        if (NSNotFound != [string rangeOfCharacterFromSet:NSCharacterSet.decimalDigitCharacterSet].location)
        {
            //do not change if not letters
            return NO;
        }
        maxLength = 1;
    }
    
    if (textField.text.length >= maxLength && range.length == 0)
    {
        return NO; // return NO to not change text
    }
    else
    {
        return YES;
    }
}

-(void) setPlaceholder: (NSString *) title textfieldIndex:(NSInteger) index
{
    switch (index) {
        case 0:
            self.textfield.placeholder = title;
            break;
        case 1:
            self.textfield2.placeholder = title;
            break;
        case 2:
            self.textfield3.placeholder = title;
            break;
        default:
            break;
    }
}

-(void) setKeyboard:(UIKeyboardType) keyboardType textfieldIndex:(NSInteger) index
{
    switch (index) {
        case 0:
            self.textfield.keyboardType = keyboardType;
            break;
        case 1:
            self.textfield2.keyboardType = keyboardType;
            break;
        case 2:
            self.textfield3.keyboardType = keyboardType;
            break;
        default:
            break;
    }
}

-(void) setPancardValuesTo: (NSString *) pancardID {
    if([pancardID length] == 10) {
        [self setTextfieldText: [pancardID substringWithRange: NSMakeRange(0, 5)] textfieldIndex:0];
        [self setTextfieldText: [pancardID substringWithRange: NSMakeRange(5, 4)] textfieldIndex:1];
        [self setTextfieldText: [pancardID substringWithRange: NSMakeRange(9, 1)] textfieldIndex:2];
    }
}


-(NSString *) getJsonValue
{
    if ([self.textfield.text isKindOfClass: [NSNull class]] ||
        [self.textfield2.text isKindOfClass: [NSNull class]] ||
        [self.textfield3.text isKindOfClass: [NSNull class]])
        return @"";
    
    if(self.textfield3.text == nil) {
        return [NSString stringWithFormat: @"%@%@", self.textfield.text, self.textfield2.text];
    }
    
    return [NSString stringWithFormat: @"%@%@%@", self.textfield.text, self.textfield2.text, self.textfield3.text];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
