//
//  FormTextfieldWithButtonCell.h
//  Shell
//
//  Created by Admin on 25/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Constants.h"
#import "FormBaseCell.h"

@protocol FormTextfieldWithButtonCellDelegate
-(void) buttonPressedWithKey:(NSString *) key;
@end

@interface FormTextfieldWithButtonCell : FormBaseCell
@property (nonatomic) id<FormTextfieldWithButtonCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *button;

-(void) setTextfieldEnabled: (BOOL) enabled;
-(void) hideTopLabel: (BOOL) hidden;
-(void) setTitle: (NSString *) title;
-(void) setTextfieldText: (NSString *) textString;
-(void) setIconImage: (NSString *) imageName;
-(NSString *) getJsonValue;
-(void) setTextfieldDelegate:(id<UITextFieldDelegate>) delegate;

-(void) setTextFieldTag:(NSInteger) tag;

-(void) setRequiredTitle: (NSAttributedString *) title placeholder:(NSString *)placeholderString;


-(void) setButtonKey:(NSString *)buttonKey;
-(void) setButtonDelegate:(id<FormTextfieldWithButtonCellDelegate>)delegate;
@end
