//
//  FormBaseCellTableViewCell.h
//  Shell
//
//  Created by Jeremy Lua on 15/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FormCellData.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FormCellUpdateDelegate <NSObject>
-(void) didUpdateCellData;
@end

@interface FormBaseCell : UITableViewCell
@property id<FormCellUpdateDelegate> updateDelegate;
//@property FormCellData *cellData;
-(NSString *) getJsonValue;
@end

NS_ASSUME_NONNULL_END
