//
//  FormBaseCell.m
//  Shell
//
//  Created by Jeremy Lua on 15/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "FormBaseCell.h"

@implementation FormBaseCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
