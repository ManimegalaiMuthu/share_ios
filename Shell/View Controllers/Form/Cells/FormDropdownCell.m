//
//  FormDropdownCell.m
//  Shell
//
//  Created by Admin on 25/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "FormDropdownCell.h"

@interface FormDropdownCell()
@property NSString *dropdownKey;
@property (weak, nonatomic) IBOutlet UIImageView *imgDownIcon;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UIButton *button;
@end

@implementation FormDropdownCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.button.titleLabel setFont: FONT_B1];
    self.button.titleLabel.text = @"";
    [self.button setTitle:@"" forState:UIControlStateNormal];
    [self.topLabel setFont: FONT_B1];
}

-(void)setButtonContentHorizontalAlignment: (UIControlContentHorizontalAlignment) alignment
{
    [self.button setContentHorizontalAlignment:alignment];
}

-(void)setDropdownDelegate:(id<FormDropdownCellDelegate>)delegate
{
    self.delegate = delegate;
    
    [self.button addTarget: self action: @selector(buttonPressed) forControlEvents: UIControlEventTouchUpInside];
}

-(void)setBirthdateDelegate:(id<FormDropdownCellDelegate>)delegate
{
    self.delegate = delegate;
    
    [self.button addTarget: self action: @selector(birthdayPressed) forControlEvents: UIControlEventTouchUpInside];
}

-(void) birthdayPressed
{
    [self.delegate birthdatePressedWithKey: self.dropdownKey];
}

-(void) buttonPressed
{
    [self.delegate cellPressedWithKey: self.dropdownKey];
}

-(void) setSelectionTitle:(NSString *) selectionTitle
{
    [self setSelectionTitle: selectionTitle textColor: COLOUR_DARKGREY];
}

-(void) setSelectionTitle:(NSString *) selectionTitle textColor:(UIColor*)color
{
    [self.button setTitle: selectionTitle forState: UIControlStateNormal];
    
    if (![self isUserInteractionEnabled])
    {
        [self.button setTitleColor: COLOUR_LIGHTGREY forState:UIControlStateNormal];
        self.topLabel.textColor = COLOUR_LIGHTGREY;
    }
    else
    {
        [self.button setTitleColor:color forState:UIControlStateNormal];
    }
}

-(void) setRequiredTitle: (NSAttributedString *) title placeholder:(NSString *)placeholderString
{
    //        self.textfield.placeholder = [NSString stringWithFormat: @"%@ %@",  LOCALIZATION(C_TEXTFIELD_PLEASEENTER) , title];
    
    self.topLabel.attributedText = title;
    
}

- (void)setButtonTag:(NSInteger)tag
{
    self.button.tag = tag;
}

-(void) hideTopLabel: (BOOL) hidden
{
    self.topLabel.hidden = hidden;
}


-(void) setTitle: (NSString *) title
{
    self.topLabel.text = title;
}

-(NSString *) getJsonValue
{
    if ([self.button.titleLabel.text isKindOfClass: [NSNull class]])
        return @"";
    return self.button.titleLabel.text;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setUserInteractionEnabled:(BOOL)userInteractionEnabled
{
    [super setUserInteractionEnabled: userInteractionEnabled];
    //self.imgDownIcon.hidden = !userInteractionEnabled; //hide down arrow if not enabled
    if (![self isUserInteractionEnabled])
    {
        [self.button setTitleColor: COLOUR_LIGHTGREY forState: UIControlStateNormal];
    }
}


@end
