//
//  LinkedWorkshopDetailsTableViewCell.m
//  Shell
//
//  Created by Nach on 20/9/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "LinkedWorkshopDetailsTableViewCell.h"
#import "Constants.h"

@interface LinkedWorkshopDetailsTableViewCell()

@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UITextField *textfield;
@property NSInteger rowTag;

@end

@implementation LinkedWorkshopDetailsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.textfield.font = FONT_B1;
    self.textfield.textColor = COLOUR_LIGHTGREY;
    
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [self.textfield setLeftViewMode:UITextFieldViewModeAlways];
    [self.textfield setLeftView:spacerView];
    
    self.btnDelete.hidden = NO;
    self.btnDelete.userInteractionEnabled = YES;
    [NSLayoutConstraint constraintWithItem:self.btnDelete attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:20].active = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setText:(NSString *)workshop {
    [self.textfield setText:workshop];
}

-(void) setTag:(NSInteger) row {
    self.rowTag = row;
}

-(void) setNoDelete {
    self.btnDelete.hidden = YES;
    self.btnDelete.userInteractionEnabled = NO;
    [NSLayoutConstraint constraintWithItem:self.btnDelete attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
}

- (IBAction)deletePressed:(UIButton *)sender {
    [self.delegate deleteDataAt:self.rowTag];
}


@end
