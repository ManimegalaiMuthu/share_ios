//
//  FormImageCell.h
//  Shell
//
//  Created by Jeremy Lua on 27/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "FormBaseCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface FormImageCell : FormBaseCell
-(void) setImage:(UIImage *) image;
@end

NS_ASSUME_NONNULL_END
