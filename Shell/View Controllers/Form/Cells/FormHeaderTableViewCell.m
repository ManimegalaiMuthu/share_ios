//
//  FormHeaderTableViewCell.m
//  Shell
//
//  Created by Ankita Chhikara on 27/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "FormHeaderTableViewCell.h"
#import "Constants.h"
#import "LocalizationManager.h"

@interface FormHeaderTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblRequired;
@property (weak, nonatomic) IBOutlet UIView *line;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelHeight;

@end

@implementation FormHeaderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.lblTitle setFont: FONT_H1];
    self.userInteractionEnabled = NO;
    
    self.lblRequired.attributedText = [self addAsteriskForRequiredField: LOCALIZATION(C_PROFILE_REQUIREDFIELDS)];   //lokalise 30 jan
}

-(void) setTitle: (NSString *) title
{
    self.lblTitle.text = title;
    if([title isEqualToString:@" "])
    {
        self.line.alpha = 0;
        self.labelHeight.constant = 0;
    }
    else
    {
        self.line.alpha = 1;
        self.labelHeight.constant = 50;
    }
}

-(void) setLabel: (NSString *) text
{
    self.lblRequired.text = text;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(NSAttributedString *) addAsteriskForRequiredField: (NSString *) string
{
    NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @"* " attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B3, }];
    
    NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: string attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B3, }];
    
    [asteriskAttrString appendAttributedString: titleAttrString];
    
    return asteriskAttrString;
}

@end
