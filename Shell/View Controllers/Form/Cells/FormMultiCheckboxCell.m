//
//  FormMultiCheckboxTableViewCell.m
//  Shell
//
//  Created by Jeremy Lua on 29/5/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "FormMultiCheckboxCell.h"
#import "ContactPreferencesCollectionViewCell.h"
#import "Session.h"
#import "Headers.h"


@interface FormMultiCheckboxCell ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *colViewHeight;

@end
/* FormMultiCheckboxCell.xib
 Heights constants needs to be set for AutoLayout to lay out the UI, otherwise the UI will not be able to be shown.
 */
@implementation FormMultiCheckboxCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.lblTitle setFont: FONT_B1];
//    self.lblTitle.text = LOCALIZATION(C_TITLE_CONTACT_PREFERENCE); //lokalise 30 jan
    
    [self.multiCheckboxColView registerNib:[UINib nibWithNibName: @"ContactPreferencesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier: @"ContactPrefCell"];
    
    
    self.colViewHeight.constant = 50 * ceil([self.checkboxLabelArray count] / 2.0f);
    self.multiCheckboxColView.backgroundColor = [UIColor clearColor];
    
    if (IS_IPHONE5)
    {
        self.colViewLayout.estimatedItemSize = CGSizeMake((SCREEN_WIDTH - 100) / 2, 50);
    }
    else
    {
        if(@available(iOS 10, *))
        {
            self.colViewLayout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize;
        }
        else
        {
            self.colViewLayout.estimatedItemSize = CGSizeMake(130,50);
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - CollectionView DataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.checkboxLabelArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //dont need to reload, cell is persistent in FormTableViewDelegate
    ContactPreferencesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"ContactPrefCell" forIndexPath: indexPath];
    
    cell.delegate = self;
    
    UIButton *checkboxBtn = [cell viewWithTag: 1]; //tagged in storyboard
    //    checkboxBtn.selected  = YES;
    
    UILabel *contactPref = [cell viewWithTag: 2]; //tagged in storyboard
    contactPref.text = [self.checkboxLabelArray objectAtIndex: indexPath.row];
    contactPref.font = FONT_B2;
    
    cell.bitValue = indexPath.row;
    
    if ([[self.checkboxValueString componentsSeparatedByString: @","] containsObject: @(cell.bitValue).stringValue]){
        checkboxBtn.selected = YES;
        self.bottomLine.hidden = NO;
    }
    else {
        checkboxBtn.selected = NO;
    }
    
    
    if (!self.checkboxCellArray)
        self.checkboxCellArray = [[NSMutableArray alloc] init];
    [self.checkboxCellArray addObject: cell];
 
    return cell;
}

-(void)didSelectCell:(NSInteger) index isSelected:(BOOL) isSelected
{
    if ([self.delegate respondsToSelector: @selector(contactPrefCellDidSelectCell:isSelected:sender:)])
        [self.delegate contactPrefCellDidSelectCell: index isSelected: isSelected sender:self];
}

-(NSString *) getJsonValue
{
    NSString *checkboxString = @"";
    for (ContactPreferencesCollectionViewCell* cell in self.checkboxCellArray)
    {
        if ([cell.checkboxBtn isSelected])
        {
            if (checkboxString.length == 0)
                checkboxString = @(cell.bitValue).stringValue;
            else
                checkboxString = [checkboxString stringByAppendingString: [NSString stringWithFormat: @",%@", @(cell.bitValue).stringValue]];
        }
    }
    return checkboxString;
}

@end
