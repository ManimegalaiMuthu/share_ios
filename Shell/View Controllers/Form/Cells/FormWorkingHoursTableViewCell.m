//
//  FormWorkingHoursTableViewCell.m
//  Shell
//
//  Created by Ankita Chhikara on 31/1/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "FormWorkingHoursTableViewCell.h"
#import "FormWorkingHoursLabelTableViewCell.h"
#import "Constants.h"
#import "LocalizationManager.h"
#import "LocalizationConstants.h"

@interface FormWorkingHoursTableViewCell () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIImageView *imgDownIcon;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableviewHeight;

@property NSArray *fullWorkWeekDays;
@property NSArray *halfWorkWeekDays;

@property NSMutableArray *dataArray;
@end

@implementation FormWorkingHoursTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.button setTitle: LOCALIZATION(C_PROFILE_WORKINGHOURS) forState:UIControlStateNormal]; //lokalise 30 jan
    [self.button.titleLabel setFont: FONT_B1];
    
    if (kIsRightToLeft)
    {
        [self.button setContentHorizontalAlignment: UIControlContentHorizontalAlignmentRight];
    }
    else
    {
        [self.button setContentHorizontalAlignment: UIControlContentHorizontalAlignmentLeft];
    }
    
    [self.tableView registerNib:[UINib nibWithNibName:@"FormWorkingHoursLabelTableViewCell" bundle:nil] forCellReuseIdentifier:@"FormWorkingHoursLabelTableViewCell"];
    
    [self reloadCellData];
}

-(void) reloadCellData
{
    self.fullWorkWeekDays = @[];
    self.halfWorkWeekDays = @[];
    self.dataArray = [[NSMutableArray alloc] init];
    
    if (![[self.workingHoursDict objectForKey: @"WeekDays1"] isEqualToString: @""])
        self.fullWorkWeekDays = [[self.workingHoursDict objectForKey: @"WeekDays1"] componentsSeparatedByString: @","];
    
    if (![[self.workingHoursDict objectForKey: @"WeekDays2"] isEqualToString: @""])
        self.halfWorkWeekDays = [[self.workingHoursDict objectForKey: @"WeekDays2"] componentsSeparatedByString: @","];
    
    NSArray *leftLabelArray = @[LOCALIZATION(C_MON),
                                LOCALIZATION(C_TUE),
                                LOCALIZATION(C_WED),
                                LOCALIZATION(C_THU),
                                LOCALIZATION(C_FRI),
                                LOCALIZATION(C_SAT),
                                LOCALIZATION(C_SUN),
                                ];  //lokalise 30 jan
    for (int i = 0; i < 7; i++)
    {
        int daysBitValue = 1 << i;
        if ([self.fullWorkWeekDays containsObject: @(daysBitValue).stringValue])
        {
            [self.dataArray addObject: @{@"LeftLabel": leftLabelArray[i],
                                         @"RightLabel": [NSString stringWithFormat: @"%@ - %@", [self.workingHoursDict objectForKey: @"WorkHourStart1"], [self.workingHoursDict objectForKey: @"WorkHourEnd1"]],
                                         }
             ];
        }
        if ([self.halfWorkWeekDays containsObject: @(daysBitValue).stringValue])
        {
            [self.dataArray addObject: @{@"LeftLabel": leftLabelArray[i],
                                         @"RightLabel": [NSString stringWithFormat: @"%@ - %@", [self.workingHoursDict objectForKey: @"WorkHourStart2"], [self.workingHoursDict objectForKey: @"WorkHourEnd2"]],
                                         }
             ];
        }
    }
    
    self.tableviewHeight.constant = [self.dataArray count] * 44;
    [self.tableView reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cellData = [self.dataArray objectAtIndex: indexPath.row];
    FormWorkingHoursLabelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"FormWorkingHoursLabelTableViewCell"];
    [cell setLeftLabel: [cellData objectForKey: @"LeftLabel"]];
    [cell setRightLabel: [cellData objectForKey: @"RightLabel"]];
    return cell;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
