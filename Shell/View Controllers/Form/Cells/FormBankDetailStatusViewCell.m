//
//  FormBankDetailStatusViewCell.m
//  Shell
//
//  Created by MUTHUPANDIYAN Manimegalai on 10/10/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "FormBankDetailStatusViewCell.h"

@interface FormBankDetailStatusViewCell() <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblTop;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (nonatomic) NSString *textfieldKey;

@end
@implementation FormBankDetailStatusViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.txtName setFont: FONT_B1];
       [self.lblTop setFont: FONT_B1];
       self.lblTop.textColor = COLOUR_VERYDARKGREY;
    
    
    
    [self.lblStatus setFont:FONT_B2];
    self.lblStatus.textColor = COLOUR_VERYDARKGREY;
       
       self.txtName.delegate = self;
       if(kIsRightToLeft) {
           [self.txtName setTextAlignment:NSTextAlignmentRight];
       } else {
           [self.txtName setTextAlignment:NSTextAlignmentLeft];
       }
}

-(void) setCellKey: (NSString *) key
{
    self.textfieldKey = key;
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([self.delegate respondsToSelector: @selector(textfieldCellDidBeginEditingWithKey:)])
        [self.delegate textfieldCellDidBeginEditingWithKey: self.textfieldKey];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([self.delegate respondsToSelector: @selector(textfieldCellDidEndEditingWithKey:)])
        [self.delegate textfieldCellDidEndEditingWithKey: self.textfieldKey];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([self.delegate respondsToSelector:@selector(textFieldCell:shouldChangeCharactersInRange:replacementString:)])
        return [self.delegate textFieldCell:self.textfieldKey shouldChangeCharactersInRange:range replacementString:string];
    else
        return YES;
    
}

- (BOOL)validateStringWithAlphabet:(NSString *)string {
    NSString *stringRegex = @"[A-Za-z]";
    NSPredicate *stringPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stringRegex];

    return [stringPredicate evaluateWithObject:string];
}

-(void) setTextfieldText: (NSString *) textfieldString
{
    self.txtName.text = textfieldString;
    
}

//-(void) setIconImage: (NSString *) imageName
//{
//
//    [self.imgStatus setImage:[UIImage imageNamed:imageName]];
//
//}


-(void) setPlaceholder: (NSString *) title
{
    self.txtName.placeholder = [NSString stringWithFormat: @"%@", title];
}

-(void) setSecureTextEntry: (BOOL)isSecureTextEntry
{
    self.txtName.secureTextEntry = isSecureTextEntry;
}
-(void) setKeyboard:(UIKeyboardType) keyboardType
{
    self.txtName.keyboardType = keyboardType;
}

-(void) setTitle: (NSString *) title
{
    if ([self.lblTop isHidden])
        self.txtName.placeholder = title;
    else
//        self.textfield.placeholder = [NSString stringWithFormat: @"%@ %@",  LOCALIZATION(C_TEXTFIELD_PLEASEENTER) , title];
        
        self.txtName.placeholder = [NSString stringWithFormat: @"%@", title];
    self.lblTop.text = title;
   // self.lblStatus.text = @"mani";
}

-(void) setStatusTitle: (NSString *) statusString
{
    NSLog(@"Status string:%@",statusString);
    if ([statusString isEqualToString:@"Approved"]) {
        //self.lblStatus.textColor = COLOUR_DARKGREEN;
        [self.imgStatus setImage:[UIImage imageNamed:@"icn_success_small"]];
    }else{
      //   self.lblStatus.textColor = COLOUR_RED;
         [self.imgStatus setImage:[UIImage imageNamed:@"icn_pending_small_on"]];
    }
    self.lblStatus.text = statusString;
}

-(void) setRequiredTitle: (NSAttributedString *) title placeholder:(NSString *)placeholderString
{
        //        self.textfield.placeholder = [NSString stringWithFormat: @"%@ %@",  LOCALIZATION(C_TEXTFIELD_PLEASEENTER) , title];
    
    self.lblTop.attributedText = title;
    self.txtName.placeholder = [NSString stringWithFormat: @"%@", placeholderString];
    
}

-(void) hideTopLabel: (BOOL) hidden;
{
    self.lblTop.hidden = hidden;
}

-(void)enableCell:(BOOL)userInteractionEnabled{
    [self setUserInteractionEnabled: userInteractionEnabled];
    [self.txtName setUserInteractionEnabled: userInteractionEnabled];
    if (![self isUserInteractionEnabled])
    {
        self.txtName.textColor = COLOUR_LIGHTGREY;
        self.lblTop.textColor = COLOUR_LIGHTGREY;
    }
}

-(NSString *) getJsonValue
{
    if ([self.txtName.text isKindOfClass: [NSNull class]])
        return @"";
    return self.txtName.text;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) toggleProfilingQuestion:(BOOL) toEnable
{
    if (toEnable)
    {
        //enabled UI
        self.lblTop.textColor = COLOUR_VERYDARKGREY;
        self.userInteractionEnabled = YES;
    }
    else
    {
        //disabled UI
        self.lblTop.textColor = COLOUR_LIGHTGREY;
        self.txtName.text = @"";
        self.userInteractionEnabled = NO;
    }
}

-(void) setTextfieldDelegate:(id<FormBankDetailStatusViewCellDelegate>) delegate
{
//    self.textfield.delegate = delegate;
    self.delegate = delegate;
}

-(void) setTextFieldTag:(NSInteger) tag {
    self.txtName.tag = tag;
}


@end
