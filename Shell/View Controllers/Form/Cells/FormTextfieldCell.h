//
//  FormTextfieldCell.h
//  Shell
//
//  Created by Admin on 25/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "LocalizationManager.h"
#import <Foundation/Foundation.h>
#import "FormBaseCell.h"

@protocol FormTextfieldDelegate <NSObject>
-(void) textfieldCellDidBeginEditingWithKey:(NSString *) key;
-(void) textfieldCellDidEndEditingWithKey:(NSString *) key;
-(BOOL) textFieldCell:(NSString *) key shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
@end

@interface FormTextfieldCell : FormBaseCell
@property id<FormTextfieldDelegate> delegate;

-(void) setCellKey: (NSString *) key;
-(void) setTextfieldText: (NSString *) textfieldString;
-(void) setTitle: (NSString *) title;
-(void) setPlaceholder: (NSString *) title;
-(void) hideTopLabel: (BOOL) hidden;
-(void) setSecureTextEntry: (BOOL)isSecureTextEntry;
-(void) setKeyboard:(UIKeyboardType) keyboardType;
-(void) enableCell:(BOOL)userInteractionEnabled;
-(NSString *) getJsonValue;

-(void) setTextfieldDelegate:(id<UITextFieldDelegate>) delegate;
-(void) setRequiredTitle: (NSAttributedString *) title placeholder:(NSString *)placeholderString;

-(void) toggleProfilingQuestion:(BOOL) toEnable;

-(void) setTextFieldTag:(NSInteger) tag;

@end
