//
//  FormDropdownCell.h
//  Shell
//
//  Created by Admin on 25/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Constants.h"
#import "FormBaseCell.h"

@protocol FormDropdownCellDelegate
-(void) cellPressedWithKey:(NSString *) key;
-(void) birthdatePressedWithKey:(NSString *) key;

@end

@interface FormDropdownCell : FormBaseCell
@property (nonatomic) id<FormDropdownCellDelegate> delegate;

-(void)setButtonContentHorizontalAlignment: (UIControlContentHorizontalAlignment) alignment;
-(void)setBirthdateDelegate:(id<FormDropdownCellDelegate>)delegate;
-(void) setDropdownDelegate:(id<FormDropdownCellDelegate>)delegate;
-(void) setDropdownKey:(NSString *)dropdownKey;
-(void) hideTopLabel: (BOOL) hidden;
-(void) setTitle: (NSString *) title;
-(void) setSelectionTitle:(NSString *) selectionTitle;
-(void) setSelectionTitle:(NSString *) selectionTitle textColor:(UIColor*)color;
-(void) setRequiredTitle: (NSAttributedString *) title placeholder:(NSString *)placeholderString;
-(void) setButtonTitle: (NSString*)title;
-(void) setButtonAttributedTitle: (NSAttributedString*)title;
-(void) setButtonTag: (NSInteger)tag;
-(NSString *) getJsonValue;
//-(void) setIconImage: (NSString *) imageName;
@end
