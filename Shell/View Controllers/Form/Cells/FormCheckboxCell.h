//
//  FormCheckboxCell.h
//  Shell
//
//  Created by Jeremy Lua on 27/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "FormBaseCell.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FormCheckboxCellDelegate
-(void) hyperlinkPressed: (NSString *) link;

@end
@interface FormCheckboxCell : FormBaseCell
@property id<FormCheckboxCellDelegate> delegate;
-(void) setCheckboxSelected:(BOOL) selected;
-(void) setTitle: (NSString *) title;
-(void) setHyperlinkWithBodyText:(NSString *) bodyText hyperlinkKey:(NSString*) hyperlinkKey;
-(void) setAttributedTitle:(NSAttributedString*)title;
@end

NS_ASSUME_NONNULL_END
