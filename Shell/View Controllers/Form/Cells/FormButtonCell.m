//
//  RegistrationButtonCell.m
//  Shell
//
//  Created by Admin on 25/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "FormButtonCell.h"
@interface FormButtonCell ()

@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@end

@implementation FormButtonCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.topLabel setFont: FONT_B1];
    [self.title setFont: FONT_B1];
}

-(void) hideTopLabel: (BOOL) hidden
{
    self.topLabel.hidden = hidden;
}

-(void) setTitle: (NSString *) title //top label
{
    self.topLabel.text = title;
}

-(void) setSelectionTitle: (NSString *) selectionTitle
{
    self.title.text = selectionTitle;
}

-(void) setIconImage: (NSString *) imageName
{
    [self.imgView setImage: [UIImage imageNamed: imageName]];
}

-(NSString *) getJsonValue
{
    if ([self.title.text isKindOfClass: [NSNull class]])
        return @"";
    return self.title.text;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
