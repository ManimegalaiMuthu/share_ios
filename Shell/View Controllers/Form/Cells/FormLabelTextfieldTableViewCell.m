//
//  FormLabelTextfieldTableViewCell.m
//  Shell
//
//  Created by Ankita Chhikara on 26/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "FormLabelTextfieldTableViewCell.h"


@interface FormLabelTextfieldTableViewCell()
//@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblLeftTitle;
@property (weak, nonatomic) IBOutlet UITextField *textfield;
@end

@implementation FormLabelTextfieldTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.lblLeftTitle setFont: FONT_B1];
    [self.textfield setFont: FONT_B1];
//    [self.topLabel setFont: FONT_B1];
}
-(void) setSecureTextEntry: (BOOL)isSecureTextEntry
{
    self.textfield.secureTextEntry = isSecureTextEntry;
}
-(void) setKeyboard:(UIKeyboardType) keyboardType
{
    self.textfield.keyboardType = keyboardType;
}

-(void) setLeftTitle: (NSString *) leftTitle
{
    self.lblLeftTitle.text = leftTitle;
    [self.lblLeftTitle sizeToFit];
    [self.lblLeftTitle layoutIfNeeded];
}
-(void) setTextfieldText: (NSString *) textfieldString
{
    self.textfield.text = textfieldString;
}

//-(void) setTitle: (NSString *) title
//{
//    self.topLabel.text = title;
//}

-(void) setPlaceholder: (NSString *) title
{
    self.textfield.placeholder = title;
}
//-(void) hideTopLabel: (BOOL) hidden;
//{
//    self.topLabel.hidden = hidden;
//}

-(NSString *) getJsonValue
{
    if ([self.textfield.text isKindOfClass: [NSNull class]])
        return @"";
    return self.textfield.text;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void) setTextfieldDelegate:(id<UITextFieldDelegate>) delegate
{
    self.textfield.delegate = delegate;
}


@end
