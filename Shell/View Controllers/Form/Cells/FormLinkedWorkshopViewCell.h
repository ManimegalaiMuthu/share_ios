//
//  FormLinkedWorkshopViewCell.h
//  Shell
//
//  Created by Nach on 20/9/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FormBaseCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface FormLinkedWorkshopViewCell : FormBaseCell

@property NSMutableArray *displayArray;

-(void) setTitle: (NSString *) title;
-(void) setLinkedWorkshops:(NSMutableArray *)linkedArray;
-(void) setUserInteractionEnabled:(BOOL)userInteractionEnabled;

-(NSString *) getJsonValue;

@end

NS_ASSUME_NONNULL_END
