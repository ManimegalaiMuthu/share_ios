//
//  FormCTAButtonCell.h
//  Shell
//
//  Created by Ankita Chhikara on 27/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Constants.h"
#import "FormBaseCell.h"

@protocol FormCTACellDelegate
-(void) ctaPressedWithKey:(NSString *) key;
@end

@interface FormCTAButtonCell : FormBaseCell
@property (nonatomic) id<FormCTACellDelegate> delegate;
-(void) setButtonTitle:(NSString *) buttonTitle;
-(void) setCTADelegate:(id<FormCTACellDelegate>) delegate;
-(void) setCtaKey:(NSString *) ctaKey;
-(void) setColourWithTextColour:(UIColor *) textColour backgroundColour:(UIColor *) backgroundColour;

@end
