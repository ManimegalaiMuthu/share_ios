//
//  WebViewHelper.m
//  Shell
//
//  Created by Jeremy Lua on 9/9/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "WebViewHelper.h"
#import "LocalizationManager.h"
#import "Constants.h"
#import "WebserviceHelper.h"

@interface WebViewHelper ()

@property UIView *rootView; //Main VC for Progress indicator

@end

@implementation WebViewHelper

-(void) setWebViewWithParentView: (UIView *) parentView
              navigationDelegate: (id<WKNavigationDelegate>) delegate
                      urlAddress: (NSString *) urlAddress
{
    if ([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK])
    {
        [self setWebViewWithParentView: parentView
                    navigationDelegate: delegate
                            urlAddress: urlAddress
                               headers: [WebserviceHelper prepRequestHeader]];
    }
    else
    {
        [self setWebViewWithParentView: parentView
                    navigationDelegate: delegate
                            urlAddress: urlAddress
                               headers: [WebserviceHelper prepPostLoginHeader]];
    }
}

-(void) setWebViewWithParentView: (UIView *) parentView
              navigationDelegate: (id<WKNavigationDelegate>) delegate
                      urlAddress: (NSString *) urlAddress
                         headers: (NSDictionary *) headers
{
    WKWebViewConfiguration *webConfiguration = [[WKWebViewConfiguration alloc] init];
    [webConfiguration setDataDetectorTypes: WKDataDetectorTypeLink | WKDataDetectorTypePhoneNumber];
    [webConfiguration setWebsiteDataStore: WKWebsiteDataStore.nonPersistentDataStore];
    
    self.webView = [[WKWebView alloc] initWithFrame: CGRectMake(0, 0, parentView.frame.size.width, parentView.frame.size.height) configuration: webConfiguration];
    [self.webView setTranslatesAutoresizingMaskIntoConstraints: NO];
    
    self.webView.navigationDelegate = delegate; //webview delegate handled by this class
    [parentView addSubview: self.webView];
    
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url];
    
    [requestObj setAllHTTPHeaderFields: headers];
    
    [self.webView loadRequest:requestObj];
    self.webView.scrollView.maximumZoomScale = 1.0;
    self.webView.scrollView.minimumZoomScale = 1.0;
}

-(void) setWebViewWithParentView: (UIView *) parentView
              navigationDelegate: (id<WKNavigationDelegate>) delegate
                        htmlBody: (NSString *) htmlBodyString
{
    WKWebViewConfiguration *webConfiguration = [[WKWebViewConfiguration alloc] init];
    [webConfiguration setDataDetectorTypes: WKDataDetectorTypeLink | WKDataDetectorTypePhoneNumber];
    [webConfiguration setWebsiteDataStore: WKWebsiteDataStore.nonPersistentDataStore];
    
    self.webView = [[WKWebView alloc] initWithFrame: CGRectMake(0, 0, parentView.frame.size.width, parentView.frame.size.height) configuration: webConfiguration];
    [self.webView setTranslatesAutoresizingMaskIntoConstraints: NO];
    
    self.webView.navigationDelegate = delegate; //webview delegate handled by this class
    
    [parentView addSubview: self.webView];
    
    [self.webView loadHTMLString: htmlBodyString baseURL:nil];
}

-(void) reloadWebView: (NSString *) urlAddress
{
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url];
    
    [requestObj setAllHTTPHeaderFields: [WebserviceHelper prepPostLoginHeader]];
    
    [self.webView loadRequest:requestObj];
    self.webView.scrollView.maximumZoomScale = 1.0;
    self.webView.scrollView.minimumZoomScale = 1.0;
}

-(void) setWebConfiguration: (WKDataDetectorTypes) types
{
    [self.webView.configuration setDataDetectorTypes: types];
}

-(void) setHUDRootView:(UIView *)rootView
{
    self.rootView = rootView;
}

//Add these to the VC
//#pragma WKWebView delegate methods
//- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
//    [UpdateHUD addMBProgress: self.rootView withText:LOADING_HUD];
//}
//
//-(void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
//{
//    NSLog(@"failed to load navigation");
//    [UpdateHUD removeMBProgress: self.view];
//}
//- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
//    [UpdateHUD removeMBProgress: self.rootView];
//}
//
//- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
//
//    if(navigationAction.navigationType ==  WKNavigationTypeLinkActivated) {
//        if ([self.delegate respondsToSelector: @selector(helperLinkActivatedNavigationAction:navigationAction:)])
//            [self.delegate helperLinkActivatedNavigationAction: webView navigationAction:navigationAction];
//
//        decisionHandler(WKNavigationActionPolicyCancel);
//        return;
//    }
//
//    decisionHandler(WKNavigationActionPolicyAllow);
//    return;
//}
@end
