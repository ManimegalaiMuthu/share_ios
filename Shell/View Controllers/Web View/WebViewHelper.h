//
//  WebViewHelper.h
//  Shell
//
//  Created by Jeremy Lua on 9/9/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface WebViewHelper : NSObject
@property (strong, nonatomic) WKWebView *webView;

-(void) setWebViewWithParentView: (UIView *) parentView
              navigationDelegate: (id<WKNavigationDelegate>) delegate
                      urlAddress: (NSString *) urlAddress;


-(void) setWebViewWithParentView: (UIView *) parentView
              navigationDelegate: (id<WKNavigationDelegate>) delegate
                        htmlBody: (NSString *) htmlBodyString;

-(void) setWebViewWithParentView: (UIView *) parentView
              navigationDelegate: (id<WKNavigationDelegate>) delegate
                      urlAddress: (NSString *) urlAddress
                         headers: (NSDictionary *) headers;

-(void) reloadWebView: (NSString *) urlAddress;
-(void) setWebConfiguration: (WKDataDetectorTypes) types;
-(void) setHUDRootView:(UIView *)rootView;
@end

NS_ASSUME_NONNULL_END
