//
//  AddOrderWebViewController.m
//  Shell
//
//  Created by Admin on 28/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "AddOrderWebViewController.h"

@interface AddOrderWebViewController ()<UIWebViewDelegate, UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewToBottom;

@end

@implementation AddOrderWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self reloadWebview];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
//    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//    
//    if (GET_PROFILETYPE == kPROFILETYPE_DSR)
//        [tracker set:kGAIScreenName value:@"Pending Orders"];
//    else
//        [tracker set:kGAIScreenName value:@"Add Orders"];
//    
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)keyboardOnScreen:(NSNotification *)notification
{
    [super keyboardOnScreen: notification];
//    self.scrollViewToBottom.constant = self.keyboardFrame.size.height;
    self.scrollViewToBottom.constant = self.view.frame.size.height - self.keyboardFrame.origin.y;
}

-(void) reloadWebview
{
    NSString *urlAddress;
    if (GET_PROFILETYPE == kPROFILETYPE_DSR)
        urlAddress = [NSString stringWithFormat:@"%@%@%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_PENDINGORDER_URL];
    else
        urlAddress = [NSString stringWithFormat:@"%@%@%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_ADDORDER_URL];
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url];
    
    [requestObj setAllHTTPHeaderFields: [WebserviceHelper prepPostLoginHeader]];
    
    [self.webView loadRequest:requestObj];
    self.webView.scalesPageToFit = YES;
    self.webView.scrollView.delegate = self;
}
//
//- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
//    return nil;
//}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSMutableURLRequest *newRequest = [NSMutableURLRequest requestWithURL:request.URL];
        [newRequest setAllHTTPHeaderFields: [WebserviceHelper prepPostLoginHeader]];
        [webView performSelector:@selector(loadRequest:) withObject:newRequest afterDelay:0];
        return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
