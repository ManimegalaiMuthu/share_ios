//
//  ParticipatingWebViewController.h
//  Shell
//
//  Created by Admin on 28/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface ParticipatingWebViewController : BaseVC
-(void) reloadWebview;

@end
