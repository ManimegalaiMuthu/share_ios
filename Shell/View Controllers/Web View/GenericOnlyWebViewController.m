//
//  GenericOnlyWebViewController.m
//  Shell
//
//  Created by Nach on 31/12/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "GenericOnlyWebViewController.h"
#import <WebKit/WebKit.h>

@interface GenericOnlyWebViewController () <WKNavigationDelegate>

@property (weak, nonatomic) IBOutlet UIView *webViewContainer;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@end

@implementation GenericOnlyWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    if (kIsRightToLeft) {
        [self.btnBack setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.btnBack setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
    [Helper setNavigationBarTitle:self title:self.titleToBeDisplayed subtitle: @""];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:self.titleToBeDisplayed screenClass:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    #pragma mark - Adding WKWebView
     WebViewHelper *webViewHelper = [[WebViewHelper alloc] init];
    
    if (self.isHtmlString) {
      
      [webViewHelper setWebViewWithParentView:self.webViewContainer
                           navigationDelegate:self
                                     htmlBody:self.urlToBeDisplayed];
      
    } else {
      
      [webViewHelper setWebViewWithParentView: self.webViewContainer
                           navigationDelegate: self
                                   urlAddress: self.urlToBeDisplayed];
      
    }
    
    
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

#pragma WKWebView delegate methods
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    [UpdateHUD addMBProgress: webView withText:LOADING_HUD];
}

-(void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    NSLog(@"failed to load navigation");
    [UpdateHUD removeMBProgress: self.view];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [UpdateHUD removeMBProgress: webView];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
