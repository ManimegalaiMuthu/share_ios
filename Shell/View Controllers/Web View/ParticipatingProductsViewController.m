//
//  ParticipatingProductsViewController.m
//  Shell
//
//  Created by Admin on 28/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "ParticipatingProductsViewController.h"
#import "RDVTabBarItem.h"
#import "ParticipatingWebViewController.h"
#import "OtherWebViewController.h"
#import "ViewOrdersViewController.h"
#import "PendingOrdersViewController.h"

@interface ParticipatingProductsViewController ()
{
    ParticipatingWebViewController *participatingProductViewController;
    OtherWebViewController *otherProductViewController;
    
}

@end

@implementation ParticipatingProductsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupInterface];
    [UpdateHUD removeMBProgress: KEY_WINDOW];
}

- (void) setupInterface
{
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_PARTICIPATINGPRODUCT) subtitle: @"" size:15 subtitleSize:0]; //lokalise 31 jan

    participatingProductViewController  = [STORYBOARD_WEBVIEW instantiateViewControllerWithIdentifier: VIEW_PARTICIPATEPRODUCT];
    UINavigationController *participateNav = [[UINavigationController alloc] initWithRootViewController: participatingProductViewController];
    [participateNav setNavigationBarHidden: YES];
    [participatingProductViewController view];

    otherProductViewController  = [STORYBOARD_WEBVIEW instantiateViewControllerWithIdentifier: VIEW_OTHERPRODUCT];
    UINavigationController *otherProductNav = [[UINavigationController alloc] initWithRootViewController: otherProductViewController];
    [otherProductNav setNavigationBarHidden: YES];
    [otherProductNav view];
    
    
    NSArray *viewControllers = @[otherProductNav,
                            participateNav];
    NSArray *title = @[LOCALIZATION(C_TITLE_SHAREPRODUCT),  //lokalise 31 jan
                  LOCALIZATION(C_TITLE_ALLPRODUCT)];  //lokalise 31 jan
    
    [self setViewControllers:viewControllers];
    if (kIsRightToLeft) {
        title = title.reverseObjectEnumerator.allObjects.mutableCopy;
    }
    UIImage *selected = GET_ISADVANCE ? [UIImage imageNamed:@"tab-bg_blue"] : [UIImage imageNamed:@"tab-bg"];
    UIImage *unselected = [UIImage imageNamed:@"tab-bg_grey.png"];
    for (int i = 0; i < [self.tabBar.items count]; i++)
    {
        RDVTabBarItem *item = [self.tabBar.items objectAtIndex: i];
        [item setTitle: [title objectAtIndex: i]];
        [item setBackgroundColor: COLOUR_WHITE];
        [item setTitleUnselectedAttributes:FONT_H1 colour:COLOUR_VERYDARKGREY];
        [item setTitleSelectedAttributes:FONT_H1 colour:COLOUR_RED];
        
        [item setBackgroundSelectedImage: selected withUnselectedImage: unselected];
    }
    if (kIsRightToLeft) {
        [self tabBar:self.tabBar didSelectItemAtIndex:viewControllers.count-1];
    }
}

#pragma mark RDVTabBarDelegate
- (BOOL)tabBar:(RDVTabBar *)tabBar shouldSelectItemAtIndex:(NSInteger)index
{
    return ([tabBar selectedItem] != [[tabBar items] objectAtIndex: index]);
}

- (void)tabBar:(RDVTabBar *)tabBar didSelectItemAtIndex:(NSInteger)index
{
    //super method MUST BE PRESENT
    [super tabBar:tabBar didSelectItemAtIndex: index];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
