//
//  PrivacyPolicyViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 11/9/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "PrivacyPolicyViewController.h"
#import <WebKit/WebKit.h>

@interface PrivacyPolicyViewController () <WKNavigationDelegate>
@property (weak, nonatomic) IBOutlet UIView *webViewContainer;
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIView *agreeView;
@property (weak, nonatomic) IBOutlet UIButton *btnAgree;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *agreeViewHeight;
@end

@implementation PrivacyPolicyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_PRIVACY) subtitle: @""];  //lokalised
    
    if ([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK])  //pre_login requires back button
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    }
    else if (!self.navigationController.parentViewController)
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    }
    
//    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_TITLE_PRIVACY),  LOCALIZATION(C_SIDEMENU_BACK)];
    
    if (kIsRightToLeft) {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
#pragma mark - Adding WKWebView
    NSString *urlAddress = [NSString stringWithFormat:@"%@%@%@?c=%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_PRIVACY_URL, GET_COUNTRY_CODE];
    
    WebViewHelper *webViewHelper = [[WebViewHelper alloc] init];
    [webViewHelper setWebViewWithParentView: self.webViewContainer
                         navigationDelegate: self
                                 urlAddress: urlAddress];
//    [webViewHelper setWebConfiguration: WKDataDetectorTypeLink | WKDataDetectorTypePhoneNumber];  //default
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];

    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_PRIVACY) screenClass:nil];
}

- (IBAction)backPressed:(id)sender {
    //only available prelogin
    [self popSelf];
}

- (IBAction)agreePressed:(id)sender {
    [[WebServiceManager sharedInstance] acceptPrivacyPolicy: self];
}

-(void) showAgreeBtn
{
    self.agreeView.hidden = NO;
    self.agreeViewHeight.constant = 50;
    NSAttributedString *agreeAttrString = [[NSAttributedString alloc] initWithString: LOCALIZATION(C_ACCEPTTNC) attributes: @{NSForegroundColorAttributeName: COLOUR_WHITE,                                                                                                                                              NSFontAttributeName: FONT_BUTTON, }];          //lokalised 11 Feb
    [self.btnAgree setAttributedTitle: agreeAttrString forState: UIControlStateNormal];
    [self.btnAgree.titleLabel setTextAlignment: NSTextAlignmentCenter];
    
    [self.btnAgree.titleLabel setFont: FONT_BUTTON];
    [self.btnAgree setBackgroundColor: COLOUR_RED];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: [UIView new]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma WKWebView delegate methods
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    [UpdateHUD addMBProgress: webView withText:LOADING_HUD];
}

-(void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    NSLog(@"failed to load navigation");
    [UpdateHUD removeMBProgress: self.view];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [UpdateHUD removeMBProgress: webView];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    if(navigationAction.navigationType ==  WKNavigationTypeLinkActivated) {
        [[UIApplication sharedApplication] openURL: navigationAction.request.URL];
        
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
    return;
}

#pragma mark - WebService Delegate
-(void)processCompleted:(WebServiceResponse *)response
{
    if ([response webserviceCall] == kWEBSERVICE_ACCEPTPRIVACY)
        [self popSelf]; //if changepassword required, will be underneath TNC
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
