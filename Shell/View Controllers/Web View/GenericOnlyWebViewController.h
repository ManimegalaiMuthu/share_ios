//
//  GenericOnlyWebViewController.h
//  Shell
//
//  Created by Nach on 31/12/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface GenericOnlyWebViewController : BaseVC

@property NSString *urlToBeDisplayed;
@property NSString *titleToBeDisplayed;
@property BOOL *isHtmlString;

@end

NS_ASSUME_NONNULL_END
