//
//  PrivacyPolicyViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 11/9/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface PrivacyPolicyViewController : BaseVC

-(void) showAgreeBtn;

@end
