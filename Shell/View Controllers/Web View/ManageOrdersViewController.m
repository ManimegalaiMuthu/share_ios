//
//  ManageOrdersViewController.m
//  Shell
//
//  Created by Admin on 28/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//
#import "RDVTabBarItem.h"
#import "ManageOrdersViewController.h"
#import "AddOrderWebViewController.h"
#import "ViewOrderWebViewController.h"

#import "AddOrdersViewController.h"
#import "ViewOrdersViewController.h"
#import "PendingOrdersViewController.h"

enum kManageOrder_Type
{
    kManageOrder_MYORDERS,
    kManageOrder_ADDNEW,
};

@interface ManageOrdersViewController ()
{

    AddOrdersViewController *addOrderViewController;
    ViewOrdersViewController *viewOrderViewController;
    PendingOrdersViewController *pendingOrderViewController;
    
}
@property (strong, nonatomic) IBOutlet UIButton *btnCart;

@end

@implementation ManageOrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupInterface];
    [UpdateHUD removeMBProgress: KEY_WINDOW];

    //new code
    if (GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER){
        if(@available(iOS 11, *)){
            [self.btnCart.widthAnchor constraintEqualToConstant: 25.0].active = YES;
            [self.btnCart.heightAnchor constraintEqualToConstant: 25.0].active = YES;
        }
        UIBarButtonItem *cartIcon = [[UIBarButtonItem alloc] initWithCustomView: self.btnCart];
        self.navigationItem.rightBarButtonItem = cartIcon;
    }
}

- (IBAction)cartBtnPressed:(id)sender {
    [mSession pushMyCartView:self];
}

- (void) setupInterface
{
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_MANAGEORDER) subtitle: @""];     //lokalise 21 Jan
    
    viewOrderViewController  = [STORYBOARD_MANAGEORDERS instantiateViewControllerWithIdentifier: VIEW_VIEWORDER];
    UINavigationController *viewOrderNav = [[UINavigationController alloc] initWithRootViewController: viewOrderViewController];
    [viewOrderNav setNavigationBarHidden: YES];
    [viewOrderViewController view];

    addOrderViewController  = [STORYBOARD_MANAGEORDERS instantiateViewControllerWithIdentifier: VIEW_ADDORDER];
    UINavigationController *addNewOrderNav = [[UINavigationController alloc] initWithRootViewController: addOrderViewController];
    [addNewOrderNav setNavigationBarHidden: YES];
    [addNewOrderNav view];
    
    //same VC but different data.
    //selecttab function have been tweaked locally for this case
    pendingOrderViewController  = [STORYBOARD_MANAGEORDERS instantiateViewControllerWithIdentifier: VIEW_PENDINGORDERSVIEW];
    UINavigationController *pendingNav = [[UINavigationController alloc] initWithRootViewController: pendingOrderViewController];
    [pendingNav setNavigationBarHidden: YES];
    [pendingNav view];

    //end new code
    NSArray *title;
    NSArray * viewControllers;
    if (GET_PROFILETYPE == kPROFILETYPE_DSR || GET_PROFILETYPE == kPROFILETYPE_DMR)
    {
        title = @[LOCALIZATION(C_MANAGEORDER_PENDING),      //lokalise 21 Jan
                  LOCALIZATION(C_TITLE_ALLORDERS)];         //lokalise 21 Jan
//        [self setViewControllers: @[addNewOrderNav,
//                                    viewOrderNav]];
        //new code
        viewControllers = @[pendingNav,
                            viewOrderNav];
    }
    else
    {
        title = @[LOCALIZATION(C_ORDER_MYORDER),                //lokalise 21 Jan
                  LOCALIZATION(C_MANAGEORDER_ADDNEWORDER)];     //lokalise 21 Jan
        viewControllers = @[viewOrderNav,
                            addNewOrderNav];
    }
    if (kIsRightToLeft) {
        title = title.reverseObjectEnumerator.allObjects.mutableCopy;
//        viewControllers = viewControllers.reverseObjectEnumerator.allObjects.mutableCopy;
    }
    [self setViewControllers: viewControllers];
    
    
    UIImage *selected = GET_ISADVANCE ? [UIImage imageNamed:@"tab-bg_blue"] : [UIImage imageNamed:@"tab-bg"];
    UIImage *unselected = [UIImage imageNamed:@"tab-bg_grey.png"];
    
    for (int i = 0; i < [self.tabBar.items count]; i++)
    {
        RDVTabBarItem *item = [self.tabBar.items objectAtIndex: i];
        [item setTitle: [title objectAtIndex: i]];
        
        
        [item setBackgroundColor: COLOUR_WHITE];
        [item setTitleUnselectedAttributes:FONT_H1 colour:COLOUR_VERYDARKGREY];
        [item setTitleSelectedAttributes:FONT_H1 colour:COLOUR_RED];
        
        [item setBackgroundSelectedImage: selected withUnselectedImage: unselected];
        
    
    }
    if (kIsRightToLeft) {
        [self tabBar:self.tabBar didSelectItemAtIndex:viewControllers.count-1];
    }
}

#pragma mark RDVTabBarDelegate
- (BOOL)tabBar:(RDVTabBar *)tabBar shouldSelectItemAtIndex:(NSInteger)index
{
    for (RDVTabBarItem *item in [tabBar items])
    {
        
    }
    return ([tabBar selectedItem] != [[tabBar items] objectAtIndex: index]);
}

-(void)goToAddNewPage
{
    [self tabBar:self.tabBar didSelectItemAtIndex:1];
}

- (void)tabBar:(RDVTabBar *)tabBar didSelectItemAtIndex:(NSInteger)index
{
    //super method MUST BE PRESENT
    [super tabBar:tabBar didSelectItemAtIndex: index];
    
    
    switch (index)
    {
        case kManageOrder_ADDNEW:{

        }
            break;
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
