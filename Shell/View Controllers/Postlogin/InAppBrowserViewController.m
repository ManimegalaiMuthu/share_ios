//
//  InAppBrowserViewController.m
//  Shell
//
//  Created by Jeremy Lua on 28/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "InAppBrowserViewController.h"
#import <WebKit/WebKit.h>

@interface InAppBrowserViewController () <WebServiceManagerDelegate, WKNavigationDelegate>
@property (weak, nonatomic) IBOutlet UIView *webViewContainer;
@end

@implementation InAppBrowserViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setNavBackBtn];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
#pragma mark - Adding WKWebView
    WebViewHelper *webViewHelper = [[WebViewHelper alloc] init];
    [webViewHelper setWebViewWithParentView: self.view
                         navigationDelegate: self
                                 urlAddress: self.urlAddressString];
    //    [webViewHelper setWebConfiguration: WKDataDetectorTypeLink | WKDataDetectorTypePhoneNumber];  //default
}


@end
