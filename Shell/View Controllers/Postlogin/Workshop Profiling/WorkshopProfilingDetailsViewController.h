//
//  WorkshopProfilingDetailsViewController.h
//  Shell
//
//  Created by Jeremy Lua on 12/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface WorkshopProfilingDetailsViewController : BaseVC
@property NSDictionary *workshopDetailsData;
@end
