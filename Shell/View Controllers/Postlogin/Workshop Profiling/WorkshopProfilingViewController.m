//
//  WorkshopProfilingViewController.m
//  Shell
//
//  Created by Jeremy Lua on 12/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "WorkshopProfilingViewController.h"

enum kManageWorkshop_Range
{
    kManageWorkshop_All,
    kManageWorkshop_AtoF,
    kManageWorkshop_GtoL,
    kManageWorkshop_MtoZ,
};

@interface WorkshopProfilingViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, WebServiceManagerDelegate, CommonListDelegate>

@property IBOutletCollection(UIButton) NSArray *buttonArray;
@property (weak, nonatomic) IBOutlet UIButton *btnAll;


@property NSMutableArray *workshopListArray;

@property (weak, nonatomic) IBOutlet TableViewWithEmptyView *listTableView;

@property (weak, nonatomic) IBOutlet UITextField *txtKeyword;

@property NSInteger selectedTabIndex;
@property NSInteger selectedRangeIndex;
@end

@implementation WorkshopProfilingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_MANAGEWORKSHOP) subtitle: @""];

    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_APPROVEDWORKSHOPPROFILING) subtitle: @""];   //lokalised 11 Feb
    
    [self setupInterface];
    
    [self rangeBtnPressed: [self.buttonArray objectAtIndex: kManageWorkshop_All]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_APPROVEDWORKSHOPPROFILING) screenClass:nil];
}

- (void) setupInterface
{
    self.workshopListArray = [[NSMutableArray alloc] init];
    
    //tableview setup
    PlaceholderView *vwPlaceholder = [[PlaceholderView alloc]initWithView:self.listTableView];
    [vwPlaceholder setBackgroundColor:[UIColor clearColor]];
    self.listTableView.emptyView = vwPlaceholder;
    
    // Initialize the refresh control.
    [self.listTableView.customRefreshControl addTarget:self
                                                action:@selector(refreshTable)
                                      forControlEvents:UIControlEventValueChanged];
    
    //empty view to eliminate extra separators
    self.listTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    for (int i = 0; i < [self.buttonArray count]; i++)
    {
        UIButton *btn = [self.buttonArray objectAtIndex: i];
        btn.tag = i;
        [btn.titleLabel setFont: FONT_H1];
        [btn setTitleColor: COLOUR_VERYDARKGREY forState: UIControlStateNormal];
        
        [btn setTitleColor: COLOUR_WHITE forState: UIControlStateSelected];
        
        switch (i)
        {
            case kManageWorkshop_AtoF:
                [btn setTitle: LOCALIZATION(C_KEYWORD_SECONDRANGE) forState:UIControlStateNormal];     //lokalised 11 Feb
                break;
            case kManageWorkshop_GtoL:
                [btn setTitle: LOCALIZATION(C_KEYWORD_THIRDRANGE) forState:UIControlStateNormal];      //lokalised 11 Feb
                break;
            case kManageWorkshop_MtoZ:
                [btn setTitle: LOCALIZATION(C_KEYWORD_FOURTHRANGE) forState:UIControlStateNormal];     //lokalised 11 Feb
                break;
            default:
                break;
        }
    }
    [self.btnAll setTitle: LOCALIZATION(C_KEYWORD_ALL)  forState: UIControlStateNormal];       //lokalised 11 Feb
    self.txtKeyword.font = FONT_B1;
    self.txtKeyword.placeholder = LOCALIZATION(C_KEYWORD_SEARCH);      //lokalised 11 Feb
    self.txtKeyword.delegate = self;
    
}

-(void) refreshTable
{
    for (UIButton *btn in self.buttonArray)
    {
        if ([btn isSelected])
        {
            [self rangeBtnPressed: [self.buttonArray objectAtIndex: btn.tag]];
        }
    }
}

-(void) selectedTab: (NSInteger) index
{
    [self.workshopListArray removeAllObjects];
    self.selectedTabIndex = index;
    [self rangeBtnPressed: [self.buttonArray objectAtIndex: kManageWorkshop_All]];
}

- (IBAction)rangeBtnPressed:(UIButton *)sender {
    
    for (UIButton *btn in self.buttonArray)
    {
        if (btn != sender)
        {
            [btn setSelected: NO];
            [btn setBackgroundColor: COLOUR_WHITE];
        }
        else
        {
            [btn setSelected: YES];
            [btn setBackgroundColor: COLOUR_RED];
        }
    }
    [self searchList: sender.tag];
    self.selectedRangeIndex = sender.tag;
}

-(void) searchList: (NSInteger) index
{
    switch (index)
    {
        case kManageWorkshop_All:
            [[WebServiceManager sharedInstance] fetchApprovedWorkshopList: self.txtKeyword.text searchRange:@"" vc: self];
            break;
        case kManageWorkshop_AtoF:
            [[WebServiceManager sharedInstance] fetchApprovedWorkshopList: self.txtKeyword.text searchRange:LOCALIZATION(C_KEYWORD_SECONDRANGE) vc: self];     //lokalised 11 Feb
            //            [[WebServiceManager sharedInstance] fetchWorkshopList: self.txtKeyword.text searchRange:@"A-F" vc: self];
            break;
        case kManageWorkshop_GtoL:
            [[WebServiceManager sharedInstance] fetchApprovedWorkshopList: self.txtKeyword.text searchRange:LOCALIZATION(C_KEYWORD_THIRDRANGE) vc: self];      //lokalised 11 Feb
            //            [[WebServiceManager sharedInstance] fetchWorkshopList: self.txtKeyword.text searchRange:@"G-L" vc: self];
            break;
        case kManageWorkshop_MtoZ:
            [[WebServiceManager sharedInstance] fetchApprovedWorkshopList: self.txtKeyword.text searchRange:LOCALIZATION(C_KEYWORD_FOURTHRANGE) vc: self];     //lokalised 11 Feb
            //            [[WebServiceManager sharedInstance] fetchWorkshopList: self.txtKeyword.text searchRange:@"M-Z" vc: self];
        default:
            break;
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self searchList: self.selectedRangeIndex];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.workshopListArray count];
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"PendingCell"];
    NSDictionary *cellData = [self.workshopListArray objectAtIndex: indexPath.row];
    
    UILabel *lblName = [cell viewWithTag: 1];
    lblName.text = [cellData objectForKey: @"CompanyName"];
    lblName.font = FONT_B1;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cellData = [self.workshopListArray objectAtIndex: indexPath.row];
    
    [[WebServiceManager sharedInstance] fetchTrade:[cellData objectForKey: @"TradeID"] vc:self];
}


-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_FETCH_APPROVEDWORKSHOP:
        {
            [self.listTableView.customRefreshControl performSelectorOnMainThread: @selector(endRefreshing) withObject: self.listTableView.customRefreshControl waitUntilDone:NO];
            
            NSDictionary *data = [response getGenericResponse];
            
            //reset array
            if ([self.workshopListArray isEqual:[NSNull null]])
                self.workshopListArray = [[NSMutableArray alloc] init];
            
            self.workshopListArray = [data objectForKey: @"ApprovedWorkShop"];
            
            [self.listTableView reloadData];
        }
            break;
            
        case kWEBSERVICE_FETCHTRADE:
        {
            [mSession pushWorkshopProfilingDetails: self workshopDetailsData: [response getGenericResponse]];
        }
            break;
        default:
            break;
            
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress: KEY_WINDOW];
    
    [self.listTableView.customRefreshControl performSelectorOnMainThread: @selector(endRefreshing) withObject: self.listTableView.customRefreshControl waitUntilDone:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
