//
//  WorkshopProfilingDetailsViewController.m
//  Shell
//
//  Created by Jeremy Lua on 12/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <MapKit/MapKit.h>

#import "WorkshopProfilingDetailsViewController.h"
#import "RegistrationHeaderTableViewCell.h"
#import "RegistrationFormTextfieldCell.h"
#import "RegistrationMobileNumberCell.h"
#import "RegistrationTextfieldWithButtonCell.h"

#import "RearrangeTableViewCell.h"
#import "CheckboxTableViewCell.h"
#import "BooleanQuestionTableViewCell.h"
#import "LabelWithTextBoxTableViewCell.h"
#import "BoxedTextViewTableViewCell.h"

#define CONTACTCELL_HEIGHT 72
#define CONTACTCELLDROPDOWN_HEIGHT 85
#define WORKSHOPTEXTVIEW_HEIGHT 200

enum kContactDetails_TYPE
{
    kContactDetails_CONTACTDETAILSHEADER,
    
    kContactDetails_WORKSHOPNAME,
    kContactDetails_STREET,
    kContactDetails_TOWN,
    kContactDetails_POSTALCODE,
    kContactDetails_LOCATION,
    
    kContactDetails_MOBNUM,
    kContactDetails_EMAIL,
    
    kContactDetails_WEBSITEURL,
    kContactDetails_CONTACTNAME,
    kContactDetails_JOBTITLE,
};

#define kCompanyName     @"CompanyName"
#define kStreet          @"Street"
#define kTown            @"Town"
#define kPostalCode      @"PostalCode"
#define kLongitude       @"Longitude"
#define kLatitude        @"Latitude"
#define kMobileNumber    @"MobileNumber"
#define kEmailAddress    @"EmailAddress"
#define kContactName     @"ContactName"
#define kWebsiteURL      @"URL"
#define kJobTitle       @"JobTitle"
#define kTradeID        @"TradeID"

#define kWorkshopProfilingLocation       @"Location"

enum kWorkshopDetails_TYPE
{
    kWorkshopDetails_WORKSHOPDETAILSHEADER,
    
    kWorkshopDetails_SITESOPERATE,
    kWorkshopDetails_GARAGENETWORK,
    kWorkshopDetails_WHICHNETWORK,
//    kWorkshopDetails_SERVICESOFFERED,
    kWorkshopDetails_RAMPS,
    kWorkshopDetails_TOTALCONSUMPTION,
    kWorkshopDetails_MAINBRAND,
    kWorkshopDetails_MOSTIMPORTANT,
    kWorkshopDetails_WHEREBUYINGENGINEOIL,
    kWorkshopDetails_OILCHANGES,
//    kWorkshopDetails_OILTYPESUSE,
//    kWorkshopDetails_MAINGRADESTOCK,
    kWorkshopDetails_PACKSIZE,
//    kWorkshopDetails_IMPORTANCE,
    
//    kWorkshopDetails_WORKSHOPDETAILS,
    kWorkshopDetails_ORDERQUANTITY,
    kWorkshopDetails_ORDERDATE,
};

#define kHowManyGarages               @"HowManyGarages"
#define kAreYouPartGarage             @"AreYouPartGarage"
#define kWhichNetwork                 @"WhichNetwork"
//#define kServices                     @"Services"
//#define kServicesOther                @"ServicesOther"
#define kNumberofRamps                @"NumberofRamps"
#define kAnualConsmption              @"AnualConsmption"
#define kMainBrand                    @"MainBrand"
#define kImportantBuyingEngineOil     @"ImportantBuyingEngineOil"
#define kWhereBuyingEngineOil         @"WhereBuyingEngineOil"
#define kWeeklyNumber 	              @"WeeklyNumber"
#define kMonthlyNumber                @"MonthlyNumber"
//#define kDifferentEngineOil           @"DifferentEngineOil"
//#define kMainGrades                   @"MainGrades"
#define kPackSize                     @"PackSize"
//#define kProfileRankList              @"ProfileRankList"
#define kOrderDate                    @"OrderDate"
#define kOrderQuantity                @"OrderQuantity"


@interface WorkshopProfilingDetailsViewController ()< UITextFieldDelegate, WebServiceManagerDelegate, CommonListDelegate, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, UITextViewDelegate, BooleanQuestionDelegate, CheckboxTableViewCellDelegate, NativeDatePickerDelegate>


@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *mainScrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewToBottom;

@property CLLocationManager *locationManager;
@property CGFloat contactLatitude;
@property CGFloat contactLongitude;

@property BOOL isContactDetailsOpen;

@property NSDictionary *contactDetailsWebData;
@property NSDictionary *workshopDetailsWebData;
//@property NSMutableArray *profileRankListArray;

@property NSMutableArray *contactDetailsArray; //ui only
@property NSMutableDictionary *contactDetailsDict;  //key as json key, value = cell. retrieve value by using getJson method
@property (weak, nonatomic) IBOutlet UITableView *contactDetailsTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contactDetailsHeight;

@property BOOL isWorkshopDetailsOpen;
@property NSMutableArray *workshopDetailsArray; //ui only
@property NSMutableDictionary *workshopDetailsDict;  //key as json key, value = cell. retrieve value by using getJson method
@property (weak, nonatomic) IBOutlet UITableView *workshopDetailsTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *workshopDetailsHeight;
@end

@implementation WorkshopProfilingDetailsViewController
@synthesize listView, nativeDatePickerView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_APPROVEDWORKSHOPPROFILING) subtitle: @""];      //lokalised 11 Feb
    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_TITLE_WORKINGHOURS),  LOCALIZATION(C_TITLE_APPROVEDWORKSHOPPROFILING)];     //lokalised 11 Feb
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [[IQKeyboardManager sharedManager] setEnable: NO];
    
    
    self.contactDetailsDict = [[NSMutableDictionary alloc] init];
    self.workshopDetailsDict = [[NSMutableDictionary alloc] init];
    
    [self setupInterface];
    

    [[WebServiceManager sharedInstance] fetchWorkshopProfilingDetailsWithTradeID: [[self.workshopDetailsData objectForKey: @"REG_Member"] objectForKey: @"TradeID"] vc: self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_APPROVEDWORKSHOPPROFILING) screenClass:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
    //[self showPopUp:YES];
}

-(void) setupInterface
{
    [self.btnUpdate.titleLabel setFont: FONT_BUTTON];
    
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(@"Select location") list: [mSession getSalutations] selectionType:ListSelectionTypeSingle previouslySelected: nil];   //later lokalise
    listView.delegate = self;
}

-(void) setupContactDetailsForm
{
    [self.contactDetailsTableView registerNib:[UINib nibWithNibName:@"RegistrationFormTextfieldCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormTextfieldCell"];
    [self.contactDetailsTableView registerNib:[UINib nibWithNibName:@"RegistrationMobileNumberCell" bundle:nil] forCellReuseIdentifier:@"RegistrationMobileNumberCell"];
    [self.contactDetailsTableView registerNib:[UINib nibWithNibName:@"RegistrationTextfieldWithButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextfieldWithButtonCell"];
    
    self.contactLatitude = ISNULLCLASS([self.contactDetailsWebData objectForKey: kLatitude]) ? 0 : [[self.contactDetailsWebData objectForKey: kLatitude] doubleValue];
    
    self.contactLongitude = ISNULLCLASS([self.contactDetailsWebData objectForKey: kLongitude]) ? 0 : [[self.contactDetailsWebData objectForKey: kLongitude] doubleValue];
    
    self.contactDetailsArray = [[NSMutableArray alloc] initWithArray:
                                @[
                                  @{@"Title": LOCALIZATION(@"CONTACT DETAILS"), //later lokalise
//                                    @{@"Title": LOCALIZATION(@"CONTACT DETAILS"),
                                    @"Type": @(kREGISTRATION_HEADER),
                                    },
//                                  @{@"Title": LOCALIZATION(C_PROFILE_COMPANYNAME),
                                  @{@"Title": LOCALIZATION(@"Workshop Name"),   //later lokalise
                                    @"Type": @(kREGISTRATION_TEXTFIELD),
                                    @"Key": kCompanyName,
                                    @"Value": ISNULLCLASS([self.contactDetailsWebData objectForKey: kCompanyName])? @"" : [self.contactDetailsWebData objectForKey: kCompanyName],
                                    }, //kContactDetails_WORKSHOPNAME
                                  @{@"Title": LOCALIZATION(@"Street"),  //later lokalise
                                    @"Type": @(kREGISTRATION_TEXTFIELD),
                                    @"Key": kStreet,
                                    @"Value":  ISNULLCLASS([self.contactDetailsWebData objectForKey: kStreet])? @"" : [self.contactDetailsWebData objectForKey: kStreet],
                                    }, //kContactDetails_STREET
                                  @{@"Title": LOCALIZATION(C_PROFILE_TOWN), //lokalised 11 Feb
                                    @"Type": @(kREGISTRATION_TEXTFIELD),
                                    @"Key": kTown,
                                    @"Value":  ISNULLCLASS([self.contactDetailsWebData objectForKey: kTown])? @"" : [self.contactDetailsWebData objectForKey: kTown],
                                    }, //kContactDetails_TOWN
                                  @{@"Title": LOCALIZATION(C_PROFILE_POSTALCODE),   //lokalised 11 Feb
                                    @"Type": @(kREGISTRATION_TEXTFIELD),
                                    @"Key": kPostalCode,
                                    @"Keyboard": @(UIKeyboardTypeNumberPad),
                                    @"Value":  ISNULLCLASS([self.contactDetailsWebData objectForKey: kPostalCode])? @"" : [self.contactDetailsWebData objectForKey: kPostalCode],
                                    }, //kContactDetails_POSTALCODE
                                  @{@"Title": LOCALIZATION(C_PROFILE_LOCATION), //lokalised 11 Feb
                                    @"Type": @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                                    @"Key": kWorkshopProfilingLocation,
                                    @"Icon": @"icon-location",
                                    @"Value":
                                        [NSString stringWithFormat: @"%f, %f", self.contactLatitude, self.contactLongitude],
                                    }, //kContactDetails_LOCATION
                                  @{@"Title": LOCALIZATION(C_PROFILE_MOBILENUM),    //lokalised 11 Feb
                                    @"Type": @(kREGISTRATION_MOBNUM),
                                    @"Key": kMobileNumber,
                                    @"TextfieldEnabled": @(YES),
//                                    @"CountryCode": [mSession convertCountryCodeToCountryName: self.member.countryCode],
                                    @"CountryCode": [mSession convertCountryCodeToCountryName: GET_COUNTRY_CODE],
                                    @"MobileNumber":  ISNULLCLASS([self.contactDetailsWebData objectForKey: kMobileNumber])? @"" : [self.contactDetailsWebData objectForKey: kMobileNumber],
                                    },   //kContactDetails_MOBNUM
                                  @{@"Title": LOCALIZATION(@"Email"),   //later lokalise
                                    @"Type": @(kREGISTRATION_TEXTFIELD),
                                    @"Key": kEmailAddress,
                                    @"Keyboard": @(UIKeyboardTypeEmailAddress),
                                    @"Value":  ISNULLCLASS([self.contactDetailsWebData objectForKey: kEmailAddress])? @"" : [self.contactDetailsWebData objectForKey: kEmailAddress],
                                    }, //kContactDetails_EMAIL
#pragma mark - new fields
                                  @{@"Title": LOCALIZATION(@"Website URL"), //later lokalise
                                    @"Type": @(kREGISTRATION_TEXTFIELD),
                                    @"Key": kWebsiteURL,
                                    @"Keyboard": @(UIKeyboardTypeURL),
                                    @"Value":  ISNULLCLASS([self.contactDetailsWebData objectForKey: kWebsiteURL])? @"" : [self.contactDetailsWebData objectForKey: kWebsiteURL],
                                    }, //kContactDetails_WEBSITEURL
                                  @{@"Title": LOCALIZATION(@"Contact Name"),    //later lokalise
                                    @"Type": @(kREGISTRATION_TEXTFIELD),
                                    @"Key": kContactName,
                                    @"Value":  ISNULLCLASS([self.contactDetailsWebData objectForKey: kContactName])? @"" : [self.contactDetailsWebData objectForKey: kContactName],
                                    }, //kContactDetails_CONTACTNAME
                                  @{@"Title": LOCALIZATION(@"Job Title"),   //later lokalise
                                    @"Type": @(kREGISTRATION_TEXTFIELD),
                                    @"Key": kJobTitle,
                                    @"Value":  ISNULLCLASS([self.contactDetailsWebData objectForKey: kJobTitle])? @"" : [self.contactDetailsWebData objectForKey: kJobTitle],
                                    }, //kContactDetails_JOBTITLE
                                  ]];

    self.contactDetailsHeight.constant = CONTACTCELL_HEIGHT;
    
    [self.contactDetailsTableView reloadData];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    //most recent location update is at the end of the array.
    CLLocation *location = [locations lastObject];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:6];
    
    NSString *newLatitude   = [formatter stringFromNumber: @(location.coordinate.latitude)];
    NSString *newLongitude  = [formatter stringFromNumber: @(location.coordinate.longitude)];
    
    RegistrationTextfieldWithButtonCell *cell = [self.contactDetailsDict objectForKey: kWorkshopProfilingLocation];
    [cell setTextfieldText: [NSString stringWithFormat: @"%@, %@", newLatitude, newLongitude]];
    
    self.contactLatitude = newLatitude.doubleValue;
    self.contactLongitude = newLongitude.doubleValue;
    
    [self.locationManager stopUpdatingLocation];
    [UpdateHUD removeMBProgress: self];
}

-(void) setupWorkshopDetailsForm
{
    [self.workshopDetailsTableView registerNib:[UINib nibWithNibName:@"RegistrationFormTextfieldCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormTextfieldCell"];
    [self.workshopDetailsTableView registerNib:[UINib nibWithNibName:@"RegistrationLabelTextfieldTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationLabelTextfieldTableViewCell"];
    [self.workshopDetailsTableView registerNib:[UINib nibWithNibName:@"RegistrationFormDropdownCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormDropdownCell"];
    
    
    [self.workshopDetailsTableView registerNib:[UINib nibWithNibName:@"BooleanQuestionTableViewCell" bundle:nil] forCellReuseIdentifier:@"BooleanQuestionTableViewCell"];
    [self.workshopDetailsTableView registerNib:[UINib nibWithNibName:@"CheckboxTableViewCell" bundle:nil] forCellReuseIdentifier:@"CheckboxTableViewCell"];
    [self.workshopDetailsTableView registerNib:[UINib nibWithNibName:@"RearrangeTableViewCell" bundle:nil] forCellReuseIdentifier:@"RearrangeTableViewCell"];
    [self.workshopDetailsTableView registerNib:[UINib nibWithNibName:@"LabelWithTextBoxTableViewCell" bundle:nil] forCellReuseIdentifier:@"LabelWithTextBoxTableViewCell"];
    [self.workshopDetailsTableView registerNib:[UINib nibWithNibName:@"BoxedTextViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"BoxedTextViewTableViewCell"];
    
    
    
    
    self.workshopDetailsArray = [[NSMutableArray alloc] initWithArray:
                                @[
                                  @{@"Title": LOCALIZATION(C_TITLE_WORKSHOPDETAIL),    //lokalised
                                    @"Type": @(kREGISTRATION_HEADER),
                                    },
                                  
                                  @{@"Title": LOCALIZATION(@"How many garages/sites do they operate?"), //later lokalise
//                                    @"Placeholder": LOCALIZATION(C_PROFILE_ANSWER),
                                    @"Type": @(kPROFILING_LABELWITHTEXTBOX),
                                    @"Key": kHowManyGarages,
                                    @"Keyboard": @(UIKeyboardTypeNumberPad),
                                    @"Value":  ISNULLCLASS([self.workshopDetailsWebData objectForKey: kHowManyGarages])? @"" : [self.workshopDetailsWebData objectForKey: kHowManyGarages],
                                    }, //    kWorkshopDetails_SITESOPERATE,
                                  @{@"Title": LOCALIZATION(@"Are you part of a garage network?"),   //later lokalise
                                    @"Type": @(kPROFILING_BOOLEANQUESTION),
                                    @"Key": kAreYouPartGarage,
                                    @"Value":  ISNULLCLASS([self.workshopDetailsWebData objectForKey: kAreYouPartGarage])? @"" : [self.workshopDetailsWebData objectForKey: kAreYouPartGarage],
                                    }, //    kWorkshopDetails_GARAGENETWORK,
                                  @{@"Title": LOCALIZATION(@"If yes, which network"),   //later lokalise
                                    @"Type": @(kREGISTRATION_TEXTFIELD),
                                    @"Placeholder": LOCALIZATION(@"Which network"), //later lokalise
                                    @"Key": kWhichNetwork,
                                    @"NetworkToggle": ISNULLCLASS([self.workshopDetailsWebData objectForKey: kAreYouPartGarage])? @"" : [self.workshopDetailsWebData objectForKey: kAreYouPartGarage],
                                    @"Value":  ISNULLCLASS([self.workshopDetailsWebData objectForKey: kWhichNetwork])? @"" : [self.workshopDetailsWebData objectForKey: kWhichNetwork],
                                    }, //    kWorkshopDetails_WHICHNETWORK,
//                                  @{@"Title": LOCALIZATION(@"What Services do you offer?"),
//                                    @"Type": @(kPROFILING_CHECKBOX),
//                                    @"Key": kServices,
//                                    @"Other": @(YES),
//                                    @"OtherValue": ISNULLCLASS([self.workshopDetailsWebData objectForKey: kServicesOther])? @"" :[self.workshopDetailsWebData objectForKey: kServicesOther],
//                                    @"Value":  ISNULLCLASS([self.workshopDetailsWebData objectForKey: kServices])? @"" : [self.workshopDetailsWebData objectForKey: kServices],
//                                    }, //    kWorkshopDetails_SERVICESOFFERED,
                                  @{@"Title": LOCALIZATION(@"How many ramps does your garage have?"),   //later lokalise
                                    //                                    @"Placeholder": LOCALIZATION(C_PROFILE_ANSWER),
                                    @"Type": @(kPROFILING_LABELWITHTEXTBOX),
                                    @"Keyboard": @(UIKeyboardTypeNumberPad),
                                    @"Key": kNumberofRamps,
                                    @"Value":  ISNULLCLASS([self.workshopDetailsWebData objectForKey: kNumberofRamps])? @"" : [self.workshopDetailsWebData objectForKey: kNumberofRamps],
                                    }, //    kWorkshopDetails_RAMPS,
                                  @{@"Title": LOCALIZATION(@"Estimated total annual consumption"),  //later lokalise
                                    @"Type": @(kREGISTRATION_TEXTFIELD),
                                    @"Keyboard": @(UIKeyboardTypeDecimalPad),
                                    @"Placeholder": LOCALIZATION(@"Estimated Amount"),  //later lokalise
                                    @"Key": kAnualConsmption,
                                    @"Value":  ISNULLCLASS([self.workshopDetailsWebData objectForKey: kAnualConsmption])? @"" : [self.workshopDetailsWebData objectForKey: kAnualConsmption],
                                    }, //    kWorkshopDetails_TOTALCONSUMPTION,
                                  @{@"Title": LOCALIZATION(@"What is the main brand of engine oil that you use"),   //later lokalise
                                    @"Type": @(kREGISTRATION_DROPDOWN),
                                    @"Key": kMainBrand,
                                    @"Value": ISNULLCLASS([self.workshopDetailsWebData objectForKey: kMainBrand])? LOCALIZATION(@"Please Select") : [mSession convertToCompanyProfBuy:[[self.workshopDetailsWebData objectForKey: kMainBrand] intValue]],
                                    }, //    kWorkshopDetails_MAINBRAND,
                                  @{@"Title": LOCALIZATION(@"Whats MOST important to you when buying an engine oil?"),  //later lokalise
                                    @"Type": @(kPROFILING_TEXTVIEW),
                                    @"Key": kImportantBuyingEngineOil,
                                    @"Value":  ISNULLCLASS([self.workshopDetailsWebData objectForKey: kImportantBuyingEngineOil])? @"" : [self.workshopDetailsWebData objectForKey: kImportantBuyingEngineOil],
                                    }, //    kWorkshopDetails_MOSTIMPORTANT,
                                  @{@"Title": LOCALIZATION(@"Where do you buy your engine oils from?"), //later lokalise
                                    @"Type": @(kREGISTRATION_DROPDOWN),
                                    @"Key": kWhereBuyingEngineOil,
                                    @"Value":  ISNULLCLASS([self.workshopDetailsWebData objectForKey: kWhereBuyingEngineOil])? LOCALIZATION(@"Please Select") : [mSession convertToCompanyProfBuy:[[self.workshopDetailsWebData objectForKey: kWhereBuyingEngineOil] intValue]],
                                    }, //    kWorkshopDetails_WHEREBUYINGENGINEOIL,
                                  @{@"Title": LOCALIZATION(@"What is the weekly/monthly number of oil changes you do?"),    //later lokalise
                                    //                                    @"Placeholder": LOCALIZATION(C_PROFILE_ANSWER),
                                    @"Type": @(kPROFILING_LABELWITHTEXTBOX),
                                    @"Keyboard": @(UIKeyboardTypeNumberPad),
                                    @"Key": kWeeklyNumber, //both use Weekly for now. reserved kMonthlyNumber
                                    @"Value":  ISNULLCLASS([self.workshopDetailsWebData objectForKey: kWeeklyNumber])? @"" : [self.workshopDetailsWebData objectForKey: kWeeklyNumber],
                                    }, //    kWorkshopDetails_OILCHANGES,
//                                  @{@"Title": LOCALIZATION(@"How many different engine oil types/grades do you use?"),
//                                    @"Type": @(kPROFILING_TEXTVIEW),
//                                    @"Key": kDifferentEngineOil,
//                                    @"Value":  ISNULLCLASS([self.workshopDetailsWebData objectForKey: kDifferentEngineOil])? @"" : [self.workshopDetailsWebData objectForKey: kDifferentEngineOil],
//                                    }, //    kWorkshopDetails_OILTYPESUSE,
//                                  @{@"Title": LOCALIZATION(@"What is the main grade/specs you stock?"),
//                                    @"Type": @(kPROFILING_TEXTVIEW),
//                                    @"Key": kMainGrades,
//                                    @"Value":  ISNULLCLASS([self.workshopDetailsWebData objectForKey: kMainGrades])? @"" : [self.workshopDetailsWebData objectForKey: kMainGrades],
//                                    }, //    kWorkshopDetails_MAINGRADESTOCK,
                                  
                                  @{@"Title": LOCALIZATION(@"What is your preferred stock size?"),  //later lokalise
                                    @"Type": @(kPROFILING_CHECKBOX),
                                    @"Key": kPackSize,
                                    @"Other": @(NO),
                                    @"Value":  ISNULLCLASS([self.workshopDetailsWebData objectForKey: kPackSize])? @"" : [self.workshopDetailsWebData objectForKey: kPackSize],
                                    }, //    kWorkshopDetails_PACKSIZE,
//                                  @{@"Title": LOCALIZATION(@"Rank the below in order of importance to you, in a supplier. (Drag and drop to rearrange)"),
//                                    //                                    @"Placeholder": LOCALIZATION(C_PROFILE_ANSWER),
//                                    @"Type": @(kPROFILING_REARRANGE),
//                                    @"Key": kProfileRankList,
//                                    @"Value":  ISNULLCLASS(self.profileRankListArray)? @"" : self.profileRankListArray,
//                                    }, //    kWorkshopDetails_IMPORTANCE,
                                  //                                  @{@"Title": LOCALIZATION(C_PROFILE_COMPANYNAME),
                                  @{@"Title": LOCALIZATION(@"Date of first order?"),    //later lokalise
                                    //                                    @"Placeholder": LOCALIZATION(C_PROFILE_ANSWER),
                                    @"Type": @(kREGISTRATION_DROPDOWN),
                                    @"Key": kOrderDate,
                                    @"Value":  ISNULLCLASS([self.workshopDetailsWebData objectForKey: kOrderDate])? @"" : [[self.workshopDetailsWebData objectForKey: kOrderDate] stringByTrimmingCharactersInSet:
                                                                                                                           [NSCharacterSet whitespaceAndNewlineCharacterSet]],
                                    }, //    kWorkshopDetails_ORDERDATE,
                                  @{@"Title": LOCALIZATION(@"Order Quantity (Carton)"), //later lokalise
                                    @"Type": @(kPROFILING_LABELWITHTEXTBOX),
                                    @"Keyboard": @(UIKeyboardTypeNumberPad),
                                    @"Key": kOrderQuantity, //both use Weekly for now. reserved kMonthlyNumber
                                    @"Value":  ISNULLCLASS([self.workshopDetailsWebData objectForKey: kOrderQuantity])? @"" : [[self.workshopDetailsWebData objectForKey: kOrderQuantity] stringValue],
                                    }, //    kWorkshopDetails_ORDERQUANTITY,
                                  ]];

    [self.workshopDetailsTableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.contactDetailsTableView)
    {
       return [self.contactDetailsArray count];
    }
    else
    {
        return [self.workshopDetailsArray count];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.contactDetailsTableView)
    {
        if(self.isContactDetailsOpen){
            return CONTACTCELL_HEIGHT;
        }else{
            if(indexPath.row == 0)
                return CONTACTCELL_HEIGHT;
            else
                return 0;
        }
    }
    else
        if(indexPath.row == 0)
            return CONTACTCELL_HEIGHT;
    
        if(self.isWorkshopDetailsOpen){
            switch (indexPath.row)
            {
//                case kWorkshopDetails_SERVICESOFFERED:
//                    return CONTACTCELL_HEIGHT + (50 * [[mSession getCompanyProfService] count]); // - 1 for others
//                    //title + total data + other
//                    break;
                case kWorkshopDetails_PACKSIZE:
                    return CONTACTCELL_HEIGHT + (50 * [[mSession getCompanyProfPackSize] count]);
                    //title + total data + other
                    break;
//                case kWorkshopDetails_IMPORTANCE:
//                    return 100 + (50 * [self.profileRankListArray count]);
//              return 200 + (50 * [dataArray count]);
//                    break;
                    //kREGISTRATION_TEXTVIEW
//                case kWorkshopDetails_OILTYPESUSE:
//                    return WORKSHOPTEXTVIEW_HEIGHT;
//                    break;
//                case kWorkshopDetails_MAINGRADESTOCK:
//                    return WORKSHOPTEXTVIEW_HEIGHT;
//                    break;
                case kWorkshopDetails_MOSTIMPORTANT:
                    return WORKSHOPTEXTVIEW_HEIGHT;
                    break;
                case kWorkshopDetails_MAINBRAND:
                    return CONTACTCELLDROPDOWN_HEIGHT;
                case kWorkshopDetails_WHEREBUYINGENGINEOIL:
                    return CONTACTCELLDROPDOWN_HEIGHT;
                default:
                    return CONTACTCELL_HEIGHT;
                    break;
            }
        }else{
            return 0;
        }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//        UITableViewCell *cell = [self.referenceHeaderDict objectForKey:@"ongoing"];
    if (indexPath.row == 0) //header
    {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath: indexPath];
        UIImageView *imgIcon = [cell viewWithTag: 2];
        
        if (tableView == self.contactDetailsTableView)
        {
            if(self.isContactDetailsOpen){ //currently open, collapse it
                imgIcon.image = [UIImage imageNamed:@"icon-expand"]; //next action is to expand
                self.isContactDetailsOpen = NO;
                self.contactDetailsHeight.constant = CONTACTCELL_HEIGHT;
            }else{//currently close, expand it
                imgIcon.image = [UIImage imageNamed:@"icon-collapse"]; //next action is to collapse
                self.isContactDetailsOpen = YES;
                self.contactDetailsHeight.constant = CONTACTCELL_HEIGHT * [self.contactDetailsArray count];
            }
            [self.contactDetailsTableView layoutIfNeeded];
            [self.contactDetailsTableView reloadData];
        }
        else
        {
            if(self.isWorkshopDetailsOpen){ //currently open, collapse it
                imgIcon.image = [UIImage imageNamed:@"icon-expand"]; //next action is to expand
                self.isWorkshopDetailsOpen = NO;
                self.workshopDetailsHeight.constant = CONTACTCELL_HEIGHT;
            }else{//currently close, expand it
                imgIcon.image = [UIImage imageNamed:@"icon-collapse"]; //next action is to collapse
                self.isWorkshopDetailsOpen = YES;
                
                [self.workshopDetailsTableView reloadData];
                
                self.workshopDetailsHeight.constant = self.workshopDetailsTableView.contentSize.height;
                [self.workshopDetailsTableView layoutIfNeeded];
            }
        }
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.contactDetailsTableView)
    {
        UITableViewCell *cell;
        NSDictionary *contactDetailsData = [self.contactDetailsArray objectAtIndex: indexPath.row];
        if (indexPath.row == 0)
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"HeaderCell"];
            UILabel *lblHeader = [cell viewWithTag: 1];
            lblHeader.text = [contactDetailsData objectForKey: @"Title"];
            lblHeader.font = FONT_B(18);
        }
        else
        {
            switch ([[contactDetailsData objectForKey: @"Type"] intValue])
            {
                case kREGISTRATION_TEXTFIELD:
                {
                    RegistrationFormTextfieldCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormTextfieldCell"];
                    
                    if ([contactDetailsData objectForKey: @"Keyboard"])
                        [textfieldCell setKeyboard: [[contactDetailsData objectForKey: @"Keyboard"] intValue]];
                    if ([contactDetailsData objectForKey: @"SecureEntry"])
                        [textfieldCell setSecureTextEntry: [[contactDetailsData objectForKey: @"SecureEntry"] boolValue]];
                    
                    //see if Enabled key is available, if not see if pending or approved
                    if ([contactDetailsData objectForKey: @"Enabled"])
                        [textfieldCell enableCell: [[contactDetailsData objectForKey: @"Enabled"] boolValue]];
                    
                    [textfieldCell setTitle: [contactDetailsData objectForKey: @"Title"]];
                    [textfieldCell hideTopLabel: NO];
                    [textfieldCell  setTextfieldDelegate: self];
                    
                    if ([contactDetailsData objectForKey: @"Value"])
                        [textfieldCell setTextfieldText: [contactDetailsData objectForKey: @"Value"]];
                    
                    if ([contactDetailsData objectForKey: @"Placeholder"])
                        [textfieldCell setPlaceholder: [contactDetailsData objectForKey: @"Placeholder"]];
                    
                    [self.contactDetailsDict setObject: textfieldCell forKey: [contactDetailsData objectForKey: @"Key"]];
                    return textfieldCell;
                }
                    break;
                case kREGISTRATION_DROPDOWN:
                {
                    RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
                    //see if Enabled key is available, if not see if pending or approved
                    if ([contactDetailsData objectForKey: @"Enabled"])
                        [dropdownCell setUserInteractionEnabled: [[contactDetailsData objectForKey: @"Enabled"] boolValue]];
                    
                    [dropdownCell setTitle: [contactDetailsData objectForKey: @"Title"]];
                    //            if (indexPath.row == kWorkshopDetails_WORKINGHOURS)
                    //                [dropdownCell hideTopLabel: YES];
                    //            else
                    [dropdownCell hideTopLabel: NO];
                    dropdownCell.button.tag = indexPath.row;
                    
                    [dropdownCell.button addTarget: self action: @selector(dropdownPressed:) forControlEvents:UIControlEventTouchUpInside];
                    
                    //set initial value, blank dropdown defaulted from conversion
                    if (![[contactDetailsData objectForKey: @"Value"] isEqualToString: BLANK])
                        [dropdownCell setSelectionTitle: [contactDetailsData objectForKey: @"Value"]];
                    else
                        [dropdownCell setSelectionTitle: [contactDetailsData objectForKey: @"Title"]];
                    
                    //keep a reference of cell to submit json value later
                    [self.contactDetailsDict setObject: dropdownCell forKey: [contactDetailsData objectForKey: @"Key"]];
                    return dropdownCell;
                }
                    break;
                case kREGISTRATION_TEXTFIELDWITHBUTTON:
                {
                    RegistrationTextfieldWithButtonCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationTextfieldWithButtonCell"];
                    //see if Enabled key is available, if not see if pending or approved
                    if ([contactDetailsData objectForKey: @"Enabled"])
                        [textfieldCell setUserInteractionEnabled: [[contactDetailsData objectForKey: @"Enabled"] boolValue]];
                    
                    if ([contactDetailsData objectForKey: @"TextfieldEnabled"])
                        [textfieldCell setTextfieldEnabled: [[contactDetailsData objectForKey: @"TextfieldEnabled"] boolValue]];
                    [textfieldCell setTitle: [contactDetailsData objectForKey: @"Title"]];
                    [textfieldCell setIconImage: [contactDetailsData objectForKey: @"Icon"]];
                    [textfieldCell hideTopLabel: NO];
                    [textfieldCell  setTextfieldDelegate: self];
                    
                    switch (indexPath.row)
                    {
                        case kContactDetails_LOCATION:
                            [textfieldCell.button addTarget:self action:@selector(locationPressed:) forControlEvents:UIControlEventTouchUpInside];
                            break;
                    }
                    
                    if ([contactDetailsData objectForKey: @"Value"])
                        [textfieldCell setTextfieldText: [contactDetailsData objectForKey: @"Value"]];
                    [self.contactDetailsDict setObject: textfieldCell forKey: [contactDetailsData objectForKey: @"Key"]];
                    return textfieldCell;
                }
                    break;
                case kREGISTRATION_BUTTON:
                {
                    RegistrationButtonCell *buttonCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationButtonCell"];
                    //see if Enabled key is available, if not see if pending or approved
                    if ([contactDetailsData objectForKey: @"Enabled"])
                        [buttonCell setUserInteractionEnabled: [[contactDetailsData objectForKey: @"Enabled"] boolValue]];
                    
                    [buttonCell setTitle: [contactDetailsData objectForKey: @"Title"]];
                    [buttonCell setIconImage: [contactDetailsData objectForKey: @"Icon"]];
                    [buttonCell hideTopLabel: NO];
                    
                    [self.contactDetailsDict setObject: buttonCell forKey: [contactDetailsData objectForKey: @"Key"]];
                    return buttonCell;
                }
                    break;
                case kREGISTRATION_MOBNUM:
                {
                    RegistrationMobileNumberCell *mobNumCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationMobileNumberCell"];
                    //see if Enabled key is available, if not see if pending or approved
                    if ([contactDetailsData objectForKey: @"Enabled"])
                        [mobNumCell setUserInteractionEnabled: [[contactDetailsData objectForKey: @"Enabled"] boolValue]];
                    
                    [mobNumCell setTitle: [contactDetailsData objectForKey: @"Title"]];
                    [mobNumCell hideTopLabel: NO];
                    [mobNumCell  setTextfieldDelegate: self];
                    
                    [mobNumCell setCountryCode: [contactDetailsData objectForKey: @"CountryCode"]];
                    [mobNumCell setMobileNumber: [contactDetailsData objectForKey: @"MobileNumber"]];
                    
                    if ([contactDetailsData objectForKey: @"TextfieldEnabled"])
                        [mobNumCell setTextfieldEnabled: [[contactDetailsData objectForKey: @"TextfieldEnabled"] boolValue]];
                    [mobNumCell setMobileCodeEnabled: NO];
                    [self.contactDetailsDict setObject: mobNumCell forKey: [contactDetailsData objectForKey: @"Key"]];
                    return mobNumCell;
                }
                    break;
            }
        }
//        UIImageView *imgIcon = [cell viewWithTag: 2];
//        imgIcon.image = [UIImage imageNamed:@"icon-expand"];
        
        return cell;
    }
    else
    {
        UITableViewCell *cell;
        NSDictionary *workshopDetailsData = [self.workshopDetailsArray objectAtIndex: indexPath.row];
        if (indexPath.row == 0)
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"HeaderCell"];
            UILabel *lblHeader = [cell viewWithTag: 1];
            lblHeader.text = [workshopDetailsData objectForKey: @"Title"];
            lblHeader.font = FONT_B(18);
        }
        else
        {
            switch ([[workshopDetailsData objectForKey: @"Type"] intValue])
            {
                case kREGISTRATION_TEXTFIELD:
                {
                    RegistrationFormTextfieldCell *textfieldCell = [self.workshopDetailsTableView dequeueReusableCellWithIdentifier: @"RegistrationFormTextfieldCell"];
                    
                    
                    if ([workshopDetailsData objectForKey: @"Keyboard"])
                        [textfieldCell setKeyboard: [[workshopDetailsData objectForKey: @"Keyboard"] intValue]];
                    if ([workshopDetailsData objectForKey: @"SecureEntry"])
                        [textfieldCell setSecureTextEntry: [[workshopDetailsData objectForKey: @"SecureEntry"] boolValue]];
                    
                    //see if Enabled key is available, if not see if pending or approved
                    if ([workshopDetailsData objectForKey: @"Enabled"])
                        [textfieldCell enableCell: [[workshopDetailsData objectForKey: @"Enabled"] boolValue]];
                    
                    if ([workshopDetailsData objectForKey: @"NetworkToggle"])
                        [textfieldCell toggleProfilingQuestion: [[workshopDetailsData objectForKey: @"NetworkToggle"] boolValue]];
                    
                    [textfieldCell setTitle: [workshopDetailsData objectForKey: @"Title"]];
                    [textfieldCell hideTopLabel: NO];
                    [textfieldCell  setTextfieldDelegate: self];
                    
                    if ([workshopDetailsData objectForKey: @"Value"])
                        [textfieldCell setTextfieldText: [workshopDetailsData objectForKey: @"Value"]];
                    
                    if ([workshopDetailsData objectForKey: @"Placeholder"])
                        [textfieldCell setPlaceholder: [workshopDetailsData objectForKey: @"Placeholder"]];
                    
                    [self.workshopDetailsDict setObject: textfieldCell forKey: [workshopDetailsData objectForKey: @"Key"]];
                    return textfieldCell;
                }
                    break;
                case kREGISTRATION_DROPDOWN:
                {
                    RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
                    //see if Enabled key is available, if not see if pending or approved
                    if ([workshopDetailsData objectForKey: @"Enabled"])
                        [dropdownCell setUserInteractionEnabled: [[workshopDetailsData objectForKey: @"Enabled"] boolValue]];
                    
                    [dropdownCell setTitle: [workshopDetailsData objectForKey: @"Title"]];
                    [dropdownCell hideTopLabel: NO];
                    dropdownCell.button.tag = indexPath.row;
                    
                    if ([[workshopDetailsData objectForKey: @"Key"] isEqualToString: kOrderDate])
                    {
                        [dropdownCell.button addTarget: self action: @selector(datePressed:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else
                        [dropdownCell.button addTarget: self action: @selector(dropdownPressed:) forControlEvents:UIControlEventTouchUpInside];
                    
                    //set initial value, blank dropdown defaulted from conversion
                    if (![[workshopDetailsData objectForKey: @"Value"] isEqualToString: BLANK])
                        [dropdownCell setSelectionTitle: [workshopDetailsData objectForKey: @"Value"]];
                    else
                        [dropdownCell setSelectionTitle: [workshopDetailsData objectForKey: @"Title"]];
                    
                    //keep a reference of cell to submit json value later
                    [self.workshopDetailsDict setObject: dropdownCell forKey: [workshopDetailsData objectForKey: @"Key"]];
                    return dropdownCell;
                }
                    break;
                    
                case kPROFILING_TEXTVIEW:
                {
                    BoxedTextViewTableViewCell *textviewCell = [tableView dequeueReusableCellWithIdentifier: @"BoxedTextViewTableViewCell"];
                    textviewCell.lblTitle.text = [workshopDetailsData objectForKey: @"Title"];
                    [textviewCell setTextviewDelegate: self];
                    
                    [textviewCell setTextviewValue: [workshopDetailsData objectForKey: @"Value"]];
                    [self.workshopDetailsDict setObject: textviewCell forKey: [workshopDetailsData objectForKey: @"Key"]];
                    return textviewCell;
                }
                    break;
                case kPROFILING_LABELWITHTEXTBOX:
                {
                    LabelWithTextBoxTableViewCell *cell = [self.workshopDetailsTableView dequeueReusableCellWithIdentifier: @"LabelWithTextBoxTableViewCell"];
                    
                    if ([workshopDetailsData objectForKey: @"Keyboard"])
                        [cell setKeyboard: [[workshopDetailsData objectForKey: @"Keyboard"] intValue]];
                    if ([workshopDetailsData objectForKey: @"SecureEntry"])
                        [cell setSecureTextEntry: [[workshopDetailsData objectForKey: @"SecureEntry"] boolValue]];
                    
                    cell.lblTitle.text = [workshopDetailsData objectForKey: @"Title"];
                    [cell setTextfieldDelegate: self];
                    
                    [cell setJsonValue: [workshopDetailsData objectForKey: @"Value"]];
                    [self.workshopDetailsDict setObject: cell forKey: [workshopDetailsData objectForKey: @"Key"]];
                    return cell;
                }
                    break;
                case kPROFILING_BOOLEANQUESTION:
                {
                    BooleanQuestionTableViewCell *cell = [self.workshopDetailsTableView dequeueReusableCellWithIdentifier: @"BooleanQuestionTableViewCell"];
                    cell.lblQuestion.text = [workshopDetailsData objectForKey: @"Title"];
                    
                    cell.delegate = self;
                    
                    [cell setJsonValue: [[workshopDetailsData objectForKey: @"Value"] boolValue]];
                    
                    [self.workshopDetailsDict setObject: cell forKey: [workshopDetailsData objectForKey: @"Key"]];
                    return cell;
                }
                    break;
                    
                case kPROFILING_CHECKBOX:
                {
                    CheckboxTableViewCell *cell = [self.workshopDetailsTableView dequeueReusableCellWithIdentifier: @"CheckboxTableViewCell"];
                    cell.lblDescription.text = [workshopDetailsData objectForKey: @"Title"];
                    cell.delegate = self;
                    if ([workshopDetailsData objectForKey: @"Value"])
                    {
                        NSString *valueString = [workshopDetailsData objectForKey: @"Value"];
                        if ([valueString isEqualToString: BLANK])
                        {
                            cell.stringArray = [[NSMutableArray alloc] init];
                        }
                        else
                        {
                            cell.stringArray = [[NSMutableArray alloc] initWithArray: [valueString componentsSeparatedByString:@","]];
                        }
                    }
                    if ([[workshopDetailsData objectForKey: @"Other"] boolValue])
                    {
                        cell.hasOther = YES;
                        cell.otherString = [workshopDetailsData objectForKey: @"OtherValue"];
                    }
                    
                    //special case, use lookup table direct. no conversion on create.
//                    if (indexPath.row == kWorkshopDetails_SERVICESOFFERED)
//                        cell.dataArray = [[mSession lookupTable] objectForKey: @"CompanyProfService"];
//                    else
                        cell.dataArray = [[mSession lookupTable] objectForKey: @"CompanyProfPackSize"];
//                    if (indexPath.row == kWorkshopDetails_SERVICESOFFERED)
//                        cell.dataArray = [mSession getCompanyProfService];
//                    else
//                        cell.dataArray = [mSession getCompanyProfPackSize];
                    
                    
                    [cell.tableview reloadData];
                    [self.workshopDetailsDict setObject: cell forKey: [workshopDetailsData objectForKey: @"Key"]];
                    return cell;
                }
                    break;
//                case kPROFILING_REARRANGE:
//                {
//                    RearrangeTableViewCell *cell =
//                    [self.workshopDetailsTableView dequeueReusableCellWithIdentifier: @"RearrangeTableViewCell"];
//                    cell.dataArray = self.profileRankListArray;
//                    cell.lblDescription.text = [workshopDetailsData objectForKey: @"Title"];
//                    [cell.tableview reloadData];
//                    [self.workshopDetailsDict setObject: cell forKey: [workshopDetailsData objectForKey: @"Key"]];
//                    return cell;
//                }
                    break;
                default:
                    break;
            }
        }
        return cell;
    }
}
-(IBAction) datePressed:(UIButton *)sender
{
    [self.view endEditing: YES];
    
    RegistrationFormDropdownCell *cell = [self.workshopDetailsDict objectForKey: kOrderDate];
    NSString *dateString = cell.button.titleLabel.text;

    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy/MM/dd"];
    
    //date cannot be lesser than start date
    
    self.nativeDatePickerView = [[NativeDatePicker alloc]initWithDatepickerTitle: LOCALIZATION(@"Select Order Date") maximumDate: nil minimumDate: nil inputDateFormatter:outputFormatter outputDateFormatter:outputFormatter]; //later lokalise
    self.nativeDatePickerView.delegate = self;
    
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: nativeDatePickerView];
    nativeDatePickerView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[nativeDatePickerView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (nativeDatePickerView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[nativeDatePickerView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (nativeDatePickerView)]];
}

-(void)getDateAsString:(NSString *)dateString
{
    RegistrationFormDropdownCell *cell = [self.workshopDetailsDict objectForKey: kOrderDate];
    
    
    [cell.button setTitle: [dateString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]] forState:UIControlStateNormal];
}

#pragma mark dropdown methods
-(IBAction) dropdownPressed:(UIButton *)sender
{
    [self.view endEditing: YES];
    listView.tag = sender.tag;
    
    switch (listView.tag)
    {
        case kWorkshopDetails_MAINBRAND:
            [listView setTitleAndList: LOCALIZATION(@"Select brand") list: [mSession getCompanyProfMainBrand] hasSearchField: NO];  //later lokalise
            break;
        case kWorkshopDetails_WHEREBUYINGENGINEOIL:
            [listView setTitleAndList: LOCALIZATION(@"Select location") list: [mSession getCompanyProfBuy] hasSearchField: NO]; //later lokalise
            break;
        default:
            NSLog(@"unknown error");
            break;
    }
    [self popUpView];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        RegistrationFormDropdownCell *cell;
        switch (listView.tag)
        {
            case kWorkshopDetails_MAINBRAND:
                cell = [self.workshopDetailsDict objectForKey: kMainBrand];
                break;
            case kWorkshopDetails_WHEREBUYINGENGINEOIL:
                cell = [self.workshopDetailsDict objectForKey: kWhereBuyingEngineOil];
                break;
            default:
                return;
                break;
        }
        [cell.button setTitle:selection.firstObject forState:UIControlStateNormal];
    }
}

-(void) toggleQuestion:(BOOL) toEnable
{
    RegistrationFormTextfieldCell *cell = [self.workshopDetailsDict objectForKey: kWhichNetwork];
    [cell toggleProfilingQuestion: toEnable];
}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

- (IBAction)locationPressed:(id)sender {
    [UpdateHUD addMBProgress: self.view withText: @""];
    
    switch ([CLLocationManager authorizationStatus])
    {
        case kCLAuthorizationStatusAuthorizedAlways:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusNotDetermined:
            [UpdateHUD removeMBProgress: self];
            [self.locationManager requestWhenInUseAuthorization];
            break;
        case kCLAuthorizationStatusDenied:
        {
            [UpdateHUD removeMBProgress: self];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_ALERTVIEW_ERROR) message:LOCALIZATION(C_ALERTVIEW_PERMISSIONERRORMSG) preferredStyle:UIAlertControllerStyleAlert]; //lokalised 11 Feb
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_CANCEL) style:UIAlertActionStyleCancel handler:nil];  //lokalised 11 Feb
            UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_SETTING) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) { //lokalised 11 Feb
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                            UIApplicationOpenSettingsURLString]];
            }];
            
            [alertController addAction:cancelAction];
            [alertController addAction:settingsAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
            break;
        case kCLAuthorizationStatusRestricted:
            [UpdateHUD removeMBProgress: self];
            break;
    }
}
-(void) didEndEditing
{
    self.scrollViewToBottom.constant = 0;
    self.btnUpdate.hidden = NO;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    self.scrollViewToBottom.constant = 0;
    self.btnUpdate.hidden = NO;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.scrollViewToBottom.constant = 0;
    self.btnUpdate.hidden = NO;
}

-(void)keyboardOnScreen:(NSNotification *)notification
{
    [super keyboardOnScreen: notification];
        self.btnUpdate.hidden = YES;
        self.scrollViewToBottom.constant = self.keyboardFrame.size.height - 50;
}

- (IBAction)updateWorkshopProfiling:(id)sender {
    
    NSDictionary *contactDetailsParams;
    if ([self.contactDetailsDict count] > 0)
    {
        NSString *contactCompanyName = [[self.contactDetailsDict objectForKey: kCompanyName] getJsonValue];
        NSString *contactStreet = [[self.contactDetailsDict objectForKey: kStreet] getJsonValue];
        NSString *contactTown = [[self.contactDetailsDict objectForKey: kTown] getJsonValue];
        NSString *contactPostalCode = [[self.contactDetailsDict objectForKey: kPostalCode] getJsonValue];
        NSString *contactEmailAddress = [[self.contactDetailsDict objectForKey: kEmailAddress] getJsonValue];
        NSString *contactContactName = [[self.contactDetailsDict objectForKey: kContactName] getJsonValue];
        NSString *contactMobileNumber = [[self.contactDetailsDict objectForKey: kMobileNumber] getMobileNumberJsonValue];
        NSString *contactURL = [[self.contactDetailsDict objectForKey: kWebsiteURL] getJsonValue];
        NSString *contactJobTitle = [[self.contactDetailsDict objectForKey: kJobTitle] getJsonValue];
    
        contactDetailsParams = @{
                                 kTradeID          : ISNULLCLASS([[self.workshopDetailsData objectForKey: @"REG_Member"] objectForKey: @"TradeID"]) ? @"" : [[self.workshopDetailsData objectForKey: @"REG_Member"] objectForKey: @"TradeID"],
                                 kCompanyName     : contactCompanyName,
                                 kStreet          : contactStreet,
                                 kTown            : contactTown,
                                 kPostalCode      : contactPostalCode,
                                 kLongitude       : @(self.contactLongitude).stringValue,
                                 kLatitude        : @(self.contactLatitude).stringValue,
                                 kMobileNumber    : contactMobileNumber,
                                 kEmailAddress    : contactEmailAddress,
                                 kContactName     : contactContactName,
                                 kWebsiteURL      : contactURL,
                                 kJobTitle       : contactJobTitle,
                                 };
    }
    else
    {
        //never expanded cell before, not updated before, use existing data
        contactDetailsParams = self.contactDetailsWebData;
    }
    
//    NSArray *workshopProfileRearrange;/
    NSDictionary *workshopDetailsParams;
    if ([self.workshopDetailsDict count] > 0)
    {
        
        NSString *workshopGarages = [[self.workshopDetailsDict objectForKey: kHowManyGarages] getJsonValue];
        NSString *workshopPartGarage = [[self.workshopDetailsDict objectForKey: kAreYouPartGarage] getJsonValue];

        NSString *workshopWhichNetwork = [[self.workshopDetailsDict objectForKey: kWhichNetwork] getJsonValue];
        
        
        NSString *workshopServices = @"";
        NSString *workshopOtherServices = @"";
//        NSArray *workshopServicesArray = [[self.workshopDetailsDict objectForKey: kServices] getJsonArrayToConvert];
//        for (NSString *serviceString in workshopServicesArray)
//        {
//            NSString *convertedString;
//            if ([serviceString containsString: @"Others"])
//            {
//                //handle others
//                NSArray *otherStringArray = [serviceString componentsSeparatedByString: @":"];
//                workshopOtherServices = [otherStringArray lastObject];
//
//                convertedString = [mSession convertToCompanyProfServiceKeyCode: [otherStringArray objectAtIndex: 1]];
//            }
//            else
//                convertedString = [mSession convertToCompanyProfServiceKeyCode: serviceString];
//            if ([workshopServices isEqualToString: @""])
//                workshopServices = convertedString;
//            else
//                workshopServices = [workshopServices stringByAppendingString: [NSString stringWithFormat: @",%@", convertedString]];
//        }
        
        NSString *workshopRamp = [[self.workshopDetailsDict objectForKey: kNumberofRamps] getJsonValue];
        NSString *workshopAnnualConsumption = [[self.workshopDetailsDict objectForKey: kAnualConsmption] getJsonValue];
        NSString *workshopMainBrand = [mSession convertToCompanyProfMainBrandKeyCode: [[self.workshopDetailsDict objectForKey: kMainBrand] getJsonValue]];
        NSString *workshopImportant = [[self.workshopDetailsDict objectForKey: kImportantBuyingEngineOil] getJsonValue];
        NSString *workshopWhereBuyEngine = [mSession convertToCompanyProfBuyKeyCode: [[self.workshopDetailsDict objectForKey: kWhereBuyingEngineOil] getJsonValue]];
        NSString *workshopkWeeklyNumber = [[self.workshopDetailsDict objectForKey: kWeeklyNumber] getJsonValue];
        //    NSString *workshopkMonthlyNumber = [[self.workshopDetailsDict objectForKey: kMonthlyNumber] getJsonValue];
//        NSString *workshopDifferentEngine = [[self.workshopDetailsDict objectForKey: kDifferentEngineOil] getJsonValue];
//        NSString *workshopMainGrade = [[self.workshopDetailsDict objectForKey: kMainGrades] getJsonValue];
        
        
        NSString *workshopPackSize = @"";
        NSArray *workshopPackSizeArray = [[self.workshopDetailsDict objectForKey: kPackSize] getJsonArrayToConvert];
        for (NSString *packSizeString in workshopPackSizeArray)
        {
            NSString *convertedString = [mSession convertToCompanyProfPackSizeKeyCode: packSizeString];
            if ([workshopPackSize isEqualToString: @""])
                workshopPackSize = convertedString;
            else
                workshopPackSize = [workshopPackSize stringByAppendingString: [NSString stringWithFormat: @",%@", convertedString]];
        }
        
        NSString *orderDateString = [[self.workshopDetailsDict objectForKey: kOrderDate] getJsonValue];
        NSInteger orderQuantity = [[[self.workshopDetailsDict objectForKey: kOrderQuantity] getJsonValue] intValue];
        
//        workshopProfileRearrange = [(RearrangeTableViewCell *)[self.workshopDetailsDict objectForKey: kProfileRankList] getJsonArrayValue];
        
        workshopDetailsParams = @{
                                  kHowManyGarages               : workshopGarages,
                                  kAreYouPartGarage             : @(workshopPartGarage.intValue),
                                  kWhichNetwork                 : workshopWhichNetwork,
//                                  kServices                     : workshopServices,
//                                  kServicesOther                : workshopOtherServices,
                                  kNumberofRamps                : workshopRamp,
                                  kAnualConsmption              : workshopAnnualConsumption,
                                  kMainBrand                    : workshopMainBrand,
                                  kImportantBuyingEngineOil     : workshopImportant,
                                  kWhereBuyingEngineOil         : workshopWhereBuyEngine,
                                  kWeeklyNumber                 : workshopkWeeklyNumber,
//                                  kMonthlyNumber                : @"MonthlyNumber",
//                                  kDifferentEngineOil           : workshopDifferentEngine,
//                                  kMainGrades                   : workshopMainGrade,
                                  kPackSize : workshopPackSize,
                                  
                                  kOrderDate : orderDateString,
                                  kOrderQuantity : @(orderQuantity),
                                  };
    }
    else
    {
        //never expanded cell before, not updated before, use existing data
        workshopDetailsParams = self.workshopDetailsWebData;
//        workshopProfileRearrange = self.profileRankListArray;
    }
    
    NSDictionary *updateParams = @{
                                   @"ContactDetails" : contactDetailsParams,
                                   @"WorkshopDetails" : workshopDetailsParams,
//                                   @"ProfileRankList" : workshopProfileRearrange,
                                   };
    
    [[WebServiceManager sharedInstance] updateWorkshopProfilingDetailsWithDictionary:updateParams vc: self];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_FETCHWORKSHOPDETAILSUK:
        {
            NSDictionary *respData = [response getGenericResponse];
            
            self.contactDetailsWebData = [[NSMutableDictionary alloc] initWithDictionary: [respData objectForKey: @"ContactDetails"]];
            self.workshopDetailsWebData = [[NSMutableDictionary alloc] initWithDictionary: [respData objectForKey: @"WorkshopDetails"]];
//            if (![[respData objectForKey: @"ProfileRankList"] isKindOfClass: [NSNull class] ])
//                self.profileRankListArray = [[NSMutableArray alloc] initWithArray: [respData objectForKey: @"ProfileRankList"]];
//            else
//                self.profileRankListArray = [[NSMutableArray alloc] init];
            [self setupContactDetailsForm];
            [self setupWorkshopDetailsForm];
            
        }
            break;
        case kWEBSERVICE_UPDATEWORKSHOPDETAILSUK:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                
            }];
        }
            break;
        default:
            break;
    }
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[IQKeyboardManager sharedManager] setEnable: YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
