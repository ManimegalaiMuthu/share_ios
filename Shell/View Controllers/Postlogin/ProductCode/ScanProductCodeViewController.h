//
//  ScanProductCodeViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 25/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface ScanProductCodeViewController : BaseVC
@property NSString *previousVehicleNumber;
@property NSString *previousState;
@end
