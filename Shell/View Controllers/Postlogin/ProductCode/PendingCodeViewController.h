//
//  PendingCodeViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 2/9/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface PendingCodeViewController : BaseVC

@property BOOL isScanBottle;
@property BOOL forConsumer;

@end
