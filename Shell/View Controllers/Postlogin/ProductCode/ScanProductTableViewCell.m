//
//  ScanProductTableViewCell.m
//  Shell
//
//  Created by Admin on 29/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "ScanProductTableViewCell.h"
#import "Constants.h"

@interface ScanProductTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *txtID;
@property (weak, nonatomic) IBOutlet UILabel *txtScannedCode;
@property (weak, nonatomic) IBOutlet UILabel *txtStatus;

@end
@implementation ScanProductTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.txtID.font = FONT_B1;
    self.txtScannedCode.font = self.txtID.font;
    self.txtStatus.font = self.txtID.font;
    
    if(kIsRightToLeft) {
        [self.txtID setTextAlignment:NSTextAlignmentRight];
        [self.txtScannedCode setTextAlignment:NSTextAlignmentRight];
        [self.txtStatus setTextAlignment:NSTextAlignmentRight];
    } else {
        [self.txtID setTextAlignment:NSTextAlignmentLeft];
        [self.txtScannedCode setTextAlignment:NSTextAlignmentLeft];
        [self.txtStatus setTextAlignment:NSTextAlignmentLeft];
    }
    
    self.btnDelete.titleLabel.font = self.txtID.font;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setID:(NSString *)value
{
    _txtID.text = value;
}
-(void)setScannedCode:(NSString*)value
{
    _txtScannedCode.text = value;
}
-(void)setPendingStatus:(NSString*)value
{
    _txtStatus.text = value;
}


@end
