//
//  ScanProductCodeViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 25/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <Photos/Photos.h>
#import "SWGApiClient.h"
#import "SWGConfiguration.h"
// load models
#import "SWGCoordinate.h"
#import "SWGInlineResponse200.h"
#import "SWGInlineResponse200ProcessingTime.h"
#import "SWGInlineResponse400.h"
#import "SWGPlateCandidate.h"
#import "SWGPlateDetails.h"
#import "SWGRegionOfInterest.h"
#import "SWGVehicleCandidate.h"
#import "SWGVehicleDetails.h"
// load API classes for accessing endpoints
#import "SWGDefaultApi.h"

#import <MapKit/MapKit.h>
#import "ScanProductCodeViewController.h"
#import "QRCodeScanner.h"
#import "PopupValueEntryViewController.h"

@interface ScanProductCodeViewController ()<UITableViewDataSource,UITableViewDataSource, QRCodeScannerDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, WebServiceManagerDelegate, CLLocationManagerDelegate,PopupValueEntryDelegate>
@property (strong, nonatomic) IBOutlet UIButton *refreshBtn;
@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UITextField *txtVehicleNum;
@property (weak, nonatomic) IBOutlet UITextField *txtMileage;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *manualCodePopupView;
@property (weak, nonatomic) IBOutlet UITextField *txtCode;

@property (weak, nonatomic) IBOutlet UIButton *btnScan;
@property (weak, nonatomic) IBOutlet UIButton *btnManual;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectScanManual;

@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnAddCode;

@property (weak, nonatomic) IBOutlet UILabel *txtNo;
@property (weak, nonatomic) IBOutlet UILabel *txtScannedCode;
@property (weak, nonatomic) IBOutlet UILabel *txtStatus;
@property (weak, nonatomic) IBOutlet UILabel *txtDelete;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property NSMutableArray *arrScanCode;

@property (weak, nonatomic) IBOutlet UITextField *txtVehicleAndMileage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtVehicleAndMileageHeight; //20 or hide depends on textfield


@property (weak, nonatomic) IBOutlet UIView *offlineScanView;
@property (weak, nonatomic) IBOutlet UILabel *lblOfflineText;
@property (weak, nonatomic) IBOutlet UIButton *btnOfflineYes;
@property (weak, nonatomic) IBOutlet UIButton *btnOfflineNo;


@property CLLocationManager *locationManager;
@property CGPoint currentLocation;
@property BOOL didFindLocation;

@property NSString *currentCodeString;

//Thai State selection & VN Rimula Phase 2
@property (weak, nonatomic) IBOutlet UIView *dropdownSelectView;
@property (weak, nonatomic) IBOutlet UIButton *btnDropdown;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchFieldTopSpace;
@property NSArray *stateTable;



@property (weak, nonatomic) IBOutlet UIButton *btnInviteCustomer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtVehicleTrailingSpace;
@property (weak, nonatomic) IBOutlet UIView *mileageLineView;

@property (weak, nonatomic) IBOutlet UIButton *btnCamera;

enum kPopupSelection_Type
{
    kPopupSelection_State,
    kPopupSelection_VehicleType,
};
@end

@implementation ScanProductCodeViewController
@synthesize listView;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if(IS_COUNTRY(COUNTRYCODE_INDIA)) {
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_RUSSIA_REGISTEROILCHANGE) subtitle: @"" size: 15 subtitleSize: 0];      //lokalised
    } else{
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_SCANPRODUCTCODE) subtitle: @"" size: 15 subtitleSize: 0];         //lokalised
    }
     self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_TITLE_SCANPRODUCTCODE),  LOCALIZATION(C_REWARDS_BACK)];     //lokalised
    
    if (kIsRightToLeft) {
        [self.txtVehicleNum setTextAlignment:NSTextAlignmentRight];
        [self.txtMileage setTextAlignment:NSTextAlignmentRight];
        [self.txtCode setTextAlignment:NSTextAlignmentRight];
        [self.txtVehicleAndMileage setTextAlignment:NSTextAlignmentRight];
        [self.txtNo setTextAlignment:NSTextAlignmentRight];
        [self.txtScannedCode setTextAlignment:NSTextAlignmentRight];
        [self.txtStatus setTextAlignment:NSTextAlignmentRight];
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.txtVehicleNum setTextAlignment:NSTextAlignmentLeft];
        [self.txtMileage setTextAlignment:NSTextAlignmentLeft];
        [self.txtCode setTextAlignment:NSTextAlignmentLeft];
        [self.txtVehicleAndMileage setTextAlignment:NSTextAlignmentLeft];
        [self.txtNo setTextAlignment:NSTextAlignmentLeft];
        [self.txtScannedCode setTextAlignment:NSTextAlignmentLeft];
        [self.txtStatus setTextAlignment:NSTextAlignmentLeft];
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
    _arrScanCode = [[NSMutableArray alloc]init];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_SCANPRODUCTCODE) screenClass:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self showManualCodeEnterPopup:NO];
    [self.tableView registerNib:[UINib nibWithNibName:@"ScanProductTableViewCell" bundle:nil] forCellReuseIdentifier:@"ScanProductTableViewCell"];
    // Declare the Gesture.
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc]
                                             initWithTarget:self
                                             action:@selector(handleTap:)];
    gesRecognizer.delegate = self;
    
    
    if ([self.navigationController.childViewControllers count] > 1)
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
        self.txtVehicleNum.text = self.previousVehicleNumber;
    }
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.refreshBtn];
    if(@available(iOS 11, *)){
        [self.refreshBtn.widthAnchor constraintEqualToConstant: 30.0].active = YES;
        [self.refreshBtn.heightAnchor constraintEqualToConstant: 30.0].active = YES;
    }
    
    [mQRCodeScanner setDelegate: self];
    
    // Add Gesture to your view.
    [self.manualCodePopupView addGestureRecognizer:gesRecognizer];
    
    //VN Rimula
    if ([[mSession profileInfo] hasVehicleType])
    {
        //preload listviews
        listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_DROPDOWN_VEHICLETYPE_HEADER)             //lokalised
                                                       list: [mSession getVehicleType]
                                              selectionType:ListSelectionTypeSingle
                                         previouslySelected: nil];
        [listView setHasSearchField: NO];
        listView.delegate = self;
        
        [self.btnDropdown setTitle: LOCALIZATION(C_DROPDOWN_VEHICLETYPE_HEADER) forState:UIControlStateNormal];     //lokalised
        [self.btnDropdown.titleLabel setFont: FONT_B1];
        [self initView];
        
    }
    //Thai only
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
    {
        [[WebServiceManager sharedInstance] loadState: [[mSession lookupTable] objectForKey: @"LookUpVersion"]
                                          countryCode: GET_COUNTRY_CODE
                                                   vc: self];
        [self.btnDropdown.titleLabel setFont: FONT_B1];
        if ([self.previousState length] > 0)
            [self.btnDropdown setTitle: self.previousState forState:UIControlStateNormal];
        else
            [self.btnDropdown setTitle: LOCALIZATION(C_THAI_VEHICLE_STATE) forState:UIControlStateNormal];     //lokalised
    }
    else
    {
        [self.dropdownSelectView removeFromSuperview];
        self.searchFieldTopSpace.constant = 10;
        [self initView];
    }
    
    if (GET_SCANNEDCODES)
    {
        self.offlineScanView.hidden = NO;
    }
    
    if(![[mSession profileInfo] hasCurrentMileage]) {
        self.mileageLineView.hidden = YES;
        self.txtMileage.hidden = YES;
        self.txtMileage.text = @"0";
        
        [NSLayoutConstraint constraintWithItem:self.txtMileage attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
    }
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [self.locationManager requestWhenInUseAuthorization];
}

-(void)initView
{
    [self.lblSelectScanManual setFont: FONT_B2];
    self.lblSelectScanManual.text = LOCALIZATION(C_SCANCODE_SELECTFOLLOWING);       //lokalised
    
    [self.btnScan.titleLabel setFont: FONT_BUTTON];
    [self.btnScan setBackgroundColor: COLOUR_YELLOW];
    [self.btnScan setTitle:LOCALIZATION(C_FORM_SCAN) forState:UIControlStateNormal];     //lokalised
    [self.btnScan setTitleColor: COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    
    [self.btnManual.titleLabel setFont: FONT_BUTTON];
    [self.btnManual setBackgroundColor: COLOUR_YELLOW];
    [self.btnManual setTitle:LOCALIZATION(C_FORM_MANUAL) forState:UIControlStateNormal];     //lokalised
    [self.btnManual setTitleColor: COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    
    [self.btnCancel.titleLabel setFont: FONT_BUTTON];
    [self.btnCancel setBackgroundColor: COLOUR_YELLOW];
    [self.btnCancel setTitleColor: COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    
    self.txtVehicleNum.font = FONT_B1;
    self.txtVehicleNum.placeholder = LOCALIZATION(C_SEARCHFIELD_VEHICLEID);      //lokalised
    self.txtVehicleNum.delegate = self; //delegate for GA
    
    self.txtMileage.font = FONT_B1;
    self.txtMileage.placeholder = LOCALIZATION(C_SEARCHFIELD_MILEAGE);           //lokalised
    
    self.txtCode.font = FONT_B1;
    self.txtCode.placeholder = LOCALIZATION(C_FORM_ENTERCODETITLE);               //lokalised
    self.txtVehicleAndMileage.font = FONT_B1;
    self.lblTitle.font = FONT_B1;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.txtNo.font = FONT_B1;
    self.txtNo.textColor = COLOUR_VERYDARKGREY;
    self.txtNo.text = LOCALIZATION(C_TABLE_NO);      //lokalised
    
    self.txtScannedCode.font = self.txtNo.font;
    self.txtScannedCode.textColor = self.txtNo.textColor;
    self.txtScannedCode.text= LOCALIZATION(C_TABLE_SCANNEDCODE);             //lokalised
    if ([GET_LOCALIZATION isEqualToString: kChinese])
        self.txtScannedCode.textAlignment = NSTextAlignmentCenter;
    
    self.txtStatus.font = self.txtNo.font;
    self.txtStatus.textColor = self.txtNo.textColor;
    self.txtStatus.text = LOCALIZATION(C_STATUS);        //lokalised
    
    self.txtDelete.font = self.txtNo.font;
    self.txtDelete.textColor = self.txtNo.textColor;
    self.txtDelete.text = LOCALIZATION(C_DELETE);            //lokalised
    
    self.lblTitle.font = FONT_H1;
    self.lblTitle.text = LOCALIZATION(C_FORM_ENTERCODETITLE);    //lokalised
    self.txtCode.font = FONT_B1;
    self.btnAddCode.titleLabel.font = FONT_BUTTON;
    [self.btnAddCode setTitle: LOCALIZATION(C_FORM_ADDCODE) forState:UIControlStateNormal]; //lokalised
    [self.btnAddCode setBackgroundColor:COLOUR_RED];
    
    self.btnCancel.titleLabel.font = FONT_BUTTON;
    [self.btnCancel setTitle:LOCALIZATION(C_FORM_CANCEL) forState:UIControlStateNormal];     //lokalised
    self.btnSubmit.titleLabel.font = FONT_BUTTON;
    [self.btnSubmit setTitle:LOCALIZATION(C_SCANPRODUCTCODE_SUBMIT) forState:UIControlStateNormal];  //lokalised
    [self.btnSubmit setBackgroundColor:COLOUR_RED];
    
    if(GET_PROFILETYPE == kPROFILETYPE_DSR)
    {
        self.btnSubmit.hidden = YES;
    }
    
    self.lblOfflineText.font = FONT_B2;
    self.lblOfflineText.text = LOCALIZATION(C_FORM_OFFLINESCAN);         //lokalised
    
    self.btnOfflineYes.titleLabel.font = FONT_BUTTON;
    [self.btnOfflineYes setTitle:LOCALIZATION(C_FORM_YES) forState:UIControlStateNormal];        //lokalised
    self.btnOfflineNo.titleLabel.font = FONT_BUTTON;
    [self.btnOfflineNo setBackgroundColor: COLOUR_YELLOW];
    [self.btnOfflineNo setTitle:LOCALIZATION(C_FORM_NO) forState:UIControlStateNormal];          //lokalised
    [self.btnOfflineYes setBackgroundColor:COLOUR_RED];
    
    switch ([CLLocationManager authorizationStatus])
    {
        case kCLAuthorizationStatusNotDetermined:
            [self.locationManager requestWhenInUseAuthorization];
            break;
        default:
            break;
    }
    
    UIImage *refreshImage = GET_ISADVANCE ? [UIImage imageNamed:@"ico_refresh_blue"] : [UIImage imageNamed:@"ico_refresh-red"];
    [self.refreshBtn setImage:refreshImage forState:UIControlStateNormal];
    
    //Register Oil Change App Side Alphanumeric Validation for ID & KSA
    self.txtCode.delegate = self;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.txtVehicleNum)
    {
        
        LoginInfo *loginInfo = [mSession loadUserProfile];
        
        [FIRAnalytics logEventWithName: @"OilChange_VehPlate_Manual"
                            parameters: @{@"TradeID": loginInfo.tradeID,}];
    }
}

- (IBAction)textFieldDidChange:(id)sender {
    if (self.txtVehicleNum.text.length == 0 &&
        self.txtMileage.text.length == 0)
    {
        self.txtVehicleAndMileageHeight.constant = 0;
    }
    else
    {
        self.txtVehicleAndMileageHeight.constant = 20;
    }
    
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
    {
        self.txtVehicleAndMileage.text = GET_ISADVANCE ? [NSString stringWithFormat: @"%@: %@%@", LOCALIZATION(C_SEARCHVEHICLE_VEHICLEID), self.btnDropdown.titleLabel.text, self.txtVehicleNum.text] : [NSString stringWithFormat: @"%@: %@%@ \t%@: %@", LOCALIZATION(C_SEARCHVEHICLE_VEHICLEID), self.btnDropdown.titleLabel.text, self.txtVehicleNum.text, LOCALIZATION(C_SEARCHVEHICLE_MILEAGE), self.txtMileage.text];        //lokalised
    }
    else
    {
//        self.txtVehicleAndMileage.text = [NSString stringWithFormat: @"For Vehicle: %@ \tMileage: %@", self.txtVehicleNum.text, self.txtMileage.text];
        self.txtVehicleAndMileage.text = GET_ISADVANCE ? [NSString stringWithFormat: @"%@: %@", LOCALIZATION(C_SEARCHVEHICLE_VEHICLEID), self.txtVehicleNum.text] : [NSString stringWithFormat: @"%@: %@ \t%@: %@", LOCALIZATION(C_SEARCHVEHICLE_VEHICLEID), self.txtVehicleNum.text, LOCALIZATION(C_SEARCHVEHICLE_MILEAGE), self.txtMileage.text];         //lokalised
        
        if([[mSession profileInfo] hasVehicleType]) {
            NSString *vehicleTypeKeyCode = [mSession convertToVehicleTypeKeyCode: self.btnDropdown.titleLabel.text];
            if([vehicleTypeKeyCode isEqualToString:@"8"]) {
                self.txtVehicleAndMileage.text = [NSString stringWithFormat: @"%@: %@", LOCALIZATION(C_SEARCHVEHICLE_VEHICLEID), self.txtVehicleNum.text];
            }
        }
    }
}

- (IBAction)backPressed:(id)sender {
    //only available prelogin
    [self popSelf];
}


- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer{
    [self showManualCodeEnterPopup:NO];
    [self clearTextField];
    
}

-(void)clearTextField
{
    self.txtCode.text =@"";
    if([self.txtCode isFirstResponder])
        [self.txtCode resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)scanPressed:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        LoginInfo *loginInfo = [mSession loadUserProfile];
        [FIRAnalytics logEventWithName: @"OilChange_ProdCode_QRScan"
                            parameters: @{@"TradeID": loginInfo.tradeID,}];
    });
    
    if([self.txtVehicleNum isFirstResponder])
       [self.txtVehicleNum resignFirstResponder];
    if([self.txtMileage isFirstResponder])
       [self.txtMileage resignFirstResponder];
     [mQRCodeScanner showQRCodeScannerOnVC:self withMessage:LOCALIZATION(C_REGISTEROILCHANGE_SUBTITLE)];         //lokalised
}

-(void)qrCodeScanSuccess:(NSString *)scanString
{
    NSString *scannedCodeString;
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDONESIA])
    {
        scannedCodeString = scanString;
    }
    else
    {
        
         scannedCodeString = [[scanString componentsSeparatedByString:@"="] lastObject];
    }

    if(![[mSession profileInfo]hasMultipleSubmissionOilChange]) {
        for (NSDictionary *dict in _arrScanCode)
        {
            if ([scannedCodeString isEqualToString: [dict objectForKey: @"Code"]])
            {
                [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_EXIST)];      //lokalised
                return;
            }
        }
    }
    
    
    if(scanString.length > 0){
        self.currentCodeString = scannedCodeString;
        [self updatelocation];
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    if (self.didFindLocation)
        return;
    
    //most recent location update is at the end of the array.
    CLLocation *location = [locations lastObject];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:6];
    
    self.currentLocation = CGPointMake(location.coordinate.latitude, location.coordinate.longitude);
    
    self.didFindLocation = YES;
    [self.locationManager stopUpdatingLocation];
    
    if(![[mSession profileInfo]hasMultipleSubmissionOilChange])
    {
        //ipod touch getting duplicate value. Manually check
        for(NSDictionary *duplicate in _arrScanCode)
        {
            if([[duplicate objectForKey:@"Code"] isEqualToString:self.currentCodeString]){
                [UpdateHUD removeMBProgress: self];
                return;
            }
        }
    }
    
    NSMutableDictionary *codeDict = [NSMutableDictionary dictionaryWithDictionary:
                                     @{@"Code"          :self.currentCodeString,
                                       @"VehicleNumber" : self.txtVehicleNum.text,
                                       @"Mileage"       : self.txtMileage.text,
                                       @"Date"          : [self getTodayDate],
                                       @"Latitude"      : @(self.currentLocation.x),
                                       @"Longitude"     : @(self.currentLocation.y),
                                       }];
    
    [_arrScanCode addObject: codeDict];
    self.currentLocation = CGPointMake(0, 0);
    
    [_tableView reloadData];
    
    [UpdateHUD removeMBProgress: self];
}

-(void) updatelocation {
    [UpdateHUD addMBProgress: self.view withText: @""];
    self.didFindLocation = NO;
    switch ([CLLocationManager authorizationStatus])
    {
        case kCLAuthorizationStatusAuthorizedAlways:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.locationManager startUpdatingLocation];
            break;
        default:
        {
            //send 0,0
            self.currentLocation = CGPointMake(0, 0);
            
            //get state
            //    NSString *stateKeyCode = [mSession convertToStateKeyCode:self.btnState.titleLabel.text stateDetailedListArray: self.stateTable];
            
            NSMutableDictionary *codeDict = [NSMutableDictionary dictionaryWithDictionary:
                                             @{@"Code"          :self.currentCodeString,
                                               @"VehicleNumber" : self.txtVehicleNum.text,
                                               @"Mileage"       : self.txtMileage.text,
                                               @"Date"          : [self getTodayDate],
                                               @"Latitude"      : @(self.currentLocation.x),
                                               @"Longitude"     : @(self.currentLocation.y),
                                               }];
            
            [_arrScanCode addObject: codeDict];
            [UpdateHUD removeMBProgress: self];
            [_tableView reloadData];
        }
            break;
    }
    
}

- (IBAction)manualPressed:(id)sender {
//    [self showManualCodeEnterPopup:YES];
    LoginInfo *loginInfo = [mSession loadUserProfile];
    [FIRAnalytics logEventWithName: @"OilChange_ProdCode_Manual"
                        parameters: @{@"TradeID": loginInfo.tradeID,}];
    [self popupValueEntryVCWithTitle:LOCALIZATION(C_FORM_ENTERCODETITLE) submitTitle:LOCALIZATION(C_FORM_ADDCODE) cancelTitle:LOCALIZATION(C_FORM_CANCEL) placeHolder:LOCALIZATION(C_FORM_ENTERCODETITLE)];
}

- (IBAction)submitPressed:(id)sender {
    
   /*removing app side validation as server has the validation already and to sync with android behaviour
    
   if ([self.txtVehicleNum.text isEqualToString: @""])
    {
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_VEHICLENUM_ERROR)];        //lokalised
        return;
    }
    if ([self.txtMileage.text isEqualToString: @""] && [[mSession profileInfo] hasCurrentMileage])
    {
        if(![[mSession profileInfo] hasVehicleType]) {
            [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_MILEAGE_ERROR)];           //lokalised
            return;
        }
    }
    */
    
    if ([self.arrScanCode count] == 0)
    {
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_ERROR)];              //lokalised
        return;
    }
    
    //get state
    NSString *stateKeyCode = [mSession convertToStateKeyCode:self.btnDropdown.titleLabel.text stateDetailedListArray: self.stateTable];
    NSMutableDictionary *submitCodeDict = [[NSMutableDictionary alloc] init];
    
    if([[mSession profileInfo]hasVehicleType]) {
        NSString *vehicleTypeKeyCode = [mSession convertToVehicleTypeKeyCode: self.btnDropdown.titleLabel.text];
        [submitCodeDict setObject:@[@{ @"VehicleNumber": self.txtVehicleNum.text,
                                       @"Mileage": self.txtMileage.text,
                                       @"Coupons": self.arrScanCode,
                                       @"StateID": stateKeyCode,
                                       @"VehicleType": vehicleTypeKeyCode,
                                       },
                                    ]
                           forKey:@"VehicleList"];
    } else {
        [submitCodeDict setObject:@[@{ @"VehicleNumber": self.txtVehicleNum.text,
                                       @"Mileage": self.txtMileage.text,
                                       @"Coupons": self.arrScanCode,
                                       @"StateID": stateKeyCode,
                                       },
                                    ]
                           forKey:@"VehicleList"];
    }
 
    if ([mSession getReachability])
    {
        [[WebServiceManager sharedInstance] submitCode: submitCodeDict
                                                    vc:self];
    }
    else
    {
        //save
        NSMutableArray *pendingDict = [[NSMutableArray alloc] initWithArray: GET_SCANNEDCODES copyItems:YES];
        
        if (!pendingDict)
            pendingDict = [[NSMutableArray alloc] init];
        
        for (NSMutableDictionary *dict in self.arrScanCode)
        {
            [dict setObject: self.txtVehicleNum.text forKey: @"VehicleNumber"];
        }
        
        [pendingDict addObject: @{ @"VehicleNumber": self.txtVehicleNum.text,
                                   @"Mileage": self.txtMileage.text,
                                   @"Coupons": self.arrScanCode,
                                   @"StateID": stateKeyCode,
                                   @"Date": [self getTodayDate],
                                   }];
        
        SET_SCANNEDCODES(pendingDict);
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [_arrScanCode removeAllObjects];
        self.txtVehicleNum.text = @"";
        self.txtMileage.text = @"";
        
        [self.tableView reloadData];
        
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_ALERT_SAVEDCODE)];        //lokalised
        
    }
    
}

-(NSString *)getTodayDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    return [formatter stringFromDate: [NSDate date]];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_SUBMITCODE:
        {
            NSMutableDictionary *respData = [[NSMutableDictionary alloc] initWithDictionary:[response getGenericResponse]];
            
            [respData setObject: self.txtVehicleNum.text forKey:@"VehicleId"];
            
            NSString *stateId = [mSession convertToStateKeyCode: self.btnDropdown.titleLabel.text stateDetailedListArray: GET_LOADSTATE];
            
            if (self.btnDropdown)
                [respData setObject: self.btnDropdown.titleLabel.text forKey:@"StateName"];
            
            [mSession pushRegisterCodeResult: respData vc:self];
         
            [self.btnDropdown setTitle: LOCALIZATION(C_THAI_VEHICLE_STATE) forState:UIControlStateNormal];     //lokalised
            self.txtVehicleNum.text = @"";
            self.txtMileage.text = @"";
            self.txtVehicleAndMileage.text = @"";
            self.txtVehicleAndMileageHeight.constant = 0;
            [self.txtVehicleAndMileage setUserInteractionEnabled:NO];
            [self.arrScanCode removeAllObjects];
            [self.tableView reloadData];
//            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
//                
//            }];
        }
            break;
        case kWEBSERVICE_STATE:
            SET_LOADSTATE([[response getGenericResponse] objectForKey: @"StateDetailsList"]);
            self.stateTable = [[response getGenericResponse] objectForKey: @"StateDetailsList"];

            //preload listviews
            listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_THAI_VEHICLE_STATE)             //lokalised
                                                           list: [mSession getStateNames: self.stateTable]
                                                  selectionType:ListSelectionTypeSingle previouslySelected: nil];
            [listView setHasSearchField: YES];
            listView.delegate = self;
            
            [self initView];
            
            break;
        default:
            break;
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress: KEY_WINDOW];
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
            self.stateTable = GET_LOADSTATE;
            
            //preload listviews
            listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_THAI_VEHICLE_STATE)         //lokalised
                                                           list: [mSession getStateNames: self.stateTable]
                                                  selectionType:ListSelectionTypeSingle previouslySelected: nil];
            [listView setHasSearchField: YES];
            listView.delegate = self;
            
            [self initView];
            break;
        default:
            break;
    }
}

- (IBAction)dropdownPressed:(id)sender {
    if ([[mSession profileInfo] hasVehicleType]) 
    {
        listView.tag = kPopupSelection_VehicleType;
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_VEHICLETYPE_HEADER)
                             list: [mSession getVehicleType]
                   hasSearchField: NO];
    }
        //Thai only
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
    {
        listView.tag = kPopupSelection_State;
        [listView setTitleAndList: LOCALIZATION(C_THAI_VEHICLE_STATE)
                             list: [mSession getStateNames: self.stateTable]
                   hasSearchField: YES];
    }
    [self popUpView];
}

- (IBAction)vehicleTypePressed:(UIButton *)sender {
    listView.tag = kPopupSelection_VehicleType;
    [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_VEHICLETYPE_HEADER)
                         list: [mSession getVehicleType]
               hasSearchField: NO];

    [self popUpView];

}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        switch (listView.tag)
        {
            case kPopupSelection_State:
            {
                [self.btnDropdown setTitle: selection.firstObject forState: UIControlStateNormal];
            }
                break;
                
            case kPopupSelection_VehicleType:
            {
                NSString *vehicleCode = [mSession convertToVehicleTypeKeyCode: selection.firstObject]; //KeyCode
                if ([vehicleCode isEqualToString: @"8"])
                {
                    self.txtMileage.text = @"";
                }
                self.txtMileage.enabled = ![vehicleCode isEqualToString: @"8"];
                
                [self.btnDropdown setTitle: selection.firstObject forState:UIControlStateNormal];
            }
                break;
        }
    }
    
}

- (IBAction)cancelPressed:(id)sender {
    [self showManualCodeEnterPopup:NO];
    [self clearTextField];
    
}
- (IBAction)addCodePressed:(id)sender {
    
    if(![[mSession profileInfo]hasMultipleSubmissionOilChange]) {
        for (NSDictionary *dict in _arrScanCode)
        {
            if ([self.txtCode.text isEqualToString: [dict objectForKey: @"Code"]])
            {
                [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_EXIST)];         //lokalised
                return;
            }
        }
    }
    
    [self showManualCodeEnterPopup:NO];
    


    if(self.txtCode.text.length > 0)
    
    {
        self.currentCodeString = self.txtCode.text;
        [self updatelocation];
    }
    [self clearTextField];
}

-(void)showManualCodeEnterPopup:(Boolean)value
{
    if (value)
    {
        LoginInfo *loginInfo = [mSession loadUserProfile];
        
        [FIRAnalytics logEventWithName: @"OilChange_ProdCode_Manual"
                            parameters: @{@"TradeID": loginInfo.tradeID,}];
    }
    self.manualCodePopupView.hidden = !value;
}

- (IBAction)offlineNoPressed:(id)sender {
    self.offlineScanView.hidden = YES;
}
- (IBAction)offlineYesPressed:(id)sender {
    self.offlineScanView.hidden = YES;
    //push to pending code view
    [mSession performSelector: @selector(pushPendingCodeView:) withObject: self afterDelay: 0];
}


#pragma mark - textField Delgate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if((IS_COUNTRY(COUNTRYCODE_SAUDI) || IS_COUNTRY(COUNTRYCODE_INDONESIA)) && textField != self.txtVehicleNum) {
        NSCharacterSet *blockCharacters = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789\n"];
           
           if([string rangeOfCharacterFromSet:blockCharacters].location == NSNotFound) {
               if ([string isEqualToString: @""]) //check for backspace first, proceed if yes
                   return YES;
               else if (![string isEqualToString: @" "])
               {
                   [mAlert showErrorAlertWithMessage: LOCALIZATION(C_REGISTEROILCHANGE_INPUTERROR)];
                   [textField resignFirstResponder];
               }
               return NO;
           } else {
               return YES;
           }
    } else {
        return YES;
    }
}


#pragma mark - tableview delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arrScanCode.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ScanProductTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ScanProductTableViewCell"];
    NSDictionary *codeStringDict = [_arrScanCode objectAtIndex:indexPath.row];
    
    [cell setID: @(indexPath.row + 1).stringValue];
    [cell setScannedCode: [codeStringDict objectForKey: @"Code"]];
    [cell setPendingStatus: LOCALIZATION(C_SCANCODE_STATUS_PENDING)];           //lokalised
    
    cell.btnDelete.tag = indexPath.row;
    cell.btnDelete.backgroundColor = COLOUR_RED;
    [cell.btnDelete setTitle: LOCALIZATION(C_DELETE) forState: UIControlStateNormal];   //lokalised
    [cell.btnDelete addTarget: self action: @selector(deletePressed:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(void)deletePressed:(UIButton*)sender
{
    [_arrScanCode removeObjectAtIndex:sender.tag];
    [_tableView reloadData];
}

- (IBAction)refreshPressed:(id)sender {
    [mLookupManager reloadLookup];
    [[WebServiceManager sharedInstance] loadState: [[mSession lookupTable] objectForKey: @"LookUpVersion"]
                                      countryCode: GET_COUNTRY_CODE
                                               vc: self];
}

#pragma mark - PopupValueEntryDelegate
-(void) valueSubmitted:(NSString *)value {
   if(![[mSession profileInfo]hasMultipleSubmissionOilChange]) {
       for (NSDictionary *dict in _arrScanCode)
       {
           if ([value isEqualToString: [dict objectForKey: @"Code"]])
           {
               [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_EXIST)];         //lokalised
               return;
           }
       }
   }
   if(value.length > 0)
   
   {
       self.currentCodeString = value;
       [self updatelocation];
   }
}

#pragma mark - Car Plate Scanning
- (IBAction)cameraPressed:(id)sender
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        LoginInfo *loginInfo = [mSession loadUserProfile];
        [FIRAnalytics logEventWithName: @"OilChange_VehPlate_Scan"
                            parameters: @{@"TradeID": loginInfo.tradeID,}];
    });
    
    [mSession pushPhotoTakingViewControllerWithParent: self];
}

-(void)imageTaken:(UIImage *)image key:(NSString *)keyString
{
    if (!image)
        return;
    
    UIImage *newImage = [self rotateImage:image];
    
    NSData *imgData = UIImageJPEGRepresentation(newImage, 0.1);
    NSString *base64String = [imgData base64EncodedStringWithOptions: 0];
    
    NSString* secretKey = @"sk_a94baee6f07c7836e8ef292d"; // The secret key used to authenticate your account.  You can view your  secret key by visiting  https://cloud.openalpr.com/
    NSString *country = [self getCountryCode];
    //    NSString* country = @"us"; // Defines the training data used by OpenALPR.  \"us\" analyzes  North-American style plates.  \"eu\" analyzes European-style plates.  This field is required if using the \"plate\" task  You may use multiple datasets by using commas between the country  codes.  For example, 'au,auwide' would analyze using both the  Australian plate styles.  A full list of supported country codes  can be found here https://github.com/openalpr/openalpr/tree/master/runtime_data/config
    //au, auwide, br, br2, eu, fr, gb, in, kr, kr2, mx, sg, us, vn2
    
    if([country length] == 0)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:@"Invalid Country"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {
                                 //BUTTON OK CLICK EVENT
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    NSNumber* recognizeVehicle = @(0); // If set to 1, the vehicle will also be recognized in the image This requires an additional credit per request  (optional) (default to 0)
    NSString* state = @""; // Corresponds to a US state or EU country code used by OpenALPR pattern  recognition.  For example, using \"md\" matches US plates against the  Maryland plate patterns.  Using \"fr\" matches European plates against  the French plate patterns.  (optional) (default to )
    NSNumber* returnImage = @(0); // If set to 1, the image you uploaded will be encoded in base64 and  sent back along with the response  (optional) (default to 0)
    NSNumber* topn = @(5); // The number of results you would like to be returned for plate  candidates and vehicle classifications  (optional) (default to 10)
    NSString* prewarp = @""; // Prewarp configuration is used to calibrate the analyses for the  angle of a particular camera.  More information is available here http://doc.openalpr.com/accuracy_improvements.html#calibration  (optional) (default to )
    
    SWGDefaultApi *apiInstance = [[SWGDefaultApi alloc] init];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [apiInstance recognizeBytesWithImageBytes:base64String
                                    secretKey:secretKey
                                      country:country
                             recognizeVehicle:recognizeVehicle
                                        state:state
                                  returnImage:returnImage
                                         topn:topn
                                      prewarp:prewarp
                            completionHandler: ^(SWGInlineResponse200* output, NSError* error) {
                                if (output)
                                {
                                    if ([output.results count] > 0)
                                    {
                                        SWGPlateDetails *plateDetails = [output.results objectAtIndex: 0];
                                        
                                        self.txtVehicleNum.text = plateDetails.plate; //String of car plate
                                        [self.txtVehicleNum becomeFirstResponder];
                                    }
                                    else
                                    {
                                        UIAlertController *alert = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_GLOBAL_ERROR)
                                                                                                       message: LOCALIZATION(C_ERROR_CANNOTRECOGNIZE)
                                                                                                preferredStyle:UIAlertControllerStyleAlert];
                                        
                                        UIAlertAction *ok = [UIAlertAction actionWithTitle: LOCALIZATION(C_BUTTON_OK) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                             {
                                                                 //BUTTON OK CLICK EVENT
                                                             }];
                                        [alert addAction:ok];
                                        [self presentViewController:alert animated:YES completion:nil];
                                        //                                        self.imgView.image = nil;
                                    }
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                }
                                if (error) {
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    
                                    UIAlertController *alert = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_GLOBAL_ERROR)
                                                                                                   message: LOCALIZATION(C_ERROR_UNKNOWNERROR)
                                                                                            preferredStyle:UIAlertControllerStyleAlert];
                                    
                                    UIAlertAction *ok = [UIAlertAction actionWithTitle: LOCALIZATION(C_BUTTON_OK) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                         {
                                                             //BUTTON OK CLICK EVENT
                                                         }];
                                    [alert addAction:ok];
                                    [self presentViewController:alert animated:YES completion:nil];
                                }
                            }];
}


-(NSString *) getCountryCode
{
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
    {
        return @"th";
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDIA])
    {
        return @"in";
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDONESIA])
    {
        return @"id";
    }
    else if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_SAUDI])
    {
        return @"sa";
    }
    else
    {
        return @"eu";
    }
}

- (UIImage*) rotateImage:(UIImage* )originalImage {
    
    UIImageOrientation orientation = originalImage.imageOrientation;
    
    UIGraphicsBeginImageContext(originalImage.size);
    
    [originalImage drawAtPoint:CGPointMake(0, 0)];
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orientation == UIImageOrientationRight) {
        CGContextRotateCTM (context, [self radians:90]);
    } else if (orientation == UIImageOrientationLeft) {
        CGContextRotateCTM (context, [self radians:90]);
    } else if (orientation == UIImageOrientationDown) {
        // NOTHING
    } else if (orientation == UIImageOrientationUp) {
        CGContextRotateCTM (context, [self radians:0]);
    }
    
    return UIGraphicsGetImageFromCurrentImageContext();
    
}

- (CGFloat) radians:(int)degree {
    return (degree/180)*(22/7);
}

- (UIImage *)imageWithImage:(UIImage *)image
{
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    CGSize newSize = image.size;
    
    CGFloat aspectRatio = newSize.width / newSize.height;
    
    if (newSize.width >= newSize.height)
    {
        //landscape or square, set width to 1280
        newSize.width = 1280;
        newSize.height = newSize.width * aspectRatio;
    }
    else
    {
        //portrait
        newSize.height = 1280;
        newSize.width = newSize.height * aspectRatio;
    }
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
