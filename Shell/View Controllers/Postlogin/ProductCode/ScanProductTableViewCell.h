//
//  ScanProductTableViewCell.h
//  Shell
//
//  Created by Admin on 29/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScanProductTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
-(void)setID:(NSString *)value;

-(void)setScannedCode:(NSString*)value;

-(void)setPendingStatus:(NSString*)value;

@end
