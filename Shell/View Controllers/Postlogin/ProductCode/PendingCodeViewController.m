//
//  PendingCodeViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 2/9/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "PendingCodeViewController.h"

@interface PendingCodeViewController () <UITableViewDelegate, UITableViewDataSource, WebServiceManagerDelegate>
@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UITableView *pendingCodeTableView;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnClear;

@property NSMutableArray *codesArray;
@property NSMutableArray *selloutArray;
@end

@implementation PendingCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.codesArray = [[NSMutableArray alloc] init];
    self.selloutArray = [[NSMutableArray alloc] init];
    [self setupInterface];
    [self processScannedCodes];
    
    if (kIsRightToLeft) {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
}

-(void) setupInterface
{
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_PENDINGCODE) subtitle: @""];          //lokalise 24 Jan

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_TITLE_PENDINGCODE),  LOCALIZATION(C_REWARDS_BACK)];      //lokalise 24 Jan
    
    
    self.btnSubmit.titleLabel.font = FONT_BUTTON;
    [self.btnSubmit setTitle:LOCALIZATION(C_FORM_SUBMIT) forState:UIControlStateNormal];        //lokalised
    [self.btnSubmit setBackgroundColor:COLOUR_RED];
    self.btnClear.titleLabel.font = FONT_BUTTON;
    [self.btnClear setTitle:LOCALIZATION(C_FORM_CLEAR) forState:UIControlStateNormal];          //lokalise 24 Jan
    [self.btnClear setBackgroundColor:COLOUR_RED];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    if(self.isScanBottle) {
        [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_QL_SCANBOTTLE),LOCALIZATION_EN(C_TITLE_PENDINGCODE)] screenClass:nil];
    } else {
        [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_PENDINGCODE) screenClass:nil];
    }
}

-(void) processScannedCodes
{
    NSArray *savedVehicles; //vehicle
    
    if(self.isScanBottle) {
        savedVehicles = GET_SCANNEDCODES_IN;
        
        for (NSDictionary *vehicle in savedVehicles)
        {
            NSArray *couponArray = [vehicle objectForKey: @"Coupons"];
            for (int i = 0; i < [couponArray count]; i++)
            {
                NSString *codeString = [[couponArray objectAtIndex: i] objectForKey: @"Code"];
                if(self.forConsumer && [[vehicle objectForKey:@"Isconsumer"] boolValue]) {
                    [self.codesArray addObject: @{@"SHARECode": [vehicle objectForKey:  @"VehicleNumber"],
                                                  @"Code": codeString,
                                                  @"Date": [vehicle objectForKey: @"Date"]
                    }];
                    [self.selloutArray addObject:[couponArray objectAtIndex:i]];
                } else {
                    [self.codesArray addObject: @{@"SHARECode": [vehicle objectForKey:  @"SHARECode"],
                                                  @"Code": codeString,
                                                  @"Date": [vehicle objectForKey: @"Date"]
                    }];
                    [self.selloutArray addObject:[couponArray objectAtIndex:i]];
                }
            }
        }
        
    } else {
        savedVehicles = GET_SCANNEDCODES;
        
        for (NSDictionary *vehicle in savedVehicles)
        {
            NSArray *couponArray = [vehicle objectForKey: @"Coupons"];
            for (int i = 0; i < [couponArray count]; i++)
            {
                NSString *codeString = [[couponArray objectAtIndex: i] objectForKey: @"Code"];
                [self.codesArray addObject: @{@"VehicleNumber": [vehicle objectForKey:  @"VehicleNumber"],
                                              @"Mileage": [vehicle objectForKey: @"Mileage"],
                                              @"Code": codeString,
                                              @"Date": [vehicle objectForKey: @"Date"]
                                              }];
            }
        }
        
    }
    
    
    [self.pendingCodeTableView reloadData];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (indexPath.row == 0) //header
    {
        if(self.isScanBottle) {
             cell = [tableView dequeueReusableCellWithIdentifier:@"INPendingCodeHeader"];
        } else {
             cell = [tableView dequeueReusableCellWithIdentifier:@"PendingCodeHeader"];
        }
        
        UILabel *lblNo = [cell viewWithTag: 1];
        lblNo.font = FONT_H2;
        lblNo.text = LOCALIZATION(C_TABLE_NO);
        
        UILabel *lblDate = [cell viewWithTag: 2];
        lblDate.font = lblNo.font;
        lblDate.text = LOCALIZATION(C_POINTS_DATE);
        
        UILabel *lblVehicleId = [cell viewWithTag: 3];
        lblVehicleId.font = lblNo.font;
        lblVehicleId.text = LOCALIZATION(C_SCANBOTTLE_IDORCODE);
        
        UILabel *lblCode = [cell viewWithTag: 5];
        lblCode.font = lblNo.font;
        lblCode.text = LOCALIZATION(C_PROMO_SCANNEDCODE);
        
        if(kIsRightToLeft) {
            [lblNo setTextAlignment:NSTextAlignmentRight];
            [lblDate setTextAlignment:NSTextAlignmentRight];
            [lblVehicleId setTextAlignment:NSTextAlignmentRight];
            [lblCode setTextAlignment:NSTextAlignmentRight];
        } else {
            [lblNo setTextAlignment:NSTextAlignmentLeft];
            [lblDate setTextAlignment:NSTextAlignmentLeft];
            [lblVehicleId setTextAlignment:NSTextAlignmentLeft];
            [lblCode setTextAlignment:NSTextAlignmentLeft];
        }
        
        if(!self.isScanBottle) {
            UILabel *lblMileage = [cell viewWithTag: 4];
            lblMileage.font = lblNo.font;
            lblMileage.text = LOCALIZATION(C_SEARCHVEHICLE_MILEAGE);
            lblVehicleId.text = LOCALIZATION(C_RESULT_VEHICLEID);
            if(kIsRightToLeft) {
                [lblMileage setTextAlignment:NSTextAlignmentRight];
            } else {
                [lblMileage setTextAlignment:NSTextAlignmentLeft];
            }
        }
    }
    else
    {
        if(self.isScanBottle) {
             cell = [tableView dequeueReusableCellWithIdentifier:@"INPendingCodeCell"];
        } else {
             cell = [tableView dequeueReusableCellWithIdentifier:@"PendingCodeCell"];
        }
        
        NSDictionary *vehicleData = [self.codesArray objectAtIndex: indexPath.row - 1];
        
        UILabel *lblNo = [cell viewWithTag: 1];
        lblNo.font = FONT_B2;
        lblNo.text = @(indexPath.row).stringValue;
        
        UILabel *lblDate = [cell viewWithTag: 2];
        lblDate.font = lblNo.font;
        lblDate.text = [vehicleData objectForKey: @"Date"];
        
        UILabel *lblVehicleId = [cell viewWithTag: 3];
        lblVehicleId.font = lblNo.font;
        lblVehicleId.text = [vehicleData objectForKey: @"SHARECode"];
        
        UILabel *lblCode = [cell viewWithTag: 5];
        lblCode.font = lblNo.font;
        lblCode.text = [vehicleData objectForKey: @"Code"];
        
        if(kIsRightToLeft) {
            [lblNo setTextAlignment:NSTextAlignmentRight];
            [lblDate setTextAlignment:NSTextAlignmentRight];
            [lblVehicleId setTextAlignment:NSTextAlignmentRight];
            [lblCode setTextAlignment:NSTextAlignmentRight];
        } else {
            [lblNo setTextAlignment:NSTextAlignmentLeft];
            [lblDate setTextAlignment:NSTextAlignmentLeft];
            [lblVehicleId setTextAlignment:NSTextAlignmentLeft];
            [lblCode setTextAlignment:NSTextAlignmentLeft];
        }
        
        if(!self.isScanBottle) {
            UILabel *lblMileage = [cell viewWithTag: 4];
            lblMileage.font = lblNo.font;
            lblMileage.text = [vehicleData objectForKey: @"Mileage"];
            if(kIsRightToLeft) {
                [lblMileage setTextAlignment:NSTextAlignmentRight];
            } else {
                [lblMileage setTextAlignment:NSTextAlignmentLeft];
            }
        }
        
    }
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.codesArray count] + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (IBAction)submitPressed:(id)sender {
    
    if(self.isScanBottle) {
        if([self.selloutArray count] > 0) {
            NSMutableDictionary *submitCodeDict = [[NSMutableDictionary alloc] init];
            
            [submitCodeDict setObject:@[@{
                                   @"Coupons"          : self.selloutArray,
                                   @"VehicleNumber"    : [[GET_SCANNEDCODES_IN objectAtIndex:0] objectForKey: @"VehicleNumber"],
                                   @"StateID"          : @""                       ,
                                   }]
                       forKey:@"VehicleListForSellout"];
            [submitCodeDict setObject:@(self.forConsumer) forKey:@"Isconsumer"];
            [submitCodeDict setObject:[[GET_SCANNEDCODES_IN objectAtIndex:0] objectForKey: @"SHARECode"] forKey:@"Sharecode"];
            
            [[WebServiceManager sharedInstance] submitCodesForSellout:submitCodeDict vc:self];
        }
    } else {
        if(GET_SCANNEDCODES) {
            [[WebServiceManager sharedInstance] submitCode: @{@"VehicleList": GET_SCANNEDCODES}
            vc:self];
        }
    }
    
}


- (IBAction)clearPressed:(id)sender {
    
    if(self.isScanBottle) {
        CLEAR_SCANNEDCODES_IN;
        [self.selloutArray removeAllObjects];
    } else {
        CLEAR_SCANNEDCODES;
    }
    [self.codesArray removeAllObjects];
    [self processScannedCodes];
}


- (IBAction)backPressed:(id)sender {
    //only available prelogin
    [self popSelf];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_SUBMITCODE:
        {
            [mSession pushRegisterCodeResult: [response getGenericResponse] vc:self];
            [self.codesArray removeAllObjects];
            CLEAR_SCANNEDCODES;//clear codes from user defaults. code has been submitted to server successfully
            [self processScannedCodes];
        }
            break;
        case kWEBSERVICE_SUBMITCODE_SELLOUT:{
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                if(self.forConsumer) {
                    [mSession pushScanBottleResult:[response getGenericResponse] isScanBottle:YES isConsumer:YES vc:self];
                } else {
                    [mSession pushScanBottleResult:[response getGenericResponse] isScanBottle:YES isConsumer:NO vc:self];
                }
                [self.codesArray removeAllObjects];
                [self.selloutArray removeAllObjects];
                CLEAR_SCANNEDCODES_IN;//clear codes from user defaults. code has been submitted to server successfully
                [self processScannedCodes];
            }
        }
            break;
        default:
            break;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
