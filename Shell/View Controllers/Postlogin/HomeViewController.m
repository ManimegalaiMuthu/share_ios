//
//  HomeViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 19/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "HomeViewController.h"
#import "ProfileInfo.h"
#import "LoyaltyProgramGaugeView.h"
#import "LoyaltyBreakdownView.h"
#import "CVSpeedoMeterView.h"
#import "HXPromotionView.h"
#import "RUProgressBarView.h"
#import "RUPartnersBarView.h"
#import <WebKit/WebKit.h>


@interface HomeViewController () <WebServiceManagerDelegate, UIScrollViewDelegate, UICollectionViewDataSource, WKNavigationDelegate/*, QRCodeScannerDelegate*/>

//@property (weak, nonatomic) IBOutlet UIScrollView *carouselScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *carouselPageControl;

@property (strong, nonatomic) IBOutlet UICollectionView *carouselColView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *carouselColViewLayout;

@property NSMutableArray *carouselEnumArray;
@property NSMutableDictionary *carouselProfileDict;
@property NSMutableDictionary *carouselPointsDict;
@property NSMutableDictionary *carouselPerformanceDict;
@property NSMutableDictionary *carouselPartnerDict;
@property NSMutableDictionary *carouselPartnerDictB2B;
@property NSMutableDictionary *carouselPartnerDictB2C;
@property NSMutableDictionary *carouselPointsDictB2B;
@property NSMutableDictionary *carouselPointsDictB2C;

@property (weak, nonatomic) IBOutlet UIView *acctDeactivatedView;
@property (weak, nonatomic) IBOutlet UILabel *lblAcctDeactivate;

typedef enum
{
    kCAROUSEL_PROFILE,
    kCAROUSEL_POINTS,
    kCAROUSEL_FASHAREBADGE,
    kCAROUSEL_MYPERFORMANCE,
    kCAROUSEL_OUTLETPERFORMANCE,
    kCAROUSEL_SHAREBADGE,
    kCAROUSEL_LOYALTYPROGRAM,
    kCAROUSEL_LOYALTYBREAKDOWN,
    kCAROUSEL_CNYPROMOTION,
    kCAROUSEL_CVSPEEDOMETER,
    kCAROUSEL_AGRIPROMOTION,
    kCAROUSEL_HXPROMOTION,
    //Russia
    kCAROUSEL_RU_POINTS_B2C,
    kCAROUSEL_PARTNERS_B2C,
    kCAROUSEL_PARTNERSENDED_B2C,
    kCAROUSEL_RU_POINTS_B2B,
    kCAROUSEL_PARTNERS_B2B,
    kCAROUSEL_PARTNERSENDED_B2B,
    kCAROUSEL_DSRPARTNERS,
    
} kCAROUSEL_TYPES;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


@property (weak, nonatomic) IBOutlet UILabel *lblQuickLinksHeader;
@property NSMutableArray *quickLinksEnumArray;
@property NSMutableArray *quickLinksArray;
@property (weak, nonatomic) IBOutlet UICollectionView *quickLinksColView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *quickLinkColViewLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *quickLinksColViewHeight;

//survey popup
@property (strong, nonatomic) IBOutlet UIView *surveyView;
@property (weak, nonatomic) IBOutlet UIButton *surveyCloseButton;


//in-appnotification popup
@property WebViewHelper *webViewHelper;
@property (strong, nonatomic) IBOutlet UIView *inAppNotificationView;
@property (weak, nonatomic) IBOutlet UIView *inAppWebViewContainer;
@property (weak, nonatomic) IBOutlet UIButton *btnInAppReadMore;
@property (weak, nonatomic) IBOutlet UIButton *btnInAppClose;
@property NSMutableArray *inAppIdArray;

@property (strong, nonatomic) IBOutlet UIButton *btnNotification;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *carouselColViewHeight;
@property NSDictionary *loyaltyPerformanceCarouselDetails;

@property (weak, nonatomic) IBOutlet UIView *callButtonView;
@property (weak, nonatomic) IBOutlet UIImageView *callButtonView_Image;
@property (weak, nonatomic) IBOutlet UILabel *callButtonView_Label;

@property (strong, nonatomic) IBOutlet UIView *notificationView;
@property (weak, nonatomic) IBOutlet UILabel *notificationView_Label;

@property NSInteger currentPage_Carousel;
@property float timerInterval;
@property BOOL idleWait;
@property BOOL isAutoScrollEnabled;
@property BOOL isShowingNextInApp;

@property BOOL isPromotionRedeemed;
@property NSMutableDictionary *promotionDictionary;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    //QRCode Delegate
//    mQRCodeScanner.delegate = self;
    self.timerInterval = 10.0;
    self.idleWait = false;
    
    if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_INDONESIA] || [GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_INDIA] || [GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_RUSSIA]) {
        self.carouselColViewLayout.itemSize = CGSizeMake(SCREEN_WIDTH, 320.0);
        self.carouselColViewHeight.constant = 320.0;
    } else {
        self.carouselColViewLayout.itemSize = CGSizeMake(SCREEN_WIDTH, 280.0);
        self.carouselColViewHeight.constant = 280.0;
    }
    
    //Language Change Notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveLanguageChangedNotification:)
                                                 name:kNotificationLanguageChanged
                                               object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    
     
    [FIRAnalytics setScreenName: LOCALIZATION_EN(C_TITLE_HOME) screenClass: nil];
}

-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.isAutoScrollEnabled = false; //Stopping the autoScroll
    self.idleWait = true;
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_HOME) subtitle: @""]; //lokalised 16 Jan
    
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
//        if (GET_CONSUMER_TNC)
//            [[WebServiceManager sharedInstance] requestConsumerTNC: [GET_CONSUMER_TNC objectForKey: @"Version"]
//                                                              view:nil];
//        else
//            [[WebServiceManager sharedInstance] requestConsumerTNC: @""
//                                                              view:nil];
        
        if (GET_WORKSHOP_TNC)
            [[WebServiceManager sharedInstance] requestWorkshopTNC: [GET_WORKSHOP_TNC objectForKey: @"Version"]
                                                              view:nil];
        else
            [[WebServiceManager sharedInstance] requestWorkshopTNC: @""
                                                              view:nil];
        
        if (GET_DSR_TNC)
            [[WebServiceManager sharedInstance] requestDSRTNC: [GET_DSR_TNC objectForKey: @"Version"]
                                                         view:nil];
        else
            [[WebServiceManager sharedInstance] requestDSRTNC: @""
                                                         view:nil];
    }
    
    [[WebServiceManager sharedInstance] fetchHome: self];
    
    NSLog(@"%@", [mSession lookupTable]);
    
    if (IS_COUNTRY(COUNTRYCODE_UNITEDKINGDOM)) //if UK hide button
        self.callButtonView.hidden = YES;
    else if ([[[mSession lookupTable] objectForKey: @"ContactUSCSNumber"] isKindOfClass: [NSNull class]])
        self.callButtonView.hidden = YES;
    else
        self.callButtonView.hidden = NO;
    
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        self.callButtonView.hidden = NO;
    }
    
    if (GET_ISADVANCE) {
        self.callButtonView_Image.image = [UIImage imageNamed:@"btn_callcs_blue"];
    } else if (kIsRightToLeft) {
        self.callButtonView_Image.image = [UIImage imageNamed:@"btn_callcs_red_flip"];
    } else {
        self.callButtonView_Image.image = [UIImage imageNamed:@"btn_callcs_red"];
    }
    self.callButtonView_Label.text = LOCALIZATION(C_HOME_CUSTOMERSERVICE);
    self.callButtonView_Label.font = FONT_B2;
    self.callButtonView_Label.textColor = COLOUR_WHITE;
    
    [self.callButtonView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callPressed:)]];
    
    [self setupInterface];
    [self setupLocalisation];
    
}

typedef enum
{
    kQUICKLINK_DSRMANAGEWORKSHOP = 1000,
    kQUICKLINK_DSRMANAGEORDERS,
    kQUICKLINK_DSRWORKSHOPPERFORMANCE,
    kQUICKLINK_MYNETWORK,
    kQUICKLINK_DSROILDCHANGEREGISTRATION,
    kQUICKLINK_MYLIBRARY,
    kQUICKLINK_SEARCHVEHICLEID,
    kQUICKLINK_MYREWARDS,
    kQUICKLINK_MYPERFORMANCE,
    kQUICKLINK_OILCHANGEWALKTHROUGH,
    
    
    kQUICKLINK_MANAGEWORKSHOP       = 100,
    kQUICKLINK_PARTICIPATINGPRODUCT,
    kQUICKLINK_TARGET,
    kQUICKLINK_MANAGEORDERS,
    kQUICKLINK_PROMOTIONS,
    kQUICKLINK_SALESKIT,
    kQUICKLINK_MYWORKSHOP,
    kQUICKLINK_REGISTERPRODUCTCODE,
    kQUICKLINK_CUSTOMERREDEMPTION,
    
    kQUICKLINK_INVITECUSTOMER,
    kQUICKLINK_TRADEMANAGEORDERS,
    kQUICKLINK_SCANDECAL,
    kQUICKLINK_ACTIVATEDECAL,
    kQUICKLINK_WORKSHOPPROFILING,
    kQUICKLINK_LOYALTYREWARD,
    
    kQUICKLINK_OILCHANGEREGISTRATION,
    
    kQUICKLINK_IN_MECHPERFORMANCE,
    kQUICKLINK_IN_MANAGEMECHANICS,
    
    kQUICKLINK_WORKSHOPOFFERS,
    kQUICKLINK_CASHINCENTIVES,
    kQUICKLINKS_SCANBOTTLE,
    
    kQUICKLINK_SDA,
    kQUICKLINK_SWA,
    
    //Russia
    kQUICKLINK_PARTNERSOFSHELL,
    kQUICKLINK_RU_WORKSHOPPERFORMANCE,
    
} kQUICKLINK_TYPES;

-(void) setupInterface
{
    
    self.carouselPageControl.pageIndicatorTintColor = COLOUR_VERYPALEGREY;
    self.carouselPageControl.currentPageIndicatorTintColor = COLOUR_RED;
    
#pragma mark in App notification interface
    
    self.btnInAppReadMore.titleLabel.font = FONT_BUTTON;
    [self.btnInAppReadMore setTitle: LOCALIZATION(C_NOTIFICATION_READMORE) forState: UIControlStateNormal];   //lokalised 16 Jan
    self.btnInAppReadMore.backgroundColor = COLOUR_RED;
    
    self.btnInAppClose.titleLabel.font = FONT_BUTTON;
    [self.btnInAppClose setTitle: LOCALIZATION(C_NOTIFICATION_CLOSE) forState: UIControlStateNormal];   //lokalised 16 Jan
    self.btnInAppClose.backgroundColor = COLOUR_YELLOW;
}
-(void) setupNewQuickLinksMenu
{
    self.quickLinksEnumArray = [[NSMutableArray alloc] init];
    self.quickLinksArray = [[NSMutableArray alloc] init];
    
    
    switch (GET_PROFILETYPE)
    {
        case kPROFILETYPE_DSR:
            
            if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_UNITEDKINGDOM] )//if UK show menu
            {
                [self.quickLinksEnumArray addObject: @(kQUICKLINK_WORKSHOPPROFILING)];
                [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_TITLE_APPROVEDWORKSHOPPROFILING),  //lokalised
                                                   @"Icon": @"sideicon-manageworkshop",
                                                   }];
            }
            else if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_INDIA])
            {
                [self setupDSRQuickLinksIndia];
            }
            else
                [self setupDSRQuickLinks];
            break;
        case kPROFILETYPE_TRADEOWNER:
            if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_INDIA])
            {
                [self setupTOQuickLinksIndia];
            }
            else
                [self setupTOQuickLinks];
            break;
        case kPROFILETYPE_MECHANIC:
            [self setupMechQuickLinks];
            break;
        case kPROFILETYPE_FORECOURTATTENDANT:
            [self setupFAQuickLinks];
            break;
        case kPROFILETYPE_DMR:
            [self setupDMRQuickLinks];
            break;
        case kPROFILETYPE_DSM:
            [self setupDSMQuickLinks];
            break;
        default:
            break;
    }
    
    self.quickLinkColViewLayout.itemSize = CGSizeMake((self.quickLinksColView.frame.size.width - 30) / 3, 110);
    self.quickLinksColViewHeight.constant = 110 * ceil([self.quickLinksArray count] / 3.0f);
    [self.quickLinksColView reloadData];
    
    if ([self.quickLinksArray count] == 0)
        self.lblQuickLinksHeader.hidden = YES;
    else
        self.lblQuickLinksHeader.hidden = NO;
}

-(void) setupLocalisation
{
    self.lblQuickLinksHeader.text = LOCALIZATION(C_HOME_QUICKLINKS);   //lokalised 16 Jan
    self.lblQuickLinksHeader.font = FONT_H1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.quickLinksColView)
        return [self.quickLinksArray count];
    else if (collectionView == self.carouselColView)
        return [self.carouselEnumArray count];
    return 0;
}

typedef enum
{
    kCAROUSEL_PROFILE_HEADER = 1,
    kCAROUSEL_PROFILE_NAME,
    kCAROUSEL_PROFILE_COMPANYNAME,
    
    kCAROUSEL_PROFILE_COMPANYTIERICON,
    kCAROUSEL_PROFILE_COMPANYTIERTEXT,
    
    kCAROUSEL_PROFILE_BUTTON = 999,
} kCAROUSEL_PROFILE_TAG;

typedef enum
{
    kCAROUSEL_FASHAREBADGE_NAME_HEADER = 1,
    kCAROUSEL_FASHAREBADGE_NAME,
    kCAROUSEL_FASHAREBADGE_SHAREBADGEID_HEADER,
    kCAROUSEL_FASHAREBADGE_SHAREBADGEID,
    kCAROUSEL_FASHAREBADGE_IMAGE,
    kCAROUSEL_FASHAREBADGE_FOOTER = 6,
    
    kCAROUSEL_FASHAREBADGE_BADGEVIEW = 1001,
} kCAROUSEL_FASHAREBADGE_TAG;

typedef enum
{
    kCAROUSEL_POINTS_AVAILABLE_HEADER = 1,
    kCAROUSEL_POINTS_AVAILABLE_POINTS,
    
    kCAROUSEL_POINTS_EXPIRING_HEADER    = 3,
    kCAROUSEL_POINTS_FIRSTDATE          = 10, //storyboard: jun 18
    kCAROUSEL_POINTS_SECONDDATE, //storyboard: jul 18
    kCAROUSEL_POINTS_THIRDDATE,  //storyboard: aug 18
    
    kCAROUSEL_POINTS_FIRSTPOINTS        = 20,
    kCAROUSEL_POINTS_SECONDPOINTS,
    kCAROUSEL_POINTS_THIRDPOINTS,
    
    kCAROUSEL_POINTS_BUTTON = 999,
} kCAROUSEL_POINTS_TAG;

typedef enum
{
    kCAROUSEL_PERFORMANCE_HEADER = 1,
    kCAROUSEL_PERFORMANCE_FIRSTKEY      = 101,
    kCAROUSEL_PERFORMANCE_FIRSTDATA,
    kCAROUSEL_PERFORMANCE_FIRSTSEPARATOR,
    kCAROUSEL_PERFORMANCE_SECONDKEY     = 104,
    kCAROUSEL_PERFORMANCE_SECONDDATA,
    kCAROUSEL_PERFORMANCE_SECONDSEPARATOR,
    kCAROUSEL_PERFORMANCE_THIRDKEY      = 107,
    kCAROUSEL_PERFORMANCE_THIRDDATA,
    
    kCAROUSEL_PERFORMANCE_BUTTON = 999,
} kCAROUSEL_PERFORMANCE_TAG;

typedef enum {
    kCAROUSEL_MYPERFORMANCE_HEADER = 1,
    kCAROUSEL_MYPERFORMANCE_SUBHEADER,
    
    kCAROUSEL_MYPERFORMANCE_FIRSTKEY      = 101,
    kCAROUSEL_MYPERFORMANCE_FIRSTDATA,
    kCAROUSEL_MYPERFORMANCE_FIRSTSEPARATOR,
    kCAROUSEL_MYPERFORMANCE_SECONDKEY     = 104,
    kCAROUSEL_MYPERFORMANCE_SECONDDATA,
    kCAROUSEL_MYPERFORMANCE_SECONDSEPARATOR,
    kCAROUSEL_MYPERFORMANCE_THIRDKEY      = 107,
    kCAROUSEL_MYPERFORMANCE_THIRDDATA,
    
    kCAROUSEL_MYPERFORMANCE_BUTTON = 999,
    
}kCAROUSEL_MYPERFORMANCE_TAG;


typedef enum {
    kCAROUSEL_OUTLETPERFORMANCE_HEADER = 1,
    kCAROUSEL_OUTLETPERFORMANCE_SUBHEADER,
    
    kCAROUSEL_OUTLETPERFORMANCE_FIRSTKEY      = 101,
    kCAROUSEL_OUTLETPERFORMANCE_FIRSTDATA,
    kCAROUSEL_OUTLETPERFORMANCE_FIRSTSEPARATOR,
    kCAROUSEL_OUTLETPERFORMANCE_SECONDKEY     = 104,
    kCAROUSEL_OUTLETPERFORMANCE_SECONDDATA,
    kCAROUSEL_OUTLETPERFORMANCE_SECONDSEPARATOR,
    kCAROUSEL_OUTLETPERFORMANCE_THIRDKEY      = 107,
    kCAROUSEL_OUTLETPERFORMANCE_THIRDDATA,
    
    kCAROUSEL_OUTLETPERFORMANCE_BUTTON = 900,
    
}kCAROUSEL_OUTLETPERFORMANCE_TAG;

typedef enum {
    
    kCAROUSEL_LOYALTYPROGRAM_HEADER = 1,
    kCAROUSEL_LOYALTYPROGRAM_PERIOD,
    kCAROUSEL_LOYALTYPROGRAM_DISCLAIMERTEXT,
    kCAROUSEL_LOYALTYPROGRAM_GAUGEVIEW,
    
    kCAROUSEL_LOYALTYPROGRAM_FOOTERTEXT = 699,
    
    kCAROUSEL_LOYALTYPROGRAM_BUTTON = 990,
    
}kCAROUSEL_LOYALTYPROGRAM_TAG;

typedef enum {
    
    kCAROUSEL_LOYALTYBREAKDOWN_HEADER = 1,
    kCAROUSEL_LOYALTYBREAKDOWN_PERIOD,
    kCAROUSEL_LOYALTYBREAKDOWN_DISCLAIMERTEXT,
    kCAROUSEL_LOYALTYBREAKDOWN_BARVIEW,
    
    
    kCAROUSEL_LOYALTYBREAKDOWN_BUTTON = 990,
    
}kCAROUSEL_LOYALTYBREAKDOWN_TAG;

typedef enum {
    kCAROUSEL_CNYPROMOTION_IMAGE = 699,
    
    kCAROUSEL_CNYPROMOTION_HEADER = 1,
    kCAROUSEL_CNYPROMOTION_PERIOD,
    kCAROUSEL_CNYPROMOTION_FOOTERTEXT,
    kCAROUSEL_CNYPROMOTION_DAYSLEFTVIEW,
    kCAROUSEL_CNYPROMOTION_DAYSLEFTLABEL,
    
    kCAROUSEL_CNYPROMOTION_BUTTON = 990,
    
    kCAROUSEL_CNYPROMOTION_REDEEMBUTTON = 1399,
    kCAROUSEL_CNYPROMOTION_REDEEMMESSAGELABEL,
}kCAROUSEL_CNYPROMOTION_TAG;

typedef enum {
    
    kCAROUSEL_CVSPEEDOMETER_HEADER = 1,
    kCAROUSEL_CVSPEEDOMETER_PERIOD,
    kCAROUSEL_CVSPEEDOMETER_DATAUPDATEDLABEL,
    
    kCAROUSEL_CVSPEEDOMETER_GAUGEVIEW = 4,
    
    kCAROUSEL_CVSPEEDOMETER_FOOTERTEXT = 699,
    
    kCAROUSEL_CVSPEEDOMETER_BUTTON = 990,
    
}kCAROUSEL_CVSPEEDOMETER_TAG;

typedef enum {
    
    kCAROUSEL_AGRIPROMO_HEADER = 1,
    kCAROUSEL_AGRIPROMO_PERIOD,
    kCAROUSEL_AGRIPROMO_DATAUPDATEDLABEL,
    kCAROUSEL_AGRIPROMO_GAUGEVIEW = 4,
    
    kCAROUSEL_AGRIPROMO_FOOTERTEXT = 699,
    
    kCAROUSEL_AGRIPROMO_BUTTON = 990,
    
}kCAROUSEL_AGRIPROMO_TAG;

typedef enum {
    
    kCAROUSEL_HXSPEEDOMETER_HEADER = 1,
    kCAROUSEL_HXSPEEDOMETER_PERIOD,
    kCAROUSEL_HXSPEEDOMETER_DATAUPDATEDLABEL,
    
    kCAROUSEL_HXSPEEDOMETER_GAUGEVIEW = 4,
    
    kCAROUSEL_HXSPEEDOMETER_FOOTERTEXT = 699,
    
    kCAROUSEL_HXSPEEDOMETER_BUTTON = 990,
    
}kCAROUSEL_HXSPEEDOMETER_TAG;

typedef enum
{
    kCAROUSEL_PARTNERS_HEADER         = 1,
    kCAROUSEL_PARTNERS_SUBHEADER,
    
    kCAROUSEL_PARTNERS_PROGRESSBAR  = 300,
    
    kCAROUSEL_PARTNERS_BUTTON = 999,
} kCAROUSEL_PARTNERS_TAG;


typedef enum
{
    kCAROUSEL_RU_POINTS_HEADER         = 1,
    kCAROUSEL_RU_POINTS_SUBHEADER,
    kCAROUSEL_RU_POINTS_FOOTER,
    
    kCAROUSEL_RU_POINTS_BAR            = 200,
    
    kCAROUSEL_RU_POINTS_BAR_MIN        = 5,
    kCAROUSEL_RU_POINTS_BAR_MAX,
    
    kCAROUSEL_RU_POINTS_FIRSTPOINTS        = 101,   //0W
    kCAROUSEL_RU_POINTS_SECONDPOINTS,               //ultra
    kCAROUSEL_RU_POINTS_THIRDPOINTS,                //HX8
    
    kCAROUSEL_RU_POINTS_FIRSTLABEL        = 201,   //0W
    kCAROUSEL_RU_POINTS_SECONDLABEL,               //ultra
    kCAROUSEL_RU_POINTS_THIRDLABEL,                //HX8
    
    
    kCAROUSEL_RU_POINTS_BUTTON = 999,
} kCAROUSEL_RU_POINTS_TAG;

typedef enum
{
    kCAROUSEL_PARTNERS_ENDED_HEADER         = 1,
    kCAROUSEL_PARTNERS_ENDED_ICON,
    kCAROUSEL_PARTNERS_ENDED_CONTENT,
    kCAROUSEL_PARTNERS_ENDED_SUBCONTENT,
    //    kCAROUSEL_PARTNERS_BUTTON = 999,
} kCAROUSEL_PARTNERS_ENDED_TAG;

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    if (collectionView == self.carouselColView)
    {
        kCAROUSEL_TYPES carouselType =  [[self.carouselEnumArray objectAtIndex: indexPath.row] intValue];
        
        switch (carouselType) {
            case kCAROUSEL_HXPROMOTION:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HXPromtionCarouselCell" forIndexPath:indexPath];
                
                NSDictionary *cellData = [GET_HOMEDATA objectForKey:@"HX8Promotion"];
                
                if(![cellData isKindOfClass:[NSNull class]] && cellData != nil) {
                    UILabel *lblNameHeader = [cell viewWithTag:kCAROUSEL_HXSPEEDOMETER_HEADER];
                    lblNameHeader.text =[NSString stringWithFormat:@"%@",[cellData objectForKey:@"HX8PromoHeading"]];
                    lblNameHeader.font = FONT_H1;
                    lblNameHeader.textColor = COLOUR_VERYDARKGREY;
                    
                    UILabel *lblPeriod = [cell viewWithTag:kCAROUSEL_HXSPEEDOMETER_PERIOD];
                    lblPeriod.text = [NSString stringWithFormat:@"%@: %@ - %@",LOCALIZATION(C_LOYALTY_PERIOD),[cellData objectForKey:@"HX8PromoStartDate"],[cellData objectForKey:@"HX8PromoEndDate"]];
                    lblPeriod.font = FONT_B2;
                    lblPeriod.textColor = COLOUR_VERYDARKGREY;
                    
//                    UILabel *lblDataUpdated = [cell viewWithTag:kCAROUSEL_CVSPEEDOMETER_DATAUPDATEDLABEL];
//                    lblDataUpdated.font = FONT_B(12);
//                    lblDataUpdated.textColor = COLOUR_VERYDARKGREY;
//                    lblDataUpdated.text = [NSString stringWithFormat:@"%@ %@",[cellData objectForKey:@"CVUpdatedAsOfText"],[cellData objectForKey:@"CVUpdatedAsOfValue"]];
                    
                    HXPromotionView *gaugeView = [cell viewWithTag:kCAROUSEL_HXSPEEDOMETER_GAUGEVIEW];
                    [gaugeView.superview setNeedsDisplay];
                    [gaugeView loadBar: cellData isCarousel:YES];
                    
                    TTTAttributedLabel *lblFooterText = [cell viewWithTag:kCAROUSEL_HXSPEEDOMETER_FOOTERTEXT];
                    NSString *csString = [NSString stringWithFormat:@"%@",[cellData objectForKey:@"HX8PromoFooterText"]];
                    NSString *footerString;
                    if(SCREEN_WIDTH <= 320) {
                        csString = [csString stringByReplacingOccurrencesOfString:@"<b>" withString:@"<b style=\"color:#DD1D21 ; font-size:14px\">"];
                        footerString = [NSString stringWithFormat:@"<style>body{font-family: 'FuturaStd-Book'; font-size:10; color: #404040;}</style>%@",csString];
                    } else {
                        csString = [csString stringByReplacingOccurrencesOfString:@"<b>" withString:@"<b style=\"color:#DD1D21 ; font-size:15px\">"];
                        footerString = [NSString stringWithFormat:@"<style>body{font-family: 'FuturaStd-Book'; font-size:12; color: #404040;}</style>%@",csString];
                    }
                    lblFooterText.attributedText = [[NSAttributedString alloc] initWithData: [footerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                    
                    lblFooterText.font = FONT_B3;
                    lblFooterText.textColor = COLOUR_VERYDARKGREY;
                    lblFooterText.textAlignment = NSTextAlignmentCenter;
                    [lblFooterText sizeToFit];
                    
                    UIButton *btnView = [cell viewWithTag: kCAROUSEL_HXSPEEDOMETER_BUTTON];
                    if(![[cellData objectForKey:@"HasBtnScanNow"] boolValue]) {
                        [btnView setTitle: LOCALIZATION(C_BTN_VIEWRECENTACTIVITY) forState:UIControlStateNormal];
                        [btnView addTarget:self action:@selector(viewRecentActivityPressed:) forControlEvents:UIControlEventTouchDown];
                    } else {
                        [btnView setTitle: LOCALIZATION(C_BTN_SCANNOW) forState:UIControlStateNormal];
                        [btnView addTarget:self action:@selector(scanNowPressed:) forControlEvents:UIControlEventTouchDown];
                    }
                    btnView.titleLabel.font = FONT_BUTTON;
                    
                    [NSLayoutConstraint constraintWithItem:btnView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:45].active = YES;
                }
                
            }
                break;
            case kCAROUSEL_AGRIPROMOTION:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AgriPromoCarousel" forIndexPath:indexPath];
                
                NSDictionary *cellData = [GET_HOMEDATA objectForKey:@"AgriPromotion"];
                
                if(![cellData isKindOfClass:[NSNull class]] && cellData != nil) {
                    UILabel *lblNameHeader = [cell viewWithTag:kCAROUSEL_AGRIPROMO_HEADER];
                    lblNameHeader.text =[NSString stringWithFormat:@"%@",[cellData objectForKey:@"AgriHeading"]];
                    lblNameHeader.font = FONT_H1;
                    lblNameHeader.textColor = COLOUR_VERYDARKGREY;
                    
                    UILabel *lblPeriod = [cell viewWithTag:kCAROUSEL_AGRIPROMO_PERIOD];
                    lblPeriod.text = [NSString stringWithFormat:@"%@: %@ - %@",LOCALIZATION(C_LOYALTY_PERIOD),[cellData objectForKey:@"AgriStartDate"],[cellData objectForKey:@"AgriEndDate"]];
                    lblPeriod.font = FONT_B2;
                    lblPeriod.textColor = COLOUR_VERYDARKGREY;
                    
                    UILabel *lblDataUpdated = [cell viewWithTag:kCAROUSEL_AGRIPROMO_DATAUPDATEDLABEL];
                    lblDataUpdated.font = FONT_B(12);
                    lblDataUpdated.textColor = COLOUR_VERYDARKGREY;
                    lblDataUpdated.text = [NSString stringWithFormat:@"%@ %@",[cellData objectForKey:@"AgriUpdatedAsOfText"],[cellData objectForKey:@"AgriUpdatedAsOfValue"]];
                    
                    NSDictionary *viewData = @{
                                               @"TextTotalCarton" : @"R1/R3 Oil Changes",
                                               @"TextPremiumTier" : @"R3 Oil Changes",
                                               @"ColorCodeDefaultOverall" : @"#A6A6A6",
                                               @"ColorCodeDefaultPremium" : @"#D9D9D9",
                                               @"NoOfOverallOrdered" : [cellData objectForKey:@"AgriAchievedTarget"],
                                               @"NoOfPremiumOrdered" : [cellData objectForKey:@"AgriBoosterAchievedTarget"],
                                               @"TotalPremiumTarget" : [cellData objectForKey:@"AgriBoosterTotalTarget"],
                                               @"OverallTarget" : [cellData objectForKey:@"AgriTotalTarget"],
                                               @"ColorCodeOverall" : [cellData objectForKey:@"AgriColorCode"],
                                               @"ColorCodePremium" : [cellData objectForKey:@"AgriBoosterColorCode"],
                                               @"PremiumFirstTarget": [cellData objectForKey:@"AgriBoosterFirstTarget"],
                                               @"PremiumSecondTarget": [cellData objectForKey:@"AgriBoosterSecondTarget"],
                                               @"TotalFirstTarget": [cellData objectForKey:@"AgriFirstTarget"],
                                               @"TotalSecondTarget": [cellData objectForKey:@"AgriSecondTarget"],
                                               };
                    LoyaltyProgramGaugeView *gaugeView = [cell viewWithTag:kCAROUSEL_AGRIPROMO_GAUGEVIEW];
                    [gaugeView.superview setNeedsDisplay];
                    gaugeView.isAgri = true;
                    [gaugeView loadBar: viewData isCarousel:YES];
                    
                    TTTAttributedLabel *lblFooterText = [cell viewWithTag:kCAROUSEL_AGRIPROMO_FOOTERTEXT];
                    NSString *csString = [NSString stringWithFormat:@"%@",[cellData objectForKey:@"AgriFooterText"]];
                    NSString *footerString;
                    if(SCREEN_WIDTH <= 320) {
                        csString = [csString stringByReplacingOccurrencesOfString:@"<b>" withString:@"<b style=\"color:#DD1D21 ; font-size:14px\">"];
                        footerString = [NSString stringWithFormat:@"<style>body{font-family: 'FuturaStd-Book'; font-size:11; color: #404040;}</style>%@",csString];
                    } else {
                        csString = [csString stringByReplacingOccurrencesOfString:@"<b>" withString:@"<b style=\"color:#DD1D21 ; font-size:15px\">"];
                        footerString = [NSString stringWithFormat:@"<style>body{font-family: 'FuturaStd-Book'; font-size:12; color: #404040;}</style>%@",csString];
                    }
                    lblFooterText.attributedText = [[NSAttributedString alloc] initWithData: [footerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                    
                    lblFooterText.font = FONT_B3;
                    lblFooterText.textColor = COLOUR_VERYDARKGREY;
                    lblFooterText.textAlignment = NSTextAlignmentCenter;
                    
                    UIButton *btnView = [cell viewWithTag: kCAROUSEL_CVSPEEDOMETER_BUTTON];
                    if([[cellData objectForKey:@"AgriIsComplete"] boolValue]) {
                        [btnView setTitle: LOCALIZATION(C_BTN_VIEWRECENTACTIVITY) forState:UIControlStateNormal];
                        [btnView addTarget:self action:@selector(viewRecentActivityPressed:) forControlEvents:UIControlEventTouchDown];
                    } else {
                        [btnView setTitle: LOCALIZATION(C_BTN_SCANNOW) forState:UIControlStateNormal];
                        [btnView addTarget:self action:@selector(scanNowPressed:) forControlEvents:UIControlEventTouchDown];
                    }
                    btnView.titleLabel.font = FONT_BUTTON;
                    
                    [NSLayoutConstraint constraintWithItem:btnView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:45].active = YES;
                }
            }
                break;
            case kCAROUSEL_CVSPEEDOMETER:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CVSpeedoMeterCarousel" forIndexPath:indexPath];
                
                NSDictionary *cellData = [GET_HOMEDATA objectForKey:@"CVBoosterPromotion"];
                
                if(![cellData isKindOfClass:[NSNull class]] && cellData != nil) {
                    UILabel *lblNameHeader = [cell viewWithTag:kCAROUSEL_CVSPEEDOMETER_HEADER];
                    lblNameHeader.text =[NSString stringWithFormat:@"%@",[cellData objectForKey:@"CVHeading"]];
                    lblNameHeader.font = FONT_H1;
                    lblNameHeader.textColor = COLOUR_VERYDARKGREY;
                    
                    UILabel *lblPeriod = [cell viewWithTag:kCAROUSEL_CVSPEEDOMETER_PERIOD];
                    lblPeriod.text = [NSString stringWithFormat:@"%@: %@ - %@",LOCALIZATION(C_LOYALTY_PERIOD),[cellData objectForKey:@"CVStartDate"],[cellData objectForKey:@"CVEndDate"]];
                    lblPeriod.font = FONT_B2;
                    lblPeriod.textColor = COLOUR_VERYDARKGREY;
                    
                    UILabel *lblDataUpdated = [cell viewWithTag:kCAROUSEL_CVSPEEDOMETER_DATAUPDATEDLABEL];
                    lblDataUpdated.font = FONT_B(12);
                    lblDataUpdated.textColor = COLOUR_VERYDARKGREY;
                    lblDataUpdated.text = [NSString stringWithFormat:@"%@ %@",[cellData objectForKey:@"CVUpdatedAsOfText"],[cellData objectForKey:@"CVUpdatedAsOfValue"]];
                    
                    CVSpeedoMeterView *gaugeView = [cell viewWithTag:kCAROUSEL_CVSPEEDOMETER_GAUGEVIEW];
                    [gaugeView.superview setNeedsDisplay];
                    [gaugeView loadBar: cellData isCarousel:YES];
                    
                    TTTAttributedLabel *lblFooterText = [cell viewWithTag:kCAROUSEL_CVSPEEDOMETER_FOOTERTEXT];
                    NSString *csString = [NSString stringWithFormat:@"%@",[cellData objectForKey:@"CVFooterText"]];
                    NSString *footerString;
                    if(SCREEN_WIDTH <= 320) {
                        csString = [csString stringByReplacingOccurrencesOfString:@"<b>" withString:@"<b style=\"color:#DD1D21 ; font-size:14px\">"];
                        footerString = [NSString stringWithFormat:@"<style>body{font-family: 'FuturaStd-Book'; font-size:11; color: #404040;}</style>%@",csString];
                    } else {
                        csString = [csString stringByReplacingOccurrencesOfString:@"<b>" withString:@"<b style=\"color:#DD1D21 ; font-size:15px\">"];
                        footerString = [NSString stringWithFormat:@"<style>body{font-family: 'FuturaStd-Book'; font-size:12; color: #404040;}</style>%@",csString];
                    }
                    lblFooterText.attributedText = [[NSAttributedString alloc] initWithData: [footerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                   
                    lblFooterText.font = FONT_B3;
                    lblFooterText.textColor = COLOUR_VERYDARKGREY;
                    lblFooterText.textAlignment = NSTextAlignmentCenter;
                    
                    UIButton *btnView = [cell viewWithTag: kCAROUSEL_CVSPEEDOMETER_BUTTON];
                    if([[cellData objectForKey:@"CVIsComplete"] boolValue]) {
                        [btnView setTitle: LOCALIZATION(C_BTN_VIEWRECENTACTIVITY) forState:UIControlStateNormal];
                        [btnView addTarget:self action:@selector(viewRecentActivityPressed:) forControlEvents:UIControlEventTouchDown];
                    } else {
                        [btnView setTitle: LOCALIZATION(C_BTN_SCANNOW) forState:UIControlStateNormal];
                        [btnView addTarget:self action:@selector(scanNowPressed:) forControlEvents:UIControlEventTouchDown];
                    }
                    btnView.titleLabel.font = FONT_BUTTON;
                    
                    [NSLayoutConstraint constraintWithItem:btnView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:45].active = YES;
                }
                
            }
                break;
            case kCAROUSEL_CNYPROMOTION: {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"IDCNYPromotionCollectionViewCell" forIndexPath:indexPath];
                
                if(!_isPromotionRedeemed) {
                    self.promotionDictionary = [GET_HOMEDATA objectForKey: @"TradesPromotion"];
                }
                NSDictionary *cellData = self.promotionDictionary;
                
                TTTAttributedLabel *redeemMessageLabel = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_REDEEMMESSAGELABEL];
                redeemMessageLabel.hidden = YES;
                
                NSString *imageURL = [[cellData objectForKey:@"TPImageUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
                UIImageView *rewardStatusImage = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_IMAGE];
//                if(_isPromotionRedeemed) {
//                    [NSLayoutConstraint constraintWithItem:rewardStatusImage attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:150].active = YES;
//                }
                [rewardStatusImage sd_setImageWithURL:[NSURL URLWithString: imageURL] placeholderImage:[UIImage imageNamed:@"bg_lebaran_default"] options:SDWebImageRefreshCached | SDWebImageRetryFailed];
                
                UILabel *lblNameHeader = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_HEADER];
                lblNameHeader.text = [NSString stringWithFormat:@"%@",[cellData objectForKey:@"TPCSHeader"]];
                lblNameHeader.font = FONT_H1;
                lblNameHeader.textColor = COLOUR_VERYDARKGREY;
                
                UILabel *lblPeriod = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_PERIOD];
                lblPeriod.text = [NSString stringWithFormat:@"%@: %@ - %@",LOCALIZATION(C_LOYALTY_PERIOD),[cellData objectForKey:@"TPStartDate"],[cellData objectForKey:@"TPEndDate"]];     //lokalised
                lblPeriod.font = FONT_B2;
                lblPeriod.textColor = COLOUR_VERYDARKGREY;
                
                TTTAttributedLabel *lblFooterText = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_FOOTERTEXT];
                if([[cellData objectForKey:@"TPCSFooter"] isEqualToString:@""] || [[cellData objectForKey:@"TPCSFooter"] isKindOfClass:[NSNull class]]) {
                    lblFooterText.hidden = YES;
                } else {
                    lblFooterText.hidden = NO;
                    NSString *footerString = [NSString stringWithFormat:@"<style>body{font-family: 'FuturaStd-Book'; font-size:10; color: #404040;}</style>%@",[cellData objectForKey:@"TPCSFooter"]];
                    lblFooterText.attributedText = [[NSAttributedString alloc] initWithData: [footerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                }
                
                UIView *daysLeftView = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_DAYSLEFTVIEW];
                
                UIButton *btn_carousel = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_BUTTON];
                UIButton *btnRedeem = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_REDEEMBUTTON];
                if([[cellData objectForKey:@"HasTPViewDetailsBtn"] boolValue]) {
                    daysLeftView.hidden = NO;
                    
                    UILabel *lblDaysLeft = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_DAYSLEFTLABEL];
                    NSString *daysLeftString = [LOCALIZATION(C_CAROUSEL_PROMOTION_DAYSLEFT) stringByReplacingOccurrencesOfString:@"XXX" withString: [NSString stringWithFormat:@"%@",[cellData objectForKey:@"TPDaysLeft"]]];        //lokalised
                    NSMutableAttributedString *daysLeftAttrString = [[NSMutableAttributedString alloc]initWithString:daysLeftString attributes:@{
                                                                                                                                                                          NSFontAttributeName : FONT_B3,
                                                                                                                                                                          NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,
                                                                                                                                                                          }];
                    [daysLeftAttrString setAttributes:@{NSFontAttributeName : FONT_H(20), NSForegroundColorAttributeName : COLOUR_VERYDARKGREY} range: [daysLeftString rangeOfString:[NSString stringWithFormat:@"%@",[cellData objectForKey:@"TPDaysLeft"]]]];
                    lblDaysLeft.attributedText = daysLeftAttrString;
                    
                    daysLeftView.layer.cornerRadius = 30;
                    daysLeftView.layer.masksToBounds = YES;
                    daysLeftView.layer.borderWidth = 1;
                    daysLeftView.layer.borderColor = [COLOUR_VERYDARKGREY CGColor];
                    
                    btn_carousel.hidden = NO;
                    [NSLayoutConstraint constraintWithItem:btn_carousel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:45].active = YES;
                    [btn_carousel setTitle:LOCALIZATION(C_FORM_VIEWDETAIL) forState:UIControlStateNormal];      //lokalised
                    
                    [btnRedeem setUserInteractionEnabled:NO];
                    
                } else {
                    daysLeftView.hidden = YES;
                    btn_carousel.hidden = YES;
                    [NSLayoutConstraint constraintWithItem:btn_carousel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
                    
                }
                
//                if([[cellData objectForKey:@"HasTPRedeemBtn"] boolValue]) {
//                    [btnRedeem setUserInteractionEnabled:YES];
//                }
                
                if(![[cellData objectForKey:@"TPFinalMessage1"] isEqualToString:@""] && ![[cellData objectForKey:@"TPFinalMessage1"] isKindOfClass:[NSNull class]]) {
                    redeemMessageLabel.hidden = NO;
                    
                    if(SCREEN_WIDTH <= 320) {
                        [NSLayoutConstraint constraintWithItem:redeemMessageLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:40].active = YES;
                    }
                    
                    NSString *redeemString = [NSString stringWithFormat:@"<style>body{font-family: 'FuturaStd-Book'; font-size:12; color: #404040;}</style>%@",[cellData objectForKey:@"TPFinalMessage1"]];
                    redeemMessageLabel.attributedText = [[NSAttributedString alloc] initWithData: [redeemString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                    [btnRedeem setUserInteractionEnabled:NO];
                } else if(![[cellData objectForKey:@"TPFinalMessage2"] isEqualToString:@""] && ![[cellData objectForKey:@"TPFinalMessage2"] isKindOfClass:[NSNull class]]) {
                    redeemMessageLabel.hidden = NO;
                    
                    [NSLayoutConstraint constraintWithItem:redeemMessageLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0].active = YES;
                    
                    NSString *redeemString = [NSString stringWithFormat:@"<style>body{font-family: 'FuturaStd-Book'; font-size:15; color: #404040;}</style>%@",[cellData objectForKey:@"TPFinalMessage2"]];
                    redeemMessageLabel.attributedText = [[NSAttributedString alloc] initWithData: [redeemString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                    [btnRedeem setUserInteractionEnabled:NO];
                }
                
            }
                break;
            case kCAROUSEL_SHAREBADGE:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FAShareBadgeCell" forIndexPath:indexPath];
                
                UILabel *lblNameHeader = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_NAME_HEADER];
                lblNameHeader.text = LOCALIZATION(C_HOMECAROUSEL_NAME);   //lokalised 16 Jan
                
                UILabel *lblName = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_NAME];
                lblName.text = [[mSession profileInfo] fullName];
                
                UILabel *lblBadgeHeader = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_SHAREBADGEID_HEADER];
                lblBadgeHeader.text = LOCALIZATION(C_HOMECAROUSEL_MECHID);  //lokalised 16 Jan
                
                UILabel *lblBadge = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_SHAREBADGEID];
                lblBadge.text = [[mSession profileInfo] shareTagCode];
                
                
                NSString *urlString = [[[mSession profileInfo] shareQRCode] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
                
                UIImageView *faShareQRImage = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_IMAGE];
                [faShareQRImage sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"icon-code"] options: (SDWebImageRefreshCached | SDWebImageRetryFailed)];
                
                UILabel *lblFooter = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_FOOTER];
                lblFooter.hidden = YES;
                
                UIView *badgeView = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_BADGEVIEW];
                [NSLayoutConstraint constraintWithItem:badgeView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0].active = YES;
            }
                break;
            case kCAROUSEL_LOYALTYPROGRAM:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LoyaltyProgramCarousel" forIndexPath:indexPath];
//                NSDictionary *firstMonth = [[_loyaltyPerformanceCarouselDetails objectForKey:@"MonthlyOverviewList"] objectAtIndex:0];
                
                NSDictionary *cellData = _loyaltyPerformanceCarouselDetails;
                
                if(![cellData isKindOfClass:[NSNull class]] && cellData != nil) {
                    UILabel *lblNameHeader = [cell viewWithTag:kCAROUSEL_LOYALTYPROGRAM_HEADER];
                    lblNameHeader.text = LOCALIZATION(C_LOYALTY_LOYALTYPROGRAM);    //lokalised 7 Feb
                    lblNameHeader.font = FONT_H1;
                    lblNameHeader.textColor = COLOUR_VERYDARKGREY;
                    
                    UILabel *lblPeriod = [cell viewWithTag:kCAROUSEL_LOYALTYPROGRAM_PERIOD];
                    lblPeriod.text = [NSString stringWithFormat:@"%@: %@ - %@",LOCALIZATION(C_LOYALTY_PERIOD),[cellData objectForKey:@"StartDate"],[cellData objectForKey:@"EndDate"]];  //lokalise 8 Feb
                    lblPeriod.font = FONT_B2;
                    lblPeriod.textColor = COLOUR_VERYDARKGREY;
                    
                    UILabel *lblDisclaimerText = [cell viewWithTag:kCAROUSEL_LOYALTYPROGRAM_DISCLAIMERTEXT];
                    lblDisclaimerText.text = [NSString stringWithFormat:@"%@", [cellData objectForKey:@"TextCSHeaderMessage"]];
                    lblDisclaimerText.font = FONT_B3;
                    lblDisclaimerText.textColor = COLOUR_VERYDARKGREY;
                    
                    LoyaltyProgramGaugeView *gaugeView = [cell viewWithTag:kCAROUSEL_LOYALTYPROGRAM_GAUGEVIEW];
                    [gaugeView.superview setNeedsDisplay];
                    gaugeView.isAgri = false;
                    NSDictionary *viewData = @{
//                                               @"TextTotalCarton" : @"Total Carton",
                                                                              @"TextTotalCarton" : [cellData objectForKey:@"TextTotalCartons"] == nil  && [[cellData objectForKey:@"TextTotalCartons"] isKindOfClass:[NSNull class]]? @"" : [cellData objectForKey:@"TextTotalCartons"],
//                                               @"TextPremiumTier" : @"Premium Tier",
                                                                              @"TextPremiumTier" :  [cellData objectForKey:@"TextPremiumTier"] == nil && [[cellData objectForKey:@"TextPremiumTier"] isKindOfClass:[NSNull class]]? @"" : [cellData objectForKey:@"TextPremiumTier"],
                                               @"ColorCodeDefaultOverall" : [_loyaltyPerformanceCarouselDetails objectForKey:@"ColorCodeDefaultOverall"],
                                               @"ColorCodeDefaultPremium" : [_loyaltyPerformanceCarouselDetails objectForKey:@"ColorCodeDefaultPremium"],
                                               @"NoOfOverallOrdered" : [_loyaltyPerformanceCarouselDetails objectForKey:@"TotalOrdered"],
                                               @"NoOfPremiumOrdered" : [_loyaltyPerformanceCarouselDetails objectForKey:@"TotalPremiumAcheived"],
                                               @"TotalPremiumTarget" : [_loyaltyPerformanceCarouselDetails objectForKey:@"TotalPremiumTarget"],
                                               @"OverallTarget" : [_loyaltyPerformanceCarouselDetails objectForKey:@"TotalTargetQty"],
                                               @"ColorCodeOverall" : [_loyaltyPerformanceCarouselDetails objectForKey:@"ColorCodeOverall"],
                                               @"ColorCodePremium" : [_loyaltyPerformanceCarouselDetails objectForKey:@"ColorCodePremium"],
                                               };
                    [gaugeView loadBar: viewData isCarousel:YES];
                    
                    TTTAttributedLabel *lblFooterText = [cell viewWithTag:kCAROUSEL_LOYALTYPROGRAM_FOOTERTEXT];
                    if([[cellData objectForKey:@"isComplete"] boolValue]) {
                        NSString *footerString = [NSString stringWithFormat:@"<style>body{font-family: 'FuturaStd-Book'; font-size:13; color: #404040;}</style>%@",[cellData objectForKey:@"TextCSFooterMessage"]];
                        lblFooterText.attributedText = [[NSAttributedString alloc] initWithData: [footerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                    }
                    else{
                        NSString *csString = [NSString stringWithFormat:@"%@",[cellData objectForKey:@"TextCSFooterMessage"]];
//                        NSString *csString = @"Order <b>#PendingCartons#</b> and <b>#PendingPremCartons#</b> to achieve your rewards!";
                        NSString *footerString;
                        if(SCREEN_WIDTH <= 320) {
                            csString = [csString stringByReplacingOccurrencesOfString:@"<b>" withString:@"<b style=\"color:#DD1D21 ; font-size:14px\">"];
                            footerString = [NSString stringWithFormat:@"<style>body{font-family: 'FuturaStd-Book'; font-size:11; color: #404040;}</style>%@",csString];
                        } else {
                            csString = [csString stringByReplacingOccurrencesOfString:@"<b>" withString:@"<b style=\"color:#DD1D21 ; font-size:15px\">"];
                            footerString = [NSString stringWithFormat:@"<style>body{font-family: 'FuturaStd-Book'; font-size:12; color: #404040;}</style>%@",csString];
                        }
                        lblFooterText.attributedText = [[NSAttributedString alloc] initWithData: [footerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                    }
//                    lblFooterText.text = @"";
                    lblFooterText.font = FONT_B3;
                    lblFooterText.textColor = COLOUR_VERYDARKGREY;
                    
                    UIButton *btnView = [cell viewWithTag: kCAROUSEL_LOYALTYPROGRAM_BUTTON];
                    if([[cellData objectForKey:@"isComplete"] boolValue]) {
                         [NSLayoutConstraint constraintWithItem:btnView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
                        btnView.hidden = YES;
                    } else {
                        [btnView setTitle: LOCALIZATION(C_LOYALTY_ORDERNOW) forState:UIControlStateNormal];  // lokalised 8 Feb
                    }
                    btnView.titleLabel.font = FONT_BUTTON;
                    
                    [NSLayoutConstraint constraintWithItem:btnView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:45].active = YES;
                    //                }
                }
                
            }
                break;
            case kCAROUSEL_LOYALTYBREAKDOWN:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LoyaltyBreakdownCarousel" forIndexPath:indexPath];
                NSDictionary *cellData = _loyaltyPerformanceCarouselDetails;
//                NSDictionary *firstMonth = [[_loyaltyPerformanceCarouselDetails objectForKey:@"MonthlyOverviewList"] objectAtIndex:0];
                
                UILabel *lblNameHeader = [cell viewWithTag:kCAROUSEL_LOYALTYBREAKDOWN_HEADER];
                lblNameHeader.text = LOCALIZATION(C_LOYALTY_LOYALTYPROGRAMBREAKDOWN);  //lokalised 8 Feb
                lblNameHeader.font = FONT_H1;
                lblNameHeader.textColor = COLOUR_VERYDARKGREY;
                
                UILabel *lblPeriod = [cell viewWithTag:kCAROUSEL_LOYALTYBREAKDOWN_PERIOD];
                lblPeriod.text = [NSString stringWithFormat:@"%@: %@ - %@",LOCALIZATION(C_LOYALTY_PERIOD),[cellData objectForKey:@"StartDate"],[cellData objectForKey:@"EndDate"]];  //lokalised 8 Feb
                lblPeriod.font = FONT_B2;
                lblPeriod.textColor = COLOUR_VERYDARKGREY;
                
                UILabel *lblDisclaimerText = [cell viewWithTag:kCAROUSEL_LOYALTYBREAKDOWN_DISCLAIMERTEXT];
                lblDisclaimerText.text = [NSString stringWithFormat:@"%@", [cellData objectForKey:@"TextCSHeaderMessage"]];
                lblDisclaimerText.font = FONT_B3;
                lblDisclaimerText.textColor = COLOUR_VERYDARKGREY;
                
                LoyaltyBreakdownView *barView = [cell viewWithTag:kCAROUSEL_LOYALTYBREAKDOWN_BARVIEW];
                [barView.superview setNeedsDisplay];
                [barView loadBar:cellData isCarousel:YES];
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_LOYALTYBREAKDOWN_BUTTON];
                [btnView setTitle: LOCALIZATION(C_LOYALTY_VIEWBREAKDOWN) forState:UIControlStateNormal];  // lokalised 8 Feb
                btnView.titleLabel.font = FONT_BUTTON;
            }
                break;
            case kCAROUSEL_FASHAREBADGE:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FAShareBadgeCell" forIndexPath:indexPath];
                
                UILabel *lblNameHeader = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_NAME_HEADER];
                lblNameHeader.text = LOCALIZATION(C_HOMECAROUSEL_NAME); //Lokalise 16 Jan
                
                UILabel *lblName = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_NAME];
                lblName.text = [[mSession profileInfo] fullName];
                
                UILabel *lblBadgeHeader = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_SHAREBADGEID_HEADER];
                lblBadgeHeader.text = LOCALIZATION(C_HOMECAROUSEL_SHAREBADGEID); //Lokalise 16 Jan
                
                UILabel *lblBadge = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_SHAREBADGEID];
                lblBadge.text = [[mSession profileInfo] faShareCode];
                
                
                NSString *urlString = [[[mSession profileInfo] faShareQRCode] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

                UIImageView *faShareQRImage = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_IMAGE];
                [faShareQRImage sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"icon-code"] options: (SDWebImageRefreshCached | SDWebImageRetryFailed)];
                
                UILabel *lblFooter = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_FOOTER];
                lblFooter .text = LOCALIZATION(C_HOMECAROUSEL_BADGEFOOTER); //Lokalise 16 Jan
                lblFooter.font = FONT_B2;
                
                UIView *badgeView = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_BADGEVIEW];
                [NSLayoutConstraint constraintWithItem:badgeView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:10].active = YES;
                
            }
                break;
            case kCAROUSEL_PROFILE:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProfileCell" forIndexPath:indexPath];
                
                UILabel *lblHeader = [cell viewWithTag: kCAROUSEL_PROFILE_HEADER];
                lblHeader.text = LOCALIZATION(C_HEADER_MYPROFILE);  //lokalised 17 jan
                lblHeader.font = FONT_H1;
                
                UILabel *lblName = [cell viewWithTag: kCAROUSEL_PROFILE_NAME];
                lblName.text = [[mSession profileInfo] fullName];
                lblName.font = FONT_H(25);
                
                UILabel *lblCompanyName = [cell viewWithTag: kCAROUSEL_PROFILE_COMPANYNAME];
                lblCompanyName.text = [[mSession profileInfo] companyName];
                lblCompanyName.font = FONT_B1;
                
                
                UIImageView *imgTierIcon = [cell viewWithTag: kCAROUSEL_PROFILE_COMPANYTIERICON];
                
                UILabel *lblTierText = [cell viewWithTag: kCAROUSEL_PROFILE_COMPANYTIERTEXT];
                if ([[[mSession profileInfo] companyCertTierText] length] > 0)
                {
                    imgTierIcon.hidden = NO;
                    imgTierIcon.image = [UIImage imageNamed: [[mSession profileInfo] companyCertTierImage]];

                    lblTierText.hidden = NO;
                    lblTierText.text = [[mSession profileInfo] companyCertTierText];
                    lblTierText.font = FONT_H1;
                }
                else
                {
                    lblTierText.hidden = YES;
                    imgTierIcon.hidden = YES;
                }
                
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_PROFILE_BUTTON];
                [btnView setTitle: LOCALIZATION(C_FORM_VIEWPROFILE) forState:UIControlStateNormal]; //lokalised 17 jan
                btnView.titleLabel.font = FONT_BUTTON;
            }
                break;
                
            case kCAROUSEL_POINTS:
            {
                NSDictionary *pointsData = [GET_HOMEDATA objectForKey: @"MyPoints"];

                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PointsCell" forIndexPath:indexPath];
                
                UILabel *lblAvailablePointsHeader = [cell viewWithTag: kCAROUSEL_POINTS_AVAILABLE_HEADER];
                lblAvailablePointsHeader.text = LOCALIZATION(C_HOMECAROUSEL_AVAILABLEPOINTS); //lokalise 17 jan
                lblAvailablePointsHeader.font = FONT_H(18);
                
                UILabel *lblAvailablePoints = [cell viewWithTag: kCAROUSEL_POINTS_AVAILABLE_POINTS];
                lblAvailablePoints.font = FONT_H(20);
                NSString *availablePointString = [self addCommaThousandSeparatorsWithInputString:[pointsData objectForKey: @"BalancePoints"]];
                lblAvailablePoints.text = availablePointString;
                lblAvailablePoints.textColor = COLOUR_RED;
                
                
                NSArray *expiringPoints = [pointsData objectForKey: @"ExpiringPoints"];
                
                UILabel *lblExpiringPointsHeader = [cell viewWithTag: kCAROUSEL_POINTS_EXPIRING_HEADER];
                lblExpiringPointsHeader.text = LOCALIZATION(C_HOMECAROUSEL_EXPIRINGPOINTS); //lokalise 17 jan
                lblExpiringPointsHeader.font = FONT_H(18);
                
                //points
                for (int i = 0; i < 3 ; i++)
                {
                    //use first date and points as offset
                    NSDictionary *expiringPointsData = [expiringPoints objectAtIndex: i];
                    
                    UILabel *lblExpiringDate = [cell viewWithTag: kCAROUSEL_POINTS_FIRSTDATE + i];
                    lblExpiringDate.font = FONT_B1;
//                    NSString *expiryDateConvertedString = [self convertDate: [expiringPointsData objectForKey: @"ExpiryDate"]];
                    lblExpiringDate.text = [expiringPointsData objectForKey: @"ExpiryDate"]
                    ;
                    
                    UILabel *lblExpiringPoints = [cell viewWithTag: kCAROUSEL_POINTS_FIRSTPOINTS + i];
                    lblExpiringPoints.text =  [self addCommaThousandSeparatorsWithInputString: [expiringPointsData objectForKey: @"PointsExpiring"]];
                    lblExpiringPoints.font = FONT_H1;
                }
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_POINTS_BUTTON];
                
                if ([[mSession profileInfo] hasMyRewards])
                    btnView.hidden = NO;
                else
                    btnView.hidden = YES;
                [btnView setTitle: LOCALIZATION(C_HOMECAROUSEL_REDEEMPOINTS) forState:UIControlStateNormal];    //lokalise 17 jan
                btnView.titleLabel.font = FONT_BUTTON;
            }

                break;
            
            case kCAROUSEL_MYPERFORMANCE:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FAPerformanceCell" forIndexPath:indexPath];
                
                UILabel *lblPerformanceHeader = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_HEADER];
                lblPerformanceHeader.text = LOCALIZATION(C_TITLE_PERFORMANCE);  //lokalised
                lblPerformanceHeader.font = FONT_H1;
                
                UILabel *lblPerformanceSubHeader = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_SUBHEADER];
                lblPerformanceSubHeader.font = FONT_B2;
                
                NSDictionary *performanceData = [GET_HOMEDATA objectForKey: @"MyPerformance"];
                lblPerformanceSubHeader.text = [NSString stringWithFormat:@"%@ %@",LOCALIZATION(C_HOMECAROUSEL_UPDATEDON),[performanceData objectForKey:@"Month"]]; //lokalised
                
                if (![[performanceData objectForKey: @"PerformanceKeyOne"] isKindOfClass:[NSNull class]] && ![[performanceData objectForKey: @"PerformanceKeyOne"] isEqualToString: BLANK])
                {
                    UILabel *lblFirstKey = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_FIRSTKEY];
                    UILabel *lblFirstData = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_FIRSTDATA];
                    
                    lblFirstKey.text = [performanceData objectForKey: @"PerformanceKeyOne"];
                    lblFirstData.text = [[performanceData objectForKey: @"PerformanceValueOne"] stringValue];
                    
                    lblFirstKey.font = FONT_B(12);
                    lblFirstData.font = FONT_H(18);
                    
                    lblFirstKey.textColor = COLOUR_DARKGREY;
                    lblFirstData.textColor = COLOUR_RED;
                    
                    lblFirstKey.hidden = NO;
                    lblFirstData.hidden = NO;
                    
                    if (kIsRightToLeft) {
                        lblFirstData.textAlignment = NSTextAlignmentLeft;
                    } else {
                        lblFirstData.textAlignment = NSTextAlignmentRight;
                    }
                }
                
                if (![[performanceData objectForKey: @"PerformanceKeyTwo"] isKindOfClass:[NSNull class]] && ![[performanceData objectForKey: @"PerformanceKeyTwo"] isEqualToString: BLANK])
                {
                    UILabel *lblSecondKey = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_SECONDKEY];
                    UILabel *lblSecondData = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_SECONDDATA];
                    UIView *lblFirstSeparator = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_FIRSTSEPARATOR];
                    
                    lblSecondKey.text = [performanceData objectForKey: @"PerformanceKeyTwo"];
                    lblSecondData.text = [[performanceData objectForKey: @"PerformanceValueTwo"] stringValue];
                    
                    lblSecondKey.font = FONT_B(12);
                    lblSecondData.font = FONT_H(18);
                    
                    lblSecondKey.textColor = COLOUR_DARKGREY;
                    lblSecondData.textColor = COLOUR_RED;
                    
                    lblSecondKey.hidden = NO;
                    lblSecondData.hidden = NO;
                    lblFirstSeparator.hidden = NO;
                    
                    if (kIsRightToLeft) {
                        lblSecondData.textAlignment = NSTextAlignmentLeft;
                    } else {
                        lblSecondData.textAlignment = NSTextAlignmentRight;
                    }
                }
                
                if (![[performanceData objectForKey: @"PerformanceKeyThree"] isKindOfClass:[NSNull class]] && ![[performanceData objectForKey: @"PerformanceKeyThree"] isEqualToString: BLANK])
                {
                    UILabel *lblThirdKey = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_THIRDKEY];
                    UILabel *lblThirdData = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_THIRDDATA];
                    UIView *lblSecondSeparator = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_SECONDSEPARATOR];
                    
                    lblThirdKey.text = [performanceData objectForKey: @"PerformanceKeyThree"];
                    lblThirdData.text = [[performanceData objectForKey: @"PerformanceValueThree"] stringValue];
                    
                    lblThirdKey.font = FONT_B(12);
                    lblThirdData.font = FONT_H(18);
                    
                    lblThirdKey.textColor = COLOUR_DARKGREY;
                    lblThirdData.textColor = COLOUR_RED;
                    
                    lblThirdKey.hidden = NO;
                    lblThirdData.hidden = NO;
                    lblSecondSeparator.hidden = NO;
                    
                    if (kIsRightToLeft) {
                        lblThirdData.textAlignment = NSTextAlignmentLeft;
                    } else {
                        lblThirdData.textAlignment = NSTextAlignmentRight;
                    }
                }
                
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_BUTTON];
                [btnView setTitle: LOCALIZATION(C_FORM_VIEWDETAIL) forState:UIControlStateNormal];  //lokalise 17 jan
                btnView.titleLabel.font = FONT_BUTTON;
            }
                break;
            case kCAROUSEL_OUTLETPERFORMANCE:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"OutletPerformanceCell" forIndexPath:indexPath];
                
                UILabel *lblPerformanceHeader = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_HEADER];
                lblPerformanceHeader.font = FONT_H1;
                
                UILabel *lblPerformanceSubHeader = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_SUBHEADER];
                
                NSDictionary *performanceData = [GET_HOMEDATA objectForKey: @"MyOutletPerformance"];
                lblPerformanceHeader.text = [performanceData objectForKey:@"CarouselHeader"];
                lblPerformanceSubHeader.text = [NSString stringWithFormat:@"%@ %@",LOCALIZATION(C_HOMECAROUSEL_UPDATEDON),[performanceData objectForKey:@"OutletPerfUpdatedOn"]]; //lokalised
                lblPerformanceSubHeader.font = FONT_B2;
//                lblPerformanceSubHeader.text = [performanceData objectForKey:@"OutletPerfUpdatedOn"];
                
                if (![[performanceData objectForKey: @"PerformanceKeyOne"] isKindOfClass:[NSNull class]] && ![[performanceData objectForKey: @"PerformanceKeyOne"] isEqualToString: BLANK])
                {
                    UILabel *lblFirstKey = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_FIRSTKEY];
                    UILabel *lblFirstData = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_FIRSTDATA];
                    
                    lblFirstKey.text = [performanceData objectForKey: @"PerformanceKeyOne"];
                    lblFirstData.text = [[performanceData objectForKey: @"PerformanceValueOne"] stringValue];
                    
                    lblFirstKey.font = FONT_B(13);
                    lblFirstData.font = FONT_H(18);
                    
                    lblFirstKey.textColor = COLOUR_DARKGREY;
                    lblFirstData.textColor = COLOUR_RED;
                    
                    lblFirstKey.hidden = NO;
                    lblFirstData.hidden = NO;
                    
                    if (kIsRightToLeft) {
                        lblFirstData.textAlignment = NSTextAlignmentLeft;
                    } else {
                        lblFirstData.textAlignment = NSTextAlignmentRight;
                    }
                    
                    if(![[performanceData objectForKey:@"PerformanceIconOne"] isKindOfClass:[NSNull class]]) {
                        UIImageView *imageView = [cell viewWithTag:6900];
                        UIImage *image = [UIImage imageNamed:[performanceData objectForKey:@"PerformanceIconOne"]];
                          if(![image isKindOfClass:[NSNull class]]) {
                              [imageView setImage:image];
                          }
                    }
                }
                
                if (![[performanceData objectForKey: @"PerformanceKeyTwo"] isKindOfClass:[NSNull class]] && ![[performanceData objectForKey: @"PerformanceKeyTwo"] isEqualToString: BLANK])
                {
                    UILabel *lblSecondKey = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_SECONDKEY];
                    UILabel *lblSecondData = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_SECONDDATA];
                    UIView *lblFirstSeparator = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_FIRSTSEPARATOR];
                    
                    lblSecondKey.text = [performanceData objectForKey: @"PerformanceKeyTwo"];
                    lblSecondData.text = [[performanceData objectForKey: @"PerformanceValueTwo"] stringValue];
                    
                    lblSecondKey.font = FONT_B(13);
                    lblSecondData.font = FONT_H(18);
                    
                    lblSecondKey.textColor = COLOUR_DARKGREY;
                    lblSecondData.textColor = COLOUR_RED;
                    
                    lblSecondKey.hidden = NO;
                    lblSecondData.hidden = NO;
                    lblFirstSeparator.hidden = NO;
                    
                    if (kIsRightToLeft) {
                        lblSecondData.textAlignment = NSTextAlignmentLeft;
                    } else {
                        lblSecondData.textAlignment = NSTextAlignmentRight;
                    }
                    
                    if(![[performanceData objectForKey:@"PerformanceIconTwo"] isKindOfClass:[NSNull class]]) {
                        UIImageView *imageView = [cell viewWithTag:6901];
                        UIImage *image = [UIImage imageNamed:[performanceData objectForKey:@"PerformanceIconTwo"]];
                        if(![image isKindOfClass:[NSNull class]]) {
                            [imageView setImage:image];
                        }
                    }
                    
                }
                
                if (![[performanceData objectForKey: @"PerformanceKeyThree"] isKindOfClass:[NSNull class]] && ![[performanceData objectForKey: @"PerformanceKeyThree"] isEqualToString: BLANK])
                {
                    UILabel *lblThirdKey = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_THIRDKEY];
                    UILabel *lblThirdData = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_THIRDDATA];
                    UIView *lblSecondSeparator = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_SECONDSEPARATOR];
                    
                    lblThirdKey.text = [performanceData objectForKey: @"PerformanceKeyThree"];
                    lblThirdData.text = [[performanceData objectForKey: @"PerformanceValueThree"] stringValue];
                    
                    lblThirdKey.font = FONT_B(13);
                    lblThirdData.font = FONT_H(18);
                    
                    lblThirdKey.textColor = COLOUR_DARKGREY;
                    lblThirdData.textColor = COLOUR_RED;
                    
                    lblThirdKey.hidden = NO;
                    lblThirdData.hidden = NO;
                    lblSecondSeparator.hidden = NO;
                    
                    if (kIsRightToLeft) {
                        lblThirdData.textAlignment = NSTextAlignmentLeft;
                    } else {
                        lblThirdData.textAlignment = NSTextAlignmentRight;
                    }
                    
                    if(![[performanceData objectForKey:@"PerformanceIconThree"] isKindOfClass:[NSNull class]]) {
                        UIImageView *imageView = [cell viewWithTag:6902];
                        UIImage *image = [UIImage imageNamed:[performanceData objectForKey:@"PerformanceIconThree"]];
                        if(![image isKindOfClass:[NSNull class]]) {
                            [imageView setImage:image];
                        }
                    }
                }
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_BUTTON];
                [btnView setTitle: LOCALIZATION(C_FORM_VIEWDETAIL) forState:UIControlStateNormal];  //lokalise 17 jan
                btnView.titleLabel.font = FONT_BUTTON;
                
                if([[performanceData objectForKey:@"HasPerformanceViewDetails"]boolValue]) {
                    
                } else {
                    btnView.hidden = YES;
                }
                
            }
                break;
            case kCAROUSEL_PARTNERS_B2B:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PartnersCell" forIndexPath:indexPath];
                
                UILabel *lblHeader = [cell viewWithTag: kCAROUSEL_PARTNERS_HEADER];
                lblHeader.text = [NSString stringWithFormat:@"%@ (B2B)",LOCALIZATION(C_SIDEMENU_PARTNERS)];                             //lokalised
                
                UILabel *lblSubheader = [cell viewWithTag: kCAROUSEL_PARTNERS_SUBHEADER];
                lblSubheader.text = LOCALIZATION(C_RUSSIA_CAROUSEL_FINALSCORE);                 //lokalised
                
                RUPartnersBarView *partnersBarView = [cell viewWithTag: kCAROUSEL_PARTNERS_PROGRESSBAR];
                [partnersBarView.superview setNeedsDisplay];
                [partnersBarView loadBar: @{@"BusinessType" : @(1),@"Data" : self.carouselPartnerDictB2B}];
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_PARTNERS_BUTTON];
                [btnView setTitle: LOCALIZATION(C_RUSSIA_CAROUSEL_EARNPOINTBTN) forState:UIControlStateNormal];  //loklaised
                btnView.titleLabel.font = FONT_BUTTON;
            }
                break;
                
            case kCAROUSEL_PARTNERSENDED_B2B:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PartnersEndedCell" forIndexPath:indexPath];
                
                UILabel *lblHeader = [cell viewWithTag: 1];
                lblHeader.text = [NSString stringWithFormat:@"%@ (B2B)",LOCALIZATION(C_SIDEMENU_PARTNERS)];                             //lokalised
                
                UILabel *lblThankYou = [cell viewWithTag: 2];
                lblThankYou.text = LOCALIZATION(C_RUSSIA_THANKYOUPARTICIPATION);                //lokalised
                
                UILabel *lblMoreInteresting = [cell viewWithTag: 3];
                lblMoreInteresting.text = LOCALIZATION(C_RUSSIA_MOREINTERESTINGCAMPAIGN);       //lokalised
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_PARTNERS_BUTTON];
                [btnView setTitle: LOCALIZATION(C_RUSSIA_CAROUSEL_EARNPOINTBTN) forState:UIControlStateNormal];     //laoklised
                btnView.titleLabel.font = FONT_BUTTON;
            }
                break;
            case kCAROUSEL_RU_POINTS_B2B:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RUPointsCell" forIndexPath:indexPath];
                
                UILabel *lblHeader = [cell viewWithTag: kCAROUSEL_RU_POINTS_HEADER];
                lblHeader.text = [NSString stringWithFormat:@"%@ (B2B)",LOCALIZATION(C_TITLE_MYPOINTS)];                                                    //lokalised
                
                UILabel *lblSubheader = [cell viewWithTag: kCAROUSEL_RU_POINTS_SUBHEADER];
                lblSubheader.text = LOCALIZATION(C_RUSSIA_CAROUSEL_FINALSCORE);                                     //lokalised
                
                RUProgressBarView *progressBarView = [cell viewWithTag: 200];
                
                UILabel *lblMinPoints = [cell viewWithTag: 5];
                
                lblMinPoints.text = [NSString stringWithFormat: @"%@ %@",
                                     @"0", LOCALIZATION(C_REWARDS_PTS)];                                            //lokalised
                
                UILabel *lblMaxPoints = [cell viewWithTag: 6];
                
                lblMaxPoints.text = [NSString stringWithFormat: @"%@ %@", [self.carouselPointsDictB2B objectForKey: @"MaxPoints"], LOCALIZATION(C_REWARDS_PTS)];   //lokalised
                
                progressBarView.maxPoints = [[self.carouselPointsDictB2B objectForKey: @"MaxPoints"] intValue];
                
                if (![[self.carouselPointsDictB2B objectForKey: @"ProductPoints"] isKindOfClass: [NSNull class]])
                {
                    NSArray *productPoints = [self.carouselPointsDictB2B objectForKey: @"ProductPoints"];
                    for (int i = 0; i < [productPoints count]; i++)
                    {
                        NSDictionary *productData = [productPoints objectAtIndex: i];
                        UILabel *lblPoints, *lblPointsData;
                        switch (i)
                        {
                            case 0:
                            {
                                progressBarView.firstPoints = [[productData objectForKey: @"Points"] intValue];
                                lblPoints = [cell viewWithTag: 201];
                                lblPointsData = [cell viewWithTag: 101];
                                
                            }
                                break;
                            case 1:
                            {
                                progressBarView.secondPoints = [[productData objectForKey: @"Points"] intValue];
                                lblPoints = [cell viewWithTag: 202];
                                lblPointsData = [cell viewWithTag: 102];
                            }
                                break;
                            case 2:
                            {
                                progressBarView.thirdPoints = [[productData objectForKey: @"Points"] intValue];
                                lblPoints = [cell viewWithTag: 203];
                                lblPointsData = [cell viewWithTag: 103];
                            }
                                break;
                            default:
                                break;
                        }
                        lblPoints.text = [productData objectForKey: @"GroupName"];
                        lblPointsData.text = [NSString stringWithFormat: @"%@ %@", [productData objectForKey: @"Points"], LOCALIZATION(C_REWARDS_PTS)];     //lokalised
                    }
                }
                else
                {
                    progressBarView.firstPoints = 0;
                    progressBarView.secondPoints = 0;
                    progressBarView.thirdPoints = 0;
                }
                
                [progressBarView reloadBar];
                
                UILabel *lblFooter = [cell viewWithTag: kCAROUSEL_RU_POINTS_FOOTER];
                
                //lookup
                NSDictionary *partnersLookupTable = [[mSession lookupTable] objectForKey: @"PartnersShellClubB2B"];
                NSString *unitString = @"";
                
                if ([[self.carouselPointsDictB2B objectForKey: @"PointsReqToReachMileStone"] intValue] > 0)
                {
                    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat: @"%@%@ ", [self.carouselPointsDictB2B objectForKey: @"PointsReqToReachMileStone"], unitString] attributes:@{NSForegroundColorAttributeName: COLOUR_RED, NSFontAttributeName: FONT_H1, }];
                    
                    NSAttributedString *remainingStringAttr = [[NSAttributedString alloc] initWithString: [self.carouselPointsDictB2B objectForKey: @"SummaryText"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY, NSFontAttributeName: FONT_B3,}];
                    
                    [attrString appendAttributedString: remainingStringAttr];
                    
                    lblFooter.attributedText = attrString;
                }
                else
                {
                    lblFooter.attributedText = [[NSAttributedString alloc] initWithString: [self.carouselPointsDictB2B objectForKey: @"SummaryText"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY, NSFontAttributeName: FONT_B3,}];
                }
                
                
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_RU_POINTS_BUTTON];
                
                if ([[mSession profileInfo] hasMyRewards])
                    btnView.hidden = NO;
                else
                    btnView.hidden = YES;
                [btnView setTitle: LOCALIZATION(C_HOMECAROUSEL_REDEEMPOINTS) forState:UIControlStateNormal];            //lokalised
                btnView.titleLabel.font = FONT_BUTTON;
            }
                
                break;
            case kCAROUSEL_PARTNERS_B2C:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PartnersCell" forIndexPath:indexPath];
                
                UILabel *lblHeader = [cell viewWithTag: kCAROUSEL_PARTNERS_HEADER];
                lblHeader.text = [NSString stringWithFormat:@"%@ (B2C)",LOCALIZATION(C_SIDEMENU_PARTNERS)];                             //lokalised
                
                UILabel *lblSubheader = [cell viewWithTag: kCAROUSEL_PARTNERS_SUBHEADER];
                lblSubheader.text = LOCALIZATION(C_RUSSIA_CAROUSEL_FINALSCORE);                 //lokalised
                
                RUPartnersBarView *partnersBarView = [cell viewWithTag: kCAROUSEL_PARTNERS_PROGRESSBAR];
                [partnersBarView.superview setNeedsDisplay];
                [partnersBarView loadBar: @{@"BusinessType" : @(2),@"Data" : self.carouselPartnerDictB2C}];
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_PARTNERS_BUTTON];
                [btnView setTitle: LOCALIZATION(C_RUSSIA_CAROUSEL_EARNPOINTBTN) forState:UIControlStateNormal];  //loklaised
                btnView.titleLabel.font = FONT_BUTTON;
            }
                break;
                
            case kCAROUSEL_PARTNERSENDED_B2C:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PartnersEndedCell" forIndexPath:indexPath];
                
                UILabel *lblHeader = [cell viewWithTag: 1];
                lblHeader.text = [NSString stringWithFormat:@"%@ (B2C)",LOCALIZATION(C_SIDEMENU_PARTNERS)];                             //lokalised
                
                UILabel *lblThankYou = [cell viewWithTag: 2];
                lblThankYou.text = LOCALIZATION(C_RUSSIA_THANKYOUPARTICIPATION);                //lokalised
                
                UILabel *lblMoreInteresting = [cell viewWithTag: 3];
                lblMoreInteresting.text = LOCALIZATION(C_RUSSIA_MOREINTERESTINGCAMPAIGN);       //lokalised
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_PARTNERS_BUTTON];
                [btnView setTitle: LOCALIZATION(C_RUSSIA_CAROUSEL_EARNPOINTBTN) forState:UIControlStateNormal];     //laoklised
                btnView.titleLabel.font = FONT_BUTTON;
            }
                break;
            case kCAROUSEL_RU_POINTS_B2C:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RUPointsCell" forIndexPath:indexPath];
                
                    UILabel *lblHeader = [cell viewWithTag: kCAROUSEL_RU_POINTS_HEADER];
                lblHeader.text = [NSString stringWithFormat:@"%@ (B2C)",LOCALIZATION(C_TITLE_MYPOINTS)];                                                    //lokalised
                
                UILabel *lblSubheader = [cell viewWithTag: kCAROUSEL_RU_POINTS_SUBHEADER];
                lblSubheader.text = LOCALIZATION(C_RUSSIA_CAROUSEL_FINALSCORE);                                     //lokalised
                
                RUProgressBarView *progressBarView = [cell viewWithTag: 200];
                
                UILabel *lblMinPoints = [cell viewWithTag: 5];
                
                lblMinPoints.text = [NSString stringWithFormat: @"%@ %@",
                                     @"0", LOCALIZATION(C_REWARDS_PTS)];                                            //lokalised
                
                UILabel *lblMaxPoints = [cell viewWithTag: 6];
                
                lblMaxPoints.text = [NSString stringWithFormat: @"%@ %@", [self.carouselPointsDictB2C objectForKey: @"MaxPoints"], LOCALIZATION(C_REWARDS_PTS)];   //lokalised
                
                progressBarView.maxPoints = [[self.carouselPointsDictB2C objectForKey: @"MaxPoints"] intValue];
                
                if (![[self.carouselPointsDictB2C objectForKey: @"ProductPoints"] isKindOfClass: [NSNull class]])
                {
                    NSArray *productPoints = [self.carouselPointsDictB2C objectForKey: @"ProductPoints"];
                    for (int i = 0; i < [productPoints count]; i++)
                    {
                        NSDictionary *productData = [productPoints objectAtIndex: i];
                        UILabel *lblPoints, *lblPointsData;
                        switch (i)
                        {
                            case 0:
                            {
                                progressBarView.firstPoints = [[productData objectForKey: @"Points"] intValue];
                                lblPoints = [cell viewWithTag: 201];
                                lblPointsData = [cell viewWithTag: 101];
                                
                            }
                                break;
                            case 1:
                            {
                                progressBarView.secondPoints = [[productData objectForKey: @"Points"] intValue];
                                lblPoints = [cell viewWithTag: 202];
                                lblPointsData = [cell viewWithTag: 102];
                            }
                                break;
                            case 2:
                            {
                                progressBarView.thirdPoints = [[productData objectForKey: @"Points"] intValue];
                                lblPoints = [cell viewWithTag: 203];
                                lblPointsData = [cell viewWithTag: 103];
                            }
                                break;
                            default:
                                break;
                        }
                        lblPoints.text = [productData objectForKey: @"GroupName"];
                        lblPointsData.text = [NSString stringWithFormat: @"%@ %@", [productData objectForKey: @"Points"], LOCALIZATION(C_REWARDS_PTS)];     //lokalised
                    }
                }
                else
                {
                    progressBarView.firstPoints = 0;
                    progressBarView.secondPoints = 0;
                    progressBarView.thirdPoints = 0;
                }
                
                [progressBarView reloadBar];
                
                UILabel *lblFooter = [cell viewWithTag: kCAROUSEL_RU_POINTS_FOOTER];
                
                //lookup
                NSDictionary *partnersLookupTable = [[mSession lookupTable] objectForKey: @"PartnersShellClubB2C"];
                NSString *unitString = @"";
                
                if ([[self.carouselPointsDictB2C objectForKey: @"PointsReqToReachMileStone"] intValue] > 0)
                {
                    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat: @"%@%@ ", [self.carouselPointsDictB2C objectForKey: @"PointsReqToReachMileStone"], unitString] attributes:@{NSForegroundColorAttributeName: COLOUR_RED, NSFontAttributeName: FONT_H1, }];
                    
                    NSAttributedString *remainingStringAttr = [[NSAttributedString alloc] initWithString: [self.carouselPointsDictB2C objectForKey: @"SummaryText"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY, NSFontAttributeName: FONT_B3,}];
                    
                    [attrString appendAttributedString: remainingStringAttr];
                    
                    lblFooter.attributedText = attrString;
                }
                else
                {
                    lblFooter.attributedText = [[NSAttributedString alloc] initWithString: [self.carouselPointsDictB2C objectForKey: @"SummaryText"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY, NSFontAttributeName: FONT_B3,}];
                }
                
                
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_RU_POINTS_BUTTON];
                
                if ([[mSession profileInfo] hasMyRewards])
                    btnView.hidden = NO;
                else
                    btnView.hidden = YES;
                [btnView setTitle: LOCALIZATION(C_HOMECAROUSEL_REDEEMPOINTS) forState:UIControlStateNormal];            //lokalised
                btnView.titleLabel.font = FONT_BUTTON;
            }
                
                break;
            case kCAROUSEL_DSRPARTNERS:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DSRPartnersCell" forIndexPath:indexPath];
                
                NSDictionary *cellData = [self.carouselPartnerDict objectForKey:@"PerformanceMetrics"];
                
                UILabel *lblHeader = [cell viewWithTag: kCAROUSEL_RU_POINTS_HEADER];
                lblHeader.text = LOCALIZATION(C_SIDEMENU_PARTNERS);                             //lokalised
                
                if(![cellData isKindOfClass:[NSNull class]]) {
                    UILabel *lblSubheader = [cell viewWithTag: kCAROUSEL_RU_POINTS_SUBHEADER];
                    lblSubheader.text = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_HOMECAROUSEL_UPDATEDON), [cellData objectForKey: @"LastUpdateDate"]];
                    
                    UILabel *lblFirstKey = [cell viewWithTag: kCAROUSEL_RU_POINTS_FIRSTLABEL];
                    lblFirstKey.text = [cellData objectForKey: @"PerformanceKeyOne"];
                    UILabel *lblFirstValue = [cell viewWithTag: kCAROUSEL_RU_POINTS_FIRSTPOINTS];
                    lblFirstValue.text = [[cellData objectForKey: @"PerformanceValueOne"] stringValue];
                    
                    UILabel *lblSecondKey = [cell viewWithTag: kCAROUSEL_RU_POINTS_SECONDLABEL];
                    lblSecondKey.text = [cellData objectForKey: @"PerformanceKeyTwo"];
                    UILabel *lblSecondValue = [cell viewWithTag: kCAROUSEL_RU_POINTS_SECONDPOINTS];
                    lblSecondValue.text = [[cellData objectForKey: @"PerformanceValueTwo"] stringValue];
                    
                    UILabel *lblThirdKey = [cell viewWithTag: kCAROUSEL_RU_POINTS_THIRDLABEL];
                    lblThirdKey.text = [cellData objectForKey: @"PerformanceKeyThree"];
                    UILabel *lblThirdValue = [cell viewWithTag: kCAROUSEL_RU_POINTS_THIRDPOINTS];
                    lblThirdValue.text = [[cellData objectForKey: @"PerformanceValueThree"] stringValue];
                }
                
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_RU_POINTS_BUTTON];
                
                [btnView setTitle: LOCALIZATION(C_RU_CAROUSEL_VIEWPERFORMANCE) forState:UIControlStateNormal];      //lokalised
                btnView.titleLabel.font = FONT_BUTTON;
            }
                break;
            default:
                break;
        }
    }
    else if (collectionView == self.quickLinksColView)
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QuickLinkCell" forIndexPath:indexPath];
        
        cell.tag = [[self.quickLinksEnumArray objectAtIndex: indexPath.row] intValue];
        
        NSDictionary *cellData = [self.quickLinksArray objectAtIndex: indexPath.row];
        
        UIImageView *cellImgView = [cell viewWithTag: 1]; //tagged in storyboard
        cellImgView.image = [UIImage imageNamed: [cellData objectForKey: @"Icon"]];
        
        
        UILabel *cellLbl = [cell viewWithTag: 2]; //tagged in storyboard
        cellLbl.font = FONT_B2;
        cellLbl.text = [cellData objectForKey: @"Label"];
        
        
        //if not last column, show right line
        UIView *rightLine = [cell viewWithTag: 11];
        switch (indexPath.row % 3)
        {
                //        case 0:
                //            break;
                //        case 1:
                //            break;
            case 2: //last row
                rightLine.hidden = YES;
                break;
            default:
                rightLine.hidden = NO;
                break;
        }
        
        //if not last row, show bottom line
        UIView *bottomLine = [cell viewWithTag: 10];
        //check for remainder
        if ([self.quickLinksArray count] % 3 == 0) //perfect multiples of 3 row
        {
            //indexpath starts 0, count must - 1
            //check if last 3, hide if true
            //row index has to be more than count - 3
            if (indexPath.row > abs([self.quickLinksArray count] - 1) - 3)
                bottomLine.hidden = YES;
            else
                bottomLine.hidden = NO;
        }
        else
        {
            //indexpath starts 0, count must - 1
            //check if last row (remainder), hide if true
            //row index has to be more than total count - remainder
            if (indexPath.row >= [self.quickLinksArray count] - ([self.quickLinksArray count] % 3))
            {
                //last row
                bottomLine.hidden = YES;
                
                //total quick link only 1 element OR
                //last element is middle element
                if ([self.quickLinksArray count] == 1)
                    rightLine.hidden = YES;
                else if (indexPath.row % 3 == 1 &&  [self.quickLinksArray count] < 3)
                    rightLine.hidden = YES;
            }
            else
                bottomLine.hidden = NO;
            
            
        }
    }
    return cell;
}

-(NSString *) addCommaThousandSeparatorsWithInputString: (NSString *) inputString
{
    
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setNumberStyle:NSNumberFormatterDecimalStyle]; // to get commas (or locale equivalent)
    [fmt setGroupingSeparator:@","];
//    [fmt setMinimumFractionDigits: 2]; // must have 2 decimal place
//    [fmt setMaximumFractionDigits: 2];
    

    //convert inputString to double value
    //double value to decimal style string
    return [fmt stringFromNumber:@([inputString doubleValue])];
}

-(NSString *) convertDate: (NSString *) inputString
{
    //convert string to date
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat: @"dd-MM-yyyy"];
    [inputFormatter setTimeZone: [NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *inputDate = [inputFormatter dateFromString: inputString];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat: @"MMM yy"];
    [outputFormatter setTimeZone: [NSTimeZone timeZoneWithName:@"GMT"]];
    
    //convert inputString to double value
    //double value to decimal style string
    return [outputFormatter stringFromDate: inputDate];
}

-(void) setupDSRQuickLinks
{
    if ([[mSession profileInfo] hasQLHasManageWorkshop])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_DSRMANAGEWORKSHOP)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MANAGEMYWORKSHOP), //lokalise 17 jan
                                       @"Icon": @"quicklinks_manageworkshops_midgrey.png",
                                       }];
    }
    
    if ([[mSession profileInfo] hasQLHasLoyaltyProgram])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_LOYALTYREWARD)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_LOYALTY_LOYALTYPROGRAM),    // lokalised
                                           @"Icon": @"quicklinks_partnersofshell_midgrey.png",
                                           }];
    }

    if ([[mSession profileInfo] hasQLHasOrdering_V2])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_DSRMANAGEORDERS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MANAGEORDERS),   //lokalise 17 jan
                                           @"Icon": @"quicklinks_manageorder_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasRewards]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_MYREWARDS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYREWARDS), //lokalise 17 jan
                                           @"Icon": @"quicklinks_myrewards_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasRUPerformance])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_RU_WORKSHOPPERFORMANCE)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_PARTNERS),                 //lokalised
                                           @"Icon": @"quicklinks_partnersofshell_midgrey.png",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasAsset])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYLIBRARY)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYLIBRARY),  //lokalise 17 jan
                                           @"Icon": @"quicklinks_mylibrary_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasSearchVehicleID])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SEARCHVEHICLEID)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_SEARCHVEHICLEID),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_searchvehicleid_midgrey",
                                           }];
    }
    
    //Register Oil Change Walkthrough
    if ([[mSession profileInfo] hasQLOilChangeWalkThrough]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_OILCHANGEWALKTHROUGH)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_REGOILCHANGE),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_registerproductcode_walkthru_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasParticipatingProducts]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_PARTICIPATINGPRODUCT)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_PARTICIPATINGPRODUCTS),  //lokalised 16 Jan
                                           @"Icon": @"sideicon_participatingproducts_midgrey",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasDistributorAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SDA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SDA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasWorkshopAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SWA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SWA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
    
    [self.quickLinksColView reloadData];
}

-(void) setupDSRQuickLinksIndia {
    
//    if ([[mSession profileInfo] hasQLHasWorkShopPerformance]) {
//    [self.quickLinksEnumArray addObject:@(kQUICKLINK_DSRWORKSHOPPERFORMANCE)];
//    [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_PERFORMANCEPOINTS),
//                                       @"Icon": @"sideicon_performance_midgrey",
//                                       }];
//    }
    
//    if ([[mSession profileInfo] hasQLHasMyNetwork]) {
    [self.quickLinksEnumArray addObject:@(kQUICKLINK_MYNETWORK)];
    [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYNETWORK),   //lokalise 17 jan
                                       @"Icon": @"sideicon_managestaff_midgrey",
                                       }];
//    }
    

    if ([[mSession profileInfo] hasQLOilChangeWalkThrough]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_OILCHANGEWALKTHROUGH)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_REGOILCHANGE),   //lokalise 17 jan
                                           @"Icon": @"quicklinks_registerproductcode_walkthru_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLCashIncentive])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_CASHINCENTIVES)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_CASHINCENTIVE),
                                           @"Icon": @"quicklinks_cash_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasOrdering_V2])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_DSRMANAGEORDERS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MANAGEORDERS),   //lokalise 17 jan
                                           @"Icon": @"quicklinks_manageorder_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasRewards]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_MYREWARDS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYREWARDS), //lokalise 17 jan
                                           @"Icon": @"quicklinks_myrewards_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasAsset])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYLIBRARY)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYLIBRARY), //lokalise 17 jan
                                           @"Icon": @"quicklinks_mylibrary_midgrey",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasDistributorAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SDA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SDA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasWorkshopAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SWA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SWA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
    [self.quickLinksColView reloadData];
}

-(void) setupTOQuickLinks
{
    if ([[mSession profileInfo] hasQLHasPartnersOfShellClub])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_PARTNERSOFSHELL)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_PARTNERS),             //lokalised
                                           @"Icon": @"quicklinks_partnersofshell_midgrey.png",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasOrdering_V2])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_TRADEMANAGEORDERS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MANAGEORDERS), //lokalise 17 jan
                                           @"Icon": @"quicklinks_manageorder_midgrey",
                                           }];
    }
    
    
    if ([[mSession profileInfo] hasQLHasRegisterProductCode])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_REGISTERPRODUCTCODE)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_REGOILCHANGE),      //lokalise 17 jan
                                           @"Icon": @"quicklinks_registerproductcode_midgrey",
                                           }];
    }

    if ([[mSession profileInfo] hasQLHasCustomerRedemption])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_CUSTOMERREDEMPTION)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_CUSTREDEMPTION),     //lokalise 17 jan
                                           @"Icon": @"quicklinks_customerredemption_midgrey",
                                           }];
    }

    if ([[mSession profileInfo] hasQLHasSearchVehicleID])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SEARCHVEHICLEID)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_SEARCHVEHICLEID),   //lokalise 17 jan
                                           @"Icon": @"quicklinks_searchvehicleid_midgrey",
                                           }];
    }

    if ([[mSession profileInfo] hasQLHasScanDecal])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SCANDECAL)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_SCANDECAL),          //lokalise 17 jan
                                           @"Icon": @"quicklinks_scandecal_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasDecalActivation])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_ACTIVATEDECAL)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_ACTIVATEDECAL), //Scan Decal  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_activatedecal_midgrey",
                                           }];
    }

    if ([[mSession profileInfo] hasQLHasInviteConsumer])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_INVITECUSTOMER)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_INVITECUSTOMER),     //lokalise 17 jan
                                           @"Icon": @"quicklinks_invite_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasRewards]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_MYREWARDS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYREWARDS), //lokalise 17 jan
                                           @"Icon": @"quicklinks_myrewards_midgrey",
                                           }];
    }

    if ([[mSession profileInfo] hasQLHasAsset])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYLIBRARY)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYLIBRARY),              //lokalise 17 jan
                                           @"Icon": @"quicklinks_mylibrary_midgrey",
                                           }];
    }
    if ([[mSession profileInfo] hasQLHasParticipatingProducts]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_PARTICIPATINGPRODUCT)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_PARTICIPATINGPRODUCTS),  //lokalise 17 jan
                                           @"Icon": @"sideicon_participatingproducts_midgrey",
                                           }];
    }
    if ([[mSession profileInfo] hasQLHasWorkShopOffer])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_WORKSHOPOFFERS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_TITLE_WORKSHOPOFFER),       //lokalised
                                           @"Icon": @"sideicon_workshopoffers_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasRUPerformance])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_RU_WORKSHOPPERFORMANCE)];
        
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_PERFORMANCE_WORKSHOP),      //lokalised
                                           @"Icon": @"sideicon_performance_midgrey",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasWorkshopAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SWA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SWA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
}

-(void) setupTOQuickLinksIndia {
    
    if([[mSession profileInfo] hasQLHasManageWorkshop]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_MYNETWORK)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYNETWORK),   //lokalise 17 jan
                                           @"Icon": @"sideicon_managestaff_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasOrdering_V2])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_TRADEMANAGEORDERS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MANAGEORDERS), //lokalise 17 jan
                                           @"Icon": @"quicklinks_manageorder_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasCSHasFAPerformance])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYPERFORMANCE)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYPERFORMANCE),  //lokalise 17 jan
                                           @"Icon": @"sideicon_performance_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasRegisterProductCode]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_OILCHANGEREGISTRATION)];  //lokalise 17 jan
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_REGOILCHANGE),
                                           @"Icon": @"quicklinks_registerproductcode_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasSearchVehicleID])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SEARCHVEHICLEID)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_SEARCHVEHICLEID), //lokalised 16 Jan
                                           @"Icon": @"quicklinks_searchvehicleid_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLCashIncentive])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_CASHINCENTIVES)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_CASHINCENTIVE),
                                           @"Icon": @"quicklinks_cash_midgrey",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasBottleSale]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINKS_SCANBOTTLE)];
        [self.quickLinksArray addObject:@{
               @"Label" : LOCALIZATION(C_QL_SCANBOTTLE),
               @"Icon"  : @"quicklinks_scanbottle_midgray",
           }];
    }
    
    if ([[mSession profileInfo] hasQLHasCustomerRedemption])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_CUSTOMERREDEMPTION)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_CUSTREDEMPTION),
                                           @"Icon": @"quicklinks_customerredemption_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasRewards]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_MYREWARDS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYREWARDS),  //lokalise 17 jan
                                           @"Icon": @"quicklinks_myrewards_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasAsset])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYLIBRARY)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYLIBRARY),   //lokalise 17 jan
                                           @"Icon": @"quicklinks_mylibrary_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasWorkShopOffer])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_WORKSHOPOFFERS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_TITLE_WORKSHOPOFFER),       //lokalised
                                           @"Icon": @"sideicon_workshopoffers_midgrey",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasWorkshopAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SWA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SWA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
    [self.quickLinksColView reloadData];
}

-(void) setupMechQuickLinks
{
    if (![[mSession profileInfo] redeemOnly])//redeem only. add menu if false)
    {
        if ([[mSession profileInfo] hasQLHasSearchVehicleID])
        {
            [self.quickLinksEnumArray addObject: @(kQUICKLINK_SEARCHVEHICLEID)];
            [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_SEARCHVEHICLEID),  //lokalise 17 jan
                                               @"Icon": @"quicklinks_searchvehicleid_midgrey",
                                               }];
        }
        
        if ([[mSession profileInfo] hasQLHasRegisterProductCode])
        {
            [self.quickLinksEnumArray addObject: @(kQUICKLINK_REGISTERPRODUCTCODE)];
            [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_REGOILCHANGE),  //lokalise 17 jan
                                               @"Icon": @"quicklinks_registerproductcode_midgrey",
                                               }];
        }
        
        if ([[mSession profileInfo] hasQLHasScanDecal])
        {
            [self.quickLinksEnumArray addObject: @(kQUICKLINK_SCANDECAL)];
            [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_SCANDECAL),      //lokalise 17 jan
                                               @"Icon": @"quicklinks_scandecal_midgrey",
                                               }];
        }
        
        if ([[mSession profileInfo] hasQLHasDecalActivation])
        {
            [self.quickLinksEnumArray addObject: @(kQUICKLINK_ACTIVATEDECAL)];
            [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_ACTIVATEDECAL), //Scan Decal  //lokalised 16 Jan
                                               @"Icon": @"quicklinks_activatedecal_midgrey",
                                               }];
        }
    }
    
    if ([[mSession profileInfo] hasQLHasRewards])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYREWARDS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYREWARDS),          //lokalise 17 jan
                                           @"Icon": @"quicklinks_myrewards_midgrey",
                                           }];
    }
    
    
    if ([[mSession profileInfo] hasQLHasAsset])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYLIBRARY)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYLIBRARY), //lokalise 17 jan
                                           @"Icon": @"quicklinks_mylibrary_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasInviteConsumer])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_INVITECUSTOMER)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_INVITECUSTOMER), //lokalise 17 jan
                                           @"Icon": @"quicklinks_invite_midgrey",
                                           }];
    }
    if ([[mSession profileInfo] hasQLHasParticipatingProducts]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_PARTICIPATINGPRODUCT)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_PARTICIPATINGPRODUCTS), //lokalise 17 jan
                                           @"Icon": @"sideicon_participatingproducts_midgrey",
                                           }];
    }
    if ([[mSession profileInfo] hasQLHasCustomerRedemption])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_CUSTOMERREDEMPTION)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_CUSTREDEMPTION), //lokalise 17 jan
                                           @"Icon": @"quicklinks_customerredemption_midgrey",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasWorkshopAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SWA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SWA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
}

-(void) setupFAQuickLinks {
    if (![[mSession profileInfo] redeemOnly])//redeem only. add menu if false)
    {
        if ([[mSession profileInfo] hasQLHasSearchVehicleID])
        {
            [self.quickLinksEnumArray addObject: @(kQUICKLINK_SEARCHVEHICLEID)];
            [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_SEARCHVEHICLEID),  //lokalised 16 Jan
                                               @"Icon": @"quicklinks_searchvehicleid_midgrey",
                                               }];
        }
        
        if ([[mSession profileInfo] hasQLHasRegisterProductCode])
        {
            [self.quickLinksEnumArray addObject: @(kQUICKLINK_REGISTERPRODUCTCODE)];
            [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_REGOILCHANGE),  //lokalised 16 Jan
                                               @"Icon": @"quicklinks_registerproductcode_midgrey",
                                               }];
        }
        
        if ([[mSession profileInfo] hasQLHasScanDecal])
        {
            [self.quickLinksEnumArray addObject: @(kQUICKLINK_SCANDECAL)];
            [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_SCANDECAL), //Scan Decal  //lokalised 16 Jan
                                               @"Icon": @"quicklinks_scandecal_midgrey",
                                               }];
        }
        
        if ([[mSession profileInfo] hasQLHasDecalActivation])
        {
            [self.quickLinksEnumArray addObject: @(kQUICKLINK_ACTIVATEDECAL)];
            [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_ACTIVATEDECAL), //Scan Decal  //lokalised 16 Jan
                                               @"Icon": @"quicklinks_activatedecal_midgrey",
                                               }];
        }
    }
    
    if ([[mSession profileInfo] hasQLHasRewards])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYREWARDS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYREWARDS),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_myrewards_midgrey",
                                           }];
    }
    
    
    if ([[mSession profileInfo] hasQLHasAsset])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYLIBRARY)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYLIBRARY),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_mylibrary_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasInviteConsumer])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_INVITECUSTOMER)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_INVITECUSTOMER),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_invite_midgrey",
                                           }];
    }
    
//    if ([[mSession profileInfo] hasCSHasFAPerformance])
//    {
//        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYPERFORMANCE)];
//        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_TITLE_PERFORMANCE),
//                                           @"Icon": @"sideicon_performance_midgrey",
//                                           }];
//    }
    if ([[mSession profileInfo] hasQLHasParticipatingProducts]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_PARTICIPATINGPRODUCT)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_PARTICIPATINGPRODUCTS),  //lokalised 16 Jan
                                           @"Icon": @"sideicon_participatingproducts_midgrey",
                                           }];
    }
    if ([[mSession profileInfo] hasQLHasCustomerRedemption])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_CUSTOMERREDEMPTION)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_CUSTREDEMPTION),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_customerredemption_midgrey",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasWorkshopAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SWA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SWA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
}

-(void) setupDMRQuickLinks
{
//    if ([[mSession profileInfo] hasQLHasMechanicPerformance])
//    {
//        [self.quickLinksEnumArray addObject: @(kQUICKLINK_IN_MECHPERFORMANCE)];
//        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_TITLE_MECHPERFORMANCE),
//                                           @"Icon": @"sideicon_performance_midgrey",
//                                           }];
//    }
    
    //manage mechanics
    if ([[mSession profileInfo] hasQLHasManageStaff])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_IN_MANAGEMECHANICS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MANAGEMYMECH),  //lokalised 16 Jan
                                           @"Icon": @"sideicon_managestaff_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasOrdering_V2])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_DSRMANAGEORDERS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MANAGEORDERS),   //lokalise 17 jan
                                           @"Icon": @"quicklinks_manageorder_midgrey",
                                           }];
    }
    
    //Register Oil Change Walkthrough
    if ([[mSession profileInfo] hasQLOilChangeWalkThrough]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_OILCHANGEWALKTHROUGH)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_REGOILCHANGE),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_registerproductcode_walkthru_midgrey",
                                           }];
    }
    
    //My Rewards
    if ([[mSession profileInfo] hasQLHasRewards])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYREWARDS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYREWARDS),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_myrewards_midgrey",
                                           }];
    }
    
    //My Library
    if ([[mSession profileInfo] hasQLHasAsset])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYLIBRARY)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYLIBRARY),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_mylibrary_midgrey",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasDistributorAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SDA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SDA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasWorkshopAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SWA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SWA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
}

-(void) setupDSMQuickLinks {
    if ([[mSession profileInfo] hasQLHasPartnersOfShellClub])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_PARTNERSOFSHELL)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_PARTNERS),             //lokalised
                                           @"Icon": @"quicklinks_partnersofshell_midgrey.png",
          
        
        }];
    }
    
    //My Rewards
    if ([[mSession profileInfo] hasQLHasRewards])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYREWARDS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYREWARDS),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_myrewards_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasAsset])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYLIBRARY)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYLIBRARY),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_mylibrary_midgrey",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasDistributorAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SDA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SDA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasWorkshopAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SWA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SWA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
}

- (IBAction)viewProfilePressed:(id)sender {
    [UpdateHUD addMBProgress: KEY_WINDOW withText:LOADING_HUD];
    
    //only india TO/iM/DSR/DSM profile due to Bank Details
    if (IS_COUNTRY(COUNTRYCODE_INDIA)) {
        if(GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER || GET_PROFILETYPE == kPROFILETYPE_DSR || GET_PROFILETYPE == kPROFILETYPE_DMR) {
            [mSession loadProfileTabView];
        } else {
            [mSession loadMyProfileView];
        }
    }
    else
        [mSession loadMyProfileView];
}

- (IBAction)viewPointsPressed:(id)sender {
    [mSession loadRewardsView];
}

- (IBAction)viewPerformancePressed:(id)sender {
    [mSession loadINPerformance];
}

- (IBAction )landOnPerformancePressed:(UIButton *)sender {
    [mSession setLoadPerformance:YES];
//    [mSession loadPerformanceView];
    [mSession loadINPerformance];
}


- (IBAction)viewTOLoyaltyPressed:(id)sender {
    [mSession loadTOLoyaltySummaryView];
}

- (IBAction)orderNowPressed:(UIButton *)sender {
    [mSession loadManageOrder];
}

- (IBAction)loyaltyBreakdownPressed:(UIButton *)sender {
    [mSession pushLoyaltyCartonBreakdown:_loyaltyPerformanceCarouselDetails vc:self];
}

-(void) scanNowPressed:(UIButton *)sender {
    [mSession loadScanProductView];
}

-(void) viewRecentActivityPressed:(UIButton *) sender {
    [mSession loadRecentActivity];
}

- (IBAction)earnPointsPressed:(id)sender {
    [mSession loadRUEarnPoints];
}

- (IBAction)dsrViewPerformancePressed:(id)sender
{
    [mSession loadRUWorkshopPerformance];
}

- (IBAction)firstKeyPressed:(id)sender
{
    [mSession loadRUWorkshopPerformanceFromCarousel: @"Order BreakDown"];
}

- (IBAction)secondKeyPressed:(id)sender
{
    [mSession loadRUWorkshopPerformanceFromCarousel: @"Oil Change BreakDown"];
}

- (IBAction)thirdKeyPressed:(id)sender
{
    [mSession loadRUWorkshopPerformanceFromCarousel: @"Customer Break BreakDown"];
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath: indexPath];
    if (collectionView == self.carouselColView)
    {
        
    }
    else if (collectionView == self.quickLinksColView)
    {
        switch (cell.tag)
        {
            case kQUICKLINK_MYNETWORK:
                [mSession loadManageWorkshopView];
                //[mSession loadMyNetworkView];
                break;
            case kQUICKLINK_DSRWORKSHOPPERFORMANCE:
//                [mSession loadPerformanceView];
                [mSession loadINPerformance];
                break;
            case kQUICKLINK_OILCHANGEWALKTHROUGH:
                [mSession pushWalkthroughView:self WithType:WALKTHROUGH_OilChange];
//                [mSession loadScanProductView];
                break;
            case kQUICKLINK_OILCHANGEREGISTRATION:
                [mSession loadScanProductView];
                break;
                
            case kQUICKLINK_DSRMANAGEWORKSHOP:
                [mSession loadManageWorkshopView];
                break;
            case kQUICKLINK_DSRMANAGEORDERS:
                [mSession loadManageOrder];
                break;
            case kQUICKLINK_MYLIBRARY:
                [mSession loadMyLibraryView];
                break;
            case kQUICKLINK_SEARCHVEHICLEID:
                [mSession loadSearchVehicleView];
                break;
            case kQUICKLINK_MYREWARDS:
                [mSession loadRewardsView];
                break;
                
            case kQUICKLINK_CASHINCENTIVES:
                [mSession loadCashIncentiveView];
                break;
            
            case kQUICKLINK_TRADEMANAGEORDERS:
                [mSession loadManageOrder];
                break;
            case kQUICKLINK_REGISTERPRODUCTCODE:
                [mSession loadScanProductView];
                break;
            case kQUICKLINK_CUSTOMERREDEMPTION:
                [mSession loadPromoCodeMasterView];
                break;
            //don't need to duplicate search vehicle ID
            case kQUICKLINK_SCANDECAL:
//                [mQRCodeScanner showZBarScannerOnVC: self];
                [mSession loadSearchVehicleViewWithDecal: nil];
                break;
            case kQUICKLINK_INVITECUSTOMER:
//                [mSession loadInviteCustomerView];
                
                [mSession loadRegisterCustomerView: @{@"PromoCode" : @"",
                                                      @"VehicleNumber": @"",
                                                      @"VehicleStateID": @"",
                                                      
                                                      }];
                break;
            //don't need to duplicate my library
                
            case kQUICKLINK_WORKSHOPPROFILING:
                [mSession loadWorkshopProfilingView];
                break;
                
            case kQUICKLINK_ACTIVATEDECAL:
                [mSession loadDecalView];
                break;
            case kQUICKLINK_MYPERFORMANCE:
//                [mSession loadPerformanceView];
                [mSession loadINPerformance];
                break;
            case kQUICKLINK_PARTICIPATINGPRODUCT:
                [mSession loadParticipatingProduct];
                break;
            case kQUICKLINK_IN_MANAGEMECHANICS:
                [mSession loadManageWorkshopView];
                break;
            case kQUICKLINK_IN_MECHPERFORMANCE:
//                [mSession loadPerformanceView];
                [mSession loadINPerformance];
                break;
            case kQUICKLINK_LOYALTYREWARD:
                [mSession loadLoyaltyContract];
                break;
            case kQUICKLINK_WORKSHOPOFFERS:
                [mSession loadWorkshopOfferView];
                break;
            case kQUICKLINK_SDA:
                [mSession loadSDA:self];
                break;
            case kQUICKLINK_SWA:
                [mSession loadSWA:self];
                break;
            case kQUICKLINK_PARTNERSOFSHELL:
                [mSession loadRUPartners];
                break;
            case kQUICKLINK_RU_WORKSHOPPERFORMANCE:
                [mSession loadRUWorkshopPerformance];
                break;
            case kQUICKLINKS_SCANBOTTLE:
                [mSession loadScanBottle];
                break;
            default:
                break;
        }
    }
}

//-(void)qrCodeScanSuccess:(NSString *)scanString
//{
//    [mSession loadSearchVehicleViewWithDecal: scanString];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (IBAction)changePage:(id)sender {
//    CGFloat x = self.carouselPageControl.currentPage * self.carouselScrollView.frame.size.width;
//    [self.carouselScrollView setContentOffset:CGPointMake(x, 0) animated:YES];
//}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    if (scrollView.isDragging || scrollView.isDecelerating){
//        self.carouselPageControl.currentPage = lround(self.carouselScrollView.contentOffset.x / (self.carouselScrollView.contentSize.width / self.carouselPageControl.numberOfPages));
//    }
//}

-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.carouselColView) {
        if (kIsRightToLeft) {
            self.carouselPageControl.currentPage = (scrollView.contentSize.width/scrollView.frame.size.width) - (scrollView.contentOffset.x/scrollView.frame.size.width) - 1;
        } else {
            self.carouselPageControl.currentPage = scrollView.contentOffset.x / scrollView.frame.size.width;
        }
    }
}

- (void) processSurvey
{
    CGSize scrollContentSize = self.scrollView.contentSize;
    self.surveyView.frame = CGRectMake(5, 5, scrollContentSize.width, scrollContentSize.height);
    [self.view addSubview: self.surveyView];
    
#pragma mark - Adding WKWebView
    NSString *urlAddress = [NSString stringWithFormat:@"%@%@%@?sid=%@&ss=1",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_SURVEYFORM_URL, [[mSession profileInfo] surveyId]];
    
    WebViewHelper *webViewHelper = [[WebViewHelper alloc] init];
    [webViewHelper setWebViewWithParentView: self.surveyView
                         navigationDelegate: self
                                 urlAddress: urlAddress];
    [webViewHelper setWebConfiguration: WKDataDetectorTypeLink];
    
    self.surveyCloseButton.frame = CGRectMake(SCREEN_WIDTH - 60, 0, 20, 20);
    [self.surveyView addSubview:self.surveyCloseButton];
}


- (void) processInAppNotification
{
    if(self.isShowingNextInApp) {
        [UpdateHUD removeMBProgress: KEY_WINDOW];
        self.isShowingNextInApp = false;
    }
    
    if ([self.inAppIdArray count] > 0)
    {
        self.inAppNotificationView.frame = CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview: self.inAppNotificationView];
        
#pragma mark - Adding WKWebView
        NSString *urlAddress = [NSString stringWithFormat:@"%@%@%@?pnid=%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_INAPPNOTIFICATION_URL, [self.inAppIdArray objectAtIndex: 0]];
        
        self.webViewHelper = [[WebViewHelper alloc] init];
        [self.webViewHelper setWebViewWithParentView: self.inAppWebViewContainer
                             navigationDelegate: self
                                     urlAddress: urlAddress];
        
        [self.webViewHelper setWebConfiguration: WKDataDetectorTypeLink];
    }
    else
    {
        if ([[mSession profileInfo] showSurveyPp])
        {
            [self processSurvey];
        }
    }
}

- (IBAction)readMoreInApp:(id)sender {
    //push to notification list > notification details with ID
    [mSession handlePushNotificationWithReferenceKey: @"NL"
                                      referenceValue: [self.inAppIdArray objectAtIndex: 0]];
}


- (IBAction)closeInApp:(id)sender {
    
    //call read api
    [[WebServiceManager sharedInstance] readPushMessage: [self.inAppIdArray objectAtIndex: 0] vc: nil];
    
    //remove first object
    [self.inAppIdArray removeObjectAtIndex: 0];
    
    //"close" notification, proceed to see next notification
    [self.inAppNotificationView removeFromSuperview];
    
    //check for more inapp push, otherwise, survey or close
    if ([self.inAppIdArray count] > 0) {
        self.isShowingNextInApp = true;
        [UpdateHUD addMBProgress: KEY_WINDOW withText: @""];
    }
    [self performSelector: @selector(processInAppNotification) withObject:nil afterDelay:0.1f];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    if([navigationAction.request.URL.absoluteString containsString: @"SurveyLater.aspx"]) {
        [self closeSurvey: nil];
    }
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (IBAction)notificationViewPressed:(id)sender
{
//    [mSession loadNotificationsView];
    [mSession pushNotificationView:self];
}

- (IBAction) callPressed:(id)sender {
    NSString *csNumberString =  [[mSession lookupTable] objectForKey: @"ContactUSCSNumber"];
    
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        switch([[mSession profileInfo] businessType]) {
            case 1:{
                csNumberString =  [[mSession lookupTable] objectForKey: @"ContactUSB2BCSNumber"];
                NSString *callUrlString = [NSString stringWithFormat: @"tel:%@",
                                           [csNumberString stringByReplacingOccurrencesOfString: @" " withString: @""]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: callUrlString]];
            }
                break;
            case 2:
            {
                csNumberString =  [[mSession lookupTable] objectForKey: @"ContactUSB2CCSNumber"];
                NSString *callUrlString = [NSString stringWithFormat: @"tel:%@",
                                           [csNumberString stringByReplacingOccurrencesOfString: @" " withString: @""]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: callUrlString]];
            }
                break;
            case 3:
            {
                [self popupCustomerService];
            }
                break;
            default:
                break;
        }
    } else {
        //need to remove space
        NSString *callUrlString = [NSString stringWithFormat: @"tel:%@",
                                   [csNumberString stringByReplacingOccurrencesOfString: @" " withString: @""]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: callUrlString]];
    }
}

- (IBAction)closeSurvey:(id)sender {
    
    if (![self.webViewHelper.webView.URL.absoluteString containsString: @"SurveyThanks.aspx"])
    {
        [[WebServiceManager sharedInstance] closeSurveyPopup: @{
                                                                @"SurveyID": [[mSession profileInfo] surveyId],
                                                                @"IsShowPP": @(NO),
                                                                }
                                                          vc: nil];
    }
    
    [self.surveyView removeFromSuperview];
}

- (IBAction)promotion_viewDetailsPressed:(UIButton *)sender {
    //[self popupAttributedInfoWithTitle:@"" content:[NSString stringWithFormat:@"%@",[promotionData objectForKey:@"TPDetails"]]];
    if([GET_LOCALIZATION isEqualToString: kEnglish]) {
        [self popupInitWithURL:[NSString stringWithFormat:@"%@%@ViewDetails.aspx?c=%@&lc=1",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, GET_COUNTRY_CODE] headerTitle:@"" buttonTitle:LOCALIZATION(C_GLOBAL_OK)];
    } else {
        [self popupInitWithURL:[NSString stringWithFormat:@"%@%@ViewDetails.aspx?c=%@&lc=2",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, GET_COUNTRY_CODE] headerTitle:@"" buttonTitle:LOCALIZATION(C_GLOBAL_OK)];
    }
    
}

- (IBAction)promotion_redeemPressed:(UIButton *)sender {
    [[WebServiceManager sharedInstance] promotionRedeem:self];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_FETCHHOME:
        {
            SET_HOMEDATA([response getGenericResponse]);
             [self setFetchNewHomeData];
            
        }
            break;
            
        case kWEBSERVICE_LOYALTY_WORKSHOPPERFORMANCE: {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
               _loyaltyPerformanceCarouselDetails = [response getGenericResponse];
                
                SET_PROGRAMBREAKDOWN(_loyaltyPerformanceCarouselDetails);
                
                [_carouselColView reloadData];
                if(![[mSession profileInfo] hasCSHasTradePromotion]) {
                    self.currentPage_Carousel = 0;
                    self.isAutoScrollEnabled = true;
                    [self.carouselColView performBatchUpdates:^{}
                                                   completion:^(BOOL finished) {
                                                       /// collection-view finished reload
                                                       [self performSelector:@selector(scrollPages) withObject:nil afterDelay:self.timerInterval];
                                                   }];
                }
            }
        }
            break;
            
        case kWEBSERVICE_PROMOTION_REDEEMTRADEPROMOTION: {
            if([[[response getJSON] objectForKey:@"ResponseCode"] intValue] == 5000) {
                self.isPromotionRedeemed = YES;
                NSDictionary *data = [[response getJSON] objectForKey:@"Data"];
                [self.promotionDictionary setObject:[data objectForKey:@"RedeemMessage"] forKey:@"RedeemMessage"];
                [self.promotionDictionary setObject:[data objectForKey:@"TPImageUrl"] forKey:@"TPImageUrl"];
                [self.promotionDictionary setObject:[data objectForKey:@"TPFooter"] forKey:@"TPCSFooter"];
                [self.carouselColView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathWithIndex:0]]];
            } else {
                [self popUpViewWithTitle:[[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Header"] content:[[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Message"]];
            }
        }
            break;
            
        default:
            break;
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_FETCHHOME:
        {
            if(GET_HOMEDATA)
            {
                [self setFetchNewHomeData];
            }
        }
            break;
        default:
            break;
    }
}

-(void) setFetchNewHomeData
{
    NSDictionary *respData = GET_HOMEDATA;
    ProfileInfo *profileInfo = [[ProfileInfo alloc] initWithData: [respData objectForKey: @"MyProfile"]];
    [mSession setProfileInfo: profileInfo];
    
    self.idleWait = false;
    
    if([[mSession profileInfo] isAccountDeActive])
    {
        NSString *contactNumber = @"";
        if (![[[mSession lookupTable] objectForKey: @"ContactUSCSNumber"] isKindOfClass: [NSNull class]])
        {
            contactNumber = [[mSession lookupTable] objectForKey: @"ContactUSCSNumber"];
        }
        NSString *deactivateWithNumber = [LOCALIZATION(C_HOME_ACCOUNTDEACTIVATED) stringByReplacingOccurrencesOfString: @"####" withString: contactNumber];    //lokalised
        self.lblAcctDeactivate.text = deactivateWithNumber;
        self.acctDeactivatedView.hidden = NO;
    }
    else
    {
        [self.acctDeactivatedView removeFromSuperview];
    }
    self.carouselPartnerDict = [respData objectForKey: @"MyPartnersClub"];
    self.carouselPartnerDictB2B = [respData objectForKey: @"MyPartnersClubB2B"];
    self.carouselPartnerDictB2C = [respData objectForKey: @"MyPartnersClubB2C"];
    self.carouselPointsDict = [respData objectForKey: @"MyPoints"];
    self.carouselPointsDictB2B = [respData objectForKey: @"MyPointsB2B"];
    self.carouselPointsDictB2C = [respData objectForKey: @"MyPointsB2C"];
    self.carouselEnumArray = [[NSMutableArray alloc] init];
    
    if([[mSession profileInfo] hasCSHasLebaranPromotion]) {
        [self.carouselEnumArray addObject:@(kCAROUSEL_CNYPROMOTION)];
    }
    
    if([[mSession profileInfo] hasCSHasCVBooster]) {
        [self.carouselEnumArray addObject:@(kCAROUSEL_CVSPEEDOMETER)];
    }
    
    if([[mSession profileInfo] hasCSHasAgri]) {
        [self.carouselEnumArray addObject:@(kCAROUSEL_AGRIPROMOTION)];
    }
    
    if([[mSession profileInfo] hasCSHasHX8Promotion]) {
        [self.carouselEnumArray addObject:@(kCAROUSEL_HXPROMOTION)];
    }
    
    if([[mSession profileInfo] hasCSHasLoyaltyProgram]) {
        SET_LOYALTYTYPE(@{@"PerformanceCallType" : @"O"});
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[WebServiceManager sharedInstance]fetchLoyaltyPerformaceForContract:@{
                                                                               @"ContractReference": @"",
                                                                               @"CallType" : @"O",
                                                                               }   vc:self];
        [self.carouselEnumArray addObject:@(kCAROUSEL_LOYALTYPROGRAM)];
    }
    
    if([[mSession profileInfo] hasCSHasLoyaltyBD]) {
        [self.carouselEnumArray addObject:@(kCAROUSEL_LOYALTYBREAKDOWN)];
    }
    
    if([[mSession profileInfo] hasCSHasFABadge]) {
      //FA Share Badge
        [self.carouselEnumArray addObject:@(kCAROUSEL_FASHAREBADGE)];
    }
    
    //For India
    if([[mSession profileInfo] hasCSHasBadge]) {
        [self.carouselEnumArray addObject:@(kCAROUSEL_SHAREBADGE)];
    }
    
    if (GET_PROFILETYPE != kPROFILETYPE_DSR &&
        [[mSession profileInfo] hasCSHasMyPoints] && !IS_COUNTRY(COUNTRYCODE_RUSSIA)) //add points
    {
        [self.carouselEnumArray addObject: @(kCAROUSEL_POINTS)];
    }
    
    if ([[mSession profileInfo] hasCSHasMyProfile])
        [self.carouselEnumArray addObject: @(kCAROUSEL_PROFILE)];
    
    if([[mSession profileInfo] hasCSHasFAPerformance] || [[mSession profileInfo] hasCSHasOutletPerformanceV2])
    {
        [self.carouselEnumArray addObject:@(kCAROUSEL_OUTLETPERFORMANCE)];
    }
    
    if ([[mSession profileInfo ] hasCSHasDSRPartnersOfShellClub])
    {
        [self.carouselEnumArray addObject: @(kCAROUSEL_DSRPARTNERS)];
    }
    
    if ([[mSession profileInfo] hasCSHasPartnersOfShellClub])
    {
        switch([[mSession profileInfo] businessType]) {
            case 1:
            {
                if (![[self.carouselPartnerDict objectForKey: @"IsPromotionActive"] boolValue])
                    [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERS_B2B)];
                else
                    [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERSENDED_B2B)];
            }
                break;
            case 2:
            {
                if ([[self.carouselPartnerDict objectForKey: @"IsPromotionActive"] boolValue])
                    [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERS_B2C)];
                else
                    [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERSENDED_B2C)];
            }
                break;
            default:
                break;
        }
    }
    
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA) &&  [[mSession profileInfo] hasCSHasMyPoints]) {
        if(GET_PROFILETYPE == kPROFILETYPE_DSM || GET_PROFILETYPE == kPROFILETYPE_DSR) {
            [self.carouselEnumArray addObject:@(kCAROUSEL_POINTS)];
        } else {
            switch([[mSession profileInfo] businessType]) {
                case 1:
                {
                    [self.carouselEnumArray addObject:@(kCAROUSEL_RU_POINTS_B2B)];
                }
                    break;
                case 2:
                {
                    [self.carouselEnumArray addObject:@(kCAROUSEL_RU_POINTS_B2C)];
                }
                    break;
                default:
                    break;
            }
        }
    }
    
    if ([[mSession profileInfo] hasCSHasPartnersOfShellClub])
    {
        if(IS_COUNTRY(COUNTRYCODE_RUSSIA) &&  [[mSession profileInfo] hasCSHasMyPoints]) {
            switch([[mSession profileInfo] businessType]) {
                case  3:
                {
                    if ([[self.carouselPartnerDict objectForKey: @"IsPromotionActive"] boolValue])
                        [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERS_B2B)];
                    else
                        [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERSENDED_B2B)];
                    [self.carouselEnumArray addObject:@(kCAROUSEL_RU_POINTS_B2B)];
                    if ([[self.carouselPartnerDict objectForKey: @"IsPromotionActive"] boolValue])
                        [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERS_B2C)];
                    else
                        [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERSENDED_B2C)];
                    [self.carouselEnumArray addObject:@(kCAROUSEL_RU_POINTS_B2C)];
                }
                    break;
                default:
                    break;
            }
        }
    }
    
    self.carouselPageControl.numberOfPages = [self.carouselEnumArray count];
    //update to ProfileData
    [[NSNotificationCenter defaultCenter] postNotificationName: @"UpdateProfileInfo" object: nil];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"Unread Icon Changed" object: nil];
    [self setupNewQuickLinksMenu];
    
    //read notification
    [self.inAppIdArray removeAllObjects];
    
    //Add In App Notification below change password and TNC
    if ([[[respData objectForKey: @"MyProfile"] objectForKey: @"HasPushNotif"] boolValue])
    {
        self.inAppIdArray = [[respData objectForKey: @"MyProfile"] objectForKey: @"PushNotifList"];
        [self processInAppNotification];
    }
    //Add survey below change password and TNC
    else if ([[[respData objectForKey: @"MyProfile"] objectForKey: @"ShowSurveyPP"] boolValue])
    {
        [self processSurvey];
    }
    
    //TNC priority first. view hierachy: Home > Change Password > TNC
    //check for change password. show if need.
    if (![[[respData objectForKey: @"MyProfile"] objectForKey: @"ChangePassword"] boolValue])
    {
        [mSession pushChangePasswordView: GET_LOGIN_NAME showBackBtn:YES vc:self];
    }
    
    if(![[[respData objectForKey: @"MyProfile"] objectForKey: @"AcceptPrivacyPolicy"] boolValue] && IS_COUNTRY(COUNTRYCODE_RUSSIA))
        [mSession pushPrivacyView:YES vc:self];
    
    //    check for TNC. 1/YES = accepted.
    if (![[[respData objectForKey: @"MyProfile"] objectForKey: @"TermCondition"] boolValue])
    {
        if (GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
        {
            [mSession pushTNCView:YES tncType: 1 vc:self];
        }
        else
        {
            [mSession pushTNCView:YES tncType: 2 vc:self];
        }
    }
    
    //Timer Setup to auto rotate and idle
    [self.carouselColView reloadData];
    if(![[mSession profileInfo] hasCSHasLoyaltyProgram] && ![[mSession profileInfo] hasCSHasTradePromotion] && ![[mSession profileInfo] hasCSHasHX8Promotion]) {
        self.currentPage_Carousel = 0;
        self.isAutoScrollEnabled = true;
        [self.carouselColView performBatchUpdates:^{}
                               completion:^(BOOL finished) {
                                   /// collection-view finished reload
                                  [self performSelector:@selector(scrollPages) withObject:nil afterDelay:self.timerInterval];
                               }];
        
    }
    
    //Notification View Setup
    if(![GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_UNITEDKINGDOM])
    {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.notificationView];
        self.notificationView.userInteractionEnabled = YES;
        
        if ([[mSession profileInfo] unReadPushMessage] > 0)
        {
            [self.notificationView_Label setHidden: NO];
            
            self.notificationView_Label.font = FONT_B3;
            
            if ([[mSession profileInfo] unReadPushMessage] > 99)
                [self.notificationView_Label setText: @"99+"];
            else
                [self.notificationView_Label setText:[NSString stringWithFormat:@"%ld", (long)[[mSession profileInfo] unReadPushMessage]]];
            [self.view layoutIfNeeded];
            [self.view setNeedsLayout];
            
            
            self.notificationView_Label.layer.cornerRadius = 13.0 / 2.0;
            self.notificationView_Label.layer.masksToBounds = YES;
            self.notificationView_Label.backgroundColor = COLOUR_RED;
        }
        else
        {
            [self.notificationView_Label setHidden: YES];
        }
    }
}

#pragma mark - AutoRotation of Carousel

-(void)scrollPages{
    if(!self.idleWait) {
        self.currentPage_Carousel = (self.carouselPageControl.currentPage + 1) % self.carouselPageControl.numberOfPages;
        self.carouselPageControl.currentPage = self.currentPage_Carousel;
        [self.carouselColView performBatchUpdates:^{
            [self.carouselColView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:self.currentPage_Carousel inSection:0]]];
        }
                                       completion:^(BOOL finished) {
                                           // collection-view finished reload
                                           if(finished) {
                                               [self.carouselColView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.currentPage_Carousel inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
                                               [self performSelector:@selector(scrollPages) withObject:nil afterDelay:self.timerInterval];
                                           }
                                       }];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if(self.isAutoScrollEnabled) {
        self.idleWait = true;
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
        
        [self performSelector:@selector(idleTimerReset) withObject:nil afterDelay:self.timerInterval];
    }
}

-(void) idleTimerReset {
    if(self.isAutoScrollEnabled) {
        self.idleWait = false;
        
        [self scrollPages];
    }
}

#pragma mark - Localization Methods
- (void) receiveLanguageChangedNotification:(NSNotification *) notification
{
    if ([notification.name isEqualToString:kNotificationLanguageChanged])
    {
        LoginInfo *loginInfo = [mSession loadUserProfile];
        if([[mSession profileInfo] hasCSHasLoyaltyProgram] && loginInfo.tradeID != nil) {
            SET_LOYALTYTYPE(@{@"PerformanceCallType" : @"O"});
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[WebServiceManager sharedInstance]fetchLoyaltyPerformaceForContract:@{
                                                                                   @"ContractReference": @"",
                                                                                   @"CallType" : @"O",
                                                                                   }   vc:self];
            [self.carouselEnumArray addObject:@(kCAROUSEL_LOYALTYPROGRAM)];
        }

        [mSession updateAnalyticsUserProperty];
    }
}


@end
