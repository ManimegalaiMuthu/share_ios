//
//  ScanForRewardsHistoryViewController.h
//  Shell
//
//  Created by Nach on 27/5/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ScanForRewardsHistoryViewController : BaseVC

@property NSArray *productNameList;

@end

NS_ASSUME_NONNULL_END
