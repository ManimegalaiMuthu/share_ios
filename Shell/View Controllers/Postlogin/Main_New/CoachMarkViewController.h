//
//  CoachMarkViewController.h
//  Shell
//
//  Created by Nach on 28/1/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@protocol CoachMarkDelegate <NSObject>
- (void)coachMarkDismissed;
@end

@interface CoachMarkViewController : BaseVC

@property NSArray *coachMarkData;
@property (nonatomic,weak) NSObject <CoachMarkDelegate>  *delegate;

@end

NS_ASSUME_NONNULL_END
