//
//  AllParticipatingProducts_NewViewController.m
//  Shell
//
//  Created by Nach on 12/5/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "AllParticipatingProducts_V3ViewController.h"
#import "ShareParticipatingProduct_V3ListModel.h"

#define kCollectionViewCell_AllProducts @"AllParticipatingProductsImageCollectionViewCell"

typedef enum {
    kCollectionViewDetail_ImageView = 999,
}kCollectionViewDetail_Tag;

@interface AllParticipatingProducts_V3ViewController () <WebServiceManagerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *collectionViewFlowLayout;


@property NSMutableArray *allProductsArray;
@property NSMutableArray *allImagesArray;

@end

@implementation AllParticipatingProducts_V3ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[WebServiceManager sharedInstance] fetchAllParticipatingProducts: self];
    
    self.lblHeader.text = LOCALIZATION(C_PARTICIPATINGPRODUCTS_HEADER);
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    self.lblHeader.font = FONT_B1;
    
    if(kIsRightToLeft) {
        [self.lblHeader setTextAlignment:NSTextAlignmentRight];
    } else {
        [self.lblHeader setTextAlignment:NSTextAlignmentLeft];
    }
}

-(void) viewWillAppear:(BOOL)animated {
    [self.collectionView reloadData];
}

-(void) setupInterface {
    [self setProductGrpImageArray];
    [self.collectionView reloadData];
}

-(void) setProductGrpImageArray {
    self.allImagesArray = [[NSMutableArray alloc] init];
    for(ShareParticipatingProduct_V3ListModel *dict in self.allProductsArray) {
        [self.allImagesArray addObject:[dict productGroupImage]];
    }
}

#pragma mark - WebServiceManager
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_FETCHALLPARTICIPATINGPRODUCTS: {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                NSArray *allProductsArray = [[NSArray alloc] initWithArray:[response getGenericResponse]];
                self.allProductsArray = [[NSMutableArray alloc] init];
                for(NSDictionary *dict in allProductsArray) {
                    [self.allProductsArray addObject:[[ShareParticipatingProduct_V3ListModel alloc] initForAllProductsWithData:dict]];
                }
                [self setupInterface];
            }
        }
            break;
        default:
            break;
    }
}

#pragma mark CollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(self.allImagesArray != nil)
        return [self.allImagesArray count];
    else
        return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewCell_AllProducts forIndexPath:indexPath];
    
    UIImageView *imageView = [cell viewWithTag:kCollectionViewDetail_ImageView];
    [imageView sd_setImageWithURL:[self.allImagesArray objectAtIndex:indexPath.row] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached | SDWebImageRetryFailed];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ShareParticipatingProduct_V3ListModel *selectedProductGrpDetails = [self.allProductsArray objectAtIndex:indexPath.row];
    [mSession pushParticipatingProductsWith:[selectedProductGrpDetails productGroupDetails] onVc:self.parentViewController];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((SCREEN_WIDTH - 40) / 2, 100);
}

@end
