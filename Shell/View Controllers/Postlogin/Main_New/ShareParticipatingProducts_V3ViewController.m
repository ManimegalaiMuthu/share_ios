//
//  ShareParticipatingProducts_NewViewController.m
//  Shell
//
//  Created by Nach on 12/5/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "ShareParticipatingProducts_V3ViewController.h"
#import "ShareParticipatingProduct_V3ListModel.h"

#define kTableViewCell_ParticipatingProductGrp          @"ParticipatingProductGrpTableViewCell"
#define kTableViewCell_ParticipatingProductDetails      @"ParticipatingProductsDetailsTableViewCell"


typedef enum {
    
    kTableViewDetails_Header_ImageView      = 999,
    
    kTableViewDetails_Header_SerialNumber   = 1001,
    kTableViewDetails_Header_Heading              ,
    kTableViewDetails_Header_Description          ,
    kTableViewDetails_Header_Logo                 ,
    
    kTableViewDetails_Details_SKU_Title     = 2001,
    kTableViewDetails_Details_Points_Title        ,
    
    kTableViewDetails_Details_SKU_Data      = 4001,
    kTableViewDetails_Details_Points_Data         ,
    
}kTableViewCellDetails_Tag;


@interface ShareParticipatingProducts_V3ViewController () <UITableViewDataSource,UITableViewDelegate,WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIView *selectByProductGrpView;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectByProductGrp;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectByProductGrp;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property NSMutableArray *productsGrpArray;

@property NSMutableArray *productsListModel;

@end

@implementation ShareParticipatingProducts_V3ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[WebServiceManager sharedInstance] fetchShareProducts:self];
    
    self.lblSelectByProductGrp.text = LOCALIZATION(C_POPUP_SELECTBYPRODUCTGRP);
    self.lblSelectByProductGrp.textColor = COLOUR_VERYDARKGREY;
    self.lblSelectByProductGrp.font = FONT_B1;
    
    self.productsListModel = [[NSMutableArray alloc] init];
    
    [self.tableView setTableFooterView:[UIView new]];
    
}

-(void) setupInterface {
    [self setProductGrpArray];
    [self.tableView reloadData];
    if(self.tableView.contentSize.height < 250) {
         self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, 450);
    }
    
    [self.tableView layoutIfNeeded];
    [self.view layoutIfNeeded];
    [self.view setNeedsLayout];
    [self.view setNeedsDisplay];
}

-(void) setProductGrpArray {
    self.productsGrpArray = [[NSMutableArray alloc] init];
    for(ShareParticipatingProduct_V3ListModel *dict in self.productsListModel) {
        [self.productsGrpArray addObject:[NSString stringWithFormat:@"%@",[dict productGroupName]]];
    }
}

- (IBAction)selectByProductGrpSelected:(UIButton *)sender {
    if(self.productsGrpArray != nil) {
         [self popUpListViewWithTitle: LOCALIZATION(C_POPUP_SELECTBYPRODUCTGRP) listArray:self.productsGrpArray withSearchField:NO];
    }
}


-(void)dropDownSelection:(NSArray *)selection {
    if(selection.count > 0) {
        int count = 0;
        for(NSString *productGrp in self.productsGrpArray) {
            if([productGrp isEqualToString:[selection firstObject]]) {
                [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:count] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            }
            count += 1;
        }
    }
}



#pragma mark - WebserviceManager
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_FETCHSHAREPRODUCTS: {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                NSArray *shareProductsArray = [[NSArray alloc] initWithArray:[response getGenericResponse]];
                for(NSDictionary *dict in shareProductsArray) {
                    [self.productsListModel addObject:[[ShareParticipatingProduct_V3ListModel alloc] initWithData:dict]];
                }
                [self setupInterface];
            }
        }
            break;
        default:
            break;
    }
}

#pragma mark - TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(self.productsGrpArray != nil)
        return [self.productsGrpArray count];
    else
        return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *grpDetailsArray = [[self.productsListModel objectAtIndex:section] productGroupDetails];
    return [grpDetailsArray count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCell_ParticipatingProductGrp];
    
    ShareParticipatingProduct_V3ListModel *cellModel = [self.productsListModel objectAtIndex:section];
    
    UIImageView *imageView = [cell viewWithTag:kTableViewDetails_Header_ImageView];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[cellModel productGroupImage]] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached | SDWebImageRetryFailed];
    
//    UILabel *lblSerialNumber = [cell viewWithTag:kTableViewDetails_Header_SerialNumber];
//    lblSerialNumber.text = (section < 9) ? [NSString stringWithFormat:@"0%ld",(section + 1)] : [NSString stringWithFormat:@"%ld",(section + 1)];
//    lblSerialNumber.textColor = COLOUR_RED;
//    lblSerialNumber.font = FONT_B1;
    
    UILabel *lblHeading = [cell viewWithTag:kTableViewDetails_Header_Heading];
    lblHeading.text = [NSString stringWithFormat:@"%@",[cellModel productGroupName]];
    lblHeading.textColor = COLOUR_VERYDARKGREY;
    lblHeading.font = FONT_H(18);
    
    UIImageView *logoImageView = [cell viewWithTag:kTableViewDetails_Header_Logo];
    if(![[cellModel productGroupLogo] isEqualToString:@""]) {
        [lblHeading sizeToFit];
        [logoImageView setHidden:NO];
        if([[cellModel productGroupID] isEqualToString:@"BS6"]) {
            logoImageView.image = [UIImage imageNamed:@"icn_packshot_power_MY@3x.png"];
        }
    } else {
        [logoImageView setHidden:YES];
    }
    
    
    UILabel *lblDescription = [cell viewWithTag:kTableViewDetails_Header_Description];
    lblDescription.text = [NSString stringWithFormat:@"%@",[cellModel productGroupDescription]];
    lblDescription.textColor = COLOUR_VERYDARKGREY;
    lblDescription.font = FONT_B2;
    
    UILabel *lblSku_Title = [cell viewWithTag:kTableViewDetails_Details_SKU_Title];
    lblSku_Title.text = LOCALIZATION(C_TITLE_PRODUCTSTITLE);
    lblSku_Title.textColor = COLOUR_VERYDARKGREY;
    lblSku_Title.font = FONT_H1;
    
    UILabel *lblPoints_Title = [cell viewWithTag:kTableViewDetails_Details_Points_Title];
    lblPoints_Title.text = IS_COUNTRY(COUNTRYCODE_INDIA) ? LOCALIZATION(C_PARTICIPATINGPRODUCTS_CASH) : LOCALIZATION(C_POINTS_POINTS);
    lblPoints_Title.textColor = COLOUR_VERYDARKGREY;
    lblPoints_Title.font = FONT_H1;
    
    if(kIsRightToLeft) {
        lblHeading.textAlignment = NSTextAlignmentRight;
        lblDescription.textAlignment = NSTextAlignmentRight;
        lblSku_Title.textAlignment = NSTextAlignmentRight;
        lblPoints_Title.textAlignment = NSTextAlignmentLeft;
    }
    
    return cell.contentView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCell_ParticipatingProductDetails];
        
    ProductGroupDetailsModel *cellModel = [[[self.productsListModel objectAtIndex:indexPath.section] productGroupDetails] objectAtIndex:indexPath.row];
    
    UILabel *lblSku_Data = [cell viewWithTag:kTableViewDetails_Details_SKU_Data];
    lblSku_Data.text = [NSString stringWithFormat:@"%@",[cellModel productName]];
    lblSku_Data.textColor = COLOUR_VERYDARKGREY;
    lblSku_Data.font = FONT_B1;
    
    UILabel *lblPoints_Data = [cell viewWithTag:kTableViewDetails_Details_Points_Data];
    if(IS_COUNTRY(COUNTRYCODE_INDIA)) {
        lblPoints_Data.text = [NSString stringWithFormat:@"%ld",(long)[cellModel cash]];
    } else {
        lblPoints_Data.text = [NSString stringWithFormat:@"%ld",(long)[cellModel points]];
    }
    lblPoints_Data.textColor = COLOUR_VERYDARKGREY;
    lblPoints_Data.font = FONT_B1;
        
    if(kIsRightToLeft) {
        lblSku_Data.textAlignment = NSTextAlignmentRight;
        lblPoints_Data.textAlignment = NSTextAlignmentLeft;
    }
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *viewForFooter = [[UIView alloc]init];
    viewForFooter.backgroundColor = COLOUR_WHITE;
    return viewForFooter;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 40.0;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
