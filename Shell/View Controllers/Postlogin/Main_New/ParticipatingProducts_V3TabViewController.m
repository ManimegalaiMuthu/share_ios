//
//  ParticipatingProducts_V3TabViewController.m
//  Shell
//
//  Created by Nach on 12/5/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "ParticipatingProducts_V3TabViewController.h"
#import "ShareParticipatingProducts_V3ViewController.h"
#import "AllParticipatingProducts_V3ViewController.h"
#import "RDVTabBarItem.h"
#import "LocalizationManager.h"
#import "UpdateHUD.h"

@interface ParticipatingProducts_V3TabViewController() {
    ShareParticipatingProducts_V3ViewController *shareParticipatingProducts;
    AllParticipatingProducts_V3ViewController *allParticipatingProducts;
}

@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@end

@implementation ParticipatingProducts_V3TabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupInterface];
}

-(void) setupInterface {
    
     [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_PARTICIPATINGPRODUCT) subtitle: @"" size:15 subtitleSize:0]; //lokalised 11 Feb
    
    if ([self.navigationController.childViewControllers count] > 1)
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    
    shareParticipatingProducts = [STORYBOARD_MAINV2 instantiateViewControllerWithIdentifier: VIEW_SHAREPARTICIPATINGPRODUCTS_V3];
    UINavigationController *shareProductListNav = [[UINavigationController alloc] initWithRootViewController: shareParticipatingProducts];
    [shareProductListNav setNavigationBarHidden:YES];
    [shareParticipatingProducts view];
    
    allParticipatingProducts = [STORYBOARD_MAINV2 instantiateViewControllerWithIdentifier: VIEW_ALLPARTICIPATINGPRODUCTS_V3];
    UINavigationController *allProductListNav = [[UINavigationController alloc] initWithRootViewController: allParticipatingProducts];
    [allProductListNav setNavigationBarHidden:YES];
    [allParticipatingProducts view];
    
    
    NSArray *viewControllers = @[shareProductListNav,
                        allProductListNav
                        ];
    NSArray *title = @[LOCALIZATION(C_TITLE_SHAREPRODUCT),
                       LOCALIZATION(C_TITLE_ALLPRODUCT)
                    ];
    [self setViewControllers: viewControllers];
    
    UIImage *selected = GET_ISADVANCE ? [UIImage imageNamed:@"tab-bg_blue"] : [UIImage imageNamed:@"tab-bg"];
    UIImage *unselected = [UIImage imageNamed:@"tab-bg_grey.png"];
    if (kIsRightToLeft) {
        title = title.reverseObjectEnumerator.allObjects.mutableCopy;
    }
    for (int i = 0; i < [self.tabBar.items count]; i++)
    {
        RDVTabBarItem *item = [self.tabBar.items objectAtIndex: i];
        [item setTitle: [title objectAtIndex: i]];
        
        [item setBackgroundColor: COLOUR_WHITE];
        [item setTitleUnselectedAttributes:FONT_H1 colour:COLOUR_VERYDARKGREY];
        [item setTitleSelectedAttributes:FONT_H1 colour:COLOUR_RED];
        
        [item setBackgroundSelectedImage: selected withUnselectedImage: unselected];
        
    }
    if (kIsRightToLeft) {
        [self tabBar:self.tabBar didSelectItemAtIndex:self.viewControllers.count-1];
    }
}

-(void)tabBar:(RDVTabBar *)tabBar didSelectItemAtIndex:(NSInteger)index
{
    [super tabBar:tabBar didSelectItemAtIndex: index];
    
}

- (IBAction)backPressed:(UIButton *)sender {
    [UpdateHUD removeSpotifyHUD:CURRENT_VIEW];
    
    CATransition *animation = [CATransition animation];
    animation.type = kCATransitionFade;
    animation.duration = 0.4;
    [self.navigationController.view.layer addAnimation:animation forKey:nil];
    
    [self.navigationController popViewControllerAnimated:NO];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
