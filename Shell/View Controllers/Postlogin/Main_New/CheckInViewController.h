//
//  CheckInViewController.h
//  Shell
//
//  Created by Nach on 19/5/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@protocol CheckInDelegate <NSObject>

-(void) checkedInToday;

@end;

@interface CheckInViewController : BaseVC

@property id<CheckInDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
