//
//  HomePromotionViewController.m
//  Shell
//
//  Created by Jeremy Lua on 18/12/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "HomePromotionViewController.h"
#import "HomeNotificationViewController.h"
#import "HomePromotionListViewController.h"

@interface HomePromotionViewController () <UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *categoryHeaderFlowLayout;
@property (weak, nonatomic) IBOutlet UICollectionView *categoryHeaderCollectionView;
@property (weak, nonatomic) IBOutlet UIView *vcContainerView;

@property HomeNotificationViewController *notificationVC;
@property HomePromotionListViewController *promotionVC;

@property NSMutableArray *categoryHeaderArray;
@property NSMutableArray *categoryCellArray;

@property NSMutableArray *homePromotionComponentArray;

@end

@implementation HomePromotionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    self.categoryHeaderArray = [[NSMutableArray alloc] init];
    self.categoryCellArray = [[NSMutableArray alloc] init];
    self.homePromotionComponentArray = [[NSMutableArray alloc] init];
    
    NSDictionary *respData = [GET_HOMEDATA objectForKey: @"HomesPromotion"];
    NSArray *promoArray = [respData objectForKey:@"Promotion"];
    
    if(![promoArray isKindOfClass:[NSNull class]] && promoArray != nil && [promoArray count] > 0)
    {
        [self.categoryHeaderArray addObject:LOCALIZATION(C_HOME_PROMOTIONS)];
        [self.homePromotionComponentArray addObject:@"Promotion"];
    }

    NSArray *newsArray = [respData objectForKey:@"News"];
    if(![newsArray isKindOfClass:[NSNull class]] && newsArray != nil && [newsArray count] > 0)
    {
        [self.categoryHeaderArray addObject:LOCALIZATION(C_HOME_NEWS_TAB)];
        [self.homePromotionComponentArray addObject:@"News"];
    }
    
    [self.categoryHeaderArray addObject:LOCALIZATION(C_HOME_INBOX_TAB)];
    [self.homePromotionComponentArray addObject:@"Inbox"];
    
    self.categoryHeaderFlowLayout.itemSize = CGSizeMake(SCREEN_WIDTH / ([self.categoryHeaderArray count]), 40);
    [self.categoryHeaderCollectionView reloadData];
    [self setupChildView];
}

-(void) setupChildView {
    NSString *selectedTabName = [self.homePromotionComponentArray objectAtIndex:self.selectedIndex];
    if([selectedTabName isEqualToString:@"Inbox"]) {
        self.notificationVC = [STORYBOARD_MAINV2 instantiateViewControllerWithIdentifier:VIEW_HOMENOTIFICATIONS];
        [self addChildViewController: self.notificationVC];
        [self.vcContainerView addSubview: self.notificationVC.view];
        [self.notificationVC.view setFrame: CGRectMake(0, 0, self.vcContainerView.frame.size.width, self.vcContainerView.frame.size.height)];
    } else {
        self.promotionVC = [STORYBOARD_MAINV2 instantiateViewControllerWithIdentifier:VIEW_HOMEPROMOTION_LIST];
        NSDictionary *respData = [GET_HOMEDATA objectForKey: @"HomesPromotion"];
        if([selectedTabName isEqualToString:@"Promotion"]) {
            self.promotionVC.dataArray = [respData objectForKey:@"Promotion"];
            self.promotionVC.isPromotion = YES;
        } else {
            self.promotionVC.dataArray = [respData objectForKey:@"News"];
            self.promotionVC.isPromotion = NO;
        }
        [self addChildViewController: self.promotionVC];
        [self.vcContainerView addSubview: self.promotionVC.view];
        [self.promotionVC.view setFrame: CGRectMake(0, 0, self.vcContainerView.frame.size.width, self.vcContainerView.frame.size.height)];
    }
}

- (IBAction)dismissBtnPressed:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UICollectionView
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.categoryHeaderArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
   
     UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"CategoryCell" forIndexPath:indexPath];
           
     UILabel *lblTitle = [cell viewWithTag: 1];
     lblTitle.text = [self.categoryHeaderArray objectAtIndex: indexPath.row];
     lblTitle.font = FONT_H2;
     
     UIView *bottomLine = [cell viewWithTag: 2];
     if (indexPath.row == self.selectedIndex)
     {
         lblTitle.textColor = COLOUR_RED;
         [bottomLine setBackgroundColor:COLOUR_RED];
         bottomLine.hidden = NO;
     }
     else
     {
         lblTitle.textColor = COLOUR_VERYDARKGREY;
         bottomLine.hidden = YES;
     }
     
    [self.categoryCellArray addObject:cell];
     return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    for (UICollectionViewCell *categoryCell in self.categoryCellArray)
    {
        UIView *bottomLine = [categoryCell viewWithTag: 2];
        bottomLine.hidden = YES;
        
        UILabel *lblTitle = [categoryCell viewWithTag: 1];
        lblTitle.textColor = COLOUR_VERYDARKGREY;
        bottomLine.hidden = YES;
    }
    
    UICollectionViewCell *selectedCell = [collectionView cellForItemAtIndexPath: indexPath];
    UILabel *lblTitle = [selectedCell viewWithTag: 1];
    lblTitle.textColor = COLOUR_RED;
    
    UIView *bottomLine = [selectedCell viewWithTag: 2];
    [bottomLine setBackgroundColor:COLOUR_RED];
    bottomLine.hidden = NO;
    
    self.selectedIndex = indexPath.row;
    
    [self setupChildView];
}


@end
