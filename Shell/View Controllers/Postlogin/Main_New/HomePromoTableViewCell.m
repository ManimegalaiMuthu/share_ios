//
//  HomePromoTableViewCell.m
//  Shell
//
//  Created by Jeremy Lua on 16/12/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "TTTAttributedLabel.h"
#import "HomePromoTableViewCell.h"
#import "Constants.h"
#import "LocalizationManager.h"
#import "Helper.h"
#import "UIImageView+WebCache.h"

@interface HomePromoTableViewCell () <UICollectionViewDelegate, UICollectionViewDataSource, TTTAttributedLabelDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *promoColView;

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblViewAll;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageForViewAll;

@end


@implementation HomePromoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.lblViewAll.text = LOCALIZATION(C_HOME_VIEWALL_BTN); //Lokalised 16 jan
    [Helper setHyperlinkLabel:self.lblViewAll hyperlinkText: self.lblViewAll.text  hyperlinkFont: FONT_B2 bodyText: self.lblViewAll.text bodyFont: FONT_B2 urlString:@"ViewAll"];
    self.lblViewAll.delegate = self;
    
    self.lblViewAll.font = FONT_B2;
    self.lblHeader.font = FONT_H2;
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    self.lblViewAll.textColor = COLOUR_VERYDARKGREY;
    
    if (kIsRightToLeft) {
        self.arrowImageForViewAll.image = [UIImage imageNamed:@"icon-greyarrL.png"];
    } else {
        self.arrowImageForViewAll.image = [UIImage imageNamed:@"icon-greyarrR.png"];
    }
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url absoluteString] isEqualToString: @"ViewAll"])
    {
        if ([self.delegate respondsToSelector: @selector(didSelectViewAll:)])
        {
            [self.delegate didSelectViewAll: self.cellIndex];
        }
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dataDict = [self.dataArray objectAtIndex: indexPath.row];
    [self.delegate didSelectCell: self.cellIndex selectedData: dataDict];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.dataArray count];
}

-(void)reloadCellData
{
    [self.promoColView reloadData];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"PromotionsCollectionView" forIndexPath: indexPath];

    NSDictionary *dataDict = [self.dataArray objectAtIndex: indexPath.row];
    
    UIImageView *promoImg = [cell viewWithTag: 999];
                
    NSString *urlString = [dataDict objectForKey: @"Image"];
    if([dataDict objectForKey:@"Image"] == nil) {
        urlString = [dataDict objectForKey:@"Text"];
    }
    [promoImg sd_setImageWithURL: [NSURL URLWithString: urlString] placeholderImage: [UIImage imageNamed: @""] options: (SDWebImageRetryFailed | SDWebImageRefreshCached)];
    
    UILabel *lblPromoTitle = [cell viewWithTag: 1000];
    lblPromoTitle.text = [dataDict objectForKey: @"Title"];
    lblPromoTitle.font = FONT_H2;
    lblPromoTitle.textColor = COLOUR_VERYDARKGREY;
    lblPromoTitle.textAlignment = kIsRightToLeft ? NSTextAlignmentRight : NSTextAlignmentLeft;
    return cell;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
