//
//  CheckInViewController.m
//  Shell
//
//  Created by Nach on 19/5/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "CheckInViewController.h"

@interface CheckInViewController () <WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UIView *tipOfTheDay_View;
@property (weak, nonatomic) IBOutlet UIScrollView *tipOfTheDay_ScrollView;
@property (weak, nonatomic) IBOutlet UILabel *tipOfTheDay_Title;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *tipOfTheDay_Data;

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblWonMessage;

@end

@implementation CheckInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSDictionary *tradePromotionDict = [GET_HOMEDATA objectForKey: @"TradesPromotion"];
    [[WebServiceManager sharedInstance] checkInToday:@{
        @"PromotionStartDate"  : [tradePromotionDict objectForKey:@"TPStartDate"],
    } vc:self];
    [self setupInterface];
}

-(void) setupInterface {
    self.tipOfTheDay_Title.textColor = COLOUR_VERYDARKGREY;
    self.tipOfTheDay_Title.font = FONT_H1;
    self.tipOfTheDay_Title.text = @"";
    
    self.tipOfTheDay_Data.textColor = COLOUR_VERYDARKGREY;
    self.tipOfTheDay_Data.font = FONT_B1;
    self.tipOfTheDay_Data.text = @"";
    
    self.lblWonMessage.textColor = COLOUR_VERYDARKGREY;
    self.lblWonMessage.font = FONT_B1;
    self.lblWonMessage.text = @"";
}

- (IBAction)closeBtnPressed:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate checkedInToday];
    }];
}

#pragma mark - WebService
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_GAMIFICATION_CHECKIN_TODAY :{
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                self.tipOfTheDay_Title.text = [NSString stringWithFormat:@"%@",[[response getGenericResponse] objectForKey:@"Header"]];
                
                NSString *messageString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:15; color: #404040;}</style>%@",[[response getGenericResponse] objectForKey:@"Message"] ];
                 self.tipOfTheDay_Data.attributedText = [[NSAttributedString alloc] initWithData: [messageString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                
                NSString *wonMessage = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:15; color: #404040;}</style>%@",[[response getGenericResponse] objectForKey:@"Footer"] ];
                self.lblWonMessage.attributedText = [[NSAttributedString alloc] initWithData: [wonMessage dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                
            } else {
                [self dismissViewControllerAnimated:YES completion:^{
                    
                }];
            }
        }
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
