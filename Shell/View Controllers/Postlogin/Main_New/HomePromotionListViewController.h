//
//  HomePromotionListViewController.h
//  Shell
//
//  Created by Nach on 20/12/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "BaseVC.h"
#import "HomePromotionViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomePromotionListViewController : BaseVC

@property NSArray *dataArray;
@property BOOL isPromotion;

@end

NS_ASSUME_NONNULL_END
