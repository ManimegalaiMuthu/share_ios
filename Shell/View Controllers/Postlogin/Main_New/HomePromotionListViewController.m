//
//  HomePromotionListViewController.m
//  Shell
//
//  Created by Nach on 20/12/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "HomePromotionListViewController.h"
#import "HomePromotionViewController.h"

@interface HomePromotionListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property UIView *viewConcerned;

@end

@implementation HomePromotionListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView reloadData];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_HOME_PROMOTIONS) screenClass:nil];
}

#pragma mark - Tableview
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.isPromotion?@"HomePromotionListCell":@"HomeNewsListCell"];
    NSDictionary *cellData = [self.dataArray objectAtIndex:indexPath.row];
    
    UIImageView *promoImg = [cell viewWithTag: 999];
    NSString *urlString = [cellData objectForKey: @"Image"];
    [promoImg sd_setImageWithURL: [NSURL URLWithString: urlString] placeholderImage: [UIImage imageNamed: @""] options: (SDWebImageRetryFailed | SDWebImageRefreshCached)];
    
    if(self.isPromotion) {
        UIView *tagView = [cell viewWithTag:1000];
        [tagView.layer setBorderColor:[COLOUR_RED CGColor]];
        [tagView.layer setBorderWidth:1.0];
        
        UILabel *lblTag = [cell viewWithTag:1001];
        lblTag.font = FONT_B3;
        lblTag.textColor = COLOUR_RED;
        lblTag.text = [cellData objectForKey:@"Tag"];
        
        UILabel *promoPeriod = [cell viewWithTag:1003];
        promoPeriod.text = [[cellData objectForKey:@"PromotionPeriod"] isKindOfClass:[NSNull class]]?@"":[cellData objectForKey:@"PromotionPeriod"];
        promoPeriod.textColor = COLOUR_VERYDARKGREY;
        promoPeriod.font = FONT_B3;
        promoPeriod.textAlignment = kIsRightToLeft ? NSTextAlignmentRight : NSTextAlignmentLeft;
    }
    
    UILabel *promoTitle = [cell viewWithTag:1002];
    promoTitle.text = [[cellData objectForKey:@"Title"] isKindOfClass:[NSNull class]]?@"":[cellData objectForKey:@"Title"];
    promoTitle.textColor = COLOUR_VERYDARKGREY;
    promoTitle.font = FONT_H2;
    promoTitle.textAlignment = kIsRightToLeft ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    if(IS_COUNTRY(COUNTRYCODE_INDIA)) {
        UIPinchGestureRecognizer *gesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(zoomingImage:)];
        [cell addGestureRecognizer:gesture];
        self.viewConcerned = cell;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [mSession pushPromoDetailsWith:[self.dataArray objectAtIndex:indexPath.row] andIsPromotion:self.isPromotion onVc:self];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.viewConcerned;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
