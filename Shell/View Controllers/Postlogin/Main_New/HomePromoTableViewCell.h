//
//  HomePromoTableViewCell.h
//  Shell
//
//  Created by Jeremy Lua on 16/12/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol HomePromoCellDelegate <NSObject>

-(void) didSelectViewAll: (NSUInteger) index;
-(void) didSelectCell: (NSUInteger) index selectedData:(NSDictionary *) data;

@end

@interface HomePromoTableViewCell : UITableViewCell
@property id<HomePromoCellDelegate> delegate;
@property NSUInteger cellIndex;
@property NSMutableArray *dataArray;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;

-(void) reloadCellData;
@end
NS_ASSUME_NONNULL_END
