//
//  ParticipatingProducts_V3TabViewController.h
//  Shell
//
//  Created by Nach on 12/5/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "RDVTabBarController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ParticipatingProducts_V3TabViewController : RDVTabBarController

@end

NS_ASSUME_NONNULL_END
