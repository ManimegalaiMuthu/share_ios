//
//  HomeNotificationViewController.h
//  Shell
//
//  Created by Nach on 20/12/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "BaseVC.h"
#import "HomePromotionViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeNotificationViewController : BaseVC

@end

NS_ASSUME_NONNULL_END
