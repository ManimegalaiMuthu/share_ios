//
//  ScanForRewardsHistoryViewController.m
//  Shell
//
//  Created by Nach on 27/5/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "ScanForRewardsHistoryViewController.h"
#import "CASMonthYearPicker.h"

@interface ScanForRewardsHistoryViewController () <UITableViewDelegate,UITableViewDataSource,WebServiceManagerDelegate,MonthYearPickerDelegate>

@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalDisbursed;
@property (weak, nonatomic) IBOutlet UILabel *lblDisbursedData;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderDisclaimer;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *filterByProductView;
@property (weak, nonatomic) IBOutlet UIButton *btnFilterByProduct;

@property (weak, nonatomic) IBOutlet UIView *dataSelectionView;
@property (weak, nonatomic) IBOutlet UIButton *btnStartDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEndDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property (weak, nonatomic) IBOutlet UIButton *btnEnter;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *emptyView;
@property (weak, nonatomic) IBOutlet UILabel *lblEmpty;

@property NSDate *startDate;
@property NSDate *endDate;
@property BOOL isStartDate;
@property CASMonthYearPicker *dateView;

@property NSArray *transactionArray;
@property NSString *productNameSelected;


@end

@implementation ScanForRewardsHistoryViewController
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_HOME_CAROUSEL_SCANFORREWARDS_HEADER) subtitle:@""];
    
   self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@",LOCALIZATION_EN(C_HOME_CAROUSEL_SCANFORREWARDS_HEADER)] screenClass:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.lblTotalDisbursed.text = LOCALIZATION(C_SCANFORREWARDS_TOTALDISBURSED);
    self.lblTotalDisbursed.textColor = COLOUR_RED;
    self.lblTotalDisbursed.font = FONT_H1;
    
    self.lblDisbursedData.textColor = COLOUR_RED;
    self.lblDisbursedData.font = FONT_H1;
    
    self.lblHeaderDisclaimer.text = LOCALIZATION(C_SCANFORREWARDS_HEADER_CONTENT);
    self.lblHeaderDisclaimer.textColor = COLOUR_VERYDARKGREY;
    self.lblHeaderDisclaimer.font = FONT_B(11);
    
    [self.btnFilterByProduct setTitle:LOCALIZATION(C_SCANFORREWARDS_FILTERBYPRODUCT_PLACEHOLDER) forState:UIControlStateNormal];
    self.btnFilterByProduct.titleLabel.textColor = COLOUR_VERYDARKGREY;
    self.btnFilterByProduct.titleLabel.font = FONT_B2;
    
    [self.btnStartDate setTitle: LOCALIZATION(C_ORDER_STARTDATE) forState: UIControlStateNormal];
    self.btnStartDate.titleLabel.textColor = COLOUR_VERYDARKGREY;
    self.btnStartDate.titleLabel.font = FONT_B2;
    
    [self.btnEndDate setTitle: LOCALIZATION(C_ORDER_ENDDATE) forState: UIControlStateNormal];
    self.btnEndDate.titleLabel.textColor = COLOUR_VERYDARKGREY;
    self.btnEndDate.titleLabel.font = FONT_B2;
    
    self.lblTo.text = LOCALIZATION(C_RU_STEPONE_TO);
    self.lblTo.textColor = COLOUR_VERYDARKGREY;
    self.lblTo.font = FONT_B2;
    
    self.btnEnter.titleLabel.font = FONT_H2;
    [self.btnEnter setTitle:LOCALIZATION(C_POINTS_GO) forState:UIControlStateNormal];
    [self.btnEnter setBackgroundColor:COLOUR_RED];
    
    self.lblEmpty.text = LOCALIZATION(C_TABLE_NODATAFOUND);
    self.lblEmpty.textColor = COLOUR_LIGHTGREY;
    self.lblEmpty.font = FONT_B2;
    
    [self.tableView setTableFooterView:[UIView new]];
    
    dateView = [[CASMonthYearPicker alloc] initWithTitle:LOCALIZATION(C_PERFORMANCE_SELECTDATE) previouslySelected:nil withYearHidden:NO];
    dateView.delegate = self;
    
    self.productNameSelected = @"";
    
    [self setEmptyDataView:YES];
    
    [[WebServiceManager sharedInstance] fetchScanNRewardHistory:@{@"dateFrom":@"",
                                                                  @"dateTo":@"",
                                                                  @"productName": self.productNameSelected} onVc:self];
}

-(void) setEmptyDataView:(BOOL)isEmpty {
    self.emptyView.hidden = !isEmpty;
    self.tableView.hidden = isEmpty;
}

- (IBAction)filterByProductPressed:(UIButton *)sender {
    [self popUpListViewWithTitle: LOCALIZATION(C_SCANFORREWARDS_FILTERBYPRODUCT_PLACEHOLDER) listArray:self.productNameList withSearchField: NO];
}

- (IBAction)startDatePressed:(UIButton *)sender {
    self.isStartDate = YES;
    self.dateView.hidden = NO;
       
    [self popUpDatePicker];
}

- (IBAction)endDatePressed:(UIButton *)sender {
    self.isStartDate = NO;
    self.dateView.hidden = NO;
       
    [self popUpDatePicker];
}

- (IBAction)dateEnterPressed:(UIButton *)sender {
    
    self.dateView.hidden = YES;
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    [inputFormatter setDateFormat:@"MMM yyyy"];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    [outputFormatter setDateFormat:@"dd-MM-yyyy"];
    
    NSDate *startDate = [inputFormatter dateFromString: self.btnStartDate.titleLabel.text];
    NSString *startDateString = @"";
    if (startDate)
        startDateString = [outputFormatter stringFromDate: startDate];
    
    NSDate *endDate = [inputFormatter dateFromString: self.btnEndDate.titleLabel.text];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* comps = [calendar components:NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitYear fromDate:endDate];
    [comps setMonth:[comps month]+1];
    [comps setDay:0];
    endDate = [calendar dateFromComponents:comps];
    NSString *endDateString = @"";
    if (endDate)
        endDateString = [outputFormatter stringFromDate: endDate];
    
    [[WebServiceManager sharedInstance] fetchScanNRewardHistory:@{@"dateFrom":startDateString,
                                                                  @"dateTo":endDateString,
                                                                  @"productName": self.productNameSelected} onVc:self];
}

- (IBAction)backPressed:(UIButton *)sender {
    [self popSelf];
}

#pragma Date Picker delegate method
-(void) popUpDatePicker
{
    [self.view endEditing: YES];
    
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}

- (void)selectedMonthYear:(NSDate *)selectedDate
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    
    //setup the necesary date adjustments
    NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: selectedDate];
    [selectedComponent setDay: 1];
    selectedDate = [gregorian dateFromComponents: selectedComponent];
    
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setMonth: 3];
    [offsetComponents setDay: -1];
    
    NSDateFormatter *btnDateFormatter = [[NSDateFormatter alloc] init];
    [btnDateFormatter setDateFormat:@"MMM yyyy"];
    
    [btnDateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    if (self.isStartDate)
    {
        NSDate *offsetDate = [gregorian dateByAddingComponents:offsetComponents toDate: selectedDate options:0];
        
        NSString *selectedDateString;
        NSString *offsetDateString;
        //check end date if more than current date
        if ([offsetDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
        {
            //check start date selection if more than current date.
            if ([selectedDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
            {
                NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
                [selectedComponent setDay: 1];
                selectedDate = [gregorian dateFromComponents: selectedComponent];
            }
            selectedDateString = [btnDateFormatter stringFromDate: selectedDate]; //first of current month
            offsetDateString = [btnDateFormatter stringFromDate: [NSDate date]]; //current date
            offsetDate = [NSDate date]; //set end date to current date
        }
        else
        {
            //start date will never be more than current date if above validation of end date clears.
            selectedDateString = [btnDateFormatter stringFromDate: selectedDate];
            offsetDateString = [btnDateFormatter stringFromDate: offsetDate]; //offset still 2 months later
        }
        
        [self.btnStartDate setTitle: selectedDateString forState: UIControlStateNormal];
        [self.btnEndDate setTitle: offsetDateString forState: UIControlStateNormal];
        self.startDate = selectedDate;
        self.endDate = offsetDate;
        
    }
    else
    {
        //handle only end date
        
        //set date to end of selected month
        NSDateComponents *endOfMonthComponents = [[NSDateComponents alloc] init];
        [endOfMonthComponents setYear: 0];
        [endOfMonthComponents setMonth: 1];
        [endOfMonthComponents setDay: -1];
        selectedDate = [gregorian dateByAddingComponents: endOfMonthComponents toDate: selectedDate options: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)];
        
        //check date from startdate
        NSDate *offsetDate = [gregorian dateByAddingComponents:offsetComponents toDate: self.startDate options:0];
        
        //if offset date is more than current month, change offset to current month
        if ([offsetDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
        {
            //reset offset date to last day of current month
            NSDateComponents *currentDateComponents = [gregorian components: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
            [currentDateComponents setDay: 1];
            [currentDateComponents setMonth: [currentDateComponents month] + 1];
            offsetDate = [gregorian dateFromComponents: currentDateComponents];
            
            [currentDateComponents setDay: -1];
            [currentDateComponents setMonth: 0];
            [currentDateComponents setYear: 0];
            offsetDate = [gregorian dateByAddingComponents: currentDateComponents toDate: offsetDate options:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) ];
        }
        
        //if selection more than threshold (+2 months from start month)
        if ([selectedDate timeIntervalSinceReferenceDate] > [offsetDate timeIntervalSinceReferenceDate])
        {
            selectedDate = offsetDate;
        }
        else if ([selectedDate timeIntervalSinceReferenceDate] < [self.startDate timeIntervalSinceReferenceDate])
        {
            //if selection less than start month
            selectedDate = self.startDate;
        }
        else
        {
            
        }
        
        //don't need to change start date.
        NSString *selectedDateString = [btnDateFormatter stringFromDate: selectedDate];
        
        [self.btnEndDate setTitle: selectedDateString forState: UIControlStateNormal];
        self.endDate = selectedDate;
        
    }
}

#pragma mark - popup Delegate
-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        if([selection.firstObject isEqualToString:LOCALIZATION(C_KEYWORD_ALL)]) {
            self.productNameSelected = @"";
        } else {
            self.productNameSelected = [NSString stringWithFormat:@"%@",selection.firstObject];
        }
        [self.btnFilterByProduct setTitle:selection.firstObject forState:UIControlStateNormal];
        [self dateEnterPressed:nil];
    }
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.transactionArray.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ScanNRewardHeaderCell"];
    
    TTTAttributedLabel *lblDate = [cell viewWithTag:1];
    NSMutableAttributedString *dateString = [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(C_MANAGEWORKSHOP_IN_DETAIL_TITLE) attributes:@{
                                                                                                                                                          NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,
                                                                                                                                                          NSFontAttributeName: FONT_H2,
                                                                                                                                                          }];
    [dateString appendAttributedString: [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",LOCALIZATION(C_RUSSIA_DATEFORMAT)] attributes:@{
                                                                                                                                                                     NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,
                                                                                                                                                                     NSFontAttributeName: FONT_B(9),
                                                                                                                                                                     }]];
    lblDate.attributedText = dateString;
    
    UILabel *lblQty = [cell viewWithTag:2];
    lblQty.text = LOCALIZATION(C_REWARDS_SMALL_QTY);
    lblQty.textColor = COLOUR_VERYDARKGREY;
    lblQty.font = FONT_H2;
    
    return cell;
}

enum {
    kSCANNREWARDDATA_DATE = 1,
    kSCANNREWARDDATA_DESC,
    kSCANNREWARDDATA_PTS,
}kSCANNREWARDDATA_TAG;

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ScanNRewardDataCell"];
    
    NSDictionary *cellData = [self.transactionArray objectAtIndex:indexPath.row];
    
    UILabel *lblDate = [cell viewWithTag:kSCANNREWARDDATA_DATE];
    lblDate.text = [NSString stringWithFormat:@"%@",[cellData objectForKey:@"RedeemDate"]];
    lblDate.textColor = COLOUR_VERYDARKGREY;
    lblDate.font = FONT_B(9);
    
    UILabel *lblDesc = [cell viewWithTag:kSCANNREWARDDATA_DESC];
    lblDesc.text = [NSString stringWithFormat:@"%@",[cellData objectForKey:@"TransactionDescription"]];
    lblDesc.textColor = COLOUR_VERYDARKGREY;
    lblDesc.font = FONT_H2;
    
    UILabel *lblPts = [cell viewWithTag:kSCANNREWARDDATA_PTS];
    lblPts.text =[NSString stringWithFormat:@"%@",[cellData objectForKey:@"Quality"]];
    lblPts.textColor = COLOUR_VERYDARKGREY;
    lblPts.font = FONT_H1;
    
    return cell;
}

#pragma mark - WebService
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_TRADE_EARNNREDEEM_REPORT: {
            if([[[response getGenericResponse] objectForKey: @"EarnandRedeemReportDetails"] isKindOfClass:[NSNull class]]) {
                self.transactionArray = [[NSArray alloc] init];
            } else {
                self.transactionArray = [[response getGenericResponse] objectForKey: @"EarnandRedeemReportDetails"];
            }
            
            
            if([self.transactionArray count] > 0) {
                [self setEmptyDataView:NO];
                [self.tableView reloadData];
                
                NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@ ",[[self.transactionArray firstObject] objectForKey:@"TotalDisbursed"]] attributes:@{
                                                                                                        NSForegroundColorAttributeName:COLOUR_RED,   NSFontAttributeName : FONT_H1,
                                                                                                                                                                                                           }];
                [attrString appendAttributedString: [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(C_INVENTORY_BOTTLES) attributes:@{
                                                                                                        NSForegroundColorAttributeName:COLOUR_RED,
                                                                                                        NSFontAttributeName : FONT_B3,
                                                                                                                                               }]];
                self.lblDisbursedData.attributedText = attrString;
                
                [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:([self.transactionArray count] * 75) + 45.0].active = YES;
            } else {
                [self setEmptyDataView:YES];
                [self.tableView reloadData];
            }
        }
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
