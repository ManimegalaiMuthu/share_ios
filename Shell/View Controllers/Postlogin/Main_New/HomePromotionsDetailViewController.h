//
//  HomePromotionsDetailViewController.h
//  Shell
//
//  Created by Nach on 19/12/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomePromotionsDetailViewController : BaseVC

@property NSDictionary *promoDetails;
@property BOOL isPromotion;

@end

NS_ASSUME_NONNULL_END
