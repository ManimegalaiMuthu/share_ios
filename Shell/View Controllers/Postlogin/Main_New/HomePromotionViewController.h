//
//  HomePromotionViewController.h
//  Shell
//
//  Created by Jeremy Lua on 18/12/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomePromotionViewController : BaseVC

@property NSInteger selectedIndex;

@end

NS_ASSUME_NONNULL_END
