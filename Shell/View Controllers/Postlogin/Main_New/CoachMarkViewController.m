//
//  CoachMarkViewController.m
//  Shell
//
//  Created by Nach on 28/1/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "CoachMarkViewController.h"

#define CTABTN_HEIGHT       40.0
#define CTABTN_WIDTH        142.0
#define IMAGE_WIDTHHEIGHT   70.0
#define CAPTIONLABEL_HEIGHT SCREEN_HEIGHT < 800 ? 95 : 120

@interface CoachMarkViewController ()

@property (weak, nonatomic) IBOutlet UIButton *btnSkipAll;
@property (weak, nonatomic) IBOutlet UIView *maskingView;

@property int currentIndex;
@property NSMutableArray *addedSubViews;

@end

@implementation CoachMarkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.view.opaque = NO;
    [self.view setBackgroundColor: [UIColor clearColor]];
    
    self.currentIndex = 0;
    
    [self.btnSkipAll setAttributedTitle: [[NSAttributedString alloc] initWithString: LOCALIZATION(C_BTN_SKIPALL) attributes: @{NSForegroundColorAttributeName: COLOUR_WHITE,
                                                                                                                               NSFontAttributeName: FONT_B1,
                                                                                                                               NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),}] forState: UIControlStateNormal];
}


-(void) viewDidAppear:(BOOL)animated {
    self.addedSubViews = [[NSMutableArray alloc] init];
    [self setupCoachMark];
}

-(void) setupCoachMark {
//    [self.maskingView.layer removeFromSuperlayer];
    
    NSDictionary *data = [self.coachMarkData objectAtIndex:self.currentIndex];
    
    UILabel *captionLabel = [[UILabel alloc] init];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[data objectForKey:@"caption"]];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 1.0f;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedString.length)];
    captionLabel.attributedText = attributedString;
    captionLabel.numberOfLines = 0;
    captionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    captionLabel.textColor = COLOUR_WHITE;
    captionLabel.font = FONT_B(18);
    captionLabel.textAlignment = NSTextAlignmentCenter;
    [captionLabel sizeToFit];
    CGSize captionLabelSize = captionLabel.frame.size;
    CGFloat aspectRatio = captionLabelSize.height / captionLabelSize.width;
    UIButton *btn = [[UIButton alloc] init];
    [btn.layer setBorderColor:COLOUR_WHITE.CGColor];
    [btn.layer setBorderWidth:1.0f];
    
    [btn.titleLabel setFont:FONT_B(18)];
    [btn setTitleColor:COLOUR_WHITE forState:UIControlStateNormal];
    if(self.currentIndex == ([self.coachMarkData count] - 1)) {
        [btn setTitle:LOCALIZATION(C_BTN_OKAYGOTIT) forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(skipAllAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.btnSkipAll setHidden:YES];
    } else {
        [btn setTitle:LOCALIZATION(C_RUSSIA_NEXT_BTN) forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(nextPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.btnSkipAll setHidden:NO];
    }
    
    UIImageView *arrowImage = [[UIImageView alloc] init];
    CGRect maskRect = [[data objectForKey:@"rectangle"] CGRectValue];
    
    switch([[data objectForKey:@"side"] intValue]) {
        case kCOACHMARK_UP_RIGHTSIDE:
        {
            [arrowImage setImage:[UIImage imageNamed:@"coachmark_up-left_-1.png"]];
            [arrowImage setFrame:CGRectMake(maskRect.origin.x - IMAGE_WIDTHHEIGHT, maskRect.origin.y + (maskRect.size.height / 2), IMAGE_WIDTHHEIGHT , IMAGE_WIDTHHEIGHT )];
            if(SCREEN_HEIGHT < 800) {
                [captionLabel setFrame:CGRectMake(20.0, arrowImage.frame.origin.y + 60.0 , SCREEN_WIDTH - 20.0, CAPTIONLABEL_HEIGHT)];
            } else {
                [captionLabel setFrame:CGRectMake(30.0, arrowImage.frame.origin.y + 60.0 , SCREEN_WIDTH - 60.0, CAPTIONLABEL_HEIGHT)];
            }
            if(btn.intrinsicContentSize.width < 142) {
                [btn setFrame:CGRectMake(SCREEN_WIDTH * 0.35 , captionLabel.frame.origin.y + captionLabel.frame.size.height + 2.0, CTABTN_WIDTH, CTABTN_HEIGHT)];
            } else {
               [btn setFrame:CGRectMake(SCREEN_WIDTH * 0.35 , captionLabel.frame.origin.y + captionLabel.frame.size.height + 2.0, btn.intrinsicContentSize.width, CTABTN_HEIGHT)];
            }
            
        }
            break;
        case kCOACHMARK_UP_LEFTSIDE:
        {
            [arrowImage setImage:[UIImage imageNamed:@"coachmark_up-right_1.png"]];
            [arrowImage setFrame:CGRectMake(30.0, maskRect.origin.y + maskRect.size.height + 15.0, IMAGE_WIDTHHEIGHT , IMAGE_WIDTHHEIGHT)];
            [captionLabel setFrame:CGRectMake(60.0, maskRect.origin.y + maskRect.size.height + IMAGE_WIDTHHEIGHT + 10.0 , SCREEN_WIDTH * 0.8 , CAPTIONLABEL_HEIGHT + 20.0)];
            [NSLayoutConstraint constraintWithItem:captionLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:captionLabel attribute:NSLayoutAttributeWidth multiplier:aspectRatio constant:0].active = YES;
            if(btn.intrinsicContentSize.width < 142) {
                [btn setFrame:CGRectMake((SCREEN_WIDTH * 0.62) / 2, captionLabel.frame.origin.y + captionLabel.frame.size.height + 2.0,  CTABTN_WIDTH, CTABTN_HEIGHT)];
            } else {
                [btn setFrame:CGRectMake(SCREEN_WIDTH * 0.22, captionLabel.frame.origin.y + captionLabel.frame.size.height + 2.0, btn.intrinsicContentSize.width + 10, CTABTN_HEIGHT)];
            }
            
        }
            break;
        case kCOACHMARK_DOWN_RIGHTSIDE:
        {
            [arrowImage setImage:[UIImage imageNamed:@"coachmark_down-right_1.png"]];
            [arrowImage setFrame:CGRectMake(60.0, maskRect.origin.y - IMAGE_WIDTHHEIGHT - 10, IMAGE_WIDTHHEIGHT , IMAGE_WIDTHHEIGHT)];
            if(btn.intrinsicContentSize.width < 142) {
                [btn setFrame:CGRectMake(125.0, maskRect.origin.y - IMAGE_WIDTHHEIGHT - 20.0, CTABTN_WIDTH, CTABTN_HEIGHT)];
            } else {
                [btn setFrame:CGRectMake(125.0, maskRect.origin.y - IMAGE_WIDTHHEIGHT - 20.0, btn.intrinsicContentSize.width + 10 , CTABTN_HEIGHT)];
            }
            [captionLabel setFrame:CGRectMake(60.0, maskRect.origin.y - 195.0 , SCREEN_WIDTH * 0.65 , CAPTIONLABEL_HEIGHT)];
        }
            break;
        case kCOACHMARK_DOWN_LEFTSIDE:
        {
            [arrowImage setImage:[UIImage imageNamed:@"coachmark_down-left_1.png"]];
            [arrowImage setFrame:CGRectMake(SCREEN_WIDTH - (IMAGE_WIDTHHEIGHT * 2), maskRect.origin.y - IMAGE_WIDTHHEIGHT - 10, IMAGE_WIDTHHEIGHT , IMAGE_WIDTHHEIGHT)];
            [btn setFrame:CGRectMake(SCREEN_WIDTH * 0.35 , maskRect.origin.y - (IMAGE_WIDTHHEIGHT * 2), btn.intrinsicContentSize.width + 10, CTABTN_HEIGHT)];
            if(btn.intrinsicContentSize.width < 142) {
                [btn setFrame:CGRectMake(SCREEN_WIDTH * 0.35 , maskRect.origin.y - (IMAGE_WIDTHHEIGHT * 2), CTABTN_WIDTH, CTABTN_HEIGHT)];
                [captionLabel setFrame:CGRectMake(SCREEN_WIDTH * 0.25 , maskRect.origin.y - (IMAGE_WIDTHHEIGHT * 3.8) , SCREEN_WIDTH * 0.55 , CAPTIONLABEL_HEIGHT)];
            } else {
                [btn setFrame:CGRectMake(SCREEN_WIDTH * 0.35 , maskRect.origin.y - (IMAGE_WIDTHHEIGHT * 2), btn.intrinsicContentSize.width + 10, CTABTN_HEIGHT)];
                [captionLabel setFrame:CGRectMake(SCREEN_WIDTH * 0.32 , maskRect.origin.y - (IMAGE_WIDTHHEIGHT * 3.8) , SCREEN_WIDTH * 0.60 , CAPTIONLABEL_HEIGHT)];
            }
        }
            break;
        case kCOACHMARK_CENTER:
        {
            [arrowImage setImage:[UIImage imageNamed:@""]];
            [arrowImage setFrame:CGRectMake(0,0,0,0)];
            [captionLabel setFrame:CGRectMake(30.0, SCREEN_HEIGHT / 2.25 , SCREEN_WIDTH - 60.0 , CAPTIONLABEL_HEIGHT)];
            [btn setFrame:CGRectMake(SCREEN_WIDTH * 0.35, captionLabel.frame.origin.y + captionLabel.frame.size.height + 5.0, CTABTN_WIDTH, CTABTN_HEIGHT)];
        }
            break;
        case kCOACHMARK_LEFT_UPSIDE:
        {
            [arrowImage setImage:[UIImage imageNamed:@"coachmark_up-left_2.png"]];
            [arrowImage setFrame:CGRectMake(maskRect.origin.x + maskRect.size.width + 10.0, maskRect.origin.y + 5.0, IMAGE_WIDTHHEIGHT, IMAGE_WIDTHHEIGHT)];
            [captionLabel setFrame:CGRectMake(SCREEN_WIDTH / 3.2, arrowImage.frame.origin.y + IMAGE_WIDTHHEIGHT + 10.0 , SCREEN_WIDTH * 0.65 , CAPTIONLABEL_HEIGHT + 20.0)];
            [btn setFrame:CGRectMake(SCREEN_WIDTH / 2.1 , captionLabel.frame.origin.y + CAPTIONLABEL_HEIGHT + 30.0, btn.intrinsicContentSize.width + 10, CTABTN_HEIGHT)];
            
        }
            break;
        case kCOACHMARK_RIGHT_UPSIDE:
        {
            [arrowImage setImage:[UIImage imageNamed:@"coachmark_up-left_-1.png"]];
            [arrowImage setFrame:CGRectMake(maskRect.origin.x  -  IMAGE_WIDTHHEIGHT - 10.0, maskRect.origin.y + 5.0, IMAGE_WIDTHHEIGHT, IMAGE_WIDTHHEIGHT)];
            [captionLabel setFrame:CGRectMake(15, arrowImage.frame.origin.y + IMAGE_WIDTHHEIGHT + 10.0 , SCREEN_WIDTH * 0.5 , CAPTIONLABEL_HEIGHT + 20.0)];
            [btn setFrame:CGRectMake(30 , captionLabel.frame.origin.y + CAPTIONLABEL_HEIGHT + 30.0, btn.intrinsicContentSize.width + 10, CTABTN_HEIGHT)];
                
        }
            break;
        default:
            break;
    }
    
    [arrowImage setContentMode:UIViewContentModeScaleAspectFit];
    
    [self.view addSubview:btn];
    [self.view addSubview:captionLabel];
    [self.view addSubview:arrowImage];
    [self.addedSubViews addObject:btn];
    [self.addedSubViews addObject:captionLabel];
    [self.addedSubViews addObject:arrowImage];
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    UIBezierPath *rectPath = [UIBezierPath bezierPathWithRect:maskRect];
    [path appendPath:rectPath];
    [path setUsesEvenOddFillRule:YES];

    CAShapeLayer *fillLayer = [CAShapeLayer layer];
    fillLayer.path = path.CGPath;
    fillLayer.fillRule = kCAFillRuleEvenOdd;
    fillLayer.fillColor = [UIColor blackColor].CGColor;
    fillLayer.opacity = 0.7;
    [self.maskingView.layer addSublayer:fillLayer];
    
    [self.view addSubview:self.btnSkipAll];
   
}

- (IBAction)nextPressed:(id)sender {
    self.currentIndex += 1;
    for(UIView *obj in self.addedSubViews) {
        [obj removeFromSuperview];
    }
    [self.addedSubViews removeAllObjects];
    for (CAShapeLayer *layer in [self.maskingView.layer sublayers])
    {
        if ([layer isKindOfClass: [CAShapeLayer class]])
        {
           [layer removeFromSuperlayer];
        }
    }

    [self setupCoachMark];
}

- (IBAction)skipAllAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^ {
        if(self.delegate != nil) {
            [self.delegate coachMarkDismissed];
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
