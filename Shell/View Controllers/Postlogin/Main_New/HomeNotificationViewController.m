//
//  HomeNotificationViewController.m
//  Shell
//
//  Created by Nach on 20/12/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "HomeNotificationViewController.h"
#import "NotificationDetailViewController.h"
#import "HomePromotionViewController.h"

@interface HomeNotificationViewController () <UITableViewDataSource,UITableViewDelegate,WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *backBtnView;
@property (weak, nonatomic) IBOutlet UILabel *btnBack;
@property (weak, nonatomic) IBOutlet UIImageView *backBtnArrowImageView;
@property (weak, nonatomic) IBOutlet UIButton *btnRight;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deleteBtnHeight;

@property BOOL isEditMode;
@property NSArray *notificationArray;
@property NSMutableArray *selectedArray;

@end

@implementation HomeNotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.isEditMode = NO;
    
    [self.btnRight setTitleColor:COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    self.btnRight.titleLabel.font = FONT_B2;
    
    self.btnBack.text = LOCALIZATION(C_SIDEMENU_BACK);
    self.btnBack.textColor =COLOUR_VERYDARKGREY;
    self.btnBack.font = FONT_B2;
    
    [self.btnDelete setBackgroundColor:COLOUR_RED];
    [self.btnDelete.titleLabel setFont: FONT_BUTTON];
    [self.btnDelete setTitle: LOCALIZATION(C_DELETE) forState:UIControlStateNormal];
    
    [self.tableView setTableFooterView:[UIView new]];
    self.tableView.hidden = YES;
    
    [self setupView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.notificationArray = [[NSArray alloc] init];
    self.selectedArray = [[NSMutableArray alloc] init];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_NOTIFICATION) screenClass:nil];
    [[WebServiceManager sharedInstance] fetchMessageList: self];
}

-(void) setupView {
    
    self.backBtnView.hidden =  !self.isEditMode;
    if(self.isEditMode) {
        [self.btnRight setTitle:LOCALIZATION(C_NOTIFICATION_SELECTALL) forState:UIControlStateNormal];
    } else {
        [self.btnRight setTitle:LOCALIZATION(C_NOTIFICATION_EDIT) forState:UIControlStateNormal];
    }
    
    if(kIsRightToLeft) {
        self.backBtnArrowImageView.image = [UIImage imageNamed:@"icon-greyarrR.png"];
    } else {
        self.backBtnArrowImageView.image = [UIImage imageNamed:@"icon-greyarrL.png"];
    }
    
    self.deleteBtnHeight.constant = self.isEditMode?50:0 ;
    [self.btnDelete setHidden:!self.isEditMode];
    
    [self.tableView reloadData];
    
}

- (IBAction)backBtnFromEditModePressed:(UIButton *)sender {
    self.isEditMode = NO;
    [self setupView];
}

- (IBAction)rightBtnPressed:(UIButton *)sender {
    if(self.isEditMode) {
        NSInteger count = [self.notificationArray count] - 1;
        while(count >= 0) {
            [self.selectedArray replaceObjectAtIndex:count withObject: @(YES)];
            count -= 1;
        }
        [self.tableView reloadData];
    }
    else {
        self.isEditMode = !self.isEditMode;
    }
    [self setupView];
}

- (IBAction)deleteBtnPressed:(UIButton *)sender {
    if(self.isEditMode) {
        NSMutableArray *notificationsToDeleteArray = [[NSMutableArray alloc] init];
        
        NSMutableArray *updatedArray = [[NSMutableArray alloc] init];
        
        for (int i =0; i < [self.selectedArray count]; i++)
        {
            NSDictionary *cellData = [self.notificationArray objectAtIndex: i];
            
            if ([[self.selectedArray objectAtIndex: i] boolValue])
            {
                //to delete current entry
                [notificationsToDeleteArray addObject: @{@"PushID": [cellData objectForKey: @"PushID"]}];
            }
            else
            {
                //entry not to delete
                [updatedArray addObject: cellData];
            }
        }
        
        [[WebServiceManager sharedInstance] removePushMessageWithList: notificationsToDeleteArray vc: self];
        
        //replace surviving entries
        self.notificationArray = updatedArray;
    }
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.notificationArray count]>0?[self.notificationArray count]:1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0;
}

enum NOTIFICATIONTYPES
{
    NOTIFICATION_SHAREPROGRAMUPDATES = 1,
    NOTIFICATION_POINTSINCENTIVESTATUS,
    NOTIFICATION_PROMOTIONSACTIVITIES,
    NOTIFICATION_DISTRIBUTOROFFERUPDATES,
    NOTIFICATION_SHELLOFFERUPDATES,
};

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if([self.notificationArray count] == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier: @"NoDataCell"];
        
        UILabel *lblNoData = [cell viewWithTag:1];
        lblNoData.text = LOCALIZATION(C_TABLE_NODATAFOUND);
        lblNoData.textColor = COLOUR_LIGHTGREY;
        lblNoData.font = FONT_B2;
        
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier: self.isEditMode?@"NotificationDeleteMessageCell":@"NotificationMessageCell"];
        
        NSDictionary *msgData = [self.notificationArray objectAtIndex: indexPath.row];
        
        UIImageView *msgIcon = [cell viewWithTag: 1]; //read/unread
        switch ([[msgData objectForKey: @"PushType"] intValue])
        {
            case NOTIFICATION_SHAREPROGRAMUPDATES:
                if ([[msgData objectForKey: @"ReadStatus"] boolValue])
                    [msgIcon setImage: [UIImage imageNamed: @"sideicon-rewards-notis"]];
                else {
                    UIImage *iconImage = GET_ISADVANCE ? [UIImage imageNamed: @"sideicon-rewards-notis_blue"] : [UIImage imageNamed: @"sideicon-rewards-notis-red"];
                    [msgIcon setImage: iconImage];
                }
                break;
            case NOTIFICATION_POINTSINCENTIVESTATUS:
                if ([[msgData objectForKey: @"ReadStatus"] boolValue])
                    [msgIcon setImage: [UIImage imageNamed: @"icon-msg-notis"]];
                else {
                    UIImage *iconImage = GET_ISADVANCE ? [UIImage imageNamed: @"icon-msg-darkgrey_blue"] : [UIImage imageNamed: @"icon-msg-darkgrey-red"];
                    [msgIcon setImage: iconImage];
                }
                break;
            case NOTIFICATION_PROMOTIONSACTIVITIES:
                if ([[msgData objectForKey: @"ReadStatus"] boolValue])
                    [msgIcon setImage: [UIImage imageNamed: @"sideicon-promo-darkgrey"]];
                else {
                    UIImage *iconImage = GET_ISADVANCE ? [UIImage imageNamed: @"sideicon-promo_blue"] : [UIImage imageNamed: @"sideicon-promo-red"];
                    [msgIcon setImage: iconImage];
                }
                break;
            case NOTIFICATION_DISTRIBUTOROFFERUPDATES:
                if ([[msgData objectForKey: @"ReadStatus"] boolValue])
                    [msgIcon setImage: [UIImage imageNamed: @"icon-notis"]];
                else {
                    UIImage *iconImage = GET_ISADVANCE ? [UIImage imageNamed: @"con-notisunread_blue"] : [UIImage imageNamed: @"icon-notisunread"];
                    [msgIcon setImage: iconImage];
                }
                break;
            case NOTIFICATION_SHELLOFFERUPDATES:
                if ([[msgData objectForKey: @"ReadStatus"] boolValue])
                    [msgIcon setImage: [UIImage imageNamed: @"icon-coin-notis"]];
                else {
                    UIImage *iconImage = GET_ISADVANCE ? [UIImage imageNamed: @"icon-coin-notis_blue"] : [UIImage imageNamed: @"icon-coin-notis-red"];
                    [msgIcon setImage: iconImage];
                }
                break;
            default:
                if ([[msgData objectForKey: @"ReadStatus"] boolValue])
                    [msgIcon setImage: [UIImage imageNamed: @"icon-notis"]];
                else {
                    UIImage *iconImage = GET_ISADVANCE ? [UIImage imageNamed: @"con-notisunread_blue"] : [UIImage imageNamed: @"icon-notisunread"];
                    [msgIcon setImage: iconImage];
                }
            break;
        }

        UILabel *lblTitle = [cell viewWithTag: 2];
        [lblTitle setFont: FONT_H1];
        lblTitle.text = [msgData objectForKey: @"Header"];
        lblTitle.textAlignment = kIsRightToLeft ? NSTextAlignmentRight : NSTextAlignmentLeft;
        
        UILabel *lblSubtitle = [cell viewWithTag: 3];
        lblSubtitle.text = [msgData objectForKey: @"ShortDescription"];
        [lblSubtitle setFont: FONT_B1];
        lblSubtitle.textAlignment = kIsRightToLeft ? NSTextAlignmentRight : NSTextAlignmentLeft;
        
        if(self.isEditMode) {
            UIButton *btnSelected = [cell viewWithTag: 4];
            btnSelected.selected = [[self.selectedArray objectAtIndex: indexPath.row] boolValue];
        } else {
            UIImageView *imgArrow = [cell viewWithTag: 4];
            if (kIsRightToLeft) {
                imgArrow.image = [UIImage imageNamed:@"icon-arrsingleL.png"];
            } else {
                imgArrow.image = [UIImage imageNamed:@"icon-arrsingleR.png"];
            }
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.isEditMode) {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath: indexPath];
        
        UIButton *btnSelected = [cell viewWithTag: 4];
        btnSelected.selected = !btnSelected.selected;
        [self.selectedArray replaceObjectAtIndex: indexPath.row withObject: @(btnSelected.selected)];
    } else {
        NSDictionary *msgData = [self.notificationArray objectAtIndex: indexPath.row];
//        [self.delegate pushVcFromParentViewControllerUsingData:[msgData objectForKey: @"PushID"] andIsFromNotification:YES];
        [mSession pushNotificationsDetailView:[msgData objectForKey: @"PushID"] vc:self];
    }
}

#pragma mark - WebService
-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_MESSAGELIST:
            //null handler
            if (![[[response getGenericResponse] objectForKey: @"PushMessages"] isKindOfClass: [NSNull class]]) {
                self.notificationArray = [[response getGenericResponse] objectForKey: @"PushMessages"];
                NSInteger count = [self.notificationArray count];
                while(count > 0) {
                    [self.selectedArray addObject:@(NO)];
                    count -= 1;
                }
                [self.tableView setUserInteractionEnabled:YES];
                [self.btnRight setUserInteractionEnabled:YES];
            }
            else {
                self.notificationArray = [NSMutableArray arrayWithArray:@[]];
                self.selectedArray = [NSMutableArray arrayWithArray:@[]];
                [self.tableView setUserInteractionEnabled:NO];
                [self.btnRight setUserInteractionEnabled:NO];
            }
            
            self.tableView.hidden = NO;
            [self.tableView reloadData];

            NSInteger unreadCount = 0;
            for (NSDictionary *notificationData in self.notificationArray)
            {
                if (![[notificationData objectForKey: @"ReadStatus"] boolValue])
                {
                    //increment unread count
                    unreadCount++;
                }
            }
            [[mSession profileInfo] setUnReadPushMessage: unreadCount];
            
            [[NSNotificationCenter defaultCenter] postNotificationName: @"Unread Icon Changed" object: nil];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"UpdateProfileInfo" object: nil];
            break;
        case kWEBSERVICE_DELETENOTIFICATION:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                [self.tableView reloadData];
                self.isEditMode = NO;
                [self setupView];
            }];
                        
        }
            break;
        default:
            break;
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress: KEY_WINDOW];
    switch (response.webserviceCall) {
        case kWEBSERVICE_DELETENOTIFICATION:
            [[WebServiceManager sharedInstance] fetchMessageList: self];
            break;
            
         default:
            break;
    }
}

@end
