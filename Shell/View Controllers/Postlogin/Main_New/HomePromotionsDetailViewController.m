//
//  HomePromotionsDetailViewController.m
//  Shell
//
//  Created by Nach on 19/12/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "HomePromotionsDetailViewController.h"
#import <WebKit/WebKit.h>

@interface HomePromotionsDetailViewController () <WKNavigationDelegate,UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *tagView;
@property (weak, nonatomic) IBOutlet UILabel *lblTag;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UIView *webViewContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewContainerHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tagViewHeight;
@property WebViewHelper *webViewHelper;
@property (weak, nonatomic) IBOutlet UIView *imageViewHolder;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@end

@implementation HomePromotionsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_RU_BREAKDOWN_DETAILS) subtitle:@""];
    if (kIsRightToLeft) {
        [self.btnBack setImage: [UIImage imageNamed:@"icon-arrsingleR.png"] forState: UIControlStateNormal];
    } else {
        [self.btnBack setImage: [UIImage imageNamed: @"icon-arrsingleL.png"] forState: UIControlStateNormal];
    }
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(@"Home_News/Home_Promotion") screenClass:nil];
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    if(![[self.promoDetails objectForKey:@"Image"] isKindOfClass:[NSNull class]]) {
        NSString *imageUrl = [self.promoDetails objectForKey:@"Image"];
        [self.imageView sd_setImageWithURL: [NSURL URLWithString: imageUrl] placeholderImage: [UIImage imageNamed: @""] options: (SDWebImageRetryFailed | SDWebImageRefreshCached) completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            CGSize imageSize = image.size;
            CGFloat aspectRatio = imageSize.height > 0.0f ? imageSize.width / imageSize.height : 0.0f;
            [NSLayoutConstraint constraintWithItem:self.imageViewHolder attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.imageViewHolder attribute:NSLayoutAttributeHeight multiplier:aspectRatio constant:0].active = YES;
            [NSLayoutConstraint constraintWithItem:self.imageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.imageView attribute:NSLayoutAttributeHeight multiplier:aspectRatio constant:0].active = YES;
        }];
    } else {
        [self.imageView setImage:[UIImage imageNamed: @""]];
    }
   
    if(self.isPromotion) {
        self.lblTag.font = FONT_B3;
        self.lblTag.textColor = COLOUR_RED;
        self.lblTag.text = [self.promoDetails objectForKey:@"Tag"];
        self.lblTag.hidden = NO;
        
        self.tagView.hidden = NO;
        self.tagViewHeight.constant = 20.0;
        [self.tagView.layer setBorderColor:[COLOUR_RED CGColor]];
        [self.tagView.layer setBorderWidth:1.0];
    } else {
        self.lblTag.hidden = YES;
        self.lblTag.text = @"";
        
        self.tagView.hidden = YES;
        self.tagViewHeight.constant = 0.0;
    }
    [self.lblTag sizeToFit];
    
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    self.lblHeader.font = FONT_H(18);
    self.lblHeader.text = [self.promoDetails objectForKey:@"Title"];
    self.lblHeader.textAlignment = kIsRightToLeft ? NSTextAlignmentRight : NSTextAlignmentLeft;

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.webViewHelper = [[WebViewHelper alloc] init];
    [self.webViewHelper setWebViewWithParentView: self.webViewContainer
                         navigationDelegate: self
                                   htmlBody: [NSString stringWithFormat:@"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>%@", [self.promoDetails objectForKey:@"LongDescription"]]];
    [self.webViewHelper setWebConfiguration: WKDataDetectorTypeLink];
    self.webViewHelper.webView.scrollView.delegate = self;
    
    if(IS_COUNTRY(COUNTRYCODE_INDIA)) {
        UIPinchGestureRecognizer *gesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(zoomingImage:)];
        [self.contentView addGestureRecognizer:gesture];
        self.scrollView.contentOffset = CGPointMake(700, 400);
        self.scrollView.delegate = self;
    }
}

- (IBAction)backPressed:(UIButton *)sender {
    [self popSelf];
}

#pragma mark - WKNavigationDelegate
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
    [webView evaluateJavaScript: @"document.documentElement.scrollHeight"
                           completionHandler: ^(id response, NSError *error) {
        self.webViewContainerHeight.constant = [response doubleValue];
        [self.webViewHelper.webView setFrame:CGRectMake(0, 0, self.webViewContainer.frame.size.width, self.webViewContainerHeight.constant)];

        
        [self.view setNeedsDisplay];
        [self.view setNeedsLayout];
        
        [self.webViewHelper.webView setNeedsDisplay];
        [self.webViewHelper.webView setNeedsLayout];
        [self.webViewHelper.webView.scrollView setScrollEnabled:NO];
    }];
    
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    if(navigationAction.navigationType ==  WKNavigationTypeLinkActivated) {
        [[UIApplication sharedApplication] openURL: navigationAction.request.URL];
        
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
    return;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.contentView;
}


@end
