//
//  Home_NewViewController.m
//  Shell
//
//  Created by Nach on 9/12/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "Home_NewViewController.h"
#import "ProfileInfo.h"
#import "LoyaltyProgramGaugeView.h"
#import "LoyaltyBreakdownView.h"
#import "CVSpeedoMeterView.h"
#import "HXPromotionView.h"
#import "ArcView.h"
#import "RUProgressBarView.h"
#import "RUPartnersBarView.h"
#import "RUSpeedoMeterBarView.h"
#import "HomePromoTableViewCell.h"
#import <WebKit/WebKit.h>

#import "HomePromotionViewController.h"
#import "CoachMarkViewController.h"

#import "THIncentiveArcView.h"
#import "PromotionScratchViewController.h"
#import "CheckInViewController.h"
#import "EarnNRedeemModel.h"
#import "THLoyaltyTierPointsModel.h"

@interface Home_NewViewController () <WebServiceManagerDelegate, UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout , WKNavigationDelegate,UITableViewDelegate,UITableViewDataSource, HomePromoCellDelegate,TTTAttributedLabelDelegate,CoachMarkDelegate,PromotionRedeemedDelegate,CheckInDelegate, ArcViewDatasource>

//survey popup
@property (strong, nonatomic) IBOutlet UIView *surveyView;
@property (weak, nonatomic) IBOutlet UIButton *surveyCloseButton;

//in-appnotification popup
@property WebViewHelper *webViewHelper;
@property (strong, nonatomic) IBOutlet UIView *inAppNotificationView;
@property (weak, nonatomic) IBOutlet UIView *inAppWebViewContainer;
@property (weak, nonatomic) IBOutlet UIButton *btnInAppReadMore;
@property (weak, nonatomic) IBOutlet UIButton *btnInAppClose;
@property NSMutableArray *inAppIdArray;

//Notification View
@property (strong, nonatomic) IBOutlet UIView *notificationView;
@property (weak, nonatomic) IBOutlet UILabel *notificationView_Label;
@property (strong, nonatomic) IBOutlet UIButton *btnNotification;
@property (strong, nonatomic) IBOutlet UIButton *btnCall;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *lblGreetings;
@property (weak, nonatomic) IBOutlet UILabel *lblFirstName;
@property (weak, nonatomic) IBOutlet UILabel *lblPointsExpiring;
@property (weak, nonatomic) IBOutlet UILabel *lblPointsAvailable;
@property (weak, nonatomic) IBOutlet UIView *nameView;
@property (weak, nonatomic) IBOutlet UIView *headerViewBorder;
@property (weak, nonatomic) IBOutlet UIView *pointsView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pointsViewWidth;
@property (weak, nonatomic) IBOutlet UIButton *btnHeaderViewArrow;
@property (weak, nonatomic) IBOutlet UIButton *btnPointsViewArrow;

@property (weak, nonatomic) IBOutlet UIPageControl *carouselPageControl;
@property (weak, nonatomic) IBOutlet UICollectionView *carouselColView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *carouselColViewLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *carouselColViewHeight;

@property (weak, nonatomic) IBOutlet UICollectionView *quickLinksColView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *quickLinkColViewLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *quickLinksColViewHeight;

@property (weak, nonatomic) IBOutlet UIView *acctDeactivatedView;
@property (weak, nonatomic) IBOutlet UILabel *lblAcctDeactivate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deactivateViewHeight;

@property (weak, nonatomic) IBOutlet UICollectionView *categoryHeaderCollectionView;
@property (weak, nonatomic) IBOutlet UITableView *categoryTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *categoryTableViewHeight;
@property (weak, nonatomic) IBOutlet UIView *borderViewForCategoryHeader;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *categoryHeaderCollectionViewLayout;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIView *aiView;
@property (weak, nonatomic) IBOutlet UIButton *btnMyShareSummary;

@property BOOL isShowingNextInApp;
@property BOOL isCallBtnAvailable;
@property BOOL isPromotionRedeemed;
@property BOOL canIncreaseHeight;
@property NSDictionary *loyaltyPerformanceCarouselDetails;
@property NSMutableArray *carouselEnumArray;
@property NSMutableDictionary *carouselProfileDict;
@property NSMutableDictionary *carouselPointsDict;
@property NSMutableDictionary *carouselPerformanceDict;
@property NSMutableDictionary *carouselPartnerDict;
@property NSMutableDictionary *carouselPartnerDictB2B;
@property NSMutableDictionary *carouselPartnerDictB2C;
@property NSMutableDictionary *carouselPointsDictB2B;
@property NSMutableDictionary *carouselPointsDictB2C;
@property NSMutableDictionary *promotionDictionary;
@property NSMutableDictionary *pilotPromotionDictionary;
@property NSMutableDictionary *tradesPromotion;
@property NSMutableDictionary *inventoryPromotion;
@property NSMutableArray *quickLinksEnumArray;
@property NSMutableArray *quickLinksArray;

@property NSMutableArray *homeBottomArray;
@property NSMutableArray *categoryCellArray;
@property NSMutableArray *homePromotionComponentArray;

@property NSDictionary *earnNRedeemData;
@property THLoyaltyTierPointsModel *thTierPointsData;
@property NSDictionary *tierDetails;

@property NSInteger selectedIndex;

@property BOOL reloadFlag;
@end

@implementation Home_NewViewController

typedef enum
{
    kCAROUSEL_PROFILE,
    kCAROUSEL_POINTS,
    kCAROUSEL_FASHAREBADGE,
    kCAROUSEL_MYPERFORMANCE,
    kCAROUSEL_OUTLETPERFORMANCE,
    kCAROUSEL_SHAREBADGE,
    kCAROUSEL_LOYALTYPROGRAM,
    kCAROUSEL_LOYALTYBREAKDOWN,
    kCAROUSEL_CNYPROMOTION,
    kCAROUSEL_CVSPEEDOMETER,
    kCAROUSEL_AGRIPROMOTION,
    kCAROUSEL_HXPROMOTION,
    kCAROUSEL_ID_LEBARANPROMOTION,
    kCAROUSEL_HELIXRACE,
    kCAROUSEL_PILOTPROMOTION,
    //Russia
    kCAROUSEL_RU_POINTS_B2C,
    kCAROUSEL_PARTNERS_B2C,

    kCAROUSEL_INVENTORYSPEEDOMETER,
    kCAROUSEL_PARTNERSENDED_B2C,
    kCAROUSEL_RU_POINTS_B2B,
    kCAROUSEL_PARTNERS_B2B,
    kCAROUSEL_PARTNERSENDED_B2B,
    kCAROUSEL_DSRPARTNERS,
    kCAROUSEL_TREARNREDEEM,
    kCAROUSEL_THINCENTIVE_OVERALL,
    kCAROUSEL_THINCENTIVE_BREAKDOWN,
    kCAROUSEL_GAMIFICATION,
    kCAROUSEL_MY_SCANFORREWARDS,
    kCAROUSEL_TIERBADGE,
} kCAROUSEL_TYPES;

typedef enum
{
    kQUICKLINK_DSRMANAGEWORKSHOP = 1000,
    kQUICKLINK_DSRMANAGEORDERS,
    kQUICKLINK_DSRWORKSHOPPERFORMANCE,
    kQUICKLINK_MYNETWORK,
    kQUICKLINK_DSROILDCHANGEREGISTRATION,
    kQUICKLINK_MYLIBRARY,
    kQUICKLINK_SPITAX,
    kQUICKLINK_SEARCHVEHICLEID,
    kQUICKLINK_MYREWARDS,
    kQUICKLINK_MYPERFORMANCE,
    kQUICKLINK_OILCHANGEWALKTHROUGH,
    kQUICKLINK_LUBEMATCH,
    
    
    kQUICKLINK_MANAGEWORKSHOP       = 100,
    kQUICKLINK_PARTICIPATINGPRODUCT,
    kQUICKLINK_TARGET,
    kQUICKLINK_MANAGEORDERS,
    kQUICKLINK_PROMOTIONS,
    kQUICKLINK_SALESKIT,
    kQUICKLINK_MYWORKSHOP,
    kQUICKLINK_REGISTERPRODUCTCODE,
    kQUICKLINK_CUSTOMERREDEMPTION,
    
    kQUICKLINK_INVITECUSTOMER,
    kQUICKLINK_TRADEMANAGEORDERS,
    kQUICKLINK_SCANDECAL,
    kQUICKLINK_ACTIVATEDECAL,
    kQUICKLINK_WORKSHOPPROFILING,
    kQUICKLINK_LOYALTYREWARD,
    
    kQUICKLINK_OILCHANGEREGISTRATION,
    
    kQUICKLINK_IN_MECHPERFORMANCE,
    kQUICKLINK_IN_MANAGEMECHANICS,
    kQUICKLINK_IN_MANAGEMYSTAFF,
    
    kQUICKLINK_WORKSHOPOFFERS,
    kQUICKLINK_CASHINCENTIVES,
    kQUICKLINKS_SCANBOTTLE,
    
    kQUICKLINK_SDA,
    kQUICKLINK_SWA,
    
    //Russia
    kQUICKLINK_PARTNERSOFSHELL,
    kQUICKLINK_RU_WORKSHOPPERFORMANCE,
    kQUICKLINK_DSR_INVENTORYMANAGEMENT,
    kQUICKLINK_TO_INVENTORYMANAGEMENT,

    kQUICKLINK_TO_HASINVENTORYMANAGEMENT,
    kQUICKLINK_DSR_HASINVENTORYMANAGEMENT,
    kQUICKLINK_MEC_HASINVENTORYMANAGEMENT,

} kQUICKLINK_TYPES;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveLanguageChangedNotification:)
        name:kNotificationLanguageChanged
      object:nil];
    
    if(SCREEN_HEIGHT < 700) {
        self.carouselColViewHeight.constant = SCREEN_HEIGHT * 0.37;
    } else {
        self.carouselColViewHeight.constant = SCREEN_HEIGHT * 0.32;
    }
    self.carouselColViewLayout.itemSize = CGSizeMake(SCREEN_WIDTH,  self.carouselColViewHeight.constant);
    self.categoryHeaderCollectionViewLayout.estimatedItemSize = CGSizeMake(200, 40);
    
    self.reloadFlag = YES;
    self.selectedIndex = 0;

    [self.btnMyShareSummary setTitle:LOCALIZATION(C_AI_HOME_MYSHARESUMMARY) forState:UIControlStateNormal];
    self.btnMyShareSummary.titleLabel.font = FONT_B1;

}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    
    [FIRAnalytics setScreenName: LOCALIZATION_EN(C_TITLE_HOME) screenClass: nil];
    
    //Setting the loading State
    [_lblGreetings setText:@""];
    [_lblGreetings setFont:FONT_B2];
    [_lblGreetings setTextColor:COLOUR_VERYDARKGREY];
    [_lblGreetings setBackgroundColor:COLOUR_VERYPALEGREY];
    
    [_lblFirstName setText:@""];
    [_lblFirstName setFont:FONT_H2];
    [_lblFirstName setTextColor:COLOUR_VERYDARKGREY];
    [_lblFirstName setBackgroundColor:COLOUR_VERYPALEGREY];
    
    if(!IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        if(GET_PROFILETYPE == kPROFILETYPE_DSR || GET_PROFILETYPE == kPROFILETYPE_DSM || GET_PROFILETYPE == kPROFILETYPE_DMR) {
            [NSLayoutConstraint constraintWithItem:self.nameView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.headerView attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0].active = YES;
            self.pointsViewWidth.constant = 0;
            self.pointsView.hidden = YES;
            [self.headerView setNeedsLayout];
        }
    }else{
        if(![[mSession profileInfo] hasCSHasMyPoints]){
                [NSLayoutConstraint constraintWithItem:self.nameView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.headerView attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0].active = YES;
                self.pointsViewWidth.constant = 0;
                self.pointsView.hidden = YES;
                [self.headerView setNeedsLayout];
        }
    }
    
    [_lblPointsAvailable setText:@""];
    [_lblPointsAvailable setFont:FONT_H2];
    [_lblPointsAvailable setTextColor:COLOUR_RED];
    [_lblPointsAvailable setBackgroundColor:COLOUR_VERYPALEGREY];
    
    [_lblPointsExpiring setText:@""];
    [_lblPointsExpiring setFont: FONT_B(8)];
    [_lblPointsExpiring setTextColor:COLOUR_VERYDARKGREY];
    self.lblPointsExpiring.hidden = YES;
    
    self.categoryHeaderCollectionView.hidden = YES;
    self.borderViewForCategoryHeader.hidden = YES;
    self.categoryTableView.hidden = YES;
    self.categoryTableView.tableFooterView = [UIView new];
    self.homeBottomArray = [[NSMutableArray alloc] init];
    self.categoryCellArray = [[NSMutableArray alloc] init];
    self.homePromotionComponentArray = [[NSMutableArray alloc] init];
    
    [[WebServiceManager sharedInstance] fetchHome: self];

}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (!self.reloadFlag)
    {
        self.reloadFlag = YES;
        return;
    }
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_HOME) subtitle: @""];
    
    if (IS_COUNTRY(COUNTRYCODE_UNITEDKINGDOM) || [[[mSession lookupTable] objectForKey: @"ContactUSCSNumber"] isKindOfClass: [NSNull class]])
        self.isCallBtnAvailable = NO;
    else
        self.isCallBtnAvailable = YES;
    
    //Accomodating B2B & B2C for RU Call Btn
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        self.isCallBtnAvailable = YES;
        if (GET_WORKSHOP_TNC)
            [[WebServiceManager sharedInstance] requestWorkshopTNC: [GET_WORKSHOP_TNC objectForKey: @"Version"]
                                                              view:nil];
        else
            [[WebServiceManager sharedInstance] requestWorkshopTNC: @""
                                                              view:nil];
        
        if (GET_DSR_TNC)
            [[WebServiceManager sharedInstance] requestDSRTNC: [GET_DSR_TNC objectForKey: @"Version"]
                                                         view:nil];
        else
            [[WebServiceManager sharedInstance] requestDSRTNC: @""
                                                         view:nil];
    }
    
    if(kIsRightToLeft) {
        [_lblGreetings setTextAlignment:NSTextAlignmentRight];
        [_lblFirstName setTextAlignment:NSTextAlignmentRight];
        [_lblPointsAvailable setTextAlignment: NSTextAlignmentRight];
        [_btnHeaderViewArrow setImage:[UIImage imageNamed:@"icon-greyarrL.png"] forState:UIControlStateNormal];
        [_btnPointsViewArrow setImage:[UIImage imageNamed:@"icon-greyarrL.png"] forState:UIControlStateNormal];
        [self.btnMyShareSummary setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
    } else {
        [_lblGreetings setTextAlignment:NSTextAlignmentLeft];
        [_lblFirstName setTextAlignment:NSTextAlignmentLeft];
        [_lblPointsAvailable setTextAlignment: NSTextAlignmentLeft];
        [_btnHeaderViewArrow setImage:[UIImage imageNamed:@"icon-greyarrR.png"] forState:UIControlStateNormal];
        [_btnPointsViewArrow setImage:[UIImage imageNamed:@"icon-greyarrR.png"] forState:UIControlStateNormal];
        [self.btnMyShareSummary setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
    }
    
    [self setupInterface];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void) setupInterface
{
    
    //in App notification interface
    self.btnInAppReadMore.titleLabel.font = FONT_BUTTON;
    [self.btnInAppReadMore setTitle: LOCALIZATION(C_NOTIFICATION_READMORE) forState: UIControlStateNormal];
    self.btnInAppReadMore.backgroundColor = COLOUR_RED;
    
    self.btnInAppClose.titleLabel.font = FONT_BUTTON;
    [self.btnInAppClose setTitle: LOCALIZATION(C_NOTIFICATION_CLOSE) forState: UIControlStateNormal];
    self.btnInAppClose.backgroundColor = COLOUR_YELLOW;
    
    self.carouselPageControl.pageIndicatorTintColor = COLOUR_VERYPALEGREY;
    self.carouselPageControl.currentPageIndicatorTintColor = COLOUR_RED;
    
}

-(void) setFetchHomeNewData {
    NSDictionary *respData = GET_HOMEDATA;

    NSLog(@"******RESPONSE******:%@",respData);

    ProfileInfo *profileInfo = [[ProfileInfo alloc] initWithData: [respData objectForKey: @"MyProfile"]];
    [mSession setProfileInfo: profileInfo];
    
    if(![GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_UNITEDKINGDOM])
    {
        if(self.isCallBtnAvailable) {
            self.navigationItem.rightBarButtonItems = @[
                                                        [[UIBarButtonItem alloc] initWithCustomView: self.btnCall]  ,
                                                        [[UIBarButtonItem alloc] initWithCustomView:self.notificationView]];
        } else {
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.notificationView];
        }
        self.notificationView.userInteractionEnabled = YES;
        
        if ([[mSession profileInfo] unReadPushMessage] > 0)
        {
            [self.notificationView_Label setHidden: NO];
            
            self.notificationView_Label.font = FONT_B3;
            
            if ([[mSession profileInfo] unReadPushMessage] > 99)
                [self.notificationView_Label setText: @"99+"];
            else
                [self.notificationView_Label setText:[NSString stringWithFormat:@"%ld", (long)[[mSession profileInfo] unReadPushMessage]]];
            [self.view layoutIfNeeded];
            [self.view setNeedsLayout];
            
            self.notificationView_Label.layer.cornerRadius = 13.0 / 2.0;
            self.notificationView_Label.layer.masksToBounds = YES;
            self.notificationView_Label.backgroundColor = COLOUR_RED;
        }
        else
        {
            [self.notificationView_Label setHidden: YES];
        }
    }
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    NSDateComponents * comps = [gregorian components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:[NSDate date]];
    _lblGreetings.backgroundColor = [UIColor clearColor];
    _lblFirstName.backgroundColor = [UIColor clearColor];
    if([comps hour] >= 00 && [comps hour] <= 11) {
        _lblGreetings.text = [NSString stringWithFormat:@"%@,",LOCALIZATION(C_HOME_GREET_MORNING)];
    } else if([comps hour] >= 12 && [comps hour] <= 17) {
        _lblGreetings.text = [NSString stringWithFormat:@"%@,",LOCALIZATION(C_HOME_GREET_AFTERNOON)];
    } else {
        _lblGreetings.text = [NSString stringWithFormat:@"%@,",LOCALIZATION(C_HOME_GREET_EVENING)];
    }
    _lblFirstName.text = profileInfo.firstName;
        
    NSDictionary *pointsData = [GET_HOMEDATA objectForKey: @"MyPoints"];
    if(![pointsData isKindOfClass:[NSNull class]]) {
        if(![[pointsData objectForKey:@"BalancePoints"] isKindOfClass:[NSNull class]]) {
            self.lblPointsAvailable.text = [self addCommaThousandSeparatorsWithInputString:[pointsData objectForKey: @"BalancePoints"]];
        } else {
          self.lblPointsAvailable.text = @"0";
        }
    }
    _lblPointsAvailable.backgroundColor = [UIColor clearColor];
    
    
//    NSString *expiringString = LOCALIZATION(@"(#points# pts expires #date#)");
//    expiringString = [expiringString stringByReplacingOccurrencesOfString: @"#points#" withString: @(profileInfo.nextExpiryPoints).stringValue];
//    expiringString = [expiringString stringByReplacingOccurrencesOfString: @"#date#" withString: profileInfo.nextExpiryDate];
    self.lblPointsExpiring.text = @"";
    
    _lblPointsExpiring.backgroundColor = [UIColor clearColor];
    
    if([[mSession profileInfo] isAccountDeActive])
       {
           NSString *contactNumber = @"";
           if (![[[mSession lookupTable] objectForKey: @"ContactUSCSNumber"] isKindOfClass: [NSNull class]])
           {
               contactNumber = [[mSession lookupTable] objectForKey: @"ContactUSCSNumber"];
           }
           NSString *deactivateWithNumber = [LOCALIZATION(C_HOME_ACCOUNTDEACTIVATED) stringByReplacingOccurrencesOfString: @"####" withString: contactNumber];    //lokalised
           self.lblAcctDeactivate.text = deactivateWithNumber;
           self.acctDeactivatedView.hidden = NO;
           self.deactivateViewHeight.constant = 47.0;
       }
       else
       {
           self.deactivateViewHeight.constant = 0;
           self.acctDeactivatedView.hidden = YES;
       }
    
    if(GET_LAUNCH_SDASWA) {
        if([[mSession profileInfo] hasWorkshopAcademy]) {
            [mSession loadSWA:self];
        } else if([[mSession profileInfo] hasDistributorAcademy]) {
            [mSession loadSDA:self];
        }
        SET_LAUNCH_SDASWA(NO);
    }
    
    //Carousel & Quickink setup
    [self carouselSetup];
    [self setupNewQuickLinksMenu];
    //update to ProfileData
    [[NSNotificationCenter defaultCenter] postNotificationName: @"UpdateProfileInfo" object: nil];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"Unread Icon Changed" object: nil];
    
    [self setupHomePromotion];
    
    NSMutableDictionary *coachmarkHasShownData = GET_COACHMARK_HASSHOWN;
    if(coachmarkHasShownData == nil) {
        coachmarkHasShownData = [[NSMutableDictionary alloc] initWithDictionary:@{}];
    }
    //For Coachmark
    if(![[coachmarkHasShownData objectForKey: HOME_COACHMARK] boolValue])
    {
        //if home coachmark has not shown.
        NSMutableArray *coachMarkData = [[NSMutableArray alloc] init];
        if(kIsRightToLeft) {
            [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(10.0, [[UIApplication sharedApplication] statusBarFrame].size.height / 1.2 , 75.0, 45.0) withCaption:LOCALIZATION(C_COACHMARK_HOME_STEP1) andOnSide:@(kCOACHMARK_UP_LEFTSIDE)]];
        } else {
            [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(SCREEN_WIDTH - 90.0, [[UIApplication sharedApplication] statusBarFrame].size.height / 1.2 , 70.0, 45.0) withCaption:LOCALIZATION(C_COACHMARK_HOME_STEP1) andOnSide:@(kCOACHMARK_UP_RIGHTSIDE)]];
        }
        if(!IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
            if(GET_PROFILETYPE == kPROFILETYPE_DSR || GET_PROFILETYPE == kPROFILETYPE_DSM || GET_PROFILETYPE == kPROFILETYPE_DMR) {
                //Points view will b hidden so no coachmark
            } else {
                [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(self.pointsView.frame.origin.x, [[UIApplication sharedApplication] statusBarFrame].size.height + 40.0 , self.pointsView.frame.size.width - 5.0, self.pointsView.frame.size.height + 10.0) withCaption:LOCALIZATION(C_COACHMARK_HOME_STEP2) andOnSide:@(kCOACHMARK_UP_RIGHTSIDE)]];
            }
        }
        [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(5.0, [[UIApplication sharedApplication] statusBarFrame].size.height + 93.0  , SCREEN_WIDTH - 10.0, self.carouselColViewHeight.constant) withCaption:LOCALIZATION(C_COACHMARK_HOME_STEP3) andOnSide:@(kCOACHMARK_UP_LEFTSIDE)]];
            
        [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(5.0, [[UIApplication sharedApplication] statusBarFrame].size.height + 90.0 +  self.carouselColViewHeight.constant , SCREEN_WIDTH - 10.0, self.quickLinksColViewHeight.constant) withCaption:LOCALIZATION(C_COACHMARK_HOME_STEP4) andOnSide:@(kCOACHMARK_DOWN_RIGHTSIDE)]];
            
        [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(5.0, [[UIApplication sharedApplication] statusBarFrame].size.height + 140.0 +  self.carouselColViewHeight.constant + self.quickLinksColViewHeight.constant , SCREEN_WIDTH - 10.0, self.categoryHeaderCollectionViewLayout.itemSize.height + 5.0) withCaption:LOCALIZATION(C_COACHMARK_HOME_STEP5) andOnSide:@(kCOACHMARK_DOWN_LEFTSIDE)]];

        //show coachmark
        [mSession showCoachMark:coachMarkData withDelegate:self onVc:self];

        [coachmarkHasShownData setObject: @(YES) forKey: HOME_COACHMARK];
        SET_COACHMARK_HASSHOWN(coachmarkHasShownData);
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else {
        [self coachMarkDismissed];
    }
    
    
    //TNC priority first. view hierachy: Home > Change Password > TNC
    //check for change password. show if need.
    if (![[[respData objectForKey: @"MyProfile"] objectForKey: @"ChangePassword"] boolValue])
    {
        [mSession pushChangePasswordView: GET_LOGIN_NAME showBackBtn:YES vc:self];
    }
    
    if(![[[respData objectForKey: @"MyProfile"] objectForKey: @"AcceptPrivacyPolicy"] boolValue] && IS_COUNTRY(COUNTRYCODE_RUSSIA))
        [mSession pushPrivacyView:YES vc:self];
    
    //    check for TNC. 1/YES = accepted.
    if (![[[respData objectForKey: @"MyProfile"] objectForKey: @"TermCondition"] boolValue])
    {
        if (GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
        {
            [mSession pushTNCView:YES tncType: 1 vc:self];
        }
        else
        {
            [mSession pushTNCView:YES tncType: 2 vc:self];
        }
    }
    
    if(self.reloadFlag) {
        [[WebServiceManager sharedInstance] fetchRewardsBannerForHome:self];
    } else {
        self.reloadFlag = YES;
    }

}

-(void) coachMarkDismissed {
    NSDictionary *respData = GET_HOMEDATA;
    //read notification
    [self.inAppIdArray removeAllObjects];
    
    //Add In App Notification below change password and TNC
    if ([[[respData objectForKey: @"MyProfile"] objectForKey: @"HasPushNotif"] boolValue])
    {
        self.inAppIdArray = [[respData objectForKey: @"MyProfile"] objectForKey: @"PushNotifList"];
        [self processInAppNotification];
    }
    //Add survey below change password and TNC
    else if ([[[respData objectForKey: @"MyProfile"] objectForKey: @"ShowSurveyPP"] boolValue])
    {
        [self processSurvey];
    }
    
    if ([[[respData objectForKey: @"MyProfile"] objectForKey: @"ShowTaxPopup"] boolValue] && GET_PROFILETYPE != kPROFILETYPE_DSR)
    {
        NSString *urlAddress = [NSString stringWithFormat:@"%@%@%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX,PAGES_COINTAX];
        [self popupTwoButtonWithUrl:urlAddress withLaterBTnTitle:LOCALIZATION(C_BTN_LATER) andActionTitle:LOCALIZATION(C_BTN_UPDATENOW) onLaterPressed:nil onActionPressed:^(BOOL finished){
            [mSession loadMoreInfoInProfile];
        }];
    }
    
    if ([[[respData objectForKey: @"MyProfile"] objectForKey: @"ShowCoinPopup"] boolValue] && GET_PROFILETYPE != kPROFILETYPE_DSR)
    {
        NSString *urlAddress = [NSString stringWithFormat:@"%@%@%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX,PAGES_JOINCOIN];
        [self popupTwoButtonWithUrl:urlAddress withLaterBTnTitle:LOCALIZATION(C_BTN_LATER) andActionTitle:LOCALIZATION(C_BTN_JOINNOW) onLaterPressed:nil onActionPressed:^(BOOL finished){
            [mSession loadMoreInfoInProfile];
        }];
    }

    if([[mSession profileInfo] hasCSHasGamification]) {
        NSMutableDictionary *coachmarkData = GET_COACHMARK_HASSHOWN;
        if(![[coachmarkData objectForKey: CHECKIN_COACHMARK] boolValue])
        {
            NSMutableArray *coachMarkData = [[NSMutableArray alloc] init];
            [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(0,0,0,0) withCaption:LOCALIZATION(C_COACHMARK_CHECKIN_STEP1) andOnSide:@(kCOACHMARK_CENTER)]];

            [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(5.0, [[UIApplication sharedApplication] statusBarFrame].size.height +  self.carouselColViewHeight.constant - 10.0 , SCREEN_WIDTH - 10.0, 61.0) withCaption:LOCALIZATION(C_COACHMARK_CHECKIN_STEP2) andOnSide:@(kCOACHMARK_UP_LEFTSIDE)]];

            [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(10.0, [[UIApplication sharedApplication] statusBarFrame].size.height + 93.0  , SCREEN_WIDTH - 100.0, self.carouselColViewHeight.constant) withCaption:LOCALIZATION(C_COACHMARK_CHECKIN_STEP3) andOnSide:@(kCOACHMARK_UP_LEFTSIDE)]];

            [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(SCREEN_WIDTH - 109.0, [[UIApplication sharedApplication] statusBarFrame].size.height + 208.0 , 100.0, 40.0) withCaption:LOCALIZATION(C_COACHMARK_CHECKIN_STEP4) andOnSide:@(kCOACHMARK_UP_RIGHTSIDE)]];

            [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(SCREEN_WIDTH - 109.0, [[UIApplication sharedApplication] statusBarFrame].size.height + 140.0 , 100.0, 115.0) withCaption:LOCALIZATION(C_COACHMARK_CHECKIN_STEP5) andOnSide:@(kCOACHMARK_UP_RIGHTSIDE)]];

            [mSession showCoachMark:coachMarkData withDelegate:nil onVc:self];

            [coachmarkData setObject: @(YES) forKey: CHECKIN_COACHMARK];
            SET_COACHMARK_HASSHOWN(coachmarkData);
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
    }
    
    if([[mSession profileInfo] hasCSMYHasEarnNReward]) {
        NSMutableDictionary *coachmarkData = GET_COACHMARK_HASSHOWN;
        if(![[coachmarkData objectForKey: MY_SCANFORREWARDS_COACHMARK] boolValue])
        {
            NSMutableArray *coachMarkData = [[NSMutableArray alloc] init];
            CGFloat tillCarouselHeight = [[UIApplication sharedApplication] statusBarFrame].size.height + 93.0 + self.carouselColViewHeight.constant;
            if(SCREEN_HEIGHT < 850 && SCREEN_HEIGHT > 800)
                tillCarouselHeight += 7;
            if(SCREEN_HEIGHT < 700)
                tillCarouselHeight += 15;
            if(GET_PROFILETYPE == kPROFILETYPE_MECHANIC) {
                if(SCREEN_HEIGHT < 800)
                    tillCarouselHeight += 58;
                else
                    tillCarouselHeight += 40;
            }
               
            [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(5.0, [[UIApplication sharedApplication] statusBarFrame].size.height + 93.0  , SCREEN_WIDTH - 10.0, self.carouselColViewHeight.constant) withCaption:LOCALIZATION(C_COACHMARK_SCANFORREWARDS_STEP1) andOnSide:@(kCOACHMARK_UP_LEFTSIDE)]];
            
            [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(10.0, tillCarouselHeight - 107.0 , 30.0, 30.0) withCaption:LOCALIZATION(C_COACHMARK_SCANFORREWARDS_STEP2) andOnSide:@(kCOACHMARK_CENTER)]];
            
            [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(38.0, tillCarouselHeight - 107.0  , 35.0, 30.0) withCaption:LOCALIZATION(C_COACHMARK_SCANFORREWARDS_STEP3) andOnSide:@(kCOACHMARK_UP_LEFTSIDE)]];
            
            [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(15.0, tillCarouselHeight - 207.0  , SCREEN_WIDTH - 30.0, 48.0) withCaption:LOCALIZATION(C_COACHMARK_SCANFORREWARDS_STEP4) andOnSide:@(kCOACHMARK_CENTER)]];
            
            if(GET_PROFILETYPE != kPROFILETYPE_MECHANIC) {
                [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(10.0, [[UIApplication sharedApplication] statusBarFrame].size.height + 93.0 + self.carouselColViewHeight.constant - 52.0  ,SCREEN_WIDTH - 20.0, 53.0) withCaption:LOCALIZATION(C_COACHMARK_SCANFORREWARDS_STEP5) andOnSide:@(kCOACHMARK_UP_LEFTSIDE)]];
            }
            
            [mSession showCoachMark:coachMarkData withDelegate:nil onVc:self];

            [coachmarkData setObject: @(YES) forKey: MY_SCANFORREWARDS_COACHMARK];
            SET_COACHMARK_HASSHOWN(coachmarkData);
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
    }
    
//    [self showAIPage];
}

-(void) setupHomePromotion
{
    NSDictionary *respData = [GET_HOMEDATA objectForKey: @"HomesPromotion"];
    NSArray *promoArray = [respData objectForKey:@"Promotion"];
    
    if([promoArray isKindOfClass:[NSNull class]] || promoArray == nil)
    {
        promoArray = [[NSMutableArray alloc] init];
    }
    else if([promoArray count] > 0)
    {
        NSMutableDictionary *promoDict = [[NSMutableDictionary alloc] init];
        [promoDict setObject: LOCALIZATION(C_HOME_PROMOTIONS) forKey: @"Title"];
        
        [promoDict setObject: promoArray forKey: @"Data"];
        [promoDict setObject: @(HOMEBOTTOMVIEW_IMAGEWITHLABEL) forKey: @"Type"];
        
        [self.homeBottomArray addObject: promoDict];
        [self.homePromotionComponentArray addObject:@"Promotion"];
    }
    
    
    NSArray *newsArray = [respData objectForKey:@"News"];
    if([newsArray isKindOfClass:[NSNull class]] || newsArray == nil)
    {
        newsArray = [[NSMutableArray alloc] init];
    }
    else if([newsArray count] > 0)
    {
        NSMutableDictionary *newsDict = [[NSMutableDictionary alloc] init];
        [newsDict setObject: LOCALIZATION(C_HOME_NEWS) forKey: @"Title"];
        
        [newsDict setObject: newsArray forKey: @"Data"];
        
        [newsDict setObject: @(HOMEBOTTOMVIEW_IMAGEWITHLABEL) forKey: @"Type"];
        
        [self.homeBottomArray addObject: newsDict];
        [self.homePromotionComponentArray addObject:@"News"];
    }
    
    if([self.homeBottomArray count] > 0) {
        NSMutableDictionary *allDict = [[NSMutableDictionary alloc] init];
        [allDict setObject: LOCALIZATION(C_HOME_ALL) forKey: @"Title"];
        [self.homeBottomArray insertObject: allDict atIndex: 0];
    } else {
        NSMutableDictionary *allDict = [[NSMutableDictionary alloc] init];
        [allDict setObject: LOCALIZATION(C_HOME_ALL) forKey: @"Title"];
        [allDict setObject: @(HOMEBOTTOMVIEW_NODATA) forKey: @"Type"];
        [self.homeBottomArray addObject:allDict];
    }
    
    [self homePageBottomViewSetup];
}

-(void) homePageBottomViewSetup
{
     if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
         self.categoryHeaderCollectionView.hidden = YES;
         self.borderViewForCategoryHeader.hidden = YES;
         self.categoryTableView.hidden = YES;
         self.categoryTableViewHeight.constant = 0;
     }else{
         if ([self.homeBottomArray count] > 0)
            {
                self.categoryTableViewHeight.constant = [self.homeBottomArray count] * 180;
                
                self.categoryHeaderCollectionView.hidden = NO;
                self.borderViewForCategoryHeader.hidden = NO;
                self.categoryTableView.hidden = NO;
                
                [self.categoryHeaderCollectionView reloadData];
                [self.categoryTableView reloadData];
                
            } else {
                self.categoryHeaderCollectionView.hidden = YES;
                self.borderViewForCategoryHeader.hidden = YES;
                self.categoryTableView.hidden = YES;
                self.categoryTableViewHeight.constant = 0;
            }
     }
   
}

#pragma mark - Carousel Setup
-(void) carouselSetup {
    NSDictionary *respData = GET_HOMEDATA;
    self.carouselPartnerDict = [respData objectForKey: @"MyPartnersClub"];
    self.carouselPartnerDictB2B = [respData objectForKey: @"MyPartnersClubB2B"];
    self.carouselPartnerDictB2C = [respData objectForKey: @"MyPartnersClubB2C"];
    self.carouselPointsDict = [respData objectForKey: @"MyPoints"];
    self.carouselPointsDictB2B = [respData objectForKey: @"MyPointsB2B"];
    self.carouselPointsDictB2C = [respData objectForKey: @"MyPointsB2C"];
    self.inventoryPromotion = [respData objectForKey:@"InventoryPromotion"];
    self.carouselEnumArray = [[NSMutableArray alloc] init];
    
    if([[mSession profileInfo] hasPilotPromotion]) {
        self.canIncreaseHeight = YES;
        [[WebServiceManager sharedInstance] fetchTradeSpeedometerDetails:self];
        [self.carouselEnumArray addObject:@(kCAROUSEL_PILOTPROMOTION)];
    }
    if([[mSession profileInfo] hasPCMOGamification]) {
        self.canIncreaseHeight = YES;
        [self.carouselEnumArray addObject:@(kCAROUSEL_HELIXRACE)];
    }
  
    if([[mSession profileInfo] hasCSHasTiering]) {
        [[WebServiceManager sharedInstance] fetchCompanyTier: self];
        [self.carouselEnumArray addObject:@(kCAROUSEL_TIERBADGE)];
    }
    
    if([[mSession profileInfo] hasCSHasGamification]) {
        self.canIncreaseHeight = YES;
        [self.carouselEnumArray addObject:@(kCAROUSEL_GAMIFICATION)];
    }
    
    if([[mSession profileInfo] hasCSMYHasEarnNReward]) {
        self.canIncreaseHeight = !(GET_PROFILETYPE == kPROFILETYPE_MECHANIC);
        [[WebServiceManager sharedInstance] loadEarnNRedeem:self];
        [self.carouselEnumArray addObject:@(kCAROUSEL_MY_SCANFORREWARDS)];
    }
    
    if([[mSession profileInfo] hasCSHasTHIncentive]) {
        self.canIncreaseHeight = YES;
        [[WebServiceManager sharedInstance]     loadLoyaltyTierPoints:self];
        [self.carouselEnumArray addObject:@(kCAROUSEL_THINCENTIVE_OVERALL)];
    }
    
    if([[mSession profileInfo] hasCSHasTHOrderBreakdown]) {
        self.canIncreaseHeight = YES;
        if(![[mSession profileInfo] hasCSHasTHIncentive]) {
            [[WebServiceManager sharedInstance] loadLoyaltyTierPoints:self];
        }
        [self.carouselEnumArray addObject:@(kCAROUSEL_THINCENTIVE_BREAKDOWN)];
    }
    
    if([[mSession profileInfo] hasCSHasEarnNReward]) {
        self.canIncreaseHeight = YES;
        [[WebServiceManager sharedInstance] loadEarnNRedeem:self];
        [self.carouselEnumArray addObject:@(kCAROUSEL_TREARNREDEEM)];
    }
    
    if([[mSession profileInfo] hasCSHasLebaranPromotion]) {
        [self.carouselEnumArray addObject:@(kCAROUSEL_ID_LEBARANPROMOTION)];
        self.canIncreaseHeight = YES;
    }
    
    if([[mSession profileInfo] hasCSHasCVBooster]) {
        self.canIncreaseHeight = YES;
        [self.carouselEnumArray addObject:@(kCAROUSEL_CVSPEEDOMETER)];
    }
    
    if([[mSession profileInfo] hasCSHasAgri]) {
        self.canIncreaseHeight = YES;
        [self.carouselEnumArray addObject:@(kCAROUSEL_AGRIPROMOTION)];
    }
    
    if([[mSession profileInfo] hasCSHasHX8Promotion]) {
        self.canIncreaseHeight = YES;
        [self.carouselEnumArray addObject:@(kCAROUSEL_HXPROMOTION)];
    }
    
    if([[mSession profileInfo] hasCSHasLoyaltyProgram]) {
        self.canIncreaseHeight = YES;
        SET_LOYALTYTYPE(@{@"PerformanceCallType" : @"O"});
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[WebServiceManager sharedInstance]fetchLoyaltyPerformaceForContract:@{
                                                                               @"ContractReference": @"",
                                                                               @"CallType" : @"O",
                                                                               }   vc:self];
        [self.carouselEnumArray addObject:@(kCAROUSEL_LOYALTYPROGRAM)];
    }
    
    if([[mSession profileInfo] hasCSHasLoyaltyBD]) {
        self.canIncreaseHeight = YES;
        [self.carouselEnumArray addObject:@(kCAROUSEL_LOYALTYBREAKDOWN)];
    }
    
    if([[mSession profileInfo] hasCSHasFABadge]) {
      //FA Share Badge
        [self.carouselEnumArray addObject:@(kCAROUSEL_FASHAREBADGE)];
    }
    
    //For India
    if([[mSession profileInfo] hasCSHasBadge]) {
        [self.carouselEnumArray addObject:@(kCAROUSEL_SHAREBADGE)];
    }
    
    if (GET_PROFILETYPE != kPROFILETYPE_DSR &&
        [[mSession profileInfo] hasCSHasMyPoints] && !IS_COUNTRY(COUNTRYCODE_RUSSIA)) //add points
    {
        [self.carouselEnumArray addObject: @(kCAROUSEL_POINTS)];
    }
    
    if ([[mSession profileInfo] hasCSHasMyProfile])
        [self.carouselEnumArray addObject: @(kCAROUSEL_PROFILE)];
    
    if([[mSession profileInfo] hasCSHasFAPerformance] || [[mSession profileInfo] hasCSHasOutletPerformanceV2])
    {
        [self.carouselEnumArray addObject:@(kCAROUSEL_OUTLETPERFORMANCE)];
    }
    
    if ([[mSession profileInfo ] hasCSHasDSRPartnersOfShellClub])
    {
        [self.carouselEnumArray addObject: @(kCAROUSEL_DSRPARTNERS)];
    }
    
    if ([[mSession profileInfo] hasCSHasPartnersOfShellClub])
    {
        switch([[mSession profileInfo] businessType]) {
            case 1:
            {
                if (![[self.carouselPartnerDict objectForKey: @"IsPromotionActive"] boolValue])
                    [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERS_B2B)];
                else
                    [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERSENDED_B2B)];
            }
                break;
            case 2:
            {
                if ([[self.carouselPartnerDict objectForKey: @"IsPromotionActive"] boolValue])
                    [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERS_B2C)];
                else
                    [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERSENDED_B2C)];
            }
                break;
            default:
                break;
        }
    }




    if ([[mSession profileInfo] hasCSHasInventorySpeedometer])
    {
      if(IS_COUNTRY(COUNTRYCODE_RUSSIA) )
      {
          self.canIncreaseHeight = YES;
          [self.carouselEnumArray addObject: @(kCAROUSEL_INVENTORYSPEEDOMETER)];

      }

    }

    if(IS_COUNTRY(COUNTRYCODE_RUSSIA) &&  [[mSession profileInfo] hasCSHasMyPoints]) {
        self.canIncreaseHeight = YES;
        if(GET_PROFILETYPE == kPROFILETYPE_DSM || GET_PROFILETYPE == kPROFILETYPE_DSR) {
            [self.carouselEnumArray addObject:@(kCAROUSEL_POINTS)];
        } else {
            switch([[mSession profileInfo] businessType]) {
                case 1:
                {
                    [self.carouselEnumArray addObject:@(kCAROUSEL_RU_POINTS_B2B)];
                }
                    break;
                case 2:
                {
                    [self.carouselEnumArray addObject:@(kCAROUSEL_RU_POINTS_B2C)];
                }
                    break;
                default:
                    break;
            }
        }
    }
    
    if ([[mSession profileInfo] hasCSHasPartnersOfShellClub])
    {
        if(IS_COUNTRY(COUNTRYCODE_RUSSIA) &&  [[mSession profileInfo] hasCSHasMyPoints]) {
            self.canIncreaseHeight = YES;
            switch([[mSession profileInfo] businessType]) {
                case  3:
                {
                    if ([[self.carouselPartnerDict objectForKey: @"IsPromotionActive"] boolValue])
                        [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERS_B2B)];
                    else
                        [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERSENDED_B2B)];
                    [self.carouselEnumArray addObject:@(kCAROUSEL_RU_POINTS_B2B)];
                    if ([[self.carouselPartnerDict objectForKey: @"IsPromotionActive"] boolValue])
                        [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERS_B2C)];
                    else
                        [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERSENDED_B2C)];
                    [self.carouselEnumArray addObject:@(kCAROUSEL_RU_POINTS_B2C)];
                }
                    break;
                default:
                    break;
            }
        }
    }
    
    if ([[mSession profileInfo] hasCSHasInventorySpeedometer])
      {
          if(IS_COUNTRY(COUNTRYCODE_RUSSIA) &&  [[mSession profileInfo] hasCSHasMyPoints]) {
              self.canIncreaseHeight = YES;
              [self.carouselEnumArray addObject: @(kCAROUSEL_INVENTORYSPEEDOMETER)];

          }
      }

    if(self.canIncreaseHeight) {
        if(SCREEN_HEIGHT < 700) {
            self.carouselColViewHeight.constant = SCREEN_HEIGHT * 0.50;
        }else if(SCREEN_HEIGHT < 800) {
            self.carouselColViewHeight.constant = SCREEN_HEIGHT * 0.50;
        } else if(SCREEN_HEIGHT < 850) {
            self.carouselColViewHeight.constant = SCREEN_HEIGHT * 0.50;
        } else if(SCREEN_HEIGHT < 900) {
            self.carouselColViewHeight.constant = SCREEN_HEIGHT * 0.46;
        }
    }
    
    self.carouselColViewLayout.itemSize = CGSizeMake(SCREEN_WIDTH,  self.carouselColViewHeight.constant);
    self.carouselPageControl.numberOfPages = [self.carouselEnumArray count];
    if (kIsRightToLeft) {
        self.carouselPageControl.currentPage = [self.carouselEnumArray count] - 1;
    } else {
        self.carouselPageControl.currentPage = 0;
    }
    [self.carouselColView reloadData];
}

-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.carouselColView) {
        if (kIsRightToLeft) {
            self.carouselPageControl.currentPage = (scrollView.contentSize.width/scrollView.frame.size.width) - (scrollView.contentOffset.x/scrollView.frame.size.width) - 1;
        } else {
            self.carouselPageControl.currentPage = scrollView.contentOffset.x / scrollView.frame.size.width;
        }
    }
}

#pragma mark - AI
-(void) showAIPage {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    if(![GET_DATESTORED_LATEST isEqualToString:dateString] || GET_DATESTORED_LATEST == nil) {
        [mSession popupAIPage:self];
        SET_DATESTORED_LATEST(dateString);
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

#pragma mark - QuickLink Setup
-(void) setupNewQuickLinksMenu
{
    self.quickLinksEnumArray = [[NSMutableArray alloc] init];
    self.quickLinksArray = [[NSMutableArray alloc] init];
    
    switch (GET_PROFILETYPE)
    {
        case kPROFILETYPE_DSR:
            
            if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_UNITEDKINGDOM] )//if UK show menu
            {
                [self.quickLinksEnumArray addObject: @(kQUICKLINK_WORKSHOPPROFILING)];
                [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_TITLE_APPROVEDWORKSHOPPROFILING),  //lokalised
                                                   @"Icon": @"sideicon-manageworkshop",
                                                   }];
            }
            else if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_INDIA])
            {
                [self setupDSRQuickLinksIndia];
            }
            else
                [self setupDSRQuickLinks];
            break;
        case kPROFILETYPE_TRADEOWNER:
            if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_INDIA])
            {
                [self setupTOQuickLinksIndia];
            }
            else
                [self setupTOQuickLinks];
            break;
        case kPROFILETYPE_MECHANIC:
            [self setupMechQuickLinks];
            break;
        case kPROFILETYPE_FORECOURTATTENDANT:
            [self setupFAQuickLinks];
            break;
        case kPROFILETYPE_DMR:
            [self setupDMRQuickLinks];
            break;
        case kPROFILETYPE_DSM:
            [self setupDSMQuickLinks];
            break;
        default:
            break;
    }
    
    self.quickLinkColViewLayout.itemSize = CGSizeMake((SCREEN_WIDTH - 30) / 4, 95);
    [self.quickLinksColView reloadData];
    
}

// home page center links
-(void) setupDSRQuickLinks
{
    if ([[mSession profileInfo] hasQLHasManageWorkshop])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_DSRMANAGEWORKSHOP)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MANAGEMYWORKSHOP), //lokalise 17 jan
                                       @"Icon": @"quicklinks_manageworkshops_midgrey.png",
                                       }];
    }
    
    if ([[mSession profileInfo] hasQLHasInventoryManagement])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_DSR_INVENTORYMANAGEMENT)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_TITLE_INVENTORY),
                                           @"Icon": @"quicklinks_inventory_midgrey.png",
                                           }];
    }

    if ([[mSession profileInfo] HasInventory])
       {
           [self.quickLinksEnumArray addObject: @(kQUICKLINK_DSR_INVENTORYMANAGEMENT)];
           [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_TITLE_INVENTORY),
                                              @"Icon": @"quicklinks_inventory_midgrey.png",
                                              }];
       }

    if ([[mSession profileInfo] hasQLHasLoyaltyProgram])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_LOYALTYREWARD)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_LOYALTY_LOYALTYPROGRAM),    // lokalised
                                           @"Icon": @"quicklinks_partnersofshell_midgrey.png",
                                           }];
    }

    if ([[mSession profileInfo] hasQLHasOrdering_V2])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_DSRMANAGEORDERS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MANAGEORDERS),   //lokalise 17 jan
                                           @"Icon": @"quicklinks_manageorder_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasRewards]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_MYREWARDS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYREWARDS), //lokalise 17 jan
                                           @"Icon": @"quicklinks_myrewards_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasRUPerformance])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_RU_WORKSHOPPERFORMANCE)];
//        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_PARTNERS),                 //lokalised
//                                           @"Icon": @"quicklinks_partnersofshell_midgrey.png",
//                                           }];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_PARTNERS),
                                           @"Icon": @"sideicon_performance_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasAsset])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYLIBRARY)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYLIBRARY),  //lokalise 17 jan
                                           @"Icon": @"quicklinks_mylibrary_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasOneplusOne])
       {
           [self.quickLinksEnumArray addObject: @(kQUICKLINK_SPITAX)];
           [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(TITLE_SPIRAX_GADUS_ONE_PLUS_ONE),  //lokalise 17 jan
                                              @"Icon": @"sideicon_performance_midgrey",
                                              }];
       }

    if ([[mSession profileInfo] hasQLHasSearchVehicleID])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SEARCHVEHICLEID)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_SEARCHVEHICLEID),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_searchvehicleid_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasLubeMatch])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_LUBEMATCH)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_REGOILCHANGE),
                                           @"Icon": @"quicklinks_registerproductcode_midgrey",
                                           }];
    }
    
    //Register Oil Change Walkthrough
    if ([[mSession profileInfo] hasQLOilChangeWalkThrough]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_OILCHANGEWALKTHROUGH)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_REGOILCHANGE),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_registerproductcode_walkthru_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasParticipatingProducts]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_PARTICIPATINGPRODUCT)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_PARTICIPATINGPRODUCTS),  //lokalised 16 Jan
                                           @"Icon": @"sideicon_participatingproducts_midgrey",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasDistributorAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SDA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SDA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasWorkshopAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SWA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SWA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
    
    [self.quickLinksColView reloadData];
}

- (void) ShowAlert:(NSString *)Message {
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:@""
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIView *firstSubview = alert.view.subviews.firstObject;
    UIView *alertContentView = firstSubview.subviews.firstObject;
    for (UIView *subSubView in alertContentView.subviews) {
        subSubView.backgroundColor = [UIColor colorWithRed:141/255.0f green:0/255.0f blue:254/255.0f alpha:1.0f];
    }
    NSMutableAttributedString *AS = [[NSMutableAttributedString alloc] initWithString:Message];
    [AS addAttribute: NSForegroundColorAttributeName value: [UIColor whiteColor] range: NSMakeRange(0,AS.length)];
    [alert setValue:AS forKey:@"attributedTitle"];
    [self presentViewController:alert animated:YES completion:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:^{
        }];
    });
}

-(void) setupDSRQuickLinksIndia {
    
    [self.quickLinksEnumArray addObject:@(kQUICKLINK_MYNETWORK)];
    [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYNETWORK),   //lokalise 17 jan
                                       @"Icon": @"sideicon_managestaff_midgrey",
                                       }];

    if ([[mSession profileInfo] hasQLOilChangeWalkThrough]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_OILCHANGEWALKTHROUGH)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_REGOILCHANGE),   //lokalise 17 jan
                                           @"Icon": @"quicklinks_registerproductcode_walkthru_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasLubeMatch])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_LUBEMATCH)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_REGOILCHANGE),
                                           @"Icon": @"quicklinks_registerproductcode_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLCashIncentive])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_CASHINCENTIVES)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_CASHINCENTIVE),
                                           @"Icon": @"quicklinks_cash_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasOrdering_V2])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_DSRMANAGEORDERS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MANAGEORDERS),   //lokalise 17 jan
                                           @"Icon": @"quicklinks_manageorder_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasRewards]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_MYREWARDS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYREWARDS), //lokalise 17 jan
                                           @"Icon": @"quicklinks_myrewards_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasAsset])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYLIBRARY)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYLIBRARY), //lokalise 17 jan
                                           @"Icon": @"quicklinks_mylibrary_midgrey",
                                           }];
    }
    if ([[mSession profileInfo] hasQLHasOneplusOne])
          {
              [self.quickLinksEnumArray addObject: @(kQUICKLINK_SPITAX)];
              [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(TITLE_SPIRAX_GADUS_ONE_PLUS_ONE),  //lokalise 17 jan
                                                 @"Icon": @"sideicon_performance_midgrey",
                                                 }];
          }

    if([[mSession profileInfo] hasQLHasDistributorAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SDA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SDA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasWorkshopAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SWA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SWA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
    [self.quickLinksColView reloadData];
}

-(void) setupTOQuickLinks
{
    if ([[mSession profileInfo] hasQLHasInventoryManagement])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_TO_INVENTORYMANAGEMENT)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_TITLE_INVENTORY),
                                           @"Icon": @"quicklinks_inventory_midgrey.png",
                                           }];
    }

    if ([[mSession profileInfo] hasQLHasLoyaltyProgram])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_LOYALTYREWARD)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_LOYALTY_LOYALTYPROGRAM),    // lokalised
                                           @"Icon": @"quicklinks_partnersofshell_midgrey.png",
                                           }];
    }

    if ([[mSession profileInfo] HasInventory])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_TO_HASINVENTORYMANAGEMENT)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_TITLE_INVENTORY),
                                           @"Icon": @"quicklinks_inventory_midgrey.png",
                                           }];
    }

    if ([[mSession profileInfo] hasQLHasPartnersOfShellClub])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_PARTNERSOFSHELL)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_PARTNERS),             //lokalised
                                           @"Icon": @"quicklinks_partnersofshell_midgrey.png",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasOrdering_V2])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_TRADEMANAGEORDERS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MANAGEORDERS), //lokalise 17 jan
                                           @"Icon": @"quicklinks_manageorder_midgrey",
                                           }];
    }
    
    
    if ([[mSession profileInfo] hasQLHasRegisterProductCode])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_REGISTERPRODUCTCODE)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_REGOILCHANGE),      //lokalise 17 jan
                                           @"Icon": @"quicklinks_registerproductcode_midgrey",
                                           }];
    }

    if ([[mSession profileInfo] hasQLHasCustomerRedemption])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_CUSTOMERREDEMPTION)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_CUSTREDEMPTION),     //lokalise 17 jan
                                           @"Icon": @"quicklinks_customerredemption_midgrey",
                                           }];
    }

    if ([[mSession profileInfo] hasQLHasSearchVehicleID])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SEARCHVEHICLEID)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_SEARCHVEHICLEID),   //lokalise 17 jan
                                           @"Icon": @"quicklinks_searchvehicleid_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasLubeMatch])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_LUBEMATCH)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_REGOILCHANGE),
                                           @"Icon": @"quicklinks_registerproductcode_midgrey",
                                           }];
    }
    

    if ([[mSession profileInfo] hasQLHasScanDecal])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SCANDECAL)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_SCANDECAL),          //lokalise 17 jan
                                           @"Icon": @"quicklinks_scandecal_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasDecalActivation])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_ACTIVATEDECAL)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_ACTIVATEDECAL), //Scan Decal  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_activatedecal_midgrey",
                                           }];
    }

    if ([[mSession profileInfo] hasQLHasInviteConsumer])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_INVITECUSTOMER)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_INVITECUSTOMER),     //lokalise 17 jan
                                           @"Icon": @"quicklinks_invite_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasRewards]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_MYREWARDS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYREWARDS), //lokalise 17 jan
                                           @"Icon": @"quicklinks_myrewards_midgrey",
                                           }];
    }
    
   /* if ([[mSession profileInfo] hasQLHasManageStaff])
       {
           [self.quickLinksEnumArray addObject: @(kQUICKLINK_IN_MANAGEMYSTAFF)];
           [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_MANAGESTAFF),  //lokalised 16 Jan
                                              @"Icon": @"sideicon_managestaff_midgrey",
                                              }];
       }*/

    if ([[mSession profileInfo] hasQLHasAsset])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYLIBRARY)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYLIBRARY),              //lokalise 17 jan
                                           @"Icon": @"quicklinks_mylibrary_midgrey",
                                           }];
    }
    if ([[mSession profileInfo] hasQLHasOneplusOne])
          {
              [self.quickLinksEnumArray addObject: @(kQUICKLINK_SPITAX)];
              [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(TITLE_SPIRAX_GADUS_ONE_PLUS_ONE),  //lokalise 17 jan
                                                 @"Icon": @"sideicon_performance_midgrey",
                                                 }];
          }

    if ([[mSession profileInfo] hasQLHasParticipatingProducts]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_PARTICIPATINGPRODUCT)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_PARTICIPATINGPRODUCTS),  //lokalise 17 jan
                                           @"Icon": @"sideicon_participatingproducts_midgrey",
                                           }];
    }
    if ([[mSession profileInfo] hasQLHasWorkShopOffer])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_WORKSHOPOFFERS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_TITLE_WORKSHOPOFFER),       //lokalised
                                           @"Icon": @"sideicon_workshopoffers_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasRUPerformance])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_RU_WORKSHOPPERFORMANCE)];
        
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_WORKSHOPPERFORMANCE),      //lokalised
                                           @"Icon": @"sideicon_performance_midgrey",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasWorkshopAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SWA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SWA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
}

-(void) setupTOQuickLinksIndia {
    
    if([[mSession profileInfo] hasQLHasManageWorkshop]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_MYNETWORK)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYNETWORK),   //lokalise 17 jan
                                           @"Icon": @"sideicon_managestaff_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasOrdering_V2])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_TRADEMANAGEORDERS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MANAGEORDERS), //lokalise 17 jan
                                           @"Icon": @"quicklinks_manageorder_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasCSHasFAPerformance])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYPERFORMANCE)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYPERFORMANCE),  //lokalise 17 jan
                                           @"Icon": @"sideicon_performance_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasRegisterProductCode]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_OILCHANGEREGISTRATION)];  //lokalise 17 jan
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_REGOILCHANGE),
                                           @"Icon": @"quicklinks_registerproductcode_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasSearchVehicleID])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SEARCHVEHICLEID)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_SEARCHVEHICLEID), //lokalised 16 Jan
                                           @"Icon": @"quicklinks_searchvehicleid_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasLubeMatch])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_LUBEMATCH)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_REGOILCHANGE),
                                           @"Icon": @"quicklinks_registerproductcode_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLCashIncentive])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_CASHINCENTIVES)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_CASHINCENTIVE),
                                           @"Icon": @"quicklinks_cash_midgrey",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasBottleSale]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINKS_SCANBOTTLE)];
        [self.quickLinksArray addObject:@{
               @"Label" : LOCALIZATION(C_QL_SCANBOTTLE),
               @"Icon"  : @"quicklinks_scanbottle_midgray",
           }];
    }
    
    if ([[mSession profileInfo] hasQLHasCustomerRedemption])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_CUSTOMERREDEMPTION)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_CUSTREDEMPTION),
                                           @"Icon": @"quicklinks_customerredemption_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasRewards]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_MYREWARDS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYREWARDS),  //lokalise 17 jan
                                           @"Icon": @"quicklinks_myrewards_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasAsset])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYLIBRARY)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYLIBRARY),   //lokalise 17 jan
                                           @"Icon": @"quicklinks_mylibrary_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasOneplusOne])
          {
              [self.quickLinksEnumArray addObject: @(kQUICKLINK_SPITAX)];
              [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(TITLE_SPIRAX_GADUS_ONE_PLUS_ONE),  //lokalise 17 jan
                                                 @"Icon": @"sideicon_performance_midgrey",
                                                 }];
          }

    if ([[mSession profileInfo] hasQLHasWorkShopOffer])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_WORKSHOPOFFERS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_TITLE_WORKSHOPOFFER),       //lokalised
                                           @"Icon": @"sideicon_workshopoffers_midgrey",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasWorkshopAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SWA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SWA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
    [self.quickLinksColView reloadData];
}

-(void) setupMechQuickLinks
{
    if (![[mSession profileInfo] redeemOnly])//redeem only. add menu if false)
    {
        if ([[mSession profileInfo] hasQLHasSearchVehicleID])
        {
            [self.quickLinksEnumArray addObject: @(kQUICKLINK_SEARCHVEHICLEID)];
            [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_SEARCHVEHICLEID),  //lokalise 17 jan
                                               @"Icon": @"quicklinks_searchvehicleid_midgrey",
                                               }];
        }
        

        if ([[mSession profileInfo] HasInventory])
        {
            [self.quickLinksEnumArray addObject: @(kQUICKLINK_MEC_HASINVENTORYMANAGEMENT)];
            [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_TITLE_INVENTORY),
                                               @"Icon": @"quicklinks_inventory_midgrey.png",
                                               }];
        }


        if ([[mSession profileInfo] hasQLHasRegisterProductCode])
        {
            [self.quickLinksEnumArray addObject: @(kQUICKLINK_REGISTERPRODUCTCODE)];
            [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_REGOILCHANGE),  //lokalise 17 jan
                                               @"Icon": @"quicklinks_registerproductcode_midgrey",
                                               }];
        }
        
        if ([[mSession profileInfo] hasQLHasLubeMatch])
        {
            [self.quickLinksEnumArray addObject: @(kQUICKLINK_LUBEMATCH)];
            [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_REGOILCHANGE),
                                               @"Icon": @"quicklinks_registerproductcode_midgrey",
                                               }];
        }
        
        if ([[mSession profileInfo] hasQLHasScanDecal])
        {
            [self.quickLinksEnumArray addObject: @(kQUICKLINK_SCANDECAL)];
            [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_SCANDECAL),      //lokalise 17 jan
                                               @"Icon": @"quicklinks_scandecal_midgrey",
                                               }];
        }
        
        if ([[mSession profileInfo] hasQLHasDecalActivation])
        {
            [self.quickLinksEnumArray addObject: @(kQUICKLINK_ACTIVATEDECAL)];
            [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_ACTIVATEDECAL), //Scan Decal  //lokalised 16 Jan
                                               @"Icon": @"quicklinks_activatedecal_midgrey",
                                               }];
        }
    }
    
    if ([[mSession profileInfo] hasQLHasRewards])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYREWARDS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYREWARDS),          //lokalise 17 jan
                                           @"Icon": @"quicklinks_myrewards_midgrey",
                                           }];
    }
    
    
    if ([[mSession profileInfo] hasQLHasAsset])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYLIBRARY)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYLIBRARY), //lokalise 17 jan
                                           @"Icon": @"quicklinks_mylibrary_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasOneplusOne])
          {
              [self.quickLinksEnumArray addObject: @(kQUICKLINK_SPITAX)];
              [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(TITLE_SPIRAX_GADUS_ONE_PLUS_ONE),  //lokalise 17 jan
                                                 @"Icon": @"sideicon_performance_midgrey",
                                                 }];
          }

    if ([[mSession profileInfo] hasQLHasInviteConsumer])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_INVITECUSTOMER)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_INVITECUSTOMER), //lokalise 17 jan
                                           @"Icon": @"quicklinks_invite_midgrey",
                                           }];
    }
    if ([[mSession profileInfo] hasQLHasParticipatingProducts]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_PARTICIPATINGPRODUCT)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_PARTICIPATINGPRODUCTS), //lokalise 17 jan
                                           @"Icon": @"sideicon_participatingproducts_midgrey",
                                           }];
    }
    if ([[mSession profileInfo] hasQLHasCustomerRedemption])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_CUSTOMERREDEMPTION)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_CUSTREDEMPTION), //lokalise 17 jan
                                           @"Icon": @"quicklinks_customerredemption_midgrey",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasWorkshopAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SWA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SWA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
}

-(void) setupFAQuickLinks {
    if (![[mSession profileInfo] redeemOnly])//redeem only. add menu if false)
    {
        if ([[mSession profileInfo] hasQLHasSearchVehicleID])
        {
            [self.quickLinksEnumArray addObject: @(kQUICKLINK_SEARCHVEHICLEID)];
            [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_SEARCHVEHICLEID),  //lokalised 16 Jan
                                               @"Icon": @"quicklinks_searchvehicleid_midgrey",
                                               }];
        }
        
        if ([[mSession profileInfo] hasQLHasRegisterProductCode])
        {
            [self.quickLinksEnumArray addObject: @(kQUICKLINK_REGISTERPRODUCTCODE)];
            [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_REGOILCHANGE),  //lokalised 16 Jan
                                               @"Icon": @"quicklinks_registerproductcode_midgrey",
                                               }];
        }
        
        if ([[mSession profileInfo] hasQLHasScanDecal])
        {
            [self.quickLinksEnumArray addObject: @(kQUICKLINK_SCANDECAL)];
            [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_SCANDECAL), //Scan Decal  //lokalised 16 Jan
                                               @"Icon": @"quicklinks_scandecal_midgrey",
                                               }];
        }
        
        if ([[mSession profileInfo] hasQLHasDecalActivation])
        {
            [self.quickLinksEnumArray addObject: @(kQUICKLINK_ACTIVATEDECAL)];
            [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_ACTIVATEDECAL), //Scan Decal  //lokalised 16 Jan
                                               @"Icon": @"quicklinks_activatedecal_midgrey",
                                               }];
        }
    }
    
    if ([[mSession profileInfo] hasQLHasRewards])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYREWARDS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYREWARDS),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_myrewards_midgrey",
                                           }];
    }
    
    
    if ([[mSession profileInfo] hasQLHasAsset])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYLIBRARY)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYLIBRARY),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_mylibrary_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasOneplusOne])
          {
              [self.quickLinksEnumArray addObject: @(kQUICKLINK_SPITAX)];
              [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(TITLE_SPIRAX_GADUS_ONE_PLUS_ONE),  //lokalise 17 jan
                                                 @"Icon": @"sideicon_performance_midgrey",
                                                 }];
          }

    if ([[mSession profileInfo] hasQLHasInviteConsumer])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_INVITECUSTOMER)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_INVITECUSTOMER),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_invite_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasParticipatingProducts]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_PARTICIPATINGPRODUCT)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_PARTICIPATINGPRODUCTS),  //lokalised 16 Jan
                                           @"Icon": @"sideicon_participatingproducts_midgrey",
                                           }];
    }
    if ([[mSession profileInfo] hasQLHasCustomerRedemption])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_CUSTOMERREDEMPTION)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_CUSTREDEMPTION),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_customerredemption_midgrey",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasWorkshopAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SWA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SWA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
}

-(void) setupDMRQuickLinks
{
    //manage mechanics
    if ([[mSession profileInfo] hasQLHasManageStaff])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_IN_MANAGEMECHANICS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MANAGEMYMECH),  //lokalised 16 Jan
                                           @"Icon": @"sideicon_managestaff_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasOrdering_V2])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_DSRMANAGEORDERS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MANAGEORDERS),   //lokalise 17 jan
                                           @"Icon": @"quicklinks_manageorder_midgrey",
                                           }];
    }
    
    //Register Oil Change Walkthrough
    if ([[mSession profileInfo] hasQLOilChangeWalkThrough]) {
        [self.quickLinksEnumArray addObject:@(kQUICKLINK_OILCHANGEWALKTHROUGH)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_REGOILCHANGE),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_registerproductcode_walkthru_midgrey",
                                           }];
    }
    
    //My Rewards
    if ([[mSession profileInfo] hasQLHasRewards])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYREWARDS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYREWARDS),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_myrewards_midgrey",
                                           }];
    }
    
    //My Library
    if ([[mSession profileInfo] hasQLHasAsset])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYLIBRARY)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYLIBRARY),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_mylibrary_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasOneplusOne])
          {
              [self.quickLinksEnumArray addObject: @(kQUICKLINK_SPITAX)];
              [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(TITLE_SPIRAX_GADUS_ONE_PLUS_ONE),  //lokalise 17 jan
                                                 @"Icon": @"sideicon_performance_midgrey",
                                                 }];
          }

    if([[mSession profileInfo] hasQLHasDistributorAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SDA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SDA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasWorkshopAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SWA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SWA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
}

-(void) setupDSMQuickLinks {
    if ([[mSession profileInfo] hasQLHasPartnersOfShellClub])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_PARTNERSOFSHELL)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_PARTNERS),             //lokalised
                                           @"Icon": @"quicklinks_partnersofshell_midgrey.png",
          
        
        }];
    }
    
    //My Rewards
    if ([[mSession profileInfo] hasQLHasRewards])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYREWARDS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYREWARDS),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_myrewards_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasAsset])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYLIBRARY)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_QL_MYLIBRARY),  //lokalised 16 Jan
                                           @"Icon": @"quicklinks_mylibrary_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasOneplusOne])
          {
              [self.quickLinksEnumArray addObject: @(kQUICKLINK_SPITAX)];
              [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(TITLE_SPIRAX_GADUS_ONE_PLUS_ONE),  //lokalise 17 jan
                                                 @"Icon": @"sideicon_performance_midgrey",
                                                 }];
          }

    if([[mSession profileInfo] hasQLHasDistributorAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SDA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SDA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
    if([[mSession profileInfo] hasQLHasWorkshopAcademy]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SWA)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_DRAWER_SWA), //lokalise 17 jan
                                           @"Icon": @"quicklinks_swa_midgray",
                                           }];
    }
    
}

#pragma mark - Helper Methods
-(NSString *) convertDate: (NSString *) inputString
{
    //convert string to date
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat: @"dd-MM-yyyy"];
    [inputFormatter setTimeZone: [NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *inputDate = [inputFormatter dateFromString: inputString];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat: @"MMM yy"];
    [outputFormatter setTimeZone: [NSTimeZone timeZoneWithName:@"GMT"]];
    
    //convert inputString to double value
    //double value to decimal style string
    return [outputFormatter stringFromDate: inputDate];
}

-(NSString *) addCommaThousandSeparatorsWithInputString: (NSString *) inputString
{
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    [fmt setNumberStyle:NSNumberFormatterDecimalStyle]; // to get commas (or locale equivalent)
    [fmt setGroupingSeparator:@","];

    return [fmt stringFromNumber:@([inputString doubleValue])];
}

- (void) processSurvey
{
    CGSize scrollContentSize = self.view.frame.size;
    self.surveyView.frame = CGRectMake(5, 5, scrollContentSize.width, scrollContentSize.height);
    [self.view addSubview: self.surveyView];
    
    NSString *urlAddress = [NSString stringWithFormat:@"%@%@%@?sid=%@&ss=1",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_SURVEYFORM_URL, [[mSession profileInfo] surveyId]];
    
    WebViewHelper *webViewHelper = [[WebViewHelper alloc] init];
    [webViewHelper setWebViewWithParentView: self.surveyView
                         navigationDelegate: self
                                 urlAddress: urlAddress];
    [webViewHelper setWebConfiguration: WKDataDetectorTypeLink];
    
    self.surveyCloseButton.frame = CGRectMake(SCREEN_WIDTH - 60, 0, 20, 20);
    [self.surveyView addSubview:self.surveyCloseButton];
}


- (void) processInAppNotification
{
    if(self.isShowingNextInApp) {
        [UpdateHUD removeMBProgress: KEY_WINDOW];
        self.isShowingNextInApp = false;
    }
    
    if ([self.inAppIdArray count] > 0)
    {
        self.inAppNotificationView.frame = CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height - 30);
        [self.view addSubview: self.inAppNotificationView];
        self.inAppWebViewContainer.frame = CGRectMake(15,15, self.view.frame.size.width - 50, self.view.frame.size.height - 200);
        
        NSString *urlAddress = [NSString stringWithFormat:@"%@%@%@?pnid=%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_INAPPNOTIFICATION_URL, [self.inAppIdArray objectAtIndex: 0]];
        
        self.webViewHelper = [[WebViewHelper alloc] init];
        [self.webViewHelper setWebViewWithParentView: self.inAppWebViewContainer
                             navigationDelegate: self
                                     urlAddress: urlAddress];
        
        [self.webViewHelper setWebConfiguration: WKDataDetectorTypeLink];
    }
    else
    {
        if ([[mSession profileInfo] showSurveyPp])
        {
            [self processSurvey];
        }
    }
}

- (CAAnimation*) getShakeAnimation
{
    CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];

    CGFloat wobbleAngle = 0.50f;

    NSValue* valLeft = [NSValue valueWithCATransform3D:CATransform3DMakeRotation(wobbleAngle, 0.0f, 0.0f, 1.0f)];
    NSValue* valRight = [NSValue valueWithCATransform3D:CATransform3DMakeRotation(-wobbleAngle, 0.0f, 0.0f, 1.0f)];
    animation.values = [NSArray arrayWithObjects:valLeft, valRight, nil];

    animation.autoreverses = YES;
    animation.duration = 0.125;
    animation.repeatCount = HUGE_VALF;

    return animation;
}

#pragma mark - Delegate Method
-(void) promotionIsRedeemed {
    [[WebServiceManager sharedInstance] fetchHome: self];
}

-(void) checkedInToday {
    [[WebServiceManager sharedInstance] fetchHome: self];
}

#pragma mark - Action Methods
- (IBAction) callPressed:(id)sender {
    NSString *csNumberString =  [[mSession lookupTable] objectForKey: @"ContactUSCSNumber"];
    
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        switch([[mSession profileInfo] businessType]) {
            case 1:{
                csNumberString =  [[mSession lookupTable] objectForKey: @"ContactUSB2BCSNumber"];
                NSString *callUrlString = [NSString stringWithFormat: @"tel:%@",
                                           [csNumberString stringByReplacingOccurrencesOfString: @" " withString: @""]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: callUrlString]];
            }
                break;
            case 2:
            {
                csNumberString =  [[mSession lookupTable] objectForKey: @"ContactUSB2CCSNumber"];
                NSString *callUrlString = [NSString stringWithFormat: @"tel:%@",
                                           [csNumberString stringByReplacingOccurrencesOfString: @" " withString: @""]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: callUrlString]];
            }
                break;
            case 3:
            {
                [self popupCustomerService];
            }
                break;
            default:
                break;
        }
    } else {
        //need to remove space
        NSString *callUrlString = [NSString stringWithFormat: @"tel:%@",
                                   [csNumberString stringByReplacingOccurrencesOfString: @" " withString: @""]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: callUrlString]];
    }
}
- (IBAction)notificationPressed:(UIButton *)sender {
    if([self.homePromotionComponentArray containsObject:@"Promotion"] && [self.homePromotionComponentArray containsObject:@"News"]) {
        [mSession pushHomePromotionsWithSelectedIndex:2 onVc:self];
    } else if([self.homePromotionComponentArray containsObject:@"Promotion"] || [self.homePromotionComponentArray containsObject:@"News"]) {
        [mSession pushHomePromotionsWithSelectedIndex:1 onVc:self];
    } else {
        [mSession pushHomePromotionsWithSelectedIndex:0 onVc:self];
    }
}

- (IBAction)viewProfilePressed:(id)sender {
    [UpdateHUD addMBProgress: KEY_WINDOW withText:LOADING_HUD];
    
    //only india TO/iM profile due to Bank Details
     if (IS_COUNTRY(COUNTRYCODE_INDIA)) {
           if(GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER || GET_PROFILETYPE == kPROFILETYPE_DSR || GET_PROFILETYPE == kPROFILETYPE_DMR) {
               [mSession loadProfileTabView];
           } else {
               [mSession loadMyProfileView];
           }
      } else if(IS_COUNTRY(COUNTRYCODE_INDONESIA)) {
          if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
              [mSession loadMyProfileView];
          } else {
               [mSession loadProfileTabView];
          }
      }
       else
           [mSession loadMyProfileView];
}

- (IBAction)viewPointsPressed:(id)sender {
    [mSession loadRewardsView];
}

- (IBAction)viewPerformancePressed:(id)sender {
    [mSession loadINPerformance];
}

- (IBAction )landOnPerformancePressed:(UIButton *)sender {
    [mSession setLoadPerformance:YES];
//    [mSession loadPerformanceView];
    [mSession loadINPerformance];
}

- (IBAction) profilePointsPressed:(id)sender {
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
         [mSession loadRUWorkshopPerformance];
    } else {
         [mSession loadRecentActivity];
    }
}

- (IBAction)viewTOLoyaltyPressed:(id)sender {
    [mSession loadTOLoyaltySummaryView];
}

- (IBAction)orderNowPressed:(UIButton *)sender {
    [mSession loadManageOrder];
}

- (IBAction)loyaltyBreakdownPressed:(UIButton *)sender {
//    [mSession pushLoyaltyCartonBreakdown:_loyaltyPerformanceCarouselDetails vc:self];
    [mSession loadLoyaltyContract];
}

-(void) scanNowPressed:(UIButton *)sender {
    [mSession loadScanProductView];
}

-(void) viewRecentActivityPressed:(UIButton *) sender {
    [mSession loadRecentActivity];
}

- (IBAction)earnPointsPressed:(id)sender {
    [mSession loadRUEarnPoints];
}

- (IBAction)dsrViewPerformancePressed:(id)sender
{
    [mSession loadRUWorkshopPerformance];
}

- (IBAction)firstKeyPressed:(id)sender
{
    [mSession loadRUWorkshopPerformanceFromCarousel: @"Order BreakDown"];
}

- (IBAction)secondKeyPressed:(id)sender
{
    [mSession loadRUWorkshopPerformanceFromCarousel: @"Oil Change BreakDown"];
}

- (IBAction)thirdKeyPressed:(id)sender
{
    [mSession loadRUWorkshopPerformanceFromCarousel: @"Customer Break BreakDown"];
}

- (IBAction)closeSurvey:(id)sender {
    
    if (![self.webViewHelper.webView.URL.absoluteString containsString: @"SurveyThanks.aspx"])
    {
        [[WebServiceManager sharedInstance] closeSurveyPopup: @{
                                                                @"SurveyID": [[mSession profileInfo] surveyId],
                                                                @"IsShowPP": @(NO),
                                                                }
                                                          vc: nil];
    }
    
    [self.surveyView removeFromSuperview];
}

- (IBAction)promotion_viewDetailsPressed:(UIButton *)sender {
    
//    if([GET_LOCALIZATION isEqualToString: kEnglish]) {
//        [self popupInitWithURL:[NSString stringWithFormat:@"%@%@ViewDetails.aspx?c=%@&lc=1",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, GET_COUNTRY_CODE] headerTitle:@"" buttonTitle:LOCALIZATION(C_GLOBAL_OK)];
//    } else {
//        [self popupInitWithURL:[NSString stringWithFormat:@"%@%@ViewDetails.aspx?c=%@&lc=2",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, GET_COUNTRY_CODE] headerTitle:@"" buttonTitle:LOCALIZATION(C_GLOBAL_OK)];
//    }
    [[WebServiceManager sharedInstance] viewPromotionDetailsForPromotion:self];
    
}

- (IBAction)promotion_redeemPressed:(UIButton *)sender {
//    [[WebServiceManager sharedInstance] promotionRedeem:self];
    [mSession presentScratchViewOn:self forGamification:NO];
}

- (IBAction)readMoreInApp:(id)sender {
    //push to notification list > notification details with ID
    [mSession handlePushNotificationWithReferenceKey: @"NL"
                                      referenceValue: [self.inAppIdArray objectAtIndex: 0]];
}


- (IBAction)checkinBtnPressed:(UIButton *)sender {
    [mSession pushCheckInTodayPopuponVc:self];
}

- (IBAction)grandPrizePressed:(UIButton *)sender {
    [mSession presentScratchViewOn:self forGamification:YES];
}

-(void) viewMorePressed:(UIButton *)sender {
    NSMutableArray *productNameArray = [[NSMutableArray alloc] init];
    NSArray *redeemDetails = [self.earnNRedeemData objectForKey:@"AutoRedemptionDetails"];
    if(![redeemDetails isKindOfClass:[NSNull class]]) {
        [productNameArray addObject:LOCALIZATION(C_KEYWORD_ALL)];
        for(NSDictionary *dict in redeemDetails) {
            [productNameArray addObject: [dict objectForKey:@"ProductGroup"]];
        }
        [mSession pushScanNRewardReport:productNameArray vc:self];
    }
}


- (IBAction)closeInApp:(id)sender {
    
    //call read api
    [[WebServiceManager sharedInstance] readPushMessage: [self.inAppIdArray objectAtIndex: 0] vc: nil];
    
    //remove first object
    [self.inAppIdArray removeObjectAtIndex: 0];
    
    //"close" notification, proceed to see next notification
    [self.inAppNotificationView removeFromSuperview];
    
    //check for more inapp push, otherwise, survey or close
    if ([self.inAppIdArray count] > 0) {
        self.isShowingNextInApp = true;
        [UpdateHUD addMBProgress: KEY_WINDOW withText: @""];
    }
    [self performSelector: @selector(processInAppNotification) withObject:nil afterDelay:0.1f];
}

- (IBAction)btnMyShareSummaryPressed:(UIButton *)sender {
    [mSession popupAIPage:self];
}

#pragma mark ArcView datasource

-(int)arcView:(ArcView*) arcView minValueFor:(int) view {
  return 0;
}
-(int)arcView:(ArcView*) arcView maxValueFor:(int) view {
  return [self.pilotPromotionDictionary[@"MaxOilChangeCount"] intValue];
}
-(int)arcView:(ArcView*) arcView progressValueFor:(int) view {
  return [self.pilotPromotionDictionary[@"CurrentOilChangeCount"] intValue];
}
- (UIColor *)arcView:(ArcView *)arcView colorForArc:(int)view {

  if ([self.pilotPromotionDictionary[@"MaxOilChangeCount"] intValue] == [self.pilotPromotionDictionary[@"CurrentOilChangeCount"] intValue]) { return COLOUR_LIGHTGREEN; }
  return COLOUR_YELLOW;
}
- (UIColor *)arcView:(ArcView *)arcView backgroundColorForArc:(int)view {
  return COLOUR_VERYPALEGREY;
}
- (CGFloat)arcView:(ArcView *)arcView barWidthFor:(int)view {
  return 10;
}
- (NSAttributedString *)arcView:(ArcView *)arcView centerTextForArcView:(int)view {

  int currentValue = [self.pilotPromotionDictionary[@"CurrentOilChangeCount"] intValue];
  int maxValue = [self.pilotPromotionDictionary[@"MaxOilChangeCount"] intValue];
  NSDictionary *attrDict = @{
      NSFontAttributeName: FONT_B3,
  };
  NSAttributedString *returnString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"AX5/AX7 Oil Changes\n%d/%d", currentValue, maxValue] attributes:attrDict];

  return returnString;
}
- (int)arcView:(ArcView *)arcView numberOfMarkersFor:(int)view {
  NSString *mileStones = self.pilotPromotionDictionary[@"MileStones"];
  NSArray *mileStoneArray = [mileStones componentsSeparatedByString:@","];
  return mileStoneArray.count+2;
}
- (int)arcView:(ArcView *)arcView valueForMarkerAtIndex:(int)index {
  if (index == 0) {
    return 0;
  }
  NSString *mileStones = self.pilotPromotionDictionary[@"MileStones"];
  NSArray *mileStoneArray = [mileStones componentsSeparatedByString:@","];
  if (index == mileStoneArray.count+1) {
    return [self.pilotPromotionDictionary[@"MaxOilChangeCount"] intValue];
  }
  return [mileStoneArray[index-1] intValue];
}
-(NSAttributedString*)arcView:(ArcView*) arcView outerTitleForMarkerAtIndex:(int) index {
  if (index == 0) {
    return [[NSAttributedString alloc] initWithString: @"0"];
  }
  NSString *mileStones = self.pilotPromotionDictionary[@"MileStones"];
  NSArray *mileStoneArray = [mileStones componentsSeparatedByString:@","];
  if (index == mileStoneArray.count+1) {
    int count = [self.pilotPromotionDictionary[@"MaxOilChangeCount"] intValue];
    NSString *string = [[NSString alloc] initWithFormat: @"%d", count];
    return [[NSAttributedString alloc] initWithString: string];
  }
  int count = [mileStoneArray[index-1] intValue];
  NSString *string = [[NSString alloc] initWithFormat: @"%d", count];
  return [[NSAttributedString alloc] initWithString: string];
}

#pragma mark - TTTAttributedLabel Label Delegate
-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url absoluteString] isEqualToString: @"registerOil"]) {
       [mSession loadScanProductView];
    } else if ([[url absoluteString] isEqualToString: @"GamificationDetails"]) {
        [[WebServiceManager sharedInstance] viewGamificationDetails:self];
    } else if ([[url absoluteString] isEqualToString: @"badgeLearnMore"]) {
        [mSession pushGenericWebViewWith:LOCALIZATION(C_SHAREPARTNERSCLUB_TITLE) andURL:[self.tierDetails objectForKey:@"InfoLink"] onVC:self];
    }
}

#pragma mark - Pan Gesture Recognizer

- (IBAction)pullUpAction:(UIPanGestureRecognizer *)sender {
    CGPoint velocity = [sender velocityInView:self.scrollView];
    if(velocity.x > 0)
    {
        [mSession popupAIPage:self];
    }
}

#pragma mark - Localization Methods
- (void) receiveLanguageChangedNotification:(NSNotification *) notification
{
    if ([notification.name isEqualToString:kNotificationLanguageChanged])
    {
        LoginInfo *loginInfo = [mSession loadUserProfile];
        if([[mSession profileInfo] hasCSHasLoyaltyProgram] && loginInfo.tradeID != nil) {
            SET_LOYALTYTYPE(@{@"PerformanceCallType" : @"O"});
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[WebServiceManager sharedInstance]fetchLoyaltyPerformaceForContract:@{
                                                                                   @"ContractReference": @"",
                                                                                   @"CallType" : @"O",
                                                                                   }   vc:self];
            [self.carouselEnumArray addObject:@(kCAROUSEL_LOYALTYPROGRAM)];
        }

        [mSession updateAnalyticsUserProperty];
    }
}

#pragma mark - UICollectionView Delegate & DataSource
typedef enum
{
    kCAROUSEL_PROFILE_HEADER = 1,
    kCAROUSEL_PROFILE_NAME,
    kCAROUSEL_PROFILE_COMPANYNAME,
    
    kCAROUSEL_PROFILE_COMPANYTIERICON,
    kCAROUSEL_PROFILE_COMPANYTIERTEXT,
    
    kCAROUSEL_PROFILE_BUTTON = 999,
} kCAROUSEL_PROFILE_TAG;

typedef enum
{
    kCAROUSEL_FASHAREBADGE_NAME_HEADER = 1,
    kCAROUSEL_FASHAREBADGE_NAME,
    kCAROUSEL_FASHAREBADGE_SHAREBADGEID_HEADER,
    kCAROUSEL_FASHAREBADGE_SHAREBADGEID,
    kCAROUSEL_FASHAREBADGE_IMAGE,
    kCAROUSEL_FASHAREBADGE_FOOTER = 6,
    kCAROUSEL_FASHAREBADGE_IMAGEVIEW,
    kCAROUSEL_FASHAREBADGE_BACKGROUND_IMAGE,
    
    kCAROUSEL_FASHAREBADGE_BADGEVIEW = 1001,
    kCAROUSEL_FASHAREBADGE_TOPIMAGE,
} kCAROUSEL_FASHAREBADGE_TAG;

typedef enum
{
    kCAROUSEL_POINTS_AVAILABLE_HEADER = 1,
    kCAROUSEL_POINTS_AVAILABLE_POINTS,
    
    kCAROUSEL_POINTS_EXPIRING_HEADER    = 3,
    kCAROUSEL_POINTS_FIRSTDATE          = 10, //storyboard: jun 18
    kCAROUSEL_POINTS_SECONDDATE, //storyboard: jul 18
    kCAROUSEL_POINTS_THIRDDATE,  //storyboard: aug 18
    
    kCAROUSEL_POINTS_FIRSTPOINTS        = 20,
    kCAROUSEL_POINTS_SECONDPOINTS,
    kCAROUSEL_POINTS_THIRDPOINTS,
    
    kCAROUSEL_POINTS_BUTTON = 999,
} kCAROUSEL_POINTS_TAG;

typedef enum
{
    kCAROUSEL_PERFORMANCE_HEADER = 1,
    kCAROUSEL_PERFORMANCE_FIRSTKEY      = 101,
    kCAROUSEL_PERFORMANCE_FIRSTDATA,
    kCAROUSEL_PERFORMANCE_FIRSTSEPARATOR,
    kCAROUSEL_PERFORMANCE_SECONDKEY     = 104,
    kCAROUSEL_PERFORMANCE_SECONDDATA,
    kCAROUSEL_PERFORMANCE_SECONDSEPARATOR,
    kCAROUSEL_PERFORMANCE_THIRDKEY      = 107,
    kCAROUSEL_PERFORMANCE_THIRDDATA,
    
    kCAROUSEL_PERFORMANCE_BUTTON = 999,
} kCAROUSEL_PERFORMANCE_TAG;

typedef enum {
    kCAROUSEL_MYPERFORMANCE_HEADER = 1,
    kCAROUSEL_MYPERFORMANCE_SUBHEADER,
    
    kCAROUSEL_MYPERFORMANCE_FIRSTKEY      = 101,
    kCAROUSEL_MYPERFORMANCE_FIRSTDATA,
    kCAROUSEL_MYPERFORMANCE_FIRSTSEPARATOR,
    kCAROUSEL_MYPERFORMANCE_SECONDKEY     = 104,
    kCAROUSEL_MYPERFORMANCE_SECONDDATA,
    kCAROUSEL_MYPERFORMANCE_SECONDSEPARATOR,
    kCAROUSEL_MYPERFORMANCE_THIRDKEY      = 107,
    kCAROUSEL_MYPERFORMANCE_THIRDDATA,
    
    kCAROUSEL_MYPERFORMANCE_BUTTON = 999,
    
}kCAROUSEL_MYPERFORMANCE_TAG;


typedef enum {
    kCAROUSEL_OUTLETPERFORMANCE_HEADER = 1,
    kCAROUSEL_OUTLETPERFORMANCE_SUBHEADER,
    
    kCAROUSEL_OUTLETPERFORMANCE_FIRSTKEY      = 101,
    kCAROUSEL_OUTLETPERFORMANCE_FIRSTDATA,
    kCAROUSEL_OUTLETPERFORMANCE_FIRSTSEPARATOR,
    kCAROUSEL_OUTLETPERFORMANCE_SECONDKEY     = 104,
    kCAROUSEL_OUTLETPERFORMANCE_SECONDDATA,
    kCAROUSEL_OUTLETPERFORMANCE_SECONDSEPARATOR,
    kCAROUSEL_OUTLETPERFORMANCE_THIRDKEY      = 107,
    kCAROUSEL_OUTLETPERFORMANCE_THIRDDATA,
    
    kCAROUSEL_OUTLETPERFORMANCE_BUTTON = 900,
    
}kCAROUSEL_OUTLETPERFORMANCE_TAG;

typedef enum {
    
    kCAROUSEL_LOYALTYPROGRAM_HEADER = 1,
    kCAROUSEL_LOYALTYPROGRAM_PERIOD,
    kCAROUSEL_LOYALTYPROGRAM_DISCLAIMERTEXT,
    kCAROUSEL_LOYALTYPROGRAM_GAUGEVIEW,
    
    kCAROUSEL_LOYALTYPROGRAM_FOOTERTEXT = 699,
    
    kCAROUSEL_LOYALTYPROGRAM_BUTTON = 990,
    
}kCAROUSEL_LOYALTYPROGRAM_TAG;

typedef enum {
    
    kCAROUSEL_LOYALTYBREAKDOWN_HEADER = 1,
    kCAROUSEL_LOYALTYBREAKDOWN_PERIOD,
    kCAROUSEL_LOYALTYBREAKDOWN_DISCLAIMERTEXT,
    kCAROUSEL_LOYALTYBREAKDOWN_BARVIEW,
    
    
    kCAROUSEL_LOYALTYBREAKDOWN_BUTTON = 990,
    
}kCAROUSEL_LOYALTYBREAKDOWN_TAG;

typedef enum {
    kCAROUSEL_CNYPROMOTION_IMAGE = 699,
    
    kCAROUSEL_CNYPROMOTION_HEADER = 1,
    kCAROUSEL_CNYPROMOTION_PERIOD,
    kCAROUSEL_CNYPROMOTION_FOOTERTEXT,
    kCAROUSEL_CNYPROMOTION_DAYSLEFTVIEW,
    kCAROUSEL_CNYPROMOTION_DAYSLEFTLABEL,
    
    kCAROUSEL_CNYPROMOTION_BUTTON = 990,
    
    kCAROUSEL_CNYPROMOTION_REDEEMBUTTON = 1399,
    kCAROUSEL_CNYPROMOTION_REDEEMMESSAGELABEL,
    
    kCAROUSEL_CNYPROMOTION_REDEEMVIEW = 1499,
    kCAROUSEL_CNYPROMOTION_FINALMESSAGE,
}kCAROUSEL_CNYPROMOTION_TAG;

typedef enum {
    
    kCAROUSEL_CVSPEEDOMETER_HEADER = 1,
    kCAROUSEL_CVSPEEDOMETER_SUBHEADER,
    kCAROUSEL_CVSPEEDOMETER_PROGRESSBAR  = 300,
    kCAROUSEL_CVSPEEDOMETER_PERIOD,
    kCAROUSEL_CVSPEEDOMETER_DATAUPDATEDLABEL,
    
    kCAROUSEL_CVSPEEDOMETER_GAUGEVIEW = 4,
    
    kCAROUSEL_CVSPEEDOMETER_FOOTERTEXT = 699,
    
    kCAROUSEL_CVSPEEDOMETER_BUTTON = 990,
    
}kCAROUSEL_CVSPEEDOMETER_TAG;

typedef enum {
    
    kCAROUSEL_AGRIPROMO_HEADER = 1,
    kCAROUSEL_AGRIPROMO_PERIOD,
    kCAROUSEL_AGRIPROMO_DATAUPDATEDLABEL,
    kCAROUSEL_AGRIPROMO_GAUGEVIEW = 4,
    
    kCAROUSEL_AGRIPROMO_FOOTERTEXT = 699,
    
    kCAROUSEL_AGRIPROMO_BUTTON = 990,
    
}kCAROUSEL_AGRIPROMO_TAG;

typedef enum {
    
    kCAROUSEL_HXSPEEDOMETER_HEADER = 1,
    kCAROUSEL_HXSPEEDOMETER_PERIOD,
    kCAROUSEL_HXSPEEDOMETER_DATAUPDATEDLABEL,
    
    kCAROUSEL_HXSPEEDOMETER_GAUGEVIEW = 4,
    
    kCAROUSEL_HXSPEEDOMETER_FOOTERTEXT = 699,
    
    kCAROUSEL_HXSPEEDOMETER_BUTTON = 990,
    
}kCAROUSEL_HXSPEEDOMETER_TAG;

typedef enum
{
    kCAROUSEL_PARTNERS_HEADER         = 1,
    kCAROUSEL_PARTNERS_SUBHEADER,
    
    kCAROUSEL_PARTNERS_PROGRESSBAR  = 300,
    
    kCAROUSEL_PARTNERS_BUTTON = 999,
} kCAROUSEL_PARTNERS_TAG;


typedef enum
{
    kCAROUSEL_RU_POINTS_HEADER         = 1,
    kCAROUSEL_RU_POINTS_SUBHEADER,
    kCAROUSEL_RU_POINTS_FOOTER,
    
    kCAROUSEL_RU_POINTS_BAR            = 200,
    
    kCAROUSEL_RU_POINTS_BAR_MIN        = 5,
    kCAROUSEL_RU_POINTS_BAR_MAX,
    
    kCAROUSEL_RU_POINTS_FIRSTPOINTS        = 101,   //0W
    kCAROUSEL_RU_POINTS_SECONDPOINTS,               //ultra
    kCAROUSEL_RU_POINTS_THIRDPOINTS,                //HX8
    
    kCAROUSEL_RU_POINTS_FIRSTLABEL        = 201,   //0W
    kCAROUSEL_RU_POINTS_SECONDLABEL,               //ultra
    kCAROUSEL_RU_POINTS_THIRDLABEL,                //HX8
    
    
    kCAROUSEL_RU_POINTS_BUTTON = 999,
} kCAROUSEL_RU_POINTS_TAG;

typedef enum
{
    kCAROUSEL_PARTNERS_ENDED_HEADER         = 1,
    kCAROUSEL_PARTNERS_ENDED_ICON,
    kCAROUSEL_PARTNERS_ENDED_CONTENT,
    kCAROUSEL_PARTNERS_ENDED_SUBCONTENT,
    //    kCAROUSEL_PARTNERS_BUTTON = 999,
} kCAROUSEL_PARTNERS_ENDED_TAG;

typedef enum {
    
    kCAROUSEL_EARNREDEEM_TITLE = 1,
    kCAROUSEL_EARNREDEEM_PERIOD,
    kCAROUSEL_EARNREDEEM_HEADER,
    kCAROUSEL_EARNREDEEM_STARTDATE,
    kCAROUSEL_EARNREDEEM_ENDDATE,
    kCAROUSEL_EARNREDEEM_FOOTER,
    
    kCAROUSEL_EARNREDEEM_PROGRESSBAR = 999,
    kCAROUSEL_EARNREDEEM_GREYBAR,
    
    kCAROUSEL_EARNREDEEM_BUTTON = 9999,
    
}kCAROUSEL_EARNREDEEM_TAG;

typedef enum {
    
    kCAROUSEL_THINCENTIVE_HEADER = 1,
    kCAROUSEL_THINCENTIVE_PERIOD,
    kCAROUSEL_THINCENTIVE_DATAUPDATEDLABEL,
    
    kCAROUSEL_THINCENTIVE_GAUGEVIEW = 4,
    
    kCAROUSEL_THINCENTIVE_FOOTERTEXT = 5,
    
}kCAROUSEL_THINCENTIVE_TAG;

typedef enum {
    kCAROUSEL_GAMIFICATION_HEADER = 1,
    kCAROUSEL_GAMIFICATION_CHECKIN_PTS,
    kCAROUSEL_GAMIFICATION_CHECKIN_IMAGE,

    kCAROUSEL_GAMIFICATION_BTN_CHECKIN  = 990,
    kCAROUSEL_GAMIFICATION_FOOTER,

    kCAROUSEL_GAMIFICATION_GRANDPRIZE_VIEW = 4000,
    kCAROUSEL_GAMIFICATION_GRANDPRIZE,
    kCAROUSEL_GAMIFICATION_GRANDPRIZE_IMAGE,
    kCAROUSEL_GAMIFICATION_DAYSLEFT,
    kCAROUSEL_GAMIFICATION_GRANDPRIZE_BTN,

}kCAROUSEL_GAMIFICATION_TAG;

typedef enum {

    kCAROUSEL_MY_SCANFORREWARDS_HEADER = 1,
    kCAROUSEL_MY_SCANFORREWARDS_PERIOD,
    kCAROUSEL_MY_SCANFORREWARDS_UPDATEDASOF,
    kCAROUSEL_MY_SCANFORREWARDS_CONTENT,
    
    kCAROUSEL_MY_SCANFORREWARDS_VIEW = 4,
    
    kCAROUSEL_MY_SCANFORREWARDS_VIEW_1 = 1000,
    kCAROUSEL_MY_SCANFORREWARDS_VIEW_2 = 2000,
    kCAROUSEL_MY_SCANFORREWARDS_VIEW_3 = 3000,
    kCAROUSEL_MY_SCANFORREWARDS_VIEW_4 = 4000,
    kCAROUSEL_MY_SCANFORREWARDS_VIEW_5 = 5000,
    kCAROUSEL_MY_SCANFORREWARDS_VIEW_6 = 6000,
    
    kCAROUSEL_MY_SCANFORREWARDS_BUTTON = 999,
    
}kCAROUSEL_MY_SCANFORREWARDS_TAG;

typedef enum {
    
    kCAROUSEL_TIERBADGE_FOOTERMESSAGE = 1,
    kCAROUSEL_TIERBADGE_QRVIEW,
    kCAROUSEL_TIERBADGE_QRVIEW_ICON,
    kCAROUSEL_TIERBADGE_NAME,
    kCAROUSEL_TIERBADGE_SHAREBADGE_TITLE,
    kCAROUSEL_TIERBADGE_SHAREID,
    
    kCAROUSEL_TIERBADGE_IMAGEVIEW = 999,
    kCAROUSEL_TIERBADGE_GREYBAR,
    kCAROUSEL_TIERBADGE_PROGRESSCOMPLETED,
    
}kCAROUSEL_TIERBADGE_TAG;

typedef enum {
  
  kCAROUSEL_HELIXRACE_TITLE = 1,
  kCAROUSEL_HELIXRACE_DATE,
  kCAROUSEL_HELIXRACE_BACKGORUNDIMAGE,
  kCAROUSEL_HELIXRACE_DAYSLEFT,
  kCAROUSEL_HELIXRACE_DAYSLEFTCOUNTER,
  kCAROUSEL_HELIXRACE_DETAILSBUTTON,
  kCAROUSEL_HELIXRACE_FOOTER,
  kCAROUSEL_HELIXRACE_COMPLETEDCONTENT,
  kCAROUSEL_HELIXRACE_SCANNOWBUTTON,
}kCAROUSEL_HELIXRACE_TAG;

typedef enum {

  kCAROUSEL_PILOTPROMOTION_TITLE = 1,
  kCAROUSEL_PILOTPROMOTION_SUBTITLE,
  kCAROUSEL_PILOTPROMOTION_SPEEDOMETER,
  kCAROUSEL_PILOTPROMOTION_DISCLAIMER,
  kCAROUSEL_PILOTPROMOTION_BUTTON,

}kCAROUSEL_PILOTPROMOTION_TAG;

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
     NSLog(@"carouselColView array:%@",self.carouselColView);
    if (collectionView == self.quickLinksColView)
        return [self.quickLinksArray count];
    else if (collectionView == self.carouselColView)
        return [self.carouselEnumArray count];
    else if (collectionView == self.categoryHeaderCollectionView)
//        return self.numberOfCategory;
        return [self.homeBottomArray count];
    return 0;
}

-(void)showDetailsDidTap:(UITapGestureRecognizer *) sender
{
  [[WebServiceManager sharedInstance] viewHelixGamificationDetails:self];
}
-(void)scanNowDidTap:(UITapGestureRecognizer *) sender
{
  [mSession loadScanProductView];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    if (collectionView == self.carouselColView)
    {
        NSLog(@"CAROUS:%@",self.carouselEnumArray);
        kCAROUSEL_TYPES carouselType =  [[self.carouselEnumArray objectAtIndex: indexPath.row] intValue];
        
        switch (carouselType) {

            case kCAROUSEL_PILOTPROMOTION: {

                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PilotPromotionCell" forIndexPath:indexPath];

                UILabel *lblTitle = [cell viewWithTag:kCAROUSEL_PILOTPROMOTION_TITLE];
                lblTitle.text = self.pilotPromotionDictionary[@"PromotionTitle"];
                lblTitle.font = FONT_H2;

                UILabel *lblSubTitle = [cell viewWithTag:kCAROUSEL_PILOTPROMOTION_SUBTITLE];
                lblSubTitle.text = self.pilotPromotionDictionary[@"PromotionDescription"];
                lblSubTitle.font = FONT_B2;

                ArcView *arcView = [cell viewWithTag:kCAROUSEL_PILOTPROMOTION_SPEEDOMETER];
                arcView.datasource = self;
                [arcView redraw];

                UILabel *lblDisclaimer = [cell viewWithTag:kCAROUSEL_PILOTPROMOTION_DISCLAIMER];
                lblDisclaimer.text = self.pilotPromotionDictionary[@"AwardMessage"];
                lblDisclaimer.font = FONT_B3;

                UIButton *btnScanNow = [cell viewWithTag:kCAROUSEL_PILOTPROMOTION_BUTTON];
                [btnScanNow setTitle:LOCALIZATION(C_BTN_SCANNOW) forState:UIControlStateNormal];
                [btnScanNow setTitleColor:COLOUR_DARKGREY forState:UIControlStateNormal];
                btnScanNow.titleLabel.font = FONT_H2;
                btnScanNow.backgroundColor = COLOUR_YELLOW;
                [btnScanNow addTarget:self action:@selector(scanNowDidTap:) forControlEvents:UIControlEventTouchUpInside];

            }
            break;

            case kCAROUSEL_HELIXRACE: {

              self.tradesPromotion = [GET_HOMEDATA objectForKey: @"TradesPromotion"];
              
              cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HelixRaceCell" forIndexPath:indexPath];
              
              UILabel *lblTitle = [cell viewWithTag: kCAROUSEL_HELIXRACE_TITLE];
              lblTitle.text = self.tradesPromotion[@"TPCSHeader"];
              lblTitle.font = FONT_H2;
              
              UILabel *lblDate = [cell viewWithTag: kCAROUSEL_HELIXRACE_DATE];
              NSMutableString *stringDate = [[NSMutableString alloc] initWithString: LOCALIZATION(@"Promotion Period")]; //TODO: Lokalise
              [stringDate appendString: @": "];
              [stringDate appendString: self.tradesPromotion[@"TPStartDate"]];
              [stringDate appendString: @" - "];
              [stringDate appendString: self.tradesPromotion[@"TPEndDate"]];
              lblDate.text = stringDate;
              lblDate.font = FONT_B3;
              
              NSString * imageName = self.tradesPromotion[@"TPImage"];
              UIImageView *imageBackground = [cell viewWithTag: kCAROUSEL_HELIXRACE_BACKGORUNDIMAGE];
              [imageBackground setImage: [UIImage imageNamed: imageName]];
              
              UILabel *lblDaysLeft = [cell viewWithTag: kCAROUSEL_HELIXRACE_DAYSLEFT];
              UILabel *lblDaysLeftCounter = [cell viewWithTag: kCAROUSEL_HELIXRACE_DAYSLEFTCOUNTER];
              UILabel *lblViewDetails = [cell viewWithTag: kCAROUSEL_HELIXRACE_DETAILSBUTTON];
              TTTAttributedLabel *lblFooter = [cell viewWithTag: kCAROUSEL_HELIXRACE_FOOTER];
              TTTAttributedLabel *lblCompleteContent = [cell viewWithTag: kCAROUSEL_HELIXRACE_COMPLETEDCONTENT];
              UILabel *lblScanNow = [cell viewWithTag:kCAROUSEL_HELIXRACE_SCANNOWBUTTON];
              
              [lblViewDetails addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget:self action: @selector(showDetailsDidTap:)] ];
              [lblScanNow addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget:self action: @selector(scanNowDidTap:)] ];
              
              if ([imageName isEqualToString: @"summary.png"]) {
                for (NSLayoutConstraint *constraint in [imageBackground.superview constraints]) {
                  if ([constraint.identifier isEqualToString: @"imageViewToFooter"]) {
                    constraint.priority = UILayoutPriorityDefaultLow;
                  }
                  if ([constraint.identifier isEqualToString: @"imageViewToSuper"]) {
                    constraint.priority = UILayoutPriorityDefaultHigh+10;
                  }
                }
                lblDaysLeft.superview.alpha = 0;
                lblDaysLeftCounter.superview.alpha = 0;
                lblFooter.superview.alpha = 0;
                lblFooter.alpha = 0;
                lblViewDetails.alpha = 0;
                lblCompleteContent.alpha = 1;

                NSString * contentString = self.tradesPromotion[@"TPFinalMessage1"];
                lblCompleteContent.attributedText = [[NSAttributedString alloc] initWithData: [contentString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];

              } else {
                for (NSLayoutConstraint *constraint in [imageBackground constraints]) {
                  if ([constraint.identifier isEqualToString: @"imageViewToFooter"]) {
                    constraint.priority = UILayoutPriorityDefaultHigh+10;
                  }
                  if ([constraint.identifier isEqualToString: @"imageViewToSuper"]) {
                    constraint.priority = UILayoutPriorityDefaultLow;
                  }
                }
                lblDaysLeft.superview.alpha = 1;
                lblDaysLeftCounter.superview.alpha = 1;
                lblFooter.alpha = 1;
                lblViewDetails.alpha = 1;
                if ([self.tradesPromotion[@"HasTPViewDetailsBtn"] boolValue]) {
                  lblFooter.superview.alpha = 1;
                } else {
                  lblFooter.superview.alpha = 0;
                }
                if ([[mSession profileInfo] hasQLHasRegisterProductCode]) {
                  lblScanNow.superview.alpha = 1;
                } else {
                  lblScanNow.superview.alpha = 0;
                }
                lblCompleteContent.alpha = 0;

                lblDaysLeft.text = [LOCALIZATION(@"days_left") stringByReplacingOccurrencesOfString:@" " withString:@"\n"];
                lblDaysLeft.font = FONT_B2;

                lblDaysLeftCounter.text = [NSString stringWithFormat:@"%@",self.tradesPromotion[@"TPDaysLeft"]];
                lblDaysLeftCounter.font = FONT_H1;

                NSString *footerString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:10; color: #404040;}</style>%@",self.tradesPromotion[@"TPCSFooter"]];
                lblFooter.attributedText = [[NSAttributedString alloc] initWithData: [footerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                lblFooter.superview.backgroundColor = COLOUR_PALEGREY;

                lblViewDetails.textColor = COLOUR_WHITE;
                lblViewDetails.font = FONT_B2;
                [lblViewDetails.superview setBackgroundColor: COLOUR_RED];
                lblViewDetails.text = LOCALIZATION(C_FORM_VIEWDETAIL);

                lblScanNow.textColor = COLOUR_WHITE;
                lblScanNow.font = FONT_B2;
                [lblScanNow.superview setBackgroundColor: COLOUR_RED];
                lblScanNow.text = LOCALIZATION(C_BTN_SCANNOW);
              }

              [cell setNeedsLayout];
              [cell layoutIfNeeded];

            }
            break;

            case kCAROUSEL_TIERBADGE: {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TieringBadgeCell" forIndexPath:indexPath];
                
                if(self.tierDetails != nil && ![self.tierDetails isEqualToDictionary:@{}]) {
                    UIImageView *badgeImageView = [cell viewWithTag:kCAROUSEL_TIERBADGE_IMAGEVIEW];
                    
                    UILabel *lblName = [cell viewWithTag:kCAROUSEL_TIERBADGE_NAME];
                    lblName.text = [[mSession profileInfo] fullName];
                    lblName.font = FONT_H1;
                    lblName.textColor = COLOUR_VERYDARKGREY;
                    
                    UILabel *lblBadgeHeader = [cell viewWithTag:kCAROUSEL_TIERBADGE_SHAREBADGE_TITLE];
                    lblBadgeHeader.text = LOCALIZATION(C_SCANBOTTLE_RESULT_SHARECODE);
                    lblBadgeHeader.font = FONT_B(12);
                    lblBadgeHeader.textColor = COLOUR_WHITE;
                    
                    UILabel *lblBadge = [cell viewWithTag:kCAROUSEL_TIERBADGE_SHAREID];
                    lblBadge.text = [[mSession profileInfo] shareTagCode];
                    lblBadge.font = FONT_H1;
                    lblBadge.textColor = COLOUR_WHITE;
                    
                    NSString *urlString = [[[mSession profileInfo] shareQRCode] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
                    UIImageView *faShareQRImage = [cell viewWithTag:kCAROUSEL_TIERBADGE_QRVIEW_ICON];
                    
                    [faShareQRImage sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"icon-code"] options: (SDWebImageRefreshCached | SDWebImageRetryFailed)];
                    
                    UIImage *badgeImage = [UIImage imageNamed:@"appcard_base.png"];
                    UIColor *color = [Helper colorFromHex:@"#DD1D21"];
                    
                    if (IS_COUNTRY(COUNTRYCODE_INDIA)){
                        
                        NSString *startingDate = @"10-08-2020 00:00:00 AM";
                        NSString *endingDate = @"16-08-2020 00:00:00 AM";

                        NSDateFormatter *startForm=[[NSDateFormatter alloc]init];
                        [startForm setDateFormat:@"dd-MM-yyyy HH:mm:ss a"];
                       
                        NSDate *startDate = [startForm dateFromString:startingDate];
                        NSDate *endDate = [startForm dateFromString:endingDate];
                       
                        
                        NSDate *today = [NSDate date];
                        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                        [dateFormat setDateFormat:@"dd/MM/yyyy HH:mm:ss a"];
                        NSString *dateString = [dateFormat stringFromDate:today];
                        
                         NSDate *currentDate = [startForm dateFromString:dateString];
                        
                        
                        if ((([currentDate compare:startDate]==NSOrderedDescending) && ([currentDate compare:endDate]==NSOrderedAscending)) || (([currentDate compare:startDate]==NSOrderedAscending) && ([currentDate compare:endDate]==NSOrderedDescending))) {
                            NSLog(@"date is in range");
                            
                            switch([[self.tierDetails objectForKey:@"CurrentTierCode"] intValue]) {
                                    
                                 

                                case kTIERING_STANDARD :
                                    badgeImage = [UIImage imageNamed:@"independenceday_normal.png"];
                                    color = [Helper colorFromHex:@"#7A4941"];
                                    lblName.textColor = COLOUR_VERYDARKGREY;
                                    break;
                                case kTIERING_BRONZE :
                                    badgeImage = [UIImage imageNamed:@"appcard_bronze.png"];
                                    color = [Helper colorFromHex:@"#7A4941"];
                                    lblName.textColor = COLOUR_WHITE;
                                    break;
                                case kTIERING_SILVER:
                                    badgeImage = [UIImage imageNamed:@"independenceday_silver.png"];
                                    color = [Helper colorFromHex:@"#A0A8A9"];
                                    lblBadgeHeader.textColor = COLOUR_VERYDARKGREY;
                                    lblBadge.textColor = COLOUR_VERYDARKGREY;
                                    break;
                                case kTIERING_GOLD:
                                    badgeImage = [UIImage imageNamed:@"independenceday_gold.png"];
                                    color = [Helper colorFromHex:@"#A29466"];
                                    lblBadgeHeader.textColor = COLOUR_VERYDARKGREY;
                                    lblBadge.textColor = COLOUR_VERYDARKGREY;
                                    break;
                                case kTIERING_PLATINUM:
                                    badgeImage = [UIImage imageNamed:@"appcard_platinum.png"];
                                    color = [Helper colorFromHex:@"#404040"];
                                    lblName.textColor = COLOUR_WHITE;
                                    break;
                                default:
                                    break;
                                               }
                
                        }
                        else{
                            NSLog(@"date is not in range");
                            switch([[self.tierDetails objectForKey:@"CurrentTierCode"] intValue]) {
                                case kTIERING_BRONZE :
                                    badgeImage = [UIImage imageNamed:@"appcard_bronze.png"];
                                    color = [Helper colorFromHex:@"#7A4941"];
                                    lblName.textColor = COLOUR_WHITE;
                                    break;
                                case kTIERING_SILVER:
                                    badgeImage = [UIImage imageNamed:@"appcard_silver.png"];
                                    color = [Helper colorFromHex:@"#A0A8A9"];
                                    lblBadgeHeader.textColor = COLOUR_VERYDARKGREY;
                                    lblBadge.textColor = COLOUR_VERYDARKGREY;
                                    break;
                                case kTIERING_GOLD:
                                    badgeImage = [UIImage imageNamed:@"appcard_gold.png"];
                                    color = [Helper colorFromHex:@"#A29466"];
                                    lblBadgeHeader.textColor = COLOUR_VERYDARKGREY;
                                    lblBadge.textColor = COLOUR_VERYDARKGREY;
                                    break;
                                case kTIERING_PLATINUM:
                                    badgeImage = [UIImage imageNamed:@"appcard_platinum.png"];
                                    color = [Helper colorFromHex:@"#404040"];
                                    lblName.textColor = COLOUR_WHITE;
                                    break;
                                default:
                                    break;
                            }
                        }
                        
                       
                    }
                    
                    else {
                    
                    switch([[self.tierDetails objectForKey:@"CurrentTierCode"] intValue]) {
                        case kTIERING_BRONZE :
                            badgeImage = [UIImage imageNamed:@"appcard_bronze.png"];
                            color = [Helper colorFromHex:@"#7A4941"];
                            lblName.textColor = COLOUR_WHITE;
                            break;
                        case kTIERING_SILVER:
                            badgeImage = [UIImage imageNamed:@"appcard_silver.png"];
                            color = [Helper colorFromHex:@"#A0A8A9"];
                            lblBadgeHeader.textColor = COLOUR_VERYDARKGREY;
                            lblBadge.textColor = COLOUR_VERYDARKGREY;
                            break;
                        case kTIERING_GOLD:
                            badgeImage = [UIImage imageNamed:@"appcard_gold.png"];
                            color = [Helper colorFromHex:@"#A29466"];
                            lblBadgeHeader.textColor = COLOUR_VERYDARKGREY;
                            lblBadge.textColor = COLOUR_VERYDARKGREY;
                            break;
                        case kTIERING_PLATINUM:
                            badgeImage = [UIImage imageNamed:@"appcard_platinum.png"];
                            color = [Helper colorFromHex:@"#404040"];
                            lblName.textColor = COLOUR_WHITE;
                            break;
                        default:
                            break;
                    }
                    }
    
                    [badgeImageView setImage:badgeImage];
                    
                    TTTAttributedLabel *lblFooter = [cell viewWithTag:kCAROUSEL_TIERBADGE_FOOTERMESSAGE];
                    NSString *footerMessage = [NSString stringWithFormat:@"%@ %@",[self.tierDetails objectForKey:@"DisplayMessage"],LOCALIZATION(C_HOME_LEARNMORE)];
                    [Helper setHyperlinkLabel:lblFooter hyperlinkText:LOCALIZATION(C_HOME_LEARNMORE) hyperlinkFont:FONT_H3 bodyText:footerMessage bodyFont:FONT_B3 urlString:@"badgeLearnMore"];
                    lblFooter.textColor = COLOUR_VERYDARKGREY;
                    lblFooter.textAlignment = NSTextAlignmentCenter;
                    lblFooter.delegate = self;
                    
                    UIView *progressBar = [cell viewWithTag:kCAROUSEL_TIERBADGE_PROGRESSCOMPLETED];
                    [progressBar setBackgroundColor:color];
                    
                    int achievedTarget = [[self.tierDetails objectForKey:@"AchievedPercentage"] intValue];
                    [NSLayoutConstraint constraintWithItem:progressBar attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:((SCREEN_WIDTH - 60) * achievedTarget / 100)].active = YES;
                    
                }
                
            }
                break;
            case kCAROUSEL_MY_SCANFORREWARDS: {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MYScanNRedeemCell" forIndexPath:indexPath];
                
                UILabel *lblTitle = [cell viewWithTag:kCAROUSEL_MY_SCANFORREWARDS_HEADER];
                lblTitle.text = LOCALIZATION(C_HOME_CAROUSEL_SCANFORREWARDS_HEADER);
                lblTitle.font = FONT_H1;
                lblTitle.textColor = COLOUR_VERYDARKGREY;
                               
                UILabel *lblPeriod = [cell viewWithTag:kCAROUSEL_MY_SCANFORREWARDS_PERIOD];
                lblPeriod.font = FONT_B2;
                lblPeriod.textColor = COLOUR_VERYDARKGREY;
                lblPeriod.hidden = YES;
                               
//                UILabel *lblDataUpdated = [cell viewWithTag:kCAROUSEL_MY_SCANFORREWARDS_UPDATEDASOF];
//                lblDataUpdated.font = FONT_B(9);
//                lblDataUpdated.textColor = COLOUR_VERYDARKGREY;
                
                UILabel *lblContent = [cell viewWithTag:kCAROUSEL_MY_SCANFORREWARDS_CONTENT];
                lblContent.text = LOCALIZATION(C_HOME_CAROUSEL_SCANFORREWARDS_CONTENT);
                if(SCREEN_HEIGHT < 800) {
                    lblContent.font = FONT_B(11);
                } else {
                    lblContent.font = FONT_B2;
                }
                lblContent.textColor = COLOUR_VERYDARKGREY;
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_MY_SCANFORREWARDS_BUTTON];
                [btnView setUserInteractionEnabled:NO];
                if(GET_PROFILETYPE == kPROFILETYPE_MECHANIC) {
                    [btnView setHidden:YES];
                    [NSLayoutConstraint constraintWithItem:btnView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
                    
                } else {
                    [btnView setHidden:NO];
                    [btnView setTitle: LOCALIZATION(C_HOME_CAROUSEL_SCANFORREWARDS_VIEWMORE_BTN) forState:UIControlStateNormal];
                    [btnView addTarget:self action:@selector(viewMorePressed:) forControlEvents:UIControlEventTouchDown];
                    btnView.titleLabel.font = FONT_BUTTON;
                    [btnView setUserInteractionEnabled:NO];
                }
                
                if(self.earnNRedeemData != nil && ![self.earnNRedeemData isEqualToDictionary:@{}]) {
                    [btnView setUserInteractionEnabled:YES];
//                    if(![[_earnNRedeemData objectForKey:@"LastRedemptionDate"] isKindOfClass:[NSNull class]]) {
//                        lblPeriod.text = [NSString stringWithFormat:@"%@:%@ - %@",LOCALIZATION(C_LOYALTY_PERIOD),[_earnNRedeemData objectForKey:@"LastRedemptionDate"],[_earnNRedeemData objectForKey:@"NextRedemptionDate"]];
//                    } else {
//                         lblPeriod.text = [NSString stringWithFormat:@"%@: - ",LOCALIZATION(C_LOYALTY_PERIOD)];
//                    }
                    
                    
//                    lblDataUpdated.text = [NSString stringWithFormat:@"*%@ %@",LOCALIZATION(C_HOMECAROUSEL_UPDATEDON),[self.earnNRedeemData objectForKey:@"LastUpdateDate"]];
                    
                    NSMutableArray *redeemDetails = [[NSMutableArray alloc] init];
                    NSArray *autoRedeemArray = [self.earnNRedeemData objectForKey:@"AutoRedemptionDetails"];
                    NSMutableDictionary *bottleDetailsDict = [[NSMutableDictionary alloc] init];
                    if(![autoRedeemArray isKindOfClass:[NSNull class]] && [autoRedeemArray count] > 0) {
                        for(NSDictionary *dict in autoRedeemArray) {
                            [redeemDetails addObject:[[EarnNRedeemModel alloc] initWithData:dict]];
                        }
                        for(EarnNRedeemModel *dict in redeemDetails) {
                            NSDictionary *objDetails = @{
                                @"Bottles" : [dict bottles],
                                @"Points"  : [NSString stringWithFormat:@"%@/%@",[dict pointsToReachMilestone],[dict pointsPerBottle]],
                            };
                            if([[dict productGroupID] isEqualToString:@"58"]) {
                                [bottleDetailsDict setObject:objDetails forKey:@(1000)];
                            } else if([[dict productGroupID] isEqualToString:@"16"]) {
                                [bottleDetailsDict setObject:objDetails forKey:@(2000)];
                            } else if([[dict productGroupID] isEqualToString:@"15"]) {
                                [bottleDetailsDict setObject:objDetails forKey:@(3000)];
                            } else if([[dict productGroupID] isEqualToString:@"57"]) {
                                [bottleDetailsDict setObject:objDetails forKey:@(4000)];
                            } else if([[dict productGroupID] isEqualToString:@"12"]) {
                                [bottleDetailsDict setObject:objDetails forKey:@(5000)];
                            } else if([[dict productGroupID] isEqualToString:@"13"]) {
                                [bottleDetailsDict setObject:objDetails forKey:@(6000)];
                            }
                        }
                    }
                    
                    int viewTag = 1000;
                    
                    for(int i = 1;i < 7;i++) {
                        UILabel *lblBottles = [cell viewWithTag:(viewTag + 1)];
                        lblBottles.text = [[bottleDetailsDict objectForKey:@(viewTag)] objectForKey:@"Bottles"];
                        lblBottles.layer.masksToBounds = YES;
                        lblBottles.layer.cornerRadius = 19;
                        lblBottles.font = FONT_B2;
                        
                        UILabel *lblMilestone = [cell viewWithTag:(viewTag + 3)];
                        lblMilestone.text = [[bottleDetailsDict objectForKey:@(viewTag)] objectForKey:@"Points"];
                        lblMilestone.textColor = COLOUR_RED;
                        lblMilestone.font = FONT_H(11);
                        
                        viewTag += 1000;
                    }
                }
                
            }
                break;
            case kCAROUSEL_GAMIFICATION:{
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GamificationCollectionViewCell" forIndexPath:indexPath];
                
                NSDictionary *cellData = [GET_HOMEDATA objectForKey: @"TradesPromotion"];
                
                UILabel *lblHeader = [cell viewWithTag:kCAROUSEL_GAMIFICATION_HEADER];
                lblHeader.text = [cellData objectForKey:@"TPCSHeader"];
                lblHeader.font = FONT_H1;
                lblHeader.textColor = COLOUR_VERYDARKGREY;
                
                UILabel *lblChekinPts = [cell viewWithTag:kCAROUSEL_GAMIFICATION_CHECKIN_PTS];
                lblChekinPts.text = [NSString stringWithFormat:@"%@ %@",LOCALIZATION(C_HOME_CAROUSEL_CHECKIN_PTS),[cellData objectForKey:@"VoucherValue"]];
                lblChekinPts.font = FONT_B2;
                lblChekinPts.textColor = COLOUR_VERYDARKGREY;
                
                NSInteger exactMileStone = [[mSession profileInfo] gamification_Milestone];
                NSInteger mileStone = exactMileStone > 7 ? (exactMileStone % 7 != 0 ? exactMileStone % 7 : 7) : exactMileStone;
                UIImageView *checkInImage = [cell viewWithTag:kCAROUSEL_GAMIFICATION_CHECKIN_IMAGE];
                checkInImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"checkin-%ld.png",(long)mileStone]];
                
                UIButton *checkinBtn = [cell viewWithTag:kCAROUSEL_GAMIFICATION_BTN_CHECKIN];
                if([[mSession profileInfo] gamification_HasCheckedInToday]) {
                    [checkinBtn setTitle:LOCALIZATION(C_HOME_CAROUSEL_CHECKIN_TMRW_BTN) forState:UIControlStateNormal];
                    [checkinBtn setBackgroundColor:COLOUR_BACKGROUNDGREY];
                    [checkinBtn setTitleColor:COLOUR_LIGHTGREY forState:UIControlStateNormal];
                    checkinBtn.layer.borderColor = [COLOUR_LIGHTGREY CGColor];
                    checkinBtn.layer.borderWidth = 1.0;
                    [checkinBtn setUserInteractionEnabled:NO];
                } else {
                    [checkinBtn setTitle:LOCALIZATION(C_HOME_CAROUSEL_CHECKIN_TODAY_BTN) forState:UIControlStateNormal];
                    [checkinBtn setBackgroundColor:COLOUR_YELLOW];
                    [checkinBtn setTitleColor:COLOUR_VERYDARKGREY forState:UIControlStateNormal];
                    [checkinBtn setUserInteractionEnabled:YES];
                }
                
                TTTAttributedLabel *lblFooter = [cell viewWithTag:kCAROUSEL_GAMIFICATION_FOOTER];
                [Helper setHyperlinkLabel:lblFooter hyperlinkText:LOCALIZATION(C_FORM_VIEWDETAIL) hyperlinkFont:FONT_H(12) bodyText:[NSString stringWithFormat:@"%@\n%@",[cellData objectForKey:@"TPCSFooter"],LOCALIZATION(C_FORM_VIEWDETAIL)] bodyFont:FONT_B(12) urlString:@"GamificationDetails"];
                lblFooter.textColor = COLOUR_VERYDARKGREY;
                lblFooter.textAlignment = NSTextAlignmentCenter;
                lblFooter.delegate = self;
                
                UILabel *lblHeader_GrandPrize = [cell viewWithTag:kCAROUSEL_GAMIFICATION_GRANDPRIZE];
                lblHeader_GrandPrize.text = LOCALIZATION(C_HOME_CAROUSEL_CHECKIN_GRANDPRIZE);
                lblHeader_GrandPrize.font = FONT_H(12);
                lblHeader_GrandPrize.textColor = COLOUR_VERYDARKGREY;
                
                UILabel *lblDaysLeft_GrandPrize = [cell viewWithTag:kCAROUSEL_GAMIFICATION_DAYSLEFT];
                lblDaysLeft_GrandPrize.font = FONT_B(11);
                lblDaysLeft_GrandPrize.textColor = COLOUR_VERYDARKGREY;
                
                UIButton *grandPrizeBtn = [cell viewWithTag:kCAROUSEL_GAMIFICATION_GRANDPRIZE_BTN];
                BOOL hasParticipated = [[cellData objectForKey:@"HasParticipated"] boolValue];
                [grandPrizeBtn setUserInteractionEnabled:NO];
                lblDaysLeft_GrandPrize.text = [LOCALIZATION(C_HOME_CAROUSEL_CHECKIN_DAYSLEFT) stringByReplacingOccurrencesOfString:@"XX" withString:[NSString stringWithFormat:@"%ld",21 - exactMileStone]];
                
                if(exactMileStone >= 21 && !hasParticipated) {
                    [grandPrizeBtn setUserInteractionEnabled:YES];
                    lblDaysLeft_GrandPrize.text = LOCALIZATION(C_HOME_CAROUSEL_CHECKIN_OPENNOW);
                } else if(exactMileStone >= 21 && hasParticipated) {
                    lblDaysLeft_GrandPrize.text = LOCALIZATION(C_HOME_CAROUSEL_CHECKIN_REDEEMED);
                }
                else {
                    NSInteger mileStoneLeft = 21 - exactMileStone;
                    if(mileStoneLeft > [[cellData objectForKey:@"TPDaysLeft"] intValue]) {
                        UIView *grandPrizeView = [cell viewWithTag:kCAROUSEL_GAMIFICATION_GRANDPRIZE_VIEW];
                        [grandPrizeView setHidden:YES];
                    }
                }
                
            }
                break;
            case kCAROUSEL_ID_LEBARANPROMOTION:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"IDLebaranPromotionCollectionViewCell" forIndexPath:indexPath];
                               
                NSDictionary *cellData = [GET_HOMEDATA objectForKey: @"TradesPromotion"];
                               
                UIImageView *rewardStatusImage = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_IMAGE];
                if(![[cellData objectForKey:@"TPImage"] isKindOfClass:[NSNull class]]) {
                    [rewardStatusImage setImage:[UIImage imageNamed:[cellData objectForKey:@"TPImage"]]];
                } else {
                     [rewardStatusImage setImage:[UIImage imageNamed:@"ID-lebaran-MCO_0.png"]];
                }
                               
                UILabel *lblNameHeader = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_HEADER];
                lblNameHeader.text = [NSString stringWithFormat:@"%@",[cellData objectForKey:@"TPCSHeader"]];
                lblNameHeader.font = FONT_H1;
                lblNameHeader.textColor = COLOUR_WHITE;
                               
                UILabel *lblPeriod = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_PERIOD];
                lblPeriod.text = [NSString stringWithFormat:@"%@: %@ - %@",LOCALIZATION(C_LOYALTY_PERIOD),[cellData objectForKey:@"TPStartDate"],[cellData objectForKey:@"TPEndDate"]];
                lblPeriod.font = FONT_B2;
                lblPeriod.textColor = COLOUR_WHITE;
                               
                TTTAttributedLabel *lblFooterText = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_FOOTERTEXT];
                if([[cellData objectForKey:@"TPCSFooter"] isEqualToString:@""] || [[cellData objectForKey:@"TPCSFooter"] isKindOfClass:[NSNull class]]) {
                    lblFooterText.hidden = YES;
                } else {
                    lblFooterText.hidden = NO;
                    NSString *footerString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:10; color: #FFFFFF;}</style>%@",[cellData objectForKey:@"TPCSFooter"]];
                    lblFooterText.attributedText = [[NSAttributedString alloc] initWithData: [footerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                }
                               
                UIView *daysLeftView = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_DAYSLEFTVIEW];
                               
                UIButton *btn_carousel = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_BUTTON];
                
                UIButton *btnRedeem = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_REDEEMBUTTON];
                UIView *scratchView = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_REDEEMVIEW];
                TTTAttributedLabel *finalMessage = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_FINALMESSAGE];
                
                if([[cellData objectForKey:@"HasTPViewDetailsBtn"] boolValue]) {
                    daysLeftView.hidden = NO;
                                   
                    UILabel *lblDaysLeft = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_DAYSLEFTLABEL];
                    NSString *daysLeftString = [LOCALIZATION(C_CAROUSEL_PROMOTION_DAYSLEFT) stringByReplacingOccurrencesOfString:@"XXX" withString: [NSString stringWithFormat:@"%@",[cellData objectForKey:@"TPDaysLeft"]]];
                    NSMutableAttributedString *daysLeftAttrString = [[NSMutableAttributedString alloc]initWithString:daysLeftString attributes:@{
                            NSFontAttributeName : FONT_B3,
                            NSForegroundColorAttributeName: COLOUR_YELLOW,
                            
                    }];
                    [daysLeftAttrString setAttributes:@{NSFontAttributeName : FONT_H(18), NSForegroundColorAttributeName : COLOUR_YELLOW} range: [daysLeftString rangeOfString:[NSString stringWithFormat:@"%@",[cellData objectForKey:@"TPDaysLeft"]]]];
                    lblDaysLeft.attributedText = daysLeftAttrString;
                                   
                    daysLeftView.layer.cornerRadius = 34;
                    daysLeftView.layer.masksToBounds = YES;
                    daysLeftView.layer.borderWidth = 1;
                    daysLeftView.layer.borderColor = [COLOUR_YELLOW CGColor];
                                   
                    btn_carousel.hidden = NO;
                    [NSLayoutConstraint constraintWithItem:btn_carousel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:40
                     ].active = YES;
                    [btn_carousel setTitle:LOCALIZATION(C_FORM_VIEWDETAIL) forState:UIControlStateNormal];
                         
                   
                    if([[cellData objectForKey:@"HasParticipated"] boolValue]) {
                        scratchView.hidden = YES;
                        [btnRedeem setUserInteractionEnabled:NO];
                    } else {
                        scratchView.hidden = NO;
                        [btnRedeem setUserInteractionEnabled:YES];
                        
                        [scratchView.layer addAnimation:[self getShakeAnimation] forKey:@"iconShake"];
                        scratchView.transform = CGAffineTransformIdentity;
                    }
                    
                    finalMessage.hidden = YES;
                                   
                } else {
                    daysLeftView.hidden = YES;
                    btn_carousel.hidden = YES;
                    scratchView.hidden = YES;
                    lblFooterText.hidden = YES;
                    [btnRedeem setUserInteractionEnabled:NO];
                    [NSLayoutConstraint constraintWithItem:btn_carousel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
                    
                    finalMessage.hidden = NO;
                    NSString *finalMessageString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:14px; color: #FFFFFF;}</style>%@",[cellData objectForKey:@"TPFinalMessage1"]];
                    finalMessage.attributedText = [[NSAttributedString alloc] initWithData: [finalMessageString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                                   
                }
                               
            }
                break;
            case kCAROUSEL_THINCENTIVE_OVERALL:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"THIncentiveCarousel" forIndexPath:indexPath];
                
                UILabel *lblTitle = [cell viewWithTag:kCAROUSEL_THINCENTIVE_HEADER];
                lblTitle.text = LOCALIZATION(C_HOME_THINCENTIVE_TITLE);
                lblTitle.font = FONT_H1;
                lblTitle.textColor = COLOUR_VERYDARKGREY;
                
                UILabel *lblPeriod = [cell viewWithTag:kCAROUSEL_THINCENTIVE_PERIOD];
                lblPeriod.font = FONT_B3;
                lblPeriod.textColor = COLOUR_VERYDARKGREY;
                
                UILabel *lblDataUpdated = [cell viewWithTag:kCAROUSEL_THINCENTIVE_DATAUPDATEDLABEL];
                lblDataUpdated.font = FONT_B(8);
                lblDataUpdated.textColor = COLOUR_VERYDARKGREY;
                lblDataUpdated.text = LOCALIZATION(C_HOME_THINCENTIVE_DISCLAIMER);
                
                THIncentiveArcView *gaugeView = [cell viewWithTag:kCAROUSEL_THINCENTIVE_GAUGEVIEW];
                [gaugeView.superview setNeedsDisplay];
                [gaugeView setArcCenter:CGPointMake((SCREEN_WIDTH/ 2) - 12, self.carouselColViewHeight.constant * 0.54)];
                [gaugeView setRadius:(self.carouselColViewHeight.constant / 2.2)];
                
                TTTAttributedLabel *lblFooterText = [cell viewWithTag:kCAROUSEL_THINCENTIVE_FOOTERTEXT];
                
                if(self.thTierPointsData != nil) {
                    lblPeriod.text = [NSString stringWithFormat:@"%@: %@ - %@\n%@ %@",LOCALIZATION(C_LOYALTY_PERIOD),[self.thTierPointsData startDate],[self.thTierPointsData endDate],LOCALIZATION(C_HOMECAROUSEL_UPDATEDON),[[mSession profileInfo] loyaltyTHAsOfDate]];
                    [gaugeView loadBar:self.thTierPointsData isBreakdown:false];
                    
                    if([self.thTierPointsData tier] < 5) {
                        NSString *footerString;
                        if(IS_THAI_LANGUAGE) {
                            NSString *csString = [LOCALIZATION(C_HOME_THINCENTIVE_FOOTER) stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"XX %@",LOCALIZATION(C_HOME_THINCENTIVE_BAHT)] withString:[NSString stringWithFormat:@"<b style=\"color:#DD1D21 ; font-size:14px ; font-family: 'Kittithada Bold 75';\"> %@ %@ </b>",[self addCommaThousandSeparatorsWithInputString:[self.thTierPointsData bahtToBeEarned]],LOCALIZATION(C_HOME_THINCENTIVE_BAHT)]];
                            csString = [csString stringByReplacingOccurrencesOfString:@"#Tier#" withString:[NSString stringWithFormat:@"%ld",[self.thTierPointsData tier] + 1]];
                            footerString = [NSString stringWithFormat:@"<style>body{font-family: 'Kittithada Roman 55'; font-size:13px; color: #404040;}</style>%@",csString];
                            
                        } else {
                            NSString *csString = [LOCALIZATION(C_HOME_THINCENTIVE_FOOTER) stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"XX %@",LOCALIZATION(C_HOME_THINCENTIVE_BAHT)] withString:[NSString stringWithFormat:@"<b style=\"color:#DD1D21 ; font-size:14px ; font-family: 'ShellFutura-Bold'; \"> %@ %@ </b>",[self addCommaThousandSeparatorsWithInputString:[self.thTierPointsData bahtToBeEarned]],LOCALIZATION(C_HOME_THINCENTIVE_BAHT)]];
                            csString = [csString stringByReplacingOccurrencesOfString:@"#Tier#" withString:[NSString stringWithFormat:@"%ld",[self.thTierPointsData tier] + 1]];
                            footerString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:13px; color: #404040;}</style>%@",csString];
                        }
                        lblFooterText.attributedText = [[NSAttributedString alloc] initWithData: [footerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                        [lblFooterText setHidden:NO];
                    } else {
                        [lblFooterText setHidden:YES];
                    }
                }
                
            }
                break;
            case kCAROUSEL_THINCENTIVE_BREAKDOWN:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"THIncentiveCarousel" forIndexPath:indexPath];
                            
                UILabel *lblTitle = [cell viewWithTag:kCAROUSEL_THINCENTIVE_HEADER];
                lblTitle.text = LOCALIZATION(C_HOME_THINCENTIVE_ORDERBREAKDOWN_TITLE);
                lblTitle.font = FONT_H1;
                lblTitle.textColor = COLOUR_VERYDARKGREY;
                            
                UILabel *lblPeriod = [cell viewWithTag:kCAROUSEL_THINCENTIVE_PERIOD];
                lblPeriod.font = FONT_B3;
                lblPeriod.textColor = COLOUR_VERYDARKGREY;
                            
                UILabel *lblDataUpdated = [cell viewWithTag:kCAROUSEL_THINCENTIVE_DATAUPDATEDLABEL];
                lblDataUpdated.font = FONT_B(8);
                lblDataUpdated.textColor = COLOUR_VERYDARKGREY;
                lblDataUpdated.text = LOCALIZATION(C_HOME_THINCENTIVE_DISCLAIMER);
                            
                THIncentiveArcView *gaugeView = [cell viewWithTag:kCAROUSEL_THINCENTIVE_GAUGEVIEW];
                [gaugeView.superview setNeedsDisplay];
                [gaugeView setArcCenter:CGPointMake((SCREEN_WIDTH/ 2) - 12, self.carouselColViewHeight.constant * 0.54)];
                [gaugeView setRadius:(self.carouselColViewHeight.constant / 2.2)];
                            
                TTTAttributedLabel *lblFooterText = [cell viewWithTag:kCAROUSEL_THINCENTIVE_FOOTERTEXT];
            
                if(self.thTierPointsData != nil) {
                    lblPeriod.text = [NSString stringWithFormat:@"%@: %@ - %@\n%@ %@",LOCALIZATION(C_LOYALTY_PERIOD),[self.thTierPointsData startDate],[self.thTierPointsData endDate],LOCALIZATION(C_HOMECAROUSEL_UPDATEDON),[[mSession profileInfo] loyaltyTHAsOfDate]];
                    [gaugeView loadBar:self.thTierPointsData isBreakdown:true];
                    
                    if([self.thTierPointsData tier] < 5) {
                        NSString *footerString;
                        if(IS_THAI_LANGUAGE) {
                            NSString *csString = [LOCALIZATION(C_HOME_THINCENTIVE_FOOTER) stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"XX %@",LOCALIZATION(C_HOME_THINCENTIVE_BAHT)] withString:[NSString stringWithFormat:@"<b style=\"color:#DD1D21 ; font-size:14px ; font-family: 'Kittithada Bold 75';\"> %@ %@ </b>",[self addCommaThousandSeparatorsWithInputString:[self.thTierPointsData bahtToBeEarned]],LOCALIZATION(C_HOME_THINCENTIVE_BAHT)]];
                            csString = [csString stringByReplacingOccurrencesOfString:@"#Tier#" withString:[NSString stringWithFormat:@"%ld",[self.thTierPointsData tier] + 1]];
                            footerString = [NSString stringWithFormat:@"<style>body{font-family: 'Kittithada Roman 55'; font-size:13px; color: #404040;}</style>%@",csString];
                        } else {
                            NSString *csString = [LOCALIZATION(C_HOME_THINCENTIVE_FOOTER) stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"XX %@",LOCALIZATION(C_HOME_THINCENTIVE_BAHT)] withString:[NSString stringWithFormat:@"<b style=\"color:#DD1D21 ; font-size:11px ; font-family: 'ShellFutura-Bold' \"> %@ %@ </b>",[self addCommaThousandSeparatorsWithInputString:[self.thTierPointsData bahtToBeEarned]],LOCALIZATION(C_HOME_THINCENTIVE_BAHT)]];
                            csString = [csString stringByReplacingOccurrencesOfString:@"#Tier#" withString:[NSString stringWithFormat:@"%ld",[self.thTierPointsData tier] + 1]];
                            footerString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:10px; color: #404040;}</style>%@",csString];
                        }
                        lblFooterText.attributedText = [[NSAttributedString alloc] initWithData: [footerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                        [lblFooterText setHidden:NO];
                    } else {
                        [lblFooterText setHidden:YES];
                    }
                }
                
            }
                break;
            case kCAROUSEL_TREARNREDEEM:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TREarnRedeemCarouselCell" forIndexPath:indexPath];
                
                UILabel *lblTitle = [cell viewWithTag:kCAROUSEL_EARNREDEEM_TITLE];
                lblTitle.text = LOCALIZATION(C_HOME_EARNREDEEM_TITLE);
                lblTitle.font = FONT_H1;
                lblTitle.textColor = COLOUR_VERYDARKGREY;
                
                UILabel *lblPeriod = [cell viewWithTag:kCAROUSEL_EARNREDEEM_PERIOD];
                lblPeriod.text = [NSString stringWithFormat:@"%@ xx/xx/xxxx",LOCALIZATION(C_HOMECAROUSEL_UPDATEDON)];
                lblPeriod.font = FONT_B(11);
                lblPeriod.textColor = COLOUR_VERYDARKGREY;
                
                UILabel *lblHeader = [cell viewWithTag:kCAROUSEL_EARNREDEEM_HEADER];
                lblHeader.text = LOCALIZATION(C_HOME_EARNREDEEM_CONTENT);
                lblHeader.font = FONT_B(11);
                lblHeader.textColor = COLOUR_VERYDARKGREY;
                
                UILabel *lblStartDate = [cell viewWithTag:kCAROUSEL_EARNREDEEM_STARTDATE];
                lblStartDate.text = @"Jan'20";
                lblStartDate.font = FONT_B(11);
                lblStartDate.textColor = COLOUR_VERYDARKGREY;
                
                UILabel *lblEndDate = [cell viewWithTag:kCAROUSEL_EARNREDEEM_ENDDATE];
                lblEndDate.text = @"Jun'20";
                lblEndDate.font = FONT_B(11);
                lblEndDate.textColor = COLOUR_VERYDARKGREY;
                
                UIView *greyBar = [cell viewWithTag:kCAROUSEL_EARNREDEEM_GREYBAR];
                UIView *progressBar = [cell viewWithTag:kCAROUSEL_EARNREDEEM_PROGRESSBAR];
                                   
                TTTAttributedLabel *lblFooter = [cell viewWithTag:kCAROUSEL_EARNREDEEM_FOOTER];
                lblFooter.font = FONT_B(11);
                
                if(self.earnNRedeemData != nil && ![self.earnNRedeemData isEqualToDictionary:@{}]) {
                    lblPeriod.text = [NSString stringWithFormat:@"%@ %@",LOCALIZATION(C_HOMECAROUSEL_UPDATEDON),[self.earnNRedeemData objectForKey:@"LastUpdateDate"]];
                    lblStartDate.text = [NSString stringWithFormat:@"%@",[self.earnNRedeemData objectForKey:@"LastRedemptionDate"]];
                    lblEndDate.text = [NSString stringWithFormat:@"%@",[self.earnNRedeemData objectForKey:@"NextRedemptionDate"]];
                    NSString *content = [NSString stringWithFormat:@"<b>%@ %@ </b>%@",[self.earnNRedeemData objectForKey:@"AvailableDays"],LOCALIZATION(C_RU_LOCKEDREWARDS_DAYS), LOCALIZATION(C_HOME_EARNREDEEM_FOOTER)];
                    content = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:11; color: #404040;}</style>%@",content];
                    lblFooter.attributedText = [[NSAttributedString alloc] initWithData: [content dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                    
                    if(![[self.earnNRedeemData objectForKey:@"AvailableDays"] isKindOfClass:[NSNull class]]) {
                        //Setting Progress Bar
                        float passedDays = 183.0 - [[self.earnNRedeemData objectForKey:@"AvailableDays"] floatValue];
                        [NSLayoutConstraint constraintWithItem:progressBar attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:((SCREEN_WIDTH - 32) / 183.0 * passedDays)].active = YES;
                    } else {
                        [NSLayoutConstraint constraintWithItem:progressBar attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:((SCREEN_WIDTH - 32) / 183.0 * 10.0)].active = YES;
                    }
                    
                    NSMutableArray *redeemDetails = [[NSMutableArray alloc] init];
                    NSArray *autoRedeemArray = [self.earnNRedeemData objectForKey:@"AutoRedemptionDetails"];
                    NSMutableDictionary *bottleDetailsDict = [[NSMutableDictionary alloc] init];
                    if(![autoRedeemArray isKindOfClass:[NSNull class]] && [autoRedeemArray count] > 0) {
                        for(NSDictionary *dict in autoRedeemArray) {
                            [redeemDetails addObject:[[EarnNRedeemModel alloc] initWithData:dict]];
                        }
                        for(EarnNRedeemModel *dict in redeemDetails) {
                            if([[dict productGroupID] isEqualToString:@"12"]) {
                                [bottleDetailsDict setObject:@{
                                    @"Bottles" : [dict bottles],
                                    @"Points"  : [dict pointsToReachMilestone],
                                } forKey:@"3000"];
                            } else if([[dict productGroupID] isEqualToString:@"15"]) {
                                [bottleDetailsDict setObject:@{
                                    @"Bottles" : [dict bottles],
                                    @"Points"  : [dict pointsToReachMilestone],
                                } forKey:@"2000"];
                            } else if([[dict productGroupID] isEqualToString:@"16"]) {
                                [bottleDetailsDict setObject:@{
                                    @"Bottles" : [dict bottles],
                                    @"Points"  : [dict pointsToReachMilestone],
                                } forKey:@"1000"];
                            }
                        }
                    }
                    
                    int viewTag = 1000;
                        
                    while(viewTag < 4000) {
                    
                        for(int i = 1;i < 5;i++) {
                            switch(i) {
                                case 1:
                                {
                                    UIImageView *imgView = [cell viewWithTag:(viewTag + 1)];
                                    if(viewTag == 1000) {
                                        [imgView setImage:[UIImage imageNamed:@"icn_packshot_ultra_TR.png"]];
                                    } else if(viewTag == 2000) {
                                        [imgView setImage:[UIImage imageNamed:@"icn_packshot_hx8_TR.png"]];
                                    } else {
                                        [imgView setImage:[UIImage imageNamed:@"icn_packshot_hx7_TR.png"]];
                                    }
                                    [imgView setContentMode:UIViewContentModeScaleAspectFit];
                                }
                                    break;
                                case 2:{
                                    UILabel *lblX = [cell viewWithTag:(viewTag + 2)];
                                    lblX.text = @"x";
                                    lblX.textColor = COLOUR_VERYDARKGREY;
                                    lblX.font = FONT_B2;
                                }
                                    break;
                                case 3:
                                {
                                    UILabel *lblNumber = [cell viewWithTag:(viewTag + 3)];
                                    lblNumber.font = FONT_B1;
                                    lblNumber.textColor = COLOUR_WHITE;
                                    lblNumber.layer.masksToBounds = YES;
                                    lblNumber.layer.cornerRadius = 20;
                                    if(viewTag == 1000) {
                                        lblNumber.backgroundColor = COLOUR_VERYDARKGREY;
                                    } else if(viewTag == 2000) {
                                        lblNumber.backgroundColor = COLOUR_MIDGREY;
                                    } else {
                                        lblNumber.backgroundColor = COLOUR_DARKBLUE;
                                    }
                                    lblNumber.text = [[bottleDetailsDict objectForKey:[NSString stringWithFormat:@"%d",viewTag]] objectForKey:@"Bottles"];
                                }
                                    break;
                                case 4:
                                {
                                    TTTAttributedLabel *lblContent = [cell viewWithTag:(viewTag + 4)];
                                    NSString *content = [NSString stringWithFormat:@"<b>%@ %@ </b>%@",[[bottleDetailsDict objectForKey:[NSString stringWithFormat:@"%d",viewTag]] objectForKey:@"Points"],LOCALIZATION(C_REWARDS_PTS), LOCALIZATION(C_HOME_EARNREDEEM_FREEBOTTLE)];
                                    content = [content stringByReplacingOccurrencesOfString:@"<b>" withString:@"<b style=\"color:#DD1D21 ; font-size:11px\">"];
                                            NSString *footerString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:11; color: #404040;}</style>%@",content];
                                    lblContent.attributedText = [[NSAttributedString alloc] initWithData: [footerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                                }
                                    break;
                                default:
                                    break;
                            }
                        }
                            
                        viewTag += 1000;
                            
                    }
                    
                }
                
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_EARNREDEEM_BUTTON];
                [btnView setTitle: LOCALIZATION(C_SEARCHVEHICLE_REGISTERPRODUCTCODE) forState:UIControlStateNormal];
                [btnView addTarget:self action:@selector(scanNowPressed:) forControlEvents:UIControlEventTouchDown];
                btnView.titleLabel.font = FONT_BUTTON;
                
            }
                break;
            case kCAROUSEL_HXPROMOTION:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HXPromtionCarouselCell" forIndexPath:indexPath];
                
                NSDictionary *cellData = [GET_HOMEDATA objectForKey:@"HX8Promotion"];
                
                if(![cellData isKindOfClass:[NSNull class]] && cellData != nil) {
                    UILabel *lblNameHeader = [cell viewWithTag:kCAROUSEL_HXSPEEDOMETER_HEADER];
                    lblNameHeader.text =[NSString stringWithFormat:@"%@",[cellData objectForKey:@"HX8PromoHeading"]];
                    if(SCREEN_WIDTH <= 320) {
                         lblNameHeader.font = FONT_H2;
                    } else {
                         lblNameHeader.font = FONT_H1;
                    }
                    lblNameHeader.textColor = COLOUR_VERYDARKGREY;
                    
                    UILabel *lblPeriod = [cell viewWithTag:kCAROUSEL_HXSPEEDOMETER_PERIOD];
                    lblPeriod.text = [NSString stringWithFormat:@"%@: %@ - %@",LOCALIZATION(C_LOYALTY_PERIOD),[cellData objectForKey:@"HX8PromoStartDate"],[cellData objectForKey:@"HX8PromoEndDate"]];
                    lblPeriod.font = FONT_B2;
                    if(SCREEN_WIDTH <= 320) {
                        lblPeriod.font = FONT_B3;
                    } else {
                        lblPeriod.font = FONT_B2;
                    }
                    lblPeriod.textColor = COLOUR_VERYDARKGREY;
                    
                    HXPromotionView *gaugeView = [cell viewWithTag:kCAROUSEL_HXSPEEDOMETER_GAUGEVIEW];
                    [gaugeView.superview setNeedsDisplay];
                    [gaugeView loadBar: cellData isCarousel:YES];
                    
                    TTTAttributedLabel *lblFooterText = [cell viewWithTag:kCAROUSEL_HXSPEEDOMETER_FOOTERTEXT];
                    NSString *csString = [NSString stringWithFormat:@"%@",[cellData objectForKey:@"HX8PromoFooterText"]];
                    NSString *footerString;
                    if(SCREEN_WIDTH <= 320) {
                        csString = [csString stringByReplacingOccurrencesOfString:@"<b>" withString:@"<b style=\"color:#DD1D21 ; font-size:14px\">"];
                        footerString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:10; color: #404040;}</style>%@",csString];
                    } else {
                        csString = [csString stringByReplacingOccurrencesOfString:@"<b>" withString:@"<b style=\"color:#DD1D21 ; font-size:15px\">"];
                        footerString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:12; color: #404040;}</style>%@",csString];
                    }
                    lblFooterText.attributedText = [[NSAttributedString alloc] initWithData: [footerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                    
                    lblFooterText.font = FONT_B3;
                    lblFooterText.textColor = COLOUR_VERYDARKGREY;
                    lblFooterText.textAlignment = NSTextAlignmentCenter;
                    [lblFooterText sizeToFit];
                    
                    UIButton *btnView = [cell viewWithTag: kCAROUSEL_HXSPEEDOMETER_BUTTON];
                    if(![[cellData objectForKey:@"HasBtnScanNow"] boolValue]) {
                        [btnView setTitle: LOCALIZATION(C_BTN_VIEWRECENTACTIVITY) forState:UIControlStateNormal];
                        [btnView addTarget:self action:@selector(viewRecentActivityPressed:) forControlEvents:UIControlEventTouchDown];
                    } else {
                        [btnView setTitle: LOCALIZATION(C_BTN_SCANNOW) forState:UIControlStateNormal];
                        [btnView addTarget:self action:@selector(scanNowPressed:) forControlEvents:UIControlEventTouchDown];
                    }
                    btnView.titleLabel.font = FONT_BUTTON;
                    
                    [NSLayoutConstraint constraintWithItem:btnView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:45].active = YES;
                }
                
            }
                break;
            case kCAROUSEL_AGRIPROMOTION:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AgriPromoCarousel" forIndexPath:indexPath];
                
                NSDictionary *cellData = [GET_HOMEDATA objectForKey:@"AgriPromotion"];
                
                if(![cellData isKindOfClass:[NSNull class]] && cellData != nil) {
                    UILabel *lblNameHeader = [cell viewWithTag:kCAROUSEL_AGRIPROMO_HEADER];
                    lblNameHeader.text =[NSString stringWithFormat:@"%@",[cellData objectForKey:@"AgriHeading"]];
                    if(SCREEN_WIDTH <= 320) {
                         lblNameHeader.font = FONT_H2;
                    } else {
                         lblNameHeader.font = FONT_H1;
                    }
                    lblNameHeader.textColor = COLOUR_VERYDARKGREY;
                    
                    UILabel *lblPeriod = [cell viewWithTag:kCAROUSEL_AGRIPROMO_PERIOD];
                    lblPeriod.text = [NSString stringWithFormat:@"%@: %@ - %@",LOCALIZATION(C_LOYALTY_PERIOD),[cellData objectForKey:@"AgriStartDate"],[cellData objectForKey:@"AgriEndDate"]];
                    if(SCREEN_WIDTH <= 320) {
                        lblPeriod.font = FONT_B3;
                    } else {
                        lblPeriod.font = FONT_B2;
                    }
                    lblPeriod.textColor = COLOUR_VERYDARKGREY;
                    
                    UILabel *lblDataUpdated = [cell viewWithTag:kCAROUSEL_AGRIPROMO_DATAUPDATEDLABEL];
                    if(SCREEN_WIDTH <= 320) {
                         lblDataUpdated.font = FONT_B3;
                    } else {
                        lblDataUpdated.font = FONT_B(12);
                    }
                    lblDataUpdated.textColor = COLOUR_VERYDARKGREY;
                    lblDataUpdated.text = [NSString stringWithFormat:@"%@ %@",[cellData objectForKey:@"AgriUpdatedAsOfText"],[cellData objectForKey:@"AgriUpdatedAsOfValue"]];
                    
                    NSDictionary *viewData = @{
                                               @"TextTotalCarton" : @"R1/R3 Oil Changes",
                                               @"TextPremiumTier" : @"R3 Oil Changes",
                                               @"ColorCodeDefaultOverall" : @"#A6A6A6",
                                               @"ColorCodeDefaultPremium" : @"#D9D9D9",
                                               @"NoOfOverallOrdered" : [cellData objectForKey:@"AgriAchievedTarget"],
                                               @"NoOfPremiumOrdered" : [cellData objectForKey:@"AgriBoosterAchievedTarget"],
                                               @"TotalPremiumTarget" : [cellData objectForKey:@"AgriBoosterTotalTarget"],
                                               @"OverallTarget" : [cellData objectForKey:@"AgriTotalTarget"],
                                               @"ColorCodeOverall" : [cellData objectForKey:@"AgriColorCode"],
                                               @"ColorCodePremium" : [cellData objectForKey:@"AgriBoosterColorCode"],
                                               @"PremiumFirstTarget": [cellData objectForKey:@"AgriBoosterFirstTarget"],
                                               @"PremiumSecondTarget": [cellData objectForKey:@"AgriBoosterSecondTarget"],
                                               @"TotalFirstTarget": [cellData objectForKey:@"AgriFirstTarget"],
                                               @"TotalSecondTarget": [cellData objectForKey:@"AgriSecondTarget"],
                                               };
                    LoyaltyProgramGaugeView *gaugeView = [cell viewWithTag:kCAROUSEL_AGRIPROMO_GAUGEVIEW];
                    [gaugeView.superview setNeedsDisplay];
                    gaugeView.isAgri = true;
                    [gaugeView loadBar: viewData isCarousel:YES];
                    
                    TTTAttributedLabel *lblFooterText = [cell viewWithTag:kCAROUSEL_AGRIPROMO_FOOTERTEXT];
                    NSString *csString = [NSString stringWithFormat:@"%@",[cellData objectForKey:@"AgriFooterText"]];
                    NSString *footerString;
                    if(SCREEN_WIDTH <= 320) {
                        csString = [csString stringByReplacingOccurrencesOfString:@"<b>" withString:@"<b style=\"color:#DD1D21 ; font-size:14px\">"];
                        footerString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:11; color: #404040;}</style>%@",csString];
                    } else {
                        csString = [csString stringByReplacingOccurrencesOfString:@"<b>" withString:@"<b style=\"color:#DD1D21 ; font-size:15px\">"];
                        footerString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:12; color: #404040;}</style>%@",csString];
                    }
                    lblFooterText.attributedText = [[NSAttributedString alloc] initWithData: [footerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                    
                    lblFooterText.font = FONT_B3;
                    lblFooterText.textColor = COLOUR_VERYDARKGREY;
                    lblFooterText.textAlignment = NSTextAlignmentCenter;
                    
                    UIButton *btnView = [cell viewWithTag: kCAROUSEL_CVSPEEDOMETER_BUTTON];
                    if([[cellData objectForKey:@"AgriIsComplete"] boolValue]) {
                        [btnView setTitle: LOCALIZATION(C_BTN_VIEWRECENTACTIVITY) forState:UIControlStateNormal];
                        [btnView addTarget:self action:@selector(viewRecentActivityPressed:) forControlEvents:UIControlEventTouchDown];
                    } else {
                        [btnView setTitle: LOCALIZATION(C_BTN_SCANNOW) forState:UIControlStateNormal];
                        [btnView addTarget:self action:@selector(scanNowPressed:) forControlEvents:UIControlEventTouchDown];
                    }
                    btnView.titleLabel.font = FONT_BUTTON;
                    
                    [NSLayoutConstraint constraintWithItem:btnView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:45].active = YES;
                }
            }
                break;
            case kCAROUSEL_CVSPEEDOMETER:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CVSpeedoMeterCarousel" forIndexPath:indexPath];
                
                NSDictionary *cellData = [GET_HOMEDATA objectForKey:@"CVBoosterPromotion"];
                
                if(![cellData isKindOfClass:[NSNull class]] && cellData != nil) {
                    UILabel *lblNameHeader = [cell viewWithTag:kCAROUSEL_CVSPEEDOMETER_HEADER];
                    lblNameHeader.text =[NSString stringWithFormat:@"%@",[cellData objectForKey:@"CVHeading"]];
                    if(SCREEN_WIDTH <= 320) {
                         lblNameHeader.font = FONT_H2;
                    } else {
                         lblNameHeader.font = FONT_H1;
                    }
                    lblNameHeader.textColor = COLOUR_VERYDARKGREY;
                    
                    UILabel *lblPeriod = [cell viewWithTag:kCAROUSEL_CVSPEEDOMETER_PERIOD];
                    lblPeriod.text = [NSString stringWithFormat:@"%@: %@ - %@",LOCALIZATION(C_LOYALTY_PERIOD),[cellData objectForKey:@"CVStartDate"],[cellData objectForKey:@"CVEndDate"]];
                    if(SCREEN_WIDTH <= 320) {
                        lblPeriod.font = FONT_B3;
                    } else {
                        lblPeriod.font = FONT_B2;
                    }
                    lblPeriod.textColor = COLOUR_VERYDARKGREY;
                    
                    UILabel *lblDataUpdated = [cell viewWithTag:kCAROUSEL_CVSPEEDOMETER_DATAUPDATEDLABEL];
                    if(SCREEN_WIDTH <= 320) {
                         lblDataUpdated.font = FONT_B3;
                    } else {
                        lblDataUpdated.font = FONT_B(12);
                    }
                    lblDataUpdated.textColor = COLOUR_VERYDARKGREY;
                    lblDataUpdated.text = [NSString stringWithFormat:@"%@ %@",[cellData objectForKey:@"CVUpdatedAsOfText"],[cellData objectForKey:@"CVUpdatedAsOfValue"]];
                    
                    CVSpeedoMeterView *gaugeView = [cell viewWithTag:kCAROUSEL_CVSPEEDOMETER_GAUGEVIEW];
                    [gaugeView.superview setNeedsDisplay];
                    [gaugeView loadBar: cellData isCarousel:YES];
                    
                    TTTAttributedLabel *lblFooterText = [cell viewWithTag:kCAROUSEL_CVSPEEDOMETER_FOOTERTEXT];
                    NSString *csString = [NSString stringWithFormat:@"%@",[cellData objectForKey:@"CVFooterText"]];
                    NSString *footerString;
                    if(SCREEN_WIDTH <= 320) {
                        csString = [csString stringByReplacingOccurrencesOfString:@"<b>" withString:@"<b style=\"color:#DD1D21 ; font-size:14px\">"];
                        footerString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:11; color: #404040;}</style>%@",csString];
                    } else {
                        csString = [csString stringByReplacingOccurrencesOfString:@"<b>" withString:@"<b style=\"color:#DD1D21 ; font-size:15px\">"];
                        footerString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:12; color: #404040;}</style>%@",csString];
                    }
                    lblFooterText.attributedText = [[NSAttributedString alloc] initWithData: [footerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                   
                    lblFooterText.font = FONT_B3;
                    lblFooterText.textColor = COLOUR_VERYDARKGREY;
                    lblFooterText.textAlignment = NSTextAlignmentCenter;
                    
                    UIButton *btnView = [cell viewWithTag: kCAROUSEL_CVSPEEDOMETER_BUTTON];
                    if([[cellData objectForKey:@"CVIsComplete"] boolValue]) {
                        [btnView setTitle: LOCALIZATION(C_BTN_VIEWRECENTACTIVITY) forState:UIControlStateNormal];
                        [btnView addTarget:self action:@selector(viewRecentActivityPressed:) forControlEvents:UIControlEventTouchDown];
                    } else {
                        [btnView setTitle: LOCALIZATION(C_BTN_SCANNOW) forState:UIControlStateNormal];
                        [btnView addTarget:self action:@selector(scanNowPressed:) forControlEvents:UIControlEventTouchDown];
                    }
                    btnView.titleLabel.font = FONT_BUTTON;
                    
                    [NSLayoutConstraint constraintWithItem:btnView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:45].active = YES;
                }
                
            }
                break;
            case kCAROUSEL_CNYPROMOTION: {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"IDCNYPromotionCollectionViewCell" forIndexPath:indexPath];
                
                if(!_isPromotionRedeemed) {
                    self.promotionDictionary = [GET_HOMEDATA objectForKey: @"TradesPromotion"];
                }
                NSDictionary *cellData = self.promotionDictionary;
                
                TTTAttributedLabel *redeemMessageLabel = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_REDEEMMESSAGELABEL];
                redeemMessageLabel.hidden = YES;
                
                NSString *imageURL = [[cellData objectForKey:@"TPImageUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
                UIImageView *rewardStatusImage = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_IMAGE];
                [rewardStatusImage sd_setImageWithURL:[NSURL URLWithString: imageURL] placeholderImage:[UIImage imageNamed:@"bg_lebaran_default"] options:SDWebImageRefreshCached | SDWebImageRetryFailed];
                
                UILabel *lblNameHeader = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_HEADER];
                lblNameHeader.text = [NSString stringWithFormat:@"%@",[cellData objectForKey:@"TPCSHeader"]];
                lblNameHeader.font = FONT_H1;
                lblNameHeader.textColor = COLOUR_VERYDARKGREY;
                
                UILabel *lblPeriod = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_PERIOD];
                lblPeriod.text = [NSString stringWithFormat:@"%@: %@ - %@",LOCALIZATION(C_LOYALTY_PERIOD),[cellData objectForKey:@"TPStartDate"],[cellData objectForKey:@"TPEndDate"]];     //lokalised
                lblPeriod.font = FONT_B2;
                lblPeriod.textColor = COLOUR_VERYDARKGREY;
                
                TTTAttributedLabel *lblFooterText = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_FOOTERTEXT];
                if([[cellData objectForKey:@"TPCSFooter"] isEqualToString:@""] || [[cellData objectForKey:@"TPCSFooter"] isKindOfClass:[NSNull class]]) {
                    lblFooterText.hidden = YES;
                } else {
                    lblFooterText.hidden = NO;
                    NSString *footerString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:10; color: #404040;}</style>%@",[cellData objectForKey:@"TPCSFooter"]];
                    lblFooterText.attributedText = [[NSAttributedString alloc] initWithData: [footerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                }
                
                UIView *daysLeftView = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_DAYSLEFTVIEW];
                
                UIButton *btn_carousel = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_BUTTON];
                UIButton *btnRedeem = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_REDEEMBUTTON];
                if([[cellData objectForKey:@"HasTPViewDetailsBtn"] boolValue]) {
                    daysLeftView.hidden = NO;
                    
                    UILabel *lblDaysLeft = [cell viewWithTag:kCAROUSEL_CNYPROMOTION_DAYSLEFTLABEL];
                    NSString *daysLeftString = [LOCALIZATION(C_CAROUSEL_PROMOTION_DAYSLEFT) stringByReplacingOccurrencesOfString:@"XXX" withString: [NSString stringWithFormat:@"%@",[cellData objectForKey:@"TPDaysLeft"]]];        //lokalised
                    NSMutableAttributedString *daysLeftAttrString = [[NSMutableAttributedString alloc]initWithString:daysLeftString attributes:@{
                                                                                                                                                                          NSFontAttributeName : FONT_B3,
                                                                                                                                                                          NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,
                                                                                                                                                                          }];
                    [daysLeftAttrString setAttributes:@{NSFontAttributeName : FONT_H(20), NSForegroundColorAttributeName : COLOUR_VERYDARKGREY} range: [daysLeftString rangeOfString:[NSString stringWithFormat:@"%@",[cellData objectForKey:@"TPDaysLeft"]]]];
                    lblDaysLeft.attributedText = daysLeftAttrString;
                    
                    daysLeftView.layer.cornerRadius = 30;
                    daysLeftView.layer.masksToBounds = YES;
                    daysLeftView.layer.borderWidth = 1;
                    daysLeftView.layer.borderColor = [COLOUR_VERYDARKGREY CGColor];
                    
                    btn_carousel.hidden = NO;
                    [NSLayoutConstraint constraintWithItem:btn_carousel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:45].active = YES;
                    [btn_carousel setTitle:LOCALIZATION(C_FORM_VIEWDETAIL) forState:UIControlStateNormal];      //lokalised
                    
                    [btnRedeem setUserInteractionEnabled:NO];
                    
                } else {
                    daysLeftView.hidden = YES;
                    btn_carousel.hidden = YES;
                    [NSLayoutConstraint constraintWithItem:btn_carousel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
                    
                }
                
                if(![[cellData objectForKey:@"TPFinalMessage1"] isEqualToString:@""] && ![[cellData objectForKey:@"TPFinalMessage1"] isKindOfClass:[NSNull class]]) {
                    redeemMessageLabel.hidden = NO;
                    
                    if(SCREEN_WIDTH <= 320) {
                        [NSLayoutConstraint constraintWithItem:redeemMessageLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:40].active = YES;
                    }
                    
                    NSString *redeemString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:12; color: #404040;}</style>%@",[cellData objectForKey:@"TPFinalMessage1"]];
                    redeemMessageLabel.attributedText = [[NSAttributedString alloc] initWithData: [redeemString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                    [btnRedeem setUserInteractionEnabled:NO];
                } else if(![[cellData objectForKey:@"TPFinalMessage2"] isEqualToString:@""] && ![[cellData objectForKey:@"TPFinalMessage2"] isKindOfClass:[NSNull class]]) {
                    redeemMessageLabel.hidden = NO;
                    
                    [NSLayoutConstraint constraintWithItem:redeemMessageLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:cell attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0].active = YES;
                    
                    NSString *redeemString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:15; color: #404040;}</style>%@",[cellData objectForKey:@"TPFinalMessage2"]];
                    redeemMessageLabel.attributedText = [[NSAttributedString alloc] initWithData: [redeemString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                    [btnRedeem setUserInteractionEnabled:NO];
                }
                
            }
                break;
            case kCAROUSEL_SHAREBADGE:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FAShareBadgeCell" forIndexPath:indexPath];
                                
                UILabel *lblNameHeader = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_NAME_HEADER];
                lblNameHeader.text = LOCALIZATION(C_HOMECAROUSEL_NAME);   //lokalised 16 Jan
                
                UILabel *lblName = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_NAME];
                lblName.text = [[mSession profileInfo] fullName];
                
                UILabel *lblBadgeHeader = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_SHAREBADGEID_HEADER];
                lblBadgeHeader.text = LOCALIZATION(C_SCANBOTTLE_RESULT_SHARECODE);  //lokalised 16 Jan
                
                UILabel *lblBadge = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_SHAREBADGEID];
                lblBadge.text = [[mSession profileInfo] shareTagCode];
                
                NSString *urlString = [[[mSession profileInfo] shareQRCode] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
                
                UIImageView *faShareQRImage = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_IMAGE];
                [faShareQRImage sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"icon-code"] options: (SDWebImageRefreshCached | SDWebImageRetryFailed)];
                
                TTTAttributedLabel *lblFooter = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_FOOTER];
                lblFooter.textColor = COLOUR_VERYDARKGREY;
                lblFooter.textAlignment = NSTextAlignmentCenter;
                lblFooter.delegate = self;
                [Helper setHyperlinkLabel:lblFooter hyperlinkText:LOCALIZATION(C_HOMECAROUSEL_BADGEFOOTER_GENERAL_LINK) hyperlinkFont:FONT_H3 bodyText:LOCALIZATION(C_HOME_BADGEFOOTER_GENERAL_FULLTEXT) bodyFont:FONT_B3 urlString:@"registerOil"];
                
                UIView *badgeView = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_BADGEVIEW];
                BOOL setFooterHidden = NO;
                LoginInfo *loginInfo = [mSession loadUserProfile];
                
                if(IS_COUNTRY(COUNTRYCODE_THAILAND)) {
                    if ([[loginInfo companyType] isEqualToString:@"32768"] || [[loginInfo companyType] isEqualToString:@"65536"] || [[loginInfo companyType] isEqualToString:@"131072"]) {
                        setFooterHidden = YES;
                    }
                } else if(IS_COUNTRY(COUNTRYCODE_INDONESIA)) {
                    if ([[loginInfo companyType] isEqualToString:@"2"] || [[loginInfo companyType] isEqualToString:@"8"]) {
                        setFooterHidden = YES;
                    }
                }
                
                if(setFooterHidden) {
                    [lblFooter setHidden:YES];
                    [NSLayoutConstraint constraintWithItem:lblFooter attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
                    [NSLayoutConstraint constraintWithItem:badgeView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0].active = YES;
                } else {
                    [lblFooter setHidden:NO];
                }
                
                if(SCREEN_WIDTH <= 320) {
                    UIView *QRImageView = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_IMAGEVIEW];
                    [NSLayoutConstraint constraintWithItem:QRImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:75].active = YES;
                }
                
                UIImageView *topBadgeImage = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_TOPIMAGE];
                if(IS_COUNTRY(COUNTRYCODE_TURKEY)) {
                    [topBadgeImage setImage: [UIImage imageNamed:@"logo_card_TR.png"]];
                } else {
                    [topBadgeImage setImage: [UIImage imageNamed:@"carousel_share_logo.png"]];
                }
                [topBadgeImage setContentMode:UIViewContentModeScaleAspectFit];
            }
                break;
            case kCAROUSEL_LOYALTYPROGRAM:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LoyaltyProgramCarousel" forIndexPath:indexPath];
                
                NSDictionary *cellData = _loyaltyPerformanceCarouselDetails;
                
                if(![cellData isKindOfClass:[NSNull class]] && cellData != nil) {
                    UILabel *lblNameHeader = [cell viewWithTag:kCAROUSEL_LOYALTYPROGRAM_HEADER];
                    lblNameHeader.text = LOCALIZATION(C_LOYALTY_LOYALTYPROGRAM);    //lokalised 7 Feb
                    if(SCREEN_WIDTH <= 320 || SCREEN_HEIGHT < 700) {
                        lblNameHeader.font = FONT_H2;
                    } else {
                        lblNameHeader.font = FONT_H1;
                    }
                    lblNameHeader.textColor = COLOUR_VERYDARKGREY;
                    
                    UILabel *lblPeriod = [cell viewWithTag:kCAROUSEL_LOYALTYPROGRAM_PERIOD];
                    lblPeriod.text = [NSString stringWithFormat:@"%@: %@ - %@",LOCALIZATION(C_LOYALTY_PERIOD),[cellData objectForKey:@"StartDate"],[cellData objectForKey:@"EndDate"]];  //lokalise 8 Feb
                    if(SCREEN_WIDTH <= 320 || SCREEN_HEIGHT < 700) {
                        lblPeriod.font = FONT_B3;
                    } else {
                        lblPeriod.font = FONT_B2;
                    }
                    lblPeriod.textColor = COLOUR_VERYDARKGREY;
                    
                    UILabel *lblDisclaimerText = [cell viewWithTag:kCAROUSEL_LOYALTYPROGRAM_DISCLAIMERTEXT];
                    lblDisclaimerText.text = [NSString stringWithFormat:@"%@", [cellData objectForKey:@"TextCSHeaderMessage"]];
                    lblDisclaimerText.font = SCREEN_HEIGHT < 700 ? FONT_B(8) : FONT_B3;
                    lblDisclaimerText.textColor = COLOUR_VERYDARKGREY;
                    
                    LoyaltyProgramGaugeView *gaugeView = [cell viewWithTag:kCAROUSEL_LOYALTYPROGRAM_GAUGEVIEW];
                    [gaugeView.superview setNeedsDisplay];
                    gaugeView.isAgri = false;
                    NSDictionary *viewData = @{
                                               @"TextTotalCarton" : [cellData objectForKey:@"TextTotalCartons"] == nil  && [[cellData objectForKey:@"TextTotalCartons"] isKindOfClass:[NSNull class]]? @"" : [cellData objectForKey:@"TextTotalCartons"],
                                               @"TextPremiumTier" :  [cellData objectForKey:@"TextPremiumTier"] == nil && [[cellData objectForKey:@"TextPremiumTier"] isKindOfClass:[NSNull class]]? @"" : [cellData objectForKey:@"TextPremiumTier"],
                                               @"ColorCodeDefaultOverall" : [_loyaltyPerformanceCarouselDetails objectForKey:@"ColorCodeDefaultOverall"],
                                               @"ColorCodeDefaultPremium" : [_loyaltyPerformanceCarouselDetails objectForKey:@"ColorCodeDefaultPremium"],
                                               @"NoOfOverallOrdered" : [_loyaltyPerformanceCarouselDetails objectForKey:@"TotalOrdered"],
                                               @"NoOfPremiumOrdered" : [_loyaltyPerformanceCarouselDetails objectForKey:@"TotalPremiumAcheived"],
                                               @"TotalPremiumTarget" : [_loyaltyPerformanceCarouselDetails objectForKey:@"TotalPremiumTarget"],
                                               @"OverallTarget" : [_loyaltyPerformanceCarouselDetails objectForKey:@"TotalTargetQty"],
                                               @"ColorCodeOverall" : [_loyaltyPerformanceCarouselDetails objectForKey:@"ColorCodeOverall"],
                                               @"ColorCodePremium" : [_loyaltyPerformanceCarouselDetails objectForKey:@"ColorCodePremium"],
                                               };
                    [gaugeView loadBar: viewData isCarousel:YES];
                    
                    TTTAttributedLabel *lblFooterText = [cell viewWithTag:kCAROUSEL_LOYALTYPROGRAM_FOOTERTEXT];
                    if([[cellData objectForKey:@"IsComplete"] boolValue]) {
                        NSString *footerString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:13; color: #404040;}</style>%@",[cellData objectForKey:@"TextCSFooterMessage"]];
                        lblFooterText.attributedText = [[NSAttributedString alloc] initWithData: [footerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                    }
                    else{
                        NSString *csString = [NSString stringWithFormat:@"%@",[cellData objectForKey:@"TextCSFooterMessage"]];
                        NSString *footerString;
                        if(SCREEN_WIDTH <= 320 || SCREEN_HEIGHT < 700) {
                            csString = [csString stringByReplacingOccurrencesOfString:@"<b>" withString:@"<b style=\"color:#DD1D21 ; font-size:12px\">"];
                            footerString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:10; color: #404040;}</style>%@",csString];
                        } else {
                            csString = [csString stringByReplacingOccurrencesOfString:@"<b>" withString:@"<b style=\"color:#DD1D21 ; font-size:13px\">"];
                            footerString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:10; color: #404040;}</style>%@",csString];
                        }
                        lblFooterText.attributedText = [[NSAttributedString alloc] initWithData: [footerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                    }
                    lblFooterText.font = FONT_B3;
                    lblFooterText.textColor = COLOUR_VERYDARKGREY;
                    
                    UIButton *btnView = [cell viewWithTag: kCAROUSEL_LOYALTYPROGRAM_BUTTON];
                    if([[cellData objectForKey:@"IsComplete"] boolValue]) {
                         [NSLayoutConstraint constraintWithItem:btnView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
                        [NSLayoutConstraint constraintWithItem:lblFooterText attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:btnView attribute:NSLayoutAttributeTop multiplier:1.0 constant:-15].active = YES;
                        btnView.hidden = YES;
                    } else {
                        [btnView setTitle: LOCALIZATION(C_LOYALTY_ORDERNOW) forState:UIControlStateNormal];  // lokalised 8 Feb
                    }
                    btnView.titleLabel.font = FONT_BUTTON;
                    
                    if(SCREEN_WIDTH <= 320 || SCREEN_HEIGHT < 700) {
                         [NSLayoutConstraint constraintWithItem:btnView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:40].active = YES;
                    } else {
                         [NSLayoutConstraint constraintWithItem:btnView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:45].active = YES;
                    }
                }
                
            }
                break;
            case kCAROUSEL_LOYALTYBREAKDOWN:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LoyaltyBreakdownCarousel" forIndexPath:indexPath];
                NSDictionary *cellData = _loyaltyPerformanceCarouselDetails;
                
                UILabel *lblNameHeader = [cell viewWithTag:kCAROUSEL_LOYALTYBREAKDOWN_HEADER];
                lblNameHeader.text = LOCALIZATION(C_LOYALTY_LOYALTYPROGRAMBREAKDOWN);  //lokalised 8 Feb
                if(SCREEN_WIDTH <= 320 || SCREEN_HEIGHT < 700) {
                    lblNameHeader.font = FONT_H2;
                } else {
                    lblNameHeader.font = FONT_H1;
                }
                lblNameHeader.textColor = COLOUR_VERYDARKGREY;
                
                UILabel *lblPeriod = [cell viewWithTag:kCAROUSEL_LOYALTYBREAKDOWN_PERIOD];
                lblPeriod.text = [NSString stringWithFormat:@"%@: %@ - %@",LOCALIZATION(C_LOYALTY_PERIOD),[cellData objectForKey:@"StartDate"],[cellData objectForKey:@"EndDate"]];  //lokalised 8 Feb
                if(SCREEN_WIDTH <= 320 || SCREEN_HEIGHT < 700) {
                    lblPeriod.font = FONT_B3;
                } else {
                    lblPeriod.font = FONT_B2;
                }
                lblPeriod.textColor = COLOUR_VERYDARKGREY;
                
                UILabel *lblDisclaimerText = [cell viewWithTag:kCAROUSEL_LOYALTYBREAKDOWN_DISCLAIMERTEXT];
                lblDisclaimerText.text = [NSString stringWithFormat:@"%@", [cellData objectForKey:@"TextCSHeaderMessage"]];
                lblDisclaimerText.font = SCREEN_HEIGHT < 700? FONT_B(8) : FONT_B3;
                lblDisclaimerText.textColor = COLOUR_VERYDARKGREY;
                
                LoyaltyBreakdownView *barView = [cell viewWithTag:kCAROUSEL_LOYALTYBREAKDOWN_BARVIEW];
                [barView.superview setNeedsDisplay];
                [barView.superview layoutIfNeeded];
                [barView loadBar:cellData isCarousel:YES];
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_LOYALTYBREAKDOWN_BUTTON];
                [btnView setTitle: LOCALIZATION(C_LOYALTY_VIEWBREAKDOWN) forState:UIControlStateNormal];  // lokalised 8 Feb
                btnView.titleLabel.font = FONT_BUTTON;
                if(SCREEN_WIDTH <= 320 || SCREEN_HEIGHT < 700) {
                     [NSLayoutConstraint constraintWithItem:btnView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:40].active = YES;
                } else {
                     [NSLayoutConstraint constraintWithItem:btnView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:45].active = YES;
                }
            }
                break;
            case kCAROUSEL_FASHAREBADGE:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FAShareBadgeCell" forIndexPath:indexPath];
                
                UILabel *lblNameHeader = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_NAME_HEADER];
                lblNameHeader.text = LOCALIZATION(C_HOMECAROUSEL_NAME); //Lokalise 16 Jan
                
                UILabel *lblName = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_NAME];
                lblName.text = [[mSession profileInfo] fullName];
                
                UILabel *lblBadgeHeader = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_SHAREBADGEID_HEADER];
                lblBadgeHeader.text = LOCALIZATION(C_HOMECAROUSEL_SHAREBADGEID); //Lokalise 16 Jan
                
                UILabel *lblBadge = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_SHAREBADGEID];
                lblBadge.text = [[mSession profileInfo] faShareCode];
                
                
                NSString *urlString = [[[mSession profileInfo] faShareQRCode] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

                UIImageView *faShareQRImage = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_IMAGE];
                [faShareQRImage sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"icon-code"] options: (SDWebImageRefreshCached | SDWebImageRetryFailed)];
                
                UILabel *lblFooter = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_FOOTER];
                lblFooter .text = LOCALIZATION(C_HOMECAROUSEL_BADGEFOOTER); //Lokalise 16 Jan
                lblFooter.font = FONT_B2;
                
                UIView *badgeView = [cell viewWithTag:kCAROUSEL_FASHAREBADGE_BADGEVIEW];
                [NSLayoutConstraint constraintWithItem:badgeView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:10].active = YES;
                
            }
                break;
            case kCAROUSEL_PROFILE:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProfileCell" forIndexPath:indexPath];
                
                UILabel *lblHeader = [cell viewWithTag: kCAROUSEL_PROFILE_HEADER];
                lblHeader.text = LOCALIZATION(C_HEADER_MYPROFILE);  //lokalised 17 jan
                lblHeader.font = FONT_H1;
                
                UILabel *lblName = [cell viewWithTag: kCAROUSEL_PROFILE_NAME];
                lblName.text = [[mSession profileInfo] fullName];
                lblName.font = FONT_H(25);
                
                UILabel *lblCompanyName = [cell viewWithTag: kCAROUSEL_PROFILE_COMPANYNAME];
                lblCompanyName.text = [[mSession profileInfo] companyName];
                lblCompanyName.font = FONT_B1;
                
                
                UIImageView *imgTierIcon = [cell viewWithTag: kCAROUSEL_PROFILE_COMPANYTIERICON];
                
                UILabel *lblTierText = [cell viewWithTag: kCAROUSEL_PROFILE_COMPANYTIERTEXT];
                if ([[[mSession profileInfo] companyCertTierText] length] > 0)
                {
                    imgTierIcon.hidden = NO;
                    imgTierIcon.image = [UIImage imageNamed: [[mSession profileInfo] companyCertTierImage]];

                    lblTierText.hidden = NO;
                    lblTierText.text = [[mSession profileInfo] companyCertTierText];
                    lblTierText.font = FONT_H1;
                }
                else
                {
                    lblTierText.hidden = YES;
                    imgTierIcon.hidden = YES;
                }
                
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_PROFILE_BUTTON];
                [btnView setTitle: LOCALIZATION(C_FORM_VIEWPROFILE) forState:UIControlStateNormal]; //lokalised 17 jan
                btnView.titleLabel.font = FONT_BUTTON;
            }
                break;
                
            case kCAROUSEL_POINTS:
            {
                NSDictionary *pointsData = [GET_HOMEDATA objectForKey: @"MyPoints"];

                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PointsCell" forIndexPath:indexPath];
                
                UILabel *lblAvailablePointsHeader = [cell viewWithTag: kCAROUSEL_POINTS_AVAILABLE_HEADER];
                lblAvailablePointsHeader.text = LOCALIZATION(C_HOMECAROUSEL_AVAILABLEPOINTS); //lokalise 17 jan
                lblAvailablePointsHeader.font = FONT_H(18);
                
                UILabel *lblAvailablePoints = [cell viewWithTag: kCAROUSEL_POINTS_AVAILABLE_POINTS];
                lblAvailablePoints.font = FONT_H(20);
                NSString *availablePointString = [self addCommaThousandSeparatorsWithInputString:[pointsData objectForKey: @"BalancePoints"]];
                lblAvailablePoints.text = availablePointString;
                lblAvailablePoints.textColor = COLOUR_RED;
                
                
                NSArray *expiringPoints = [pointsData objectForKey: @"ExpiringPoints"];
                
                UILabel *lblExpiringPointsHeader = [cell viewWithTag: kCAROUSEL_POINTS_EXPIRING_HEADER];
                lblExpiringPointsHeader.text = LOCALIZATION(C_HOMECAROUSEL_EXPIRINGPOINTS); //lokalise 17 jan
                lblExpiringPointsHeader.font = FONT_H(18);
                
                //points
              for (int i = 0; i < 3; i++)
                {
                    if (![expiringPoints isKindOfClass:[NSArray class]]) { break; }
                    if ([expiringPoints count] >= i) { break; }
                    //use first date and points as offset
                    NSDictionary *expiringPointsData = expiringPoints[i];
                    
                    UILabel *lblExpiringDate = [cell viewWithTag: kCAROUSEL_POINTS_FIRSTDATE + i];
                    lblExpiringDate.font = FONT_B1;
                    lblExpiringDate.text = [expiringPointsData objectForKey: @"ExpiryDate"]
                    ;
                    
                    UILabel *lblExpiringPoints = [cell viewWithTag: kCAROUSEL_POINTS_FIRSTPOINTS + i];
                    lblExpiringPoints.text =  [self addCommaThousandSeparatorsWithInputString: [expiringPointsData objectForKey: @"PointsExpiring"]];
                    lblExpiringPoints.font = FONT_H1;
                }
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_POINTS_BUTTON];
                
                if ([[mSession profileInfo] hasMyRewards])
                    btnView.hidden = NO;
                else
                    btnView.hidden = YES;
                [btnView setTitle: LOCALIZATION(C_HOMECAROUSEL_REDEEMPOINTS) forState:UIControlStateNormal];    //lokalise 17 jan
                btnView.titleLabel.font = FONT_BUTTON;
            }

                break;
            
            case kCAROUSEL_MYPERFORMANCE:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FAPerformanceCell" forIndexPath:indexPath];
                
                UILabel *lblPerformanceHeader = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_HEADER];
                lblPerformanceHeader.text = LOCALIZATION(C_TITLE_PERFORMANCE);  //lokalised
                lblPerformanceHeader.font = FONT_H1;
                
                UILabel *lblPerformanceSubHeader = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_SUBHEADER];
                lblPerformanceSubHeader.font = FONT_B2;
                
                NSDictionary *performanceData = [GET_HOMEDATA objectForKey: @"MyPerformance"];
                lblPerformanceSubHeader.text = [NSString stringWithFormat:@"%@ %@",LOCALIZATION(C_HOMECAROUSEL_UPDATEDON),[performanceData objectForKey:@"Month"]]; //lokalised
                
                if (![[performanceData objectForKey: @"PerformanceKeyOne"] isKindOfClass:[NSNull class]] && ![[performanceData objectForKey: @"PerformanceKeyOne"] isEqualToString: BLANK])
                {
                    UILabel *lblFirstKey = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_FIRSTKEY];
                    UILabel *lblFirstData = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_FIRSTDATA];
                    
                    lblFirstKey.text = [performanceData objectForKey: @"PerformanceKeyOne"];
                    lblFirstData.text = [[performanceData objectForKey: @"PerformanceValueOne"] stringValue];
                    
                    lblFirstKey.font = FONT_B(12);
                    lblFirstData.font = FONT_H(18);
                    
                    lblFirstKey.textColor = COLOUR_DARKGREY;
                    lblFirstData.textColor = COLOUR_RED;
                    
                    lblFirstKey.hidden = NO;
                    lblFirstData.hidden = NO;
                    
                    if (kIsRightToLeft) {
                        lblFirstData.textAlignment = NSTextAlignmentLeft;
                    } else {
                        lblFirstData.textAlignment = NSTextAlignmentRight;
                    }
                }
                
                if (![[performanceData objectForKey: @"PerformanceKeyTwo"] isKindOfClass:[NSNull class]] && ![[performanceData objectForKey: @"PerformanceKeyTwo"] isEqualToString: BLANK])
                {
                    UILabel *lblSecondKey = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_SECONDKEY];
                    UILabel *lblSecondData = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_SECONDDATA];
                    UIView *lblFirstSeparator = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_FIRSTSEPARATOR];
                    
                    lblSecondKey.text = [performanceData objectForKey: @"PerformanceKeyTwo"];
                    lblSecondData.text = [[performanceData objectForKey: @"PerformanceValueTwo"] stringValue];
                    
                    lblSecondKey.font = FONT_B(12);
                    lblSecondData.font = FONT_H(18);
                    
                    lblSecondKey.textColor = COLOUR_DARKGREY;
                    lblSecondData.textColor = COLOUR_RED;
                    
                    lblSecondKey.hidden = NO;
                    lblSecondData.hidden = NO;
                    lblFirstSeparator.hidden = NO;
                    
                    if (kIsRightToLeft) {
                        lblSecondData.textAlignment = NSTextAlignmentLeft;
                    } else {
                        lblSecondData.textAlignment = NSTextAlignmentRight;
                    }
                }
                
                if (![[performanceData objectForKey: @"PerformanceKeyThree"] isKindOfClass:[NSNull class]] && ![[performanceData objectForKey: @"PerformanceKeyThree"] isEqualToString: BLANK])
                {
                    UILabel *lblThirdKey = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_THIRDKEY];
                    UILabel *lblThirdData = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_THIRDDATA];
                    UIView *lblSecondSeparator = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_SECONDSEPARATOR];
                    
                    lblThirdKey.text = [performanceData objectForKey: @"PerformanceKeyThree"];
                    lblThirdData.text = [[performanceData objectForKey: @"PerformanceValueThree"] stringValue];
                    
                    lblThirdKey.font = FONT_B(12);
                    lblThirdData.font = FONT_H(18);
                    
                    lblThirdKey.textColor = COLOUR_DARKGREY;
                    lblThirdData.textColor = COLOUR_RED;
                    
                    lblThirdKey.hidden = NO;
                    lblThirdData.hidden = NO;
                    lblSecondSeparator.hidden = NO;
                    
                    if (kIsRightToLeft) {
                        lblThirdData.textAlignment = NSTextAlignmentLeft;
                    } else {
                        lblThirdData.textAlignment = NSTextAlignmentRight;
                    }
                }
                
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_MYPERFORMANCE_BUTTON];
                [btnView setTitle: LOCALIZATION(C_FORM_VIEWDETAIL) forState:UIControlStateNormal];  //lokalise 17 jan
                btnView.titleLabel.font = FONT_BUTTON;
            }
                break;
            case kCAROUSEL_OUTLETPERFORMANCE:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"OutletPerformanceCell" forIndexPath:indexPath];
                
                UILabel *lblPerformanceHeader = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_HEADER];
                lblPerformanceHeader.font = FONT_H1;
                
                UILabel *lblPerformanceSubHeader = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_SUBHEADER];
                
                NSDictionary *performanceData = [GET_HOMEDATA objectForKey: @"MyOutletPerformance"];
                lblPerformanceHeader.text = [performanceData objectForKey:@"CarouselHeader"];
                lblPerformanceSubHeader.text = [NSString stringWithFormat:@"%@ %@",LOCALIZATION(C_HOMECAROUSEL_UPDATEDON),[performanceData objectForKey:@"OutletPerfUpdatedOn"]]; //lokalised
                lblPerformanceSubHeader.font = FONT_B2;
//                lblPerformanceSubHeader.text = [performanceData objectForKey:@"OutletPerfUpdatedOn"];
                
                if (![[performanceData objectForKey: @"PerformanceKeyOne"] isKindOfClass:[NSNull class]] && ![[performanceData objectForKey: @"PerformanceKeyOne"] isEqualToString: BLANK])
                {
                    UILabel *lblFirstKey = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_FIRSTKEY];
                    UILabel *lblFirstData = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_FIRSTDATA];
                    
                    lblFirstKey.text = [performanceData objectForKey: @"PerformanceKeyOne"];
                    lblFirstData.text = [[performanceData objectForKey: @"PerformanceValueOne"] stringValue];
                    
                    lblFirstKey.font = FONT_B(13);
                    lblFirstData.font = FONT_H(18);
                    
                    lblFirstKey.textColor = COLOUR_DARKGREY;
                    lblFirstData.textColor = COLOUR_RED;
                    
                    lblFirstKey.hidden = NO;
                    lblFirstData.hidden = NO;
                    
                    if (kIsRightToLeft) {
                        lblFirstData.textAlignment = NSTextAlignmentLeft;
                    } else {
                        lblFirstData.textAlignment = NSTextAlignmentRight;
                    }
                    
                    if(![[performanceData objectForKey:@"PerformanceIconOne"] isKindOfClass:[NSNull class]]) {
                        UIImageView *imageView = [cell viewWithTag:6900];
                        UIImage *image = [UIImage imageNamed:[performanceData objectForKey:@"PerformanceIconOne"]];
                          if(![image isKindOfClass:[NSNull class]]) {
                              [imageView setImage:image];
                          }
                    }
                }
                
                if (![[performanceData objectForKey: @"PerformanceKeyTwo"] isKindOfClass:[NSNull class]] && ![[performanceData objectForKey: @"PerformanceKeyTwo"] isEqualToString: BLANK])
                {
                    UILabel *lblSecondKey = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_SECONDKEY];
                    UILabel *lblSecondData = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_SECONDDATA];
                    UIView *lblFirstSeparator = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_FIRSTSEPARATOR];
                    
                    lblSecondKey.text = [performanceData objectForKey: @"PerformanceKeyTwo"];
                    lblSecondData.text = [[performanceData objectForKey: @"PerformanceValueTwo"] stringValue];
                    
                    lblSecondKey.font = FONT_B(13);
                    lblSecondData.font = FONT_H(18);
                    
                    lblSecondKey.textColor = COLOUR_DARKGREY;
                    lblSecondData.textColor = COLOUR_RED;
                    
                    lblSecondKey.hidden = NO;
                    lblSecondData.hidden = NO;
                    lblFirstSeparator.hidden = NO;
                    
                    if (kIsRightToLeft) {
                        lblSecondData.textAlignment = NSTextAlignmentLeft;
                    } else {
                        lblSecondData.textAlignment = NSTextAlignmentRight;
                    }
                    
                    if(![[performanceData objectForKey:@"PerformanceIconTwo"] isKindOfClass:[NSNull class]]) {
                        UIImageView *imageView = [cell viewWithTag:6901];
                        UIImage *image = [UIImage imageNamed:[performanceData objectForKey:@"PerformanceIconTwo"]];
                        if(![image isKindOfClass:[NSNull class]]) {
                            [imageView setImage:image];
                        }
                    }
                    
                }
                
                if (![[performanceData objectForKey: @"PerformanceKeyThree"] isKindOfClass:[NSNull class]] && ![[performanceData objectForKey: @"PerformanceKeyThree"] isEqualToString: BLANK])
                {
                    UILabel *lblThirdKey = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_THIRDKEY];
                    UILabel *lblThirdData = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_THIRDDATA];
                    UIView *lblSecondSeparator = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_SECONDSEPARATOR];
                    
                    lblThirdKey.text = [performanceData objectForKey: @"PerformanceKeyThree"];
                    lblThirdData.text = [[performanceData objectForKey: @"PerformanceValueThree"] stringValue];
                    
                    lblThirdKey.font = FONT_B(13);
                    lblThirdData.font = FONT_H(18);
                    
                    lblThirdKey.textColor = COLOUR_DARKGREY;
                    lblThirdData.textColor = COLOUR_RED;
                    
                    lblThirdKey.hidden = NO;
                    lblThirdData.hidden = NO;
                    lblSecondSeparator.hidden = NO;
                    
                    if (kIsRightToLeft) {
                        lblThirdData.textAlignment = NSTextAlignmentLeft;
                    } else {
                        lblThirdData.textAlignment = NSTextAlignmentRight;
                    }
                    
                    if(![[performanceData objectForKey:@"PerformanceIconThree"] isKindOfClass:[NSNull class]]) {
                        UIImageView *imageView = [cell viewWithTag:6902];
                        UIImage *image = [UIImage imageNamed:[performanceData objectForKey:@"PerformanceIconThree"]];
                        if(![image isKindOfClass:[NSNull class]]) {
                            [imageView setImage:image];
                        }
                    }
                }
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_OUTLETPERFORMANCE_BUTTON];
                [btnView setTitle: LOCALIZATION(C_FORM_VIEWDETAIL) forState:UIControlStateNormal];  //lokalise 17 jan
                btnView.titleLabel.font = FONT_BUTTON;
                
                if([[performanceData objectForKey:@"HasPerformanceViewDetails"]boolValue]) {
                    
                } else {
                    btnView.hidden = YES;
                }
                
            }
                break;
            case kCAROUSEL_PARTNERS_B2B:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PartnersCell" forIndexPath:indexPath];
                
                UILabel *lblHeader = [cell viewWithTag: kCAROUSEL_PARTNERS_HEADER];
                lblHeader.text = [NSString stringWithFormat:@"%@ (B2B)",LOCALIZATION(C_SIDEMENU_PARTNERS)];                             //lokalised
                
                UILabel *lblSubheader = [cell viewWithTag: kCAROUSEL_PARTNERS_SUBHEADER];
                lblSubheader.text = LOCALIZATION(C_RUSSIA_CAROUSEL_FINALSCORE);                 //lokalised
                
                RUPartnersBarView *partnersBarView = [cell viewWithTag: kCAROUSEL_PARTNERS_PROGRESSBAR];
                [partnersBarView.superview setNeedsDisplay];
                [partnersBarView loadBar: @{@"BusinessType" : @(1),@"Data" : self.carouselPartnerDictB2B}];
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_PARTNERS_BUTTON];
                [btnView setTitle: LOCALIZATION(C_RUSSIA_CAROUSEL_EARNPOINTBTN) forState:UIControlStateNormal];  //loklaised
                btnView.titleLabel.font = FONT_BUTTON;
            }
                break;
                
            case kCAROUSEL_PARTNERSENDED_B2B:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PartnersEndedCell" forIndexPath:indexPath];
                
                UILabel *lblHeader = [cell viewWithTag: 1];
                lblHeader.text = [NSString stringWithFormat:@"%@ (B2B)",LOCALIZATION(C_SIDEMENU_PARTNERS)];                             //lokalised
                
                UILabel *lblThankYou = [cell viewWithTag: 2];
                lblThankYou.text = LOCALIZATION(C_RUSSIA_THANKYOUPARTICIPATION);                //lokalised
                
                UILabel *lblMoreInteresting = [cell viewWithTag: 3];
                lblMoreInteresting.text = LOCALIZATION(C_RUSSIA_MOREINTERESTINGCAMPAIGN);       //lokalised
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_PARTNERS_BUTTON];
                [btnView setTitle: LOCALIZATION(C_RUSSIA_CAROUSEL_EARNPOINTBTN) forState:UIControlStateNormal];     //laoklised
                btnView.titleLabel.font = FONT_BUTTON;
            }
                break;
            case kCAROUSEL_RU_POINTS_B2B:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RUPointsCell" forIndexPath:indexPath];
                
                UILabel *lblHeader = [cell viewWithTag: kCAROUSEL_RU_POINTS_HEADER];
                lblHeader.text = [NSString stringWithFormat:@"%@ (B2B)",LOCALIZATION(C_TITLE_MYPOINTS)];                                                    //lokalised
                
                UILabel *lblSubheader = [cell viewWithTag: kCAROUSEL_RU_POINTS_SUBHEADER];
                lblSubheader.text = LOCALIZATION(C_RUSSIA_CAROUSEL_FINALSCORE);                                     //lokalised
                
                RUProgressBarView *progressBarView = [cell viewWithTag: 200];
                
                UILabel *lblMinPoints = [cell viewWithTag: 5];
                
                lblMinPoints.text = [NSString stringWithFormat: @"%@ %@",
                                     @"0", LOCALIZATION(C_REWARDS_PTS)];                                            //lokalised
                
                UILabel *lblMaxPoints = [cell viewWithTag: 6];
                
                lblMaxPoints.text = [NSString stringWithFormat: @"%@ %@", [self.carouselPointsDictB2B objectForKey: @"MaxPoints"], LOCALIZATION(C_REWARDS_PTS)];   //lokalised
                
                progressBarView.maxPoints = [[self.carouselPointsDictB2B objectForKey: @"MaxPoints"] intValue];
                
                if (![[self.carouselPointsDictB2B objectForKey: @"ProductPoints"] isKindOfClass: [NSNull class]])
                {
                    NSArray *productPoints = [self.carouselPointsDictB2B objectForKey: @"ProductPoints"];
                    for (int i = 0; i < [productPoints count]; i++)
                    {
                        NSDictionary *productData = [productPoints objectAtIndex: i];
                        UILabel *lblPoints, *lblPointsData;
                        switch (i)
                        {
                            case 0:
                            {
                                progressBarView.firstPoints = [[productData objectForKey: @"Points"] intValue];
                                lblPoints = [cell viewWithTag: 201];
                                lblPointsData = [cell viewWithTag: 101];
                                
                            }
                                break;
                            case 1:
                            {
                                progressBarView.secondPoints = [[productData objectForKey: @"Points"] intValue];
                                lblPoints = [cell viewWithTag: 202];
                                lblPointsData = [cell viewWithTag: 102];
                            }
                                break;
                            case 2:
                            {
                                progressBarView.thirdPoints = [[productData objectForKey: @"Points"] intValue];
                                lblPoints = [cell viewWithTag: 203];
                                lblPointsData = [cell viewWithTag: 103];
                            }
                                break;
                            default:
                                break;
                        }
                        lblPoints.text = [productData objectForKey: @"GroupName"];
                        lblPointsData.text = [NSString stringWithFormat: @"%@ %@", [productData objectForKey: @"Points"], LOCALIZATION(C_REWARDS_PTS)];     //lokalised
                    }
                }
                else
                {
                    progressBarView.firstPoints = 0;
                    progressBarView.secondPoints = 0;
                    progressBarView.thirdPoints = 0;
                }
                
                [progressBarView reloadBar];
                
                UILabel *lblFooter = [cell viewWithTag: kCAROUSEL_RU_POINTS_FOOTER];
                
                //lookup
                NSDictionary *partnersLookupTable = [[mSession lookupTable] objectForKey: @"PartnersShellClubB2B"];
                NSString *unitString = @"";
                
                if ([[self.carouselPointsDictB2B objectForKey: @"PointsReqToReachMileStone"] intValue] > 0)
                {
                    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat: @"%@%@ ", [self.carouselPointsDictB2B objectForKey: @"PointsReqToReachMileStone"], unitString] attributes:@{NSForegroundColorAttributeName: COLOUR_RED, NSFontAttributeName: FONT_H1, }];
                    
                    NSAttributedString *remainingStringAttr = [[NSAttributedString alloc] initWithString: [self.carouselPointsDictB2B objectForKey: @"SummaryText"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY, NSFontAttributeName: FONT_B3,}];
                    
                    [attrString appendAttributedString: remainingStringAttr];
                    
                    lblFooter.attributedText = attrString;
                }
                else
                {
                    lblFooter.attributedText = [[NSAttributedString alloc] initWithString: [self.carouselPointsDictB2B objectForKey: @"SummaryText"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY, NSFontAttributeName: FONT_B3,}];
                }
                
                
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_RU_POINTS_BUTTON];
                
                if ([[mSession profileInfo] hasMyRewards])
                    btnView.hidden = NO;
                else
                    btnView.hidden = YES;
                [btnView setTitle: LOCALIZATION(C_HOMECAROUSEL_REDEEMPOINTS) forState:UIControlStateNormal];            //lokalised
                btnView.titleLabel.font = FONT_BUTTON;
            }
                
                break;
            case kCAROUSEL_PARTNERS_B2C:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PartnersCell" forIndexPath:indexPath];
                
                UILabel *lblHeader = [cell viewWithTag: kCAROUSEL_PARTNERS_HEADER];
                lblHeader.text = [NSString stringWithFormat:@"%@ (B2C)",LOCALIZATION(C_SIDEMENU_PARTNERS)];                             //lokalised
                
                UILabel *lblSubheader = [cell viewWithTag: kCAROUSEL_PARTNERS_SUBHEADER];
                lblSubheader.text = LOCALIZATION(C_RUSSIA_CAROUSEL_FINALSCORE);                 //lokalised
                
                RUPartnersBarView *partnersBarView = [cell viewWithTag: kCAROUSEL_PARTNERS_PROGRESSBAR];
                [partnersBarView.superview setNeedsDisplay];
                [partnersBarView loadBar: @{@"BusinessType" : @(2),@"Data" : self.carouselPartnerDictB2C}];
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_PARTNERS_BUTTON];
                [btnView setTitle: LOCALIZATION(C_RUSSIA_CAROUSEL_EARNPOINTBTN) forState:UIControlStateNormal];  //loklaised
                btnView.titleLabel.font = FONT_BUTTON;
            }
                break;
                
            case kCAROUSEL_PARTNERSENDED_B2C:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PartnersEndedCell" forIndexPath:indexPath];
                
                UILabel *lblHeader = [cell viewWithTag: 1];
                lblHeader.text = [NSString stringWithFormat:@"%@ (B2C)",LOCALIZATION(C_SIDEMENU_PARTNERS)];                             //lokalised
                
                UILabel *lblThankYou = [cell viewWithTag: 2];
                lblThankYou.text = LOCALIZATION(C_RUSSIA_THANKYOUPARTICIPATION);                //lokalised
                
                UILabel *lblMoreInteresting = [cell viewWithTag: 3];
                lblMoreInteresting.text = LOCALIZATION(C_RUSSIA_MOREINTERESTINGCAMPAIGN);       //lokalised
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_PARTNERS_BUTTON];
                [btnView setTitle: LOCALIZATION(C_RUSSIA_CAROUSEL_EARNPOINTBTN) forState:UIControlStateNormal];     //laoklised
                btnView.titleLabel.font = FONT_BUTTON;
            }
                break;

            case kCAROUSEL_INVENTORYSPEEDOMETER:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SpeedoMeterBarCell" forIndexPath:indexPath];
                UILabel *lblHeader = [cell viewWithTag: kCAROUSEL_CVSPEEDOMETER_HEADER];

                 lblHeader.text = [NSString stringWithFormat: @"%@", [self.inventoryPromotion objectForKey: @"InventoryProgramTitle"]];


                NSString *subHeader = [NSString stringWithFormat: @"%@ \n\n %@ \n\n %@", [self.inventoryPromotion objectForKey: @"InventorySpeedometerDescription1"], [self.inventoryPromotion objectForKey: @"InventorySpeedometerDescription2"],
                    [self.inventoryPromotion objectForKey: @"InventorySpeedometerDescription3"]];

                UILabel *lblSubheader =
                [cell viewWithTag: kCAROUSEL_CVSPEEDOMETER_SUBHEADER];
                lblSubheader.text = subHeader;

                RUSpeedoMeterBarView *partnersBarView = [cell viewWithTag: kCAROUSEL_CVSPEEDOMETER_PROGRESSBAR];
                [partnersBarView.superview setNeedsDisplay];
                [partnersBarView loadBar: @{@"BusinessType" : @(2),@"Data" : self.inventoryPromotion}];

                UIButton *btnView = [cell viewWithTag: kCAROUSEL_PARTNERS_BUTTON];
                btnView.hidden = TRUE;
               }
                break;


            case kCAROUSEL_RU_POINTS_B2C:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RUPointsCell" forIndexPath:indexPath];
                
                    UILabel *lblHeader = [cell viewWithTag: kCAROUSEL_RU_POINTS_HEADER];
                lblHeader.text = [NSString stringWithFormat:@"%@ (B2C)",LOCALIZATION(C_TITLE_MYPOINTS)];                                                    //lokalised
                
                UILabel *lblSubheader = [cell viewWithTag: kCAROUSEL_RU_POINTS_SUBHEADER];
                lblSubheader.text = LOCALIZATION(C_RUSSIA_CAROUSEL_FINALSCORE);                                     //lokalised
                
                RUProgressBarView *progressBarView = [cell viewWithTag: 200];
                
                UILabel *lblMinPoints = [cell viewWithTag: 5];
                
                lblMinPoints.text = [NSString stringWithFormat: @"%@ %@",
                                     @"0", LOCALIZATION(C_REWARDS_PTS)];                                            //lokalised
                
                UILabel *lblMaxPoints = [cell viewWithTag: 6];
                
                lblMaxPoints.text = [NSString stringWithFormat: @"%@ %@", [self.carouselPointsDictB2C objectForKey: @"MaxPoints"], LOCALIZATION(C_REWARDS_PTS)];   //lokalised
                
                progressBarView.maxPoints = [[self.carouselPointsDictB2C objectForKey: @"MaxPoints"] intValue];
                
                if (![[self.carouselPointsDictB2C objectForKey: @"ProductPoints"] isKindOfClass: [NSNull class]])
                {
                    NSArray *productPoints = [self.carouselPointsDictB2C objectForKey: @"ProductPoints"];
                    for (int i = 0; i < [productPoints count]; i++)
                    {
                        NSDictionary *productData = [productPoints objectAtIndex: i];
                        UILabel *lblPoints, *lblPointsData;
                        switch (i)
                        {
                            case 0:
                            {
                                progressBarView.firstPoints = [[productData objectForKey: @"Points"] intValue];
                                lblPoints = [cell viewWithTag: 201];
                                lblPointsData = [cell viewWithTag: 101];
                                
                            }
                                break;
                            case 1:
                            {
                                progressBarView.secondPoints = [[productData objectForKey: @"Points"] intValue];
                                lblPoints = [cell viewWithTag: 202];
                                lblPointsData = [cell viewWithTag: 102];
                            }
                                break;
                            case 2:
                            {
                                progressBarView.thirdPoints = [[productData objectForKey: @"Points"] intValue];
                                lblPoints = [cell viewWithTag: 203];
                                lblPointsData = [cell viewWithTag: 103];
                            }
                                break;
                            default:
                                break;
                        }
                        lblPoints.text = [productData objectForKey: @"GroupName"];
                        lblPointsData.text = [NSString stringWithFormat: @"%@ %@", [productData objectForKey: @"Points"], LOCALIZATION(C_REWARDS_PTS)];     //lokalised
                    }
                }
                else
                {
                    progressBarView.firstPoints = 0;
                    progressBarView.secondPoints = 0;
                    progressBarView.thirdPoints = 0;
                }
                
                [progressBarView reloadBar];
                
                UILabel *lblFooter = [cell viewWithTag: kCAROUSEL_RU_POINTS_FOOTER];
                
                //lookup
                NSDictionary *partnersLookupTable = [[mSession lookupTable] objectForKey: @"PartnersShellClubB2C"];
                NSString *unitString = @"";
                
                if ([[self.carouselPointsDictB2C objectForKey: @"PointsReqToReachMileStone"] intValue] > 0)
                {
                    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat: @"%@%@ ", [self.carouselPointsDictB2C objectForKey: @"PointsReqToReachMileStone"], unitString] attributes:@{NSForegroundColorAttributeName: COLOUR_RED, NSFontAttributeName: FONT_H1, }];
                    
                    NSAttributedString *remainingStringAttr = [[NSAttributedString alloc] initWithString: [self.carouselPointsDictB2C objectForKey: @"SummaryText"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY, NSFontAttributeName: FONT_B3,}];
                    
                    [attrString appendAttributedString: remainingStringAttr];
                    
                    lblFooter.attributedText = attrString;
                }
                else
                {
                    lblFooter.attributedText = [[NSAttributedString alloc] initWithString: [self.carouselPointsDictB2C objectForKey: @"SummaryText"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY, NSFontAttributeName: FONT_B3,}];
                }
                
                
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_RU_POINTS_BUTTON];
                
                if ([[mSession profileInfo] hasMyRewards])
                    btnView.hidden = NO;
                else
                    btnView.hidden = YES;
                [btnView setTitle: LOCALIZATION(C_HOMECAROUSEL_REDEEMPOINTS) forState:UIControlStateNormal];            //lokalised
                btnView.titleLabel.font = FONT_BUTTON;
            }
                
                break;
            case kCAROUSEL_DSRPARTNERS:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DSRPartnersCell" forIndexPath:indexPath];
                
                NSDictionary *cellData = [self.carouselPartnerDict objectForKey:@"PerformanceMetrics"];
                
                UILabel *lblHeader = [cell viewWithTag: kCAROUSEL_RU_POINTS_HEADER];
                lblHeader.text = LOCALIZATION(C_SIDEMENU_PARTNERS);                             //lokalised
                lblHeader.textColor = COLOUR_VERYDARKGREY;
                
                if(![cellData isKindOfClass:[NSNull class]]) {
                    UILabel *lblSubheader = [cell viewWithTag: kCAROUSEL_RU_POINTS_SUBHEADER];
                    lblSubheader.text = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_HOMECAROUSEL_UPDATEDON), [cellData objectForKey: @"LastUpdateDate"]];
                    lblSubheader.textColor = COLOUR_VERYDARKGREY;
                    
                    UILabel *lblFirstKey = [cell viewWithTag: kCAROUSEL_RU_POINTS_FIRSTLABEL];
                    lblFirstKey.text = [cellData objectForKey: @"PerformanceKeyOne"];
                    lblFirstKey.textColor = COLOUR_VERYDARKGREY;
                    UILabel *lblFirstValue = [cell viewWithTag: kCAROUSEL_RU_POINTS_FIRSTPOINTS];
                    lblFirstValue.text = [[cellData objectForKey: @"PerformanceValueOne"] stringValue];
                    
                    UILabel *lblSecondKey = [cell viewWithTag: kCAROUSEL_RU_POINTS_SECONDLABEL];
                    lblSecondKey.text = [cellData objectForKey: @"PerformanceKeyTwo"];
                    lblSecondKey.textColor = COLOUR_VERYDARKGREY;
                    UILabel *lblSecondValue = [cell viewWithTag: kCAROUSEL_RU_POINTS_SECONDPOINTS];
                    lblSecondValue.text = [[cellData objectForKey: @"PerformanceValueTwo"] stringValue];
                    
                    UILabel *lblThirdKey = [cell viewWithTag: kCAROUSEL_RU_POINTS_THIRDLABEL];
                    lblThirdKey.text = [cellData objectForKey: @"PerformanceKeyThree"];
                    lblThirdKey.textColor = COLOUR_VERYDARKGREY;
                    UILabel *lblThirdValue = [cell viewWithTag: kCAROUSEL_RU_POINTS_THIRDPOINTS];
                    lblThirdValue.text = [[cellData objectForKey: @"PerformanceValueThree"] stringValue];
                }
                
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_RU_POINTS_BUTTON];
                
                [btnView setTitle: LOCALIZATION(C_RU_CAROUSEL_VIEWPERFORMANCE) forState:UIControlStateNormal];      //lokalised
                btnView.titleLabel.font = FONT_BUTTON;
            }
                break;
            default:
                break;
        }
    }
    else if (collectionView == self.quickLinksColView)
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QuickLinkCell" forIndexPath:indexPath];
        
        cell.tag = [[self.quickLinksEnumArray objectAtIndex: indexPath.row] intValue];
        
        NSDictionary *cellData = [self.quickLinksArray objectAtIndex: indexPath.row];
        
        UIImageView *cellImgView = [cell viewWithTag: 1]; //tagged in storyboard
        cellImgView.image = [UIImage imageNamed: [cellData objectForKey: @"Icon"]];
        
        UILabel *cellLbl = [cell viewWithTag: 2]; //tagged in storyboard
        cellLbl.font = FONT_B(12);
        cellLbl.textColor = COLOUR_MIDGREY;
        cellLbl.text = [cellData objectForKey: @"Label"];
        [cellLbl sizeToFit];
        [cellLbl setTextAlignment:NSTextAlignmentCenter];
        
        //if not last column, show right line
        UIView *rightLine = [cell viewWithTag: 11];
        switch (indexPath.row % 4)
        {
            case 3: //last row
                rightLine.hidden = YES;
                break;
            default:
                rightLine.hidden = NO;
                break;
        }
        
        //if not last row, show bottom line
        UIView *bottomLine = [cell viewWithTag: 10];
        //check for remainder
        if ([self.quickLinksArray count] % 4 == 0) //perfect multiples of 3 row
        {
            //indexpath starts 0, count must - 1
            //check if last 3, hide if true
            //row index has to be more than count - 3
            if (indexPath.row > abs([self.quickLinksArray count] - 1) - 4)
                bottomLine.hidden = YES;
            else
                bottomLine.hidden = NO;
        }
        else
        {
            //indexpath starts 0, count must - 1
            //check if last row (remainder), hide if true
            //row index has to be more than total count - remainder
            if (indexPath.row >= [self.quickLinksArray count] - ([self.quickLinksArray count] % 4))
            {
                //last row
                bottomLine.hidden = YES;
                
                //total quick link only 1 element OR
                //last element is middle element
                if ([self.quickLinksArray count] == 1)
                    rightLine.hidden = YES;
                else if (indexPath.row % 4 == 1 &&  [self.quickLinksArray count] < 4)
                    rightLine.hidden = YES;
            }
            else
                bottomLine.hidden = NO;
            
            
        }
    }
    else if (collectionView == self.categoryHeaderCollectionView)
    {
        NSInteger actualIndex;
        if (kIsRightToLeft) {
            actualIndex = self.homeBottomArray.count-indexPath.row - 1;
        } else {
            actualIndex = indexPath.row;
        }
        cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"CategoryCell" forIndexPath:indexPath];
        
        UILabel *lblTitle = [cell viewWithTag: 1];
        NSDictionary *categoryDict = [self.homeBottomArray objectAtIndex: actualIndex];
        lblTitle.text = [categoryDict objectForKey: @"Title"];
        lblTitle.font = FONT_H2;
//        [lblTitle sizeToFit];
        
        UIView *bottomLine = [cell viewWithTag: 2];
        if (actualIndex == self.selectedIndex)
        {
            lblTitle.textColor = COLOUR_RED;
            [bottomLine setBackgroundColor:COLOUR_RED];
            bottomLine.hidden = NO;
        }
        else
        {
            lblTitle.textColor = COLOUR_VERYDARKGREY;
            [bottomLine setBackgroundColor:COLOUR_RED];
            bottomLine.hidden = YES;
        }
        
        [self.categoryCellArray addObject: cell];
    }
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath: indexPath];
    if (collectionView == self.carouselColView)
    {
      
    }
    else if (collectionView == self.quickLinksColView)
    {
        switch (cell.tag)
        {
            case kQUICKLINK_DSR_INVENTORYMANAGEMENT:
                [mSession loadDSRInventoryManagement];
                break;
            case kQUICKLINK_TO_INVENTORYMANAGEMENT:
                [mSession loadTOInventoryManagement];
                break;
            case kQUICKLINK_DSR_HASINVENTORYMANAGEMENT:
                  [mSession loadDSRInventoryManagement];
                break;
            case kQUICKLINK_TO_HASINVENTORYMANAGEMENT:
                 [mSession loadTOInventoryManagement];
                break;
            case kQUICKLINK_MEC_HASINVENTORYMANAGEMENT:
                 [mSession loadTOInventoryManagement];
                break;
            case kQUICKLINK_MYNETWORK:
                [mSession loadManageWorkshopView];
                //[mSession loadMyNetworkView];
                break;
            case kQUICKLINK_DSRWORKSHOPPERFORMANCE:
//                [mSession loadPerformanceView];
                [mSession loadINPerformance];
                break;
            case kQUICKLINK_OILCHANGEWALKTHROUGH:
                [mSession pushWalkthroughView:self WithType:WALKTHROUGH_OilChange];
//                [mSession loadScanProductView];
                break;
            case kQUICKLINK_OILCHANGEREGISTRATION:
                [mSession loadScanProductView];
                break;
                
            case kQUICKLINK_DSRMANAGEWORKSHOP:
                [mSession loadManageWorkshopView];
                break;
            case kQUICKLINK_DSRMANAGEORDERS:
                [mSession loadManageOrder];
                break;
            case kQUICKLINK_MYLIBRARY:
                [mSession loadMyLibraryView];
                break;
            case kQUICKLINK_SPITAX:
                [mSession loadSpiraxWorkshopView];
                break;

            case kQUICKLINK_SEARCHVEHICLEID:
                [mSession loadSearchVehicleView];
                break;
            case kQUICKLINK_MYREWARDS:
                [mSession loadRewardsView];
                break;
                
            case kQUICKLINK_CASHINCENTIVES:
                [mSession loadCashIncentiveView];
                break;
            
            case kQUICKLINK_TRADEMANAGEORDERS:
                [mSession loadManageOrder];
                break;
            case kQUICKLINK_REGISTERPRODUCTCODE:
                [mSession loadScanProductView];
                break;
            case kQUICKLINK_CUSTOMERREDEMPTION:
                [mSession loadPromoCodeMasterView];
                break;
            //don't need to duplicate search vehicle ID
            case kQUICKLINK_SCANDECAL:
//                [mQRCodeScanner showZBarScannerOnVC: self];
                [mSession loadSearchVehicleViewWithDecal: nil];
                break;
            case kQUICKLINK_INVITECUSTOMER:
//                [mSession loadInviteCustomerView];
                
                [mSession loadRegisterCustomerView: @{@"PromoCode" : @"",
                                                      @"VehicleNumber": @"",
                                                      @"VehicleStateID": @"",
                                                      
                                                      }];
                break;
            //don't need to duplicate my library
                
            case kQUICKLINK_WORKSHOPPROFILING:
                [mSession loadWorkshopProfilingView];
                break;
                
            case kQUICKLINK_ACTIVATEDECAL:
                [mSession loadDecalView];
                break;
            case kQUICKLINK_MYPERFORMANCE:
//                [mSession loadPerformanceView];
                [mSession loadINPerformance];
                break;
            case kQUICKLINK_PARTICIPATINGPRODUCT:
                [mSession loadParticipatingProduct];
                break;
            case kQUICKLINK_IN_MANAGEMECHANICS:
                [mSession loadManageWorkshopView];
                break;
            case kQUICKLINK_IN_MECHPERFORMANCE:
//                [mSession loadPerformanceView];
                [mSession loadINPerformance];
                break;
            case kQUICKLINK_LOYALTYREWARD:
                [mSession loadLoyaltyContract];
                break;
            case kQUICKLINK_WORKSHOPOFFERS:
                [mSession loadWorkshopOfferView];
                break;
            case kQUICKLINK_SDA:
                [mSession loadSDA:self];
                break;
            case kQUICKLINK_SWA:
                [mSession loadSWA:self];
                break;
            case kQUICKLINK_PARTNERSOFSHELL:
                [mSession loadRUPartners];
                break;
            case kQUICKLINK_RU_WORKSHOPPERFORMANCE:
                [mSession loadRUWorkshopPerformance];
                break;
            case kQUICKLINKS_SCANBOTTLE:
                [mSession loadScanBottle];
                break;
            case kQUICKLINK_LUBEMATCH:
                [mSession loadLubeMatch];
                break;
            case kQUICKLINK_IN_MANAGEMYSTAFF:
                [mSession loadManageMechanic];
                break;
            default:
                break;
        }
    } else if(collectionView == self.categoryHeaderCollectionView) {
        NSInteger actualIndex;
        if (kIsRightToLeft) {
            actualIndex = self.homeBottomArray.count - indexPath.row - 1;
        } else {
            actualIndex = indexPath.row;
        }
        for (UICollectionViewCell *categoryCell in self.categoryCellArray)
        {
            UIView *bottomLine = [categoryCell viewWithTag: 2];
            bottomLine.hidden = YES;
            
            UILabel *lblTitle = [categoryCell viewWithTag: 1];
            lblTitle.textColor = COLOUR_VERYDARKGREY;
            bottomLine.hidden = YES;
        }
        
        UICollectionViewCell *selectedCell = [collectionView cellForItemAtIndexPath: indexPath];
        UILabel *lblTitle = [selectedCell viewWithTag: 1];
        lblTitle.textColor = COLOUR_RED;
        
        UIView *bottomLine = [selectedCell viewWithTag: 2];
        [bottomLine setBackgroundColor:COLOUR_RED];
        bottomLine.hidden = NO;
        
        self.selectedIndex = actualIndex;
        
        [self.categoryTableView reloadData];
    }
}

#pragma mark - TableView Delegate & DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return self.numberOfCategory > 0 ? self.numberOfCategory - 1 : 0;
    switch (self.selectedIndex)
    {
        case 0: //all. total count should not include "all"
            if([self.homeBottomArray count] == 1){
                return 1;
            } else {
                return [self.homeBottomArray count] - 1;
            }
            break;
        default: //every category by default should only have one row
            return 1;
            break;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    NSDictionary *dataDict = [self.homeBottomArray objectAtIndex: self.selectedIndex];
    
    if ([[dataDict objectForKey: @"Title"] isEqualToString: LOCALIZATION(C_HOME_ALL)])
    {
        if([self.homeBottomArray count] > 1) {
            dataDict = [self.homeBottomArray objectAtIndex: indexPath.row + 1];
        }
    }
    
    {
        NSInteger cellType = [[dataDict objectForKey: @"Type"] intValue];
        switch (cellType)
        {
            case HOMEBOTTOMVIEW_IMAGEWITHLABEL:
            {
                //promotion
                HomePromoTableViewCell *promoCell = [tableView dequeueReusableCellWithIdentifier: @"PromotionsTableViewCell"];
                promoCell.lblHeader.text = [dataDict objectForKey: @"Title"];
                promoCell.dataArray = [dataDict objectForKey: @"Data"];
                promoCell.cellIndex = indexPath.row;
                promoCell.delegate = self;

                [promoCell reloadCellData];
                
                if(IS_COUNTRY(COUNTRYCODE_INDIA)) {
                    UIPinchGestureRecognizer *gesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(zoomingImage:)];
                    [promoCell addGestureRecognizer:gesture];
                }
                
                return promoCell;
            }
                break;
            case HOMEBOTTOMVIEW_REWARDS:
            {
                HomePromoTableViewCell *promoCell = [tableView dequeueReusableCellWithIdentifier: @"RewardsTableViewCell"];
                promoCell.lblHeader.text = [dataDict objectForKey: @"Title"];
                promoCell.dataArray = [dataDict objectForKey: @"Data"];
                promoCell.cellIndex = indexPath.row;
                promoCell.delegate = self;
                
                if(IS_COUNTRY(COUNTRYCODE_INDIA)) {
                    UIPinchGestureRecognizer *gesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(zoomingImage:)];
                    [promoCell addGestureRecognizer:gesture];
                }
                
                [promoCell reloadCellData];
                return promoCell;
            }
                break;
            case HOMEBOTTOMVIEW_NODATA:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"EmptySectionTableViewCell"];
                UILabel *lblNoData = [cell viewWithTag:1001];
                lblNoData.text = LOCALIZATION(C_HOME_EMPTYSECTION);
                lblNoData.textColor = COLOUR_LIGHTGREY;
                lblNoData.font = FONT_B1;
            }
                break;
            default:
                break;
        }
    }
    return cell;
}

-(void) didSelectViewAll:(NSUInteger)index
{
    self.reloadFlag = NO;
    
    if([[self.homePromotionComponentArray objectAtIndex:index] isEqualToString:@"Rewards"]) {
        [mSession loadRewardsView];
    } else {
        [mSession pushHomePromotionsWithSelectedIndex:index onVc:self];
    }
}

-(void) didSelectCell:(NSUInteger)index selectedData:(NSDictionary *)data {
    
    self.reloadFlag = NO;
    
    if(index == 0 && [[self.homePromotionComponentArray objectAtIndex:0] isEqualToString:@"Promotion"]) {
        [mSession pushPromoDetailsWith:data andIsPromotion:YES onVc:self];
    }
    else if([[self.homePromotionComponentArray objectAtIndex:index] isEqualToString:@"Rewards"]) {
        [[WebServiceManager sharedInstance] fetchRewardsDetails:[data objectForKey: @"CategoryId"]
        itemCode:[data objectForKey: @"RefId"]
              vc:self];
    }
    else {
        [mSession pushPromoDetailsWith:data andIsPromotion:NO onVc:self];
    }
}


#pragma mark - WebService Delegate
- (void)processCompleted:(WebServiceResponse *)response {
    NSLog(@"success processCompleted ---xxxx--- %@", [response getJSON]);
    switch(response.webserviceCall) {
        case kWEBSERVICE_FETCHHOME: {
            SET_HOMEDATA([response getGenericResponse]);
            [self setFetchHomeNewData];
            //Configuring ShortCutItems
            [mSession configureShortcutItems:YES];
        }
            break;
        
      case kWEBSERVICE_GAMIFICATION_HELIX_VIEWDETAILS: {
        
        self.tradesPromotion = [GET_HOMEDATA objectForKey: @"TradesPromotion"];
        
        NSString *title = self.tradesPromotion[@"TPCSHeader"];
        NSString *htmlString = [response getJSON][@"Data"][@"Message"];
        
        [mSession pushGenericWebViewWith:title andHtmlString:htmlString onVC:self];
        
      }
        break;

        case kWEBSERVICE_FETECHTRADESPEEDOMETERDETAILS: {

            self.pilotPromotionDictionary = [response getJSON][@"Data"];
            [self.quickLinksColView reloadData];

        }

        case kWEBSERVICE_LOYALTY_WORKSHOPPERFORMANCE: {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
               _loyaltyPerformanceCarouselDetails = [response getGenericResponse];
                
                SET_PROGRAMBREAKDOWN(_loyaltyPerformanceCarouselDetails);
                
                [_carouselColView reloadData];
//                if(![[mSession profileInfo] hasCSHasTradePromotion]) {
//                    [self.carouselColView performBatchUpdates:^{}
//                                                   completion:^(BOOL finished) {
//                                                       /// collection-view finished reload
//                                                       [self performSelector:@selector(scrollPages) withObject:nil afterDelay:self.timerInterval];
//                                                   }];
//                    }
                }
            }
                break;
                
            case kWEBSERVICE_PROMOTION_REDEEMTRADEPROMOTION: {
                if([[[response getJSON] objectForKey:@"ResponseCode"] intValue] == 5000) {
                    self.isPromotionRedeemed = YES;
                    NSDictionary *data = [[response getJSON] objectForKey:@"Data"];
                    [self.promotionDictionary setObject:[data objectForKey:@"RedeemMessage"] forKey:@"RedeemMessage"];
                    [self.promotionDictionary setObject:[data objectForKey:@"TPImageUrl"] forKey:@"TPImageUrl"];
                    [self.promotionDictionary setObject:[data objectForKey:@"TPFooter"] forKey:@"TPCSFooter"];
                    [self.carouselColView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathWithIndex:0]]];
                } else {
                    [self popUpViewWithTitle:[[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Header"] content:[[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Message"]];
                }
            }
                break;
        case kWEBSERVICE_FETCHHOMEPROMOTION:
            break;
        case kWEBSERVICE_FETCHREWARDSFORHOME: {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                SET_REWARDS_CANREDEEM([[[response getGenericResponse] objectForKey:@"CanRedeem"] boolValue]);
                NSArray *rewardsRespArray = [[response getGenericResponse] objectForKey:@"RewardsBanner"];
                
                if(![rewardsRespArray isKindOfClass:[NSNull class]] && [rewardsRespArray count] > 0) {
                    NSMutableDictionary *rewardsDict = [[NSMutableDictionary alloc] init];
                    [rewardsDict setObject: LOCALIZATION(C_REWARDS_FEATUREDREWARDS) forKey: @"Title"];

                    [rewardsDict setObject: rewardsRespArray forKey: @"Data"];
                    
                    [rewardsDict setObject: @(HOMEBOTTOMVIEW_REWARDS) forKey: @"Type"];
                    
                    if([self.homeBottomArray count] == 0) {
                        NSMutableDictionary *allDict = [[NSMutableDictionary alloc] init];
                        [allDict setObject: LOCALIZATION(C_HOME_ALL) forKey: @"Title"];
                        [self.homeBottomArray insertObject: allDict atIndex: 0];
                    }
                    
                    [self.homeBottomArray addObject: rewardsDict];
                    [self.homePromotionComponentArray addObject:@"Rewards"];
                    [self homePageBottomViewSetup];
                }
            }
        }
            break;
        case kWEBSERVICE_REWARDSFETCHDETAILS:
        {
            [mSession pushRewardsItemDetailsView: [response getGenericResponse] vc:self];
        }
                break;
        case kWEBSERVICE_TRADE_EARNNREDEEM:
        {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                self.earnNRedeemData = [[NSDictionary alloc] initWithDictionary:[response getGenericResponse]];
                [self.carouselColView reloadData];
            }
        }
            break;
        case kWEBSERVICE_LOYALTY_TH_LOADTIERPOINTS:
        {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                self.thTierPointsData = [[THLoyaltyTierPointsModel alloc] initWithData:[response getGenericResponse]];
                [self.carouselColView reloadData];
            }
        }
            break;
        case kWEBSERVICE_GAMIFICATION_VIEWDETAILS:
        case kWEBSERVICE_GAMIFICATION_CHECKIN_VIEWDETAILS:
        {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                    [mSession pushPromotionViewDetailsWithData:@{
                        @"Title" : [[GET_HOMEDATA objectForKey:@"TradesPromotion"] objectForKey:@"TPCSHeader"],
                        @"Data"  : [[response getGenericResponse] objectForKey:@"Message"],
                    } onVc:self];
            }
        }
            break;
        case kWEBSERVICE_FETCHCOMPANYTIER: {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                self.tierDetails = [[NSDictionary alloc] initWithDictionary:[response getGenericResponse]];
                [self.carouselColView reloadData];
            }
        }
            break;
        default:
            break;
    }
}

enum HOMEBOTTOMVIEW_TYPES
{
    HOMEBOTTOMVIEW_IMAGEWITHLABEL = 0,
    HOMEBOTTOMVIEW_REWARDS,
    HOMEBOTTOMVIEW_NODATA,
};

-(void)processFailed:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_FETCHHOME:
        {
            if(GET_HOMEDATA)
            {
                [self setFetchHomeNewData];
            }
        }
            break;
        case kWEBSERVICE_FETCHREWARDSFORHOME: {
            [self homePageBottomViewSetup];
        }
            break;
        default:
            break;
    }
}

#pragma mark - WebView Navigation Delegate
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    if(navigationAction.navigationType ==  WKNavigationTypeLinkActivated) {
        [[UIApplication sharedApplication] openURL: navigationAction.request.URL];
        
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
    return;
}


@end
