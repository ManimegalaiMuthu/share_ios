//
//  ScanCode_DecalMainViewController.m
//  Shell
//
//  Created by Jeremy Lua on 25/5/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "ScanCode_DecalMainViewController.h"
#import "QRCodeScanner.h"

@interface ScanCode_DecalMainViewController () <UITableViewDataSource,UITableViewDataSource, QRCodeScannerDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, WebServiceManagerDelegate>

@property (strong, nonatomic) IBOutlet UIButton *backBtn;


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtCode;

@property (weak, nonatomic) IBOutlet UIButton *btnScan;
@property (weak, nonatomic) IBOutlet UIButton *btnManual;
@property (weak, nonatomic) IBOutlet UILabel *lblDecalMainContent;

@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnAddCode;

@property (weak, nonatomic) IBOutlet UILabel *txtNo;
@property (weak, nonatomic) IBOutlet UILabel *txtScannedCode;
@property (weak, nonatomic) IBOutlet UILabel *txtStatus;
@property (weak, nonatomic) IBOutlet UILabel *txtDelete;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property NSMutableArray *arrScanCode;

@property NSString *lastScannedCodeString;


@end

@implementation ScanCode_DecalMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.arrScanCode = [[NSMutableArray alloc] init];
    [self.tableView registerNib:[UINib nibWithNibName:@"ScanProductTableViewCell" bundle:nil] forCellReuseIdentifier:@"ScanProductTableViewCell"];
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_DECAL_ACTIVATEDECAL) subtitle: @"" size: 15 subtitleSize: 0];       //lokalise 24 Jan
    
    if (kIsRightToLeft) {
        [self.txtCode setTextAlignment:NSTextAlignmentRight];
        [self.txtNo setTextAlignment:NSTextAlignmentRight];
        [self.txtScannedCode setTextAlignment:NSTextAlignmentRight];
        [self.txtStatus setTextAlignment:NSTextAlignmentRight];
        [self.lblDecalMainContent setTextAlignment:NSTextAlignmentRight];
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.txtCode setTextAlignment:NSTextAlignmentLeft];
        [self.txtNo setTextAlignment:NSTextAlignmentLeft];
        [self.txtScannedCode setTextAlignment:NSTextAlignmentLeft];
        [self.txtStatus setTextAlignment:NSTextAlignmentLeft];
        [self.lblDecalMainContent setTextAlignment:NSTextAlignmentLeft];
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
    
    [mQRCodeScanner setDelegate: self];
    // Do any additional setup after loading the view.
    [self setupInterface];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_DECAL_ACTIVATEDECAL) screenClass:nil];
}

-(void) setupInterface
{
    self.lblDecalMainContent.font = FONT_B1;
    self.lblDecalMainContent.text = LOCALIZATION(C_DECAL_TAGTOWORKSHOP);            //lokalise 24 Jan
    
    
    [self.btnScan.titleLabel setFont: FONT_BUTTON];
    [self.btnScan setBackgroundColor: COLOUR_YELLOW];
    [self.btnScan setTitle:LOCALIZATION(C_DECAL_SCANDECAL) forState:UIControlStateNormal];  //lokalise 24 Jan
    [self.btnScan setTitleColor: COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    
    [self.btnManual.titleLabel setFont: FONT_BUTTON];
    [self.btnManual setBackgroundColor: COLOUR_YELLOW];
    [self.btnManual setTitle:LOCALIZATION(C_DECAL_ENTERDECAL) forState:UIControlStateNormal]; //lokalise 24 Jan
    [self.btnManual setTitleColor: COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    
    [self.btnCancel.titleLabel setFont: FONT_BUTTON];
    [self.btnCancel setBackgroundColor: COLOUR_YELLOW];
    [self.btnCancel setTitleColor: COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    
    self.txtCode.font = FONT_B1;
    self.txtCode.placeholder = LOCALIZATION(C_FORM_ENTERCODETITLE);                 //lokalise 24 Jan
    self.lblTitle.font = FONT_B1;
    
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.txtNo.font = FONT_B1;
    self.txtNo.textColor = COLOUR_VERYDARKGREY;
    self.txtNo.text = LOCALIZATION(C_TABLE_NO);                                     //lokalise 24 Jan
    
    self.txtScannedCode.font = self.txtNo.font;
    self.txtScannedCode.textColor = self.txtNo.textColor;
    self.txtScannedCode.text= LOCALIZATION(C_TABLE_SCANNEDCODE);                   //lokalise 24 Jan
    if ([GET_LOCALIZATION isEqualToString: kChinese])
        self.txtScannedCode.textAlignment = NSTextAlignmentCenter;
    
    self.txtStatus.font = self.txtNo.font;
    self.txtStatus.textColor = self.txtNo.textColor;
    self.txtStatus.text = LOCALIZATION(C_STATUS);                                   //lokalised
    
    self.txtDelete.font = self.txtNo.font;
    self.txtDelete.textColor = self.txtNo.textColor;
    self.txtDelete.text = LOCALIZATION(C_DELETE);                                    //lokalise 24 Jan
    
    self.lblTitle.font = FONT_H1;
    self.lblTitle.text = LOCALIZATION(C_FORM_ENTERCODETITLE);                       //lokalise 24 Jan
    self.txtCode.font = FONT_B1;
    self.btnAddCode.titleLabel.font = FONT_BUTTON;
    [self.btnAddCode setTitle: LOCALIZATION(C_FORM_ADDCODE) forState:UIControlStateNormal]; //lokalise 24 Jan
    self.btnAddCode.backgroundColor = COLOUR_RED;
    
    self.btnCancel.titleLabel.font = FONT_BUTTON;
    [self.btnCancel setTitle:LOCALIZATION(C_FORM_CANCEL) forState:UIControlStateNormal];  //lokalised
    self.btnCancel.backgroundColor = COLOUR_YELLOW;
    
    
    self.btnSubmit.titleLabel.font = FONT_BUTTON;
    [self.btnSubmit setTitle:LOCALIZATION(C_DECAL_SUBMITDECAL) forState:UIControlStateNormal];  //lokalise 24 Jan
    [self.btnSubmit setBackgroundColor:COLOUR_RED];
    
//    if(GET_PROFILETYPE == kPROFILETYPE_DSR)
//    {
//        self.btnSubmit.hidden = YES;
//    }
}
- (IBAction)submitPressed:(id)sender {
    
    if ([self.arrScanCode count] == 0)
    {
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_ERROR)];     //lokalise 24 Jan
        return;
    }
    
    
    [[WebServiceManager sharedInstance] tagDecalCodeToTradeOwner: self.arrScanCode vc: self];
}

- (IBAction)scanPressed:(id)sender {
    self.lastScannedCodeString = @"";
    [mQRCodeScanner showDecalScanner: self startCount: [self.arrScanCode count]];
}

#define DELAY_SECONDS 1
-(void)qrCodeScanSuccess:(NSString *)scanString
{
    if ([self.lastScannedCodeString isEqualToString: scanString])
        return;
    else
        self.lastScannedCodeString = scanString;
    
    if ([mAlert isAlertViewActive])
        return;
    
    if ([self.arrScanCode count] > 50)
        return;
    
    for (NSDictionary *dict in self.arrScanCode)
    {
        if ([scanString isEqualToString: [dict objectForKey: @"Codes"]])
        {
            [mAlert setIsAlertViewActive: YES];
            [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_EXIST) onCompletion:^(BOOL finish) {     //lokalise 24 Jan
//                [mAlert setIsAlertViewActive: NO];
                [mAlert performSelector: @selector(setIsAlertViewActive:) withObject:@(NO) afterDelay: DELAY_SECONDS];
            }];
            return;
        }
    }
    NSString *scannedCodeString;
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDONESIA])
    {
        scannedCodeString = scanString;
    }
    else
    {
        
        scannedCodeString = [[scanString componentsSeparatedByString:@"="] lastObject];
    }
    
    [self.arrScanCode addObject: @{@"Codes": scannedCodeString,}];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:KEY_WINDOW animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.detailsLabelText = LOCALIZATION(C_GLOBAL_SUCCESS);          //lokalised
    hud.detailsLabelFont = FONT_B1;
    hud.removeFromSuperViewOnHide = YES;
    hud.margin = 25.f;
    [hud hide:YES afterDelay:DELAY_SECONDS];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"addSuccessCount" object:self];
    
    
    [mAlert setIsAlertViewActive: YES];
    [mAlert performSelector: @selector(setIsAlertViewActive:) withObject:@(NO) afterDelay:DELAY_SECONDS];
}

-(void)qrCodeScanCancelled
{
    [self.tableView reloadData];
}

- (IBAction)backPressed:(id)sender {
    //only available prelogin
    [self popSelf];
}

- (IBAction)cancelPressed:(id)sender {
    [self showPopUp:NO];
    [self clearTextField];
    
}

- (IBAction)manualPressed:(id)sender {
    [self.txtCode becomeFirstResponder];
    [self showPopUp:YES];
}

- (IBAction)addCodePressed:(id)sender {
    
    for (NSDictionary *dict in _arrScanCode)
    {
        if ([self.txtCode.text isEqualToString: [dict objectForKey: @"Codes"]])
        {
            [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_EXIST)];         //lokalise 24 Jan
            return;
        }
    }
    
    [self showPopUp:NO];
    
    if(self.txtCode.text.length > 0)
    {
        [self.arrScanCode addObject: @{@"Codes": self.txtCode.text,}];
    }
    [self clearTextField];
    [self.tableView reloadData];
}

-(void)clearTextField
{
    self.txtCode.text = @"";
}

- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer{
    [self showPopUp:NO];
}

-(void)showPopUp:(Boolean)value
{
    self.popUpView.hidden = !value;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arrScanCode.count;
}

-(void)deletePressed:(UIButton*)sender
{
    [_arrScanCode removeObjectAtIndex:sender.tag];
    [_tableView reloadData];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ScanProductTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ScanProductTableViewCell"];
    NSDictionary *codeStringDict = [_arrScanCode objectAtIndex:indexPath.row];
    
    [cell setID: @(indexPath.row + 1).stringValue];
    [cell setScannedCode: [codeStringDict objectForKey: @"Codes"]];
    [cell setPendingStatus: LOCALIZATION(C_SCANCODE_STATUS_PENDING)];               //lokalise 23 Jan
    
    cell.btnDelete.tag = indexPath.row;
    [cell.btnDelete setTitle: LOCALIZATION(C_DELETE) forState: UIControlStateNormal];       //lokalise 24 Jan
    [cell.btnDelete addTarget: self action: @selector(deletePressed:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_TAGDECALTOTRADEOWNER:
        {
            [mSession pushDecalResultsWithDictionary: [response getGenericResponse] parentController:self];
            
            [self.arrScanCode removeAllObjects];
            [self.tableView reloadData];
            //            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
            //
            //            }];
        }
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
