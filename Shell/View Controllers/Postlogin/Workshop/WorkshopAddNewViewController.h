//
//  WorkshopAddNewViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 20/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "TradeRegistrationViewController.h"
#import "BaseVC.h"

@protocol WorkshopAddNewDelegate <NSObject>
-(void)didRegisterWorkshop;
@end

@interface WorkshopAddNewViewController: BaseVC
@property id<WorkshopAddNewDelegate> delegate;
-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict;
@end
