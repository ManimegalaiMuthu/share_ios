//
//  MyNetworkAddNewViewController.m
//  Shell
//
//  Created by Ben on 27/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyNetworkAddNewViewController.h"

@interface MyNetworkAddNewViewController()

@property (weak, nonatomic) IBOutlet UILabel* mainLabel;
@property (weak, nonatomic) IBOutlet UILabel* workshopLabel;
@property (weak, nonatomic) IBOutlet UILabel* retailerLabel;

@end

@implementation MyNetworkAddNewViewController

- (void)viewDidLoad {
    self.mainLabel.font = FONT_BUTTON;
    self.mainLabel.text = LOCALIZATION(C_DROPDOWN_SELECTCUSTOMERTYPE);      //lokalised
    self.mainLabel.textColor = COLOUR_VERYDARKGREY;
    self.workshopLabel.font = FONT_B1;
    self.workshopLabel.text = LOCALIZATION(C_MANAGEWORKSHOP_IN_WORKSHOP);   //lokalise 8 Feb
    self.workshopLabel.textColor = COLOUR_DARKGREY;
    self.retailerLabel.font = FONT_B1;
    self.retailerLabel.text = LOCALIZATION(C_MANAGEWORKSHOP_IN_RETAILER);   //lokalise 8 Feb
    self.retailerLabel.textColor = COLOUR_DARKGREY;
}

-(IBAction)workshopPressed:(id)sender {
    [mSession pushINWorkshopAddNewView:self];
}

-(IBAction)retailerPressed:(id)sender {
    self.retailerLabel.text = @"Pressed";
}

@end
