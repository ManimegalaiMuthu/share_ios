//
//  WorkshopListTableViewCell.h
//  Shell
//
//  Created by Ankita Chhikara on 15/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface WorkshopListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *imgWorkshopStatus;

@end
