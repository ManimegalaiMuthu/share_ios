//
//  WorkshopListTableViewCell.m
//  Shell
//
//  Created by Ankita Chhikara on 15/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "WorkshopListTableViewCell.h"

@interface WorkshopListTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;

@end


@implementation WorkshopListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.lblName.font = FONT_B1;
    
    if (kIsRightToLeft)
    {
        [self.imgArrow setImage: [UIImage imageNamed: @"icon-arrsingleL.png"]];
        self.lblName.textAlignment = NSTextAlignmentRight;
    }
    else
    {
        [self.imgArrow setImage: [UIImage imageNamed: @"icon-arrsingleR.png"]];
        self.lblName.textAlignment = NSTextAlignmentLeft;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
