//
//  ManageWorkshopViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 25/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "RDVTabBarItem.h"
#import "ManageWorkshopTabViewController.h"
#import "WorkshopListViewController.h"
#import "WorkshopAddNewViewController.h"
#import "WorkingHoursViewController.h"
#import "INMechanicAddNewViewController.h"
#import "AddNewWorkshop_NewViewController.h"
#import "RUWorkshopRegistrationViewController.h"

@interface ManageWorkshopTabViewController () <WorkingHoursViewDelegate, WorkshopAddNewDelegate, AddNewWorkshop_NewDelegate>
{
    WorkshopListViewController *workshopListViewController;
    
    //    WorkshopAddNewViewController *workshopAddNewViewController;
    
    INMechanicAddNewViewController *imAddNewViewController; //IM Add new controller
    
    AddNewWorkshop_NewViewController *addNewWorkshop_NewViewController;
    RUWorkshopRegistrationViewController *ruWorkshopRegistrationView;
}


@property NSMutableArray *workshopListArray;


@end

@implementation ManageWorkshopTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupInterface];
    [UpdateHUD removeMBProgress: KEY_WINDOW];
}


- (void) setupInterface
{
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        if (GET_PROFILETYPE == kPROFILETYPE_DSR)
            [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_MYNETWORK) subtitle:@""];         //lokalise 28 Jan
        else if ((GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER) && [[[mSession loadUserProfile] companyType] isEqualToString:@"16"])
            [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_MYNETWORK) subtitle:@""];
        else
            [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_IN_MANAGEMYMECHANICS) subtitle: @""];  //lokalise 28 Jan
    }
    else
    {
        [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_MANAGEWORKSHOP) subtitle: @""];      //lokalise 28 Jan
    }
    
    workshopListViewController  = [STORYBOARD_MANAGEWORKSHOP instantiateViewControllerWithIdentifier: VIEW_WORKSHOPLIST];
    UINavigationController *workshopListNav = [[UINavigationController alloc] initWithRootViewController: workshopListViewController];
    [workshopListNav setNavigationBarHidden: YES];
    [workshopListViewController view];
    
    NSArray *viewControllers;
    NSArray *title;
    //same VC but different data.
    //selecttab function have been tweaked locally for this case
    if ([[mSession profileInfo] hasAddNewWorkshop])
    {
        UINavigationController *workshopAddNewNav;

        addNewWorkshop_NewViewController  = [STORYBOARD_MANAGEWORKSHOP instantiateViewControllerWithIdentifier: VIEW_ADDNEWWORKSHOP_NEW];
        workshopAddNewNav = [[UINavigationController alloc] initWithRootViewController: addNewWorkshop_NewViewController];
        
        [workshopAddNewNav setNavigationBarHidden: YES];
        [addNewWorkshop_NewViewController view];
        addNewWorkshop_NewViewController.delegate = self;

        if (IS_COUNTRY(COUNTRYCODE_INDIA))
        {
            if (GET_PROFILETYPE == kPROFILETYPE_DSR || (GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER && [[[mSession loadUserProfile] companyType] isEqualToString:@"16"]))
            {
                UIViewController* networkAddNewVC = [STORYBOARD_MANAGEWORKSHOP instantiateViewControllerWithIdentifier: @"MyNetworkAddNew"];

                if([[mSession profileInfo] hasPendingWorkshopList]) {
                    viewControllers = @[workshopListNav,
                                        workshopListNav,
                                        networkAddNewVC
                                        ];
                    title = @[LOCALIZATION(C_MANAGEWORKSHOP_PENDING),       //lokalise 28 Jan
                              LOCALIZATION(C_MANAGEWORKSHOP_APPROVE),       //lokalise 28 Jan
                              LOCALIZATION(C_MANAGEWORKSHOP_ADDNEW)         //lokalise 28 Jan
                            ];
                } else {
                    viewControllers = @[
                                        workshopListNav,
                                        networkAddNewVC
                                        ];
                    title = @[
                              LOCALIZATION(C_MANAGEWORKSHOP_APPROVE),       
                              LOCALIZATION(C_MANAGEWORKSHOP_ADDNEW)         
                    ];
                }
            }
            else
            {
                imAddNewViewController = [STORYBOARD_MANAGEWORKSHOP instantiateViewControllerWithIdentifier: VIEW_INMECHANIC_ADDNEW];
                UINavigationController *imAddNewNav = [[UINavigationController alloc] initWithRootViewController: imAddNewViewController];
                [imAddNewNav setNavigationBarHidden: YES];
                [imAddNewViewController view];
                //            imAddNewViewController.delegate = self; //for pushes later

                if([[mSession profileInfo] hasPendingWorkshopList]) {
                    viewControllers = @[workshopListNav,
                                        workshopListNav,
                                        imAddNewNav
                                        ];
                    title = @[LOCALIZATION(C_MANAGEWORKSHOP_PENDING),       //lokalise 28 Jan
                              LOCALIZATION(C_MANAGEWORKSHOP_APPROVE),       //lokalise 28 Jan
                              LOCALIZATION(C_MANAGEWORKSHOP_ADDNEW)         //lokalise 28 Jan
                            ];
                } else {
                    viewControllers = @[
                                        workshopListNav,
                                        imAddNewNav
                                        ];
                    title = @[
                              LOCALIZATION(C_MANAGEWORKSHOP_APPROVE),
                              LOCALIZATION(C_MANAGEWORKSHOP_ADDNEW)
                    ];
                }
            }
        }
        else if (IS_COUNTRY(COUNTRYCODE_UNITEDKINGDOM))//if UK hide menu
        {
            viewControllers = @[workshopListNav,
                                ];
            title = @[
                      LOCALIZATION(C_MANAGEWORKSHOP_APPROVE),       //lokalise 28 Jan
                      ];
        }
        
        else if (IS_COUNTRY(COUNTRYCODE_RUSSIA))
        {
            ruWorkshopRegistrationView = [STORYBOARD_MANAGEWORKSHOP instantiateViewControllerWithIdentifier: VIEW_RU_WORKSHOP_REGISTRATION];
            UINavigationController *ruWorkshopNav = [[UINavigationController alloc] initWithRootViewController: ruWorkshopRegistrationView];
            [ruWorkshopNav setNavigationBarHidden: YES];
            [ruWorkshopRegistrationView view];
            
            if([[mSession profileInfo] hasPendingWorkshopList]) {
                viewControllers = @[workshopListNav,
                                    workshopListNav,
                                    ruWorkshopNav
                                    ];
                title = @[LOCALIZATION(C_MANAGEWORKSHOP_PENDING),
                          LOCALIZATION(C_MANAGEWORKSHOP_APPROVE),
                          LOCALIZATION(C_RU_REGISTRATION_TITLE)
                        ];
            } else {
                viewControllers = @[
                                    workshopListNav,
                                    ruWorkshopNav
                                    ];
                title = @[
                          LOCALIZATION(C_MANAGEWORKSHOP_APPROVE),
                        //  LOCALIZATION(C_MANAGEWORKSHOP_ADDNEW)
                          LOCALIZATION(C_RU_REGISTRATION_TITLE)
                ];
            }
        }
        
        
        
        
        
        else
        {
            if([[mSession profileInfo] hasPendingWorkshopList]) {
                viewControllers = @[workshopListNav,
                                    workshopListNav,
                                    workshopAddNewNav
                                    ];
                title = @[LOCALIZATION(C_MANAGEWORKSHOP_PENDING),
                          LOCALIZATION(C_MANAGEWORKSHOP_APPROVE),
                          LOCALIZATION(C_MANAGEWORKSHOP_ADDNEW)
                        ];
            } else {
                viewControllers = @[
                                    workshopListNav,
                                    workshopAddNewNav
                                    ];
                title = @[
                          LOCALIZATION(C_MANAGEWORKSHOP_APPROVE),
                          LOCALIZATION(C_MANAGEWORKSHOP_ADDNEW)
                ];
            }
        }
    }
  /*  else if([[mSession profileInfo] hasAddNewRegWorkshop]) //Chicago Workshop Add New - Only for RU
    {
        ruWorkshopRegistrationView = [STORYBOARD_MANAGEWORKSHOP instantiateViewControllerWithIdentifier: VIEW_RU_WORKSHOP_REGISTRATION];
        UINavigationController *ruWorkshopNav = [[UINavigationController alloc] initWithRootViewController: ruWorkshopRegistrationView];
        [ruWorkshopNav setNavigationBarHidden: YES];
        [ruWorkshopRegistrationView view];
        
        if([[mSession profileInfo] hasPendingWorkshopList]) {
            viewControllers = @[workshopListNav,
                                workshopListNav,
                                ruWorkshopNav
                                ];
            title = @[LOCALIZATION(C_MANAGEWORKSHOP_PENDING),
                      LOCALIZATION(C_MANAGEWORKSHOP_APPROVE),
                      LOCALIZATION(C_RU_REGISTRATION_TITLE)
                    ];
        } else {
            viewControllers = @[
                                workshopListNav,
                                ruWorkshopNav
                                ];
            title = @[
                      LOCALIZATION(C_MANAGEWORKSHOP_APPROVE),
                      LOCALIZATION(C_RU_REGISTRATION_TITLE)
            ];
        }
    }*/
    else
    {
       if([[mSession profileInfo] hasPendingWorkshopList]) {
            viewControllers = @[workshopListNav,
                                workshopListNav
                                ];
            title = @[LOCALIZATION(C_MANAGEWORKSHOP_PENDING),
                      LOCALIZATION(C_MANAGEWORKSHOP_APPROVE)
                    ];
        }
    }
    
    if (kIsRightToLeft) {
//        viewControllers = viewControllers.reverseObjectEnumerator.allObjects.mutableCopy;
        title = title.reverseObjectEnumerator.allObjects.mutableCopy;
    }
    
    [self setViewControllers: viewControllers];
    UIImage *selected = GET_ISADVANCE ? [UIImage imageNamed:@"tab-bg_blue"] : [UIImage imageNamed:@"tab-bg"];
    UIImage *unselected = [UIImage imageNamed:@"tab-bg_grey.png"];
    
    for (int i = 0; i < [self.tabBar.items count]; i++)
    {
        RDVTabBarItem *item = [self.tabBar.items objectAtIndex: i];
        [item setTitle: [title objectAtIndex: i]];
        
        [item setBackgroundColor: COLOUR_WHITE];
        
        
        if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
        {
            [item setTitleUnselectedAttributes:FONT_H2 colour:COLOUR_VERYDARKGREY];
            [item setTitleSelectedAttributes:FONT_H2 colour:COLOUR_RED];
        }
        else
        {
            [item setTitleUnselectedAttributes:FONT_H1 colour:COLOUR_VERYDARKGREY];
            [item setTitleSelectedAttributes:FONT_H1 colour:COLOUR_RED];
        }
        [item setBackgroundSelectedImage: selected withUnselectedImage: unselected];
        
        //change selected tab colour
        if (i == 0)
        {
            if (![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_UNITEDKINGDOM] &&  [[mSession profileInfo] hasPendingWorkshopList])
                [workshopListViewController selectedTab: i];
            else
                [workshopListViewController selectedTab: 1];
        }
    }
    if (kIsRightToLeft) {
        [self tabBar:self.tabBar didSelectItemAtIndex:self.tabBar.items.count-1];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict
{
    //    if (IS_COUNTRY(COUNTRYCODE_INDONESIA))
    //    {
    [addNewWorkshop_NewViewController didFinishWorkingHoursSelection: workingHoursDict];
    //    }
    //    else
    //    {
    //        [workshopAddNewViewController didFinishWorkingHoursSelection: workingHoursDict];
    //    }
}

#pragma mark RDVTabBarDelegate
- (BOOL)tabBar:(RDVTabBar *)tabBar shouldSelectItemAtIndex:(NSInteger)index
{
    for (RDVTabBarItem *item in [tabBar items])
    {
        
    }
    return ([tabBar selectedItem] != [[tabBar items] objectAtIndex: index]);
}

- (void)tabBar:(RDVTabBar *)tabBar didSelectItemAtIndex:(NSInteger)index
{
    //super method MUST BE PRESENT
    [super tabBar:tabBar didSelectItemAtIndex: index];
    
    //    [workshopListViewController hideLegend: nil];
    
    if (index == [[tabBar items] count] - 1
        && [[mSession profileInfo] hasAddNewWorkshop]) //add new to be last object of tab
    {
        //to remove this fully later, new form supports IQKeyboard
        //        if (!IS_COUNTRY(COUNTRYCODE_INDIA) && !IS_COUNTRY(COUNTRYCODE_INDONESIA)) //indo using new form, IQKeyboard will support
        //            [[IQKeyboardManager sharedManager] setEnable: NO];
        if(index == [[tabBar items] count] - 1 && kIsRightToLeft) {
             [workshopListViewController selectedTab: 0];
        }
    }
    else
    {
        //        [[IQKeyboardManager sharedManager] setEnable: YES];
        //perform workshoplistvc actions when selected (reload tableview, etc)
        [workshopListViewController selectedTab: index];
    }
    
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[IQKeyboardManager sharedManager] setEnable: YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
