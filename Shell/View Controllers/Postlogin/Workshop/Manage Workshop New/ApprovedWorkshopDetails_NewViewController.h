//
//  ApprovedWorkshopDetails_NewViewController.h
//  Shell
//
//  Created by Jeremy Lua on 29/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@protocol ApprovedWorkshopDetailsDelegate <NSObject>
-(void) didUpdateWorkshop;
@end

NS_ASSUME_NONNULL_BEGIN

@interface ApprovedWorkshopDetails_NewViewController : BaseVC

@property NSDictionary *workshopData;
@property (weak) id<ApprovedWorkshopDetailsDelegate> detailsDelegate;
-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict;

@end

NS_ASSUME_NONNULL_END
