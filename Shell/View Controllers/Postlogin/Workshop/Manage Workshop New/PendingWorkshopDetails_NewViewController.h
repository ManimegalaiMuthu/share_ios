//
//  PendingWorkshopDetails_NewViewController.h
//  Shell
//
//  Created by Jeremy Lua on 7/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@protocol PendingWorkshopDetailsDelegate <NSObject>
-(void) didUpdateWorkshop;
@end
NS_ASSUME_NONNULL_BEGIN

@interface PendingWorkshopDetails_NewViewController : BaseVC

@property NSDictionary *workshopData;
@property (weak) id<PendingWorkshopDetailsDelegate> detailsDelegate;
-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict;
@end

NS_ASSUME_NONNULL_END
