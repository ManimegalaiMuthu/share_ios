//
//  RURegistrationViewController.m
//  Shell
//
//  Created by Nach on 18/9/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "RUWorkshopRegistrationViewController.h"
#import "ManageWorkshopTabViewController.h"
#import "PopupSelectMultipleViewController.h"
#import "AddNewWorkshop_NewViewController.h"

@interface RUWorkshopRegistrationViewController () <WebServiceManagerDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,AddNewWorkshop_NewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblRegisteredName;
@property (weak, nonatomic) IBOutlet UILabel *lblRegisteredNameHint;
@property (weak, nonatomic) IBOutlet UITextField *textRegisteredWorkshopName;
@property (weak, nonatomic) IBOutlet UILabel *lblParticipants;
@property (weak, nonatomic) IBOutlet UIView *dropDownView;
@property (weak, nonatomic) IBOutlet UIButton *btnParticipantsSelected;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;

@property NSMutableArray *selectedParticipants;
@property NSMutableArray *participantsList;
@property NSMutableArray *defaultList;
@property NSArray *responseData;

@property NSInteger selectedIndex;
@property NSString *selectedPrimaryWorkshopRef;
@property (nonatomic, assign) BOOL myBoolWorkshop;

@end

@implementation RUWorkshopRegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myBoolWorkshop = NO;
    // Do any additional setup after loading the view.
    if (![self.parentViewController.parentViewController isKindOfClass: [ManageWorkshopTabViewController class]])
    {
        [Helper setNavigationBarTitle:self title: LOCALIZATION(C_RU_REGISTRATION_TITLE) subtitle:@"" size:15 subtitleSize:0];
    }
    
    self.lblHeader.text = LOCALIZATION(C_RU_REGISTRATION_HEADER);
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    self.lblHeader.font = FONT_B1;
    
    self.lblRegisteredName.text = LOCALIZATION(C_RU_REGISTRATION_REGISTEREDNAME);
    self.lblRegisteredName.textColor = COLOUR_VERYDARKGREY;
    self.lblRegisteredName.font = FONT_B1;
    
    self.lblRegisteredNameHint.text = LOCALIZATION(C_RU_REGISTRATION_REGISTEREDNAME_HINT);
    self.lblRegisteredNameHint.textColor = COLOUR_VERYDARKGREY;
    self.lblRegisteredNameHint.font = FONT_B(11);
    
    self.textRegisteredWorkshopName.textColor = COLOUR_VERYDARKGREY;
    self.textRegisteredWorkshopName.font = FONT_B1;
    self.textRegisteredWorkshopName.delegate = self;
    self.textRegisteredWorkshopName.text = @"";
    
    self.lblParticipants.text = LOCALIZATION(C_RU_REGISTRATION_PARTICIPANTS);
    self.lblParticipants.textColor = COLOUR_VERYDARKGREY;
    self.lblParticipants.font = FONT_B1;
    
    [self.btnParticipantsSelected.titleLabel setFont: FONT_B1];
    
    [self.btnNext setTitle:LOCALIZATION(C_RUSSIA_NEXT_BTN) forState:UIControlStateNormal];
    [self.btnNext setBackgroundColor:COLOUR_RED];
    [self.btnNext.titleLabel setFont:FONT_BUTTON];
    
    self.tableView.tableFooterView = [UIView new];
    
    self.textRegisteredWorkshopName.placeholder = LOCALIZATION(C_RU_REGISTRATION_REGISTEREDNAME_PLACEHOLDER);
    
    [self.btnParticipantsSelected setTitle: LOCALIZATION(C_RU_REGISTRATION_PARTICIPANTS_PLACEHOLDER) forState:UIControlStateNormal];
    [self.btnParticipantsSelected.titleLabel setTextColor:COLOUR_PALEGREY];
    
    self.selectedParticipants = [[NSMutableArray alloc]init];
    self.participantsList = [[NSMutableArray alloc] init];
    self.defaultList = [[NSMutableArray alloc]init];
    
    [self.btnNext setUserInteractionEnabled:NO];
    [self.btnNext setAlpha:0.5];
    
    self.selectedIndex = -1;
    self.selectedPrimaryWorkshopRef = @"";
    [self.tableView reloadData];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_MANAGEWORKSHOP),LOCALIZATION_EN(C_RU_REGISTRATION_TITLE)] screenClass:nil];
}

- (IBAction)dropDownPressed:(UIButton *)sender {
    self.selectedParticipants = self.participantsList;
    [self popUpMultipleSelectListViewWithTitle:LOCALIZATION(C_LOYALTY_SELECTWORKSHOP) listArray:@[] withSearchField:YES withPreselect:self.selectedParticipants withVc:self.myBoolWorkshop];
}


- (IBAction)nextPressed:(UIButton *)sender {
    
    self.selectedPrimaryWorkshopRef = self.participantsList[self.selectedIndex];
    
    NSMutableArray *linkedWorkshopArray = [[NSMutableArray alloc]init];
    NSString *primaryOutletID = @"";

    for(NSDictionary *dict in self.defaultList) {
        if([self.selectedPrimaryWorkshopRef isEqualToString: [dict objectForKey:@"CompanyNameWithReference"]]) {
            primaryOutletID = [dict objectForKey:@"POSTaxID"];
            if([linkedWorkshopArray count] > 0) {
                if([linkedWorkshopArray containsObject:[dict objectForKey:@"OutletCode"]]) {
                    [linkedWorkshopArray removeObject:[dict objectForKey:@"OutletCode"]];
                }
                [linkedWorkshopArray insertObject:[dict objectForKey:@"OutletCode"] atIndex:0];
            } else {
                 [linkedWorkshopArray addObject:[dict objectForKey:@"OutletCode"]];
            }
        }
        else if([self.participantsList containsObject:[dict objectForKey:@"CompanyNameWithReference"]]) {
            if(![linkedWorkshopArray containsObject:[dict objectForKey:@"OutletCode"]]) {
                [linkedWorkshopArray addObject:[dict objectForKey:@"OutletCode"]];
            }
        }
    }
        
    [mSession pushActivateRegistration:@{
                                         @"LinkedWorkshops" : linkedWorkshopArray,
                                         @"PrimaryOutletID" : primaryOutletID,
                                         @"RegisteredName"  : self.textRegisteredWorkshopName.text,
                                         } vc:self.parentViewController delegate:self];
}

-(void) btnDeletePressed:(UIButton *)sender
{
    UITableViewCell *cell = (UITableViewCell *) sender.superview.superview;
    if(self.selectedIndex == cell.tag) {
        self.selectedIndex = -1;
        self.selectedPrimaryWorkshopRef = @"";
    }
    [self.participantsList removeObjectAtIndex:cell.tag];
    self.tableViewHeight.constant = 60.0 + ([self.participantsList count] * 50.0);
    if([self.participantsList count] == 0) {
        self.tableViewHeight.constant = 110;
    }
    if([self.textRegisteredWorkshopName.text length] > 0 && [self.participantsList count] > 0) {
        [self.btnNext setUserInteractionEnabled:YES];
        [self.btnNext setAlpha:1.0];
    } else {
        [self.btnNext setUserInteractionEnabled:NO];
        [self.btnNext setAlpha:0.5];
    }
    [self.tableView reloadData];
}

-(void) selectPrimaryView:(UIButton *)sender
{
    UITableViewCell *cell = (UITableViewCell *) sender.superview.superview;
    self.selectedIndex = cell.tag;
    
    [self.tableView reloadData];
}

-(void) didRegisterWorkshop {
    [self viewDidLoad];
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.participantsList count] > 0 ? [self.participantsList count] : 1 ;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:@"RegistrationTableHeaderCell"];
    
    UILabel *lblWorkshopName = [headerCell viewWithTag:1];
    lblWorkshopName.text = LOCALIZATION(C_RU_REGISTRATION_TABLE_WORKSHOPNAME);
    lblWorkshopName.textColor = COLOUR_VERYDARKGREY;
    lblWorkshopName.font = FONT_H1;
    
    UILabel *lblUseAsPrimary = [headerCell viewWithTag:2];
    lblUseAsPrimary.text = LOCALIZATION(C_RU_REGISTRATION_TABLE_PRIMARY);
    lblUseAsPrimary.textColor = COLOUR_VERYDARKGREY;
    lblUseAsPrimary.font = FONT_H1;
    lblUseAsPrimary.hidden = YES;
    
    return headerCell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if([self.participantsList count] == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"NoDataRegistrationCell"];
        
        UILabel *lblNoWorkshop = [cell viewWithTag:1];
        lblNoWorkshop.text = LOCALIZATION(C_RU_REGISTRATION_NOWORKSHOP);
        lblNoWorkshop.textColor = COLOUR_VERYDARKGREY;
        lblNoWorkshop.font = FONT_B1;
        
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"RegistrationTableDataCell"];
        
        cell.tag = indexPath.row;
        
        UILabel *lblWorkshopName = [cell viewWithTag:1000];
        lblWorkshopName.text = [NSString stringWithFormat:@"%@",self.participantsList[indexPath.row]];
        lblWorkshopName.textColor = COLOUR_VERYDARKGREY;
        lblWorkshopName.font = FONT_B1;
        
        UIButton *btnPrimarySelect = [cell viewWithTag:1001];
        [btnPrimarySelect addTarget:self action: @selector(selectPrimaryView:) forControlEvents:UIControlEventTouchUpInside];
        
        UIView *selectedView = [cell viewWithTag:1002];
        selectedView.hidden = NO;
        if(self.selectedIndex == -1 && indexPath.row == 0) {
            selectedView.backgroundColor = COLOUR_RED;
            self.selectedIndex = 0;
        } else if(self.selectedIndex == indexPath.row){
            selectedView.backgroundColor = COLOUR_RED;
        } else {
            selectedView.backgroundColor = COLOUR_WHITE;
        }
        
        UIButton *btnDelete = [cell viewWithTag:1003];
        [btnDelete addTarget:self action: @selector(btnDeletePressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60.0;
}

#pragma mark - Drop Down Selection
- (void)dropDownMultipleSelection:(NSArray *)selection withFullData:(NSArray *)completeDataArray {
    if([selection count] > 0) {
        
        if([self.selectedParticipants count] > 0) {
            NSMutableArray *alreadySelectedArray = [NSMutableArray arrayWithArray:self.selectedParticipants];
            for (NSString *workshopName in self.selectedParticipants) {
                if(![selection containsObject:workshopName]) {
                    [alreadySelectedArray removeObject:workshopName];
                }
            }
            
            self.selectedParticipants = alreadySelectedArray;
            
            for (NSString *workshopName in selection) {
                if(![self.selectedParticipants containsObject:workshopName]) {
                    [self.selectedParticipants addObject:workshopName];
                }
            }
            
            self.participantsList = self.selectedParticipants;
        } else {
            self.selectedParticipants =  [NSMutableArray arrayWithArray:selection];
            self.participantsList = self.selectedParticipants;
        }
        
        [self.defaultList addObjectsFromArray:completeDataArray];
        
        self.tableViewHeight.constant = 60.0 + (([self.participantsList count] > 0 ? [self.participantsList count] : 1)* 50.0);
        [self.tableView reloadData];
        
        if([self.textRegisteredWorkshopName.text length] > 0 && [self.participantsList count] > 0) {
            [self.btnNext setUserInteractionEnabled:YES];
            [self.btnNext setAlpha:1.0];
        }  else {
            [self.btnNext setUserInteractionEnabled:NO];
            [self.btnNext setAlpha:0.5];
        }
    }
}

#pragma mark - TextField
- (void)textFieldDidEndEditing:(UITextField *)textField {
    if([textField.text length] > 0 && [self.participantsList count] > 0) {
        [self.btnNext setUserInteractionEnabled:YES];
        [self.btnNext setAlpha:1.0];
    }  else {
        [self.btnNext setUserInteractionEnabled:NO];
        [self.btnNext setAlpha:0.5];
    }
}

#pragma mark - WebService
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_FETCHCTO:
            self.responseData = [response getGenericResponse];
            
            for (NSDictionary *dict in self.responseData) {
                [self.defaultList addObject: [dict objectForKey:@"CompanyNameWithReference"]];
            }
           
            self.tableViewHeight.constant = 60.0 + (([self.participantsList count] > 0 ? [self.participantsList count] : 1)* 50.0);
            [self.tableView reloadData];
            break;
            
            
        default:
            break;
    }
}

@end
