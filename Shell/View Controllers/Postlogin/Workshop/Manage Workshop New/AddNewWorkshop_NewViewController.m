//
//  AddNewWorkshop_NewViewController.m
//  Shell
//
//  Created by Jeremy Lua on 12/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <MapKit/MapKit.h>
//#import "WorkshopDetailsViewController.h"
#import "CASDatePickerView.h"
#import "WorkingHoursViewController.h"
#import "QRCodeScanner.h"
#import "GalleryUploadViewController.h"
#import "RUCustomerSignatureViewController.h"
#import "PhotoTakingViewController.h"

#import "PendingWorkshopDetails_NewViewController.h"
#import "FormsTableViewDelegate.h"
#import "PopUpYesNoViewController.h"
#import "PopupTextViewController.h"
#import "PopupInfoViewController.h"

#import "AddNewWorkshop_NewViewController.h"


#define kUserID             @"UserID"
#define kDSRCode            @"DSRCode"
#define kUserPassword       @"UserPassword"
#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kEmailAddress       @"EmailAddress"
#define kDOB                @"DOB"
#define kDOBMonth           @"DOBMonth"
#define kDOBDay             @"DOBDay"
#define kCountryCode        @"CountryCode"
#define kReferenceCode          @"ReferenceCode"        //Workshop Code
#define kWorkshopQRCode         @"WorkshopQRCode"       //Workshop SHARE Code (for QR Scan) //mechanic share code here
#define kCompanySource          @"CompanySource"
#define kCompanyType            @"CompanyType"
#define kCompanyCategory            @"CompanyCategory" //to change again?
#define kCompanyName            @"CompanyName"
#define kCompanyEmailAddress    @"CompanyEmailAddress"
#define kCompanyTier            @"CompanyTier"
#define kContactNumber          @"ContactNumber"
#define kOilChangePerMonth      @"OilChangePerMonth"
#define kOilChangePercentage    @"OilChangePercentage"
#define kHalfSyntheticOilChanges    @"HalfSyntheticOilChanges"
#define kShellHelixQty          @"ShellHelixQty"
#define kStatus                 @"Status"
#define kRemarks                @"Remarks"
#define kAddressType    @"AddressType"
#define kAddress1       @"Address1"
#define kAddress2       @"Address2"
#define kStreetName     @"StreetName"
#define kCompanyCity    @"CompanyCity"
#define kCompanyState   @"CompanyState"
#define kPostalCode     @"PostalCode"
#define kLongitude      @"Longitude"
#define kLatitude       @"Latitude"

#define kWorkingHours       @"WorkingHours"
#define kWorkshopLocation   @"Location"

#define kApprovalStatus         @"ApprovalStatus"
#define kActivationStatus       @"ActivationStatus"

//MechanicType use CompanySource
#define kMechanicType           @"CompanySource" //MechanicType use CompanySource
#define kCustomerCategory       @"CustomerCategory"

//Bank Details
#define kValidateCheckboxes          @"ValidateCheckboxes"
#define kPANCardId          @"PANCardId"
#define kPANCardImage       @"PANCardImage"
#define kBeneficiaryName    @"BeneficiaryName"
#define kBankID             @"BankID"
#define kBeneficiaryName    @"BeneficiaryName"
#define kAccountNumber      @"AccountNumber"
#define kConfirmAccountNumber @"ConfirmAccountNumber"

#define kIFSCCode           @"IFSCCode"
#define kBeneficiaryImage   @"BeneficiaryImage" //for UI only

//last section
#define kDigitalSignature          @"DigitalSignature"    //Russia, signature
#define kBankCheckBox   @"BankCheckBox" //for checkbox only
#define kTncCheckBox   @"TncCheckBox" //for checkbox only
#define kContactPreference  @"ContactPreferences"       //contact preferences checkboxes

#define kOutletIDs     @"OutletIDs"
#define kPrimaryOutletID    @"PrimaryOutletID"
#define kPrimaryOutletCode  @"PrimaryOutletCode"

#define kRegisterCTA         @"RegisterCTA"
#define kUpdateCTA           @"UpdateCTA"


@interface AddNewWorkshop_NewViewController () <WorkingHoursViewDelegate, WebServiceManagerDelegate, PhotoTakingVCDelegate, FormsTableViewDelegate, CLLocationManagerDelegate, QRCodeScannerDelegate, CommonListDelegate, RUCustomerSignatureDelegate>

@property (weak, nonatomic) IBOutlet UITableView *regFormTableView;

@property FormsTableViewDelegate *tableViewDelegate;

@property CASDatePickerView *dateView;

@property NSDictionary *workingHoursDict;
@property NSArray *stateTable;

@property Member *member;
@property CompanyInfo *companyInfo;
@property CompanyAddress *companyAddress;

@property CLLocationManager *locationManager;

@property NSArray *companyCategoryList;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@end

@implementation AddNewWorkshop_NewViewController
@synthesize listView;
@synthesize dateView;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"oilchange:%@",kOilChangePerMonth);
    
    // Do any additional setup after loading the view.
    self.stateTable = GET_LOADSTATE;
    
    if ([self.navigationController.childViewControllers count] > 1) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_RU_ACTIVATE_PREREGISTERED_TITLE) subtitle: @"" size: 15 subtitleSize: 0];
    }
    else {
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_IN_MANAGEMYMECHANICS) subtitle: @"" size: 15 subtitleSize: 0];
    }
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager requestWhenInUseAuthorization];
    
    if (!self.workingHoursDict ||
        [self.workingHoursDict isKindOfClass: [NSNull class]])
        self.workingHoursDict = @{@"WorkHourStart1":   @"0:00",
                                  @"WorkHourEnd1":     @"0:00",
                                  @"WeekDays1":        @"",
                                  @"PublicHoliday":    @"0",
                                  @"WorkHourStart2":   @"0:00",
                                  @"WorkHourEnd2":     @"0:00",
                                  @"WeekDays2":        @"",
                                  };
    
    if (!self.registeredName ||
        [self.registeredName isKindOfClass: [NSNull class]]) {
        self.registeredName = @"";
        self.primaryOutletID = @"";
        self.linkedWorkshopArray = @[];
    }
    
    
    [[WebServiceManager sharedInstance] loadState: [[mSession lookupTable] objectForKey: @"LookUpVersion"]
                                      countryCode: GET_COUNTRY_CODE
                                               vc: self];
    
    if([[[mSession lookupTable] objectForKey: @"IsShowCompanyCategory"] boolValue] || [GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_INDIA]) {
        [[WebServiceManager sharedInstance] loadCompanyCategory:self];
    }
}

//-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
//{
//    if ([[url absoluteString] isEqualToString: @"WorkshopTNC"])
//    {
//        NSDictionary *data = GET_WORKSHOP_TNC;
//
//        PopupWebviewViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPWEBVIEW];
//
//        [popupVc popupInitWithHTMLBody: [data objectForKey: @"TncText"] headerTitle: LOCALIZATION(C_SIDEMENU_TNC) buttonTitle: LOCALIZATION(C_GLOBAL_OK)];
//    }
//    else if ([[url absoluteString] isEqualToString: @"WorkshopPrivacy"])
//    {
//        PopupWebviewViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPWEBVIEW];
//
//        NSString *urlAddress = [NSString stringWithFormat:@"%@%@%@?c=%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_PRIVACY_URL, GET_COUNTRY_CODE];
//        [popupVc popupInitWithURL: urlAddress headerTitle: LOCALIZATION(C_TITLE_PRIVACY) buttonTitle: LOCALIZATION(C_GLOBAL_OK)];
//    }
//
//}

-(void) setupGoogleAnalytics
{
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        if (GET_PROFILETYPE == kPROFILETYPE_DSR)
            [FIRAnalytics setScreenName:[NSString stringWithFormat: @"%@ / %@", LOCALIZATION_EN(C_TITLE_MYNETWORK), LOCALIZATION_EN(C_MANAGEWORKSHOP_ADDNEW)] screenClass:nil];
        else
            [FIRAnalytics setScreenName:[NSString stringWithFormat: @"%@ / %@", LOCALIZATION_EN(C_TITLE_IN_MANAGEMYMECHANICS), LOCALIZATION_EN(C_MANAGEWORKSHOP_ADDNEW)] screenClass:nil];
    }
    else
    {
        [FIRAnalytics setScreenName:[NSString stringWithFormat: @"%@ / %@", LOCALIZATION_EN(C_TITLE_MANAGEWORKSHOP), LOCALIZATION_EN(C_MANAGEWORKSHOP_ADDNEW)] screenClass:nil];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_COUNTRY)
                                                   list: [mSession getCountryNames]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];       //lokalise 4 feb
    listView.delegate = self;
    
    //preload dateview
    dateView = [[CASDatePickerView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_BIRTHDAY) previouslySelected:nil withYearHidden:YES];       //lokalise 4 feb
    
    dateView.delegate = self;
    [self setupGoogleAnalytics];
}

-(void) setupInterface
{
    self.member = [[Member alloc] init];
    self.companyInfo = [[CompanyInfo alloc] init];
    self.companyAddress = [[CompanyAddress alloc] init];
    
    self.member.countryCode = GET_COUNTRY_CODE;
    
    if (!self.workingHoursDict ||
        [self.workingHoursDict isKindOfClass: [NSNull class]])
        self.workingHoursDict = @{@"WorkHourStart1":   @"0:00",
                                  @"WorkHourEnd1":     @"0:00",
                                  @"WeekDays1":        @"",
                                  @"PublicHoliday":    @"0",
                                  @"WorkHourStart2":   @"0:00",
                                  @"WorkHourEnd2":     @"0:00",
                                  @"WeekDays2":        @"",
                                  };
    self.tableViewDelegate = [[FormsTableViewDelegate alloc] init];
    
    NSMutableArray *workshopSectionArray = [[NSMutableArray alloc] init];
    [self setupWorkshopSectionArray: workshopSectionArray];
    [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_HEADER_WORKSHOPPROFILE)];     //lokalised 4 feb
    [self.tableViewDelegate.initializingArray addObject: workshopSectionArray];
    
    NSMutableArray *ownerSectionArray = [[NSMutableArray alloc] init];
    [self setupOwnerProfileSectionArray: ownerSectionArray];
    [self.tableViewDelegate.initializingArray addObject: ownerSectionArray];
    [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_HEADER_OWNERPROFILE)];     //lokalised 4 feb
    
    NSMutableArray* addressDetailsArray = [NSMutableArray new];
    [self setupAddressArray:addressDetailsArray];
    [self.tableViewDelegate.initializingArray addObject: addressDetailsArray];
    [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_PROFILE_ADDRESS)];     //lokalised 4 feb
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        NSMutableArray *bankDetailsArray = [[NSMutableArray alloc] init];
        [self setupBankDetailsArray: bankDetailsArray];
        [self.tableViewDelegate.initializingArray addObject: bankDetailsArray];
        [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_HEADER_BANKDETAILS)];        //lokalise 30 jan
        
        NSMutableArray *countrySpecificLastSectionArray = [[NSMutableArray alloc] init];
        [self setupCountrySpecificLastSectionArray: countrySpecificLastSectionArray];
        [self.tableViewDelegate.initializingArray addObject: countrySpecificLastSectionArray];
        [self.tableViewDelegate.headersArray addObject: @""];        //lokalise 30 jan
    }
    
    if(!IS_COUNTRY(COUNTRYCODE_INDIA)) {
        NSMutableArray *contactPrefArray = [NSMutableArray new];
        [self setupContactPrefArray:contactPrefArray];
        [self.tableViewDelegate.headersArray addObject: @""];
        [self.tableViewDelegate.initializingArray addObject:contactPrefArray];
    }
    
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        NSMutableArray *countrySpecificLastSectionArray = [[NSMutableArray alloc] init];
        [self setupCountrySpecificLastSectionArray: countrySpecificLastSectionArray];
        [self.tableViewDelegate.initializingArray addObject: countrySpecificLastSectionArray];
        [self.tableViewDelegate.headersArray addObject: @""];        //lokalise 30 jan
    }
    
    NSMutableArray *lastSectionArray = [[NSMutableArray alloc] init];
    [self setupLastSectionArray: lastSectionArray];
    [self.tableViewDelegate.initializingArray addObject: lastSectionArray];
    [self.tableViewDelegate.headersArray addObject: @""];
    
    self.regFormTableView.delegate = self.tableViewDelegate;
    self.regFormTableView.dataSource = self.tableViewDelegate;
    
    
    [self.tableViewDelegate registerNib: self.regFormTableView];
    [self.tableViewDelegate setDelegate: self];

    if(@available(iOS 11, *))
    {
        self.regFormTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.regFormTableView setContentInset:UIEdgeInsetsMake(15, 0, 15, 0)];
    
    
    self.regFormTableView.backgroundColor = [UIColor clearColor];
    self.regFormTableView.backgroundView = nil;
}


#pragma mark Setup Form methods
-(void) setupWorkshopSectionArray: (NSMutableArray *) workshopSectionArray
{
    [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_COMPANYSOURCE),     //lokalised 4 feb
                                       kForm_Type: @(kREGISTRATION_DROPDOWN),
                                       kForm_Key: kCompanySource,
                                       kForm_Value: LOCALIZATION(C_PROFILE_COMPANYSOURCE_PLACEHOLDER),     //lokalised 4 feb
                                       }];
    
    if (!IS_COUNTRY(COUNTRYCODE_INDIA) && !(IS_COUNTRY(COUNTRYCODE_RUSSIA)))
    {
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_ADDNEWREMARKS),     //lokalised 4 feb
                                           kForm_Type: @(kREGISTRATION_TEXTVIEW),
                                           kForm_Key: kRemarks,
                                           kForm_Placeholder: LOCALIZATION(C_PROFILE_ADDNEWREMARKS),     //lokalised 4 feb
                                           }];
    }
    
    [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPTYPE),     //lokalised 4 feb
                                       kForm_Type: @(kREGISTRATION_DROPDOWN),
                                       kForm_Key: kCompanyType,
                                       kForm_Value: LOCALIZATION(C_PROFILE_WORKSHOPTYPE_PLACEHOLDER),     //lokalised 4 feb
                                       }];    //workshop type
    
    if ([[[mSession lookupTable] objectForKey: @"IsShowCompanyCategory"] boolValue])
    {
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPCATEGORY),     //lokalised 4 feb
                                           //                                      @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPCATEGORY),
                                           kForm_Type: @(kREGISTRATION_DROPDOWN),
                                           kForm_Key: kCompanyCategory,
                                           kForm_Value: LOCALIZATION(C_DROPDOWN_WORKSHOPCATEGORY),      //lokalised 4 feb
                                           }];    //workshop category
    }
    
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_COMPANYNAME),     //lokalised 4 feb
                                           kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                           kForm_Key: kCompanyName,
                                           kForm_Value: self.registeredName,
                                           }]; //company name
    } else {
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_COMPANYNAME),     //lokalised 4 feb
                                           kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                           kForm_Key: kCompanyName,
                                           kForm_Value: @"",
                                           }]; //company name
    }
    
   // if(IS_COUNTRY(COUNTRYCODE_RUSSIA) && [[mSession profileInfo] hasAddNewRegWorkshop]) {
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        [workshopSectionArray addObject:@{
                                          kForm_Title : LOCALIZATION(C_FORM_LINKEDWORKSHOP),
                                          kForm_Type : @(kFORM_LINKEDWORKSHOP),
                                          kForm_LinkedWorkshopDict : self.linkedWorkshopArray,
                                          kForm_Key: kOutletIDs,
                                          kForm_Enabled : @(YES),
                                        }];
        
        [workshopSectionArray addObject:@{
                                          kForm_Title : LOCALIZATION(C_FORM_POSTAXID),
                                          kForm_Type : @(kREGISTRATION_TEXTFIELD),
                                          kForm_Key: kPrimaryOutletID,
                                          kForm_Optional: @(!IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                                          kForm_Enabled: @(NO),
                                          kForm_Value: self.primaryOutletID,
                                          }];
    }
    
    if(!IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPSHARECODE),     //lokalised 4 feb
                                           kForm_Type: @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                                           kForm_Icon: @"icon-code",
                                           kForm_Key: kWorkshopQRCode,
        //                                 kForm_Value: ,
                                           kForm_TextfieldEnabled: @(YES),
                                           kForm_Enabled: @(YES),
                                           kForm_Optional: @(!(IS_COUNTRY(COUNTRYCODE_MALAYSIA) ||
                                                                   IS_COUNTRY(COUNTRYCODE_THAILAND) ||
                                                                   IS_COUNTRY(COUNTRYCODE_VIETNAM)  ||
                                                                   IS_COUNTRY(COUNTRYCODE_HONGKONG) ||
                                                                   IS_COUNTRY(COUNTRYCODE_SAUDI))), //Required field
                                               }];   //Workshop SHARE Code
        
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPCODE),     //lokalised 4 feb
                                           kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                           kForm_Key: kReferenceCode,
                                           kForm_Optional: @(YES),
                                                }];   //Workshop Code
        
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_CONTACTNUMBER),     //lokalised 4 feb
                                           kForm_Placeholder: LOCALIZATION(C_PROFILE_CONTACTNUMBER_PLACEHOLDER),     //lokalised 4 feb
                                           kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                           kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                           kForm_Key: kContactNumber,
                                           kForm_Optional: @(YES),
                                           }]; //contact number
        
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_EMAILADDRESS),     //lokalised 4 feb
                                           kForm_Placeholder: LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER),     //lokalised 4 feb
                                           kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                           kForm_Keyboard: @(UIKeyboardTypeEmailAddress),
                                           kForm_Key: kCompanyEmailAddress,
//                                           kForm_Value: @"",
                                           kForm_Optional: @(YES),
                                           }]; //email address
       
        if (!IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKINGHOURS),     //lokalised 4 feb
                                           kForm_Type: @(kREGISTRATION_WORKINGHOURS),
                                           kForm_Key: kWorkingHours,
                                           kForm_Value: @"", //using kForm_WorkingHours instead
//                                           kForm_WorkingHours: [self.workingHoursDict isKindOfClass:[NSNull class]] ? @"" : self.workingHoursDict,
                                           kForm_WorkingHours: self.workingHoursDict,   //must link dictionary to cell
                                           kForm_Optional: @(YES),
                                           kForm_Enabled: @(YES), //MUST BE PRESENT
                                           }];
        }
        

        if (!IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
                [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_OILCHANGEPERDAY),     //lokalised 4 feb
                                                       kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER),     //lokalised 4 feb
                                                       kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
                                                       kForm_Key: kOilChangePerMonth,
                                                       kForm_Keyboard: @(UIKeyboardTypeNumberPad),
            //                                           kForm_Value: @"",
                                                       }];
                    
                    [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_OILCHANGEPERCENT),  //lokalised 4 feb
                                                       kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER),  //lokalised 4 feb
                                                       kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
                                                       kForm_Key: kOilChangePercentage,
                                                       kForm_Keyboard: @(UIKeyboardTypeDecimalPad),
            //                                           kForm_Value: @"",
                                                       }];
                    
                    [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SEMIOILCHANGEPERCENT),  //lokalised 4 feb
                                                       kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER),  //lokalised 4 feb
                                                       kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
                                                       kForm_Key: kHalfSyntheticOilChanges,
                                                       kForm_Keyboard: @(UIKeyboardTypeDecimalPad),
                                                       kForm_Value: @"",
                                                       }];
                    
                    [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SHELLHELIX),  //lokalised 4 feb
                                                       kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER),  //lokalised 4 feb
                                                       kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
                                                       kForm_Key: kShellHelixQty,
                                                       kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                                       kForm_Value: @"",
                                                       }];
        }
        
    } else {
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPCODE),     //lokalised 4 feb
                                           kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                           kForm_Key: kReferenceCode,
                                           kForm_Optional: @(YES),
        }];   //Workshop Code
    }

}

-(void) setupOwnerProfileSectionArray: (NSMutableArray *)ownerSectionArray
{
    [ownerSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SALUTATION),  //lokalised 4 feb
                                    kForm_Type: @(kREGISTRATION_DROPDOWN),
                                    kForm_Key: kSalutation,
                                    kForm_Value: LOCALIZATION(C_PROFILE_SALUTATION_PLACEHOLDER),  //lokalised 4 feb
//                                    kForm_Value: @"",
                                    kForm_Optional: @(YES),
                                    }];  //button
    
    [ownerSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_FIRSTNAME),  //lokalised 4 feb
                                    kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                    kForm_Key: kFirstName,
//                                    kForm_Value: @"",
                                    }]; //first name
    
    
    [ownerSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_LASTNAME),  //lokalised 4 feb
                                    kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                    kForm_Key: kLastName,
                                    kForm_Value: @"",
                                    }]; //last name
    
    if (!IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [ownerSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_DOB),  //lokalised 4 feb
                                        kForm_Type: @(kREGISTRATION_BIRTHDATE),
                                        kForm_Key: kDOB,
                                        kForm_Value: LOCALIZATION(C_PROFILE_DOB_PLACEHOLDER),  //lokalised 4 feb
//                                        kForm_Value: @"",
//                                        kForm_Enabled:  @(IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                                        kForm_Optional: @(YES),
                                        }];
    }
    
    [ownerSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_MOBILENUM),  //lokalised 4 feb
                                    kForm_Type: @(kREGISTRATION_MOBNUM),
                                    kForm_Key: kMobileNumber,
                                    kForm_TextfieldEnabled: @(YES),
                                    kForm_CountryCode: [mSession convertCountryCodeToCountryDisplayName: self.member.countryCode],
//                                    kForm_MobileNumber: @"",
                                    }];   //Mobile number
    
    if (!IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [ownerSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_OWNEREMAILADDRESS),  //lokalised 4 feb
                                        kForm_Placeholder: LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER),  //lokalised 4 feb
                                        kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                        kForm_Key: kEmailAddress,
                                        kForm_Keyboard: @(UIKeyboardTypeEmailAddress),
//                                        kForm_Value: @"",
                                        kForm_Optional: @(!IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                                        }]; //email
    }
}

-(void) setupAddressArray: (NSMutableArray *) addressArray
{
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_LOCATION),  //lokalised 4 feb
                               kForm_Type: @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                               kForm_TextfieldEnabled: @(NO),
                               kForm_Icon: @"icon-location",
                               kForm_Key: kWorkshopLocation,
                               kForm_Enabled: @(YES),
//                               kForm_Value: @"",
                               }];    //location
    
    if(![GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_HONGKONG]) {
        [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_POSTALCODE),  //lokalised 4 feb
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_Key: kPostalCode,
                                   kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                   kForm_HasDelegate : @(IS_COUNTRY(COUNTRYCODE_INDIA)), //textfield delegate for postal code
                                   //                               kForm_Value: @"",
                                   }]; //postal code
    }
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_STATE),  //lokalised 4 feb
                               kForm_Type: @(kREGISTRATION_DROPDOWN),
                               kForm_Key: kCompanyState,
                               kForm_Value: LOCALIZATION(C_PROFILE_STATE_PLACEHOLDER),  //lokalised 4 feb
                               }];
    
    if (!IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_TOWN),  //lokalised 4 feb
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_Key: kCompanyCity, //kCity?
                                   kForm_Value: self.companyAddress.companyCity,
                                   kForm_Optional: @(!(IS_COUNTRY(COUNTRYCODE_RUSSIA) ||
                                                       IS_COUNTRY(COUNTRYCODE_INDIA))),
                                   }];
    }
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_ADDRESSLINE1),  //lokalised 4 feb
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_Key: kAddress1,
                               kForm_Value: self.companyAddress.address1,
                               //                               kForm_Optional: @(!IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                               }]; //address line 1
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_ADDRESSLINE2),  //lokalised 4 feb
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_Key: kAddress2,
                               kForm_Value: self.companyAddress.address2,
                               kForm_Optional: @(YES),
                               }];
}

-(void) setupBankDetailsArray: (NSMutableArray *) bankDetailsArray
{
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(@"Validate Account by"),  //lokalised 4 feb
                                   kForm_Type: @(kFORM_MULTICHECKBOX),
                                   kForm_Key: kValidateCheckboxes,
                                   kForm_Enabled: @(YES),
                                   }];
    
//    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_PANCARDID),  //lokalised 4 feb
//                                   kForm_Type: @(kFORM_PANCARD_ID),
//                                   kForm_Key: kPANCardId,
////                                   kForm_Value: @"",
////                                   kForm_Enabled: @(YES),
////                                   kForm_Optional: @(YES),  //controlled in formdelegate level.
//                                   }];
//    
//    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_PANCARDIMAGE),  //lokalised 4 feb
//                                   kForm_Type: @(
//                                       kFORM_PANCARD_IMAGE),
//                                   kForm_Key: kPANCardImage,
//                                   kForm_Placeholder: LOCALIZATION(C_FORM_PANCARDIMAGE_PLACEHOLDER),  //lokalised 4 feb
////                                   kForm_Value: @"",
////                                   kForm_Optional: @(YES),  //controlled in formdelegate level.
//                                   }];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BENEFICIARYNAME),  //lokalised 4 feb
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_Key: kBeneficiaryName,
                                   kForm_Placeholder: LOCALIZATION(C_FORM_BENEFICIARYNAME_PLACEHOLDER),  //lokalised 4 feb
                                   kForm_Value: @"",
                                   }];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BENEFICIARYACCTNUM),  //lokalised 4 feb
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                   kForm_Key: kAccountNumber,
                                   kForm_SecureEntry: @(YES),
                                   kForm_HasDelegate : @(IS_COUNTRY(COUNTRYCODE_INDIA)), //textfield delegate for account number
                                   kForm_Value: @"",
                                   }];

    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_CONFIRMBENEFICIARYNUM),  //lokalised 4 feb
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                   kForm_Key: kConfirmAccountNumber,
                                   kForm_Value: @"",
                                   }];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BENEFICIARYIFSC),  //lokalised 4 feb
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_Key: kIFSCCode,
                                   kForm_Value: @"",
                                   kForm_HasDelegate : @(IS_COUNTRY(COUNTRYCODE_INDIA)), //textfield delegate for ifsc code
                                   }];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BANKNAME),  //lokalised 4 feb
                                   kForm_Type: @(kREGISTRATION_DROPDOWN),
                                   kForm_Key: kBankID,
                                   //                                   kForm_Value: @"",
                                   kForm_Enabled: @(NO),
                                   }];
    
    [bankDetailsArray addObject: @{kForm_Type: @(kFORM_PROFILE_IMAGE),
                                   kForm_Key: kBeneficiaryImage, //for UI only
                                   kForm_Value: @"cheque_sample",
                                   }]; //
}

-(void) setupCountrySpecificLastSectionArray: (NSMutableArray *) countrySpecificLastSectionArray {
    
    if (IS_COUNTRY(COUNTRYCODE_RUSSIA))
    {
        [countrySpecificLastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_RUSSIA_CUSTOMERSIGNATURE),  //lokalised 4 feb
                                       kForm_Type: @(kREGISTRATION_SIGNATURE),
                                       kForm_Key: kDigitalSignature,
                                       kForm_Enabled: @(YES),
                                       }];
    }
    
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [countrySpecificLastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BANKINFO),  //lokalised 4 feb
                                       kForm_Type: @(kREGISTRATION_TNCCHECKBOX),
                                       kForm_Key: kBankCheckBox,
                                       kForm_Enabled: @(YES),
                                       }];
    }
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA) || IS_COUNTRY(COUNTRYCODE_RUSSIA))
    {
        
        [countrySpecificLastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_TNC_IN),  //lokalised 4 feb
                                       kForm_Type: @(kREGISTRATION_TNCCHECKBOX),
                                       kForm_Key: kTncCheckBox,
                                       kForm_Hyperlinks: @{LOCALIZATION(C_RU_STEPFOUR_TNCLINK): @"tnc",   //lokalised 4 feb
                                                           LOCALIZATION(C_RU_STEPFOUR_PRIVACYLINK) : @"privacypolicy",  //lokalised 4 feb
                                                           },
                                       kForm_Enabled: @(YES),
                                       }];
    }
    
}

-(void) setupLastSectionArray: (NSMutableArray *)lastSectionArray
{
   
    [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_REGISTER),  //lokalised 4 feb
                                   kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                   kForm_Key: kRegisterCTA,
                                   kForm_Enabled: @(YES),
                                   kForm_TextColour: COLOUR_WHITE,
                                   kForm_BackgroundColour: COLOUR_RED,
                                   }]; //Register
}

-(void) setupContactPrefArray: (NSMutableArray*) contactPrefArray {
    [contactPrefArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_CONTACT_PREFERENCE),  //lokalised 4 feb
                                   kForm_Type: @(kREGISTRATION_CONTACTPREF),
                                   kForm_Key: kContactPreference,
                                   kForm_Enabled: @(YES),
                                   kForm_BackgroundColour: COLOUR_WHITE,
                                   kForm_Optional: IS_COUNTRY(COUNTRYCODE_SAUDI) ? @(NO) : @(YES),
                                   }];
}
                                                                 
#pragma mark form methods
-(void) dropdownPressedWithKey:(NSString *) formKey
{
    [self.view endEditing: YES];
    listView.formKey = formKey;
    
    //Company Source
    if ([formKey isEqualToString: kCompanySource])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_COMPANYSOURCE) list: [mSession getCompanySource] hasSearchField: NO];     //lokalised 4 feb
    }
    //    //Distributor Approval - disabled, no need popup
    //    else if ([formKey isEqualToString: kApprovalStatus])
    //    {
    //        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_COMPANYSOURCE) list: [mSession ] hasSearchField: NO];
    //    }
    else if ([formKey isEqualToString: kCustomerCategory])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_WORKSHOPCATEGORY) list: [mSession getCompanyCategory] hasSearchField: NO];  //lokalised 4 feb
    }
    else if ([formKey isEqualToString: kActivationStatus])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_ACTIVATIONSTATUS) list: [mSession getLeadStatus] hasSearchField: NO];        //lokalised
    }       //lokalise 4 feb
    //Workshop Type
    else if ([formKey isEqualToString: kCompanyType])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_WORKSHOPTYPE) list: [mSession getCompanyTypes] hasSearchField: NO];          //lokalised
    }   //lokalised 4 feb
    //Workshop Category
    else if ([formKey isEqualToString: kCompanyCategory])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_WORKSHOPCATEGORY) list: [self getCompanyCategoryValues] hasSearchField: NO];    //lokalised 4 feb
    }
    else if ([formKey isEqualToString: kCompanyState])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_STATE) list: [mSession getStateNames: self.stateTable] hasSearchField: YES];    //lokalised 4 feb
    }
    else if ([formKey isEqualToString: kSalutation])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO];    //lokalised 4 feb
    }
    else if ([formKey isEqualToString: kBankID])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_BANKNAME) list: [mSession getBanksDetailsList] hasSearchField: NO];    //lokalised 4 feb
    }
    else
    {
        listView.formKey = nil;
        [mAlert showErrorAlertWithMessage: @"Unknown Error"];
        return;
    }
    [self popUpView];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        [self.tableViewDelegate editCell: listView.formKey withDropdownSelection: selection.firstObject];
        listView.formKey = nil;
    }
}
-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

-(void) didPressWorkingHours
{
    if (!self.workingHoursDict ||
        [self.workingHoursDict isKindOfClass: [NSNull class]])
        self.workingHoursDict = @{@"WorkHourStart1":   @"0:00",
                                  @"WorkHourEnd1":     @"0:00",
                                  @"WeekDays1":        @"",
                                  @"PublicHoliday":    @"0",
                                  @"WorkHourStart2":   @"0:00",
                                  @"WorkHourEnd2":     @"0:00",
                                  @"WeekDays2":        @"",
                                  };
    
    //let tab bar push instead
    [mSession pushWorkingHoursView: self.navigationController.parentViewController workingHoursData: self.workingHoursDict];
}

-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict
{
    //assign working hours
    self.workingHoursDict = workingHoursDict;
    
    [self.tableViewDelegate updateWorkingHours: self.workingHoursDict];
    
    [self.regFormTableView reloadData];
    
    //    NSArray *fullWorkWeekDays;
    //    NSArray *halfWorkWeekDays;
    //
    //    if (![[workingHoursDict objectForKey: @"WeekDays1"] isEqualToString: @""])
    //        fullWorkWeekDays = [[workingHoursDict objectForKey: @"WeekDays1"] componentsSeparatedByString: @","];
    //
    //    if (![[workingHoursDict objectForKey: @"WeekDays2"] isEqualToString: @""])
    //        halfWorkWeekDays = [[workingHoursDict objectForKey: @"WeekDays2"] componentsSeparatedByString: @","];
}

-(void) ctaPressedWithKey:(NSString *) ctaKey
{
    if ([ctaKey isEqualToString: kRegisterCTA])
    {
        [self registerWorkshopCTA];
    }
//    else if ([ctaKey isEqualToString: kUpdateCTA])
//    {
//        [self updatePressed];
//    }
}

-(void) registerWorkshopCTA
{
    //patched for confirm account
    NSDictionary *jsonValuesDict = [self.tableViewDelegate getJsonValues];
    if (IS_COUNTRY(COUNTRYCODE_INDIA) &&
        ![[jsonValuesDict objectForKey: kAccountNumber] isEqualToString: [jsonValuesDict objectForKey: kConfirmAccountNumber]])
    {
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_FORM_BENEFICIARYNUM_NOTMATCH)];
        return;
    }
    
    NSMutableDictionary *updateParams = [self setupForUpdate];
    NSMutableDictionary *regCompanyDict = [updateParams objectForKey: @"REG_Company"];
    [regCompanyDict setObject: @(4) forKey: kStatus];
    
    [updateParams setObject: self.workingHoursDict forKey: @"REG_CompanyWorkHours"];
    

    [[WebServiceManager sharedInstance] addNewWorkshop:updateParams
                                                    vc:self];
    
}

-(NSMutableDictionary *) setupForUpdate
{
    NSDictionary *jsonValuesDict = [self.tableViewDelegate getJsonValues];
    
    
    
  //  [jsonValuesDict setObject: @"" forKey: kOilChangePerMonth];
    
    NSLog(@"jsonValuesDict:%@",jsonValuesDict);
    
    NSMutableDictionary *updateParams = [[NSMutableDictionary alloc] init];
    
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
        [updateParams setObject: @([[jsonValuesDict objectForKey: kBankCheckBox] boolValue]) forKey: @"BankInfoSharing"];
    [updateParams setObject: GET_COUNTRY_CODE forKey: @"CountryCode"];
    
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        NSMutableDictionary *regBankDict = [[NSMutableDictionary alloc] init];
        [regBankDict setObject:[jsonValuesDict objectForKey: kBeneficiaryName]  forKey: kBeneficiaryName];
        [regBankDict setObject:[jsonValuesDict objectForKey: kAccountNumber] forKey: kAccountNumber];
        [regBankDict setObject:[jsonValuesDict objectForKey: kIFSCCode] forKey: kIFSCCode];
        [regBankDict setObject: [mSession convertToBankNameKeyCode: [jsonValuesDict objectForKey: kBankID]] forKey: kBankID];
        
        [updateParams setObject: regBankDict forKey: @"REG_Bank"];
    }
    
#pragma mark REG_Company API parameters
    NSMutableDictionary *regCompanyDict = [NSMutableDictionary new];
    [regCompanyDict setObject: [mSession convertToCompanyTypesKeyCode: [jsonValuesDict objectForKey: kCompanyType]]
                       forKey: kCompanyType];
    
    if (!IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        if ([jsonValuesDict objectForKey:kRemarks])
            [regCompanyDict setObject: [jsonValuesDict objectForKey:kRemarks] forKey: kRemarks];
        else
            [regCompanyDict setObject: @"" forKey: kRemarks];
    }
    
    if ([[[mSession lookupTable] objectForKey: @"IsShowCompanyCategory"] boolValue])
    {
        [regCompanyDict setObject: [self convertToCompanyCategoryCode: [jsonValuesDict objectForKey: kCompanyCategory]]
                           forKey: kCompanyCategory]; //customer category
    }
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [regCompanyDict setObject: [NSString stringWithFormat: @"%@ %@", [jsonValuesDict objectForKey:kFirstName], [jsonValuesDict objectForKey:kLastName]]
                           forKey: kCompanyName];
    }
    else
    {
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kCompanyName] forKey: kCompanyName];
    }
    

    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        if ([jsonValuesDict objectForKey: kCompanySource])
            [regCompanyDict setObject: [mSession convertToCompanySourceKeyCode: [jsonValuesDict objectForKey: kCompanySource]]
                               forKey: kCompanySource];
        else
            [regCompanyDict setObject: @"1" forKey: kCompanySource];
    }
    if ([jsonValuesDict objectForKey: kCompanySource])
        [regCompanyDict setObject: [mSession convertToCompanySourceKeyCode: [jsonValuesDict objectForKey: kCompanySource]]
                           forKey: kCompanySource];
//    else
//        [regCompanyDict setObject: self.companyInfo.companySource
//                           forKey: kCompanySource];
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kMobileNumber] forKey: kContactNumber];
    }
    else
    {
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kContactNumber] forKey: kContactNumber];
    }
    //    [regCompanyDict setObject: [mSession convertToLeadStatusKeyCode: [jsonValuesDict objectForKey: kActivationStatus]]
    //                       forKey: kStatus];
    [regCompanyDict setObject: [jsonValuesDict objectForKey:kReferenceCode] forKey: kReferenceCode];
    
    if (!IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [regCompanyDict setObject: [jsonValuesDict objectForKey: kWorkshopQRCode] forKey: kWorkshopQRCode];
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kCompanyEmailAddress] forKey: kCompanyEmailAddress];
        
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kOilChangePerMonth] forKey: kOilChangePerMonth];
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kOilChangePercentage] forKey: kOilChangePercentage];
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kHalfSyntheticOilChanges] forKey: kHalfSyntheticOilChanges];
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kShellHelixQty] forKey: kShellHelixQty];
        
    }
    
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA) && [[mSession profileInfo] hasAddNewRegWorkshop]) {
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kPrimaryOutletID] forKey:kPrimaryOutletID];
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kOutletIDs] forKey:kOutletIDs];
        [regCompanyDict setObject: [self.linkedWorkshopArray firstObject] forKey:kPrimaryOutletCode];
    }
    
    [updateParams setObject: regCompanyDict forKey: @"REG_Company"];
    
#pragma mark REG_CompanyAddress API parameters
    NSMutableDictionary *regCompanyAddressDict = [NSMutableDictionary new];
    [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kAddress1] forKey: kAddress1];
    [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kAddress2] forKey: kAddress2];
    [regCompanyAddressDict setObject: @"1" forKey: @"AddressType"];
    [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kCompanyCity] forKey: kCompanyCity];
    NSString *stateString = [jsonValuesDict objectForKey: kCompanyState];
    [regCompanyAddressDict setObject: [mSession convertToStateKeyCode: stateString stateDetailedListArray:self.stateTable]
                              forKey: kCompanyState];
    [regCompanyAddressDict setObject: @(self.companyAddress.latitude) forKey: @"Latitude"];
    [regCompanyAddressDict setObject: @(self.companyAddress.longitude) forKey: @"Longitude"];
    if(![GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_HONGKONG]) {
        [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kPostalCode] forKey: kPostalCode];
    } else {
        [regCompanyAddressDict setObject: @"" forKey: kPostalCode];
    }
    [regCompanyAddressDict setObject:@"" forKey: @"StreetName"];
    
    [updateParams setObject: regCompanyAddressDict forKey: @"REG_CompanyAddress"];
    
    [updateParams setObject: @{} forKey: @"REG_CompanyWorkHours"];
    [updateParams setObject: @[] forKey: @"REG_CompanyService"];
    
    
#pragma mark REG_Member API parameters
    NSMutableDictionary *regMemberDict = [NSMutableDictionary new];
    //    [regMemberDict setObject: @"" forKey: @"ContactPreferences"];
    [regMemberDict setObject: [jsonValuesDict objectForKey:kFirstName] forKey: kFirstName];
    [regMemberDict setObject: @"1" forKey: @"Gender"];
    [regMemberDict setObject: [jsonValuesDict objectForKey:kLastName] forKey: kLastName];
    [regMemberDict setObject: [jsonValuesDict objectForKey:kMobileNumber] forKey: kMobileNumber];
    [regMemberDict setObject: @"1" forKey: @"RegistrationMode"];
    [regMemberDict setObject: @"1" forKey: @"RegistrationType"];
    [regMemberDict setObject: @([[mSession convertToSalutationKeyCode: [jsonValuesDict objectForKey: kSalutation]] intValue]) forKey: kSalutation];
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
        [regMemberDict setObject: self.member.tradeID forKey: @"TradeID"]; //india uses iM
    else
        [regMemberDict setObject: self.member.leadID forKey: @"LeadID"];
    [regMemberDict setObject: [jsonValuesDict objectForKey:kMobileNumber] forKey: @"UserID"];
    if(!IS_COUNTRY(COUNTRYCODE_INDIA)) {
        [regMemberDict setObject: [jsonValuesDict objectForKey:kContactPreference] forKey: kContactPreference];
        [regMemberDict setObject: [jsonValuesDict objectForKey:kEmailAddress] forKey: kEmailAddress];
        [regMemberDict setObject:self.member.dobMonth forKey: @"DOBMonth"];
        [regMemberDict setObject:self.member.dobDay forKey:@"DOBDay"];
    }
    [updateParams setObject: regMemberDict forKey: @"REG_Member"];
    
    [updateParams setObject: @{@"Status":@(0),
                               } forKey: @"REG_CompanyStatus"];
    
    [updateParams setObject: @([[jsonValuesDict objectForKey: kTncCheckBox] boolValue]) forKey: @"TermCondition"];
    
    
    if ([jsonValuesDict objectForKey: kDigitalSignature] &&
        ![[jsonValuesDict objectForKey: kDigitalSignature] isEqualToString: @""])
        [updateParams setObject: [jsonValuesDict objectForKey: kDigitalSignature] forKey: kDigitalSignature];
    return updateParams;
}

#pragma mark birthdate dropdown delegate methods
-(void)birthdatePressedWithKey:(NSString *)formKey
{
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}

- (void)selectedDate:(NSDate *)selectedDate
{
    //do smething
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //   for API
    [formatter setDateFormat:@"dd"];
    self.member.dobDay = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"MM"];
    self.member.dobMonth = [formatter stringFromDate:selectedDate];
    
    if(kIsRightToLeft) {
        [formatter setDateFormat:@"MMMM"];
    } else {
        [formatter setDateFormat:@"dd MMMM"];
    }
    
    if ([GET_LOCALIZATION isEqualToString: kThai])
        [formatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
    else
        [formatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    NSString *selectedDateString = [formatter stringFromDate:selectedDate];
    
    //add formkey to birthday class if needed
    if(kIsRightToLeft) {
        [self.tableViewDelegate editCell: kDOB withDropdownSelection: [NSString stringWithFormat:@"%@ %@",self.member.dobDay,selectedDateString]];
    } else {
        [self.tableViewDelegate editCell: kDOB withDropdownSelection: selectedDateString];
    }
}


#pragma mark Date methods
-(NSString *) convertDateForDisplay:(NSString *)monthString day:(NSString *) dayString
{
    if ([monthString isEqualToString: BLANK] || [dayString isEqualToString: BLANK])
        return LOCALIZATION(C_PROFILE_DOB_PLACEHOLDER);    //lokalised 4 feb
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    if ([GET_LOCALIZATION isEqualToString: kThai])
        [dateFormat setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
    else
        [dateFormat setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    [dateFormat setDateFormat:@"MMdd"];
    NSDate *date = [dateFormat dateFromString: [NSString stringWithFormat: @"%@%@", monthString, dayString]];
    
    // Convert date object to desired output format
    [dateFormat setDateFormat:@"dd MMMM"];
    return [dateFormat stringFromDate:date];
}

-(void)hyperlinkPressed:(NSString *) link
{
    //kForm_Hyperlinks: @{LOCALIZATION(C_FORM_TNC_IN): @"tnc",
    //                    LOCALIZATION(C_PROFILE_PRIVACY) : @"privacypolicy"
    if ([link isEqualToString: @"tnc"])
    {
        if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
            [mSession pushTNCView: self];
        } else {
            [mSession pushTNCView: self.navigationController.parentViewController];
        }
    }
    else if ([link isEqualToString: @"privacypolicy"])
    {
        if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
            [mSession pushPrivacyView: self];
        } else {
            [mSession pushPrivacyView: self.navigationController.parentViewController];
        }
    }
}
#pragma mark signature delegate
-(void) signaturePressed:(NSString *)key
{
    [mSession pushRUCustomerSignatureWithDelegate: self delegate: self];
}
-(void)didFinishSignature:(UIImage *)image
{
    [self.tableViewDelegate editCell: kDigitalSignature withSignatureImage: image];
}

- (IBAction)backPressed:(UIButton *)sender {
    [self popSelf];
}

#pragma mark textfield with buttons delegate methods
-(void)textfieldCellDidBeginEditingWithKey:(NSString *)key
{
    if ([key isEqualToString: kAccountNumber])
        [self.tableViewDelegate editCell: kAccountNumber toSecureField: NO];
}

-(void)textfieldCellDidEndEditingWithKey:(NSString *)key
{
    if ([key isEqualToString: kIFSCCode])
    {
        [self.tableViewDelegate editCell: kBankID withDropdownSelection: @""];    //Bank Name list
        NSString *ifscCodeString = [[self.tableViewDelegate getJsonValues] objectForKey: kIFSCCode];
        [mWebServiceManager fetchBankDetails: ifscCodeString vc:self];
    }
    else if ([key isEqualToString: kAccountNumber])
        [self.tableViewDelegate editCell: kAccountNumber toSecureField: NO];
}


- (void)textfieldWithButtonPressedWithKey:(NSString *)buttonKey
{
    if([buttonKey isEqualToString:kWorkshopQRCode])
    {
        mQRCodeScanner.delegate = self;
        [mQRCodeScanner showZBarScannerOnVC: self];
    }
    //    else if([buttonKey isEqualToString: kMechanicSharecode])
    //    {
    //        mQRCodeScanner.delegate = self;
    //        [mQRCodeScanner showZBarScannerOnVC: self];
    //    }
    else if([buttonKey isEqualToString:kWorkshopLocation])
    {
        [self locationPressed];
    }
}
- (void)locationPressed {
    [UpdateHUD addMBProgress: self.view withText: @""];
    switch ([CLLocationManager authorizationStatus])
    {
        case kCLAuthorizationStatusAuthorizedAlways:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusNotDetermined:
            [UpdateHUD removeMBProgress: self];
            [self.locationManager requestWhenInUseAuthorization];
            break;
        case kCLAuthorizationStatusDenied:
        {
            [UpdateHUD removeMBProgress: self];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_ALERTVIEW_ERROR) message:LOCALIZATION(C_ALERTVIEW_PERMISSIONERRORMSG) preferredStyle:UIAlertControllerStyleAlert];    //lokalised 4 feb
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_CANCEL) style:UIAlertActionStyleCancel handler:nil];    //lokalised 4 feb
            UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_SETTING) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {    //lokalised 4 feb
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                            UIApplicationOpenSettingsURLString]];
            }];
            
            [alertController addAction:cancelAction];
            [alertController addAction:settingsAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
            break;
        case kCLAuthorizationStatusRestricted:
            [UpdateHUD removeMBProgress: self];
            break;
    }
    
}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [UpdateHUD removeMBProgress: self];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    //most recent location update is at the end of the array.
    CLLocation *location = [locations lastObject];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:6];
    
    NSString *newLatitude   = [formatter stringFromNumber: @(location.coordinate.latitude)];
    NSString *newLongitude  = [formatter stringFromNumber: @(location.coordinate.longitude)];
    
    [self.tableViewDelegate editCell:kWorkshopLocation withTextfieldButtonText:[NSString stringWithFormat: @"%@, %@", newLatitude, newLongitude]];
    
    self.companyAddress.latitude = newLatitude.doubleValue;
    self.companyAddress.longitude = newLongitude.doubleValue;
    
    [self.locationManager stopUpdatingLocation];
    [UpdateHUD removeMBProgress: self];
}

- (void)qrCodeScanSuccess:(NSString *)scanString
{
    [self.tableViewDelegate editCell:kWorkshopQRCode withTextfieldButtonText:[[scanString componentsSeparatedByString:@"="] lastObject]];
}


#pragma mark Webservice Delegate
-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
            SET_LOADSTATE([[response getGenericResponse] objectForKey: @"StateDetailsList"]);
            self.stateTable = [[response getGenericResponse] objectForKey: @"StateDetailsList"];
            
            //have to get state table before setting up form
            [self setupInterface];
            [UpdateHUD removeMBProgress: KEY_WINDOW];
            break;
        case kWEBSERVICE_ADDNEWWORKSHOP:
        {
            {
                [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                    [self setupInterface];
                    if(IS_COUNTRY(COUNTRYCODE_RUSSIA) && [[mSession profileInfo] hasAddNewRegWorkshop]) {
                        [self.delegate didRegisterWorkshop];
                        [self popSelf];
                    }
                    [UpdateHUD removeMBProgress: KEY_WINDOW];
                }];
            }
        }
            break;
        case kWEBSERVICE_TRADE_LOADCOMPANYCATEGORY:
        {
            self.companyCategoryList = [[response getGenericResponse] objectForKey:@"CompanyCategory"];
            if([self.companyCategoryList isKindOfClass:[NSNull class]] || self.companyCategoryList == nil) {
                self.companyCategoryList = [[mSession lookupTable] objectForKey: @"CompanyCategory"];
            }
        }
            break;
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress: KEY_WINDOW];
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
            self.stateTable = GET_LOADSTATE;
            
            [self setupInterface];
            break;
        default:
            break;
    }
}

#pragma mark - Company Category
-(NSArray *) getCompanyCategoryValues {
    NSMutableArray *keyValueArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in self.companyCategoryList)
    {
        [keyValueArray addObject: [dict objectForKey: @"KeyValue"]];
    }
    return keyValueArray;
}

- (NSString *) convertToCompanyCategoryCode: (NSString *)keyValue
{
    for (NSDictionary *dict in self.companyCategoryList)
    {
        if ([keyValue isEqualToString: [dict objectForKey: @"KeyValue"]])
        {
            return [dict objectForKey: @"KeyCode"];
        }
    }
    return @"";
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
