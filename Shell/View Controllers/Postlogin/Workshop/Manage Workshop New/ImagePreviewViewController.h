//
//  ImagePreviewViewController.h
//  Shell
//
//  Created by Jeremy Lua on 29/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ImagePreviewViewController : BaseVC
@property NSString *title;
@property UIImage *previewImage;
@end

NS_ASSUME_NONNULL_END
