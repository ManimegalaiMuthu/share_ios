//
//  ApprovedWorkshopDetails_NewViewController.m
//  Shell
//
//  Created by Jeremy Lua on 29/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "ApprovedWorkshopDetails_NewViewController.h"
#import "FormsTableViewDelegate.h"
#import <MapKit/MapKit.h>
#import "QRCodeScanner.h"
#import "CASDatePickerView.h"

#pragma mark Workshop Profile macros
#define kCompanyType           @"CompanyType"           //Workshop Type
#define kCompanyCategory        @"CompanyCategory"      //Workshop Category
#define kCompanyName            @"CompanyName"          // Registered workshop name
#define kWorkshopQRCode         @"WorkshopQRCode"       //Workshop SHARE Code (for QR Scan)
#define kReferenceCode          @"ReferenceCode"        //Workshop Code
#define kContactNumber          @"ContactNumber"        //landline telephone number
#define kCompanyEmailAddress    @"CompanyEmailAddress"  //email address
#define kWorkingHours           @"WorkingHours"         //working hours

#define kCompanySource           @"CompanySource" //MechanicType use CompanySource. Key only for api submission. Not visible

#pragma mark Owner Profile macros
#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kDOB                @"DOB"
#define kDOBMonth           @"DOBMonth"     //not visible, for Dropdown & api
#define kDOBDay             @"DOBDay"       //not visible, for Dropdown & api
#define kMobileNumber       @"MobileNumber"
#define kEmailAddress       @"EmailAddress" //Owner email Address

#define kMechanicIDBadge       @"MechanicIDBadge"

#pragma mark Address macros
#define kWorkshopLocation   @"Location"
#define kPostalCode         @"PostalCode"
#define kState              @"State"
#define kCompanyCity        @"CompanyCity"      //used for Town
#define kAddress1           @"Address1"
#define kAddress2           @"Address2"

#define kCompanyState   @"CompanyState"     //not visible, for api

#pragma mark Others macros
#define kDigitalSignature          @"DigitalSignature"    //Russia, signature
#define kTermCondition      @"TermCondition"    //India/Russia, checkbox
#define kBankInfoSharing    @"BankInfoSharing"  //India only, Checkbox
#define kContactPreference  @"ContactPreferences"

#define kOutletIDs     @"OutletIDs"
#define kPrimaryOutletID    @"PrimaryOutletID"

#define kManageStaffCTA          @"ManageStaffCTA"
#define kUpdateCTA          @"UpdateCTA"
#define kDeactivateCTA      @"DeactivateCTA"

#define kCustomerCategory @"CustomerCategory"

@interface ApprovedWorkshopDetails_NewViewController () <WebServiceManagerDelegate, CLLocationManagerDelegate, QRCodeScannerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *regFormTableView;

@property NSDictionary *workingHoursDict;
@property NSArray *stateTable;

@property Member *member;
@property CompanyInfo *companyInfo;
@property CompanyAddress *companyAddress;

@property FormsTableViewDelegate *tableViewDelegate;
@property CLLocationManager *locationManager;

@property CASDatePickerView *dateView; //birthdate

@property NSArray *companyCategoryList;
@property NSMutableArray *workshopProfileArray;
@property NSMutableArray *mechanicProfileArray;

@end

@implementation ApprovedWorkshopDetails_NewViewController
@synthesize listView;
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if(GET_PROFILETYPE == kPROFILETYPE_DMR)
    {
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_IN_MANAGEMYMECHANICS) subtitle: @"" size: 15 subtitleSize: 0];        //lokalise 4 feb
    }
    else
    {
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_MYNETWORK) subtitle: @"" size: 15 subtitleSize: 0];       //lokalised
    }        //lokalise 4 feb
    [self setNavBackBtn];
    self.stateTable = GET_LOADSTATE;
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager requestWhenInUseAuthorization];
    
    [[WebServiceManager sharedInstance] loadState: [[mSession lookupTable] objectForKey: @"LookUpVersion"]
                                      countryCode: GET_COUNTRY_CODE
                                               vc: self];
    if([[[mSession lookupTable] objectForKey: @"IsShowCompanyCategory"] boolValue] || [GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_INDIA]) {
        [[WebServiceManager sharedInstance] loadCompanyCategory:self];
    }
}

-(void) setupInterface
{
    self.member = [[Member alloc] initWithData:[self.workshopData objectForKey: @"REG_Member"]];
    self.companyInfo = [[CompanyInfo alloc] initWithData: [self.workshopData objectForKey: @"REG_Company"]];
    self.companyAddress = [[CompanyAddress alloc] initWithData: [self.workshopData objectForKey: @"REG_CompanyAddress"]];
    
    self.workingHoursDict = [self.workshopData objectForKey: @"REG_CompanyWorkHours"];
    
    if (!self.workingHoursDict ||
        [self.workingHoursDict isKindOfClass: [NSNull class]])
        self.workingHoursDict = @{@"WorkHourStart1":   @"0:00",
                                  @"WorkHourEnd1":     @"0:00",
                                  @"WeekDays1":        @"",
                                  @"PublicHoliday":    @"0",
                                  @"WorkHourStart2":   @"0:00",
                                  @"WorkHourEnd2":     @"0:00",
                                  @"WeekDays2":        @"",
                                  };
    self.tableViewDelegate = [[FormsTableViewDelegate alloc] init];
    
    if(GET_PROFILETYPE == kPROFILETYPE_DMR)
    {
        self.mechanicProfileArray = [[NSMutableArray alloc] init];
        [self setupMechanicProfileArray: self.mechanicProfileArray];
        [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_HEADER_MECHANICPROFILE)]; //more than 1 char, to show required fields label         //lokalise 4 feb
        [self.tableViewDelegate.initializingArray addObject: self.mechanicProfileArray];
    }
    else
    {
        self.workshopProfileArray = [[NSMutableArray alloc] init];
        [self setupWorkshopProfileArray: self.workshopProfileArray];
        [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_HEADER_WORKSHOPPROFILE)]; //more than 1 char, to show required fields label        //lokalise 4 feb
        [self.tableViewDelegate.initializingArray addObject: self.workshopProfileArray];

        NSMutableArray *ownerProfileArray = [[NSMutableArray alloc] init];
        [self setupOwnerProfileArray: ownerProfileArray];
        [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_HEADER_OWNERPROFILE)]; //more than 1 char, to show required fields label        //lokalise 4 feb
        [self.tableViewDelegate.initializingArray addObject: ownerProfileArray];
    }
    NSMutableArray *addressArray = [[NSMutableArray alloc] init];
    [self setupAddressArray: addressArray];
    [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_PROFILE_ADDRESS)]; //more than 1 char, to show required fields label        //lokalise 4 feb
    [self.tableViewDelegate.initializingArray addObject: addressArray];
    
    if (IS_COUNTRY(COUNTRYCODE_SAUDI))
    {
        NSMutableArray *contactPrefArray = [NSMutableArray new];
        [self setupContactPrefArray:contactPrefArray];
        [self.tableViewDelegate.headersArray addObject: @""];
        [self.tableViewDelegate.initializingArray addObject:contactPrefArray];
    }
    
    NSMutableArray *lastSectionArray = [NSMutableArray new];
    [self setupLastSectionArray:lastSectionArray];
    [self.tableViewDelegate.headersArray addObject: @""];
    [self.tableViewDelegate.initializingArray addObject:lastSectionArray];
    
    self.regFormTableView.delegate = self.tableViewDelegate;
    self.regFormTableView.dataSource = self.tableViewDelegate;
    
    [self.tableViewDelegate registerNib: self.regFormTableView];
    [self.tableViewDelegate setDelegate: self];
    
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_COUNTRY)        //lokalise 4 feb
                                                   list: [mSession getCountryNames]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
    
    //preload dateview
    dateView = [[CASDatePickerView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_BIRTHDAY) previouslySelected:nil withYearHidden:YES];        //lokalise 4 feb
    
    dateView.delegate = self;
    
    if(@available(iOS 11, *))
    {
        self.regFormTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.regFormTableView setContentInset:UIEdgeInsetsMake(15, 0, 15, 0)];
    
    
    self.regFormTableView.backgroundColor = [UIColor clearColor];
    self.regFormTableView.backgroundView = nil;
    [self setupGoogleAnalytics];
}

-(void) setupGoogleAnalytics
{
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_MANAGEWORKSHOP_APPROVE) screenClass:nil];
}

- (void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        [self.tableViewDelegate editCell: listView.formKey withDropdownSelection: selection.firstObject];
        listView.formKey = nil;
    }
}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

-(void) dropdownPressedWithKey:(NSString *) formKey
{
    [self.view endEditing: YES];
    listView.formKey = formKey;
    
    //Salutation
    if ([formKey isEqualToString: kSalutation])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO];        //lokalise 4 feb
    }
    //Company Source
    else if ([formKey isEqualToString: kCompanySource])
    {
        if (IS_COUNTRY(COUNTRYCODE_INDIA))
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_MECHSOURCE) list: [mSession getCompanyTypes] hasSearchField: NO];        //lokalise 4 feb
        else
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_COMPANYSOURCE) list: [mSession getCompanySource] hasSearchField: NO];        //lokalise 4 feb
    }
    //Workshop Type
    else if ([formKey isEqualToString: kCompanyType])
    {
        if (IS_COUNTRY(COUNTRYCODE_INDIA))
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_MECHTYPE) list: [mSession getCompanyTypes] hasSearchField: NO];        //lokalise 4 feb
        else
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_WORKSHOPTYPE) list: [mSession getCompanyTypes] hasSearchField: NO];        //lokalise 4 feb
    }
//    else if ([formKey isEqualToString: kCompanySource])
//    {
//        [listView setTitleAndList: LOCALIZATION(@"Select Mechanic Type") list: [mSession getCompanySource] hasSearchField: NO];
//    }
    else if ([formKey isEqualToString: kCompanyCategory])
    {
        if (IS_COUNTRY(COUNTRYCODE_INDIA))
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_MECHCATEGORY) list: [self getCompanyCategoryValues] hasSearchField: NO];        //lokalise 4 feb
        else
            [listView setTitleAndList: LOCALIZATION(C_PROFILE_WORKSHOPCATEGORY_PLACEHOLDER) list: [self getCompanyCategoryValues] hasSearchField: NO];        //lokalise 4 feb
    }
    else if ([formKey isEqualToString: kCompanyState])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_STATE) list: [mSession getStateNames: self.stateTable] hasSearchField: YES];        //lokalise 4 feb
    }
    else
    {
        listView.formKey = nil;
        [mAlert showErrorAlertWithMessage: @"Unknown Error"];
        return;
    }
    [self popUpView];
}

- (void)textfieldWithButtonPressedWithKey:(NSString *)buttonKey
{
    if([buttonKey isEqualToString:@"MechanicShareCode"])
    {
        mQRCodeScanner.delegate = self;
        [mQRCodeScanner showZBarScannerOnVC: self];
    }
    else if([buttonKey isEqualToString:kWorkshopQRCode])
    {
        mQRCodeScanner.delegate = self;
        [mQRCodeScanner showZBarScannerOnVC: self];
    }
    else if([buttonKey isEqualToString:kWorkshopLocation])
    {
        [self locationPressed];
    }
}
- (void)locationPressed {
    [UpdateHUD addMBProgress: self.view withText: @""];
    switch ([CLLocationManager authorizationStatus])
    {
        case kCLAuthorizationStatusAuthorizedAlways:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusNotDetermined:
            [UpdateHUD removeMBProgress: self];
            [self.locationManager requestWhenInUseAuthorization];
            break;
        case kCLAuthorizationStatusDenied:
        {
            [UpdateHUD removeMBProgress: self];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_ALERTVIEW_ERROR) message:LOCALIZATION(C_ALERTVIEW_PERMISSIONERRORMSG) preferredStyle:UIAlertControllerStyleAlert];        //lokalise 4 feb
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_CANCEL) style:UIAlertActionStyleCancel handler:nil];        //lokalise 4 feb
            UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_SETTING) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {        //lokalise 4 feb
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                            UIApplicationOpenSettingsURLString]];
            }];
            
            [alertController addAction:cancelAction];
            [alertController addAction:settingsAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
            break;
        case kCLAuthorizationStatusRestricted:
            [UpdateHUD removeMBProgress: self];
            break;
    }
    
}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [UpdateHUD removeMBProgress: self];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    //most recent location update is at the end of the array.
    CLLocation *location = [locations lastObject];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:6];
    
    NSString *newLatitude   = [formatter stringFromNumber: @(location.coordinate.latitude)];
    NSString *newLongitude  = [formatter stringFromNumber: @(location.coordinate.longitude)];
    
    [self.tableViewDelegate editCell:kWorkshopLocation withTextfieldButtonText:[NSString stringWithFormat: @"%@, %@", newLatitude, newLongitude]];
    
    self.companyAddress.latitude = newLatitude.doubleValue;
    self.companyAddress.longitude = newLongitude.doubleValue;
    
    [self.locationManager stopUpdatingLocation];
    [UpdateHUD removeMBProgress: self];
}

-(void) ctaPressedWithKey:(NSString *) ctaKey
{
    if ([ctaKey isEqualToString: kUpdateCTA])
    {
        [self updatePressed];
    }
    else if ([ctaKey isEqualToString: kManageStaffCTA])
    {
        [self manageStaffPressed];
    }
    else if ([ctaKey isEqualToString: kDeactivateCTA])
    {
//        [self popUpYesNoWithTitle: LOCALIZATION(@"Are you sure you want to deactivate this mechanic?")
//                          content: LOCALIZATION(@"Mechanic will not be allowed to redeem any rewards, but he can still access to his account")
        if(GET_PROFILETYPE == kPROFILETYPE_DMR) {
            [self popUpYesNoWithTitle: LOCALIZATION(@"")
                              content: LOCALIZATION(C_POPUP_DEACTIVATEMECH)   //lokalised 4 feb
                          yesBtnTitle: LOCALIZATION(C_FORM_YES)   //lokalised 4 feb
                           noBtnTitle: LOCALIZATION(C_FORM_NO)   //lokalised 4 feb
                         onYesPressed:^(BOOL finished) {
                             [self deactivatePressed];
                         }
                          onNoPressed:^(BOOL finished) {
                              
                          }];
        } else {
            [self popUpYesNoWithTitle: LOCALIZATION(@"")
                              content: LOCALIZATION(C_POPUP_DEACTIVATEOWNER)   //lokalised 15 Feb
                          yesBtnTitle: LOCALIZATION(C_FORM_YES)   //lokalised 4 feb
                           noBtnTitle: LOCALIZATION(C_FORM_NO)   //lokalised 4 feb
                         onYesPressed:^(BOOL finished) {
                             [self deactivatePressed];
                         }
                          onNoPressed:^(BOOL finished) {
                              
                          }];
        }
    }
}

-(NSMutableDictionary *) setupForUpdate
{
    NSMutableDictionary *updateParams = [[NSMutableDictionary alloc] init];
    NSDictionary *jsonValuesDict = [self.tableViewDelegate getJsonValues];
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
        [updateParams setObject: @(NO) forKey: @"BankInfoSharing"];
    
    [updateParams setObject: GET_COUNTRY_CODE forKey: @"CountryCode"];
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [updateParams setObject: @{} forKey: @"REG_Bank"];
        [updateParams setObject: @{} forKey: @"REG_PAN"];
    }
    
    
#pragma mark REG_Company API parameters
    NSMutableDictionary *regCompanyDict = [NSMutableDictionary new];
    
    if ([[[mSession lookupTable] objectForKey: @"IsShowCompanyCategory"] boolValue])
    {
        [regCompanyDict setObject: [self convertToCompanyCategoryCode: [jsonValuesDict objectForKey: kCompanyCategory]]
                           forKey: kCompanyCategory]; //company category
    }
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [regCompanyDict setObject: [mSession convertToCompanyTypesKeyCode: [jsonValuesDict objectForKey: kCompanyType]] forKey: kCompanyType];
        
        [regCompanyDict setObject: [self convertToCompanyCategoryCode: [jsonValuesDict objectForKey: kCompanyCategory]]
                           forKey: kCompanyCategory]; //customer category
        if (GET_PROFILETYPE == kPROFILETYPE_DMR)
            [regCompanyDict setObject: [NSString stringWithFormat: @"%@ %@", [jsonValuesDict objectForKey:kFirstName], [jsonValuesDict objectForKey:kLastName]]
                               forKey: kCompanyName];
        else
            [regCompanyDict setObject: [jsonValuesDict objectForKey:kCompanyName] forKey: kCompanyName];

    }
    else
    {
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kCompanyName] forKey: kCompanyName];
    }
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        if ([jsonValuesDict objectForKey: kCompanySource])
            [regCompanyDict setObject: [mSession convertToCompanySourceKeyCode: [jsonValuesDict objectForKey: kCompanySource]]
                               forKey: kCompanySource];
        else
            [regCompanyDict setObject: @"1" forKey: kCompanySource];
    }
    else
    {
        if ([jsonValuesDict objectForKey: kCompanySource])
            [regCompanyDict setObject: [mSession convertToCompanySourceKeyCode: [jsonValuesDict objectForKey: kCompanySource]]
                               forKey: kCompanySource];
        else
            [regCompanyDict setObject: self.companyInfo.companySource
                               forKey: kCompanySource];
        
    }
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kMobileNumber] forKey: kContactNumber];
    else
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kContactNumber] forKey: kContactNumber];
    
    [regCompanyDict setObject: [jsonValuesDict objectForKey: kWorkshopQRCode] forKey: kWorkshopQRCode];
    if([jsonValuesDict objectForKey: kReferenceCode]) {
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kReferenceCode] forKey: kReferenceCode];
    }
    if (!IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [regCompanyDict setObject: [mSession convertToCompanyTypesKeyCode: [jsonValuesDict objectForKey: kCompanyType]] forKey: kCompanyType];
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kCompanyEmailAddress] forKey: kCompanyEmailAddress];
    }
    
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kPrimaryOutletID] forKey:kPrimaryOutletID];
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kOutletIDs] forKey:kOutletIDs];
    }
    
    [updateParams setObject: regCompanyDict forKey: @"REG_Company"];
    
#pragma mark REG_CompanyAddress API parameters
    NSMutableDictionary *regCompanyAddressDict = [NSMutableDictionary new];
    [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kAddress1] forKey: kAddress1];
    [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kAddress2] forKey: kAddress2];
    [regCompanyAddressDict setObject: @"1" forKey: @"AddressType"];
//    if (!IS_COUNTRY(COUNTRYCODE_INDIA))
        [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kCompanyCity] forKey: kCompanyCity];
    NSString *stateString = [jsonValuesDict objectForKey: kCompanyState];
    [regCompanyAddressDict setObject: [mSession convertToStateKeyCode: stateString stateDetailedListArray:self.stateTable]
                              forKey: kCompanyState];
    [regCompanyAddressDict setObject: @(self.companyAddress.latitude) forKey: @"Latitude"];
    [regCompanyAddressDict setObject: @(self.companyAddress.longitude) forKey: @"Longitude"];
    if(![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_HONGKONG]) {
        [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kPostalCode] forKey: kPostalCode];
    } else {
        [regCompanyAddressDict setObject: @"" forKey: kPostalCode];
    }
    [regCompanyAddressDict setObject:@"" forKey: @"StreetName"];
    
    [updateParams setObject: regCompanyAddressDict forKey: @"REG_CompanyAddress"];
    
    [updateParams setObject: self.workingHoursDict forKey: @"REG_CompanyWorkHours"];
    [updateParams setObject: @[] forKey: @"REG_CompanyService"];
    
#pragma mark REG_Member API parameters
    NSMutableDictionary *regMemberDict = [NSMutableDictionary new];
    [regMemberDict setObject: self.member.contactPreference forKey: @"ContactPreferences"];
    [regMemberDict setObject: [jsonValuesDict objectForKey:kFirstName] forKey: kFirstName];
    [regMemberDict setObject: @"1" forKey: @"Gender"];
    [regMemberDict setObject: [jsonValuesDict objectForKey:kLastName] forKey: kLastName];
    [regMemberDict setObject: [jsonValuesDict objectForKey:kMobileNumber] forKey: kMobileNumber];
    if (!IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [regMemberDict setObject: [jsonValuesDict objectForKey:kEmailAddress] forKey: kEmailAddress];
        [regMemberDict setObject:self.member.dobMonth forKey: @"DOBMonth"];
        [regMemberDict setObject:self.member.dobDay forKey:@"DOBDay"];
    }
        
    [regMemberDict setObject: @"1" forKey: @"RegistrationMode"];
    [regMemberDict setObject: @"1" forKey: @"RegistrationType"];
    [regMemberDict setObject: @([[mSession convertToSalutationKeyCode: [jsonValuesDict objectForKey: kSalutation]] intValue]) forKey: kSalutation];
    [regMemberDict setObject: [self.workshopData objectForKey:@"TradeID"] forKey: @"TradeID"];
    [regMemberDict setObject: [jsonValuesDict objectForKey:kMobileNumber] forKey: @"UserID"];
    
    if(IS_COUNTRY(COUNTRYCODE_SAUDI)) {
        [regMemberDict setObject: [jsonValuesDict objectForKey:kContactPreference] forKey: kContactPreference];
    }
    
    [updateParams setObject: regMemberDict forKey: @"REG_Member"];
    
    [updateParams setObject: @{@"Status":@(0),
                               } forKey: @"REG_CompanyStatus"];
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
        [updateParams setObject: @(NO) forKey: @"TermCondition"];
    else
        [updateParams setObject: @(YES) forKey: @"TermCondition"];
    
    return updateParams;
}

-(void) updatePressed
{
    NSMutableDictionary *updateParams = [self setupForUpdate];
    
    [[WebServiceManager sharedInstance] updateApprovedWorkshop: updateParams vc: self];
}

- (void)manageStaffPressed {
    //push mechanics
    [mSession pushManageMechanic:self.workshopData[@"TradeID"]  vc:self.navigationController.parentViewController];
}

-(void) deactivatePressed
{
    NSMutableDictionary *deactivateParams = [self setupForUpdate];
    NSMutableDictionary *regCompanyDict = [deactivateParams objectForKey: @"REG_Company"];
//    [regCompanyDict setObject: @"4" forKey: @"Status"];
//    [regCompanyDict setObject: @(self.companyInfo.companyType).stringValue forKey: @"CompanyType"];
    
    [deactivateParams setObject: @{@"Status":@(4),
                               } forKey: @"REG_CompanyStatus"];
    [[WebServiceManager sharedInstance] updateApprovedWorkshop: deactivateParams vc: self];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_STATE:
        {
            SET_LOADSTATE([[response getGenericResponse] objectForKey: @"StateDetailsList"]);
            self.stateTable = [[response getGenericResponse] objectForKey: @"StateDetailsList"];
            if (![[[mSession lookupTable] objectForKey: @"IsShowCompanyCategory"] boolValue]) {
                if(![GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_INDIA]){
                     [self setupInterface];
                }
            }
        }
            break;
            
        case kWEBSERVICE_UPDATEWORKSHOP:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                //non-assisted pop back to root screen
                if (self.detailsDelegate)
                    [self.detailsDelegate didUpdateWorkshop]; //details is in tabcontroller
                else
                    [self popSelf];
            }];
        }
            break;
        case kWEBSERVICE_UPDATEIM:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                //non-assisted pop back to root screen
                if (self.detailsDelegate)
                    [self.detailsDelegate didUpdateWorkshop]; //details is in tabcontroller
                else
                    [self popSelf];
            }];
        }
            break;
        case kWEBSERVICE_TRADE_LOADCOMPANYCATEGORY:
        {
            self.companyCategoryList = [[response getGenericResponse] objectForKey:@"CompanyCategory"];
            if([self.companyCategoryList isKindOfClass:[NSNull class]] || self.companyCategoryList == nil) {
                self.companyCategoryList = [[mSession lookupTable] objectForKey: @"CompanyCategory"];
            }
            [self setupInterface];
        }
            break;
        default:
            break;
    }
}


#pragma mark Setup Form methods
-(void) setupRetailerArray: (NSMutableArray *) retailerArray
{
    [retailerArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_CUSTOMERCATEGORY),       //lokalise 4 feb
                                kForm_Type: @(kREGISTRATION_DROPDOWN),
                                kForm_Placeholder: LOCALIZATION(C_DROPDOWN_CUSTOMERCATEGORY),       //lokalise 4 feb
                                kForm_Key: kCustomerCategory,
                                kForm_Value: [self convertCodeToValue: self.companyInfo.companyCategory],
                                }];
    
    [retailerArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SALUTATION),       //lokalise 4 feb
                                kForm_Type: @(kREGISTRATION_DROPDOWN),
                                kForm_Placeholder: LOCALIZATION(C_PROFILE_SALUTATION_PLACEHOLDER),       //lokalise 4 feb
                                kForm_Key: kSalutation,
                                kForm_Value: [mSession convertToSalutation: self.member.salutation],
                                }];
    
    [retailerArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_FIRSTNAME),       //lokalise 4 feb
                                kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                kForm_Key: kFirstName,
                                kForm_Value: self.member.firstName,
                                }]; //first name
    
    
    [retailerArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_LASTNAME),       //lokalise 4 feb
                                kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                kForm_Key: kLastName,
                                kForm_Value: self.member.lastName,
                                }]; //last name
    
    [retailerArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_MOBILENUM),      //lokalise 4 feb
                                kForm_Type: @(kREGISTRATION_MOBNUM),
                                kForm_Key: kMobileNumber,
                                kForm_TextfieldEnabled: @(NO),
                                kForm_CountryCode: [mSession convertCountryCodeToCountryName: self.member.countryCode],
                                kForm_MobileNumber: self.member.mobileNumber,
                                }];   //Mobile number
    
    [retailerArray addObject:@{kForm_Title: LOCALIZATION(C_FORM_RETAILERIDBADGE),       //lokalise 4 feb
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_Key: kWorkshopQRCode,
                               kForm_TextfieldEnabled: @(NO),
                               kForm_Value: @"",
                               }];
}

-(void) setupWorkshopProfileArray: (NSMutableArray *) workshopProfileArray
{
//    if (IS_COUNTRY(COUNTRYCODE_INDIA))
//    {
//        [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_COMPANYSOURCE),
//                                           kForm_Type: @(kREGISTRATION_DROPDOWN),
//                                           kForm_Key: kCompanySource,
//                                           kForm_Value: [self.companyInfo.companySource boolValue] ? [mSession convertToCompanySource: [self.companyInfo.companySource intValue]] : LOCALIZATION(C_PROFILE_COMPANYSOURCE_PLACEHOLDER),
//                                           kForm_Enabled: @(NO),
//                                           }];
//    }
    
    [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPTYPE),   //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_DROPDOWN),
                                       kForm_Placeholder: LOCALIZATION(C_PROFILE_WORKSHOPTYPE_PLACEHOLDER),   //lokalise 4 feb
                                       kForm_Key: kCompanyType,
                                       kForm_Value: self.companyInfo.companyType > 0 ? [mSession convertToCompanyTypes: self.companyInfo.companyType] : LOCALIZATION(C_PROFILE_WORKSHOPTYPE_PLACEHOLDER),   //lokalise 4 feb
                                       kForm_Enabled: @(IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                                       }];
    
    if ([[[mSession lookupTable] objectForKey: @"IsShowCompanyCategory"] boolValue] || IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        NSString *companyCategoryString;
        if ([self.companyInfo.companySource boolValue])
            companyCategoryString = [self convertCodeToValue: self.companyInfo.companyCategory];
        else
        {
                companyCategoryString = LOCALIZATION(C_PROFILE_WORKSHOPCATEGORY_PLACEHOLDER);   //lokalise 4 feb
        }
        
        [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPCATEGORY),   //lokalise 4 feb
                                           kForm_Type: @(kREGISTRATION_DROPDOWN),
                                           kForm_Placeholder: LOCALIZATION(C_PROFILE_WORKSHOPCATEGORY_PLACEHOLDER),   //lokalise 4 feb
                                           kForm_Key: kCompanyCategory,
                                           kForm_Value: companyCategoryString,
                                           kForm_Enabled: @(NO),
                                           }];
    }
    
    [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_COMPANYNAME),   //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                       kForm_Key: kCompanyName,
                                       kForm_Value: self.companyInfo.companyName,
//                                       kForm_Enabled: @(IS_COUNTRY(COUNTRYCODE_RUSSIA)), //russia editable
                                       }]; //company name
    
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        
        NSArray *linkedOutletMappingListArray = [self.workshopData objectForKey:@"OutletMappingList"];
        
        if(![linkedOutletMappingListArray isKindOfClass:[NSNull class]]) {
            NSMutableArray *linkedWorkshopArray = [[NSMutableArray alloc] init];
            
            for(NSDictionary *dict in linkedOutletMappingListArray) {
                [linkedWorkshopArray addObject:[dict objectForKey:@"OutletCode"]];
            }
        
            [workshopProfileArray addObject:@{
                                          kForm_Title : LOCALIZATION(C_FORM_LINKEDWORKSHOP),
                                          kForm_Type : @(kFORM_LINKEDWORKSHOP),
                                          kForm_LinkedWorkshopDict : linkedWorkshopArray,
                                          kForm_Key: kOutletIDs,
                                          kForm_Enabled: @(NO),
                                          }];
        } else {
            [workshopProfileArray addObject:@{
                                              kForm_Title : LOCALIZATION(C_FORM_LINKEDWORKSHOP),
                                              kForm_Type : @(kFORM_LINKEDWORKSHOP),
                                              kForm_LinkedWorkshopDict : @[],
                                              kForm_Key: kOutletIDs,
                                              kForm_Enabled: @(NO),
                                              }];
        }
        
        [workshopProfileArray addObject:@{
                                          kForm_Title : LOCALIZATION(C_FORM_POSTAXID),
                                          kForm_Type : @(kREGISTRATION_TEXTFIELD),
                                          kForm_Key: kPrimaryOutletID,
                                          kForm_Optional: @(!IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                                          kForm_Enabled: @(NO),
                                          kForm_Value: self.companyInfo.posTaxID,
                                          }];
    }
    
    [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPSHARECODE),   //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                                       kForm_Icon: @"icon-code",
                                       kForm_Key: kWorkshopQRCode,
                                       kForm_Value:  self.companyInfo.workshopQRCode,
                                       kForm_Enabled: @(!IS_COUNTRY(COUNTRYCODE_INDONESIA)),
                                       kForm_TextfieldEnabled: @(!IS_COUNTRY(COUNTRYCODE_INDONESIA)),
                                       kForm_Optional: @(!(IS_COUNTRY(COUNTRYCODE_MALAYSIA) ||
                                                           IS_COUNTRY(COUNTRYCODE_VIETNAM) ||
                                                           IS_COUNTRY(COUNTRYCODE_HONGKONG) ||
                                                           IS_COUNTRY(COUNTRYCODE_SAUDI))), //Required field for Malaysia Russia
                                       }];   //Workshop SHARE Code
    
    if (!(IS_COUNTRY(COUNTRYCODE_RUSSIA) &&
          IS_COUNTRY(COUNTRYCODE_INDIA)))
    {
        if(!IS_COUNTRY(COUNTRYCODE_INDIA) || (IS_COUNTRY(COUNTRYCODE_INDIA) && (self.companyInfo.companyType == 2 || self.companyInfo.companyType == 4))) {
            [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPCODE),   //lokalise 4 feb
                                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                               kForm_Key: kReferenceCode,
                                               kForm_Value: self.companyInfo.referenceCode,
                                               kForm_Enabled: @(NO),
                                               kForm_Optional: @(YES),
                                            }]; //workshop code
        }
    }
    
    if (!IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_APPROVEDCONTACTNUMBER),   //lokalise 4 feb
                                           kForm_Placeholder: LOCALIZATION(C_PROFILE_CONTACTNUMBER_PLACEHOLDER),   //lokalise 4 feb
                                           kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                           kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                           kForm_Key: kContactNumber,
    //                                       kForm_Enabled: @(IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                                           kForm_Enabled: @(YES),
                                           kForm_Value: self.companyInfo.contactNumber,
                                           kForm_Optional: @(!IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                                           }]; //landline telephone number
        
        [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPEMAILADDRESS),   //lokalise 4 feb
                                           kForm_Placeholder: LOCALIZATION(C_PROFILE_WORKSHOPEMAILADDRESS),   //lokalise 4 feb
                                           kForm_Keyboard: @(UIKeyboardTypeEmailAddress),
                                           kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                           kForm_Key: kCompanyEmailAddress,
    //                                       kForm_Enabled: @(IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                                           kForm_Enabled: @(YES),
                                           kForm_Value: self.companyInfo.companyEmailAddress,
                                           kForm_Optional: @(YES),
                                           }]; //company name
        
        [workshopProfileArray addObject:@{kForm_Title: LOCALIZATION(C_PROFILE_WORKINGHOURS),   //lokalise 4 feb
                                          kForm_Type: @(kREGISTRATION_WORKINGHOURS),
                                          kForm_Key: kWorkingHours,
                                          kForm_Enabled: @(YES), //MUST BE PRESENT
                                          kForm_Value: @"", //using kForm_WorkingHours instead
                                          kForm_WorkingHours: [self.workingHoursDict isKindOfClass:[NSNull class]] ? @"" : self.workingHoursDict,
                                          kForm_Optional: @(YES),
                                          }];
    }
}

-(void) setupOwnerProfileArray: (NSMutableArray *) ownerProfileArray
{
    [ownerProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SALUTATION),   //lokalise 4 feb
                                    kForm_Type: @(kREGISTRATION_DROPDOWN),
                                    kForm_Placeholder: LOCALIZATION(C_PROFILE_SALUTATION_PLACEHOLDER),   //lokalise 4 feb
                                    kForm_Key: kSalutation,
                                    kForm_Value: [mSession convertToSalutation: self.member.salutation],
//                                    kForm_Enabled: @(IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                                    kForm_Enabled: @(YES),
                                    kForm_Optional: @(YES),
                                       }];
    
    [ownerProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_FIRSTNAME),   //lokalise 4 feb
                                    kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                    kForm_Key: kFirstName,
                                    kForm_Value: self.member.firstName,
//                                    kForm_Enabled: @(IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                                    kForm_Enabled: @(YES),
                                       }]; //first name
    
    
    [ownerProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_LASTNAME),   //lokalise 4 feb
                                    kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                    kForm_Key: kLastName,
                                    kForm_Value: self.member.lastName,
//                                    kForm_Enabled: @(IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                                    kForm_Enabled: @(YES),
                                       }]; //last name
    
    [ownerProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_DOB),   //lokalise 4 feb
                                    kForm_Type: @(kREGISTRATION_BIRTHDATE),
                                    kForm_Key: kDOB,
                                    kForm_Value: [self convertDateForDisplay: self.member.dobMonth day:self.member.dobDay],
//                                    kForm_Enabled:  @(IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                                    kForm_Enabled: @(YES),
                                    kForm_Optional: @(YES),
                            }];
    
    [ownerProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_MOBILENUM),   //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_MOBNUM),
                                       kForm_Key: kMobileNumber,
                                       kForm_TextfieldEnabled: @(YES), //if not approved, allow editing
                                       kForm_CountryCode: [mSession convertCountryCodeToCountryName: GET_COUNTRY_CODE],
                                       kForm_MobileNumber: self.member.mobileNumber,
//                                       kForm_Enabled: @(IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                                        kForm_Enabled: @(NO),
                                       }];   //Mobile number
    if (!IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [ownerProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_OWNEREMAILADDRESS),   //lokalise 4 feb
                                           kForm_Placeholder: LOCALIZATION(C_PROFILE_WORKSHOPEMAILADDRESS),   //lokalise 4 feb
                                           kForm_Keyboard: @(UIKeyboardTypeEmailAddress),
                                           kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                           kForm_Key: kEmailAddress,
    //                                       kForm_Enabled: @(IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                                        kForm_Enabled: @(YES),
                                           kForm_Value: self.member.emailAddress,
                                           kForm_Optional: @(!IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                                           }]; //company email
    }

}


-(void) setupMechanicProfileArray: (NSMutableArray *) mechanicProfileArray
{
//    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(@"Mechanic Source"),
//                                       kForm_Type: @(kREGISTRATION_DROPDOWN),
//                                       kForm_Key: kCompanySource,
//                                       kForm_Value: [self.companyInfo.companySource boolValue] ? [mSession convertToCompanySource: [self.companyInfo.companySource intValue]] : LOCALIZATION(@"Select Mechanic Source"),
//                                       kForm_Enabled: @(NO),
//                                       }];
    
    NSString *mechanicTypeString = [mSession convertToCompanyTypes: self.companyInfo.companyType];
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_MECHTYPE),   //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_DROPDOWN),
                                       kForm_Placeholder: LOCALIZATION(C_DROPDOWN_MECHTYPE),   //lokalise 4 feb
                                       kForm_Key: kCompanyType,
                                       kForm_Value: [mechanicTypeString length] > 0 ? mechanicTypeString : LOCALIZATION(C_DROPDOWN_MECHTYPE),   //lokalise 4 feb
                                       kForm_Enabled: @(NO),
                                       }];
    
    NSString *mechanicCategoryString = [self convertCodeToValue: self.companyInfo.companyCategory];
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_MECHCATEGORY),      //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_DROPDOWN),
                                       kForm_Placeholder: LOCALIZATION(C_DROPDOWN_MECHCATEGORY),      //lokalise 4 feb
                                       kForm_Key: kCompanyCategory,
                                       kForm_Value:  [mechanicCategoryString length] > 0 ? mechanicCategoryString : LOCALIZATION(C_DROPDOWN_MECHCATEGORY),      //lokalise 4 feb
                                       kForm_Enabled: @(NO),
                                       }];
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_MECHSHARECODE),      //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                                       kForm_Icon: @"icon-code",
                                       kForm_Key: kWorkshopQRCode,
                                       kForm_Value:  self.companyInfo.workshopQRCode,
                                       kForm_Enabled: @(YES),
                                       kForm_TextfieldEnabled: @(YES),
                                       kForm_Optional: @(YES),
                                       }];   //Workshop SHARE Code
    
    if(self.companyInfo.companyType == 2 || self.companyInfo.companyType == 4) {
        [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPCODE),      //lokalise 4 feb
                                           kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                           kForm_Key: kReferenceCode,
                                           kForm_Value:  self.companyInfo.referenceCode,
                                           kForm_Optional: @(YES),
                                    }];   //Workshop SHARE Code
    }
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SALUTATION),      //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_DROPDOWN),
                                       kForm_Placeholder: LOCALIZATION(C_PROFILE_SALUTATION_PLACEHOLDER),      //lokalise 4 feb
                                       kForm_Key: kSalutation,
                                       kForm_Value: [mSession convertToSalutation: self.member.salutation],
                                       kForm_Enabled: @(YES),
                                       kForm_Optional: @(YES),
                                       }];
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_FIRSTNAME),      //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                       kForm_Key: kFirstName,
                                       kForm_Value: self.member.firstName,
                                       kForm_Enabled: @(YES),
                                       }]; //first name
    
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_LASTNAME),      //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                       kForm_Key: kLastName,
                                       kForm_Value: self.member.lastName,
                                       kForm_Enabled: @(YES),
                                       }]; //last name
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_MOBILENUM),      //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_MOBNUM),
                                       kForm_Key: kMobileNumber,
                                       kForm_TextfieldEnabled: @(YES), //if not approved, allow editing
                                       kForm_CountryCode: [mSession convertCountryCodeToCountryName: GET_COUNTRY_CODE],
                                       kForm_MobileNumber: self.member.mobileNumber,
                                       kForm_Enabled: @(NO),
                                       }];   //Mobile number
}

-(void) setupAddressArray: (NSMutableArray *) addressArray
{
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_LOCATION),      //lokalise 4 feb
                               kForm_Type: @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                               kForm_TextfieldEnabled: @(NO),
                               kForm_Icon: @"icon-location",
                               kForm_Key: kWorkshopLocation,
//                               kForm_Enabled: @(!IS_COUNTRY(COUNTRYCODE_INDIA)),
                               kForm_Enabled: @(YES),
                               kForm_Value: [NSString stringWithFormat: @"%f, %f", self.companyAddress.latitude, self.companyAddress.longitude],
                               }];    //location
    
    if(![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_HONGKONG]) {
        [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_POSTALCODE),      //lokalise 4 feb
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_Key: kPostalCode,
                                   kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                   kForm_HasDelegate : @(IS_COUNTRY(COUNTRYCODE_INDIA)), //textfield delegate for postal code. india search address with postal
                                   //                               kForm_Enabled: @(!(IS_COUNTRY(COUNTRYCODE_MALAYSIA) ||
                                   //                                                  IS_COUNTRY(COUNTRYCODE_VIETNAM) ||
                                   //                                                  IS_COUNTRY(COUNTRYCODE_RUSSIA))),
                                   kForm_Enabled: @(NO),
                                   kForm_Optional: @(!(IS_COUNTRY(COUNTRYCODE_MALAYSIA) &&
                                                       IS_COUNTRY(COUNTRYCODE_VIETNAM))),
                                   kForm_Value: self.companyAddress.postalCode,
                                   }]; //postal code
    }
    
    NSString *stateString = [mSession convertToStateName:self.companyAddress.companyState stateDetailedListArray:self.stateTable];
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_STATE),      //lokalise 4 feb
                               kForm_Type: @(kREGISTRATION_DROPDOWN),
                               kForm_Placeholder: LOCALIZATION(C_PROFILE_STATE_PLACEHOLDER),      //lokalise 4 feb
                               kForm_Key: kCompanyState,
                               kForm_Value: ([stateString length] > 0) ? stateString : LOCALIZATION(C_PROFILE_STATE_PLACEHOLDER),
//                               kForm_Enabled: @(IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                               kForm_Enabled: @(NO),
                               }];
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_TOWN),      //lokalise 4 feb
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_Key: kCompanyCity,
                               kForm_Value: self.companyAddress.companyCity,
//                               kForm_Enabled: @(IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                               kForm_Optional: @(!(IS_COUNTRY(COUNTRYCODE_RUSSIA) ||
                                                   IS_COUNTRY(COUNTRYCODE_INDIA))),
                               kForm_Enabled: @(NO),
                               }];  //Town
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_ADDRESSLINE1),      //lokalise 4 feb
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_Key: kAddress1,
                               kForm_Value: self.companyAddress.address1,
//                               kForm_Enabled: @(IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                               kForm_Enabled: @(NO),
                               }]; //address line 1
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_ADDRESSLINE2),      //lokalise 4 feb
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_Key: kAddress2,
                               kForm_Value: self.companyAddress.address2,
                               kForm_Optional: @(YES),
//                               kForm_Enabled: @(IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                               kForm_Enabled: @(NO),
                               }];
}

-(void) setupContactPrefArray: (NSMutableArray*) contactPrefArray {
    //contact preference
    [contactPrefArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_CONTACT_PREFERENCE),  //lokalised 4 feb
                                   kForm_Type: @(kREGISTRATION_CONTACTPREF),
                                   kForm_Key: kContactPreference,
                                   kForm_Enabled: @(YES),
                                   kForm_BackgroundColour: COLOUR_WHITE,
                                   kForm_Value: self.member.contactPreference,
                                   kForm_Optional: @(NO),
                                   }];
}


-(void) setupLastSectionArray: (NSMutableArray *)lastSectionArray
{
    
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        if (self.companyInfo.hasDSRManageStaff) {
            [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_MANAGEMECHANIC),       //lokalise 4 feb
                                                  kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                                  kForm_Key: kManageStaffCTA,
                                                  kForm_Enabled: @(YES),
                                                  kForm_TextColour: COLOUR_VERYDARKGREY,
                                                  kForm_BackgroundColour: COLOUR_YELLOW,
                                               }]; //Manage Staff
        }
        
        [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_UPDATE),       //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                       kForm_Key: kUpdateCTA,
                                       kForm_Enabled: @(YES),
                                       kForm_TextColour: COLOUR_WHITE,
                                       kForm_BackgroundColour: COLOUR_RED,
                                    }]; //Update
        
//        [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_MECHANICDETAILS_DEACTIVATE),       //lokalise 4 feb
//                                       kForm_Type: @(kREGISTRATION_BUTTONCELL),
//                                       kForm_Key: kDeactivateCTA,
//                                       kForm_Enabled: @(YES),
//                                       kForm_TextColour: COLOUR_VERYDARKGREY,
//                                       kForm_BackgroundColour: COLOUR_YELLOW,
//                                    }]; //Deactivate
        
    } else {
        if(self.companyInfo.hasDSRManageStaff && !(IS_COUNTRY(COUNTRYCODE_INDIA))) {
            [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_MANAGEMECHANIC),       //lokalise 4 feb
                                           kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                           kForm_Key: kManageStaffCTA,
                                           kForm_Enabled: @(YES),
                                           kForm_TextColour: COLOUR_VERYDARKGREY,
                                           kForm_BackgroundColour: COLOUR_YELLOW,
                                    }]; //Manage Staff
        }
        
        if([[mSession profileInfo] hasBtnUpdate]) {
            [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_UPDATE),       //lokalise 4 feb
                                           kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                           kForm_Key: kUpdateCTA,
                                           kForm_Enabled: @(YES),
                                           kForm_TextColour: COLOUR_WHITE,
                                           kForm_BackgroundColour: COLOUR_RED,
                                    }]; //Update
        }
        
        if([[mSession profileInfo] hasBtnDeactivate]) {
            [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_MECHANICDETAILS_DEACTIVATE),       //lokalise 4 feb
                                           kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                           kForm_Key: kDeactivateCTA,
                                           kForm_Enabled: @(YES),
                                           kForm_TextColour: COLOUR_VERYDARKGREY,
                                           kForm_BackgroundColour: COLOUR_YELLOW,
                                        }]; //Deactivate
        }
            
    }
    
}


#pragma mark Working Hours Delegate methods
-(void) didPressWorkingHours
{
    if (!self.workingHoursDict ||
        [self.workingHoursDict isKindOfClass: [NSNull class]])
        self.workingHoursDict = @{@"WorkHourStart1":   @"0:00",
                                  @"WorkHourEnd1":     @"0:00",
                                  @"WeekDays1":        @"",
                                  @"PublicHoliday":    @"0",
                                  @"WorkHourStart2":   @"0:00",
                                  @"WorkHourEnd2":     @"0:00",
                                  @"WeekDays2":        @"",
                                  };
    
    //let tab bar push instead
    [mSession pushWorkingHoursView: self.navigationController.parentViewController workingHoursData: self.workingHoursDict];
}

-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict
{
    //assign working hours
    self.workingHoursDict = workingHoursDict;
    
    [self.tableViewDelegate updateWorkingHours: self.workingHoursDict];
    
    [self.regFormTableView reloadData];
    
    //    NSArray *fullWorkWeekDays;
    //    NSArray *halfWorkWeekDays;
    //
    //    if (![[workingHoursDict objectForKey: @"WeekDays1"] isEqualToString: @""])
    //        fullWorkWeekDays = [[workingHoursDict objectForKey: @"WeekDays1"] componentsSeparatedByString: @","];
    //
    //    if (![[workingHoursDict objectForKey: @"WeekDays2"] isEqualToString: @""])
    //        halfWorkWeekDays = [[workingHoursDict objectForKey: @"WeekDays2"] componentsSeparatedByString: @","];
}

#pragma mark QR Code Scanner delegate
- (void)qrCodeScanSuccess:(NSString *)scanString
{
    [self.tableViewDelegate editCell:kWorkshopQRCode withTextfieldButtonText:[[scanString componentsSeparatedByString:@"="] lastObject]];
}

#pragma mark birthdate dropdown delegate methods
-(void)birthdatePressedWithKey:(NSString *)formKey
{
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}

- (void)selectedDate:(NSDate *)selectedDate
{
    //do smething
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //   for API
    [formatter setDateFormat:@"dd"];
    self.member.dobDay = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"MM"];
    self.member.dobMonth = [formatter stringFromDate:selectedDate];
    
    if(kIsRightToLeft) {
        [formatter setDateFormat:@"MMMM"];
    } else {
        [formatter setDateFormat:@"dd MMMM"];
    }
    
    if ([GET_LOCALIZATION isEqualToString: kThai])
        [formatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
    else
        [formatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    NSString *selectedDateString = [formatter stringFromDate:selectedDate];
    
    //add formkey to birthday class if needed
    if(kIsRightToLeft) {
        [self.tableViewDelegate editCell: kDOB withDropdownSelection: [NSString stringWithFormat:@"%@ %@",self.member.dobDay,selectedDateString]];
    } else {
        [self.tableViewDelegate editCell: kDOB withDropdownSelection: selectedDateString];
    }
}

#pragma mark Date methods

-(NSString *) convertDateForDisplay:(NSString *)monthString day:(NSString *) dayString
{
    if ([monthString isEqualToString: BLANK] || [dayString isEqualToString: BLANK])
        return LOCALIZATION(C_PROFILE_DOB_PLACEHOLDER);       //lokalise 4 feb
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    if ([GET_LOCALIZATION isEqualToString: kThai])
        [dateFormat setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
    else
        [dateFormat setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    [dateFormat setDateFormat:@"MMdd"];
    NSDate *date = [dateFormat dateFromString: [NSString stringWithFormat: @"%@%@", monthString, dayString]];
    
    // Convert date object to desired output format
    [dateFormat setDateFormat:@"dd MMMM"];
    return [dateFormat stringFromDate:date];
}


#pragma mark signature delegate
-(void) signaturePressed:(NSString *)key
{
    [mSession pushRUCustomerSignatureWithDelegate: self.navigationController.parentViewController delegate: self];
}
-(void)didFinishSignature:(UIImage *)image
{
    [self.tableViewDelegate editCell: kDigitalSignature withSignatureImage: image];
}

#pragma mark - Company Category
-(NSArray *) getCompanyCategoryValues {
    NSMutableArray *keyValueArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in self.companyCategoryList)
    {
        [keyValueArray addObject: [dict objectForKey: @"KeyValue"]];
    }
    return keyValueArray;
}

- (NSString *) convertToCompanyCategoryCode: (NSString *)keyValue
{
    for (NSDictionary *dict in self.companyCategoryList)
    {
        if ([keyValue isEqualToString: [dict objectForKey: @"KeyValue"]])
        {
            return [dict objectForKey: @"KeyCode"];
        }
    }
    return @"";
}

- (NSString *) convertCodeToValue:(NSInteger) keyCode
{
    for (NSDictionary *dict in self.companyCategoryList)
    {
        if (keyCode == [[dict objectForKey: @"KeyCode"] intValue])
        {
            return [dict objectForKey: @"KeyValue"];
        }
    }
    return @"";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
