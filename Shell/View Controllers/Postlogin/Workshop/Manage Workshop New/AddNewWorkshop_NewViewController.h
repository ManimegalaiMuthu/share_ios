//
//  AddNewWorkshop_NewViewController.h
//  Shell
//
//  Created by Jeremy Lua on 12/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@protocol AddNewWorkshop_NewDelegate <NSObject>
-(void)didRegisterWorkshop;
@end

@interface AddNewWorkshop_NewViewController: BaseVC
@property id<AddNewWorkshop_NewDelegate> delegate;
@property NSArray *linkedWorkshopArray;
@property NSString *primaryOutletID;
@property NSString *registeredName;
-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict;

@end

NS_ASSUME_NONNULL_END
