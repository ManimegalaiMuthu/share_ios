//
//  ImagePreviewViewController.m
//  Shell
//
//  Created by Jeremy Lua on 29/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "ImagePreviewViewController.h"

@interface ImagePreviewViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@end

@implementation ImagePreviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title: self.title subtitle: @"" size: 15 subtitleSize: 0];
    [self setNavBackBtn];
    
    [self.imgView setImage: self.previewImage];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
