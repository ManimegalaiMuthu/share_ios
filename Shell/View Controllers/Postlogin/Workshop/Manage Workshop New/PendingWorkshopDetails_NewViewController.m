//
//  PendingWorkshopDetails_NewViewController.m
//  Shell
//
//  Created by Jeremy Lua on 7/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <MapKit/MapKit.h>
//#import "WorkshopDetailsViewController.h"
#import "CASDatePickerView.h"
#import "WorkingHoursViewController.h"
#import "QRCodeScanner.h"
#import "GalleryUploadViewController.h"
#import "RUCustomerSignatureViewController.h"
#import "PhotoTakingViewController.h"

#import "PendingWorkshopDetails_NewViewController.h"
#import "FormsTableViewDelegate.h"
#import "PopUpYesNoViewController.h"
#import "PopupTextViewController.h"
#import "PopupInfoViewController.h"

#define kUserID             @"UserID"
#define kDSRCode            @"DSRCode"
#define kUserPassword       @"UserPassword"
#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kEmailAddress       @"EmailAddress"
#define kDOB                @"DOB"
#define kDOBMonth           @"DOBMonth"
#define kDOBDay             @"DOBDay"
#define kCountryCode        @"CountryCode"
#define kReferenceCode          @"ReferenceCode"        //Workshop Code
#define kWorkshopQRCode         @"WorkshopQRCode"       //Workshop SHARE Code (for QR Scan) //mechanic share code here
#define kCompanySource          @"CompanySource"
#define kCompanyType            @"CompanyType"
#define kCompanyCategory            @"CompanyCategory" //to change again?
#define kCompanyName            @"CompanyName"
#define kCompanyEmailAddress    @"CompanyEmailAddress"
#define kCompanyTier            @"CompanyTier"
#define kContactNumber          @"ContactNumber"
#define kOilChangePerMonth      @"OilChangePerMonth"
#define kOilChangePercentage    @"OilChangePercentage"
#define kHalfSyntheticOilChanges    @"HalfSyntheticOilChanges"
#define kShellHelixQty          @"ShellHelixQty"
#define kStatus                 @"Status"
#define kRemarks                @"Remarks"
#define kAddressType    @"AddressType"
#define kAddress1       @"Address1"
#define kAddress2       @"Address2"
#define kStreetName     @"StreetName"
#define kCompanyCity    @"CompanyCity"
#define kCompanyState   @"CompanyState"
#define kPostalCode     @"PostalCode"
#define kLongitude      @"Longitude"
#define kLatitude       @"Latitude"

#define kWorkingHours       @"WorkingHours"
#define kWorkshopLocation   @"Location"

#define kApprovalStatus         @"ApprovalStatus"
#define kActivationStatus       @"ActivationStatus"

//MechanicType use CompanySource
#define kMechanicType           @"CompanySource" //MechanicType use CompanySource
#define kCompanyCategory       @"CompanyCategory"

//Bank Details
#define kValidateCheckboxes     @"ValidateCheckboxes"
#define kPANCardId              @"PANCardId"
#define kPANCardImage           @"PANCardImage"
#define kDrivingLicense         @"DrivingLicenseID"
#define kDrivingLicenseImage    @"DrivingLicenseImage"
#define kVoterID                @"VoterID"
#define kVoterIDImage           @"VoterIDImage"
#define kBeneficiaryName    @"BeneficiaryName"
#define kBankID             @"BankID"

#define kAccountNumber      @"AccountNumber"
#define kConfirmAccountNumber      @"ConfirmAccountNumber"

#define kIFSCCode           @"IFSCCode"
#define kBeneficiaryImage   @"BeneficiaryImage" //for UI only
#define kContactPreference  @"ContactPreferences"
//last section
#define kDigitalSignature          @"DigitalSignature"    //Russia, signature
#define kBankCheckBox   @"BankCheckBox" //for checkbox only
#define kTncCheckBox   @"TncCheckBox" //for checkbox only

#define kRegisterCTA         @"RegisterCTA"
#define kUpdateCTA           @"UpdateCTA"


@interface PendingWorkshopDetails_NewViewController () <WorkingHoursViewDelegate, WebServiceManagerDelegate, PhotoTakingVCDelegate, FormsTableViewDelegate, CLLocationManagerDelegate, QRCodeScannerDelegate, CommonListDelegate, RUCustomerSignatureDelegate>

@property (weak, nonatomic) IBOutlet UITableView *regFormTableView;

@property FormsTableViewDelegate *tableViewDelegate;


@property CASDatePickerView *dateView;

@property NSDictionary *workingHoursDict;
@property NSArray *stateTable;

@property Member *member;
@property CompanyInfo *companyInfo;
@property CompanyAddress *companyAddress;
@property NSString *signatureString;

@property CLLocationManager *locationManager;

@property NSArray *companyCategoryList;
@property NSMutableArray *workshopSectionArray;
@property NSMutableArray *mechanicProfileArray;

@property BOOL isValidated;

@end

@implementation PendingWorkshopDetails_NewViewController
@synthesize listView;
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.stateTable = GET_LOADSTATE;
    if(GET_PROFILETYPE == kPROFILETYPE_DMR)
    {
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_IN_MANAGEMYMECHANICS) subtitle: @"" size: 15 subtitleSize: 0]; //lokalise 4 feb
    }
    else
    {
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_MYNETWORK) subtitle: @"" size: 15 subtitleSize: 0]; //lokalise 4 feb
    }
    [self setNavBackBtn];
    
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager requestWhenInUseAuthorization];
    
    [[WebServiceManager sharedInstance] loadState: [[mSession lookupTable] objectForKey: @"LookUpVersion"]
                                      countryCode: GET_COUNTRY_CODE
                                               vc: self];
    
    if([[[mSession lookupTable] objectForKey: @"IsShowCompanyCategory"] boolValue] || [GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_INDIA]) {
        [[WebServiceManager sharedInstance] loadCompanyCategory:self];
    }
    
    self.isValidated = TRUE;
}

-(void) setupInterface
{
    self.member = [[Member alloc] initWithData:[self.workshopData objectForKey: @"REG_Member"]];
    self.companyInfo = [[CompanyInfo alloc] initWithData: [self.workshopData objectForKey: @"REG_Company"]];
    self.companyAddress = [[CompanyAddress alloc] initWithData: [self.workshopData objectForKey: @"REG_CompanyAddress"]];
    
    
    self.workingHoursDict = [self.workshopData objectForKey: @"REG_CompanyWorkHours"];
    
    if (!self.workingHoursDict ||
        [self.workingHoursDict isKindOfClass: [NSNull class]])
        self.workingHoursDict = @{@"WorkHourStart1":   @"0:00",
                                  @"WorkHourEnd1":     @"0:00",
                                  @"WeekDays1":        @"",
                                  @"PublicHoliday":    @"0",
                                  @"WorkHourStart2":   @"0:00",
                                  @"WorkHourEnd2":     @"0:00",
                                  @"WeekDays2":        @"",
                                  };
    self.tableViewDelegate = [[FormsTableViewDelegate alloc] init];
    
    
//    if (IS_COUNTRY(COUNTRYCODE_INDIA))
//    {
//        if([self.companyInfo.companySource boolValue])
//        {
//            NSString* companySource = [@"Lead Source+" stringByAppendingString: [mSession convertToCompanySource: [self.companyInfo.companySource intValue]]];
//            [self.tableViewDelegate.headersArray addObject: LOCALIZATION(companySource)];
//            [self.tableViewDelegate.initializingArray addObject:[NSMutableArray new]];
//        }
//    }

    if(GET_PROFILETYPE == kPROFILETYPE_DMR)
    {
        self.mechanicProfileArray = [NSMutableArray new];
        [self setupMechanicProfileArray:self.mechanicProfileArray];
        [self.tableViewDelegate.initializingArray addObject: self.mechanicProfileArray];
        [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_HEADER_MECHANICPROFILE)]; //lokalise 4 feb
    }
    else
    {
        self.workshopSectionArray = [[NSMutableArray alloc] init];
        [self setupWorkshopSectionArray: self.workshopSectionArray];
        [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_HEADER_WORKSHOPPROFILE)]; //lokalise 4 feb
        [self.tableViewDelegate.initializingArray addObject: self.workshopSectionArray];
        
        NSMutableArray *ownerSectionArray = [[NSMutableArray alloc] init];
        [self setupOwnerProfileSectionArray: ownerSectionArray];
        [self.tableViewDelegate.initializingArray addObject: ownerSectionArray];
        [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_HEADER_OWNERPROFILE)]; //lokalise 4 feb
    }
    
    NSMutableArray* addressDetailsArray = [NSMutableArray new];
    [self setupAddressArray:addressDetailsArray];
    [self.tableViewDelegate.initializingArray addObject: addressDetailsArray];
    [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_PROFILE_ADDRESS)]; //lokalise 4 feb
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        NSMutableArray *bankDetailsArray = [[NSMutableArray alloc] init];
        [self setupBankDetailsArray: bankDetailsArray];
        [self.tableViewDelegate.initializingArray addObject: bankDetailsArray];
        [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_HEADER_BANKDETAILS)]; //lokalise 4 feb
        
        NSMutableArray *cardValidationArray = [[NSMutableArray alloc] init];
        [self setupAccountValidationArray: cardValidationArray];
        [self.tableViewDelegate.headersArray addObject: LOCALIZATION(@"Account Verification")];
        [self.tableViewDelegate.initializingArray addObject: cardValidationArray];
        
    }
    
    if (IS_COUNTRY(COUNTRYCODE_SAUDI))
    {
        NSMutableArray *contactPrefArray = [NSMutableArray new];
        [self setupContactPrefArray:contactPrefArray];
        [self.tableViewDelegate.headersArray addObject: @""];
        [self.tableViewDelegate.initializingArray addObject:contactPrefArray];
    }
    
    NSMutableArray *lastSectionArray = [[NSMutableArray alloc] init];
    [self setupLastSectionArray: lastSectionArray];
    [self.tableViewDelegate.initializingArray addObject: lastSectionArray];
    [self.tableViewDelegate.headersArray addObject: @""];
    
    
    self.regFormTableView.delegate = self.tableViewDelegate;
    self.regFormTableView.dataSource = self.tableViewDelegate;

    
    [self.tableViewDelegate registerNib: self.regFormTableView];
    [self.tableViewDelegate setDelegate: self];

    if(@available(iOS 11, *))
    {
        self.regFormTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.regFormTableView setContentInset:UIEdgeInsetsMake(15, 0, 15, 0)];
    
    
    self.regFormTableView.backgroundColor = [UIColor clearColor];
    self.regFormTableView.backgroundView = nil;
    
    [self setupGoogleAnalytics];
}


-(void) setupGoogleAnalytics
{
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_MANAGEWORKSHOP_PENDING) screenClass:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    //    [self setupInterface];
    
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_COUNTRY) //lokalise 4 feb
                                                   list: [mSession getCountryNames]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
    
    //preload dateview
    dateView = [[CASDatePickerView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_BIRTHDAY) previouslySelected:nil withYearHidden:YES]; //lokalise 4 feb
    
    dateView.delegate = self;
}

-(void) updatePressed
{
    [self popUpYesNoWithTitle: LOCALIZATION(C_CTA_SELECTFOLLOWING)      //lokalise 4 feb
                      content:@""
                  yesBtnTitle:LOCALIZATION(C_CTA_SAVEALLCHANGES)      //lokalise 4 feb
                   noBtnTitle:LOCALIZATION(C_CTA_DELETEACCOUNT)      //lokalise 4 feb
                 onYesPressed:^(BOOL finished) {
                     
                     //patched for confirm account
                     NSDictionary *jsonValuesDict = [self.tableViewDelegate getJsonValues];
                     if (IS_COUNTRY(COUNTRYCODE_INDIA) &&
                         ![[jsonValuesDict objectForKey: kAccountNumber] isEqualToString: [jsonValuesDict objectForKey: kConfirmAccountNumber]])
                     {
                         [mAlert showErrorAlertWithMessage: LOCALIZATION(C_FORM_BENEFICIARYNUM_NOTMATCH)];
                         return;
                     }
                     
                     NSMutableDictionary *updateParams = [self setupForUpdate];
                     NSMutableDictionary *regCompanyDict = [updateParams objectForKey: @"REG_Company"];
                     [regCompanyDict setObject: @"2" forKey: kStatus];
                     
                     if(self.isValidated) {
                         if (GET_PROFILETYPE == kPROFILETYPE_DMR)
                             [[WebServiceManager sharedInstance] updateIM: updateParams vc:self];
                         else
                             [[WebServiceManager sharedInstance] updateWorkshop: updateParams vc:self];
                     }
                 } onNoPressed:^(BOOL finished) {
        [self popUpTextViewWithTitle:LOCALIZATION(C_CTA_REASONDELETE)       //lokalise 4 feb
                      yesButtonTitle:LOCALIZATION(C_FORM_SUBMIT)         //lokalise 4 feb
                       noButtonTitle:LOCALIZATION(C_FORM_CANCEL)         //lokalise 4 feb
                         placeHolder:LOCALIZATION(C_PROFILE_ADDNEWREMARKS)       //lokalise 4 feb
                   completionHandler:^(BOOL isYes, NSString* remarks) {
                       if(isYes)
                       {
                           NSMutableDictionary *deactivateParams = [self setupForUpdate];
                           NSMutableDictionary *regCompanyDict = [deactivateParams objectForKey: @"REG_Company"];
                           [regCompanyDict setObject: @"32" forKey: kStatus];
                           
                           if(self.isValidated) {
                               if (GET_PROFILETYPE == kPROFILETYPE_DMR)
                                   [[WebServiceManager sharedInstance] updateIM: deactivateParams vc:self];
                               else
                                   [[WebServiceManager sharedInstance] updateWorkshop: deactivateParams vc:self];
                           }
                       }
                       else
                       {
                           
                       }
                   }];
                 }];
}


-(void) dropdownPressedWithKey:(NSString *) formKey
{
    [self.view endEditing: YES];
    listView.formKey = formKey;
    
    //Company Source
    if ([formKey isEqualToString: kCompanySource])
    {
        if (IS_COUNTRY(COUNTRYCODE_INDIA))
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_MECHSOURCE) list: [mSession getCompanySource] hasSearchField: NO];         //lokalise 4 feb
        else
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_COMPANYSOURCE) list: [mSession getCompanySource] hasSearchField: NO];        //lokalise 4 feb
    }
//    //Distributor Approval - disabled, no need popup
//    else if ([formKey isEqualToString: kApprovalStatus])
//    {
//        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_COMPANYSOURCE) list: [mSession ] hasSearchField: NO];
//    }
    //Workshop Type
    else if ([formKey isEqualToString: kCompanyType])
    {
        if (IS_COUNTRY(COUNTRYCODE_INDIA))
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_MECHTYPE) list: [mSession getCompanyTypes] hasSearchField: NO];        //lokalise 4 feb
        else
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_WORKSHOPTYPE) list: [mSession getCompanyTypes] hasSearchField: NO];        //lokalise 4 feb
    }
    else if ([formKey isEqualToString: kCompanyCategory])
    {
        if (IS_COUNTRY(COUNTRYCODE_INDIA))
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_MECHCATEGORY) list: [self getCompanyCategoryValues] hasSearchField: NO];        //lokalise 4 feb
        else
            [listView setTitleAndList: LOCALIZATION(C_PROFILE_WORKSHOPCATEGORY_PLACEHOLDER) list: [self getCompanyCategoryValues] hasSearchField: NO];        //lokalise 4 feb
    }
    else if ([formKey isEqualToString: kActivationStatus])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_ACTIVATIONSTATUS) list: [mSession getLeadStatus] hasSearchField: NO];       //lokalise 4 feb
    }
    //Workshop Type
    else if ([formKey isEqualToString: kCompanyType])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_WORKSHOPTYPE) list: [mSession getCompanyTypes] hasSearchField: NO];       //lokalise 4 feb
    }
    //Workshop Category
    else if ([formKey isEqualToString: kCompanyCategory])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_WORKSHOPCATEGORY) list: [self getCompanyCategoryValues] hasSearchField: NO];       //lokalise 4 feb
    }
    else if ([formKey isEqualToString: kCompanyState])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_STATE) list: [mSession getStateNames: self.stateTable] hasSearchField: YES];       //lokalise 4 feb
    }
    else if ([formKey isEqualToString: kSalutation])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO];       //lokalise 4 feb
    }
    else if ([formKey isEqualToString: kBankID])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_BANKNAME) list: [mSession getBanksDetailsList] hasSearchField: NO];       //lokalise 4 feb
    }
    else
    {
        listView.formKey = nil;
        [mAlert showErrorAlertWithMessage: @"Unknown Error"];
        return;
    }
    [self popUpView];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        [self.tableViewDelegate editCell: listView.formKey withDropdownSelection: selection.firstObject];
        if(IS_COUNTRY(COUNTRYCODE_INDIA) && [listView.formKey isEqualToString:kCompanyType]) {
            NSString *companyTypeCode = [mSession convertToCompanyTypesKeyCode:selection.firstObject];
            if([companyTypeCode isEqualToString:@"2"] || [companyTypeCode isEqualToString:@"4"]) {
                NSInteger indexInteger;
                if(([self.workshopSectionArray count] != 4 && GET_PROFILETYPE == kPROFILETYPE_DSR) || ([self.mechanicProfileArray count] != 7 && GET_PROFILETYPE == kPROFILETYPE_DMR)) {
                    if(GET_PROFILETYPE == kPROFILETYPE_DMR) {
                        [self.mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPCODE),
                                                                kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                                                kForm_Key: kReferenceCode,
                                                                kForm_Value: self.companyInfo.referenceCode,
                                                                kForm_Optional: @(YES),
                                                        }];   //Workshop Code
                        indexInteger = 2;
                    } else {
                        [self.workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPCODE),
                                                                kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                                                kForm_Key: kReferenceCode,
                                                                kForm_Value: self.companyInfo.referenceCode,
                                                                kForm_Optional: @(YES),
                                                            }];   //Workshop Code
                        indexInteger = 3;
                    }
                    [self.regFormTableView beginUpdates];
                    [self.regFormTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:indexInteger inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                    [self.regFormTableView endUpdates];
                    
                    [self.regFormTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
                }
            } else {
                if(([self.workshopSectionArray count] == 4 && GET_PROFILETYPE == kPROFILETYPE_DSR) || ([self.mechanicProfileArray count] == 7 && GET_PROFILETYPE == kPROFILETYPE_DMR)) {
                    NSInteger indexInteger;
                    if(GET_PROFILETYPE == kPROFILETYPE_DMR) {
                        indexInteger = 2;
                        [self.mechanicProfileArray removeObjectAtIndex:2];
                    } else {
                        [self.workshopSectionArray removeLastObject];
                        indexInteger = 3;
                    }
                    [self.regFormTableView beginUpdates];
                    [self.regFormTableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:indexInteger inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                    [self.regFormTableView endUpdates];
                    
                    [self.regFormTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
                }
            }
        }
        listView.formKey = nil;
    }
}
-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

-(void) didPressWorkingHours
{
    if (!self.workingHoursDict ||
        [self.workingHoursDict isKindOfClass: [NSNull class]])
        self.workingHoursDict = @{@"WorkHourStart1":   @"0:00",
                                  @"WorkHourEnd1":     @"0:00",
                                  @"WeekDays1":        @"",
                                  @"PublicHoliday":    @"0",
                                  @"WorkHourStart2":   @"0:00",
                                  @"WorkHourEnd2":     @"0:00",
                                  @"WeekDays2":        @"",
                                  };
    
    //let tab bar push instead
    [mSession pushWorkingHoursView: self.navigationController.parentViewController workingHoursData: self.workingHoursDict];
}

-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict
{
    //assign working hours
    self.workingHoursDict = workingHoursDict;
    
    [self.tableViewDelegate updateWorkingHours: self.workingHoursDict];
    
    [self.regFormTableView reloadData];
    
//    NSArray *fullWorkWeekDays;
//    NSArray *halfWorkWeekDays;
//
//    if (![[workingHoursDict objectForKey: @"WeekDays1"] isEqualToString: @""])
//        fullWorkWeekDays = [[workingHoursDict objectForKey: @"WeekDays1"] componentsSeparatedByString: @","];
//
//    if (![[workingHoursDict objectForKey: @"WeekDays2"] isEqualToString: @""])
//        halfWorkWeekDays = [[workingHoursDict objectForKey: @"WeekDays2"] componentsSeparatedByString: @","];
}

-(void) ctaPressedWithKey:(NSString *) ctaKey
{
    if ([ctaKey isEqualToString: kRegisterCTA])
    {
        [self registerWorkshopCTA];
    }
    else if ([ctaKey isEqualToString: kUpdateCTA])
    {
        [self updatePressed];
    }
}

-(void) showPhoto:(UIImage *) image
{
    //popup or push to controller with image
    [mSession pushImagePreview: self image: image title: @"Uploaded Image"];
}

-(void) takePhoto: (NSString *) cellKey
{
    if ([cellKey isEqualToString: kPANCardImage])
        [mSession pushPanCardPhotoTakingWithParent: self delegateVc: self  message: LOCALIZATION(C_FORM_PANCARD_PHOTOFORMAT) key: cellKey];
    else if ([cellKey isEqualToString: kDrivingLicenseImage])
        [mSession pushPanCardPhotoTakingWithParent: self delegateVc: self  message: LOCALIZATION(C_FORM_DRIVINGLICENSE_PHOTOFORMAT) key: cellKey];
    else if ([cellKey isEqualToString: kVoterIDImage])
        [mSession pushPanCardPhotoTakingWithParent: self delegateVc: self  message: LOCALIZATION(C_FORM_VOTERID_PHOTOFORMAT) key: cellKey];
}

-(void)imageTaken:(UIImage *)image key:(NSString *)key
{
    [self.tableViewDelegate editCell: key withImage: image];
}

-(void)textfieldCellDidBeginEditingWithKey:(NSString *)key
{
    if ([key isEqualToString: kAccountNumber])
        [self.tableViewDelegate editCell: kAccountNumber toSecureField: NO];
}

-(void)textfieldCellDidEndEditingWithKey:(NSString *)key
{
    if ([key isEqualToString: kPostalCode])
    {
        NSString *postalCode = [[self.tableViewDelegate getJsonValues] objectForKey: kPostalCode];
        [mWebServiceManager fetchPostalAddress: postalCode vc:self];
    }
    else if ([key isEqualToString: kIFSCCode])
    {
        [self.tableViewDelegate editCell: kBankID withDropdownSelection: @""];    //Bank Name list
        NSString *ifscCodeString = [[self.tableViewDelegate getJsonValues] objectForKey: kIFSCCode];
        [mWebServiceManager fetchBankDetails: ifscCodeString vc:self];
    }
    else if ([key isEqualToString: kAccountNumber])
        [self.tableViewDelegate editCell: kAccountNumber toSecureField: YES];
}


#pragma mark textfield with buttons delegate methods
- (void)textfieldWithButtonPressedWithKey:(NSString *)buttonKey
{
    if([buttonKey isEqualToString:kWorkshopQRCode])
    {
        mQRCodeScanner.delegate = self;
        [mQRCodeScanner showZBarScannerOnVC: self];
    }
//    else if([buttonKey isEqualToString: kMechanicSharecode])
//    {
//        mQRCodeScanner.delegate = self;
//        [mQRCodeScanner showZBarScannerOnVC: self];
//    }
    else if([buttonKey isEqualToString:kWorkshopLocation])
    {
        [self locationPressed];
    }
}
- (void)locationPressed {
    [UpdateHUD addMBProgress: self.view withText: @""];
    switch ([CLLocationManager authorizationStatus])
    {
        case kCLAuthorizationStatusAuthorizedAlways:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusNotDetermined:
            [UpdateHUD removeMBProgress: self];
            [self.locationManager requestWhenInUseAuthorization];
            break;
        case kCLAuthorizationStatusDenied:
        {
            [UpdateHUD removeMBProgress: self];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_ALERTVIEW_ERROR) message:LOCALIZATION(C_ALERTVIEW_PERMISSIONERRORMSG) preferredStyle:UIAlertControllerStyleAlert];       //lokalise 4 feb
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_CANCEL) style:UIAlertActionStyleCancel handler:nil];       //lokalise 4 feb
            UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_SETTING) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {       //lokalise 4 feb
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                            UIApplicationOpenSettingsURLString]];
            }];
            
            [alertController addAction:cancelAction];
            [alertController addAction:settingsAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
            break;
        case kCLAuthorizationStatusRestricted:
            [UpdateHUD removeMBProgress: self];
            break;
    }
    
}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [UpdateHUD removeMBProgress: self];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    //most recent location update is at the end of the array.
    CLLocation *location = [locations lastObject];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:6];
    
    NSString *newLatitude   = [formatter stringFromNumber: @(location.coordinate.latitude)];
    NSString *newLongitude  = [formatter stringFromNumber: @(location.coordinate.longitude)];
    
    [self.tableViewDelegate editCell:kWorkshopLocation withTextfieldButtonText:[NSString stringWithFormat: @"%@, %@", newLatitude, newLongitude]];
    
    self.companyAddress.latitude = newLatitude.doubleValue;
    self.companyAddress.longitude = newLongitude.doubleValue;
    
    [self.locationManager stopUpdatingLocation];
    [UpdateHUD removeMBProgress: self];
}

-(NSMutableDictionary *) setupForUpdate
{
    NSDictionary *jsonValuesDict = [self.tableViewDelegate getJsonValues];
    
    NSMutableDictionary *updateParams = [[NSMutableDictionary alloc] init];
    
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
        [updateParams setObject: @([[jsonValuesDict objectForKey: kBankCheckBox] boolValue]) forKey: @"BankInfoSharing"];
    [updateParams setObject: GET_COUNTRY_CODE forKey: @"CountryCode"];
    
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        NSMutableDictionary *regBankDict = [[NSMutableDictionary alloc] init];
        [regBankDict setObject:[jsonValuesDict objectForKey: kBeneficiaryName]  forKey: kBeneficiaryName];
        [regBankDict setObject:[jsonValuesDict objectForKey: kAccountNumber] forKey: kAccountNumber];
        [regBankDict setObject:[jsonValuesDict objectForKey: kIFSCCode] forKey: kIFSCCode];
        [regBankDict setObject: [mSession convertToBankNameKeyCode: [jsonValuesDict objectForKey: kBankID]] forKey: kBankID];
        
        [updateParams setObject: regBankDict forKey: @"REG_Bank"];
        
        NSMutableDictionary *regPanDict = [[NSMutableDictionary alloc] init];
        [regPanDict setObject: [jsonValuesDict objectForKey: kPANCardId] forKey: kPANCardId];
        if([jsonValuesDict objectForKey: kPANCardImage])
            [regPanDict setObject: [jsonValuesDict objectForKey: kPANCardImage] forKey: kPANCardImage];
        
        NSMutableDictionary *regDrivingLicense = [[NSMutableDictionary alloc] init];
        [regDrivingLicense setObject: [jsonValuesDict objectForKey: kDrivingLicense] forKey: kDrivingLicense];
        if([jsonValuesDict objectForKey: kDrivingLicense])
            [regDrivingLicense setObject: [jsonValuesDict objectForKey: kDrivingLicenseImage] forKey: kDrivingLicenseImage];
        
        NSMutableDictionary *regVoterDict = [[NSMutableDictionary alloc] init];
        [regVoterDict setObject: [jsonValuesDict objectForKey: kVoterID] forKey: kVoterID];
        if([jsonValuesDict objectForKey: kVoterID])
            [regVoterDict setObject: [jsonValuesDict objectForKey: kVoterIDImage] forKey: kVoterIDImage];
        
        NSString *checkboxesString = [jsonValuesDict objectForKey: kValidateCheckboxes];
        NSArray *stringArray = [checkboxesString isEqualToString:@""] ? @[] : [checkboxesString componentsSeparatedByString: @","];
        NSString *idProofTypeString = @"";
        for (NSString *string in stringArray)
        {
            NSString *pancardRegex;
            NSPredicate *pancardTest;
            switch ([string intValue])
            {
                case 0:
                {
                    idProofTypeString = @"1";
                    
                    pancardRegex = REGEX_PANCARD;
                    pancardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pancardRegex];
                    
                    if([[jsonValuesDict objectForKey:kPANCardId] isEqualToString:@""]) {
                        self.isValidated = FALSE;
                        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_PAN_REQUIRED)];
                        return @{};
                    }
                    else if(![pancardTest evaluateWithObject: [jsonValuesDict objectForKey:kPANCardId]]){
                        self.isValidated = FALSE;
                        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_PANFORMAT)];
                        return @{};
                    }
                    else if([[jsonValuesDict objectForKey:kPANCardImage] isEqualToString:@""]) {
                        self.isValidated = FALSE;
                        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_PANIMAGE_REQUIRED)];
                        return @{};
                    }
                    [updateParams setObject:regPanDict forKey:@"REG_PAN"];
                }
                    break;
                case 1:
                {
                    if(![idProofTypeString isEqualToString:BLANK])
                       idProofTypeString = [NSString stringWithFormat:@"%@,2",idProofTypeString];
                    else
                        idProofTypeString = @"2";
                    
                    pancardRegex = REGEX_DRIVINGLICENSE;
                    pancardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pancardRegex];
                    
                    if([[jsonValuesDict objectForKey:kDrivingLicense] isEqualToString:@""]) {
                        self.isValidated = FALSE;
                        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_DRIVINGLICENSE_REQUIRED)];
                        return @{};
                    }
                    else if(![pancardTest evaluateWithObject: [jsonValuesDict objectForKey:kDrivingLicense]]){
                        self.isValidated = FALSE;
                        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_DRIVINGLICENSEFORMAT)];
                        return @{};
                    }
                    else if([[jsonValuesDict objectForKey:kDrivingLicenseImage] isEqualToString:@""]) {
                        self.isValidated = FALSE;
                        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_DRIVINGLICENSEIMAGE_REQUIRED)];
                        return @{};
                    }
                     [updateParams setObject:regDrivingLicense forKey:@"REG_DrivingLicense"];
                }
                    break;
                case 2:
                {
                    if(![idProofTypeString isEqualToString:BLANK])
                       idProofTypeString = [NSString stringWithFormat:@"%@,4",idProofTypeString];
                    else
                        idProofTypeString = @"4";
                    
                    pancardRegex = REGEX_VOTERID;
                    pancardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pancardRegex];
                    
                    if([[jsonValuesDict objectForKey:kVoterID] isEqualToString:@""]) {
                        self.isValidated = FALSE;
                        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_VOTERID_REQUIRED)];
                        return @{};
                    }
                    else if(![pancardTest evaluateWithObject: [jsonValuesDict objectForKey:kVoterID]]){
                        self.isValidated = FALSE;
                        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_VOTERIDFORMAT)];
                        return @{};
                    }
                    else if([[jsonValuesDict objectForKey:kVoterIDImage] isEqualToString:@""]) {
                        self.isValidated = FALSE;
                        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_VOTERIDIMAGE_REQUIRED)];
                        return @{};
                    }
                    [updateParams setObject:regVoterDict forKey:@"REG_VoterID"];
                }
                    break;
            }
        }
        
        [updateParams setObject:idProofTypeString forKey:@"IDProofType"];
        if ([updateParams objectForKey:@"REG_PAN"] == nil) {
            [regPanDict setObject:@"" forKey:kPANCardId];
            [regPanDict setObject:@"" forKey:kPANCardImage];
            
            [updateParams setObject:regPanDict forKey:@"REG_PAN"];
        }
        if ([updateParams objectForKey:@"REG_DrivingLicense"] == nil) {
            [regDrivingLicense setObject:@"" forKey:kDrivingLicense];
            [regDrivingLicense setObject:@"" forKey:kDrivingLicenseImage];
            
            [updateParams setObject:regDrivingLicense forKey:@"REG_DrivingLicense"];
        }
        if ([updateParams objectForKey:@"REG_VoterID"] == nil) {
            [regVoterDict setObject:@"" forKey:kVoterID];
            [regVoterDict setObject:@"" forKey:kVoterIDImage];
            
            [updateParams setObject:regVoterDict forKey:@"REG_VoterID"];
        }
    }
    
#pragma mark REG_Company API parameters
    NSMutableDictionary *regCompanyDict = [NSMutableDictionary new];
    [regCompanyDict setObject: [mSession convertToCompanyTypesKeyCode: [jsonValuesDict objectForKey: kCompanyType]]
                       forKey: kCompanyType];
    
    if ([[[mSession lookupTable] objectForKey: @"IsShowCompanyCategory"] boolValue] || IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [regCompanyDict setObject: [self convertToCompanyCategoryCode: [jsonValuesDict objectForKey: kCompanyCategory]]
                           forKey: kCompanyCategory]; //customer category
    }
    if (IS_COUNTRY(COUNTRYCODE_INDIA) && GET_PROFILETYPE == kPROFILETYPE_DMR)
    {
        [regCompanyDict setObject: [NSString stringWithFormat: @"%@ %@", [jsonValuesDict objectForKey:kFirstName], [jsonValuesDict objectForKey:kLastName]]
                           forKey: kCompanyName];
    }
    else
    {
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kCompanyName] forKey: kCompanyName];
    }
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        if ([jsonValuesDict objectForKey: kCompanySource])
            [regCompanyDict setObject: [mSession convertToCompanySourceKeyCode: [jsonValuesDict objectForKey: kCompanySource]]
                               forKey: kCompanySource];
        else
            [regCompanyDict setObject: @"1" forKey: kCompanySource];
    }
    else
    {
        if ([jsonValuesDict objectForKey: kCompanySource])
            [regCompanyDict setObject: [mSession convertToCompanySourceKeyCode: [jsonValuesDict objectForKey: kCompanySource]]
                               forKey: kCompanySource];
        else
            [regCompanyDict setObject: self.companyInfo.companySource
                               forKey: kCompanySource];
    }
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kMobileNumber] forKey: kContactNumber];
        [regCompanyDict setObject:@"" forKey:kReferenceCode];
    }
    else
    {
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kContactNumber] forKey: kContactNumber];
    }
//    [regCompanyDict setObject: [mSession convertToLeadStatusKeyCode: [jsonValuesDict objectForKey: kActivationStatus]]
//                       forKey: kStatus];
    if ([jsonValuesDict objectForKey:kReferenceCode])
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kReferenceCode] forKey: kReferenceCode];
    else
        [regCompanyDict setObject:@"" forKey:kReferenceCode];
    if (!IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [regCompanyDict setObject: [jsonValuesDict objectForKey: kWorkshopQRCode] forKey: kWorkshopQRCode];
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kCompanyEmailAddress] forKey: kCompanyEmailAddress];
        
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kOilChangePerMonth] forKey: kOilChangePerMonth];
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kOilChangePercentage] forKey: kOilChangePercentage];
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kHalfSyntheticOilChanges] forKey: kHalfSyntheticOilChanges];
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kShellHelixQty] forKey: kShellHelixQty];
        
    }
    
    [updateParams setObject: regCompanyDict forKey: @"REG_Company"];
    
#pragma mark REG_CompanyAddress API parameters
    NSMutableDictionary *regCompanyAddressDict = [NSMutableDictionary new];
    [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kAddress1] forKey: kAddress1];
    [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kAddress2] forKey: kAddress2];
    [regCompanyAddressDict setObject: @"1" forKey: @"AddressType"];
    if ([jsonValuesDict objectForKey: kCompanyCity])
        [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kCompanyCity] forKey: kCompanyCity];
    NSString *stateString = [jsonValuesDict objectForKey: kCompanyState];
    [regCompanyAddressDict setObject: [mSession convertToStateKeyCode: stateString stateDetailedListArray:self.stateTable]
                              forKey: kCompanyState];
    [regCompanyAddressDict setObject: @(self.companyAddress.latitude) forKey: @"Latitude"];
    [regCompanyAddressDict setObject: @(self.companyAddress.longitude) forKey: @"Longitude"];
    if(![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_HONGKONG]) {
        [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kPostalCode] forKey: kPostalCode];
    } else {
        [regCompanyAddressDict setObject: @"" forKey: kPostalCode];
    }
    [regCompanyAddressDict setObject:@"" forKey: @"StreetName"];
    
    [updateParams setObject: regCompanyAddressDict forKey: @"REG_CompanyAddress"];
    
    [updateParams setObject: self.workingHoursDict forKey: @"REG_CompanyWorkHours"];
    
    [updateParams setObject: @[] forKey: @"REG_CompanyService"];
    
 
#pragma mark REG_Member API parameters
    NSMutableDictionary *regMemberDict = [NSMutableDictionary new];
    //    [regMemberDict setObject: @"" forKey: @"ContactPreferences"];
    [regMemberDict setObject: [jsonValuesDict objectForKey:kFirstName] forKey: kFirstName];
    [regMemberDict setObject: @"1" forKey: @"Gender"];
    [regMemberDict setObject: [jsonValuesDict objectForKey:kLastName] forKey: kLastName];
    [regMemberDict setObject: [jsonValuesDict objectForKey:kMobileNumber] forKey: kMobileNumber];
    [regMemberDict setObject: @"1" forKey: @"RegistrationMode"];
    [regMemberDict setObject: @"1" forKey: @"RegistrationType"];
    [regMemberDict setObject: @([[mSession convertToSalutationKeyCode: [jsonValuesDict objectForKey: kSalutation]] intValue]) forKey: kSalutation];
//    if (IS_COUNTRY(COUNTRYCODE_INDIA) && GET_PROFILETYPE == kPROFILETYPE_DMR)
//        [regMemberDict setObject: [[self.workshopData objectForKey: @"REG_Member"] objectForKey: @"TradeID"] forKey: @"TradeID"]; //india uses iM
//    else
    [regMemberDict setObject: [self.workshopData objectForKey:@"LeadID"] forKey: @"LeadID"];
    [regMemberDict setObject: [jsonValuesDict objectForKey:kMobileNumber] forKey: @"UserID"];
    
    if(IS_COUNTRY(COUNTRYCODE_SAUDI)) {
         [regMemberDict setObject: [jsonValuesDict objectForKey:kContactPreference] forKey: kContactPreference];
    }
    
    [updateParams setObject: regMemberDict forKey: @"REG_Member"];
    
    
        [updateParams setObject: @{@"Status":@(0),
                                   } forKey: @"REG_CompanyStatus"];
    
    [updateParams setObject: @([[jsonValuesDict objectForKey: kTncCheckBox] boolValue]) forKey: @"TermCondition"];
    
    return updateParams;
}

-(void) registerWorkshopCTA
{
    //patched for confirm account
    NSDictionary *jsonValuesDict = [self.tableViewDelegate getJsonValues];
    if (IS_COUNTRY(COUNTRYCODE_INDIA) &&
        ![[jsonValuesDict objectForKey: kAccountNumber] isEqualToString: [jsonValuesDict objectForKey: kConfirmAccountNumber]])
    {
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_FORM_BENEFICIARYNUM_NOTMATCH)];
        return;
    }
    
    NSMutableDictionary *updateParams = [self setupForUpdate];
    NSMutableDictionary *regCompanyDict = [updateParams objectForKey: @"REG_Company"];
    [regCompanyDict setObject: @"16" forKey: kStatus];
    
    if(self.isValidated) {
        if (IS_COUNTRY(COUNTRYCODE_INDIA) && GET_PROFILETYPE == kPROFILETYPE_DMR)
            [[WebServiceManager sharedInstance] updateIM: updateParams vc:self];
        else
            [[WebServiceManager sharedInstance] updateWorkshop: updateParams vc:self];
    }
}


#pragma mark Setup Form methods
-(void) setupWorkshopSectionArray: (NSMutableArray *) workshopSectionArray
{
    if (!IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_COMPANYSOURCE),       //lokalise 4 feb
                                           kForm_Type: @(kREGISTRATION_DROPDOWN),
                                           kForm_Key: kCompanySource,
                                           kForm_Placeholder: LOCALIZATION(C_PROFILE_COMPANYSOURCE_PLACEHOLDER),       //lokalise 4 feb
                                           kForm_Value: [self.companyInfo.companySource boolValue] ? [mSession convertToCompanySource: [self.companyInfo.companySource intValue]] : LOCALIZATION(C_PROFILE_COMPANYSOURCE_PLACEHOLDER),       //lokalise 4 feb
                                           }];
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_APPROVALSTATUS),       //lokalise 4 feb
                                           kForm_Type: @(kREGISTRATION_DROPDOWN),
                                           kForm_Key: kApprovalStatus,
                                           kForm_Value: self.companyInfo.approvalStatus,
                                           kForm_Enabled: @(NO),
                                           }];
    }
    [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPTYPE),       //lokalise 4 feb
                                       kForm_Placeholder: LOCALIZATION(C_PROFILE_WORKSHOPTYPE_PLACEHOLDER),       //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_DROPDOWN),
                                       kForm_Key: kCompanyType,
                                       kForm_Value: [mSession convertToCompanyTypes: self.companyInfo.companyType],
                                       }];    //workshop type
    
    if ([[[mSession lookupTable] objectForKey: @"IsShowCompanyCategory"] boolValue] || IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPCATEGORY),       //lokalise 4 feb
                                           //                                      @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPCATEGORY),
                                           kForm_Placeholder: LOCALIZATION(C_PROFILE_WORKSHOPCATEGORY_PLACEHOLDER),       //lokalise 4 feb
                                           kForm_Type: @(kREGISTRATION_DROPDOWN),
                                           kForm_Key: kCompanyCategory,
                                           kForm_Value: [self convertCodeToValue: self.companyInfo.companyCategory],
                                           }];    //workshop category
    }
    
    [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_COMPANYNAME),       //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                       kForm_Key: kCompanyName,
                                       kForm_Value: self.companyInfo.companyName,
                                       }]; //company name
    
    if(!IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPSHARECODE),       //lokalise 4 feb
                                               kForm_Type: @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                                               kForm_Icon: @"icon-code",
                                               kForm_Key: kWorkshopQRCode,
                                               kForm_Value:  self.companyInfo.workshopQRCode,
                                               kForm_TextfieldEnabled: @(YES),
                                               kForm_Enabled: @(YES),
                                               kForm_Optional: @(!(IS_COUNTRY(COUNTRYCODE_MALAYSIA) ||
                                                                   IS_COUNTRY(COUNTRYCODE_VIETNAM) || IS_COUNTRY(COUNTRYCODE_HONGKONG) || (IS_COUNTRY(COUNTRYCODE_SAUDI)))), //Required field
        //                                       kForm_Optional: @(!IS_COUNTRY(COUNTRYCODE_RUSSIA) && !IS_COUNTRY(COUNTRYCODE_INDIA)),
                                               }];   //Workshop SHARE Code
        
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPCODE),
                                           kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                           kForm_Key: kReferenceCode,
                                           kForm_Value: self.companyInfo.referenceCode,
                                           kForm_Optional: @(YES),
                                        }];   //Workshop Code

        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_CONTACTNUMBER),       //lokalise 4 feb
                                           kForm_Placeholder: LOCALIZATION(C_PROFILE_CONTACTNUMBER_PLACEHOLDER),       //lokalise 4 feb
                                           kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                           kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                           kForm_Key: kContactNumber,
                                           kForm_Value: self.companyInfo.contactNumber,
                                           kForm_Optional: @(!IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                                           }]; //contact number

        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_EMAILADDRESS),       //lokalise 4 feb
                                           kForm_Placeholder: LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER),       //lokalise 4 feb
                                           kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                           kForm_Keyboard: @(UIKeyboardTypeEmailAddress),
                                           kForm_Key: kCompanyEmailAddress,
                                           kForm_Value: self.companyInfo.companyEmailAddress,
                                           kForm_Optional: @(YES),
                                           }]; //email address
        
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKINGHOURS),       //lokalise 4 feb
                                           kForm_Type: @(kREGISTRATION_WORKINGHOURS),
                                           kForm_Key: kWorkingHours,
                                           kForm_Value: @"", //using kForm_WorkingHours instead
                                           kForm_WorkingHours: [self.workingHoursDict isKindOfClass:[NSNull class]] ? @"" : self.workingHoursDict,
                                           kForm_Optional: @(YES),
                                           kForm_Enabled: @(YES), //MUST BE PRESENT
                                           }];
        
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_OILCHANGEPERDAY),       //lokalise 4 feb
                                           kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER),       //lokalise 4 feb
                                           kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
                                           kForm_Key: kOilChangePerMonth,
                                           kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                           kForm_Value: self.companyInfo.oilChangePerMonth,
                                           }];
        
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_OILCHANGEPERCENT),       //lokalise 4 feb
                                           kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER),       //lokalise 4 feb
                                           kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
                                           kForm_Key: kOilChangePercentage,
                                           kForm_Keyboard: @(UIKeyboardTypeDecimalPad),
                                           kForm_Value: self.companyInfo.oilChangePercentage,
                                           }];
        
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SEMIOILCHANGEPERCENT),       //lokalise 4 feb
                                           kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER),       //lokalise 4 feb
                                           kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
                                           kForm_Key: kHalfSyntheticOilChanges,
                                           kForm_Keyboard: @(UIKeyboardTypeDecimalPad),
                                           kForm_Value: self.companyInfo.halfSyntheticOilChanges,
                                           }];
        
        [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SHELLHELIX),       //lokalise 4 feb
                                           kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER),       //lokalise 4 feb
                                           kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
                                           kForm_Key: kShellHelixQty,
                                           kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                           kForm_Value: self.companyInfo.shellHelixQty,
                                           }];

    } else {
        if(self.companyInfo.companyType == 2 || self.companyInfo.companyType == 4) {
            [workshopSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPCODE),
                                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                               kForm_Key: kReferenceCode,
                                               kForm_Value: self.companyInfo.referenceCode,
                                               kForm_Optional: @(YES),
                                            }];   //Workshop Code
        }
    }
}

-(void) setupOwnerProfileSectionArray: (NSMutableArray *)ownerSectionArray
{
    [ownerSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SALUTATION),       //lokalise 4 feb
                                    kForm_Type: @(kREGISTRATION_DROPDOWN),
                                    kForm_Placeholder: LOCALIZATION(C_PROFILE_SALUTATION_PLACEHOLDER),       //lokalise 4 feb
                                    kForm_Key: kSalutation,
                                    kForm_Value: [mSession convertToSalutation: self.member.salutation],
                                    kForm_Optional: @(YES),
                                    }];  //button
    
    [ownerSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_FIRSTNAME),       //lokalise 4 feb
                                             kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                             kForm_Key: kFirstName,
                                             kForm_Value: self.member.firstName,
                                             }]; //first name
    
    
    [ownerSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_LASTNAME),       //lokalise 4 feb
                                             kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                             kForm_Key: kLastName,
                                             kForm_Value: self.member.lastName,
                                             }]; //last name
    if (!IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [ownerSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_DOB),       //lokalise 4 feb
                                        kForm_Type: @(kREGISTRATION_BIRTHDATE),
                                        kForm_Key: kDOB,
                                        kForm_Value: [self convertDateForDisplay: self.member.dobMonth day: self.member.dobDay],
                                        kForm_Optional: @(YES),
                                        }];
    }
    
    [ownerSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_MOBILENUM),       //lokalise 4 feb
                                             kForm_Type: @(kREGISTRATION_MOBNUM),
                                             kForm_Key: kMobileNumber,
                                             kForm_TextfieldEnabled: @(YES), //if not approved, allow editing
                                             kForm_CountryCode: [mSession convertCountryCodeToCountryName: self.member.countryCode],
                                             kForm_MobileNumber: self.member.mobileNumber,
                                             }];   //Mobile number
    
    if (!IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [ownerSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_OWNEREMAILADDRESS),       //lokalise 4 feb
                                        kForm_Placeholder: LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER),       //lokalise 4 feb
                                        kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                        kForm_Key: kEmailAddress,
                                        kForm_Keyboard: @(UIKeyboardTypeEmailAddress),
                                        kForm_Value: self.member.emailAddress,
                                        kForm_Optional: @(YES),
                                        }]; //email
    }
}
-(void) setupMechanicProfileArray: (NSMutableArray *) mechanicProfileArray
{
//    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(@"Mechanic Source"),
//                                       kForm_Type: @(kREGISTRATION_DROPDOWN),
//                                       kForm_Key: kCompanySource,
//                                       kForm_Value: [self.companyInfo.companySource boolValue] ? [mSession convertToCompanySource: [self.companyInfo.companySource intValue]] : LOCALIZATION(@"Select Mechanic Source"),
//                                       }];

    NSString *mechanicTypeString = [mSession convertToCompanyTypes: self.companyInfo.companyType];
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_MECHTYPE),       //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_DROPDOWN),
                                       kForm_Placeholder: LOCALIZATION(C_DROPDOWN_MECHTYPE),       //lokalise 4 feb
                                       kForm_Key: kCompanyType,
                                       kForm_Value: [mechanicTypeString length] > 0 ? mechanicTypeString : LOCALIZATION(C_DROPDOWN_MECHTYPE),       //lokalise 4 feb
                                       }];

    NSString *mechanicCategoryString = [self convertCodeToValue: self.companyInfo.companyCategory];
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_MECHCATEGORY),       //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_DROPDOWN),
                                       kForm_Placeholder: LOCALIZATION(C_DROPDOWN_MECHCATEGORY),       //lokalise 4 feb
                                       kForm_Key: kCompanyCategory,
                                       kForm_Value:  [mechanicCategoryString length] > 0 ? mechanicCategoryString : LOCALIZATION(C_DROPDOWN_MECHCATEGORY),       //lokalise 4 feb
                                       }];
    
    
//    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(@"Mechanic Type"),
//                                       kForm_Type: @(kREGISTRATION_DROPDOWN),
//                                       kForm_Key: kMechanicType,
//                                       kForm_Value: [self.companyInfo.companySource boolValue] ? [mSession convertToCompanySource: [self.companyInfo.companySource intValue]] : LOCALIZATION(@"Select Mechanic Type"),
//                                       }];
//    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(@"Mechanic Category"),
//                                       kForm_Type: @(kREGISTRATION_DROPDOWN),
//                                       kForm_Key: kCompanyCategory,
//                                       kForm_Value: [mSession convertToCompanyCategory: self.companyInfo.companyCategory],
//                                       }];
    
//    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_MECHSHARECODE),       //lokalise 4 feb
//                                       kForm_Type: @(kREGISTRATION_TEXTFIELDWITHBUTTON),
//                                       kForm_Icon: @"icon-code",
//                                       kForm_Key: kWorkshopQRCode,
//                                       kForm_Value:  self.companyInfo.workshopQRCode,
//                                       kForm_Enabled: @(YES),
//                                       kForm_TextfieldEnabled: @(YES),
//                                       kForm_Optional: @(YES),
//                                       }];   //Workshop SHARE Code
    
    if(self.companyInfo.companyType == 2 || self.companyInfo.companyType == 4) {
        [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPCODE),
                                              kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                              kForm_Key: kReferenceCode,
                                              kForm_Value: self.companyInfo.referenceCode,
                                              kForm_Optional: @(YES),
                                           }];   //Workshop Code
    }
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SALUTATION),       //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_DROPDOWN),
                                       kForm_Placeholder: LOCALIZATION(C_PROFILE_SALUTATION_PLACEHOLDER),       //lokalise 4 feb
                                       kForm_Key: kSalutation,
                                       kForm_Value: [mSession convertToSalutation: self.member.salutation],
                                       kForm_Optional: @(YES),
                                       }];
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_FIRSTNAME),       //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                       kForm_Key: kFirstName,
                                       kForm_Value: self.member.firstName,
                                       }]; //first name
    
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_LASTNAME),       //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                       kForm_Key: kLastName,
                                       kForm_Value: self.member.lastName,
                                       }]; //last name
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_MOBILENUM),       //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_MOBNUM),
                                       kForm_Key: kMobileNumber,
                                       kForm_TextfieldEnabled: @(YES), //if not approved, allow editing
                                       kForm_CountryCode: [mSession convertCountryCodeToCountryDisplayName: GET_COUNTRY_CODE],
                                       kForm_MobileNumber: self.member.mobileNumber,
                                       }];   //Mobile number
}

-(void) setupAddressArray: (NSMutableArray *) addressArray
{
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_LOCATION),       //lokalise 4 feb
                               kForm_Type: @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                               kForm_TextfieldEnabled: @(NO),
                               kForm_Icon: @"icon-location",
                               kForm_Key: kWorkshopLocation,
                               kForm_Enabled: @(YES),
                               kForm_Value: [NSString stringWithFormat: @"%f, %f", self.companyAddress.latitude, self.companyAddress.longitude],
                               }];    //location
    
    if(![GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_HONGKONG]) {
        [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_POSTALCODE),      //lokalise 4 feb
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_Key: kPostalCode,
                                   kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                   kForm_HasDelegate : @(IS_COUNTRY(COUNTRYCODE_INDIA)), //textfield delegate for postal code
                                   kForm_Value: self.companyAddress.postalCode,
                                   kForm_Enabled: @([[mSession profileInfo] canEditAddress]),
                                   }]; //postal code
    }
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_STATE),      //lokalise 4 feb
                               kForm_Type: @(kREGISTRATION_DROPDOWN),
                               kForm_Placeholder: LOCALIZATION(C_PROFILE_STATE_PLACEHOLDER),      //lokalise 4 feb
                               kForm_Key: kCompanyState,
                               kForm_Value: [mSession convertToStateName:self.companyAddress.companyState stateDetailedListArray:self.stateTable],
                               kForm_Enabled: @([[mSession profileInfo] canEditAddress]),
                               }];
    

    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_TOWN),      //lokalise 4 feb
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_Key: kCompanyCity, //kCity?
                               kForm_Value: self.companyAddress.companyCity,
                               kForm_Optional: @(!(IS_COUNTRY(COUNTRYCODE_RUSSIA) ||
                                                   IS_COUNTRY(COUNTRYCODE_INDIA))),
                               kForm_Enabled: @([[mSession profileInfo] canEditAddress]),
                               }];
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_ADDRESSLINE1),      //lokalise 4 feb
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_Key: kAddress1,
                               kForm_Value: self.companyAddress.address1,
//                               kForm_Optional: @(!IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                               kForm_Enabled: @([[mSession profileInfo] canEditAddress]),
                               }]; //address line 1
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_ADDRESSLINE2),      //lokalise 4 feb
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_Key: kAddress2,
                               kForm_Value: self.companyAddress.address2,
                               kForm_Optional: @(YES),
                               kForm_Enabled: @([[mSession profileInfo] canEditAddress]),
                               }];
}

-(void) setupBankDetailsArray: (NSMutableArray *) bankDetailsArray
{
    NSDictionary* bankDetails = self.workshopData[@"REG_Bank"];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BENEFICIARYNAME),      //lokalise 4 feb
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_Key: kBeneficiaryName,
                                   kForm_Placeholder: LOCALIZATION(C_FORM_BENEFICIARYNAME_PLACEHOLDER),      //lokalise 4 feb
                                   kForm_Value: [bankDetails[@"BeneficiaryName"] isKindOfClass: [NSNull class]] ? @"" : bankDetails[@"BeneficiaryName"],
                                   }];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BENEFICIARYACCTNUM),      //lokalise 4 feb
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                   kForm_Key: kAccountNumber,
                                   kForm_SecureEntry: @(YES),
                                   kForm_Value: [bankDetails[@"AccountNumber"] isKindOfClass: [NSNull class]] ? @"" : bankDetails[@"AccountNumber"],
                                   kForm_HasDelegate : @(IS_COUNTRY(COUNTRYCODE_INDIA)), //textfield delegate for account number
                                   }];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_CONFIRMBENEFICIARYNUM),  //lokalised 4 feb
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                   kForm_Key: kConfirmAccountNumber,
                                   kForm_Value: [bankDetails[@"AccountNumber"] isKindOfClass: [NSNull class]] ? @"" : bankDetails[@"AccountNumber"],
                                   }];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BENEFICIARYIFSC),      //lokalise 4 feb
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_Key: kIFSCCode,
                                   kForm_Value: [bankDetails[@"IFSCCode"] isKindOfClass: [NSNull class]] ? @"" : bankDetails[@"IFSCCode"],
                                   kForm_HasDelegate : @(IS_COUNTRY(COUNTRYCODE_INDIA)), //textfield delegate for ifsc code
                                   }];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BANKNAME),      //lokalise 4 feb
                                   kForm_Type: @(kREGISTRATION_DROPDOWN),
                                   kForm_Placeholder: LOCALIZATION(C_DROPDOWN_BANKNAME),      //lokalise 4 feb
                                   kForm_Key: kBankID,
                                   kForm_Value: [bankDetails[@"BankID"] isKindOfClass:[NSNull class]] ? @"" : [mSession convertToBankName: [[bankDetails objectForKey: @"BankID"] intValue]],
                                   kForm_Enabled: @(NO),
                                   }];
    
    [bankDetailsArray addObject: @{kForm_Type: @(kFORM_PROFILE_IMAGE),
                                   kForm_Key: kBeneficiaryImage, //for UI only
                                   kForm_Value: @"cheque_sample",
                                   }]; //
}

#pragma mark - bank account validation
-(void) setupAccountValidationArray: (NSMutableArray *) cardValidationArray {
    
    NSDictionary *panDetails = self.workshopData[@"REG_PAN"];
    NSDictionary *drivingLicenseDetails = self.workshopData[@"REG_DrivingLicense"];
    NSDictionary *voterDetails = self.workshopData[@"REG_VoterID"];
    
    NSString *checkboxString = @"";
    if(![panDetails[@"PANCardId"] isKindOfClass: [NSNull class]])
       checkboxString = [checkboxString stringByAppendingString:@"0"];
    if(![drivingLicenseDetails[@"DrivingLicenseID"] isKindOfClass: [NSNull class]])
        checkboxString = [checkboxString isEqualToString:@""] ? [checkboxString stringByAppendingString:@"1"] : [checkboxString stringByAppendingString:@",1"];
    if(![voterDetails[@"VoterID"] isKindOfClass: [NSNull class]])
        checkboxString = [checkboxString isEqualToString:@""] ? [checkboxString stringByAppendingString:@"2"] : [checkboxString stringByAppendingString:@",2"];
    
    
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(@"Validate Account by"),  //lokalised 4 feb
                                      kForm_Type: @(kFORM_MULTICHECKBOX),
                                      kForm_Key: kValidateCheckboxes,
                                      kForm_Enabled: @(YES),
                                      kForm_HasDelegate: @(YES),
                                      kForm_CheckboxLabels: @[LOCALIZATION(C_FORM_PANCARD),
                                                              LOCALIZATION(C_FORM_DRIVINGLICENSEID),
                                                              LOCALIZATION(C_FORM_VOTERID),
                                                              ],
                                      kForm_LinkedCellKeys: @[kPANCardId,
                                                              kDrivingLicense,
                                                              kVoterID,
                                                              ],
                                     kForm_Value: checkboxString,
                                      }];
    
    
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_PANCARDID), //lokalised 8 Feb
                                      kForm_Type: @(kFORM_PANCARD_ID_NEW),
                                      kForm_Value: panDetails[@"PANCardId"],
                                      kForm_Key: kPANCardId,
                                      //                                   kForm_Optional: @(YES),  //controlled in formdelegate level.
                                      }];
    
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_PANCARDIMAGE),  //lokalised 8 Feb
                                      kForm_Type: @(
                                          kFORM_PANCARD_IMAGE),
                                      kForm_Value: panDetails[@"PANCardImage"],
                                      kForm_Key: kPANCardImage,
                                      kForm_Placeholder: LOCALIZATION(C_FORM_PANCARDIMAGE_PLACEHOLDER),    //lokalised 8 Feb
                                      //                                   kForm_Optional: @(YES),  //controlled in formdelegate level.
                                      }];
    
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_DRIVINGLICENSEID), //lokalised 8 Feb
                                      kForm_Type: @(kFORM_DRIVERLICENSE),
                                      kForm_Value: drivingLicenseDetails[@"DrivingLicenseID"],
                                      kForm_Key: kDrivingLicense,
                                      //                                   kForm_Optional: @(YES),  //controlled in formdelegate level.
                                      }];
    
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_DRIVINGLICENSEIMAGE),  //lokalised 8 Feb
                                      kForm_Type: @(kFORM_PANCARD_IMAGE),//share with pan card image
                                      kForm_Value: drivingLicenseDetails[@"DrivingLicenseImage"],
                                      kForm_Key: kDrivingLicenseImage,
                                      kForm_Placeholder: LOCALIZATION(C_FORM_PANCARDIMAGE_PLACEHOLDER),    //lokalised 8 Feb
                                      //                                   kForm_Optional: @(YES),  //controlled in formdelegate level.
                                      }];
    
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_VOTERID), //lokalised 8 Feb
                                      kForm_Type: @(kFORM_VOTERID),
                                      kForm_Value: voterDetails[@"VoterID"],
                                      kForm_Key: kVoterID,
                                      //                                   kForm_Optional: @(YES),  //controlled in formdelegate level.
                                      }];
    
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_VOTERIDIMAGE),  //lokalised 8 Feb
                                      kForm_Type: @(kFORM_PANCARD_IMAGE), //share with pan card image
                                      kForm_Value: voterDetails[@"VoterIDImage"],
                                      kForm_Key: kVoterIDImage,
                                      //                                   kForm_Optional: @(YES),  //controlled in formdelegate level.
                                      }];
        
}

-(void) setupContactPrefArray: (NSMutableArray*) contactPrefArray {
    //contact preference
    [contactPrefArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_CONTACT_PREFERENCE),  //lokalised 4 feb
                                   kForm_Type: @(kREGISTRATION_CONTACTPREF),
                                   kForm_Key: kContactPreference,
                                   kForm_Enabled: @(YES),
                                   kForm_BackgroundColour: COLOUR_WHITE,
                                   kForm_Value: self.member.contactPreference,
                                   kForm_Optional: @(NO),
                                   }];
}


-(void) setupLastSectionArray: (NSMutableArray *)lastSectionArray
{
    if (IS_COUNTRY(COUNTRYCODE_RUSSIA))
    {
        NSString *digitalSignatureString = [self.workshopData objectForKey: kDigitalSignature];
        if ([digitalSignatureString isKindOfClass: [NSNull class]])
            digitalSignatureString = @"";
        [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_RUSSIA_CUSTOMERSIGNATURE),      //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_SIGNATURE),
                                       kForm_Key: kDigitalSignature,
                                       kForm_Enabled: @(YES),
                                       kForm_Value: digitalSignatureString,
                                       }];
    }
    
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BANKAGREE), //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_TNCCHECKBOX),
                                       kForm_Key: kBankCheckBox,
                                       kForm_Enabled: @(YES),
                                       }];
    }
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA) || IS_COUNTRY(COUNTRYCODE_RUSSIA))
    {
        
        [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_TNC_IN), //lokalise 4 feb
                                       kForm_Type: @(kREGISTRATION_TNCCHECKBOX),
                                       kForm_Key: kTncCheckBox,
                                       kForm_Hyperlinks: @{LOCALIZATION(C_FORM_TNC_IN): @"tnc", //lokalise 4 feb
                                                           LOCALIZATION(C_PROFILE_PRIVACY) : @"privacypolicy",  //lokalise 4 feb
                                                           },
                                       kForm_Enabled: @(YES),
                                       }];
    }
    
    [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_REGISTER), //lokalise 4 feb
                                   kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                   kForm_Key: kRegisterCTA,
                                   kForm_Enabled: @(YES),
                                   kForm_TextColour: COLOUR_WHITE,
                                   kForm_BackgroundColour: COLOUR_RED,
                                   }]; //Register
    
    [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_UPDATE), //lokalise 4 feb
                                   kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                   kForm_Key: kUpdateCTA,
                                   kForm_Enabled: @(YES),
                                   kForm_TextColour: COLOUR_VERYDARKGREY,
                                   kForm_BackgroundColour: COLOUR_YELLOW,
                                   }]; //Update
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_FETCH_POSTALADDRESS:
        {
            NSDictionary* responseDictionary = [response getGenericResponse];
            NSDictionary* postalDetails = responseDictionary[@"PostalDetails"];
            
            NSString* stateId = postalDetails[@"StateId"];
            NSString* townName = postalDetails[@"TownName"];
            
            if([stateId isKindOfClass:NSString.class])
            {
                NSString* stateName = [mSession convertToStateName:stateId stateDetailedListArray:GET_LOADSTATE];

                [self.tableViewDelegate editCell: kCompanyCity withTextfieldString: townName];
                [self.tableViewDelegate editCell: kCompanyState withTextfieldString: stateName];
            }
        }
            break;
        case kWEBSERVICE_FETCH_BANKDETAILS:
        {
            [self.tableViewDelegate editCell: kBankID withDropdownSelection: [[response getGenericResponse] objectForKey: @"BankName"]];    //Bank Name list
        }
            break;
            
        case kWEBSERVICE_STATE:
        {
            SET_LOADSTATE([[response getGenericResponse] objectForKey: @"StateDetailsList"]);
            self.stateTable = [[response getGenericResponse] objectForKey: @"StateDetailsList"];
            if (![[[mSession lookupTable] objectForKey: @"IsShowCompanyCategory"] boolValue]) {
                if(![GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_INDIA]){
                    [self setupInterface];
                    [UpdateHUD removeMBProgress: KEY_WINDOW];
                }
            }
        }
            break;
        case kWEBSERVICE_UPDATEWORKSHOP:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                //non-assisted pop back to root screen
                if (self.detailsDelegate)
                    [self.detailsDelegate didUpdateWorkshop]; //details is in tabcontroller
                else
                    [self popSelf];
            }];
        }
            break;
        case kWEBSERVICE_UPDATEIM:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                //non-assisted pop back to root screen
                if (self.detailsDelegate)
                    [self.detailsDelegate didUpdateWorkshop]; //details is in tabcontroller
                else
                    [self popSelf];
            }];
        }
            
            break;
        case kWEBSERVICE_TRADE_LOADCOMPANYCATEGORY:
        {
            self.companyCategoryList = [[response getGenericResponse] objectForKey:@"CompanyCategory"];
            if([self.companyCategoryList isKindOfClass:[NSNull class]] || self.companyCategoryList == nil) {
                self.companyCategoryList = [[mSession lookupTable] objectForKey: @"CompanyCategory"];
            }
            [self setupInterface];
            [UpdateHUD removeMBProgress: KEY_WINDOW];
        }
            break;
        default:
//            [self popUpViewWithTitle:@"Successful" content:@"Mechanic details has been updated"];
            break;
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress: KEY_WINDOW];
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
            self.stateTable = GET_LOADSTATE;
            
            [self setupInterface];
            break;
        default:
            break;
    }
}

#pragma mark birthdate dropdown delegate methods
-(void)birthdatePressedWithKey:(NSString *)formKey
{
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}

- (void)selectedDate:(NSDate *)selectedDate
{
    //do smething
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //   for API
    [formatter setDateFormat:@"dd"];
    self.member.dobDay = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"MM"];
    self.member.dobMonth = [formatter stringFromDate:selectedDate];
    
    if(kIsRightToLeft) {
        [formatter setDateFormat:@"MMMM"];
    } else {
        [formatter setDateFormat:@"dd MMMM"];
    }
    
    if ([GET_LOCALIZATION isEqualToString: kThai])
        [formatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
    else
        [formatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    NSString *selectedDateString = [formatter stringFromDate:selectedDate];
    
    //add formkey to birthday class if needed
    if(kIsRightToLeft) {
        [self.tableViewDelegate editCell: kDOB withDropdownSelection: [NSString stringWithFormat:@"%@ %@",self.member.dobDay,selectedDateString]];
    } else {
        [self.tableViewDelegate editCell: kDOB withDropdownSelection: selectedDateString];
    }
}

#pragma mark Date methods
-(NSString *) convertDateForDisplay:(NSString *)monthString day:(NSString *) dayString
{
    if ([monthString isEqualToString: BLANK] || [dayString isEqualToString: BLANK])
        return LOCALIZATION(C_PROFILE_DOB_PLACEHOLDER); //lokalise 4 feb
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    if ([GET_LOCALIZATION isEqualToString: kThai])
        [dateFormat setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
    else
        [dateFormat setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    [dateFormat setDateFormat:@"MMdd"];
    NSDate *date = [dateFormat dateFromString: [NSString stringWithFormat: @"%@%@", monthString, dayString]];
    
    // Convert date object to desired output format
    [dateFormat setDateFormat:@"dd MMMM"];
    return [dateFormat stringFromDate:date];
}

-(void)hyperlinkPressed:(NSString *) link
{
    //kForm_Hyperlinks: @{LOCALIZATION(C_FORM_TNC_IN): @"tnc",
    //                    LOCALIZATION(C_PROFILE_PRIVACY) : @"privacypolicy"
    if ([link isEqualToString: @"tnc"])
    {
        [mSession pushTNCView: self];
    }
    else if ([link isEqualToString: @"privacypolicy"])
    {
        [mSession pushPrivacyView: self];
    }
}


#pragma mark QR Code Scanner delegate
- (void)qrCodeScanSuccess:(NSString *)scanString
{
    [self.tableViewDelegate editCell:kWorkshopQRCode withTextfieldButtonText:[[scanString componentsSeparatedByString:@"="] lastObject]];
}

#pragma mark signature delegate
-(void) signaturePressed:(NSString *)key
{
    [mSession pushRUCustomerSignatureWithDelegate: self.navigationController.parentViewController delegate: self];
}
-(void)didFinishSignature:(UIImage *)image
{
    [self.tableViewDelegate editCell: kDigitalSignature withSignatureImage: image];
}

#pragma mark - Company Category
-(NSArray *) getCompanyCategoryValues {
    NSMutableArray *keyValueArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in self.companyCategoryList)
    {
        [keyValueArray addObject: [dict objectForKey: @"KeyValue"]];
    }
    return keyValueArray;
}

- (NSString *) convertToCompanyCategoryCode: (NSString *)keyValue
{
    for (NSDictionary *dict in self.companyCategoryList)
    {
        if ([keyValue isEqualToString: [dict objectForKey: @"KeyValue"]])
        {
            return [dict objectForKey: @"KeyCode"];
        }
    }
    return @"";
}

- (NSString *) convertCodeToValue:(NSInteger) keyCode
{
    for (NSDictionary *dict in self.companyCategoryList)
    {
        if (keyCode == [[dict objectForKey: @"KeyCode"] intValue])
        {
            return [dict objectForKey: @"KeyValue"];
        }
    }
    return @"";
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
