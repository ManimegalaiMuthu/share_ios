//
//  WorkshopDetailsViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 16/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "WorkshopDetailsViewController.h"
#import "CASDatePickerView.h"
#import "WorkingHoursViewController.h"
#import "QRCodeScanner.h"
#import "GalleryUploadViewController.h"
#import "RUCustomerSignatureViewController.h"

enum kWorkshopDetails_TYPE
{
    kWorkshopDetails_WORKSHOPHEADER,
    
    kWorkshopDetails_COMPANYSOURCE,
    kWorkshopDetails_APPROVALSTATUS,
    kWorkshopDetails_ACTIVATIONSTATUS,
    kWorkshopDetails_ACTIVATIONREMARKS,
    
    kWorkshopDetails_WORKSHOPTYPE,
    kWorkshopDetails_WORKSHOPCATEGORY,
    kWorkshopDetails_REGWORKSHOPNAME,
    kWorkshopDetails_LOCATION,
    kWorkshopDetails_WORKSHOPQRCODE,
    kWorkshopDetails_WORKSHOPCODE,
    
    kWorkshopDetails_WORKSHOPTELEPHONE,
    kWorkshopDetails_WORKSHOPEMAIL,
    kWorkshopDetails_ADDRESSLINEONE,
    kWorkshopDetails_ADDRESSLINETWO,
    kWorkshopDetails_TOWN,
    kWorkshopDetails_STATE,
    kWorkshopDetails_POSTALCODE,
    kWorkshopDetails_WORKINGHOURS,
    
    kWorkshopDetails_OILCHANGEPERDAY,
    kWorkshopDetails_SYNTHETICPERDAY,
    kWorkshopDetails_HALFSYNTHETICPERDAY,
    kWorkshopDetails_SHELLHELIXPERDAY,
    
    kWorkshopDetails_OWNERHEADER,
    kWorkshopDetails_SALUTATION,
    kWorkshopDetails_FIRSTNAME,
    kWorkshopDetails_LASTNAME,
    kWorkshopDetails_MOBNUM,
    kWorkshopDetails_EMAIL,
    kWorkshopDetails_DOB,
};

#define CELL_HEIGHT 72

#define COLVIEW_CELLHEIGHT      50
#define COLVIEW_LINESPACING     10

#define kUserID             @"UserID"
#define kDSRCode            @"DSRCode"
#define kUserPassword       @"UserPassword"
#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kEmailAddress       @"EmailAddress"
#define kDOB                @"DOB"
#define kDOBMonth           @"DOBMonth"
#define kDOBDay             @"DOBDay"
#define kCountryCode        @"CountryCode"
#define kReferenceCode          @"ReferenceCode"        //Workshop Code
#define kWorkshopQRCode         @"WorkshopQRCode"       //Workshop SHARE Code (for QR Scan)
#define kCompanySource          @"CompanySource"
#define kCompanyType            @"CompanyType"
#define kCompanyCategory            @"CompanyCategory" //to change again?
#define kCompanyName            @"CompanyName"
#define kCompanyEmailAddress    @"CompanyEmailAddress"
#define kCompanyTier            @"CompanyTier"
#define kContactNumber          @"ContactNumber"
#define kOilChangePerMonth      @"OilChangePerMonth"
#define kOilChangePercentage    @"OilChangePercentage"
#define kHalfSyntheticOilChanges    @"HalfSyntheticOilChanges"
#define kShellHelixQty          @"ShellHelixQty"
#define kStatus                 @"Status"
#define kRemarks                @"Remarks"
#define kAddressType    @"AddressType"
#define kAddress1       @"Address1"
#define kAddress2       @"Address2"
#define kStreetName     @"StreetName"
#define kCompanyCity    @"CompanyCity"
#define kCompanyState   @"CompanyState"
#define kPostalCode     @"PostalCode"
#define kLongitude      @"Longitude"
#define kLatitude       @"Latitude"

#define kWorkingHours       @"WorkingHours"
#define kWorkshopLocation   @"Location"

#define kApprovalStatus         @"ApprovalStatus"
#define kActivationStatus       @"ActivationStatus"

@interface WorkshopDetailsViewController () <WorkingHoursViewDelegate, UITextFieldDelegate, WebServiceManagerDelegate, CommonListDelegate, DatePickerDelegate, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, QRCodeScannerDelegate, GalleryUploadViewDelegate, UITextViewDelegate, RUCustomerSignatureDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewToBottom;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@property CASDatePickerView *dateView;

@property (weak, nonatomic) IBOutlet UILabel *lblContactPrefHeader;

@property (weak, nonatomic) IBOutlet UIButton *btnUpload;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;

@property (weak, nonatomic) IBOutlet UIButton *btnManageMechanic;

#pragma mark contact Pref
@property (weak, nonatomic) IBOutlet UIView *contactPrefView;
@property (weak, nonatomic) IBOutlet UICollectionView *colView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *colViewHeight; //to adjust according to number of cells <1 + (count / 2)>
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *colViewLayout; //to adjust cell width size according to screen width
@property NSMutableArray *contactPrefArray;


//@property (strong, nonatomic) IBOutlet MKMapView *mapView;
//@property (weak, nonatomic) IBOutlet UIButton *btnAddCustomer;

@property UIImageView *workshopProfilePicture;

@property NSDictionary *workingHoursDict;
@property NSArray *stateTable;

@property Member *member;
@property CompanyInfo *companyInfo;
@property CompanyAddress *companyAddress;

//@property (weak, nonatomic) IBOutlet UITableView *workingHoursTableView;

@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@property CLLocationManager *locationManager;

@property NSMutableArray *registrationFormArray; //ui only
@property NSMutableDictionary *regSubmissionDict;  //key as json key, value = cell. retrieve value by using getJson method
@property (weak, nonatomic) IBOutlet UITableView *regFormTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight; //to adjust according number of cells

@property NSArray *workshopImagesData;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *signatureHeight; //0 if not russia
@property (weak, nonatomic) IBOutlet UIView *signatureView;
@property (weak, nonatomic) IBOutlet UILabel *lblSignature;
@property (weak, nonatomic) IBOutlet UIImageView *imgSignature;

@property (weak, nonatomic) IBOutlet UIView *agreeTNC;
@property (weak, nonatomic) IBOutlet UIButton *btnTncCheckbox;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblTnc;

@property (weak, nonatomic) IBOutlet UILabel *lblRequiredFields;

@property NSArray *companyCategoryList;

@end

@implementation WorkshopDetailsViewController
@synthesize listView;
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
//    //QRCode Delegate
//    mQRCodeScanner.delegate = self;
//    
//    self.member = [[Member alloc] init];
//    self.companyInfo = [[CompanyInfo alloc] init];
//    self.companyAddress = [[CompanyAddress alloc] init];
//    
//    self.member.countryCode = GET_COUNTRY_CODE;
//    
//    self.locationManager = [[CLLocationManager alloc] init];
//    self.locationManager.delegate = self;
//    self.locationManager.distanceFilter = kCLDistanceFilterNone;
//    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//    
//    
//    [self.colView registerNib: [UINib nibWithNibName: @"ContactPreferencesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ContactPrefCell"];
    
    //PENDING WORKSHOP DETAILS.
    //hide manage mechanics.
    if (!self.isApproved)
    {
        [self.btnManageMechanic removeFromSuperview];
    }
    
    self.lblSignature.text = LOCALIZATION(C_RUSSIA_CUSTOMERSIGNATURE);      //lokalised
    
#warning - remove from storyboard later
    [self.contactPrefView removeFromSuperview];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
//    [self setupInterface];
    
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_COUNTRY)        //lokalised
                                                   list: [mSession getCountryNames]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
    
    dateView = [[CASDatePickerView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_BIRTHDAY) previouslySelected:nil withYearHidden:YES];       //lokalised
    dateView.delegate = self;
    
    [self.btnManageMechanic.titleLabel setFont: FONT_BUTTON];
    [self.btnManageMechanic setTitle:LOCALIZATION(C_FORM_MANAGEMECHANIC) forState:UIControlStateNormal];        //lokalise 28 Jan
    
    [self.btnUpload.titleLabel setFont: FONT_BUTTON];
    [self.btnUpload setTitle:LOCALIZATION(C_FORM_UPLOADWORKSHOPIMAGE) forState:UIControlStateNormal]; //lokalise 287 Jan
    
    [self.btnRegister.titleLabel setFont: FONT_BUTTON];
    if (self.isApproved)
        [self.btnRegister setTitle:LOCALIZATION(C_FORM_UPDATE) forState:UIControlStateNormal];  //lokalised
    else
        [self.btnRegister setTitle:LOCALIZATION(C_FORM_REGISTER) forState:UIControlStateNormal];  //lokalsie 28 Jan
    [Helper setCustomFontButtonContentModes: self.btnManageMechanic];
    
//    self.btnManageMechanic.hidden = ![[mSession profileInfo] hasDSRManageStaff];
    
    
    NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @"*" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B3, }];
    
    NSMutableAttributedString *requiredFieldsAttrString = [[NSMutableAttributedString alloc] initWithString: LOCALIZATION(C_PROFILE_REQUIREDFIELDS) attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B3, }];       //lokalised
    
    [asteriskAttrString appendAttributedString:  requiredFieldsAttrString];
    
    //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
    self.lblRequiredFields.attributedText = asteriskAttrString;
    
    if(![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
    {
        self.signatureHeight.constant = 0;

        [self.agreeTNC removeFromSuperview];
    }
    else
    {
        self.lblSignature.text = LOCALIZATION(C_RUSSIA_CUSTOMERSIGNATURE);      //lokalised
        
        if (self.isApproved)
        {
            self.signatureHeight.constant = 0;
            self.imgSignature.hidden = YES;
            [self.agreeTNC removeFromSuperview];
        }
        else
        {
            [Helper setHyperlinkLabel: self.lblTnc hyperlinkText: LOCALIZATION(C_RU_STEPFOUR_TNCLINK) hyperlinkFont: FONT_B2 bodyText: LOCALIZATION(C_RU_STEPFOUR_TNC) bodyFont:FONT_B2 urlString: @"WorkshopTNC"];  //lokalise 28 Jan
//            [Helper addHyperlink:self.lblTnc hyperlinkText: LOCALIZATION(C_RU_STEPFOUR_PRIVACYLINK) hyperlinkFont: FONT_B2 urlString:@"Workshop" bodyText: self.lblTnc.text];
        }
    }
    
    [self setupData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    
    [[IQKeyboardManager sharedManager] setEnable: NO];
}

-(void) setupGoogleAnalytics
{
//    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//    if (self.isApproved)
//        [tracker set:kGAIScreenName value:@"Approved Workshop detail"];
//    else
//        [tracker set:kGAIScreenName value:@"Pending Workshop detail"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void) setupRegistrationForm
{
    self.member = [[Member alloc] initWithData:[self.workshopData objectForKey: @"REG_Member"]];
    self.companyInfo = [[CompanyInfo alloc] initWithData: [self.workshopData objectForKey: @"REG_Company"]];
    
    self.btnManageMechanic.hidden = !self.companyInfo.hasDSRManageStaff;
    
    self.companyAddress = [[CompanyAddress alloc] initWithData: [self.workshopData objectForKey: @"REG_CompanyAddress"]];
    //    [self.workshopData objectForKey: @"REG_CompanyStatus"];
    self.workingHoursDict = [self.workshopData objectForKey: @"REG_CompanyWorkHours"];
    if (!self.workingHoursDict ||
        [self.workingHoursDict isKindOfClass: [NSNull class]])
        self.workingHoursDict = @{@"WorkHourStart1":   @"0:00",
                                  @"WorkHourEnd1":     @"0:00",
                                  @"WeekDays1":        @"",
                                  @"PublicHoliday":    @"0",
                                  @"WorkHourStart2":   @"0:00",
                                  @"WorkHourEnd2":     @"0:00",
                                  @"WeekDays2":        @"",
                                  };
    
    
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormTextfieldCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormTextfieldCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationButtonCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormDropdownCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormDropdownCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationHeaderTableViewCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationLabelTextfieldTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationLabelTextfieldTableViewCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationMobileNumberCell" bundle:nil] forCellReuseIdentifier:@"RegistrationMobileNumberCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationTextfieldWithButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextfieldWithButtonCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationTextViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextViewTableViewCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationWorkingHoursTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationWorkingHoursTableViewCell"];
    
    self.regSubmissionDict = [[NSMutableDictionary alloc] init];
    
#pragma mark - Workshop Profile Setup
    self.registrationFormArray = [[NSMutableArray alloc] init];

    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_HEADER_WORKSHOPPROFILE),
                                      @"Type": @(kREGISTRATION_HEADER),     //loakilse 28 Jan
                                             }];
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_COMPANYSOURCE),   //lokalised
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key": kCompanySource,
//                                      @"Value": self.companyInfo.companySource,
                                      @"Value": [self.companyInfo.companySource boolValue] ? [mSession convertToCompanySource: [self.companyInfo.companySource intValue]] : LOCALIZATION(C_PROFILE_COMPANYSOURCE_PLACEHOLDER),
                                             }];    //lokalise 28 Jan
    
    [self.registrationFormArray addObject:
                                    @{@"Title": LOCALIZATION(C_PROFILE_APPROVALSTATUS), //lokalise 28 Jan
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Enabled": @(NO),
                                      @"Key": kApprovalStatus,
                                      @"Value": self.companyInfo.approvalStatus,
                                      }];   //approval status
    
    [self.registrationFormArray addObject: @{@"Title": [self getActivationStatus],
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key": kActivationStatus,
                                      @"Value": (self.isApproved) ? [mSession convertToCompanyStatus: self.companyInfo.status] : [mSession convertToLeadStatus: self.companyInfo.status],
                                             }];   //activation status
    
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_ACTIVATIONREMARKS),  //lokalise 28 jan
                                      @"Placeholder": LOCALIZATION(C_PROFILE_ACTIVATIONREMARKS_PLACEHOLDER), //lokalise 28 jan
                                      @"Type": @(kREGISTRATION_TEXTVIEW),
                                      @"Key": kRemarks,
                                      @"Value": self.companyInfo.remarks,
                                             }];   //activation remarks
                                    

    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_WORKSHOPTYPE),  //loklaise 28 Jan
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key": kCompanyType,
                                      @"Value": [mSession convertToCompanyTypes: self.companyInfo.companyType],
                                             }];    //workshop type
    
    if ([[[mSession lookupTable] objectForKey: @"IsShowCompanyCategory"] boolValue])
    {
        [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_WORKSHOPCATEGORY), //lokalise 28 jan
    //                                      @{@"Title": LOCALIZATION(C_PROFILE_WORKSHOPCATEGORY),
                                          @"Type": @(kREGISTRATION_DROPDOWN),
                                          @"Key": kCompanyCategory,
                                          @"Value": [self convertCodeToValue: self.companyInfo.companyCategory],
                                                 }];    //workshop category
      }
    
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_COMPANYNAME), //lokalise 28 Jan
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kCompanyName,
                                      @"Value": self.companyInfo.companyName,
                                             }]; //company name
    
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_LOCATION),    //lokalise 28 Jan
                                      @"Type": @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                                      @"TextfieldEnabled": @(NO),
                                      @"Icon": @"icon-location",
                                      @"Key": kWorkshopLocation,
                                      @"Enabled": @(YES),
                                      @"Value": [NSString stringWithFormat: @"%f, %f", self.companyAddress.latitude, self.companyAddress.longitude],
//                                             @"Optional": @(YES), //Made required GLOBAL after VN addition
                                             }];    //location

    if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_VIETNAM]) {
        [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_WORKSHOPSHARECODE), //lokalise 28 Jan
                                                 @"Type": @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                                                 @"Icon": @"icon-code",
                                                 @"Key": kWorkshopQRCode,
                                                 @"Value": self.companyInfo.workshopQRCode,
                                                 @"Enabled": @(YES),
                                                 @"Optional":@(NO),
                                                 }];   //Workshop SHARE Code
    } else {
        [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_WORKSHOPSHARECODE), //lokalise 28 Jan
                                                 @"Type": @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                                                 @"Icon": @"icon-code",
                                                 @"Key": kWorkshopQRCode,
                                                 @"Value": self.companyInfo.workshopQRCode,
                                                 @"Enabled": @(YES),
                                                 @"Optional":@(YES),
                                                 }];   //Workshop SHARE Code
    }
    
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_WORKSHOPCODE),        //lokalise 28 jan
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kReferenceCode,
                                      @"Value": self.companyInfo.referenceCode,
                                             @"Optional": @(![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA]),
                                             }];   //Workshop Code
  
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_CONTACTNUMBER),   //lokalise 28 Jan
                                      @"Placeholder": LOCALIZATION(C_PROFILE_CONTACTNUMBER_PLACEHOLDER), //lokalise 28 Jan
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Keyboard": @(UIKeyboardTypeNumberPad),
                                      @"Key": kContactNumber,
                                      @"Value": self.companyInfo.contactNumber,
                                         @"Optional": @(![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA]),
                                             }]; //contact number

    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_EMAILADDRESS),  //lokalise 28 Jan
                                      @"Placeholder": (self.isApproved) ? LOCALIZATION(C_PROFILE_WORKSHOPEMAILADDRESS) : LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER),  //lokalise 28 jan
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Keyboard": @(UIKeyboardTypeEmailAddress),
                                      @"Key": kCompanyEmailAddress,
                                      @"Value": self.companyInfo.companyEmailAddress,
                                             @"Optional": @(YES),
                                             }]; //last name

    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_ADDRESSLINE1), //lokalised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kAddress1,
                                      @"Value": self.companyAddress.address1,
                                             @"Enabled": (!self.isApproved) ? @([[mSession profileInfo] canEditAddress]) : @(NO),
                                             }]; //last name

    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_ADDRESSLINE2),  //lokalised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kAddress2,
                                      @"Value": self.companyAddress.address2,
                                             @"Enabled": (!self.isApproved) ? @([[mSession profileInfo] canEditAddress]) : @(NO),
                                             @"Optional": @(YES),
                                             }];
    
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_TOWN),  //lokalised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kCompanyCity,
                                      @"Value": self.companyAddress.companyCity,
                                             @"Enabled": (!self.isApproved) ? @([[mSession profileInfo] canEditAddress]) : @(NO),
                                             @"Optional": @(![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA]),
                                             }];
    
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_STATE),  //lokaliose 28 Jan
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key": kCompanyState,
                                      @"Value": [mSession convertToStateName:self.companyAddress.companyState stateDetailedListArray:self.stateTable],
                                             @"Enabled": (!self.isApproved) ? @([[mSession profileInfo] canEditAddress]) : @(NO),
                                             }];
    
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_POSTALCODE),  //loklaised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kPostalCode,
                                      @"Keyboard": @(UIKeyboardTypeNumberPad),
                                      @"Value": self.companyAddress.postalCode,
                                             @"Enabled": (!self.isApproved) ? @([[mSession profileInfo] canEditAddress]) : @(NO),
                                             }];

    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_WORKINGHOURS),        //lokalise 28 Jan
//                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Type": @(kREGISTRATION_WORKINGHOURS),
                                      @"Key": kWorkingHours,
//                                      @"Value": LOCALIZATION(C_PROFILE_WORKINGHOURS),
                                             }];
    
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_OILCHANGEPERDAY),     //lokalise 28 Jan
                                      @"Placeholder": LOCALIZATION(C_PROFILE_ANSWER),
                                      @"Type": @(kREGISTRATION_LABELWITHTEXTFIELD),
                                      @"Key": kOilChangePerMonth,
                                      @"Keyboard": @(UIKeyboardTypeNumberPad),
                                      @"Value": self.companyInfo.oilChangePerMonth,
                                             }];
    
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_OILCHANGEPERCENT),    //lokalise 28 Jan
                                      @"Placeholder": LOCALIZATION(C_PROFILE_ANSWER),
                                      @"Type": @(kREGISTRATION_LABELWITHTEXTFIELD),
                                      @"Key": kOilChangePercentage,
                                      @"Keyboard": @(UIKeyboardTypeDecimalPad),
                                      @"Value": self.companyInfo.oilChangePercentage,
                                             }];
    
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_SEMIOILCHANGEPERCENT),  //lokalise 28 Jan
                                      @"Placeholder": LOCALIZATION(C_PROFILE_ANSWER),
                                      @"Type": @(kREGISTRATION_LABELWITHTEXTFIELD),
                                      @"Key": kHalfSyntheticOilChanges,
                                      @"Keyboard": @(UIKeyboardTypeDecimalPad),
                                      @"Value": self.companyInfo.halfSyntheticOilChanges,
                                             }];

    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_SHELLHELIX),              //lokalise 28 Jan
                                      @"Placeholder": LOCALIZATION(C_PROFILE_ANSWER),
                                      @"Type": @(kREGISTRATION_LABELWITHTEXTFIELD),
                                      @"Key": kShellHelixQty,
                                      @"Keyboard": @(UIKeyboardTypeNumberPad),
                                      @"Value": self.companyInfo.shellHelixQty,
                                             }];
    
#pragma mark - Owner Profile Form setup
    
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_HEADER_OWNERPROFILE),             //lokailse 28 Jan
                                      @"Type": @(kREGISTRATION_HEADER),
                                             }];

    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_SALUTATION),              //lokalise 28 Jan
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key": kSalutation,
                                      @"Value": [mSession convertToSalutation: self.member.salutation],
                                             @"Optional": @(YES),
                                             }];  //button

    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_FIRSTNAME),               //lokalise 28 Jan
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kFirstName,
                                      @"Value": self.member.firstName,
                                             }]; //first name
    

    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_LASTNAME),                //lokalise 28 Jan
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kLastName,
                                      @"Value": self.member.lastName,
                                             }]; //last name

    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_DOB),                     //lokalise 28 Jan
                                      @"Type": @(kREGISTRATION_BIRTHDATE),
                                      @"Key": kDOB,
                                      @"Value": [self convertDateForDisplay: self.member.dobMonth day:self.member.dobDay],
                                             }];   //Date of birth

    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_MOBILENUM),               //loklaise 28 Jan
                                      @"Type": @(kREGISTRATION_MOBNUM),
                                      @"Key": kMobileNumber,
                                      @"TextfieldEnabled": @(!self.isApproved), //if not approved, allow editing
                                      @"CountryCode": [mSession convertCountryCodeToCountryName: self.member.countryCode],
                                      @"MobileNumber": self.member.mobileNumber,
                                             }];   //Mobile number

    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_OWNEREMAILADDRESSOPT),        //loklaise 28 Jan
                                      @"Placeholder": LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER),     //loklaise 28 Jan
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kEmailAddress,
                                      @"Keyboard": @(UIKeyboardTypeEmailAddress),
                                      @"Value": self.member.emailAddress,
                                             @"Optional": @(YES),
                                             }]; //email
 
    if (self.isApproved)
        [mSession convertToCompanyStatus: self.companyInfo.status];
    else
        [mSession convertToLeadStatus: self.companyInfo.status];
    
    NSArray *fullWorkWeekDays;
    NSArray *halfWorkWeekDays;
    
    if (![[self.workingHoursDict objectForKey: @"WeekDays1"] isEqualToString: @""])
        fullWorkWeekDays = [[self.workingHoursDict objectForKey: @"WeekDays1"] componentsSeparatedByString: @","];
    
    if (![[self.workingHoursDict objectForKey: @"WeekDays2"] isEqualToString: @""])
        halfWorkWeekDays = [[self.workingHoursDict objectForKey: @"WeekDays2"] componentsSeparatedByString: @","];
    
    CGFloat workingHoursOffset = (44 * ([fullWorkWeekDays count] + [halfWorkWeekDays count]));
    
    if (self.isApproved)
    {
        //set tableview height. 1 shorter cells. 5 cell less (dsm approval + 4 questions). no textview
        self.tableViewHeight.constant = CELL_HEIGHT * ([self.registrationFormArray count] - 5) - (CELL_HEIGHT + 50) + workingHoursOffset;
    }
    else
        //set pending details tableview height. 4 shorter cells + 73 from textview
        self.tableViewHeight.constant = CELL_HEIGHT * [self.registrationFormArray count] - ((CELL_HEIGHT - 50) * 5) + (73) + workingHoursOffset;
    self.regFormTableView.tableFooterView = [UIView new];
    
    [self.regFormTableView reloadData];
    
    if (![[self.workshopData objectForKey: @"DigitalSignature"] isKindOfClass: [NSNull class]] &&
        [GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA] && !self.isApproved)
    {
        //approved workshop dont need digital signature
        NSString *str = [self.workshopData objectForKey: @"DigitalSignature"];
        NSData *data = [[NSData alloc]  initWithBase64EncodedString: [self.workshopData objectForKey: @"DigitalSignature"] options: NSDataBase64DecodingIgnoreUnknownCharacters];
        self.imgSignature.image = [[UIImage alloc] initWithData: data];
    }
}

-(NSString *) getActivationStatus
{
    if (self.isApproved)
        return LOCALIZATION(C_PROFILE_STATUS);          //loklaise 28 Jan
    else
        return LOCALIZATION(C_PROFILE_ACTIVATIONSTATUS); //lokalise 28 Jan
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *fieldDataDict = [self.registrationFormArray objectAtIndex: indexPath.row];
    
//    if (indexPath.row == kWorkshopDetails_COMPANYSOURCE)
    if([[fieldDataDict objectForKey: @"Key"] isEqualToString: kCompanySource])
    {
        if (self.isApproved)
            return 0; ///hide
        else
            return CELL_HEIGHT;
    }
//    if (indexPath.row == kWorkshopDetails_APPROVALSTATUS)
    if([[fieldDataDict objectForKey: @"Key"] isEqualToString: kApprovalStatus])
    {
        if (self.isApproved)
            return 0; ///hide
        else
            return CELL_HEIGHT;
    }
//    if (indexPath.row == kWorkshopDetails_OILCHANGEPERDAY ||
//        indexPath.row == kWorkshopDetails_SYNTHETICPERDAY ||
//        indexPath.row == kWorkshopDetails_HALFSYNTHETICPERDAY ||
//        indexPath.row == kWorkshopDetails_SHELLHELIXPERDAY)
    if ([[fieldDataDict objectForKey: @"Key"] isEqualToString: kOilChangePerMonth] ||
        [[fieldDataDict objectForKey: @"Key"] isEqualToString: kOilChangePercentage] ||
        [[fieldDataDict objectForKey: @"Key"] isEqualToString: kHalfSyntheticOilChanges] ||
        [[fieldDataDict objectForKey: @"Key"] isEqualToString: kShellHelixQty])
    {
        if (self.isApproved)
            return 0; ///hide
        else
            return 50;
    }
//    if (indexPath.row == kWorkshopDetails_WORKINGHOURS)
    if ([[fieldDataDict objectForKey: @"Key"] isEqualToString: kWorkingHours])
    {
        NSArray *fullWorkWeekDays;
        NSArray *halfWorkWeekDays;
        
        if (![[self.workingHoursDict objectForKey: @"WeekDays1"] isEqualToString: @""])
            fullWorkWeekDays = [[self.workingHoursDict objectForKey: @"WeekDays1"] componentsSeparatedByString: @","];
        
        if (![[self.workingHoursDict objectForKey: @"WeekDays2"] isEqualToString: @""])
            halfWorkWeekDays = [[self.workingHoursDict objectForKey: @"WeekDays2"] componentsSeparatedByString: @","];
        
        return 44 + (44 * ([fullWorkWeekDays count] + [halfWorkWeekDays count]));
    }
//    else if (indexPath.row == kWorkshopDetails_ACTIVATIONREMARKS)
    if ([[fieldDataDict objectForKey: @"Key"] isEqualToString: kRemarks])
    {
        if (self.isApproved)
            return 0; ///hide
        else
            return 145;
    }
    else
        return CELL_HEIGHT;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *regFieldData = [self.registrationFormArray objectAtIndex: indexPath.row];
    switch ([[regFieldData objectForKey: @"Type"] intValue])
    {
        case kREGISTRATION_TEXTFIELD:
        {
            RegistrationFormTextfieldCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormTextfieldCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            
            //see if Enabled key is available, if not see if pending or approved
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell enableCell: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            else
                [textfieldCell enableCell: !self.isApproved];
            
            if([[regFieldData objectForKey: @"Key"] isEqualToString: kReferenceCode] && [GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_VIETNAM] && !self.isApproved) {
                [textfieldCell setUserInteractionEnabled: false];
            }
            
            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];

            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [textfieldCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [textfieldCell hideTopLabel: NO];
            [textfieldCell  setTextfieldDelegate: self];
            
            if ([regFieldData objectForKey: @"Value"])
                [textfieldCell setTextfieldText: [regFieldData objectForKey: @"Value"]];
            
            if ([regFieldData objectForKey: @"Placeholder"])
                [textfieldCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
            
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_DROPDOWN:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            //see if Enabled key is available, if not see if pending or approved
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            else
                [dropdownCell setUserInteractionEnabled: !self.isApproved];
            
            if([[regFieldData objectForKey: @"Key"] isEqualToString: kCompanyType] && [GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_VIETNAM]) {
                [dropdownCell setUserInteractionEnabled: false];
            }
            
            
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];

            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [dropdownCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [dropdownCell hideTopLabel: NO];
            
            if ([[regFieldData objectForKey: @"Key"] isEqualToString: kSalutation])
                dropdownCell.button.tag = kWorkshopDetails_SALUTATION;
            else if ([[regFieldData objectForKey: @"Key"] isEqualToString: kCompanyState])
                dropdownCell.button.tag = kWorkshopDetails_STATE;
            else
                dropdownCell.button.tag = indexPath.row;
            
            [dropdownCell.button addTarget: self action: @selector(dropdownPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            //set initial value, blank dropdown defaulted from conversion
            if (![[regFieldData objectForKey: @"Value"] isEqualToString: BLANK])
                [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            else
                [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Title"]];
            
            //keep a reference of cell to submit json value later
            [self.regSubmissionDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        case kREGISTRATION_TEXTFIELDWITHBUTTON:
        {
            RegistrationTextfieldWithButtonCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationTextfieldWithButtonCell"];
            //see if Enabled key is available, if not see if pending or approved
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            else
                [textfieldCell setUserInteractionEnabled: !self.isApproved];
            
            if([[regFieldData objectForKey: @"Key"] isEqualToString: kWorkshopLocation] && [GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_VIETNAM] && !self.isApproved) {
                [textfieldCell setUserInteractionEnabled: false];
            }
        
            
            if ([regFieldData objectForKey: @"TextfieldEnabled"])
                [textfieldCell setTextfieldEnabled: [[regFieldData objectForKey: @"TextfieldEnabled"] boolValue]];
            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            
            
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [textfieldCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            
            [textfieldCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [textfieldCell hideTopLabel: NO];
            [textfieldCell  setTextfieldDelegate: self];
            
            if ([[regFieldData objectForKey: @"Key"] isEqualToString: kWorkshopQRCode])
            {
                [textfieldCell.button addTarget:self action:@selector(btnScanDSRCode:) forControlEvents:UIControlEventTouchUpInside];
            }
            else if ([[regFieldData objectForKey: @"Key"] isEqualToString: kWorkshopLocation])
            {
                
                [textfieldCell.button addTarget:self action:@selector(locationPressed:) forControlEvents:UIControlEventTouchUpInside];
            }
//            switch (indexPath.row)
//            {
//                case kWorkshopDetails_WORKSHOPQRCODE:
//                    [textfieldCell.button addTarget:self action:@selector(btnScanDSRCode:) forControlEvents:UIControlEventTouchUpInside];
//                    break;
//                case kWorkshopDetails_LOCATION:
//                    [textfieldCell.button addTarget:self action:@selector(locationPressed:) forControlEvents:UIControlEventTouchUpInside];
//                    break;
//            }
            
            if ([regFieldData objectForKey: @"Value"])
                [textfieldCell setTextfieldText: [regFieldData objectForKey: @"Value"]];
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_BUTTON:
        {
            RegistrationButtonCell *buttonCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationButtonCell"];
            //see if Enabled key is available, if not see if pending or approved
            if ([regFieldData objectForKey: @"Enabled"])
                [buttonCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            else
                [buttonCell setUserInteractionEnabled: !self.isApproved];
            
            [buttonCell setTitle: [regFieldData objectForKey: @"Title"]];
            [buttonCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [buttonCell hideTopLabel: NO];
            
            [self.regSubmissionDict setObject: buttonCell forKey: [regFieldData objectForKey: @"Key"]];
            return buttonCell;
        }
            break;
        case kREGISTRATION_MOBNUM:
        {
            RegistrationMobileNumberCell *mobNumCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationMobileNumberCell"];
            //see if Enabled key is available, if not see if pending or approved
            if ([regFieldData objectForKey: @"Enabled"])
                [mobNumCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            else
                [mobNumCell setUserInteractionEnabled: !self.isApproved];
            
            [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [mobNumCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [mobNumCell hideTopLabel: NO];
            [mobNumCell  setTextfieldDelegate: self];
            
            [mobNumCell setCountryCode: [regFieldData objectForKey: @"CountryCode"]];
            [mobNumCell setMobileNumber: [regFieldData objectForKey: @"MobileNumber"]];
            
            if ([regFieldData objectForKey: @"TextfieldEnabled"])
                [mobNumCell setTextfieldEnabled: [[regFieldData objectForKey: @"TextfieldEnabled"] boolValue]];
            [mobNumCell setMobileCodeEnabled: NO];
            
            [self.regSubmissionDict setObject: mobNumCell forKey: [regFieldData objectForKey: @"Key"]];
            return mobNumCell;
        }
            break;
        case kREGISTRATION_LABELWITHTEXTFIELD:
        {
            RegistrationLabelTextfieldTableViewCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationLabelTextfieldTableViewCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            //see if Enabled key is available, if not see if pending or approved
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            else
                [textfieldCell setUserInteractionEnabled: !self.isApproved];
            
            [textfieldCell setLeftTitle: [regFieldData objectForKey: @"Title"]];
            [textfieldCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
//            [textfieldCell hideTopLabel: YES];
            [textfieldCell  setTextfieldDelegate: self];
            
            if ([regFieldData objectForKey: @"Value"])
                [textfieldCell setTextfieldText: [regFieldData objectForKey: @"Value"]];
            
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_BIRTHDATE:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            //see if Enabled key is available, if not see if pending or approved
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            else
                [dropdownCell setUserInteractionEnabled: !self.isApproved];
            
            //Salutation
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            [dropdownCell hideTopLabel: NO];
            dropdownCell.button.tag = indexPath.row;
            
            [dropdownCell.button addTarget: self action: @selector(birthdayPressed:) forControlEvents:UIControlEventTouchUpInside];
            [self.regSubmissionDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        case kREGISTRATION_TEXTVIEW:
        {
            RegistrationTextViewTableViewCell *textviewCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationTextViewTableViewCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textviewCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textviewCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            
            //see if Enabled key is available, if not see if pending or approved
            if ([regFieldData objectForKey: @"Enabled"])
                [textviewCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            else
                [textviewCell setUserInteractionEnabled: !self.isApproved];
            
            [textviewCell setTitle: [regFieldData objectForKey: @"Title"]];
            [textviewCell hideTopLabel: NO];
            [textviewCell setTextviewDelegate: self];
            
            if ([regFieldData objectForKey: @"Value"])
                [textviewCell setTextviewText: [regFieldData objectForKey: @"Value"]];
            
            if ([regFieldData objectForKey: @"Placeholder"])
                [textviewCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
            
            [self.regSubmissionDict setObject: textviewCell forKey: [regFieldData objectForKey: @"Key"]];
            return textviewCell;
        }
            break;
        case kREGISTRATION_HEADER:
        {
            RegistrationHeaderTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationHeaderTableViewCell"];
            [headerCell setTitle: [regFieldData objectForKey: @"Title"]];
            return headerCell;
        }
            break;
        case kREGISTRATION_WORKINGHOURS:
        {
            RegistrationWorkingHoursTableViewCell *workingHoursCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationWorkingHoursTableViewCell"];
            
            if (!self.isApproved) //allow working hours to be modified if pending
                [workingHoursCell.button addTarget: self action: @selector(workingHoursPressed:) forControlEvents:UIControlEventTouchUpInside];
            workingHoursCell.workingHoursDict = self.workingHoursDict;
            
            [workingHoursCell reloadCellData];
            return workingHoursCell;
        }
            break;
            
        default:
            NSLog(@"UNKNOWN CELL TYPE");
            return [UITableViewCell new];
            break;
    }
    return [UITableViewCell new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"DID SELECT TABLEVIEW, %i", indexPath.row);
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    self.scrollViewToBottom.constant = self.keyboardFrame.size.height;
    RegistrationTextViewTableViewCell *textViewCell = [self.regSubmissionDict objectForKey: kRemarks];
    
    [textViewCell didBeginEditing];
    if (self.mainScrollView.contentOffset.y < textViewCell.frame.origin.y)
        [UIView animateWithDuration:0.25f animations:^{
            self.mainScrollView.contentOffset = CGPointMake(0, textViewCell.frame.origin.y);
        }];
    else
        [self.mainScrollView scrollRectToVisible: textViewCell.frame animated:YES];
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    self.scrollViewToBottom.constant = 0;
    RegistrationTextViewTableViewCell *textViewCell = [self.regSubmissionDict objectForKey: kRemarks];
    
    [textViewCell didEndEditing];
}

//-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
//{
//    return [[mSession getContactPreference] count];
//}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ContactPreferencesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"ContactPrefCell" forIndexPath: indexPath];
    
    UIButton *checkboxBtn = [cell viewWithTag: 1]; //tagged in storyboard
    
    UILabel *contactPref = [cell viewWithTag: 2]; //tagged in storyboard
    contactPref.text = [[mSession getContactPreference] objectAtIndex: [indexPath row]];
    contactPref.font = FONT_B1;
    
    cell.bitValue = [[mSession convertToContactPreferenceKeyCode: contactPref.text] intValue];
    
    if ([[self.member.contactPreference componentsSeparatedByString: @","] containsObject: @(cell.bitValue).stringValue])
        checkboxBtn.selected = YES;
    
    if (!self.contactPrefArray)
        self.contactPrefArray = [[NSMutableArray alloc] init];
    [self.contactPrefArray addObject: cell];
    
    return cell;
}

- (IBAction)checkboxPressed:(UIButton *)sender
{
    sender.selected = !sender.selected;
}

-(NSString *) convertDateForDisplay:(NSString *)monthString day:(NSString *) dayString
{
    if ([monthString isEqualToString: BLANK] || [dayString isEqualToString: BLANK])
        return LOCALIZATION(C_PROFILE_DOB_PLACEHOLDER);         //lokalise 28 jan
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    [dateFormat setDateFormat:@"MMdd"];
    NSDate *date = [dateFormat dateFromString: [NSString stringWithFormat: @"%@%@", monthString, dayString]];
    
    // Convert date object to desired output format
    [dateFormat setDateFormat:@"dd MMMM"];
    return [dateFormat stringFromDate:date];
}

-(void) setupData
{
    if([[[mSession lookupTable] objectForKey: @"IsShowCompanyCategory"] boolValue] || [GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_INDIA]) {
        [[WebServiceManager sharedInstance] loadCompanyCategory:self];
    }
}

-(void) dropdownPressed:(UIButton *)sender
{
    [self.view endEditing: YES];
    listView.tag = sender.tag;
    
    switch (listView.tag)
    {
        case kWorkshopDetails_COMPANYSOURCE:
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_COMPANYSOURCE) list: [mSession getCompanySource] hasSearchField: NO];  //lokalise 28 Jan
            break;
        case kWorkshopDetails_WORKSHOPTYPE:
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_WORKSHOPTYPE) list: [mSession getCompanyTypes] hasSearchField: NO];    //lokalised
            break;
        case kWorkshopDetails_WORKSHOPCATEGORY:
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_WORKSHOPCATEGORY) list: [self getCompanyCategoryValues] hasSearchField: NO];  //lokalised
//            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_WORKSHOPCATEGORY) list: [mSession getCompanyTypes] hasSearchField: NO];
            break;
//        case kWorkshopDetails_STATE:
//            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_STATE) list: [mSession getStateNames: self.stateTable] hasSearchField: YES];
//            break;
//        case kWorkshopDetails_SALUTATION:
//            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO];
//            break;
//        case kWorkshopDetails_WORKINGHOURS:
//            [self workingHoursPressed: nil];
//            return;
//            break;
        case kWorkshopDetails_ACTIVATIONSTATUS:
            if (self.isApproved)
                [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_ACTIVATIONSTATUS) list: [mSession getCompanyStatus] hasSearchField: NO];   //lokalised 4 feb
            else
                [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_ACTIVATIONSTATUS) list: [mSession getLeadStatus] hasSearchField: NO];   //lokalised 4 feb
            break;
        default:
        {
            RegistrationFormDropdownCell *cell = sender.superview.superview;
            
            NSArray *keyArray = [self.regSubmissionDict allKeysForObject: cell];
            
            if ( [keyArray.firstObject isEqualToString: kSalutation])
            {
                [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO];   //lokalised
            }
            else if ( [keyArray.firstObject isEqualToString: kCompanyState])
            {
                [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_STATE) list: [mSession getStateNames: self.stateTable] hasSearchField: YES];  //lokalised
            }
            
        }
            break;
    }
    
    [self popUpView];
}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

- (IBAction)locationPressed:(id)sender {
    [UpdateHUD addMBProgress: self.view withText: @""];
    
    switch ([CLLocationManager authorizationStatus])
    {
        case kCLAuthorizationStatusAuthorizedAlways:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusNotDetermined:
            [UpdateHUD removeMBProgress: self];
            [self.locationManager requestWhenInUseAuthorization];
            break;
        case kCLAuthorizationStatusDenied:
        {
            [UpdateHUD removeMBProgress: self];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_ALERTVIEW_ERROR) message:LOCALIZATION(C_ALERTVIEW_PERMISSIONERRORMSG) preferredStyle:UIAlertControllerStyleAlert];   //lokalised
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_CANCEL) style:UIAlertActionStyleCancel handler:nil];
            UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_SETTING) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                            UIApplicationOpenSettingsURLString]];
            }];   //lokalised
            
            [alertController addAction:cancelAction];
            [alertController addAction:settingsAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
            break;
        case kCLAuthorizationStatusRestricted:
            [UpdateHUD removeMBProgress: self];
            break;
    }
    
}


- (IBAction)updateWorkshopPressed:(id)sender {
    
    NSString *contactPrefString = @"";
    for (ContactPreferencesCollectionViewCell* cell in self.contactPrefArray)
    {
        if ([cell.checkboxBtn isSelected])
        {
            if (contactPrefString.length == 0)
                contactPrefString = @(cell.bitValue).stringValue;
            else
                contactPrefString = [contactPrefString stringByAppendingString: [NSString stringWithFormat: @",%@", @(cell.bitValue).stringValue]];
        }
    }
    self.member.contactPreference    = contactPrefString;
    
    self.member.salutation = [[mSession convertToSalutationKeyCode: [[self.regSubmissionDict objectForKey: kSalutation] getJsonValue]] intValue];
    self.member.firstName =  [[self.regSubmissionDict objectForKey:kFirstName] getJsonValue];
    self.member.lastName =  [[self.regSubmissionDict objectForKey:kLastName] getJsonValue];
    //    self.member.mobileNumber =  [[self.regSubmissionDict objectForKey:kMobileNumber] getMobileNumberJsonValue];
    self.member.mobileNumber =  [[self.regSubmissionDict objectForKey:kMobileNumber] getMobileNumberJsonValue];
    self.member.emailAddress =  [[self.regSubmissionDict objectForKey:kEmailAddress] getJsonValue];
    
    
    self.companyInfo.workshopQRCode         = [[self.regSubmissionDict objectForKey: kWorkshopQRCode] getJsonValue];
    self.companyInfo.referenceCode         = [[self.regSubmissionDict objectForKey: kReferenceCode] getJsonValue];

    self.companyInfo.companySource         = [mSession convertToCompanySourceKeyCode: [[self.regSubmissionDict objectForKey: kCompanySource] getJsonValue]];
    self.companyInfo.companyType            = [[mSession convertToCompanyTypesKeyCode: [[self.regSubmissionDict objectForKey: kCompanyType] getJsonValue]] intValue];
    if (!self.companyInfo.companyType)
        self.companyInfo.companyType = 0;
    
    if ([[[mSession lookupTable] objectForKey: @"IsShowCompanyCategory"] boolValue])
    {
        self.companyInfo.companyCategory            = [[self convertToCompanyCategoryCode: [[self.regSubmissionDict objectForKey: kCompanyCategory] getJsonValue]] intValue];
        if (!self.companyInfo.companyCategory)
            self.companyInfo.companyCategory = 0;
    }
    
    self.companyInfo.companyName            = [[self.regSubmissionDict objectForKey: kCompanyName] getJsonValue];
    self.companyInfo.companyEmailAddress    = [[self.regSubmissionDict objectForKey: kCompanyEmailAddress] getJsonValue];
    self.companyInfo.contactNumber          = [[self.regSubmissionDict objectForKey: kContactNumber] getJsonValue];
    
    self.companyInfo.oilChangePerMonth            = [[self.regSubmissionDict objectForKey: kOilChangePerMonth] getJsonValue];
    self.companyInfo.oilChangePercentage            = [[self.regSubmissionDict objectForKey: kOilChangePercentage] getJsonValue];
    self.companyInfo.halfSyntheticOilChanges            = [[self.regSubmissionDict objectForKey: kHalfSyntheticOilChanges] getJsonValue];
    self.companyInfo.shellHelixQty            = [[self.regSubmissionDict objectForKey: kShellHelixQty] getJsonValue];
    
    
    
//    (self.isApproved) ? [mSession convertToCompanyStatus: self.companyInfo.status] : [mSession convertToLeadStatus: self.companyInfo.status],
    if (self.isApproved)
        self.companyInfo.status                 = [[mSession convertToCompanyStatusKeyCode: [[self.regSubmissionDict objectForKey: kActivationStatus] getJsonValue]] intValue];
    else
        self.companyInfo.status                 = [[mSession convertToLeadStatusKeyCode: [[self.regSubmissionDict objectForKey: kActivationStatus] getJsonValue]] intValue];
        
    self.companyInfo.remarks                = [[self.regSubmissionDict objectForKey: kRemarks] getJsonValue];
    
    
    self.companyAddress.address1        = [[self.regSubmissionDict objectForKey: kAddress1] getJsonValue];
    self.companyAddress.address2        = [[self.regSubmissionDict objectForKey: kAddress2] getJsonValue];
    
    self.companyAddress.companyCity = [[self.regSubmissionDict objectForKey: kCompanyCity] getJsonValue]; //town
    if (!self.companyAddress.companyCity)
        self.companyAddress.companyCity = @"";
    
    NSString *stateString = [[self.regSubmissionDict objectForKey: kCompanyState] getJsonValue];
    self.companyAddress.companyState    = [mSession convertToStateKeyCode: stateString stateDetailedListArray:self.stateTable];
    self.companyAddress.postalCode      = [[self.regSubmissionDict objectForKey: kPostalCode] getJsonValue];
    
    NSArray *companyService = @[
//                                @{@"ServiceID": @"1"},
//                                @{@"ServiceID": @"2"},
                                ];

//    NSDictionary *updateParams = @{@"CountryCode":            self.member.countryCode,
//                                     @"REG_Company":            [self.companyInfo getJSON],
//                                     @"REG_CompanyAddress":     [self.companyAddress getJSON],
//                                     @"REG_CompanyService":     companyService,
//                                     @"REG_CompanyWorkHours":   self.workingHoursDict,
//                                     @"REG_Member":             [self.member getJSON],
//                                     };
  
    NSMutableDictionary *updateParams = [[NSMutableDictionary alloc] init];
    
    [updateParams setObject:self.member.countryCode forKey: @"CountryCode"];
    if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
    {
        if (self.isApproved)
            [updateParams setObject:@(YES) forKey: @"TermCondition"];
        else
            [updateParams setObject:@(self.btnTncCheckbox.selected) forKey: @"TermCondition"];
    }
    else
        [updateParams setObject:@(YES) forKey: @"TermCondition"];
    [updateParams setObject:[self.companyInfo getJSON] forKey: @"REG_Company"];
    [updateParams setObject:[self.companyAddress getJSON] forKey: @"REG_CompanyAddress"];
    [updateParams setObject:companyService forKey: @"REG_CompanyService"];
    [updateParams setObject:self.workingHoursDict forKey: @"REG_CompanyWorkHours"];
    [updateParams setObject:[self.member getJSON] forKey: @"REG_Member"];
    
    
    if (self.imgSignature.image)
    {
        //set image
        [updateParams setObject: [UIImageJPEGRepresentation(self.imgSignature.image, 0.5f) base64EncodedStringWithOptions:0]  forKey: @"DigitalSignature"];
    }
    
    
    if (self.isApproved)
        [[WebServiceManager sharedInstance] updateApprovedWorkshop: updateParams
                                                                vc:self];
    else
        [[WebServiceManager sharedInstance] updateWorkshop: updateParams  vc:self];
}

- (IBAction)signaturePressed:(id)sender
{
    if (!self.isApproved)
        [mSession pushRUCustomerSignatureWithDelegate: self.parentViewController.parentViewController delegate: self];
}

-(void)didFinishSignature:(UIImage *)image
{
    self.imgSignature.image = image;
}



-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        RegistrationFormDropdownCell *cell;
        switch (listView.tag)
        {
            case kWorkshopDetails_COMPANYSOURCE:
                cell = [self.regSubmissionDict objectForKey: kCompanySource];
                break;
            case kWorkshopDetails_WORKSHOPTYPE:
                cell = [self.regSubmissionDict objectForKey: kCompanyType];
                break;
            case kWorkshopDetails_WORKSHOPCATEGORY:
                cell = [self.regSubmissionDict objectForKey: kCompanyCategory];
                break;
            case kWorkshopDetails_STATE:
                cell = [self.regSubmissionDict objectForKey: kCompanyState];
                break;
            case kWorkshopDetails_SALUTATION:
                cell = [self.regSubmissionDict objectForKey: kSalutation];
                break;
            case kWorkshopDetails_ACTIVATIONSTATUS:
                cell = [self.regSubmissionDict objectForKey: kActivationStatus];
                break;
            default:
                return;
                break;
        }
        [cell.button setTitle:selection.firstObject forState:UIControlStateNormal];
    }
}


- (IBAction)workingHoursPressed:(id)sender {
    
    if (!self.workingHoursDict ||
        [self.workingHoursDict isKindOfClass: [NSNull class]])
        self.workingHoursDict = @{@"WorkHourStart1":   @"0:00",
                                  @"WorkHourEnd1":     @"0:00",
                                  @"WeekDays1":        @"",
                                  @"PublicHoliday":    @"0",
                                  @"WorkHourStart2":   @"0:00",
                                  @"WorkHourEnd2":     @"0:00",
                                  @"WeekDays2":        @"",
                                  };
    
    //let tab bar push instead
    [mSession pushWorkingHoursView: self.navigationController.parentViewController workingHoursData: self.workingHoursDict];
}

- (IBAction)manageMechanics:(id)sender {
    //push mechanics
    [mSession pushManageMechanic: self.member.tradeID  vc:self.navigationController.parentViewController];
}

- (IBAction)uploadImagePressed:(id)sender {
    if (self.isApproved)
        [mSession pushGalleryView: self.member.tradeID isApproved:self.isApproved vc: self.navigationController.parentViewController delegate: self];
    else
        [mSession pushGalleryView: self.member.leadID isApproved:self.isApproved vc: self.navigationController.parentViewController delegate: self];
}

//uploading done in gallery vc if lead/tradeid available
-(void)didFinishUpload:(NSArray *)imageDataArray
{
    self.workshopImagesData = imageDataArray;
}

-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict
{
    //assign working hours
    self.workingHoursDict = workingHoursDict;
    
    [self.regFormTableView reloadRowsAtIndexPaths: @[[NSIndexPath indexPathForRow: kWorkshopDetails_WORKINGHOURS inSection:0]] withRowAnimation: UITableViewRowAnimationNone];
    
    [self.regFormTableView reloadRowsAtIndexPaths: @[[NSIndexPath indexPathForRow: kWorkshopDetails_OILCHANGEPERDAY inSection:0]] withRowAnimation: UITableViewRowAnimationNone];
    [self.regFormTableView reloadRowsAtIndexPaths: @[[NSIndexPath indexPathForRow: kWorkshopDetails_SYNTHETICPERDAY inSection:0]] withRowAnimation: UITableViewRowAnimationNone];
    [self.regFormTableView reloadRowsAtIndexPaths: @[[NSIndexPath indexPathForRow: kWorkshopDetails_HALFSYNTHETICPERDAY inSection:0]] withRowAnimation: UITableViewRowAnimationNone];
    [self.regFormTableView reloadRowsAtIndexPaths: @[[NSIndexPath indexPathForRow: kWorkshopDetails_SHELLHELIXPERDAY inSection:0]] withRowAnimation: UITableViewRowAnimationNone];
    
    NSArray *fullWorkWeekDays;
    NSArray *halfWorkWeekDays;
    
    if (![[workingHoursDict objectForKey: @"WeekDays1"] isEqualToString: @""])
        fullWorkWeekDays = [[workingHoursDict objectForKey: @"WeekDays1"] componentsSeparatedByString: @","];
    
    if (![[workingHoursDict objectForKey: @"WeekDays2"] isEqualToString: @""])
        halfWorkWeekDays = [[workingHoursDict objectForKey: @"WeekDays2"] componentsSeparatedByString: @","];
    
    CGFloat workingHoursOffset = (44 * ([fullWorkWeekDays count] + [halfWorkWeekDays count]));
    
    if (self.isApproved)
    {
        //set tableview height. 1 shorter cells. 5 cell less (dsm approval + 4 questions). no textview
        self.tableViewHeight.constant = CELL_HEIGHT * ([self.registrationFormArray count] - 5) - (CELL_HEIGHT - 50) + workingHoursOffset;
    }
    else
        //set pending details tableview height. 4 shorter cells + 73 from textview
        self.tableViewHeight.constant = CELL_HEIGHT * [self.registrationFormArray count] - ((CELL_HEIGHT - 50) * 5) + (73) + workingHoursOffset;
    
//    [self.workingHoursTableView reloadData];
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.scrollViewToBottom.constant = 0;
}

-(void)keyboardOnScreen:(NSNotification *)notification
{
    if (!listView.superview)
        [super keyboardOnScreen: notification];
//    self.scrollViewToBottom.constant = self.keyboardFrame.size.height;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)selectedDate:(NSDate *)selectedDate
{
    //do smething
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //   for API
    [formatter setDateFormat:@"dd"];
    self.member.dobDay = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"MM"];
    self.member.dobMonth = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"dd MMMM"];
    
    [formatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    NSString *selectedDateString = [formatter stringFromDate:selectedDate];
    
    
    RegistrationFormDropdownCell *cell = [self.regSubmissionDict objectForKey: kDOB];
    [cell setSelectionTitle: selectedDateString];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
        {

            SET_LOADSTATE([[response getGenericResponse] objectForKey: @"StateDetailsList"]);
            self.stateTable = [[response getGenericResponse] objectForKey: @"StateDetailsList"];
            if (![[[mSession lookupTable] objectForKey: @"IsShowCompanyCategory"] boolValue]) {
                if(![GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_INDIA]){
                    [self setupRegistrationForm];
                }
            }
        }
            break;
        case kWEBSERVICE_UPDATEWORKSHOP:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                //non-assisted pop back to root screen
                [self.detailsDelegate didUpdateWorkshop]; //details is in tabcontroller
            }];
        }
            break;
        case kWEBSERVICE_TRADE_LOADCOMPANYCATEGORY:
        {
            self.companyCategoryList = [[response getGenericResponse] objectForKey:@"CompanyCategory"];
            if([self.companyCategoryList isKindOfClass:[NSNull class]] || self.companyCategoryList == nil) {
                self.companyCategoryList = [[mSession lookupTable] objectForKey: @"CompanyCategory"];
            }
            [self setupRegistrationForm];
        }
            break;
        default:
            break;
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress: KEY_WINDOW];
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
            self.stateTable = GET_LOADSTATE;
            
            [self setupRegistrationForm];
            break;
        default:
            break;
    }
}

#pragma mark - Company Category
-(NSArray *) getCompanyCategoryValues {
    NSMutableArray *keyValueArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in self.companyCategoryList)
    {
        [keyValueArray addObject: [dict objectForKey: @"KeyValue"]];
    }
    return keyValueArray;
}

- (NSString *) convertToCompanyCategoryCode: (NSString *)keyValue
{
    for (NSDictionary *dict in self.companyCategoryList)
    {
        if ([keyValue isEqualToString: [dict objectForKey: @"KeyValue"]])
        {
            return [dict objectForKey: @"KeyCode"];
        }
    }
    return @"";
}

- (NSString *) convertCodeToValue:(NSInteger) keyCode
{
    for (NSDictionary *dict in self.companyCategoryList)
    {
        if (keyCode == [[dict objectForKey: @"KeyCode"] intValue])
        {
            return [dict objectForKey: @"KeyValue"];
        }
    }
    return @"";
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
