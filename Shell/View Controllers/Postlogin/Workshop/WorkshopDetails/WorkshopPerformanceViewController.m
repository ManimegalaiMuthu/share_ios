//
//  WorkshopPerformanceViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 16/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "WorkshopPerformanceViewController.h"


#define CELL_HEIGHT 60

@interface WorkshopPerformanceViewController () <WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *performanceHeader;
@property (weak, nonatomic) IBOutlet UITableView *performanceTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *performanceTableViewHeight;

@property (weak, nonatomic) IBOutlet UILabel *workshopHeader;
@property (weak, nonatomic) IBOutlet UITableView *workshopTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *workshopTableViewHeight;

@property NSMutableArray *performanceListArray;
@property NSMutableArray *workshopPerformanceListArray;

@end

@implementation WorkshopPerformanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //empty view to eliminate extra separators
    self.performanceTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.workshopTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.performanceHeader setFont: FONT_H1];
    self.performanceHeader.text = LOCALIZATION(C_PERFORMANCE_PROGRAMPERFORMANCE);       //lokalised
    self.workshopHeader.text = LOCALIZATION(C_PERFORMANCE_WORKSHOP);                    //lokalised
    [self.workshopHeader setFont: self.performanceHeader.font];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
//    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//    [tracker set:kGAIScreenName value:@"Workshop performance"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[WebServiceManager sharedInstance] fetchWorkshopPerformanceByDSR: self.tradeID view:self];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_performanceTableView == tableView)
        return [self.performanceListArray count];
    else if (_workshopTableView == tableView)
        return [self.workshopPerformanceListArray count];
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *dict;
    if (tableView == self.workshopTableView)
        dict = [self.workshopPerformanceListArray objectAtIndex: indexPath.row];
    else
        dict = [self.performanceListArray objectAtIndex: indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"WorkshopPerformanceCell"];
    
    UILabel *lblKey = [cell viewWithTag: 1];
    [lblKey setFont: FONT_B1];
    
    UILabel *lblValue = [cell viewWithTag: 2];
    [lblValue setFont: FONT_H(30)];
    [lblValue setTextColor: COLOUR_RED];
    
    
    lblKey.text = [dict objectForKey: @"PerformanceDesc"];
    lblValue.text = [dict objectForKey: @"PerformanceValue"];
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)processCompleted:(WebServiceResponse *)response
{
    NSDictionary *profileData = [response getGenericResponse];
    
    self.performanceListArray = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in [profileData objectForKey: @"WorkshopPerformance"])
    {
        [self.performanceListArray addObject: dict];
    }
    
    self.workshopPerformanceListArray = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in [profileData objectForKey: @"Performance"])
    {
        [self.workshopPerformanceListArray addObject: dict];
    }
    
    if(![[profileData objectForKey:@"ProgPerfText"] isKindOfClass:[NSNull class]]) {
        self.workshopHeader.text = [profileData objectForKey:@"ProgPerfText"];
    }
    
    if(![[profileData objectForKey:@"WSPerfText"] isKindOfClass:[NSNull class]]) {
        self.performanceHeader.text = [profileData objectForKey:@"WSPerfText"];
    }
    
    self.performanceTableViewHeight.constant = CELL_HEIGHT * [self.performanceListArray count];
    
    self.workshopTableViewHeight.constant = CELL_HEIGHT * [self.workshopPerformanceListArray count];
    
    [self.performanceTableView reloadData];
    [self.workshopTableView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
