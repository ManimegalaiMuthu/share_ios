//
//  WorkshopDetailsTabViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 16/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "RDVTabBarController.h"
#import <UIKit/UIKit.h>
#import "Helper.h"

@interface WorkshopDetailsTabViewController : RDVTabBarController
@property NSDictionary *workshopDetailsData;
@end
