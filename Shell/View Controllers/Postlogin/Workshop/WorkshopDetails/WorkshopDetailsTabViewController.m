//
//  WorkshopDetailsTabViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 16/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "RDVTabBarItem.h"
#import "WorkshopDetailsTabViewController.h"

#import "WorkshopDetailsViewController.h"
#import "WorkshopPerformanceViewController.h"
#import "WorkshopDetailProfilingViewController.h"
#import "WorkingHoursViewController.h"

#import "PendingWorkshopDetails_NewViewController.h"
#import "ApprovedWorkshopDetails_NewViewController.h"

#import "PerformanceDashboard_NewViewController.h"

enum kWorkshopDetails_Tab
{
    kWorkshopDetails_Details,
    kWorkshopDetails_Performance,
    kWorkshopDetails_Profile,
};

@interface WorkshopDetailsTabViewController () <RDVTabBarDelegate, WorkingHoursViewDelegate, WebServiceManagerDelegate, WorkshopDetailsDelegate, PendingWorkshopDetailsDelegate, ApprovedWorkshopDetailsDelegate>
{
    WorkshopDetailsViewController           *workshopDetailsViewController;
    PendingWorkshopDetails_NewViewController *pendingWorkshopDetailsViewController;
    ApprovedWorkshopDetails_NewViewController *approvedWorkshopDetailsViewController;
    
    WorkshopPerformanceViewController       *workshopPerformanceViewController;
    PerformanceDashboard_NewViewController *dashboardViewController;
    WorkshopDetailProfilingViewController   *workshopProfileViewController;
}
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@end

@implementation WorkshopDetailsTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (self.workshopDetailsData)
    {
        [self setupInterface];

        //setup back button
        if (kIsRightToLeft) {
            [self.backBtn setImage: [UIImage imageNamed:@"icon-arrsingleR.png"] forState: UIControlStateNormal];
        } else {
            [self.backBtn setImage: [UIImage imageNamed: @"icon-arrsingleL.png"] forState: UIControlStateNormal];
        }
        if(@available(iOS 11, *)){
            [self.backBtn.widthAnchor constraintEqualToConstant: self.backBtn.frame.size.width].active = YES;
            [self.backBtn.heightAnchor constraintEqualToConstant: self.backBtn.frame.size.height].active = YES;
        }
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
        
    }
    else
    {
        LoginInfo *loginInfo = [mSession loadUserProfile];
        [[WebServiceManager sharedInstance] fetchTrade: loginInfo.tradeID
                                                    vc:self];
    }
}

- (void) setupInterface
{
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        if(GET_PROFILETYPE == kPROFILETYPE_DSR)
        {
            [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_MYNETWORK) subtitle:@"" size:15 subtitleSize:0];   //lokalised
        }
        else
        {
            [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_IN_MANAGEMYMECHANICS) subtitle: @"" size: 15 subtitleSize: 0];  //lokalised
        }
    }
    else
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_MANAGEWORKSHOP) subtitle: @"" size: 15 subtitleSize: 0];        //lokalised
    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_TITLE_MANAGEWORKSHOP),  LOCALIZATION(C_REWARDS_BACK)];

    
    workshopProfileViewController = [self.storyboard instantiateViewControllerWithIdentifier: VIEW_WORKSHOPPROFILING];
    UINavigationController *workshopProfileNav = [[UINavigationController alloc] initWithRootViewController: workshopProfileViewController];
    [workshopProfileNav setNavigationBarHidden: YES];
    
    
    //same VC but different data.
    //working hours vc will check for nil VC
    //workshopdetails, pendingworkshop_new, approvedworkshop_new to be unique (i.e only one will be initialised)
    
    Member *memberInfo = [[Member alloc] initWithData: [self.workshopDetailsData objectForKey: @"REG_Member"]];
    NSArray *title;
    
    //PENDING workshop Settings (Pending contains only lead ID)
    if (ISNULL(memberInfo.tradeID) || [memberInfo.tradeID isEqualToString: @""])
    {
//        workshopDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier: VIEW_WORKSHOPDETAILS];
//        UINavigationController *workshopDetailsNav = [[UINavigationController alloc] initWithRootViewController: workshopDetailsViewController];
//        workshopDetailsViewController.workshopData = self.workshopDetailsData;
//        [workshopDetailsNav setNavigationBarHidden: YES];
//        workshopDetailsViewController.detailsDelegate = self;
        
        pendingWorkshopDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier: VIEW_PENDINGWORKSHOPDETAILS_NEW];
        UINavigationController *workshopDetailsNav = [[UINavigationController alloc] initWithRootViewController: pendingWorkshopDetailsViewController];
        pendingWorkshopDetailsViewController.workshopData = self.workshopDetailsData;
        [workshopDetailsNav setNavigationBarHidden: YES];
        pendingWorkshopDetailsViewController.detailsDelegate = self;
        pendingWorkshopDetailsViewController.workshopData = self.workshopDetailsData;
        
        //selecttab function have been tweaked locally for this case
        [self setViewControllers: @[workshopDetailsNav,
                                    workshopProfileNav
                                    ]];
        
        title = @[LOCALIZATION(C_TITLE_WORKSHOPDETAIL),     //lokalised
                  LOCALIZATION(C_TITLE_WORKSHOPPROFILING),  //lokalised
                  ];
        workshopProfileViewController.leadID = [self.workshopDetailsData objectForKey:@"LeadID"];
//        workshopDetailsViewController.isApproved = NO;
    }
    //APPROVED workshop Settings
    else
    {
        workshopProfileViewController = [self.storyboard instantiateViewControllerWithIdentifier: VIEW_WORKSHOPPROFILING];
        UINavigationController *workshopProfileNav = [[UINavigationController alloc] initWithRootViewController: workshopProfileViewController];
        [workshopProfileNav setNavigationBarHidden: YES];
        
        
        UINavigationController *perfNav;
        if ([[mSession profileInfo] IsPerfOpt1])
        {
            workshopPerformanceViewController = [self.storyboard instantiateViewControllerWithIdentifier: VIEW_WORKSHOPPERFORMANCE];
            perfNav = [[UINavigationController alloc] initWithRootViewController: workshopPerformanceViewController];
            workshopPerformanceViewController.tradeID = [self.workshopDetailsData objectForKey: @"TradeID"];
            [perfNav setNavigationBarHidden: YES];
        }
        
        else if ([[mSession profileInfo] IsPerfOpt2])
        {
            dashboardViewController = [STORYBOARD_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_PERFORMANCE_DASHBOARD_NEW];
            perfNav = [[UINavigationController alloc]initWithRootViewController: dashboardViewController];
            [perfNav setNavigationBarHidden: YES];
            dashboardViewController.tradeID = [self.workshopDetailsData objectForKey: @"TradeID"];
            dashboardViewController.isTab = YES;
        }
        else
        {
            workshopPerformanceViewController = [self.storyboard instantiateViewControllerWithIdentifier: VIEW_WORKSHOPPERFORMANCE];
            perfNav = [[UINavigationController alloc] initWithRootViewController: workshopPerformanceViewController];
            workshopPerformanceViewController.tradeID = [self.workshopDetailsData objectForKey: @"TradeID"];
            [perfNav setNavigationBarHidden: YES];
        }
        
//        workshopDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier: VIEW_WORKSHOPDETAILS];
//        workshopDetailsNav = [[UINavigationController alloc] initWithRootViewController: workshopDetailsViewController];
//        workshopDetailsViewController.workshopData = self.workshopDetailsData;
//        [workshopDetailsNav setNavigationBarHidden: YES];
//        workshopDetailsViewController.detailsDelegate = self;
        
        approvedWorkshopDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier: VIEW_APPROVEDWORKSHOPDETAILS_NEW];
        UINavigationController *approvedWorkshopDetailsNav = [[UINavigationController alloc] initWithRootViewController: approvedWorkshopDetailsViewController];
        approvedWorkshopDetailsViewController.workshopData = self.workshopDetailsData;
        [approvedWorkshopDetailsNav setNavigationBarHidden: YES];
        approvedWorkshopDetailsViewController.detailsDelegate = self;
        
        if (IS_COUNTRY(COUNTRYCODE_INDIA))
        {
            //selecttab function have been tweaked locally for this case
            [self setViewControllers: @[approvedWorkshopDetailsNav,
//                                        workshopPerformanceNav
                                        perfNav,
                                        ]];
            title = @[LOCALIZATION(C_MANAGEWORKSHOP_IN_DETAIL_TITLE),           //lokalised
                      LOCALIZATION(C_TITLE_WORKSHOPDETAILSPERFORMANCE),         //lokalised
                      ];
        }
        else
        {
            //selecttab function have been tweaked locally for this case
            if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
            {
                [self setViewControllers: @[approvedWorkshopDetailsNav, //workshopDetailsNav,
                                            workshopProfileNav
                                            ]];
                
                title = @[LOCALIZATION(C_TITLE_WORKSHOPDETAIL),             //lokalised
                          LOCALIZATION(C_TITLE_APPROVEDWORKSHOPPROFILING),  //lokalised
                          ];
            }
            else
            {
                [self setViewControllers: @[approvedWorkshopDetailsNav, //workshopDetailsNav,
//                                            workshopPerformanceNav,
                                            perfNav,
                                            workshopProfileNav
                                            ]];
                
                title = @[LOCALIZATION(C_TITLE_WORKSHOPDETAIL),                 //lokalised
                          LOCALIZATION(C_TITLE_WORKSHOPDETAILSPERFORMANCE),     //lokalised
                          LOCALIZATION(C_TITLE_APPROVEDWORKSHOPPROFILING),      //lokalised
                          ];
            }
        }
        
        workshopProfileViewController.tradeID = [self.workshopDetailsData objectForKey: @"TradeID"];
//        workshopDetailsViewController.isApproved = YES;
    }
    
    UIImage *selected = GET_ISADVANCE ? [UIImage imageNamed:@"tab-bg_blue"] : [UIImage imageNamed:@"tab-bg"];
    UIImage *unselected = [UIImage imageNamed:@"tab-bg_grey.png"];
    if (kIsRightToLeft) {
        title = title.reverseObjectEnumerator.allObjects.mutableCopy;
    }
    for (int i = 0; i < [self.tabBar.items count]; i++)
    {
        RDVTabBarItem *item = [self.tabBar.items objectAtIndex: i];
        [item setTitle: [title objectAtIndex: i]];
        
        [item setBackgroundColor: COLOUR_WHITE];
        [item setTitleUnselectedAttributes: FONT_H2 colour:COLOUR_VERYDARKGREY];
        [item setTitleSelectedAttributes: FONT_H2 colour:COLOUR_RED];
        
        [item setBackgroundSelectedImage: selected withUnselectedImage: unselected];
        
        //change selected tab colour
        if (i == 0)
        {
            [self setSelectedIndex: 0];
        }
    }
    if (kIsRightToLeft) {
        [self tabBar:self.tabBar didSelectItemAtIndex:self.viewControllers.count-1];
    }
}

-(void)didUpdateWorkshop
{
    [self backPressed: nil];
}

#pragma mark RDVTabBarDelegate
- (BOOL)tabBar:(RDVTabBar *)tabBar shouldSelectItemAtIndex:(NSInteger)index
{
    for (RDVTabBarItem *item in [tabBar items])
    {
        
    }
    return ([tabBar selectedItem] != [[tabBar items] objectAtIndex: index]);
}

- (void)tabBar:(RDVTabBar *)tabBar didSelectItemAtIndex:(NSInteger)index
{
    //super method MUST BE PRESENT
    [super tabBar:tabBar didSelectItemAtIndex: index];
    
    switch (index)
    {
        case kWorkshopDetails_Details:
            [[IQKeyboardManager sharedManager] setEnable: NO];
            break;
        case kWorkshopDetails_Performance:
            [[IQKeyboardManager sharedManager] setEnable: YES];
            break;
        case kWorkshopDetails_Profile:
            [[IQKeyboardManager sharedManager] setEnable: YES];
            break;
    }
//    if (index == [[tabBar items] count] - 1) //add new to be last object of tab
//    {
//        //        tradeRegViewController
//    }
//    else
//    {
//    }
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[IQKeyboardManager sharedManager] setEnable: YES];
}

-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict
{
    //only one view controller will be initialised at any one time.
    if (workshopDetailsViewController)
        [workshopDetailsViewController didFinishWorkingHoursSelection: workingHoursDict];
    else if (pendingWorkshopDetailsViewController)
        [pendingWorkshopDetailsViewController didFinishWorkingHoursSelection: workingHoursDict];
    else if (approvedWorkshopDetailsViewController)
        [approvedWorkshopDetailsViewController didFinishWorkingHoursSelection: workingHoursDict];
}

- (IBAction)backPressed:(id)sender {
    [UpdateHUD removeSpotifyHUD:CURRENT_VIEW];
    
    CATransition *animation = [CATransition animation];
    animation.type = kCATransitionFade;
    animation.duration = 0.4;
    [self.navigationController.view.layer addAnimation:animation forKey:nil];
    
#warning test for popping the only controller
    //    if ([[vc.navigationController childViewControllers] count] > 1)
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    self.workshopDetailsData = [response getGenericResponse];
    [self setupInterface];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
