//
//  WorkshopDetailProfilingViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 23/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "WorkshopDetailProfilingViewController.h"

@interface WorkshopDetailProfilingViewController () <WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet TableViewWithEmptyView *profileTableView;
@property NSMutableArray *profileListArray;
@end

@implementation WorkshopDetailProfilingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    PlaceholderView *vwPlaceholder = [[PlaceholderView alloc]initWithView:self.profileTableView];
    [vwPlaceholder setBackgroundColor:[UIColor clearColor]];
    self.profileTableView.emptyView = vwPlaceholder;
    
    // Initialize the refresh control.
    if(@available(iOS 10, *))
        self.profileTableView.refreshControl = nil;
    [self.profileTableView.customRefreshControl removeFromSuperview];
    self.profileTableView.customRefreshControl = nil; //profiling dont need refresh
    
    
//    [self.profileTableView.customRefreshControl addTarget:self
//                                                   action:@selector(refreshTable)
//                                         forControlEvents:UIControlEventValueChanged];
    
    //empty view to eliminate extra separators
    self.profileTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)extracted {
    [[WebServiceManager sharedInstance] loadProfiling:self.leadID
                                            isPending:YES
                                                   vc:self];
}

-(void) refreshTable
{
    if (self.leadID)
        [self extracted];
    else
        [[WebServiceManager sharedInstance] loadProfiling:self.tradeID
                                                isPending:NO
                                                       vc:self];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    [self refreshTable];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
//    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//    [tracker set:kGAIScreenName value:@"Workshop detail profiling"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.profileListArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"WorkshopProfileCell"];
    
    UILabel *lblKey = [cell viewWithTag: 1];
    [lblKey setFont: FONT_B1];
    
    UILabel *lblValue = [cell viewWithTag: 2];
    [lblValue setFont: FONT_B1];
    
    NSDictionary *dict = [self.profileListArray objectAtIndex: indexPath.row];
    
    lblKey.text = [dict objectForKey: @"KeyCode"];
    lblValue.text = [dict objectForKey: @"KeyValue"];
    
    return cell;
}
-(void)processCompleted:(WebServiceResponse *)response
{
    [self.profileTableView.customRefreshControl performSelectorOnMainThread: @selector(endRefreshing) withObject: self.profileTableView.customRefreshControl waitUntilDone:NO];
    
    NSDictionary *profileData = [response getGenericResponse];
    
    self.profileListArray = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in [profileData objectForKey: @"ProfilingList"])
    {
        [self.profileListArray addObject: dict];
    }
    
    [self.profileTableView reloadData];
}

//-(void)processFailed:(WebServiceResponse *)response
//{
//    [UpdateHUD removeMBProgress: KEY_WINDOW];
//
//    [self.profileTableView.customRefreshControl performSelectorOnMainThread: @selector(endRefreshing) withObject: self.profileTableView.customRefreshControl waitUntilDone:NO];
//}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
