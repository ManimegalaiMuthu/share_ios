//
//  WorkshopDetailProfilingViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 23/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface WorkshopDetailProfilingViewController : BaseVC

@property NSString *leadID;
@property NSString *tradeID;
@end
