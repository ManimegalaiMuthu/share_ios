//
//  WorkshopDetailsViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 16/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "WorkshopAddNewViewController.h"
@protocol WorkshopDetailsDelegate <NSObject>
-(void) didUpdateWorkshop;
@end

@interface WorkshopDetailsViewController : WorkshopAddNewViewController
@property BOOL isApproved;
@property NSDictionary *workshopData;
@property (weak) id<WorkshopDetailsDelegate> detailsDelegate;
-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict;
@end

