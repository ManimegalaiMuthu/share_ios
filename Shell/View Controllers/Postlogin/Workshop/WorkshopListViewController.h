//
//  WorkshopListViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 15/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface WorkshopListViewController : BaseVC
-(void) selectedTab: (NSInteger) index;
//- (IBAction)hideLegend:(id)sender;
@end
