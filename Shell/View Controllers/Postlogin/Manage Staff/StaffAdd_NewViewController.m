//
//  StaffAdd_NewViewController.m
//  Shell
//
//  Created by Nach on 8/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "StaffAdd_NewViewController.h"
#import "StaffFormViewController.h"

#define kUserID             @"UserID"
#define kDSRCode            @"DSRCode"
#define kUserPassword       @"UserPassword"
#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kEmailAddress       @"EmailAddress"
#define kDOB                @"DOB"
#define kDOBMonth           @"DOBMonth"
#define kDOBDay             @"DOBDay"
#define kRedeem             @"Redeem"
#define kSignature          @"Signature"
#define kTnC                @"TnC"

@interface StaffAdd_NewViewController ()<StaffRegistrationDelegate,WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblRequiredText;
@property (weak, nonatomic) IBOutlet UIView *staffFormView;

@property StaffFormViewController *staffFormTableView;
@property NSMutableArray *registrationFormArray; //ui only

@end

@implementation StaffAdd_NewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view
    NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @"*" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B3, }];
    
    NSMutableAttributedString *requiredFieldsAttrString = [[NSMutableAttributedString alloc] initWithString: LOCALIZATION(C_PROFILE_REQUIREDFIELDS) attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B3, }];   //lokalised 11 Feb
    
    [asteriskAttrString appendAttributedString:  requiredFieldsAttrString];
    
    self.lblRequiredText.attributedText = asteriskAttrString;
    
    _staffFormTableView = [STORYBOARD_FORM_CONTROLLERS instantiateViewControllerWithIdentifier:VIEW_STAFFFORM];
    [self.staffFormView addSubview:[_staffFormTableView view]];
    [self.staffFormTableView.view setFrame: CGRectMake(0, 0, self.staffFormView.frame.size.width, self.staffFormView.frame.size.height)];
    [self.staffFormTableView setDelegate:self];
    
    if (!self.workshopTradeID)    //use own trade ID
    {
        LoginInfo *loginInfo = [mSession loadUserProfile];
        self.workshopTradeID = loginInfo.tradeID;
    }
    
    [self setupInterface];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_MANAGEMECHANIC),LOCALIZATION_EN(C_MANAGEMECHANIC_ADDNEW)] screenClass:nil];
}

-(void) setupInterface {
    if(![[mSession profileInfo]isManageStaffOpt2]) {
        self.registrationFormArray = [[NSMutableArray alloc] initWithArray:
                                      @[@{@"Title": LOCALIZATION(C_HEADER_PROFILE), //lokalised 11 Feb
                                          @"Type": @(kREGISTRATION_HEADER),
                                          },
                                        @{@"Title": LOCALIZATION(C_PROFILE_SALUTATION), //lokalised 11 Feb
                                          @"Type": @(kREGISTRATION_DROPDOWN),
                                          @"Key": kSalutation,
                                          @"Value": LOCALIZATION(C_PROFILE_SALUTATION), //lokalised 11 Feb
                                          },  //button
                                        @{@"Title": LOCALIZATION(C_PROFILE_FIRSTNAME),  //lokalised 11 Feb
                                          @"Type": @(kREGISTRATION_TEXTFIELD),
                                          @"Key": kFirstName,
                                          @"Value": @"",
                                          }, //first name
                                        @{@"Title": LOCALIZATION(C_PROFILE_LASTNAME),   //lokalised 11 Feb
                                          @"Type": @(kREGISTRATION_TEXTFIELD),
                                          @"Key": kLastName,
                                          @"Value": @"",
                                          }, //last name
                                        @{@"Title": LOCALIZATION(C_PROFILE_MOBILENUM),  //lokalised 11 Feb
                                          @"Type": @(kREGISTRATION_MOBNUM),
                                          @"Key": kMobileNumber,
                                          @"CountryCode": [mSession convertCountryCodeToCountryName: GET_COUNTRY_CODE],
                                          @"TextfieldEnabled": @(YES),
                                          @"MobileNumber": @"",
                                          },   //Mobile number
                                        @{@"Title": LOCALIZATION(C_PROFILE_EMAILADDRESS),   //lokalised 11 Feb
                                          @"Placeholder": LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER), //lokalised 11 Feb
                                          @"Type": @(kREGISTRATION_TEXTFIELD),
                                          @"Key": kEmailAddress,
                                          @"Keyboard": @(UIKeyboardTypeEmailAddress),
                                          @"Value": @"",
                                          @"Optional": @(YES),
                                          }, //email
                                        @{@"Title": LOCALIZATION(C_PROFILE_DOB),    //lokalised 11 Feb
                                          @"Type": @(kREGISTRATION_BIRTHDATE),
                                          @"Key": kDOB,
                                          @"Value": LOCALIZATION(C_PROFILE_DOB_PLACEHOLDER),    //lokalised 11 Feb
                                          },   //Date of birth
                                        ]
                                      ];
    }
    
    if([[mSession profileInfo] enableCustomerRedemption]){
        [self.registrationFormArray addObject:@{
                                                @"Type": @(kPROFILING_CHECKBOX),
                                                @"Key": kRedeem,
                                                }];
        
    }
    
    if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_RUSSIA]) {
        [self.registrationFormArray addObject:@{
                                                @"Type": @(kREGISTRATION_SIGNATURE),
                                                @"Key": kSignature,
                                                }];
        [self.registrationFormArray addObject:@{
                                                @"Type": @(kREGISTRATION_TNCCHECKBOX),
                                                @"Key": kTnC,
                                                }];
    }
    
    self.staffFormTableView.registrationFormArray =  self.registrationFormArray;
    [self.staffFormTableView setupInterface];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - StaffRegistrationDelegate
-(void)registerStaff:(NSDictionary *)registrationDict {
    NSMutableDictionary *registerParams = registrationDict.mutableCopy;
    [registerParams setObject:self.workshopTradeID forKey:@"WorkshopTradeID"];
    
    [[WebServiceManager sharedInstance] addMechanic:registerParams vc:self];
}

#pragma mark - WebService Manager
-(void)processCompleted:(WebServiceResponse *)response
{
    [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
        [self setupInterface];
        [self.delegate didRegisterSuccess];
    }];
}

@end
