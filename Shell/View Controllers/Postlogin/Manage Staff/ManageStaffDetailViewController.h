//
//  ManageStaffDetailViewController.h
//  Shell
//
//  Created by Edenred on 28/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ManageStaffDetailViewController : BaseVC

@property NSString *tradeID;

@end

NS_ASSUME_NONNULL_END
