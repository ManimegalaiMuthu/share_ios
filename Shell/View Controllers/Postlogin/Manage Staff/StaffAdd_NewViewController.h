//
//  StaffAdd_NewViewController.h
//  Shell
//
//  Created by Nach on 8/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@protocol StaffAddNewDelegate_New
-(void)didRegisterSuccess;
@end

@interface StaffAdd_NewViewController : BaseVC

@property NSString *workshopTradeID;
@property id<StaffAddNewDelegate_New> delegate;

@end

NS_ASSUME_NONNULL_END
