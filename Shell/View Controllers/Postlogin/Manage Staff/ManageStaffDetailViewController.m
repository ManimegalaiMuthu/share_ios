//
//  ManageStaffDetailViewController.m
//  Shell
//
//  Created by Edenred on 28/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "ManageStaffDetailViewController.h"
#import "TTTAttributedLabel.h"
#import "CASDatePickerView.h"
#import "QRCodeScanner.h"

enum kManageStaff_TYPE
{
    kManageStaff_HEADER = 0,
    kManageStaff_STAFFTYPE,
    kManageStaff_SALUTATION,
    kManageStaff_FIRSTNAME,
    kManageStaff_LASTNAME,
    kManageStaff_MOBNUM,
    kManageStaff_FASHARECODE,
    kManageStaff_EMAIL,
    kManageStaff_DOB,
};

#define CELL_HEIGHT 80

#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kEmailAddress       @"EmailAddress"
#define kDOB                @"DOB"
#define kDOBMonth           @"DOBMonth"
#define kDOBDay             @"DOBDay"
#define kStaffType          @"StaffType"
#define kFAShareBadge       @"FAShareCode"


@interface ManageStaffDetailViewController () <UITableViewDelegate,UITableViewDataSource,WebServiceManagerDelegate,CommonListDelegate,DatePickerDelegate,UITextFieldDelegate,QRCodeScannerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *requiredFieldsLabel;
@property (weak, nonatomic) IBOutlet UITableView *detailsFormTableView;
@property (weak, nonatomic) IBOutlet UIView *redeemOnlyView;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *redeemOnlyLabel;
@property (weak, nonatomic) IBOutlet UIButton *deactivateBtn;
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *checkButton;
@property (weak, nonatomic) IBOutlet UIButton *updateButton;

@property NSMutableArray *registrationFormArray; //ui only
@property NSMutableDictionary *regSubmissionDict;
@property Member *member;
@property CASDatePickerView *dateView;
@property NSDictionary *responseData;

@end

@implementation ManageStaffDetailViewController
@synthesize listView;
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_MECHANICDETAILS) subtitle: @""];  //lokalised 8 Feb
    
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    
    self.detailsFormTableView.delegate = self;
    self.detailsFormTableView.dataSource = self;
    [[WebServiceManager sharedInstance]fetchMechanicDetail:self.tradeID vc:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    mQRCodeScanner.delegate = self;
    [self setupGoogleAnalytics];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

-(void) setupGoogleAnalytics
{
//    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//    [tracker set:kGAIScreenName value:@"Staff Details"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_MANAGEMECHANIC),LOCALIZATION_EN(C_TITLE_MANAGEMECHANIC)] screenClass:nil];
}

-(void) setupInterface {
    
    if (![_responseData objectForKey: @"EnableCustomerRedemption"] || [[_responseData objectForKey: @"EnableCustomerRedemption"] isKindOfClass: [NSNull class]])
        [self.checkButton setSelected: NO];
    else
        [self.checkButton setSelected: [[_responseData objectForKey: @"EnableCustomerRedemption"] boolValue]];
    if([[mSession profileInfo] enableCustomerRedemption])
    {
        [self.redeemOnlyLabel setFont: FONT_B2];
        self.redeemOnlyLabel.text = LOCALIZATION(C_MECH_REDEEMFLAG);    //lokalised 8 Feb
    }
    else
    {
        [self.redeemOnlyView removeFromSuperview];
    }
    NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @"*" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B3, }];
    
    NSMutableAttributedString *requiredFieldsAttrString = [[NSMutableAttributedString alloc] initWithString: LOCALIZATION(C_PROFILE_REQUIREDFIELDS) attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B3, }];   //lokalised 8 Feb
    
    [asteriskAttrString appendAttributedString:  requiredFieldsAttrString];
    
    self.requiredFieldsLabel.attributedText = asteriskAttrString;
    
    listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_PROFILE_STAFFTYPE_PLACEHOLDER)  //lokalised 8 Feb
                                                   list: [mSession getStaffAccountType]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
    
    
    dateView = [[CASDatePickerView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_BIRTHDAY) previouslySelected:nil withYearHidden:YES];   //lokalised 8 Feb
    
    dateView.delegate = self;
    
    [self.deactivateBtn.titleLabel setFont: FONT_BUTTON];
    [self.deactivateBtn setTitle:LOCALIZATION(C_MECHANICDETAILS_DEACTIVATE) forState:UIControlStateNormal]; //lokalised 8 Feb
    [Helper setCustomFontButtonContentModes: self.deactivateBtn];
    
    if([[_responseData objectForKey:@"HasUpdateMechanic"]boolValue]) {
        self.updateButton.hidden = NO;
        [self.updateButton setTitle:LOCALIZATION(C_MECH_UPDATEDETAILS) forState:UIControlStateNormal];  //lokalised 8 Feb
        [self.updateButton.titleLabel setFont: FONT_BUTTON];
        [Helper setCustomFontButtonContentModes: self.updateButton];
    }
//    if(GET_PROFILETYPE == kPROFILETYPE_DSR && ![[mSession profileInfo] enableCustomerRedemption]) {
//        self.updateButton.hidden = YES;
//    }
}

-(void) setupRegistrationForm {
    [self setupInterface];
    
    [self.detailsFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormTextfieldCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormTextfieldCell"];
    [self.detailsFormTableView registerNib:[UINib nibWithNibName:@"RegistrationButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationButtonCell"];
    [self.detailsFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormDropdownCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormDropdownCell"];
    [self.detailsFormTableView registerNib:[UINib nibWithNibName:@"RegistrationHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationHeaderTableViewCell"];
    [self.detailsFormTableView registerNib:[UINib nibWithNibName:@"RegistrationLabelTextfieldTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationLabelTextfieldTableViewCell"];
    [self.detailsFormTableView registerNib:[UINib nibWithNibName:@"RegistrationMobileNumberCell" bundle:nil] forCellReuseIdentifier:@"RegistrationMobileNumberCell"];
    [self.detailsFormTableView registerNib:[UINib nibWithNibName:@"RegistrationTextfieldWithButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextfieldWithButtonCell"];
    [self.detailsFormTableView registerNib:[UINib nibWithNibName:@"RegistrationTextViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextViewTableViewCell"];
    
    self.registrationFormArray = [[NSMutableArray alloc] initWithArray:
                                  
                                  
#pragma mark - Owner Profile Form setup
                                  @[@{@"Title": LOCALIZATION(C_HEADER_PROFILE), //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_HEADER),
                                      },
                                    @{@"Title": LOCALIZATION(C_HEADER_STAFFTYPE),   //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key": kStaffType,
                                      @"Value": [mSession convertToStaffAccountType:_member.staffType],
                                      },
                                    @{@"Title": LOCALIZATION(C_PROFILE_SALUTATION), //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key": kSalutation,
                                      @"Value": [mSession convertToSalutation:_member.salutation],
                                      @"Optional" : @(YES),
                                      },  //button
                                    @{@"Title": LOCALIZATION(C_PROFILE_FIRSTNAME),  //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kFirstName,
                                      @"Value": _member.firstName,
                                      }, //first name
                                    @{@"Title": LOCALIZATION(C_PROFILE_LASTNAME),   //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kLastName,
                                      @"Value": _member.lastName,
                                      }, //last name
                                    @{@"Title": LOCALIZATION(C_PROFILE_MOBILENUM),  //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_MOBNUM),
                                      @"Key": kMobileNumber,
                                      @"CountryCode": [mSession convertCountryCodeToCountryName: GET_COUNTRY_CODE],
                                      @"TextfieldEnabled": @(YES),
                                      @"MobileNumber": _member.mobileNumber,
                                      },  //Mobile number
                                    @{@"Title": LOCALIZATION(C_PROFILE_FASHAREBADGECODE),   //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                                      @"Icon": @"icon-code",
                                      @"Key": kFAShareBadge,
                                      @"Value": _member.faShareCode,
                                      @"Optional" : @(YES),
                                      },  //FA Share Badge
                                    @{@"Title": LOCALIZATION(C_PROFILE_EMAILADDRESS),   //lokalised 8 Feb
                                      @"Placeholder": LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER), //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kEmailAddress,
                                      @"Keyboard": @(UIKeyboardTypeEmailAddress),
                                      @"Value": _member.emailAddress,
                                      @"Optional" : @(YES),
                                      }, //email
                                    @{@"Title": LOCALIZATION(C_PROFILE_DOB),    //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_BIRTHDATE),
                                      @"Key": kDOB,
                                      @"Value": [self convertDateForDisplay: self.member.dobMonth day:self.member.dobDay],
                                      },   //Date of birth
                                    ]
                                  ];
    
    if (_member.staffType != 2) {
        [_registrationFormArray removeObjectAtIndex:kManageStaff_FASHARECODE];
    }
    
    self.tableViewHeight.constant = CELL_HEIGHT * [self.registrationFormArray count];
    _detailsFormTableView.tableFooterView = [UIView new];
    [_detailsFormTableView reloadData];
//    [_detailsFormTableView setUserInteractionEnabled:NO];
    
}
- (IBAction)backPressed:(UIButton *)sender {
    [self popSelf];
}

- (IBAction)checkBtnAction:(UIButton *)sender {
}

- (IBAction)btnClickAction:(UIButton *)sender {
    NSDictionary *registerParams = @{
                                     @"Salutation": @(self.member.salutation),
                                     @"FirstName": self.member.firstName,
                                     @"LastName": self.member.lastName,
                                     @"MobileNumber": self.member.mobileNumber,
                                     @"EmailAddress": self.member.emailAddress,
                                     @"ContactPreferences": self.member.contactPreference,
                                     @"DOBMonth": self.member.dobMonth,
                                     @"DOBDay": self.member.dobDay,
                                     @"TradeID": self.tradeID,
                                     @"RegistrationStatus": @(4),
                                     @"EnableCustomerRedemption": self.checkButton.selected ? @"1" : @"0",
                                     @"TermCondition": @(1),
                                     @"StaffType":@(self.member.staffType),
                                     @"FAShareCode":self.member.faShareCode,
                                     };
    [[WebServiceManager sharedInstance] updateMechanic: registerParams vc:self];
}

-(void) processCompleted:(WebServiceResponse *)response {
    switch (response.webserviceCall) {
        case kWEBSERVICE_FETCHMECHANIC:
            _responseData = [response getGenericResponse];
            _member = [[Member alloc]initWithData:_responseData];
             [self setupRegistrationForm];
            break;
        case kWEBSERVICE_UPDATEMECHANIC:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                [self backPressed: nil];
            }];
        }
            break;
        default:
            break;
    }
}

//TableView

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.registrationFormArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *regFieldData = [self.registrationFormArray objectAtIndex: indexPath.row];
    
    switch ([[regFieldData objectForKey: @"Type"] intValue])
    {
        case kREGISTRATION_TEXTFIELD:
        {
            RegistrationFormTextfieldCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormTextfieldCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [textfieldCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [textfieldCell hideTopLabel: NO];

            [textfieldCell setTextfieldText:[regFieldData objectForKey:@"Value"]];
            
            [textfieldCell setUserInteractionEnabled:NO];
            
            return textfieldCell;
        }
            break;
        case kREGISTRATION_DROPDOWN:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
            
                [dropdownCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            
            [dropdownCell setSelectionTitle:[regFieldData objectForKey:@"Value"]];
            [dropdownCell hideTopLabel: NO];
            dropdownCell.button.tag = indexPath.row;
            
            [dropdownCell.button addTarget: self action: @selector(dropdownPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            [dropdownCell setUserInteractionEnabled:NO];
            
            return dropdownCell;
        }
            break;
        case kREGISTRATION_TEXTFIELDWITHBUTTON:
        {
            RegistrationTextfieldWithButtonCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationTextfieldWithButtonCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            if ([regFieldData objectForKey: @"TextfieldEnabled"])
                [textfieldCell setTextfieldEnabled: [[regFieldData objectForKey: @"TextfieldEnabled"] boolValue]];
            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [textfieldCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [textfieldCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [textfieldCell hideTopLabel: NO];
            [textfieldCell setTextfieldDelegate: self];
            [textfieldCell setTextFieldTag:indexPath.row];
            
            if ([[regFieldData objectForKey: @"Key"] isEqualToString: kFAShareBadge])
            {
                [textfieldCell.button addTarget:self action:@selector(btnScanFABadgeCode:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            if ([regFieldData objectForKey: @"Value"])
                [textfieldCell setTextfieldText: [regFieldData objectForKey: @"Value"]];
            
            if (![[_responseData objectForKey:@"HasUpdateMechanic"] boolValue]) {
                 [textfieldCell setUserInteractionEnabled:NO];
                 [textfieldCell.button setUserInteractionEnabled:NO];
            }
            
            return textfieldCell;
        }
            break;
        case kREGISTRATION_BUTTON:
        {
            RegistrationButtonCell *buttonCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationButtonCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [buttonCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [buttonCell setTitle: [regFieldData objectForKey: @"Title"]];
            [buttonCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [buttonCell hideTopLabel: NO];
            
            [buttonCell setTitle:[regFieldData objectForKey:@"Value"]];
            
            [buttonCell setUserInteractionEnabled:NO];
            return buttonCell;
        }
            break;
        case kREGISTRATION_MOBNUM:
        {
            RegistrationMobileNumberCell *mobNumCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationMobileNumberCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [mobNumCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            if ([regFieldData objectForKey: @"TextfieldEnabled"])
                [mobNumCell setTextfieldEnabled: [[regFieldData objectForKey: @"TextfieldEnabled"] boolValue]];
            [mobNumCell setMobileCodeEnabled: NO];
            [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
//            [mobNumCell setTextfieldDelegate:self];
            [mobNumCell setTextFieldTag:indexPath.row];
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [mobNumCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [mobNumCell hideTopLabel: NO];
            
            [mobNumCell setCountryCode: [regFieldData objectForKey: @"CountryCode"]];
            [mobNumCell setMobileNumber: [regFieldData objectForKey: @"MobileNumber"]];
            
            [mobNumCell setUserInteractionEnabled:NO];
            
            return mobNumCell;
        }
            break;
        case kREGISTRATION_LABELWITHTEXTFIELD:
        {
            RegistrationLabelTextfieldTableViewCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationLabelTextfieldTableViewCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [textfieldCell setLeftTitle: [regFieldData objectForKey: @"Title"]];
            [textfieldCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
//            [textfieldCell hideTopLabel: YES];
            
            [textfieldCell setUserInteractionEnabled:NO];
            
            return textfieldCell;
        }
            break;
        case kREGISTRATION_BIRTHDATE:
        {
            RegistrationFormDropdownCell *bdaySelectionCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [bdaySelectionCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [bdaySelectionCell setTitle: [regFieldData objectForKey: @"Title"]];
            [bdaySelectionCell setSelectionTitle:[regFieldData objectForKey: @"Value"]];
        
            [bdaySelectionCell hideTopLabel: NO];
            bdaySelectionCell.button.tag = indexPath.row;
            
            [bdaySelectionCell setUserInteractionEnabled:NO];
            
//            [bdaySelectionCell.button addTarget: self action: @selector(birthdayPressed:) forControlEvents:UIControlEventTouchUpInside];
            return bdaySelectionCell;
        }
            break;
        case kREGISTRATION_HEADER:
        {
            RegistrationHeaderTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationHeaderTableViewCell"];
            [headerCell setTitle: [regFieldData objectForKey: @"Title"]];
            return headerCell;
        }
            break;
        default:
            NSLog(@"UNKNOWN CELL TYPE");
            return [UITableViewCell new];
            break;
    }
    return [UITableViewCell new];
}

-(NSString *) convertDateForDisplay:(NSString *)monthString day:(NSString *) dayString
{
    if ([monthString isEqualToString: BLANK] || [dayString isEqualToString: BLANK])
        return LOCALIZATION(C_PROFILE_DOB_PLACEHOLDER); //lokalised 8 Feb
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    [dateFormat setDateFormat:@"MMdd"];
    NSDate *date = [dateFormat dateFromString: [NSString stringWithFormat: @"%@%@", monthString, dayString]];
    
    // Convert date object to desired output format
    [dateFormat setDateFormat:@"dd MMMM"];
    return [dateFormat stringFromDate:date];
}

-(void) dropdownPressed:(UIButton *)sender
{
    [self.view endEditing: YES];
    listView.tag = sender.tag;
    
    switch (listView.tag)
    {
        case kManageStaff_STAFFTYPE:
            [listView setTitleAndList: LOCALIZATION(C_PROFILE_STAFFTYPE_PLACEHOLDER) list: [mSession getStaffAccountType] hasSearchField: NO];  //lokalised 8 Feb
            break;
        case kManageStaff_SALUTATION:
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO]; //lokalised 8 Feb
            break;
        default:
            return;
            break;
    }
    [self popUpView];
}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

- (IBAction)updateBtnAction:(UIButton *)sender {
    
    NSDictionary *registerParams = @{
                                     @"Salutation": @(self.member.salutation),
                                     @"FirstName": self.member.firstName,
                                     @"LastName": self.member.lastName,
                                     @"MobileNumber": self.member.mobileNumber,
                                     @"EmailAddress": self.member.emailAddress,
                                     @"ContactPreferences": self.member.contactPreference,
                                     @"DOBMonth": self.member.dobMonth,
                                     @"DOBDay": self.member.dobDay,
                                     @"TradeID": self.tradeID,
                                     @"RegistrationStatus": @(1),
                                     @"EnableCustomerRedemption": self.checkButton.selected ? @"1" : @"0",
                                     @"TermCondition": @(1),
                                     @"StaffType":@(self.member.staffType),
                                     @"FAShareCode":self.member.faShareCode,
                                     };
    [[WebServiceManager sharedInstance] updateMechanic:registerParams vc:self];
    
}

-(void) textFieldDidEndEditing:(UITextField *)textField {
    switch(textField.tag) {
        case kManageStaff_FASHARECODE:
            self.member.faShareCode = textField.text;
            break;
        default:
            break;
    }
}

- (IBAction)btnScanFABadgeCode:(id)sender {
    [mQRCodeScanner showZBarScannerOnVC: self];
}

- (void)qrCodeScanSuccess:(NSString *)scanString
{
    NSString *scannedValue = [[scanString componentsSeparatedByString:@"="] lastObject];
    self.member.faShareCode = scannedValue;
    [self.registrationFormArray replaceObjectAtIndex:kManageStaff_FASHARECODE
                                          withObject:@{
                                                       @"Title": LOCALIZATION(C_PROFILE_FASHAREBADGECODE),  //lokalised 8 Feb
                                                       @"Type": @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                                                       @"Icon": @"icon-code",
                                                       @"Key": kFAShareBadge,
                                                       @"Value": scannedValue,
                                                       }];
    [self.detailsFormTableView reloadData];
//    NSLog(@"scan success ---------------------");
}

@end

