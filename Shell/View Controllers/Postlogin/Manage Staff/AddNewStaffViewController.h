//
//  AddNewStaffViewController.h
//  Shell
//
//  Created by Edenred on 24/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@protocol StaffAddNewDelegate
-(void)didRegisterSuccess;
@end

@interface AddNewStaffViewController : BaseVC

@property NSString *workshopTradeID;
@property id<StaffAddNewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
