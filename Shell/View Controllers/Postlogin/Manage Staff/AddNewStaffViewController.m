//
//  AddNewStaffViewController.m
//  Shell
//
//  Created by Edenred on 24/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "AddNewStaffViewController.h"
#import "TTTAttributedLabel.h"
#import "CASDatePickerView.h"
#import "QRCodeScanner.h"

enum kAddNewStaff_TYPE
{
    kAddNewStaff_HEADER = 0,
    kAddNewStaff_STAFFTYPE,
    kAddNewStaff_SALUTATION,
    kAddNewStaff_FIRSTNAME,
    kAddNewStaff_LASTNAME,
    kAddNewStaff_MOBNUM,
    kAddNewStaff_FASHARECODE,
    kAddNewStaff_EMAIL,
    kAddNewStaff_DOB,
};

#define CELL_HEIGHT 80

#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kEmailAddress       @"EmailAddress"
#define kDOB                @"DOB"
#define kDOBMonth           @"DOBMonth"
#define kDOBDay             @"DOBDay"
#define kStaffType          @"StaffType"
#define kFAShareBadge       @"FAShareCode"

#define KEYINREGSUBMISSONDICTFROMTABLE [_regSubmissionDict objectForKey:[regFieldData objectForKey:@"Key"]]

@interface AddNewStaffViewController () <UITableViewDelegate,UITableViewDataSource,WebServiceManagerDelegate,CommonListDelegate,DatePickerDelegate,QRCodeScannerDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *requiredFieldsLabel;
@property (weak, nonatomic) IBOutlet UITableView *registerStaffTableView;
@property (weak, nonatomic) IBOutlet UIView *redeemView;
@property (weak, nonatomic) IBOutlet UIButton *redeemButton;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *redeemViewLabel;
@property (weak, nonatomic) IBOutlet UIButton *registerNewStaffBtn;

@property NSMutableArray *registrationFormArray; //ui only
@property NSMutableDictionary *regSubmissionDict;
@property Member *member;
@property CASDatePickerView *dateView;
@property NSMutableDictionary *enteredValues;

@property BOOL isMechanic;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;

@end

@implementation AddNewStaffViewController
@synthesize listView;
@synthesize dateView;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.registerStaffTableView.delegate = self;
    self.registerStaffTableView.dataSource = self;
    
    [self setupRegistrationForm];
    
    if (!self.workshopTradeID)    //use own trade ID
    {
        LoginInfo *loginInfo = [mSession loadUserProfile];
        self.workshopTradeID = loginInfo.tradeID;
    }
    
    if([[mSession profileInfo] enableCustomerRedemption])
    {
        [self.redeemViewLabel setFont: FONT_B2];
        self.redeemViewLabel.text = LOCALIZATION(C_MECH_REDEEMFLAG);    //lokalised 8 Feb
    }
    else
    {
        [self.redeemView removeFromSuperview];
    }
    
    NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @"*" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B3, }];
    
    NSMutableAttributedString *requiredFieldsAttrString = [[NSMutableAttributedString alloc] initWithString: LOCALIZATION(C_PROFILE_REQUIREDFIELDS) attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B3, }];   //lokalised 8 Feb
    
    [asteriskAttrString appendAttributedString:  requiredFieldsAttrString];
    
    //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
    self.requiredFieldsLabel.attributedText = asteriskAttrString;
    
    _isMechanic = false;
}

-(void) setupRegistrationForm
{
    [self.registerStaffTableView registerNib:[UINib nibWithNibName:@"RegistrationFormTextfieldCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormTextfieldCell"];
    [self.registerStaffTableView registerNib:[UINib nibWithNibName:@"RegistrationButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationButtonCell"];
    [self.registerStaffTableView registerNib:[UINib nibWithNibName:@"RegistrationFormDropdownCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormDropdownCell"];
    [self.registerStaffTableView registerNib:[UINib nibWithNibName:@"RegistrationHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationHeaderTableViewCell"];
    [self.registerStaffTableView registerNib:[UINib nibWithNibName:@"RegistrationLabelTextfieldTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationLabelTextfieldTableViewCell"];
    [self.registerStaffTableView registerNib:[UINib nibWithNibName:@"RegistrationMobileNumberCell" bundle:nil] forCellReuseIdentifier:@"RegistrationMobileNumberCell"];
    [self.registerStaffTableView registerNib:[UINib nibWithNibName:@"RegistrationTextfieldWithButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextfieldWithButtonCell"];
    [self.registerStaffTableView registerNib:[UINib nibWithNibName:@"RegistrationTextViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextViewTableViewCell"];
    
    self.regSubmissionDict = [[NSMutableDictionary alloc] init];
    self.enteredValues = [[NSMutableDictionary alloc]init];
    
    self.registrationFormArray = [[NSMutableArray alloc] initWithArray:
                                  
                                  
#pragma mark - Owner Profile Form setup
                                  @[@{@"Title": LOCALIZATION(C_HEADER_PROFILE), //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_HEADER),
                                      },
                                    @{@"Title": LOCALIZATION(C_HEADER_STAFFTYPE),   //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key": kStaffType,
                                      @"Value": LOCALIZATION(C_PROFILE_STAFFTYPE_PLACEHOLDER),  //lokalised 8 Feb
                                      },
                                    @{@"Title": LOCALIZATION(C_PROFILE_SALUTATION), //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key": kSalutation,
                                      @"Value": LOCALIZATION(C_PROFILE_SALUTATION), //lokalised 8 Feb
                                      },  //button
                                    @{@"Title": LOCALIZATION(C_PROFILE_FIRSTNAME),  //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kFirstName,
                                      @"Value": @"",
                                      }, //first name
                                    @{@"Title": LOCALIZATION(C_PROFILE_LASTNAME),   //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kLastName,
                                      @"Value": @"",
                                      }, //last name
                                    @{@"Title": LOCALIZATION(C_PROFILE_MOBILENUM),  //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_MOBNUM),
                                      @"Key": kMobileNumber,
                                      @"CountryCode": [mSession convertCountryCodeToCountryName: GET_COUNTRY_CODE],
                                      @"TextfieldEnabled": @(YES),
                                      @"MobileNumber": @"",
                                      },  //Mobile number
                                    @{@"Title": LOCALIZATION(C_PROFILE_FASHAREBADGECODE),   //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                                      @"Icon": @"icon-code",
                                      @"Key": kFAShareBadge,
                                      @"Value": @"",
                                      @"Optional": @(YES),
                                      },  //FA Share Badge
                                    @{@"Title": LOCALIZATION(C_PROFILE_EMAILADDRESS),   //lokalised 8 Feb
                                      @"Placeholder": LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER), //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kEmailAddress,
                                      @"Keyboard": @(UIKeyboardTypeEmailAddress),
                                      @"Value": @"",
                                      @"Optional": @(YES),
                                      }, //email
                                    @{@"Title": LOCALIZATION(C_PROFILE_DOB),    //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_BIRTHDATE),
                                      @"Key": kDOB,
                                      @"Value": LOCALIZATION(C_PROFILE_DOB_PLACEHOLDER),    //lokalised 8 Feb
                                      },   //Date of birth
                                    ]
                                  ];
    
    //set tableview height. 4 shorter cells
    self.tableViewHeight.constant = CELL_HEIGHT * [self.registrationFormArray count];
    self.registerStaffTableView.tableFooterView = [UIView new];
    [self.registerStaffTableView reloadData];
    
    self.member = [[Member alloc] init];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    
    mQRCodeScanner.delegate = self;
    [self setupGoogleAnalytics];
}

-(void) setupGoogleAnalytics
{
//    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//    [tracker set:kGAIScreenName value:@"Add New Staff"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_MANAGEMECHANIC),LOCALIZATION_EN(C_MANAGEMECHANIC_ADDNEW)] screenClass:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    [self setupInterface];
    [self setupRegistrationForm];
}

-(void) setupInterface
{
    listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_PROFILE_STAFFTYPE_PLACEHOLDER)  //lokalised 8 Feb
                                                   list: [mSession getStaffAccountType]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
    
    
    dateView = [[CASDatePickerView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_BIRTHDAY) previouslySelected:nil withYearHidden:YES];   //lokalised 8 Feb
    
    dateView.delegate = self;
    
    [self.registerNewStaffBtn.titleLabel setFont: FONT_BUTTON];
    [self.registerNewStaffBtn setTitle:LOCALIZATION(C_FORM_REGISTERNEWSTAFF) forState:UIControlStateNormal];    //lokalised 8 Feb
    [Helper setCustomFontButtonContentModes: self.registerNewStaffBtn];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     NSDictionary *regFieldData = [self.registrationFormArray objectAtIndex: indexPath.row];
    
    switch ([[regFieldData objectForKey: @"Type"] intValue])
    {
        case kREGISTRATION_TEXTFIELD:
        {
            RegistrationFormTextfieldCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormTextfieldCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [textfieldCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [textfieldCell hideTopLabel: NO];
            [textfieldCell setTextfieldDelegate:self];
            [textfieldCell setTextFieldTag:indexPath.row];
            
            if([_enteredValues objectForKey:[regFieldData objectForKey:@"Key"]] == nil || [[_enteredValues objectForKey:[regFieldData objectForKey:@"Key"]] isEqualToString:@""]) {
                if ([regFieldData objectForKey: @"Value"])
                    [textfieldCell setTextfieldText: [regFieldData objectForKey: @"Value"]];
                if ([regFieldData objectForKey: @"Placeholder"])
                    [textfieldCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
            } else {
                [textfieldCell setTextfieldText:[_enteredValues objectForKey:[regFieldData objectForKey:@"Key"]]];
            }
           
            if(KEYINREGSUBMISSONDICTFROMTABLE == nil) {
                 [self.regSubmissionDict setObject:textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
                [self.enteredValues setObject:@"" forKey:[regFieldData objectForKey:@"Key"]];
                
            }
            return textfieldCell;
        }
            break;
        case kREGISTRATION_DROPDOWN:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [dropdownCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            
            if([_enteredValues objectForKey:[regFieldData objectForKey:@"Key"]] == nil || [[_enteredValues objectForKey:[regFieldData objectForKey:@"Key"]] isEqualToString:@""]) {
                [dropdownCell setSelectionTitle:[regFieldData objectForKey: @"Value"]];
            } else {
                 [dropdownCell setSelectionTitle:[_enteredValues objectForKey:[regFieldData objectForKey:@"Key"]]];
            }
            [dropdownCell hideTopLabel: NO];
            dropdownCell.button.tag = indexPath.row;
            
            [dropdownCell.button addTarget: self action: @selector(dropdownPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            
            //keep a reference of cell to submit json value later
            if(KEYINREGSUBMISSONDICTFROMTABLE == nil) {
                [self.regSubmissionDict setObject:dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
                 [self.enteredValues setObject:@"" forKey:[regFieldData objectForKey:@"Key"]];
            }
            return dropdownCell;
        }
            break;
        case kREGISTRATION_TEXTFIELDWITHBUTTON:
        {
            RegistrationTextfieldWithButtonCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationTextfieldWithButtonCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            if ([regFieldData objectForKey: @"TextfieldEnabled"])
                [textfieldCell setTextfieldEnabled: [[regFieldData objectForKey: @"TextfieldEnabled"] boolValue]];
            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [textfieldCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [textfieldCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [textfieldCell hideTopLabel: NO];
            [textfieldCell setTextfieldDelegate: self];
            [textfieldCell setTextFieldTag:indexPath.row];
            
            if ([[regFieldData objectForKey: @"Key"] isEqualToString: kFAShareBadge])
            {
                [textfieldCell.button addTarget:self action:@selector(btnScanFABadgeCode:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            if([_enteredValues objectForKey:[regFieldData objectForKey:@"Key"]] == nil || [[_enteredValues objectForKey:[regFieldData objectForKey:@"Key"]] isEqualToString:@""]) {
                if ([regFieldData objectForKey: @"Value"])
                    [textfieldCell setTextfieldText: [regFieldData objectForKey: @"Value"]];
            } else {
                [textfieldCell setTextfieldText:[_enteredValues objectForKey:[regFieldData objectForKey:@"Key"]]];
            }
            
            if(KEYINREGSUBMISSONDICTFROMTABLE == nil) {
                [self.regSubmissionDict setObject:textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
                 [self.enteredValues setObject:@"" forKey:[regFieldData objectForKey:@"Key"]];
            }
            return textfieldCell;
        }
            break;
        case kREGISTRATION_BUTTON:
        {
            RegistrationButtonCell *buttonCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationButtonCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [buttonCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [buttonCell setTitle: [regFieldData objectForKey: @"Title"]];
            [buttonCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [buttonCell hideTopLabel: NO];
            
            if(KEYINREGSUBMISSONDICTFROMTABLE == nil) {
                [self.regSubmissionDict setObject:buttonCell forKey: [regFieldData objectForKey: @"Key"]];
                 [self.enteredValues setObject:@"" forKey:[regFieldData objectForKey:@"Key"]];
            }
            return buttonCell;
        }
            break;
        case kREGISTRATION_MOBNUM:
        {
            RegistrationMobileNumberCell *mobNumCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationMobileNumberCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [mobNumCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            if ([regFieldData objectForKey: @"TextfieldEnabled"])
                [mobNumCell setTextfieldEnabled: [[regFieldData objectForKey: @"TextfieldEnabled"] boolValue]];
            [mobNumCell setMobileCodeEnabled: NO];
            [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
            [mobNumCell setTextfieldDelegate:self];
            [mobNumCell setTextFieldTag:indexPath.row];
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [mobNumCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [mobNumCell hideTopLabel: NO];
            
            [mobNumCell setCountryCode: [regFieldData objectForKey: @"CountryCode"]];
            [mobNumCell setMobileNumber: [regFieldData objectForKey: @"MobileNumber"]];
            
            if([_enteredValues objectForKey:[regFieldData objectForKey:@"Key"]] != nil) {
                [mobNumCell setMobileNumber:[_enteredValues objectForKey:[regFieldData objectForKey:@"Key"]]];
            }
            
            if(KEYINREGSUBMISSONDICTFROMTABLE == nil) {
                [self.regSubmissionDict setObject:mobNumCell forKey: [regFieldData objectForKey: @"Key"]];
                 [self.enteredValues setObject:@"" forKey:[regFieldData objectForKey:@"Key"]];
            }
                
            return mobNumCell;
        }
            break;
        case kREGISTRATION_LABELWITHTEXTFIELD:
        {
            RegistrationLabelTextfieldTableViewCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationLabelTextfieldTableViewCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [textfieldCell setLeftTitle: [regFieldData objectForKey: @"Title"]];
            [textfieldCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
//            [textfieldCell hideTopLabel: YES];
            
            if(KEYINREGSUBMISSONDICTFROMTABLE == nil) {
                [self.regSubmissionDict setObject:textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
                 [self.enteredValues setObject:@"" forKey:[regFieldData objectForKey:@"Key"]];
            }
            return textfieldCell;
        }
            break;
        case kREGISTRATION_BIRTHDATE:
        {
            RegistrationFormDropdownCell *bdaySelectionCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [bdaySelectionCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            //Salutation
            [bdaySelectionCell setTitle: [regFieldData objectForKey: @"Title"]];
            if([_enteredValues objectForKey:[regFieldData objectForKey:@"Key"]] == nil || [[_enteredValues objectForKey:[regFieldData objectForKey:@"Key"]] isEqualToString:@""]) {
                [bdaySelectionCell setSelectionTitle:[regFieldData objectForKey: @"Value"]];
            } else {
                [bdaySelectionCell setSelectionTitle:[_enteredValues objectForKey:[regFieldData objectForKey:@"Key"]]];
            }
            [bdaySelectionCell hideTopLabel: NO];
            bdaySelectionCell.button.tag = indexPath.row;
            
            [bdaySelectionCell.button addTarget: self action: @selector(birthdayPressed:) forControlEvents:UIControlEventTouchUpInside];
            if(KEYINREGSUBMISSONDICTFROMTABLE == nil) {
                [self.regSubmissionDict setObject:bdaySelectionCell forKey: [regFieldData objectForKey: @"Key"]];
                 [self.enteredValues setObject:@"" forKey:[regFieldData objectForKey:@"Key"]];
            }
            return bdaySelectionCell;
        }
            break;
        case kREGISTRATION_HEADER:
        {
            RegistrationHeaderTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationHeaderTableViewCell"];
             [headerCell setTitle: [regFieldData objectForKey: @"Title"]];
            return headerCell;
        }
            break;
        default:
            NSLog(@"UNKNOWN CELL TYPE");
            return [UITableViewCell new];
            break;
    }
    return [UITableViewCell new];
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.registrationFormArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

- (IBAction)redeemBtnClicked:(UIButton *)sender {
    if ([sender isSelected])
        sender.selected = NO;
    else
        sender.selected = YES;
}

- (IBAction)registerNewStaffAction:(UIButton *)sender {
    self.member.firstName =   ([_enteredValues objectForKey: kFirstName] == nil) ? @"" : [_enteredValues objectForKey: kFirstName];
    self.member.lastName =    ([_enteredValues objectForKey: kLastName] == nil) ? @"" : [_enteredValues objectForKey: kLastName];
    self.member.mobileNumber = ([_enteredValues objectForKey: kMobileNumber] == nil) ? @"" : [_enteredValues objectForKey: kMobileNumber];
    self.member.emailAddress = ([_enteredValues objectForKey: kEmailAddress] == nil) ? @"" : [_enteredValues objectForKey: kEmailAddress];
    self.member.countryCode  = GET_COUNTRY_CODE;
    self.member.salutation   = [[mSession convertToSalutationKeyCode:[_enteredValues objectForKey:kSalutation]] intValue];
    self.member.staffType = [[mSession convertToStaffAccountTypeCode:[_enteredValues objectForKey:kStaffType]]intValue];
    
    
    NSMutableDictionary *registerParams = [[NSMutableDictionary alloc] init];
    
    [registerParams setObject:self.workshopTradeID forKey:@"WorkshopTradeID"];
    [registerParams setObject: @(self.member.salutation) forKey:@"Salutation"];
    [registerParams setObject:@(self.member.staffType) forKey:@"StaffType"];
    [registerParams setObject:self.member.firstName forKey:@"FirstName"];
    [registerParams setObject:self.member.lastName forKey:@"LastName"];
    [registerParams setObject:self.member.mobileNumber forKey:@"MobileNumber"];
    [registerParams setObject:self.member.emailAddress forKey: @"EmailAddress"];
    [registerParams setObject:self.member.dobMonth forKey: @"DOBMonth"];
    [registerParams setObject:self.member.dobDay forKey:@"DOBDay"];
    [registerParams setObject:@(self.redeemButton.selected) forKey:@"EnableCustomerRedemption"];
    
    if([[_enteredValues objectForKey:kStaffType] isEqualToString:[mSession convertToStaffAccountType:2]]) {
        self.member.faShareCode = [_enteredValues objectForKey:kFAShareBadge];
        [registerParams setObject:self.member.faShareCode forKey:@"FAShareCode"];
    }
    
    
    [[WebServiceManager sharedInstance] addMechanic:registerParams vc:self];
    
}

-(void) dropdownPressed:(UIButton *)sender
{
    [self.view endEditing: YES];
    listView.tag = sender.tag;
    
    switch (listView.tag)
    {
        case kAddNewStaff_STAFFTYPE:
            [listView setTitleAndList: LOCALIZATION(C_PROFILE_STAFFTYPE_PLACEHOLDER) list: [mSession getStaffAccountType] hasSearchField: NO];  //lokalised 8 Feb
            break;
        case kAddNewStaff_SALUTATION:
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO]; //lokalised 8 Feb
            break;
        default:
            return;
            break;
    }
    [self popUpView];
}

- (IBAction)birthdayPressed:(UIButton *)sender {
    int tagToBeChecked = kAddNewStaff_DOB;
    if(_isMechanic == true) {
        tagToBeChecked -= 1;
    }
    if(sender.tag == tagToBeChecked) {
        [self.view endEditing: YES];
        
        UIWindow* window = [[UIApplication sharedApplication] keyWindow];
        [window addSubview: dateView];
        dateView.translatesAutoresizingMaskIntoConstraints = NO;
        [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings (dateView)]];
        [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings (dateView)]];
    }
}

- (void)selectedDate:(NSDate *)selectedDate
{
    //do smething
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //   for API
    [formatter setDateFormat:@"dd"];
    self.member.dobDay = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"MM"];
    self.member.dobMonth = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"dd MMMM"];
    
    [formatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    NSString *selectedDateString = [formatter stringFromDate:selectedDate];
    
    
    RegistrationFormDropdownCell *cell = [self.regSubmissionDict objectForKey: kDOB];
    [cell setSelectionTitle: selectedDateString];
    [_enteredValues setObject:selectedDateString forKey:kDOB];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        RegistrationFormDropdownCell *cell;
        switch (listView.tag)
        {
            case kAddNewStaff_STAFFTYPE:
                cell = [self.regSubmissionDict objectForKey:kStaffType];
                [_enteredValues setObject:selection.firstObject forKey:kStaffType];
                [self staffTypeUpdated];
                break;
            case kAddNewStaff_SALUTATION:
                cell = [self.regSubmissionDict objectForKey: kSalutation];
                [_enteredValues setObject:selection.firstObject forKey:kSalutation];
                break;
            default:
                return;
                break;
        }
        [cell setSelectionTitle:selection.firstObject];
    }
}

-(void) staffTypeUpdated {
    if([[_enteredValues objectForKey:kStaffType] isEqualToString:[mSession convertToStaffAccountType:2]]) {
        _isMechanic = false;
        if ([[[self.registrationFormArray objectAtIndex:kAddNewStaff_FASHARECODE] objectForKey:@"Key"] isEqualToString:kFAShareBadge]) {
            [self.registrationFormArray replaceObjectAtIndex:kAddNewStaff_FASHARECODE
                                                  withObject:@{@"Title": LOCALIZATION(C_PROFILE_FASHAREBADGECODE),  //lokalised 8 Feb
                                                               @"Type": @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                                                               @"Icon": @"icon-code",
                                                               @"Key": kFAShareBadge,
                                                               @"Value": @"",
                                                               @"Optional": @(NO),
                                                               }];
        } else {
            [self.registrationFormArray insertObject:@{@"Title": LOCALIZATION(C_PROFILE_FASHAREBADGECODE),  //lokalised 8 Feb
                                                       @"Type": @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                                                       @"Icon": @"icon-code",
                                                       @"Key": kFAShareBadge,
                                                       @"Value": @"",
                                                       @"Optional": @(NO),
                                                       } atIndex:kAddNewStaff_FASHARECODE];
        }
    } else {
//        [self.registrationFormArray replaceObjectAtIndex:kAddNewStaff_FASHARECODE
//                                              withObject:@{@"Title": LOCALIZATION(C_PROFILE_FASHAREBADGECODE),
//                                                           @"Type": @(kREGISTRATION_TEXTFIELDWITHBUTTON),
//                                                           @"Icon": @"icon-code",
//                                                           @"Key": kFAShareBadge,
//                                                           @"Value": @"",
//                                                           @"Optional": @(YES),
//                                                           }];
        [self.registrationFormArray removeObjectAtIndex:kAddNewStaff_FASHARECODE];
        _isMechanic = true;
    }
//    [self.registerStaffTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow: kAddNewStaff_FASHARECODE inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    [_registerStaffTableView reloadData];
}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
        _enteredValues = @{}.mutableCopy;
        [self setupRegistrationForm];
        [self.delegate didRegisterSuccess];
    }];
}

- (IBAction)btnScanFABadgeCode:(id)sender {
    [mQRCodeScanner showZBarScannerOnVC: self];
}

- (void)qrCodeScanSuccess:(NSString *)scanString
{
    RegistrationTextfieldWithButtonCell *cell = [self.regSubmissionDict objectForKey: kFAShareBadge];
    [cell setTextfieldText: [[scanString componentsSeparatedByString:@"="] lastObject]];
    [_enteredValues setObject:[[scanString componentsSeparatedByString:@"="] lastObject] forKey:kFAShareBadge];
    NSLog(@"scan success ---------------------");
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void) textFieldDidEndEditing:(UITextField *)textField {
    switch(textField.tag) {
        case kAddNewStaff_FIRSTNAME: {
           [_enteredValues setObject:textField.text forKey:kFirstName];
        }
            break;
        case kAddNewStaff_LASTNAME: {
            [_enteredValues setObject:textField.text forKey:kLastName];
        }
            break;
        case kAddNewStaff_MOBNUM: {
            [_enteredValues setObject:textField.text forKey:kMobileNumber];
        }
            break;
        case kAddNewStaff_FASHARECODE: {
            [_enteredValues setObject:textField.text forKey:kFAShareBadge];
        }
            break;
        case kAddNewStaff_EMAIL: {
            [_enteredValues setObject:textField.text forKey:kEmailAddress];
        }
            break;
    }
}

@end
