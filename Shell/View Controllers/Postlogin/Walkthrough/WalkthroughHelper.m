//
//  WalkthroughHelper.m
//  Shell
//
//  Created by Ben on 27/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "LocalizationManager.h"
#import "WalkthroughHelper.h"

@implementation WalkthroughHelper

+(BOOL)isWalkthroughSeen:(enum WalkthroughType) type
{
    NSMutableDictionary *walkthroughData = GET_WALKTHROUGH_TABLE;
    switch (type) {
        case WALKTHROUGH_WorkshopOffers:
        {
            BOOL isWalkthroughSeen = [walkthroughData objectForKey:VIEW_RU_WORKSHOPOFFER];
            return isWalkthroughSeen;
            break;
        }
            
        case WALKTHROUGH_OilChange:
        {
            break;
        }
            
        case WALKTHORUGH_Rewards:
        {
            break;
        }
            
        default:
            break;
    }
    return false;
}

+(NSMutableArray*)getArrayForType:(enum WalkthroughType) type
{
    NSMutableArray* walkthroughArray = [[NSMutableArray alloc] init];
    switch (type)
    {
    case WALKTHROUGH_WorkshopOffers:
        {
            [walkthroughArray addObject:     @{@"Image": @"tradeworkshopoffers-1",
                                               @"CellType": @"FirstCell",
                                               @"Header" : LOCALIZATION(C_TITLE_WORKSHOPOFFER),     //lokalised
                                               @"Subheader" : LOCALIZATION(@""),
                                               @"Content" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_CONTENT_INTRO),        //lokalised
                                               }
             ];
            [walkthroughArray addObject:     @{@"Image": ([GET_LOCALIZATION isEqualToString: kRussian]) ? @"tradeworkshopoffers-2_ru" : @"tradeworkshopoffers-2",
                                               @"CellType": @"ContentCell",
                                               @"Header" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_HOWITWORKS),            //lokalised
                                               @"Subheader" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_STEP_ONE),           //lokalised
                                               @"Content" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_CONTENT_ONE),          //lokalised
                                               }
             ];
            [walkthroughArray addObject:     @{@"Image": ([GET_LOCALIZATION isEqualToString: kRussian]) ? @"tradeworkshopoffers-3_ru" :  @"tradeworkshopoffers-3",
                                               @"CellType": @"ContentCell",
                                               @"Header" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_HOWITWORKS),            //lokalised
                                               @"Subheader" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_STEP_TWO),           //lokalised
                                               @"Content" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_CONTENT_TWO),          //lokalised
                                               }
             ];
            [walkthroughArray addObject:     @{@"Image": ([GET_LOCALIZATION isEqualToString: kRussian]) ? @"tradeworkshopoffers-4_ru" :  @"tradeworkshopoffers-4",
                                               @"CellType": @"ContentCell",
                                               @"Header" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_HOWITWORKS),            //lokalised
                                               @"Subheader" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_STEP_THREE),         //lokalised
                                               @"Content" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_CONTENT_THREE),        //lokalised
                                               }
             ];
            [walkthroughArray addObject:     @{@"Image": @"tradeworkshopoffers-5",
                                               @"CellType": @"EndCell",
                                               @"Header" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_STEP_LAST),             //lokalised
                                               @"Subheader" : LOCALIZATION(@""),
                                               @"Content" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_CONTENT_LAST),         //lokalised
                                               @"ButtonTitle" : LOCALIZATION(C_FORM_DONE),                                  //lokalised
                                               }
             ];
            break;
        }
            
        case WALKTHROUGH_OilChange:
        {
            [walkthroughArray addObject:     @{@"Image": @"walkthru-oilchange-icon",
                                               @"CellType": @"FirstCell",
                                               @"Header" : LOCALIZATION(C_WALKTHROUGH_REGISTEROILCHANGE_HEADER),            //lokalised
                                               @"Subheader" : LOCALIZATION(@""),
                                               @"Content" : LOCALIZATION(C_WALKTHROUGH_REGISTEROILCHANGE),                  //lokalised
                                               }
             ];
            [walkthroughArray addObject:     @{@"Image": @"walkthru-oilchange-step1",
                                               @"CellType": @"ContentCell",
                                               @"Header" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_HOWITWORKS),            //lokalised
                                               @"Subheader" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_STEP_ONE),           //lokalised
                                               @"Content" : LOCALIZATION(C_WALKTHROUGH_REGISTEROILCHANGE_CONTENT_ONE),      //lokalised
                                               }
             ];
            [walkthroughArray addObject:     @{@"Image": @"walkthru-oilchange-step2",
                                               @"CellType": @"ContentCell",
                                               @"Header" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_HOWITWORKS),            //lokalised
                                               @"Subheader" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_STEP_TWO),           //lokalised
                                               @"Content" : LOCALIZATION(C_WALKTHROUGH_REGISTEROILCHANGE_CONTENT_TWO),      //lokalised
                                               }
             ];
            [walkthroughArray addObject:     @{@"Image": @"walkthru-oilchange-step3",
                                               @"CellType": @"EndCell",
                                               @"Header" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_STEP_LAST),             //lokalised
                                               @"Subheader" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_STEP_LAST),          //lokalised
                                               @"Content" : LOCALIZATION(C_WALKTHROUGH_REGISTEROILCHANGE_CONTENT_LAST),     //lokalised
                                               @"ButtonTitle" : LOCALIZATION(C_FORM_DONE),
                                               }
             ];
            break;
        }
            
        case WALKTHORUGH_Rewards:
        {
            [walkthroughArray addObject:     @{@"Image": @"walkthru-rewards-step1",
                                               @"CellType": @"ContentCell",
                                               @"Header" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_HOWITWORKS),            //lokalised
                                               @"Subheader" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_STEP_ONE),           //lokalised
                                               @"Content" : LOCALIZATION(C_WALKTHROUGH_REWARDS_CONTENT_ONE),                //lokalised
                                               }
             ];
            [walkthroughArray addObject:     @{@"Image": @"walkthru-rewards-step2",
                                               @"CellType": @"ContentCell",
                                               @"Header" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_HOWITWORKS),            //lokalised
                                               @"Subheader" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_STEP_TWO),           //lokalised
                                               @"Content" : LOCALIZATION(C_WALKTHROUGH_REWARDS_CONTENT_TWO),                //lokalised
                                               }
             ];
            [walkthroughArray addObject:     @{@"Image": @"walkthru-rewards-step3",
                                               @"CellType": @"EndCell",
                                               @"Header" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_HOWITWORKS),            //lokalised
                                               @"Subheader" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_STEP_THREE),         //lokalised
                                               @"Content" : LOCALIZATION(C_WALKTHROUGH_REWARDS_CONTENT_LAST),               //lokalised
                                               @"ButtonTitle" : LOCALIZATION(C_FORM_DONE),                                  //lokalised
                                               }
             ];
            break;
        }
    };
    return walkthroughArray;
}

+(void)setWalkthroughReadForType:(enum WalkthroughType) type
{
    //walkthrough available
    NSMutableDictionary *walkthroughData = GET_WALKTHROUGH_TABLE;
    switch (type)
    {
        case WALKTHROUGH_WorkshopOffers:
        {
            [walkthroughData setObject: @(YES) forKey: VIEW_RU_WORKSHOPOFFER];
        }
        case WALKTHROUGH_OilChange:
        {
            
            break;
        }
        case WALKTHORUGH_Rewards:
        {
            break;
        }
    };
    SET_WALKTHROUGH_TABLE(walkthroughData);
}

@end
