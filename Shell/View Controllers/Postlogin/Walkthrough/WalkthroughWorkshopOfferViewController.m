//
//  WalkthroughWorkshopOfferViewController.m
//  Shell
//
//  Created by Jeremy Lua on 26/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "WalkthroughWorkshopOfferViewController.h"
#import "WalkthroughHelper.h"

@interface WalkthroughWorkshopOfferViewController () <UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *colView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *colViewLayout;

@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property NSMutableArray *walkthroughArray;
@end

@implementation WalkthroughWorkshopOfferViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    
    UIButton *btnClose = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30, 30)];
    [btnClose setImage: [UIImage imageNamed: @"icon-close.png"] forState: UIControlStateNormal];
    [btnClose addTarget:self action: @selector(backPressed:) forControlEvents: UIControlEventTouchUpInside];
    if(@available(iOS 11, *)){
        [btnClose.widthAnchor constraintEqualToConstant: btnClose.frame.size.width].active = YES;
        [btnClose.heightAnchor constraintEqualToConstant: btnClose.frame.size.height].active = YES;
    }
    UIBarButtonItem *closeBarBtn = [[UIBarButtonItem alloc] initWithCustomView: btnClose];
    self.navigationItem.rightBarButtonItem = closeBarBtn;
    
    
    self.colViewLayout.itemSize = CGSizeMake(320 * (SCREEN_WIDTH / 320), 431 * (SCREEN_WIDTH / 320));
    
//    @{@"Image": @"image",
//      @"Header" : @"Header",
//      @"Subheader" : @"Subheader",
//      @"Content" : @"Content",
//      }
    self.walkthroughArray = [[NSMutableArray alloc] init];
    [self.walkthroughArray addObject:     @{@"Image": @"tradeworkshopoffers-1",
                                            @"Header" : LOCALIZATION(C_TITLE_WORKSHOPOFFER),        //lokalised
                                            @"Subheader" : LOCALIZATION(@""),
                                            @"Content" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_CONTENT_INTRO),   //lokalised
                                            }
     ];
    [self.walkthroughArray addObject:     @{@"Image": ([GET_LOCALIZATION isEqualToString: kRussian]) ? @"tradeworkshopoffers-2_ru" : @"tradeworkshopoffers-2",
                                            @"Header" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_HOWITWORKS),       //lokalised
                                            @"Subheader" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_STEP_ONE),      //lokalised
                                            @"Content" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_CONTENT_ONE),     //lokalised
                                            }
     ];
    [self.walkthroughArray addObject:     @{@"Image": ([GET_LOCALIZATION isEqualToString: kRussian]) ? @"tradeworkshopoffers-3_ru" :  @"tradeworkshopoffers-3",
                                            @"Header" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_HOWITWORKS),       //lokalised
                                            @"Subheader" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_STEP_TWO),      //lokalised
                                            @"Content" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_CONTENT_TWO),     //lokalised
                                            }
     ];
    [self.walkthroughArray addObject:     @{@"Image": ([GET_LOCALIZATION isEqualToString: kRussian]) ? @"tradeworkshopoffers-4_ru" :  @"tradeworkshopoffers-4",
                                            @"Header" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_HOWITWORKS),           //lokalised
                                            @"Subheader" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_STEP_THREE),        //lokalised
                                            @"Content" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_CONTENT_THREE),       //lokalised
                                            }
     ];
    [self.walkthroughArray addObject:     @{@"Image": @"tradeworkshopoffers-5",
                                            @"Header" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_STEP_LAST),            //lokalised
                                            @"Subheader" : LOCALIZATION(@""),
                                            @"Content" : LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_CONTENT_LAST),        //lokalised
                                            }
     ];
    [self.btnBack setTitle: LOCALIZATION(C_RUSSIA_NEXT_BTN) forState: UIControlStateNormal];
    [self.btnBack setBackgroundColor:COLOUR_RED];
}


-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.colView)
        self.pageControl.currentPage = scrollView.contentOffset.x / scrollView.frame.size.width;
    
    if (self.pageControl.currentPage == [self.walkthroughArray count] - 1)
    {
        [self.btnBack setTitle: LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_LASTBUTTON) forState: UIControlStateNormal];       //lokalised
    }
    else
    {
        [self.btnBack setTitle: LOCALIZATION(C_RUSSIA_NEXT_BTN) forState: UIControlStateNormal];                            //lokalised
    }
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    self.pageControl.numberOfPages = [self.walkthroughArray count];
    return [self.walkthroughArray count];
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    NSDictionary *cellData = [self.walkthroughArray objectAtIndex: indexPath.row];
    if (indexPath.row == 0)
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"FirstCell" forIndexPath: indexPath];
    }
    else if (indexPath.row == [self.walkthroughArray count] - 1) //last cell
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"EndCell" forIndexPath: indexPath];
        
    }
    else //other cells;
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"ContentCell" forIndexPath: indexPath];
    }
    
    UILabel *lblHeader = [cell viewWithTag: WALKTHROUGH_RedHeader];
    lblHeader.text = [cellData objectForKey: @"Header"];
    
    UILabel *lblSubHeader = [cell viewWithTag: WALKTHROUGH_Subheader];
    lblSubHeader.text = [cellData objectForKey: @"Subheader"];
    
    UILabel *lblContent = [cell viewWithTag: WALKTHROUGH_Content];
    lblContent.text = [cellData objectForKey: @"Content"];
    
    UIImageView *imgView = [cell viewWithTag: WALKTHROUGH_Image];
    imgView.image = [UIImage imageNamed: [cellData objectForKey: @"Image"]];
    
    return cell;
}

-(void) setWalkthroughRead
{
    //walkthrough available
    NSMutableDictionary *walkthroughData = GET_WALKTHROUGH_TABLE;
    [walkthroughData setObject: @(YES) forKey: VIEW_RU_WORKSHOPOFFER];
    
    SET_WALKTHROUGH_TABLE(walkthroughData);
}

- (IBAction)backPressed:(id)sender
{
    //load next
    [self setWalkthroughRead];
    [self popSelf];
}

- (IBAction)nextPressed:(id)sender
{
    if (self.pageControl.currentPage == [self.walkthroughArray count] - 1)
    {
        [self setWalkthroughRead];
        UIViewController *vc = [[self.parentViewController childViewControllers] firstObject];
        
        [self popSelf];
        [mSession pushRUAddNewOfferStepOne: vc];
        
    }
    else
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow: self.pageControl.currentPage + 1 inSection:0];
        [self.colView scrollToItemAtIndexPath: indexPath atScrollPosition: UICollectionViewScrollPositionCenteredHorizontally animated: YES];
        self.pageControl.currentPage += 1;
        
        if (self.pageControl.currentPage == [self.walkthroughArray count] - 1)
        {
            [self.btnBack setTitle: LOCALIZATION(C_WALKTHROUGH_WORKSHOPOFFER_LASTBUTTON) forState: UIControlStateNormal];   //lokalised
        }
        else
        {
            [self.btnBack setTitle: LOCALIZATION(C_RUSSIA_NEXT_BTN) forState: UIControlStateNormal];        //lokalised
        }
//        self.colView.contentOffset = CGPointMake(self.colView.contentOffset.x + SCREEN_WIDTH, 0);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
