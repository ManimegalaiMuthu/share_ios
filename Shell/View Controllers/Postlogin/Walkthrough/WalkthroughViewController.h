//
//  WalkthroughViewController.h
//  Shell
//
//  Created by Jeremy Lua on 26/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"
#import "WalkthroughHelper.h"

/**
 WalkthroughViewController is a reusable class for Walkthroughs
 For displaying just simply pass in the WalkthroughType refered from WalkthroughHelper
 For additional types of Walkthorugh, Edit WalkthroughHelper with more types
 */
@interface WalkthroughViewController : BaseVC

@property enum WalkthroughType type;
@property NSMutableArray *walkthroughArray;

-(void)setupWalkthroughArray: (NSMutableArray*) array withType:(enum WalkthroughType)type;

@end
