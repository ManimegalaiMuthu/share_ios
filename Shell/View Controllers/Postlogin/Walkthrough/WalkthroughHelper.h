//
//  WalkthroughHelper.h
//  Shell
//
//  Created by Ben on 27/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

enum WalkthroughType
{
    WALKTHROUGH_WorkshopOffers,
    WALKTHROUGH_OilChange,
    WALKTHORUGH_Rewards,
};

enum
{
    WALKTHROUGH_Image = 1,
    WALKTHROUGH_RedHeader,
    WALKTHROUGH_Subheader,
    WALKTHROUGH_Content,
    WALKTHROUGH_Background = 699,
};

@interface WalkthroughHelper : NSObject

/**
 This method is used to check if a walkthrough has already been viewed. WalkthroughVC does not take into consideration if a walkthrough have been viewed it will show the walkthrough it is requested to. Use this method to determine if you want to show the walkthrough to begin with
 */
+(BOOL)isWalkthroughSeen:(enum WalkthroughType) type;
/**
 This method returns an array compatible with WalkthroughViewController. Simnply pass the returned array with the same type you have passed into this method to WalkthroughViewController and it will display the walkthorugh.
 */
+(NSMutableArray*)getArrayForType:(enum WalkthroughType) type;
/**
 This method is automatically called by WalkthroughViewController. Only manually call this if you are NOT using WalkthroughViewController
 */
+(void)setWalkthroughReadForType:(enum WalkthroughType) type;

@end
