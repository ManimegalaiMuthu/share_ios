//
//  WalkthroughViewController.m
//  Shell
//
//  Created by Jeremy Lua on 26/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "WalkthroughViewController.h"
#import "WalkthroughHelper.h"

@interface WalkthroughViewController () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *colView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *colViewLayout;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end

@implementation WalkthroughViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    
    UIButton *btnClose = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30, 30)];
    [btnClose setImage: [UIImage imageNamed: @"icon-close.png"] forState: UIControlStateNormal];
    [btnClose addTarget:self action: @selector(backPressed:) forControlEvents: UIControlEventTouchUpInside];
    if(@available(iOS 11, *)){
        [btnClose.widthAnchor constraintEqualToConstant: btnClose.frame.size.width].active = YES;
        [btnClose.heightAnchor constraintEqualToConstant: btnClose.frame.size.height].active = YES;
    }
    UIBarButtonItem *closeBarBtn = [[UIBarButtonItem alloc] initWithCustomView: btnClose];
    self.navigationItem.rightBarButtonItem = closeBarBtn;
    
    self.colViewLayout.itemSize = self.view.frame.size;
    
    [self.btnBack setTitle: LOCALIZATION(C_RUSSIA_NEXT_BTN) forState: UIControlStateNormal];    //lokalised
    [self.btnBack setBackgroundColor:COLOUR_RED];
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [self setButtonTitle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)setupWalkthroughArray: (NSMutableArray*) array withType:(enum WalkthroughType)type
{
    self.walkthroughArray = array;
    self.type = type;
    [self.colView reloadData];
}

-(void) setWalkthroughRead
{
    [WalkthroughHelper setWalkthroughReadForType:self.type];
}

- (void)backPressed:(UIButton*)sender
{
    [self setWalkthroughRead];
    if([[self.navigationController childViewControllers]count] > 1) {
        [self popSelf];
    } else {
        [mSession loadHomeView];
    }
}

- (IBAction)nextPressed:(id)sender
{
    if (self.pageControl.currentPage == [self.walkthroughArray count] - 1)
    {
        [self setWalkthroughRead];
        if([[self.navigationController childViewControllers]count] > 1) {
            [self popSelf];
        } else {
           [mSession loadHomeView];
        }
    }
    else
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow: self.pageControl.currentPage + 1 inSection:0];
        [self.colView scrollToItemAtIndexPath: indexPath atScrollPosition: UICollectionViewScrollPositionCenteredHorizontally animated: YES];
        self.pageControl.currentPage += 1;
        [self setButtonTitle];
        //        self.colView.contentOffset = CGPointMake(self.colView.contentOffset.x + SCREEN_WIDTH, 0);
    }
}

-(IBAction)pagerValueChanged:(UIPageControl*)sender
{
    [self.colView scrollRectToVisible:CGRectMake(self.colView.frame.size.width*sender.currentPage, 0,
                                                 self.colView.frame.size.width, self.colView.frame.size.height)
                             animated:true];
}

-(void)setButtonTitle
{
    if(self.walkthroughArray[self.pageControl.currentPage])
    {
        NSString* doneButtonString = self.walkthroughArray[self.pageControl.currentPage][@"ButtonTitle"];
        if(doneButtonString)
        {
            [self.btnBack setTitle: LOCALIZATION(doneButtonString) forState: UIControlStateNormal];
            return;
        }
    }
    if(self.pageControl.currentPage == self.pageControl.numberOfPages)
    {
        [self.btnBack setTitle: LOCALIZATION(C_FORM_DONE) forState: UIControlStateNormal];      //lokalised
    }
    else
    {
        [self.btnBack setTitle: LOCALIZATION(C_RUSSIA_NEXT_BTN) forState: UIControlStateNormal];    //lokalised
    }
}

//MARK: UIScrollViewDelegate
-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.colView)
        self.pageControl.currentPage = scrollView.contentOffset.x / scrollView.frame.size.width;
    [self setButtonTitle];
}

//MARK: UICollectionViewDatasource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    self.pageControl.numberOfPages = [self.walkthroughArray count];
    return [self.walkthroughArray count];
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    NSDictionary *cellData = [self.walkthroughArray objectAtIndex: indexPath.row];
    cell = [collectionView dequeueReusableCellWithReuseIdentifier: [cellData objectForKey:@"CellType"] forIndexPath: indexPath];
    
    UILabel *lblHeader = [cell viewWithTag: WALKTHROUGH_RedHeader];
    lblHeader.text = [cellData objectForKey: @"Header"];
    
    UILabel *lblSubHeader = [cell viewWithTag: WALKTHROUGH_Subheader];
    lblSubHeader.text = [cellData objectForKey: @"Subheader"];
    
    UILabel *lblContent = [cell viewWithTag: WALKTHROUGH_Content];
    lblContent.text = [cellData objectForKey: @"Content"];
    
    if(_type == WALKTHROUGH_OilChange) {
        UIImageView *bgImage = [cell viewWithTag:WALKTHROUGH_Background];
        bgImage.hidden = YES;
    }
    
    UIImageView *imgView = [cell viewWithTag: WALKTHROUGH_Image];
    imgView.image = [UIImage imageNamed: [cellData objectForKey: @"Image"]];
    
    return cell;
}

@end
