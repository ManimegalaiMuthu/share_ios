//
//  PhotoTakingViewController.h
//  Shell
//
//  Created by Ben on 26/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"

@protocol PhotoTakingVCDelegate <NSObject>
-(void)imageTaken:(UIImage*) image key:(NSString *) key;
@end

@interface PhotoTakingViewController : BaseVC
@property (weak, nonatomic) id<PhotoTakingVCDelegate> delegate;
@property NSString* message;
@property NSString *keyString; //for cell
@property BOOL showBorderOverlay;
@property BOOL allowRetake;
@end
