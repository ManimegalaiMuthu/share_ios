//
//  InviteCustomerViewController.m
//  Shell
//
//  Created by Jeremy Lua on 14/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "InviteCustomerViewController.h"

@interface InviteCustomerViewController () <WebServiceManagerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtMobileNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@end

@implementation InviteCustomerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_INVITECUSTOMER) subtitle: @""];
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_INVITECUSTOMER) subtitle: @""];          //lokalise 24 Jan
    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_TITLE_INVITECUSTOMER),  LOCALIZATION(C_REWARDS_BACK)];   //lokalise 24 jan
    
    self.txtMobileNumber.font = FONT_B1;
    self.txtMobileNumber.placeholder = LOCALIZATION(C_PROFILE_MOBILENUM);           //lokalised
    
    self.txtVehicleNumber.font = FONT_B1;
    self.txtVehicleNumber.placeholder = LOCALIZATION(C_SEARCHFIELD_VEHICLEID);      //lokalise 23 Jan
    
    self.btnSubmit.titleLabel.font = FONT_BUTTON;
    self.btnSubmit.backgroundColor = COLOUR_RED;
    
    if(GET_PROFILETYPE == kPROFILETYPE_DSR)
    {
        self.btnSubmit.hidden = YES;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_INVITECUSTOMER) screenClass:nil];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    if (self.isPush)
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    }
}

- (IBAction)submitPressed:(id)sender {
    //Submit API
    
    NSDictionary *submitParams = @{@"Name": @"",
                                   @"MobileNumber": self.txtMobileNumber.text,
                                   @"EmailAddress": @"",
                                   @"VehicleNumber": self.txtVehicleNumber.text,
                                   };
    
    [[WebServiceManager sharedInstance] inviteConsumer: submitParams vc:self];
}

- (IBAction)backPressed:(id)sender {
    //only available prelogin
    [self popSelf];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_INVITECONSUMER:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                self.txtMobileNumber.text = @"";
                self.txtVehicleNumber.text = @"";
            }];
        }
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
