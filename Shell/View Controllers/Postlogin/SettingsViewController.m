//
//  SettingsViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 22/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController () <CommonListDelegate, WebServiceManagerDelegate>
//@property (weak, nonatomic) IBOutlet UILabel *lblSMS;
//@property (weak, nonatomic) IBOutlet UISwitch *smsSwitch;

//@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
//@property (weak, nonatomic) IBOutlet UISwitch *emailSwitch;

@property (weak, nonatomic) IBOutlet UILabel *lblPushNotification;
@property (weak, nonatomic) IBOutlet UISwitch *pushNotificationSwitch;

@property (weak, nonatomic) IBOutlet UILabel *lblLanguageTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnLanguage;

@property (weak, nonatomic) IBOutlet UILabel *lblRefreshTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnRefresh;

@property (weak, nonatomic) IBOutlet UILabel *lblLogout;
@property (weak, nonatomic) IBOutlet UIButton *btnLogout;

@property (weak, nonatomic) IBOutlet UILabel *lblVersion;
@end

@implementation SettingsViewController
@synthesize listView; //must synthesize list view constraints to work

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupInterface];
    [[WebServiceManager sharedInstance] fetchPushStatus: self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveLanguageChangedNotification:)
                                                 name:kNotificationLanguageChanged
                                               object:nil];
    
    if (kIsRightToLeft) {
        [self.btnLanguage setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.btnLanguage setTitleEdgeInsets: UIEdgeInsetsMake(0, 25, 0, 0)];
    } else {
        [self.btnLanguage setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.btnLanguage setTitleEdgeInsets: UIEdgeInsetsMake(0, 0, 0, 25)];
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_SETTINGS) screenClass:nil];
}


-(void) setupInterface
{
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_SETTINGS) subtitle: @""];    //lokalise 17 jan
    
    self.pushNotificationSwitch.onTintColor = COLOUR_YELLOW;
    
    AppInfo *appinfo = [[AppInfo alloc]initWithAppInfo];
    if (IS_DEVELOPMENT_VERSION || IS_STAGING_VERSION)
        self.lblVersion.text = [NSString stringWithFormat:@"%@ %@ (%@)",LOCALIZATION(C_GLOBAL_VERSION).capitalizedString, appinfo.appVersion, appinfo.appBuild ];     //lokalise 17 jan
    else
        self.lblVersion.text = [NSString stringWithFormat:@"%@ %@",LOCALIZATION(C_GLOBAL_VERSION).capitalizedString, appinfo.appVersion];     //lokalise 17 jan
//    self.lblSMS.text = LOCALIZATION(C_SETTINGS_SMS);
//    self.lblEmail.text = LOCALIZATION(C_SETTINGS_EMAIL);
    self.lblPushNotification.text = LOCALIZATION(C_SETTINGS_PUSH_NOTIFICATION); //lokalise 17 jan
    
    
    [self.lblPushNotification setFont: FONT_B1];
    [self.lblVersion setFont: FONT_B1];
    
    [self.lblLanguageTitle setFont: self.lblPushNotification.font];
    self.lblLanguageTitle.text = LOCALIZATION(C_SETTINGS_LANGUAGE);     //lokalise 17 jan
    
    [self.btnLanguage.titleLabel setFont: FONT_B1];
    
    [self.lblRefreshTitle setFont: self.lblPushNotification.font];
    self.lblRefreshTitle.text = LOCALIZATION(C_SETTINGS_REFRESHDATA);     //lokalise 17 jan
    
    
    NSString *sortCode = [mSession convertLanguageKeyCodeToSortCode:  @([kAvailableLanguages indexOfObject: GET_LOCALIZATION]).stringValue];
    NSInteger indexCode = [[mSession convertLanguageSortCodeToKeyCode: sortCode] intValue];
    
    if ([sortCode isEqualToString: [kThai uppercaseString]])
        [self.btnLanguage setTitle: @"ภาษาไทย" forState:UIControlStateNormal];
    else if (indexCode == [kAvailableLanguages indexOfObject: kChineseT_HK])
        [self.btnLanguage setTitle: @"中文" forState:UIControlStateNormal];
    else if (indexCode == [kAvailableLanguages indexOfObject: kFilipino])
        [self.btnLanguage setTitle: @"PH" forState:UIControlStateNormal];
    else
        [self.btnLanguage setTitle: sortCode  forState: UIControlStateNormal];
    
    self.lblLogout.text = LOCALIZATION(C_SETTINGS_LOGOUT);     //lokalise 17 jan
    [self.lblLogout setFont: self.lblPushNotification.font];
    
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_DROPDOWN_LANGUAGE) list: [mSession getLanguageList] selectionType:ListSelectionTypeSingle previouslySelected: nil];     //lokalise 17 jan
    listView.delegate = self;
}

- (void) receiveLanguageChangedNotification:(NSNotification *) notification
{
    if ([notification.name isEqualToString:kNotificationLanguageChanged])
    {
        [self setupInterface];
        [mLookupManager reloadLookup];
        [[IQKeyboardManager sharedManager] setToolbarDoneBarButtonItemText: LOCALIZATION(C_FORM_DONE)];

        [mSession updateAnalyticsUserProperty];
    }
}

- (IBAction)languageSelectPressed:(id)sender {
//    listView.tag = kPopupSelection_Language;
    [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_LANGUAGE) list: [mSession getLanguageList] hasSearchField: NO];      //lokalise 17 jan
    
    [self popUpView];
}

- (IBAction)pushNotificationChanged:(id)sender {
    [[WebServiceManager sharedInstance] updatePushStatus: [self.pushNotificationSwitch isOn]
                                                      vc:self];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        
        NSString *languageSortCode = [mSession convertLanguageNameToSortCode: selection.firstObject];
        NSInteger languageIndexCode = [[mSession convertLanguageNameToLanguageKeyCode: selection.firstObject] intValue];
        
        if ([kAvailableLanguages objectAtIndex: languageIndexCode])
        {
//            [self.btnLanguage setTitle: languageSortCode forState:UIControlStateNormal];
            if ([languageSortCode isEqualToString: [kThai uppercaseString]])
                [self.btnLanguage setTitle: @"ภาษาไทย" forState:UIControlStateNormal];
            else if (languageIndexCode == [kAvailableLanguages indexOfObject: kChineseT_HK])
                [self.btnLanguage setTitle: @"中文" forState:UIControlStateNormal];
            else if (languageIndexCode == [kAvailableLanguages indexOfObject: kFilipino])
                [self.btnLanguage setTitle: @"PH" forState:UIControlStateNormal];
            else
                [self.btnLanguage setTitle: languageSortCode  forState: UIControlStateNormal];
            
            [[LocalizationManager sharedInstance] setLanguage: [kAvailableLanguages objectAtIndex: languageIndexCode]];
            [mSession configureShortcutItems:YES];
            
            //TODO: Change to Saudi or whatever id as Right To Left
            if ([[kAvailableLanguages objectAtIndex: languageIndexCode] isEqualToString: kArabic] ||
                [[kAvailableLanguages objectAtIndex: languageIndexCode] isEqualToString: kUrdu])
            {
                if (!(kIsRightToLeft)) {
                    [UIView.appearance setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
                    [mSession loadSettingsView];
                    
                }
            } else {
                if (kIsRightToLeft) {
                    [UIView.appearance setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
                    [mSession loadSettingsView];
                }
            }
        }
        //update language code here
        [[WebServiceManager sharedInstance] updateLanguage: @(languageIndexCode).stringValue vc: nil];
    }
}
-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_UPDATEPUSH:
            
            break;
        case kWEBSERVICE_FETCHPUSH:
            
            [self.pushNotificationSwitch setOn: [[[response getGenericResponse] objectForKey: @"PushSatus"] boolValue]];
            break;
        case kWEBSERVICE_STATE:
            SET_LOADSTATE([[response getGenericResponse] objectForKey: @"StateDetailsList"]);
            
            break;
        case kWEBSERVICE_UPDATELANGUAGE:
            break;
            
        case kWEBSERVICE_LOGOUT:
            SET_ISLOCATIONDETERMINED(NO);
            [[NSUserDefaults standardUserDefaults] synchronize];
            [mSession reset];
            [mSession loadLoginView];
            break;
        default:
            break;
    }
}

- (IBAction)refreshDataPressed:(id)sender {
    if (IS_STAGING_VERSION)
    {
        SET_WALKTHROUGH_TABLE([[NSMutableDictionary alloc] init]);
    }
    
    [mLookupManager reloadLookup];
    [[WebServiceManager sharedInstance] loadState: [[mSession lookupTable] objectForKey: @"LookUpVersion"]
                                      countryCode: GET_COUNTRY_CODE
                                               vc: self];
//    if (GET_CONSUMER_TNC)
//        [[WebServiceManager sharedInstance] requestConsumerTNC: [GET_CONSUMER_TNC objectForKey: @"Version"]
//                                                          view:nil];
//    else
//        [[WebServiceManager sharedInstance] requestConsumerTNC: @""
//                                                          view:nil];
    
    if (GET_WORKSHOP_TNC)
        [[WebServiceManager sharedInstance] requestWorkshopTNC: [GET_WORKSHOP_TNC objectForKey: @"Version"]
                                                          view:nil];
    else
        [[WebServiceManager sharedInstance] requestWorkshopTNC: @""
                                                          view:nil];
    
    if (GET_DSR_TNC)
        [[WebServiceManager sharedInstance] requestDSRTNC: [GET_DSR_TNC objectForKey: @"Version"]
                                                     view:nil];
    else
        [[WebServiceManager sharedInstance] requestDSRTNC: @""
                                                     view:nil];
}

- (IBAction)logoutPressed:(id)sender {
    [[WebServiceManager sharedInstance] logout: self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)switchPressed:(id)sender {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
