//
//  InviteTradeViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 5/10/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "InviteTradeViewController.h"

@interface InviteTradeViewController ()
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnExisting;
//@property (weak, nonatomic) IBOutlet UIButton *btnCountryCode;
@property (weak, nonatomic) IBOutlet UITextField *txtMobNum;
@property (weak, nonatomic) IBOutlet UITextField *txtWorkshopName;

@end

enum kPopupSelection_Type
{
//    kPopupSelection_Country,
    kPopupSelection_Source,
};

@implementation InviteTradeViewController
@synthesize listView; //must synthesize list view constraints to work

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_SIDEMENU_INVITETRADE) subtitle: @""];   //lokalised
 
    [self setupInterface];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];

    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_SIDEMENU_INVITETRADE) screenClass:nil];
}

-(void) setupInterface
{
    self.txtMobNum.placeholder = LOCALIZATION(C_PROFILE_MOBILENUM);         //lokalised
    self.txtMobNum.font = FONT_B1;
    
    self.txtWorkshopName.font = self.txtMobNum.font;
    self.txtWorkshopName.placeholder = LOCALIZATION(C_PROFILE_COMPANYNAME); //lokalised
    
//    self.btnCountryCode.titleLabel.font = self.txtMobNum.font;
    
    self.btnSubmit.titleLabel.font = FONT_BUTTON;
    [self.btnSubmit setTitle:LOCALIZATION(C_FORM_SUBMIT) forState:UIControlStateNormal]; //lokalised
    [Helper setCustomFontButtonContentModes: self.btnSubmit];
    self.btnSubmit.backgroundColor = COLOUR_RED;
    
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_DROPDOWN_COUNTRY) list: [mSession getCountryDialingCodes] selectionType:ListSelectionTypeSingle previouslySelected: nil];       //lokalised
    listView.delegate = self;
    
    
    
    self.btnExisting.titleLabel.font = self.txtMobNum.font;
    [self.btnExisting setTitle:LOCALIZATION(C_DROPDOWN_COMPANYSOURCE) forState:UIControlStateNormal];       //lokalised
    [Helper setCustomFontButtonContentModes: self.btnExisting];
    
    NSString *firstCountryDialCode = [[mSession getCountryDialingCodes] objectAtIndex: 0];
    NSString *firstCountryNameString = [mSession convertDialCodeToCountryName: firstCountryDialCode];
//    [self.btnCountryCode setTitle: firstCountryNameString forState: UIControlStateNormal];
    
    if (kIsRightToLeft) {
        [self.txtMobNum setTextAlignment:NSTextAlignmentRight];
        [self.txtWorkshopName setTextAlignment:NSTextAlignmentRight];
        [self.btnExisting setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
//        [self.btnExisting setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 15)];
    } else {
        [self.txtMobNum setTextAlignment:NSTextAlignmentLeft];
        [self.txtWorkshopName setTextAlignment:NSTextAlignmentLeft];
        [self.btnExisting setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
//        [self.btnExisting setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];
    }
}

- (IBAction)existingWorkshopPressed:(UIButton *)sender {
    listView.tag = kPopupSelection_Source;
    [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_COMPANYSOURCE) list: [mSession getCompanySource] hasSearchField: NO];        //lokalised
    
    [self popUpView];
}

//- (IBAction)countryCodePressed:(UIButton *)sender {
//    listView.tag = kPopupSelection_Country;
//    [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_COUNTRY) list: [mSession getCountryDialingCodes]];
//    
//    [self popUpView];
//    
//}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        switch (listView.tag)
        {
//            case kPopupSelection_Country:
//            {
//                NSString *countryName = [mSession convertDialCodeToCountryName: selection.firstObject];
//                [self.btnCountryCode setTitle: countryName forState:UIControlStateNormal];
//            }
//                break;
                
            case kPopupSelection_Source:
            {
                [self.btnExisting setTitle: selection.firstObject forState:UIControlStateNormal];
            }
                break;
        }
    }
}

- (IBAction)submitPressed:(id)sender {
    [[WebServiceManager sharedInstance] inviteTrade: self.txtWorkshopName.text
                                       mobileNumber:self.txtMobNum.text
                                      companySource: [mSession convertToCompanySourceKeyCode:self.btnExisting.titleLabel.text]
//                                      companySource:self.btnExisting.titleLabel.text
                                                 vc:self];
}
-(void)processCompleted:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress:self.view];
    
    [mAlert showSuccessAlertWithMessage: [response getResponseMessage]
                           onCompletion:^(BOOL finished) {
                           }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
