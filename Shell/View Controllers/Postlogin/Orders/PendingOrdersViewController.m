//
//  PendingOrdersViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 22/1/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PendingOrdersViewController.h"
#import "PendingOrderTableViewCell.h"

#define CELL_HEIGHT 80

@interface PendingOrdersViewController ()<UITableViewDataSource,UITableViewDelegate,WebServiceManagerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSMutableArray *pendingArray;
@end

@implementation PendingOrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self  setupInterface];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_MANAGEORDER),LOCALIZATION_EN(C_MANAGEORDER_PENDING)] screenClass:nil];
}

-(void)viewDidAppear:(BOOL)animated{
    [[WebServiceManager sharedInstance]fetchPendingOrder:self];
}

-(void)setupInterface
{
    [self.tableView registerNib:[UINib nibWithNibName:@"PendingOrderTableViewCell" bundle:nil] forCellReuseIdentifier:@"PendingOrderTableViewCell"];
    self.pendingArray = [[NSMutableArray alloc]init];
    self.tableView.tableFooterView = [UITableView new];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.pendingArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PendingOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PendingOrderTableViewCell"];
    NSDictionary *pendingDetail = [self.pendingArray objectAtIndex:indexPath.row];
    [cell setupInterface:pendingDetail];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIImageView *imgArrow = [cell viewWithTag:2];
    if (kIsRightToLeft) {
        imgArrow.image = [UIImage imageNamed:@"icon-arrsingleL.png"];
    } else {
        imgArrow.image = [UIImage imageNamed:@"icon-arrsingleR.png"];
    }
    
    return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *orderID = [[self.pendingArray objectAtIndex:indexPath.row] objectForKey:@"OrderID"];
    [mSession pushOrderDetail:self.navigationController.parentViewController orderID:orderID isShareOrder:YES];
}

- (void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_FETCHPENDINGORDER:{
            if([[response getGenericResponse] isEqual:[NSNull null]] || [[[response getGenericResponse] objectForKey:@"PendingOrder"] isEqual:[NSNull null]])
                return;
            self.pendingArray = [[NSMutableArray alloc]initWithArray:[[response getGenericResponse] objectForKey:@"PendingOrder"]];
            [self.tableView reloadData];
        }
            break;
            
        default:
            break;
    }
}
- (void)processFailed:(WebServiceResponse *)response
{
    
}

@end
