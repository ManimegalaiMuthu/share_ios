//
//  OrderDetailsViewController.h
//  Shell
//
//  Created by Admin on 13/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface OrderDetailsViewController : BaseVC
@property NSString *orderID;
@property BOOL isShareOrder;
@end
