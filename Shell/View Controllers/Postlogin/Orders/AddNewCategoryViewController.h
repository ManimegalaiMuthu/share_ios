//
//  AddNewCategroyViewController.h
//  Shell
//
//  Created by Admin on 13/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface AddNewCategoryViewController : BaseVC
@property NSString *productID;
@property NSString *categoryName;
@property NSArray *mainCategoryArray;
@end
