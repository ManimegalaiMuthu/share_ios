//
//  OrderConfirmationViewController.m
//  Shell
//
//  Created by Admin on 15/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "OrderConfirmationViewController.h"

@interface OrderConfirmationViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblThankyouHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblConfirmMsg;
@property (weak, nonatomic) IBOutlet UIButton *btnViewMyOrders;

@end

@implementation OrderConfirmationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = [[GET_CONFIRMORDERKEYS objectForKey:@"title"] uppercaseString];
    
    self.lblThankyouHeader.font = FONT_H1;
    self.lblConfirmMsg.font = FONT_B2;
//    self.lblThankyouHeader.textColor = COLOUR_RED;
    
    self.lblThankyouHeader.text = [GET_CONFIRMORDERKEYS objectForKey:@"header"];
    self.lblConfirmMsg.text = [GET_CONFIRMORDERKEYS objectForKey:@"subtitle"];
    
    self.btnViewMyOrders.backgroundColor = COLOUR_RED;
    self.btnViewMyOrders.titleLabel.font = FONT_H1;
    [self.btnViewMyOrders setTitle: LOCALIZATION(C_ORDER_VIEWORDERS) forState:UIControlStateNormal];    //lokalise 21 Jan
    
    SET_CONFIRMORDERKEYS(@{});
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_ORDER_CONFIRMATION_TITLE) screenClass:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)viewMyOrderBtnPressed:(id)sender{
    [mSession loadManageOrder];
}


@end
