//
//  ViewOrdersViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 22/1/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "ViewOrdersViewController.h"
#import "MyOrdersTableViewCell.h"
#import "PopupInfoTableViewController.h"

#define CELL_HEIGHT 82
#define DATERANGEVIEWHEIGHT 60

enum kRedemptionHistory_Type
{
    kRedemptionHistory_DateRange = 1,
    kRedemptionHistory_AllStatus,
    kRedemptionHistory_OrderSource,
};


@interface ViewOrdersViewController ()<UITableViewDelegate,UITableViewDataSource,WebServiceManagerDelegate,CommonListDelegate,NativeDatePickerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnDateRange;
@property (weak, nonatomic) IBOutlet UIButton *btnAllStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnStartDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEndDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEnter;
@property (weak, nonatomic) IBOutlet UIView *dateRangeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateRangeViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;

@property (weak, nonatomic) IBOutlet UIView *statusView;

@property (weak, nonatomic) IBOutlet UIView *orderSourceView;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderSource;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orderSourceViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *btnLegend;

@property NSArray *defaultOrderArray;
@property NSMutableArray *myOrdersArray;
@end

@implementation ViewOrdersViewController
@synthesize listView,nativeDatePickerView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.dateRangeViewHeight.constant = 0;
    self.dateRangeView.hidden = YES;
    self.tableView.tableFooterView = [UITableView new];
    
    self.btnAllStatus.superview.layer.borderWidth = 1;
    self.btnAllStatus.superview.layer.borderColor = UIColor.lightGrayColor.CGColor;
    self.btnDateRange.superview.layer.borderWidth = 1;
    self.btnDateRange.superview.layer.borderColor = UIColor.lightGrayColor.CGColor;
    //TODO: find out source of image always being on right of button.
    if (kIsRightToLeft) {
        [self.btnAllStatus setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.btnDateRange setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    } else {
        [self.btnAllStatus setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.btnDateRange setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    }
    
    if(IS_COUNTRY(COUNTRYCODE_INDONESIA)) {
        self.orderSourceView.hidden = NO;
        self.orderSourceViewHeight.constant = 55.0;
        [self.btnOrderSource setUserInteractionEnabled:YES];
        self.btnLegend.hidden = NO;
    } else {
        self.orderSourceView.hidden = YES;
        self.orderSourceViewHeight.constant = 0.0;
        [self.btnOrderSource setUserInteractionEnabled:NO];
        self.btnLegend.hidden = YES;
        [NSLayoutConstraint constraintWithItem:self.statusView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-15].active = YES;
    }
    
    self.defaultOrderArray = [[NSArray alloc] init];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
        [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_MANAGEORDER),LOCALIZATION_EN(C_TITLE_ALLORDERS)] screenClass:nil];
    }else {
        [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_MANAGEORDER),LOCALIZATION_EN(C_ORDER_MYORDER)] screenClass:nil];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [[WebServiceManager sharedInstance] fetchAllOrders:self dict:@{@"OrderDateSearch" : @"1",
                                                                   @"OrderStatus" : @"63",
                                                                   @"StartDate" : @"",
                                                                   @"EndDate": @""
                                                                   }];
    self.btnAllStatus.titleLabel.font = FONT_B1;
    self.btnDateRange.titleLabel.font = FONT_B1;
    
    self.btnStartDate.titleLabel.font = FONT_B1;
    [self.btnStartDate setTitle: LOCALIZATION(C_ORDER_STARTDATE) forState: UIControlStateNormal]; //lokalise 21 Jan
    
    self.btnEndDate.titleLabel.font = FONT_B1;
    [self.btnEndDate setTitle: LOCALIZATION(C_ORDER_ENDDATE) forState: UIControlStateNormal];   //lokalise 21 Jan
    
    self.btnEnter.titleLabel.font = FONT_B1;
    [self.btnEnter setBackgroundColor:COLOUR_RED];
    [self.btnEnter setTitle: LOCALIZATION(C_ORDER_ENTER) forState: UIControlStateNormal];   //lokalise 21 Jan
    
    self.lblTo.text = LOCALIZATION(C_POINTS_TO);
    self.lblTo.font = FONT_B1;
    
    if(IS_COUNTRY(COUNTRYCODE_INDONESIA)) {
        [self.btnOrderSource setTitle:LOCALIZATION(C_MANAGEORDERS_FILTERBYSOURCE_PLACEHOLDER) forState:UIControlStateNormal];
        self.btnOrderSource.titleLabel.font = FONT_B1;
        [self.btnOrderSource setTitleColor:COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    }
    
    NSDictionary *lookupOrderDate = [[[mSession lookupTable] objectForKey: @"OrderDate"] firstObject];
    [self.btnDateRange setTitle:[lookupOrderDate objectForKey: @"KeyValue"] forState:UIControlStateNormal];
    
    NSDictionary *lookupOrderStatus = [[[mSession lookupTable] objectForKey: @"OrderStatus"] firstObject];
    [self.btnAllStatus setTitle:[lookupOrderStatus objectForKey: @"KeyValue"] forState:UIControlStateNormal];
}

-(void)setupInterface:(NSArray *) allOrderArray
{
    self.defaultOrderArray = allOrderArray;
    self.myOrdersArray = [[NSMutableArray alloc]initWithArray:allOrderArray];
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dateRangeBtnPressed:(id)sender {
    listView = [[CASCommonListView alloc]initWithTitle:LOCALIZATION(C_ORDER_DATERANGE) list:[mSession getOrderDate] selectionType:ListSelectionTypeSingle previouslySelected:nil];  //lokalise 21 Jan
    listView.tag = kRedemptionHistory_DateRange;
    listView.delegate = self;
    
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}
- (IBAction)allStatusBtnPressed:(id)sender {
    listView = [[CASCommonListView alloc]initWithTitle:LOCALIZATION(C_STATUS) list:[mSession getOrderStatus] selectionType:ListSelectionTypeSingle previouslySelected:nil];     //lokalise 21 Jan
    listView.tag = kRedemptionHistory_AllStatus;
    listView.delegate = self;
    
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

- (IBAction)startDateBtnPressed:(id)sender {
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];
    
    //date cannot be greater than end date
    NSDate *endMaxDate = [outputFormatter dateFromString: self.btnEndDate.titleLabel.text];
    
    if (endMaxDate){
        self.nativeDatePickerView = [[NativeDatePicker alloc]initWithDatepickerTitle:LOCALIZATION(C_POINTS_STARTDATE) maximumDate:endMaxDate minimumDate:nil inputDateFormatter:outputFormatter outputDateFormatter:outputFormatter];       //lokalise 21 Jan
        self.nativeDatePickerView.delegate = self;
    }
    else{
        self.nativeDatePickerView = [[NativeDatePicker alloc]initWithDatepickerTitle:LOCALIZATION(C_POINTS_STARTDATE) maximumDate:[NSDate date] minimumDate:nil inputDateFormatter:outputFormatter outputDateFormatter:outputFormatter];       //lokalise 21 Jan
        self.nativeDatePickerView.delegate = self;
    }
    self.nativeDatePickerView.tag = 1;
    [self showDatePicker];
}

- (IBAction)endDateBtnPressed:(id)sender {
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];
    
    //date cannot be lesser than start date
    NSDate *startMinDate = [outputFormatter dateFromString: self.btnStartDate.titleLabel.text];
    if (startMinDate){
        self.nativeDatePickerView = [[NativeDatePicker alloc]initWithDatepickerTitle:LOCALIZATION(C_POINTS_ENDDATE) maximumDate:[NSDate date] minimumDate:startMinDate inputDateFormatter:outputFormatter outputDateFormatter:outputFormatter];     //lokalise 21 Jan
        self.nativeDatePickerView.delegate = self;
        
    }else{
        self.nativeDatePickerView = [[NativeDatePicker alloc]initWithDatepickerTitle:LOCALIZATION(C_POINTS_ENDDATE) maximumDate:[NSDate date] minimumDate:nil inputDateFormatter:outputFormatter outputDateFormatter:outputFormatter];      //lokalise 21 Jan
        self.nativeDatePickerView.delegate = self;
    }
    self.nativeDatePickerView.tag = 2;
    [self showDatePicker];
}

-(void)showDatePicker{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: nativeDatePickerView];
    nativeDatePickerView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[nativeDatePickerView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (nativeDatePickerView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[nativeDatePickerView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (nativeDatePickerView)]];
}

-(void)getDateAsString:(NSString *)dateString
{
    switch (nativeDatePickerView.tag) {
        case 1:{
            [self.btnStartDate setTitle:dateString forState:UIControlStateNormal];
        }
            break;
        case 2:{
            [self.btnEndDate setTitle:dateString forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
}

- (IBAction)enterBtnPressed:(id)sender {
    NSString *orderStatusKeycode = [mSession convertToOrderStatusKeyCode:self.btnAllStatus.titleLabel.text];
    NSString *orderDateKeycode = [mSession convertToOrderDateKeyCode:self.btnDateRange.titleLabel.text];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];
    
    NSDate *startDate = [outputFormatter dateFromString:self.btnStartDate.titleLabel.text];
    NSDate *endDate = [outputFormatter dateFromString:self.btnEndDate.titleLabel.text];
    
    
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat:@"dd MMM yyyy"];
    [inputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    [inputFormatter setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierGregorian]];

    NSString *startD = (startDate == nil)?@"":[inputFormatter stringFromDate: startDate];
    NSString *endD = (endDate == nil)?@"":[inputFormatter stringFromDate: endDate];
    
//    NSString *startD = (startDate == nil)?@"":self.btnStartDate.titleLabel.text;
//    NSString *endD = (endDate == nil)?@"":self.btnEndDate.titleLabel.text;
    
    [self getSubmitDict:orderStatusKeycode orderDate:orderDateKeycode startDate:startD endDate:endD];
}

- (IBAction)orderSourceBtnPressed:(UIButton *)sender {
    listView = [[CASCommonListView alloc]initWithTitle:LOCALIZATION(C_POPUP_ORDERSOURCE)
                                                  list:@[ LOCALIZATION(C_KEYWORD_ALL),
                        LOCALIZATION(C_MANAGEORDERS_ORDERSOURCE_SHARE),
                        LOCALIZATION(C_MANAGEORDERS_ORDERSOURCE_OTHER)]
                    selectionType:ListSelectionTypeSingle previouslySelected:nil];
    listView.tag = kRedemptionHistory_OrderSource;
    listView.delegate = self;
       
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings (listView)]];
}

- (IBAction)legendPressed:(UIButton *)sender {
    [self.view endEditing: YES];
       
    PopupInfoTableViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPINFOTABLEVIEW];
       
    [self popUpViewWithPopupVC:popupVc];
       
    [popupVc initOrdersSadixLegendsPopup];
}

-(void)dropDownSelection:(NSArray *)selection
{
    switch (listView.tag) {
        case kRedemptionHistory_DateRange:{
            if (selection.count > 0) {
                [self.btnDateRange setTitle:selection.firstObject forState:UIControlStateNormal];
                
                NSString *orderStatusKeycode = [mSession convertToOrderStatusKeyCode:self.btnAllStatus.titleLabel.text];
                NSString *orderDateKeycode = [mSession convertToOrderDateKeyCode:selection.firstObject];
                
                if([orderDateKeycode isEqualToString:@"2"]){
                    self.dateRangeViewHeight.constant = DATERANGEVIEWHEIGHT;
                    self.dateRangeView.hidden = NO;
                }else{
                    self.dateRangeViewHeight.constant = 0;
                    self.dateRangeView.hidden = YES;
                    [self getSubmitDict:orderStatusKeycode orderDate:orderDateKeycode startDate:@"" endDate:@""];
                }
            }
        }
            break;
        case kRedemptionHistory_AllStatus:{
            if (selection.count > 0) {
                [self.btnAllStatus setTitle:selection.firstObject forState:UIControlStateNormal];
                NSString *orderStatusKeycode = [mSession convertToOrderStatusKeyCode:selection.firstObject];
                NSString *orderDateKeycode = [mSession convertToOrderDateKeyCode:self.btnDateRange.titleLabel.text];
                [self getSubmitDict:orderStatusKeycode orderDate:orderDateKeycode startDate:@"" endDate:@""];
                
            }
        }
            break;
        case kRedemptionHistory_OrderSource: {
            if(selection.count > 0) {
                [self.btnOrderSource setTitle:selection.firstObject forState:UIControlStateNormal];
                [self.myOrdersArray removeAllObjects];
                if([selection.firstObject isEqualToString:LOCALIZATION(C_KEYWORD_ALL)]) {
                    self.myOrdersArray = [NSMutableArray arrayWithArray:self.defaultOrderArray];
                } else {
                    NSString *orderSourceSelected = @"1";
                    if([selection.firstObject isEqualToString:LOCALIZATION(C_MANAGEORDERS_ORDERSOURCE_OTHER)]) {
                        orderSourceSelected = @"2";
                    }
                    NSString *orderSource;
                    for(NSDictionary *dict in self.defaultOrderArray) {
                        orderSource = [NSString stringWithFormat:@"%@",[dict objectForKey:@"OrderSource"]];
                        if([orderSource isEqualToString:orderSourceSelected]) {
                            [self.myOrdersArray addObject:dict];
                        }
                    }
                }
                [self.tableView reloadData];
            }
        }
            break;
        default:
            break;
    }

}

-(void)getSubmitDict :(NSString *)orderStatus orderDate:(NSString *)orderDate startDate:(NSString *)startDate endDate:(NSString *)endDate
{
    NSDictionary *submitDict = @{@"OrderStatus" : orderStatus,
                                 @"OrderDate"   : orderDate,
                                 @"StartDate"   : startDate,
                                 @"EndDate"     : endDate
                                 };
    [[WebServiceManager sharedInstance] fetchAllOrders:self dict:submitDict];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.myOrdersArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyOrdersTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"MyOrdersCell"];
    NSDictionary *orderDict = [self.myOrdersArray objectAtIndex:indexPath.row];
    [cell setupInterface:orderDict];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIImageView *imgArrow = [cell viewWithTag:5];
    if (kIsRightToLeft) {
        imgArrow.image = [UIImage imageNamed:@"icon-arrsingleL.png"];
    } else {
        imgArrow.image = [UIImage imageNamed:@"icon-arrsingleR.png"];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *orderID = [[self.myOrdersArray objectAtIndex:indexPath.row] objectForKey:@"OrderID"];
    if(IS_COUNTRY(COUNTRYCODE_INDONESIA)) {
        NSString *orderSourceString = [NSString stringWithFormat:@"%@",[[self.myOrdersArray objectAtIndex:indexPath.row] objectForKey:@"OrderSource"]];
        if([orderSourceString isEqualToString:@"1"]) {
            [mSession pushOrderDetail:self.navigationController.parentViewController orderID:orderID isShareOrder:YES];
        } else {
            [mSession pushOrderDetail:self.navigationController.parentViewController orderID:orderID isShareOrder:NO];
        }
    } else {
        [mSession pushOrderDetail:self.navigationController.parentViewController orderID:orderID isShareOrder:YES];
    }
}

- (void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_ALLORDERS:{
            if([[response getGenericResponse] isEqual:[NSNull null]] || [[[response getGenericResponse] objectForKey:@"AllOrder"] isEqual:[NSNull null]])
                return;
            
            [self setupInterface:[[response getGenericResponse] objectForKey:@"AllOrder"]];
        }
            break;
            
        default:
            break;
    }
}

- (void)processFailed:(WebServiceResponse *)response
{
    
}

@end
