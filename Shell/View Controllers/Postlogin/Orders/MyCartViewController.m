//
//  MyCartViewController.m
//  Shell
//
//  Created by Admin on 13/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "MyCartViewController.h"
#import "MyCartTableViewCell.h"
#import "ManageOrdersViewController.h"

#define CELL_HEIGH 180
@interface MyCartViewController ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,WebServiceManagerDelegate>
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lblRemarksHeader;
@property (weak, nonatomic) IBOutlet UITextView *remarksTextView;
@property (weak, nonatomic) IBOutlet UIView *lowerView;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderTotalHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblDisclaimer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeight;
@property (weak, nonatomic) IBOutlet UIButton *btnPlaceOrder;
@property (weak, nonatomic) IBOutlet UILabel *lblTextviewPlaceholder;
@property (weak, nonatomic) IBOutlet UIView *emptyListView;
@property (weak, nonatomic) IBOutlet UILabel *lblEmptyViewHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnEmptyViewAddItems;


@property BOOL isFirst;
@property NSMutableArray *myCartArray;
@end

@implementation MyCartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_ORDER_MYCART) subtitle: @""];      //lokalise 21 Jan
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orderTotal)
                                                 name:@"OrderCart"
                                               object:nil];
    self.isFirst = YES;
    [self setupInterface];
    
    if (kIsRightToLeft) {
        [self.btnBack setImage: [UIImage imageNamed:@"icon-arrsingleR.png"] forState: UIControlStateNormal];
    } else {
        [self.btnBack setImage: [UIImage imageNamed: @"icon-arrsingleL.png"] forState: UIControlStateNormal];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_ORDER_MYCART) screenClass:nil];
}

-(void)setupInterface
{
    [self.tableView registerNib:[UINib nibWithNibName:@"MyCartTableViewCell" bundle:nil] forCellReuseIdentifier:@"MyCartTableViewCell"];
    
    self.lblRemarksHeader.font = FONT_H1;
    self.remarksTextView.font = FONT_B1;
    self.lblTextviewPlaceholder.font = self.remarksTextView.font;
    self.lblTextviewPlaceholder.textColor = COLOUR_LIGHTGREY;
    
    self.lblRemarksHeader.text = LOCALIZATION(C_ORDER_REMARKS);             //lokalise 21 Jan
    self.lblTextviewPlaceholder.text = LOCALIZATION(C_ORDER_ENTERREMARKS);  //lokalise 21 Jan
    self.lowerView.backgroundColor = COLOUR_VERYPALEGREY;
    
    self.lblOrderTotalHeader.font = FONT_H1;
    self.lblOrderTotal.font = self.lblOrderTotalHeader.font;
    self.lblOrderTotal.textColor = COLOUR_RED;
    self.lblDisclaimer.font = FONT_B2;
    
    self.lblOrderTotalHeader.text = LOCALIZATION(C_ORDER_TOTAL);        //lokalise 21 Jan
    self.lblDisclaimer.text = [mSession getDisclaimerMsgForMyCart];
    
    self.btnPlaceOrder.backgroundColor = COLOUR_RED;
    self.btnPlaceOrder.titleLabel.font = FONT_H1;
    [self.btnPlaceOrder setTitle:LOCALIZATION(C_ORDER_PLACEORDER) forState:UIControlStateNormal];       //lokalise 21 Jan
    
    
    self.lblEmptyViewHeader.font = FONT_H1;
    self.lblEmptyViewHeader.text = LOCALIZATION(C_ORDER_CARTEMPTY);         //lokalise 21 Jan
    
    self.btnEmptyViewAddItems.titleLabel.font = FONT_H1;
    self.btnEmptyViewAddItems.backgroundColor = COLOUR_RED;
    [self.btnEmptyViewAddItems setTitle:LOCALIZATION(C_ORDER_STARTADDINGITEM) forState:UIControlStateNormal];   //lokalise 21 Jan
    
    
    if (kIsRightToLeft) {
        [self.lblRemarksHeader setTextAlignment:NSTextAlignmentRight];
        [self.remarksTextView setTextAlignment:NSTextAlignmentRight];
    } else {
        [self.lblRemarksHeader setTextAlignment:NSTextAlignmentLeft];
        [self.remarksTextView setTextAlignment:NSTextAlignmentLeft];
    }
    
    self.remarksTextView.delegate = self;
    
    [self orderTotal];
}

-(void)orderTotal{
    NSDictionary *dict = GET_ORDERCART;    
    if([dict count] >0){
        double quantity = 0;
        double totalPrice = 0.0;
        for(int i  = 0;i < [dict allKeys].count; i++){
            double counter = [[[dict objectForKey:[[dict allKeys] objectAtIndex:i]] objectForKey:@"quantity"] doubleValue];
            quantity += counter;
            totalPrice += ([[[dict objectForKey:[[dict allKeys] objectAtIndex:i]] objectForKey:@"value"] doubleValue]);
            
        }
        self.lblOrderTotal.text = [NSString stringWithFormat:@"%.2f",totalPrice];
        self.emptyListView.hidden = YES;
    }else{
        self.lblOrderTotal.text = @"0.00";
        self.emptyListView.hidden = NO;
    }
    
    NSMutableArray *allKeysArray = [[NSMutableArray alloc]initWithArray:[GET_ORDERCART allKeys]];
    NSMutableArray *holderArray = [[NSMutableArray alloc]init];
    for(int i = 0;i<self.myCartArray.count;i++){
        if([allKeysArray containsObject:[self.myCartArray objectAtIndex:i]]){
            [holderArray addObject:[self.myCartArray objectAtIndex:i]];
        }
    }
    
    if(self.isFirst){
        self.myCartArray = [[NSMutableArray alloc]initWithArray:[GET_ORDERCART allKeys]];
        self.isFirst = NO;
    }else{
        self.myCartArray = [[NSMutableArray alloc]initWithArray:holderArray];
    }
    self.tableHeight.constant = CELL_HEIGH*self.myCartArray.count;
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.lblTextviewPlaceholder.hidden = YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if([textView.text isEqualToString:@""]){
        self.lblTextviewPlaceholder.hidden = NO;
    }else{
        self.lblTextviewPlaceholder.hidden = YES;
    }
}

- (IBAction)backBtnPressed:(id)sender {
    [self popSelf];
}

- (IBAction)addItemsBtnPressed:(id)sender {
    
    [(ManageOrdersViewController*)[[self.navigationController childViewControllers] firstObject] goToAddNewPage];
    
    [self popToIndex:0];
}

- (IBAction)placeOrderBtnPressed:(id)sender {
    NSLog(@"get order cart ---- %@",GET_ORDERCART);
    NSMutableArray *productList = [[NSMutableArray alloc]init];
    
    for(int i = 0;i<[GET_ORDERCART allKeys].count;i++){
        NSDictionary *dict = [GET_ORDERCART objectForKey:[[GET_ORDERCART allKeys]objectAtIndex:i]];
        NSDictionary *addDict = @{@"ProductID" : [dict objectForKey:@"productID"],
                                  @"Quantity" : [dict objectForKey:@"quantity"],
                                  @"UnitPrice" : [dict objectForKey:@"unitprice"],
                                  @"SubTotal" : [dict objectForKey:@"value"],
                                  };
        [productList addObject:addDict];
    }
    NSDictionary * submitDict = @{@"ProdList" :productList,
                                  @"Remarks" : (self.remarksTextView.text == nil)?@"":self.remarksTextView.text
                                  };
    [[WebServiceManager sharedInstance]addNewOrder:self submitDict:submitDict];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.myCartArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyCartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCartTableViewCell"];
    NSDictionary *cartItem = [GET_ORDERCART objectForKey:[self.myCartArray objectAtIndex:indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setupInterface: cartItem];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGH;
}

- (void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_ADDNEWORDERS:{
            SET_ORDERCART(@{});
            
            NSDictionary *confirmDict =@{@"title" : LOCALIZATION(C_ORDER_CONFIRMATION_TITLE),       //lokalise 21 Jan
                                         @"header" : [[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Header"],
                                         @"subtitle" : [[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Message"]
                                         };
            
            SET_CONFIRMORDERKEYS(confirmDict);
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderCart" object:nil];
            
            [mSession loadOrderConfirmView];
        }
            break;
            
        default:
            break;
    }
}

- (void)processFailed:(WebServiceResponse *)response
{
    
}

@end
