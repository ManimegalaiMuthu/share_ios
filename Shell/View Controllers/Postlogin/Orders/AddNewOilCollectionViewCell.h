//
//  AddNewOilCollectionViewCell.h
//  Shell
//
//  Created by Admin on 13/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddNewOilCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
