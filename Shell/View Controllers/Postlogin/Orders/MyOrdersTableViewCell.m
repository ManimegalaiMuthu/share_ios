//
//  MyOrdersTableViewCell.m
//  Shell
//
//  Created by Admin on 13/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "MyOrdersTableViewCell.h"
#import "Constants.h"
#import "LocalizationManager.h"

@implementation MyOrdersTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lblOrderID.font = FONT_H1;
    self.lblDate.font = FONT_B2;
    self.lblOrderSum.font = FONT_B2;
    self.lblStatus.font = FONT_H1;
    
    if(IS_COUNTRY(COUNTRYCODE_INDONESIA)) {
        self.sadixView.hidden = NO;
        self.sadixView_Width.constant = 42.0;
        [NSLayoutConstraint constraintWithItem:self.lblStatus attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:75.0].active = YES;
    } else {
        self.sadixView.hidden = YES;
        self.sadixView_Width.constant = 0.0;
        [NSLayoutConstraint constraintWithItem:self.lblStatus attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:82.0].active = YES;
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setupInterface:(NSDictionary *)dict
{
    self.lblOrderID.text = [NSString stringWithFormat:@"%@: %@",LOCALIZATION(C_ORDER_ORDERID),[dict objectForKey:@"OrderID"]];      //lokalise 21 Jan
    self.lblDate.text = [NSString stringWithFormat:@"%@: %@",LOCALIZATION(C_ORDER_PLACEDON),[dict objectForKey:@"OrderDate"]];      //lokalise 21 Jan
    
    if (GET_PROFILETYPE == kPROFILETYPE_DSR){
        self.lblOrderSum.text = [NSString stringWithFormat:@"%@: %@",LOCALIZATION(C_ORDER_ORDEREDBY),[dict objectForKey:@"OrderBy"]];   //lokalise 21 Jan
        self.lblOrderSum.hidden = NO;
    }else{
        self.lblOrderSum.hidden = YES;
//        self.lblOrderSum.text = [NSString stringWithFormat:@"%@: %.2f",LOCALIZATION(@"Order Total"),[[dict objectForKey:@"TotalOrder"] doubleValue]];
    }
    
    NSInteger statusType = [[dict objectForKey:@"StatusCode"] integerValue];
    switch (statusType) {
        case kOrderStatus_Pending:{
            self.lblStatus.backgroundColor = COLOUR_YELLOW;
            self.lblStatus.textColor = COLOUR_VERYDARKGREY;
        }
            break;
        case kOrderStatus_Approved:{
            self.lblStatus.backgroundColor = COLOUR_DARKBLUE;
            self.lblStatus.textColor = COLOUR_WHITE;
        }
            break;
        case kOrderStatus_Completed:{
            self.lblStatus.backgroundColor = COLOUR_DARKGREEN;
            self.lblStatus.textColor = COLOUR_WHITE;
        }
            break;
        case kOrderStatus_Rejected:{
            self.lblStatus.backgroundColor = COLOUR_MIDGREY;
            self.lblStatus.textColor = COLOUR_WHITE;
        }
            break;
        case kOrderStatus_DSM_Rejected:{
            self.lblStatus.backgroundColor = COLOUR_MIDGREY;
            self.lblStatus.textColor = COLOUR_WHITE;
        }
            break;
        case kOrderStatus_Cancel:{
            self.lblStatus.backgroundColor = COLOUR_DARKGREY;
            self.lblStatus.textColor = COLOUR_WHITE;
        }
            break;
            
        default:
            break;
    }
    
    self.lblStatus.text = [dict objectForKey:@"OrderStatus"];
    
    if(IS_COUNTRY(COUNTRYCODE_INDONESIA)) {
        int orderSource = 1;
        if(![[dict objectForKey:@"OrderSource"] isKindOfClass:[NSNull class]] && [dict objectForKey:@"OrderSource"] != nil) {
            orderSource = [[dict objectForKey:@"OrderSource"] intValue];
        }
        if(orderSource == 1) {
            [self.sadixView_Image setImage:[UIImage imageNamed: @"icn_share-orders.png"]];
        } else {
            [self.sadixView_Image setImage:[UIImage imageNamed: @"icn_other-orders.png"]];
        }
    }
}

@end
