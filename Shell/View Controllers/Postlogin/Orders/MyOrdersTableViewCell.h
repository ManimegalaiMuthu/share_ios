//
//  MyOrdersTableViewCell.h
//  Shell
//
//  Created by Admin on 13/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyOrdersTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *sadixView;
@property (weak, nonatomic) IBOutlet UIImageView *sadixView_Image;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sadixView_Width;

@property (weak, nonatomic) IBOutlet UILabel *lblOrderID;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderSum;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
-(void)setupInterface:(NSDictionary *)dict;
@end
