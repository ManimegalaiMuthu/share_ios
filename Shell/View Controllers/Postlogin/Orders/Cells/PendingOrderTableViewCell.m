//
//  PendingOrderTableViewCell.m
//  Shell
//
//  Created by Admin on 15/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PendingOrderTableViewCell.h"
#import "Constants.h"
#import "LocalizationManager.h"

@interface PendingOrderTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *lblOrderID;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderBy;

@end
@implementation PendingOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lblOrderID.font = FONT_H1;
    self.lblDate.font = FONT_B2;
    self.lblOrderBy.font = FONT_B2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setupInterface:(NSDictionary *)dict
{
    self.lblOrderID.text = [NSString stringWithFormat:@"%@ %@", LOCALIZATION(C_ORDER_ORDERID), [dict objectForKey:@"OrderID"]];         //lokalise 21 Jan
    self.lblDate.text = [NSString stringWithFormat: @"%@ %@",LOCALIZATION(C_ORDER_PLACEDON), [dict objectForKey:@"OrderDate"]];         //lokalise 21 Jan
    self.lblOrderBy.text = [NSString stringWithFormat:@"%@: %@", LOCALIZATION(C_ORDER_ORDEREDBY) ,[dict objectForKey:@"OrderBy"]];      //lokalise 21 Jan
}

@end
