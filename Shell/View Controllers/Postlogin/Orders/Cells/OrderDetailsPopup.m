//
//  OrderDetailsPopup.m
//  Shell
//
//  Created by Li Zheng yang on 18/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "OrderDetailsPopup.h"
#import "Constants.h"
#import "LocalizationManager.h"

@interface OrderDetailsPopup()
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtitle;
@property (weak, nonatomic) IBOutlet UIButton *btnNo;
@property (weak, nonatomic) IBOutlet UIButton *btnYes;

@end
@implementation OrderDetailsPopup

- (id)initWithPopup:(NSString *)title subtitle:(NSString *)subtitle{
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OrderDetailsPopup" owner:self options:nil];
    self = nib[0];
    
    self.lblTitle.font = FONT_H1;
    self.lblSubtitle.font = FONT_B2;
    
    self.lblTitle.text = title;
    self.lblSubtitle.text = subtitle;
    
    self.btnYes.titleLabel.font = FONT_H1;
    self.btnNo.titleLabel.font = self.btnYes.titleLabel.font;
    
    self.btnYes.backgroundColor = COLOUR_RED;
    self.btnNo.backgroundColor = COLOUR_YELLOW;
    
    [self.btnNo setTitle:LOCALIZATION(C_FORM_NO) forState:UIControlStateNormal];        //lokalise 21 Jan
    [self.btnYes setTitle:LOCALIZATION(C_FORM_YES) forState:UIControlStateNormal];      //lokalise 21 Jan
    
    return self;
}

- (IBAction)noBtnPressed:(id)sender {
    [self removeFromSuperview];
}

- (IBAction)yesBtnPressed:(id)sender {
    [self.delegate yesBtnPressed];
}

@end
