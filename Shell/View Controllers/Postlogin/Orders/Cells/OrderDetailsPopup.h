//
//  OrderDetailsPopup.h
//  Shell
//
//  Created by Li Zheng yang on 18/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol  OrderDetailsPopupDelegate<NSObject>
-(void)yesBtnPressed;
@end
@interface OrderDetailsPopup : UIView
- (id)initWithPopup:(NSString *)title subtitle:(NSString *)subtitle;
@property id<OrderDetailsPopupDelegate> delegate;
@end
