//
//  PendingOrderTableViewCell.h
//  Shell
//
//  Created by Admin on 15/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PendingOrderTableViewCell : UITableViewCell
-(void)setupInterface:(NSDictionary *)dict;
@end
