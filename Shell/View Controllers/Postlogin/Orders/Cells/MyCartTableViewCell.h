//
//  MyCartTableViewCell.h
//  Shell
//
//  Created by Admin on 15/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MyCartDelegate <NSObject>
-(void)orderAdded;
-(void)orderDeleted;
-(void)orderMinus;
@end

@interface MyCartTableViewCell : UITableViewCell

@property id<MyCartDelegate> delegate;
-(void)setupInterface:(NSDictionary *)dict;
@end
