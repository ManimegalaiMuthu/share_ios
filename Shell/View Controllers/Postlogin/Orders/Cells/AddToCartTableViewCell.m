//
//  AddToCartTableViewCell.m
//  Shell
//
//  Created by Admin on 13/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "AddToCartTableViewCell.h"
#import "Constants.h"
#import "LocalizationManager.h"
#import "UIImageView+WebCache.h"

@interface AddToCartTableViewCell()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imgPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCost;
@property (weak, nonatomic) IBOutlet UITextField *txtCounter;

@property (weak, nonatomic) IBOutlet UIButton *btnMinus;
@property (weak, nonatomic) IBOutlet UIButton *btnPlus;
@property (weak, nonatomic) IBOutlet UIButton *btnAddToCart;

@property double count;
@property NSMutableDictionary *refernceDict;
@end
@implementation AddToCartTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.count = 0.0;
    self.lblTitle.font = FONT_H1;
    self.btnAddToCart.titleLabel.font = self.lblTitle.font;
    self.btnAddToCart.backgroundColor = COLOUR_RED;
    self.lblCost.textColor = COLOUR_RED;
    [self.btnAddToCart setTitle:LOCALIZATION(C_ORDER_ADD) forState:UIControlStateNormal];       //lokalise 21 Jan
    [self.btnAddToCart setTitle:LOCALIZATION(C_FORM_UPDATE) forState:UIControlStateSelected];   //lokalise 21 Jan
    
    self.txtCounter.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

-(void)setupInterface:(NSDictionary *)dict
{
    self.refernceDict = [[NSMutableDictionary alloc]initWithDictionary:dict];
    NSString *urlString = [[dict objectForKey:@"ProductImageUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [self.imgPhoto sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"bottle"] options:SDWebImageRefreshCached];
    [self.imgPhoto sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"bottle"]];
    self.lblTitle.text = [dict objectForKey:@"ProductName"];
    self.lblCost.text = [NSString stringWithFormat:@"%.2f",[[dict objectForKey:@"ProductPrice"] doubleValue]];
    self.txtCounter.text = [NSString stringWithFormat:@"%0.1f",self.count];
    
    if([[dict objectForKey:@"CanAdd"] integerValue] == 0){
        self.btnMinus.hidden = YES;
        self.btnPlus.hidden = YES;
        self.txtCounter.hidden = YES;
        self.btnAddToCart.hidden = YES;
    }else{
        self.btnMinus.hidden = NO;
        self.btnPlus.hidden = NO;
        self.txtCounter.hidden = NO;
        self.btnAddToCart.hidden = NO;
    }
    
    if([[dict objectForKey:@"ShowPrice"] integerValue] == 0){
        self.lblCost.textColor = COLOUR_RED;
        self.lblCost.text = [dict objectForKey:@"Text"];
        self.lblCost.font = FONT_B2;
    }else{
        self.lblCost.text = [NSString stringWithFormat:@"%0.2f",[[dict objectForKey:@"ProductPrice"] doubleValue]];
        self.lblCost.textColor = COLOUR_RED;
        self.lblCost.font = FONT_H1;
    }
}

-(void)setQuantitiy:(double)quantity
{
    self.count = quantity;
    self.txtCounter.text = [NSString stringWithFormat:@"%0.1f",self.count];
    if(quantity >0){
        [self.btnAddToCart setSelected:YES];
    }else{
        [self.btnAddToCart setSelected:NO];
    }
}

- (IBAction)minusBtnPressed:(id)sender {
    if(self.count-0.5 >= 0){
        self.count -= 0.5;
        self.txtCounter.text = [NSString stringWithFormat:@"%0.1f",self.count];
    }
}

- (IBAction)plusBtnPressed:(id)sender {
    self.count +=0.5;
    self.txtCounter.text = [NSString stringWithFormat:@"%0.1f",self.count];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if([textField.text isEqualToString:@""]){
        self.count = 0;
    }else{
        self.count = [textField.text doubleValue];
    }
    self.txtCounter.text = [NSString stringWithFormat:@"%0.1f",self.count];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if([newString doubleValue] < 10000){
        NSArray *sep = [newString componentsSeparatedByString:@"."];
        if([sep count] == 2){
            NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
            if(sepStr.length > 1){
                return NO;
            }else
                return YES;
        }
        return YES;
    }else{
        return NO;
    }
}

- (IBAction)addToCartBtnPressed:(id)sender
{
    if([self.txtCounter isFirstResponder]) {
        [self.txtCounter resignFirstResponder];
    }

    if(self.count>0){
        [self addItemsToCart];
        [self.btnAddToCart setSelected:YES];
    }else{
        
        if(self.btnAddToCart.selected){
            if(GET_ORDERCART != nil && [GET_ORDERCART allKeys].count != 0){
                NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:GET_ORDERCART];
                if([dict objectForKey:[self.refernceDict objectForKey:@"ProductID"]] != nil){
                    [dict removeObjectForKey:[self.refernceDict objectForKey:@"ProductID"]];
                }
                SET_ORDERCART(dict);
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderCart" object:nil];
            }
            [self.btnAddToCart setSelected:NO];
        }else{
            self.count += 1.0;
            self.txtCounter.text = [NSString stringWithFormat:@"%.1f",self.count];
            [self addItemsToCart];
            [self.btnAddToCart setSelected:YES];
        }
    }
}

-(void)addItemsToCart
{
    if(GET_ORDERCART == nil || [GET_ORDERCART allKeys].count == 0){
        NSDictionary *dict = @{[self.refernceDict objectForKey:@"ProductID"] : @{@"quantity" : @(self.count),
                                                                                 @"value" : @([[self.refernceDict objectForKey:@"ProductPrice"] floatValue] *self.count),
                                                                                 @"image" : [self.refernceDict objectForKey:@"ProductImageUrl"],
                                                                                 @"submenu" : [self.refernceDict objectForKey:@"Text"],
                                                                                 @"title" : [self.refernceDict objectForKey:@"ProductName"],
                                                                                 @"productID":[self.refernceDict objectForKey:@"ProductID"],
                                                                                 @"unitprice" : [self.refernceDict objectForKey:@"ProductPrice"]
                                                                                 }};
        SET_ORDERCART(dict);
    }else{
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:GET_ORDERCART];
        [dict setObject:@{@"quantity" : @(self.count),
                          @"value" : @([[self.refernceDict objectForKey:@"ProductPrice"] floatValue] *self.count),
                          @"image" : [self.refernceDict objectForKey:@"ProductImageUrl"],
                          @"submenu" : [self.refernceDict objectForKey:@"Text"],
                          @"title" : [self.refernceDict objectForKey:@"ProductName"],
                          @"productID":[self.refernceDict objectForKey:@"ProductID"],
                          @"unitprice" : [self.refernceDict objectForKey:@"ProductPrice"]
                          } forKey:[self.refernceDict objectForKey:@"ProductID"]];
        SET_ORDERCART(dict);
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderCart" object:nil];
}


@end
