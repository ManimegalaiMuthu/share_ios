//
//  OrderDetailTableViewCell.m
//  Shell
//
//  Created by Admin on 14/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "OrderDetailTableViewCell.h"
#import "Constants.h"
#import "LocalizationManager.h"
#import "UIImageView+WebCache.h"

@interface OrderDetailTableViewCell()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imagePhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSubmenu;
@property (weak, nonatomic) IBOutlet UIView *lowerView;
@property (weak, nonatomic) IBOutlet UILabel *lblQuantityHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblApprovedHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitPriceHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtotalHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtotal;
@property (weak, nonatomic) IBOutlet UITextField *txtApproved;

@property NSMutableDictionary *itemDict;
@property double count;
@property double unitPrice;
@end
@implementation OrderDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lblTitle.font = FONT_H1;
    self.lblSubmenu.font = FONT_B1;
    
    self.lblQuantityHeader.font = FONT_B2;
    self.lblApprovedHeader.font = self.lblQuantityHeader.font;
    self.lblUnitPriceHeader.font = self.lblQuantityHeader.font;
    self.lblSubtotalHeader.font = self.lblQuantityHeader.font;
    
    self.lblQuantity.font = self.lblQuantityHeader.font;
    self.txtApproved.font = self.lblQuantityHeader.font;
    self.lblUnitPrice.font = self.lblQuantityHeader.font;
    self.lblSubtotal.font = self.lblQuantityHeader.font;
    
    self.lblSubtotal.textColor = COLOUR_RED;
    
    self.lblQuantityHeader.text = LOCALIZATION(C_ORDER_QUANTITY);               //lokalise 21 Jan
    self.lblApprovedHeader.text = LOCALIZATION(C_ORDER_APPROVEDQUANTITY);       //lokalise 21 Jan
    self.lblUnitPriceHeader.text = LOCALIZATION(C_ORDER_UNITPRICE);             //lokalise 21 Jan
    self.lblSubtotalHeader.text = LOCALIZATION(C_ORDER_SUBTOTAL);               //lokalise 21 Jan
    
    
    self.txtApproved.delegate = self;
    self.lowerView.backgroundColor = COLOUR_VERYPALEGREY;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    double holder = 0;
    if([textField.text isEqualToString:@""]){
        holder = 0;
    }else{
        holder = [self.txtApproved.text doubleValue];
    }
    
    [self.itemDict setObject:[NSString stringWithFormat:@"%.1f",holder] forKey:@"ApprovedQuantity"];
    [self.itemDict setObject:[NSString stringWithFormat:@"%.2f",self.unitPrice*holder] forKey:@"SubTotal"];
    self.lblSubtotal.text = [NSString stringWithFormat:@"%0.2f",(self.unitPrice*holder)];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ApproveUpdate" object:nil userInfo:self.itemDict];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if([newString doubleValue] < 10000){
        NSArray *sep = [newString componentsSeparatedByString:@"."];
        if([sep count] == 2){
            NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
            if(sepStr.length > 1){
                return NO;
            }else
                return YES;
        }
        return YES;
    }else{
        return NO;
    }
}

-(void)setupInterface:(NSDictionary *)dict
{
//    [self.imagePhoto sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"ProductImageUrl"]]];
    NSString *urlString = [[dict objectForKey:@"ProductImageUrl"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [self.imagePhoto sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"bottle"] options:SDWebImageRefreshCached];
    [self.imagePhoto sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"bottle"]];
    
    self.lblTitle.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"ProductName"]];
    self.lblSubmenu.hidden = YES;
    self.lblQuantity.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"Quantity"]];
    self.txtApproved.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"ApprovedQuantity"]];
    self.lblUnitPrice.text = [NSString stringWithFormat:@"%.2f",[[dict objectForKey:@"UnitPrice"] doubleValue]];
    self.lblSubtotal.text = [NSString stringWithFormat:@"%.2f",[[dict objectForKey:@"SubTotal"] doubleValue]];
    
    self.count = [[NSString stringWithFormat:@"%@",[dict objectForKey:@"Quantity"]] doubleValue];
    self.unitPrice = [[NSString stringWithFormat:@"%@",[dict objectForKey:@"UnitPrice"]] doubleValue];
    self.itemDict = [[NSMutableDictionary alloc]initWithDictionary:dict];
}

-(void)setEditableApprovedQuantity :(BOOL) isEnable{
    if(isEnable){
        self.txtApproved.backgroundColor = COLOUR_WHITE;
    }else{
        self.txtApproved.backgroundColor = [UIColor clearColor];
        self.txtApproved.borderStyle = UITextBorderStyleNone;
    }
    
    self.txtApproved.enabled = isEnable;
}

-(NSString *)getApprovedQuantity
{
    if([self.txtApproved isFirstResponder]) {
        [self.txtApproved resignFirstResponder];
    }

    if([self.txtApproved.text isEqualToString:@""])
        return @"";
    else
        return self.txtApproved.text;
}
@end
