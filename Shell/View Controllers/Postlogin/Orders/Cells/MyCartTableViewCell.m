//
//  MyCartTableViewCell.m
//  Shell
//
//  Created by Admin on 15/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "MyCartTableViewCell.h"
#import "Constants.h"
#import "LocalizationManager.h"
#import "UIImageView+WebCache.h"

@interface MyCartTableViewCell()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imagePhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSubmenu;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIView *lowerView;
@property (weak, nonatomic) IBOutlet UILabel *lblQuantityHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitpriceHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtotalHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnMinus;
@property (weak, nonatomic) IBOutlet UITextField *txtNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnPlus;
@property (weak, nonatomic) IBOutlet UILabel *lblUnitprice;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtotal;

@property double quantity;
@property double unitPrice;
@property NSMutableDictionary *itemDict;
@end
@implementation MyCartTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lblTitle.font = FONT_H1;
    self.lblSubmenu.font = FONT_B1;
    
    self.lblQuantityHeader.font = FONT_B2;
    self.lblUnitpriceHeader.font = self.lblQuantityHeader.font;
    self.lblSubtotalHeader.font = self.lblQuantityHeader.font;
    
    self.lblUnitprice.font = self.lblQuantityHeader.font;
    self.lblSubtotal.font = self.lblQuantityHeader.font;
    self.lblSubtotal.textColor = COLOUR_RED;
    
    self.lowerView.backgroundColor = COLOUR_VERYPALEGREY;
    
    self.quantity = 0.0;
    
    self.lblQuantityHeader.text = LOCALIZATION(C_ORDER_QUANTITY);       //lokalise 21 Jan
    self.lblUnitpriceHeader.text = LOCALIZATION(C_ORDER_UNITPRICE);     //lokalise 21 Jan
    self.lblSubtotalHeader.text = LOCALIZATION(C_ORDER_SUBTOTAL);       //lokalise 21 Jan
    
    if (kIsRightToLeft)
    {
        [self.btnDelete setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    } else {
        [self.btnDelete setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    }
    self.txtNumber.delegate = self;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if([textField.text isEqualToString:@""]){
        self.txtNumber.text = @"0.5";
        self.quantity = 0.5;
    }else{
        self.quantity = [textField.text doubleValue];
        if(self.quantity < 0.5){
            self.txtNumber.text = @"0.5";
            self.quantity = 0.5;
        }
    }
    
    self.txtNumber.text = [NSString stringWithFormat:@"%0.1f",self.quantity];
    double subtotal = self.quantity*self.unitPrice;
    self.lblSubtotal.text = [NSString stringWithFormat:@"$%0.2f",subtotal];
    
    [self.itemDict setObject:@(self.quantity) forKey:@"quantity"];
    [self.itemDict setObject:@(self.unitPrice *self.quantity) forKey:@"value"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:GET_ORDERCART];
    [dict setObject:self.itemDict forKey:[self.itemDict objectForKey:@"productID"]];
    SET_ORDERCART(dict);
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderCart" object:nil];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if([newString doubleValue] < 10000){
        NSArray *sep = [newString componentsSeparatedByString:@"."];
        if([sep count] == 2){
            NSString *sepStr=[NSString stringWithFormat:@"%@",[sep objectAtIndex:1]];
            if(sepStr.length > 1){
                return NO;
            }else
                return YES;
        }
        return YES;
    }else{
        return NO;
    }
}

-(void)setupInterface:(NSDictionary *)dict
{
    self.itemDict = [[NSMutableDictionary alloc] initWithDictionary:dict];
    NSString *urlString = [[dict objectForKey:@"image"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [self.imagePhoto sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"bottle"] options:SDWebImageRefreshCached];
    [self.imagePhoto sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"bottle"]];
    
    self.lblTitle.text = [dict objectForKey:@"title"];
    self.lblSubmenu.text = [dict objectForKey:@"submenu"];
    self.txtNumber.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"quantity"]];
    self.lblUnitprice.text = [NSString stringWithFormat:@"%0.2f",[[dict objectForKey:@"unitprice"] doubleValue]];
    self.lblSubtotal.text = [NSString stringWithFormat:@"%0.2f",[[dict objectForKey:@"value"] doubleValue]];
    
    self.unitPrice = [[dict objectForKey:@"unitprice"] doubleValue];
    self.quantity = [[dict objectForKey:@"quantity"] doubleValue];
    
}

- (IBAction)minusBtnPressed:(id)sender {
    if(self.quantity-0.5 >= 0.5){
        self.quantity -=0.5;
        self.txtNumber.text = [NSString stringWithFormat:@"%0.1f",self.quantity];
        double subtotal = self.quantity*self.unitPrice;
        self.lblSubtotal.text = [NSString stringWithFormat:@"%0.2f",subtotal];
        
        [self.itemDict setObject:@(self.quantity) forKey:@"quantity"];
        [self.itemDict setObject:@(self.unitPrice *self.quantity) forKey:@"value"];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:GET_ORDERCART];
        [dict setObject:self.itemDict forKey:[self.itemDict objectForKey:@"productID"]];
        SET_ORDERCART(dict);
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderCart" object:nil];
    }
}

- (IBAction)plusBtnPressed:(id)sender {
    self.quantity += 0.5;
    self.txtNumber.text = [NSString stringWithFormat:@"%0.1f",self.quantity];
    double subtotal = self.quantity*self.unitPrice;
    self.lblSubtotal.text = [NSString stringWithFormat:@"%0.2f",subtotal];
    
    [self.itemDict setObject:@(self.quantity) forKey:@"quantity"];
    [self.itemDict setObject:@(self.unitPrice *self.quantity) forKey:@"value"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:GET_ORDERCART];
    [dict setObject:self.itemDict forKey:[self.itemDict objectForKey:@"productID"]];
    SET_ORDERCART(dict);
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderCart" object:nil];
}

- (IBAction)deleteBtnPressed:(id)sender {
    if(GET_ORDERCART != nil && [(NSDictionary *) GET_ORDERCART count] != 0){
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:GET_ORDERCART];
        if([dict objectForKey:[self.itemDict objectForKey:@"productID"]] != nil){
            [dict removeObjectForKey:[self.itemDict objectForKey:@"productID"]];
        }
        
        SET_ORDERCART(dict);
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderCart" object:nil];
    }
}

@end
