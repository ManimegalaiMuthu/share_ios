//
//  OrderDetailTableViewCell.h
//  Shell
//
//  Created by Admin on 14/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailTableViewCell : UITableViewCell
-(void)setupInterface:(NSDictionary *)dict;
-(void)setEditableApprovedQuantity :(BOOL) isEnable;
-(NSString *)getApprovedQuantity;
@end
