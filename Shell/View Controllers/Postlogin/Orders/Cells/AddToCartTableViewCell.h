//
//  AddToCartTableViewCell.h
//  Shell
//
//  Created by Admin on 13/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol OrderAddedDelegate <NSObject>
-(void)orderAdded:(NSDictionary *) orderDict;
@end

@interface AddToCartTableViewCell : UITableViewCell
@property id<OrderAddedDelegate> delegate;
-(void)setQuantitiy:(double)quantity;
-(void)setupInterface:(NSDictionary *)dict;
@end
