//
//  AddOrdersViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 22/1/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "AddOrdersViewController.h"
#import "AddNewOilCollectionViewCell.h"
#import "Shell-Swift.h"

#define PADDING 15
#define SPACING 10
#define CARTVIEWHEIGH 80

@interface AddOrdersViewController ()<WebServiceManagerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,FSPagerViewDelegate,FSPagerViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *categoryCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *categoryCollectionCellFlow;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *categoryCollectionHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblCategoryHeader;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cartViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lblCartTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtotal;
@property (weak, nonatomic) IBOutlet UIView *cartView;
@property (weak, nonatomic) IBOutlet UIView *linkbreakView;
@property (weak, nonatomic) IBOutlet UIView *cartViewBackground;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *carousellHeight;
@property (weak, nonatomic) IBOutlet FSPagerView *pagerView;
@property (weak, nonatomic) IBOutlet FSPageControl *pagerController;

@property (weak, nonatomic) IBOutlet UIView *carousellView;
@property NSMutableArray *categoryArray;
@property NSMutableArray *carousellArray;

@property NSTimer *timer;
@end

@implementation AddOrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.lblCategoryHeader.font = FONT_H1;
    self.lblCategoryHeader.text = LOCALIZATION(C_REWARDS_BROWSEBYCATEGORY);           //lokalise in library
    if(kIsRightToLeft) {
        [self.lblCategoryHeader setTextAlignment:NSTextAlignmentRight];
    } else {
        [self.lblCategoryHeader setTextAlignment:NSTextAlignmentLeft];
    }
    
    self.linkbreakView.backgroundColor = COLOUR_YELLOW;
    self.cartViewHeight.constant = 0;
    self.cartView.hidden = YES;
    self.cartViewBackground.backgroundColor = COLOUR_RED;
    
    self.lblSubtotal.font = FONT_B1;
    self.lblCartTitle.font = FONT_H1;
    self.lblQuantity.font = self.lblSubtotal.font;
    self.lblQuantity.backgroundColor = [UIColor colorWithRed:0.52 green:0.07 blue:0.08 alpha:1.0];
    
    [self setupCarouselView:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveOrderCart)
                                                 name:@"OrderCart"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderCart" object:nil];
    [[WebServiceManager sharedInstance]fetchProductCategoryList:self];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_REWARDS_BROWSEBYCATEGORY) screenClass:nil];
}

-(void)setupCarouselView:(BOOL)isEnable{
    [self.pagerView registerClass:[FSPagerViewCell class] forCellWithReuseIdentifier:@"cell"];
    if(isEnable){
        self.carousellArray = [[NSMutableArray alloc]initWithArray:@[@"img_ultra1L",@"img_ultra4L"]];
        self.pagerController.numberOfPages = self.carousellArray.count;
        
        [self.pagerController setStrokeColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.pagerController setStrokeColor:[UIColor lightGrayColor] forState:UIControlStateSelected];
        
        [self.pagerController setFillColor:COLOUR_WHITE forState:UIControlStateNormal];
        [self.pagerController setFillColor:COLOUR_YELLOW forState:UIControlStateSelected];
        
    }else{
        self.carousellView.hidden = YES;
        self.carousellHeight.constant = 0;
        self.carousellArray = [[NSMutableArray alloc]initWithArray:@[]];
    }
}
#pragma carousel view
- (NSInteger)numberOfItemsInPagerView:(FSPagerView * _Nonnull)pagerView SWIFT_WARN_UNUSED_RESULT
{
    return self.carousellArray.count;
}
- (FSPagerViewCell * _Nonnull)pagerView:(FSPagerView * _Nonnull)pagerView cellForItemAtIndex:(NSInteger)index SWIFT_WARN_UNUSED_RESULT
{
    FSPagerViewCell *cell = [pagerView dequeueReusableCellWithReuseIdentifier:@"cell" atIndex:index];
    cell.imageView.image = [UIImage imageNamed:[self.carousellArray objectAtIndex:index]];
    cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
    return cell;
}

- (void)pagerViewWillEndDragging:(FSPagerView *) pagerView targetIndex:(NSInteger)index
{
    self.pagerController.currentPage = index;
}

- (void)pagerView:(FSPagerView *)pagerView willDisplayCell:(FSPagerViewCell *)cell forItemAtIndex:(NSInteger)index
{
    self.pagerController.currentPage = index;
}


-(void)receiveOrderCart
{
//    SET_ORDERCART(@{});
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    NSLog(@"%@",GET_ORDERCART);
    
    NSDictionary *dict = GET_ORDERCART;
    if([dict count] >0){
        self.cartView.hidden = NO;
        double quantity = 0;
        double totalPrice = 0.0;
        for(int i  = 0;i < [dict allKeys].count; i++){
            double counter = [[[dict objectForKey:[[dict allKeys] objectAtIndex:i]] objectForKey:@"quantity"] doubleValue];
            quantity += counter;
            totalPrice += ([[[dict objectForKey:[[dict allKeys] objectAtIndex:i]] objectForKey:@"value"] doubleValue]);
            
        }
        self.lblQuantity.text = [NSString stringWithFormat:@"%0.1f",quantity];
        self.lblCartTitle.text = LOCALIZATION(C_ORDER_VIEWCART);            //lokalise 21 Jan
        self.lblSubtotal.text = [NSString stringWithFormat:@"%.2f",totalPrice];
        self.cartViewHeight.constant = CARTVIEWHEIGH;
    }else{
        self.cartView.hidden = YES;
        self.cartViewHeight.constant = 0;
    }
}

-(void)setupInterface :(NSArray *)responseArray
{
    self.categoryArray = [[NSMutableArray alloc]initWithArray:responseArray];
    int width = (SCREEN_WIDTH - PADDING*2 - SPACING)/2;
    self.categoryCollectionCellFlow.itemSize = CGSizeMake(width, width*0.8);
    int row = ceil(self.categoryArray.count/2.0);
    self.categoryCollectionHeight.constant = width*0.8*row + SPACING*(row+1);
    [self.categoryCollectionView reloadData];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.categoryArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AddNewOilCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"categoryCell" forIndexPath:indexPath];
    NSDictionary *category = [self.categoryArray objectAtIndex:indexPath.row];
    NSString *urlString = [[category objectForKey:@"ImageURL"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [cell.imgPhoto sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"bottle"] options:SDWebImageRefreshCached];
    [cell.imgPhoto sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"bottle"]];
    
    cell.lblTitle.text = [category objectForKey:@"CatergoryName"];
    cell.backgroundColor = COLOUR_VERYPALEGREY;
    cell.lblTitle.font = FONT_B1;
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *productID = [[self.categoryArray objectAtIndex:indexPath.row] objectForKey:@"CatergoryID"];
    NSString *categoryName = [[self.categoryArray objectAtIndex:indexPath.row] objectForKey:@"CatergoryName"];
    
    [mSession pushCategoryView:self.navigationController.parentViewController productID:productID categoryName:categoryName categoryArray:self.categoryArray];  //pass the whole category as 
}

- (IBAction)addCartBtnPressed:(id)sender
{
    [mSession pushMyCartView:self.navigationController.parentViewController];
}

- (void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_CATEGORYLIST:{
            if([[response getGenericResponse]isEqual:[NSNull null]] || [[[response getGenericResponse] objectForKey:@"ProdCategory"]isEqual:[NSNull null]])
                return;
            [self setupInterface:[[response getGenericResponse] objectForKey:@"ProdCategory"]];
        }
            
            break;
            
        default:
            break;
    }
}

- (void)processFailed:(WebServiceResponse *)response
{
    
}
@end
