//
//  OrderDetailsViewController.m
//  Shell
//
//  Created by Admin on 13/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "OrderDetailsViewController.h"
#import "OrderDetailTableViewCell.h"

#define CELL_HEIGHT 170

#define TEXTVIEW_HEIGHT 100

@interface OrderDetailsViewController ()<UITableViewDelegate,UITableViewDataSource,WebServiceManagerDelegate,OrderDetailsPopupDelegate>
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderId;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryAddressHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblCancelOn;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderTotal;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblRemarksHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblRemarks;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderRemark;
@property (weak, nonatomic) IBOutlet UILabel *lblRemarkSum;
@property (weak, nonatomic) IBOutlet UILabel *lblRemarkMsg;
@property (weak, nonatomic) IBOutlet UIButton *btnDSRLast;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnDSRHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnAtTheEndHeight;
//@property (weak, nonatomic) IBOutlet UILabel *DSRRemarkHeader;
//@property (weak, nonatomic) IBOutlet UILabel *DSRRemarkMsg;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *DSRRemarkHeight;
//@property (weak, nonatomic) IBOutlet UIView *DSRRemarksView;
@property (weak, nonatomic) IBOutlet UITextView *txtViewRemarks;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtViewRemarksHeight;

@property (weak, nonatomic) IBOutlet UIButton *btnAtTheEnd;
@property NSMutableDictionary *referenceDict;
@property NSMutableDictionary *statusDict;
@property NSMutableArray *cartOrderArray;
@property NSInteger statusType;
@property OrderDetailsPopup *popup;

@property NSInteger isFrom;
@end

@implementation OrderDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_ORDER_ORDERDETAILS) subtitle: @""];        //lokalise 21 Jan
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    self.referenceDict = [[NSMutableDictionary alloc]init];
    [[WebServiceManager sharedInstance]fetchOrderDetails:self orderID:self.orderID];
    
//    self.DSRRemarkHeight.constant = 0;
//    self.DSRRemarksView.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateSum:)
                                                 name:@"ApproveUpdate"
                                               object:nil];
    if (kIsRightToLeft) {
        [self.btnBack setImage: [UIImage imageNamed:@"icon-arrsingleR.png"] forState: UIControlStateNormal];
        [self.lblDeliveryAddress setTextAlignment:NSTextAlignmentRight];
        [self.lblDeliveryAddressHeader setTextAlignment:NSTextAlignmentRight];
        [self.lblDate setTextAlignment:NSTextAlignmentRight];
        [self.lblCancelOn setTextAlignment:NSTextAlignmentRight];
        [self.lblRemarksHeader setTextAlignment:NSTextAlignmentRight];
        [self.lblRemarks setTextAlignment:NSTextAlignmentRight];
        [self.lblHeaderRemark setTextAlignment:NSTextAlignmentRight];
        [self.lblRemarkSum setTextAlignment:NSTextAlignmentRight];
        [self.lblRemarkMsg setTextAlignment:NSTextAlignmentRight];
        
        [self.txtViewRemarks setTextAlignment:NSTextAlignmentRight];
    } else {
        [self.btnBack setImage: [UIImage imageNamed: @"icon-arrsingleL.png"] forState: UIControlStateNormal];
        [self.lblDeliveryAddress setTextAlignment:NSTextAlignmentLeft];
        [self.lblDeliveryAddressHeader setTextAlignment:NSTextAlignmentLeft];
        [self.lblDate setTextAlignment:NSTextAlignmentLeft];
        [self.lblCancelOn setTextAlignment:NSTextAlignmentLeft];
        [self.lblRemarksHeader setTextAlignment:NSTextAlignmentLeft];
        [self.lblRemarks setTextAlignment:NSTextAlignmentLeft];
        [self.lblHeaderRemark setTextAlignment:NSTextAlignmentLeft];
        [self.lblRemarkSum setTextAlignment:NSTextAlignmentLeft];
        [self.lblRemarkMsg setTextAlignment:NSTextAlignmentLeft];
        
        [self.txtViewRemarks setTextAlignment:NSTextAlignmentLeft];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_ORDER_ORDERDETAILS) screenClass:nil];
}

-(void)updateSum:(NSNotification *) notification{
    
    NSDictionary *dict = notification.userInfo;
    double sum = 0.0;
    for (int i= 0; i<self.cartOrderArray.count; i++) {
        NSMutableDictionary *item = [[NSMutableDictionary alloc] initWithDictionary:[self.cartOrderArray objectAtIndex:i]];
        if([[item objectForKey:@"ProductID"] integerValue] == [[dict objectForKey:@"ProductID"] integerValue]){
            sum += [[dict objectForKey:@"SubTotal"] doubleValue];
            [self.cartOrderArray replaceObjectAtIndex:i withObject:dict];
        }else{
            sum += [[item objectForKey:@"SubTotal"] doubleValue];
        }
    }
    self.lblRemarkSum.text = [NSString stringWithFormat:@"%0.2f",(sum)];
}

-(void)setupInterface :(NSDictionary *)responseDict
{
    self.statusDict = [[NSMutableDictionary alloc]initWithDictionary:responseDict];
    self.lblOrderId.font = FONT_H1;
    self.lblDeliveryAddressHeader.font = FONT_B2;
    self.lblDeliveryAddress.font = self.lblDeliveryAddressHeader.font;
    self.lblDate.font = self.lblDeliveryAddressHeader.font;
    self.lblOrderTotal.font = self.lblDeliveryAddressHeader.font;
    self.lblCancelOn.font = self.lblDeliveryAddressHeader.font;
    
    self.lblStatus.font = FONT_H1;
    
    self.lblOrderId.text = [[responseDict objectForKey:@"BasicDetails"] objectForKey:@"OrderID"];
    self.lblDeliveryAddressHeader.text = LOCALIZATION(C_ORDER_ADDRESS);         //lokalise 21 Jan
    
    self.lblDeliveryAddress.text = [[responseDict objectForKey:@"BasicDetails"] objectForKey:@"DeliveryAddress"];
    
    self.lblOrderTotal.text = [NSString stringWithFormat:@"%@ %.2f",LOCALIZATION(C_ORDER_TOTAL),[[[responseDict objectForKey:@"BasicDetails"] objectForKey:@"TotalOrder"] doubleValue]];        //lokalise 21 Jan
    
    _statusType = [[[responseDict objectForKey:@"BasicDetails"] objectForKey:@"StatusCode"] integerValue];
    
    self.txtViewRemarksHeight.constant = 0;
    self.txtViewRemarks.hidden = YES;
    
    switch (_statusType) {
        case kOrderStatus_Pending:{
            self.lblStatus.backgroundColor = COLOUR_YELLOW;
            self.lblStatus.textColor = COLOUR_VERYDARKGREY;
            self.lblStatus.text = [[responseDict objectForKey:@"BasicDetails"] objectForKey:@"OrderStatus"];
            
            self.lblDate.hidden = YES;
            self.lblCancelOn.text = [NSString stringWithFormat:@"%@ : %@",LOCALIZATION(C_ORDER_PLACEDON),[[responseDict objectForKey:@"BasicDetails"] objectForKey:@"OrderDate"]];      //lokalise 21 Jan
            
            if (GET_PROFILETYPE == kPROFILETYPE_DSR){
                [self.btnAtTheEnd setTitle:LOCALIZATION(C_ORDER_APPROVEDORDER) forState:UIControlStateNormal];  //lokalise 21 Jan
                [self.btnDSRLast setTitle:LOCALIZATION(C_ORDER_REJECTORDER) forState:UIControlStateNormal];     //lokalise 21 Jan
                self.btnDSRLast.backgroundColor = COLOUR_YELLOW;
                self.btnDSRLast.titleLabel.font = FONT_H1;
                self.txtViewRemarksHeight.constant = 100;
                self.txtViewRemarks.text = [[responseDict objectForKey:@"BasicDetails"] objectForKey:@"Remarks"];
                self.txtViewRemarks.hidden = NO;
            }else{
                self.btnDSRHeight.constant = 0;
                self.btnDSRLast.hidden = YES;
                
                [self.btnAtTheEnd setTitle: LOCALIZATION(C_ORDER_CANCELORDER) forState:UIControlStateNormal];   //lokalise 21 Jan
            }
        }
            break;
        case kOrderStatus_Approved:{
            self.lblStatus.backgroundColor = COLOUR_DARKBLUE;
            self.lblStatus.textColor = COLOUR_WHITE;
            self.lblStatus.text = [[responseDict objectForKey:@"BasicDetails"] objectForKey:@"OrderStatus"];
            
            self.lblDate.text = [NSString stringWithFormat:@"%@ %@",LOCALIZATION(C_ORDER_PLACEDON),[[responseDict objectForKey:@"BasicDetails"] objectForKey:@"OrderDate"]];    //lokalise 21 Jan
            self.lblCancelOn.text = [NSString stringWithFormat:@"%@ %@",[[responseDict objectForKey:@"BasicDetails"] objectForKey:@"StatusChangeText"],[[responseDict objectForKey:@"BasicDetails"] objectForKey:@"LastModifiedDate"]];
            
            if (GET_PROFILETYPE == kPROFILETYPE_DSR){
                self.btnAtTheEnd.hidden = YES;
                self.btnAtTheEndHeight.constant = 0;
                self.btnDSRHeight.constant = 0;
                self.btnDSRLast.hidden = YES;
            }else{
                self.btnDSRHeight.constant = 0;
                self.btnDSRLast.hidden = YES;
                [self.btnAtTheEnd setTitle:LOCALIZATION(C_ORDER_REORDER) forState:UIControlStateNormal];    //lokalise 21 Jan
            }
        }
            break;
        case kOrderStatus_Completed:{
            self.lblStatus.backgroundColor = COLOUR_DARKGREEN;
            self.lblStatus.textColor = COLOUR_WHITE;
            self.lblStatus.text = [[responseDict objectForKey:@"BasicDetails"] objectForKey:@"OrderStatus"];
            
            self.lblDate.text = [NSString stringWithFormat:@"%@ %@",LOCALIZATION(C_ORDER_PLACEDON),[[responseDict objectForKey:@"BasicDetails"] objectForKey:@"OrderDate"]];    //lokalise 21 Jan
            self.lblCancelOn.text = [NSString stringWithFormat:@"%@ %@",[[responseDict objectForKey:@"BasicDetails"] objectForKey:@"StatusChangeText"],[[responseDict objectForKey:@"BasicDetails"] objectForKey:@"LastModifiedDate"]];
            
            if (GET_PROFILETYPE == kPROFILETYPE_DSR){
                self.btnAtTheEnd.hidden = YES;
                self.btnAtTheEndHeight.constant = 0;
                self.btnDSRHeight.constant = 0;
                self.btnDSRLast.hidden = YES;
                
                
//                self.DSRRemarksView.hidden = NO;
//                self.DSRRemarkHeight.constant = 60;
            }else{
                self.btnDSRHeight.constant = 0;
                self.btnDSRLast.hidden = YES;
                [self.btnAtTheEnd setTitle:LOCALIZATION(C_ORDER_REORDER) forState:UIControlStateNormal];    //lokalise 21 Jan
            }
        }
            break;
        case kOrderStatus_Rejected:{
            self.lblStatus.backgroundColor = COLOUR_MIDGREY;
            self.lblStatus.textColor = COLOUR_WHITE;
            self.lblStatus.text = [[responseDict objectForKey:@"BasicDetails"] objectForKey:@"OrderStatus"];
            
            self.lblDate.text = [NSString stringWithFormat:@"%@ %@",LOCALIZATION(C_ORDER_PLACEDON),[[responseDict objectForKey:@"BasicDetails"] objectForKey:@"OrderDate"]];    //lokalise 21 Jan
            self.lblCancelOn.text = [NSString stringWithFormat:@"%@ %@",[[responseDict objectForKey:@"BasicDetails"] objectForKey:@"StatusChangeText"],[[responseDict objectForKey:@"BasicDetails"] objectForKey:@"LastModifiedDate"]];
            
            if (GET_PROFILETYPE == kPROFILETYPE_DSR){
                self.btnAtTheEnd.hidden = YES;
                self.btnAtTheEndHeight.constant = 0;
                self.btnDSRHeight.constant = 0;
                self.btnDSRLast.hidden = YES;
            }else{
                self.btnDSRHeight.constant = 0;
                self.btnDSRLast.hidden = YES;
                [self.btnAtTheEnd setTitle:LOCALIZATION(C_ORDER_REORDER) forState:UIControlStateNormal];        //lokalise 21 Jan
            }
        }
            break;
        case kOrderStatus_DSM_Rejected:{
            self.lblStatus.backgroundColor = COLOUR_MIDGREY;
            self.lblStatus.textColor = COLOUR_WHITE;
            self.lblStatus.text = [[responseDict objectForKey:@"BasicDetails"] objectForKey:@"OrderStatus"];
            
            self.lblDate.text = [NSString stringWithFormat:@"%@ %@",LOCALIZATION(C_ORDER_PLACEDON),[[responseDict objectForKey:@"BasicDetails"] objectForKey:@"OrderDate"]];    //lokalise 21 Jan
            self.lblCancelOn.text = [NSString stringWithFormat:@"%@ %@",[[responseDict objectForKey:@"BasicDetails"] objectForKey:@"StatusChangeText"],[[responseDict objectForKey:@"BasicDetails"] objectForKey:@"LastModifiedDate"]];
            
            if (GET_PROFILETYPE == kPROFILETYPE_DSR){
                self.btnAtTheEnd.hidden = YES;
                self.btnAtTheEndHeight.constant = 0;
                self.btnDSRHeight.constant = 0;
                self.btnDSRLast.hidden = YES;
            }else{
                self.btnDSRHeight.constant = 0;
                self.btnDSRLast.hidden = YES;
                [self.btnAtTheEnd setTitle:LOCALIZATION(C_ORDER_REORDER) forState:UIControlStateNormal];        //lokalise 21 Jan
            }
        }
            break;
        case kOrderStatus_Cancel:{
            self.lblStatus.backgroundColor = COLOUR_DARKGREY;
            self.lblStatus.textColor = COLOUR_WHITE;
            self.lblStatus.text = [[responseDict objectForKey:@"BasicDetails"] objectForKey:@"OrderStatus"];
            
            self.lblDate.text = [NSString stringWithFormat:@"%@ %@",LOCALIZATION(C_ORDER_PLACEDON),[[responseDict objectForKey:@"BasicDetails"] objectForKey:@"OrderDate"]];        //lokalise 21 Jan
            self.lblCancelOn.text = [NSString stringWithFormat:@"%@ %@",[[responseDict objectForKey:@"BasicDetails"] objectForKey:@"StatusChangeText"],[[responseDict objectForKey:@"BasicDetails"] objectForKey:@"LastModifiedDate"]];
            
            if (GET_PROFILETYPE == kPROFILETYPE_DSR){
                self.btnAtTheEnd.hidden = YES;
                self.btnAtTheEndHeight.constant = 0;
                self.btnDSRHeight.constant = 0;
                self.btnDSRLast.hidden = YES;
            }else{
                self.btnDSRHeight.constant = 0;
                self.btnDSRLast.hidden = YES;
                [self.btnAtTheEnd setTitle:LOCALIZATION(C_ORDER_REORDER) forState:UIControlStateNormal];        //lokalise 21 Jan
            }
        }
            break;
            
        default:
            break;
    }
    
    self.lblRemarksHeader.font = FONT_H1;
    self.lblRemarks.font = FONT_B2;
    self.txtViewRemarks.font = FONT_B2;
    
    self.lblHeaderRemark.font = FONT_H1;
    self.lblRemarkSum.font = FONT_H1;
    self.lblRemarkMsg.font = FONT_B2;
    self.btnAtTheEnd.titleLabel.font = FONT_H1;
    self.btnAtTheEnd.backgroundColor = COLOUR_RED;
    
//    self.DSRRemarkHeader.font = self.lblRemarksHeader.font;
//    self.DSRRemarkMsg.font = self.lblRemarks.font;
//
//    self.DSRRemarkHeader.text = LOCALIZATION(@"Distributor Remarks");
//    self.DSRRemarkMsg.text = [[responseDict objectForKey:@"BasicDetails"] objectForKey:@"DistributorRemarks"];
    
    self.lblRemarkSum.textColor = COLOUR_RED;
    
    self.lblRemarksHeader.text = LOCALIZATION(C_ORDER_REMARKS);     //lokalise 21 Jan
    self.lblRemarks.text = [[responseDict objectForKey:@"BasicDetails"] objectForKey:@"Remarks"];
    self.lblHeaderRemark.text = LOCALIZATION(C_ORDER_TOTAL);        //lokalise 21 Jan
    self.lblRemarkSum.text = [NSString stringWithFormat:@"%.2f",[[[responseDict objectForKey:@"BasicDetails"] objectForKey:@"TotalOrder"] doubleValue]];
    self.lblRemarkMsg.text = [[responseDict objectForKey:@"BasicDetails"] objectForKey:@"DisclaimerText"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OrderDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"OrderDetailTableViewCell"];
    self.tableView.tableFooterView = [UITableView new];
    
    self.cartOrderArray = [[NSMutableArray alloc]initWithArray:([[responseDict objectForKey:@"OrderDetail"] isEqual:[NSNull null]])?@[]:[responseDict objectForKey:@"OrderDetail"]];
    self.tableHeight.constant = self.cartOrderArray.count * CELL_HEIGHT;
    [self.tableView reloadData];
    
    //FOR SADIX ORDER
    if(!self.isShareOrder) {
        self.btnAtTheEnd.hidden = YES;
        self.btnAtTheEndHeight.constant = 0;
        self.btnDSRHeight.constant = 0;
        self.btnDSRLast.hidden = YES;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backBtnPressed:(id)sender {
    [self popSelf];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.cartOrderArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OrderDetailTableViewCell"];
    NSDictionary *dict = [self.cartOrderArray objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setupInterface:dict];
    
    if (GET_PROFILETYPE == kPROFILETYPE_DSR){
        switch (_statusType)
        {
            case kOrderStatus_Pending:
                [cell setEditableApprovedQuantity:YES];
                break;
            default:
                [cell setEditableApprovedQuantity:NO];
                break;
        }
    }
    else{
        [cell setEditableApprovedQuantity:NO];
    }
    
    [self.referenceDict setObject:cell forKey:[NSString stringWithFormat:@"%ld",indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

-(void)popupShow{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: _popup];
    _popup.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_popup]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (_popup)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_popup]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (_popup)]];
}

- (IBAction)cancelbtnPressed:(id)sender {
    if (GET_PROFILETYPE == kPROFILETYPE_DSR){
        NSArray *orderDetails = [self.statusDict objectForKey:@"OrderDetail"];
        NSMutableArray *submitArray = [[NSMutableArray alloc]init];
        for(int i= 0;i < orderDetails.count;i++){
            OrderDetailTableViewCell *cell = [self.referenceDict objectForKey:[NSString stringWithFormat:@"%d",i]];
            NSString *approvedQuantity = [cell getApprovedQuantity];
            
            NSDictionary *dict = @{@"LineNumber" : [[orderDetails objectAtIndex:i] objectForKey:@"LineNumber"],
                                   @"IssuedItemQuantity" : approvedQuantity
                                   };
            [submitArray addObject:dict];
        }
        NSDictionary *submitDict;
        
        if (self.statusType == kOrderStatus_Pending)
        {
            //use textview for remarks
            submitDict = @{@"OrderID" :[[self.statusDict objectForKey:@"BasicDetails"] objectForKey:@"OrderID"],
                           @"Status" : [NSString stringWithFormat:@"%d",kOrderStatus_Approved],
                           @"ProductDetail" : submitArray,
                           @"Remarks" : self.txtViewRemarks.text,
                                         };
        }
        else //approved, reorder?
        {
            submitDict = @{@"OrderID" :[[self.statusDict objectForKey:@"BasicDetails"] objectForKey:@"OrderID"],
                           @"Status" : [NSString stringWithFormat:@"%d",kOrderStatus_Approved],
                           @"ProductDetail" : submitArray,
                           @"Remarks" : [[self.statusDict objectForKey:@"BasicDetails"] objectForKey:@"Remarks"]
                           };
        }
        
        self.isFrom = 1;
        [[WebServiceManager sharedInstance]updateOrderStatus:self submitDict:submitDict];
    }else{
        switch (_statusType) {
            case kOrderStatus_Pending:{
                _popup = [[OrderDetailsPopup alloc]initWithPopup:LOCALIZATION(C_ORDER_CANCELORDERCONFIRMATION) subtitle:LOCALIZATION(C_ORDER_CANCELLATIONWARNING)];     //lokalise 21 Jan
                _popup.delegate = self;
                [self popupShow];
            }
                break;
            default:
                _popup = [[OrderDetailsPopup alloc]initWithPopup:LOCALIZATION(C_ORDER_ITEMSADDED) subtitle:LOCALIZATION(C_ORDER_ADDEDTOCART)];      //lokalise 21 Jan
                _popup.delegate = self;
                [self popupShow];
                break;
        }
    }
}

-(void)yesBtnPressed
{
    NSArray *orderDetails = [self.statusDict objectForKey:@"OrderDetail"];
    NSMutableArray *submitArray = [[NSMutableArray alloc]init];
    NSMutableDictionary *reorderToCart = [[NSMutableDictionary alloc]init];
    for(int i= 0;i < orderDetails.count;i++){
        NSDictionary *dict = @{@"LineNumber" : [[orderDetails objectAtIndex:i] objectForKey:@"LineNumber"],
                               @"IssuedItemQuantity" : [[orderDetails objectAtIndex:i] objectForKey:@"ApprovedQuantity"]
                               };
        [submitArray addObject:dict];
        NSDictionary *addToCartDict = @{@"quantity":@([[[orderDetails objectAtIndex:i] objectForKey:@"Quantity"] doubleValue]),
                                        @"value" : @([[[orderDetails objectAtIndex:i] objectForKey:@"SubTotal"] doubleValue]),
                                        @"image" :[[orderDetails objectAtIndex:i] objectForKey:@"ProductImageUrl"],
                                        @"submenu" :@"",
                                        @"title":[[orderDetails objectAtIndex:i] objectForKey:@"ProductName"],
                                        @"productID" : [[orderDetails objectAtIndex:i] objectForKey:@"ProductID"],
                                        @"unitprice" : [[orderDetails objectAtIndex:i] objectForKey:@"UnitPrice"]
                                        };
        [reorderToCart setObject:addToCartDict forKey:[[orderDetails objectAtIndex:i] objectForKey:@"ProductID"]];
    }
    [_popup removeFromSuperview];
    
    switch (_statusType) {
        case kOrderStatus_Pending:{
            
            
            NSDictionary *submitDict = @{@"OrderID" :[[self.statusDict objectForKey:@"BasicDetails"] objectForKey:@"OrderID"],
                                         @"Status" : [NSString stringWithFormat:@"%d",kOrderStatus_Cancel],
                                         @"ProductDetail" : submitArray,
                                         @"Remarks" : [[self.statusDict objectForKey:@"BasicDetails"] objectForKey:@"Remarks"]
                                         };
            [[WebServiceManager sharedInstance]updateOrderStatus:self submitDict:submitDict];
        }
            break;
        case kOrderStatus_Approved:{
            [self reorder:reorderToCart];
        }
            break;
        case kOrderStatus_Completed:{
            [self reorder:reorderToCart];
        }
            break;
        case kOrderStatus_Rejected:{
            [self reorder:reorderToCart];
        }
            break;
        case kOrderStatus_DSM_Rejected:{
            [self reorder:reorderToCart];
        }
            break;
        case kOrderStatus_Cancel:{
            [self reorder:reorderToCart];
        }
            break;
            
        default:
            break;
    }
}

-(void)reorder:(NSDictionary *)dict{
    
    NSMutableDictionary *cartDict;
    if ([GET_ORDERCART allKeys].count ==0) {
        cartDict = [[NSMutableDictionary alloc]initWithDictionary:dict];
        
    }else{
        cartDict = [[NSMutableDictionary alloc]initWithDictionary:GET_ORDERCART];
        
        for(int i = 0;i<[dict allKeys].count;i++){
            if([cartDict objectForKey:[[dict allKeys] objectAtIndex:i]]== nil){
                [cartDict setObject:[dict objectForKey:[[dict allKeys] objectAtIndex:i]] forKey:[[dict allKeys] objectAtIndex:i]];
            }else{
                NSMutableDictionary *reorderDict = [[NSMutableDictionary alloc]initWithDictionary:[dict objectForKey:[[dict allKeys] objectAtIndex:i]]];
                double reorderQuantity = [[reorderDict objectForKey:@"quantity"] doubleValue];
                double existQuantity = [[[cartDict objectForKey:[[dict allKeys] objectAtIndex:i]] objectForKey:@"quantity"] doubleValue];
                double unitPrice = [[reorderDict objectForKey:@"unitprice"] doubleValue];
                
                [reorderDict setObject:@(reorderQuantity+existQuantity) forKey:@"quantity"];
                [reorderDict setObject:@(unitPrice * (reorderQuantity+existQuantity)) forKey:@"unitprice"];
                [cartDict setObject:reorderDict forKey:[[dict allKeys] objectAtIndex:i]];
            }
        }
        
    }
    
    SET_ORDERCART(cartDict);
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderCart" object:nil];
    [mSession pushMyCartView:self];
}

- (IBAction)dsrBtnPressed:(id)sender {
    self.isFrom = 2;
    NSArray *orderDetails = [self.statusDict objectForKey:@"OrderDetail"];
    NSMutableArray *submitArray = [[NSMutableArray alloc]init];
    for(int i= 0;i < orderDetails.count;i++){
        NSDictionary *dict = @{@"LineNumber" : [[orderDetails objectAtIndex:i] objectForKey:@"LineNumber"],
                               @"IssuedItemQuantity" : [[orderDetails objectAtIndex:i] objectForKey:@"ApprovedQuantity"]
                               };
        [submitArray addObject:dict];
        
        NSDictionary *submitDict;
        if (self.statusType == kOrderStatus_Pending)
        {
            //use textview for remarks
            submitDict = @{@"OrderID" :[[self.statusDict objectForKey:@"BasicDetails"] objectForKey:@"OrderID"],
                           @"Status" : [NSString stringWithFormat:@"%d",kOrderStatus_Rejected],
                           @"ProductDetail" : submitArray,
                           @"Remarks" : self.txtViewRemarks.text,
                           };
        }
        else //approved, reorder?
        {
            submitDict = @{@"OrderID" :[[self.statusDict objectForKey:@"BasicDetails"] objectForKey:@"OrderID"],
                           @"Status" : [NSString stringWithFormat:@"%d",kOrderStatus_Rejected],
                           @"ProductDetail" : submitArray,
                           @"Remarks" : [[self.statusDict objectForKey:@"BasicDetails"] objectForKey:@"Remarks"]
                           };
        }
        

        [[WebServiceManager sharedInstance]updateOrderStatus:self submitDict:submitDict];
    }
}

- (void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_ORDERDETAILS:{
            if([[response getGenericResponse] isEqual:[NSNull null]])
                return;
            
            [self setupInterface:[response getGenericResponse]];
        }
            break;
        case kWEBSERVICE_UPDATESTATUS:{
            if (GET_PROFILETYPE == kPROFILETYPE_DSR){
                if(self.isFrom == 1){
                    NSDictionary *confirmDict =@{@"title" : LOCALIZATION(C_ORDER_APPROVALCONF_TITLE),       //lokalise 21 Jan
                                                 @"header" : [[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Header"],
                                                 @"subtitle" : [[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Message"]
                                                 };
                    
                    SET_CONFIRMORDERKEYS(confirmDict);
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [mSession loadOrderConfirmView];
                }else{
                    NSDictionary *confirmDict = @{@"title" : LOCALIZATION(C_ORDER_REJECTCONF_TITLE),        //lokalise 21 Jan
                                                 @"header" : [[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Header"],
                                                 @"subtitle" : [[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Message"]
                                                 };
                    
                    SET_CONFIRMORDERKEYS(confirmDict);
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [mSession loadOrderConfirmView];
                }
            }else{
                switch (_statusType) {
                    case kOrderStatus_Pending:{
                        NSDictionary *confirmDict =@{@"title" : LOCALIZATION(C_MANAGEORDER_CANCELLATION_TITLE),       //lokalise 21 Jan
                                                     @"header" : [[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Header"],
                                                     @"subtitle" : [[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Message"]
                                                     };
                        
                        SET_CONFIRMORDERKEYS(confirmDict);
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [mSession loadOrderConfirmView];
                    
                    }
                        break;
                }
                
            }
            
        }
            break;
        default:
            break;
    }
}

- (void)processFailed:(WebServiceResponse *)response
{
    
}

@end
