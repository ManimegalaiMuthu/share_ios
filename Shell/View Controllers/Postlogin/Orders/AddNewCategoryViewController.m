//
//  AddNewCategroyViewController.m
//  Shell
//
//  Created by Admin on 13/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "AddNewCategoryViewController.h"
#import "AddToCartTableViewCell.h"

#define CELL_HEIGHT 120
#define CARTVIEWHEIGH 80

@interface AddNewCategoryViewController ()<UITableViewDelegate,UITableViewDataSource,WebServiceManagerDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableviewHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;


@property (weak, nonatomic) IBOutlet UIView *cartView;
@property (weak, nonatomic) IBOutlet UIView *cartBtnView;
@property (weak, nonatomic) IBOutlet UIButton *btnCartView;
@property (weak, nonatomic) IBOutlet UILabel *lblQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lblViewCartHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblSum;
@property (weak, nonatomic) IBOutlet UIView *linkbreakView;
@property (strong, nonatomic) IBOutlet UIButton *btnCart;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cartViewHeight;

@property NSMutableArray *categoryArray;

@property NSMutableArray *mainCategoryCellArray;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *mainCategoryColViewLayout;
@property (weak, nonatomic) IBOutlet UICollectionView *addNewCategoryColView;

@end

@implementation AddNewCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_MANAGEORDER) subtitle: @""];         //lokalise 21 Jan
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    
    if(@available(iOS 11, *)){
        [self.btnCart.widthAnchor constraintEqualToConstant: 25.0].active = YES;
        [self.btnCart.heightAnchor constraintEqualToConstant: 25.0].active = YES;
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnCart];
    
    self.cartBtnView.backgroundColor = COLOUR_RED;
    self.linkbreakView.backgroundColor = COLOUR_YELLOW;
    self.cartViewHeight.constant = 0;
    self.cartView.hidden = YES;
    
    self.lblSum.font = FONT_B1;
    self.lblViewCartHeader.font = FONT_H1;
    self.lblQuantity.font = self.lblSum.font;
    self.lblHeader.font = FONT_H1;
//    SET_ORDERCART(@{});
//    [[NSUserDefaults standardUserDefaults] synchronize];
    self.lblHeader.text = self.categoryName;
    self.lblQuantity.backgroundColor = [UIColor colorWithRed:0.52 green:0.07 blue:0.08 alpha:1.0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveOrderCart)
                                                 name:@"OrderCart"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderCart" object:nil];
    
    self.mainCategoryCellArray = [[NSMutableArray alloc] init];
    self.mainCategoryColViewLayout.estimatedItemSize = CGSizeMake(120, 50);
    if (kIsRightToLeft) {
        [self.btnBack setImage: [UIImage imageNamed:@"icon-arrsingleR.png"] forState: UIControlStateNormal];
        self.lblHeader.textAlignment = NSTextAlignmentRight;
    } else {
        [self.btnBack setImage: [UIImage imageNamed: @"icon-arrsingleL.png"] forState: UIControlStateNormal];
         self.lblHeader.textAlignment = NSTextAlignmentLeft;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_MANAGEORDER) screenClass:nil];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self.mainCategoryColViewLayout invalidateLayout];
    [self.addNewCategoryColView reloadData];
    [self.addNewCategoryColView selectItemAtIndexPath:[NSIndexPath indexPathForItem:self.mainCategoryArray.count-1 inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionRight];
    
    [[WebServiceManager sharedInstance]fetchAllProductsInCategory:self productID:self.productID];
}

-(void)receiveOrderCart
{
    NSDictionary *dict = GET_ORDERCART;
    if([dict count] >0){
        self.cartView.hidden = NO;
        double quantity = 0;
        double totalPrice = 0.0;
        for(int i  = 0;i < [dict allKeys].count; i++){
            double counter = [[[dict objectForKey:[[dict allKeys] objectAtIndex:i]] objectForKey:@"quantity"] doubleValue];
            quantity += counter;
            totalPrice += ([[[dict objectForKey:[[dict allKeys] objectAtIndex:i]] objectForKey:@"value"] doubleValue]);
            
        }
        self.lblQuantity.text = [NSString stringWithFormat:@"%.1f",quantity];
        self.lblViewCartHeader.text = LOCALIZATION(C_ORDER_VIEWCART);       //lokalise 21 Jan
        self.lblSum.text = [NSString stringWithFormat:@"%.2f",totalPrice];
        self.cartViewHeight.constant = CARTVIEWHEIGH;
    }else{
        self.cartView.hidden = YES;
        self.cartViewHeight.constant = 0;
    }
    
}

-(void)setupInterface :(NSArray *)responseArray
{
    [self.tableView registerNib:[UINib nibWithNibName:@"AddToCartTableViewCell" bundle:nil] forCellReuseIdentifier:@"AddToCartTableViewCell"];
    self.categoryArray = [[NSMutableArray alloc]initWithArray:responseArray];
    self.tableviewHeight.constant = self.categoryArray.count *CELL_HEIGHT;
    self.tableView.tableFooterView = [UITableView new];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.categoryArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AddToCartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddToCartTableViewCell"];
    NSDictionary *dict = [self.categoryArray objectAtIndex:indexPath.row];
    
    [cell setupInterface:dict];
    
    NSDictionary *orderCartDict = GET_ORDERCART;
    if([orderCartDict objectForKey:[dict objectForKey:@"ProductID"]] != nil){
        double quantity = [[[orderCartDict objectForKey:[dict objectForKey:@"ProductID"]] objectForKey:@"quantity"] doubleValue];
        [cell setQuantitiy:quantity];
    }else{
        [cell setQuantitiy:0];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

- (IBAction)backBtnPressed:(id)sender {
    [self popSelf];
}

- (IBAction)cartBtnPressed:(id)sender {
    [mSession pushMyCartView:self];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger actualIndex;
    if (kIsRightToLeft) {
        actualIndex = self.mainCategoryArray.count-indexPath.row-1;
    } else {
        actualIndex = indexPath.row;
    }
    for (UICollectionViewCell *cell in self.mainCategoryCellArray)
    {
        UIView *bottomLine = [cell viewWithTag: 2];
        bottomLine.hidden = YES;
        
        UILabel *lblTitle = [cell viewWithTag: 1];
        lblTitle.textColor = COLOUR_VERYDARKGREY;
        bottomLine.hidden = YES;
    }
    
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath: indexPath];
    UILabel *lblTitle = [cell viewWithTag: 1];
    lblTitle.textColor = COLOUR_RED;
    
    UIView *bottomLine = [cell viewWithTag: 2];
    bottomLine.hidden = NO;
    
    NSDictionary *data = [self.mainCategoryArray objectAtIndex: actualIndex];

    self.lblHeader.text = [data objectForKey:@"CatergoryName"];
    [[WebServiceManager sharedInstance]fetchAllProductsInCategory:self productID: [data objectForKey:@"CatergoryID"]];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger actualIndex;
    if (kIsRightToLeft) {
        actualIndex = self.mainCategoryArray.count-indexPath.row-1;
    } else {
        actualIndex = indexPath.row;
    }
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"CategoryCell" forIndexPath:indexPath];
    
    NSDictionary *data = [self.mainCategoryArray objectAtIndex: actualIndex];
    
    UILabel *lblTitle = [cell viewWithTag: 1];
    lblTitle.text = [data objectForKey:@"CatergoryName"];
    lblTitle.font = FONT_H1;
    
    UIView *bottomLine = [cell viewWithTag: 2];
    bottomLine.backgroundColor = COLOUR_RED;
    
    if ([lblTitle.text isEqualToString: self.lblHeader.text])
    {
        lblTitle.textColor = COLOUR_RED;
        bottomLine.hidden = NO;
    }
    else
    {
        lblTitle.textColor = COLOUR_VERYDARKGREY;
        bottomLine.hidden = YES;
    }
    [[cell contentView] setAutoresizingMask:UIViewAutoresizingFlexibleWidth];

    [self.mainCategoryCellArray addObject: cell];
    return cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.mainCategoryArray count];
}



- (void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_ALLPRODUCTSINCATEGORYLIST:{
            self.categoryArray = [[NSMutableArray alloc] init];
            if([[response getGenericResponse] isEqual:[NSNull null]] || [[[response getGenericResponse] objectForKey:@"ProductListDet"] isEqual:[NSNull null]]){
                [self.tableView reloadData];
                return;
            }
            [self setupInterface:[[response getGenericResponse] objectForKey:@"ProductListDet"]];
        }
            break;
        default:
            break;
    }
}

- (void)processFailed:(WebServiceResponse *)response
{
    
}

@end
