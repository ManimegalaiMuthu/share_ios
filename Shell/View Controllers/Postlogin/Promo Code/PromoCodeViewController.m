//
//  PromoCodeViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 18/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "PromoCodeViewController.h"
#import "PromoCodeMasterViewController.h"
#import "QRCodeScanner.h"
#import "PopupRegisterCustomerViewController.h"
#import "RegisterCustomerCompleteViewController.h"

#define CELL_HEIGHT 55
#define SUBMITOILCHANGE_HEIGHT 85

@interface PromoCodeViewController () <WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource, QRCodeScannerDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, CLLocationManagerDelegate, TTTAttributedLabelDelegate, PopupRegisterCustomerViewDelegate, RegisterCustomerCompleteViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtPromoCode;

@property (weak, nonatomic) IBOutlet UILabel *lblVehicleId;
@property (weak, nonatomic) IBOutlet UILabel *lblOfferDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblQty;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *qtyTopSpace;
@property (weak, nonatomic) IBOutlet UILabel *lblValidity;

@property (weak, nonatomic) IBOutlet UIButton *btnScan;
@property (weak, nonatomic) IBOutlet UIButton *btnManual;

@property (weak, nonatomic) IBOutlet UILabel *lblNo;
@property (weak, nonatomic) IBOutlet UILabel *lblScannedCode;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblDelete;


@property (weak, nonatomic) IBOutlet UIView *offerDetailsView;
@property (weak, nonatomic) IBOutlet UIView *scanCodeView;

//@property (weak, nonatomic) IBOutlet UIButton *goBtn;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;

@property NSMutableArray *redemptionArray;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@property BOOL isPromoCodeQR;
@property NSDictionary *promoOfferDict;


@property (weak, nonatomic) IBOutlet UIView *manualEntryPopupView;
@property (weak, nonatomic) IBOutlet UIButton *btnPopupSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnPopupCancel;
@property (weak, nonatomic) IBOutlet UILabel *lblPopupHeader;
@property (weak, nonatomic) IBOutlet UITextField *txtPopupCode;


@property CLLocationManager *locationManager;
@property CGPoint currentLocation;
@property BOOL didFindLocation;

@property (weak, nonatomic) IBOutlet UIView *enterMileageView;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmitOilChange;
@property (weak, nonatomic) IBOutlet UILabel *lblSubmitOilChange;

@property (weak, nonatomic) IBOutlet UITextField *txtEnterMileage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *enterMileageViewHeight;



@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblInviteConsumer;

@end

@implementation PromoCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    mQRCodeScanner.delegate = self;
    
    // Declare the Gesture.
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc]
                                             initWithTarget:self
                                             action:@selector(handleTap:)];
    gesRecognizer.delegate = self;
    [self.manualEntryPopupView addGestureRecognizer: gesRecognizer];
    
    [self setupInterface];
    
    [Helper setHyperlinkLabel:self.lblInviteConsumer hyperlinkText:LOCALIZATION(C_INVITE_CUSTOMER) bodyText:LOCALIZATION(C_INVITE_CUSTOMER) urlString: @"Invite"];   //lokalised 1 Feb
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [self.locationManager requestWhenInUseAuthorization];
    
    if (kIsRightToLeft) {
        self.txtPromoCode.textAlignment = NSTextAlignmentRight;
    } else {
        self.txtPromoCode.textAlignment = NSTextAlignmentLeft;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    self.shouldLoadTextfieldEndApi = YES;
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat: @"%@ / %@", LOCALIZATION_EN(C_TITLE_PROMOCODE), LOCALIZATION_EN(C_PROMO_REDEEMOFFERTAB)] screenClass:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void) setupInterface
{
    self.redemptionArray = [[NSMutableArray alloc] init];
    
//    self.lblOfferHeader.font = FONT_H1;
//    self.lblOfferHeader.text = LOCALIZATION();
    self.txtPromoCode.font = FONT_B1;
    self.txtPromoCode.delegate = self;
    self.txtPromoCode.placeholder = LOCALIZATION(C_PROMO_ENTERPROMOCODE);  //lokalised 1 Feb
    
    self.lblVehicleId.font = FONT_B1;
    self.lblVehicleId.text = [NSString stringWithFormat: @"%@: %@", LOCALIZATION(C_PROMO_VEHICLEID), @""];  //lokalised 1 Feb
    self.lblOfferDetails.text = [NSString stringWithFormat: @"%@: %@", LOCALIZATION(C_PROMO_REWARDDETAILS), @""];  //lokalised 1 Feb
    self.lblQty.text = [NSString stringWithFormat: @"%@: %@", LOCALIZATION(C_PROMO_DETAIL_QTY), @""];  //lokalised 1 Feb
    self.lblValidity.text = [NSString stringWithFormat: @"%@: %@", LOCALIZATION(C_PROMO_VALIDITY), @""];  //lokalised 1 Feb
    
    self.lblQty.font = self.lblVehicleId.font;
    self.lblValidity.font = self.lblVehicleId.font;
    self.lblOfferDetails.font = self.lblVehicleId.font;
    
    self.lblSubmitOilChange.text = LOCALIZATION(C_PROMO_SUBMITOILCHANGE); //lokalised 1 Feb
    self.lblSubmitOilChange.font = FONT_B2;
    
    self.txtEnterMileage.placeholder = LOCALIZATION(C_SEARCHFIELD_MILEAGE); //lokalised 1 Feb
    self.txtEnterMileage.font = FONT_B1;
    
    
    
    [self.btnScan.titleLabel setFont: FONT_BUTTON];
    [self.btnScan setBackgroundColor: COLOUR_YELLOW];
    [self.btnScan setTitle:LOCALIZATION(C_PROMO_SCAN_CODE) forState:UIControlStateNormal];   //lokalised 1 Feb
    [self.btnScan setTitleColor: COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    
    [self.btnManual.titleLabel setFont: FONT_BUTTON];
    [self.btnManual setBackgroundColor: COLOUR_YELLOW];
    [self.btnManual setTitle:LOCALIZATION(C_PROMO_ENTER_CODE) forState:UIControlStateNormal];    //lokalised 1 Feb
    [self.btnManual setTitleColor: COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    
//    [Helper setCustomFontButtonContentModes: self.goBtn];
//    [self.goBtn.titleLabel setFont: FONT_B1];
//    [self.goBtn setTitle: LOCALIZATION(C_PROMO_ENTER) forState: UIControlStateNormal];
    
    self.lblNo.font = FONT_B1;
    self.lblScannedCode.font = self.lblNo.font;
    self.lblStatus.font = self.lblNo.font;
    self.lblDelete.font = self.lblNo.font;
    
    self.lblNo.text = LOCALIZATION(C_TABLE_NO);    //lokalised 1 Feb
    self.lblScannedCode.text = LOCALIZATION(C_PROMO_SCANNEDCODE);    //lokalised 1 Feb
    self.lblStatus.text = LOCALIZATION(C_STATUS);  //lokalised 1 Feb
    self.lblDelete.text = LOCALIZATION(C_DELETE);   //lokalised 1 Feb
    
    self.btnSubmit.titleLabel.font = FONT_BUTTON;
    [self.btnSubmit setTitle: LOCALIZATION(C_PROMO_COMPLETEREDEMPTION) forState: UIControlStateNormal];  //lokalised 1 Feb
    self.btnSubmit.backgroundColor = COLOUR_RED;
    
    self.lblPopupHeader.font = FONT_H1;
    self.lblPopupHeader.text = LOCALIZATION(C_PROMO_POPUP_HEADER);  //lokalised 1 Feb
    
    self.txtPopupCode.font = FONT_B1;
    self.txtPopupCode.placeholder = LOCALIZATION(C_PROMO_POPUP_PLACEHOLDER);  //lokalised 1 Feb
    [self.btnPopupCancel.titleLabel setFont: FONT_BUTTON];
    [self.btnPopupSubmit.titleLabel setFont: FONT_BUTTON];
    [self.btnPopupSubmit setBackgroundColor:COLOUR_RED];
    
    [self.btnPopupCancel setTitle: LOCALIZATION(C_FORM_CANCEL) forState: UIControlStateNormal]; //lokalised 1 Feb
    [self.btnPopupSubmit setTitle: LOCALIZATION(C_FORM_SUBMIT) forState: UIControlStateNormal]; //lokalised 1 Feb
    
    
    //empty view to eliminate extra separators
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    self.tableViewHeight.constant = CELL_HEIGHT * [self.redemptionArray count];
    return [self.redemptionArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"PromoCell"];
    
    NSDictionary *scannedCodeData = [self.redemptionArray objectAtIndex: indexPath.row];
    
    UILabel *lblNumber = [cell viewWithTag: 1];
    lblNumber.text = @(indexPath.row + 1).stringValue;
    lblNumber.font = FONT_B1;
    
    UILabel *lblScannedCode = [cell viewWithTag: 2];
    lblScannedCode.text = [scannedCodeData objectForKey:@"Code"];
    lblScannedCode.font = lblNumber.font;
    
    UILabel *lblStatus = [cell viewWithTag: 3];
    if (![scannedCodeData objectForKey: @"Remarks"])
        lblStatus.text = LOCALIZATION(C_SCANCODE_STATUS_PENDING); //lokalised 1 Feb
    else if ([[scannedCodeData objectForKey: @"Remarks"] isEqualToString: BLANK])
        lblStatus.text = LOCALIZATION(C_SCANCODE_STATUS_PENDING); //lokalised 1 Feb
    else
        lblStatus.text = [scannedCodeData objectForKey: @"Remarks"];
    lblStatus.font = lblNumber.font;
    lblStatus.numberOfLines = 0;
    
    UIButton *deleteBtn = [cell viewWithTag: 4];
    deleteBtn.tag = indexPath.row;
    
    cell.contentView.layer.borderWidth = 2.0f;
    cell.contentView.layer.borderColor = [[UIColor clearColor] CGColor];
    
    if ([[scannedCodeData allKeys] containsObject: @"Status"])
    {
        if (![[scannedCodeData objectForKey: @"Status"] boolValue])
            cell.contentView.layer.borderColor = [COLOUR_RED CGColor];
    }
    
    return cell;
}

- (IBAction)manualPressed:(id)sender {
    self.manualEntryPopupView.hidden = NO;
}

- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer{
    self.manualEntryPopupView.hidden = YES;
}

- (IBAction)promoCodeQRPressed:(id)sender {
    if ([self.txtPromoCode isFirstResponder])
    {
        self.shouldLoadTextfieldEndApi = NO;
    }
    self.isPromoCodeQR = YES;
    [mQRCodeScanner showZBarScannerOnVC: self];
}

- (IBAction)popupCancelPressed:(id)sender {
    self.txtPopupCode.text = @"";
    self.manualEntryPopupView.hidden = YES;
}

- (IBAction)popupSubmitPressed:(id)sender {
    
    if(![[mSession profileInfo]hasMultipleSubmissionOilChange])
    {
        for (NSDictionary *dict in self.redemptionArray)
        {
            if ([self.txtPopupCode.text isEqualToString: [dict objectForKey: @"Code"]])
            {
                [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_EXIST)];  //lokalised 1 Feb
                return;
            }
        }
    }
    
    self.manualEntryPopupView.hidden = YES;
    
    [self.redemptionArray addObject: @{@"Code" : self.txtPopupCode.text,
                                       }];
    [self.tableView reloadData];
    self.txtPopupCode.text = @"";
    
}

- (IBAction)scanPressed:(id)sender {
    self.isPromoCodeQR = NO;
    if ([self.txtPromoCode isFirstResponder])
    {
        self.shouldLoadTextfieldEndApi = NO;
    }
    [mQRCodeScanner showZBarScannerOnVC: self];
}

-(void)qrCodeScanSuccess:(NSString *)scanString
{
    NSString *scannedCodeString = [[scanString componentsSeparatedByString:@"="] lastObject];
    if (self.isPromoCodeQR)
    {
        self.txtPromoCode.text = scannedCodeString;
        [[WebServiceManager sharedInstance] getPromoDetails: self.txtPromoCode.text vc: self];
    }
    else
    {
        if ([self.txtPromoCode isFirstResponder])
        {
            self.shouldLoadTextfieldEndApi = NO;
        }
        
        if(![[mSession profileInfo]hasMultipleSubmissionOilChange])
        {
            for (NSDictionary *dict in self.redemptionArray)
            {
                if ([scannedCodeString isEqualToString: [dict objectForKey: @"Code"]])
                {
                    [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_EXIST)]; //lokalised 1 feb
                    return;
                }
            }
        }
        
        [self.redemptionArray addObject: @{@"Code" : scannedCodeString,
                                           }];
        [self.tableView reloadData];
    }
}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event  {
//    UITouch *touch = [touches anyObject];
//    if ([self.txtPromoCode isFirstResponder])
//    {
//
//    }
//}


-(void) textFieldDidEndEditing:(UITextField *)textField
{
//    [self initRegisterCustomerPopup: @{@"PromoCode": self.txtPromoCode.text}];
//    return;
    
    if (textField == self.txtPromoCode)
    {
        if (self.shouldLoadTextfieldEndApi)
        {
            [[WebServiceManager sharedInstance] getPromoDetails: self.txtPromoCode.text
                                                         vc: self];
            [self.redemptionArray removeAllObjects];
            [_tableView reloadData];
            self.shouldLoadTextfieldEndApi = YES;
            
            self.lblInviteConsumer.hidden = YES;
        }
    }
}

- (IBAction)deletePressed:(UIButton *)sender
{
    [self.redemptionArray removeObjectAtIndex:sender.tag];
    [_tableView reloadData];
}

- (IBAction)submitPressed:(id)sender
{
    self.didFindLocation = NO;
    [UpdateHUD addMBProgress: self.view withText: @""];
    switch ([CLLocationManager authorizationStatus])
    {
        case kCLAuthorizationStatusAuthorizedAlways:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.locationManager startUpdatingLocation];
            break;
        default:
        {
            //send 0,0
            self.currentLocation = CGPointMake(0, 0);
            
            NSDictionary *paramDict = @{@"PromoOfferID": [self.promoOfferDict objectForKey: @"PromoOfferID"],
                                        @"PromoCode": [self.promoOfferDict objectForKey: @"PromoCode"],
                                        @"Latitude"      : @(self.currentLocation.x),
                                        @"Longitude"     : @(self.currentLocation.y),
                                        @"Codes": self.redemptionArray,
                                        @"AddOilChange":    @(self.btnSubmitOilChange.selected),
                                        @"Mileage":         @(self.txtEnterMileage.text.intValue),
                                        };
            
            [UpdateHUD removeMBProgress: self];
            
            [[WebServiceManager sharedInstance] submitPromoCode: paramDict
                                                             vc: self];
        }
            break;
    }
}



-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    if (self.didFindLocation)
        return;
    
    //most recent location update is at the end of the array.
    CLLocation *location = [locations lastObject];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:6];
    
    self.currentLocation = CGPointMake(location.coordinate.latitude, location.coordinate.longitude);
    
    self.didFindLocation = YES;
    [self.locationManager stopUpdatingLocation];
    
    NSDictionary *paramDict = @{@"PromoOfferID": [self.promoOfferDict objectForKey: @"PromoOfferID"],
                                @"PromoCode": [self.promoOfferDict objectForKey: @"PromoCode"],
                                @"Latitude"      : @(self.currentLocation.x),
                                @"Longitude"     : @(self.currentLocation.y),
                                @"Codes": self.redemptionArray,
                                @"AddOilChange":    @(self.btnSubmitOilChange.selected),
                                @"Mileage":         @(self.txtEnterMileage.text.intValue),
                                };
    
    [UpdateHUD removeMBProgress: self];
    self.currentLocation = CGPointMake(0, 0);
    
    [[WebServiceManager sharedInstance] submitPromoCode: paramDict
                                                     vc: self];
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url absoluteString] isEqualToString: @"Invite"])
    {
        //show popup
        [self prepareToRegisterCustomer];
    }
}

-(void) prepareToRegisterCustomer
{
    self.lblInviteConsumer.hidden = NO;
    
    [self initRegisterCustomerPopup: @{@"PromoCode": self.txtPromoCode.text}];
}

-(void) prepareToRegisterCustomer:(NSDictionary *)responseData {
    self.lblInviteConsumer.hidden = NO;
    
    [self initRegisterCustomerPopup:responseData];
}

-(void)didPressedRegisterCustomer
{
    [self completeRegister];
}

-(void) completeRegister
{
    [[WebServiceManager sharedInstance] getPromoDetails: self.txtPromoCode.text
                                                     vc: self];
    [self.redemptionArray removeAllObjects];
    [_tableView reloadData];
    self.shouldLoadTextfieldEndApi = YES;
    
    self.lblInviteConsumer.hidden = YES;
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_PROMOGETDETAILS:
        {
            self.lblInviteConsumer.hidden = YES;
            self.promoOfferDict = [response getGenericResponse];
            self.lblVehicleId.text = [NSString stringWithFormat: @"%@: %@", LOCALIZATION(C_PROMO_VEHICLEID), [self.promoOfferDict objectForKey: @"VehicleNumber"] ];     //lokalise 1 Feb
            self.lblOfferDetails.text = [NSString stringWithFormat: @"%@: %@", LOCALIZATION(C_PROMO_REDEMPTIONITEM), [self.promoOfferDict objectForKey: @"OfferDetails"]];      //lokalise 1 Feb
            if ([[self.promoOfferDict objectForKey: @"ShowQTY"] boolValue])
            {
                self.lblQty.text = [NSString stringWithFormat: @"%@: %@", LOCALIZATION(C_PROMO_DETAIL_QTY), [self.promoOfferDict objectForKey: @"NumberOfScans"]];  //lokalised 1 feb
                self.qtyTopSpace.constant = 10;
            }
            else
            {
                self.qtyTopSpace.constant = 0;
                self.lblQty.text = @"";
            }
            self.lblValidity.text = [NSString stringWithFormat: @"%@: %@", LOCALIZATION(C_PROMO_VALIDITY), [self.promoOfferDict objectForKey: @"Validity"]];  //lokalised 1 feb

            self.txtEnterMileage.text = @"";		
            if ([[self.promoOfferDict objectForKey: @"EnableOilChange"] boolValue])
            {
                self.enterMileageViewHeight.constant = SUBMITOILCHANGE_HEIGHT; //default checked
                self.txtEnterMileage.hidden = NO;
                self.btnSubmitOilChange.selected = YES;
                self.enterMileageView.hidden = NO;
                
                if ([[self.promoOfferDict objectForKey: @"ForceOilChange"] boolValue])
                    self.btnSubmitOilChange.userInteractionEnabled = NO;
                else
                    self.btnSubmitOilChange.userInteractionEnabled = YES;
            }
            else
            {
                self.btnSubmitOilChange.selected = NO;
                self.enterMileageViewHeight.constant = 0;
                self.enterMileageView.hidden = YES;
            }

            
            if ([[self.promoOfferDict objectForKey: @"AllowQRScan"] boolValue])
                self.scanCodeView.hidden = NO; //show if allow QR scan
            else
                self.scanCodeView.hidden = YES;
            
            self.offerDetailsView.hidden = NO;
            
            //v2 here
            //check for v1 also
            if(GET_PROFILETYPE == kPROFILETYPE_DSR)
                self.btnSubmit.hidden = YES;
            else
                self.btnSubmit.hidden = NO;
            
            if (![[response getResponseMessage] isKindOfClass: [NSNull class]])
            {
                [mAlert showSuccessAlertWithMessage: response.getResponseMessage onCompletion:^(BOOL finished) {
                }];
            }
        }
            break;
        case kWEBSERVICE_PROMOSUBMITCODE:
        {
            if (![[response getGenericResponse] isKindOfClass: [NSNull class]])
            {
                [mSession pushPromoCodeResultView: [response getGenericResponse]  vc: self.navigationController.parentViewController];
                
                self.txtPromoCode.text = @"";
                
                self.scanCodeView.hidden = YES;
                self.offerDetailsView.hidden = YES;
                self.btnSubmit.hidden = YES;
                
                self.promoOfferDict = nil;
                [self.redemptionArray removeAllObjects];
                [self.tableView reloadData];
            }
            else
            {
                [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                    //clear fields?
                    self.txtPromoCode.text = @"";
                    
                    self.scanCodeView.hidden = YES;
                    self.offerDetailsView.hidden = YES;
                    self.btnSubmit.hidden = YES;
                    
                    self.promoOfferDict = nil;
                    [self.redemptionArray removeAllObjects];
                    [self.tableView reloadData];
                }];
            }
        }
            break;
//        case kWEBSERVICE_PROMOFETCHHISTORY:
//            break;
        default:
            break;
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_PROMOGETDETAILS:
            self.scanCodeView.hidden = YES;
            self.offerDetailsView.hidden = YES;
            self.btnSubmit.hidden = YES;
            
            //consumer not registered
            if ([response getResponseCode] == 8000)
            {
                //show popup
                if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND]) {
                    [self prepareToRegisterCustomer:[[response getJSON]objectForKey:@"Data"]];
                } else {
                    [self prepareToRegisterCustomer];
                }
                return;
            }
            break;
        case kWEBSERVICE_PROMOSUBMITCODE:
            if (response.errorInResponse)
            {
                if ([[response getGenericResponse] isKindOfClass: [NSNull class]])
                {
                    return;
                }
                if ([response getGenericResponse])
                {
                    NSDictionary *respData = [response getGenericResponse];
                    self.redemptionArray = [respData objectForKey:@"ProductCodes"];
                    
                    //null handler
                    if ([self.redemptionArray isKindOfClass: [NSNull class]])
                        self.redemptionArray = [[NSMutableArray alloc] init];
                    
                    [self.tableView reloadData];
                }
            }
            break;
        default:
            break;
    }
}

- (IBAction)checkBoxSelected:(UIButton *)sender {
    self.txtEnterMileage.text = @"";
    if ([sender isSelected])
    {
        sender.selected = NO;
        self.enterMileageViewHeight.constant = SUBMITOILCHANGE_HEIGHT - 35;
        self.txtEnterMileage.hidden = YES;
    }
    else
    {
        sender.selected = YES;
        self.enterMileageViewHeight.constant = SUBMITOILCHANGE_HEIGHT;
        self.txtEnterMileage.hidden = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnDelete:(id)sender {
}
@end
