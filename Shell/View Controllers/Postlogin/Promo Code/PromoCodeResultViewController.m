//
//  PromoCodeResultViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 18/12/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "PromoCodeResultViewController.h"

#define CELL_HEIGHT 70

@interface PromoCodeResultViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet UILabel *lblRedemptionCompletedHeader;

@property (weak, nonatomic) IBOutlet UILabel *lblConfirmationHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleId;
@property (weak, nonatomic) IBOutlet UILabel *lblOfferDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblRemarks;

@property (weak, nonatomic) IBOutlet UILabel *lblVehicleIdData;
@property (weak, nonatomic) IBOutlet UILabel *lblOfferDetailsData;
@property (weak, nonatomic) IBOutlet UILabel *lblRemarksData;


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSArray *couponArray;
@end

@implementation PromoCodeResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_PROMOCODE) subtitle: @"" size: 15 subtitleSize: 0];   //lokalised 1 feb
    [self.btnBack setTintColor: COLOUR_RED];
    [self.btnBack.titleLabel setFont: FONT_BUTTON];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    self.tableView.tableFooterView = [UIView new];
    
    self.lblRedemptionCompletedHeader.text = LOCALIZATION(C_PROMO_RESULTS_COMPLETEDHEADER);   //lokalised 1 feb
    self.lblRedemptionCompletedHeader.font = FONT_H(18);
    
    self.lblConfirmationHeader.font = FONT_H1;
    self.lblConfirmationHeader.attributedText = [[NSAttributedString alloc] initWithString: LOCALIZATION(C_PROMO_RESULTS_CONFIRMATIONHEADER)  //lokalised 1 feb
                                    attributes: @{NSFontAttributeName: FONT_H1,
                                                  NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}];
    
    self.lblVehicleId.font = FONT_H1;
    self.lblVehicleIdData.font = FONT_B1;
    self.lblOfferDetails.font = self.lblVehicleId.font;
    self.lblOfferDetailsData.font = self.lblVehicleIdData.font;
    self.lblRemarks.font = self.lblVehicleId.font;
    self.lblRemarksData.font = self.lblVehicleIdData.font;

    self.lblVehicleId.text = LOCALIZATION(C_PROMO_RESULTS_VEHICLEID);               //lokalise 1 feb
    self.lblOfferDetails.text = LOCALIZATION(C_PROMO_RESULTS_OFFERDETAILS);         //lokalise 1 feb
    self.lblRemarks.text = LOCALIZATION(C_PROMO_RESULTS_REMARKS);                //lokalise 1 feb
    
    self.lblVehicleIdData.text = ([[self.resultsDict objectForKey: @"VehicleNumber"] isKindOfClass: [NSNull class]]) ? @"" : [self.resultsDict objectForKey:@"VehicleNumber"];
    self.lblOfferDetailsData.text = ([[self.resultsDict objectForKey: @"RedemptionDetails"] isKindOfClass: [NSNull class]]) ? @"" : [self.resultsDict objectForKey: @"RedemptionDetails"];
    
    self.lblRemarksData.text = ([[self.resultsDict objectForKey: @"OrderReference"] isKindOfClass: [NSNull class]]) ? @"" : [self.resultsDict objectForKey: @"OrderReference"];
    self.lblRemarksData.textColor = COLOUR_RED;
    
    self.couponArray =  ([[self.resultsDict objectForKey: @"ProductCodes"] isKindOfClass: [NSNull class]]) ? [[NSMutableArray alloc] init] : [self.resultsDict objectForKey: @"ProductCodes"];
    
    [self.tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_PROMOCODE) screenClass:nil];
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

#pragma mark - tableview delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.couponArray count] > 0)
        return [self.couponArray count] + 1;
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (indexPath.row == 0) //header
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"PendingCodeHeader"];
#warning refactor names later
        
        UILabel *lblNo = [cell viewWithTag: 1];
        lblNo.font = FONT_H2;
        lblNo.text = LOCALIZATION(C_PROMO_RESULTS_NO);        //lokalise 1 feb
        
        UILabel *lblCode = [cell viewWithTag: 2];
        lblCode.font = lblNo.font;
        lblCode.text = LOCALIZATION(C_PROMO_RESULTS_CODE);        //lokalise 1 feb
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"PendingCodeCell"];
        
        NSDictionary *couponData = [self.couponArray objectAtIndex: indexPath.row-1];
        
        UILabel *lblNo = [cell viewWithTag: 1];
        lblNo.text = @(indexPath.row).stringValue; //index 0 is header
        lblNo.font = FONT_B2;
        
        UILabel *lblCode = [cell viewWithTag: 2];
        lblCode.text = [NSString stringWithFormat: @"%@\n%@",
                        ([[couponData objectForKey: @"Code"] isKindOfClass: [NSNull class]]) ? @"" : [couponData objectForKey: @"Code"],
                        ([[couponData objectForKey: @"Desc"] isKindOfClass: [NSNull class]]) ? @"" : [couponData objectForKey: @"Desc"]];
        lblCode.font = FONT_B2;
        
        UILabel *lblResult = [cell viewWithTag: 3];
        lblResult.text = [couponData objectForKey: @"Remarks"];
        lblResult.font = FONT_B1;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
