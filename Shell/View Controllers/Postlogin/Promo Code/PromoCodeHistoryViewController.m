//
//  PromoCodeHistoryViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 21/11/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "PromoCodeHistoryViewController.h"

@interface PromoCodeHistoryViewController () <WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource, CommonListDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet TableViewWithEmptyView *tableView;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet UIButton *btnStartDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEndDate;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIButton *btnType;
@property (weak, nonatomic) IBOutlet UIButton *goBtn;
@property (weak, nonatomic) IBOutlet UILabel *datePickerTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;

@property (weak, nonatomic) IBOutlet UIView *detailsPopupView;
@property (weak, nonatomic) IBOutlet UIScrollView *detailScrollView;


@property (weak, nonatomic) IBOutlet UILabel *lblDetailsHeader;

@property (weak, nonatomic) IBOutlet UILabel *lblDetailsPopupVehicleId;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailsPopupRedemptionDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailsPopupProductName;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailsPopupProductCodes;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailsPopupQty;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailsPopupVerificationNum;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailsPopupApprovedBy;

@property (weak, nonatomic) IBOutlet UILabel *lblDetailsPopupVehicleIdData;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailsPopupRedemptionDetailsData;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailsPopupProductNameData;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailsPopupProductCodesData;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailsPopupQtyData;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailsPopupVerificationNumData;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailsPopupApprovedByData;

@property (weak, nonatomic) IBOutlet UIButton *btnDetailsPopupOk;
@property (weak, nonatomic) IBOutlet UIView *typeLineView;


@property NSMutableArray *historyArray;

@end

@implementation PromoCodeHistoryViewController
@synthesize listView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.datePickerView.hidden = YES;
//    if ([GET_LOCALIZATION isEqualToString: kThai])
//    {
//        [self.datePicker setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
//        [self.datePicker setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierBuddhist]];
//    }
    
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_PROMO_SELECT_OFFERTYPE) list: [mSession getRedemptionType] selectionType:ListSelectionTypeSingle previouslySelected: nil];  //lokalised 1 feb
    listView.delegate = self;
    
    self.tableView.tableFooterView = [UIView new];
    [self.typeLineView setBackgroundColor:COLOUR_RED];
    
    // Initialize the refresh control.
    
    if(@available(iOS 10, *))
        self.tableView.refreshControl = nil;
    [self.tableView.customRefreshControl removeFromSuperview];
    self.tableView.customRefreshControl = nil; //profiling dont need refresh
    
    self.historyArray = [[NSMutableArray alloc] init];
    
    [self setupInterface];
    
    NSDictionary *requestParamsDict = @{@"FromDate": @"",
                                        @"ToDate": @"",
                                        @"OfferType": [mSession convertToRedemptionTypeKeyCode: self.btnType.titleLabel.text]
                                        };
    
    [[WebServiceManager sharedInstance] fetchPromoRedemptionHistory: requestParamsDict
                                                                 vc:self];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat: @"%@ / %@", LOCALIZATION_EN(C_TITLE_PROMOCODE), LOCALIZATION_EN(C_PROMO_REDEMPTIONHISTORYTAB)] screenClass:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
}

-(void) setupInterface
{
    [Helper setCustomFontButtonContentModes: self.btnType];
    [self.btnType.titleLabel setFont: FONT_B1];
    [self.btnType setTitle: LOCALIZATION(C_PROMO_SELECT_OFFERTYPE) forState: UIControlStateNormal];     //lokalised 1 feb
    
    
    [Helper setCustomFontButtonContentModes: self.btnStartDate];
    [self.btnStartDate.titleLabel setFont: FONT_B2];
    [self.btnStartDate setTitle: LOCALIZATION(C_POINTS_STARTDATE) forState: UIControlStateNormal];  //lokalised 1 feb
    
    [Helper setCustomFontButtonContentModes: self.btnEndDate];
    [self.btnEndDate.titleLabel setFont: FONT_B2];
    [self.btnEndDate setTitle: LOCALIZATION(C_POINTS_ENDDATE) forState: UIControlStateNormal];  //lokalised 1 feb
    
    [Helper setCustomFontButtonContentModes: self.goBtn];
    [self.goBtn.titleLabel setFont: FONT_B1];
    [self.goBtn setTitle: LOCALIZATION(C_POINTS_GO) forState: UIControlStateNormal];  //lokalised 1 feb
    [self.goBtn setBackgroundColor:COLOUR_RED];
    
    [self.lblTo setFont: FONT_B2];
    self.lblTo.text =  LOCALIZATION(C_POINTS_TO);  //lokalised 1 feb
    
    
    self.lblDetailsHeader.text = LOCALIZATION(C_PROMO_DETAIL_HEADER);  //lokalised 1 feb
    self.lblDetailsHeader.font = FONT_H1;
    
    self.lblDetailsPopupVehicleId.text          = LOCALIZATION(C_PROMO_DETAIL_VEHICLEID);             //lokalised 1 feb
    self.lblDetailsPopupRedemptionDetails.text  = LOCALIZATION(C_PROMO_DETAIL_REDEMPTIONDETAILS);        //lokalised 1 feb
    self.lblDetailsPopupProductName.text        = LOCALIZATION(C_PROMO_DETAIL_PRODUCTNAME);           //lokalised 1 feb
    self.lblDetailsPopupProductCodes.text       = LOCALIZATION(C_PROMO_DETAIL_PRODUCTCODES);          //lokalised 1 feb
    self.lblDetailsPopupQty.text                = LOCALIZATION(C_PROMO_DETAIL_QTY);                   //lokalised 1 feb
    self.lblDetailsPopupVerificationNum.text    = LOCALIZATION(C_PROMO_DETAIL_VERIFICATIONNO);        //lokalised 1 feb
    self.lblDetailsPopupApprovedBy.text         = LOCALIZATION(C_PROMO_DETAIL_APPROVEDBY);            //lokalised 1 feb
    
    self.lblDetailsPopupVehicleId.font          = FONT_H2;
    self.lblDetailsPopupRedemptionDetails.font  = self.lblDetailsPopupVehicleId.font;
    self.lblDetailsPopupProductName.font        = self.lblDetailsPopupVehicleId.font;
    self.lblDetailsPopupProductCodes.font       = self.lblDetailsPopupVehicleId.font;
    self.lblDetailsPopupQty.font                = self.lblDetailsPopupVehicleId.font;
    self.lblDetailsPopupVerificationNum.font    = self.lblDetailsPopupVehicleId.font;
    self.lblDetailsPopupApprovedBy.font         = self.lblDetailsPopupVehicleId.font;
    
    self.lblDetailsPopupVehicleIdData.font           = FONT_B2;
    self.lblDetailsPopupRedemptionDetailsData.font   = self.lblDetailsPopupVehicleIdData.font;
    self.lblDetailsPopupProductNameData.font         = self.lblDetailsPopupVehicleIdData.font;
    self.lblDetailsPopupProductCodesData.font        = self.lblDetailsPopupVehicleIdData.font;
    self.lblDetailsPopupQtyData.font                 = self.lblDetailsPopupVehicleIdData.font;
    self.lblDetailsPopupVerificationNumData.font     = self.lblDetailsPopupVehicleIdData.font;
    self.lblDetailsPopupApprovedByData.font          = self.lblDetailsPopupVehicleIdData.font;
    
    //change redempt ID to red
    self.lblDetailsPopupVerificationNumData.textColor = COLOUR_RED;
    
    [self.btnDetailsPopupOk.titleLabel setFont: FONT_BUTTON];
    [self.btnDetailsPopupOk setTitle: LOCALIZATION(C_BUTTON_OK) forState: UIControlStateNormal];    //lokalised 1 feb
    self.btnDetailsPopupOk.backgroundColor = COLOUR_RED;
    
//    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc]
//                                             initWithTarget:self
//                                             action:@selector(hidePopup:)];
//    gesRecognizer.delegate = self;
//    [self.detailsPopupView addGestureRecognizer: gesRecognizer];
}

- (IBAction)typePressed:(id)sender {
    [listView setTitleAndList: LOCALIZATION(C_PROMO_SELECT_OFFERTYPE) list: [mSession getRedemptionType] hasSearchField: NO];   //lokalised 1 feb
    
    [self popUpView];
}
-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        [self.btnType setTitle: selection.firstObject forState:UIControlStateNormal];
        [self goPressed:nil];
    }
}

- (IBAction)startDatePressed:(id)sender {
    self.datePicker.tag = 1;
    self.datePickerTitle.text = LOCALIZATION(C_POINTS_STARTDATE);  //lokalised 1 feb
    self.datePickerView.hidden = NO;
}
- (IBAction)endDatePressed:(id)sender {
    self.datePicker.tag = 2;
    self.datePickerTitle.text = LOCALIZATION(C_POINTS_ENDDATE);  //lokalised 1 feb
    self.datePickerView.hidden = NO;
}

- (IBAction)datePickerDonePressed:(id)sender {
    self.datePickerView.hidden = YES;
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];
    
//    if ([GET_LOCALIZATION isEqualToString: kThai])
//    {
//        [outputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
//        [outputFormatter setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierBuddhist]];
//    }
    
    if(self.datePicker.tag == 1)
        [self.btnStartDate setTitle:[outputFormatter stringFromDate:self.datePicker.date] forState:UIControlStateNormal];
    else
        [self.btnEndDate setTitle:[outputFormatter stringFromDate:self.datePicker.date] forState:UIControlStateNormal];
    
}
- (IBAction)goPressed:(id)sender {
    self.datePickerView.hidden = YES;
    
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat:@"dd MMM yyyy"];
    //    if ([GET_LOCALIZATION isEqualToString: kThai])
    //    {
    //        [inputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
    //        [inputFormatter setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierBuddhist]];
    //    }
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy/MM/dd"];
    //    if ([GET_LOCALIZATION isEqualToString: kThai])
    //    {
    //        [outputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
    //        [outputFormatter setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierGregorian]];
    //    }
    
    NSDate *startDate = [inputFormatter dateFromString: self.btnStartDate.titleLabel.text];
    NSString *startDateString = @"";
    if (startDate)
        startDateString = [outputFormatter stringFromDate: startDate];
    
    NSDate *endDate = [inputFormatter dateFromString: self.btnEndDate.titleLabel.text];
    NSString *endDateString = @"";
    if (endDate)
        endDateString = [outputFormatter stringFromDate: endDate];
    
    NSDictionary *requestParamsDict = @{@"FromDate":startDateString,
                                        @"ToDate":endDateString,
                                        @"OfferType": [mSession convertToRedemptionTypeKeyCode: self.btnType.titleLabel.text]
                                        
                                        };
    
    [[WebServiceManager sharedInstance] fetchPromoRedemptionHistory: requestParamsDict
                                                                 vc:self];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.historyArray count] > 0)
        return [self.historyArray count] + 1;
    else
        return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.tableView && indexPath.row == 0){
        return  60;
    }
    return UITableViewAutomaticDimension;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.row == 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"HistoryHeaderCell"];
        
        UILabel *lblPromoType = [cell viewWithTag: 1];
        lblPromoType.text = LOCALIZATION(C_PROMO_PROMOTYPE);  //lokalised 1 feb
        lblPromoType.font = FONT_H2;
        
//        UILabel *lblDateHeader = [cell viewWithTag: 2];
//        lblDateHeader.font = lblPromoType.font;
//        lblDateHeader.text = LOCALIZATION(C_PROMO_REDEEMEDDATE);
        
        TTTAttributedLabel *lblDateHeader = [cell viewWithTag: 2];
        NSString *text = [NSString stringWithFormat: @"%@\r%@", LOCALIZATION(C_PROMO_REDEEMEDDATE), @"(DD/MM/YY)"];   //lokalised 1 feb
        lblDateHeader.font = FONT_H2;
        
        [lblDateHeader setText:text afterInheritingLabelAttributesAndConfiguringWithBlock:^(NSMutableAttributedString *mutableAttributedString) {
            NSRange whiteRange = [text rangeOfString:@"(DD/MM/YY)"];
            if (whiteRange.location != NSNotFound) {
                // Core Text APIs use C functions without a direct bridge to UIFont. See Apple's "Core Text Programming Guide" to learn how to configure string attributes.
                [mutableAttributedString addAttribute: NSFontAttributeName value: FONT_B3 range:whiteRange];
            }
            
            return mutableAttributedString;
        }];
        lblDateHeader.textAlignment = NSTextAlignmentNatural;
        
        UILabel *lblRedemptionVerificationNumHeader = [cell viewWithTag: 3];
        lblRedemptionVerificationNumHeader.font = lblPromoType.font;
        lblRedemptionVerificationNumHeader.text = LOCALIZATION(C_PROMO_DETAIL_VERIFICATIONNO);   //lokalised 1 feb
        
        if (kIsRightToLeft) {
            lblDateHeader.textAlignment = NSTextAlignmentRight;
        } else {
            lblDateHeader.textAlignment = NSTextAlignmentLeft;
        }
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"HistoryCell"];
        
        NSDictionary *pointsDict = [self.historyArray objectAtIndex: indexPath.row - 1]; //index 0 = header

        UILabel *lblOfferType = [cell viewWithTag: 1];
        lblOfferType.text = ([[pointsDict objectForKey: @"OfferType"] isKindOfClass: [NSNull class]]) ? @"" : [pointsDict objectForKey: @"OfferType"];
        lblOfferType.font = FONT_B2;
        
        UILabel *lblDate = [cell viewWithTag: 2];
        lblDate.font = lblOfferType.font;
        lblDate.text = ([[pointsDict objectForKey: @"RedeemDate"] isKindOfClass: [NSNull class]]) ? @"" : [pointsDict objectForKey: @"RedeemDate"];
        
        UILabel *lblRedemptionVerificationNum = [cell viewWithTag: 3];
        lblRedemptionVerificationNum.font = lblOfferType.font;
        lblRedemptionVerificationNum.text = ([[pointsDict objectForKey: @"OrderReferenceNumber"] isKindOfClass: [NSNull class]]) ? @"" : [pointsDict objectForKey: @"OrderReferenceNumber"];
        
        UIImageView * imgArrow = [cell viewWithTag:4];
        if (kIsRightToLeft) {
            imgArrow.image = [UIImage imageNamed:@"icon-arrsingleL.png"];
        } else {
            imgArrow.image = [UIImage imageNamed:@"icon-arrsingleR.png"];
        }
        
        lblRedemptionVerificationNum.textColor = COLOUR_RED;
    }
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
        return;
    
    self.detailScrollView.contentOffset = CGPointMake(0,0);
    
    NSDictionary *selectedData = [self.historyArray objectAtIndex: indexPath.row - 1]; //index 0 = header
    NSLog(@"selected cell");
    
    self.detailsPopupView.hidden = NO;
    
    self.lblDetailsPopupVehicleIdData.text =            ([[selectedData objectForKey: @"VehicleNumber"] isKindOfClass: [NSNull class]]) ? @"" : [selectedData objectForKey:@"VehicleNumber"];
    self.lblDetailsPopupRedemptionDetailsData.text =    ([[selectedData objectForKey: @"OfferDetails"] isKindOfClass: [NSNull class]]) ? @"" : [selectedData objectForKey:@"OfferDetails"];
    self.lblDetailsPopupProductNameData.text =          ([[selectedData objectForKey: @"ProductName"] isKindOfClass: [NSNull class]]) ? @"" : [selectedData objectForKey:@"ProductName"];
    self.lblDetailsPopupProductCodesData.text =         ([[selectedData objectForKey: @"ProductCodes"] isKindOfClass: [NSNull class]]) ? @"" : [selectedData objectForKey:@"ProductCodes"];
    self.lblDetailsPopupQtyData.text =                  ([[selectedData objectForKey: @"NumberOfScans"] isKindOfClass: [NSNull class]]) ? @"" : [selectedData objectForKey:@"NumberOfScans"];
    self.lblDetailsPopupVerificationNumData.text =      ([[selectedData objectForKey: @"OrderReferenceNumber"] isKindOfClass: [NSNull class]]) ? @"" : [selectedData objectForKey:@"OrderReferenceNumber"];
    self.lblDetailsPopupApprovedByData.text =           ([[selectedData objectForKey: @"ApprovedBy"] isKindOfClass: [NSNull class]]) ? @"" : [selectedData objectForKey:@"ApprovedBy"];
}

- (IBAction)hidePopup:(id)sender
{
    self.detailsPopupView.hidden = YES;
    
    self.lblDetailsPopupVehicleIdData.text = @"";
    self.lblDetailsPopupRedemptionDetailsData.text = @"";
    self.lblDetailsPopupProductNameData.text = @"";
    self.lblDetailsPopupProductCodesData.text = @"";
    self.lblDetailsPopupQtyData.text = @"";
    self.lblDetailsPopupVerificationNumData.text = @"";
    self.lblDetailsPopupApprovedByData.text = @"";
    
}

-(void)processCompleted:(WebServiceResponse *)response
{
    self.historyArray = [[response getGenericResponse] objectForKey: @"PromoRedemptionHistory"];
    
    [self.tableView reloadData];
}
-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
