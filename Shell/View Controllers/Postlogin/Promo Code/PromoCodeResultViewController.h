//
//  PromoCodeResultViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 18/12/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface PromoCodeResultViewController : BaseVC

@property NSDictionary *resultsDict;
@end
