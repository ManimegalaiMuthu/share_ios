//
//  PromoCodeMasterViewController.m
//  Shell
//
//  Created by Admin on 24/11/17.
//  Copyright © 2017 Edenred. All rights reserved.
//
#import "RDVTabBarItem.h"
#import "PromoCodeMasterViewController.h"
#import "PromoCodeHistoryViewController.h"
#import "PromoCodeViewController.h"

@interface PromoCodeMasterViewController (){
    PromoCodeHistoryViewController *promoHistoryVC;
    PromoCodeViewController *promoCodeVC;
}
@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@end

@implementation PromoCodeMasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupInterface];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnPressed:(id)sender {
    [UpdateHUD removeSpotifyHUD:CURRENT_VIEW];
    
    promoCodeVC.shouldLoadTextfieldEndApi = NO;
    
    CATransition *animation = [CATransition animation];
    animation.type = kCATransitionFade;
    animation.duration = 0.4;
    [self.navigationController.view.layer addAnimation:animation forKey:nil];
    
#warning test for popping the only controller
    //    if ([[vc.navigationController childViewControllers] count] > 1)
    
    [self.navigationController popViewControllerAnimated:NO];
}

-(void) registerCustomer:(NSNotification *) notification
{
    NSDictionary *promoDict = notification.object;
    [mSession pushRegisterConsumerOTPViewWithDict:promoDict vc: self];
}

- (void) setupInterface
{
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_PROMOCODE) subtitle: @""];   //lokalise 31 Jan
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(registerCustomer:)
                                                 name:@"CustomerRegister"
                                               object:nil];
    
    if ([self.navigationController.childViewControllers count] > 1)
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    }
//    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_TITLE_PROMOCODE),  LOCALIZATION(C_REWARDS_BACK)];
    promoCodeVC  = [STORYBOARD_PROMOCODE instantiateViewControllerWithIdentifier: VIEW_PROMOTIONCODE];
    UINavigationController *promoCodeNav = [[UINavigationController alloc] initWithRootViewController: promoCodeVC];
    [promoCodeNav setNavigationBarHidden: YES];
    [promoCodeVC view];
    
    promoHistoryVC  = [STORYBOARD_PROMOCODE instantiateViewControllerWithIdentifier: VIEW_PROMOTIONHISTORY];
    UINavigationController *promoHistoryNav = [[UINavigationController alloc] initWithRootViewController: promoHistoryVC];
    [promoHistoryNav setNavigationBarHidden: YES];
    [promoHistoryNav view];
    
    NSArray *title = @[LOCALIZATION(C_PROMO_REDEEMOFFERTAB),        //lokalised 1 Feb
                       LOCALIZATION(C_PROMO_REDEMPTIONHISTORYTAB)]; //lokalised 1 Feb
    [self setViewControllers: @[promoCodeNav,
                                promoHistoryNav]];
    
    if (kIsRightToLeft) {
        title = title.reverseObjectEnumerator.allObjects.mutableCopy;
    }
    
   UIImage *selected = GET_ISADVANCE ? [UIImage imageNamed:@"tab-bg_blue"] : [UIImage imageNamed:@"tab-bg"];
    
    for (int i = 0; i < [self.tabBar.items count]; i++)
    {
        RDVTabBarItem *item = [self.tabBar.items objectAtIndex: i];
        [item setTitle: [title objectAtIndex: i]];
        
        
        [item setBackgroundColor: COLOUR_WHITE];
        [item setTitleUnselectedAttributes:FONT_H1 colour:COLOUR_VERYDARKGREY];
        [item setTitleSelectedAttributes:FONT_H1 colour:COLOUR_RED];
        
        [item setBackgroundSelectedImage: selected withUnselectedImage: nil];
    }
    
    if (kIsRightToLeft) {
        [self tabBar:self.tabBar didSelectItemAtIndex: self.viewControllers.count-1];
    }
}

#pragma mark RDVTabBarDelegate
- (BOOL)tabBar:(RDVTabBar *)tabBar shouldSelectItemAtIndex:(NSInteger)index
{
    for (RDVTabBarItem *item in [tabBar items])
    {
        if (index == 1)
            promoCodeVC.shouldLoadTextfieldEndApi = NO;
    }
    return ([tabBar selectedItem] != [[tabBar items] objectAtIndex: index]);
}

- (void)tabBar:(RDVTabBar *)tabBar didSelectItemAtIndex:(NSInteger)index
{
    //super method MUST BE PRESENT
    [super tabBar:tabBar didSelectItemAtIndex: index];
    
    
    
}

@end
