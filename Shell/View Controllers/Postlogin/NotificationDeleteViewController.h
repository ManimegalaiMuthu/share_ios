//
//  NotificationDeleteViewController.h
//  Shell
//
//  Created by Jeremy Lua on 31/5/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface NotificationDeleteViewController : BaseVC
@property NSMutableArray *notificationArray;

@end
