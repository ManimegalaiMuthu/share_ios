//
//  TermsAndConditionsViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 18/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface TermsAndConditionsViewController : BaseVC
@property NSInteger tncType;
-(void) showAgreeBtn;
@end
