//
//  LeaderboardWebViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 12/10/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "LeaderboardWebViewController.h"
#import <WebKit/WebKit.h>

@interface LeaderboardWebViewController ()<WKNavigationDelegate, UIScrollViewDelegate>

@end

@implementation LeaderboardWebViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
     [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_WORKSHOPPERFORMANCE),LOCALIZATION_EN(C_PERFORMANCE_LEADERBOARD)] screenClass:nil];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
#pragma mark - Adding WKWebView
    WebViewHelper *webViewHelper = [[WebViewHelper alloc] init];
    [webViewHelper setWebViewWithParentView: self.view
                         navigationDelegate: self
                                 urlAddress: @""];
    //    [webViewHelper setWebConfiguration: WKDataDetectorTypeLink | WKDataDetectorTypePhoneNumber];  //default
    
    NSString *urlAddress = [NSString stringWithFormat:@"%@%@%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_LEADERBOARD_URL];
    
    [webViewHelper reloadWebView: urlAddress];
    webViewHelper.webView.scrollView.delegate = self;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return nil;
}


#pragma WKWebView delegate methods
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    [UpdateHUD addMBProgress: self.view withText:LOADING_HUD];
}

-(void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    NSLog(@"failed to load navigation");
    [UpdateHUD removeMBProgress: self.view];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [UpdateHUD removeMBProgress: self.view];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    if(navigationAction.navigationType ==  WKNavigationTypeLinkActivated) {
        NSMutableURLRequest *newRequest = [NSMutableURLRequest requestWithURL:navigationAction.request.URL];
        [newRequest setAllHTTPHeaderFields: [WebserviceHelper prepPostLoginHeader]];
        [webView performSelector:@selector(loadRequest:) withObject:newRequest afterDelay:0];
        
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
    return;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
