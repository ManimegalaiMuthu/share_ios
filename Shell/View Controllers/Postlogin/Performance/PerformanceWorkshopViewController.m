//
//  PerformanceWorkshopViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 24/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "PerformanceTabViewController.h"
#import "PerformanceWorkshopViewController.h"
#define CELL_HEIGHT 60

@interface PerformanceWorkshopViewController () <WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *workshopHeader;
@property (weak, nonatomic) IBOutlet UITableView *workshopTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *workshopTableViewHeight;
@property (weak, nonatomic) IBOutlet UIView *workshopView;

@property (weak, nonatomic) IBOutlet UILabel *performanceHeader;
@property (weak, nonatomic) IBOutlet UIView *performanceHeaderLine;    //to hide in DSR
@property (weak, nonatomic) IBOutlet UITableView *performanceTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *performanceTableViewHeight;

@property (weak, nonatomic) IBOutlet UIView *workshopPerformnace_HeaderBorderView;

@property NSMutableArray *performanceListArray;
@property NSMutableArray *workshopPerformanceListArray;

@end

@implementation PerformanceWorkshopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    //empty view to eliminate extra separators
    self.performanceTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.workshopTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.performanceHeaderLine setBackgroundColor:COLOUR_RED];
    [self.workshopPerformnace_HeaderBorderView setBackgroundColor:COLOUR_RED];
    
    [self.workshopHeader setFont: FONT_H1];
    
    if (![self.parentViewController.parentViewController isKindOfClass: [PerformanceTabViewController class]])
    {
        [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_WORKSHOPPERFORMANCE) subtitle:@"" size:15 subtitleSize:0];   //lokalised 28 Jan
    }
    
    switch (GET_PROFILETYPE)
    {
        case kPROFILETYPE_DSR:
            self.workshopHeader.text = LOCALIZATION(C_PERFORMANCE_TOP5WORKSHOP);    //lokalised 28 Jan
            self.performanceHeader.hidden = YES;
            self.performanceHeaderLine.hidden = YES;
            break;
        case kPROFILETYPE_TRADEOWNER:
//            self.workshopHeader.text = LOCALIZATION(C_PERFORMANCE_WORKSHOP);  //lokalised 28 Jan
            break;
        case kPROFILETYPE_MECHANIC:
        case kPROFILETYPE_FORECOURTATTENDANT:
            [self.workshopView removeFromSuperview];
//            self.workshopHeader.text = LOCALIZATION(C_PERFORMANCE_WORKSHOP);  //lokalised 28 Jan
            break;
        default:
            break;
    }
    
    self.performanceHeader.text = LOCALIZATION(C_PERFORMANCE_PROGRAMPERFORMANCE);   //lokalised 28 Jan
    [self.performanceHeader setFont: self.workshopHeader.font];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
//    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//    [tracker set:kGAIScreenName value:@"Performance workshop"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    switch (GET_PROFILETYPE)
    {
        case kPROFILETYPE_DSR:
            [[WebServiceManager sharedInstance] fetchDSRPerformance: self];
            break;
        case kPROFILETYPE_TRADEOWNER:
            [[WebServiceManager sharedInstance] fetchTradeOwnerPerformance: self];
            break;
        case kPROFILETYPE_MECHANIC:
        case kPROFILETYPE_FORECOURTATTENDANT:
            [[WebServiceManager sharedInstance] fetchTradeOwnerPerformance: self];
            break;
        default:
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.workshopTableView)
        return [self.workshopPerformanceListArray count];
    else
        return [self.performanceListArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *dict;
    if (tableView == self.workshopTableView)
        dict = [self.workshopPerformanceListArray objectAtIndex: indexPath.row];
    else
        dict = [self.performanceListArray objectAtIndex: indexPath.row];
    
    
    UITableViewCell *cell;
    if (GET_PROFILETYPE == kPROFILETYPE_DSR)
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"TopFiveCell"];
        
        UILabel *lblKey = [cell viewWithTag: 1];
        [lblKey setFont: FONT_B1];
        
        
        lblKey.text = [dict objectForKey: @"CompanyName"];
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"WorkshopPerformanceCell"];
        
        UILabel *lblKey = [cell viewWithTag: 1];
        [lblKey setFont: FONT_B1];
        
        UILabel *lblValue = [cell viewWithTag: 2];
        [lblValue setFont: FONT_H(30)];
        [lblValue setTextColor: COLOUR_RED];
        
        
        lblKey.text = [dict objectForKey: @"PerformanceDesc"];
        lblValue.text = [dict objectForKey: @"PerformanceValue"];
    }
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)processCompleted:(WebServiceResponse *)response
{
    NSDictionary *profileData = [response getGenericResponse];
    
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_DSRPERFORMANCE:
            self.workshopPerformanceListArray = [[NSMutableArray alloc] init];
            if (![[profileData objectForKey:@"DSRTopCompanies"] isKindOfClass:[NSNull class]]) {
                for (NSDictionary *dict in [profileData objectForKey: @"DSRTopCompanies"])
                {
                    [self.workshopPerformanceListArray addObject: dict];
                }
            }
            
            self.workshopTableViewHeight.constant = CELL_HEIGHT * [self.workshopPerformanceListArray count];
            [self.workshopTableView reloadData];
            self.performanceTableViewHeight.constant = 0;
            
            break;
        case kWEBSERVICE_TRADEOWNERPERFORMANCE:
            self.workshopPerformanceListArray = [[NSMutableArray alloc] init];
//            if (![[profileData objectForKey:@"FAPerformance"] isKindOfClass:[NSNull class]]) {
//                for (NSDictionary *dict in [profileData objectForKey: @"FAPerformance"])
//                {
//                    [self.workshopPerformanceListArray addObject: dict];
//                }
//                
//                self.workshopHeader.text = @"Forecourt Performance";
//                self.performanceHeader.text = LOCALIZATION(C_PERFORMANCE_WORKSHOP);   //lokalised 28 Jan
//            }
            
            if (![[profileData objectForKey:@"Performance"] isKindOfClass:[NSNull class]]) {
                for (NSDictionary *dict in [profileData objectForKey: @"Performance"])
                {
                    [self.workshopPerformanceListArray addObject: dict];
                }
            }
            
            
            //dont show workshop performance if empty.
           
            self.performanceListArray = [[NSMutableArray alloc] init];
            if(![[profileData objectForKey:@"WorkshopPerformance"]isKindOfClass:[NSNull class]]) {
                for (NSDictionary *dict in [profileData objectForKey: @"WorkshopPerformance"])
                {
                    [self.performanceListArray addObject: dict];
                }
            }
            
            if([self.workshopPerformanceListArray count] == 0)
                [self.workshopView removeFromSuperview];
            
            self.workshopTableViewHeight.constant = CELL_HEIGHT * [self.workshopPerformanceListArray count];
            self.performanceTableViewHeight.constant = CELL_HEIGHT * [self.performanceListArray count];
            
            self.workshopHeader.text = [profileData objectForKey: @"ProgPerfText"];
            self.performanceHeader.text = [profileData objectForKey: @"WSPerfText"];
            
            [self.workshopTableView reloadData];
            [self.performanceTableView reloadData];
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
