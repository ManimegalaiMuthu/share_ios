//
//  THPerformancePointsViewController.m
//  Shell
//
//  Created by Edenred on 2/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "THPerformancePointsViewController.h"
#import "PerformanceTabViewController.h"
#import "TableViewWithEmptyView.h"

@interface THPerformancePointsViewController ()<WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource, CommonListDelegate>

@property (weak, nonatomic) IBOutlet TableViewWithEmptyView *tableView;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet UIButton *btnStartDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEndDate;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIButton *btnType;
@property (weak, nonatomic) IBOutlet UIButton *btnGO;
@property (weak, nonatomic) IBOutlet UILabel *datePickerTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;

@property NSMutableArray *transactionArray;

@end

@implementation THPerformancePointsViewController
@synthesize listView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (![self.parentViewController.parentViewController isKindOfClass: [PerformanceTabViewController class]])
    {
        [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_WORKSHOPPERFORMANCE) subtitle:@"" size:15 subtitleSize:0];   //lokalised 28 Jan
    }
    
    self.datePickerView.hidden = YES;
    if ([GET_LOCALIZATION isEqualToString: kThai])
    {
        [self.datePicker setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
        [self.datePicker setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierBuddhist]];
    } else {
        [self.datePicker setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    }
    
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_POINTS_SEARCHBYTYPE) list: [mSession getTransactionTypes] selectionType:ListSelectionTypeSingle previouslySelected: nil];   //lokalised 28 Jan
    listView.delegate = self;
    
    self.tableView.tableFooterView = [UIView new];
    
    if (GET_PROFILETYPE == kPROFILETYPE_MECHANIC || GET_PROFILETYPE == kPROFILETYPE_FORECOURTATTENDANT)
    {
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_MECHPERFORMANCE) subtitle: @""];  //lokalised 28 Jan
    }
    
    [self setupInterface];
    
    [[WebServiceManager sharedInstance] fetchTransactionHistory:@{@"FromDate":@"",
                                                                  @"ToDate":@"",
                                                                  @"ServiceType": [mSession convertToTransactionTypesKeyCode: self.btnType.titleLabel.text]} vc:self];
    
    //tableView
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 30;
    PlaceholderView *vwPlaceholder = [[PlaceholderView alloc]initWithView:self.tableView];
    [vwPlaceholder setBackgroundColor:[UIColor clearColor]];
    self.tableView.emptyView = vwPlaceholder;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
//    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//    [tracker set:kGAIScreenName value:@"Performance points"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
     [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_WORKSHOPPERFORMANCE),LOCALIZATION_EN(C_TITLE_MYPOINTS)] screenClass:nil];
}

-(void) setupInterface
{
    [Helper setCustomFontButtonContentModes: self.btnType];
    [self.btnType.titleLabel setFont: FONT_B1];
    [self.btnType setTitle: LOCALIZATION(C_POINTS_SEARCHBYTYPE) forState: UIControlStateNormal];    //lokalised 28 Jan
    
    
    [Helper setCustomFontButtonContentModes: self.btnStartDate];
    [self.btnStartDate.titleLabel setFont: FONT_B2];
    [self.btnStartDate setTitle: LOCALIZATION(C_POINTS_STARTDATE) forState: UIControlStateNormal];  //lokalised 28 Jan
    
    [Helper setCustomFontButtonContentModes: self.btnEndDate];
    [self.btnEndDate.titleLabel setFont: FONT_B2];
    [self.btnEndDate setTitle: LOCALIZATION(C_POINTS_ENDDATE) forState: UIControlStateNormal];  //lokalised 28 Jan
    
    [Helper setCustomFontButtonContentModes: self.btnGO];
    [self.btnGO.titleLabel setFont: FONT_B1];
    [self.btnGO setTitle: LOCALIZATION(C_POINTS_GO) forState: UIControlStateNormal];    //lokalised 28 Jan
    
    [self.lblTo setFont: FONT_B2];
    self.lblTo.text =  LOCALIZATION(C_POINTS_TO);   //lokalised 28 Jan
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
}

- (IBAction)typePressed:(UIButton *)sender {
    [listView setTitleAndList: LOCALIZATION(C_POINTS_SEARCHBYTYPE) list: [mSession getTransactionTypes] hasSearchField: NO];    //lokalised 28 Jan
    
    [self popUpView];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        [self.btnType setTitle: selection.firstObject forState:UIControlStateNormal];
        [self goPressed:nil];
    }
}

- (IBAction)startDatePressed:(UIButton *)sender {
    self.datePicker.tag = 1;
    self.datePickerTitle.text = LOCALIZATION(C_POINTS_STARTDATE);   //lokalised 28 Jan
    self.datePickerView.hidden = NO;
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];
    self.datePicker.minimumDate = nil;
    
    //date cannot be greater than end date
    NSDate *endMaxDate = [outputFormatter dateFromString: self.btnEndDate.titleLabel.text];
    if (endMaxDate)
        self.datePicker.maximumDate = endMaxDate;
    else
        self.datePicker.maximumDate = [NSDate date];
}

- (IBAction)toDatePressed:(id)sender
{
    self.datePicker.tag = 2;
    self.datePickerTitle.text = LOCALIZATION(C_POINTS_ENDDATE); //lokalised 28 Jan
    self.datePickerView.hidden = NO;
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];
    
    //date cannot be lesser than start date
    NSDate *startMinDate = [outputFormatter dateFromString: self.btnStartDate.titleLabel.text];
    if (startMinDate)
        self.datePicker.minimumDate = startMinDate;
    else
        self.datePicker.minimumDate = nil;
    
    //end date cannot be greater than today's date
    self.datePicker.maximumDate = [NSDate date];
}

- (IBAction)datePickerDonePressed:(id)sender {
    self.datePickerView.hidden = YES;
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];
    if ([GET_LOCALIZATION isEqualToString: kThai])
    {
        [outputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
        [outputFormatter setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierBuddhist]];
    } else {
        [outputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    }
    if(self.datePicker.tag == 1)
        [self.btnStartDate setTitle:[outputFormatter stringFromDate:self.datePicker.date] forState:UIControlStateNormal];
    else
        [self.btnEndDate setTitle:[outputFormatter stringFromDate:self.datePicker.date] forState:UIControlStateNormal];
    
}
- (IBAction)goPressed:(id)sender {
    self.datePickerView.hidden = YES;
    
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    if ([GET_LOCALIZATION isEqualToString: kThai])
    {
        [inputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
        [inputFormatter setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierBuddhist]];
    } else {
        [inputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    }
    [inputFormatter setDateFormat:@"dd MMM yyyy"];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy/MM/dd"];
    if ([GET_LOCALIZATION isEqualToString: kThai])
    {
        [outputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
        [outputFormatter setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierGregorian]];
    } else {
        [outputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    }
    
    NSDate *startDate = [inputFormatter dateFromString: self.btnStartDate.titleLabel.text];
    NSString *startDateString = @"";
    if (startDate)
        startDateString = [outputFormatter stringFromDate: startDate];
    
    NSDate *endDate = [inputFormatter dateFromString: self.btnEndDate.titleLabel.text];
    NSString *endDateString = @"";
    if (endDate)
        endDateString = [outputFormatter stringFromDate: endDate];
    
    [[WebServiceManager sharedInstance] fetchTransactionHistory: @{@"FromDate":startDateString,
                                                                   @"ToDate":endDateString,
                                                                   @"ServiceType": [mSession convertToTransactionTypesKeyCode: self.btnType.titleLabel.text]} vc:self];
}


-(void)processCompleted:(WebServiceResponse *)response
{
    self.transactionArray = [[response getGenericResponse] objectForKey: @"TransactionHistory"];
    
    [self.tableView reloadData];
}
-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return [self.transactionArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"THPointsCell"];
    
    NSDictionary *pointsDict = [self.transactionArray objectAtIndex: indexPath.row];
    
    UILabel *lblName = [cell viewWithTag: 1];
    lblName.text = [pointsDict objectForKey: @"Description"];
    
    UILabel *lblDate = [cell viewWithTag: 2];
    lblDate.text = [pointsDict objectForKey: @"TransactionDate"];
    
    
    UILabel *lblPoints = [cell viewWithTag: 3];
    lblPoints.font = FONT_H(30);
    if ([[pointsDict objectForKey: @"TypeColor"] intValue] == 1)
        lblPoints.textColor = COLOUR_DARKGREEN;
    else if ([[pointsDict objectForKey: @"TypeColor"] intValue] == 2)
        lblPoints.textColor = COLOUR_LIGHTGREY;
    else
        lblPoints.textColor = COLOUR_RED;
    lblPoints.text = [NSString stringWithFormat: @"+%.0f %@", [[pointsDict objectForKey: @"Points"] floatValue], LOCALIZATION(C_REWARDS_PTS)];  //lokalised 28 Jan
    
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    return cell;
    
}

@end
