//
//  MyPerformanceViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 24/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "MyPerformanceViewController.h"

@interface MyPerformanceViewController () <WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UITableView *performanceTableView;
@property (weak, nonatomic) IBOutlet UIView *borderView;


@property NSMutableArray *performanceArray;
@end

@implementation MyPerformanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.performanceArray = [[NSMutableArray alloc] init];
    
    [[WebServiceManager sharedInstance] fetchDSRPerformance: self];
    self.performanceTableView.tableFooterView = [UIView new];
    self.lblHeader.text = LOCALIZATION(C_PERFORMANCE_PROGRAMPERFORMANCE);   //lokalised 28 Jan
    
    [self.lblHeader setFont: FONT_B1];
    
    [self.borderView setBackgroundColor:COLOUR_RED];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_WORKSHOPPERFORMANCE),LOCALIZATION_EN(C_TITLE_PERFORMANCE)] screenClass:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.performanceArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"MyPerformanceCell" forIndexPath: indexPath];
    
    NSDictionary *performanceData = [self.performanceArray objectAtIndex: indexPath.row];
    
    UILabel *title = [cell viewWithTag: 1]; //tagged in storyboard
    title.text = [performanceData objectForKey: @"PerformanceDesc"];
    [title setFont: FONT_B1];
    
    
    UILabel *lblValue = [cell viewWithTag: 2]; //tagged in storyboard
    lblValue.text = [performanceData objectForKey: @"PerformanceValue"];
    [lblValue setFont: FONT_H(25)];
    [lblValue setTextColor: COLOUR_RED];
    
    return cell;
}

-(void)processCompleted:(WebServiceResponse *)response
{
    self.performanceArray = [[response getGenericResponse] objectForKey: @"Performance"];
    [self.performanceTableView reloadData];
    if(![[[response getGenericResponse] objectForKey:@"ProgPerfText"] isKindOfClass:[NSNull class]]) {
        self.lblHeader.text = [[response getGenericResponse] objectForKey:@"ProgPerfText"];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
