//
//  MyCoinsPerformanceViewController.m
//  Shell
//
//  Created by Nach on 30/12/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "MyCoinsPerformanceViewController.h"
#import "PerformanceTabViewController.h"
#import "CASMonthYearPicker.h"
#import "PopupInfoTableViewController.h"

@interface MyCoinsPerformanceViewController ()<WebServiceManagerDelegate, MonthYearPickerDelegate,UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *lblAvailablePoints;
@property (weak, nonatomic) IBOutlet UILabel *lblAvailablePoints_Data;
@property (weak, nonatomic) IBOutlet UILabel *lblExpires;

@property (weak, nonatomic) IBOutlet UILabel *lblCoinbackInfo;
@property (weak, nonatomic) IBOutlet UIView *coinbackInfoView;

@property (weak, nonatomic) IBOutlet UIView *filterByStatusView;
@property (weak, nonatomic) IBOutlet UIButton *btnFilterByStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnLegend;
@property (weak, nonatomic) IBOutlet UIView *filterByTransactionView;
@property (weak, nonatomic) IBOutlet UIButton *btnFilterByTransaction;
@property (weak, nonatomic) IBOutlet UIView *dateSelectionView;
@property (weak, nonatomic) IBOutlet UIButton *btnStartDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property (weak, nonatomic) IBOutlet UIButton *btnEndDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEnter;

@property (weak, nonatomic) IBOutlet UIView *emptyDataView;
@property (weak, nonatomic) IBOutlet UILabel *lblEmptyData;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property NSDate *startDate;
@property NSDate *endDate;
@property BOOL isStartDate;
@property CASMonthYearPicker *dateView;

@property NSArray *transactionArray;

@end

@implementation MyCoinsPerformanceViewController
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (![self.parentViewController.parentViewController isKindOfClass: [PerformanceTabViewController class]])
    {
        [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_MYCOINS) subtitle:@"" size:15 subtitleSize:0];
    }
    
    self.lblAvailablePoints.text = LOCALIZATION(C_COINS_AVAILABLEBALANCE);
    self.lblAvailablePoints.font = FONT_B(18);
    self.lblAvailablePoints.textColor = COLOUR_VERYDARKGREY;
    
    self.lblAvailablePoints_Data.text = @"";
    self.lblAvailablePoints_Data.font = FONT_H(18);
    self.lblAvailablePoints_Data.textColor = COLOUR_RED;
    
    self.lblCoinbackInfo.text = LOCALIZATION(C_COINS_STARTEARNING);
    self.lblCoinbackInfo.font = FONT_H1;
    self.lblCoinbackInfo.textColor = COLOUR_VERYDARKGREY;
    
    [self.btnFilterByStatus setTitle:LOCALIZATION(C_FILTER_BY_STATUS) forState:UIControlStateNormal];
    self.btnFilterByStatus.titleLabel.textColor = COLOUR_VERYDARKGREY;
    self.btnFilterByStatus.titleLabel.font = FONT_B2;
    
    [self.btnFilterByTransaction setTitle:LOCALIZATION(C_FILTER_BY_TRANSACTION) forState:UIControlStateNormal];
    self.btnFilterByTransaction.titleLabel.textColor = COLOUR_VERYDARKGREY;
    self.btnFilterByTransaction.titleLabel.font = FONT_B2;
    
    [self.btnStartDate setTitle: LOCALIZATION(C_ORDER_STARTDATE) forState: UIControlStateNormal];
    self.btnStartDate.titleLabel.textColor = COLOUR_VERYDARKGREY;
    self.btnStartDate.titleLabel.font = FONT_B2;
    
    [self.btnEndDate setTitle: LOCALIZATION(C_ORDER_ENDDATE) forState: UIControlStateNormal];
    self.btnEndDate.titleLabel.textColor = COLOUR_VERYDARKGREY;
    self.btnEndDate.titleLabel.font = FONT_B2;
    
    self.lblTo.text = LOCALIZATION(C_RU_STEPONE_TO);
    self.lblTo.textColor = COLOUR_VERYDARKGREY;
    self.lblTo.font = FONT_B2;
    
    self.btnEnter.titleLabel.font = FONT_H2;
    [self.btnEnter setTitle:LOCALIZATION(C_POINTS_GO) forState:UIControlStateNormal];
    [self.btnEnter setBackgroundColor:COLOUR_RED];
    
    self.emptyDataView.hidden = YES;
    self.lblEmptyData.text = LOCALIZATION(C_TABLE_NODATAFOUND);
    self.lblEmptyData.textColor = COLOUR_DARKGREY;
    self.lblEmptyData.font = FONT_B1;
    
    self.tableView.hidden = YES;
    [self.tableView setTableFooterView:[UIView new]];
    
    dateView = [[CASMonthYearPicker alloc] initWithTitle:LOCALIZATION(C_PERFORMANCE_SELECTDATE) previouslySelected:nil withYearHidden:NO];
    dateView.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION(C_TITLE_WORKSHOPPERFORMANCE),LOCALIZATION(C_TITLE_MYCOINS)] screenClass:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[WebServiceManager sharedInstance] fetchTransactionHistory:@{@"FromDate":@"",
                                                                     @"ToDate":@"",
                                                                     @"ServiceType": @""} vc:self];
}

-(NSString *) addCommaThousandSeparatorsWithInputString: (NSString *) inputString
{
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setNumberStyle:NSNumberFormatterDecimalStyle]; // to get commas (or locale equivalent)
    [fmt setGroupingSeparator:@","];

    return [fmt stringFromNumber:@([inputString doubleValue])];
}


- (IBAction)coinbackInfoPressed:(UIButton *)sender {
    NSString *urlAddress = [NSString stringWithFormat:@"%@%@%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX,PAGES_COINSPROMO];
    [mSession pushGenericWebViewWith:LOCALIZATION(C_TITLE_COINBACKPROGRAM) andURL:urlAddress onVC:self.parentViewController];
}


- (IBAction)legendPressed:(UIButton *)sender {
    [self.view endEditing: YES];
    
    PopupInfoTableViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPINFOTABLEVIEW];
    
    [self popUpViewWithPopupVC:popupVc];
    
    [popupVc initPointsLegendPopup];
}

- (IBAction)filterByTransactionPressed:(UIButton *)sender {
//    [self popUpListViewWithTitle: LOCALIZATION(C_FILTER_BY_STATUS) listArray:[mSession getTransactionTypes] withSearchField: NO];
}

- (IBAction)filterByStatusPressed:(UIButton *)sender {
    [self popUpListViewWithTitle: LOCALIZATION(C_FILTER_BY_STATUS) listArray:[mSession getTransactionTypes] withSearchField: NO];
}


- (IBAction)startDatePressed:(UIButton *)sender {
    self.isStartDate = YES;
    self.dateView.hidden = NO;
    
    [self popUpDatePicker];
}

- (IBAction)endDatePressed:(UIButton *)sender {
    self.isStartDate = NO;
    self.dateView.hidden = NO;
    
    [self popUpDatePicker];
}

- (IBAction)dateEnterPressed:(UIButton *)sender {
    self.dateView.hidden = YES;
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    [inputFormatter setDateFormat:@"MMM yyyy"];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    [outputFormatter setDateFormat:@"yyyy/MM/dd"];
    
    NSDate *startDate = [inputFormatter dateFromString: self.btnStartDate.titleLabel.text];
    NSString *startDateString = @"";
    if (startDate)
        startDateString = [outputFormatter stringFromDate: startDate];
    
    NSDate *endDate = [inputFormatter dateFromString: self.btnEndDate.titleLabel.text];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* comps = [calendar components:NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitYear fromDate:endDate];
    [comps setMonth:[comps month]+1];
    [comps setDay:0];
    endDate = [calendar dateFromComponents:comps];
    NSString *endDateString = @"";
    if (endDate)
        endDateString = [outputFormatter stringFromDate: endDate];
    
    [[WebServiceManager sharedInstance] fetchTransactionHistory: @{@"FromDate":startDateString,
                                                                   @"ToDate":endDateString,
                                                                   @"ServiceType": [mSession convertToTransactionTypesKeyCode: [self.btnFilterByStatus titleForState:UIControlStateNormal]]} vc:self];
    
}

#pragma Date Picker delegate method
-(void) popUpDatePicker
{
    [self.view endEditing: YES];
    
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}

- (void)selectedMonthYear:(NSDate *)selectedDate
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    
    //setup the necesary date adjustments
    NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: selectedDate];
    [selectedComponent setDay: 1];
    selectedDate = [gregorian dateFromComponents: selectedComponent];
    
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setMonth: 3];
    [offsetComponents setDay: -1];
    
    NSDateFormatter *btnDateFormatter = [[NSDateFormatter alloc] init];
    [btnDateFormatter setDateFormat:@"MMM yyyy"];
    
    [btnDateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    if (self.isStartDate)
    {
        NSDate *offsetDate = [gregorian dateByAddingComponents:offsetComponents toDate: selectedDate options:0];
        
        NSString *selectedDateString;
        NSString *offsetDateString;
        //check end date if more than current date
        if ([offsetDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
        {
            //check start date selection if more than current date.
            if ([selectedDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
            {
                NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
                [selectedComponent setDay: 1];
                selectedDate = [gregorian dateFromComponents: selectedComponent];
            }
            selectedDateString = [btnDateFormatter stringFromDate: selectedDate]; //first of current month
            offsetDateString = [btnDateFormatter stringFromDate: [NSDate date]]; //current date
            offsetDate = [NSDate date]; //set end date to current date
        }
        else
        {
            //start date will never be more than current date if above validation of end date clears.
            selectedDateString = [btnDateFormatter stringFromDate: selectedDate];
            offsetDateString = [btnDateFormatter stringFromDate: offsetDate]; //offset still 2 months later
        }
        
        [self.btnStartDate setTitle: selectedDateString forState: UIControlStateNormal];
        [self.btnEndDate setTitle: offsetDateString forState: UIControlStateNormal];
        self.startDate = selectedDate;
        self.endDate = offsetDate;
        
    }
    else
    {
        //handle only end date
        
        //set date to end of selected month
        NSDateComponents *endOfMonthComponents = [[NSDateComponents alloc] init];
        [endOfMonthComponents setYear: 0];
        [endOfMonthComponents setMonth: 1];
        [endOfMonthComponents setDay: -1];
        selectedDate = [gregorian dateByAddingComponents: endOfMonthComponents toDate: selectedDate options: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)];
        
        //check date from startdate
        NSDate *offsetDate = [gregorian dateByAddingComponents:offsetComponents toDate: self.startDate options:0];
        
        //if offset date is more than current month, change offset to current month
        if ([offsetDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
        {
            //reset offset date to last day of current month
            NSDateComponents *currentDateComponents = [gregorian components: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
            [currentDateComponents setDay: 1];
            [currentDateComponents setMonth: [currentDateComponents month] + 1];
            offsetDate = [gregorian dateFromComponents: currentDateComponents];
            
            [currentDateComponents setDay: -1];
            [currentDateComponents setMonth: 0];
            [currentDateComponents setYear: 0];
            offsetDate = [gregorian dateByAddingComponents: currentDateComponents toDate: offsetDate options:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) ];
        }
        
        //if selection more than threshold (+2 months from start month)
        if ([selectedDate timeIntervalSinceReferenceDate] > [offsetDate timeIntervalSinceReferenceDate])
        {
            selectedDate = offsetDate;
        }
        else if ([selectedDate timeIntervalSinceReferenceDate] < [self.startDate timeIntervalSinceReferenceDate])
        {
            //if selection less than start month
            selectedDate = self.startDate;
        }
        else
        {
            
        }
        
        //don't need to change start date.
        NSString *selectedDateString = [btnDateFormatter stringFromDate: selectedDate];
        
        [self.btnEndDate setTitle: selectedDateString forState: UIControlStateNormal];
        self.endDate = selectedDate;
        
    }
}

#pragma mark - popup Delegate
-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        [self.btnFilterByStatus setTitle:selection.firstObject forState:UIControlStateNormal];
        [self dateEnterPressed:nil];
    }
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.transactionArray count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyPointsHeaderCell"];
    
    TTTAttributedLabel *lblDate = [cell viewWithTag:1];
    NSMutableAttributedString *dateString = [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(C_MANAGEWORKSHOP_IN_DETAIL_TITLE) attributes:@{
                                                                                                                                                          NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,
                                                                                                                                                          NSFontAttributeName: FONT_H2,
                                                                                                                                                          }];
    [dateString appendAttributedString: [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",LOCALIZATION(C_RUSSIA_DATEFORMAT)] attributes:@{
                                                                                                                                                                     NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,
                                                                                                                                                                     NSFontAttributeName: FONT_B(9),
                                                                                                                                                                     }]];
    lblDate.attributedText = dateString;
    
    UILabel *lblOilChange = [cell viewWithTag:2];
    lblOilChange.text = LOCALIZATION(C_POINTS_POINTS);
    lblOilChange.textColor = COLOUR_VERYDARKGREY;
    lblOilChange.font = FONT_H2;
    
    return cell;
}

enum {
    kMYCOINSPOINTSCELL_DATE = 1,
    kMYCOINSPOINTSCELL_DESC,
    kMYCOINSPOINTSCELL_PTS,
    
    kMYCOINSPOINTSCELL_BTN = 999,
}kMYCOINSPOINTSCELL_TAG;

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyPointsTableViewCell"];
    
    NSDictionary *cellData = [self.transactionArray objectAtIndex:indexPath.row];
    
    UILabel *lblDate = [cell viewWithTag:kMYCOINSPOINTSCELL_DATE];
    lblDate.text = [NSString stringWithFormat:@"%@",[cellData objectForKey:@"TransactionDate"]];
    lblDate.textColor = COLOUR_VERYDARKGREY;
    lblDate.font = FONT_B(9);
    
    UILabel *lblDesc = [cell viewWithTag:kMYCOINSPOINTSCELL_DESC];
    lblDesc.text = [NSString stringWithFormat:@"%@",[cellData objectForKey:@"Description"]];
    lblDesc.textColor = COLOUR_VERYDARKGREY;
    lblDesc.font = FONT_H2;
    
    UILabel *lblPts = [cell viewWithTag:kMYCOINSPOINTSCELL_PTS];
    lblPts.text =[NSString stringWithFormat:@"%@",[cellData objectForKey:@"Points"]];
    lblPts.textColor = COLOUR_VERYDARKGREY;
    lblPts.font = FONT_H1;
    
    UIButton *statusBtn = [cell viewWithTag:kMYCOINSPOINTSCELL_BTN];
    UIImage *img = [UIImage imageNamed:@"icn_add_small.png"];
    if ([[cellData objectForKey: @"TypeColor"] intValue] == 1)
        img = [UIImage imageNamed:@"icn_add_small.png"];
    else if([[cellData objectForKey: @"TypeColor"] intValue] == 2)
        img = [UIImage imageNamed:@"icn_minus_small"];
    else if ([[cellData objectForKey: @"TypeColor"] intValue] == 3)
        img = [UIImage imageNamed:@"icn_pending_small.png"];
    [statusBtn setImage:img forState:UIControlStateNormal];
    
    return cell;
}

#pragma mark - WebserviceManager
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_FETCHTRANSACTION:
        {
            self.transactionArray = [[response getGenericResponse] objectForKey: @"TransactionHistory"];
            if(![[[response getGenericResponse] objectForKey: @"EarnedPoints"] isKindOfClass:[NSNull class]]) {
                self.lblAvailablePoints_Data.text = [self addCommaThousandSeparatorsWithInputString:[[response getGenericResponse] objectForKey: @"EarnedPoints"]];
            }
            
            if([self.transactionArray count] > 0) {
                [self.tableView reloadData];
                self.tableView.hidden = NO;
                self.emptyDataView.hidden = YES;
                [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:([self.transactionArray count] + 2) * 75].active = YES;
            } else {
                self.tableView.hidden = YES;
                self.emptyDataView.hidden = NO;
            }
        }
            break;
        default:
            break;
    }
}

@end
