//
//  Performance_BreakdownTableViewCell.h
//  Shell
//
//  Created by Jeremy Lua on 17/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Performance_BreakdownTableViewCell : UITableViewCell
@property NSMutableArray *breakdownArray;
@property NSMutableArray *monthListArray;
@property (weak, nonatomic) IBOutlet UICollectionView *colView;
@end

NS_ASSUME_NONNULL_END
