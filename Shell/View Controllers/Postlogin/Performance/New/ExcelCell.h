//
//  ExcelCell.h
//  Shell
//
//  Created by Jeremy Lua on 18/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExcelCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;

@property (weak, nonatomic) IBOutlet UIView *bottomLineView;
@property (weak, nonatomic) IBOutlet UIView *rightLineView;
@end

NS_ASSUME_NONNULL_END
