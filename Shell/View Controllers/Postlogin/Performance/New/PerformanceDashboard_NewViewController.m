//
//  PerformanceDashboard_NewViewController.m
//  Shell
//
//  Created by Jeremy Lua on 17/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PerformanceDashboard_NewViewController.h"
#import "PerformanceDashboard_HeaderCell.h"

@interface PerformanceDashboard_NewViewController () <WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource, CommonListDelegate>

@property NSMutableArray *summaryDataArray; //contain all data for display

@property (weak, nonatomic) IBOutlet UITableView *performanceTableView;

@property NSInteger selectedSectionIndex; //filtering section to refresh after api. to clear after API response
//@property NSMutableDictionary *selectedSectionDict; //to hold data of selected section. to clear after API response

@property NSInteger selectedDropdownIndex; //for dropdownFilter

@property NSMutableArray *filterDataArray; //list of filter array (for dropdown list)

@property NSString *footerString;
@property NSString *updatedString;

//valuestatement CTA
@property BOOL isShowingCTA;
@property NSString *linkUrlString;
@property NSString *categoryId; //categoryId for library
@end

@implementation PerformanceDashboard_NewViewController
@synthesize listView;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle: self title: LOCALIZATION(C_PERFORMANCE_WORKSHOP) subtitle: @""]; //lokalised 28 Jan
    
    if (!self.tradeID)
        self.tradeID = @"";
    
    self.isTab = self.isTab ? self.isTab : false;
    
    [[WebServiceManager sharedInstance] fetchPerformanceSummary: @{@"IsAll": @(YES),
                                                                   @"FilterByTradeID" : self.tradeID,
                                                                   }
                                                             vc:self];
    
    [self.performanceTableView setBackgroundColor: COLOUR_VERYPALEGREY];    //have to manually set
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_COUNTRY)
                                                   list: [mSession getCountryNames]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];   //lokalised 28 Jan
    listView.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_PERFORMANCE_WORKSHOP) screenClass:nil];
}

enum kPerformanceDate_TYPES
{
    kPerformanceDate_Start = 1,
    kPerformanceDate_End,
    
};


#pragma mark Table delegate


enum
{
    PERFORMANCEDASHBOARD_CONTENT_IMAGE = 1,
    PERFORMANCEDASHBOARD_CONTENT_BODY,
    PERFORMANCEDASHBOARD_CONTENT_DATA,
    PERFORMANCEDASHBOARD_CONTENT_LINE,
    PERFORMANCEDASHBOARD_CONTENT_ARROW,
} PERFORMANCEDASHBOARD_CONTENT_TAG;

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.summaryDataArray count] + 2; //sections + header and footer
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
        return 1;
    else if (section == ([self.summaryDataArray count] + 1))    //footer section
    {
        if (self.isShowingCTA)
            return 2; //text + button
        else
            return 1; //text only
    }
    else
    {
        NSDictionary *sectionSummaryData = [self.summaryDataArray objectAtIndex: section - 1]; //summary data, section 0 is main header
        NSArray *sectionDataArray = [sectionSummaryData objectForKey: @"SectionData"]; //full data of single section
        if ([sectionDataArray count] == 0)
            return 1;
        return [sectionDataArray count];
    }
    return 0;
}

-(void)didPressHeaderDropdown:(UIButton *) sender
{
    NSInteger index = sender.tag;
    if (index > 0 &&  //0 reserved for main header
        index < [self.summaryDataArray count] + 1) //e.g. 2 sections = 2 + 1
    {
        NSDictionary *sectionSummaryData = [self.summaryDataArray objectAtIndex: index - 1];
        if (![[sectionSummaryData objectForKey: @"HasSectionFilter"] boolValue])
            return; //no filter, shouldn't reach here
        self.filterDataArray = [sectionSummaryData objectForKey: @"FilterData"];
        
        NSMutableArray *listArray = [NSMutableArray new];
        for (NSDictionary *dict in self.filterDataArray)
        {
            [listArray addObject: [dict objectForKey: @"KeyValue"]];
        }
        [listView setTitleAndList: LOCALIZATION(C_SUMMARY_VIEWBY) list: listArray hasSearchField: NO];   //lokalised 28 Jan
        
        [self popUpView];
    }
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        
        for (NSDictionary *dict in self.filterDataArray)
        {
            if ([[dict objectForKey: @"KeyValue"] isEqualToString: selection.firstObject])
            {
                self.selectedSectionIndex = [[dict objectForKey: @"SectionID"] integerValue];
                [[WebServiceManager sharedInstance] fetchPerformanceSummary: dict
                                                                         vc:self];
                self.selectedDropdownIndex = [self.filterDataArray indexOfObject: dict];
            }
        }
        self.filterDataArray = nil;
    }
}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

#pragma mark Tableview delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.section == 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"MainHeaderCell"];
        UILabel *lblHeader = [cell viewWithTag: 1];
        lblHeader.text = LOCALIZATION(C_SUMMARY_HEADERTEXT);    //lokalised 28 Jan
        lblHeader.font = FONT_B1;
        
        UILabel *lblUpdated = [cell viewWithTag: 2];
        lblUpdated.text = self.updatedString;
        lblUpdated.textColor = COLOUR_VERYDARKGREY;
        lblUpdated.font = FONT_B3;
        
        [cell setBackgroundColor: [UIColor clearColor]];
    }
    else if (indexPath.section == ([self.summaryDataArray count] + 1))    //footer section
    {
        if (indexPath.row == 0)
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"PerformanceFooterCell"];

            UILabel *lblFooter = [cell viewWithTag: 1];
            lblFooter.text = self.footerString;
            lblFooter.font = FONT_B3;
            
            [cell setBackgroundColor: [UIColor clearColor]];
            return cell;
        }
        else
        {
            //button
            cell = [tableView dequeueReusableCellWithIdentifier: @"PerformanceButtonCell"];
            
            UIButton *btnViewSummary = [cell viewWithTag: 1];
            [btnViewSummary setTitle: LOCALIZATION(C_BUTTON_VIEWVALUESTATEMENT) forState: UIControlStateNormal];    //lokalised 28 Jan
            //target already set in storyboard
            [btnViewSummary.titleLabel setFont: FONT_BUTTON];
            [btnViewSummary setBackgroundColor:COLOUR_RED];
            
            [cell setBackgroundColor: [UIColor clearColor]];
        }
    }
    else
    {
        NSDictionary *sectionSummaryData = [self.summaryDataArray objectAtIndex: indexPath.section - 1]; //summary data, section 0 is main header
        NSArray *sectionDataArray = [sectionSummaryData objectForKey: @"SectionData"]; //full data of single section
        
        if ([sectionDataArray count] == 0)
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"NoDataCell"];
            
            UILabel *nodataLabel = [cell viewWithTag:1];
            nodataLabel.text = LOCALIZATION(C_TABLE_NODATAFOUND);   //lokalised 11 Feb
            nodataLabel.font = FONT_B1;
            nodataLabel.textColor = COLOUR_VERYDARKGREY;
            
            return cell;
        }
        
        NSDictionary *cellData = [sectionDataArray objectAtIndex: indexPath.row]; //individual section data
        
        cell = [tableView dequeueReusableCellWithIdentifier: @"PerformanceCell"];
        
        UIImageView *imgView = [cell viewWithTag: PERFORMANCEDASHBOARD_CONTENT_IMAGE];
        
#define PERFORMANCE_CONSUMERS               @"CustomerBreakDown"
#define PERFORMANCE_DECAL                   @"Decal"
#define PERFORMANCE_FA                      @"ForecourtAttendant"
#define PERFORMANCE_OILCHANGE               @"OilChangeBreakDown"
#define PERFORMANCE_ORDERS                  @"Orders"
#define PERFORMANCE_PENDINGWORKSHOPS        @"PendingWorkshop"
#define PERFORMANCE_POINTS                  @"PointsBreakDown"
#define PERFORMANCE_PSCANNED                @"ProductScanBreakDown"
#define PERFORMANCE_REDEMPTIONS             @"Redemptions"
#define PERFORMANCE_REFERRALS               @"Referrals"
#define PERFORMANCE_STAFF                   @"Staff"
#define PERFORMANCE_TOTALWORKSHOPS          @"StaffBreakDown"
#define PERFORMANCE_WAPPROVED               @"ApprovedWorkshop"
#define PERFORMANCE_LCDSRBREAKDOWN         @"LCDSRBreakDown"
#define PERFORMANCE_LCTOBREAKDOWN            @"LCTOBreakDown"
        
        imgView.image = [UIImage imageNamed: [cellData objectForKey: @"LinkIcon"]];
        
//        icn_oilchange_verydarkgrey
//        icn_pscanned_verydarkgrey
//        icn_points_verydarkgrey
//        icn_redemptions_verydarkgrey
//        icn_orders_verydarkgrey
//        icn_decal_verydarkgrey.png
//        icn_wapproved_verydarkgrey.png
//        icn_pendingworkshops_verydarkgrey
//        icn_totalworkshops_verydarkgrey
//        icn_staff_verydarkgrey
//        icn_consumers_verydarkgrey.png
//        icn_fa_verydarkgrey.png
//        icn_referrals_verydarkgrey.png
        
//        if ([[cellData objectForKey: @"LinkIcon"] isEqualToString: PERFORMANCE_CONSUMERS])
//            imgView.image = [UIImage imageNamed: @"icn_consumers_verydarkgrey.png"];
//        else if ([[cellData objectForKey: @"LinkIcon"] isEqualToString:PERFORMANCE_DECAL])
//            imgView.image = [UIImage imageNamed: @"icn_decal_verydarkgrey.png"];
//        else if ([[cellData objectForKey: @"LinkIcon"] isEqualToString:PERFORMANCE_FA])
//            imgView.image = [UIImage imageNamed: @"icn_fa_verydarkgrey.png"];
//        else if ([[cellData objectForKey: @"LinkIcon"] isEqualToString: PERFORMANCE_OILCHANGE])
//            imgView.image = [UIImage imageNamed: @"icn_oilchange_verydarkgrey.png"];
//        else if ([[cellData objectForKey: @"LinkIcon"] isEqualToString:PERFORMANCE_ORDERS])
//            imgView.image = [UIImage imageNamed: @"icn_orders_verydarkgrey.png"];
//        else if ([[cellData objectForKey: @"LinkIcon"] isEqualToString:PERFORMANCE_PENDINGWORKSHOPS])
//            imgView.image = [UIImage imageNamed: @"icn_pendingworkshops_verydarkgrey.png"];
//        else if ([[cellData objectForKey: @"LinkIcon"] isEqualToString:PERFORMANCE_POINTS])
//            imgView.image = [UIImage imageNamed: @"icn_points_verydarkgrey.png"];
//        else if ([[cellData objectForKey: @"LinkIcon"] isEqualToString:PERFORMANCE_PSCANNED])
//            imgView.image = [UIImage imageNamed: @"icn_pscanned_verydarkgrey.png"];
//        else if ([[cellData objectForKey: @"LinkIcon"] isEqualToString:PERFORMANCE_REDEMPTIONS])
//            imgView.image = [UIImage imageNamed: @"icn_redemptions_verydarkgrey.png"];
//        else if ([[cellData objectForKey: @"LinkIcon"] isEqualToString:PERFORMANCE_REFERRALS])
//            imgView.image = [UIImage imageNamed: @"icn_referrals_verydarkgrey.png.png"];
//        else if ([[cellData objectForKey: @"LinkIcon"] isEqualToString:PERFORMANCE_STAFF])
//            imgView.image = [UIImage imageNamed: @"icn_staff_verydarkgrey.png"];
//        else if ([[cellData objectForKey: @"LinkIcon"] isEqualToString:PERFORMANCE_TOTALWORKSHOPS])
//            imgView.image = [UIImage imageNamed: @"icn_totalworkshops_verydarkgrey.png"];
//        else if ([[cellData objectForKey: @"LinkIcon"] isEqualToString:PERFORMANCE_WAPPROVED])
//            imgView.image = [UIImage imageNamed: @"icn_wapproved_verydarkgrey.png"];
//        else if ([[cellData objectForKey: @"LinkIcon"] isEqualToString:PERFORMANCE_LOYALTYCONTRACT])
//            imgView.image = [UIImage imageNamed: @"icn_loyalty_verydarkgrey.png"];
//        else if ([[cellData objectForKey: @"LinkIcon"] isEqualToString:PERFORMANCE_TOTALCARTONS])
//            imgView.image = [UIImage imageNamed: @"icn_totalcartons_verydarkgrey.png"];
        
        UILabel *lblBody = [cell viewWithTag: PERFORMANCEDASHBOARD_CONTENT_BODY];
        lblBody.text = [cellData objectForKey: @"Name"];
        lblBody.font = FONT_B1;
        
        UILabel *lblData = [cell viewWithTag: PERFORMANCEDASHBOARD_CONTENT_DATA];
        lblData.text = [cellData objectForKey: @"Total"];
        lblData.font = FONT_H(25);
        lblData.textColor = COLOUR_RED;
        
        if (indexPath.row == [sectionDataArray count] - 1)
        {
            UIView *line = [cell viewWithTag: PERFORMANCEDASHBOARD_CONTENT_LINE];
            line.hidden = YES;
        }
        
        UIImageView *arrowImgView = [cell viewWithTag: PERFORMANCEDASHBOARD_CONTENT_ARROW];
        arrowImgView.hidden = [[cellData objectForKey:@"LinkUrl"] isEqualToString:@""];
        if (kIsRightToLeft) {
            arrowImgView.image = [UIImage imageNamed:@"icon-greyarrL.png"];
        } else {
            arrowImgView.image = [UIImage imageNamed:@"icon-greyarrR.png"];
        }
        
        [cell setBackgroundColor: COLOUR_WHITE];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section > 0 &&  //0 reserved for main header
        indexPath.section < [self.summaryDataArray count] + 1) //e.g. 2 sections = 2 + 1
    {
        NSDictionary *selectedSectionSummaryData = [self.summaryDataArray objectAtIndex: indexPath.section - 1]; //summary data, section 0 is main header
        NSString *sectionNameString = [selectedSectionSummaryData objectForKey: @"SectionName"];
        NSArray *selectedSectionDataArray = [selectedSectionSummaryData objectForKey: @"SectionData"]; //full data of single section for processing
        
        NSDictionary *selectedCellData = [selectedSectionDataArray objectAtIndex: indexPath.row]; //individual section data
        if ([[selectedCellData objectForKey: @"IsInCarousel"] boolValue])
        {
            int selectedIndex = 0;
            int i = 0;
            NSMutableArray *detailsSectionDataArray = [NSMutableArray new]; //prepare new array to take out unnecessary sections
            //process data to push to detail screen
            for (NSDictionary *sectionSummaryData in self.summaryDataArray)
            {
                //cannot use indexPath.row as index might change when go into details
                //some categories may not make it into category list due to IsInCarousel key
                for (NSDictionary *dict in [sectionSummaryData objectForKey: @"SectionData"])
                {
                    //if selected cell is same as current data, mark selected index
                    if ([[selectedCellData objectForKey: @"Name"] isEqualToString: [dict objectForKey: @"Name"]])
                    {
                        selectedIndex = i;
                    }
                    if ([[dict objectForKey: @"IsInCarousel"] boolValue])
                    {
                        [detailsSectionDataArray addObject: dict];
                        i++;    //increase index for next
                    }
                }
            }   //end of for loop
            
            //check to go details screen or not. Must be outside for loop
            if(_isTab) {
                [mSession pushPerformanceDetails: detailsSectionDataArray
                                         vcTitle: sectionNameString
                                summaryDataArray: self.summaryDataArray
                                 originalTradeId: self.tradeID
                                   selectedIndex: selectedIndex
                                              vc: self.parentViewController.parentViewController];
            } else {
                [mSession pushPerformanceDetails: detailsSectionDataArray
                                         vcTitle: sectionNameString
                                summaryDataArray: self.summaryDataArray
                                 originalTradeId: self.tradeID
                                   selectedIndex: selectedIndex
                                              vc: self];
                
            }
        }
        else
        {
            //go other screens
            if ([[selectedCellData objectForKey: @"LinkUrl"] isEqualToString: PERFORMANCE_PENDINGWORKSHOPS])
            {
                [mSession loadManageWorkshopView];
            }
            else if ([[selectedCellData objectForKey: @"LinkUrl"] isEqualToString: PERFORMANCE_WAPPROVED])
            {
                [mSession loadApprovedWorkshop];
            }
            else if ([[selectedCellData objectForKey: @"LinkUrl"] isEqualToString: PERFORMANCE_LCDSRBREAKDOWN])
            {
                [mSession loadLoyaltyContractProgramPerformance];
            }
            else if ([[selectedCellData objectForKey: @"LinkUrl"] isEqualToString: PERFORMANCE_LCTOBREAKDOWN])
            {
                if(_isTab) {
                    [mSession pushLoyaltyCartonBreakdown: GET_PROGRAMBREAKDOWN vc: self.parentViewController.parentViewController];
                } else {
                    [mSession pushLoyaltyCartonBreakdown: GET_PROGRAMBREAKDOWN vc: self];
                }
            }
        }
//        NSString *navKey = [cellData objectForKey: @"OnClickRedirectPath"];
//
//        if ([navKey isEqualToString: @"Order BreakDown"])
//        {
//            //parent is tab controller
//            [mSession pushOrderBreakdown: self.parentViewController.parentViewController];
//        }
//        else if ([navKey isEqualToString: @"Staff BreakDown"])
//        {
//            [mSession pushStaffBreakdown:self.parentViewController.parentViewController];
//        }
//        else if ([navKey isEqualToString: @"Oil Change BreakDown"])
//        {
//            [mSession pushOilChangeBreakdown:self.parentViewController.parentViewController];
//        }
//        else if ([navKey isEqualToString: @"Customer BreakDown"])
//        {
//            [mSession pushCustomerBreakdown:self.parentViewController.parentViewController];
//        }
//        else if ([navKey isEqualToString: @"Offer Redemption BreakDown"])
//        {
//            [mSession pushOfferRedemptionBreakdown:self.parentViewController.parentViewController];
//        }
//        else if([navKey isEqualToString:@"Points Earned BreakDown"]) {
//            [mSession pushPointsBreakdown: self.parentViewController.parentViewController];
//        }
    }
    
}

- (IBAction)valueStatementPressed:(id)sender
{
    //redirect directly to value statement
    [mSession loadPerformanceCTA: self.categoryId];

}


enum
{
    TABLE_HEADER_MAIN = 1,
    TABLE_HEADER_SUB,
} TABLE_HEADER;

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section > 0 &&  //0 reserved for main header
        section < [self.summaryDataArray count] + 1) //e.g. 2 sections = 2 + 1
        return UITableViewAutomaticDimension;
    return 0;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section > 0 &&  //0 reserved for main header
        section < [self.summaryDataArray count] + 1) //e.g. 2 sections = 2 + 1
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"PerformanceFooterSpaceCell"];
        
        return cell.contentView;
    }
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section > 0 &&  //0 reserved for main header
        section < [self.summaryDataArray count] + 1) //e.g. 2 sections = 2 + 1
        return 30.0;
    return 5;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section > 0 &&  //0 reserved for main header
        section < [self.summaryDataArray count] + 1) //e.g. 2 sections = 2 + 1
    {
//        NSArray *nib = [[NSBundle mainBundle] loadNibNamed: @"PerformanceDashboard_HeaderCell" owner:self options:nil];
//        PerformanceDashboard_HeaderCell *cell = [nib objectAtIndex:0];

        PerformanceDashboard_HeaderCell *cell = [tableView dequeueReusableCellWithIdentifier: @"PerformanceHeaderCell"];
        
        NSDictionary *sectionData = [self.summaryDataArray objectAtIndex: section - 1];

        if ([[sectionData objectForKey: @"SectionID"] isEqualToString: @"8"])
            cell.lineView.backgroundColor = COLOUR_YELLOW;
        else
            cell.lineView.backgroundColor = COLOUR_RED;
        
        
        cell.dropdownListArray = [NSMutableArray new];
        if ([[sectionData objectForKey: @"HasSectionFilter"] boolValue])
        {
            //has filter
            [cell setButtonEnabled: YES];
            cell.sectionIndex = [[sectionData valueForKey: @"SectionID"] intValue];
            
            [cell.btnHeader addTarget: cell action: @selector(didPressHeaderDropdown:) forControlEvents: UIControlEventTouchUpInside];
            cell.btnHeader.tag = section;   //to be in line with [self.summaryDataArray objectAtIndex: index - 1]; section = index - 1
            
            for (NSDictionary *dict in [sectionData objectForKey: @"FilterData"])
            {
                [cell.dropdownListArray addObject: [dict objectForKey: @"KeyValue"]];
            }
//            cell.lblHeader.text =  [cell.dropdownListArray firstObject]; //first item of filter data keyvalue
            cell.lblHeader.text = [cell.dropdownListArray objectAtIndex: self.selectedDropdownIndex];
        }
        else
        {
            [cell setButtonEnabled: NO];
            cell.lblHeader.text = [sectionData objectForKey: @"SectionName"];
        }
        cell.lblHeader.font = FONT_H1;
        cell.lblHeader.textColor = COLOUR_VERYDARKGREY;
        
        [cell setBackgroundColor: COLOUR_WHITE];
        return cell.contentView;
    }
    return [UIView new];
    
}

- (IBAction)viewSummaryPressed:(id)sender
{
//    [mSession loadRUPartners];
}



//#pragma mark Date methods
//- (IBAction)startDatePress:(id)sender
//{
//    self.datePicker.tag = kPerformanceDate_Start;
////    self.datePickerTitle.text = LOCALIZATION(C_POINTS_STARTDATE);   //lokalised 28 Jan
//    self.datePickerView.hidden = NO;
//    
//    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
//    [outputFormatter setDateFormat:@"dd MMM yyyy"];
//    self.datePicker.minimumDate = nil;
//    
//    //date cannot be greater than end date
//    NSDate *endMaxDate = [outputFormatter dateFromString: self.btnEndDate.titleLabel.text];
//    if (endMaxDate)
//        self.datePicker.maximumDate = endMaxDate;
//    else
//        self.datePicker.maximumDate = [NSDate date];
//}
//
//- (IBAction)toDatePressed:(id)sender
//{
//    self.datePicker.tag = kPerformanceDate_End;
////    self.datePickerTitle.text = LOCALIZATION(C_POINTS_ENDDATE); //lokalised 28 Jan
//    self.datePickerView.hidden = NO;
//    
//    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
//    [outputFormatter setDateFormat:@"dd MMM yyyy"];
//    
//    //date cannot be lesser than start date
//    NSDate *startMinDate = [outputFormatter dateFromString: self.btnStartDate.titleLabel.text];
//    if (startMinDate)
//        self.datePicker.minimumDate = startMinDate;
//    else
//        self.datePicker.minimumDate = nil;
//    
//    //end date cannot be greater than today's date
//    self.datePicker.maximumDate = [NSDate date];
//}
//
//- (IBAction)datePickerDonePressed:(id)sender {
//    self.datePickerView.hidden = YES;
//    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
//    [outputFormatter setDateFormat:@"dd MMM yyyy"];
//    if ([GET_LOCALIZATION isEqualToString: kThai])
//    {
//        [outputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
//        [outputFormatter setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierBuddhist]];
//    }
//    if(self.datePicker.tag == 1)
//        [self.btnStartDate setTitle:[outputFormatter stringFromDate:self.datePicker.date] forState:UIControlStateNormal];
//    else
//        [self.btnEndDate setTitle:[outputFormatter stringFromDate:self.datePicker.date] forState:UIControlStateNormal];
//    
//}
//- (IBAction)goPressed:(id)sender {
//    self.datePickerView.hidden = YES;
//    
//    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
//    if ([GET_LOCALIZATION isEqualToString: kThai])
//    {
//        [inputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
//        [inputFormatter setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierBuddhist]];
//    }
//    [inputFormatter setDateFormat:@"dd MMM yyyy"];
//    
//    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
//    [outputFormatter setDateFormat:@"yyyy/MM/dd"];
//    if ([GET_LOCALIZATION isEqualToString: kThai])
//    {
//        [outputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
//        [outputFormatter setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierGregorian]];
//    }
//    
//    NSDate *startDate = [inputFormatter dateFromString: self.btnStartDate.titleLabel.text];
//    NSString *startDateString = @"";
//    if (startDate)
//        startDateString = [outputFormatter stringFromDate: startDate];
//    
//    NSDate *endDate = [inputFormatter dateFromString: self.btnEndDate.titleLabel.text];
//    NSString *endDateString = @"";
//    if (endDate)
//        endDateString = [outputFormatter stringFromDate: endDate];
//    
////    [[WebServiceManager sharedInstance] fetchTransactionHistory: @{@"FromDate":startDateString,
////                                                                   @"ToDate":endDateString,
////                                                                   @"ServiceType": [mSession convertToTransactionTypesKeyCode: self.btnType.titleLabel.text]} vc:self];
//}



-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_FETCHPERFORMANCESUMMARY:
        {
            if (self.selectedSectionIndex > 0)
            {
                //replace data of selected section.
                NSMutableDictionary *respData = [[[response getGenericResponse] objectForKey: @"SummaryData"] firstObject];
                //summary data index = sectionId - 1
                [self.summaryDataArray replaceObjectAtIndex: self.selectedSectionIndex - 1 withObject: respData];
            }
            else
            {
                self.summaryDataArray = [[response getGenericResponse] objectForKey: @"SummaryData"];
            }
            
            if([self.summaryDataArray isKindOfClass:[NSNull class]]) {
                self.summaryDataArray = @[];
            }
            self.isShowingCTA = [[[response getGenericResponse] objectForKey: @"HasValueStatement"] boolValue]; //for value statement cta
            self.categoryId = [[response getGenericResponse] objectForKey: @"CategoryID"];  //for value statement cta
            
            self.linkUrlString = [[response getGenericResponse] objectForKey: @"LinkUrl"];
            self.updatedString = [[response getGenericResponse] objectForKey: @"Text"];
            self.footerString = [[response getGenericResponse] objectForKey: @"Text2"];
            if([self.updatedString isKindOfClass:[NSNull class]]) {
                self.updatedString = @"";
            }
            if([self.footerString isKindOfClass:[NSNull class]]) {
                self.footerString = @"";
            }
            [self.performanceTableView reloadData];
            self.selectedSectionIndex = 0;
        }
            break;
        case kWEBSERVICE_FETCHSECTIONBREAKDOWN:
            
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
