//
//  PerformanceDashboard_HeaderCell.h
//  Shell
//
//  Created by Jeremy Lua on 17/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PerformanceDashboard_HeaderCell : UITableViewCell

@property NSInteger sectionIndex;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UIImageView *imgDropdown;
@property (weak, nonatomic) IBOutlet UIButton *btnHeader;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property NSMutableArray *dropdownListArray;

-(void) setButtonEnabled:(BOOL) enabled;
@end

NS_ASSUME_NONNULL_END
