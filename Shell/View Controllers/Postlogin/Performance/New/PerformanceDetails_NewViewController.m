//
//  PerformanceDetails_NewViewController.m
//  Shell
//
//  Created by Jeremy Lua on 17/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PerformanceDetails_NewViewController.h"

#import "PerformanceChart_NewViewController.h"      //circle chart

@interface PerformanceDetails_NewViewController () <WebServiceManagerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, PerformanceChart_NewViewDelegate>

@property PerformanceChart_NewViewController *performanceChartVc;

@property (weak, nonatomic) IBOutlet UICollectionView *categoryColView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *mainCategoryColViewLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *categoryColViewHeight;

@property (weak, nonatomic) IBOutlet UIView *vcContainerView;
@property NSMutableArray *mainCategoryCellArray;    //to hold cell references

@property NSDate *startDate;
@property NSDate *endDate;
@property NSString *startDateString;
@property NSString *endDateString;
@property NSString *tradeIdString;


@end

@implementation PerformanceDetails_NewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle: self title: self.vcTitle subtitle: @""];
    self.mainCategoryCellArray = [NSMutableArray new];
    self.mainCategoryColViewLayout.estimatedItemSize = CGSizeMake(120, 50);

    if ([self.originalTradeId length] > 0)
        self.tradeIdString = self.originalTradeId;
    
    [self setupInterface];
    
    [self setNavBackBtn];
    if(kIsRightToLeft) {
        NSInteger actualIndex = self.mainCategoryArray.count-self.selectedIndex-1;
        [self.categoryColView scrollToItemAtIndexPath: [NSIndexPath indexPathForItem:actualIndex inSection:0] atScrollPosition: UICollectionViewScrollPositionRight animated:NO];
    } else {
        [self.categoryColView scrollToItemAtIndexPath: [NSIndexPath indexPathForItem:self.selectedIndex inSection:0] atScrollPosition: UICollectionViewScrollPositionRight animated:NO];
    }
}


-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
//        case kWEBSERVICE_FETCHSECTIONBREAKDOWN:
//        {
//            NSDictionary *respData = [response getGenericResponse];
//        }
//            break;
//
        default:
            break;
    }
}

-(void) setupInterface
{
    //fetch list
    NSDictionary *cellData = [self.mainCategoryArray objectAtIndex: self.selectedIndex];
    
    self.performanceChartVc = [STORYBOARD_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_PERFORMANCE_CHART_NEW];
    
    self.performanceChartVc.groupDataDict = cellData;
    self.performanceChartVc.delegate = self;
    self.performanceChartVc.groupDataDict = cellData;
    
    self.performanceChartVc.startDate = self.startDate;
    self.performanceChartVc.startDateString = self.startDateString;
    
    self.performanceChartVc.endDate = self.endDate;
    self.performanceChartVc.endDateString = self.endDateString;
    self.performanceChartVc.staffTradeIdString = self.tradeIdString;
    
    [self addChildViewController: self.performanceChartVc];
    [self.vcContainerView addSubview: self.performanceChartVc.view];
    [self.performanceChartVc.view setFrame: CGRectMake(0, 0, self.vcContainerView.frame.size.width, self.vcContainerView.frame.size.height)];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    //    [[WebServiceManager sharedInstance] fetchPointsSummary: self];
    
    [self.categoryColView.collectionViewLayout invalidateLayout];
    [self.categoryColView reloadData];
    
    [self.categoryColView selectItemAtIndexPath:[NSIndexPath indexPathForItem:self.selectedIndex inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
    
}

#pragma mark Collection View for each category
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    //    if (collectionView == self.categoryColView)
    {
        return [self.mainCategoryArray count];
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger actualIndex = indexPath.row;
    if (kIsRightToLeft) {
        actualIndex = self.mainCategoryArray.count - indexPath.row - 1;
    }
    UICollectionViewCell *cell;
    //    if (collectionView == self.categoryColView)
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"CategoryCell" forIndexPath:indexPath];
        
        NSDictionary *cellData = [self.mainCategoryArray objectAtIndex: actualIndex];
        
        UILabel *lblTitle = [cell viewWithTag: 1];
        lblTitle.font = FONT_H1;
        lblTitle.text = [cellData objectForKey: @"Name"];
        
        UIView *bottomLine = [cell viewWithTag: 2];
        if (actualIndex == self.selectedIndex)
        {
            lblTitle.textColor = COLOUR_RED;
            bottomLine.backgroundColor = COLOUR_RED;
            bottomLine.hidden = NO;
            
            for (NSDictionary *dict in self.summaryDataArray)
            {
                for (NSDictionary *groupDataDict in [dict objectForKey: @"SectionData"])
                {
                    if ([[cellData objectForKey: @"GroupID"] isEqualToString: [groupDataDict objectForKey: @"GroupID"]])
                    {
                        [FIRAnalytics logEventWithName: @"PerformanceDetails"
                        parameters:@{
                                     @"SectionID" : [dict objectForKey: @"SectionID"],
                                     @"SectionName":[dict objectForKey: @"SectionName"],
                                     @"GroupID": [cellData objectForKey: @"GroupID"],
                                     @"GroupName": [cellData objectForKey: @"Name"],
                                     }];
                        break;
                        break;
                    }
                }
            }
        }
        else
        {
            lblTitle.textColor = COLOUR_VERYDARKGREY;
            bottomLine.hidden = YES;
        }
    }
    [self.mainCategoryCellArray addObject: cell];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger actualIndex = indexPath.row;
    if (kIsRightToLeft) {
        actualIndex = self.mainCategoryArray.count - indexPath.row - 1;
    }
    if (collectionView == self.categoryColView)
    {
        for (UICollectionViewCell *cell in self.mainCategoryCellArray)
        {
            UIView *bottomLine = [cell viewWithTag: 2];
            bottomLine.hidden = YES;
            
            UILabel *lblTitle = [cell viewWithTag: 1];
            lblTitle.textColor = COLOUR_VERYDARKGREY;
            bottomLine.hidden = YES;
        }
        
        UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath: indexPath];
        UILabel *lblTitle = [cell viewWithTag: 1];
        lblTitle.textColor = COLOUR_RED;
        
        UIView *bottomLine = [cell viewWithTag: 2];
        [bottomLine setBackgroundColor:COLOUR_RED];
        bottomLine.hidden = NO;
        
        self.selectedIndex = actualIndex;
        
        //fetch list
        NSDictionary *cellData = [self.mainCategoryArray objectAtIndex: self.selectedIndex];

        //check for title
        for (NSDictionary *dict in self.summaryDataArray)
        {
            for (NSDictionary *groupDataDict in [dict objectForKey: @"SectionData"])
            {
                if ([[cellData objectForKey: @"GroupID"] isEqualToString: [groupDataDict objectForKey: @"GroupID"]])
                {
                    [FIRAnalytics logEventWithName: @"PerformanceDetails"
                    parameters:@{
                                 @"SectionID" : [dict objectForKey: @"SectionID"],
                                 @"SectionName":[dict objectForKey: @"SectionName"],
                                 @"GroupID": [cellData objectForKey: @"GroupID"],
                                 @"GroupName": [cellData objectForKey: @"Name"],
                                 }];
                    self.vcTitle = [dict objectForKey: @"SectionName"];
                    [Helper setNavigationBarTitle: self title: self.vcTitle subtitle: @""];
                    break;
                    break;
                }
            }
        }
        
        [self.performanceChartVc removeFromParentViewController];
        [self.performanceChartVc.view removeFromSuperview];
        
        self.performanceChartVc = [STORYBOARD_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_PERFORMANCE_CHART_NEW];
        
        self.performanceChartVc.delegate = self;
        self.performanceChartVc.groupDataDict = cellData;
        
        self.performanceChartVc.startDate = self.startDate;
        self.performanceChartVc.startDateString = self.startDateString;
        
        self.performanceChartVc.endDate = self.endDate;
        self.performanceChartVc.endDateString = self.endDateString;
        self.performanceChartVc.staffTradeIdString = self.tradeIdString;
        
        [self addChildViewController: self.performanceChartVc];
        [self.vcContainerView addSubview: self.performanceChartVc.view];
        [self.performanceChartVc.view setFrame: CGRectMake(0, 0, self.vcContainerView.frame.size.width, self.vcContainerView.frame.size.height)];
 
//        if (![categoryData objectForKey: @"KeyCode"])
//            self.itemListVc.categoryCode = @"";
//        else if ([[categoryData objectForKey:@"KeyCode"]isEqualToString:BLANK])
//            self.itemListVc.categoryCode = @"";
//        else
//            self.itemListVc.categoryCode = [categoryData objectForKey: @"KeyCode"];
//        [self.itemListVc performSelector: @selector(refreshDataWithCategoryCode) withObject:nil afterDelay:0.1f];
    }
}

-(void) updateDatesWithStartDate:(NSDate *) startDate startDateString:(NSString *)startDateString endDate:(NSDate *) endDate endDateString:(NSString *) endDateString
{
    self.startDate = startDate;
    self.endDate = endDate;
    self.startDateString = startDateString;
    self.endDateString = endDateString;
}

-(void) updateDatesWithStartDate:(NSDate *) startDate startDateString:(NSString *)startDateString endDate:(NSDate *) endDate endDateString:(nonnull NSString *)endDateString tradeIdString:(nonnull NSString *)tradeIdString
{
    [self updateDatesWithStartDate: startDate startDateString:startDateString endDate:endDate endDateString:endDateString];
    
    self.tradeIdString = tradeIdString;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
