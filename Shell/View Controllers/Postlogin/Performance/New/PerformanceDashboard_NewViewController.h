//
//  PerformanceDashboard_NewViewController.h
//  Shell
//
//  Created by Jeremy Lua on 17/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface PerformanceDashboard_NewViewController : BaseVC

@property NSString *tradeID; //for approve workshop performance tab
@property BOOL isTab;
@end

NS_ASSUME_NONNULL_END
