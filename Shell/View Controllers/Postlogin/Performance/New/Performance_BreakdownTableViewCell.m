//
//  Performance_BreakdownTableViewCell.m
//  Shell
//
//  Created by Jeremy Lua on 17/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "Performance_BreakdownTableViewCell.h"
#import "ExcelCell.h"
#import "ExcelLeftHeaderCell.h"
#import "Shell-Swift.h"
#import "Constants.h"
#import "Helper.h"
#import "Headers.h"

@interface Performance_BreakdownTableViewCell ()  <ExcelCollectionViewLayoutDelegate, UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet ExcelCollectionViewLayout *colViewLayout;
@end

@implementation Performance_BreakdownTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.colView.delegate = self;
    self.colView.dataSource = self;
    self.colViewLayout.delegate = self;
    
    [self.colView registerNib: [UINib nibWithNibName: @"ExcelLeftHeaderCell" bundle: nil] forCellWithReuseIdentifier: @"ExcelLeftHeaderCell"];
    [self.colView registerNib: [UINib nibWithNibName: @"ExcelCell" bundle: nil] forCellWithReuseIdentifier: @"ExcelCell"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.monthListArray count] + 3; //section header, total, total %
//    return 6;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [self.breakdownArray count] + 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath * actualIndexPath = indexPath;
    if (kIsRightToLeft) {
        actualIndexPath = [NSIndexPath indexPathForItem:self.monthListArray.count+2-actualIndexPath.item inSection:actualIndexPath.section];
    }
    if (actualIndexPath == [NSIndexPath indexPathForRow:0 inSection:0])
    {
        ExcelCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"ExcelCell" forIndexPath:actualIndexPath];
        //top left most cell, leave blank
        cell.backgroundColor = COLOUR_WHITE;
        cell.title.text = @"";
        return cell;
    }
    else if (actualIndexPath.section == 0) //month list row
    {
        ExcelCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"ExcelCell" forIndexPath:actualIndexPath];
        cell.backgroundColor = COLOUR_WHITE;
        cell.title.text = @"";
        if (actualIndexPath.row == 0)
        {
            cell.title.text = @""; //blank
        }
        else if (actualIndexPath.row <= [self.monthListArray count])
        {
            NSDictionary *monthDict = [self.monthListArray objectAtIndex: actualIndexPath.row - 1];
            cell.title.text = [monthDict objectForKey: @"MonthName"];
        }
        else if (actualIndexPath.row == [self.monthListArray count] + 1) //total
        {
            cell.title.text = LOCALIZATION(C_RU_BREAKDOWN_TOTAL);   //lokalised 11 Feb
        }
        else if (actualIndexPath.row == [self.monthListArray count] + 2) //total %
        {
            cell.title.text = [NSString stringWithFormat:@"%@ %%",LOCALIZATION(C_RU_BREAKDOWN_TOTAL)];  //lokalised 11 Feb
        }
        else
        {
            cell.title.text = @""; //blank
        }
        cell.title.font = FONT_H2;
        cell.bottomLineView.backgroundColor = COLOUR_VERYDARKGREY;
        return cell;
    }
    else if (actualIndexPath.row == 0) //item array
    {
        ExcelLeftHeaderCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"ExcelLeftHeaderCell" forIndexPath:actualIndexPath];
        
        NSDictionary *rowDataDict = [self.breakdownArray objectAtIndex: actualIndexPath.section - 1]; //breakdown row will always be +1, row 0 is header
        cell.title.text = [rowDataDict objectForKey: @"Name"];
        cell.title.font = FONT_B2;
        cell.colourTagView.backgroundColor = [Helper colorFromHex: [rowDataDict objectForKey: @"ColorCodes"]];
        return cell;
    }
    else
    {
        //section & row >= 1
        ExcelCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"ExcelCell" forIndexPath:actualIndexPath];
        cell.backgroundColor = COLOUR_WHITE;
        cell.bottomLineView.backgroundColor = COLOUR_VERYPALEGREY;
        cell.rightLineView.hidden = YES;
        cell.title.text = @"";
        NSDictionary *rowDataDict = [self.breakdownArray objectAtIndex: actualIndexPath.section - 1];

        //row 1,2,3 = monthValue1,2,3
        //indexPath.row will never be used here
        if (actualIndexPath.row == 1 && actualIndexPath.row <= [self.monthListArray count])
        {
            cell.title.text = [rowDataDict objectForKey: @"MonthValue1"];
        }
        else if (actualIndexPath.row == 2 && actualIndexPath.row <= [self.monthListArray count])
        {
            cell.title.text = [rowDataDict objectForKey: @"MonthValue2"];
        }
        else if (actualIndexPath.row == 3 && actualIndexPath.row <= [self.monthListArray count])
        {
            cell.title.text = [rowDataDict objectForKey: @"MonthValue3"];
        }
        else if (actualIndexPath.row == [self.monthListArray count] + 1)
        {
            //Total
            cell.backgroundColor = COLOUR_VERYPALEGREY;
            cell.title.text = [rowDataDict objectForKey: @"Total"];
            cell.rightLineView.hidden = NO;
        }
        else if (actualIndexPath.row == [self.monthListArray count] + 2)
        {
            //Total %
            cell.title.text = [rowDataDict objectForKey: @"TotalPercent"];
            cell.backgroundColor = COLOUR_VERYPALEGREY;
        }
        else
            cell.title.text = @"";
        cell.title.font = FONT_B2;
        return cell;
    }
    
    return [UICollectionViewCell new];
}

- (CGSize)collectionViewLayout:(ExcelCollectionViewLayout * _Nonnull)collectionViewLayout sizeForItemAtColumn:(NSInteger)columnIndex
{
    if (kIsRightToLeft && columnIndex == self.monthListArray.count+2)
        return CGSizeMake(100, 50);
    else if (!(kIsRightToLeft) && columnIndex == 0)
        return CGSizeMake(100, 50);
    else
        return CGSizeMake(80, 50);
}

@end
