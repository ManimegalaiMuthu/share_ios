//
//  PerformanceDashboard_HeaderCell.m
//  Shell
//
//  Created by Jeremy Lua on 17/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PerformanceDashboard_HeaderCell.h"

@implementation PerformanceDashboard_HeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setButtonEnabled:(BOOL) enabled
{
    self.imgDropdown.hidden = !enabled;
    self.btnHeader.hidden = !enabled;
}

@end
