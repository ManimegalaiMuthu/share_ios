//
//  PerformanceChart_NewViewController.m
//  Shell
//
//  Created by Jeremy Lua on 17/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PerformanceChart_NewViewController.h"
#import "Shell-Swift.h"
#import "Performance_BreakdownTableViewCell.h"
#import "CASMonthYearPicker.h"

@interface PerformanceChart_NewViewController () <WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource, MonthYearPickerDelegate, CommonListDelegate>

@property (weak, nonatomic) IBOutlet UITableView *breakdownTableView;

@property BOOL isStartDate;

@property NSString *staffName;

@property NSDictionary *sectionBreakdownDict;

@property NSInteger currentGroupIndex;
@property CASMonthYearPicker *dateView;

@property NSMutableArray *staffListArray;
@property NSMutableArray *staffListDataArray;
@end

@implementation PerformanceChart_NewViewController
@synthesize listView;
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.breakdownTableView registerNib: [UINib nibWithNibName: @"Performance_BreakdownTableViewCell" bundle: nil] forCellReuseIdentifier: @"Performance_BreakdownTableViewCell"];

    if ([[self.groupDataDict objectForKey: @"HasFilter"] boolValue])
    {
        if(GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER || GET_PROFILETYPE == kPROFILETYPE_DMR){
            LoginInfo *loginInfo = [mSession loadUserProfile];
            self.staffTradeIdString = loginInfo.tradeID;
            [[WebServiceManager sharedInstance] fetchTrade:self.staffTradeIdString vc: self];
        }
        else
            [[WebServiceManager sharedInstance] fetchApprovedWorkshopList: @"" searchRange:@"" vc: self];
    }
    else
    {
        [self setupInterface];
    }
}

-(void) setupInterface
{
    if (!self.startDate)
    {
        //setup default date
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
        //setup the necesary date adjustments
        NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
        [selectedComponent setDay: 1];
        NSDate *selectedDate = [gregorian dateFromComponents: selectedComponent];
        
        [selectedComponent setDay: 0];
        [selectedComponent setMonth: -2];
        [selectedComponent setYear: 0];
        selectedDate = [gregorian dateByAddingComponents: selectedComponent toDate: selectedDate options:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)];

        self.startDate = selectedDate;
        self.isStartDate = YES;
        [self selectedMonthYear: self.startDate];
    }
    
    if (!self.staffTradeIdString)
        self.staffTradeIdString = @"";
    self.currentGroupIndex = 0;
    
    [self enterPressed: nil];
    
    if (!self.staffName)
    {
        if (GET_PROFILETYPE == kPROFILETYPE_DMR || GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
            self.staffName = LOCALIZATION(C_PERFORMANCE_ALLSTAFF);      //lokalised 28 Jan
        else
            self.staffName = LOCALIZATION(C_PERFORMANCE_ALLWORKSHOPS);  //lokalised 28 Jan
            
    }
    
    

    //preload listView
    listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(@"")
                                                   list: @[]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;

    //preload dateView
    dateView = [[CASMonthYearPicker alloc] initWithTitle:LOCALIZATION(C_PERFORMANCE_SELECTDATE) //lokalised 28 Jan
                                      previouslySelected:nil
                                          withYearHidden:NO];
    
    dateView.delegate = self;
}

- (IBAction)staffPressed:(UIButton *)sender
{
//    [listView setTitleAndList: LOCALIZATION(C_PERFORMANCE_SELECTSTAFF) list: self.staffListArray hasSearchField: NO];
    [listView setTitleAndList: LOCALIZATION(C_PERFORMANCE_FILTERBY) list: self.staffListArray hasSearchField: NO];  //lokalised 28 Jan

    [self popUpView];
}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

-(void)dropDownSelection:(NSArray *)selection
{
    NSString *searchKey = @"CompanyName";
    if(GET_PROFILETYPE == kPROFILETYPE_DMR || GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
        searchKey = @"Name";
    if (selection.count > 0)
    {
        for (NSDictionary *dict in self.staffListDataArray)
        {
           if ([[dict objectForKey:searchKey] isEqualToString: selection.firstObject])
            {
                //update staff
                self.staffTradeIdString = [dict objectForKey: @"TradeID"];
                self.staffName = [dict objectForKey: searchKey];

                if ([[self.groupDataDict objectForKey: @"HasFilter"] boolValue])
                    [self.delegate updateDatesWithStartDate: self.startDate startDateString: self.startDateString endDate: self.endDate endDateString:self.endDateString tradeIdString: self.staffTradeIdString];
                
                [self.breakdownTableView reloadRowsAtIndexPaths: @[[NSIndexPath indexPathForRow:0 inSection:0]]
                                               withRowAnimation: UITableViewRowAnimationNone];
                [self enterPressed: nil];
            }
        }
    }
}

#pragma mark date delegates

- (IBAction)startDatePressed:(UIButton *)sender
{
    self.isStartDate = YES;
    
    [self popUpDatePicker];
}

- (IBAction)endDatePressed:(UIButton *)sender
{
    self.isStartDate = NO;
    
    [self popUpDatePicker];
}

-(void) popUpDatePicker
{
    [self.view endEditing: YES];
    
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}

- (IBAction)enterPressed:(UIButton *)sender
{
    NSMutableDictionary *tradeDict = [[NSMutableDictionary alloc] init];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy/MM/dd"];
    
    [tradeDict setObject: [outputFormatter stringFromDate: self.startDate] forKey:@"StartDate"];
    [tradeDict setObject: [outputFormatter stringFromDate: self.endDate] forKey:@"EndDate"];

    [tradeDict setObject: [self.groupDataDict objectForKey: @"GroupID"] forKey: @"GroupID"];
    
    [tradeDict setObject: self.staffTradeIdString forKey: @"FilterByTradeID"];
    
    if ([[tradeDict objectForKey: @"FilterByTradeID"] length] > 0)
    {
        //have id
        [tradeDict setObject: @(NO) forKey: @"IsAll"];
    }
    else //no ID
        [tradeDict setObject: @(YES) forKey: @"IsAll"];

    [[WebServiceManager sharedInstance] fetchSectionBreakdown: tradeDict vc: self];
}


- (IBAction)leftButtonPressed:(UIButton *)sender
{
    self.currentGroupIndex--;
    if (self.currentGroupIndex < 0)
    {
        self.currentGroupIndex = 0;
    }
    [self.breakdownTableView reloadData];
}

- (IBAction)rightButtonPressed:(UIButton *)sender
{
    self.currentGroupIndex++;
    
    //get single item from group
    NSArray *groupItemArray = [self.sectionBreakdownDict objectForKey: @"GroupItem"];
    if (self.currentGroupIndex >= [groupItemArray count])
    {
        self.currentGroupIndex = [groupItemArray count] - 1;
    }
    [self.breakdownTableView reloadData];
}

#pragma mark - web service delegate

- (void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_FETCHSECTIONBREAKDOWN:
            if ([[response getGenericResponse] isKindOfClass: [NSNull class]])
            {
                self.sectionBreakdownDict = [NSDictionary new];
            }
            else
            {
                self.sectionBreakdownDict = [response getGenericResponse];
            }
            [self.breakdownTableView reloadData];
            break;
        case kWEBSERVICE_FETCHWORKSHOPAPPROVEDLIST:
            self.staffListDataArray = [[response getGenericResponse] objectForKey: @"ApprovedWorkShop"];
            if([self.staffListDataArray isKindOfClass: [NSNull class]])
            {
                self.staffListDataArray = [NSMutableArray new];
            }
            if (GET_PROFILETYPE == kPROFILETYPE_DMR  || GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
            {
                [self.staffListDataArray insertObject: @{@"CompanyName": LOCALIZATION(C_PERFORMANCE_ALLSTAFF),  //lokalised 28 Jan
                                                         @"TradeID": @""} atIndex: 0];
            }
            else
            {
                [self.staffListDataArray insertObject: @{@"CompanyName": LOCALIZATION(C_PERFORMANCE_ALLWORKSHOPS),  //lokalised 28 Jan
                                                         @"TradeID": @""} atIndex: 0];
            }
            self.staffListArray = [NSMutableArray new];
            
            for (NSDictionary *dict in self.staffListDataArray)
            {
                [self.staffListArray addObject: [dict objectForKey: @"CompanyName"]];
                if ([self.staffTradeIdString isEqualToString: [dict objectForKey: @"TradeID"]])
                    self.staffName = [dict objectForKey: @"CompanyName"];
            }
            [self setupInterface];
            break;
        case kWEBSERVICE_FETCHTRADE:
            self.staffListDataArray = [[response getGenericResponse] objectForKey: @"MechanicList"];
            
            if([self.staffListDataArray isKindOfClass: [NSNull class]])
            {
                self.staffListDataArray = [NSMutableArray new];
            }
            if (GET_PROFILETYPE == kPROFILETYPE_DMR || GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
           {
               [self.staffListDataArray insertObject: @{@"Name": LOCALIZATION(C_PERFORMANCE_ALLSTAFF),  //lokalised 28 Jan
                                                        @"TradeID": self.staffTradeIdString} atIndex: 0];
           }
           else
           {
               [self.staffListDataArray insertObject: @{@"CompanyName": LOCALIZATION(C_PERFORMANCE_ALLWORKSHOPS),  //lokalised 28 Jan
                                                        @"TradeID": @""} atIndex: 0];
           }
           self.staffListArray = [NSMutableArray new];
           
           for (NSDictionary *dict in self.staffListDataArray)
           {
               [self.staffListArray addObject: [dict objectForKey: @"Name"]];
           }
           [self setupInterface];
            break;
        default:
            break;
    }
}

#pragma mark table delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([self.sectionBreakdownDict count] == 0)
        return 2; //Filter and no data cell
    else
        return 5;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.sectionBreakdownDict count] == 0)
        return 1; //show no data cell
    
    //Only nochart breakdown table will have header
    if (section == 3 && ![[self.groupDataDict objectForKey: @"IsChart"] boolValue])
    {
        NSArray *groupItemArray = [self.sectionBreakdownDict objectForKey: @"GroupItem"];
        NSDictionary *itemDict = [groupItemArray objectAtIndex: self.currentGroupIndex];
        NSArray *breakdownArray = [[NSMutableArray alloc] initWithArray: [itemDict objectForKey: @"Item"]];
        
        return [breakdownArray count];
    }
    
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //Only nochart breakdown table will have header
    if (section == 3 && ![[self.groupDataDict objectForKey: @"IsChart"] boolValue])
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"BreakdownHeaderCell"];
        
//        NSArray *monthListArray = [self.month objectForKey: @"MonthList"];
        UILabel *lblMonthOne = [cell viewWithTag: 4];
        UILabel *lblMonthTwo = [cell viewWithTag:  5];
        UILabel *lblMonthThree = [cell viewWithTag: 6];
        
        lblMonthOne.font = FONT_H2;
        lblMonthTwo.font = FONT_H2;
        lblMonthThree.font = FONT_H2;
        
        NSArray *monthListArray = [self.sectionBreakdownDict objectForKey: @"MonthList"];
        
        if ([monthListArray count] < 3)
        {
            lblMonthThree.hidden = YES;
        }
        else
        {
            lblMonthThree.text = [[monthListArray objectAtIndex: 2] objectForKey: @"MonthName"];
        }
        
        if ([monthListArray count] < 2)
        {
            lblMonthTwo.hidden = YES;
        }
        else
        {
            lblMonthTwo.text = [[monthListArray objectAtIndex: 1] objectForKey: @"MonthName"];
        }
        
        if ([monthListArray count] < 1)
        {
            lblMonthOne.hidden = YES;
        }
        else
        {
            lblMonthOne.text = [[monthListArray objectAtIndex: 0] objectForKey: @"MonthName"];
        }
        
        
        UILabel *lblTotalHeader = [cell viewWithTag: 7];
        lblTotalHeader.text = LOCALIZATION(C_RU_BREAKDOWN_TOTAL);   //lokalised 28 Jan
        lblTotalHeader.font = FONT_H2;
        
        cell.contentView.backgroundColor = COLOUR_WHITE;

        
        
        return [cell contentView];
    }
    else
    {
        return [UIView new];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([self.sectionBreakdownDict count] == 0)
        return 0;
    if (section == 3 && ![[self.groupDataDict objectForKey: @"IsChart"] boolValue])
        return 50;
    else
        return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.sectionBreakdownDict count] == 0)
        return UITableViewAutomaticDimension;
    
    if (indexPath.section != 3)
        return UITableViewAutomaticDimension;
    else
    {
        NSArray *groupItemArray = [self.sectionBreakdownDict objectForKey: @"GroupItem"];
        NSDictionary *itemDict = [groupItemArray objectAtIndex: self.currentGroupIndex];
        NSArray *breakdownArray = [itemDict objectForKey: @"Item"];
        
        if (![[self.groupDataDict objectForKey: @"IsChart"] boolValue])
            return 50; //no chart is individual rows
        else
            return ([breakdownArray count] + 1) * 50; //+1 for header cell in collection view
        
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.sectionBreakdownDict)
        return [UITableViewCell new];
    
    UITableViewCell *cell;
    switch (indexPath.section)
    {
        case 0: //Filter or Filter with Staff
        {
            if ([[self.groupDataDict objectForKey: @"HasFilter"] boolValue])
            {
                cell = [tableView dequeueReusableCellWithIdentifier: @"FilterStaffCell"];
                
                UIButton *btnStaff = [cell viewWithTag: 5];
                [btnStaff setTitle: self.staffName forState: UIControlStateNormal];
                [btnStaff.titleLabel setFont: FONT_B2];
                
                if (kIsRightToLeft) {
                    [btnStaff setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
                } else {
                    [btnStaff setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
                }
            }
            else
            {
                cell = [tableView dequeueReusableCellWithIdentifier: @"FilterCell"];
            }
            UILabel *lblFilterHeader = [cell viewWithTag: 6];
            lblFilterHeader.text = LOCALIZATION(C_PERFORMANCE_FILTERBYDATERANGE);   //lokalised 28 Jan
            lblFilterHeader.font = FONT_B2;
            
            UIButton *btnStartDate = [cell viewWithTag: 1];
            [btnStartDate setTitle: self.startDateString forState: UIControlStateNormal];
            btnStartDate.titleLabel.font = FONT_B2;
            
            
            UILabel *lblTo = [cell viewWithTag: 2];
            lblTo.text = LOCALIZATION(C_POINTS_TO); //lokalised 28 Jan
            lblTo.font = FONT_B2;
            
            UIButton *btnEndDate = [cell viewWithTag: 3];
            [btnEndDate setTitle: self.endDateString forState: UIControlStateNormal];
            btnEndDate.titleLabel.font = FONT_B2;
            
            UIButton *btnEnter = [cell viewWithTag: 4];
            [btnEnter setTitle: LOCALIZATION(C_POINTS_GO) forState: UIControlStateNormal];  //lokalised 28 Jan
            btnEnter.titleLabel.font = FONT_H2;
            [btnEnter setBackgroundColor:COLOUR_RED];
        }
            break;
        case 1: //Main Header (no circle) or No Data Cell
        {
            if ([self.sectionBreakdownDict count] == 0)
            {
                cell = [tableView dequeueReusableCellWithIdentifier: @"NoDataCell"];
                
                UILabel *lblNoData = [cell viewWithTag: 1];
                lblNoData.text = LOCALIZATION(C_TABLE_NODATAFOUND); //lokalised 28 Jan
                lblNoData.font = FONT_B1;
                lblNoData.textColor = COLOUR_VERYDARKGREY;
            }
            else
            {
                cell = [tableView dequeueReusableCellWithIdentifier: @"MainHeaderCell"];

                UILabel *lblHeader = [cell viewWithTag: 1];
                lblHeader.text = [self.sectionBreakdownDict objectForKey: @"Header"];
                lblHeader.font = FONT_H(18);
                
                UILabel *lblTotal = [cell viewWithTag: 2];
                lblTotal.text = [self.sectionBreakdownDict objectForKey: @"Total"];
                lblTotal.font = FONT_H(25);
                lblTotal.textColor = COLOUR_RED;
            }
        }
            break;
        case 2: //Breakdown chart (circle or title only
        {
            
            //get single item from group
            NSArray *groupItemArray = [self.sectionBreakdownDict objectForKey: @"GroupItem"];
            NSDictionary *itemDict = [groupItemArray objectAtIndex: self.currentGroupIndex];
            
            if ([[self.groupDataDict objectForKey: @"IsChart"] boolValue])
            {
                cell = [tableView dequeueReusableCellWithIdentifier: @"ChartCell"];
                cell.backgroundColor = COLOUR_WHITE;
                [self constructCircle: [cell viewWithTag: 5] breakdownArray: [itemDict objectForKey: @"Item"]];
            }
            else
            {
                cell = [tableView dequeueReusableCellWithIdentifier: @"NoChartCell"];
                cell.backgroundColor = COLOUR_VERYPALEGREY;
            }
            
            UILabel *lblTitle = [cell viewWithTag: 6];
            lblTitle.text = [itemDict objectForKey: @"Title"];
            lblTitle.font = FONT_H2;
            
            
            UIButton *btnLeft = [cell viewWithTag: 11];
            if (self.currentGroupIndex == 0)
            {
                btnLeft.hidden = YES;
            }
            else
            {
                btnLeft.hidden = NO;
            }
            
            
            UIButton *btnRight = [cell viewWithTag: 12];
            if (self.currentGroupIndex == [groupItemArray count] - 1)
            {
                btnRight.hidden = YES;
            }
            else
            {
                btnRight.hidden = NO;
            }
            
            if (kIsRightToLeft) {
                [btnLeft setImage:[UIImage imageNamed:@"icn_chartright_verydarkgrey.png"] forState:UIControlStateNormal];
                [btnRight setImage:[UIImage imageNamed:@"icn_chartleft_verydarkgrey.png"] forState:UIControlStateNormal];
            } else {
                [btnLeft setImage:[UIImage imageNamed:@"icn_chartleft_verydarkgrey.png"] forState:UIControlStateNormal];
                [btnRight setImage:[UIImage imageNamed:@"icn_chartright_verydarkgrey.png"] forState:UIControlStateNormal];
            }
        }
            break;
        case 3: //Breakdown chart
        {
            if ([[self.groupDataDict objectForKey: @"IsChart"] boolValue])
            {
                //get single item from group
                NSArray *groupItemArray = [self.sectionBreakdownDict objectForKey: @"GroupItem"];
                NSDictionary *itemDict = [groupItemArray objectAtIndex: self.currentGroupIndex];
                
//                Performance_BreakdownTableViewCell *excelCell = [tableView dequeueReusableCellWithIdentifier: @"Performance_BreakdownTableViewCell"];
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed: @"Performance_BreakdownTableViewCell" owner:self options:nil];
                Performance_BreakdownTableViewCell *excelCell = [nib objectAtIndex:0];
                
                [excelCell.colView.collectionViewLayout invalidateLayout];
                excelCell.breakdownArray = [[NSMutableArray alloc] initWithArray: [itemDict objectForKey: @"Item"]];
                excelCell.monthListArray = [[NSMutableArray alloc] initWithArray: [self.sectionBreakdownDict objectForKey: @"MonthList"]];
                [excelCell.colView reloadData];
                return excelCell;
            }
            else
            {
                NSArray *groupItemArray = [self.sectionBreakdownDict objectForKey: @"GroupItem"];
                NSDictionary *itemDict = [groupItemArray objectAtIndex: self.currentGroupIndex];
                NSArray *breakdownArray = [[NSMutableArray alloc] initWithArray: [itemDict objectForKey: @"Item"]];
                
                NSDictionary *cellData = [breakdownArray objectAtIndex: indexPath.row]; //individual data in row

                cell = [tableView dequeueReusableCellWithIdentifier: @"BreakdownCell"];
                cell.backgroundColor = COLOUR_WHITE;
                
                UIView *colourTagView = [cell viewWithTag: 1];
//                colourTagView.backgroundColor = [Helper colorFromHex: [cellData objectForKey: @"ColorCodes"]];
                colourTagView.hidden = YES;
                
                UILabel *lblProductName = [cell viewWithTag:  2];
                lblProductName.text =  [cellData objectForKey: @"Name"];
                lblProductName.font = FONT_B2;
                
                //4,5,6 - monthvalue
                //7 - total
                UILabel *lblMonthOne = [cell viewWithTag: 4];
                UILabel *lblMonthTwo = [cell viewWithTag:  5];
                UILabel *lblMonthThree = [cell viewWithTag: 6];
                UILabel *lblTotal = [cell viewWithTag: 7];
                
                lblMonthOne.font = FONT_B2;
                lblMonthTwo.font = FONT_B2;
                lblMonthThree.font = FONT_B2;
                lblTotal.font = FONT_B2;
                
                NSArray *monthListArray = [self.sectionBreakdownDict objectForKey: @"MonthList"];
                
                if ([monthListArray count] < 3)
                {
                    lblMonthThree.hidden = YES;
                }
                else
                {
                    lblMonthThree.text = [cellData objectForKey: @"MonthValue3"];
                }

                if ([monthListArray count] < 2)
                {
                    lblMonthTwo.hidden = YES;
                }
                else
                {
                    lblMonthTwo.text = [cellData objectForKey: @"MonthValue2"];
                }

                if ([monthListArray count] < 1)
                {
                    lblMonthOne.hidden = YES;
                }
                else
                {
                    lblMonthOne.text = [cellData objectForKey: @"MonthValue1"];
                }
                lblTotal.text = [cellData objectForKey: @"Total"];
                
                return cell;
            }
        }
            break;
        case 4:
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"UpdatedDateCell"];
            
            UILabel *lblNoData = [cell viewWithTag: 1];
//            lblNoData.text = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_HOMECAROUSEL_UPDATEDON), [self.sectionBreakdownDict objectForKey: @"UpdatedDate"]]; //lokalised 28 Jan
            if ([[self.sectionBreakdownDict objectForKey: @"UpdatedDate"] isKindOfClass: [NSNull class]])
                lblNoData.text = @"";
            else
                lblNoData.text = [self.sectionBreakdownDict objectForKey: @"UpdatedDate"];
            lblNoData.font = FONT_B2;
        }
            break;
        default:
            return [UITableViewCell new];
            break;
    }
    return cell;
}


#pragma Date Picker delegate method
- (void)selectedMonthYear:(NSDate *)selectedDate
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    
    //setup the necesary date adjustments
    NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: selectedDate];
    [selectedComponent setDay: 1];
    selectedDate = [gregorian dateFromComponents: selectedComponent];
    
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setMonth: 3];
    [offsetComponents setDay: -1];
    
    NSDateFormatter *btnDateFormatter = [[NSDateFormatter alloc] init];
    [btnDateFormatter setDateFormat:@"MMM yyyy"];
    if ([GET_LOCALIZATION isEqualToString: kRussian])
        [btnDateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kRussian]];
    else
        [btnDateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    
    if (self.isStartDate)
    {
        NSDate *offsetDate = [gregorian dateByAddingComponents:offsetComponents toDate: selectedDate options:0];
        
        NSString *selectedDateString;
        NSString *offsetDateString;
        //check end date if more than current date
        if ([offsetDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
        {
            //check start date selection if more than current date.
            if ([selectedDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
            {
                NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
                [selectedComponent setDay: 1];
                selectedDate = [gregorian dateFromComponents: selectedComponent];
            }
            selectedDateString = [btnDateFormatter stringFromDate: selectedDate]; //first of current month
            offsetDateString = [btnDateFormatter stringFromDate: [NSDate date]]; //current date
            offsetDate = [NSDate date]; //set end date to current date
        }
        else
        {
            //start date will never be more than current date if above validation of end date clears.
            selectedDateString = [btnDateFormatter stringFromDate: selectedDate];
            offsetDateString = [btnDateFormatter stringFromDate: offsetDate]; //offset still 2 months later
        }
        
        
        //hardcode to first three character in russian
        if ([GET_LOCALIZATION isEqualToString: kRussian])
        {
            NSMutableArray *selectedDateStringArray = [[NSMutableArray alloc] initWithArray: [selectedDateString componentsSeparatedByString: @" "]];
            NSString *selectedMonth = [selectedDateStringArray objectAtIndex: 0];
            selectedMonth = [selectedMonth substringToIndex: 3];
            selectedDateString = [[NSString stringWithFormat: @"%@ %@", selectedMonth, [selectedDateStringArray objectAtIndex: 1]] capitalizedString];
            
            NSMutableArray *offsetDateStringArray = [[NSMutableArray alloc] initWithArray: [offsetDateString componentsSeparatedByString: @" "]];
            NSString *offsetMonth = [offsetDateStringArray objectAtIndex: 0];
            offsetMonth = [offsetMonth substringToIndex: 3];
            offsetDateString = [[NSString stringWithFormat: @"%@ %@", offsetMonth, [offsetDateStringArray objectAtIndex: 1]] capitalizedString]; //Capitalised, e.g Aug, Sep, instead of aug, sep
        }
        
        self.startDate = selectedDate;
        self.endDate = offsetDate;
        self.startDateString = selectedDateString;
        self.endDateString = offsetDateString;
        
        //end start is offset by 3 months
        NSRange range = NSMakeRange(0, 1); //reload section 0
        NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
        [self.breakdownTableView reloadSections:section withRowAnimation:UITableViewRowAnimationNone];

    }
    else
    {
        //handle only end date
        
        //set date to end of selected month
        NSDateComponents *endOfMonthComponents = [[NSDateComponents alloc] init];
        [endOfMonthComponents setYear: 0];
        [endOfMonthComponents setMonth: 1];
        [endOfMonthComponents setDay: -1];
        selectedDate = [gregorian dateByAddingComponents: endOfMonthComponents toDate: selectedDate options: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)];
        
        //check date from startdate
        NSDate *offsetDate = [gregorian dateByAddingComponents:offsetComponents toDate: self.startDate options:0];
        
        //if offset date is more than current month, change offset to current month
        if ([offsetDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
        {
            //reset offset date to last day of current month
            NSDateComponents *currentDateComponents = [gregorian components: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
            [currentDateComponents setDay: 1];
            [currentDateComponents setMonth: [currentDateComponents month] + 1];
            offsetDate = [gregorian dateFromComponents: currentDateComponents];
            
            [currentDateComponents setDay: -1];
            [currentDateComponents setMonth: 0];
            [currentDateComponents setYear: 0];
            offsetDate = [gregorian dateByAddingComponents: currentDateComponents toDate: offsetDate options:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) ];
        }
        
        //if selection more than threshold (+2 months from start month)
        if ([selectedDate timeIntervalSinceReferenceDate] > [offsetDate timeIntervalSinceReferenceDate])
        {
            selectedDate = offsetDate;
        }
        else if ([selectedDate timeIntervalSinceReferenceDate] < [self.startDate timeIntervalSinceReferenceDate])
        {
            //if selection less than start month
            selectedDate = self.startDate;
        }
        else
        {
            
        }
        
        //don't need to change start date.
        NSString *selectedDateString = [btnDateFormatter stringFromDate: selectedDate];
        
        //hardcode to first three character in russian
        if ([GET_LOCALIZATION isEqualToString: kRussian])
        {
            NSMutableArray *selectedDateStringArray = [[NSMutableArray alloc] initWithArray: [selectedDateString componentsSeparatedByString: @" "]];
            NSString *selectedMonth = [selectedDateStringArray objectAtIndex: 0];
            selectedMonth = [selectedMonth substringToIndex: 3];
            selectedDateString = [[NSString stringWithFormat: @"%@ %@", selectedMonth, [selectedDateStringArray objectAtIndex: 1]] capitalizedString];
        }
        
//        [self.btnEndDate setTitle: selectedDateString forState: UIControlStateNormal];
        self.endDateString = selectedDateString;
        self.endDate = selectedDate;
        
        NSRange range = NSMakeRange(0, 1); //reload section 0
        NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
        [self.breakdownTableView reloadSections:section withRowAnimation:UITableViewRowAnimationNone];
    }
    
    [self.delegate updateDatesWithStartDate: self.startDate startDateString: self.startDateString endDate: self.endDate endDateString:self.endDateString];
        
}

#define START_ANGLE -90 //12 o clock
#define degreesToRadians(degrees)((M_PI * degrees)/180)
-(void) constructCircle:(UIView *)view breakdownArray:(NSMutableArray *)breakdownArray
{
    //breakdownArray = tier or category. pick out the percent
    CGFloat lastAngleInRadians = degreesToRadians(START_ANGLE);
    
    int i = 0;
    for (NSDictionary *dict in breakdownArray)
    {
        CGFloat percentage = [[dict objectForKey: @"TotalPercent"] floatValue];
        CGFloat nextAngleInRadians = lastAngleInRadians + (degreesToRadians(360  * percentage * 0.01));
        
        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
        [bezierPath addArcWithCenter: CGPointMake(view.frame.size.width / 2, view.frame.size.width / 2)
                              radius: (view.frame.size.width / 2) + 15
                          startAngle: lastAngleInRadians
                            endAngle: nextAngleInRadians
                           clockwise:YES];
        
        CAShapeLayer *progressLayer = [[CAShapeLayer alloc] init];
        [progressLayer setPath:bezierPath.CGPath];
        
        UIColor *color = [Helper colorFromHex: [dict objectForKey: @"ColorCodes"]];
        [progressLayer setStrokeColor: [color CGColor]];
        
        [progressLayer setFillColor:[UIColor clearColor].CGColor];
        [progressLayer setLineWidth: 30];
        [view.layer addSublayer:progressLayer];
        
        //get ready for the next arc draw
        lastAngleInRadians = nextAngleInRadians;
        i++;
    }
    
    //complete circle if not completed
//    if (lastAngleInRadians != degreesToRadians(START_ANGLE))
//    {
//        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
//        [bezierPath addArcWithCenter: CGPointMake(view.frame.size.width / 2, view.frame.size.width / 2)
//                              radius: (view.frame.size.width / 2) + 15
//                          startAngle: lastAngleInRadians
//                            endAngle: degreesToRadians(START_ANGLE)
//                           clockwise:YES];
//        
//        CAShapeLayer *progressLayer = [[CAShapeLayer alloc] init];
//        [progressLayer setPath:bezierPath.CGPath];
//        
//        UIColor *color = [Helper colorFromHex: @"#D9D9D9"];
//        [progressLayer setStrokeColor: [color CGColor]];
//        
//        [progressLayer setFillColor:[UIColor clearColor].CGColor];
//        [progressLayer setLineWidth: 30];
//        [view.layer addSublayer:progressLayer];
//    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
