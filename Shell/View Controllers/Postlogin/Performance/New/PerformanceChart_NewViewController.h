//
//  PerformanceChart_NewViewController.h
//  Shell
//
//  Created by Jeremy Lua on 17/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PerformanceChart_NewViewDelegate <NSObject>
-(void) updateDatesWithStartDate:(NSDate *) startDate
                 startDateString:(NSString *)startDateString
                         endDate:(NSDate *) endDate
                   endDateString:(NSString *) endDateString;    //for no filter

-(void) updateDatesWithStartDate:(NSDate *) startDate
                 startDateString:(NSString *)startDateString
                         endDate:(NSDate *) endDate
                   endDateString:(NSString *) endDateString
                   tradeIdString:(NSString *) tradeIdString;     //for has filter
@end

@interface PerformanceChart_NewViewController : BaseVC
@property (weak) id<PerformanceChart_NewViewDelegate> delegate;
@property NSDictionary *groupDataDict;
@property NSDate *startDate;
@property NSDate *endDate;
@property NSString *startDateString;
@property NSString *endDateString;
@property NSString *staffTradeIdString;
@end

NS_ASSUME_NONNULL_END
