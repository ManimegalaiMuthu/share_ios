//
//  PerformanceDetails_NewViewController.h
//  Shell
//
//  Created by Jeremy Lua on 17/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface PerformanceDetails_NewViewController : BaseVC
@property NSArray *summaryDataArray;
@property NSArray *mainCategoryArray;
@property NSUInteger selectedIndex;
@property NSString *vcTitle;
@property NSString *originalTradeId;
@end

NS_ASSUME_NONNULL_END
