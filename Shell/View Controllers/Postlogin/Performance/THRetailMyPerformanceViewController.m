//
//  THRetailMyPerformanceViewController.m
//  Shell
//
//  Created by Edenred on 5/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "THRetailMyPerformanceViewController.h"
#import "PerformanceTabViewController.h"
#define CELL_HEIGHT 60

@interface THRetailMyPerformanceViewController () <WebServiceManagerDelegate>
@property (weak, nonatomic) IBOutlet UIView *performanceView;
@property (weak, nonatomic) IBOutlet UILabel *performanceHeader;
@property (weak, nonatomic) IBOutlet UILabel *faHeader;
@property (weak, nonatomic) IBOutlet UITableView *performanceTableView;
@property (weak, nonatomic) IBOutlet UITableView *faPerformanceTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *faTableHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *performanceTableHeight;
@property (weak, nonatomic) IBOutlet UIView *faPerformanceView;

@property NSMutableArray *performanceListArray;
@property NSMutableArray *faPerformanceListArray;

@property BOOL faPerformancePresent;

@end

@implementation THRetailMyPerformanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (![self.parentViewController.parentViewController isKindOfClass: [PerformanceTabViewController class]])
    {
        [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_WORKSHOPPERFORMANCE) subtitle:@"" size:15 subtitleSize:0];   //lokalised 28 Jan
    }
    
    self.faPerformanceTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.performanceTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.faHeader.font = FONT_H1;
    self.performanceHeader.font = FONT_H1;
    
    self.faPerformancePresent = NO;
    
    switch(GET_PROFILETYPE) {
        case kPROFILETYPE_DSR:
        case kPROFILETYPE_TRADEOWNER:
            self.performanceHeader.text = LOCALIZATION(C_PERFORMANCE_WORKSHOP);             //lokalised 28 Jan
            break;
        case kPROFILETYPE_MECHANIC:
        case kPROFILETYPE_FORECOURTATTENDANT:
            self.performanceHeader.text = LOCALIZATION(C_PERFORMANCE_PROGRAMPERFORMANCE);   //lokalised 28 Jan
            break;
        default:
            break;
            
    }
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_WORKSHOPPERFORMANCE) screenClass:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    switch(GET_PROFILETYPE) {
        case kPROFILETYPE_DSR:
            [[WebServiceManager sharedInstance] fetchDSRPerformance: self];
            break;
        case kPROFILETYPE_TRADEOWNER:
            [[WebServiceManager sharedInstance] fetchTradeOwnerPerformance: self];
            break;
        case kPROFILETYPE_MECHANIC:
        case kPROFILETYPE_FORECOURTATTENDANT:
            [[WebServiceManager sharedInstance] fetchTradeOwnerPerformance: self];
            break;
        default:
            break;
            
    }
}

-(void)processCompleted:(WebServiceResponse *)response
{
    NSDictionary *profileData = [response getGenericResponse];

    
    self.faPerformanceListArray = [[NSMutableArray alloc] init];
    if (![[profileData objectForKey:@"FAPerformance"] isKindOfClass:[NSNull class]]) {
        for (NSDictionary *dict in [profileData objectForKey: @"FAPerformance"])
        {
            [self.faPerformanceListArray addObject: dict];
        }
        
        if(![[profileData objectForKey:@"FAPerfText"] isKindOfClass:[NSNull class]]) {
            self.faHeader.text = [profileData objectForKey:@"FAPerfText"];
        } else {
            self.faHeader.text = LOCALIZATION(C_PERFORMANCE_PROGRAMPERFORMANCE);    //lokalised 28 Jan
        }
        self.faPerformancePresent = YES;
    }
    
    self.performanceListArray = [[NSMutableArray alloc] init];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    if(![[profileData objectForKey:@"Performance"]isKindOfClass:[NSNull class]]) {
        for (NSDictionary *dict in [profileData objectForKey: @"Performance"])
        {
            [array addObject: dict];
        }
    }
    
    if (![[profileData objectForKey:@"WorkshopPerformance"]isKindOfClass:[NSNull class]]) {
        for (NSDictionary *dict in [profileData objectForKey: @"WorkshopPerformance"])
        {
            [self.performanceListArray addObject: dict];
        }
    }
    
    if(!_faPerformancePresent && _performanceListArray.count > 0) {
        _faPerformanceListArray = _performanceListArray;
        _performanceListArray = array;
        
        self.faHeader.text = [profileData objectForKey:@"WSPerfText"];
        self.performanceHeader.text = [profileData objectForKey:@"ProgPerfText"];
    } else if (_performanceListArray.count == 0){
        _performanceListArray = array;
    }
    
    if(![[profileData objectForKey:@"ProgPerfText"] isKindOfClass:[NSNull class]]) {
        self.performanceHeader.text = [profileData objectForKey:@"ProgPerfText"];
    } else {
        self.performanceHeader.text = [profileData objectForKey:@"WSPerfText"];
    }
    
    if([self.performanceListArray count] == 0)
        [self.performanceView removeFromSuperview];
    
    if([self.faPerformanceListArray count] == 0) {
        self.faPerformanceView.hidden = YES;
        self.faTableHeight.constant = 0;
    }
    
    self.faTableHeight.constant = CELL_HEIGHT * [self.faPerformanceListArray count];
    self.performanceTableHeight.constant = CELL_HEIGHT * [self.performanceListArray count];
    
    [self.view layoutIfNeeded];
    [self.view layoutSubviews];
    
    [self.faPerformanceTableView reloadData];
    [self.performanceTableView reloadData];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.faPerformanceTableView)
        return [self.faPerformanceListArray count];
    else
        return [self.performanceListArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *dict;
    if (tableView == self.faPerformanceTableView)
        dict = [self.faPerformanceListArray objectAtIndex: indexPath.row];
    else
        dict = [self.performanceListArray objectAtIndex: indexPath.row];
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"WorkshopPerformanceCell"];
    
    UILabel *lblKey = [cell viewWithTag: 1];
    [lblKey setFont: FONT_B1];
    
    UILabel *lblValue = [cell viewWithTag: 2];
    [lblValue setFont: FONT_H(30)];
    [lblValue setTextColor: COLOUR_RED];
    
    
    lblKey.text = [dict objectForKey: @"PerformanceDesc"];
    lblValue.text = [dict objectForKey: @"PerformanceValue"];
    
    return cell;
}


@end
