//
//  PerformanceTabViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 24/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "RDVTabBarItem.h"
#import "PerformanceTabViewController.h"
#import "MyPerformanceViewController.h"
#import "PerformancePointsViewController.h"
#import "PerformanceWorkshopViewController.h"
#import "LeaderboardWebViewController.h"
#import "THPerformancePointsViewController.h"
#import "THRetailMyPerformanceViewController.h"
#import "MyCoinsPerformanceViewController.h"

@interface PerformanceTabViewController ()
{
    MyPerformanceViewController *myPerformanceViewController;
    PerformancePointsViewController *performancePointsViewController;
    PerformanceWorkshopViewController *performanceWorkshopViewController;
    LeaderboardWebViewController *leaderboardViewController;
    THPerformancePointsViewController *thPerformanceViewController;
    THRetailMyPerformanceViewController *thRetailMyPerformanceViewController;
    MyCoinsPerformanceViewController *coinsPerformanceController;
}

@end

@implementation PerformanceTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupInterface];
}


- (void) setupInterface
{
    
    performanceWorkshopViewController  = [STORYBOARD_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_PERFORMANCEWORKSHOP];
    UINavigationController *performanceWorkshopNav = [[UINavigationController alloc] initWithRootViewController: performanceWorkshopViewController];
    [performanceWorkshopNav setNavigationBarHidden: YES];
    
    NSArray *title;
   
    switch (GET_PROFILETYPE)
    {
        case kPROFILETYPE_DSR:
        {
            myPerformanceViewController  = [STORYBOARD_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_MYPERFORMANCE];
            thRetailMyPerformanceViewController = [STORYBOARD_THRetail instantiateViewControllerWithIdentifier:VIEW_TH_MYPERFORMANCE];
           
            UINavigationController *myPerformanceNav;
            if(![GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND]) {
                myPerformanceNav = [[UINavigationController alloc] initWithRootViewController: myPerformanceViewController];
            } else {
                myPerformanceNav = [[UINavigationController alloc] initWithRootViewController: thRetailMyPerformanceViewController];
                
            }
            [myPerformanceNav setNavigationBarHidden: YES];
            
            if([[mSession profileInfo] hasLeaderBoard]) {
                leaderboardViewController  = [STORYBOARD_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_LEADERBOARD];
                UINavigationController *leaderboardNav = [[UINavigationController alloc] initWithRootViewController: leaderboardViewController];
                [leaderboardNav setNavigationBarHidden: YES];
                
                if([GET_LOCALIZATION isEqualToString:kVietnamese]) {
                    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_WORKSHOPPERFORMANCE) subtitle: @""]; //lokalised 28 Jan
                    title = @[LOCALIZATION(C_TITLE_MECHPERFORMANCE),        //lokalised 28 Jan
                              LOCALIZATION(C_PERFORMANCE_LEADERBOARD),      //lokalised 28 Jan
                              LOCALIZATION(C_TABMENU_WORKSHOPPERFORMANCE),  //lokalised 28 Jan
                              ];
                } else {
                    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_PERFORMANCE) subtitle: @""]; //lokalised 28 Jan
                    title = @[LOCALIZATION(C_TITLE_PERFORMANCE),            //lokalised 28 Jan
                              LOCALIZATION(C_PERFORMANCE_LEADERBOARD),      //lokalised 28 Jan
                              LOCALIZATION(C_TABMENU_WORKSHOPPERFORMANCE),  //lokalised 28 Jan
                              ];                }
                [self setViewControllers: @[myPerformanceNav,
                                            //                                performancePointsNav,
                                            leaderboardNav,
                                            performanceWorkshopNav,
                                            ]];
            } else if (![[mSession profileInfo] hasRUPerformance]) {
                [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_WORKSHOPPERFORMANCE) subtitle: @""]; //lokalised 28 Jan
//                title = @[LOCALIZATION(C_TITLE_PERFORMANCE)];
                self.tabBarHidden = YES;
                [self.tabBar removeFromSuperview];
                [self setViewControllers: @[myPerformanceNav,
                                            ]];
                
            }
            else {
                [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_PERFORMANCE) subtitle: @""]; //lokalised 28 Jan
                if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_VIETNAM]) {
                    title = @[LOCALIZATION(C_PERFORMANCE_PROGRAMPERFORMANCE),   //lokalised 28 Jan
                              LOCALIZATION(C_TABMENU_WORKSHOPPERFORMANCE),  //lokalised 28 Jan
                              ];
                } else {
                    title = @[LOCALIZATION(C_TITLE_PERFORMANCE),    //lokalised 28 Jan
                              LOCALIZATION(C_TABMENU_WORKSHOPPERFORMANCE),  //lokalised 28 Jan
                              ];
                }
                [self setViewControllers: @[myPerformanceNav,
                                            //                                performancePointsNav,
//                                            leaderboardNav,
                                            performanceWorkshopNav,
                                            ]];
            }
        }
            break;
        case kPROFILETYPE_TRADEOWNER:
        {
            //workshop
            if(IS_COUNTRY(COUNTRYCODE_INDONESIA)) {
                coinsPerformanceController  = [STORYBOARD_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_MYCOIN_PERFORMANCE];
                UINavigationController *performancePointsNav = [[UINavigationController alloc] initWithRootViewController: coinsPerformanceController];
                [performancePointsNav setNavigationBarHidden: YES];
                           
                [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_WORKSHOPPERFORMANCE) subtitle:@"" size:15 subtitleSize:0];
                           
                title = @[LOCALIZATION(C_TITLE_PERFORMANCE),
                          LOCALIZATION(C_TITLE_MYCOINS),
                                     ];
                [self setViewControllers: @[
                                            performanceWorkshopNav,
                                            performancePointsNav,
                                    ]];
            } else {
                performancePointsViewController  = [STORYBOARD_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_PERFORMANCEPOINTS];
                UINavigationController *performancePointsNav = [[UINavigationController alloc] initWithRootViewController: performancePointsViewController];
                [performancePointsNav setNavigationBarHidden: YES];
               
                [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_WORKSHOPPERFORMANCE) subtitle:@"" size:15 subtitleSize:0];   //lokalised 28 Jan

                if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND]) {
                    thRetailMyPerformanceViewController = [STORYBOARD_THRetail instantiateViewControllerWithIdentifier:VIEW_TH_MYPERFORMANCE];
                    performanceWorkshopNav = [[UINavigationController alloc] initWithRootViewController: thRetailMyPerformanceViewController];
                    [performanceWorkshopNav setNavigationBarHidden: YES];
                }
               
                title = @[LOCALIZATION(C_TITLE_PERFORMANCE),
                         LOCALIZATION(C_TABMENU_WORKSHOPPERFORMANCE),
                         ];    //lokalised 28 Jan
                [self setViewControllers: @[
                                           performanceWorkshopNav,
                                           performancePointsNav,
                                           ]];
            }
            
            if([mSession loadPerformance]) {
                [self setSelectedIndex:[self.viewControllers indexOfObject:performanceWorkshopNav]];
                [mSession setLoadPerformance:NO];
            }
        }
            break;
        case kPROFILETYPE_MECHANIC:
        case kPROFILETYPE_FORECOURTATTENDANT:
        {
            [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_PERFORMANCE) subtitle: @""]; //lokalised 28 Jan
            
            UINavigationController *performancePointsNav;
           
            performancePointsViewController  = [STORYBOARD_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_PERFORMANCEPOINTS];
            performancePointsNav = [[UINavigationController alloc] initWithRootViewController: performancePointsViewController];
            
            if(IS_COUNTRY(COUNTRYCODE_INDONESIA)) {
                coinsPerformanceController  = [STORYBOARD_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_MYCOIN_PERFORMANCE];
                performancePointsNav = [[UINavigationController alloc] initWithRootViewController: coinsPerformanceController];
            }
            
            if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND]) {
                 thRetailMyPerformanceViewController = [STORYBOARD_THRetail instantiateViewControllerWithIdentifier:VIEW_TH_MYPERFORMANCE];
                performanceWorkshopNav = [[UINavigationController alloc] initWithRootViewController: thRetailMyPerformanceViewController];
                [performanceWorkshopNav setNavigationBarHidden: YES];
            }
            [performancePointsNav setNavigationBarHidden: YES];
            title = @[IS_COUNTRY(COUNTRYCODE_INDONESIA)?LOCALIZATION(C_TITLE_MYCOINS):LOCALIZATION(C_TITLE_MYPOINTS),           //lokalised 28 Jan
                      LOCALIZATION(C_TITLE_MECHPERFORMANCE),    //lokalised 28 Jan
                      ];   
            
            [self setViewControllers: @[performancePointsNav,
                                        performanceWorkshopNav
                                        ]];
            if([mSession loadPerformance]) {
                [self setSelectedIndex:[self.viewControllers indexOfObject:performanceWorkshopNav]];
                [mSession setLoadPerformance:NO];
            }
            
        }
            break;
        default:
            break;
    }

    
    UIImage *selected = GET_ISADVANCE ? [UIImage imageNamed:@"tab-bg_blue"] : [UIImage imageNamed:@"tab-bg"];
    UIImage *unselected = [UIImage imageNamed:@"tab-bg_grey.png"];
    if (kIsRightToLeft) {
        title = title.reverseObjectEnumerator.allObjects.mutableCopy;
    }
    for (int i = 0; i < [self.tabBar.items count]; i++)
    {
        RDVTabBarItem *item = [self.tabBar.items objectAtIndex: i];
        [item setTitle: [title objectAtIndex: i]];
        
        [item setBackgroundColor: COLOUR_WHITE];
        [item setTitleUnselectedAttributes:FONT_H1 colour:COLOUR_VERYDARKGREY];
        [item setTitleSelectedAttributes:FONT_H1 colour:COLOUR_RED];
        
        [item setBackgroundSelectedImage: selected withUnselectedImage: unselected];
    }
    if (kIsRightToLeft) {
        [self tabBar:self.tabBar didSelectItemAtIndex:self.viewControllers.count-1];
    }
}

#pragma mark RDVTabBarDelegate
- (BOOL)tabBar:(RDVTabBar *)tabBar shouldSelectItemAtIndex:(NSInteger)index
{
    for (RDVTabBarItem *item in [tabBar items])
    {
        
    }
    return ([tabBar selectedItem] != [[tabBar items] objectAtIndex: index]);
}

- (void)tabBar:(RDVTabBar *)tabBar didSelectItemAtIndex:(NSInteger)index
{
    //super method MUST BE PRESENT
    [super tabBar:tabBar didSelectItemAtIndex: index];
    
    if (index == [[tabBar items] count] - 1) //add new to be last object of tab
    {
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
