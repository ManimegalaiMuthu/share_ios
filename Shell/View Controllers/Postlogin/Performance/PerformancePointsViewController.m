//
//  PerformancePointsViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 24/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "PerformanceTabViewController.h"
#import "PerformancePointsViewController.h"

@interface PerformancePointsViewController ()<WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource, CommonListDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet UIButton *btnStartDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEndDate;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIButton *btnType;
@property (weak, nonatomic) IBOutlet UIButton *goBtn;
@property (weak, nonatomic) IBOutlet UILabel *datePickerTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property (weak, nonatomic) IBOutlet UIView *borderView;
@property (weak, nonatomic) IBOutlet UIButton *doneBtn;


@property NSMutableArray *transactionArray;
@end

@implementation PerformancePointsViewController
@synthesize listView;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //[[WebServiceManager sharedInstance] fetchTransactionHistory:];
    
    if (![self.parentViewController.parentViewController isKindOfClass: [PerformanceTabViewController class]])
    {
        [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_WORKSHOPPERFORMANCE) subtitle:@"" size:15 subtitleSize:0];   //lokalised 28 Jan
    }
    
    self.datePickerView.hidden = YES;
    
    
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_POINTS_SEARCHBYTYPE) list: [mSession getTransactionTypes] selectionType:ListSelectionTypeSingle previouslySelected: nil];   //lokalised 28 Jan
    listView.delegate = self;
    
    self.tableView.tableFooterView = [UIView new];
    
    [self.borderView setBackgroundColor:COLOUR_RED];
    
    if (GET_PROFILETYPE == kPROFILETYPE_MECHANIC)
    {
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_MECHPERFORMANCE) subtitle: @""];  //lokalised 28 Jan
    }
    
    [self setupInterface];
    
    [[WebServiceManager sharedInstance] fetchTransactionHistory:@{@"FromDate":@"",
                                                                  @"ToDate":@"",
                                                                  @"ServiceType": [mSession convertToTransactionTypesKeyCode: self.btnType.titleLabel.text]} vc:self];
    
    if (kIsRightToLeft) {
        [self.btnType setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    } else {
        [self.btnType setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_WORKSHOPPERFORMANCE),LOCALIZATION_EN(C_TITLE_MYPOINTS)] screenClass:nil];
}

-(void) setupInterface
{
    [Helper setCustomFontButtonContentModes: self.btnType];
    [self.btnType.titleLabel setFont: FONT_B1];
    [self.btnType setTitle: LOCALIZATION(C_POINTS_SEARCHBYTYPE) forState: UIControlStateNormal];    //lokalised 28 Jan
    
    
    [Helper setCustomFontButtonContentModes: self.btnStartDate];
    [self.btnStartDate.titleLabel setFont: FONT_B2];
    [self.btnStartDate setTitle: LOCALIZATION(C_POINTS_STARTDATE) forState: UIControlStateNormal];  //lokalised 28 Jan
    
    [Helper setCustomFontButtonContentModes: self.btnEndDate];
    [self.btnEndDate.titleLabel setFont: FONT_B2];
    [self.btnEndDate setTitle: LOCALIZATION(C_POINTS_ENDDATE) forState: UIControlStateNormal];  //lokalised 28 Jan
    
    [Helper setCustomFontButtonContentModes: self.goBtn];
    [self.goBtn.titleLabel setFont: FONT_B1];
    [self.goBtn setTitle: LOCALIZATION(C_POINTS_GO) forState: UIControlStateNormal];    //lokalised 28 Jan
    [self.goBtn setBackgroundColor:COLOUR_RED];
    
    [self.lblTo setFont: FONT_B2];
    self.lblTo.text =  LOCALIZATION(C_POINTS_TO);   //lokalised 28 Jan
    
    [self.doneBtn setTitle: LOCALIZATION(C_FORM_DONE) forState: UIControlStateNormal];
    
    self.datePicker.locale = [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
}

- (IBAction)typePressed:(id)sender {
    [listView setTitleAndList: LOCALIZATION(C_POINTS_SEARCHBYTYPE) list: [mSession getTransactionTypes] hasSearchField: NO];    //lokalised 28 Jan
    
    [self popUpView];
}
-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        [self.btnType setTitle: selection.firstObject forState:UIControlStateNormal];
        [self goPressed:nil];
    }
}

- (IBAction)startDatePress:(id)sender
{
    self.datePicker.tag = 1;
    self.datePickerTitle.text = LOCALIZATION(C_POINTS_STARTDATE);   //lokalised 28 Jan
    self.datePickerView.hidden = NO;
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
   
    if(kIsRightToLeft) {
        [outputFormatter setDateFormat:@"MMM"];
    } else {
        [outputFormatter setDateFormat:@"dd MMM yyyy"];
    }
    if ([GET_LOCALIZATION isEqualToString: kThai])
    {
        [outputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
        [outputFormatter setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierBuddhist]];
    } else {
        [outputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    }
    self.datePicker.minimumDate = nil;
    
    //date cannot be greater than end date
    NSDate *endMaxDate = [outputFormatter dateFromString: self.btnEndDate.titleLabel.text];
    if (endMaxDate)
        self.datePicker.maximumDate = endMaxDate;
    else
        self.datePicker.maximumDate = [NSDate date];
}

- (IBAction)toDatePressed:(id)sender
{
    self.datePicker.tag = 2;
    self.datePickerTitle.text = LOCALIZATION(C_POINTS_ENDDATE); //lokalised 28 Jan
    self.datePickerView.hidden = NO;
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
   
    if(kIsRightToLeft) {
        [outputFormatter setDateFormat:@"MMM"];
    } else {
        [outputFormatter setDateFormat:@"dd MMM yyyy"];
    }
    if ([GET_LOCALIZATION isEqualToString: kThai])
    {
        [outputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
        [outputFormatter setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierBuddhist]];
    } else {
        [outputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    }
    
    //date cannot be lesser than start date
    NSDate *startMinDate = [outputFormatter dateFromString: self.btnStartDate.titleLabel.text];
    if (startMinDate)
        self.datePicker.minimumDate = startMinDate;
    else
        self.datePicker.minimumDate = nil;
    
    //end date cannot be greater than today's date
    self.datePicker.maximumDate = [NSDate date];
}

- (IBAction)datePickerDonePressed:(id)sender {
    self.datePickerView.hidden = YES;
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    if(kIsRightToLeft) {
         [outputFormatter setDateFormat:@"MMM"];
    } else {
         [outputFormatter setDateFormat:@"dd MMM yyyy"];
    }
    if ([GET_LOCALIZATION isEqualToString: kThai])
    {
        [outputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
        [outputFormatter setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierBuddhist]];
    } else {
        [outputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    }
    if(self.datePicker.tag == 1) {
        if(kIsRightToLeft) {
            NSCalendar* calendar = [NSCalendar currentCalendar];
            NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitDay fromDate:self.datePicker.date];
            
            [self.btnStartDate setTitle:[NSString stringWithFormat:@"%ld %@ %ld",(long)[components day],[outputFormatter stringFromDate:self.datePicker.date],(long)[components year]] forState:UIControlStateNormal];
        } else {
             [self.btnStartDate setTitle:[outputFormatter stringFromDate:self.datePicker.date] forState:UIControlStateNormal];
        }
    }
    else {
        if(kIsRightToLeft) {
            NSCalendar* calendar = [NSCalendar currentCalendar];
            NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitDay fromDate:self.datePicker.date];
            
            [self.btnEndDate setTitle:[NSString stringWithFormat:@"%ld %@ %ld",(long)[components day],[outputFormatter stringFromDate:self.datePicker.date],(long)[components year]] forState:UIControlStateNormal];
        } else {
            [self.btnEndDate setTitle:[outputFormatter stringFromDate:self.datePicker.date] forState:UIControlStateNormal];
        }
    }
    
}
- (IBAction)goPressed:(id)sender {
    self.datePickerView.hidden = YES;
    
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    if ([GET_LOCALIZATION isEqualToString: kThai])
    {
        [inputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
        [inputFormatter setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierBuddhist]];
    } else {
        [inputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    }
    [inputFormatter setDateFormat:@"dd MMM yyyy"];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy/MM/dd"];
    [outputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kEnglish]];
    
    NSDate *startDate = [inputFormatter dateFromString: self.btnStartDate.titleLabel.text];
    NSString *startDateString = @"";
    if (startDate)
        startDateString = [outputFormatter stringFromDate: startDate];
    
    NSDate *endDate = [inputFormatter dateFromString: self.btnEndDate.titleLabel.text];
    NSString *endDateString = @"";
    if (endDate)
        endDateString = [outputFormatter stringFromDate: endDate];
    
    [[WebServiceManager sharedInstance] fetchTransactionHistory: @{@"FromDate":startDateString,
                                                                  @"ToDate":endDateString,
                                                                  @"ServiceType": [mSession convertToTransactionTypesKeyCode: self.btnType.titleLabel.text]} vc:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.transactionArray count] > 0)
        return [self.transactionArray count] + 1;
    else
        return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.row == 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"PointsHeaderCell"];
        
//        UILabel *lblDateHeader = [cell viewWithTag: 1];
//        lblDateHeader.text = [NSString stringWithFormat: @"%@\r%@", LOCALIZATION(C_POINTS_DATE), @"(DD/MM/YY)"];  //lokalised 28 Jan
//        lblDateHeader.font = FONT_H1;

        TTTAttributedLabel *lblDateHeader = [cell viewWithTag: 1];
        NSString *text = [NSString stringWithFormat: @"%@\r%@", LOCALIZATION(C_POINTS_DATE), @"(DD/MM/YY)"];    //lokalised 28 Jan
        lblDateHeader.font = FONT_H1;

        [lblDateHeader setText:text afterInheritingLabelAttributesAndConfiguringWithBlock:^(NSMutableAttributedString *mutableAttributedString) {
            NSRange whiteRange = [text rangeOfString:@"(DD/MM/YY)"];
            if (whiteRange.location != NSNotFound) {
                // Core Text APIs use C functions without a direct bridge to UIFont. See Apple's "Core Text Programming Guide" to learn how to configure string attributes.
                [mutableAttributedString addAttribute: NSFontAttributeName value: FONT_B1 range:whiteRange];
            }

            return mutableAttributedString;
        }];
        
        UILabel *lblDescHeader = [cell viewWithTag: 2];
        lblDescHeader.font = lblDateHeader.font;
        lblDescHeader.text = LOCALIZATION(C_POINTS_DESCRIPTION);    //lokalised 28 Jan
        
        UILabel *lblPointsHeader = [cell viewWithTag: 3];
        lblPointsHeader.font = lblDateHeader.font;
        lblPointsHeader.text = LOCALIZATION(C_POINTS_POINTS);       //lokalised 28 Jan
        
        if (kIsRightToLeft) {
            lblDateHeader.textAlignment = NSTextAlignmentRight;
            lblPointsHeader.textAlignment = NSTextAlignmentLeft;
        } else {
            lblDateHeader.textAlignment = NSTextAlignmentLeft;
            lblPointsHeader.textAlignment = NSTextAlignmentRight;
        }
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"PointsCell"];
     
        NSDictionary *pointsDict = [self.transactionArray objectAtIndex: indexPath.row - 1];
        
        UILabel *lblDate = [cell viewWithTag: 1];
        lblDate.font = FONT_B1;
        lblDate.text = [pointsDict objectForKey: @"TransactionDate"];
        
        UILabel *lblDesc = [cell viewWithTag: 2];
        lblDesc.font = lblDate.font;
        lblDesc.text = [pointsDict objectForKey: @"Description"];
        
        UILabel *lblPoints = [cell viewWithTag: 3];
        lblPoints.font = FONT_H(30);
        if ([[pointsDict objectForKey: @"TypeColor"] intValue] == 1)
            lblPoints.textColor = COLOUR_DARKGREEN;
        else if ([[pointsDict objectForKey: @"TypeColor"] intValue] == 2)
            lblPoints.textColor = COLOUR_LIGHTGREY;
        else
            lblPoints.textColor = COLOUR_RED;
        
        lblPoints.text = [[pointsDict objectForKey: @"Points"] stringValue]; //server returning number value
        
        if (kIsRightToLeft) {
            lblPoints.textAlignment = NSTextAlignmentLeft;
        } else {
            lblPoints.textAlignment = NSTextAlignmentRight;
        }
    }
    
    return cell;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)processCompleted:(WebServiceResponse *)response
{
    self.transactionArray = [[response getGenericResponse] objectForKey: @"TransactionHistory"];
    
    [self.tableView reloadData];
}
-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

@end
