//
//  RegisterCodeResultViewController.m
//  Shell
//
//  Created by Admin on 7/9/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "RegisterCodeResultViewController.h"
#import "PopupTagDecalToVehicleViewController.h"
#import "PopupInfoTableViewController.h"

#define CELL_HEIGHT 70
@interface RegisterCodeResultViewController () <UITableViewDelegate,UITableViewDataSource, PopupTagDecalToVehicleDelegate>
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property NSArray *couponArray;
@property NSString *vehicleNumber;

@property NSMutableDictionary *vehicleIdDict;

@property (weak, nonatomic) IBOutlet UIButton *btnTagDecal;
@end

@implementation RegisterCodeResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_REGISTERCODERESULT) subtitle: @"" size: 15 subtitleSize: 0];      //lokalise 24 Jan
    
    if(self.isScanBottle) {
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_GLOBAL_SUCCESS) subtitle:@""];
    }
    
    [self.btnBack setTintColor: COLOUR_RED];
    [self.btnBack.titleLabel setFont: FONT_BUTTON];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    
    
    if (![[mSession profileInfo] hasDecal])
    {
        self.btnTagDecal.hidden = YES;
        [NSLayoutConstraint constraintWithItem:self.btnTagDecal attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
    }
    else
    {
        [self.btnTagDecal setTitle: LOCALIZATION(C_DECAL_TAGDECALTOVEHICLE) forState:UIControlStateNormal];         //lokalise 24 Jan
        [_btnTagDecal setBackgroundColor:COLOUR_RED];
        [self.btnTagDecal.titleLabel setFont: FONT_BUTTON];
        [NSLayoutConstraint constraintWithItem:self.btnTagDecal attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:50].active = YES;
    }
    
    self.tableView.tableFooterView = [UIView new];
    self.tableView.estimatedRowHeight = 100;
    self.tableView.estimatedSectionHeaderHeight = 100;

    self.couponArray = [self.resultsDict objectForKey: @"FullCoupons"];
    
    if (kIsRightToLeft) {
        [self.btnBack setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.btnBack setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
    
    [self setupVehicleSections];
    [self.tableView reloadData];
}

-(void) setupVehicleSections
{
    self.vehicleIdDict = [[NSMutableDictionary alloc] init];
    
    for (NSDictionary *couponDict in self.couponArray)
    {
        NSString *vehicleIdString = [couponDict objectForKey: @"VehicleNumber"];
        
        NSMutableArray *couponsArray = [self.vehicleIdDict objectForKey: vehicleIdString];
        //if dictionary dont already contain vehicle id
        if(!couponsArray)
        {
            //create new mutable array and set
            couponsArray = [[NSMutableArray alloc] init];
            
        }
        //add coupon into array
        [couponsArray addObject: couponDict];
        [self.vehicleIdDict setObject: couponsArray forKey: vehicleIdString];
    }
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    
    if(self.isScanBottle) {
        [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / Success",LOCALIZATION_EN(C_QL_SCANBOTTLE)] screenClass:nil];
    } else {
        [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / Success",LOCALIZATION_EN(C_TITLE_REGISTERCODERESULT)] screenClass:nil];
    }
    
}

- (IBAction)tagDecalPressed:(id)sender {
    NSString *vehicleId = [self.resultsDict objectForKey: @"VehicleId"];
    NSString *stateName = [self.resultsDict objectForKey: @"StateName"];
    
    [self initTagDecalToVehicleWithVehicleId: vehicleId stateName: stateName];
}

//delegate method
-(void) didTagVehicle
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backPressed:(id)sender {
    if(![[mSession profileInfo] hasShowScanSearchVehicle] && [[self.navigationController childViewControllers] count] >= 2) {
        [self popToIndex:0];
    } else if([[mSession profileInfo] hasLubeMatch] && [[self.navigationController childViewControllers] count] >= 2) {
        [self popToIndex:0];
    }
    else
        [self popSelf];
}

#pragma mark - tableview delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //number of section = number of vehicle id
    if ([self.vehicleIdDict count] > 0)
    {
        return [self.vehicleIdDict count] + [[self.resultsDict objectForKey: @"CanInvite"] boolValue]; //if caninvite, add one more section for one more cell
    }
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //last row
    if (section == [self.vehicleIdDict count])
        return [[self.resultsDict objectForKey: @"CanInvite"] boolValue];
    else
    {
        NSString *vehicleIdKey = [[self.vehicleIdDict allKeys] objectAtIndex: section];
        NSArray *vehicleCouponsArray = [self.vehicleIdDict objectForKey: vehicleIdKey];
        return [vehicleCouponsArray count];
    }
//    if ([self.couponArray count] > 0)
//        return [self.couponArray count] + [[self.resultsDict objectForKey: @"CanInvite"] boolValue]; //if caninvite, add one more cell
    return 0;
}

enum
{
    kCODERESULTS_STATUS_SUCCESS =   1 << 0,
    kCODERESULTS_STATUS_USED =      1 << 1,
    kCODERESULTS_STATUS_INVALID =   1 << 2,
    kCODERESULTS_STATUS_UNAUTH =    1 << 3,
} kCODERESULTS_STATUS_TAG;

enum
{
    kCODERESULTS_HEADER_VEHICLEID_LABEL = 1,
    kCODERESULTS_HEADER_VEHICLEID,
    kCODERESULTS_HEADER_CODEDESC,
    kCODERESULTS_HEADER_STATUS,
    kCODERESULTS_HEADER_ICON_TEXT = 100,
    kCODERESULTS_HEADER_ICON_IMAGE,
    kCODERESULTS_HEADER_ICON_TEXTTWO,
} kCODERESULTS_HEADER_TAG;

enum
{
    kCODERESULTS_HEADER_IN_NAME_LABEL = 1,
    kCODERESULTS_HEADER_IN_NAME,
    kCODERESULTS_HEADER_IN_SHARECODE_LABEL,
    kCODERESULTS_HEADER_IN_SHARECODE,
    kCODERESULTS_HEADER_IN_CODEDESC,
    kCODERESULTS_HEADER_IN_STATUS,
    kCODERESULTS_HEADER_IN_ICON_TEXT = 100,
    kCODERESULTS_HEADER_IN_ICON_IMAGE,
    kCODERESULTS_HEADER_IN_ICON_TEXTTWO,
} kCODERESULTS_HEADER_IN_TAG;

enum
{
    kCODERESULTS_CONTENT_CODEDESC        = 1,
    kCODERESULTS_CONTENT_STATUS_IMAGE,
    kCODERESULTS_CONTENT_STATUS,
} kCODERESULTS_CONTENT_TAG;

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == [self.vehicleIdDict count])
    {
        return 0;
    }
    return UITableViewAutomaticDimension;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == [self.vehicleIdDict count])
    {
        return [UIView new];
    }
    else if(self.isScanBottle && !self.isConsumer) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"INScanBottleHeader"];
        
            UILabel *lblNameHeader = [cell viewWithTag: kCODERESULTS_HEADER_IN_NAME_LABEL];
            lblNameHeader.text = LOCALIZATION(C_HOMECAROUSEL_NAME);
            lblNameHeader.font = FONT_H1;
            
            UILabel *lblName = [cell viewWithTag: kCODERESULTS_HEADER_IN_NAME];
            lblName.text = [self.resultsDict objectForKey:@"WorkshopName"];
            lblName.font = FONT_B1;
        
            UILabel *lblShareCodeHeader = [cell viewWithTag: kCODERESULTS_HEADER_IN_SHARECODE_LABEL];
            lblShareCodeHeader.text = LOCALIZATION(C_SCANBOTTLE_RESULT_SHARECODE);
            lblShareCodeHeader.font = FONT_H1;
            
            UILabel *lblShareCode = [cell viewWithTag: kCODERESULTS_HEADER_IN_SHARECODE];
            lblShareCode.text = [self.resultsDict objectForKey:@"Sharecode"];
            lblShareCode.font = FONT_B1;
        
            UILabel *lblCodeDesc = [cell viewWithTag: kCODERESULTS_HEADER_IN_CODEDESC];
            lblCodeDesc.text = LOCALIZATION(C_RESULT_CODEDESCRIPTION);
            lblCodeDesc.font = FONT_H1;
            
            UILabel *lblStatus = [cell viewWithTag: kCODERESULTS_HEADER_IN_STATUS];
            lblStatus.text = LOCALIZATION(C_RESULT_STATUS);
            lblStatus.font = FONT_H1;
            
            
            UILabel *lblIconText = [cell viewWithTag: kCODERESULTS_HEADER_IN_ICON_TEXT];
            UILabel *lblIconText2 = [cell viewWithTag: kCODERESULTS_HEADER_IN_ICON_TEXTTWO];
            lblIconText.text = LOCALIZATION(C_REGISTEROILCHANGE_IN_CLICKON);
            lblIconText.font = FONT_B1;
              
            lblIconText2.text = LOCALIZATION(C_REGISTEROILCHANGE_IN_STATUSINFO);
            lblIconText2.font = FONT_B1;
        
        if (kIsRightToLeft) {
            [lblIconText setTextAlignment:NSTextAlignmentRight];
            [lblIconText2 setTextAlignment:NSTextAlignmentRight];
        } else {
            [lblIconText setTextAlignment:NSTextAlignmentLeft];
            [lblIconText2 setTextAlignment:NSTextAlignmentLeft];
        }
        
        if (kIsRightToLeft) {
            [lblNameHeader setTextAlignment:NSTextAlignmentRight];
            [lblName setTextAlignment:NSTextAlignmentRight];
            [lblShareCodeHeader setTextAlignment:NSTextAlignmentRight];
            [lblShareCode setTextAlignment:NSTextAlignmentRight];
            [lblCodeDesc setTextAlignment:NSTextAlignmentRight];
            [lblStatus setTextAlignment:NSTextAlignmentLeft];
        } else {
            [lblNameHeader setTextAlignment:NSTextAlignmentLeft];
            [lblName setTextAlignment:NSTextAlignmentLeft];
            [lblShareCodeHeader setTextAlignment:NSTextAlignmentLeft];
            [lblShareCode setTextAlignment:NSTextAlignmentLeft];
            [lblCodeDesc setTextAlignment:NSTextAlignmentLeft];
            [lblStatus setTextAlignment:NSTextAlignmentRight];
        }
        
            return cell.contentView;
    } else {
           UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PendingCodeHeader"];

           UILabel *lblVehicleIdHeader = [cell viewWithTag: kCODERESULTS_HEADER_VEHICLEID_LABEL];
           lblVehicleIdHeader.text = LOCALIZATION(C_RESULT_VEHICLEID);                             //lokalise 24 Jan
           lblVehicleIdHeader.font = FONT_H1;
           
           UILabel *lblVehicleId = [cell viewWithTag: kCODERESULTS_HEADER_VEHICLEID];
           lblVehicleId.text = [[self.vehicleIdDict allKeys] objectAtIndex: section];
           self.vehicleNumber = [[self.vehicleIdDict allKeys] objectAtIndex: section];
           lblVehicleId.font = FONT_B1;
           
           UILabel *lblCodeDesc = [cell viewWithTag: kCODERESULTS_HEADER_CODEDESC];
           lblCodeDesc.text = LOCALIZATION(C_RESULT_CODEDESCRIPTION);                                  //lokalise 24 Jan
           lblCodeDesc.font = FONT_H1;
           
           UILabel *lblStatus = [cell viewWithTag: kCODERESULTS_HEADER_STATUS];
           lblStatus.text = LOCALIZATION(C_RESULT_STATUS);                                             //lokalise 24 Jan
           lblStatus.font = FONT_H1;
           
           
           UILabel *lblIconText = [cell viewWithTag: kCODERESULTS_HEADER_ICON_TEXT];
           UILabel *lblIconText2 = [cell viewWithTag: kCODERESULTS_HEADER_ICON_TEXTTWO];
           lblIconText.text = LOCALIZATION(C_REGISTEROILCHANGE_IN_CLICKON);                        //lokalise 24 Jan
           lblIconText.font = FONT_B1;
           
           lblIconText2.text = LOCALIZATION(C_REGISTEROILCHANGE_IN_STATUSINFO);                    //lokalise 24 Jan
           lblIconText2.font = FONT_B1;
           
           if (kIsRightToLeft) {
               [lblIconText setTextAlignment:NSTextAlignmentRight];
               [lblIconText2 setTextAlignment:NSTextAlignmentRight];
           } else {
               [lblIconText setTextAlignment:NSTextAlignmentLeft];
               [lblIconText2 setTextAlignment:NSTextAlignmentLeft];
           }
           
           if (kIsRightToLeft) {
               [lblVehicleIdHeader setTextAlignment:NSTextAlignmentRight];
               [lblVehicleId setTextAlignment:NSTextAlignmentRight];
               [lblCodeDesc setTextAlignment:NSTextAlignmentRight];
               [lblStatus setTextAlignment:NSTextAlignmentLeft];
           } else {
               [lblVehicleIdHeader setTextAlignment:NSTextAlignmentLeft];
               [lblVehicleId setTextAlignment:NSTextAlignmentLeft];
               [lblCodeDesc setTextAlignment:NSTextAlignmentLeft];
               [lblStatus setTextAlignment:NSTextAlignmentRight];
           }
           
           return cell.contentView;
    }
    return [UIView new];
}

- (IBAction)legendPressed:(id)sender {
    [self.view endEditing: YES];
    
    PopupInfoTableViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPINFOTABLEVIEW];
    
    [self popUpViewWithPopupVC:popupVc];
    
    [popupVc initRegisterProductCodeLegendsPopup];
}



enum kProductCodeResults_TYPES
{
    kProductCodeResults_SUCCESS     = 1,
    kProductCodeResults_USED        = 1 << 1,
    kProductCodeResults_INVALID     = 1 << 2,
    kProductCodeResults_UNAUTH      = 1 << 3,
    //    kManageWorkshop_WorkshopStatus_ALL,
};
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;

    if (indexPath.section == [self.vehicleIdDict count])
    {
//        if (IS_COUNTRY(COUNTRYCODE_INDIA))
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"InviteCell"];
            
            UILabel *lblHeader = [cell viewWithTag: 1];
            lblHeader.text = LOCALIZATION(C_RUSSIA_OILCHANGERESULT_NOTCONNECTED);               //lokalise 24 Jan
            
            UILabel *lblContent = [cell viewWithTag: 2];
            lblContent.text = LOCALIZATION(C_RUSSIA_OILCHANGERESULT_REGISTERCUSTOMER);          //lokalise 24 Jan
            
            UIButton *btnRegister = [cell viewWithTag: 3];
            btnRegister.titleLabel.textAlignment = NSTextAlignmentCenter;
            [btnRegister setTitle: LOCALIZATION(C_RUSSIA_OILCHANGERESULT_INVITEBTN) forState: UIControlStateNormal];        //lokalise 24 Jan
            [btnRegister setBackgroundColor:COLOUR_RED];
            
            if(kIsRightToLeft) {
                [lblHeader setTextAlignment:NSTextAlignmentRight];
                [lblContent setTextAlignment:NSTextAlignmentRight];
            } else {
                [lblHeader setTextAlignment:NSTextAlignmentLeft];
                [lblContent setTextAlignment:NSTextAlignmentLeft];
            }
        }
//        else
//        {
//            cell = [tableView dequeueReusableCellWithIdentifier:@"CanInviteCell"];
//            
//            UILabel *header = [cell viewWithTag: 1];
//            header.text = LOCALIZATION(C_REGISTER_RESULT_NOTCONNECTED);
//            header.font = FONT_H3;
//            
//            TTTAttributedLabel *link = [cell viewWithTag: 2];
//            link.delegate = self;
//            [Helper setHyperlinkLabel:link hyperlinkText:LOCALIZATION(C_INVITE_CUSTOMER) bodyText:LOCALIZATION(C_INVITE_CUSTOMER) urlString: @"Invite"];
//        }
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"PendingCodeCell"];
        
        NSString *vehicleIdString = [[self.vehicleIdDict allKeys] objectAtIndex: indexPath.section];
        NSDictionary *couponData = [[self.vehicleIdDict objectForKey: vehicleIdString] objectAtIndex: indexPath.row];
        
        UILabel *lblCodeDesc = [cell viewWithTag: kCODERESULTS_CONTENT_CODEDESC];
        lblCodeDesc.font = FONT_B1;
        
        if ([[couponData objectForKey: @"Desc"] length] > 0)
            lblCodeDesc.text = [NSString stringWithFormat:@"%@ \r%@",[couponData objectForKey: @"Code"],[couponData objectForKey: @"Desc"]];
        else
            lblCodeDesc.text = [NSString stringWithFormat:@"%@",[couponData objectForKey: @"Code"]];
        
        UILabel *lblStatus = [cell viewWithTag: kCODERESULTS_CONTENT_STATUS];
        lblStatus.text = [couponData objectForKey: @"Status"];
        lblStatus.font = FONT_B1;
        
        UIImageView *imgStatus = [cell viewWithTag: kCODERESULTS_CONTENT_STATUS_IMAGE];
        switch ([[couponData objectForKey: @"StatusID"] intValue])
        {
            case kProductCodeResults_SUCCESS:
                [imgStatus setImage: [UIImage imageNamed: @"icn_success_small"]];
                break;
            case kProductCodeResults_USED:
                [imgStatus setImage: [UIImage imageNamed: @"icn_used_small"]];
                break;
            case kProductCodeResults_INVALID:
                [imgStatus setImage: [UIImage imageNamed: @"icn_invalid_small"]];
                break;
            case kProductCodeResults_UNAUTH:
                [imgStatus setImage: [UIImage imageNamed: @"icn_unathorized_small"]];
                break;
            default:
                break;
        }
        
        if(kIsRightToLeft) {
            [lblCodeDesc setTextAlignment:NSTextAlignmentRight];
            [lblStatus setTextAlignment:NSTextAlignmentCenter];
        } else {
            [lblCodeDesc setTextAlignment:NSTextAlignmentLeft];
            [lblStatus setTextAlignment:NSTextAlignmentCenter];
        }
        
    }
    return cell;
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url absoluteString] isEqualToString: @"Invite"])
        [mSession pushRegisterConsumer:@{@"PromoCode" : @"",
                                         @"VehicleNumber": @"", @"VehicleStateID": @""} vc: self]; //no promo code
}

- (IBAction)registerPressed:(id)sender
{
    [mSession loadRegisterCustomerView:@{@"PromoCode" : @"",
                                     @"VehicleNumber": self.vehicleNumber,
                                     @"VehicleStateID": @""}]; //no promo code
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
