//
//  FAQViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 17/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "FAQViewController.h"
#import <WebKit/WebKit.h>

@interface FAQViewController () <WKNavigationDelegate>

@property (weak, nonatomic) IBOutlet UIView *faqWebViewContainer;
@property (strong,nonatomic) WKWebView *faqWebView;

@end

@implementation FAQViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_FAQ) subtitle: @""];          //lokalised

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_FAQ) screenClass:nil];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
#pragma mark - Adding WKWebView
    NSString *urlAddress = [NSString stringWithFormat:@"%@%@%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_FAQ_URL];

    WebViewHelper *webViewHelper = [[WebViewHelper alloc] init];
    [webViewHelper setWebViewWithParentView: self.faqWebViewContainer
                         navigationDelegate: self
                                 urlAddress: urlAddress];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma WKWebView delegate methods
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    [UpdateHUD addMBProgress: self.view withText:LOADING_HUD];
}

-(void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    NSLog(@"failed to load navigation");
    [UpdateHUD removeMBProgress: self.view];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [UpdateHUD removeMBProgress: self.view];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    if(navigationAction.navigationType ==  WKNavigationTypeLinkActivated) {
        [[UIApplication sharedApplication] openURL: navigationAction.request.URL];
        
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
    return;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
