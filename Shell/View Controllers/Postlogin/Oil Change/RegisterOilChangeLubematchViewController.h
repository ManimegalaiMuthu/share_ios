//
//  RegisterOilChangeLubematchViewController.h
//  Shell
//
//  Created by Nach on 20/1/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "BaseVC.h"
#import "RegisterOilChangeSearchViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RegisterOilChangeLubematchViewController : BaseVC

//@property NSArray *vehicleLubeArray;
@property NSString *vehicleID;
@property RegisterOilChangeSearchViewController *parentVC;

@end

NS_ASSUME_NONNULL_END
