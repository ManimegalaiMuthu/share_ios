//
//  RegisterOilChangeSearchViewController.m
//  Shell
//
//  Created by Jeremy Lua on 10/1/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "RegisterOilChangeSearchViewController.h"
#import "QRCodeScanner.h"
#import "PopupTagDecalViewController.h"
#import "PopupTagDecalToVehicleViewController.h"
#import "RegisterCustomerCompleteViewController.h"
#import "PopupCameraDecalViewController.h"

#import "SWGApiClient.h"
#import "SWGConfiguration.h"
// load models
#import "SWGCoordinate.h"
#import "SWGInlineResponse200.h"
#import "SWGInlineResponse200ProcessingTime.h"
#import "SWGInlineResponse400.h"
#import "SWGPlateCandidate.h"
#import "SWGPlateDetails.h"
#import "SWGRegionOfInterest.h"
#import "SWGVehicleCandidate.h"
#import "SWGVehicleDetails.h"
// load API classes for accessing endpoints
#import "SWGDefaultApi.h"

#import "CoachMarkViewController.h"

@interface RegisterOilChangeSearchViewController () <WebServiceManagerDelegate,CommonListDelegate,QRCodeScannerDelegate, PopupTagDecalDelegate, PopupTagDecalToVehicleDelegate, RegisterCustomerCompleteViewDelegate, PopupCameraDecalDelegate,TTTAttributedLabelDelegate,UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,CoachMarkDelegate>

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerView_height;
@property (weak, nonatomic) IBOutlet UIView *stateView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stateView_height;
@property (weak, nonatomic) IBOutlet UITextField *textVehicleNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnCamera;
@property (weak, nonatomic) IBOutlet UIView *decalView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *decalView_height;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblInviteCustomer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblInviteCustomer_height;
@property (weak, nonatomic) IBOutlet UIImageView *progressbarNumberOneImage;
@property (weak, nonatomic) IBOutlet UIImageView *progressbarNumberTwoImage;
@property (weak, nonatomic) IBOutlet UIView *progressBarView;

//State View
@property (weak, nonatomic) IBOutlet UILabel *lblState;

//Decal View
@property (weak, nonatomic) IBOutlet UILabel *lblShareDecal;
@property (weak, nonatomic) IBOutlet UIButton *btnTagNewDecal;
@property (weak, nonatomic) IBOutlet UIImageView *imgDecalIcon;

@property (weak, nonatomic) IBOutlet UIButton *btnContinue;

@property (weak, nonatomic) IBOutlet UIView *emptyView;
@property (weak, nonatomic) IBOutlet UILabel *lblEmptyContent;
@property (weak, nonatomic) IBOutlet UIImageView *imgEmptyView;

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UICollectionView *headerCollectionView;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property NSArray *stateTable;
@property NSMutableArray *headerCategoryCellArray;

@property BOOL isVehicleTagged;
@property NSString *scannedDecalCode;

@property BOOL isDetailSelected;
@property BOOL isRecommended;
//@property BOOL hasLubematch;
@property BOOL hasLubematchGetBtn;
@property BOOL hasHistory;

@property NSMutableArray *tableSectionHeadingArray;
@property NSArray *offerArray;
@property NSArray *serviceHistoryArray;
@property NSMutableArray *vehicleInfoArray;
//@property NSDictionary *oatsResultDict;
@property NSMutableArray *primaryRecommendations;
@property NSMutableArray *otherRecommendations;

@property NSArray *vehicleLubeMatchArray;
@property NSString *lastUsedProductImage;
@property NSString *lastUsedProductTitle;

@end

@implementation RegisterOilChangeSearchViewController
@synthesize listView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_SCANPRODUCTCODE) subtitle: @""];
    
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
    {

        [[WebServiceManager sharedInstance] loadState: [[mSession lookupTable] objectForKey: @"LookUpVersion"]
                                          countryCode: GET_COUNTRY_CODE
                                                   vc: self];
        self.lblState.font = FONT_B1;
        self.lblState.textColor = COLOUR_VERYDARKGREY;
        self.lblState.text = LOCALIZATION(C_THAI_VEHICLE_STATE);
        self.stateView_height.constant = 30;
        self.stateView.hidden = NO;
    }
    else
    {
        self.stateView_height.constant = 0;
        self.headerView_height.constant = self.headerView_height.constant - 30.0;
        self.stateView.hidden = YES;
    }
    
    if (kIsRightToLeft) {
        [self.textVehicleNumber setTextAlignment:NSTextAlignmentRight];
    } else {
        [self.textVehicleNumber setTextAlignment:NSTextAlignmentLeft];
    }
    
    if(GET_ISADVANCE) {
        [self.progressbarNumberOneImage setImage: [UIImage imageNamed:@"progressnum-blue-1"]];
    } else {
        [self.progressbarNumberOneImage setImage: [UIImage imageNamed:@"progressnum-red-1"]];
    }
    [self.progressbarNumberTwoImage setImage: [UIImage imageNamed:@"progressnum-grey-2"]];
    
    self.stateTable = [[NSArray alloc] init];
    self.headerCategoryCellArray = [[NSMutableArray alloc] init];
    self.tableSectionHeadingArray = [[NSMutableArray alloc] init];
    self.offerArray = [[NSArray alloc] init];
    self.serviceHistoryArray = [[NSArray alloc] init];
    self.vehicleInfoArray = [[NSMutableArray alloc] init];
    
    //By Default hide DecalView
    self.decalView_height.constant = 0;
    self.decalView.hidden = YES;
    self.headerView_height.constant = self.headerView_height.constant - 70.0;
    
    self.lblInviteCustomer.hidden = YES;
    self.lblInviteCustomer_height.constant = 0;
    self.headerView_height.constant = self.headerView_height.constant - 40.0;
    
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 12, 10)];
    [self.textVehicleNumber setLeftViewMode:UITextFieldViewModeAlways];
    [self.textVehicleNumber setLeftView:spacerView];
    
    self.stateTable = [[NSMutableArray alloc] init];
    self.vehicleInfoArray = [[NSMutableArray alloc] init];
    self.offerArray = [[NSArray alloc] init];
    self.serviceHistoryArray = [[NSArray alloc] init];
    self.headerCategoryCellArray = [[NSMutableArray alloc] init];
    self.vehicleLubeMatchArray = [[NSArray alloc] init];
    self.primaryRecommendations = [[NSMutableArray alloc] init];
    self.otherRecommendations = [[NSMutableArray alloc] init];
    
    SET_LUBEMATCH_RECOMMENDED(@{});
    
    [self setUpInterface];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / Step 1",LOCALIZATION(C_TITLE_SCANPRODUCTCODE)] screenClass:nil];
    
}

-(void) setUpInterface {
    
    self.textVehicleNumber.font = FONT_B1;
    self.textVehicleNumber.placeholder = LOCALIZATION(C_SEARCHFIELD_VEHICLEID);
    self.textVehicleNumber.textColor = COLOUR_VERYDARKGREY;
    self.textVehicleNumber.delegate = self;
    
    [self.btnContinue setTitle:LOCALIZATION(C_CONSUMER_CONTINUE) forState:UIControlStateNormal];
    [self.btnContinue setBackgroundColor:COLOUR_RED];
    [self.btnContinue.titleLabel setFont:FONT_BUTTON];
    
    self.lblInviteCustomer.delegate = self;
    [Helper setHyperlinkLabel:self.lblInviteCustomer hyperlinkText:LOCALIZATION(C_INVITE_CUSTOMER) bodyText:LOCALIZATION(C_INVITE_CUSTOMER) urlString: @"Invite"];
    
    self.lblEmptyContent.text = LOCALIZATION(C_REGISTEROILCHANGE_EMPTY_CONTENT);
    self.lblEmptyContent.textColor = COLOUR_LIGHTGREY;
    self.lblEmptyContent.font = FONT_B1;
    
    self.imgEmptyView.image = [UIImage imageNamed:@"icn_vehicle_empty.png"];
    [self.imgEmptyView setHidden:NO];
    
    if(!self.lblInviteCustomer.isHidden) {
        self.lblInviteCustomer.hidden = YES;
        self.lblInviteCustomer_height.constant = 0;
        self.headerView_height.constant = self.headerView_height.constant - 40.0;
    }
    
    [self setEmptyState:YES];
    
    [self coachmarkShow];
    
}

-(void) setEmptyState:(BOOL)emptyState {
    if(emptyState) {
        self.textVehicleNumber.text = @"";
        
        [self.btnContinue setHidden:YES];
        [self.emptyView setHidden:NO];
        
        [self.containerView setHidden:YES];
    } else {
        [self.btnContinue setHidden:NO];
        [self.emptyView setHidden:YES];
        
        [self.containerView setHidden:NO];
        
        self.isDetailSelected = NO;
        [self.headerCollectionView reloadData];
    }
}

#pragma mark - Coachmark
-(void) coachmarkShow {
    NSMutableDictionary *coachmarkData = GET_COACHMARK_HASSHOWN;
    if(coachmarkData == nil) {
        coachmarkData = [[NSMutableDictionary alloc] initWithDictionary:@{}];
    }
    //For Coachmark
    if(![[coachmarkData objectForKey: LUBEMATCH_COACHMARK] boolValue])
    {
        
        NSMutableArray *coachMarkData = [[NSMutableArray alloc] init];
        [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(0,0,0,0) withCaption:LOCALIZATION(C_COACHMARK_LUBEMATCH_STEP1) andOnSide:@(kCOACHMARK_CENTER)]];
        
        [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(5.0, [[UIApplication sharedApplication] statusBarFrame].size.height + 95.0  , SCREEN_WIDTH - 10.0, 48.0) withCaption:LOCALIZATION(C_COACHMARK_LUBEMATCH_STEP2) andOnSide:@(kCOACHMARK_UP_LEFTSIDE)]];
        
        [mSession showCoachMark:coachMarkData withDelegate:nil onVc:self];

        [coachmarkData setObject: @(YES) forKey: LUBEMATCH_COACHMARK];
        SET_COACHMARK_HASSHOWN(coachmarkData);
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    else if([[coachmarkData objectForKey: LUBEMATCH_COACHMARK] boolValue] && ![[coachmarkData objectForKey: LUBEMATCH_FIRSTSEARCH_COACHMARK] boolValue] && self.emptyView.isHidden) {
        
        NSMutableArray *coachMarkData = [[NSMutableArray alloc] init];
                
        [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(5.0, [[UIApplication sharedApplication] statusBarFrame].size.height + 177.0  , SCREEN_WIDTH - 10.0, 52.0) withCaption:LOCALIZATION(C_COACHMARK_LUBEMATCH_STEP3) andOnSide:@(kCOACHMARK_UP_LEFTSIDE)]];
        
        [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(5.0, [[UIApplication sharedApplication] statusBarFrame].size.height + 177.0 , (SCREEN_WIDTH - 10.0) / 2, 52.0) withCaption:LOCALIZATION(C_COACHMARK_LUBEMATCH_STEP4) andOnSide:@(kCOACHMARK_LEFT_UPSIDE)]];
        
        [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(SCREEN_WIDTH / 2, [[UIApplication sharedApplication] statusBarFrame].size.height + 177.0 , (SCREEN_WIDTH - 15.0) / 2, 52.0) withCaption:LOCALIZATION(C_COACHMARK_LUBEMATCH_STEP5) andOnSide:@(kCOACHMARK_RIGHT_UPSIDE)]];
                
        [mSession showCoachMark:coachMarkData withDelegate:nil onVc:self];

//        [coachmarkData setObject: @(YES) forKey: LUBEMATCH_FIRSTSEARCH_COACHMARK];
//        SET_COACHMARK_HASSHOWN(coachmarkData);
//        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
}

#pragma mark - Data Set Method
-(void) setupData:(NSDictionary *)searchData {
    
    [self.vehicleInfoArray removeAllObjects];
    [self.primaryRecommendations removeAllObjects];
    [self.otherRecommendations removeAllObjects];
    
    
    if(![[searchData objectForKey:@"VehicleType"] isKindOfClass:[NSNull class]] && [searchData objectForKey:@"VehicleType"] != nil) {
        [self.vehicleInfoArray addObject:@[LOCALIZATION(C_FORM_VEHICLETYPE),[searchData objectForKey:@"VehicleType"]]];
    } else {
        [self.vehicleInfoArray addObject:@[LOCALIZATION(C_FORM_VEHICLETYPE),@""]];
    }
    
    if(![[searchData objectForKey:@"VehicleMake"] isKindOfClass:[NSNull class]] && [searchData objectForKey:@"VehicleMake"] != nil) {
        [self.vehicleInfoArray addObject:@[LOCALIZATION(C_LUBEMATCH_MAKE),[searchData objectForKey:@"VehicleMake"]]];
    } else {
        [self.vehicleInfoArray addObject:@[LOCALIZATION(C_LUBEMATCH_MAKE),@""]];
    }
    
    if(![[searchData objectForKey:@"VehicleModel"] isKindOfClass:[NSNull class]] && [searchData objectForKey:@"VehicleModel"] != nil) {
        [self.vehicleInfoArray addObject:@[LOCALIZATION(C_LUBEMATCH_MODEL),[searchData objectForKey:@"VehicleModel"]]];
    } else {
        [self.vehicleInfoArray addObject:@[LOCALIZATION(C_LUBEMATCH_MODEL),@""]];
    }
    
    if(![[searchData objectForKey:@"VehicleManufactureyear"] isKindOfClass:[NSNull class]] && [searchData objectForKey:@"VehicleManufactureyear"] != nil) {
        [self.vehicleInfoArray addObject:@[LOCALIZATION(C_LUBEMATCH_YEAR),[searchData objectForKey:@"VehicleManufactureyear"]]];
    } else {
        [self.vehicleInfoArray addObject:@[LOCALIZATION(C_LUBEMATCH_YEAR),@""]];
    }
    
    NSString *strToDisplay = @"";
    if(![[searchData objectForKey:@"MinMileage"] isKindOfClass:[NSNull class]] && [searchData objectForKey:@"MinMileage"] != nil) {
        strToDisplay = [[searchData objectForKey:@"MinMileage"] isEqualToString:@""] ? @"" : [NSString stringWithFormat:@"%@ %@",[searchData objectForKey:@"MinMileage"],LOCALIZATION(C_RU_MILEAGE_SUFFIX)];
    }
    if(![[searchData objectForKey:@"MaxMileage"] isKindOfClass:[NSNull class]] && [searchData objectForKey:@"MaxMileage"] != nil) {
        if([strToDisplay isEqualToString:@""])
            strToDisplay = [[searchData objectForKey:@"MaxMileage"] isEqualToString:@""] ? @"" : [NSString stringWithFormat:@"%@ %@",[searchData objectForKey:@"MaxMileage"],LOCALIZATION(C_RU_MILEAGE_SUFFIX)];
        else
            strToDisplay = [[searchData objectForKey:@"MaxMileage"] isEqualToString:@""] ? @"" : [strToDisplay stringByAppendingString:[NSString stringWithFormat:@"\n%@ %@",[searchData objectForKey:@"MaxMileage"],LOCALIZATION(C_RU_MILEAGE_SUFFIX)]];
    }
    if(![[searchData objectForKey:@"MileageMonthDifference"] isKindOfClass:[NSNull class]] && [searchData objectForKey:@"MileageMonthDifference"] != nil) {
        if([strToDisplay isEqualToString:@""])
            strToDisplay = [[searchData objectForKey:@"MileageMonthDifference"] isEqualToString:@""] ? @"" : [NSString stringWithFormat:@"%@ %@",[searchData objectForKey:@"MileageMonthDifference"],LOCALIZATION(C_LUBEMATCH_MONTHS)];
        else
            strToDisplay = [[searchData objectForKey:@"MileageMonthDifference"] isEqualToString:@""] ? @"" : [strToDisplay stringByAppendingString:[NSString stringWithFormat:@"\n%@ %@",[searchData objectForKey:@"MileageMonthDifference"],LOCALIZATION(C_LUBEMATCH_MONTHS)]];
    }
    [self.vehicleInfoArray addObject:@[LOCALIZATION(C_LUBEMATCH_MAINTENANCE),strToDisplay]];
    
    if(![[searchData objectForKey:@"ShewStatus"] isKindOfClass:[NSNull class]] && [searchData objectForKey:@"ShewStatus"] != nil) {
        [self.vehicleInfoArray addObject:@[LOCALIZATION(C_SEARCHVEHICLE_SHEWSTATUS),[NSString stringWithFormat:@"%@ %@",LOCALIZATION(C_LUBEMATCH_VALIDTILL),[searchData objectForKey:@"ShewStatus"]]]];
    } else {
        [self.vehicleInfoArray addObject:@[LOCALIZATION(C_SEARCHVEHICLE_SHEWSTATUS),@""]];
    }
    
    if(![[searchData objectForKey:@"ConsumerJoinDate"] isKindOfClass:[NSNull class]] && [searchData objectForKey:@"ConsumerJoinDate"] != nil) {
        [self.vehicleInfoArray addObject:@[LOCALIZATION(C_LUBEMATCH_MEMBERSINCE),[searchData objectForKey:@"ConsumerJoinDate"]]];
    } else {
        [self.vehicleInfoArray addObject:@[LOCALIZATION(C_LUBEMATCH_MEMBERSINCE),@""]];
    }
    
    if([searchData objectForKey:@"VehicleImageLube"] != nil)
        self.vehicleLubeMatchArray = [searchData objectForKey:@"VehicleImageLube"];
    if([self.vehicleLubeMatchArray isKindOfClass:[NSNull class]])
        self.vehicleLubeMatchArray = @[];
    
    if(![[searchData objectForKey:@"LastUsedImage"] isKindOfClass:[NSNull class]] && [searchData objectForKey:@"LastUsedImage"] != nil) {
        self.lastUsedProductImage = [searchData objectForKey:@"LastUsedImage"];
    } else {
        self.lastUsedProductImage = @"";
    }
    
    if(![[searchData objectForKey:@"LastUsedProduct"] isKindOfClass:[NSNull class]] && [searchData objectForKey:@"LastUsedProduct"] != nil) {
        self.lastUsedProductTitle = [searchData objectForKey:@"LastUsedProduct"];
    } else {
        self.lastUsedProductTitle = @"";
    }
        
    if([self.tableSectionHeadingArray count] > 0) {
           [self.tableSectionHeadingArray removeAllObjects];
       }
       
       if(![[searchData objectForKey: @"PromotionDetail"] isKindOfClass:[NSNull class]] && [searchData objectForKey: @"PromotionDetail"] != nil) {
           self.offerArray = [searchData objectForKey: @"PromotionDetail"];
           if([self.offerArray count] > 0)
               [self.tableSectionHeadingArray addObject:@"Redeem"];
       } else {
           self.offerArray = @[];
       }
//    if(self.hasLubematchGetBtn) // || ![self.oatsResultDict isEqualToDictionary:@{}])
//    {
        [self.tableSectionHeadingArray addObject:@"Lubematch"];
//    }
    if(self.hasHistory) {
        self.serviceHistoryArray = [searchData objectForKey: @"VehicleDetailsLube"];
        [self.tableSectionHeadingArray addObject:@"ServiceHistory"];
    } else {
        self.serviceHistoryArray = @[];
        [self.tableSectionHeadingArray addObject:@"ServiceHistory"];
    }
}

//-(void) setupOatsData
//{
////    if(![[searchData objectForKey:@"Oatsresults"] isKindOfClass:[NSNull class]] && [searchData objectForKey:@"Oatsresults"] != nil) {
////        self.oatsResultDict = [[searchData objectForKey:@"Oatsresults"] objectAtIndex:0];
//
//        if(![[self.oatsResultDict objectForKey:@"Engine"] isKindOfClass:[NSNull class]] && [self.oatsResultDict objectForKey:@"Engine"] != nil) {
//            NSArray *recommendationArray = [self.oatsResultDict objectForKey:@"Engine"];
//            if([recommendationArray count] > 0) {
//                [self.primaryRecommendations addObject:@{
//                    @"Title" : @"Engine",
//                    @"Data" : recommendationArray,
//                }];
//            }
//        }
//
//        if(![[self.oatsResultDict objectForKey:@"Transmission"] isKindOfClass:[NSNull class]] && [self.oatsResultDict objectForKey:@"Transmission"] != nil) {
//            NSArray *recommendationArray = [self.oatsResultDict objectForKey:@"Transmission"];
//            if([recommendationArray count] > 0) {
//                [self.primaryRecommendations addObject:@{
//                    @"Title" : LOCALIZATION(C_LUBEMATCH_TRANSMISSION_HEADER),
//                    @"Data" : recommendationArray,
//                }];
//            }
//        }
//
//        if(![[self.oatsResultDict objectForKey:@"Differential - rear"] isKindOfClass:[NSNull class]] && [self.oatsResultDict objectForKey:@"Differential - rear"] != nil) {
//            NSArray *recommendationArray = [self.oatsResultDict objectForKey:@"Differential - rear"];
//            if([recommendationArray count] > 0) {
//                [self.otherRecommendations addObject:@{
//                    @"Title" : LOCALIZATION(C_LUBEMATCH_REAR_HEADER),
//                    @"Data" : recommendationArray,
//                }];
//            }
//        }
//
//        if(![[self.oatsResultDict objectForKey:@"Coolant"] isKindOfClass:[NSNull class]] && [self.oatsResultDict objectForKey:@"Coolant"] != nil) {
//            NSArray *recommendationArray = [self.oatsResultDict objectForKey:@"Coolant"];
//            if([recommendationArray count] > 0) {
//                [self.otherRecommendations addObject:@{
//                    @"Title" : LOCALIZATION(C_LUBEMATCH_COOLANT_HEADER),
//                    @"Data" : recommendationArray,
//                }];
//            }
//        }
//
//        if(![[self.oatsResultDict objectForKey:@"Brake Fluid"] isKindOfClass:[NSNull class]] && [self.oatsResultDict objectForKey:@"Brake Fluid"] != nil) {
//            NSArray *recommendationArray = [self.oatsResultDict objectForKey:@"Brake Fluid"];
//            if([recommendationArray count] > 0) {
//                [self.otherRecommendations addObject:@{
//                    @"Title" : LOCALIZATION(C_LUBEMATCH_BRAKEFLUID_HEADER),
//                    @"Data" : recommendationArray,
//                }];
//            }
//        }
//
//        if(![[self.oatsResultDict objectForKey:@"Transfer box"] isKindOfClass:[NSNull class]] && [self.oatsResultDict objectForKey:@"Transfer box"] != nil) {
//            NSArray *recommendationArray = [self.oatsResultDict objectForKey:@"Transfer box"];
//            if([recommendationArray count] > 0) {
//                [self.otherRecommendations addObject:@{
//                    @"Title" : LOCALIZATION(C_LUBEMATCH_TRANSFERBOX_HEADER),
//                    @"Data" : recommendationArray,
//                }];
//            }
//        }
////
////
////    } else {
////        self.oatsResultDict = @{};
////    }
//
//}

- (IBAction)statePressed:(UIButton *)sender {
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                    views:NSDictionaryOfVariableBindings (listView)]];
}

- (IBAction)cameraPressed:(UIButton *)sender {
    if ([self.textVehicleNumber isFirstResponder])
    {
        [self.textVehicleNumber resignFirstResponder];
        return;
    }
    
    if ([[mSession profileInfo] hasDecal])
    {
        [self popupSelection];
    }
    else
    {
        [self mainImageButtonPressed: sender];
    }
}

- (IBAction)tagNewDecalPressed:(UIButton *)sender {
    [self initTagDecalToVehicleWithVehicleId:self.textVehicleNumber.text stateName: self.lblState.text];
}

- (IBAction)continuePressed:(UIButton *)sender {
    [mSession pushRegisterOilChangeFor:self.textVehicleNumber.text onVc:self];
}

#pragma mark - TextField Delegatre
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.textVehicleNumber)
    {
        NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
        NSString *trimmedString = [self.textVehicleNumber.text stringByTrimmingCharactersInSet:charSet];
        if ([trimmedString isEqualToString:@""]) {
            //if string only whitespace
            if(!self.lblInviteCustomer.isHidden) {
                self.lblInviteCustomer.hidden = YES;
                self.lblInviteCustomer_height.constant = 0;
                self.headerView_height.constant = self.headerView_height.constant - 40.0;
            }
        } else {
            NSString *stateKeyCode = @"";
            self.isRecommended = NO;
            
            if(IS_COUNTRY(COUNTRYCODE_THAILAND)) {
                stateKeyCode = [mSession convertToStateKeyCode:self.lblState.text stateDetailedListArray: self.stateTable];
            }
            
            [[WebServiceManager sharedInstance] loadSearchVehicleLubeDetails:@{
                                    @"Keyword": textField.text,
                                    @"StateID": stateKeyCode,
                                }
                                vc:self
             ];
        }
    }
}


#pragma mark - Helper Methods
-(void) completeRegister
{
    if(!IS_COUNTRY(COUNTRYCODE_THAILAND)) {
        [[WebServiceManager sharedInstance] searchVehicle:self.textVehicleNumber.text state:@"" vc:self];
    } else {
       NSString *stateKeyCode = [mSession convertToStateKeyCode:self.lblState.text stateDetailedListArray: self.stateTable];
       
       [[WebServiceManager sharedInstance] searchVehicle:self.textVehicleNumber.text state: stateKeyCode vc:self];
    }
}
    
-(void) popupSelection
{
    [self popupCameraDecal];
}

-(void)didSelectDecal
{
    [self tagDecal];
}

-(void) didSelectCarPlate
{
    [self mainImageButtonPressed: nil];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        self.lblState.text = selection.firstObject;
    }
}

//delegate method
-(void) didTagVehicle
{
    NSString *stateKeyCode = [mSession convertToStateKeyCode:self.lblState.text stateDetailedListArray: self.stateTable];
    
//    [[WebServiceManager sharedInstance] searchVehicle: self.textVehicleNumber.text state: stateKeyCode vc:self];
}

//untagged decal code delegate method
-(void)tagDecalWithVehicleId:(NSString *)vehicleId stateName:(NSString *)stateName
{
    self.textVehicleNumber.text = vehicleId;
    self.lblState.text = stateName;
    
    //getState
    NSString *stateKeyCode = [mSession convertToStateKeyCode:stateName stateDetailedListArray: self.stateTable];
    
//    [[WebServiceManager sharedInstance] searchVehicle: self.textVehicleNumber.text state: stateKeyCode vc:self];
}

-(void) showDecal:(BOOL)toShow
{
    if ([[mSession profileInfo] hasDecal] && toShow)
    {
        self.decalView.hidden = NO;
        self.decalView_height.constant = 70;
    }
    else
    {
        self.decalView.hidden = YES;
        self.decalView_height.constant = 0;
    }
}

- (void)tagDecal
{
    [mQRCodeScanner showSingleDecalScanner: self];
}

-(void)qrCodeScanSuccess:(NSString *)scanString
{
    NSString *scannedCodeString;
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDONESIA])
    {
        scannedCodeString = scanString;
    }
    else
    {
        
        scannedCodeString = [[scanString componentsSeparatedByString:@"="] lastObject];
    }
    
    self.scannedDecalCode = scannedCodeString;
//    [[WebServiceManager sharedInstance] searchVehicleWithDecal: self.scannedDecalCode vc: self];
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url absoluteString] isEqualToString: @"Invite"])
        [self inviteConsumer];
}

-(void) inviteConsumer
{
    NSString *stateID = @"";
    if(IS_COUNTRY(COUNTRYCODE_THAILAND)) {
       stateID = [mSession convertToStateKeyCode:self.lblState.text stateDetailedListArray: self.stateTable];
    }
    [mSession pushRegisterConsumer:@{@"VehicleNumber": self.textVehicleNumber.text,
                                     @"VehicleStateID": stateID,
                                     @"PromoCode": @"",
                                     }
                                vc: self]; //no promo code
    
}

- (IBAction)redeemPressed:(UIButton *) sender
{
    [mSession pushPromoCodeView: self];
}

- (IBAction)getLubematchPopup:(UIButton *)sender {
    [mSession pushLubematchPopupWith:self.vehicleLubeMatchArray forVehicleId:self.textVehicleNumber.text onVc:self];
}

-(void) getDetails {
    if(![self.vehicleID isEqualToString:@""]) {
        NSString *stateKeyCode = @"";
        self.isRecommended = YES;
        
        if(IS_COUNTRY(COUNTRYCODE_THAILAND)) {
            stateKeyCode = [mSession convertToStateKeyCode:self.lblState.text stateDetailedListArray: self.stateTable];
        }
        
        [[WebServiceManager sharedInstance] loadSearchVehicleLubeDetails:@{
                                @"Keyword": self.vehicleID,
                                @"StateID": stateKeyCode,
                            }
                            vc:self
         ];
    }
}

#pragma mark - WebServiceManager
-(void)processCompleted:(WebServiceResponse *)response
{
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
            SET_LOADSTATE([[response getGenericResponse] objectForKey: @"StateDetailsList"]);
            self.stateTable = [[response getGenericResponse] objectForKey: @"StateDetailsList"];

            //preload listviews
            listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_THAI_VEHICLE_STATE) list: [mSession getStateNames: self.stateTable] selectionType:ListSelectionTypeSingle previouslySelected: nil];
            [listView setHasSearchField: YES];
            listView.delegate = self;
            
            [self setUpInterface];
            break;
        case kWEBSERVICE_TRADE_LUBEMATCH_SEARCHVEHICLE:
        {
            NSDictionary *searchData = [response getGenericResponse];
            
            if([[searchData objectForKey:@"CanInvite"] boolValue] && self.lblInviteCustomer.isHidden) {
                self.lblInviteCustomer.hidden = NO;
                self.lblInviteCustomer_height.constant = 40;
                self.headerView_height.constant = self.headerView_height.constant + 40.0;
            } else if(![[searchData objectForKey:@"CanInvite"] boolValue] && !self.lblInviteCustomer.isHidden) {
                self.lblInviteCustomer.hidden = YES;
                self.lblInviteCustomer_height.constant = 0;
                self.headerView_height.constant = self.headerView_height.constant - 40.0;
            }
            self.btnContinue.hidden = ![[searchData objectForKey:@"ShowRegProductCodeBtn"] boolValue];
//            self.hasLubematch = [[searchData objectForKey:@"HasLubeMatch"] boolValue];
//            self.hasLubematchGetBtn = [[searchData objectForKey:@"HasGetLubeBtn"] boolValue];
            self.hasHistory = [[searchData objectForKey:@"HasHistory"] boolValue];
            
            [self setEmptyState:NO];
            [self setupData:searchData];
            
            if (![[searchData objectForKey:@"VehicleMake"] isEqualToString: @""] &&
                ![[searchData objectForKey:@"VehicleMake"] isKindOfClass:[NSNull class]] &&
                ![[searchData objectForKey:@"VehicleModel"] isEqualToString: @""] &&
                ![[searchData objectForKey:@"VehicleModel"] isKindOfClass:[NSNull class]] &&
                ![[searchData objectForKey:@"VehicleType"] isEqualToString: @""] &&
                ![[searchData objectForKey:@"VehicleType"] isKindOfClass:[NSNull class]]
                )
            {
                self.hasLubematchGetBtn = NO;
            }
            else
            {
                self.hasLubematchGetBtn = YES;
            }
            
            if(!self.hasLubematchGetBtn)
            {
                NSMutableDictionary *dict;
                dict = [[NSMutableDictionary alloc] initWithDictionary: @{ @"Type" : [searchData objectForKey:@"VehicleType"],
                                                                           @"Make" : [searchData objectForKey:@"VehicleMake"],
                                                                           @"Model" : [searchData objectForKey:@"VehicleModel"],
                }];
                NSString *queryString = @"";
                //add all the 4 details here.
                queryString = [queryString stringByAppendingString: [searchData objectForKey:@"VehicleMake"]];
                
                queryString = [queryString stringByAppendingFormat: @"+%@", [searchData objectForKey:@"VehicleModel"]];
                
                queryString = [queryString stringByAppendingFormat: @"&familygroup=%@", [searchData objectForKey:@"VehicleType"]];
                
                if (![[searchData objectForKey:@"VehicleManufactureyear"] isEqualToString: @""] && ![[searchData objectForKey:@"VehicleManufactureyear"] isKindOfClass:[NSNull class]])
                {
                    NSString *year = [NSString stringWithFormat:@"%@",[searchData objectForKey:@"VehicleManufactureyear"]];
                    queryString = [queryString stringByAppendingFormat: @"&year=%@", [year isEqualToString:@"0"] ? @"" : year];
                    [dict setObject: [searchData objectForKey:@"VehicleManufactureyear"] forKey: @"Year"];
                }
                
                 SET_LUBEMATCH_RECOMMENDED(dict);
                
                [[WebServiceManager sharedInstance] fetchOatsQueryWithString: queryString vc: self];
            }
            else
            {
                SET_LUBEMATCH_RECOMMENDED(@{}); //reset lubematch popup fields from previous success cases
                [self.tableView reloadData];
                [self coachmarkShow];
            }
        }
         break;
        case kOATS_SEARCH:
        {
            NSDictionary *data = [response getJSON];
            NSDictionary *equipmentListData = [data objectForKey: @"equipment_list"];
            NSArray *equipmentArray = [equipmentListData objectForKey: @"equipment"];
            
            if ([equipmentArray count] > 0)
            {
                //have data
                NSDictionary *resultData = [equipmentArray firstObject];
                NSString *hrefString = [resultData objectForKey: @"@href"];
                [[WebServiceManager sharedInstance] fetchOatsEquipmentWithHrefString: hrefString vc: self];
            }
            else
            {
                //no data, activate get lubematch recommendation
                self.hasLubematchGetBtn = YES;
                [self.tableView reloadData];
                [self coachmarkShow];
            }
        }
            break;
        case kOATS_EQUIPMENTHREF:
        {
            NSDictionary *data = [response getJSON];
            NSDictionary *equipmentData = [data objectForKey: @"equipment"];
            NSArray *applicationArray = [equipmentData objectForKey: @"application"];
            //setup product data
            [self setupOatsResults: applicationArray];
            
            [self.tableView reloadData];
            
            [self coachmarkShow];
        }
            break;
        default:
            break;
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress: KEY_WINDOW];
    SET_LUBEMATCH_RECOMMENDED(@{});
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
            self.stateTable = GET_LOADSTATE;
            
            //preload listviews
            listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_THAI_VEHICLE_STATE) list: [mSession getStateNames: self.stateTable] selectionType:ListSelectionTypeSingle previouslySelected: nil];
            [listView setHasSearchField: YES];
            listView.delegate = self;
            
            [self setUpInterface];
            break;
        case kOATS_SEARCH:
            //setup anyways
            [self coachmarkShow];
            break;
        case kOATS_EQUIPMENTHREF:
            //setup anyways
            [self coachmarkShow];
            break;
        default:
            break;
    }
}

#pragma mark - Car Plate Scanning
-(void)imageTaken:(UIImage *)image key:(NSString *)keyString
{
    if (!image)
        return;
    
    UIImage *newImage = [self rotateImage:image];
    
    NSData *imgData = UIImageJPEGRepresentation(newImage, 0.1);
    NSString *base64String = [imgData base64EncodedStringWithOptions: 0];
    
    NSString* secretKey = @"sk_a94baee6f07c7836e8ef292d"; // The secret key used to authenticate your account.  You can view your  secret key by visiting  https://cloud.openalpr.com/
    NSString *country = [self getCountryCode];
    //    NSString* country = @"us"; // Defines the training data used by OpenALPR.  \"us\" analyzes  North-American style plates.  \"eu\" analyzes European-style plates.  This field is required if using the \"plate\" task  You may use multiple datasets by using commas between the country  codes.  For example, 'au,auwide' would analyze using both the  Australian plate styles.  A full list of supported country codes  can be found here https://github.com/openalpr/openalpr/tree/master/runtime_data/config
    //au, auwide, br, br2, eu, fr, gb, in, kr, kr2, mx, sg, us, vn2
    
    if([country length] == 0)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:@"Invalid Country"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {
                                 //BUTTON OK CLICK EVENT
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    NSNumber* recognizeVehicle = @(1); // If set to 1, the vehicle will also be recognized in the image This requires an additional credit per request  (optional) (default to 0)
    NSString* state = @""; // Corresponds to a US state or EU country code used by OpenALPR pattern  recognition.  For example, using \"md\" matches US plates against the  Maryland plate patterns.  Using \"fr\" matches European plates against  the French plate patterns.  (optional) (default to )
    NSNumber* returnImage = @(0); // If set to 1, the image you uploaded will be encoded in base64 and  sent back along with the response  (optional) (default to 0)
    NSNumber* topn = @(5); // The number of results you would like to be returned for plate  candidates and vehicle classifications  (optional) (default to 10)
    NSString* prewarp = @""; // Prewarp configuration is used to calibrate the analyses for the  angle of a particular camera.  More information is available here http://doc.openalpr.com/accuracy_improvements.html#calibration  (optional) (default to )
    
    SWGDefaultApi *apiInstance = [[SWGDefaultApi alloc] init];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [apiInstance recognizeBytesWithImageBytes:base64String
                                    secretKey:secretKey
                                      country:country
                             recognizeVehicle:recognizeVehicle
                                        state:state
                                  returnImage:returnImage
                                         topn:topn
                                      prewarp:prewarp
                            completionHandler: ^(SWGInlineResponse200* output, NSError* error) {
                                if (output)
                                {
                                    if ([output.results count] > 0)
                                    {
                                        SWGPlateDetails *plateDetails = [output.results objectAtIndex: 0];
                                        
                                        self.textVehicleNumber.text = plateDetails.plate; //String of car plate
                                        [self.textVehicleNumber becomeFirstResponder];
                                        SWGVehicleCandidate *plateYear = [plateDetails.vehicle.year firstObject];
                                        NSString *vehicleYearString = [[plateYear.name componentsSeparatedByString: @"-"] firstObject]; //get first year;
                                        
                                        SWGVehicleCandidate *plateMakeModel = [plateDetails.vehicle.makeModel firstObject];
                                        NSString *plateMakeString = [[plateMakeModel.name componentsSeparatedByString: @"_"] firstObject]; //get make;
                                        NSString *plateModelString = [[plateMakeModel.name componentsSeparatedByString: @"_"] lastObject]; //get model;
                                        
                                        NSDictionary *dict = @{ @"Type" : @"Cars",
                                                                @"Make" : plateMakeString,
                                                                @"Model" : plateModelString,
                                                                @"Year" : vehicleYearString,
                                        };
                                        SET_LUBEMATCH_RECOMMENDED(dict);
                                    }
                                    else
                                    {
                                        UIAlertController *alert = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_GLOBAL_ERROR)
                                                                                                       message: LOCALIZATION(C_ERROR_CANNOTRECOGNIZE)
                                                                                                preferredStyle:UIAlertControllerStyleAlert];
                                        
                                        UIAlertAction *ok = [UIAlertAction actionWithTitle: LOCALIZATION(C_BUTTON_OK) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                             {
                                                                 //BUTTON OK CLICK EVENT
                                                             }];
                                        [alert addAction:ok];
                                        [self presentViewController:alert animated:YES completion:nil];
                                        //                                        self.imgView.image = nil;
                                    }
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                }
                                if (error) {
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    
                                    UIAlertController *alert = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_GLOBAL_ERROR)
                                                                                                   message: LOCALIZATION(C_ERROR_UNKNOWNERROR)
                                                                                            preferredStyle:UIAlertControllerStyleAlert];
                                    
                                    UIAlertAction *ok = [UIAlertAction actionWithTitle: LOCALIZATION(C_BUTTON_OK) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                         {
                                                             //BUTTON OK CLICK EVENT
                                                         }];
                                    [alert addAction:ok];
                                    [self presentViewController:alert animated:YES completion:nil];
                                }
                            }];
}

- (IBAction)mainImageButtonPressed:(id)sender
{
    SET_LUBEMATCH_RECOMMENDED(@{});
    [mSession pushPhotoTakingViewControllerWithParent: self];
}

-(NSString *) getCountryCode
{
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
    {
        return @"th";
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDIA])
    {
        return @"in";
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDONESIA])
    {
        return @"id";
    }
    else if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_SAUDI])
    {
        return @"sa";
    }
    else
    {
        return @"eu";
    }
}

- (UIImage*) rotateImage:(UIImage* )originalImage {
    
    UIImageOrientation orientation = originalImage.imageOrientation;
    
    UIGraphicsBeginImageContext(originalImage.size);
    
    [originalImage drawAtPoint:CGPointMake(0, 0)];
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orientation == UIImageOrientationRight) {
        CGContextRotateCTM (context, [self radians:90]);
    } else if (orientation == UIImageOrientationLeft) {
        CGContextRotateCTM (context, [self radians:90]);
    } else if (orientation == UIImageOrientationDown) {
        // NOTHING
    } else if (orientation == UIImageOrientationUp) {
        CGContextRotateCTM (context, [self radians:0]);
    }
    
    return UIGraphicsGetImageFromCurrentImageContext();
    
}

- (CGFloat) radians:(int)degree {
    return (degree/180)*(22/7);
}


- (UIImage *)imageWithImage:(UIImage *)image
{
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    CGSize newSize = image.size;
    
    CGFloat aspectRatio = newSize.width / newSize.height;
    
    if (newSize.width >= newSize.height)
    {
        //landscape or square, set width to 1280
        newSize.width = 1280;
        newSize.height = newSize.width * aspectRatio;
    }
    else
    {
        //portrait
        newSize.height = 1280;
        newSize.width = newSize.height * aspectRatio;
    }
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - Collection View
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
   
    NSInteger actualIndex;
    if (kIsRightToLeft) {
        actualIndex = indexPath.row == 0 ? 1 : 0;
    } else {
        actualIndex = indexPath.row;
    }
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"CategoryCell" forIndexPath:indexPath];
            
    UILabel *lblTitle = [cell viewWithTag: 1];
    if(actualIndex == 0) {
        [lblTitle setText:LOCALIZATION(C_RU_BREAKDOWN_OVERVIEW)];
    } else {
         [lblTitle setText:LOCALIZATION(C_RU_BREAKDOWN_DETAILS)];
    }
    [lblTitle setFont:FONT_H1];
            
    UIView *bottomLine = [cell viewWithTag: 2];
    if ((self.isDetailSelected && actualIndex == 1) || (!self.isDetailSelected && actualIndex == 0))
    {
        lblTitle.textColor = COLOUR_RED;
        [bottomLine setBackgroundColor:COLOUR_RED];
        bottomLine.hidden = NO;
    }
    else
    {
        lblTitle.textColor = COLOUR_VERYDARKGREY;
        [bottomLine setBackgroundColor:COLOUR_RED];
        bottomLine.hidden = YES;
    }
    [self.headerCategoryCellArray addObject:cell];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger actualIndex;
    if (kIsRightToLeft) {
        actualIndex = indexPath.row == 0 ? 1 : 0;
    } else {
        actualIndex = indexPath.row;
    }
    for (UICollectionViewCell *categoryCell in self.headerCategoryCellArray)
    {
        UIView *bottomLine = [categoryCell viewWithTag: 2];
        bottomLine.hidden = YES;
        
        UILabel *lblTitle = [categoryCell viewWithTag: 1];
        lblTitle.textColor = COLOUR_VERYDARKGREY;
        bottomLine.hidden = YES;
    }
    
    UICollectionViewCell *selectedCell = [collectionView cellForItemAtIndexPath: indexPath];
    UILabel *lblTitle = [selectedCell viewWithTag: 1];
    lblTitle.textColor = COLOUR_RED;
    
    UIView *bottomLine = [selectedCell viewWithTag: 2];
    [bottomLine setBackgroundColor:COLOUR_RED];
    bottomLine.hidden = NO;
    
    if(actualIndex == 1) {
        self.isDetailSelected = YES;
    } else {
        self.isDetailSelected = NO;
    }
    
    [self.headerCollectionView reloadData];
    [self.tableView reloadData];
}

#pragma mark - TableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(self.isDetailSelected) {
        if([self.otherRecommendations count] > 0) {
            return 2;
        }
        return 1;
    } else {
        return [self.tableSectionHeadingArray count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.isDetailSelected) {
        switch(section) {
            case 0:
                return 7;
                break;
            case 1:
                return [self.otherRecommendations count];
                break;
            default:
                break;
        }
    } else {
        if([self.tableSectionHeadingArray[section] isEqualToString:@"Redeem"]) {
            return [self.offerArray count] + 1;
        } else if([self.tableSectionHeadingArray[section] isEqualToString:@"Lubematch"] && self.hasLubematchGetBtn) {
            return 1;
        } else if([self.tableSectionHeadingArray[section] isEqualToString:@"ServiceHistory"] && !self.hasHistory) {
            return 1;
        } else if([self.tableSectionHeadingArray[section] isEqualToString:@"Lubematch"] && !self.hasLubematchGetBtn) {
            if(self.isRecommended) {
                return [self.primaryRecommendations count] + 1;
            } else {
                return [self.primaryRecommendations count];
            }
        } else if([self.tableSectionHeadingArray[section] isEqualToString:@"ServiceHistory"] && self.hasHistory) {
            return [self.serviceHistoryArray count];
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    if(self.isDetailSelected) {
        if(indexPath.section == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"InformationDataCell"];
            NSArray *cellDataArray = [self.vehicleInfoArray objectAtIndex:indexPath.row];
            
            UILabel *lblTitle = [cell viewWithTag: 1];
            lblTitle.font = FONT_H2;
            lblTitle.textColor = COLOUR_VERYDARKGREY;
            lblTitle.text = [cellDataArray objectAtIndex:0];
                       
            UILabel *lblData = [cell viewWithTag: 2];
            lblData.font = FONT_B2;
            lblData.textColor = COLOUR_VERYDARKGREY;
            lblData.text = [cellDataArray objectAtIndex:1];
            
            UIView *bottomBorderView = [cell viewWithTag:890];
            UIView *topBorderView = [cell viewWithTag:891];
            if(indexPath.row == 6) {
                bottomBorderView.hidden = NO;
                topBorderView.hidden = YES;
            } else if(indexPath.row == 0) {
                bottomBorderView.hidden = YES;
                topBorderView.hidden = NO;
            } else {
                bottomBorderView.hidden = YES;
                topBorderView.hidden = YES;
            }
        } else {
            cell = [tableView dequeueReusableCellWithIdentifier:@"LubeMatchDetailsCell"];
            NSDictionary *cellDict = [self.otherRecommendations objectAtIndex:indexPath.row];
            
            UILabel *lblTitle = [cell viewWithTag: 1];
            lblTitle.hidden = NO;
            [lblTitle setText:[cellDict objectForKey:@"Title"]];
            [lblTitle setFont:FONT_H1];
            [lblTitle setTextColor:COLOUR_VERYDARKGREY];
            
            NSArray *cellDataArray = [cellDict objectForKey:@"Data"];
            UIView *secondProductView = [cell viewWithTag:3];
            if([cellDataArray count] == 1) {
                secondProductView.hidden = YES;
            } else {
                secondProductView.hidden = NO;
                
                NSDictionary *dict = [cellDataArray objectAtIndex:0];
                UIImageView *imgView = [cell viewWithTag:888];
                [imgView sd_setImageWithURL: [NSURL URLWithString: [dict objectForKey:@"Image"]] placeholderImage: [UIImage imageNamed: @"bottle"] options: (SDWebImageRetryFailed | SDWebImageRefreshCached)];
                
                UIView *typeView = [cell viewWithTag:889];
                UILabel *lblType = [cell viewWithTag:890];
                [lblType setFont:FONT_H2];
                if([[dict objectForKey:@"Type"] isEqualToString:@"Premium"]) {
                    [lblType setText:LOCALIZATION(C_LUBEMATCH_TAG_PREMIUM)];
                    [typeView setBackgroundColor:COLOUR_YELLOW];
                    [lblType setTextColor:COLOUR_VERYDARKGREY];
                } else {
                    [lblType setText:LOCALIZATION(C_LUBEMATCH_TAG_STANDARD)];
                    [typeView setBackgroundColor:COLOUR_VERYDARKGREY];
                    [lblType setTextColor:COLOUR_WHITE];
                }
                
                UILabel *lblProduct = [cell viewWithTag:891];
                [lblProduct setText:[dict objectForKey:@"Product"]];
                [lblProduct setFont:FONT_H2];
                [lblProduct setTextColor:COLOUR_VERYDARKGREY];
            }
            
            UIView *firstProductView = [cell viewWithTag:2];
            firstProductView.hidden = NO;
            
            NSDictionary *dict = [cellDataArray objectAtIndex:0];
            UIImageView *imgView = [cell viewWithTag:999];
            [imgView sd_setImageWithURL: [NSURL URLWithString: [dict objectForKey:@"Image"]] placeholderImage: [UIImage imageNamed: @"bottle"] options: (SDWebImageRetryFailed | SDWebImageRefreshCached)];
            
            UIView *typeView = [cell viewWithTag:1000];
            UILabel *lblType = [cell viewWithTag:1001];
            [lblType setFont:FONT_H2];
            if([[dict objectForKey:@"Type"] isEqualToString:@"Premium"]) {
                [lblType setText:LOCALIZATION(C_LUBEMATCH_TAG_PREMIUM)];
                [typeView setBackgroundColor:COLOUR_YELLOW];
                [lblType setTextColor:COLOUR_VERYDARKGREY];
            } else {
                [lblType setText:LOCALIZATION(C_LUBEMATCH_TAG_STANDARD)];
                [typeView setBackgroundColor:COLOUR_VERYDARKGREY];
                [lblType setTextColor:COLOUR_WHITE];
            }
            
            UILabel *lblProduct = [cell viewWithTag:1002];
            [lblProduct setText:[dict objectForKey:@"Product"]];
            [lblProduct setFont:FONT_H2];
            [lblProduct setTextColor:COLOUR_VERYDARKGREY];
            
            UIView *topBorderView = [cell viewWithTag:3001];
            UIView *bottomBorderView = [cell viewWithTag:3004];
            UIView *inBetweenBorderView = [cell viewWithTag:3005];
            if(indexPath.row == 0 && [self.otherRecommendations count] == 1) {
                inBetweenBorderView.hidden = YES;
                topBorderView.hidden = NO;
                bottomBorderView.hidden = NO;
            } else if(indexPath.row == 0 && [self.otherRecommendations count] > 1) {
                inBetweenBorderView.hidden = NO;
                topBorderView.hidden = NO;
                bottomBorderView.hidden = YES;
            } else if (indexPath.row == ([self.otherRecommendations count] - 1)) {
                inBetweenBorderView.hidden = YES;
                topBorderView.hidden = YES;
                bottomBorderView.hidden = NO;
            } else {
                inBetweenBorderView.hidden = NO;
                topBorderView.hidden = YES;
                bottomBorderView.hidden = YES;
            }
        }
    } else {
        if([self.tableSectionHeadingArray[indexPath.section] isEqualToString:@"Redeem"]) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"EntitledOffersDataCell"];
            
            UILabel *lblPromo = [cell viewWithTag: 1];
            lblPromo.textColor = COLOUR_VERYDARKGREY;
            
            UILabel *lblValidity = [cell viewWithTag: 2];
            lblValidity.textColor = COLOUR_VERYDARKGREY;
            
            if(indexPath.row == 0) {
                lblPromo.text = LOCALIZATION(C_SEARCHVEHICLE_OFFERDETAILS);
                lblPromo.font = FONT_H2;
                lblValidity.text = LOCALIZATION(C_SEARCHVEHICLE_OFFERVALIDITY);
                lblValidity.font = FONT_H2;
            } else {
                NSDictionary *cellData = [self.offerArray objectAtIndex: (indexPath.row - 1)];
                lblPromo.text = [cellData objectForKey: @"PromotionOffer"];
                lblPromo.font = FONT_B2;
                lblValidity.text = [cellData objectForKey: @"Validity"];
                lblValidity.font = FONT_B2;
            }
            
            UIView *bottomBorderView = [cell viewWithTag:890];
            if(indexPath.row == [self.offerArray count]) {
                bottomBorderView.hidden = NO;
            } else {
                bottomBorderView.hidden = YES;
            }
            
        } else if([self.tableSectionHeadingArray[indexPath.section] isEqualToString:@"Lubematch"] && self.hasLubematchGetBtn && !self.hasHistory) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"GetLubeMatchCell"];
            
            UILabel *lblLubematchContent = [cell viewWithTag: 1];
            lblLubematchContent.text = LOCALIZATION(C_LUBEMATCH_NORECOMMENDATION_CONTENT);
            lblLubematchContent.textColor = COLOUR_VERYDARKGREY;
            lblLubematchContent.font = FONT_B2;
            [lblLubematchContent sizeToFit];
            
            UIButton * btnGetLubematch = [cell viewWithTag:2];
            [btnGetLubematch setTitle:LOCALIZATION(C_BTN_GETLUBEMATCH) forState:UIControlStateNormal];
            [btnGetLubematch setBackgroundColor:COLOUR_YELLOW];
            [btnGetLubematch.titleLabel setTextColor:COLOUR_VERYDARKGREY];
            [btnGetLubematch.titleLabel setFont:FONT_H2];
            [btnGetLubematch addTarget: self action:@selector(getLubematchPopup:) forControlEvents:UIControlEventTouchUpInside];
            
        } else if([self.tableSectionHeadingArray[indexPath.section] isEqualToString:@"Lubematch"] && self.hasLubematchGetBtn && self.hasHistory) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"ImageGetLubeMatchCell"];
            
            UIImageView *bottleImage = [cell viewWithTag:999];
            [bottleImage sd_setImageWithURL: [NSURL URLWithString: self.lastUsedProductImage] placeholderImage: [UIImage imageNamed: @"bottle"] options: (SDWebImageRetryFailed | SDWebImageRefreshCached)];
            
            UILabel *lblBottleTitle = [cell viewWithTag:1000];
            lblBottleTitle.text = self.lastUsedProductTitle;
            lblBottleTitle.font = FONT_H2;
            lblBottleTitle.textColor = COLOUR_VERYDARKGREY;
            
            UILabel *lblLubematchContent = [cell viewWithTag: 1];
            lblLubematchContent.text = LOCALIZATION(C_LUBEMATCH_NORECOMMENDATION_CONTENT);
            lblLubematchContent.textColor = COLOUR_VERYDARKGREY;
            lblLubematchContent.font = FONT_B2;
            [lblLubematchContent sizeToFit];
            
            UIButton * btnGetLubematch = [cell viewWithTag:2];
            [btnGetLubematch setTitle:LOCALIZATION(C_BTN_GETLUBEMATCH) forState:UIControlStateNormal];
            [btnGetLubematch setBackgroundColor:COLOUR_YELLOW];
            [btnGetLubematch.titleLabel setTextColor:COLOUR_VERYDARKGREY];
            [btnGetLubematch.titleLabel setFont:FONT_H2];
            [btnGetLubematch addTarget: self action:@selector(getLubematchPopup:) forControlEvents:UIControlEventTouchUpInside];
            
        } else if([self.tableSectionHeadingArray[indexPath.section] isEqualToString:@"Lubematch"] && !self.hasLubematchGetBtn) {
            
            if(indexPath.row == 0 && self.isRecommended) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"RecommendedTableViewCell"];
                
                UILabel *lblDetails = [cell viewWithTag:1];
                [lblDetails setTextColor:COLOUR_VERYDARKGREY];
                NSMutableAttributedString *vehicleDetailsString = [[NSMutableAttributedString alloc] initWithString:self.vehicleDetails attributes: @{                                                                                                                                          NSFontAttributeName: FONT_H2, }];
                
                NSMutableAttributedString *lblAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",LOCALIZATION(C_LUBEMATCH_RECOMMENDEDFOR)] attributes: @{                                                                                                                                             NSFontAttributeName: FONT_B2, }];
                
                [lblAttrString appendAttributedString: vehicleDetailsString];
                lblDetails.attributedText = lblAttrString;
                
                UIButton *btnEdit = [cell viewWithTag:2];
                [btnEdit setAttributedTitle: [[NSAttributedString alloc] initWithString: LOCALIZATION(C_RUSSIA_EDIT_BTN) attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle), }] forState: UIControlStateNormal];
                [btnEdit addTarget: self action:@selector(getLubematchPopup:) forControlEvents:UIControlEventTouchUpInside];
                
            } else {
                cell = [tableView dequeueReusableCellWithIdentifier:@"LubeMatchDetailsCell"];
                NSDictionary *cellDict;
                if(self.isRecommended) {
                    cellDict = [self.primaryRecommendations objectAtIndex:(indexPath.row - 1)];
                } else {
                    cellDict = [self.primaryRecommendations objectAtIndex:indexPath.row];
                }
                
                UILabel *lblTitle = [cell viewWithTag: 1];
                if([[cellDict objectForKey:@"Title"] isEqualToString:@"Engine"]) {
                    lblTitle.hidden = YES;
                    [lblTitle setText:@""];
                    [lblTitle sizeToFit];
                } else {
                    lblTitle.hidden = NO;
                    [lblTitle setText:[cellDict objectForKey:@"Title"]];
                    [lblTitle setFont:FONT_H1];
                    [lblTitle setTextColor:COLOUR_VERYDARKGREY];
                }
                
                NSArray *cellDataArray = [cellDict objectForKey:@"Data"];
                UIView *secondProductView = [cell viewWithTag:3];
                if([cellDataArray count] == 1) {
                    secondProductView.hidden = YES;
                } else {
                    secondProductView.hidden = NO;
                    
                    NSDictionary *dict = [cellDataArray objectAtIndex:0];
                    UIImageView *imgView = [cell viewWithTag:888];
                    [imgView sd_setImageWithURL: [NSURL URLWithString: [dict objectForKey:@"Image"]] placeholderImage: [UIImage imageNamed: @"bottle"] options: (SDWebImageRetryFailed | SDWebImageRefreshCached)];
                    
                    UIView *typeView = [cell viewWithTag:889];
                    UILabel *lblType = [cell viewWithTag:890];
                    [lblType setFont:FONT_H2];
                    if([[dict objectForKey:@"Type"] isEqualToString:@"Premium"]) {
                        [lblType setText:LOCALIZATION(C_LUBEMATCH_TAG_PREMIUM)];
                        [typeView setBackgroundColor:COLOUR_YELLOW];
                        [lblType setTextColor:COLOUR_VERYDARKGREY];
                    } else {
                        [lblType setText:LOCALIZATION(C_LUBEMATCH_TAG_STANDARD)];
                        [typeView setBackgroundColor:COLOUR_VERYDARKGREY];
                        [lblType setTextColor:COLOUR_WHITE];
                    }
                    
                    UILabel *lblProduct = [cell viewWithTag:891];
                    [lblProduct setText:[dict objectForKey:@"Product"]];
                    [lblProduct setFont:FONT_H2];
                    [lblProduct setTextColor:COLOUR_VERYDARKGREY];
                }
                
                UIView *firstProductView = [cell viewWithTag:2];
                firstProductView.hidden = NO;
                
                NSDictionary *dict = [cellDataArray objectAtIndex:0];
                UIImageView *imgView = [cell viewWithTag:999];
                [imgView sd_setImageWithURL: [NSURL URLWithString: [dict objectForKey:@"Image"]] placeholderImage: [UIImage imageNamed: @"bottle"] options: (SDWebImageRetryFailed | SDWebImageRefreshCached)];
                
                UIView *typeView = [cell viewWithTag:1000];
                UILabel *lblType = [cell viewWithTag:1001];
                [lblType setFont:FONT_H2];
                if([[dict objectForKey:@"Type"] isEqualToString:@"Premium"]) {
                    [lblType setText:LOCALIZATION(C_LUBEMATCH_TAG_PREMIUM)];
                    [typeView setBackgroundColor:COLOUR_YELLOW];
                    [lblType setTextColor:COLOUR_VERYDARKGREY];
                } else {
                    [lblType setText:LOCALIZATION(C_LUBEMATCH_TAG_STANDARD)];
                    [typeView setBackgroundColor:COLOUR_VERYDARKGREY];
                    [lblType setTextColor:COLOUR_WHITE];
                }
                
                UILabel *lblProduct = [cell viewWithTag:1002];
                [lblProduct setText:[dict objectForKey:@"Product"]];
                [lblProduct setFont:FONT_H2];
                [lblProduct setTextColor:COLOUR_VERYDARKGREY];
                
                UIView *topBorderView = [cell viewWithTag:3001];
                UIView *bottomBorderView = [cell viewWithTag:3004];
                UIView *inBetweenBorderView = [cell viewWithTag:3005];
                if(indexPath.row == 0 && [self.primaryRecommendations count] == 1) {
                    inBetweenBorderView.hidden = YES;
                    topBorderView.hidden = NO;
                    bottomBorderView.hidden = NO;
                } else if(indexPath.row == 0 && [self.primaryRecommendations count] > 1) {
                    inBetweenBorderView.hidden = NO;
                    topBorderView.hidden = NO;
                    bottomBorderView.hidden = YES;
                } else if (indexPath.row == 1) {
                    inBetweenBorderView.hidden = YES;
                    topBorderView.hidden = YES;
                    bottomBorderView.hidden = NO;
                }
            }
            
        } else if([self.tableSectionHeadingArray[indexPath.section] isEqualToString:@"ServiceHistory"] && !self.hasHistory) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"NoServiceHistoryCell"];
            
            UILabel *lblContent = [cell viewWithTag: 2];
            lblContent.text = LOCALIZATION(C_LUBEMATCH_NOSERVICEHISTORY);
            lblContent.textColor = COLOUR_LIGHTGREY;
            lblContent.font = FONT_B2;
            [lblContent sizeToFit];
            
        } else if ([self.tableSectionHeadingArray[indexPath.section] isEqualToString:@"ServiceHistory"] && self.hasHistory) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"ServicingDataCell"];
            NSDictionary *cellData = [self.serviceHistoryArray objectAtIndex:indexPath.row];
            
            UIView *topBorderView = [cell viewWithTag:891];
            UIView *bottomBorderView = [cell viewWithTag:890];
            if(indexPath.row == 0){
                topBorderView.hidden = NO;
                bottomBorderView.hidden = NO;
            } else{
                topBorderView.hidden = YES;
                bottomBorderView.hidden = NO;
            }
            
            UIImageView *imgForChange = [cell viewWithTag: 1];
            if([[cellData objectForKey:@"CusRedem"] boolValue]) {
                [imgForChange setImage:[UIImage imageNamed:@"icn_redemption_vdarkgrey.png"]];
                [cell.contentView setBackgroundColor:[UIColor colorWithRed:247/COLOUR_MAXBYTE green:247/COLOUR_MAXBYTE blue:247/COLOUR_MAXBYTE alpha: 1.0f]];
            } else {
                [imgForChange setImage:[UIImage imageNamed:@"icn_oilchange_vdarkgrey.png"]];
                [cell.contentView setBackgroundColor:COLOUR_WHITE];
            }
            
            UILabel *lblDate = [cell viewWithTag: 888];
            lblDate.text = [[cellData objectForKey:@"ServiceDate"] isKindOfClass:[NSNull class]] ? @"" : [cellData objectForKey:@"ServiceDate"];
            lblDate.textColor = COLOUR_VERYDARKGREY;
            lblDate.font = FONT_B3;
            
            UILabel *lblDesc = [cell viewWithTag: 889];
            lblDesc.text = [[cellData objectForKey:@"LastUsedProduct"] isKindOfClass:[NSNull class]] ? @"" : [cellData objectForKey:@"LastUsedProduct"];
            lblDesc.textColor = COLOUR_VERYDARKGREY;
            lblDesc.font = FONT_H2;
            
            UILabel *lblServicedBy = [cell viewWithTag: 890];
            lblServicedBy.text = [[cellData objectForKey:@"LastServiceBy"] isKindOfClass:[NSNull class]] ? [NSString stringWithFormat:@"%@ -",LOCALIZATION(C_LUBEMATCH_SERVICEDBY)] : [NSString stringWithFormat:@"%@ %@",LOCALIZATION(C_LUBEMATCH_SERVICEDBY),[cellData objectForKey:@"LastServiceBy"]];
            lblServicedBy.textColor = COLOUR_VERYDARKGREY;
            lblServicedBy.font = FONT_B2;
            
            UILabel *lblMileageData = [cell viewWithTag: 3];
            lblMileageData.text = [[cellData objectForKey:@"LastMileage"] isKindOfClass:[NSNull class]] ? @"" : [cellData objectForKey:@"LastMileage"];
            lblMileageData.textColor = COLOUR_VERYDARKGREY;
            lblMileageData.font = FONT_B2;
            
        }
        else {
            cell = [UITableViewCell new];
        }
    }
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewCell *cell;
    
    if(self.isDetailSelected) {
        if(section == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"RecommendationHeader"];
            
            UIView *topBorderView = [cell viewWithTag:999];
            [topBorderView setBackgroundColor:COLOUR_RED];
            
            UILabel *lblTitle = [cell viewWithTag:1];
            lblTitle.text = LOCALIZATION(C_LUBEMATCH_CUSTOMERINFO);
            lblTitle.textColor = COLOUR_VERYDARKGREY;
            lblTitle.font = FONT_H1;
        } else {
            cell = [tableView dequeueReusableCellWithIdentifier:@"RecommendationHeader"];
            
            UIView *topBorderView = [cell viewWithTag:999];
            [topBorderView setBackgroundColor:COLOUR_RED];
            
            UILabel *lblTitle = [cell viewWithTag:1];
            lblTitle.text = LOCALIZATION(C_LUBEMATCH_RECOMMENDATION_OTHER);
            lblTitle.textColor = COLOUR_VERYDARKGREY;
            lblTitle.font = FONT_H1;
        }
    } else {
        if([self.tableSectionHeadingArray[section] isEqualToString:@"Redeem"]) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"EntitledOffersHeader"];
            
            UIView *topBorderView = [cell viewWithTag:999];
            [topBorderView setBackgroundColor:COLOUR_RED];
            
            UILabel *lblEntitledOffers = [cell viewWithTag:1];
            lblEntitledOffers.text = LOCALIZATION(C_SEARCHVEHICLE_ENTITLEDOFFER);
            lblEntitledOffers.textColor = COLOUR_VERYDARKGREY;
            lblEntitledOffers.font = FONT_H1;
            
            UIButton * btnRedeem = [cell viewWithTag:2];
            [btnRedeem setTitle:LOCALIZATION(C_SEARCHVEHICLE_OFFERREDEEM) forState:UIControlStateNormal];
            [btnRedeem setBackgroundColor:COLOUR_YELLOW];
            [btnRedeem.titleLabel setTextColor:COLOUR_VERYDARKGREY];
            [btnRedeem.titleLabel setFont:FONT_H2];
            if([[mSession profileInfo] hasCustomerRedemptionBtn]){
                [btnRedeem setHidden:NO];
                [btnRedeem addTarget: self action:@selector(redeemPressed:) forControlEvents:UIControlEventTouchUpInside];
            } else {
                [btnRedeem setHidden:YES];
            }
            
        } else if([self.tableSectionHeadingArray[section] isEqualToString:@"Lubematch"]) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"RecommendationHeader"];
            
            UIView *topBorderView = [cell viewWithTag:999];
            [topBorderView setBackgroundColor:COLOUR_RED];
            
            UILabel *lblTitle = [cell viewWithTag:1];
            lblTitle.text = LOCALIZATION(C_LUBEMATCH_RECOMMENDATION_ENGINEOIL);
            lblTitle.textColor = COLOUR_VERYDARKGREY;
            lblTitle.font = FONT_H1;
            
        } else if([self.tableSectionHeadingArray[section] isEqualToString:@"ServiceHistory"]) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryHeaderCell"];
            
            UIView *topBorderView = [cell viewWithTag:999];
            [topBorderView setBackgroundColor:COLOUR_RED];
            
            UILabel *lblServiceTitle = [cell viewWithTag:1];
            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc]initWithString:LOCALIZATION(C_LUBEMATCH_SERVICEHISTORY) attributes:@{
                                                                                                                    NSFontAttributeName : FONT_H1,}];
            [attrString appendAttributedString: [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(C_RUSSIA_DATEFORMAT) attributes:@{
                                                                                                                    NSFontAttributeName : FONT_B(8),}]];
            lblServiceTitle.attributedText = attrString;
            lblServiceTitle.textColor = COLOUR_VERYDARKGREY;
            
            UILabel *lblMileageTitle = [cell viewWithTag:2];
            lblMileageTitle.text = LOCALIZATION(C_LUBEMATCH_MILEAGE);
            lblMileageTitle.textColor = COLOUR_VERYDARKGREY;
            lblMileageTitle.font = FONT_H1;
        }
    }
    
    return cell.contentView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 20.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.isDetailSelected) {
        if(indexPath.section == 0) {
            if(indexPath.row == 4) {
                return UITableViewAutomaticDimension;
            }
             return 34.0;
        }
    }
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 48.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *tableSectionFooterView = [UIView new];
    [tableSectionFooterView setBackgroundColor: [UIColor colorWithRed:247/COLOUR_MAXBYTE green:247/COLOUR_MAXBYTE blue:247/COLOUR_MAXBYTE alpha: 1.0f]];
    return tableSectionFooterView;
}

-(void) setupOatsResults: (NSArray *) applicationArray
{
    for (NSDictionary *applicationDict in applicationArray)
    {
        NSString *appTypeString = [applicationDict objectForKey:@"app_type"];
        {
            NSArray *productArray = [applicationDict objectForKey:@"product"];
            if ([productArray count] == 0)
                break;
            NSMutableArray *recommendationArray = [[NSMutableArray alloc] init];
            for (NSDictionary *productDict in productArray)
            {
                NSDictionary *reccommendedProductDict = [self getIndividualRecommendedDataWithDict: productDict];
                [recommendationArray addObject: reccommendedProductDict];
            }
         
            NSString *recommendationTitleString;
            if ([appTypeString isEqualToString: @"Engine"])
                recommendationTitleString = @"Engine";
            else if ([appTypeString isEqualToString: @"Transmission"])
                recommendationTitleString = LOCALIZATION(C_LUBEMATCH_TRANSMISSION_HEADER);
            else if ([appTypeString isEqualToString: @"Differential"])
                recommendationTitleString = LOCALIZATION(C_LUBEMATCH_REAR_HEADER);
            else if ([appTypeString isEqualToString: @"Coolant"])
                recommendationTitleString = LOCALIZATION(C_LUBEMATCH_COOLANT_HEADER);
            else if ([appTypeString isEqualToString: @"Brake Fluid"])
                recommendationTitleString = LOCALIZATION(C_LUBEMATCH_BRAKEFLUID_HEADER);
            else if ([appTypeString isEqualToString: @"Transfer box"])
                recommendationTitleString = LOCALIZATION(C_LUBEMATCH_TRANSFERBOX_HEADER);
            else
                recommendationTitleString = @"";
            
            //add data to the appropriate array
            if ([appTypeString isEqualToString: @"Engine"] ||
                [appTypeString isEqualToString: @"Transmission"])
            {
                [self.primaryRecommendations addObject:@{
                    @"Title" : recommendationTitleString,
                    @"Data" : recommendationArray,
                }];
            }
            else
            {
                [self.otherRecommendations addObject:@{
                    @"Title" : recommendationTitleString,
                    @"Data" : recommendationArray,
                }];
            }
        }
    }
}

-(NSDictionary *) getIndividualRecommendedDataWithDict:(NSDictionary *) productDict
{
    //tier name - > Type
    //product name -> Product
    //packshot -> Image
    
    NSMutableDictionary *reccommendedProductDict = [[NSMutableDictionary alloc] init];
    [reccommendedProductDict setObject: [productDict objectForKey: @"name"]
                                forKey: @"Product"];
    [reccommendedProductDict setObject: [productDict objectForKey: @"@tier_name"]
                                forKey: @"Type"];
    for (NSDictionary *resourceDict in [productDict objectForKey: @"resource"])
    {
        //resource contains other objects as well, check for packshot resource
        if ([[resourceDict objectForKey: @"@type"] isEqualToString: @"packshot"])
        {
            [reccommendedProductDict setObject: [resourceDict objectForKey: @"@href"]
                                        forKey: @"Image"];
            break;
        }
    }
    return reccommendedProductDict;
}
@end
