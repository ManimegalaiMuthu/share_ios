//
//  RegisterOilChangeLubematchViewController.m
//  Shell
//
//  Created by Nach on 20/1/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "CASYearPicker.h"
#import "RegisterOilChangeLubematchViewController.h"

@interface RegisterOilChangeLubematchViewController () <UITableViewDelegate,UITableViewDataSource, WebServiceManagerDelegate, YearPickerDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@property NSMutableArray *dataArray;
@property NSString *selectedVehicleType;
@property NSString *selectedVehicleMake;
@property NSString *selectedVehicleModel;
@property NSString *selectedVehicleYear;
@property int selectedIndex;

//@property NSMutableArray *selectedVehicleTypeMatches;
//@property NSMutableArray *selectedVehicleMakeMatches;
//@property NSMutableArray *selectedVehicleModelMatches;

@property CASYearPicker *dateView;

@end

@implementation RegisterOilChangeLubematchViewController
@synthesize dateView;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    dateView = [[CASYearPicker alloc] initWithTitle:LOCALIZATION(C_PERFORMANCE_SELECTDATE)];  //lokalised 28 Jan
    
    dateView.delegate = self;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.lblTitle.text = LOCALIZATION(C_POPUP_LUBEMATCH_RECOMMENDATION_TITLE);
    self.lblTitle.textColor = COLOUR_VERYDARKGREY;
    self.lblTitle.font = FONT_H1;
    
    self.lblHeader.text = LOCALIZATION(C_POPUP_LUBEMATCH_RECOMMENDATION_HEADER);
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    self.lblHeader.font = FONT_B1;
    
    [self.btnCancel setTitle:LOCALIZATION(C_FORM_CANCEL) forState:UIControlStateNormal];
    [self.btnCancel.layer setBorderColor:[COLOUR_RED CGColor]];
    [self.btnCancel.layer setBorderWidth:1.0f];
    [self.btnCancel setBackgroundColor:COLOUR_WHITE];
    [self.btnCancel setTitleColor:COLOUR_RED forState:UIControlStateNormal];
    [self.btnCancel.titleLabel setFont:FONT_H1];
    
    [self.btnSubmit setTitle:LOCALIZATION(C_FORM_SUBMIT) forState:UIControlStateNormal];
    [self.btnSubmit setBackgroundColor:COLOUR_RED];
    [self.btnSubmit setTitleColor:COLOUR_WHITE forState:UIControlStateNormal];
    [self.btnSubmit.titleLabel setFont:FONT_H1];
    
    self.tableView.tableFooterView = [UIView new];
//    self.dataArray = [[NSMutableArray alloc] init];
//    [self.dataArray addObjectsFromArray:@[@"",@"",@"",@""]];
    self.selectedVehicleType = @"";
    self.selectedVehicleMake = @"";
    self.selectedVehicleModel = @"";
    self.selectedVehicleYear = @"";
    self.selectedIndex = 999;
    
//    self.selectedVehicleTypeMatches = [[NSMutableArray alloc] init];
//    self.selectedVehicleMakeMatches = [[NSMutableArray alloc] init];
//    self.selectedVehicleModelMatches = [[NSMutableArray alloc] init];
    
     if(GET_LUBEMATCH_RECOMMENDED != nil && [GET_LUBEMATCH_RECOMMENDED allKeys].count != 0){
         NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:GET_LUBEMATCH_RECOMMENDED];
//         [self.dataArray replaceObjectAtIndex:0 withObject:[mSession convertToVehicleType:[[dict objectForKey:@"Type"] integerValue]]];
//         [self.dataArray replaceObjectAtIndex:1 withObject:[dict objectForKey:@"Make"]];
//         [self.dataArray replaceObjectAtIndex:2 withObject:[dict objectForKey:@"Model"]];
//         [self.dataArray replaceObjectAtIndex:3 withObject:[dict objectForKey:@"Year"]];
         
         self.selectedVehicleType = [dict objectForKey:@"Type"];
         self.selectedVehicleMake = [dict objectForKey:@"Make"];
         self.selectedVehicleModel = [dict objectForKey:@"Model"];
         self.selectedVehicleYear = [dict objectForKey:@"Year"];
     }
    
    [self.tableView reloadData];
}


#pragma mark - TableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    switch(indexPath.row) {
        case 0:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"DropdownCell"];
            UILabel *lblTitle = [cell viewWithTag: 999];
            lblTitle.attributedText = [self addAsteriskForRequiredField:LOCALIZATION(C_FORM_VEHICLETYPE)];

            UILabel *lblData = [cell viewWithTag: 1000];
            
            if (![self.selectedVehicleType isEqualToString: @""])
            {
                lblData.text = self.selectedVehicleType;
                [lblData setTextColor:COLOUR_VERYDARKGREY];
            }
            else
            {
                lblData.text = LOCALIZATION(C_POPUP_PLEASESELECT);
                [lblData setTextColor:COLOUR_PALEGREY];
            }
            lblData.font = FONT_B1;
            
            UIButton *btnDropdown = [cell viewWithTag:1001];
            [btnDropdown.superview setTag:indexPath.row];
            
        }
            break;
        case 1:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"TextfieldCell"];
            UILabel *lblTitle = [cell viewWithTag: 999];
            lblTitle.attributedText = [self addAsteriskForRequiredField:LOCALIZATION(C_LUBEMATCH_FORM_VEHICLEMAKE)];
            
            UITextField *txtData = [cell viewWithTag: 1000];
            txtData.placeholder = LOCALIZATION(C_LUBEMATCH_FORM_VEHICLEMAKE);
            txtData.delegate = self;
            [txtData.superview setTag:indexPath.row];
            txtData.font = FONT_B1;
            [txtData setTextColor:COLOUR_VERYDARKGREY];
            
            if (![self.selectedVehicleMake isEqualToString: @""])
                txtData.text = self.selectedVehicleMake;
        }
            break;
        case 2:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"TextfieldCell"];
            UILabel *lblTitle = [cell viewWithTag: 999];
            lblTitle.attributedText = [self addAsteriskForRequiredField:LOCALIZATION(C_LUBEMATCH_FORM_VEHICLEMODEL)];
            
            UITextField *txtData = [cell viewWithTag: 1000];
            txtData.placeholder = LOCALIZATION(C_LUBEMATCH_FORM_VEHICLEMODEL);
            txtData.delegate = self;
            [txtData.superview setTag:indexPath.row];
            txtData.font = FONT_B1;
            [txtData setTextColor:COLOUR_VERYDARKGREY];

            if (![self.selectedVehicleModel isEqualToString: @""])
                txtData.text = self.selectedVehicleModel;
        }
            break;
        case 3:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"DropdownCell"];
            UILabel *lblTitle = [cell viewWithTag: 999];
            lblTitle.text = LOCALIZATION(C_LUBEMATCH_FORM_VEHICLEYEAR);
            lblTitle.font = FONT_B1;
            lblTitle.textColor = COLOUR_VERYDARKGREY;
            
            UILabel *lblData = [cell viewWithTag: 1000];
            if (![self.selectedVehicleYear isEqualToString: @""])
            {
                lblData.text = self.selectedVehicleYear;
                [lblData setTextColor:COLOUR_VERYDARKGREY];
            }
            else
            {
                lblData.text = LOCALIZATION(C_POPUP_PLEASESELECT);
                [lblData setTextColor:COLOUR_PALEGREY];
            }
            
            UIButton *btnDropdown = [cell viewWithTag:1001];
            [btnDropdown.superview setTag:indexPath.row];
            lblData.font = FONT_B1;
        }
            break;
    }
    
    return cell;
}

-(void) textFieldDidBeginEditing:(UITextField *)textField {
    self.selectedIndex = textField.superview.tag;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.superview.tag == 1)
    {
        //Vehicle Make
        self.selectedVehicleMake = textField.text;
    }
    else if (textField.superview.tag == 2)
    {
        //Vehicle Model
        self.selectedVehicleModel = textField.text;
    }
    self.selectedIndex = 999;
}

-(NSAttributedString *) addAsteriskForRequiredField: (NSString *) string
{
    NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @"* " attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B3, }];
    
    NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: string attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
    
    [titleAttrString appendAttributedString: asteriskAttrString];
    
    return titleAttrString;
}

- (IBAction)btnSelected:(UIButton *) sender
{
    switch(sender.superview.tag) {
        case 0: {
            [[WebServiceManager sharedInstance] fetchOatsVehicleType: self];
            self.selectedIndex = 0;
       }
            break;
        case 3: {
            self.selectedIndex = 3;

            UIWindow* window = [[UIApplication sharedApplication] keyWindow];
            [window addSubview: dateView];
            dateView.translatesAutoresizingMaskIntoConstraints = NO;
            [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                           options:0
                                                                           metrics:nil
                                                                             views:NSDictionaryOfVariableBindings (dateView)]];
            [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                           options:0
                                                                           metrics:nil
                                                                             views:NSDictionaryOfVariableBindings (dateView)]];
        }
            break;
        default:
            break;
    }
}

- (IBAction)cancelPressed:(UIButton *)sender {
    [self removeFromParentViewController];
    [self.view removeFromSuperview];
}

- (IBAction)submitPressed:(UIButton *)sender {
    if(self.selectedIndex == 1 || self.selectedIndex == 2) {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedIndex inSection:0]];
        UITextField *textField = [cell viewWithTag:1000];
        [textField resignFirstResponder];
    }
    [[WebServiceManager sharedInstance] submitLubematchRegistration:@{
                                                                        @"VehicleNumber" : self.vehicleID,
                                                                        @"VehicleTypeName"   : self.selectedVehicleType,
                                                                        @"VehicleMake"   : self.selectedVehicleMake,
                                                                        @"VehicleModel"  : self.selectedVehicleModel,
                                                                        @"YearofManufacture" : self.selectedVehicleYear,
                                                                    }
                                                                 vc:self];
}

#pragma mark - Delegate Method
-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        switch (self.selectedIndex)
        {
            case 0:
            {
                self.selectedVehicleType = selection.firstObject;
            }
                break;
            default:
                break;
        }
        [self.tableView reloadData];
        self.selectedIndex = 999;
    }
}

-(void)didSelectYear:(NSDate *)selection
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    
    //setup the necesary date adjustments
    NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear) fromDate: selection];
    self.selectedVehicleYear = [NSString stringWithFormat: @"%li", [selectedComponent year]];
    
    [self.tableView reloadData];

}

#pragma mark - Helper methods

#pragma mark - WebService Manager Delegate
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kOATS_VEHICLETYPES:
        {
            NSDictionary *respData = [response getJSON];
            NSDictionary *indexDict = [respData objectForKey: @"index"];
            NSArray *itemArray = [indexDict objectForKey: @"item"];
            
            //array to hold text data only
            NSMutableArray *textArray = [[NSMutableArray alloc] init];
            for (NSDictionary *itemDict in itemArray)
            {
                [textArray addObject: [itemDict objectForKey: @"#text"]];
            }

            [self popUpListViewWithTitle: LOCALIZATION(C_FORM_VEHICLETYPE) listArray: textArray withSearchField: NO];
        }
            break;
        case kWEBSERVICE_TRADE_LUBEMATCH_SUBMITREGISTRATION : {
            self.parentVC.vehicleID = self.vehicleID;
            self.parentVC.vehicleDetails = [NSString stringWithFormat:@"%@ %@ %@",self.selectedVehicleMake,self.selectedVehicleModel,self.selectedVehicleYear];
//            NSDictionary *dict = @{ @"Type" : self.selectedVehicleType ,
//                                    @"Make" : self.selectedVehicleMake ,
//                                    @"Model" : self.selectedVehicleModel ,
//                                    @"Year" : self.selectedVehicleYear,
//            };
//            SET_LUBEMATCH_RECOMMENDED(dict);
            SET_LUBEMATCH_RECOMMENDED(@{});
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self.parentVC getDetails];
            [self cancelPressed:nil];
        }
            break;
        default:
            break;
    }
}

@end
