//
//  RegisterOilChangeScanViewController.m
//  Shell
//
//  Created by Jeremy Lua on 10/1/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "RegisterOilChangeScanViewController.h"

#import <Photos/Photos.h>
#import "SWGApiClient.h"
#import "SWGConfiguration.h"
// load models
#import "SWGCoordinate.h"
#import "SWGInlineResponse200.h"
#import "SWGInlineResponse200ProcessingTime.h"
#import "SWGInlineResponse400.h"
#import "SWGPlateCandidate.h"
#import "SWGPlateDetails.h"
#import "SWGRegionOfInterest.h"
#import "SWGVehicleCandidate.h"
#import "SWGVehicleDetails.h"
// load API classes for accessing endpoints
#import "SWGDefaultApi.h"
#import "QRCodeScanner.h"
#import "PopupValueEntryViewController.h"

#import <MapKit/MapKit.h>

@interface RegisterOilChangeScanViewController () <UITableViewDelegate,UITableViewDataSource,QRCodeScannerDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, WebServiceManagerDelegate, CLLocationManagerDelegate,PopupValueEntryDelegate>

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITextField *textVehicleNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnVehicleCamera;
@property (weak, nonatomic) IBOutlet UITextField *textMileage;
@property (weak, nonatomic) IBOutlet UILabel *lblKm;
@property (weak, nonatomic) IBOutlet UIButton *btnManual;
@property (weak, nonatomic) IBOutlet UIButton *btnScan;
@property (weak, nonatomic) IBOutlet UILabel *lblNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblScannedCode;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblDelete;
@property (weak, nonatomic) IBOutlet UIImageView *progressbarNumberOneImage;
@property (weak, nonatomic) IBOutlet UIImageView *progressbarNumberTwoImage;
@property (weak, nonatomic) IBOutlet UIView *progressBarView;

@property (weak, nonatomic) IBOutlet UIView *emptyView;
@property (weak, nonatomic) IBOutlet UILabel *lblEmptyContent;

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet UIView *manualCodePopupView;
@property (weak, nonatomic) IBOutlet UITextField *txtCode;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnAddCode;

@property (weak, nonatomic) IBOutlet UIView *offlineScanView;
@property (weak, nonatomic) IBOutlet UILabel *lblOfflineText;
@property (weak, nonatomic) IBOutlet UIButton *btnOfflineYes;
@property (weak, nonatomic) IBOutlet UIButton *btnOfflineNo;


@property BOOL isShareCode;

@property CLLocationManager *locationManager;
@property CGPoint currentLocation;
@property BOOL didFindLocation;

@property NSMutableArray *arrayOfScanCode;
@property NSString *currentCodeString;

@end

@implementation RegisterOilChangeScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_SCANPRODUCTCODE) subtitle: @""];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    
    self.arrayOfScanCode = [[NSMutableArray alloc] init];
    self.isShareCode = NO;
    
    if (kIsRightToLeft) {
        [self.textVehicleNumber setTextAlignment:NSTextAlignmentRight];
        [self.textMileage setTextAlignment:NSTextAlignmentRight];
        [self.lblNumber setTextAlignment:NSTextAlignmentRight];
        [self.lblScannedCode setTextAlignment:NSTextAlignmentRight];
        [self.lblStatus setTextAlignment:NSTextAlignmentRight];
        [self.lblDelete setTextAlignment:NSTextAlignmentRight];
        [self.btnBack setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.textVehicleNumber setTextAlignment:NSTextAlignmentLeft];
        [self.textMileage setTextAlignment:NSTextAlignmentLeft];
        [self.lblNumber setTextAlignment:NSTextAlignmentLeft];
        [self.lblScannedCode setTextAlignment:NSTextAlignmentLeft];
        [self.lblDelete setTextAlignment:NSTextAlignmentLeft];
        [self.lblStatus setTextAlignment:NSTextAlignmentLeft];
        [self.btnBack setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
    
    UIView *spacerView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [self.textVehicleNumber setLeftViewMode:UITextFieldViewModeAlways];
    [self.textVehicleNumber setLeftView:spacerView1];
    UIView *spacerView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [self.textMileage setLeftViewMode:UITextFieldViewModeAlways];
    [self.textMileage setLeftView:spacerView2];
    
    if(GET_ISADVANCE) {
        [self.progressbarNumberOneImage setImage: [UIImage imageNamed:@"progressnum-blue-1"]];
        [self.progressbarNumberTwoImage setImage: [UIImage imageNamed:@"progressnum-blue-2"]];
    } else {
        [self.progressbarNumberOneImage setImage: [UIImage imageNamed:@"progressnum-red-1"]];
        [self.progressbarNumberTwoImage setImage: [UIImage imageNamed:@"progressnum-red-2"]];
    }
    self.progressBarView.backgroundColor = COLOUR_RED;

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self showManualCodeEnterPopup:NO];
    [self.tableView registerNib:[UINib nibWithNibName:@"ScanProductTableViewCell" bundle:nil] forCellReuseIdentifier:@"ScanProductTableViewCell"];
    // Declare the Gesture.
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc]
                                             initWithTarget:self
                                             action:@selector(handleTap:)];
    gesRecognizer.delegate = self;
    
    
    
    [mQRCodeScanner setDelegate: self];
    
    // Add Gesture to your view.
    [self.manualCodePopupView addGestureRecognizer:gesRecognizer];
    
    
    if (GET_SCANNEDCODES)
    {
        self.offlineScanView.hidden = NO;
    }
    
    if(![[mSession profileInfo] hasCurrentMileage]) {
        self.textMileage.text = @"0";
        
        [NSLayoutConstraint constraintWithItem:self.textMileage attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
        [NSLayoutConstraint constraintWithItem:self.lblKm attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
    }
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [self.locationManager requestWhenInUseAuthorization];
    [self initView];
}

-(void)initView
{
    
    [self.btnScan.titleLabel setFont: FONT_BUTTON];
    [self.btnScan setBackgroundColor: COLOUR_YELLOW];
    [self.btnScan setTitle:LOCALIZATION(C_FORM_SCAN) forState:UIControlStateNormal];
    [self.btnScan setTitleColor: COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    
    [self.btnManual.titleLabel setFont: FONT_BUTTON];
    [self.btnManual setBackgroundColor: COLOUR_YELLOW];
    [self.btnManual setTitle:LOCALIZATION(C_FORM_MANUAL) forState:UIControlStateNormal];
    [self.btnManual setTitleColor: COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    
    [self.btnCancel.titleLabel setFont: FONT_BUTTON];
    [self.btnCancel setBackgroundColor: COLOUR_YELLOW];
    [self.btnCancel setTitleColor: COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    
    self.textVehicleNumber.font = FONT_B1;
    self.textVehicleNumber.placeholder = LOCALIZATION(C_SEARCHFIELD_VEHICLEID);
    self.textVehicleNumber.delegate = self; //delegate for GA
    
    self.textMileage.font = FONT_B1;
    self.textMileage.placeholder = LOCALIZATION(C_SEARCHFIELD_MILEAGE);
    
    self.lblKm.font = FONT_H1;
    self.lblKm.textColor = COLOUR_VERYDARKGREY;
    self.lblKm.text = LOCALIZATION(C_RU_MILEAGE_SUFFIX);
    
    self.lblEmptyContent.text = LOCALIZATION(C_LUBEMATCH_SCANEMPTY_CONTENT);
    self.lblEmptyContent.textColor = COLOUR_LIGHTGREY;
    self.lblEmptyContent.font = FONT_B1;
    
    self.txtCode.font = FONT_B1;
    self.txtCode.placeholder = LOCALIZATION(C_FORM_ENTERCODETITLE);
    self.lblTitle.font = FONT_B1;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.lblNumber.font = FONT_B1;
    self.lblNumber.textColor = COLOUR_VERYDARKGREY;
    self.lblNumber.text = LOCALIZATION(C_TABLE_NO);
    
    self.lblScannedCode.font = self.lblNumber.font;
    self.lblScannedCode.textColor = self.lblNumber.textColor;
    self.lblScannedCode.text= LOCALIZATION(C_TABLE_SCANNEDCODE);
    
    self.lblStatus.font = self.lblNumber.font;
    self.lblStatus.textColor = self.lblNumber.textColor;
    self.lblStatus.text = LOCALIZATION(C_STATUS);
    
    self.lblDelete.font = self.lblNumber.font;
    self.lblDelete.textColor = self.lblNumber.textColor;
    self.lblDelete.text = LOCALIZATION(C_DELETE);
    
    self.lblTitle.font = FONT_H1;
    self.lblTitle.text = LOCALIZATION(C_FORM_ENTERCODETITLE);
    self.txtCode.font = FONT_B1;
    self.btnAddCode.titleLabel.font = FONT_BUTTON;
    [self.btnAddCode setTitle: LOCALIZATION(C_FORM_ADDCODE) forState:UIControlStateNormal];
    [self.btnAddCode setBackgroundColor:COLOUR_RED];
    
    self.btnCancel.titleLabel.font = FONT_BUTTON;
    [self.btnCancel setTitle:LOCALIZATION(C_FORM_CANCEL) forState:UIControlStateNormal];
    self.btnSubmit.titleLabel.font = FONT_BUTTON;
    [self.btnSubmit setTitle:LOCALIZATION(C_SCANPRODUCTCODE_SUBMIT) forState:UIControlStateNormal];
    [self.btnSubmit setBackgroundColor:COLOUR_RED];
    
    if(GET_PROFILETYPE == kPROFILETYPE_DSR)
    {
        self.btnSubmit.hidden = YES;
    }
    
    self.lblOfflineText.font = FONT_B2;
    self.lblOfflineText.text = LOCALIZATION(C_FORM_OFFLINESCAN);
    
    self.btnOfflineYes.titleLabel.font = FONT_BUTTON;
    [self.btnOfflineYes setTitle:LOCALIZATION(C_FORM_YES) forState:UIControlStateNormal];
    self.btnOfflineNo.titleLabel.font = FONT_BUTTON;
    [self.btnOfflineNo setBackgroundColor: COLOUR_YELLOW];
    [self.btnOfflineNo setTitle:LOCALIZATION(C_FORM_NO) forState:UIControlStateNormal];
    [self.btnOfflineYes setBackgroundColor:COLOUR_RED];
    
    switch ([CLLocationManager authorizationStatus])
    {
        case kCLAuthorizationStatusNotDetermined:
            [self.locationManager requestWhenInUseAuthorization];
            break;
        default:
            break;
    }
    
    self.txtCode.delegate = self;
    if(![self.vehicleID isEqualToString:@""]) {
        self.textVehicleNumber.text = self.vehicleID;
    }
    
    [self setEmptyState:YES];
}

-(void) setEmptyState:(BOOL)emptyState {
    if(emptyState) {
        [self.btnSubmit setHidden:YES];
        [self.emptyView setHidden:NO];
        
        [self.containerView setHidden:YES];
    } else {
        [self.btnSubmit setHidden:NO];
        [self.emptyView setHidden:YES];
        
        [self.containerView setHidden:NO];
    }
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

- (IBAction)scanPressed:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        LoginInfo *loginInfo = [mSession loadUserProfile];
        [FIRAnalytics logEventWithName: @"OilChange_ProdCode_QRScan"
                            parameters: @{@"TradeID": loginInfo.tradeID,}];
    });
    
    if([self.textVehicleNumber isFirstResponder])
       [self.textVehicleNumber resignFirstResponder];
    if([self.textMileage isFirstResponder])
       [self.textMileage resignFirstResponder];
     [mQRCodeScanner showQRCodeScannerOnVC:self withMessage:LOCALIZATION(C_REGISTEROILCHANGE_SUBTITLE)];
}

- (IBAction)manualPressed:(id)sender {
//    [self showManualCodeEnterPopup:YES];
    LoginInfo *loginInfo = [mSession loadUserProfile];
    [FIRAnalytics logEventWithName: @"OilChange_ProdCode_Manual"
                        parameters: @{@"TradeID": loginInfo.tradeID,}];
    [self popupValueEntryVCWithTitle:LOCALIZATION(C_FORM_ENTERCODETITLE) submitTitle:LOCALIZATION(C_FORM_ADDCODE) cancelTitle:LOCALIZATION(C_FORM_CANCEL) placeHolder:LOCALIZATION(C_FORM_ENTERCODETITLE)];
}

- (IBAction)submitPressed:(id)sender {
    
    if ([self.arrayOfScanCode count] == 0)
    {
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_ERROR)];
        return;
    }
    
    NSMutableDictionary *submitCodeDict = [[NSMutableDictionary alloc] init];
    

    [submitCodeDict setObject:@[@{  @"VehicleNumber": self.textVehicleNumber.text,
                                    @"Mileage": self.textMileage.text,
                                    @"Coupons": self.arrayOfScanCode,
                                    @"StateID": @"",
                                },
                                ]
                           forKey:@"VehicleList"];

 
    if ([mSession getReachability])
    {
        [[WebServiceManager sharedInstance] submitCode: submitCodeDict
                                                    vc:self];
    }
    else
    {
        //save
        NSMutableArray *pendingDict = [[NSMutableArray alloc] initWithArray: GET_SCANNEDCODES copyItems:YES];
        
        if (!pendingDict)
            pendingDict = [[NSMutableArray alloc] init];
        
        for (NSMutableDictionary *dict in self.arrayOfScanCode)
        {
            [dict setObject: self.textVehicleNumber.text forKey: @"VehicleNumber"];
        }
        
        [pendingDict addObject: @{ @"VehicleNumber": self.textVehicleNumber.text,
                                   @"Mileage": self.textMileage.text,
                                   @"Coupons": self.arrayOfScanCode,
                                   @"StateID": @"",
                                   @"Date": [self getTodayDate],
                                   }];
        
        SET_SCANNEDCODES(pendingDict);
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.arrayOfScanCode removeAllObjects];
        self.textVehicleNumber.text = @"";
        self.textMileage.text = @"";
        [self setEmptyState:YES];
        
        [self.tableView reloadData];
        
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_ALERT_SAVEDCODE)];
        
    }
    
}

- (IBAction)cancelPressed:(id)sender {
    [self showManualCodeEnterPopup:NO];
    [self clearTextField];
    
}

- (IBAction)addCodePressed:(id)sender {
    
    if(![[mSession profileInfo]hasMultipleSubmissionOilChange]) {
        for (NSDictionary *dict in self.arrayOfScanCode)
        {
            if ([self.txtCode.text isEqualToString: [dict objectForKey: @"Code"]])
            {
                [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_EXIST)];
                return;
            }
        }
    }
    
    [self showManualCodeEnterPopup:NO];
    
    if(self.txtCode.text.length > 0)
    
    {
        self.currentCodeString = self.txtCode.text;
        [self updatelocation];
    }
    [self clearTextField];
}

- (IBAction)offlineNoPressed:(id)sender {
    self.offlineScanView.hidden = YES;
}

- (IBAction)offlineYesPressed:(id)sender {
    self.offlineScanView.hidden = YES;
    //push to pending code view
    [mSession performSelector: @selector(pushPendingCodeView:) withObject: self afterDelay: 0];
}


#pragma mark -Helper Methods
-(void)showManualCodeEnterPopup:(Boolean)value
{
    if (value)
    {
        LoginInfo *loginInfo = [mSession loadUserProfile];
        
        [FIRAnalytics logEventWithName: @"OilChange_ProdCode_Manual"
                            parameters: @{@"TradeID": loginInfo.tradeID,}];
        
        [self.txtCode becomeFirstResponder];
    }
    self.manualCodePopupView.hidden = !value;

}


- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer{
    [self showManualCodeEnterPopup:NO];
    [self clearTextField];
    
}

-(void)clearTextField
{
    self.txtCode.text = @"";
    if([self.txtCode isFirstResponder])
        [self.txtCode resignFirstResponder];
}

-(NSString *)getTodayDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    return [formatter stringFromDate: [NSDate date]];
}

-(void)qrCodeScanSuccess:(NSString *)scanString
{
    NSString *scannedCodeString;
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDONESIA])
    {
        scannedCodeString = scanString;
    }
    else
    {
        
         scannedCodeString = [[scanString componentsSeparatedByString:@"="] lastObject];
    }

    if(![[mSession profileInfo]hasMultipleSubmissionOilChange]) {
        for (NSDictionary *dict in _arrayOfScanCode)
        {
            if ([scannedCodeString isEqualToString: [dict objectForKey: @"Code"]])
            {
                [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_EXIST)];
                return;
            }
        }
    }
    
    
    if(scanString.length > 0){
        self.currentCodeString = scannedCodeString;
        [self updatelocation];
    }
}


#pragma mark - Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.arrayOfScanCode count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ScanProductTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ScanProductTableViewCell"];
    NSDictionary *codeStringDict = [self.arrayOfScanCode objectAtIndex:indexPath.row];
    
    [cell setID: @(indexPath.row + 1).stringValue];
    [cell setScannedCode: [codeStringDict objectForKey: @"Code"]];
    [cell setPendingStatus: LOCALIZATION(C_SCANCODE_STATUS_PENDING)];
    
    cell.btnDelete.tag = indexPath.row;
    cell.btnDelete.backgroundColor = COLOUR_RED;
    [cell.btnDelete setTitle: LOCALIZATION(C_DELETE) forState: UIControlStateNormal];
    [cell.btnDelete addTarget: self action: @selector(deletePressed:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(void)deletePressed:(UIButton*)sender
{
    [self.arrayOfScanCode removeObjectAtIndex:sender.tag];
    [self.tableView reloadData];
}

#pragma mark - TextField
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.textVehicleNumber)
    {
        
        LoginInfo *loginInfo = [mSession loadUserProfile];
        
        [FIRAnalytics logEventWithName: @"OilChange_VehPlate_Manual"
                            parameters: @{@"TradeID": loginInfo.tradeID,}];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if((IS_COUNTRY(COUNTRYCODE_SAUDI) || IS_COUNTRY(COUNTRYCODE_INDONESIA)) && textField != self.textVehicleNumber) {
        NSCharacterSet *blockCharacters = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789\n"];
        
        if([string rangeOfCharacterFromSet:blockCharacters].location == NSNotFound) {
            if ([string isEqualToString: @""]) //check for backspace first, proceed if yes
                return YES;
            else if (![string isEqualToString: @" "])
            {
                [mAlert showErrorAlertWithMessage: LOCALIZATION(C_REGISTEROILCHANGE_INPUTERROR)];
                [textField resignFirstResponder];
            }
            return NO;
        } else {
            return YES;
        }
    } else {
        return YES;
    }
}

#pragma mark - PopupValueEntryDelegate
-(void) valueSubmitted:(NSString *)value {
   if(![[mSession profileInfo]hasMultipleSubmissionOilChange]) {
       for (NSDictionary *dict in self.arrayOfScanCode)
       {
           if ([value isEqualToString: [dict objectForKey: @"Code"]])
           {
               [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_EXIST)];
               return;
           }
       }
   }
   
   if(value > 0)
   {
       self.currentCodeString = value;
       [self updatelocation];
   }
}

#pragma mark - Location Manager
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    if (self.didFindLocation)
        return;
    
    //most recent location update is at the end of the array.
    CLLocation *location = [locations lastObject];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:6];
    
    self.currentLocation = CGPointMake(location.coordinate.latitude, location.coordinate.longitude);
    
    self.didFindLocation = YES;
    [self.locationManager stopUpdatingLocation];
    
    if(![[mSession profileInfo]hasMultipleSubmissionOilChange])
    {
        //ipod touch getting duplicate value. Manually check
        for(NSDictionary *duplicate in self.arrayOfScanCode)
        {
            if([[duplicate objectForKey:@"Code"] isEqualToString:self.currentCodeString]){
                [UpdateHUD removeMBProgress: self];
                return;
            }
        }
    }
    
    NSMutableDictionary *codeDict = [NSMutableDictionary dictionaryWithDictionary:
                                     @{@"Code"          :self.currentCodeString,
                                       @"VehicleNumber" : self.textVehicleNumber.text,
                                       @"Mileage"       : self.textMileage.text,
                                       @"Date"          : [self getTodayDate],
                                       @"Latitude"      : @(self.currentLocation.x),
                                       @"Longitude"     : @(self.currentLocation.y),
                                       }];
    
    [self.arrayOfScanCode addObject: codeDict];
    self.currentLocation = CGPointMake(0, 0);
    
    [self setEmptyState:NO];
    [_tableView reloadData];
    
    [UpdateHUD removeMBProgress: self];
}

-(void) updatelocation {
    [UpdateHUD addMBProgress: self.view withText: @""];
    self.didFindLocation = NO;
    switch ([CLLocationManager authorizationStatus])
    {
        case kCLAuthorizationStatusAuthorizedAlways:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.locationManager startUpdatingLocation];
            break;
        default:
        {
            //send 0,0
            self.currentLocation = CGPointMake(0, 0);
            
            
            NSMutableDictionary *codeDict = [NSMutableDictionary dictionaryWithDictionary:
                                             @{@"Code"          :self.currentCodeString,
                                               @"VehicleNumber" : self.textVehicleNumber.text,
                                               @"Mileage"       : self.textMileage.text,
                                               @"Date"          : [self getTodayDate],
                                               @"Latitude"      : @(self.currentLocation.x),
                                               @"Longitude"     : @(self.currentLocation.y),
                                               }];
            
            [self.arrayOfScanCode addObject: codeDict];
            [self setEmptyState:NO];
            [UpdateHUD removeMBProgress: self];
            [_tableView reloadData];
        }
            break;
    }
    
}

#pragma mark - Webservice
-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_SUBMITCODE:
        {
            NSMutableDictionary *respData = [[NSMutableDictionary alloc] initWithDictionary:[response getGenericResponse]];
            
            [respData setObject: self.textVehicleNumber.text forKey:@"VehicleId"];
            
            [mSession pushRegisterCodeResult: respData vc:self];
         
            self.textMileage.text = @"";
            [self.arrayOfScanCode removeAllObjects];
            [self.tableView reloadData];

        }
            break;
        default:
            break;
    }
}

#pragma mark - Car Plate Scanning
- (IBAction)cameraPressed:(id)sender
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        LoginInfo *loginInfo = [mSession loadUserProfile];
        [FIRAnalytics logEventWithName: @"OilChange_VehPlate_Scan"
                            parameters: @{@"TradeID": loginInfo.tradeID,}];
    });
    
    [mSession pushPhotoTakingViewControllerWithParent: self];
}

-(void)imageTaken:(UIImage *)image key:(NSString *)keyString
{
    if (!image)
        return;
    
    UIImage *newImage = [self rotateImage:image];
    
    NSData *imgData = UIImageJPEGRepresentation(newImage, 0.1);
    NSString *base64String = [imgData base64EncodedStringWithOptions: 0];
    
    NSString* secretKey = @"sk_a94baee6f07c7836e8ef292d"; // The secret key used to authenticate your account.  You can view your  secret key by visiting  https://cloud.openalpr.com/
    NSString *country = [self getCountryCode];
    //    NSString* country = @"us"; // Defines the training data used by OpenALPR.  \"us\" analyzes  North-American style plates.  \"eu\" analyzes European-style plates.  This field is required if using the \"plate\" task  You may use multiple datasets by using commas between the country  codes.  For example, 'au,auwide' would analyze using both the  Australian plate styles.  A full list of supported country codes  can be found here https://github.com/openalpr/openalpr/tree/master/runtime_data/config
    //au, auwide, br, br2, eu, fr, gb, in, kr, kr2, mx, sg, us, vn2
    
    if([country length] == 0)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:@"Invalid Country"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {
                                 //BUTTON OK CLICK EVENT
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    NSNumber* recognizeVehicle = @(0); // If set to 1, the vehicle will also be recognized in the image This requires an additional credit per request  (optional) (default to 0)
    NSString* state = @""; // Corresponds to a US state or EU country code used by OpenALPR pattern  recognition.  For example, using \"md\" matches US plates against the  Maryland plate patterns.  Using \"fr\" matches European plates against  the French plate patterns.  (optional) (default to )
    NSNumber* returnImage = @(0); // If set to 1, the image you uploaded will be encoded in base64 and  sent back along with the response  (optional) (default to 0)
    NSNumber* topn = @(5); // The number of results you would like to be returned for plate  candidates and vehicle classifications  (optional) (default to 10)
    NSString* prewarp = @""; // Prewarp configuration is used to calibrate the analyses for the  angle of a particular camera.  More information is available here http://doc.openalpr.com/accuracy_improvements.html#calibration  (optional) (default to )
    
    SWGDefaultApi *apiInstance = [[SWGDefaultApi alloc] init];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [apiInstance recognizeBytesWithImageBytes:base64String
                                    secretKey:secretKey
                                      country:country
                             recognizeVehicle:recognizeVehicle
                                        state:state
                                  returnImage:returnImage
                                         topn:topn
                                      prewarp:prewarp
                            completionHandler: ^(SWGInlineResponse200* output, NSError* error) {
                                if (output)
                                {
                                    if ([output.results count] > 0)
                                    {
                                        SWGPlateDetails *plateDetails = [output.results objectAtIndex: 0];
                                        
                                        self.textVehicleNumber.text = plateDetails.plate; //String of car plate
                                        [self.textVehicleNumber becomeFirstResponder];
                                    }
                                    else
                                    {
                                        UIAlertController *alert = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_GLOBAL_ERROR)
                                                                                                       message: LOCALIZATION(C_ERROR_CANNOTRECOGNIZE)
                                                                                                preferredStyle:UIAlertControllerStyleAlert];
                                        
                                        UIAlertAction *ok = [UIAlertAction actionWithTitle: LOCALIZATION(C_BUTTON_OK) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                             {
                                                                 //BUTTON OK CLICK EVENT
                                                             }];
                                        [alert addAction:ok];
                                        [self presentViewController:alert animated:YES completion:nil];
                                        //                                        self.imgView.image = nil;
                                    }
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                }
                                if (error) {
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    
                                    UIAlertController *alert = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_GLOBAL_ERROR)
                                                                                                   message: LOCALIZATION(C_ERROR_UNKNOWNERROR)
                                                                                            preferredStyle:UIAlertControllerStyleAlert];
                                    
                                    UIAlertAction *ok = [UIAlertAction actionWithTitle: LOCALIZATION(C_BUTTON_OK) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                         {
                                                             //BUTTON OK CLICK EVENT
                                                         }];
                                    [alert addAction:ok];
                                    [self presentViewController:alert animated:YES completion:nil];
                                }
                            }];
}


-(NSString *) getCountryCode
{
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
    {
        return @"th";
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDIA])
    {
        return @"in";
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDONESIA])
    {
        return @"id";
    }
    else if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_SAUDI])
    {
        return @"sa";
    }
    else
    {
        return @"eu";
    }
}

- (UIImage*) rotateImage:(UIImage* )originalImage {
    
    UIImageOrientation orientation = originalImage.imageOrientation;
    
    UIGraphicsBeginImageContext(originalImage.size);
    
    [originalImage drawAtPoint:CGPointMake(0, 0)];
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orientation == UIImageOrientationRight) {
        CGContextRotateCTM (context, [self radians:90]);
    } else if (orientation == UIImageOrientationLeft) {
        CGContextRotateCTM (context, [self radians:90]);
    } else if (orientation == UIImageOrientationDown) {
        // NOTHING
    } else if (orientation == UIImageOrientationUp) {
        CGContextRotateCTM (context, [self radians:0]);
    }
    
    return UIGraphicsGetImageFromCurrentImageContext();
    
}

- (CGFloat) radians:(int)degree {
    return (degree/180)*(22/7);
}

- (UIImage *)imageWithImage:(UIImage *)image
{
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    CGSize newSize = image.size;
    
    CGFloat aspectRatio = newSize.width / newSize.height;
    
    if (newSize.width >= newSize.height)
    {
        //landscape or square, set width to 1280
        newSize.width = 1280;
        newSize.height = newSize.width * aspectRatio;
    }
    else
    {
        //portrait
        newSize.height = 1280;
        newSize.width = newSize.height * aspectRatio;
    }
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
