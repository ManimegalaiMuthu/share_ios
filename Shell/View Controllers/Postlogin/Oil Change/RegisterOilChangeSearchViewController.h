//
//  RegisterOilChangeSearchViewController.h
//  Shell
//
//  Created by Jeremy Lua on 10/1/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface RegisterOilChangeSearchViewController : BaseVC

@property NSString *vehicleID;
@property NSString *vehicleDetails;
-(void) getDetails;

@end

NS_ASSUME_NONNULL_END
