//
//  ProductsTabViewController.m
//  Shell
//
//  Created by Edenred on 27/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "ProductsTabViewController.h"
#import "ProductsTableViewController.h"
#import "RDVTabBarItem.h"
#import "LocalizationManager.h"
#import "UpdateHUD.h"

@interface ProductsTabViewController () {
    ProductsTableViewController *shareProductTableViewController;
    ProductsTableViewController *allProductsTableViewController;
}
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@end

@implementation ProductsTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupInterface];
}

-(void) setupInterface {
    
     [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_PARTICIPATINGPRODUCT) subtitle: @"" size:15 subtitleSize:0]; //lokalised 11 Feb
    
    if ([self.navigationController.childViewControllers count] > 1)
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    
    shareProductTableViewController = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_PRODUCTTABLEVIEW];
    UINavigationController *shareProductListNav = [[UINavigationController alloc] initWithRootViewController: shareProductTableViewController];
    [shareProductListNav setNavigationBarHidden:YES];
    shareProductTableViewController.fetchShareProducts = YES;
    [shareProductTableViewController view];
    
    allProductsTableViewController = [STORYBOARD_MAIN instantiateViewControllerWithIdentifier: VIEW_PRODUCTTABLEVIEW];
    UINavigationController *allProductListNav = [[UINavigationController alloc] initWithRootViewController: allProductsTableViewController];
    [allProductListNav setNavigationBarHidden:YES];
    allProductsTableViewController.fetchShareProducts = NO;
    [allProductsTableViewController view];
    
    
    NSArray *viewControllers = @[shareProductListNav,
                        allProductListNav
                        ];
    NSArray *title = @[LOCALIZATION(C_TITLE_SHAREPRODUCT),  //lokalised 11 Feb
              LOCALIZATION(C_TITLE_ALLPRODUCT)     //lokalised 11 Feb
              ];
    [self setViewControllers: viewControllers];
    
    UIImage *selected = GET_ISADVANCE ? [UIImage imageNamed:@"tab-bg_blue"] : [UIImage imageNamed:@"tab-bg"];
    UIImage *unselected = [UIImage imageNamed:@"tab-bg_grey.png"];
    if (kIsRightToLeft) {
        title = title.reverseObjectEnumerator.allObjects.mutableCopy;
    }
    for (int i = 0; i < [self.tabBar.items count]; i++)
    {
        RDVTabBarItem *item = [self.tabBar.items objectAtIndex: i];
        [item setTitle: [title objectAtIndex: i]];
        
        [item setBackgroundColor: COLOUR_WHITE];
        [item setTitleUnselectedAttributes:FONT_H1 colour:COLOUR_VERYDARKGREY];
        [item setTitleSelectedAttributes:FONT_H1 colour:COLOUR_RED];
        
        [item setBackgroundSelectedImage: selected withUnselectedImage: unselected];
        
    }
    if (kIsRightToLeft) {
        [self tabBar:self.tabBar didSelectItemAtIndex:self.viewControllers.count-1];
    }
}

-(void)tabBar:(RDVTabBar *)tabBar didSelectItemAtIndex:(NSInteger)index
{
    [super tabBar:tabBar didSelectItemAtIndex: index];
    
}
- (IBAction)backPressed:(UIButton *)sender {
    [UpdateHUD removeSpotifyHUD:CURRENT_VIEW];
    
    CATransition *animation = [CATransition animation];
    animation.type = kCATransitionFade;
    animation.duration = 0.4;
    [self.navigationController.view.layer addAnimation:animation forKey:nil];
    
#warning test for popping the only controller
    //    if ([[vc.navigationController childViewControllers] count] > 1)
    [self.navigationController popViewControllerAnimated:NO];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
