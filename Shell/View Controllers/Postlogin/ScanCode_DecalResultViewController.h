//
//  ScanCode_DecalResultViewController.h
//  Shell
//
//  Created by Jeremy Lua on 25/5/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface ScanCode_DecalResultViewController : BaseVC
@property NSDictionary *resultsDict;

@end
