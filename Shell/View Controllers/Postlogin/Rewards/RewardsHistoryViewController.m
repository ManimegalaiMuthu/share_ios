//
//  RewardsHistoryViewController.m
//  Shell
//
//  Created by Jeremy Lua on 5/7/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RewardsHistoryViewController.h"
#import "PopupInfoTableViewController.h"
#import "MyOrdersTableViewCell.h"
#import "UIBarButtonItem+Badge.h"

#define CELL_HEIGHT 80
#define DATERANGEVIEWHEIGHT 60

enum kRedemptionHistory_Type
{
    kRedemptionHistory_DateRange = 1,
    kRedemptionHistory_AllStatus,
};

enum kRedemptionHistoryStatus_Type
{
    kRedemptionHistoryStatus_PENDING = 1 << 0,
    kRedemptionHistoryStatus_PROCESSING = 1 << 1,
    kRedemptionHistoryStatus_DELIVERED = 1 << 2,
//    kRedemptionHistoryStatus_MEMBERCANCELLED = 1 << 3,
//    kRedemptionHistoryStatus_ = 1 << 4,
//    kRedemptionHistoryStatus_ADMINCANCELLED = 1 << 5,
//    kRedemptionHistoryStatus_CANCELLED = 1 << 6,
    kRedemptionHistoryStatus_CANCELLED = 104,
    kRedemptionHistoryStatus_CANCELLED_INDIA = 108,
    kRedemptionHistoryStatus_DELIVERINGTODISTRIBUTOR = 128,
    
};


@interface RewardsHistoryViewController () <UITableViewDelegate, UITableViewDataSource, WebServiceManagerDelegate, CommonListDelegate, NativeDatePickerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblCurrentPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentPointsData;
@property (weak, nonatomic) IBOutlet UILabel *lblPointsSuffix;

@property (weak, nonatomic) IBOutlet UIButton *btnCart;
@property (weak, nonatomic) IBOutlet UIButton *btnFavourites;
@property (weak, nonatomic) IBOutlet UILabel *lblFavourites;

@property (weak, nonatomic) IBOutlet UIButton *btnHistory;
@property (weak, nonatomic) IBOutlet UILabel *lblHistory;


@property (strong, nonatomic) IBOutlet UIButton *backBtn;


@property (weak, nonatomic) IBOutlet UITableView *rewardsHistoryTableView;
@property (weak, nonatomic) IBOutlet UIButton *btnDateRange;
@property (weak, nonatomic) IBOutlet UIButton *btnAllStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnStartDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;

@property (weak, nonatomic) IBOutlet UIButton *btnEndDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEnter;
@property (weak, nonatomic) IBOutlet UIView *dateRangeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateRangeViewHeight;

//@property NSMutableArray *myOrdersArray;

@property NSMutableArray *redemptionHistoryArray;
@end

@implementation RewardsHistoryViewController
@synthesize listView, nativeDatePickerView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_REWARDS_HISTORY) subtitle: @""];  //lokalise 31 jan
    
    if(@available(iOS 11, *)){
        [self.btnCart.widthAnchor constraintEqualToConstant: 25.0].active = YES;
        [self.btnCart.heightAnchor constraintEqualToConstant: 25.0].active = YES;
    }
    UIBarButtonItem *cartIcon = [[UIBarButtonItem alloc] initWithCustomView: self.btnCart];
    self.navigationItem.rightBarButtonItem = cartIcon;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    
    
    self.dateRangeViewHeight.constant = 0;
    self.dateRangeView.hidden = YES;
    self.rewardsHistoryTableView.tableFooterView = [UITableView new];
    
    
    self.btnAllStatus.titleLabel.font = FONT_B2;
    self.btnDateRange.titleLabel.font = FONT_B2;
    
    self.btnStartDate.titleLabel.font = FONT_B2;
    [self.btnStartDate setTitle: LOCALIZATION(C_ORDER_STARTDATE) forState: UIControlStateNormal];  //lokalise 31 jan
    
    [self.lblTo setFont: FONT_B2];
    self.lblTo.text =  LOCALIZATION(C_POINTS_TO);  //lokalise 31 jan
    
    self.btnEndDate.titleLabel.font = FONT_B2;
    [self.btnEndDate setTitle: LOCALIZATION(C_ORDER_ENDDATE) forState: UIControlStateNormal];  //lokalise 31 jan
    
    self.btnEnter.titleLabel.font = FONT_B2;
    [self.btnEnter setTitle: LOCALIZATION(C_ORDER_ENTER) forState: UIControlStateNormal];  //lokalise 31 jan
    [self.btnEnter setBackgroundColor:COLOUR_RED];
    
    [self.btnDateRange setTitle: [[mSession getRedeemDate] firstObject] forState:UIControlStateNormal];
    
    [self.btnAllStatus setTitle:[[mSession getRedeemStatus] firstObject] forState:UIControlStateNormal];
    
    if (kIsRightToLeft) {
        [self.btnAllStatus setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.btnAllStatus setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 15)];
        [self.btnDateRange setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.btnDateRange setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 15)];
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.btnAllStatus setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.btnAllStatus setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];
        [self.btnDateRange setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.btnDateRange setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics  setScreenName:LOCALIZATION_EN(C_REWARDS_HISTORY) screenClass:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
        if (GET_REWARDSCART)
        {
            NSArray *savedArray = GET_REWARDSCART;
            self.navigationItem.rightBarButtonItem.badgeValue = @(savedArray.count).stringValue;
            self.navigationItem.rightBarButtonItem.badgeOriginX = 15; //default value as of RewardsView
        }
    
//    [[WebServiceManager sharedInstance] fetchRewardsRedemptionHistoryWithDict:@{@"DateSearch" : [mSession convertToRedeemDateKeyCode: self.btnDateRange.titleLabel.text],
//                                                                                @"Status" : [mSession convertToRedeemStatusKeyCode: self.btnAllStatus.titleLabel.text],
//                                                                                @"StartDate" : @"",
//                                                                                @"EndDate": @""} vc: self];
//
    
    [[WebServiceManager sharedInstance] fetchPointsSummary: self];

    [self.btnStartDate setTitle: LOCALIZATION(C_ORDER_STARTDATE) forState: UIControlStateNormal];  //lokalise 31 jan
    
    [self.btnEndDate setTitle: LOCALIZATION(C_ORDER_ENDDATE) forState: UIControlStateNormal];  //lokalise 31 jan
    
    self.lblCurrentPoints.text = LOCALIZATION(C_REWARDS_CURRENTPOINTS);  //lokalise 31 jan
    self.lblCurrentPoints.font = FONT_B3;
    
    self.lblCurrentPointsData.font = FONT_H(25);
    self.lblCurrentPointsData.textColor = COLOUR_RED;
    
    
    self.lblPointsSuffix.text = LOCALIZATION(C_REWARDS_POINTS);  //lokalise 31 jan
    self.lblPointsSuffix.font = self.lblCurrentPoints.font;
    self.lblPointsSuffix.textColor = self.lblCurrentPointsData.textColor;
    
    //    self.lblCart.text = LOCALIZATION(C_REWARDS_CARTBTN);
    //    self.lblCart.font = FONT_B3;
    
    self.lblFavourites.text = LOCALIZATION(C_REWARDS_FAVOURITESBTN);  //lokalise 31 jan
    self.lblFavourites.font = FONT_B3;
    
    self.lblHistory.text = LOCALIZATION(C_REWARDS_HISTORY);  //lokalise 31 jan
    self.lblHistory.font = FONT_B3;
    self.lblHistory.textColor = COLOUR_RED;
    
    UIImage *historyImage = GET_ISADVANCE ? [UIImage imageNamed:@"icn_history_blue"] : [UIImage imageNamed:@"icn_history_red"];
    [self.btnHistory setImage:historyImage forState: UIControlStateNormal];
    
    self.lblCurrentPointsData.text = [NSString stringWithFormat: @"%.1f", [mSession rewardsCurrentPoints]];
}
- (IBAction)backPressed:(id)sender {
    
    [self popToIndex: 0];
    
}

- (IBAction)favouritesPressed:(UIButton *)sender {
    //push to fav view
    [mSession pushRewardsFavourites: self];
}

- (IBAction)cartPressed:(UIButton *)sender {
    //push to cart view
    [mSession pushRewardsCart: self];
}

- (IBAction)dateRangeBtnPressed:(id)sender {
    listView = [[CASCommonListView alloc]initWithTitle:LOCALIZATION(C_ORDER_DATERANGE) list:[mSession getRedeemDate] selectionType:ListSelectionTypeSingle previouslySelected:nil];   //lokalise 31 jan
    listView.tag = kRedemptionHistory_DateRange;
    listView.delegate = self;
    
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}


- (IBAction)showLegend:(id)sender {
    [self.view endEditing: YES];
    
    PopupInfoTableViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPINFOTABLEVIEW];
    
    [self popUpViewWithPopupVC:popupVc];
    
    [popupVc initRedemptionHistoryLegendsPopup];
}

- (IBAction)allStatusBtnPressed:(id)sender {
    listView = [[CASCommonListView alloc]initWithTitle:LOCALIZATION(C_STATUS) list:[mSession getRedeemStatus] selectionType:ListSelectionTypeSingle previouslySelected:nil];   //lokalise 31 jan
    listView.tag = kRedemptionHistory_AllStatus;
    listView.delegate = self;
    
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

- (IBAction)startDateBtnPressed:(id)sender {
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];
    
    //date cannot be greater than end date
    NSDate *endMaxDate = [outputFormatter dateFromString: self.btnEndDate.titleLabel.text];
    
    if (endMaxDate){
        self.nativeDatePickerView = [[NativeDatePicker alloc]initWithDatepickerTitle:LOCALIZATION(C_POINTS_STARTDATE) maximumDate:endMaxDate minimumDate:nil inputDateFormatter:outputFormatter outputDateFormatter:outputFormatter];   //lokalise 31 jan
        self.nativeDatePickerView.delegate = self;
    }
    else{
        self.nativeDatePickerView = [[NativeDatePicker alloc]initWithDatepickerTitle:LOCALIZATION(C_POINTS_STARTDATE) maximumDate:[NSDate date] minimumDate:nil inputDateFormatter:outputFormatter outputDateFormatter:outputFormatter];   //lokalise 31 jan
        self.nativeDatePickerView.delegate = self;
    }
    self.nativeDatePickerView.tag = 1;
    [self showDatePicker];
}

- (IBAction)endDateBtnPressed:(id)sender {
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];
    
    //date cannot be lesser than start date
    NSDate *startMinDate = [outputFormatter dateFromString: self.btnStartDate.titleLabel.text];
    if (startMinDate){
        self.nativeDatePickerView = [[NativeDatePicker alloc]initWithDatepickerTitle:LOCALIZATION(C_POINTS_ENDDATE) maximumDate:[NSDate date] minimumDate:startMinDate inputDateFormatter:outputFormatter outputDateFormatter:outputFormatter];   //lokalise 31 jan
        self.nativeDatePickerView.delegate = self;
        
    }else{
        self.nativeDatePickerView = [[NativeDatePicker alloc]initWithDatepickerTitle:LOCALIZATION(C_POINTS_ENDDATE) maximumDate:[NSDate date] minimumDate:nil inputDateFormatter:outputFormatter outputDateFormatter:outputFormatter];   //lokalise 31 jan
        self.nativeDatePickerView.delegate = self;
    }
    self.nativeDatePickerView.tag = 2;
    [self showDatePicker];
}

-(void)showDatePicker{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: nativeDatePickerView];
    nativeDatePickerView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[nativeDatePickerView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (nativeDatePickerView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[nativeDatePickerView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (nativeDatePickerView)]];
}

-(void)getDateAsString:(NSString *)dateString
{
    switch (nativeDatePickerView.tag) {
        case 1:{
            [self.btnStartDate setTitle:dateString forState:UIControlStateNormal];
        }
            break;
        case 2:{
            [self.btnEndDate setTitle:dateString forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
}

- (IBAction)enterBtnPressed:(id)sender {
    NSString *redeemStatusKeycode = [mSession convertToRedeemStatusKeyCode:self.btnAllStatus.titleLabel.text];
    NSString *redeemDateKeycode = [mSession convertToRedeemDateKeyCode:self.btnDateRange.titleLabel.text];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];
    
    NSDate *startDate = [outputFormatter dateFromString:self.btnStartDate.titleLabel.text];
    NSDate *endDate = [outputFormatter dateFromString:self.btnEndDate.titleLabel.text];
    
    
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat:@"dd MMM yyyy"];
    [inputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    [inputFormatter setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierGregorian]];
    
    NSString *startD = (startDate == nil)?@"":[inputFormatter stringFromDate: startDate];
    NSString *endD = (endDate == nil)?@"":[inputFormatter stringFromDate: endDate];
    
    //    NSString *startD = (startDate == nil)?@"":self.btnStartDate.titleLabel.text;
    //    NSString *endD = (endDate == nil)?@"":self.btnEndDate.titleLabel.text;
    
    [self getSubmitDict:redeemStatusKeycode redeemDate:redeemDateKeycode startDate:startD endDate:endD];
}

-(void)dropDownSelection:(NSArray *)selection
{
    switch (listView.tag) {
        case kRedemptionHistory_DateRange:{
            if (selection.count > 0) {
                [self.btnDateRange setTitle:selection.firstObject forState:UIControlStateNormal];
                
                NSString *redeemStatusKeycode = [mSession convertToRedeemStatusKeyCode:self.btnAllStatus.titleLabel.text];
                NSString *redeemDateKeycode = [mSession convertToRedeemDateKeyCode:selection.firstObject];
                
                if([redeemDateKeycode isEqualToString:@"2"]){
                    self.dateRangeViewHeight.constant = DATERANGEVIEWHEIGHT;
                    self.dateRangeView.hidden = NO;
                }else{
                    self.dateRangeViewHeight.constant = 0;
                    self.dateRangeView.hidden = YES;
                    [self getSubmitDict:redeemStatusKeycode redeemDate:redeemDateKeycode startDate:@"" endDate:@""];
                }
            }
        }
            break;
        case kRedemptionHistory_AllStatus:{
            if (selection.count > 0) {
                [self.btnAllStatus setTitle:selection.firstObject forState:UIControlStateNormal];
                NSString *redeemStatusKeycode = [mSession convertToRedeemStatusKeyCode:selection.firstObject];
                NSString *redeemDateKeycode = [mSession convertToRedeemDateKeyCode:self.btnDateRange.titleLabel.text];
                [self getSubmitDict:redeemStatusKeycode redeemDate:redeemDateKeycode startDate:@"" endDate:@""];
                
            }
        }
            break;
        default:
            break;
    }
    
}


-(void)getSubmitDict :(NSString *)redeemStatus redeemDate:(NSString *)redeemDate startDate:(NSString *)startDate endDate:(NSString *)endDate
{
    NSDictionary *submitDict = @{@"Status" : redeemStatus,
                                 @"DateSearch"   : redeemDate,
                                 @"StartDate"   : startDate,
                                 @"EndDate"     : endDate
                                 };
    [[WebServiceManager sharedInstance] fetchRewardsRedemptionHistoryWithDict:submitDict vc:self];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.redemptionHistoryArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cellData = [self.redemptionHistoryArray objectAtIndex: indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RedemptionHistoryCell"];
    
    UIImageView *imgStatusIcon = [cell viewWithTag: 1];
    
    NSInteger statusType = [[cellData objectForKey:@"Status"] integerValue];
    switch (statusType) {
        case kRedemptionHistoryStatus_PENDING:
            [imgStatusIcon setImage: [UIImage imageNamed: @"icn_pending_small_on"]];
            break;
        case kRedemptionHistoryStatus_PROCESSING:
            [imgStatusIcon setImage: [UIImage imageNamed: @"icn_processing_small_on"]];
            break;
        case kRedemptionHistoryStatus_DELIVERED:
            [imgStatusIcon setImage: [UIImage imageNamed: @"icn_delivered"]];
            break;
        case kRedemptionHistoryStatus_DELIVERINGTODISTRIBUTOR:
            [imgStatusIcon setImage: [UIImage imageNamed: @"icn_ongoing_on"]];
            break;
        case kRedemptionHistoryStatus_CANCELLED_INDIA:
            [imgStatusIcon setImage: [UIImage imageNamed: @"icn_cancelled"]];
            break;
//        case kRedemptionHistoryStatus_MEMBERCANCELLED:
//            [imgStatusIcon setImage: [UIImage imageNamed: @"icn_cancelled"]];
//            break;
//        case kRedemptionHistoryStatus_ADMINCANCELLED:
//            [imgStatusIcon setImage: [UIImage imageNamed: @"icn_cancelled"]];
//            break;
        case kRedemptionHistoryStatus_CANCELLED:
            [imgStatusIcon setImage: [UIImage imageNamed: @"icn_cancelled"]];
            break;
        default:
            [imgStatusIcon setImage: nil];
            break;
    }
    
    UILabel *lblRedemptionId = [cell viewWithTag: 2];
    lblRedemptionId.text = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_REWARDS_REFID), [cellData objectForKey:@"RedemptionRefId"]];   //lokalise 31 jan
    lblRedemptionId.font = FONT_H1;
    
    UILabel *lblPlacedOnDate = [cell viewWithTag: 3];
    lblPlacedOnDate.text = [NSString stringWithFormat: @"%@%@", LOCALIZATION(C_REWARDS_PLACEDON), [cellData objectForKey:@"RedemptionDate"]];   //lokalise 31 jan
    lblPlacedOnDate.font = FONT_B1;
    
    UILabel *lblItemRedeemed = [cell viewWithTag: 4];
    lblItemRedeemed.text = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_REWARDS_ITEMSREDEEMED), [cellData objectForKey:@"TotalItems"]];   //lokalise 31 jan
    lblItemRedeemed.font = FONT_B1;
    
    UIImageView *imgArrow = [cell viewWithTag:5];
    if (kIsRightToLeft) {
        imgArrow.image = [UIImage imageNamed:@"icon-arrsingleL.png"];
    } else {
        imgArrow.image = [UIImage imageNamed:@"icon-arrsingleR.png"];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *redemptionData = [self.redemptionHistoryArray objectAtIndex: indexPath.row];
    
    NSString *redemptionRefId = [redemptionData objectForKey: @"RedemptionId"];

    [[WebServiceManager sharedInstance] fetchRewardsRedemptionDetailsWithId: redemptionRefId vc: self];
}

- (void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_REWARDSREDEMPTIONHISTORY:
        {
            if (![[response getGenericResponse] isEqual: [NSNull null]])
                self.redemptionHistoryArray = [[response getGenericResponse] objectForKey:@"RedemptionHistoryData"];
            else
                self.redemptionHistoryArray = [[NSMutableArray alloc] init];
            
            [self.rewardsHistoryTableView reloadData];
//            if([[response getGenericResponse] isEqual:[NSNull null]] || [[[response getGenericResponse] objectForKey:@"AllOrder"] isEqual:[NSNull null]])
//                return;
//
//            [self setupInterface:[[response getGenericResponse] objectForKey:@"AllOrder"]];
        }
            break;

        case kWEBSERVICE_REWARDSPOINTSSUMMARY:
            [mSession setRewardsCurrentPoints: [[[response getGenericResponse] objectForKey: @"BalancePoints"] doubleValue]];
            self.lblCurrentPointsData.text = [NSString stringWithFormat: @"%.1f", [mSession rewardsCurrentPoints]];
            [[WebServiceManager sharedInstance] fetchRewardsRedemptionHistoryWithDict:@{@"DateSearch" : @"",
                                                                                        @"Status" : @"",
                                                                                        @"StartDate" : @"",
                                                                                        @"EndDate": @""} vc: self];
            break;
        case kWEBSERVICE_REWARDSREDEMPTIONDETAILS:
        {
            NSArray *redemptionData = [[response getGenericResponse] objectForKey: @"RedemptionHistoryData"];
            [mSession pushRewardsRedemptionDetailsWithDetails: redemptionData.firstObject vc:self];
        }
            break;
        default:
            break;
    }
}

- (void)processFailed:(WebServiceResponse *)response
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
