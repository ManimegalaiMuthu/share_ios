//
//  RewardsRedemptionDetailsViewController.m
//  Shell
//
//  Created by Jeremy Lua on 6/7/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RewardsRedemptionDetailsViewController.h"

@interface RewardsRedemptionDetailsViewController () <WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource>

enum kRewardRedemptionSections_TYPE
{
    kRewardRedemptionSections_DETAILS = 0,
    kRewardRedemptionSections_SUMMARY,
    kRewardRedemptionSections_CANCELLED,
    kRewardRedemptionSections_CONTACT,
};

enum kRewardRedemptionDetails_TYPE
{
    kRewardRedemptionDetails_REFID = 1,
    kRewardRedemptionDetails_CONTENT,
};
enum kRewardRedemptionSummary_TYPE
{
    kRewardRedemptionSummary_IMAGEVIEW = 1,
    kRewardRedemptionSummary_TITLE,
    kRewardRedemptionSummary_POINTS,
    kRewardRedemptionSummary_QTY,
};

enum kRewardRedemptionCancelled_TYPE
{
    kRewardRedemptionCancelled_IMAGEVIEW = 1,
    kRewardRedemptionCancelled_TITLE,
    kRewardRedemptionCancelled_REASON,
    kRewardRedemptionCancelled_CONTENT,
};

enum kRewardRedemptionContact_TYPE
{
    kRewardRedemptionContact_HEADER = 1,
    kRewardRedemptionContact_CONTENT,
    kRewardRedemptionContact_BUTTON,
};

typedef enum {
    kRewardRedemptionStatus_PENDING = 1 << 0,
    kRewardRedemptionStatus_PROCESSING = 1 << 1,
    kRewardRedemptionStatus_DELIVERED = 1 << 2,
//    kRewardRedemptionStatus_MEMBERCANCELLED = 1 << 3,
//    kRewardRedemptionStatus_ = 1 << 4,
//    kRewardRedemptionStatus_ADMINCANCELLED = 1 << 5,
//    kRewardRedemptionStatus_CANCELLED = 1 << 6,
    kRewardRedemptionStatus_CANCELLED = 104,
    kRedemptionHistoryStatus_CANCELLED_INDIA = 108,
    kRedemptionHistoryStatus_DELIVERINGTODISTRIBUTOR = 128,
} kRewardRedemptionStatus_TYPE;

@property (weak, nonatomic) IBOutlet UIImageView *imgPendingIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblPending;

@property (weak, nonatomic) IBOutlet UIImageView *imgProcessingIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblProcessing;

@property (weak, nonatomic) IBOutlet UIImageView *imgDeliveredIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblDelivered;

@property (weak, nonatomic) IBOutlet UIImageView *firstArrow;
@property (weak, nonatomic) IBOutlet UIImageView *secondArrow;

@property (weak, nonatomic) IBOutlet UITableView *redemptionDetailsTableView;

@property kRewardRedemptionStatus_TYPE statusType;
@property NSMutableArray *summaryArray;
@property NSMutableArray *cancelledArray;

@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@end

@implementation RewardsRedemptionDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_REWARDS_REDEMPTIONDETAILS_TITLE) subtitle: @""];    //lokalise 31 jan
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    
    self.statusType = [[self.redemptionDetailsDict objectForKey: @"Status"] intValue];
    
    self.summaryArray = [[NSMutableArray alloc] init];
    self.cancelledArray = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in [self.redemptionDetailsDict objectForKey: @"items"])
    {
        if ([[dict objectForKey: @"ItemStatus"] intValue] == kRewardRedemptionStatus_CANCELLED) //cancelled
        {
            [self.cancelledArray addObject: dict];
        }
        else
            [self.summaryArray addObject: dict];
    }
//    self.summaryArray = [self.redemptionDetailsDict objectForKey: @"items"];
    
    [self.imgPendingIcon setImage: [UIImage imageNamed: @"icn_pending_small_off"]];
    self.lblPending.text = LOCALIZATION(C_REWARDS_PENDING);    //lokalise 31 jan
    [self.imgProcessingIcon setImage: [UIImage imageNamed: @"icn_processing_small_off"]];
    self.lblProcessing.text = LOCALIZATION(C_REWARDS_PROCESSING);    //lokalise 31 jan
    [self.imgDeliveredIcon setImage: [UIImage imageNamed: @"icn_delivered_small_off"]];
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
        self.lblDelivered.text = LOCALIZATION(C_REWARDS_DELIVERTODIST);  //lokalise 31 jan
    else
        self.lblDelivered.text = LOCALIZATION(C_REWARDS_DELIVERED);    //lokalise 31 jan
    
    switch (self.statusType)
    {
        case kRewardRedemptionStatus_PENDING:
            [self.imgPendingIcon setImage: [UIImage imageNamed: @"icn_pending_small_on"]];
            break;
        case kRewardRedemptionStatus_PROCESSING:
            [self.imgPendingIcon setImage: [UIImage imageNamed: @"icn_pending_small_on"]];
            [self.imgProcessingIcon setImage: [UIImage imageNamed: @"icn_processing_small_on"]];
            break;
        case kRewardRedemptionStatus_DELIVERED:
        if (IS_COUNTRY(COUNTRYCODE_INDIA))
        {
            [self.imgPendingIcon setImage: [UIImage imageNamed: @"icn_processing_small_off"]];
            self.lblPending.text = LOCALIZATION(C_REWARDS_PROCESSING);  //lokalise 31 jan
            [self.imgProcessingIcon setImage: [UIImage imageNamed: @"icn_ongoing_on"]];
            self.lblProcessing.text = LOCALIZATION(C_REWARDS_DELIVERTODIST);   //lokalise 31 jan
            [self.imgDeliveredIcon setImage: [UIImage imageNamed: @"icn_delivered"]];
            self.lblDelivered.text = LOCALIZATION(C_REWARDS_ITEMRECEIVED);  //lokalise 31 jan
        }
        else
        {
            [self.imgPendingIcon setImage: [UIImage imageNamed: @"icn_pending_small_on"]];
            [self.imgProcessingIcon setImage: [UIImage imageNamed: @"icn_processing_small_on"]];
            [self.imgDeliveredIcon setImage: [UIImage imageNamed: @"icn_delivered_small_on"]];
        }
            break;
        case kRewardRedemptionStatus_CANCELLED:
            [self.imgPendingIcon setImage: [UIImage imageNamed: @"icn_pending_small_on"]];
            [self.imgProcessingIcon setImage: [UIImage imageNamed: @"icn_processing_small_on"]];
            [self.imgDeliveredIcon setImage: [UIImage imageNamed: @"icn_cancelled"]];
            self.lblDelivered.text = LOCALIZATION(C_REWARDS_CANCELLED); //lokalise 31 jan
            break;
        case kRedemptionHistoryStatus_CANCELLED_INDIA:
            [self.imgPendingIcon setImage: [UIImage imageNamed: @"icn_pending_small_on"]];
            [self.imgProcessingIcon setImage: [UIImage imageNamed: @"icn_processing_small_on"]];
            [self.imgDeliveredIcon setImage: [UIImage imageNamed: @"icn_cancelled"]];
            self.lblDelivered.text = LOCALIZATION(C_REWARDS_CANCELLED); //lokalise 31 jan
            break;
        case kRedemptionHistoryStatus_DELIVERINGTODISTRIBUTOR:
            //only INDIA
            [self.imgPendingIcon setImage: [UIImage imageNamed: @"icn_processing_small_on"]];
            self.lblPending.text = LOCALIZATION(C_REWARDS_PROCESSING);           //lokalise 31 jan
            [self.imgProcessingIcon setImage: [UIImage imageNamed: @"icn_ongoing_on"]];
            self.lblProcessing.text = LOCALIZATION(C_REWARDS_DELIVERTODIST);        //lokalise 31 jan
            [self.imgDeliveredIcon setImage: [UIImage imageNamed: @"icn_delivered"]];
            self.lblDelivered.text = LOCALIZATION(C_REWARDS_ITEMRECEIVED);      //lokalise 31 jan
            break;
//        case kRewardRedemptionStatus_ADMINCANCELLED:
//            [self.imgPendingIcon setImage: [UIImage imageNamed: @"icn_pending_small_on"]];
//            [self.imgProcessingIcon setImage: [UIImage imageNamed: @"icn_processing_small_on"]];
//            [self.imgDeliveredIcon setImage: [UIImage imageNamed: @"icn_cancelled"]];
//            self.lblDelivered.text = LOCALIZATION(C_REWARDS_CANCELLED);
//            break;
//        case kRewardRedemptionStatus_MEMBERCANCELLED:
//            [self.imgPendingIcon setImage: [UIImage imageNamed: @"icn_pending_small_on"]];
//            [self.imgProcessingIcon setImage: [UIImage imageNamed: @"icn_processing_small_on"]];
//            [self.imgDeliveredIcon setImage: [UIImage imageNamed: @"icn_cancelled"]];
//            self.lblDelivered.text = LOCALIZATION(C_REWARDS_CANCELLED);
//            break;
        default:
            break;
    }
    self.redemptionDetailsTableView.backgroundColor = COLOUR_WHITE;
    self.redemptionDetailsTableView.backgroundView = nil;

    if (kIsRightToLeft) {
        self.firstArrow.image = [UIImage imageNamed:@"icon-arrsingleL.png"];
        self.secondArrow.image = [UIImage imageNamed:@"icon-arrsingleL.png"];
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        self.firstArrow.image = [UIImage imageNamed:@"icon-arrsingleR.png"];
        self.secondArrow.image = [UIImage imageNamed:@"icon-arrsingleR.png"];
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_REWARDS_REDEMPTIONDETAILS_TITLE) screenClass:nil];
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return kRewardRedemptionSections_CONTACT + 1; //last section index + 1
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    switch (section)
    {
//        case kRewardRedemptionSections_DETAILS:
//            break;
        case kRewardRedemptionSections_SUMMARY:
            return [self.summaryArray count];
            break;
        case kRewardRedemptionSections_CANCELLED:
            return [self.cancelledArray count];
            break;
//        case kRewardRedemptionSections_CONTACT:
//            break;
        default:
            return 1;
            break;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell;
    
    switch (section)
    {
//        case kRewardRedemptionSections_DETAILS:
//            break;
        case kRewardRedemptionSections_SUMMARY:
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"SummaryHeaderCell"];
            
            UILabel *lblHeader = [cell viewWithTag: 1];
            lblHeader.text = LOCALIZATION(C_REWARDS_REDEMPTIONSUMMARY);     //lokalise 31 jan
            lblHeader.font = FONT_H1;
        }
            break;
        case kRewardRedemptionSections_CANCELLED:
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"CancelledHeaderCell"];
            
            UILabel *lblHeader = [cell viewWithTag: 1];
            lblHeader.text = LOCALIZATION(C_REWARDS_CANCELLED);     //lokalise 31 jan
            
            lblHeader.font = FONT_H1;
        }
            break;
//        case kRewardRedemptionSections_CONTACT:
//            break;
        default:
            return [[UIView alloc] initWithFrame: CGRectZero];
            break;
    }
    return cell.contentView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch (section)
    {
            //        case kRewardRedemptionSections_DETAILS:
            //            break;
        case kRewardRedemptionSections_SUMMARY:
            if ([self.summaryArray count] > 0)
                return 50;
            else
                return 1;
            break;
        case kRewardRedemptionSections_CANCELLED:
            if ([self.cancelledArray count] > 0)
                return 50;
            else
                return 1;
            break;
            //        case kRewardRedemptionSections_CONTACT:
            //            break;
        default:
            return 1;
            break;
    }
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    switch (indexPath.section)
    {
        case kRewardRedemptionSections_DETAILS:
        {
            cell = [tableView dequeueReusableCellWithIdentifier: 
                    @"DetailsCell"];
            
            UILabel *lblRefId = [cell viewWithTag: kRewardRedemptionDetails_REFID];
            
            lblRefId.text = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_REWARDS_REFID), [self.redemptionDetailsDict objectForKey: @"RedemptionRefId"]];      //lokalise 31 jan
            lblRefId.font = FONT_H1;
            
            NSString *content = @"";
            
            if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_RUSSIA]) {
                content = [NSString stringWithFormat: @"\n\n%@%@", LOCALIZATION(C_REWARDS_PLACEDON), [self.redemptionDetailsDict objectForKey: @"RedemptionDate"]];     //lokalise 31 jan
                
            } else {
                
                if (!IS_COUNTRY(COUNTRYCODE_INDIA))
                {
                    NSString *formattedAddress = [[[NSAttributedString alloc] initWithData:[[self.redemptionDetailsDict objectForKey: @"Address"] dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil] string];
                    
                    content = [NSString stringWithFormat: @"%@\n%@", LOCALIZATION(C_REWARDS_DELIVERYADDRESS), formattedAddress];     //lokalise 31 jan
                }
                content = [content stringByAppendingString: [NSString stringWithFormat: @"\n\n%@%@", LOCALIZATION(C_REWARDS_PLACEDON), [self.redemptionDetailsDict objectForKey: @"RedemptionDate"]]];      //lokalise 31 jan
                //            content = [content stringByAppendingString: [NSString stringWithFormat: @"\nPoints used: %@ %@", [self.redemptionDetailsDict objectForKey: @"Points"], LOCALIZATION(C_REWARDS_POINTS)]];
            }
            content = [content stringByAppendingString: [NSString stringWithFormat: @"\n%@%@ %@", LOCALIZATION(C_REWARDS_POINTSUSED), [self.redemptionDetailsDict objectForKey: @"TotalPoints"], LOCALIZATION(C_REWARDS_POINTS)]];     //lokalise 31 jan
            
            UILabel *lblContent = [cell viewWithTag:kRewardRedemptionDetails_CONTENT];
            lblContent.font = FONT_B1;
            lblContent.text = content;
        }
            break;
        case kRewardRedemptionSections_SUMMARY:
        {
            NSDictionary *cellData = [self.summaryArray objectAtIndex: indexPath.row];
            
            cell = [tableView dequeueReusableCellWithIdentifier: @"SummaryCell"];
            
            UIImageView *imgItem = [cell viewWithTag: kRewardRedemptionSummary_IMAGEVIEW];
            
            NSString *urlString = [[cellData objectForKey: @"ItemImage"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            [imgItem sd_setImageWithURL: [NSURL URLWithString: urlString] placeholderImage: [UIImage imageNamed: @"placeholder_giftbox"] options: (SDWebImageRefreshCached  | SDWebImageRetryFailed)];

            UILabel *lblTitle = [cell viewWithTag: kRewardRedemptionSummary_TITLE];
            lblTitle.text = [cellData objectForKey:@"ItemName"];
            lblTitle.font = FONT_H1;

            NSString *pointsContent =  [NSString stringWithFormat: @"%@ %@", [[cellData objectForKey:@"TotalItemPoits"] stringValue], LOCALIZATION(C_REWARDS_POINTS)];     //lokalise 31 jan
            
            UILabel *lblPoints = [cell viewWithTag: kRewardRedemptionSummary_POINTS];
            lblPoints.text = pointsContent;
            lblPoints.textColor = COLOUR_RED;
            lblPoints.font = FONT_B1;
            
            NSString *qtyContent =  [NSString stringWithFormat: @"%@: %@", LOCALIZATION(C_REWARDS_QUANTITY), [[cellData objectForKey:@"Quantity"] stringValue]];     //lokalise 31 jan
            
            UILabel *lblQty = [cell viewWithTag: kRewardRedemptionSummary_QTY];
            lblQty.text = qtyContent;
            lblQty.font = FONT_B1;

        }
            break;
        case kRewardRedemptionSections_CANCELLED:
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"CancelledCell"];
            
            NSDictionary *cellData = [self.cancelledArray objectAtIndex: indexPath.row];
            
            UIImageView *imgItem = [cell viewWithTag: kRewardRedemptionCancelled_IMAGEVIEW];
            
            NSString *urlString = [[cellData objectForKey: @"ItemImage"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            [imgItem sd_setImageWithURL: [NSURL URLWithString: urlString] placeholderImage: [UIImage imageNamed: @"placeholder_giftbox"] options: (SDWebImageRefreshCached  | SDWebImageRetryFailed)];
            
            UILabel *lblTitle = [cell viewWithTag: kRewardRedemptionCancelled_TITLE];
            lblTitle.text = [cellData objectForKey:@"ItemName"];
            lblTitle.font = FONT_H1;
            
            UILabel *lblReason = [cell viewWithTag: kRewardRedemptionCancelled_REASON];
            lblReason.font = FONT_B1;
            lblReason.text = LOCALIZATION(C_REWARDS_REASON);     //lokalise 31 jan
            
            UILabel *lblReasonContent = [cell viewWithTag: kRewardRedemptionCancelled_CONTENT];
            lblReasonContent.text = [cellData objectForKey:@"Remarks"];
            lblReasonContent.font = FONT_B1;
        }
            break;
        case kRewardRedemptionSections_CONTACT:
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"ContactCell"];
            
            UILabel *lblHeader = [cell viewWithTag: kRewardRedemptionContact_HEADER];
            lblHeader.font = FONT_H1;
            lblHeader.text = LOCALIZATION(C_REWARDS_CONTACTINFO);   //lokalise 31 jan
            
            UILabel *lblContent = [cell viewWithTag: kRewardRedemptionContact_CONTENT];
            lblContent.font = FONT_B1;
            if(!IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
                if (![[[mSession lookupTable] objectForKey: @"ContactUSCSNumber"] isKindOfClass: [NSNull class]])
                {
                    NSString *contactString = [LOCALIZATION(C_REWARDS_CONTACTCONTENT) stringByReplacingOccurrencesOfString:@"XXXX" withString: [[mSession lookupTable] objectForKey: @"ContactUSCSNumber"]];    //lokalise 31 jan
                    lblContent.text = contactString;
                }
                else
                {
                    lblContent.text = LOCALIZATION(C_REWARDS_CONTACTCONTENT); //lokalise 31 jan
                }
            } else {
                switch([[mSession profileInfo] businessType]) {
                    case 1:{
                        NSString *contactString = [LOCALIZATION(C_REWARDS_CONTACTCONTENT) stringByReplacingOccurrencesOfString:@"XXXX" withString: [[mSession lookupTable] objectForKey: @"ContactUSB2BCSNumber"]];    //lokalise 31 jan
                        lblContent.text = contactString;
                    }
                        break;
                    case 2:
                    {
                        NSString *contactString = [LOCALIZATION(C_REWARDS_CONTACTCONTENT) stringByReplacingOccurrencesOfString:@"XXXX" withString: [[mSession lookupTable] objectForKey: @"ContactUSB2CCSNumber"]];    //lokalise 31 jan
                        lblContent.text = contactString;
                    }
                        break;
                    case 3:
                    {
                        NSString *contactString = [LOCALIZATION(C_REWARDS_CONTACTCONTENT) stringByReplacingOccurrencesOfString:@"XXXX" withString:[NSString stringWithFormat:@"%@ %@ / %@ %@",LOCALIZATION(C_POPUP_B2B_SUPPORT), [[mSession lookupTable] objectForKey: @"ContactUSB2BCSNumber"],LOCALIZATION(C_POPUP_B2C_SUPPORT), [[mSession lookupTable] objectForKey: @"ContactUSB2CCSNumber"]]];    //lokalise 31 jan
                        lblContent.text = contactString;
                    }
                        break;
                    default:
                        break;
                }
            }
            
            UIButton *btnContact = [cell viewWithTag: kRewardRedemptionContact_BUTTON];
            btnContact.titleLabel.font = FONT_BUTTON;
            [btnContact setTitle: LOCALIZATION(C_REWARDS_CONTACTUS) forState: UIControlStateNormal];    //lokalise 31 jan
            [btnContact setBackgroundColor:COLOUR_RED];
        }
            break;
        default:
            break;
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (IBAction)contactUsPressed:(id)sender
{
    //push?
    //load?
    [mSession loadContactView];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
