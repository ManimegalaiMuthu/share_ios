//
//  RewardsRedemptionDetailsViewController.h
//  Shell
//
//  Created by Jeremy Lua on 6/7/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface RewardsRedemptionDetailsViewController : BaseVC
@property NSDictionary *redemptionDetailsDict;
@end
