//
//  RewardsItemListViewController.m
//  Shell
//
//  Created by Jeremy Lua on 5/7/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RewardsItemListViewController.h"
#import "PopupRewardFilterViewController.h"
#import "PopupViewController.h"

@interface RewardsItemListViewController () <WebServiceManagerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, PopupRewardFilterDelegate, PopupListDelegate>

#define CELL_WIDTH (SCREEN_WIDTH - 48) / 2

enum kRewardCellTag_TYPE
{
    kRewardCellTag_IMAGEVIEW = 1,
    kRewardCellTag_NAME,
    //    kRewardCellTag_DESCRIPTION,
    //    kRewardCellTag_POINTSREQUIRED,
    kRewardCellTag_POINTS = 5,
    kRewardCellTag_POINTSSUFFIX,
    
    kRewardCellTag_ORIGINALPRICE = 7,
    kRewardCellTag_PROMOTION,
    
    kRewardCellTag_LINE = 999,
};

enum kFilterCellTag_TYPE
{
    kFilterCellTag_SORTBY = 1,
    kFilterCellTag_FILTERBYPOINTS,
};

@property NSInteger selectedDropdownIndex;

@property NSInteger leftValue;
@property NSInteger rightValue;
@property BOOL filterPressed; //prevent filter content change
@property NSInteger maxPoints;
@property NSInteger minPoints;
//@property BOOL userCanRedeem;
//
//@property NSString *sortID;
@end

@implementation RewardsItemListViewController
//@synthesize listView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSArray *redeemLookupArray = [[mSession lookupTable] objectForKey: @"RedemPointRange"];
    NSString *redeemRangeString = [[redeemLookupArray firstObject] objectForKey: @"KeyValue"]; //RedemPointRange is array
    NSArray *redeemRangeStringArray = [redeemRangeString componentsSeparatedByString: @"-"]; //if slider reach first value, convert to second value
    
    self.leftValue = 0;
    self.rightValue = -1;
    self.maxPoints = [[redeemRangeStringArray lastObject] intValue];
    self.minPoints = [[redeemRangeStringArray firstObject] intValue];
    
    [self refreshDataWithCategoryCode];
}

-(void) refreshDataWithCategoryCode
{
    self.filterDictionary = [[NSMutableDictionary alloc] init];
    
    //double validation, to refactor
    if (![self.categoryCode isEqualToString:BLANK])
        [self.filterDictionary setObject: self.categoryCode forKey: @"CategoryCode"];
    else
        [self.filterDictionary setObject: @"" forKey: @"CategoryCode"];
    
    NSString *pointsRangeString = @"";
    
    if(self.rightValue != -1) {
        if (self.rightValue >= self.minPoints &&
            self.rightValue != [mSession rewardsCurrentPoints])
        {
            pointsRangeString = [NSString stringWithFormat: @"%ld-%ld", (long)self.leftValue, (long)self.maxPoints]; //all object max range
        }
        else
        {
            pointsRangeString = [NSString stringWithFormat: @"%ld-%ld", (long)self.leftValue, (long)self.rightValue];
        }
    }
    
    UICollectionViewCell *filterCell = [self.colView cellForItemAtIndexPath: [NSIndexPath indexPathForRow: kFilterCellTag_FILTERBYPOINTS - 1 inSection:0]];
    
    UIButton *btnFilterBy = [filterCell viewWithTag: 1];
    if (self.filterPressed)
        [btnFilterBy setTitle: pointsRangeString forState: UIControlStateNormal];
    
    [self.filterDictionary setObject: pointsRangeString forKey: @"PointsRange"];
    
    UICollectionViewCell *sortByCell = [self.colView cellForItemAtIndexPath: [NSIndexPath indexPathForRow: kFilterCellTag_SORTBY - 1 inSection:0]];
    
    UIButton *btnSortBy = [sortByCell viewWithTag: 1];
    
    [self.filterDictionary setObject: [mSession convertToRedeemSortKeyCode: btnSortBy.titleLabel.text] forKey:@"SortID"];
    
//    [self.filterDictionary setObject:@"0" forKey:@"SortID"];
    [self.filterDictionary setObject: pointsRangeString forKey:@"PointsRange"];
    [self.filterDictionary setObject: @(0.0) forKey:@"TotalPoints"];
    [self.filterDictionary setObject: @(self.leftValue) forKey:@"LeftValue"];
    [self.filterDictionary setObject: @(self.rightValue) forKey:@"RightValue"];
    [self.filterDictionary setObject:@(NO) forKey:@"ShowRewardsCanRedem"];
    
    [[WebServiceManager sharedInstance] fetchRewardsList: self.filterDictionary vc:self];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    {
     return [self.itemsArray count] + kFilterCellTag_FILTERBYPOINTS; //+2 for filter cells
//        return [self.itemsArray count] + 1; //+1 for view all rewards
//        return [self.itemsArray count];
    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    depending on cell, return size.
    if (indexPath.row + 1 == kFilterCellTag_SORTBY ||
        indexPath.row + 1 == kFilterCellTag_FILTERBYPOINTS) //filter cell
    {
        return CGSizeMake(CELL_WIDTH, 44);
    }
    else //reward cell
    {
        return CGSizeMake(CELL_WIDTH, CELL_WIDTH + 100);
    }
}



-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    {
        if (indexPath.row + 1 == kFilterCellTag_SORTBY) //filter cell
        {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"SortCell" forIndexPath:indexPath];

//            cell.tag = indexPath.row;
            UIButton *btnDropdown = [cell viewWithTag: 1];
            
            if (kIsRightToLeft) {
                [btnDropdown setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
            } else {
                [btnDropdown setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            }

            [Helper setCustomFontButtonContentModes: btnDropdown];
            if ([btnDropdown.titleLabel.text length] == 0)
                [btnDropdown setTitle: LOCALIZATION(C_REWARDS_SORTBY) forState: UIControlStateNormal];    //lokalise 31 jan
//                [btnDropdown setTitle: LOCALIZATION(C_REWARDS_FILTERCATEGORY) forState: UIControlStateNormal];
            [btnDropdown.titleLabel setFont: FONT_B1];
            
//            [btnDropdown addTarget: self action:@selector(sortPressed) forControlEvents: UIControlEventTouchUpInside];
        }
        else if (indexPath.row + 1 == kFilterCellTag_FILTERBYPOINTS)
        {
//            cell.tag = indexPath.row;
            cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"FilterCell" forIndexPath:indexPath];

            UIButton *btnDropdown = [cell viewWithTag: 1];

            if (kIsRightToLeft) {
                [btnDropdown setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
            } else {
                [btnDropdown setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            }
            
            [Helper setCustomFontButtonContentModes: btnDropdown];
            if ([btnDropdown.titleLabel.text length] == 0)
                [btnDropdown setTitle: LOCALIZATION(C_REWARDS_FILTERBY) forState: UIControlStateNormal];   //lokalise 31 jan
//                [btnDropdown setTitle: LOCALIZATION(C_REWARDS_FILTERPOINTS) forState: UIControlStateNormal];
            [btnDropdown.titleLabel setFont: FONT_B1];
            
//            [btnDropdown addTarget: self action:@selector(filterPressed) forControlEvents: UIControlEventTouchUpInside];
        }
        else //reward cell
        {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"RewardCell" forIndexPath:indexPath];
            
            NSDictionary *itemDetailDict = [self.itemsArray objectAtIndex: indexPath.row - kFilterCellTag_FILTERBYPOINTS]; //-2 for dropdown cells
//            NSDictionary *itemDetailDict = [self.itemsArray objectAtIndex: indexPath.row];
            
            UIImageView *cellImgView = [cell viewWithTag: kRewardCellTag_IMAGEVIEW];
            NSString *urlString = [[itemDetailDict objectForKey: @"ImageTH"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            [cellImgView sd_setImageWithURL: [NSURL URLWithString: urlString] placeholderImage: [UIImage imageNamed: @"placeholder_giftbox_thumb"] options:
             (SDWebImageRefreshCached | SDWebImageRetryFailed)];
            
            UILabel *lblName = [cell viewWithTag: kRewardCellTag_NAME];
            lblName.text = [itemDetailDict objectForKey: @"ProductName"];
            lblName.font = FONT_H2;
            lblName.textColor = COLOUR_VERYDARKGREY;
            
            UILabel *lblPointsSuffix = [cell viewWithTag: kRewardCellTag_POINTSSUFFIX];
            lblPointsSuffix.text = LOCALIZATION(C_REWARDS_POINTS);    //lokalise 31 jan
            lblPointsSuffix.font = FONT_B3;
            lblPointsSuffix.textColor = COLOUR_VERYDARKGREY;
            
            
//            UILabel *lblPoints =  [cell viewWithTag: kRewardCellTag_POINTS];
//            lblPoints.font = FONT_H1;
//            lblPoints.text = [[itemDetailDict objectForKey: @"ProductPoint"] stringValue];
            
            UILabel *lblPoints =  [cell viewWithTag: kRewardCellTag_POINTS];
            lblPoints.font = FONT_H1;
            lblPoints.textColor = COLOUR_RED;
            
            UILabel *lblOriginalPrice = [cell viewWithTag: kRewardCellTag_ORIGINALPRICE];
            lblOriginalPrice.font = FONT_H2;
            lblOriginalPrice.textColor = COLOUR_VERYDARKGREY;
            
            UILabel *lblPromotion = [cell viewWithTag: kRewardCellTag_PROMOTION];
            
            if ([[itemDetailDict objectForKey: @"DiscountedPoint"] doubleValue] > 0)
            {
                lblPoints.text = [[itemDetailDict objectForKey: @"DiscountedPoint"] stringValue];
                lblOriginalPrice.text = [NSString stringWithFormat: @"%@%@", LOCALIZATION(C_REWARDS_UP),   [[itemDetailDict objectForKey: @"ProductPoint"] stringValue]];  //lokalised 11 Feb
                lblOriginalPrice.hidden = NO;
                
                UILabel *lblPromotion = [cell viewWithTag: kRewardCellTag_PROMOTION];
                //            lblName.text = LOCALIZATION(@"PROMOTION");
                lblPromotion.font = FONT_H2;
                lblPromotion.textColor = COLOUR_RED;
                lblPromotion.hidden = NO;
            }
            else
            {
                lblPoints.text = [[itemDetailDict objectForKey: @"ProductPoint"] stringValue];
                lblOriginalPrice.hidden = YES;
                
                lblPromotion.hidden = YES;
            }
            
            UIView *lineView = [cell viewWithTag:kRewardCellTag_LINE];
            [lineView setBackgroundColor:COLOUR_RED];
        }
    }
    return cell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.canRedeem && !IS_COUNTRY(COUNTRYCODE_RUSSIA))
        return;
    
    if (indexPath.row + 1 == kFilterCellTag_SORTBY ||
        indexPath.row + 1 == kFilterCellTag_FILTERBYPOINTS) //filter cells no need select
        return;
    
    //
    NSDictionary *itemDetailDict = [self.itemsArray objectAtIndex: indexPath.row - kFilterCellTag_FILTERBYPOINTS]; //-2 for dropdown cells
//        NSDictionary *itemDetailDict = [self.itemsArray objectAtIndex: indexPath.row];
    
    //push to item details view
    [mSession pushRewardsItemDetailsView: itemDetailDict vc:self];   
}
- (IBAction)sortPressed:(UIButton *)sender
{
    self.selectedDropdownIndex = kFilterCellTag_SORTBY;
    [self popUpListViewWithTitle: LOCALIZATION(C_REWARDS_SORTBY) listArray: [mSession getRedeemSort]];   //lokalise 31 jan
}
- (IBAction)filterPressed:(UIButton *)sender
{
    self.filterPressed = YES;
    self.selectedDropdownIndex = kFilterCellTag_FILTERBYPOINTS;
    //    [self popUpListViewWithTitle: @"Filter by" listArray: [mSession getRedeemSort]];
    
//    RedemPointMaxValue
    if(self.rightValue != -1) {
        [self initFilterRewardPopup: LOCALIZATION(C_REWARDS_FILTERBY)   //lokalise 31 jan
                          leftValue: [[self.filterDictionary objectForKey: @"LeftValue"] intValue]
                         rightValue:[[self.filterDictionary objectForKey: @"RightValue"] intValue]
                          isChecked: [[self.filterDictionary objectForKey: @"ShowRewardsCanRedem"] boolValue]];
    } else {
        [self initFilterRewardPopup: LOCALIZATION(C_REWARDS_FILTERBY)   //lokalise 31 jan
                          leftValue: [[self.filterDictionary objectForKey: @"LeftValue"] intValue]
                         rightValue: self.maxPoints
                          isChecked: [[self.filterDictionary objectForKey: @"ShowRewardsCanRedem"] boolValue]];
    }
    
}

-(void)didFinishRewardFilter:(NSMutableDictionary *) rewardFilter
{
    self.leftValue = [[rewardFilter objectForKey: @"LeftValue"] intValue];
    self.rightValue = [[rewardFilter objectForKey: @"RightValue"] intValue];
    BOOL userCanRedeem = [[rewardFilter objectForKey: @"ShowRewardsCanRedem"] boolValue];
    
    [self.filterDictionary setObject:@(self.leftValue) forKey:@"LeftValue"];
    [self.filterDictionary setObject:@(self.rightValue) forKey:@"RightValue"];
    [self.filterDictionary setObject:@(userCanRedeem) forKey:@"ShowRewardsCanRedem"];
    
    NSString *pointsRangeString = @"";
    
    if (self.rightValue >= self.minPoints &&
        self.rightValue != [mSession rewardsCurrentPoints])
    {
        pointsRangeString = [NSString stringWithFormat: @"%ld-%ld", (long)self.leftValue, (long)self.maxPoints]; //all object max range
    }
    else
    {
        pointsRangeString = [NSString stringWithFormat: @"%ld-%ld", (long)self.leftValue, (long)self.rightValue];
    }
    
    UICollectionViewCell *cell = [self.colView cellForItemAtIndexPath: [NSIndexPath indexPathForRow: kFilterCellTag_FILTERBYPOINTS - 1 inSection:0]];
    
    UIButton *btnDropdown = [cell viewWithTag: 1];
    [btnDropdown setTitle: pointsRangeString forState: UIControlStateNormal];
    
    [self.filterDictionary setObject: pointsRangeString forKey: @"PointsRange"];
    
    [[WebServiceManager sharedInstance] fetchRewardsList: self.filterDictionary vc:self];
    
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0)
    {
        switch (self.selectedDropdownIndex) {
            case kFilterCellTag_SORTBY:
            {
                UICollectionViewCell *cell = [self.colView cellForItemAtIndexPath: [NSIndexPath indexPathForRow: kFilterCellTag_SORTBY - 1 inSection:0]];
                
                UIButton *btnDropdown = [cell viewWithTag: 1];
                [btnDropdown setTitle: selection.firstObject forState: UIControlStateNormal];
                
                [self.filterDictionary setObject: [mSession convertToRedeemSortKeyCode: selection.firstObject] forKey:@"SortID"];
                
                [[WebServiceManager sharedInstance] fetchRewardsList: self.filterDictionary vc:self];
            }
                break;
//            case kFilterCellTag_FILTERBYPOINTS:
//            {
//                UICollectionViewCell *cell = [self.colView cellForItemAtIndexPath: [NSIndexPath indexPathForRow: kFilterCellTag_SORTBY - 1 inSection:0]];
//
//                UIButton *btnDropdown = [cell viewWithTag: 1];
//                [btnDropdown setTitle: selection.firstObject forState: UIControlStateNormal];
//            }
            default:
                break;
        }
    }
    self.selectedDropdownIndex = 0;
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_REWARDSFETCHLIST:
        {
            self.itemsArray = [[response getGenericResponse] objectForKey: @"ItemsList"];
            
            if([self.itemsArray isKindOfClass:[NSNull class]]) {
                self.itemsArray = @[];
            }
            
            [self.colView reloadData];
        }
            
            //            [self.colView reloadData];
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
