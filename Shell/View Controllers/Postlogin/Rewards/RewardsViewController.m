//
//  RewardsViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 13/10/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "RewardsViewController.h"
#import "RewardsHomeViewController.h"
#import "RewardsCategoryViewController.h"
#import "RewardsItemDetailsViewController.h"
#import "JASidePanelController.h"

#import "UIBarButtonItem+Badge.h"

@interface RewardsViewController () <WebServiceManagerDelegate, RewardsHomeViewDelegate>

@property UIBarButtonItem *hamburgerButton;

@property (weak, nonatomic) IBOutlet UILabel *lblCurrentPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentPointsData;
@property (weak, nonatomic) IBOutlet UILabel *lblPointsSuffix;

@property (weak, nonatomic) IBOutlet UIButton *btnCart;
@property (weak, nonatomic) IBOutlet UIButton *btnFavourites;
@property (weak, nonatomic) IBOutlet UILabel *lblFavourites;

@property (weak, nonatomic) IBOutlet UIButton *btnHistory;
@property (weak, nonatomic) IBOutlet UILabel *lblHistory;

@property (weak, nonatomic) IBOutlet UIView *vcContainerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vcContainerTopSpace;

@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UIView *topSection;

@property (weak, nonatomic) IBOutlet UIButton *btnHowToRedeem;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *redeemHeight;

@property BOOL canRedeem;
@property BOOL showWalkthrough;
@end

@implementation RewardsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_VIETNAM]){
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_REWARDS) subtitle:@"" size:16 subtitleSize:12];
    } else {    //lokalise 31 Jan
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_REWARDS) subtitle: @""];
    }    //lokalise 31 Jan
    
    if(@available(iOS 11, *)){
        [self.btnCart.widthAnchor constraintEqualToConstant: 25.0].active = YES;
        [self.btnCart.heightAnchor constraintEqualToConstant: 25.0].active = YES;
    }
    
    //hide first
    self.topSection.hidden = YES;
    self.redeemHeight.constant = 0;
    self.btnHowToRedeem.hidden = YES;
    
    RewardsHomeViewController *rewardsHomeVc = [STORYBOARD_REWARDS instantiateViewControllerWithIdentifier: VIEW_REWARDS_HOME];
    rewardsHomeVc.delegate = self;
    
    [self addChildViewController: rewardsHomeVc];
    [self.vcContainerView addSubview: rewardsHomeVc.view];
    [rewardsHomeVc.view setFrame: CGRectMake(0, 0, self.vcContainerView.frame.size.width, self.vcContainerView.frame.size.height)];
    
    [self setupInterface];
    
    if (kIsRightToLeft) {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
}

-(void) rewardsShouldUpdateUIWithCanRedeemFlag:(BOOL)canRedeem showWalkthrough:(BOOL)showWalkthrough
{
    self.canRedeem = canRedeem;
    SET_REWARDS_CANREDEEM(self.canRedeem);
    self.showWalkthrough = showWalkthrough;
    
    if (self.canRedeem || IS_COUNTRY(COUNTRYCODE_RUSSIA))
    {
        self.topSection.hidden = NO;
        [NSLayoutConstraint constraintWithItem:self.topSection attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:80].active = YES;
        UIBarButtonItem *cartIcon = [[UIBarButtonItem alloc] initWithCustomView: self.btnCart];
        self.navigationItem.rightBarButtonItem = cartIcon;
        
        [[WebServiceManager sharedInstance] fetchPointsSummary: self];
        self.vcContainerTopSpace.constant = self.topSection.frame.size.height;
        
    }
    else
    {
        self.topSection.hidden = YES;
        [NSLayoutConstraint constraintWithItem:self.topSection attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
        self.vcContainerTopSpace.constant = 0;
    }
    
    if (self.showWalkthrough) {
        self.redeemHeight.constant = 50; //show button
        self.btnHowToRedeem.hidden = NO;
    }
    else {
        self.redeemHeight.constant = 0;
        self.btnHowToRedeem.hidden = YES;
    }
}

- (IBAction)howToRedeemPressed:(id)sender
{
    [mSession pushWalkthroughView: self WithType: WALKTHORUGH_Rewards];
}

-(void)didPressedCategory:(NSMutableArray *)mainCategoryArray selectedIndex:(NSUInteger)selectedIndex
{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    
    RewardsCategoryViewController *rewardsCategoryVc = [STORYBOARD_REWARDS instantiateViewControllerWithIdentifier: VIEW_REWARDS_CATEGORY];
//    rewardsCategoryVc.delegate = self;
    rewardsCategoryVc.canRedeem = self.canRedeem;
    
    rewardsCategoryVc.mainCategoryArray = mainCategoryArray;
    rewardsCategoryVc.selectedIndex = selectedIndex;
    rewardsCategoryVc.canRedeem = self.canRedeem;
    
    [self addChildViewController: rewardsCategoryVc];
    [self.vcContainerView addSubview: rewardsCategoryVc.view];
    [rewardsCategoryVc.view setFrame: CGRectMake(0, 0, self.vcContainerView.frame.size.width, self.vcContainerView.frame.size.height)];
}

-(void) didPressedFeaturedReward:(id)sender
{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    
    RewardsCategoryViewController *rewardsCategoryVc = [STORYBOARD_REWARDS instantiateViewControllerWithIdentifier: VIEW_REWARDS_CATEGORY];
    //    rewardsCategoryVc.delegate = self;
    
    //select featured reward category
    
    
    [self addChildViewController: rewardsCategoryVc];
    [self.vcContainerView addSubview: rewardsCategoryVc.view];
    [rewardsCategoryVc.view setFrame: CGRectMake(0, 0, self.vcContainerView.frame.size.width, self.vcContainerView.frame.size.height)];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [self setupGoogleAnalytics];
}

- (IBAction)backPressed:(id)sender {
//    [self popSelf];
    if ([[self.vcContainerView subviews] count] > 1)
    {
        [[[self.vcContainerView subviews] lastObject] removeFromSuperview];
        [[[self childViewControllers] lastObject] removeFromParentViewController];
        
        if ([[self.vcContainerView subviews] count ] <= 1
            )  //left homeview, change back to hamburger
        {
            JASidePanelController *rootController = (JASidePanelController *) CURRENT_VIEWCONTROLLER;
            self.navigationItem.leftBarButtonItem = [rootController barButtonForCenterPanelIsRight:kIsRightToLeft];
        }
    }
}

-(void) setupGoogleAnalytics
{
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_REWARDS) screenClass:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
//    if (GET_REWARDSCART)
    {
        NSArray *savedArray = GET_REWARDSCART;
        self.navigationItem.rightBarButtonItem.badgeValue = @(savedArray.count).stringValue;
        self.navigationItem.rightBarButtonItem.badgeOriginX = 15; //default value as of RewardsView
    }
}

-(void) setupInterface
{
    self.lblCurrentPoints.text = LOCALIZATION(C_REWARDS_CURRENTPOINTS); //lokalise 31 Jan
    self.lblCurrentPoints.font = FONT_B3;
    
    self.lblCurrentPointsData.font = FONT_H(25);
    self.lblCurrentPointsData.textColor = COLOUR_RED;
    
    self.btnHowToRedeem.titleLabel.font = FONT_BUTTON;
    [self.btnHowToRedeem setBackgroundColor:COLOUR_RED];
    [self.btnHowToRedeem setTitle:LOCALIZATION(C_BTN_HOWTOREDEEM) forState:UIControlStateNormal];
    
    self.lblPointsSuffix.text = LOCALIZATION(C_REWARDS_POINTS);      //lokalise 31 Jan
    self.lblPointsSuffix.font = self.lblCurrentPoints.font;
    self.lblPointsSuffix.textColor = self.lblCurrentPointsData.textColor;
    
//    self.lblCart.text = LOCALIZATION(C_REWARDS_CARTBTN);
//    self.lblCart.font = FONT_B3;
    
    self.lblFavourites.text = LOCALIZATION(C_REWARDS_FAVOURITESBTN);     //lokalise 31 Jan
    self.lblFavourites.font = FONT_B3;
    
    self.lblHistory.text = LOCALIZATION(C_REWARDS_HISTORY);  //lokalise 31 Jan
    self.lblHistory.font = FONT_B3;

}

- (IBAction)cartPressed:(UIButton *)sender {
    //push to cart view
    [mSession pushRewardsCart: self];
}

- (IBAction)historyPressed:(id)sender {
    [mSession pushRewardsHistory: self];
}

- (IBAction)favouritesPressed:(UIButton *)sender {
    //push to fav view
    [mSession pushRewardsFavourites: self];
    
}



-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_REWARDSPOINTSSUMMARY:
            [mSession setRewardsCurrentPoints: [[[response getGenericResponse] objectForKey: @"BalancePoints"] doubleValue]];
            self.lblCurrentPointsData.text = [NSString stringWithFormat: @"%.1f", [mSession rewardsCurrentPoints]];
//            self.lblCurrentPointsData.text = [[response getGenericResponse] objectForKey: @"BalancePoints"];
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
