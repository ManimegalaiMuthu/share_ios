//
//  RewardsItemListViewController.h
//  Shell
//
//  Created by Jeremy Lua on 5/7/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface RewardsItemListViewController : BaseVC
@property NSString *categoryCode;

@property NSArray *itemsArray;

@property NSMutableDictionary *filterDictionary;
@property (weak, nonatomic) IBOutlet UICollectionView *colView;
@property BOOL canRedeem;
-(void) refreshDataWithCategoryCode;
@end
