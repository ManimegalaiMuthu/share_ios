//
//  RewardsHomeViewController.m
//  Shell
//
//  Created by Jeremy Lua on 5/7/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RewardsHomeViewController.h"
#import "Shell-Swift.h"

@interface RewardsHomeViewController () <WebServiceManagerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, CommonListDelegate, FSPagerViewDelegate,FSPagerViewDataSource>

#define CELL_WIDTH (SCREEN_WIDTH - 48) / 2

enum kRewardCellTag_TYPE
{
    kRewardCellTag_IMAGEVIEW = 1,
    kRewardCellTag_NAME,
    //    kRewardCellTag_DESCRIPTION,
    //    kRewardCellTag_POINTSREQUIRED,
    kRewardCellTag_POINTS = 5,
    kRewardCellTag_POINTSSUFFIX,
    
    kRewardCellTag_ORIGINALPRICE = 7,
    kRewardCellTag_PROMOTION,
    
    kRewardCellTag_LINE = 999,
};

enum kCategoryCellTag_TYPE
{
    kCategoryCellTag_IMAGE = 1,
    kCategoryCellTag_LABEL,
};


@property NSArray *itemsArray;

@property (weak, nonatomic) IBOutlet UICollectionView *colView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *colViewLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *colViewHeight;

@property (weak, nonatomic) IBOutlet UILabel *lblCategoryHeader;
@property (weak, nonatomic) IBOutlet UICollectionView *categoryColView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *categoryColViewLayout;
@property NSArray *categoryArray;


#pragma mark V2
@property (weak, nonatomic) IBOutlet UILabel *lblFeaturedRewardsHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnHistory;
@property (weak, nonatomic) IBOutlet UILabel *lblHistory;


@property (weak, nonatomic) IBOutlet UIView *carouselView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *carouselHeight;
@property (weak, nonatomic) IBOutlet FSPagerView *pagerView;
@property (weak, nonatomic) IBOutlet FSPageControl *pagerController;
@property NSArray *carouselArray;

@property NSDictionary *itemDetailsData;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *categoryHeight;
@property (weak, nonatomic) IBOutlet UIView *categoryView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *categoryViewHeight;

@property BOOL canRedeem;
@property BOOL isEmptyCellNeeded;

@end

@implementation RewardsHomeViewController
@synthesize listView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupInterface];
    
    [[WebServiceManager sharedInstance] fetchRewardsHome:self];
}

-(void) setupInterface
{
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_REWARDS_FILTERCATEGORY)
                                                   list: [mSession getRewardsCategory]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];    //lokalise 31 Jan
    listView.delegate = self;

    
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        self.categoryView.hidden = YES;
        self.categoryViewHeight.constant = 0;
        self.categoryHeight.constant = 0;
        [self.view layoutSubviews];
    }
    else
    {
        self.lblCategoryHeader.text = LOCALIZATION(C_REWARDS_BROWSEBYCATEGORY);    //lokalise 31 Jan
        self.lblCategoryHeader.font = FONT_H1;
    }
    
//    self.lblFeaturedRewardsHeader.text = LOCALIZATION(C_REWARDS_FEATUREDREWARDS);
    self.lblFeaturedRewardsHeader.font = FONT_H1;
    self.categoryColViewLayout.itemSize = CGSizeMake((SCREEN_WIDTH - 30) / 4, 90);
}



-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.categoryColView)
    {
        return [self.categoryArray count];
    }
    else
    {
        //        return [self.itemsArray count] + kFilterCellTag_POINTS; //+2 for filter cells
        if ([self.itemsArray count] > 0 && !self.isEmptyCellNeeded)
            return [self.itemsArray count] + 1; //+1 for view all rewards
        else if([self.itemsArray count] > 0 && self.isEmptyCellNeeded)
            return [self.itemsArray count] + 2; //+1 Dummy Cell +1 for view all rewards
        else
            return 0;
        //        return [self.itemsArray count];
    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.categoryColView) {
        return CGSizeMake((SCREEN_WIDTH - 40) / 4, 93);
    }
    else {
        //depending on cell, return size.
        //        if (indexPath.row + 1 == kFilterCellTag_CATEGORY ||
        //            indexPath.row + 1 == kFilterCellTag_POINTS) //filter cell
        //        {
        //            return CGSizeMake(CELL_WIDTH, 44);
        //        }
        //        else //reward cell
        if ((indexPath.row == [self.itemsArray count] && !self.isEmptyCellNeeded) || (self.isEmptyCellNeeded && indexPath.row > [self.itemsArray count]))
        {
            return CGSizeMake(SCREEN_WIDTH - 30, 50);
        }
        else
        {
            return CGSizeMake(CELL_WIDTH, CELL_WIDTH + 100);
        }
    }
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    if (collectionView == self.categoryColView)
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"CategoryCell" forIndexPath:indexPath];
        
        NSDictionary *cellData = [self.categoryArray objectAtIndex: indexPath.row];
        
        UIImageView *imgView = [cell viewWithTag: kCategoryCellTag_IMAGE];
        [imgView sd_setImageWithURL: [NSURL URLWithString: [cellData objectForKey: @"Image"]] placeholderImage:nil options:
         (SDWebImageRefreshCached | SDWebImageRetryFailed)];
        //        [imgView setImage: [UIImage imageNamed: @"icn_history.png"]];
        
        UILabel *lblTitle = [cell viewWithTag: kCategoryCellTag_LABEL];
        lblTitle.font = FONT_B2;
        lblTitle.text = [cellData objectForKey: @"KeyValue"];
        
    }
    else
    {
        //        if (indexPath.row + 1 == kFilterCellTag_CATEGORY) //filter cell
        //        {
        //            cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"FilterCell" forIndexPath:indexPath];
        //
        //            UIButton *btnDropdown = [cell viewWithTag: 1];
        //
        //            [Helper setCustomFontButtonContentModes: btnDropdown];
        //            if ([btnDropdown.titleLabel.text length] == 0)
        //                [btnDropdown setTitle: LOCALIZATION(C_REWARDS_FILTERCATEGORY) forState: UIControlStateNormal];
        //            [btnDropdown.titleLabel setFont: FONT_B1];
        //        }
        //        else if (indexPath.row + 1 == kFilterCellTag_POINTS)
        //        {
        //            cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"FilterCell" forIndexPath:indexPath];
        //
        //            UIButton *btnDropdown = [cell viewWithTag: 1];
        //
        //            [Helper setCustomFontButtonContentModes: btnDropdown];
        //            if ([btnDropdown.titleLabel.text length] == 0)
        //                [btnDropdown setTitle: LOCALIZATION(C_REWARDS_FILTERPOINTS) forState: UIControlStateNormal];
        //            [btnDropdown.titleLabel setFont: FONT_B1];
        //        }
        //        else //reward cell
        if  ((indexPath.row == [self.itemsArray count] && !self.isEmptyCellNeeded) || (self.isEmptyCellNeeded && indexPath.row > [self.itemsArray count]))
        {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"ViewAllRewardCell" forIndexPath:indexPath];
            
            UIButton *viewBtn = [cell viewWithTag: 1];
            viewBtn.titleLabel.font = FONT_BUTTON;
            [viewBtn addTarget: self action:@selector(viewBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            [viewBtn setTitle: LOCALIZATION(C_REWARDS_VIEWALLREWARDS) forState:UIControlStateNormal];   //lokalise 31 jan
            [viewBtn setBackgroundColor:COLOUR_RED];
        } else if(self.isEmptyCellNeeded && indexPath.row == [self.itemsArray count]) {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"RewardCell" forIndexPath:indexPath];
            
            UIImageView *cellImgView = [cell viewWithTag: kRewardCellTag_IMAGEVIEW];
            cellImgView.hidden = YES;
            
            UILabel *lblName = [cell viewWithTag: kRewardCellTag_NAME];
            lblName.hidden = YES;
            
            UILabel *lblPointsSuffix = [cell viewWithTag: kRewardCellTag_POINTSSUFFIX];
            lblPointsSuffix.hidden = YES;
            
            UILabel *lblPoints =  [cell viewWithTag: kRewardCellTag_POINTS];
            lblPoints.hidden = YES;
            
            UILabel *lblOriginalPrice = [cell viewWithTag: kRewardCellTag_ORIGINALPRICE];
            lblOriginalPrice.hidden = YES;
            
            UILabel *lblPromotion = [cell viewWithTag: kRewardCellTag_PROMOTION];
            lblPromotion.hidden = YES;
            
            UIView *lineView = [cell viewWithTag:kRewardCellTag_LINE];
            [lineView setBackgroundColor:COLOUR_WHITE];
        }
        else
        {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"RewardCell" forIndexPath:indexPath];
            
            //            NSDictionary *itemDetailDict = [self.itemsArray objectAtIndex: indexPath.row - kFilterCellTag_POINTS]; //-2 for dropdown cells
            NSDictionary *itemDetailDict = [self.itemsArray objectAtIndex: indexPath.row];
            
            UIImageView *cellImgView = [cell viewWithTag: kRewardCellTag_IMAGEVIEW];
            NSString *urlString = [[itemDetailDict objectForKey: @"ImageTH"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            [cellImgView sd_setImageWithURL: [NSURL URLWithString: urlString] placeholderImage: [UIImage imageNamed: @"placeholder_giftbox_thumb"] options:
             (SDWebImageRefreshCached | SDWebImageRetryFailed)];
            
            UILabel *lblName = [cell viewWithTag: kRewardCellTag_NAME];
            lblName.text = [itemDetailDict objectForKey: @"ProductName"];
            lblName.font = FONT_H2;
            lblName.textColor = COLOUR_VERYDARKGREY;
            
            UILabel *lblPointsSuffix = [cell viewWithTag: kRewardCellTag_POINTSSUFFIX];
            lblPointsSuffix.text = LOCALIZATION(C_REWARDS_POINTS);  //lokalise 31 jan
            lblPointsSuffix.font = FONT_B3;
            lblPointsSuffix.textColor = COLOUR_VERYDARKGREY;
            
            

            
            UILabel *lblPoints =  [cell viewWithTag: kRewardCellTag_POINTS];
            lblPoints.font = FONT_H1;
            lblPoints.textColor = COLOUR_RED;
            
            UILabel *lblOriginalPrice = [cell viewWithTag: kRewardCellTag_ORIGINALPRICE];
            lblOriginalPrice.font = FONT_H2;
            lblOriginalPrice.textColor = COLOUR_VERYDARKGREY;
            
            UILabel *lblPromotion = [cell viewWithTag: kRewardCellTag_PROMOTION];
            
            if ([[itemDetailDict objectForKey: @"DiscountedPoint"] doubleValue] > 0)
            {
                lblPoints.text = [[itemDetailDict objectForKey: @"DiscountedPoint"] stringValue];
                lblOriginalPrice.text = [NSString stringWithFormat: @"%@%@", LOCALIZATION(C_REWARDS_UP),   [[itemDetailDict objectForKey: @"ProductPoint"] stringValue]];  //lokalised 11 Feb
                lblOriginalPrice.hidden = NO;
                
                UILabel *lblPromotion = [cell viewWithTag: kRewardCellTag_PROMOTION];
                //            lblName.text = LOCALIZATION(@"PROMOTION");
                lblPromotion.font = FONT_H2;
                lblPromotion.textColor = COLOUR_RED;
                lblPromotion.hidden = NO;
            }
            else
            {
                lblPoints.text = [[itemDetailDict objectForKey: @"ProductPoint"] stringValue];
                lblOriginalPrice.hidden = YES;
                
                lblPromotion.hidden = YES;
            }
            
            //        UILabel *lblDescription = [cell viewWithTag: kRewardCellTag_DESCRIPTION];
            //        lblDescription.text = [itemDetailDict objectForKey: @"OverlayText"];
            //        lblDescription.font = FONT_B3;
            //        lblDescription.textColor = COLOUR_MIDGREY;
            
            //        UILabel *lblPointsRequired = [cell viewWithTag: kRewardCellTag_POINTSREQUIRED];
            //        lblPointsRequired.text = [LOCALIZATION(C_REWARDS_POINTSREQUIRED) uppercaseString];
            //        lblPointsRequired.font = FONT_H3;
            
            UIView *lineView = [cell viewWithTag:kRewardCellTag_LINE];
            [lineView setBackgroundColor:COLOUR_RED];
            
        }
    }
    return cell;
}
-(void) viewBtnPressed:(id)sender
{
    [self.delegate didPressedCategory: [[NSMutableArray alloc] initWithArray: self.categoryArray] selectedIndex: 0];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == self.categoryColView)
    {
        //pass category list & selected category?
        [self.delegate didPressedCategory: [[NSMutableArray alloc] initWithArray: self.categoryArray] selectedIndex: indexPath.row];
//        [mSession pushRewardsCategoryWithCategoryArray: self.categoryArray vc:self];
    }
    else
    {
        if (!self.canRedeem && !IS_COUNTRY(COUNTRYCODE_RUSSIA))
            return;
        //        if (indexPath.row == 0) //prevent header cell from being selected and crashing the app
        //            return;
        
        if (indexPath.row == [self.itemsArray count]) //view button no need select
            return;
        //
        //        NSDictionary *itemDetailDict = [self.itemsArray objectAtIndex: indexPath.row - kFilterCellTag_POINTS]; //-2 for dropdown cells
        NSDictionary *itemDetailDict = [self.itemsArray objectAtIndex: indexPath.row];
        
        //push to item details view
        [mSession pushRewardsItemDetailsView: itemDetailDict vc:self];
    }
    
}

#pragma carousel view
-(void)setupCarouselView:(BOOL)isEnable{
    [self.pagerView registerClass:[FSPagerViewCell class] forCellWithReuseIdentifier:@"cell"];
    //    if(isEnable){
    self.pagerController.numberOfPages = self.carouselArray.count;

    [self.pagerController setImage: [UIImage imageNamed: @"icn_bullet_inactive"] forState:UIControlStateNormal];
    [self.pagerController setImage: [UIImage imageNamed: @"icn_bullet_active"] forState:UIControlStateSelected];
    
    
//    [self.pagerController setStrokeColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
//    [self.pagerController setStrokeColor:[UIColor lightGrayColor] forState:UIControlStateSelected];
//
//    [self.pagerController setFillColor:COLOUR_WHITE forState:UIControlStateNormal];
//    [self.pagerController setFillColor:COLOUR_RED forState:UIControlStateSelected];
    
    //    }else{
    //        self.carouselView.hidden = YES;
    //        self.carouselHeight.constant = 0;
    //        self.carouselArray = [[NSMutableArray alloc]initWithArray:@[]];
    //    }
}
- (NSInteger)numberOfItemsInPagerView:(FSPagerView * _Nonnull)pagerView SWIFT_WARN_UNUSED_RESULT
{
    return self.carouselArray.count;
}
- (FSPagerViewCell * _Nonnull)pagerView:(FSPagerView * _Nonnull)pagerView cellForItemAtIndex:(NSInteger)index SWIFT_WARN_UNUSED_RESULT
{
    FSPagerViewCell *cell = [pagerView dequeueReusableCellWithReuseIdentifier:@"cell" atIndex:index];
    
    
    
    [cell.contentView.layer setShadowRadius:0];
    
    NSDictionary *cellData = [self.carouselArray objectAtIndex: index];
    [cell.imageView sd_setImageWithURL: [NSURL URLWithString: [cellData objectForKey: @"Text"]] placeholderImage: [UIImage imageNamed: @"placeholder_giftbox"] options: (SDWebImageRefreshCached  | SDWebImageRetryFailed)];
    //    [cell.imageView setImage: [UIImage imageNamed: @"icn_history.png"]];
    cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
    return cell;
}

- (void)pagerViewWillEndDragging:(FSPagerView *) pagerView targetIndex:(NSInteger)index
{
    self.pagerController.currentPage = index;
}

- (void)pagerView:(FSPagerView *)pagerView willDisplayCell:(FSPagerViewCell *)cell forItemAtIndex:(NSInteger)index
{
    self.pagerController.currentPage = index;
}

-(void)pagerView:(FSPagerView *)pagerView didSelectItemAtIndex:(NSInteger)index
{
    NSDictionary *carouselData = [self.carouselArray objectAtIndex: index];
    
    if (![[carouselData objectForKey: @"IsClickable"] boolValue])
        return; //not clickable, skip
    else
    {
        if ([[carouselData objectForKey: @"IsCategory"] boolValue])
        {
            for (NSDictionary *data in self.categoryArray)
            {
                if ([[data objectForKey: @"KeyCode"] intValue] == index)
                {
                    [self.delegate didPressedCategory: [[NSMutableArray alloc] initWithArray: self.categoryArray]
                                        selectedIndex: index];
                    return;
                }
            }
        }
        else
        {
            [[WebServiceManager sharedInstance] fetchRewardsDetails:[carouselData objectForKey: @"CategoryId"]
                                                           itemCode:[carouselData objectForKey: @"RefId"]
                                                                 vc:self];
        }
        
    }
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_REWARDSFETCHDETAILS:
        {
            [mSession pushRewardsItemDetailsView: [response getGenericResponse] vc:self];
        }
            break;
        case kWEBSERVICE_REWARDSFETCHREWARDSHOME:
            
            self.canRedeem = [[[response getGenericResponse] objectForKey: @"CanRedeem"] boolValue];
            [self.delegate rewardsShouldUpdateUIWithCanRedeemFlag: self.canRedeem
                                                  showWalkthrough: [[[response getGenericResponse] objectForKey: @"ShowWalkThrough"] boolValue]];
            
            if ([[[response getGenericResponse] objectForKey: @"RewardCategory"] isEqual: [NSNull null]])
            {
                self.categoryArray = @[];
            }
            else
            {
                self.categoryArray = [[response getGenericResponse] objectForKey: @"RewardCategory"];
            }
            if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_RUSSIA]) {
                self.categoryHeight.constant = 0;
            } else {
                self.categoryHeight.constant = (80 * (SCREEN_WIDTH / 320)) * ceil(self.categoryArray.count / 4.0f); //4 = cell per row
            }
            NSLog(@"%f", ceil(self.categoryArray.count / 4.0f));
            
            
            if ([[[response getGenericResponse] objectForKey: @"HasCarousel"] boolValue])
            {
                //rewards banner
                if ([[[response getGenericResponse] objectForKey: @"RewardsBanner"] isEqual: [NSNull null]])
                {
                    self.carouselArray = @[];
                }
                else
                {
                    self.carouselArray = [[response getGenericResponse] objectForKey: @"RewardsBanner"];
                }
            }
            else
            {
                //hide all
                self.carouselArray = @[];
                self.itemsArray = @[];
                self.carouselHeight.constant = 0;
                self.lblFeaturedRewardsHeader.hidden = YES;
            }
            
            //featured rewards
            if ([[[response getGenericResponse] objectForKey: @"RewardItems"] isEqual: [NSNull null]])
            {
                self.itemsArray = @[];
            }
            else
            {
                self.itemsArray = [[response getGenericResponse] objectForKey: @"RewardItems"];
            }
            
            if ([self.itemsArray count] > 0)
            {
                self.lblFeaturedRewardsHeader.hidden = NO;
                self.isEmptyCellNeeded = !([self.itemsArray count] % 2 == 0) && [self.itemsArray count] != 1;
            }
            else
            {
                self.lblFeaturedRewardsHeader.hidden = YES;
                self.isEmptyCellNeeded = NO;
            }
            
            self.lblFeaturedRewardsHeader.text = [[response getGenericResponse] objectForKey: @"RewardHeader"];
            
            if ([self.carouselArray count] > 0)
            {
                //banner is 960 x 430. work accordingly
                self.carouselHeight.constant = (180 * (SCREEN_WIDTH / 320));    //category cell is designed based on 4 inch device, 320 width
            }
            else
            {
                //hide banner
                self.carouselHeight.constant = 0;
            }
            
            //must be after assigning array values
            self.colViewHeight.constant = (CELL_WIDTH + 100 + 15 + 50) * (int) (self.itemsArray.count / 2); //100 text space, 15 cell space, 50 button space+content
            
            if(self.itemsArray.count % 2 != 0) {
                self.colViewHeight.constant += (CELL_WIDTH + 100 + 15 + 50);
            }
            
            if(self.itemsArray.count == 1) {
                self.colViewHeight.constant = CELL_WIDTH + 100 + 15 + 60;
            }
            
            //carousel array don't get updated automatically,
            //have to setup carousel after data updated
            [self setupCarouselView:NO];
            [self.colView reloadData];
            [self.categoryColView reloadData];
            [self.pagerView reloadData];
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
