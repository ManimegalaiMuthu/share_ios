//
//  RewardsCategoryViewController
//  Shell
//
//  Created by Jeremy Lua on 5/7/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RewardsCategoryViewController.h"

#import "RewardsItemListViewController.h"

@interface RewardsCategoryViewController () <WebServiceManagerDelegate, UICollectionViewDelegate, UICollectionViewDataSource>


@property (weak, nonatomic) IBOutlet UICollectionView *categoryColView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *mainCategoryColViewLayout;

@property (weak, nonatomic) IBOutlet UIView *vcContainerView;
@property NSMutableArray *mainCategoryCellArray;    //to hold cell references

@property RewardsItemListViewController *itemListVc;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *categoryColViewHeight;
@end

@implementation RewardsCategoryViewController
@synthesize listView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupInterface];
    self.mainCategoryCellArray = [[NSMutableArray alloc] init];
    
    if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_RUSSIA]) {
        self.categoryColView.hidden = YES;
        self.categoryColViewHeight.constant = 0;
    }
    
//    NSDictionary *categoryData = [self.mainCategoryArray objectAtIndex: self.selectedIndex];
//    [categoryData objectForKey: @"KeyValue"]
//        [[WebServiceManager sharedInstance] fetchRewardsList: nil
//                                                          vc:self];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
//    [[WebServiceManager sharedInstance] fetchPointsSummary: self];
    
    [self.categoryColView.collectionViewLayout invalidateLayout];
    [self.categoryColView reloadData];
    
    if(kIsRightToLeft) {
        NSInteger actualIndex = self.mainCategoryArray.count-self.selectedIndex-1;
        [self.categoryColView scrollToItemAtIndexPath: [NSIndexPath indexPathForRow:actualIndex inSection:0] atScrollPosition: UICollectionViewScrollPositionRight animated:NO];
    } else {
        [self.categoryColView scrollToItemAtIndexPath: [NSIndexPath indexPathForRow:self.selectedIndex inSection:0] atScrollPosition: UICollectionViewScrollPositionRight animated:NO];
    }
}

-(void) setupInterface
{
    self.itemListVc = [STORYBOARD_REWARDS instantiateViewControllerWithIdentifier: VIEW_REWARDS_ITEMLIST];
    self.itemListVc.canRedeem = self.canRedeem;
    //fetch list
    NSDictionary *categoryData = [self.mainCategoryArray objectAtIndex: self.selectedIndex];
    
    if (![categoryData objectForKey: @"KeyCode"])
         self.itemListVc.categoryCode = @"";
    else if ([[categoryData objectForKey:@"KeyCode"]isEqualToString:BLANK])
        self.itemListVc.categoryCode = @"";
    else
        self.itemListVc.categoryCode = [categoryData objectForKey: @"KeyCode"];
    
    [self addChildViewController: self.itemListVc];
    [self.vcContainerView addSubview: self.itemListVc.view];
    [self.itemListVc.view setFrame: CGRectMake(0, 0, self.vcContainerView.frame.size.width, self.vcContainerView.frame.size.height)];
    
    self.mainCategoryColViewLayout.estimatedItemSize = CGSizeMake(150, 50);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
//    if (collectionView == self.categoryColView)
    {
        return [self.mainCategoryArray count];
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger actualIndex;
    if (kIsRightToLeft) {
        actualIndex = self.mainCategoryArray.count-indexPath.row-1;
    } else {
        actualIndex = indexPath.row;
    }
    
    UICollectionViewCell *cell;
//    if (collectionView == self.categoryColView)
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"CategoryCell" forIndexPath:indexPath];
        
        NSDictionary *cellData = [self.mainCategoryArray objectAtIndex: actualIndex];
        
        UILabel *lblTitle = [cell viewWithTag: 1];
        lblTitle.font = FONT_H1;
        lblTitle.text = [cellData objectForKey: @"KeyValue"];
        
        UIView *bottomLine = [cell viewWithTag: 2];
        if (actualIndex == self.selectedIndex)
        {
            lblTitle.textColor = COLOUR_RED;
            [bottomLine setBackgroundColor:COLOUR_RED];
            bottomLine.hidden = NO;
        }
        else
        {
            lblTitle.textColor = COLOUR_VERYDARKGREY;
            bottomLine.hidden = YES;
        }
    }
    [self.mainCategoryCellArray addObject: cell];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger actualIndex;
    if (kIsRightToLeft) {
        actualIndex = self.mainCategoryArray.count-indexPath.row-1;
    } else {
        actualIndex = indexPath.row;
    }
    if (collectionView == self.categoryColView)
    {
        for (UICollectionViewCell *cell in self.mainCategoryCellArray)
        {
            UIView *bottomLine = [cell viewWithTag: 2];
            bottomLine.hidden = YES;
            
            UILabel *lblTitle = [cell viewWithTag: 1];
            lblTitle.textColor = COLOUR_VERYDARKGREY;
            bottomLine.hidden = YES;
        }
        
        UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath: indexPath];
        UILabel *lblTitle = [cell viewWithTag: 1];
        lblTitle.textColor = COLOUR_RED;
        
        UIView *bottomLine = [cell viewWithTag: 2];
        [bottomLine setBackgroundColor:COLOUR_RED];
        bottomLine.hidden = NO;
        
        self.selectedIndex = actualIndex;
        
        //fetch list
        NSDictionary *categoryData = [self.mainCategoryArray objectAtIndex: self.selectedIndex];
        
        if (![categoryData objectForKey: @"KeyCode"])
            self.itemListVc.categoryCode = @"";
        else if ([[categoryData objectForKey:@"KeyCode"]isEqualToString:BLANK])
            self.itemListVc.categoryCode = @"";
        else
            self.itemListVc.categoryCode = [categoryData objectForKey: @"KeyCode"];
        [self.itemListVc performSelector: @selector(refreshDataWithCategoryCode) withObject:nil afterDelay:0.1f];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
