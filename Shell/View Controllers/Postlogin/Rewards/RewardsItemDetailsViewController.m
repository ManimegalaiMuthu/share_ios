//
//  RewardsItemDetailsViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 13/10/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "RewardsItemDetailsViewController.h"
#import "RewardsFavouritesViewController.h"
#import "UIBarButtonItem+Badge.h"

@interface RewardsItemDetailsViewController () <WebServiceManagerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentPointsHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentPointsSuffix;
@property (weak, nonatomic) IBOutlet UIButton *btnCart;
@property (weak, nonatomic) IBOutlet UILabel *lblHistory;

@property (weak, nonatomic) IBOutlet UIButton *btnHistory;

@property (weak, nonatomic) IBOutlet UIButton *btnFavourites;
@property (weak, nonatomic) IBOutlet UILabel *lblFavourites;


@property (weak, nonatomic) IBOutlet UIImageView *imgItem;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPointsRequired;

@property (weak, nonatomic) IBOutlet UILabel *lblPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblPointsSuffix;

@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

@property (weak, nonatomic) IBOutlet UIButton *btnAddCart;
@property (weak, nonatomic) IBOutlet UIButton *btnAddFavourites;
@property (weak, nonatomic) IBOutlet UILabel *lblAddFavourite;

@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UILabel *lblPromotion;
@property (weak, nonatomic) IBOutlet UILabel *lblOriginalPoints;

@property NSDictionary *itemDetailsData;
@property BOOL canRedeem;
@end

@implementation RewardsItemDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if(@available(iOS 11, *)){
        [self.btnCart.widthAnchor constraintEqualToConstant: 25.0].active = YES;
        [self.btnCart.heightAnchor constraintEqualToConstant: 25.0].active = YES;
    }
    
    self.canRedeem = GET_REWARDS_CANREDEEM;
    
    if(self.canRedeem){
        UIBarButtonItem *cartIcon = [[UIBarButtonItem alloc] initWithCustomView: self.btnCart];
        self.navigationItem.rightBarButtonItem = cartIcon;
    }
    
    if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_VIETNAM]){
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_REWARDS) subtitle:@"" size:16 subtitleSize:12];  //lokalise 31 jan
    } else {
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_REWARDS) subtitle: @""]; //lokalise 31 jan
    }
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
//    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_TITLE_REWARDS),  LOCALIZATION(C_REWARDS_BACK)];
    
    [self setupInterface];
    
    self.lblCurrentPoints.text = [NSString stringWithFormat: @"%.1f", [mSession rewardsCurrentPoints]];
    
    [[WebServiceManager sharedInstance] fetchPointsSummary: self];

    if (kIsRightToLeft) {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_REWARDS) screenClass:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
        if (GET_REWARDSCART)
        {
            NSArray *savedArray = GET_REWARDSCART;
            self.navigationItem.rightBarButtonItem.badgeValue = @(savedArray.count).stringValue;
            self.navigationItem.rightBarButtonItem.badgeOriginX = 15; //default value as of RewardsView
        }
}

- (IBAction)cartPressed:(UIButton *)sender {
    
    //push to cart view
    [mSession pushRewardsCart: self];
}

- (IBAction)historyPressed:(id)sender {
    [mSession pushRewardsHistory: self];
}

- (IBAction)favouritesPressed:(UIButton *)sender {
    
    for (UIViewController *vc in [self.navigationController childViewControllers])
    {
        if ([vc isKindOfClass: [RewardsFavouritesViewController class]])
        {
            //Only Main page and Favourites can come into Item details.
            [self popSelf];
            return;
        }
    }
    
    //push to fav view
    [mSession pushRewardsFavourites: self];
}

- (IBAction)addToCartPressed:(id)sender {

    double pointsNeeded = 0.0f;
    if ([[self.itemDetailsData objectForKey: @"DiscountedPoint"] doubleValue] > 0)
    {
        pointsNeeded = [[self.itemDetailsData objectForKey: @"DiscountedPoint"] doubleValue];
    }
    else
    {
        pointsNeeded = [[self.itemDetailsData objectForKey: @"ProductPoint"] doubleValue];
    }
    
    if (pointsNeeded > [self.lblCurrentPoints.text doubleValue])
    {
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_REWARDS_INSUFFICIENTPOPUP)];  //lokalise 31 jan
        return;
    }
    
    
    NSMutableArray *cartArray = [[NSMutableArray alloc] init];
    
    if (GET_REWARDSCART)
    {
        cartArray = [[NSMutableArray alloc] initWithArray: GET_REWARDSCART];
        
        NSInteger totalItemsCount = 0;
        CGFloat pointsRequired = 0;
        for (NSDictionary *dict in cartArray)
        {
            totalItemsCount = totalItemsCount + [[dict objectForKey: @"Quantity"] doubleValue];
            pointsRequired = pointsRequired + ([[dict objectForKey: @"ProductPoint"] doubleValue] * [[dict objectForKey: @"Quantity"] intValue]);
        }
        
        for (NSDictionary *existingDict in cartArray)
        {
            if (pointsRequired + [[self.itemDetailsData objectForKey: @"ProductPoint"] doubleValue] > [self.lblCurrentPoints.text doubleValue])
            {
                [mAlert showErrorAlertWithMessage: LOCALIZATION(C_REWARDS_INSUFFICIENTPOPUP)];      //lokalised 31 jan
                return;
            }
            
            NSInteger itemCode = [[existingDict objectForKey: @"ItemCode"] intValue];
            if ([[self.itemDetailsData objectForKey: @"ItemCode"] intValue] == itemCode)
            {
                //+1
                NSDictionary *dict = @{@"CategoryCode": [self.itemDetailsData objectForKey: @"CategoryCode"],
                                       @"ItemCode": [self.itemDetailsData objectForKey: @"ItemCode"],
                                       @"ProductName": [self.itemDetailsData objectForKey: @"ProductName"],
                                       @"ImageTH": [self.itemDetailsData objectForKey: @"ImageTH"],
                                       @"ProductPoint": [self.itemDetailsData objectForKey: @"ProductPoint"],
                                       @"DiscountedPoint": [self.itemDetailsData objectForKey: @"DiscountedPoint"],
        //                           @"ProductDescription": [self.itemDetailsData objectForKey: @"ProductDescription"],
                                       @"Quantity": @([[existingDict objectForKey: @"Quantity"] intValue] + 1).stringValue,
                                       };
                [cartArray replaceObjectAtIndex: [cartArray indexOfObject: existingDict] withObject:dict];
                SET_REWARDSCART(cartArray);
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
                    self.navigationItem.rightBarButtonItem.badgeValue = @(cartArray.count).stringValue;
                
                
                [mAlert showSuccessAlertWithMessage: LOCALIZATION(C_REWARDS_ADDEDTOCART) onCompletion:^(BOOL finished) {
                    
                }];         //lokalise 31 jan
                return;
            }
        }
    }
    NSDictionary *dict = @{@"CategoryCode": [self.itemDetailsData objectForKey: @"CategoryCode"],
                           @"ItemCode": [self.itemDetailsData objectForKey: @"ItemCode"],
                           @"ProductName": [self.itemDetailsData objectForKey: @"ProductName"],
                           @"ImageTH": [self.itemDetailsData objectForKey: @"ImageTH"],
                           @"ProductPoint": [self.itemDetailsData objectForKey: @"ProductPoint"],
                           @"DiscountedPoint": [self.itemDetailsData objectForKey: @"DiscountedPoint"],
                           //                           @"ProductDescription": [self.itemDetailsData objectForKey: @"ProductDescription"],
                           @"Quantity": @"1",
                           };
    [cartArray addObject: dict];
    
    SET_REWARDSCART(cartArray);
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
        self.navigationItem.rightBarButtonItem.badgeValue = @(cartArray.count).stringValue;
    
    
    [mAlert showSuccessAlertWithMessage: LOCALIZATION(C_REWARDS_ADDEDTOCART) onCompletion:^(BOOL finished) {
        
    }];  //lokalise 31 jan
}

- (IBAction)addToFavouritesPressed:(id)sender {
    [[WebServiceManager sharedInstance] addWishItem: [self.itemData objectForKey: @"CategoryCode"]
                                           itemCode:[self.itemData objectForKey: @"ItemCode"]
                                                 vc:self];
}


- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

-(void) setupInterface
{
    
#warning to be configured
    self.lblCurrentPointsHeader.text = LOCALIZATION(C_REWARDS_CURRENTPOINTS);        //lokalise 31 jan
    self.lblCurrentPointsHeader.font = FONT_B3;
    
    self.lblCurrentPoints.font = FONT_H(25);
    self.lblCurrentPoints.textColor = COLOUR_RED;
    
    self.lblCurrentPointsSuffix.text = LOCALIZATION(C_REWARDS_POINTS);       //lokalise 31 jan
    self.lblCurrentPointsSuffix.font = self.lblCurrentPointsHeader.font;
    self.lblCurrentPointsSuffix.textColor = self.lblCurrentPoints.textColor;
    
//    self.lblHistory.text = LOCALIZATION(C_REWARDS_CARTBTN);
    self.lblHistory.text = LOCALIZATION(C_REWARDS_HISTORY);  //lokalise 31 jan
    self.lblHistory.font = FONT_B3;
    
    self.lblFavourites.text = LOCALIZATION(C_REWARDS_FAVOURITESBTN);     //lokalise 31 jan
    self.lblFavourites.font = self.lblHistory.font;
    
    
    self.lblName.font = FONT_H(18);
    self.lblDescription.font = FONT_B1;
    
    self.lblPoints.font = FONT_H(30);
    self.lblPoints.textColor = COLOUR_RED;
    
    self.lblPointsRequired.text = LOCALIZATION(C_REWARDS_POINTSREQUIRED);        //lokalise 31 jan
    self.lblPointsRequired.font = FONT_B2;
    self.lblPointsSuffix.font = self.lblPointsRequired.font;
    self.lblPointsSuffix.text = LOCALIZATION(C_REWARDS_POINTS);             //lokalise 31 jan
    self.lblPointsSuffix.textColor = COLOUR_RED;
 
    
    [Helper setCustomFontButtonContentModes: self.btnAddCart];
    [self.btnAddCart.titleLabel setFont: FONT_BUTTON];
    [self.btnAddCart setTitle: LOCALIZATION(C_REWARDS_ADDTOCART) forState:UIControlStateNormal];        //lokalise 31 jan
    [self.btnAddCart setBackgroundColor:COLOUR_RED];
    if(!self.canRedeem) {
        [self.btnAddCart setAlpha:0.5];
        [self.btnAddCart setUserInteractionEnabled:NO];
    } else {
        [self.btnAddCart setAlpha:1.0];
        [self.btnAddCart setUserInteractionEnabled:YES];
    }
    
//    [Helper setCustomFontButtonContentModes: self.btnAddFavourites];
    self.lblAddFavourite.text = LOCALIZATION(C_REWARDS_ADDTOFAVOURITES);     //lokalise 31 jan
    self.lblAddFavourite.font = FONT_B1;
    self.lblAddFavourite.textColor = COLOUR_VERYDARKGREY
    ;
//    [self.btnAddFavourites.titleLabel setFont: FONT_BUTTON];
//    [self.btnAddFavourites setTitle: LOCALIZATION(C_REWARDS_ADDTOFAVOURITES) forState:UIControlStateNormal];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_REWARDSPOINTSSUMMARY:
            [mSession setRewardsCurrentPoints: [[[response getGenericResponse] objectForKey: @"BalancePoints"] doubleValue]];
            self.lblCurrentPoints.text = [NSString stringWithFormat: @"%.1f", [mSession rewardsCurrentPoints]];
            
            [[WebServiceManager sharedInstance] fetchRewardsDetails: [self.itemData objectForKey: @"CategoryCode"]
                                                           itemCode:[self.itemData objectForKey: @"ItemCode"]
                                                                 vc:self];
            break;
        case kWEBSERVICE_REWARDSFETCHDETAILS:
        {
            self.itemDetailsData = [response getGenericResponse];
            
            self.lblName.text = [self.itemDetailsData objectForKey: @"ProductName"];
            self.lblDescription.text = [self.itemDetailsData objectForKey: @"ProductDescription"];
            
            NSString *urlString = [[[self.itemDetailsData objectForKey: @"ImagePI"] objectAtIndex: 0] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            [self.imgItem sd_setImageWithURL: [NSURL URLWithString: urlString] placeholderImage: [UIImage imageNamed: @"placeholder_giftbox"] options: (SDWebImageRetryFailed | SDWebImageRefreshCached)];
            
            if ([[self.itemDetailsData objectForKey: @"DiscountedPoint"] doubleValue] > 0)
            {
                self.lblPoints.text = [[self.itemDetailsData objectForKey: @"DiscountedPoint"] stringValue];
                self.lblOriginalPoints.text = [NSString stringWithFormat: @"%@%@", LOCALIZATION(C_REWARDS_UP),   [[self.itemDetailsData objectForKey: @"ProductPoint"] stringValue]];  //lokalised 11 Feb
                self.lblOriginalPoints.hidden = NO;
                
                self.lblOriginalPoints.font = FONT_B2;
                self.lblOriginalPoints.textColor = COLOUR_VERYDARKGREY;
                
                //            lblName.text = LOCALIZATION(@"PROMOTION");
                self.lblPromotion.font = FONT_H2;
                self.lblPromotion.textColor = COLOUR_RED;
                self.lblPromotion.hidden = NO;
            }
            else
            {
                self.lblPoints.text = [[self.itemDetailsData objectForKey: @"ProductPoint"] stringValue];
            }
            
        }
            break;
        case kWEBSERVICE_REWARDSADDWISHITEM:
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
            }];
            break;
        default:
            break;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
