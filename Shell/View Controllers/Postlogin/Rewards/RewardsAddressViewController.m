//
//  RewardsAddressViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 13/10/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "RewardsAddressViewController.h"

#define CELL_HEIGHT 72

enum kRewardsCheckoutForm_TYPE
{
    kRewardsCheckoutForm_NAME,
    kRewardsCheckoutForm_ADDRESS,
    kRewardsCheckoutForm_TOWN,
    kRewardsCheckoutForm_STATE,
    kRewardsCheckoutForm_POSTALCODE,
    kRewardsCheckoutForm_MOBNUM,
};

#define kFirstName        @"FirstName"
#define kLastName         @"LastName"
#define kEmailAddress     @"EmailAddress"
#define kAddress1         @"Address1"
#define kAddress2         @"Address2"
#define kTown             @"Town"
#define kState            @"State"
#define kPostalCode       @"PostalCode"
#define kMobileNumber     @"MobileNumber"



@interface RewardsAddressViewController ()<UITextFieldDelegate, WebServiceManagerDelegate, CommonListDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;


@property NSMutableArray *registrationFormArray; //ui only
@property NSMutableDictionary *regSubmissionDict;  //key as json key, value = cell. retrieve value by using getJson method
@property (weak, nonatomic) IBOutlet UITableView *regFormTableView;
@property NSArray *stateTable;
@property NSDictionary *workshopDetailsData;
@property Member *member;
@property CompanyInfo *companyInfo;
@property CompanyAddress *companyAddress;
@end

@implementation RewardsAddressViewController
@synthesize listView; //must synthesize list view constraints to work

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_REWARDS_ADDRESSTITLE) subtitle: @""];       //lokalise 31 Jan
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
//    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_REWARDS_ADDRESSTITLE),  LOCALIZATION(C_REWARDS_BACK)];
    
    if(@available(iOS 11, *))
    {
        self.regFormTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.regFormTableView setContentInset:UIEdgeInsetsMake(15, 0, 15, 0)];

    [[WebServiceManager sharedInstance] loadState: [[mSession lookupTable] objectForKey: @"LookUpVersion"]
                                      countryCode: GET_COUNTRY_CODE
                                               vc: self];
    if (kIsRightToLeft) {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
    
    [self.btnSubmit setBackgroundColor:COLOUR_RED];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_REWARDS_ADDRESSTITLE) screenClass:nil];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return [self.registrationFormArray count] + 1; //+1 for deliver address button
    return [self.registrationFormArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.row == [self.registrationFormArray count])
//        return 60;
//    else
        return CELL_HEIGHT;
}

-(void) setupRegistrationForm
{
    [Helper setCustomFontButtonContentModes: self.btnSubmit];
    [self.btnSubmit setTitle: LOCALIZATION(C_REWARDS_DELIVERTOADDRESS) forState:UIControlStateNormal];        //lokalise 31 Jan
    [self.btnSubmit.titleLabel setFont: FONT_BUTTON];
    [self.btnSubmit setBackgroundColor:COLOUR_RED];

    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_DROPDOWN_STATE)
                                                   list: [mSession getStateNames:self.stateTable]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];       //lokalise 31 Jan
    listView.delegate = self;
    
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormTextfieldCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormTextfieldCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormDropdownCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormDropdownCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationMobileNumberCell" bundle:nil] forCellReuseIdentifier:@"RegistrationMobileNumberCell"];
    
    self.regSubmissionDict = [[NSMutableDictionary alloc] init];
    
    
    self.registrationFormArray = [[NSMutableArray alloc] initWithArray:
                                  @[
//                                    @{@"Title": LOCALIZATION(C_PROFILE_SALUTATION),
//                                      @"Type": @(kREGISTRATION_DROPDOWN),
//                                      @"Key": kSalutation,
//                                      @"Value": LOCALIZATION(C_PROFILE_SALUTATION_PLACEHOLDER),
                                    //                                      },  //button
                                    @{@"Title": LOCALIZATION(C_PROFILE_FIRSTNAME),       //lokalise 31 Jan
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kFirstName,
                                      @"Value": [[mSession profileInfo] firstName],
                                      @"Enabled": @([[mSession profileInfo] canEditShippingAddress]),
                                      }, //name
                                    @{@"Title": LOCALIZATION(C_PROFILE_LASTNAME),         //lokalise 31 Jan
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kLastName,
                                      @"Value": [[mSession profileInfo] lastName],
                                      @"Enabled": @([[mSession profileInfo] canEditShippingAddress]),
                                      }, //name
//                                    @{@"Title": LOCALIZATION(C_PROFILE_EMAILADDRESS),
//                                      @"Type": @(kREGISTRATION_TEXTFIELD),
//                                      @"Key": kEmailAddress,
//                                      @"Keyboard": @(UIKeyboardTypeEmailAddress),
//                                      @"Value": [[mSession profileInfo] emailAddress],
//                                      @"Enabled": @([[mSession profileInfo] canEditShippingAddress]),
//                                      }, //email
                                    @{@"Title": LOCALIZATION(C_PROFILE_ADDRESSLINE1),         //lokalise 31 Jan
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kAddress1,
                                      @"Value": self.companyAddress.address1,
                                      @"Enabled": [[mSession profileInfo] canEditShippingAddress] ? @([[mSession profileInfo] canEditAddress]) : @(NO), //check for edit address flag if shipping address is allowed to be edit
                                      @"Enabled": @([[mSession profileInfo] canEditAddress]),
                                      }, //address line 1
                                    @{@"Title": LOCALIZATION(C_PROFILE_ADDRESSLINE2),       //lokalise 31 Jan
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kAddress2,
                                      @"Value": self.companyAddress.address2,
                                      @"Enabled": [[mSession profileInfo] canEditShippingAddress] ? @([[mSession profileInfo] canEditAddress]) : @(NO), //check for edit address flag if shipping address is allowed to be edit
                                      }, //address line 2
                                    @{@"Title": LOCALIZATION(C_PROFILE_TOWN),       //lokalise 31 Jan
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kTown,
                                      @"Value": self.companyAddress.companyCity,
                                      @"Enabled": [[mSession profileInfo] canEditShippingAddress] ? @([[mSession profileInfo] canEditAddress]) : @(NO), //check for edit address flag if shipping address is allowed to be edit
                                      }, //town
                                    @{@"Title": LOCALIZATION(C_PROFILE_STATE),       //lokalise 31 Jan
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key": kState,
                                      @"Value": ([[mSession convertToStateName: self.companyAddress.companyState stateDetailedListArray: self.stateTable] isEqualToString: BLANK]) ?  LOCALIZATION(C_PROFILE_STATE_PLACEHOLDER) : [mSession convertToStateName: self.companyAddress.companyState stateDetailedListArray: self.stateTable],        //lokalise 31 Jan
                                      @"Enabled": [[mSession profileInfo] canEditShippingAddress] ? @([[mSession profileInfo] canEditAddress]) : @(NO), //check for edit address flag if shipping address is allowed to be edit
                                      },
                                    @{@"Title": LOCALIZATION(C_PROFILE_POSTALCODE),       //lokalise 31 Jan
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kPostalCode,
                                      @"Keyboard": @(UIKeyboardTypeNumberPad),
                                      @"Value": self.companyAddress.postalCode,
                                      @"Enabled": [[mSession profileInfo] canEditShippingAddress] ? @([[mSession profileInfo] canEditAddress]) : @(NO), //check for edit address flag if shipping address is allowed to be edit
                                      },
                                    @{@"Title": LOCALIZATION(C_PROFILE_MOBILENUM),       //lokalise 31 Jan
                                      @"Type": @(kREGISTRATION_MOBNUM),
                                      @"Key": kMobileNumber,
                                      @"CountryCode": [mSession convertCountryCodeToCountryName: GET_COUNTRY_CODE],
                                      @"MobileNumber": [[mSession profileInfo] mobileNumber],
                                      @"Enabled": @([[mSession profileInfo] canEditShippingAddress]),
                                      },   //Mobile number
                                    ]
                                  ];
    
    //set tableview height.
//    self.tableViewHeight.constant = CELL_HEIGHT * [self.registrationFormArray count];
    self.regFormTableView.tableFooterView = [UIView new];
    [self.regFormTableView reloadData];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    if (indexPath.row == [self.registrationFormArray count])
//    {
//        UITableViewCell *cell = [self.regFormTableView dequeueReusableCellWithIdentifier: @"DeliverCell"];
//        //button is after last field of reg field
//        UIButton *btnSubmit = [cell viewWithTag: 1];
//        [Helper setCustomFontButtonContentModes: btnSubmit];
//        [btnSubmit setTitle: LOCALIZATION(C_REWARDS_DELIVERTOADDRESS) forState:UIControlStateNormal];
//        [btnSubmit.titleLabel setFont: FONT_BUTTON];
//
//        return cell;
//    }
//    else
    {
        NSDictionary *regFieldData = [self.registrationFormArray objectAtIndex: indexPath.row];
        switch ([[regFieldData objectForKey: @"Type"] intValue])
        {
            case kREGISTRATION_TEXTFIELD:
            {
                RegistrationFormTextfieldCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormTextfieldCell"];
                
                if ([regFieldData objectForKey: @"Keyboard"])
                    [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
                if ([regFieldData objectForKey: @"SecureEntry"])
                    [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
                if ([regFieldData objectForKey: @"Enabled"])
                    [textfieldCell enableCell: [[regFieldData objectForKey: @"Enabled"] boolValue]];
                
                [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
                [textfieldCell hideTopLabel: NO];
                if ([regFieldData objectForKey: @"Value"])
                    [textfieldCell setTextfieldText: [regFieldData objectForKey: @"Value"]];
                
                [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
                return textfieldCell;
            }
                break;
            case kREGISTRATION_DROPDOWN:
            {
                RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
                if ([regFieldData objectForKey: @"Enabled"])
                    [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
                
                [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
                [dropdownCell hideTopLabel: NO];
                dropdownCell.button.tag = indexPath.row;
                
                [dropdownCell.button addTarget: self action: @selector(dropdownPressed:) forControlEvents:UIControlEventTouchUpInside];

                //set initial value
                if ([regFieldData objectForKey: @"Value"])
                    [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
                
                //keep a reference of cell to submit json value later
                [self.regSubmissionDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
                return dropdownCell;
            }
                break;
            case kREGISTRATION_MOBNUM:
            {
                RegistrationMobileNumberCell *mobNumCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationMobileNumberCell"];
                if ([regFieldData objectForKey: @"Enabled"])
                    [mobNumCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
                [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
                [mobNumCell hideTopLabel: NO];
                [mobNumCell setMobileCodeEnabled: NO];
                [mobNumCell setTextfieldDelegate:self];
                [mobNumCell setTextFieldTag:indexPath.row];
                [mobNumCell setCountryCode: [regFieldData objectForKey: @"CountryCode"]];
                [mobNumCell setMobileNumber: [regFieldData objectForKey: @"MobileNumber"]];
                
                [self.regSubmissionDict setObject: mobNumCell forKey: [regFieldData objectForKey: @"Key"]];
                return mobNumCell;
            }
                break;
        }
    }
    
    return nil;
    
}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

-(void) dropdownPressed:(UIButton *)sender
{
    [self.view endEditing: YES];
    listView.tag = sender.tag;
    
    switch (listView.tag)
    {
        case kRewardsCheckoutForm_STATE:
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_STATE) list: [mSession getStateNames:self.stateTable] hasSearchField: YES];        //lokalise 31 Jan
            break;
        default:
            break;
    }
    [self popUpView];
}
-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        RegistrationFormDropdownCell *cell = [self.regSubmissionDict objectForKey: kState];
        
        [cell.button setTitle:selection.firstObject forState:UIControlStateNormal];
    }
}

- (IBAction)submitPressed:(id)sender {
    NSString *stateString = [[self.regSubmissionDict objectForKey: kState] getJsonValue];
    ;
    
    NSMutableDictionary *addressData = [[NSMutableDictionary alloc] init];
    
    [addressData setObject: [[self.regSubmissionDict objectForKey: kFirstName] getJsonValue] forKey:kFirstName];
    [addressData setObject: [[self.regSubmissionDict objectForKey: kLastName] getJsonValue] forKey:kLastName];
    [addressData setObject: [[self.regSubmissionDict objectForKey: kAddress1] getJsonValue] forKey:kAddress1];
    [addressData setObject: [[self.regSubmissionDict objectForKey: kAddress2] getJsonValue] forKey:kAddress2];
    
    [addressData setObject: [[self.regSubmissionDict objectForKey: kTown] getJsonValue] forKey:kTown];
    [addressData setObject: self.member.emailAddress forKey:kEmailAddress];
    
    [addressData setObject: [mSession convertToStateKeyCode: stateString stateDetailedListArray:self.stateTable]  forKey:kState];
    
    [addressData setObject: [[self.regSubmissionDict objectForKey: kPostalCode] getJsonValue] forKey:kPostalCode];
    
    if ([self.regSubmissionDict objectForKey: kMobileNumber])
        [addressData setObject: [[self.regSubmissionDict objectForKey: kMobileNumber] getMobileNumberJsonValue] forKey:kMobileNumber];
    else
        [addressData setObject: [[mSession profileInfo] mobileNumber] forKey:kMobileNumber];
    
    NSString *stateNameString = [mSession convertToStateName: [addressData objectForKey: kState] stateDetailedListArray: self.stateTable];
    [addressData setObject: stateNameString forKey: @"StateName"];

//                                  kEmailAddress : [[self.regSubmissionDict objectForKey: kEmailAddress] getJsonValue],
 
    
    
    [mSession pushRewardsConfirmation: self addressData: addressData];
}


- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
        {
            SET_LOADSTATE([[response getGenericResponse] objectForKey: @"StateDetailsList"]);
            self.stateTable = [[response getGenericResponse] objectForKey: @"StateDetailsList"];
            
            LoginInfo *loginInfo = [mSession loadUserProfile];
            [[WebServiceManager sharedInstance] fetchTrade: loginInfo.tradeID
                                                        vc:self];
        }
            break;
        case kWEBSERVICE_FETCHTRADE:
            self.workshopDetailsData = [response getGenericResponse];
            
            self.member = [[Member alloc] initWithData:[self.workshopDetailsData objectForKey: @"REG_Member"]];
            self.companyInfo = [[CompanyInfo alloc] initWithData: [self.workshopDetailsData objectForKey: @"REG_Company"]];
            self.companyAddress = [[CompanyAddress alloc] initWithData: [self.workshopDetailsData objectForKey: @"REG_CompanyAddress"]];
            
            [self setupRegistrationForm];
            
            break;
        default:
            break;
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress: KEY_WINDOW];
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
        {
            self.stateTable = GET_LOADSTATE;
            
            LoginInfo *loginInfo = [mSession loadUserProfile];
            [[WebServiceManager sharedInstance] fetchTrade: loginInfo.tradeID
                                                        vc:self];
        }
            break;
        default:
            break;
    }
}

#pragma mark - TextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    switch(textField.tag) {
        case 7:
        {
            if(IS_COUNTRY(COUNTRYCODE_INDONESIA)) {
                if([textField.text isEqualToString:@""] && [string isEqualToString:@"0"]) {
                    [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_MOBILENUMBER_VALIDATION)];
                    return NO;
                } else {
                    NSString *replacedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
                    if(![replacedText isEqualToString:@""] && [[replacedText substringToIndex:1] isEqualToString:@"0"]) {
                        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_MOBILENUMBER_VALIDATION)];
                        return NO;
                    } else {
                        return YES;
                    }
                }
            }
        }
            break;
        default:
            break;
    }
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
