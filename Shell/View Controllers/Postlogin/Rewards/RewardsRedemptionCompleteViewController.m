//
//  RewardsRedemptionCompleteViewController.m
//  Shell
//
//  Created by Jeremy Lua on 10/7/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RewardsRedemptionCompleteViewController.h"

@interface RewardsRedemptionCompleteViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblThankyouHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblConfirmMsg;
@property (weak, nonatomic) IBOutlet UIButton *btnViewHistory;
@end

@implementation RewardsRedemptionCompleteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_REWARDS_CONFIRMATIONTITLE) subtitle: @""];   //lokalise 31 jan
    
    NSDictionary *responseMessageDict = [self.msgDictionary objectForKey: @"ResponseMessage"];
//    self.lblThankyouHeader.text = [responseMessageDict objectForKey: @"Header"];
    self.lblThankyouHeader.text = LOCALIZATION(C_REWARDS_THANKYOU);   //lokalise 31 jan
    self.lblThankyouHeader.font = FONT_H1;
    self.lblThankyouHeader.textColor = COLOUR_RED;
    
    
//    NSMutableAttributedString *contentAttrString = [[NSMutableAttributedString alloc] initWithString: @"Your Redemption ID is " attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
    NSMutableAttributedString *contentAttrString = [[NSMutableAttributedString alloc] initWithString: LOCALIZATION(C_REWARDS_REDEMPTIONID) attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];    //lokalise 31 jan
    
    NSString *redemptionIdString = [NSString stringWithFormat: @"\n%@", [self.msgDictionary objectForKey: @"Data"]];
    
    NSAttributedString *redemptionIdAttrString = [[NSAttributedString alloc] initWithString: redemptionIdString attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_H1, }];
    
    NSString *thankYouString = [NSString stringWithFormat: @"\n\n%@", [responseMessageDict objectForKey: @"Message"]];
    
    NSAttributedString *thankYouAttrString = [[NSAttributedString alloc] initWithString: thankYouString attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
    
    [contentAttrString appendAttributedString:  redemptionIdAttrString];
    [contentAttrString appendAttributedString:  thankYouAttrString];
    
//    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
    self.lblConfirmMsg.attributedText = contentAttrString;
    
    [self.btnViewHistory setTitle: LOCALIZATION(C_REWARDS_VIEWHISTORY) forState:UIControlStateNormal];    //lokalise 31 jan
    self.btnViewHistory.titleLabel.font = FONT_BUTTON;
    [self.btnViewHistory setBackgroundColor:COLOUR_RED];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics  setScreenName:LOCALIZATION_EN(C_REWARDS_CONFIRMATIONTITLE) screenClass:nil];
}

- (IBAction)viewHistoryPressed:(id)sender {
//    [mSession loadRewardsView];
    [mSession loadRewardsHistoryFromCompletion];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
