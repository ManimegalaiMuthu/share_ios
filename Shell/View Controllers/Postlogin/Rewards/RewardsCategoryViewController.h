//
//  RewardsCategoryViewController
//  Shell
//
//  Created by Jeremy Lua on 5/7/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface RewardsCategoryViewController : BaseVC

@property NSArray *mainCategoryArray;
@property NSUInteger selectedIndex;
@property BOOL canRedeem;
@end
