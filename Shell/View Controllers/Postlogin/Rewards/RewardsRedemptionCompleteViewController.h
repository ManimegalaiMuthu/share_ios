//
//  RewardsRedemptionCompleteViewController.h
//  Shell
//
//  Created by Jeremy Lua on 10/7/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface RewardsRedemptionCompleteViewController : BaseVC
@property NSDictionary *msgDictionary;
@end
