//
//  RewardsFavouritesViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 13/10/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "RewardsFavouritesViewController.h"
#import "RewardsItemDetailsViewController.h"
#import "UIBarButtonItem+Badge.h"

@interface RewardsFavouritesViewController () <WebServiceManagerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, CommonListDelegate>

enum kFilterCellTag_TYPE
{
    kFilterCellTag_SORTBY = 1,
    kFilterCellTag_FILTERBYPOINTS,
};

enum kRewardCellTag_TYPE
{
    kRewardCellTag_IMAGEVIEW = 1,
    kRewardCellTag_NAME,
    //    kRewardCellTag_DESCRIPTION,
    //    kRewardCellTag_POINTSREQUIRED,
    kRewardCellTag_POINTS = 5,
    kRewardCellTag_POINTSSUFFIX,
    kRewardCellTag_DELETE,
    
    kRewardCellTag_ORIGINALPRICE = 7,
    kRewardCellTag_PROMOTION,
    
    kRewardCellTag_Line = 999,
};


#define CELL_WIDTH (SCREEN_WIDTH - 48) / 2

@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UILabel *lblCurrentPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentPointsData;
@property (weak, nonatomic) IBOutlet UILabel *lblPointsSuffix;

@property (weak, nonatomic) IBOutlet UIButton *btnCart;

@property (weak, nonatomic) IBOutlet UIButton *btnFavourites;
@property (weak, nonatomic) IBOutlet UILabel *lblFavourites;

@property NSMutableArray *itemsArray;

@property (weak, nonatomic) IBOutlet UICollectionView *colView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *colViewLayout;

//V2

@property (weak, nonatomic) IBOutlet UILabel *lblHistory;
@property (weak, nonatomic) IBOutlet UIButton *btnHistory;

@end

@implementation RewardsFavouritesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_REWARDS_FAVOURITESTITLE) subtitle: @""];    //lokalise 31 jan
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
//    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_REWARDS_FAVOURITESTITLE),  LOCALIZATION(C_REWARDS_BACK)];
    
    self.lblCurrentPointsData.text = [NSString stringWithFormat: @"%.1f", [mSession rewardsCurrentPoints]];
    self.lblCurrentPointsData.textColor = COLOUR_RED;
    
    UIImage *favImage = GET_ISADVANCE ? [UIImage imageNamed:@"icn_favourite_blue"] : [UIImage imageNamed:@"favourite-red"];
    [self.btnFavourites setImage:favImage forState: UIControlStateNormal];
    self.lblFavourites.textColor = COLOUR_RED;
    
    if (kIsRightToLeft) {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
}

//-(void)viewWillAppear:(BOOL)animated{
//    [super viewWillAppear: animated];
//}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
        if(@available(iOS 11, *)){
            [self.btnCart.widthAnchor constraintEqualToConstant: 25.0].active = YES;
            [self.btnCart.heightAnchor constraintEqualToConstant: 25.0].active = YES;
        }
        UIBarButtonItem *cartIcon = [[UIBarButtonItem alloc] initWithCustomView: self.btnCart];
        self.navigationItem.rightBarButtonItem = cartIcon;
        
        if (GET_REWARDSCART)
        {
            NSArray *savedArray = GET_REWARDSCART;
            self.navigationItem.rightBarButtonItem.badgeValue = @(savedArray.count).stringValue;
            self.navigationItem.rightBarButtonItem.badgeOriginX = 15; //default value as of RewardsView
        }
}

-(void) setupGoogleAnalytics
{
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_REWARDS_FAVOURITESTITLE) screenClass:nil];
}

- (IBAction)deleteFavouriteItem:(UIButton *)sender
{
    NSDictionary *itemDetailDict = [self.itemsArray objectAtIndex: sender.superview.superview.tag - 100];

   //delete favourite entry API - reload @ completed
    [[WebServiceManager sharedInstance] removeWishItem:[itemDetailDict objectForKey: @"WishID"] vc:self];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.itemsArray count];
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CELL_WIDTH, CELL_WIDTH + 135);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *itemDetailDict = [self.itemsArray objectAtIndex: indexPath.row];
    
    //push to item details view
    [mSession pushRewardsItemDetailsView: itemDetailDict vc:self];
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"RewardCell" forIndexPath:indexPath];
    
    NSDictionary *itemDetailDict = [self.itemsArray objectAtIndex: indexPath.row];
    
    cell.tag = indexPath.row + 100; //+100 to avoid referencing following views
    
    UIImageView *cellImgView = [cell viewWithTag: kRewardCellTag_IMAGEVIEW];
    
    NSString *urlString = [[itemDetailDict objectForKey: @"ImageTH"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [cellImgView sd_setImageWithURL: [NSURL URLWithString: urlString] placeholderImage: [UIImage imageNamed: @"placeholder_giftbox_thumb"] options: (SDWebImageRefreshCached  | SDWebImageRetryFailed)];
    
    UILabel *lblName = [cell viewWithTag: kRewardCellTag_NAME];
    lblName.text = [itemDetailDict objectForKey: @"ProductName"];
    lblName.font = FONT_H2;
    lblName.textColor = COLOUR_VERYDARKGREY;
    
//    UILabel *lblDescription = [cell viewWithTag: kRewardCellTag_DESCRIPTION];
//    lblDescription.text = [itemDetailDict objectForKey: @"ShortDescription"];
//    lblDescription.font = FONT_B3;
//    lblDescription.textColor = COLOUR_MIDGREY;
//
//    UILabel *lblPointsRequired = [cell viewWithTag: kRewardCellTag_POINTSREQUIRED];
//    lblPointsRequired.text = [LOCALIZATION(C_REWARDS_POINTSREQUIRED) uppercaseString];
//    lblPointsRequired.font = FONT_H3;
    
    UILabel *lblPointsSuffix = [cell viewWithTag: kRewardCellTag_POINTSSUFFIX];
    lblPointsSuffix.text = LOCALIZATION(C_REWARDS_POINTS);    //lokalise 31 jan
    lblPointsSuffix.font = FONT_B3;
    lblPointsSuffix.textColor = COLOUR_VERYDARKGREY;
    
//    UILabel *lblPoints =  [cell viewWithTag: kRewardCellTag_POINTS];
//    lblPoints.text = [[itemDetailDict objectForKey: @"ProductPoint"] stringValue];
//    lblPoints.font = FONT_H1;
    
    UILabel *lblPoints =  [cell viewWithTag: kRewardCellTag_POINTS];
    lblPoints.textColor = COLOUR_RED;
    lblPoints.font = FONT_H1;
    
    UILabel *lblOriginalPrice = [cell viewWithTag: kRewardCellTag_ORIGINALPRICE];
    lblOriginalPrice.font = FONT_H2;
    lblOriginalPrice.textColor = COLOUR_VERYDARKGREY;
    
    UILabel *lblPromotion = [cell viewWithTag: kRewardCellTag_PROMOTION];
    
    if ([[itemDetailDict objectForKey: @"DiscountPoint"] doubleValue] > 0)
    {
        lblPoints.text = [[itemDetailDict objectForKey: @"DiscountPoint"] stringValue];
        lblOriginalPrice.text = [NSString stringWithFormat: @"%@%@", LOCALIZATION(C_REWARDS_UP),   [[itemDetailDict objectForKey: @"ProductPoint"] stringValue]];  //lokalised 11 Feb
        lblOriginalPrice.hidden = NO;
        
        UILabel *lblPromotion = [cell viewWithTag: kRewardCellTag_PROMOTION];
        //            lblName.text = LOCALIZATION(@"PROMOTION");
        lblPromotion.font = FONT_H2;
        lblPromotion.textColor = COLOUR_RED;
        lblPromotion.hidden = NO;
    }
    else
    {
        lblPoints.text = [[itemDetailDict objectForKey: @"ProductPoint"] stringValue];
        lblOriginalPrice.hidden = YES;
        
        lblPromotion.hidden = YES;
    }
    
    UIView *lineView = [cell viewWithTag:kRewardCellTag_Line];
    [lineView setBackgroundColor:COLOUR_RED];
    
    return cell;
}

- (IBAction)cartPressed:(id)sender {
    //push to cart view
    [mSession pushRewardsCart: self];
    
}

- (IBAction)historyPressed:(id)sender {
    [mSession pushRewardsHistory: self];
}


- (IBAction)backPressed:(id)sender {
    
    [self popToIndex: 0];
//    for (UIViewController *vc in [self.navigationController childViewControllers])
//    {
//        if ([vc isKindOfClass: [RewardsItemDetailsViewController class]])
//        {;
//            return;
//        }
//    }
//    [self popSelf];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_REWARDSFETCHWISHLIST:
            self.itemsArray = [[response getGenericResponse] objectForKey: @"RewardsWishList"];
            [self.colView reloadData];
            break;
        case kWEBSERVICE_REWARDSPOINTSSUMMARY:
            [mSession setRewardsCurrentPoints: [[[response getGenericResponse] objectForKey: @"BalancePoints"] doubleValue]];
            self.lblCurrentPointsData.text = [NSString stringWithFormat: @"%.1f", [mSession rewardsCurrentPoints]];
            
            [[WebServiceManager sharedInstance] fetchWishList:self];
            break;
        case kWEBSERVICE_REWARDSREMOVEWISHITEM:
            
            [[WebServiceManager sharedInstance] fetchWishList:self];
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
