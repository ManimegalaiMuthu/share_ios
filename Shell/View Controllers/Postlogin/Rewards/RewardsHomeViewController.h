//
//  RewardsHomeViewController.h
//  Shell
//
//  Created by Jeremy Lua on 5/7/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@protocol RewardsHomeViewDelegate<NSObject>
-(void)rewardsShouldUpdateUIWithCanRedeemFlag:(BOOL)canRedeem showWalkthrough:(BOOL) showWalkthrough;
-(void)didPressedCategory:(NSMutableArray *) mainCategoryArray selectedIndex:(NSUInteger)selectedIndex;
-(void)didPressedFeaturedReward:(id)sender;
@end

@interface RewardsHomeViewController : BaseVC
@property id<RewardsHomeViewDelegate> delegate;
@end
