//
//  RewardsItemDetailsViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 13/10/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface RewardsItemDetailsViewController : BaseVC

@property NSDictionary *itemData;
@end
