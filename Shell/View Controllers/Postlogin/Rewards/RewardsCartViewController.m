//
//  RewardsCartViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 13/10/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "RewardsCartViewController.h"
#import "RewardsItemDetailsViewController.h"

#define CELL_HEIGHT 130

@interface RewardsCartViewController () <WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource, TTTAttributedLabelDelegate, UITextFieldDelegate>

enum kRewardCartTag_TYPE
{
    kRewardCartTag_IMAGEVIEW = 1,
    kRewardCartTag_NAME,
    kRewardCartTag_POINTSREQUIRED, //with points
    kRewardCartTag_QUANTITYTITLE,
    kRewardCartTag_SUBTRACT,
    kRewardCartTag_QUANTITY,
    kRewardCartTag_ADD,
    kRewardCartTag_DELETE,
    
};

@property (weak, nonatomic) IBOutlet TableViewWithEmptyView *tableView;

@property (weak, nonatomic) IBOutlet UILabel *lblTotalItems;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalItemsCount;

@property (weak, nonatomic) IBOutlet UILabel *lblPointsRequired;
@property (weak, nonatomic) IBOutlet UILabel *lblPoints;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckOut;

@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblAgree;
@property (weak, nonatomic) IBOutlet UIButton *btnAgree;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;
@property NSMutableArray *cartArray;

@property NSString *currentPoints;

@property (weak, nonatomic) IBOutlet UILabel *lblTopSectionItems;
@property (weak, nonatomic) IBOutlet UIButton *btnEmptyCart;
@property (weak, nonatomic) IBOutlet UIView *itemInCartView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *itemInCartViewHeight;

@end


@implementation RewardsCartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle: self title: LOCALIZATION(C_REWARDS_CARTTITLE) subtitle: @""];        //lokalise 31 jan
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
//    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_REWARDS_CARTTITLE),  LOCALIZATION(C_REWARDS_BACK)];
    
    if (GET_REWARDS_CANREDEEM)
    {
        self.btnCheckOut.hidden = NO;
    } else {
        self.btnCheckOut.hidden = YES;
    }
    
    
    [[WebServiceManager sharedInstance] fetchPointsSummary: self];
    
    if (kIsRightToLeft) {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
        self.lblTotalItemsCount.textAlignment = NSTextAlignmentLeft;
        self.lblPoints.textAlignment = NSTextAlignmentLeft;
    } else {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
        self.lblTotalItemsCount.textAlignment = NSTextAlignmentRight;
        self.lblPoints.textAlignment = NSTextAlignmentRight;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics  setScreenName:LOCALIZATION_EN(C_REWARDS_CARTTITLE) screenClass:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    [self setupInterface];
}

-(void) setupInterface
{
    self.lblTopSectionItems.font = FONT_H2;
    self.btnCheckOut.titleLabel.font = FONT_B2;
    
    self.lblTotalItems.text = LOCALIZATION(C_REWARDS_TOTALITEMS);   //lokalise 31 jan
    self.lblTotalItems.font = FONT_B1;
    
//    self.lblTotalItemsCount.text = @"3";
    self.lblTotalItemsCount.font = FONT_B1;
    
    self.lblPointsRequired.text = LOCALIZATION(C_REWARDS_POINTSREQUIRED);    //lokalise 31 jan
    self.lblPointsRequired.font = FONT_B(18);
    
//    self.lblPoints.text = @"10,000";
    self.lblPoints.font = FONT_H(18);
    self.lblPoints.textColor = COLOUR_RED;
    
    self.cartArray = [[NSMutableArray alloc] init]; //reset cart
    
    if (GET_REWARDSCART)
    {
        NSArray *savedArray = GET_REWARDSCART;
        for (NSDictionary *dict in savedArray)
        {
            [self.cartArray addObject: [[NSMutableDictionary alloc] initWithDictionary: dict]];
        }
    }
    else
    {
        self.btnCheckOut.enabled = NO;
        self.btnCheckOut.alpha = 0.5;
    }
    
    if (self.cartArray.count == 0) {
        self.itemInCartView.hidden = YES;
        self.itemInCartViewHeight.constant = 0;
    } else {
        self.itemInCartView.hidden = NO;
        self.itemInCartViewHeight.constant = 50;
    }
     [self calculateCartTotal];
    
    [self.btnEmptyCart setTitle: LOCALIZATION(C_REWARDS_EMPTYCART) forState: UIControlStateNormal];    //lokalise 31 jan
    self.btnEmptyCart.titleLabel.font = FONT_B2;
    self.btnEmptyCart.titleLabel.textColor = COLOUR_RED;
    
    [self.btnCheckOut setTitle: LOCALIZATION(C_REWARDS_CHECKOUT) forState:UIControlStateNormal];    //lokalise 31 jan
    [self.btnCheckOut.titleLabel setFont: FONT_BUTTON];
    [self.btnCheckOut setBackgroundColor:COLOUR_RED];
    
    [self.lblAgree setFont: FONT_B2];
    self.lblAgree.text = LOCALIZATION(C_PROFILE_AGREE);    //lokalise 31 jan
    
    [Helper setHyperlinkLabel:self.lblAgree hyperlinkText: LOCALIZATION(C_PROFILE_TNC) bodyText: LOCALIZATION(C_PROFILE_AGREE) urlString:@"TNC"];       //lokalise 31 jan
    
    [Helper addHyperlink: self.lblAgree hyperlinkText: LOCALIZATION(C_PROFILE_PRIVACY) urlString:@"Privacy" bodyText: LOCALIZATION(C_PROFILE_AGREE)];  //lokalise 31 jan
    
    self.lblAgree.delegate = self;
    
    
    
    //tableview setup
    PlaceholderView *vwPlaceholder = [[PlaceholderView alloc]initWithView:self.tableView];
    [vwPlaceholder setBackgroundColor:[UIColor clearColor]];
    self.tableView.emptyView = vwPlaceholder;

    //cart dont need refresh
    if(@available(iOS 10, *))
        self.tableView.refreshControl = nil;
    [self.tableView.customRefreshControl removeFromSuperview];
    self.tableView.customRefreshControl = nil;
    
//    // Initialize the refresh control.
//    [self.tableView.customRefreshControl addTarget:self
//                                            action:@selector(refreshTable)
//                                  forControlEvents:UIControlEventValueChanged];
    
    //empty view to eliminate extra separators
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.tableViewHeight.constant = CELL_HEIGHT * [self.cartArray count];
    
    [self.tableView reloadData];
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url absoluteString] isEqualToString: @"TNC"])
        [mSession pushTNCView:NO tncType: 999
                           vc:self];
    if ([[url absoluteString] isEqualToString: @"Privacy"])
        [mSession pushPrivacyView:self];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ItemCell" forIndexPath:indexPath];
    
    cell.tag = indexPath.row + 100;
    
    NSMutableDictionary *itemDict = [self.cartArray objectAtIndex: indexPath.row];
    
    UIImageView *imgItem = [cell viewWithTag: kRewardCartTag_IMAGEVIEW];
    
    NSString *urlString = [[itemDict objectForKey: @"ImageTH"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [imgItem sd_setImageWithURL: [NSURL URLWithString: urlString] placeholderImage: [UIImage imageNamed: @"placeholder_giftbox_thumb"] options: (SDWebImageRefreshCached  | SDWebImageRetryFailed)];
    
    UILabel *lblItemName = [cell viewWithTag: kRewardCartTag_NAME];
    lblItemName.text = [itemDict objectForKey: @"ProductName"];
    lblItemName.font = FONT_H1;
    
    UILabel *lblPointsRequired = [cell viewWithTag: kRewardCartTag_POINTSREQUIRED];
    double totalPoints;
    if ([[itemDict objectForKey: @"DiscountedPoint"] doubleValue] > 0)
        totalPoints = [[itemDict objectForKey: @"DiscountedPoint"] doubleValue] * [[itemDict objectForKey: @"Quantity"] intValue];
    else
        totalPoints = [[itemDict objectForKey: @"ProductPoint"] doubleValue] * [[itemDict objectForKey: @"Quantity"] intValue];
    
    lblPointsRequired.text = [NSString stringWithFormat: @"%@  %.0f %@", LOCALIZATION(C_REWARDS_POINTSREQUIRED), totalPoints, LOCALIZATION(C_REWARDS_POINTS)]; //lokalise 31 jan
    lblPointsRequired.font = FONT_B2;
    
    UILabel *lblQuantityTitle = [cell viewWithTag: kRewardCartTag_QUANTITYTITLE];
    lblQuantityTitle.text = LOCALIZATION(C_REWARDS_QUANTITY);    //lokalise 31 jan
    lblQuantityTitle.font = FONT_B3;
    
    UITextField *txtQuantity = [cell viewWithTag: kRewardCartTag_QUANTITY];
    txtQuantity.text = [itemDict objectForKey: @"Quantity"];
    txtQuantity.font = FONT_B1;
    
    return cell;
}

-(NSString *) getCellPointsRequiredStringWithPoints:(double) points quantity:(NSInteger) quantity
{
    double totalPoints = points * quantity;
    
    return [NSString stringWithFormat: @"%@ %.0f %@", LOCALIZATION(C_REWARDS_POINTSREQUIRED), totalPoints, LOCALIZATION(C_REWARDS_POINTS)];     //lokalise 31 jan
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.cartArray count];
}

- (IBAction)backPressed:(id)sender {
    
    for (UIViewController *vc in [self.navigationController childViewControllers])
    {
        if ([vc isKindOfClass: [RewardsItemDetailsViewController class]])
        {
            [self popToIndex: [[self.navigationController childViewControllers] count] - 3];
            return;
        }
    }
    [self popSelf];
}

- (IBAction)subtractQuantityPressed:(UIButton *)sender {
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow: sender.superview.superview.tag - 100 inSection:0]];
    
    UITextField *txtQuantity = [cell viewWithTag: kRewardCartTag_QUANTITY];
    NSInteger quantity = [txtQuantity.text intValue] - 1;
    if (quantity < 1)
        quantity = 1;
    txtQuantity.text = @(quantity).stringValue;
    
    
    
    NSMutableDictionary *itemDict = [[NSMutableDictionary alloc] initWithDictionary: [self.cartArray objectAtIndex: sender.superview.superview.tag - 100]];
    [itemDict setObject: txtQuantity.text forKey: @"Quantity"];
    
    
    UILabel *lblPointsRequired = [cell viewWithTag: kRewardCartTag_POINTSREQUIRED];
    
    
    double totalPoints;
    if ([[itemDict objectForKey: @"DiscountedPoint"] doubleValue] > 0)
        totalPoints = [[itemDict objectForKey: @"DiscountedPoint"] doubleValue] * [[itemDict objectForKey: @"Quantity"] intValue];
    else
        totalPoints = [[itemDict objectForKey: @"ProductPoint"] doubleValue] * [[itemDict objectForKey: @"Quantity"] intValue];
//    double totalPoints = [[itemDict objectForKey: @"ProductPoint"] doubleValue] * quantity;
    lblPointsRequired.text = [NSString stringWithFormat: @"%@ %.0f %@", LOCALIZATION(C_REWARDS_POINTSREQUIRED), totalPoints, LOCALIZATION(C_REWARDS_POINTS)];     //lokalise 31 jan
    
    [self.cartArray replaceObjectAtIndex: sender.superview.superview.tag - 100 withObject: itemDict];
    
    SET_REWARDSCART(self.cartArray);
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self calculateCartTotal];
}

- (IBAction)addQuantityPressed:(UIButton *)sender {
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow: sender.superview.superview.tag - 100 inSection:0]];
    
    NSMutableDictionary *itemDict = [[NSMutableDictionary alloc] initWithDictionary: [self.cartArray objectAtIndex: sender.superview.superview.tag - 100]];
    
    
    if ([[itemDict objectForKey: @"DiscountedPoint"] doubleValue] > 0)
    {
        if (([self.currentPoints doubleValue] - [self.lblPoints.text doubleValue] - [[itemDict objectForKey: @"DiscountedPoint"] doubleValue]) < 0)
        {
            [mAlert showErrorAlertWithMessage:  LOCALIZATION(C_REWARDS_INSUFFICIENTPOPUP)];     //lokalise 31 jan
            return;
        }
    }
    else
    {
        if (([self.currentPoints doubleValue] - [self.lblPoints.text doubleValue] - [[itemDict objectForKey: @"ProductPoint"] doubleValue]) < 0)
        {
            [mAlert showErrorAlertWithMessage:  LOCALIZATION(C_REWARDS_INSUFFICIENTPOPUP)];     //lokalise 31 jan
            return;
        }
    }
    
    UITextField *txtQuantity = [cell viewWithTag: kRewardCartTag_QUANTITY];
    NSInteger quantity = [txtQuantity.text intValue] + 1;
    txtQuantity.text = @(quantity).stringValue;
    
    
    [itemDict setObject: txtQuantity.text forKey: @"Quantity"];
    
    UILabel *lblPointsRequired = [cell viewWithTag: kRewardCartTag_POINTSREQUIRED];
//    double totalPoints = [[itemDict objectForKey: @"ProductPoint"] doubleValue] * quantity;
    
    double totalPoints;
    if ([[itemDict objectForKey: @"DiscountedPoint"] doubleValue] > 0)
        totalPoints = [[itemDict objectForKey: @"DiscountedPoint"] doubleValue] * [[itemDict objectForKey: @"Quantity"] intValue];
    else
        totalPoints = [[itemDict objectForKey: @"ProductPoint"] doubleValue] * [[itemDict objectForKey: @"Quantity"] intValue];
    
    lblPointsRequired.text = [NSString stringWithFormat: @"%@ %.0f %@", LOCALIZATION(C_REWARDS_POINTSREQUIRED), totalPoints, LOCALIZATION(C_REWARDS_POINTS)];     //lokalise 31 jan
    
    [self.cartArray replaceObjectAtIndex: sender.superview.superview.tag - 100 withObject: itemDict];
    
    SET_REWARDSCART(self.cartArray);
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self calculateCartTotal];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow: textField.superview.superview.tag - 100 inSection:0]];
    
    NSMutableDictionary *itemDict = [[NSMutableDictionary alloc] initWithDictionary: [self.cartArray objectAtIndex: textField.superview.superview.tag - 100]];

    UITextField *txtQuantity = [cell viewWithTag: kRewardCartTag_QUANTITY];
    NSInteger quantity = [txtQuantity.text intValue];
    
    if (quantity < 1)
        quantity = 1;
    
    double currentItemTotal = [[itemDict objectForKey: @"ProductPoint"] doubleValue] * [[itemDict objectForKey: @"Quantity"] doubleValue];
    
    double requiredPointsOffset = [self.lblPoints.text doubleValue] - currentItemTotal;
    
    double newItemTotal = [[itemDict objectForKey: @"ProductPoint"] doubleValue] * quantity;
    
    if (([self.currentPoints doubleValue]  - requiredPointsOffset - newItemTotal) < 0)
    {
        [mAlert showErrorAlertWithMessage:  LOCALIZATION(C_REWARDS_INSUFFICIENTPOPUP)];     //lokalise 31 jan
        
        txtQuantity.text = [itemDict objectForKey: @"Quantity"];
        return;
    }
    
    
    
    
    txtQuantity.text = @(quantity).stringValue;
    
    
    [itemDict setObject: txtQuantity.text forKey: @"Quantity"];
    
    UILabel *lblPointsRequired = [cell viewWithTag: kRewardCartTag_POINTSREQUIRED];
//    double totalPoints = [[itemDict objectForKey: @"ProductPoint"] doubleValue] * quantity;
    
    double totalPoints;
    if ([[itemDict objectForKey: @"DiscountedPoint"] doubleValue] > 0)
        totalPoints = [[itemDict objectForKey: @"DiscountedPoint"] doubleValue] * [[itemDict objectForKey: @"Quantity"] intValue];
    else
        totalPoints = [[itemDict objectForKey: @"ProductPoint"] doubleValue] * [[itemDict objectForKey: @"Quantity"] intValue];
    
    lblPointsRequired.text = [NSString stringWithFormat: @"%@ %.0f %@", LOCALIZATION(C_REWARDS_POINTSREQUIRED), totalPoints, LOCALIZATION(C_REWARDS_POINTS)];     //lokalise 31 jan
    
    [self.cartArray replaceObjectAtIndex: textField.superview.superview.tag - 100 withObject: itemDict];
    
    SET_REWARDSCART(self.cartArray);
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self calculateCartTotal];
}


- (IBAction)deletePressed:(UIButton *)sender {
//    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow: sender.superview.superview.tag - 100 inSection:0]];
//    
    [self.cartArray removeObjectAtIndex: sender.superview.superview.tag - 100];
    
    SET_REWARDSCART(self.cartArray);
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.tableView reloadData];
    
    self.tableViewHeight.constant = CELL_HEIGHT * [self.cartArray count];
    [self calculateCartTotal];
    
    if (self.cartArray.count == 0) {
        self.itemInCartView.hidden = YES;
        self.itemInCartViewHeight.constant = 0;
    } else {
        self.itemInCartView.hidden = NO;
        self.itemInCartViewHeight.constant = 50;
    }
}

-(void) calculateCartTotal
{
    NSInteger totalItemsCount = 0;
    NSInteger pointsRequired = 0;
    for (NSDictionary *dict in self.cartArray)
    {
        totalItemsCount = totalItemsCount + [[dict objectForKey: @"Quantity"] intValue];
        
        if ([[dict objectForKey: @"DiscountedPoint"] intValue] > 0)
            pointsRequired = pointsRequired + ([[dict objectForKey: @"DiscountedPoint"] intValue] * [[dict objectForKey: @"Quantity"] intValue]);
        else
            pointsRequired = pointsRequired + ([[dict objectForKey: @"ProductPoint"] intValue] * [[dict objectForKey: @"Quantity"] intValue]);
    }
    
    self.lblPoints.text = @(pointsRequired).stringValue;
    self.lblTotalItemsCount.text = @(totalItemsCount).stringValue;
    
    NSString *itemsCartString = [LOCALIZATION(C_REWARDS_ITEMSINCART) stringByReplacingOccurrencesOfString:@"XXXX" withString: @(self.cartArray.count).stringValue];     //lokalise 31 jan
    self.lblTopSectionItems.text = itemsCartString;
    if ([self.cartArray count] == 0)
    {
        self.btnCheckOut.enabled = NO;
        self.btnCheckOut.alpha = 0.5;
    }
    else
    {
        self.btnCheckOut.enabled = YES;
        self.btnCheckOut.alpha = 1;
    }
}

- (IBAction)checkBoxSelected:(UIButton *)sender {
    if ([sender isSelected])
        sender.selected = NO;
    else
        sender.selected = YES;
}

- (IBAction)checkOutPressed:(id)sender {

//    if (self.btnAgree.isSelected)
//    else
//        [mAlert showErrorAlertWithMessage: @"Please read and accept the program Terms and Conditions to continue"];
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA) || [[mSession profileInfo] isToSkipDeliveryAddress])
    {
        LoginInfo *loginInfo = [mSession loadUserProfile];
        [[WebServiceManager sharedInstance] fetchTrade: loginInfo.tradeID
                                                    vc:self];
    }
    else
        [mSession pushRewardsAddress: self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_REWARDSPOINTSSUMMARY:
        {
            self.currentPoints = [[response getGenericResponse] objectForKey: @"BalancePoints"];
            [mSession setRewardsCurrentPoints: [[[response getGenericResponse] objectForKey: @"BalancePoints"] doubleValue]];
        }
            break;
        case kWEBSERVICE_FETCHTRADE:
        {
            NSDictionary *workshopDetailsData = [response getGenericResponse];
            
            Member *member = [[Member alloc] initWithData:[workshopDetailsData objectForKey: @"REG_Member"]];
            CompanyAddress *companyAddress = [[CompanyAddress alloc] initWithData: [workshopDetailsData objectForKey: @"REG_CompanyAddress"]];
            
            NSDictionary *addressData = @{
                                          @"Address1" : companyAddress.address1,
                                          @"Address2" : companyAddress.address2,
                                          @"FirstName" : member.firstName,
                                          @"LastName" : member.lastName,
                                          @"MobileNumber" : member.mobileNumber,
                                          @"PostalCode" : companyAddress.postalCode,
                                          @"State" : companyAddress.companyState,
                                          @"Town" : companyAddress.companyCity,
                                          @"EmailAddress" : member.emailAddress,
                                          };
            [[WebServiceManager sharedInstance] confirmOrder: self.cartArray
             
                                              rewardsAddress: addressData
             
                                                          vc: self];
        }
            break;
        case kWEBSERVICE_REWARDSCONFIRMORDER:
        {
            //Clear local cart on success
            CLEAR_REWARDSCART;
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [mSession loadRewardsCompletionWithDictionary: [response getJSON]];
        }
            break;
        default:
            break;
    }
}

- (IBAction)emptyCartPressed:(id)sender {
    [self popUpYesNoWithTitle: @"" content: LOCALIZATION(C_REWARDS_EMPTYCONFIRMATION) yesBtnTitle: LOCALIZATION(C_FORM_YES) noBtnTitle: LOCALIZATION(C_FORM_NO) onYesPressed:^(BOOL finished) {   //lokalise 31 jan
        
        CLEAR_REWARDSCART;
        [self.cartArray removeAllObjects];
        [self calculateCartTotal];
        [self.tableView reloadData];
        self.tableViewHeight.constant = CELL_HEIGHT * [self.cartArray count];
        self.itemInCartView.hidden = YES;
        self.itemInCartViewHeight.constant = 0;
    } onNoPressed:^(BOOL finished) {
    
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
