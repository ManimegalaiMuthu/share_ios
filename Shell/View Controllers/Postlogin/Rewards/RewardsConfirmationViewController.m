//
//  RewardsConfirmationViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 13/10/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "RewardsConfirmationViewController.h"


//#define ADDRESS_CELL_HEIGHT 72
#define REDEMPTION_CELL_HEIGHT 90

#define kFirstName        @"FirstName"
#define kLastName         @"LastName"
#define kEmailAddress     @"EmailAddress"
#define kAddress1         @"Address1"
#define kAddress2         @"Address2"
#define kTown             @"Town"
#define kState            @"State"
#define kPostalCode       @"PostalCode"
#define kMobileNumber     @"MobileNumber"


//enum kRewardsConfirmation_AddressCellTag_TYPE
//{
//    kRewardsConfirmation_AddressCellTag_TOPLABEL = 1,
//    kRewardsConfirmation_AddressCellTag_TEXTFIELD,
//};

//enum kRewardsConfirmation_AddressField_TYPE
//{
//    kRewardsConfirmation_AddressField_FIRSTNAME,
//    kRewardsConfirmation_AddressField_LASTNAME,
//    kRewardsConfirmation_AddressField_EMAILADDRESS,
//    kRewardsConfirmation_AddressField_ADDRESS1,
//    kRewardsConfirmation_AddressField_ADDRESS2,
//    kRewardsConfirmation_AddressField_TOWN,
//    kRewardsConfirmation_AddressField_STATE,
//    kRewardsConfirmation_AddressField_POSTAL,
//    kRewardsConfirmation_AddressField_MOBILENUMBER,
//};

enum kRewardsConfirmation_RedemptionCell_TYPE
{
//    kRewardsConfirmation_RedemptionCell_NAME = 1,
//    kRewardsConfirmation_RedemptionCell_POINTSREQUIRED,
//    kRewardsConfirmation_RedemptionCell_QUANTITY,
//    kRewardsConfirmation_RedemptionCell_POINTSDATA,
//    kRewardsConfirmation_RedemptionCell_QUANTITYDATA,
    kRewardsConfirmation_RedemptionCell_IMG = 1,
    kRewardsConfirmation_RedemptionCell_NAME,
    kRewardsConfirmation_RedemptionCell_POINTSDATA,
    kRewardsConfirmation_RedemptionCell_QUANTITY,
};

@interface RewardsConfirmationViewController ()<WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnEditAddress; //back btn

@property (weak, nonatomic) IBOutlet UILabel *lblRecipientTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipientName;

@property (weak, nonatomic) IBOutlet UILabel *lblAddressTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;

@property (weak, nonatomic) IBOutlet UILabel *lblMobileNumberTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblMobileNumber;

@property (strong, nonatomic) IBOutlet UIButton *backBtn;


@property (weak, nonatomic) IBOutlet UILabel *lblRedemptionHeader;
@property (weak, nonatomic) IBOutlet UITableView *redemptionListTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *redemptionListTableViewHeight;

@property (weak, nonatomic) IBOutlet UILabel *lblTotalItems;
@property (weak, nonatomic) IBOutlet UILabel *lblQty;

@property (weak, nonatomic) IBOutlet UILabel *lblPointsRequired;
@property (weak, nonatomic) IBOutlet UILabel *lblPoints;


@property (weak, nonatomic) IBOutlet UIButton *btnPlaceOrder;

@property NSArray *cartArray;

@end

@implementation RewardsConfirmationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_REWARDS_CONFIRMATIONTITLE) subtitle: @""];        //lokalise 31 Jan
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
//    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_REWARDS_CONFIRMATIONTITLE),  LOCALIZATION(C_REWARDS_BACK)];
    
    
    if (kIsRightToLeft) {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_REWARDS_CONFIRMATIONTITLE) screenClass:nil];
    
    [self setupInterface];
}


-(void) setupInterface
{
    [[WebServiceManager sharedInstance] fetchPointsSummary: self];

    self.lblDeliveryAddress.font = FONT_H1;
    self.lblDeliveryAddress.text = LOCALIZATION(C_REWARDS_DELIVERYDETAILS);      //lokalise 31 Jan
    
    self.lblRecipientTitle.font = FONT_H1;
    self.lblAddressTitle.font = self.lblRecipientTitle.font;
    self.lblMobileNumberTitle.font = self.lblRecipientTitle.font;
    
    self.lblRecipientTitle.text = LOCALIZATION(C_REWARDS_RECIPIENT);  //lokalise 31 Jan
    self.lblAddressTitle.text = LOCALIZATION(C_PROFILE_ADDRESS);  //lokalise 31 Jan
    self.lblMobileNumberTitle.text = LOCALIZATION(C_PROFILE_MOBILENUM);  //lokalise 31 Jan
    
    self.lblRecipientName.font = FONT_B1;
    self.lblAddress.font = FONT_B1;
    self.lblMobileNumber.font = FONT_B1;
    
    self.lblRecipientName.text = [NSString stringWithFormat: @"%@ %@", [self.addressData objectForKey: @"FirstName"], [self.addressData objectForKey: @"LastName"]];
    
    //to reconfigure again
    NSString *fullAddressString = @"";
    
    //To add if field is not empty
    //No line-break if field is empty
    if (![[self.addressData objectForKey: @"Address1"] isEqualToString: BLANK])
    {
        NSString *address1String = [NSString stringWithFormat: @"%@", [self.addressData objectForKey: @"Address1"]];
        fullAddressString = [fullAddressString stringByAppendingString: address1String];
    }
    
    if (![[self.addressData objectForKey: @"Address2"] isEqualToString: BLANK])
    {
        NSString *address2String = [NSString stringWithFormat: @"\n%@", [self.addressData objectForKey: @"Address2"]];
        fullAddressString = [fullAddressString stringByAppendingString: address2String];
    }
    
    if (![[self.addressData objectForKey: @"StateName"] isEqualToString: BLANK])
    {
        NSString *stateNameString = [NSString stringWithFormat: @"\n%@", [self.addressData objectForKey: @"StateName"]];
        fullAddressString = [fullAddressString stringByAppendingString: stateNameString];
    }
    
    if (![[self.addressData objectForKey: @"Town"] isEqualToString: BLANK])
    {
        NSString *townString = [NSString stringWithFormat: @"\n%@", [self.addressData objectForKey: @"Town"]];
        fullAddressString = [fullAddressString stringByAppendingString: townString];
    }
    
    if (![[self.addressData objectForKey: @"PostalCode"] isEqualToString: BLANK])
    {
        NSString *postalCodeString = [NSString stringWithFormat: @"\n%@", [self.addressData objectForKey: @"PostalCode"]];
        fullAddressString = [fullAddressString stringByAppendingString: postalCodeString];
    }
    //Country is definitely available
    NSString *stateString = [NSString stringWithFormat: @"\n%@", [mSession convertCountryCodeToCountryName: GET_COUNTRY_CODE]];
    fullAddressString = [fullAddressString stringByAppendingString: stateString];
    
    self.lblAddress.text = fullAddressString;
    
    
    self.lblMobileNumber.text = [self.addressData objectForKey: @"MobileNumber"];
    
    self.lblRedemptionHeader.font = FONT_H1;
//    self.lblRedemptionHeader.text = LOCALIZATION(C_REWARDS_REDEMPTIONLISTHEADER);
    self.lblRedemptionHeader.text = LOCALIZATION(C_REWARDS_REVIEWREDEMPTION);       //lokalise 31 jan
    
    self.lblTotalItems.text = LOCALIZATION(C_REWARDS_TOTALITEMS);      //lokalise 31 jan
    self.lblTotalItems.font = FONT_B1;
    
    //    self.lblTotalItemsCount.text = @"3";
    self.lblQty.font = FONT_B1;
    
    self.lblPointsRequired.text = LOCALIZATION(C_REWARDS_POINTSREQUIRED);     //lokalise 31 jan
    self.lblPointsRequired.font = FONT_B(18);
    
    //    self.lblPoints.text = @"10,000";
    self.lblPoints.font = FONT_H(18);
    self.lblPoints.textColor = COLOUR_RED;
    
    
    [Helper setCustomFontButtonContentModes: self.btnPlaceOrder];
    [self.btnPlaceOrder setBackgroundColor:COLOUR_RED];
    [self.btnPlaceOrder.titleLabel setFont: FONT_BUTTON];
//    [self.btnPlaceOrder setTitle: LOCALIZATION(C_REWARDS_PLACEORDER) forState: UIControlStateNormal];
    [self.btnPlaceOrder setTitle: LOCALIZATION(C_REWARDS_CONFIRMREDEEM) forState: UIControlStateNormal];      //lokalise 31 jan
    
    self.cartArray = GET_REWARDSCART;
    
    [self calculateCartTotal];
    
    //set tableview height
    self.redemptionListTableViewHeight.constant = REDEMPTION_CELL_HEIGHT * ([self.cartArray count] + 1); //+1 for total cell
    self.redemptionListTableView.tableFooterView = [UIView new];
    [self.redemptionListTableView reloadData];
}

-(void) calculateCartTotal
{
    NSInteger totalItemsCount = 0;
    NSInteger pointsRequired = 0;
    for (NSDictionary *dict in self.cartArray)
    {
        totalItemsCount = totalItemsCount + [[dict objectForKey: @"Quantity"] intValue];
        
        if ([[dict objectForKey: @"DiscountedPoint"] intValue] > 0)
            pointsRequired = pointsRequired + ([[dict objectForKey: @"DiscountedPoint"] intValue] * [[dict objectForKey: @"Quantity"] intValue]);
        else
            pointsRequired = pointsRequired + ([[dict objectForKey: @"ProductPoint"] intValue] * [[dict objectForKey: @"Quantity"] intValue]);
    }
    
    self.lblPoints.text = @(pointsRequired).stringValue;
    self.lblQty.text = @(totalItemsCount).stringValue;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.cartArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return REDEMPTION_CELL_HEIGHT;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *itemData = [self.cartArray objectAtIndex: indexPath.row];
    
    UITableViewCell *cell = [self.redemptionListTableView dequeueReusableCellWithIdentifier:@"ItemCell"];
    
    UIImageView *imgItem = [cell viewWithTag: kRewardsConfirmation_RedemptionCell_IMG];

    NSString *urlString = [[itemData objectForKey: @"ImageTH"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [imgItem sd_setImageWithURL: [NSURL URLWithString: urlString] placeholderImage: [UIImage imageNamed: @"placeholder_giftbox_thumb"] options: (SDWebImageRefreshCached  | SDWebImageRetryFailed)];
    
    UILabel *lblTitle = [cell viewWithTag: kRewardsConfirmation_RedemptionCell_NAME];
    lblTitle.text = [itemData objectForKey: @"ProductName"];
    lblTitle.font = FONT_H1;
    
    UILabel *lblPoints = [cell viewWithTag: kRewardsConfirmation_RedemptionCell_POINTSDATA];
    lblPoints.textColor = COLOUR_RED;
    
    double totalPoints;
    if ([[itemData objectForKey: @"DiscountedPoint"] doubleValue] > 0)
        totalPoints = [[itemData objectForKey: @"DiscountedPoint"] doubleValue] * [[itemData objectForKey: @"Quantity"] intValue];
    else
        totalPoints = [[itemData objectForKey: @"ProductPoint"] doubleValue] * [[itemData objectForKey: @"Quantity"] intValue];
    lblPoints.text = [NSString stringWithFormat: @"%.0f %@", totalPoints, LOCALIZATION(C_REWARDS_POINTS)];  //lokalise 31 jan
    lblPoints.font = FONT_H1;
    
    UILabel *lblQty = [cell viewWithTag: kRewardsConfirmation_RedemptionCell_QUANTITY];
    lblQty.text = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_REWARDS_QTY), [itemData objectForKey: @"Quantity"]];  //lokalise 31 jan
    lblQty.font = FONT_B1;
    
    if (kIsRightToLeft) {
        lblPoints.textAlignment = NSTextAlignmentLeft;
        lblQty.textAlignment = NSTextAlignmentLeft;
    } else {
        lblPoints.textAlignment = NSTextAlignmentRight;
        lblQty.textAlignment = NSTextAlignmentRight;
    }
    
    return cell;
}

- (IBAction)placeOrderPressed:(id)sender {
    [self.addressData setValue:[_addressData objectForKey:@"StateName"] forKey:kState];
    [[WebServiceManager sharedInstance] confirmOrder: self.cartArray
                                      rewardsAddress: self.addressData
                                                  vc: self];
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_REWARDSCONFIRMORDER:
        {
            //Clear local cart on success
            CLEAR_REWARDSCART;
            [[NSUserDefaults standardUserDefaults] synchronize];

            [mSession loadRewardsCompletionWithDictionary: [response getJSON]];
        }
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
