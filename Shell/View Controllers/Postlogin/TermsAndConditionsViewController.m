//
//  TermsAndConditionsViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 18/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "TermsAndConditionsViewController.h"
#import <WebKit/WebKit.h>

@interface TermsAndConditionsViewController () <WKNavigationDelegate, WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UIView *tncWebViewContainer;

@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;
@property (weak, nonatomic) IBOutlet UIView *agreeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *agreeViewHeight;

@end

@implementation TermsAndConditionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_TNC) subtitle: @"" size:13 subtitleSize:0];   //lokalised
    else
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_TNC) subtitle: @"" size:15 subtitleSize:0];   //lokalised
    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_TITLE_TNC),  LOCALIZATION(C_REWARDS_BACK)];
    
    if ([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK])  //pre_login requires back button
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    }
    else if (!self.navigationController.parentViewController)
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    }
    if (kIsRightToLeft) {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
    
    if(self.agreeView.isHidden) {
        self.agreeViewHeight.constant = 0;
    } else {
        self.agreeViewHeight.constant = 50;
    }

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];

    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_TNC) screenClass:nil];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
#pragma mark - Adding WKWebView
    WebViewHelper *webViewHelper = [[WebViewHelper alloc] init];
    [webViewHelper setWebViewWithParentView: self.tncWebViewContainer
                         navigationDelegate: self
                                 urlAddress: [self getTncUrlString]
                                    headers: [WebserviceHelper prepPagesHeader]];
    //    [webViewHelper setWebConfiguration: WKDataDetectorTypeLink | WKDataDetectorTypePhoneNumber];  //default
}

-(NSString *) getTncUrlString
{
    switch (self.tncType)
    {
        case 1: // trade owner
            return [NSString stringWithFormat:@"%@%@%@?c=%@&type=%i",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_TNC_URL, GET_COUNTRY_CODE, 1];
            break;
        case 2: //mechanic
            return [NSString stringWithFormat:@"%@%@%@?c=%@&type=%i",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_TNC_URL, GET_COUNTRY_CODE, 2];
            break;
        case 999:  //from rewards
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
            return [NSString stringWithFormat:@"%@%@%@?c=%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_TNC_URL, GET_COUNTRY_CODE];
            break;
        case 699: //From Loyalty
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
            return [NSString stringWithFormat:@"%@%@%@?c=%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_TNC_LOYALTY_URL, GET_COUNTRY_CODE];
            break;
        default:
            if ([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK])
                return [NSString stringWithFormat:@"%@%@%@?c=%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_TNC_URL, GET_COUNTRY_CODE];
            else
            {
                switch (GET_PROFILETYPE)
                {
                    case kPROFILETYPE_TRADEOWNER:
                        return [NSString stringWithFormat:@"%@%@%@?c=%@&type=%i",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_TNC_URL, GET_COUNTRY_CODE, 1];
                        break;
                    case kPROFILETYPE_FORECOURTATTENDANT:
                        return [NSString stringWithFormat:@"%@%@%@?c=%@&type=%i",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_TNC_URL, GET_COUNTRY_CODE, 2];
                        break;
                    case kPROFILETYPE_MECHANIC:
                        return [NSString stringWithFormat:@"%@%@%@?c=%@&type=%i",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_TNC_URL, GET_COUNTRY_CODE, 2];
                        break;
                    default:
                        return [NSString stringWithFormat:@"%@%@%@?c=%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_TNC_URL, GET_COUNTRY_CODE];
                        break;
                }
            }
            break;
    }
    return @"";
}

-(void) showAgreeBtn
{
    self.agreeView.hidden = NO;
    self.agreeViewHeight.constant = 50;
//    [self.agreeBtn setTitle: LOCALIZATION(C_ACCEPTTNC) forState: UIControlStateNormal];
    NSAttributedString *agreeAttrString = [[NSAttributedString alloc] initWithString: LOCALIZATION(C_ACCEPTTNC) attributes: @{NSForegroundColorAttributeName: COLOUR_WHITE,                                                                                                                                              NSFontAttributeName: FONT_BUTTON, }];          //lokalised 11 Feb
    [self.agreeBtn setAttributedTitle: agreeAttrString forState: UIControlStateNormal];
    [self.agreeBtn.titleLabel setTextAlignment: NSTextAlignmentCenter];
    
    [self.agreeBtn.titleLabel setFont: FONT_BUTTON];
    [self.agreeBtn setBackgroundColor: COLOUR_RED];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: [UIView new]];
}

- (IBAction)backPressed:(id)sender {
    //only available prelogin
    [self popSelf];
}

- (IBAction)agreePressed:(id)sender {
    
    //call tnc api
    [[WebServiceManager sharedInstance] acceptTNC: self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)processCompleted:(WebServiceResponse *)response
{
    if ([response webserviceCall] == kWEBSERVICE_ACCEPTTNC)
        [self popSelf]; //if changepassword required, will be underneath TNC
}


#pragma WKWebView delegate methods
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    [UpdateHUD addMBProgress: webView withText:LOADING_HUD];
}

-(void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    NSLog(@"failed to load navigation");
    [UpdateHUD removeMBProgress: self.view];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
     [UpdateHUD removeMBProgress: webView];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    if(navigationAction.navigationType ==  WKNavigationTypeLinkActivated) {
        [[UIApplication sharedApplication] openURL: navigationAction.request.URL];
        
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
