//
//  ScratchableView.h
//  Shell
//
//  Created by Nach on 15/4/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ScratchViewDelegate <NSObject>

-(void) scratchFinished;

@end

@interface ScratchableView : UIView

@property id<ScratchViewDelegate> delegate;
@property (nonatomic) NSString *imageName;

-(void) refreshImageViewFor:(BOOL) isGamification;

@end

NS_ASSUME_NONNULL_END
