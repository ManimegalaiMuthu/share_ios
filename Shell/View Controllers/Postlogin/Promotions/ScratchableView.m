//
//  ScratchableView.m
//  Shell
//
//  Created by Nach on 15/4/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "ScratchableView.h"
@interface ScratchableView () <UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *lblScratch;

@property (weak, nonatomic) IBOutlet UIGestureRecognizer *touchRecognizer;
@property BOOL isGamification;

@property CGPoint currentPoint;
@property CGPoint lastPoint;
@property BOOL isTouchBegan;

@property CGFloat originalImageArea;
@property CGFloat revealedArea;

@property float min_x;
@property float min_y;
@property float max_x;
@property float max_y;

@end
@implementation ScratchableView

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.lblScratch.textColor = COLOUR_VERYDARKGREY;
    self.lblScratch.font = FONT_H1;
    self.lblScratch.text = LOCALIZATION(C_POPUP_LEBARAN_SCRATCH);
    
    self.isTouchBegan = NO;
    self.originalImageArea = self.imageView.frame.size.width * self.imageView.frame.size.height;
    self.revealedArea = 0.0f;
    
    self.min_x = self.imageView.frame.size.width;
    self.min_y = self.imageView.frame.size.height;
    self.max_y = 0.0f;
    self.min_y = 0.0f;

    
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {

        // 1. load the interface
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        [self addSubview:self.contentView];
        self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
        // 4. add constraints to span entire view
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[view]-|" options:0 metrics:nil views:@{@"view":self.contentView}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[view]-|" options:0 metrics:nil views:@{@"view":self.contentView}]];

    }
    return self;
}

-(void) refreshImageViewFor:(BOOL) isGamification {
    self.isGamification = isGamification;
    [self.imageView setImage:[UIImage imageNamed:self.imageName]];
    [self.lblScratch setHidden:isGamification];
}

#pragma mark - Gesture Recognizer
- (BOOL)isMultipleTouchEnabled {
    return NO;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if(!self.isTouchBegan) {
        [self.lblScratch setHidden:YES];
        self.lastPoint = [touch locationInView:self.imageView];
    } else {
        self.isTouchBegan = YES;
        [self eraseDrawingContents];
    }
    
    if(self.lastPoint.x < self.min_x)
        self.min_x = self.lastPoint.x;
    
    if(self.lastPoint.y < self.min_y)
        self.min_y = self.lastPoint.y;
    
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    self.currentPoint = [touch locationInView:self.imageView];
    
    if(self.currentPoint.x > self.max_x)
        self.max_x = self.currentPoint.x;
    
    if(self.currentPoint.y > self.max_y)
        self.max_y = self.currentPoint.y;
    
    [self eraseDrawingContents];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    self.currentPoint = [touch locationInView:self.imageView];
    
    if(self.currentPoint.x > self.max_x)
        self.max_x = self.currentPoint.x;
    
    if(self.currentPoint.y > self.max_y)
        self.max_y = self.currentPoint.y;
    
    [self eraseDrawingContents];
    self.isTouchBegan = NO;
}

#pragma mark - Scratch Method
-(void)eraseDrawingContents
{
    CGSize imageViewSize = self.imageView.frame.size;
    UIGraphicsBeginImageContext(imageViewSize);
    [self.imageView.image drawInRect:CGRectMake(0, 0, imageViewSize.width, imageViewSize.height)];

    CGContextSaveGState(UIGraphicsGetCurrentContext());
    CGContextSetShouldAntialias(UIGraphicsGetCurrentContext(), YES);
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 25.0);

    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, nil, self.lastPoint.x , self.lastPoint.y);
    CGPathAddLineToPoint(path, nil, self.currentPoint.x , self.currentPoint.y);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeClear);
    CGContextAddPath(UIGraphicsGetCurrentContext(), path);
    CGContextStrokePath(UIGraphicsGetCurrentContext());

    self.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    CGContextRestoreGState(UIGraphicsGetCurrentContext());
    UIGraphicsEndImageContext();

    [self calculateScratchedArea];
    self.lastPoint = self.currentPoint;
}


-(void) calculateScratchedArea {
    
    self.revealedArea = (self.max_y - self.min_y) * (self.max_x - self.min_x);
    
    float scratchedPercentage = (self.revealedArea * 100.0f) / self.originalImageArea;
    
    if(scratchedPercentage > 30.0f && !self.isGamification) {
        self.contentView.hidden = YES;
        [self.contentView setUserInteractionEnabled:NO];
        [self.delegate scratchFinished];
    } else if(scratchedPercentage > 15.0f && self.isGamification) {
        self.contentView.hidden = YES;
        [self.contentView setUserInteractionEnabled:NO];
        [self.delegate scratchFinished];
    }
    
}

@end
