//
//  CVSpeeddoMeterView.h
//  Shell
//
//  Created by Nach on 19/3/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CVSpeedoMeterView : UIView

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *speedometerView;

@property NSDictionary *viewData;
@property UIBezierPath *speedoMeterPath;
@property double radius;
@property CGPoint arcCenter;

-(void) loadBar:(NSDictionary *) responseData isCarousel:(BOOL) isCarousel;

@end

NS_ASSUME_NONNULL_END
