//
//  PromotionScratchViewController.m
//  Shell
//
//  Created by Nach on 15/4/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "PromotionScratchViewController.h"

@interface PromotionScratchViewController () <ScratchViewDelegate,WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblHeader;

@property (weak, nonatomic) IBOutlet UIView *wonView; //Hidden by default
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblWonMessage;
@property (weak, nonatomic) IBOutlet UIImageView *wonImageView;

@property (weak, nonatomic) IBOutlet UIView *tryagainView;// Shown by default
@property (weak, nonatomic) IBOutlet UILabel *lblTryAgainMessage;

@property (weak, nonatomic) IBOutlet ScratchableView *scratchableView;

@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewAspectRatio;

@property (weak, nonatomic) IBOutlet UIView *containerViewForScratchView;

@property BOOL isWinner;
@property BOOL isSkipped;
@property BOOL isScratchComplete;

@end

@implementation PromotionScratchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   
    self.isSkipped = NO;
    self.isScratchComplete = NO;
    if(self.isGamification) {
        [[WebServiceManager sharedInstance] playGrandPrizeInGamification:self];
        [self.scratchableView setImageName:@"card_closed.png"];
        
        self.containerViewAspectRatio.active = NO;
        
     } else {
        LoginInfo *loginInfo = [mSession loadUserProfile];
        [[WebServiceManager sharedInstance] showPromotionResultFor:@{
            @"VoucherValue"     : [[GET_HOMEDATA objectForKey:@"TradesPromotion"] objectForKey:@"VoucherValue"],
            @"OilChangeCount"   : [[GET_HOMEDATA objectForKey:@"TradesPromotion"] objectForKey:@"TPOilChangeCount"],
            @"CompanyCategory"  : loginInfo.companyCategory,
            @"TradeID"          : loginInfo.tradeID,
        } onVC:self];
        [self.scratchableView setImageName:@"ID-lebaran_scratch-card.png"];
        
        self.containerViewAspectRatio.active = NO;
    }
    
    [self.scratchableView setUserInteractionEnabled:NO];
    [self.scratchableView refreshImageViewFor:self.isGamification];
    
    [self.bgImage setHidden:!self.isGamification];
    
    [self setupInterface];
    
}

-(void) setupInterface {
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    self.lblHeader.font = FONT_B1;
    
    self.lblWonMessage.textColor = COLOUR_VERYDARKGREY;
    self.lblWonMessage.font = FONT_B1;
    
    self.lblTryAgainMessage.textColor = COLOUR_VERYDARKGREY;
    self.lblTryAgainMessage.font = FONT_B1;

    
    if(self.isGamification) {
        [self.btnSkip setUserInteractionEnabled:NO];
        [self.btnSkip setHidden:YES];
        
        [self.wonImageView setImage:[UIImage imageNamed:@"card_prizeopen.png"]];
        
        [NSLayoutConstraint constraintWithItem:self.containerViewForScratchView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.containerView attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0].active = YES;
        [NSLayoutConstraint constraintWithItem:self.containerViewForScratchView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:60].active = YES;
        
        [NSLayoutConstraint constraintWithItem:self.lblWonMessage attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.wonImageView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:60].active = YES;
            
        [NSLayoutConstraint constraintWithItem:self.containerView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.lblHeader attribute:NSLayoutAttributeBottom multiplier:1.0 constant:30].active = YES;
        
    } else {
        [self.btnSkip setUserInteractionEnabled:YES];
        [self.btnSkip setHidden:NO];
        [self.btnSkip setAttributedTitle: [[NSAttributedString alloc] initWithString: LOCALIZATION(C_POPUP_BTN_SKIP) attributes: @{
            NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,
            NSFontAttributeName: FONT_B1,
            NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),
            
        }] forState: UIControlStateNormal];
        
        [self.wonImageView setImage:[UIImage imageNamed:@"ID-lebaran_scratch-reveal.png"]];
        
        [NSLayoutConstraint constraintWithItem:self.containerViewForScratchView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.containerView attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0].active = YES;
        
        [NSLayoutConstraint constraintWithItem:self.lblWonMessage attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.wonImageView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0].active = YES;
        
        [NSLayoutConstraint constraintWithItem:self.containerView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.lblHeader attribute:NSLayoutAttributeBottom multiplier:1.0 constant:8].active = YES;
        
    }
    
    self.scratchableView.delegate = self;
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.containerView bringSubviewToFront:self.containerViewForScratchView];
    [self.containerViewForScratchView bringSubviewToFront:self.scratchableView];
}


- (IBAction)closePressed:(UIButton *)sender {
    if(self.isScratchComplete || self.isGamification) {
        [self dismissViewControllerAnimated:YES completion:^{
            [self.delegate promotionIsRedeemed];
        }];
    } else {
        LoginInfo *loginInfo = [mSession loadUserProfile];
        self.isSkipped = YES;
          
        if(!self.isGamification) {
            [[WebServiceManager sharedInstance] showPromotionResultFor:@{
                @"VoucherValue"     : @"",
                @"OilChangeCount"   : [[GET_HOMEDATA objectForKey:@"TradesPromotion"] objectForKey:@"TPOilChangeCount"],
                @"CompanyCategory"  : loginInfo.companyCategory,
                @"TradeID"          : loginInfo.tradeID,
            } onVC:self];
        }
    }
}

- (IBAction)skipPressed:(UIButton *)sender {
   LoginInfo *loginInfo = [mSession loadUserProfile];
   self.isSkipped = YES;
   
    if(!self.isGamification) {
        [[WebServiceManager sharedInstance] showPromotionResultFor:@{
            @"VoucherValue"     : @"",
            @"OilChangeCount"   : [[GET_HOMEDATA objectForKey:@"TradesPromotion"] objectForKey:@"TPOilChangeCount"],
            @"CompanyCategory"  : loginInfo.companyCategory,
            @"TradeID"          : loginInfo.tradeID,
        } onVC:self];
    }
}

- (BOOL) canBecomeFirstResponder {
    return YES;
}

#pragma mark - Scratch Delegate
-(void) scratchFinished {
    self.scratchableView.hidden = YES;
    self.isScratchComplete = YES;
    if(self.isWinner) {
        self.wonView.hidden = NO;
        self.wonImageView.hidden = NO;
        self.tryagainView.hidden = YES;
    } else {
        self.wonView.hidden = YES;
        self.wonImageView.hidden = YES;
        self.tryagainView.hidden = NO;
    }
    
    [self.btnSkip setHidden:YES];
    [self.btnSkip setUserInteractionEnabled:NO];
    
}

#pragma mark - WebServiceManager Delegate
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_GAMIFICATION_SHOWPROMOTIONRESULT: {
            if(!self.isSkipped) {
                if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                    NSString *headerString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:14; color: #404040;}</style>%@",[[response getGenericResponse] objectForKey:@"Header"] ];
                    self.lblHeader.attributedText = [[NSAttributedString alloc] initWithData: [headerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                    [self.lblHeader sizeToFit];
                    [self.lblHeader setNeedsDisplay];
                    
                    [self.scratchableView setUserInteractionEnabled:YES];
                    self.isWinner = [[[response getGenericResponse] objectForKey:@"IsWinner"] boolValue];
                    
                    NSString *messageString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:14; color: #404040;}</style>%@",[[response getGenericResponse] objectForKey:@"Footer"] ];
                    
                    if(self.isWinner) {
                        [self.wonView setHidden:NO];
                        self.lblWonMessage.attributedText = [[NSAttributedString alloc] initWithData: [messageString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                        
                        [self.tryagainView setHidden:YES];
                    } else {
                        [self.wonView setHidden:YES];
                        
                        [self.tryagainView setHidden:NO];
                        self.lblTryAgainMessage.attributedText = [[NSAttributedString alloc] initWithData: [messageString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                    }
                    
                } else {
                    
                    [self.scratchableView setUserInteractionEnabled:NO];
                    
                }
                
            } else {
                [self dismissViewControllerAnimated:YES completion:^{
                    [self.delegate promotionIsRedeemed];
                }];
            }
        }
            break;
        case kWEBSERVICE_GAMIFICATION_CHECKIN_PLAYGRANDPRIZE : {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                NSString *headerString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:15; color: #404040;}</style>%@",[[response getGenericResponse] objectForKey:@"Header"] ];
                self.lblHeader.attributedText = [[NSAttributedString alloc] initWithData: [headerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                [self.lblHeader sizeToFit];
                [self.lblHeader setNeedsDisplay];
                               
                [self.scratchableView setUserInteractionEnabled:YES];
                self.isWinner = YES;
                               
                NSString *messageString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:15; color: #404040;}</style>%@",[[response getGenericResponse] objectForKey:@"Message"] ];
                               
                [self.wonView setHidden:NO];
                self.lblWonMessage.attributedText = [[NSAttributedString alloc] initWithData: [messageString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                                                                 
                [self.tryagainView setHidden:YES];
                
                [self.contentView layoutIfNeeded];
                [self.containerView layoutIfNeeded];
                
            } else {
                               
                [self.scratchableView setUserInteractionEnabled:NO];
                               
            }
        }
            break;
        default:
            break;
    }
}

- (void)processFailed:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_GAMIFICATION_SHOWPROMOTIONRESULT : {
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }
            break;
        default:
            break;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
