//
//  ArcView.m
//  Shell
//
//  Created by Ben on 21/5/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ArcView.h"
#import "Constants.h"
#import "Session.h"
#import "Helper.h"

#define degreesToRadians(degrees)((M_PI * degrees)/180)

@implementation ArcView

UIBezierPath *speedoMeterPath;

double radius;
CGPoint arcCenter;
CGFloat barWidth;
int minValue;
int maxValue;
int progressValue;
CGFloat labelPadding;

UIColor *backgroundColor;
UIColor *barColor;

NSString *currency;
NSNumberFormatter *numberFormatter;

-(void) redraw {
    
    minValue = [self.datasource arcView:self minValueFor:0];
    maxValue = [self.datasource arcView:self maxValueFor:0];
    progressValue = [self.datasource arcView:self progressValueFor:0];
    
    //Defaults if datatsouce does not provide
    CGPoint centerDisplacement = CGPointMake(0, 0);
    barWidth = 20;
    labelPadding = 10;
    backgroundColor = COLOUR_LIGHTGREY;
    
    
    //clear previously drawn circle
    NSMutableArray *layerToRemoveArray = [NSMutableArray new]; //to hold references to actual progress ba
    for (UILabel *label in self.subviews){
        [label removeFromSuperview];
    }
    
    for (CALayer *layer in [self.layer sublayers])
    {
        if ([layer isKindOfClass: [CAShapeLayer class]])
        {
            [layerToRemoveArray addObject: layer];
        }
    }
    
    //remove referenced layers from superlayer
    for (CALayer *layer in layerToRemoveArray)
    {
        [layer removeFromSuperlayer];
    }
    
    if ([self.datasource respondsToSelector:@selector(arcView:centerDisplacementFor:)]) {
        centerDisplacement = [self.datasource arcView:self centerDisplacementFor:0];
    }
    if ([self.datasource respondsToSelector:@selector(arcView:barWidthFor:)]) {
        barWidth = [self.datasource arcView:self barWidthFor:0];
    }
    
    CGFloat centerX = self.frame.size.width/2;
    CGFloat centerY = self.frame.size.height-15;
    arcCenter = CGPointMake(centerX+centerDisplacement.x, centerY+centerDisplacement.y);
    
    CGFloat widthForDrawing = self.frame.size.width/2-15-barWidth;
    CGFloat heightForDrawing = self.frame.size.height-30-barWidth/2;
    if (widthForDrawing > heightForDrawing) {
        radius = heightForDrawing;
    } else {
        radius = widthForDrawing;
    }
    
    currency =  [[[[mSession lookupTable] objectForKey:@"LanguageList"]objectAtIndex:0]objectForKey:@"Currency"];
    numberFormatter = [NSNumberFormatter new];
    [numberFormatter setGroupingSeparator:@","];
    [numberFormatter setGroupingSize:3];
    [numberFormatter setUsesGroupingSeparator:YES];
    [numberFormatter setCurrencySymbol:currency];
    
    speedoMeterPath = [UIBezierPath bezierPathWithArcCenter:arcCenter radius:radius startAngle:degreesToRadians(180) endAngle:degreesToRadians(0) clockwise:YES];
    
    if ([self.datasource respondsToSelector:@selector(arcView:backgroundColorForArc:)]) {
        backgroundColor = [self.datasource arcView:self backgroundColorForArc:0];
    }
    CAShapeLayer *progressLayer = [CAShapeLayer new];
    [progressLayer setPath:speedoMeterPath.CGPath];
    [progressLayer setStrokeColor: [backgroundColor CGColor]];
    [progressLayer setFillColor:[UIColor clearColor].CGColor];
    [progressLayer setLineWidth:barWidth];
    [self.layer addSublayer:progressLayer];
    
    
    [self setNeedsDisplay];
    [self setNeedsLayout];
    [self layoutSubviews];
    
    
    //Creating Markers
    if (progressValue != 0) {
        [self colourArc];
        
        if (progressValue != minValue && progressValue != maxValue && progressValue < maxValue) {
            [self currentMarking];
        }
    }
    
    if ([self.datasource respondsToSelector:@selector(arcView:numberOfMarkersFor:)] &&
        [self.datasource respondsToSelector:@selector(arcView:valueForMarkerAtIndex:)]) {
        
        if ([self.datasource respondsToSelector:@selector(arcView:paddingForMarkerLabel:)]) {
            labelPadding = [self.datasource arcView:self paddingForMarkerLabel:0];
        }
        
        for (int i = 0; i < [self.datasource arcView:self numberOfMarkersFor:0]; i++) {
            [self createMarkingViewAt:i];
        }
    }
    
    NSAttributedString *centerText = [[NSAttributedString alloc]initWithString:@""];
    if ([self.datasource respondsToSelector:@selector(arcView:centerTextForArcView:)]) {
        centerText = [self.datasource arcView:self centerTextForArcView:0];
    }
    
    UILabel *lblTotal_Data = [UILabel new];
    lblTotal_Data.attributedText = centerText;
    lblTotal_Data.textAlignment = NSTextAlignmentCenter;
    lblTotal_Data.numberOfLines = 0;
    [lblTotal_Data sizeToFit];
    [lblTotal_Data setCenter:CGPointMake(arcCenter.x, arcCenter.y-radius/2)];
    [self addSubview:lblTotal_Data];
    
    [self layoutSubviews];
}

-(void) createMarkingViewAt:(int)index {
    
    NSAttributedString *outerTitle =  [[NSAttributedString alloc]initWithString:@""];
    NSAttributedString *innerTitle = [[NSAttributedString alloc]initWithString:@""];
    
    if ([self.datasource respondsToSelector:@selector(arcView:outerTitleForMarkerAtIndex:)]) {
        outerTitle = [self.datasource arcView:self outerTitleForMarkerAtIndex:index];
    }
    if ([self.datasource respondsToSelector:@selector(arcView:innerTitleForMarkerAtIndex:)]) {
        innerTitle = [self.datasource arcView:self innerTitleForMarkerAtIndex:index];
    }
    
    if ([self.datasource respondsToSelector:@selector(arcView:colorForMarkerAtIndex:)]) {
        [self createMarkingViewFor: outerTitle
                        attributed: innerTitle
                             value:[self.datasource arcView:self valueForMarkerAtIndex:index]
                      markingColor:[self.datasource arcView:self colorForMarkerAtIndex:index]];
    } else {
        [self createMarkingViewFor: outerTitle
                        attributed: innerTitle
                             value:[self.datasource arcView:self valueForMarkerAtIndex:index]];
    }
    
}
-(void) createMarkingViewFor:(NSString *)outerTitle inner:(NSString *)innerTitle value:(int)value {
    [self createMarkingViewFor:[[NSAttributedString alloc]initWithString:outerTitle] attributed:[[NSAttributedString alloc]initWithString:innerTitle] value:value];
}
-(void) createMarkingViewFor:(NSAttributedString *)outerTitle attributed:(NSAttributedString *)innerTitle value:(int)value {
    [self createMarkingViewFor:outerTitle attributed:innerTitle value:value markingColor:COLOUR_MIDGREY];
}

-(void) createMarkingViewFor:(NSAttributedString *)outerTitle attributed:(NSAttributedString *)innerTitle value:(int)value markingColor:(UIColor*)markingColor {
    
    int x,y;
    int angle = 180 - (180.0 / ((CGFloat)maxValue / (CGFloat)value));
    
    UILabel *lblMarking = [UILabel new];
    lblMarking.backgroundColor = markingColor;
    
    UIView *lblMarkingExtension = [UIView new];
    lblMarkingExtension.backgroundColor = markingColor;
    [lblMarking addSubview:lblMarkingExtension];
    lblMarkingExtension.frame = CGRectMake(0, -barWidth/2, 1, barWidth/2);
    
    x = arcCenter.x + (radius) * cos(degreesToRadians(angle));
    y = arcCenter.y - (radius) * sin(degreesToRadians(angle));
    lblMarking.frame = CGRectMake(x, y, 1, barWidth);
    [lblMarking setCenter:CGPointMake(x, y)];
    lblMarking.transform =  CGAffineTransformRotate(CGAffineTransformIdentity, -degreesToRadians(angle)+M_PI/2);
    
    UILabel *lblMarkingValue = [UILabel new];
    lblMarkingValue.backgroundColor = [UIColor clearColor];
    lblMarkingValue.font = FONT_B3;
    lblMarkingValue.textColor = COLOUR_VERYDARKGREY;
    lblMarkingValue.attributedText = outerTitle;
    lblMarkingValue.numberOfLines = 0;
    [lblMarkingValue sizeToFit];
    x = arcCenter.x + (radius + barWidth + labelPadding) * cos(degreesToRadians(angle));
    y = arcCenter.y - (radius + barWidth + labelPadding) * sin(degreesToRadians(angle));
    lblMarkingValue.frame = CGRectMake(x, y, lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
    [lblMarkingValue setCenter:CGPointMake(x, y)];
    
    UILabel *lbleVoucher = [UILabel new];
    lbleVoucher.attributedText = innerTitle;
    lbleVoucher.textColor = COLOUR_VERYDARKGREY;
    lbleVoucher.font = FONT_B3;
    lbleVoucher.numberOfLines = 0;
    [lbleVoucher sizeToFit];
    x = arcCenter.x + (radius - barWidth - labelPadding) * cos(degreesToRadians(angle));
    y = arcCenter.y - (radius - barWidth - labelPadding) * sin(degreesToRadians(angle));
    lbleVoucher.frame = CGRectMake(x, y, lbleVoucher.frame.size.width, lbleVoucher.frame.size.height);
    [lbleVoucher setCenter:CGPointMake(x, y)];
    
    [self addSubview:lbleVoucher];
    [self addSubview:lblMarkingValue];
    [self addSubview:lblMarking];
    [self layoutSubviews];
}

-(void) colourArc{
    CGFloat overAllAngle;
    if (progressValue >= maxValue) {
        overAllAngle = 0;
    } else if (progressValue < 0) {
        overAllAngle = 180;
    } else {
        overAllAngle = (180.0 / ((CGFloat)maxValue / (CGFloat)progressValue)) - 180.0;
    }
    
    UIBezierPath *overAllPath = [UIBezierPath bezierPathWithArcCenter:arcCenter radius:radius startAngle:degreesToRadians(180) endAngle:degreesToRadians(overAllAngle) clockwise:YES];
    
    CAShapeLayer *progressLayer = [CAShapeLayer layer];
    progressLayer.path = overAllPath.CGPath;
    
    barColor = [Helper colorFromHex:@"#DD1D21"];
    if ([self.datasource respondsToSelector:@selector(arcView:backgroundColorForArc:)]) {
        barColor = [self.datasource arcView:self colorForArc:0];
    }
    [progressLayer setStrokeColor: [barColor CGColor]];
    [progressLayer setFillColor:[UIColor clearColor].CGColor];
    [progressLayer setLineWidth:barWidth];
    [self.layer addSublayer:progressLayer];
    
    [self setNeedsDisplay];
    [self setNeedsLayout];
    [self layoutSubviews];
}

-(void) currentMarking {
    
    ArcViewCursorType type = [self.datasource arcView:self cursorTypeFor:0];
    
    switch (type) {
        case ArcViewCursorTypeNone:
            break;
            
        case ArcViewCursorTypeArrow:
        {
            CGFloat overAllAngle = 180 - (180 / ((CGFloat)maxValue / (CGFloat)progressValue));
            if (overAllAngle > 180) {
                overAllAngle = 180;
            }
            if (overAllAngle < 0) {
                overAllAngle = 0;
            }
            
            UIImageView *arrowImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"carousel_triangle_red.png"]];
            
            int x = arcCenter.x + ( (radius + barWidth) * cos(degreesToRadians(overAllAngle)));
            int y = arcCenter.y - ( (radius + barWidth) * sin(degreesToRadians(overAllAngle)));
            arrowImage.frame = CGRectMake(x,y,5,5);
            
            if(overAllAngle > 110) {
                arrowImage.transform =  CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(-50));
            }else if(overAllAngle < 80) {
                arrowImage.transform =  CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(50));
            }
            
            UILabel *currentMarking = [UILabel new];
            currentMarking.font = FONT_B3;
            currentMarking.textColor = COLOUR_RED;
            currentMarking.text = [NSString stringWithFormat:@"%d",progressValue];
            [currentMarking sizeToFit];
            
            x = arcCenter.x + ( (radius + barWidth * 1.7) * cos(degreesToRadians(overAllAngle)));
            y = arcCenter.y - ( (radius + barWidth * 1.7) * sin(degreesToRadians(overAllAngle)));
            
            currentMarking.frame = CGRectMake(x, y, currentMarking.frame.size.width, currentMarking.frame.size.height);
            
            
            [self addSubview:arrowImage];
            [self addSubview:currentMarking];
        }
            break;
            
        case ArcViewCursorTypeLine:
            [self createMarkingViewFor:[NSString stringWithFormat:@"%d",progressValue] inner:@"" value:progressValue];
            
        default:
            break;
    }
    
    
    
}

@end
