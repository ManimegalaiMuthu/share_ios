//
//  CVSpeeddoMeterView.m
//  Shell
//
//  Created by Nach on 19/3/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "CVSpeedoMeterView.h"
#import "Constants.h"
#import "Helper.h"
#import "Headers.h"
#import "SDWebImageManager.h"
#import "UIImageView+WebCache.h"
#define degreesToRadians(degrees)((M_PI * degrees)/180)
#define LINE_WIDTH 20.0

@implementation CVSpeedoMeterView

-(void)awakeFromNib
{
    [super awakeFromNib];
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        //https://stackoverflow.com/questions/30335089/reuse-a-uiview-xib-in-storyboard/37668821#37668821
        // 1. load the interface
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        // 2. add as subview
        [self addSubview:self.contentView];
        
        [self setNeedsLayout];
    }
    return self;
}

-(void) loadBar: (NSDictionary *) responseData isCarousel:(BOOL) isCarousel
{
    self.viewData = responseData;
    
    //clear previously drawn circle
    NSMutableArray *layerToRemoveArray = [[NSMutableArray alloc] init]; //to hold references to actual progress ba
    for (UILabel *label in self.speedometerView.subviews){
        [label removeFromSuperview];
    }
    
    for (CALayer *layer in [self.speedometerView.layer sublayers])
    {
        if ([layer isKindOfClass: [CAShapeLayer class]])
        {
            [layerToRemoveArray addObject: layer];
        }
    }
    
    //remove referenced layers from superlayer
    for (CALayer *layer in layerToRemoveArray)
    {
        [layer removeFromSuperlayer];
    }
    
    
    if(SCREEN_WIDTH <= 320){
         self.arcCenter = CGPointMake(SCREEN_WIDTH/2 - 7.5,152);
    } else {
         self.arcCenter = CGPointMake(SCREEN_WIDTH/2 - 10.0,152);
    }
    
    if(SCREEN_WIDTH <= 320){
         self.radius = 115;
    } else {
         self.radius = 120;
    }
    
    
    
    self.speedoMeterPath = [UIBezierPath bezierPathWithArcCenter:_arcCenter radius:_radius startAngle:degreesToRadians(180) endAngle:degreesToRadians(0) clockwise:YES];
    
    CAShapeLayer *progressLayer = [[CAShapeLayer alloc] init];
    [progressLayer setPath:self.speedoMeterPath.CGPath];
    [progressLayer setStrokeColor: [COLOUR_LIGHTGREY CGColor]];
    [progressLayer setFillColor:[UIColor clearColor].CGColor];
    [progressLayer setLineWidth:LINE_WIDTH];
    [self.speedometerView.layer addSublayer:progressLayer];
    
    
    [self.speedometerView setNeedsDisplay];
    [self.speedometerView setNeedsLayout];
    [self.speedometerView layoutSubviews];
    

    //Creating Markers
    if([[_viewData objectForKey:@"CVAchievedTarget"]intValue] != 0) {
        [self colourArc];
        [self currentMarking];
    }
    
    [self createMarkingViewFor:@"MinTotal" value: 0];
    [self createMarkingViewFor:@"MaxTotal" value: [[_viewData objectForKey:@"CVTotalTarget"]intValue]];
    
    if([[_viewData objectForKey:@"CVTotalTarget"] intValue] != 0) {
        [self createMarkingViewFor:@"FirstTargetTotal" value: [[_viewData objectForKey:@"CVFirstTarget"]intValue]];
        if([[_viewData objectForKey:@"CVTotalNoOfTarget"] intValue] != 2) {
             [self createMarkingViewFor:@"SecondTargetTotal" value: [[_viewData objectForKey:@"CVSecondTarget"]intValue]];
        }
    }
    
    
    NSMutableAttributedString *textTotalCartons ;
    
    textTotalCartons= [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%d",[[
                                                                                                                 _viewData objectForKey:@"CVAchievedTarget"]intValue]] attributes:@{
                                                                                                                                                                                        NSFontAttributeName : FONT_H(18) , NSForegroundColorAttributeName : COLOUR_RED,
                                                                                                                                                                                        }];
    
    [textTotalCartons appendAttributedString: [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" /%@",[
                                                                                                                                    _viewData objectForKey:@"CVTotalTarget"]] attributes:@{
                                                                                                                                                                                           NSFontAttributeName : FONT_H2,
                                                                                                                                                                                           NSForegroundColorAttributeName : COLOUR_VERYDARKGREY,
                                                                                                                                                                                           }]];
    
    int x,y;
    x = _arcCenter.x + (_radius - (LINE_WIDTH*2)) * cos(degreesToRadians(125));
    y = _arcCenter.y - (_radius - (LINE_WIDTH*2)) * sin(degreesToRadians(155));
    
    
    UILabel *lblTotal = [[UILabel alloc]init];
    lblTotal.text = LOCALIZATION(C_CAROUSEL_TOTALOILCHANGE);
    lblTotal.font = FONT_B2;
    lblTotal.textColor = COLOUR_VERYDARKGREY;
    [lblTotal sizeToFit];
    [lblTotal setCenter:CGPointMake(self.arcCenter.x, y)];
    [self.speedometerView addSubview:lblTotal];
    
    UILabel *lblTotal_Data = [[UILabel alloc]init];
    lblTotal_Data.attributedText = textTotalCartons;
    [lblTotal_Data sizeToFit];
    [lblTotal_Data setCenter:CGPointMake(self.arcCenter.x, y + 10 + lblTotal.frame.size.height)];
    [self.speedometerView addSubview:lblTotal_Data];
    
    [self.speedometerView layoutSubviews];
    
}


-(void) createMarkingViewFor:(NSString *)markingFor value:(float)value {
    
    float overallTarget = [[_viewData objectForKey:@"CVTotalTarget"]floatValue];
    
    UILabel *lblMarking = [[UILabel alloc ]init];
    lblMarking.backgroundColor = COLOUR_MIDGREY;
    
    UILabel *lblMarkingValue = [[UILabel alloc]init];
    lblMarkingValue.backgroundColor = [UIColor clearColor];
    lblMarkingValue.font = FONT_B3;
    lblMarkingValue.textColor = COLOUR_VERYDARKGREY;
    
    //Image is assigned just for the sake of initialization of imageView
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"image-creeper"]];
                              
    if([markingFor isEqualToString:@"FirstTargetTotal"]){
        NSString *urlString = [[self.viewData objectForKey: @"CVFirstRewardImage"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        [imageView sd_setImageWithURL: [NSURL URLWithString: urlString] placeholderImage: [UIImage imageNamed: @"image-creeper"] options:
         (SDWebImageRefreshCached | SDWebImageRetryFailed)];
    } else if([markingFor isEqualToString:@"SecondTargetTotal"]){
        NSString *urlString = [[self.viewData objectForKey: @"CVSecondRewardImage"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        [imageView sd_setImageWithURL: [NSURL URLWithString: urlString] placeholderImage: [UIImage imageNamed: @"image-phone"] options:
         (SDWebImageRefreshCached | SDWebImageRetryFailed)];
    } else if([markingFor isEqualToString:@"MaxTotal"]) {
        NSString *urlString = [[self.viewData objectForKey: @"CVFinalRewardImage"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        [imageView sd_setImageWithURL: [NSURL URLWithString: urlString] placeholderImage: [UIImage imageNamed: @"image-ledtv"] options:
         (SDWebImageRefreshCached | SDWebImageRetryFailed)];
    }
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    if([markingFor isEqualToString:@"MinTotal"]) {
        lblMarking.frame = CGRectMake(_arcCenter.x - _radius * 1.15, _arcCenter.y, LINE_WIDTH * 1.7, 1);
        lblMarkingValue.text = [NSString stringWithFormat:@"%.0f",value];
        [lblMarkingValue sizeToFit];
        
        int x = _arcCenter.x + ( (_radius + (LINE_WIDTH * 1.5)) * cos(degreesToRadians(180)));
        lblMarkingValue.frame = CGRectMake(x, _arcCenter.y - 2, lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
    } else if([markingFor isEqualToString:@"FirstTargetTotal"] || [markingFor isEqualToString:@"SecondTargetTotal"]) {
        int x,y;
        int imgX,imgY;
        int angle = 180 - (180.0 / (overallTarget / value));
        
        imgX = _arcCenter.x + ( (_radius - LINE_WIDTH ) * cos(degreesToRadians(angle + 12)));
        imgY = _arcCenter.y - ( (_radius - LINE_WIDTH ) * sin(degreesToRadians(angle + 12)));
        
        if(angle > 170) {
            x = _arcCenter.x + ( (_radius ) * cos(degreesToRadians(angle)));
            y = _arcCenter.y - ( (_radius ) * sin(degreesToRadians(angle - 8)));
            
            imgX = _arcCenter.x + ( (_radius - LINE_WIDTH ) * cos(degreesToRadians(angle + 12)));
            imgY = _arcCenter.y - ( (_radius - LINE_WIDTH ) * sin(degreesToRadians(angle)));
            
        } else if(angle > 160) {
            x = _arcCenter.x + ( (_radius + LINE_WIDTH * 0.3) * cos(degreesToRadians(angle)));
            y = _arcCenter.y - ( (_radius + LINE_WIDTH * 0.3) * sin(degreesToRadians(angle)));
        }else if(angle > 110 || (angle < 65 && angle > 20)) {
            x = _arcCenter.x + ( (_radius + LINE_WIDTH * 0.75) * cos(degreesToRadians(angle)));
            y = _arcCenter.y - ( (_radius + LINE_WIDTH * 0.75) * sin(degreesToRadians(angle)));
            
            imgX = _arcCenter.x + ( (_radius - LINE_WIDTH ) * cos(degreesToRadians(angle + 8)));
            imgY = _arcCenter.y - ( (_radius - LINE_WIDTH ) * sin(degreesToRadians(angle + 8)));
            
        }
        else if(angle <= 20) {
            x = _arcCenter.x + ( (_radius ) * cos(degreesToRadians(angle)));
            y = _arcCenter.y - ( (_radius ) * sin(degreesToRadians(angle + 10)));
            
            imgX = _arcCenter.x + ( (_radius - LINE_WIDTH * 2.0 ) * cos(degreesToRadians(angle)));
            imgY = _arcCenter.y - ( (_radius - LINE_WIDTH ) * sin(degreesToRadians(angle)));
            
        }
        else {
            x = _arcCenter.x + ( (_radius + LINE_WIDTH) * cos(degreesToRadians(angle)));
            y = _arcCenter.y - ( (_radius + LINE_WIDTH) * sin(degreesToRadians(angle)));
            
            imgX = _arcCenter.x + ( (_radius - LINE_WIDTH ) * cos(degreesToRadians(angle + 20)));
            imgY = _arcCenter.y - ( (_radius - LINE_WIDTH ) * sin(degreesToRadians(angle + 12)));
        }
        
        if(angle > 160 || angle <= 20) {
            lblMarking.frame = CGRectMake(x, y, 1, LINE_WIDTH * 1.9);
        } else {
            lblMarking.frame = CGRectMake(x, y, 1, LINE_WIDTH * 2.0);
        }
        
        if(angle > 90) {
            lblMarking.transform =  CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(-50));
        }else if(angle < 90) {
            lblMarking.transform =  CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(50));
        }
        
        lblMarkingValue.text = [NSString stringWithFormat:@"%.0f",value];
        [lblMarkingValue sizeToFit];
        
        if(angle > 160) {
            x = _arcCenter.x + ( (_radius + LINE_WIDTH * 1.2) * cos(degreesToRadians(angle + 20)));
            y = _arcCenter.y - ( (_radius + LINE_WIDTH * 1.2) * sin(degreesToRadians(angle + 20)));
        }
        else if(angle > 120) {
            x = _arcCenter.x + ( (_radius + LINE_WIDTH * 1.7) * cos(degreesToRadians(angle + 10)));
            y = _arcCenter.y - ( (_radius + LINE_WIDTH * 1.7) * sin(degreesToRadians(angle + 10)));
        } else if(angle < 60 && angle >= 20 ){
            x = _arcCenter.x + ( (_radius + LINE_WIDTH * 1.7) * cos(degreesToRadians(angle - 12)));
            y = _arcCenter.y - ( (_radius + LINE_WIDTH * 1.7) * sin(degreesToRadians(angle - 12)));
        } else {
            x = _arcCenter.x + ( (_radius + LINE_WIDTH * 1.7) * cos(degreesToRadians(angle)));
            y = _arcCenter.y - ( (_radius + LINE_WIDTH * 1.7) * sin(degreesToRadians(angle)));
        }
        
        if(angle > 170) {
            lblMarkingValue.frame = CGRectMake(x - 7, y - 4 , lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
        } else if(angle < 10) {
            lblMarkingValue.frame = CGRectMake(x - 7, y + 7 , lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
        } else if(angle > 160 || angle <= 20){
            lblMarkingValue.frame = CGRectMake(x - 7, y + 5 , lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
        } else {
            lblMarkingValue.frame = CGRectMake(x - 5, y, lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
        }
        
        imageView.frame = CGRectMake(imgX, imgY, imageView.frame.size.width, imageView.frame.size.height);
        if(angle < 121 && angle > 100) {
            imageView.frame = CGRectMake(imgX - 8, imgY, imageView.frame.size.width, imageView.frame.size.height);
        } else if(angle < 80 && angle > 58) {
            imageView.frame = CGRectMake(imgX, imgY + 8, imageView.frame.size.width, imageView.frame.size.height);
        } else if(angle < 140 && angle > 120) {
             imageView.frame = CGRectMake(imgX - 5, imgY + 4, imageView.frame.size.width, imageView.frame.size.height);
        }
        [self.speedometerView addSubview:imageView];
        
    } else if([markingFor isEqualToString:@"MaxTotal"]) {
        lblMarking.frame = CGRectMake(_arcCenter.x + _radius * 0.85, _arcCenter.y, LINE_WIDTH * 1.7, 1);
        
        lblMarkingValue.text = [NSString stringWithFormat:@"%.0f",overallTarget];
        [lblMarkingValue sizeToFit];
        
        int x = _arcCenter.x + ( (_radius * 1.2)) * cos(degreesToRadians(0));
        lblMarkingValue.frame = CGRectMake(x, _arcCenter.y - 2, lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
        
        imageView.frame = CGRectMake(_arcCenter.x + _radius * 0.65, _arcCenter.y - 10, imageView.frame.size.width, imageView.frame.size.height);
        [self.speedometerView addSubview:imageView];
    }
    
    [self.speedometerView addSubview:lblMarkingValue];
    [self.speedometerView addSubview:lblMarking];
    
}

-(void) colourArc{
    CGFloat overAllAngle;
    if([[_viewData objectForKey:@"CVAchievedTarget"] intValue] >= [[_viewData objectForKey:@"CVTotalTarget"] intValue]) {
        overAllAngle = 0;
    } else {
        overAllAngle = (180.0 / ([[_viewData objectForKey:@"CVTotalTarget"] floatValue] / [[_viewData objectForKey:@"CVAchievedTarget"] intValue])) - 180.0;
    }
    
    UIBezierPath *overAllPath ;
    if(overAllAngle > -80 && overAllAngle < -45) {
        overAllPath = [UIBezierPath bezierPathWithArcCenter:_arcCenter radius:_radius startAngle:degreesToRadians(180) endAngle:degreesToRadians(overAllAngle + 20) clockwise:YES];
    } else if(overAllAngle < -110 && overAllAngle > -170) {
        overAllPath = [UIBezierPath bezierPathWithArcCenter:_arcCenter radius:_radius startAngle:degreesToRadians(180) endAngle:degreesToRadians(overAllAngle - 20) clockwise:YES];
    } else {
        overAllPath = [UIBezierPath bezierPathWithArcCenter:_arcCenter radius:_radius startAngle:degreesToRadians(180) endAngle:degreesToRadians(overAllAngle) clockwise:YES];
    }
    
    CAShapeLayer *progressLayer = [CAShapeLayer layer];
    progressLayer.path = overAllPath.CGPath;
    [progressLayer setStrokeColor: [[Helper colorFromHex:[_viewData objectForKey:@"CVColorCode"]] CGColor]];
    [progressLayer setFillColor:[UIColor clearColor].CGColor];
    [progressLayer setLineWidth:LINE_WIDTH];
    [self.speedometerView.layer addSublayer:progressLayer];
    
    [self.speedometerView setNeedsDisplay];
    [self.speedometerView setNeedsLayout];
    [self.speedometerView layoutSubviews];
}

-(void) currentMarking {
    int target_total = [[_viewData objectForKey:@"CVTotalTarget"] intValue];
    int target_1 = [[_viewData objectForKey:@"CVFirstTarget"] intValue];
    int target_2 = [[_viewData objectForKey:@"CVSecondTarget"] intValue];
    int achieved = [[_viewData objectForKey:@"CVAchievedTarget"] intValue];
    
    if(target_1 != achieved && target_2 != achieved && target_total != achieved && achieved != 0 && achieved < target_total) {
        CGFloat overAllAngle = 180 - (180 / ([[_viewData objectForKey:@"CVTotalTarget"] floatValue] / [[_viewData objectForKey:@"CVAchievedTarget"] floatValue]));
        if(overAllAngle > 110 && overAllAngle < 170) {
            overAllAngle += 6;
        } else if(overAllAngle < 80 && overAllAngle > 45) {
            overAllAngle -= 6;
        }
        
        UIImageView *arrowImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"carousel_triangle_red.png"]];
        
        int x = _arcCenter.x + ( (_radius + LINE_WIDTH) * cos(degreesToRadians(overAllAngle)));
        int y = _arcCenter.y - ( (_radius + LINE_WIDTH) * sin(degreesToRadians(overAllAngle)));
        arrowImage.frame = CGRectMake(x,y,5,5);
        
        if(overAllAngle > 110) {
            arrowImage.transform =  CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(-50));
        }else if(overAllAngle < 80) {
            arrowImage.transform =  CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(50));
        }
        
        UILabel *currentMarking = [[UILabel alloc]init];
        currentMarking.font = FONT_B3;
        currentMarking.textColor = COLOUR_RED;
        currentMarking.text = [NSString stringWithFormat:@"%d",[[_viewData objectForKey:@"CVAchievedTarget"] intValue]];
        [currentMarking sizeToFit];
        
        x = _arcCenter.x + ( (_radius + LINE_WIDTH * 1.7) * cos(degreesToRadians(overAllAngle)));
        y = _arcCenter.y - ( (_radius + LINE_WIDTH * 1.7) * sin(degreesToRadians(overAllAngle)));
        
        currentMarking.frame = CGRectMake(x, y, currentMarking.frame.size.width, currentMarking.frame.size.height);
        
        
        [self.speedometerView addSubview:arrowImage];
        [self.speedometerView addSubview:currentMarking];
    }
}


@end
