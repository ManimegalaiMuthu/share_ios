//
//  HXPromotionView.m
//  Shell
//
//  Created by Nach on 14/5/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "HXPromotionView.h"
#import "Constants.h"
#import "Helper.h"
#import "Headers.h"
#import "SDWebImageManager.h"
#import "UIImageView+WebCache.h"
#import "Session.h"
#define degreesToRadians(degrees)((M_PI * degrees)/180)
#define LINE_WIDTH 20.0

@implementation HXPromotionView

-(void)awakeFromNib
{
    [super awakeFromNib];
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        //https://stackoverflow.com/questions/30335089/reuse-a-uiview-xib-in-storyboard/37668821#37668821
        // 1. load the interface
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        // 2. add as subview
        [self addSubview:self.contentView];
        
        [self setNeedsLayout];
    }
    return self;
}

-(void) loadBar: (NSDictionary *) responseData isCarousel:(BOOL) isCarousel
{
    self.viewData = responseData;
    
//    self.viewData = @{
//                      @"HX8PromoEndDate" : @"25/7/2019",
//                      @"HX8PromoFooterText" : @"The participating mechanic will be eligible for only one eVoucher based on the oil change slab he lies in at the end of the campaign.",
//                      @"HX8PromoHeading" : @"HELIX USTAAD DHAMAKA",
//                      @"HX8PromoStartDate" : @"25/4/2019",
//                      @"HX8PromoTarget1" : @(25),
//                      @"HX8PromoTarget2" : @(75),
//                      @"HX8PromoTarget3" : @(125),
//                      @"HX8PromoTarget4" : @(175),
//                      @"HX8PromoVoucherValue1" : @(500),
//                      @"HX8PromoVoucherValue2" : @(5000),
//                      @"HX8PromoVoucherValue3" : @(10000),
//                      @"HX8PromoVoucherValue4" : @(20000),
//                      @"HX8TargetAchieved" : @(150),
//                      @"HasBtnScanNow" : @(1),
//                      };
    
    //clear previously drawn circle
    NSMutableArray *layerToRemoveArray = [[NSMutableArray alloc] init]; //to hold references to actual progress ba
    for (UILabel *label in self.hxSpeedometerView.subviews){
        [label removeFromSuperview];
    }
    
    for (CALayer *layer in [self.hxSpeedometerView.layer sublayers])
    {
        if ([layer isKindOfClass: [CAShapeLayer class]])
        {
            [layerToRemoveArray addObject: layer];
        }
    }
    
    //remove referenced layers from superlayer
    for (CALayer *layer in layerToRemoveArray)
    {
        [layer removeFromSuperlayer];
    }
    
    
    if(SCREEN_WIDTH <= 320){
        self.arcCenter = CGPointMake(SCREEN_WIDTH/2 - 7.5,148);
    } else {
        self.arcCenter = CGPointMake(SCREEN_WIDTH/2 - 10.0,148);
    }
    
    if(SCREEN_WIDTH <= 320){
        self.radius = 115;
    } else {
        self.radius = 120;
    }
    
    self.currency =  [[[[mSession lookupTable] objectForKey:@"LanguageList"]objectAtIndex:0]objectForKey:@"Currency"];
    self.numberFormatter = [[NSNumberFormatter alloc] init];
    [self.numberFormatter setGroupingSeparator:@","];
    [self.numberFormatter setGroupingSize:3];
    [self.numberFormatter setUsesGroupingSeparator:YES];
    
    self.speedoMeterPath = [UIBezierPath bezierPathWithArcCenter:_arcCenter radius:_radius startAngle:degreesToRadians(180) endAngle:degreesToRadians(0) clockwise:YES];
    
    CAShapeLayer *progressLayer = [[CAShapeLayer alloc] init];
    [progressLayer setPath:self.speedoMeterPath.CGPath];
    [progressLayer setStrokeColor: [COLOUR_LIGHTGREY CGColor]];
    [progressLayer setFillColor:[UIColor clearColor].CGColor];
    [progressLayer setLineWidth:LINE_WIDTH];
    [self.hxSpeedometerView.layer addSublayer:progressLayer];
    
    
    [self.hxSpeedometerView setNeedsDisplay];
    [self.hxSpeedometerView setNeedsLayout];
    [self.hxSpeedometerView layoutSubviews];
    
    
    //Creating Markers
    if([[_viewData objectForKey:@"HX8TargetAchieved"]intValue] != 0) {
        [self colourArc];
        [self currentMarking];
    }
    
    [self createMarkingViewFor:@"MinTotal" value: 0];
    [self createMarkingViewFor:@"MaxTotal" value: [[_viewData objectForKey:@"HX8PromoTarget4"]intValue]];
    
    if([[_viewData objectForKey:@"HX8PromoTarget4"] intValue] != 0) {
        [self createMarkingViewFor:@"FirstTargetTotal" value: [[_viewData objectForKey:@"HX8PromoTarget1"]intValue]];
        [self createMarkingViewFor:@"SecondTargetTotal" value: [[_viewData objectForKey:@"HX8PromoTarget2"]intValue]];
        [self createMarkingViewFor:@"ThirdTargetTotal" value: [[_viewData objectForKey:@"HX8PromoTarget3"]intValue]];
    }
    
    
    NSMutableAttributedString *textTotalCartons ;
    
    textTotalCartons= [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%d",[[
                                                                                                          _viewData objectForKey:@"HX8TargetAchieved"]intValue]] attributes:@{
                                                                                                                                                                             NSFontAttributeName : FONT_H(18) , NSForegroundColorAttributeName : COLOUR_RED,
                                                                                                                                                                             }];
    
    [textTotalCartons appendAttributedString: [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" /%@",[
                                                                                                                                    _viewData objectForKey:@"HX8PromoTarget4"]] attributes:@{
                                                                                                                                                                                           NSFontAttributeName : FONT_H2,
                                                                                                                                                                                           NSForegroundColorAttributeName : COLOUR_VERYDARKGREY,
                                                                                                                                                                                           }]];
    
    int x,y;
    x = _arcCenter.x + (_radius - (LINE_WIDTH*2)) * cos(degreesToRadians(125));
    y = _arcCenter.y - (_radius - (LINE_WIDTH*2)) * sin(degreesToRadians(155));
    
    
    UILabel *lblTotal = [[UILabel alloc]init];
    lblTotal.text = LOCALIZATION(C_CAROUSEL_TOTALOILCHANGE);
    lblTotal.font = FONT_B2;
    lblTotal.textColor = COLOUR_VERYDARKGREY;
    [lblTotal sizeToFit];
    [lblTotal setCenter:CGPointMake(self.arcCenter.x, y + 10)];
    [self.hxSpeedometerView addSubview:lblTotal];
    
    UILabel *lblTotal_Data = [[UILabel alloc]init];
    lblTotal_Data.attributedText = textTotalCartons;
    [lblTotal_Data sizeToFit];
    [lblTotal_Data setCenter:CGPointMake(self.arcCenter.x, y + 15 + lblTotal.frame.size.height)];
    [self.hxSpeedometerView addSubview:lblTotal_Data];
    
    [self.hxSpeedometerView layoutSubviews];
    
}


-(void) createMarkingViewFor:(NSString *)markingFor value:(int)value {
    
    float overallTarget = [[_viewData objectForKey:@"HX8PromoTarget4"]floatValue];
    
    UILabel *lblMarking = [[UILabel alloc ]init];
    lblMarking.backgroundColor = COLOUR_MIDGREY;
    
    UILabel *lblMarkingValue = [[UILabel alloc]init];
    lblMarkingValue.backgroundColor = [UIColor clearColor];
    lblMarkingValue.font = FONT_B3;
    lblMarkingValue.textColor = COLOUR_VERYDARKGREY;

    
    if([markingFor isEqualToString:@"MinTotal"]) {
        lblMarking.frame = CGRectMake(_arcCenter.x - _radius * 1.15, _arcCenter.y, LINE_WIDTH * 1.7, 1);
        lblMarkingValue.text = [NSString stringWithFormat:@"%d",value];
        [lblMarkingValue sizeToFit];
        
        int x = _arcCenter.x + ( (_radius + (LINE_WIDTH * 1.5)) * cos(degreesToRadians(180)));
        lblMarkingValue.frame = CGRectMake(x, _arcCenter.y - 2, lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
    } else if([markingFor isEqualToString:@"FirstTargetTotal"] || [markingFor isEqualToString:@"SecondTargetTotal"] || [markingFor isEqualToString:@"ThirdTargetTotal"]) {
        int x,y;
        int imgX,imgY;
        int angle = 180 - (180.0 / (overallTarget / value));
        
        if(angle > 170) {
            x = _arcCenter.x + ( (_radius ) * cos(degreesToRadians(angle)));
            y = _arcCenter.y - ( (_radius ) * sin(degreesToRadians(angle - 8)));
        } else if(angle > 160) {
            x = _arcCenter.x + ( (_radius + LINE_WIDTH * 0.2) * cos(degreesToRadians(angle)));
            y = _arcCenter.y - ( (_radius + LINE_WIDTH * 0.2) * sin(degreesToRadians(angle)));
        } else if(angle > 110 ) {
            x = _arcCenter.x + ( (_radius + LINE_WIDTH * 0.5) * cos(degreesToRadians(angle)));
            y = _arcCenter.y - ( (_radius + LINE_WIDTH * 0.5) * sin(degreesToRadians(angle)));
        } else if(angle < 65 && angle > 20) {
            x = _arcCenter.x + ( (_radius + LINE_WIDTH * 0.65) * cos(degreesToRadians(angle)));
            y = _arcCenter.y - ( (_radius + LINE_WIDTH * 0.65) * sin(degreesToRadians(angle)));
        } else if(angle <= 20) {
            x = _arcCenter.x + ( (_radius ) * cos(degreesToRadians(angle)));
            y = _arcCenter.y - ( (_radius ) * sin(degreesToRadians(angle + 10)));
        } else {
            x = _arcCenter.x + ( (_radius + LINE_WIDTH ) * cos(degreesToRadians(angle)));
            y = _arcCenter.y - ( (_radius + LINE_WIDTH ) * sin(degreesToRadians(angle)));
        }
        
        if(angle > 160 || angle <= 20) {
            lblMarking.frame = CGRectMake(x, y, 1, LINE_WIDTH * 1.9);
        } else {
            lblMarking.frame = CGRectMake(x, y, 1, LINE_WIDTH * 2.0);
        }
        
        if(angle > 110) {
            lblMarking.transform =  CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(-70));
        } else if(angle < 110 && angle > 90) {
             lblMarking.transform =  CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(-15));
        }else if(angle < 90) {
            lblMarking.transform =  CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(50));
        }
        
        lblMarkingValue.text = [NSString stringWithFormat:@"%d",value];
        [lblMarkingValue sizeToFit];
        
        UILabel *lbleVoucher = [[UILabel alloc] init];
        lbleVoucher.text = LOCALIZATION(C_CAROUSEL_EVOUCHER);
        lbleVoucher.textColor = COLOUR_VERYDARKGREY;
        lbleVoucher.font = FONT_B3;
        [lbleVoucher sizeToFit];
        
        UILabel *lbleVoucher_Data = [[UILabel alloc] init];
        lbleVoucher_Data.text = [NSString stringWithFormat:@"%@ %@",self.currency,[_numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[self.viewData objectForKey:@"HX8PromoVoucherValue3"] floatValue]]]];
        lbleVoucher_Data.textColor = COLOUR_VERYDARKGREY;
        lbleVoucher_Data.font = FONT_B3;
        
        if([markingFor isEqualToString:@"FirstTargetTotal"]){
            lbleVoucher_Data.text = [NSString stringWithFormat:@"%@ %@",self.currency,[_numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[self.viewData objectForKey:@"HX8PromoVoucherValue1"] floatValue]]]];
        } else if([markingFor isEqualToString:@"SecondTargetTotal"]){
            lbleVoucher_Data.text = [NSString stringWithFormat:@"%@ %@",self.currency,[_numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[self.viewData objectForKey:@"HX8PromoVoucherValue2"] floatValue]]]];
        }
        [lbleVoucher_Data sizeToFit];
        
        imgX = _arcCenter.x + ( (_radius - LINE_WIDTH ) * cos(degreesToRadians(angle + 12)));
        imgY = _arcCenter.y - ( (_radius - LINE_WIDTH ) * sin(degreesToRadians(angle + 12)));
        
       if(angle != 90) {
            x = _arcCenter.x + ( (_radius + LINE_WIDTH * 0.75) * cos(degreesToRadians(angle)));
            y = _arcCenter.y - ( (_radius + LINE_WIDTH * 0.75) * sin(degreesToRadians(angle)));
            
            imgX = _arcCenter.x + ( (_radius - LINE_WIDTH ) * cos(degreesToRadians(angle + 8)));
            imgY = _arcCenter.y - ( (_radius - LINE_WIDTH ) * sin(degreesToRadians(angle + 8)));
            
        }
        else {
            x = _arcCenter.x + ( (_radius + LINE_WIDTH) * cos(degreesToRadians(angle)));
            y = _arcCenter.y - ( (_radius + LINE_WIDTH) * sin(degreesToRadians(angle)));
            
            imgX = _arcCenter.x + ( (_radius - LINE_WIDTH ) * cos(degreesToRadians(angle + 20)));
            imgY = _arcCenter.y - ( (_radius - LINE_WIDTH ) * sin(degreesToRadians(angle + 12)));
        }
    
        
        if(angle > 160) {
            x = _arcCenter.x + ( (_radius + LINE_WIDTH * 1.2) * cos(degreesToRadians(angle + 20)));
            y = _arcCenter.y - ( (_radius + LINE_WIDTH * 1.2) * sin(degreesToRadians(angle + 20)));
        }
        else if(angle > 120) {
            x = _arcCenter.x + ( (_radius + LINE_WIDTH * 1.7) * cos(degreesToRadians(angle + 10)));
            y = _arcCenter.y - ( (_radius + LINE_WIDTH * 1.7) * sin(degreesToRadians(angle + 10)));
        } else if(angle < 60 && angle >= 20 ){
            x = _arcCenter.x + ( (_radius + LINE_WIDTH * 1.7) * cos(degreesToRadians(angle - 12)));
            y = _arcCenter.y - ( (_radius + LINE_WIDTH * 1.7) * sin(degreesToRadians(angle - 12)));
        } else {
            x = _arcCenter.x + ( (_radius + LINE_WIDTH * 1.7) * cos(degreesToRadians(angle)));
            y = _arcCenter.y - ( (_radius + LINE_WIDTH * 1.7) * sin(degreesToRadians(angle)));
        }
        
        if(angle > 170) {
            lblMarkingValue.frame = CGRectMake(x - 7, y - 4 , lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
        } else if(angle < 10) {
            lblMarkingValue.frame = CGRectMake(x - 7, y + 7 , lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
        } else if(angle > 160 || angle <= 20){
            lblMarkingValue.frame = CGRectMake(x - 7, y + 5 , lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
        } else {
            lblMarkingValue.frame = CGRectMake(x - 5, y, lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
        }
        
        lbleVoucher.frame = CGRectMake(imgX - 4, imgY + 2, lbleVoucher.frame.size.width, lbleVoucher.frame.size.height);
//        [lbleVoucher_Data setCenter:CGPointMake(lbleVoucher.frame.size.width / 2, imgY + 4 + lbleVoucher.frame.size.height)];
        if(angle <= 70) {
            lbleVoucher.frame = CGRectMake(imgX - 20, imgY + 12, lbleVoucher.frame.size.width, lbleVoucher.frame.size.height);
//            [lbleVoucher_Data setCenter:CGPointMake(lbleVoucher.frame.size.width / 2, imgY + 14 + lbleVoucher.frame.size.height)];
        } else if(angle >= 135) {
            lbleVoucher.frame = CGRectMake(imgX - 8, imgY + 8, lbleVoucher.frame.size.width, lbleVoucher.frame.size.height);
//            [lbleVoucher_Data setCenter:CGPointMake(lbleVoucher.frame.size.width / 2, imgY + 10 + lbleVoucher.frame.size.height)];
        }

        [self.hxSpeedometerView addSubview:lbleVoucher];
        
        [lbleVoucher_Data setCenter:CGPointMake(lbleVoucher.frame.origin.x + lbleVoucher.frame.size.width / 2, imgY + 9 + lbleVoucher.frame.size.height)];
        if(angle <= 70) {
            [lbleVoucher_Data setCenter:CGPointMake(lbleVoucher.frame.origin.x + lbleVoucher.frame.size.width / 2, imgY + 19 + lbleVoucher.frame.size.height)];
        } else if(angle >= 135) {
            [lbleVoucher_Data setCenter:CGPointMake(lbleVoucher.frame.origin.x + lbleVoucher.frame.size.width / 2, imgY + 13 + lbleVoucher.frame.size.height)];
        }
        [self.hxSpeedometerView addSubview:lbleVoucher_Data];
        [NSLayoutConstraint constraintWithItem:lbleVoucher_Data attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:lbleVoucher attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0].active = YES;
        [NSLayoutConstraint constraintWithItem:lbleVoucher_Data attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:lbleVoucher attribute:NSLayoutAttributeBottom multiplier:1.0 constant:2].active = YES;

    } else if([markingFor isEqualToString:@"MaxTotal"]) {
        lblMarking.frame = CGRectMake(_arcCenter.x + _radius * 0.85, _arcCenter.y, LINE_WIDTH * 1.7, 1);
        
        lblMarkingValue.text = [NSString stringWithFormat:@"%.0f",overallTarget];
        [lblMarkingValue sizeToFit];
        
        int x = _arcCenter.x + ( (_radius * 1.2)) * cos(degreesToRadians(0));
        lblMarkingValue.frame = CGRectMake(x, _arcCenter.y - 2, lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
        
        UILabel *lbleVoucher = [[UILabel alloc] init];
        lbleVoucher.text = LOCALIZATION(C_CAROUSEL_EVOUCHER);
        lbleVoucher.textColor = COLOUR_VERYDARKGREY;
        lbleVoucher.font = FONT_B3;
        [lbleVoucher sizeToFit];
        
        UILabel *lbleVoucher_Data = [[UILabel alloc] init];
        lbleVoucher_Data.text = [NSString stringWithFormat:@"%@ %@",self.currency,[_numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[self.viewData objectForKey:@"HX8PromoVoucherValue4"] floatValue]]]];
        lbleVoucher_Data.textColor = COLOUR_VERYDARKGREY;
        lbleVoucher_Data.font = FONT_B3;
        [lbleVoucher_Data sizeToFit];
        
        lbleVoucher.frame = CGRectMake(_arcCenter.x + _radius * 0.45, _arcCenter.y - 10, lbleVoucher.frame.size.width, lbleVoucher.frame.size.height);
        [self.hxSpeedometerView addSubview:lbleVoucher];
        [lbleVoucher_Data setCenter:CGPointMake(lbleVoucher.frame.origin.x + lbleVoucher.frame.size.width / 2, _arcCenter.y + 5)];
        [self.hxSpeedometerView addSubview:lbleVoucher_Data];
        [NSLayoutConstraint constraintWithItem:lbleVoucher_Data attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:lbleVoucher attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0].active = YES;
        [NSLayoutConstraint constraintWithItem:lbleVoucher_Data attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:lbleVoucher attribute:NSLayoutAttributeBottom multiplier:1.0 constant:2].active = YES;
    }
    
    [self.hxSpeedometerView addSubview:lblMarkingValue];
    [self.hxSpeedometerView addSubview:lblMarking];
    
     [self.hxSpeedometerView layoutSubviews];
}

-(void) colourArc{
    CGFloat overAllAngle;
    if([[_viewData objectForKey:@"HX8TargetAchieved"] intValue] >= [[_viewData objectForKey:@"HX8PromoTarget4"] intValue]) {
        overAllAngle = 0;
    } else {
        overAllAngle = (180.0 / ([[_viewData objectForKey:@"HX8PromoTarget4"] floatValue] / [[_viewData objectForKey:@"HX8TargetAchieved"] intValue])) - 180.0;
    }
    
    UIBezierPath *overAllPath ;
    if(overAllAngle > -80) {
        overAllPath = [UIBezierPath bezierPathWithArcCenter:_arcCenter radius:_radius startAngle:degreesToRadians(180) endAngle:degreesToRadians(overAllAngle) clockwise:YES];
    } else if(overAllAngle < -110) {
        overAllPath = [UIBezierPath bezierPathWithArcCenter:_arcCenter radius:_radius startAngle:degreesToRadians(180) endAngle:degreesToRadians(overAllAngle) clockwise:YES];
    } else {
        overAllPath = [UIBezierPath bezierPathWithArcCenter:_arcCenter radius:_radius startAngle:degreesToRadians(180) endAngle:degreesToRadians(overAllAngle) clockwise:YES];
    }
    
    if(overAllAngle == 0) {
       overAllPath = [UIBezierPath bezierPathWithArcCenter:_arcCenter radius:_radius startAngle:degreesToRadians(180) endAngle:degreesToRadians(overAllAngle) clockwise:YES];
    }
    
    CAShapeLayer *progressLayer = [CAShapeLayer layer];
    progressLayer.path = overAllPath.CGPath;
    [progressLayer setStrokeColor: [[Helper colorFromHex:@"#FBCE07"] CGColor]];
    [progressLayer setFillColor:[UIColor clearColor].CGColor];
    [progressLayer setLineWidth:LINE_WIDTH];
    [self.hxSpeedometerView.layer addSublayer:progressLayer];
    
    [self.hxSpeedometerView setNeedsDisplay];
    [self.hxSpeedometerView setNeedsLayout];
    [self.hxSpeedometerView layoutSubviews];
}

-(void) currentMarking {
    int target_total = [[_viewData objectForKey:@"HX8PromoTarget4"] intValue];
    int target_1 = [[_viewData objectForKey:@"HX8PromoTarget1"] intValue];
    int target_2 = [[_viewData objectForKey:@"HX8PromoTarget2"] intValue];
    int target_3 = [[_viewData objectForKey:@"HX8PromoTarget3"] intValue];
    int achieved = [[_viewData objectForKey:@"HX8TargetAchieved"] intValue];
    
    if(target_1 != achieved && target_2 != achieved && target_3 != achieved && target_total != achieved && achieved != 0 && achieved < target_total) {
        CGFloat overAllAngle = 180.0 - (180.0 / ([[_viewData objectForKey:@"HX8PromoTarget4"] floatValue] / [[_viewData objectForKey:@"HX8TargetAchieved"] floatValue]));
        if(overAllAngle > 110) {
            overAllAngle -= 3;
        } else if(overAllAngle < 80) {
            overAllAngle += 3;
        }
        
        UIImageView *arrowImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"carousel_triangle_red.png"]];
        
        int x = _arcCenter.x + ( (_radius + LINE_WIDTH) * cos(degreesToRadians(overAllAngle)));
        int y = _arcCenter.y - ( (_radius + LINE_WIDTH) * sin(degreesToRadians(overAllAngle)));
        arrowImage.frame = CGRectMake(x,y,5,5);
        
        if(overAllAngle > 110) {
            arrowImage.transform =  CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(-50));
        }else if(overAllAngle < 80) {
            arrowImage.transform =  CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(50));
        }
        
        UILabel *currentMarking = [[UILabel alloc]init];
        currentMarking.font = FONT_B3;
        currentMarking.textColor = COLOUR_RED;
        currentMarking.text = [NSString stringWithFormat:@"%d",[[_viewData objectForKey:@"HX8TargetAchieved"] intValue]];
        [currentMarking sizeToFit];
        
        x = _arcCenter.x + ( (_radius + LINE_WIDTH * 1.7) * cos(degreesToRadians(overAllAngle)));
        y = _arcCenter.y - ( (_radius + LINE_WIDTH * 1.7) * sin(degreesToRadians(overAllAngle)));
        
        currentMarking.frame = CGRectMake(x, y, currentMarking.frame.size.width, currentMarking.frame.size.height);
        
        
        [self.hxSpeedometerView addSubview:arrowImage];
        [self.hxSpeedometerView addSubview:currentMarking];
    }
}



@end
