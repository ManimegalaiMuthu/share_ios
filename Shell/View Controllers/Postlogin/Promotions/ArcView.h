//
//  ArcView.h
//  Shell
//
//  Created by Ben on 21/5/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum ArcViewCursorType : NSUInteger {
    ArcViewCursorTypeNone,
    ArcViewCursorTypeArrow,
    ArcViewCursorTypeLine
} ArcViewCursorType;

@protocol ArcViewDelegate;
@protocol ArcViewDatasource;

@interface ArcView : UIView

@property (nonatomic, weak) id <ArcViewDelegate> delegate;
@property (nonatomic, weak) id <ArcViewDatasource> datasource;

-(void) redraw;

@end

@protocol ArcViewDelegate <NSObject>

@optional
-(void)arcView:(ArcView*) arcView didTappedAt: (CGPoint)location;

@end

@protocol ArcViewDatasource <NSObject>

@required
-(int)arcView:(ArcView*) arcView minValueFor:(int) view;
-(int)arcView:(ArcView*) arcView maxValueFor:(int) view;
-(int)arcView:(ArcView*) arcView progressValueFor:(int) view;

@optional
-(ArcViewCursorType)arcView:(ArcView*) arcView cursorTypeFor:(int) view;
-(CGFloat)arcView:(ArcView*) arcView barWidthFor:(int) view;
-(UIColor*)arcView:(ArcView*) arcView colorForArc:(int) view;
-(UIColor*)arcView:(ArcView*) arcView backgroundColorForArc:(int) view;
-(NSAttributedString*)arcView:(ArcView*) arcView centerTextForArcView:(int) view;
-(CGPoint)arcView:(ArcView*) arcView centerDisplacementFor:(int) view;
-(UIEdgeInsets) arcView:(ArcView*) arcView sidePaddingFor:(int) view;//not used

-(int)arcView:(ArcView*) arcView numberOfMarkersFor:(int) view;
-(CGFloat)arcView:(ArcView*) arcView paddingForMarkerLabel:(int) view;
-(int)arcView:(ArcView*) arcView valueForMarkerAtIndex:(int) index;
-(NSAttributedString*)arcView:(ArcView*) arcView outerTitleForMarkerAtIndex:(int) index;
-(NSAttributedString*)arcView:(ArcView*) arcView innerTitleForMarkerAtIndex:(int) index;
-(UIColor*)arcView:(ArcView*) arcView colorForMarkerAtIndex:(int) index;

@end
