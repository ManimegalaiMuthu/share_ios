//
//  HXPromotionView.h
//  Shell
//
//  Created by Nach on 14/5/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HXPromotionView : UIView

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *hxSpeedometerView;

@property NSDictionary *viewData;
@property UIBezierPath *speedoMeterPath;
@property double radius;
@property CGPoint arcCenter;
@property NSString *currency;
@property NSNumberFormatter *numberFormatter;

-(void) loadBar:(NSDictionary *) responseData isCarousel:(BOOL) isCarousel;

@end

NS_ASSUME_NONNULL_END
