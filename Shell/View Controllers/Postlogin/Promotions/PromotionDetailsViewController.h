//
//  PromotionDetailsViewController.h
//  Shell
//
//  Created by Nach on 22/4/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface PromotionDetailsViewController : BaseVC

@property NSDictionary *data;

@end

NS_ASSUME_NONNULL_END
