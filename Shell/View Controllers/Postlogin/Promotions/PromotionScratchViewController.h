//
//  PromotionScratchViewController.h
//  Shell
//
//  Created by Nach on 15/4/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "BaseVC.h"
#import "ScratchableView.h"

@protocol PromotionRedeemedDelegate <NSObject>

-(void) promotionIsRedeemed;

@end;

NS_ASSUME_NONNULL_BEGIN

@interface PromotionScratchViewController : BaseVC

@property id<PromotionRedeemedDelegate> delegate;
@property BOOL isGamification;

@end

NS_ASSUME_NONNULL_END
