//
//  InviteCustomerViewController.h
//  Shell
//
//  Created by Jeremy Lua on 14/3/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface InviteCustomerViewController : BaseVC

@property (weak, nonatomic) IBOutlet UITextField *txtVehicleNumber;
@property BOOL isPush;
@end
