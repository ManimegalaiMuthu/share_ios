//
//  ProductsTableViewController.h
//  Shell
//
//  Created by Edenred on 27/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductsTableViewController : BaseVC

@property BOOL fetchShareProducts;
@property NSArray *selectedProductArray;

@end

NS_ASSUME_NONNULL_END
