//
//  MyProfileViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 23/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"
@protocol MyProfileDelegate <NSObject>
-(void) changePasswordHandler;
-(void) textfieldHandler:(BOOL) isBeginEditing;
@end

@interface MyProfileViewController : BaseVC
@property (weak) id<MyProfileDelegate> delegate;
@property Member* member;

-(void) setupRegistrationForm;
@end
