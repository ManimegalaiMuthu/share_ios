//
//  ProfileTab_NewViewController.m
//  Shell
//
//  Created by Ben on 3/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProfileTab_NewViewController.h"

#import "FormsTableViewDelegate.h"
#import "ProfileInfo.h"
#import <CoreLocation/CoreLocation.h>

#define kUserID             @"UserID"
#define kDSRCode            @"DSRCode"
#define kUserPassword       @"UserPassword"
#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kEmailAddress       @"EmailAddress"
#define kDOB                @"DOB"
#define kDOBMonth           @"DOBMonth"
#define kDOBDay             @"DOBDay"
#define kCountryCode        @"CountryCode"
#define kContactPreference  @"ContactPreferences"

#define kChangePasswordCTA        @"ChangePasswordCTA"
#define kUpdateProfileCTA        @"UpdateProfileCTA"

#define kContactNumber          @"ContactNumber"
#define kCompanyEmailAddress    @"CompanyEmailAddress"
#define kWorkingHours       @"WorkingHours"
#define kOilChangePerMonth      @"OilChangePerMonth"
#define kOilChangePercentage    @"OilChangePercentage"
#define kHalfSyntheticOilChanges    @"HalfSyntheticOilChanges"
#define kShellHelixQty          @"ShellHelixQty"

#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"

//Address
#define kWorkshopLocation   @"Location"
#define kPostalCode         @"PostalCode"
#define kCompanyState              @"CompanyState"
#define kTown               @"CompanyCity"
#define kAddress1           @"Address1"
#define kAddress2           @"Address2"


//Bank Details
#define kBeneficiaryName        @"BeneficiaryName"
#define kValidateCheckboxes     @"ValidateCheckboxes"
#define kPANCardId              @"PANCardId"
#define kPANCardImage           @"PANCardImage"
#define kDrivingLicense         @"DrivingLicenseID"
#define kDrivingLicenseImage    @"DrivingLicenseImage"
#define kVoterID                @"VoterID"
#define kVoterIDImage           @"VoterIDImage"
#define kBeneficiaryName    @"BeneficiaryName"
#define kBankID             @"BankID"
#define kAccountNumber      @"AccountNumber"
#define kConfirmAccountNumber      @"ConfirmAccountNumber"
#define kBankAccountStatus  @"BankAccountStatus"

#define kIFSCCode           @"IFSCCode"
#define kBeneficiaryImage   @"BeneficiaryImage" //for UI only

//ID Coin
#define kPhotoID            @"PhotoId"
#define kPhotoIDName        @"DrivingLicenseName"
#define kPhotoIDAddress     @"DLicenceAddress"
#define kPhotoIDImage       @"DrivingLicenseImage"
#define kTaxPayerName       @"TaxPayerName"
#define kTaxPayerAddress    @"TaxPayerAddress"
#define kTaxImage           @"TaxImage"
#define kTaxNumber          @"TaxNumber"

//last section
#define kBankCheckBox   @"BankCheckBox" //for checkbox only


@interface ProfileTab_NewViewController () <FormsTableViewDelegate, WebServiceManagerDelegate, CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *regFormTableView;
@property FormsTableViewDelegate *tableViewDelegate;

@property CLLocationManager *locationManager;
@property CGFloat latitude;
@property CGFloat longitude;
@property NSMutableDictionary* bankDetails;
@property NSMutableDictionary* addressDetails;
@property NSMutableDictionary* companyDetails;
@property NSMutableDictionary* companyHours;
@property NSMutableDictionary* bankParams;

//ID Coins
@property NSMutableDictionary *documentIDDetails;
@property BOOL isPhotoIDEmpty;

@property BOOL isTO;
@end

@implementation ProfileTab_NewViewController
BOOL isDataReady;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableViewDelegate = [FormsTableViewDelegate new];
    
    isDataReady = false;
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager requestWhenInUseAuthorization];
    
    if(IS_COUNTRY(COUNTRYCODE_INDIA)) {
        self.bankDetails = self.workshopData[@"REG_Bank"];
        [self.bankDetails addEntriesFromDictionary:self.workshopData[@"REG_PAN"]];
    }
    self.addressDetails = self.workshopData[@"REG_CompanyAddress"];
    self.companyDetails = self.workshopData[@"REG_Company"];
    self.companyHours = self.workshopData[@"REG_CompanyWorkHours"];
    if (!self.companyHours ||
        [self.companyHours isKindOfClass: [NSNull class]])
        self.companyHours = @{@"WorkHourStart1":   @"0:00",
                                  @"WorkHourEnd1":     @"0:00",
                                  @"WeekDays1":        @"",
                                  @"PublicHoliday":    @"0",
                                  @"WorkHourStart2":   @"0:00",
                                  @"WorkHourEnd2":     @"0:00",
                                  @"WeekDays2":        @"",
                                  };
    
    if(IS_COUNTRY(COUNTRYCODE_INDONESIA)) {
        self.documentIDDetails = self.workshopData[@"REG_Document"];
        [self.documentIDDetails addEntriesFromDictionary:self.workshopData[@"REG_TAX"]];
        [self.documentIDDetails addEntriesFromDictionary:self.workshopData[@"REG_DrivingLicense"]];
    }

    self.isTO = (GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER);
    
    [self setupInterface];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(self.isBankDetails) {

        [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / Account Info",LOCALIZATION_EN(C_HEADER_MYPROFILE)] screenClass:nil];

    } else {

        [FIRAnalytics setScreenName:LOCALIZATION_EN(C_HEADER_MYPROFILE) screenClass:nil];

    }
}

-(void) setupInterface
{
    if(self.isBankDetails)
    {
        
        if(IS_COUNTRY(COUNTRYCODE_INDONESIA)) {
            [self.tableViewDelegate.headersArray addObject:@""];
            [self.tableViewDelegate.initializingArray addObject:@[@{kForm_Title: LOCALIZATION(C_COINS_PROFILE_HEADER),
                                                                    kForm_Type: @(kFORM_LABELONLY),
                                                                    kForm_Key: @"",
            }]];
            
            NSMutableArray *photoIDArray = [NSMutableArray new];
            [self setupPhotoIDArray:photoIDArray];
            [self.tableViewDelegate.headersArray addObject:LOCALIZATION(C_TITLE_PHOTOID)];
            [self.tableViewDelegate.initializingArray addObject:photoIDArray];
            
            [self.tableViewDelegate.headersArray addObject:@""];
            [self.tableViewDelegate.initializingArray addObject:@[@{kForm_Title: LOCALIZATION(C_TAX_TNC),
                                                                    kForm_Type: @(kFORM_LABELONLY),
                                                                    kForm_Key: @"TaxIDLabel",
                                                                    kForm_Hyperlinks: @{LOCALIZATION(C_TAX_TNC_LINK): @"coinbackInfo",},
            }]];
            
            NSMutableArray *taxArray = [NSMutableArray new];
            [self setupTaxArray:taxArray];
            [self.tableViewDelegate.headersArray addObject:LOCALIZATION(C_TITLE_TAXID)];
            [self.tableViewDelegate.initializingArray addObject:taxArray];
            
            [self.tableViewDelegate.headersArray addObject:@""];
            [self.tableViewDelegate.initializingArray addObject:@[@{
                                                                      kForm_Title: LOCALIZATION(C_MECH_UPDATEDETAILS),
                                                                      kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                                                      kForm_Key: @"UpdateCoin",
                                                                      kForm_Enabled: self.isPhotoIDEmpty ? @(YES) : (([self.documentIDDetails[@"DrivingLicenseStatus"] isEqualToString:@""] || [self.documentIDDetails[@"DrivingLicenseStatus"] isEqualToString:@"4"] || [self.documentIDDetails[@"TaxStatus"] isEqualToString:@""] || [self.documentIDDetails[@"TaxStatus"] isEqualToString:@"4"]) ? @(YES) : @(NO)),
                                                                      kForm_TextColour: COLOUR_WHITE,
                                                                      kForm_BackgroundColour: COLOUR_RED,
            }]];
            
        } else {
            NSMutableArray *bankArray = [NSMutableArray new];
            [self setupBankArray:bankArray];
            [self.tableViewDelegate.headersArray addObject:LOCALIZATION(C_HEADER_BANKDETAILS)];
            [self.tableViewDelegate.initializingArray addObject:bankArray];
            
            NSMutableArray *cardValidationArray = [[NSMutableArray alloc] init];
            [self setupAccountValidationArray: cardValidationArray];
            [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_FORM_ACCTVERIFY)];
            [self.tableViewDelegate.initializingArray addObject: cardValidationArray];
            
            NSMutableArray *lastSectionArray = [NSMutableArray new];
            [self setupLastSectionArray: lastSectionArray];
            [self.tableViewDelegate.headersArray addObject: @""]; //0 char to skip section header
            [self.tableViewDelegate.initializingArray addObject: lastSectionArray];
        }
           
    }
    else
    {
        if(self.isTO) {
            NSMutableArray *workshopProfileArray = [NSMutableArray new];
            [self setupWorkshopProfileArray:workshopProfileArray];
            [self.tableViewDelegate.headersArray addObject: @"Workshop Profile"];
            [self.tableViewDelegate.initializingArray addObject:workshopProfileArray];
            
            NSMutableArray *profileArray = [NSMutableArray new];
            [self setupProfileArray: profileArray];
            [self.tableViewDelegate.headersArray addObject: @"My Profile"]; //more than 1 char, to show required fields label
            [self.tableViewDelegate.initializingArray addObject: profileArray];
            
        } else {
            NSMutableArray *profileArray = [NSMutableArray new];
            [self setupProfileArray: profileArray];
            [self.tableViewDelegate.headersArray addObject: @"Profile"]; //more than 1 char, to show required fields label
            [self.tableViewDelegate.initializingArray addObject: profileArray];
        }
        
       
        if(GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER) {
            NSMutableArray *addressArray = [NSMutableArray new];
            [self setupAddressArray:addressArray];
            [self.tableViewDelegate.headersArray addObject: @"Address"];
            [self.tableViewDelegate.initializingArray addObject:addressArray];
        }
        
        if (!IS_COUNTRY(COUNTRYCODE_INDIA))
        {
            NSMutableArray *contactPrefArray = [NSMutableArray new];
            [self setupContactPrefArray:contactPrefArray];
            [self.tableViewDelegate.headersArray addObject: @""];
            [self.tableViewDelegate.initializingArray addObject:contactPrefArray];
        } else if(GET_PROFILETYPE == kPROFILETYPE_DMR || GET_PROFILETYPE == kPROFILETYPE_DSR) {
            NSMutableArray *contactPrefArray = [NSMutableArray new];
            [self setupContactPrefArray:contactPrefArray];
            [self.tableViewDelegate.headersArray addObject: @""];
            [self.tableViewDelegate.initializingArray addObject:contactPrefArray];
        }
        
        NSMutableArray *lastSectionArray = [NSMutableArray new];
        [self setupLastSectionArray: lastSectionArray];
        [self.tableViewDelegate.headersArray addObject: @""]; //0 char to skip section header
        [self.tableViewDelegate.initializingArray addObject: lastSectionArray];
    }
    
    self.regFormTableView.delegate = self.tableViewDelegate;
    self.regFormTableView.dataSource = self.tableViewDelegate;
    
    [self.tableViewDelegate registerNib: self.regFormTableView];
    [self.tableViewDelegate setDelegate: self];
    
    if(@available(iOS 11, *))
    {
        self.regFormTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
//    [self.regFormTableView setContentInset:UIEdgeInsetsMake(-45, 0, -30, 0)];
    [self.regFormTableView setContentInset:UIEdgeInsetsMake(0, 0, -30, 0)];
    
    self.regFormTableView.backgroundColor = [UIColor clearColor];
    self.regFormTableView.backgroundView = nil;
    [self.regFormTableView reloadData];
}

-(void) setupProfileArray: (NSMutableArray *) profileArray
{
    [profileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SALUTATION), //lokalised 30 jan
                               kForm_Type: @(kREGISTRATION_DROPDOWN),
                               kForm_Placeholder: LOCALIZATION(C_PROFILE_SALUTATION_PLACEHOLDER),  //lokalised 30 jan
                               kForm_Key: kSalutation,
                               kForm_Value: [mSession convertToSalutation: self.member.salutation],
                               kForm_Optional: @(YES),
                               }];
    [profileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_FIRSTNAME),  //lokalised 30 jan
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_HasDelegate: @(YES),
                               kForm_Key: kFirstName,
                               kForm_Value: self.member.firstName,
                               }];
    [profileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_LASTNAME),  //lokalised 30 jan
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_HasDelegate: @(YES),
                               kForm_Key: kLastName,
                               kForm_Value: self.member.lastName,
                               }];
//    [profileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_MOBILENUM),
//                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
//                               kForm_Key: kMobileNumber,
//                               kForm_Enabled: @(NO),
//                               kForm_Value: [NSString stringWithFormat: @"%@%@", [[[mSession convertCountryCodeToCountryDialingCode: GET_COUNTRY_CODE] componentsSeparatedByString: @" "] firstObject], self.member.mobileNumber],
//                               kForm_Enabled: @(YES),
//                               }];
    [profileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_DOB),  //lokalised 30 jan
                               kForm_Type: @(kREGISTRATION_BIRTHDATE),
                               kForm_Key: kDOB,
                               kForm_Value: [self convertDateForDisplay:self.member.dobMonth day:self.member.dobDay],
                               kForm_Optional: @(YES),
                               }];
    
    [profileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_MOBILENUM),  //lokalised 30 jan
                                       kForm_Type: @(kREGISTRATION_MOBNUM),
                                       kForm_Key: kMobileNumber,
                                       kForm_HasDelegate: @(YES),
                                       kForm_TextfieldEnabled: @(NO), //
                                       kForm_CountryCode: [mSession convertCountryCodeToCountryDisplayName: GET_COUNTRY_CODE],
                                       kForm_MobileNumber: self.member.mobileNumber,
                                       kForm_Enabled: @(NO),
                                       }];   //Mobile number
    
    [profileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_EMAILADDRESS),  //lokalised 30 jan
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_HasDelegate: @(YES),
                               kForm_Key: kEmailAddress,
                               kForm_Keyboard: @(UIKeyboardTypeEmailAddress),
                               kForm_Value: self.member.emailAddress,
                               kForm_Optional: @(YES),
                               kForm_Placeholder: LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER),  //lokalised 30 jan
                               }];
//    if(!self.isTO) {
//        [profileArray addObject:@{kForm_Title: LOCALIZATION(C_PROFILE_WORKINGHOURS),
//                                          kForm_Type: @(kREGISTRATION_WORKINGHOURS),
//                                          kForm_Key: kWorkingHours,
//                                  kForm_Value: @"", //using kForm_WorkingHours instead
//                                          kForm_WorkingHours: [self.companyHours isKindOfClass:[NSNull class]] ? @"" : self.companyHours,
//                                          }];
//        [profileArray addObject:@{kForm_Title: LOCALIZATION(C_PROFILE_OILCHANGEPERDAY),
//                                          kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
//                                          kForm_Key: @"OilChangePerDay",
//                                          kForm_Value:[self.companyDetails[@"OilChangePerMonth"] isKindOfClass:[NSNull class]] ? @"" : self.companyDetails[@"OilChangePerMonth"],
//                                          }];
//        [profileArray addObject:@{kForm_Title: LOCALIZATION(C_PROFILE_SHELLHELIX),
//                                          kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
//                                          kForm_Key: @"ShellChangePerDay",
//                                          kForm_Value: [self.companyDetails[@"ShellHelixQty"] isKindOfClass:[NSNull class]] ? @"" : self.companyDetails[@"ShellHelixQty"],
//                                          }];
    
//    [profileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_OILCHANGEPERDAY),
//                                       kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER),
//                                       kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
//                                       kForm_Key: kOilChangePerMonth,
//                                       kForm_Keyboard: @(UIKeyboardTypeNumberPad),
//                               kForm_Value: [[self.companyDetails objectForKey: kOilChangePerMonth] isKindOfClass: [NSNull class]] ? @"" :  [self.companyDetails objectForKey: kOilChangePerMonth] //self.companyInfo.oilChangePerMonth,
//                                       }];
//
//    [profileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SHELLHELIX),
//                               kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER),
//                               kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
//                               kForm_Key: kShellHelixQty,
//                               kForm_Keyboard: @(UIKeyboardTypeNumberPad),
//                               kForm_Value: [[self.companyDetails objectForKey: kShellHelixQty] isKindOfClass: [NSNull class]] ? @"" :  [self.companyDetails objectForKey: kShellHelixQty] //self.companyInfo.shellHelixQty,
//                               }];
//
//    [profileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_OILCHANGEPERCENT),
//                                       kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER),
//                                       kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
//                                       kForm_Key: kOilChangePercentage,
//                                       kForm_Keyboard: @(UIKeyboardTypeDecimalPad),
//                                       kForm_Value: [[self.companyDetails objectForKey: kOilChangePercentage] isKindOfClass: [NSNull class]] ? @"" :  [self.companyDetails objectForKey: kOilChangePercentage] //self.companyInfo.oilChangePercentage,
//                                       }];
//
//    [profileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SEMIOILCHANGEPERCENT),
//                                       kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER),
//                                       kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
//                                       kForm_Key: kHalfSyntheticOilChanges,
//                                       kForm_Keyboard: @(UIKeyboardTypeDecimalPad),
//                                       kForm_Value: [[self.companyDetails objectForKey: kHalfSyntheticOilChanges] isKindOfClass: [NSNull class]] ? @"" :  [self.companyDetails objectForKey: kHalfSyntheticOilChanges] //self.companyInfo.halfSyntheticOilChanges,
//                                       }];
//    }
}

-(void) setupContactPrefArray: (NSMutableArray*) contactPrefArray {
    //contact preference
    [contactPrefArray addObject: @{//kForm_Title: LOCALIZATION(C_PROFILE_CONTACT_PREFERENCE),
                               kForm_Type: @(kREGISTRATION_CONTACTPREF),
                               kForm_Key: kContactPreference,
                               kForm_Value: self.member.contactPreference, //contact pref bitvalue strings
                               kForm_BackgroundColour: COLOUR_WHITE,
                               kForm_Optional: @(YES),
                               }];
}


-(void) setupWorkshopProfileArray: (NSMutableArray*) workshopProfileArray
{
    [workshopProfileArray addObject:@{kForm_Title: LOCALIZATION(C_PROFILE_COMPANYNAME),  //lokalised 30 jan
                                      kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                      kForm_Key: kCompanyName,
                                      kForm_HasDelegate: @(YES),
                                      kForm_Value: self.companyDetails[kCompanyName],
                                      }];
    [workshopProfileArray addObject:@{kForm_Title: LOCALIZATION(C_PROFILE_CONTACTNUMBER),  //lokalised 30 jan
                                      kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                      kForm_HasDelegate: @(YES),
                                      kForm_Key: kContactNumber,
                                      kForm_Value: self.companyDetails[kContactNumber],
                                      kForm_Optional: @(YES),
                                      }];
    [workshopProfileArray addObject:@{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPEMAILADDRESS),  //lokalised 30 jan
                                      kForm_Placeholder: LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER),  //lokalised 30 jan
                                      kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                      kForm_HasDelegate: @(YES),
                                      kForm_Key: kCompanyEmailAddress,
                                      kForm_Value: self.companyDetails[kCompanyEmailAddress],
                                      kForm_Optional: @(YES),
                                      }];
//    [workshopProfileArray addObject:@{kForm_Title: LOCALIZATION(C_PROFILE_WORKINGHOURS),
//                                      kForm_Type: @(kREGISTRATION_WORKINGHOURS),
//                                      kForm_Key: kWorkingHours,
//                                      kForm_Value: [self.companyHours isKindOfClass:[NSNull class]] ? @"" : self.companyHours,
//                                      }];
    [workshopProfileArray addObject:@{kForm_Title: LOCALIZATION(C_PROFILE_WORKINGHOURS),  //lokalised 30 jan
                              kForm_Type: @(kREGISTRATION_WORKINGHOURS),
                              kForm_Key: kWorkingHours,
                              kForm_Value: @"", //using kForm_WorkingHours instead
                              kForm_WorkingHours: self.companyHours,
                              }];
    
    [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_OILCHANGEPERDAY),  //lokalised 30 jan
                               kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER),  //lokalised 30 jan
                               kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
                               kForm_HasDelegate: @(YES),
                               kForm_Key: kOilChangePerMonth,
                               kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                               kForm_Value: [[self.companyDetails objectForKey: kOilChangePerMonth] isKindOfClass: [NSNull class]] ? @"" :  [self.companyDetails objectForKey: kOilChangePerMonth] //self.companyInfo.oilChangePerMonth,
                               }];
    
    [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SHELLHELIX),  //lokalised 30 jan
                                       kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER),  //lokalised 30 jan
                                       kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
                                       kForm_HasDelegate: @(YES),
                                       kForm_Key: kShellHelixQty,
                                       kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                       kForm_Value: [[self.companyDetails objectForKey: kShellHelixQty] isKindOfClass: [NSNull class]] ? @"" :  [self.companyDetails objectForKey: kShellHelixQty] //self.companyInfo.shellHelixQty,
                                       }];
    
    [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_OILCHANGEPERCENT),  //lokalised 30 jan
                               kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER),  //lokalised 30 jan
                               kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
                               kForm_HasDelegate: @(YES),
                               kForm_Key: kOilChangePercentage,
                               kForm_Keyboard: @(UIKeyboardTypeDecimalPad),
                               kForm_Value: [[self.companyDetails objectForKey: kOilChangePercentage] isKindOfClass: [NSNull class]] ? @"" :  [self.companyDetails objectForKey: kOilChangePercentage] //self.companyInfo.oilChangePercentage,
                               }];
    
    [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SEMIOILCHANGEPERCENT),  //lokalised 30 jan
                               kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER),  //lokalised 30 jan
                               kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
                               kForm_HasDelegate: @(YES),
                               kForm_Key: kHalfSyntheticOilChanges,
                               kForm_Keyboard: @(UIKeyboardTypeDecimalPad),
                               kForm_Value: [[self.companyDetails objectForKey: kHalfSyntheticOilChanges] isKindOfClass: [NSNull class]] ? @"" :  [self.companyDetails objectForKey: kHalfSyntheticOilChanges] //self.companyInfo.halfSyntheticOilChanges,
                               }];
}
-(void) setupAddressArray: (NSMutableArray*) addressArray
{
    NSString* locationString = [NSString stringWithFormat: @"%@, %@", self.addressDetails[@"Latitude"], self.addressDetails[@"Longitude"]];
    
    self.latitude = [self.addressDetails[@"Latitude"] doubleValue];
    self.longitude = [self.addressDetails[@"Longitude"] doubleValue];
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_LOCATION),  //lokalised 30 jan
                               kForm_Type: @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                               kForm_TextfieldEnabled: @(NO),
                               kForm_Icon: @"icon-location",
                               kForm_Key: kWorkshopLocation,
                               kForm_Value: locationString,
                               kForm_Enabled: @(YES),
                               //                                       kForm_Value: [NSString stringWithFormat: @"%f, %f", self.companyAddress.latitude, self.companyAddress.longitude],
                               }];    //location
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_POSTALCODE),  //lokalised 30 jan
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_HasDelegate: @(YES),
                               kForm_Key: kPostalCode,
                               kForm_Value: self.addressDetails[@"PostalCode"],
                               kForm_HasDelegate : @(YES), //textfield delegate for postal code
                               }]; //postal code
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_STATE),  //lokalised 30 jan
                               kForm_Type: @(kREGISTRATION_DROPDOWN),
                               kForm_Placeholder: LOCALIZATION(C_PROFILE_STATE_PLACEHOLDER),  //lokalised 30 jan
                               kForm_Key: kCompanyState,
                               kForm_Value: [mSession convertToStateName:self.addressDetails[@"CompanyState"] stateDetailedListArray:GET_LOADSTATE],
                               kForm_Value: LOCALIZATION(C_PROFILE_STATE_PLACEHOLDER),
                               }];
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_TOWN),  //lokalised 30 jan
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_HasDelegate: @(YES),
                               kForm_Key: kTown, //kCity?
                               kForm_Value: self.addressDetails[@"CompanyCity"],
                               //                                       kForm_Value: self.companyAddress.companyCity,
                               }];
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_ADDRESSLINE1),  //lokalised 30 jan
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_HasDelegate: @(YES),
                               kForm_Key: kAddress1,
                               kForm_Value: self.addressDetails[@"Address1"],
                               //                                       kForm_Value: self.companyAddress.address1,
                               }]; //address line 1
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_ADDRESSLINE2),  //lokalised 30 jan
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_HasDelegate: @(YES),
                               kForm_Key: kAddress2,
                               kForm_Value: self.addressDetails[@"Address2"],
                               //                                       kForm_Value: self.companyAddress.address2,
                               kForm_Optional: @(YES),
                               }];
    
}
-(void) setupLastSectionArray: (NSMutableArray *)lastSectionArray
{
    if(!self.isBankDetails) {
        [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_CHANGEPASSWORD),  //lokalised 30 jan
                                       kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                       kForm_Key: kChangePasswordCTA,
                                       kForm_Enabled: @(YES),
                                       kForm_TextColour: COLOUR_VERYDARKGREY,
                                       kForm_BackgroundColour: COLOUR_YELLOW,
                                       }];
        
        [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_UPDATEPROFILE),  //lokalised 30 jan
                                       kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                       kForm_Key: kUpdateProfileCTA,
                                       kForm_Enabled: @(YES),
                                       kForm_TextColour: COLOUR_WHITE,
                                       kForm_BackgroundColour: COLOUR_RED,
                                       }];
    }
    else {
        
        [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BANKINFO),    //lokalised 30 jan
                                kForm_Type: @(kREGISTRATION_TNCCHECKBOX),
                                kForm_Key: kBankCheckBox,
                                kForm_Enabled: @(YES),
                                kForm_CheckboxSelected : @([[self.workshopData objectForKey:@"BankInfoSharing"] boolValue]),
                                }];
        
        
        [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_UPDATE),   //lokalised 30 jan
                                       kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                       kForm_Key: @"UpdateBank",
                                       kForm_Enabled: @(YES),
                                       kForm_TextColour: COLOUR_WHITE,
                                       kForm_BackgroundColour: COLOUR_RED,
                                       }];
    }
}

-(void) setupBankArray:(NSMutableArray*)bankArray
{
//    NSString *panCardIdString = @"";
//    if (![self.bankDetails[@"PANCardId"] isKindOfClass:[NSNull class]])
//    {
//        panCardIdString = self.bankDetails[@"PANCardId"];
//        for (int i = 0; i < [panCardIdString length] - 4; i ++)
//        {
//            panCardIdString = [panCardIdString stringByReplacingCharactersInRange: NSMakeRange(i, 1) withString: @"*"];
//        }
////        panCardIdString = self.bankDetails[@"PANCardId"];
//    }
    
    [bankArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BENEFICIARYNAME),    //lokalised 30 jan
                            kForm_Type: @(kREGISTRATION_STATUSVEIFICATION),
                            kForm_HasDelegate: @(YES),
                            kForm_Key: kBeneficiaryName,
                            kForm_Placeholder: LOCALIZATION(C_FORM_BENEFICIARYNAME_PLACEHOLDER),    //lokalised 30 jan
                            kForm_Value: [self.bankDetails[@"BeneficiaryName"] isKindOfClass:[NSNull class]] ? @"" : self.bankDetails[@"BeneficiaryName"],
                            kForm_Optional: @(YES),
                            kForm_StatusVerify: [self.bankDetails[@"BankAccountStatus"] isKindOfClass:[NSNull class]] ? @"" : self.bankDetails[@"BankAccountStatus"],
                            }];
    
    [bankArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BENEFICIARYACCTNUM),    //lokalised 30 jan
                            kForm_Type: @(kREGISTRATION_TEXTFIELD),
                            kForm_HasDelegate: @(YES),
                            kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                            kForm_Key: kAccountNumber,
                            kForm_SecureEntry: @(YES),
                            kForm_Value: [self.bankDetails[@"AccountNumber"] isKindOfClass:[NSNull class]] ? @"" : self.bankDetails[@"AccountNumber"],
                            kForm_HasDelegate : @(IS_COUNTRY(COUNTRYCODE_INDIA)), //textfield delegate for account number
                            kForm_Optional: @(YES),
                            }];
    
    [bankArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_CONFIRMBENEFICIARYNUM),  //lokalised 4 feb
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_HasDelegate: @(YES),
                                   kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                   kForm_Key: kConfirmAccountNumber,
                                   kForm_Value: [self.bankDetails[@"AccountNumber"] isKindOfClass:[NSNull class]] ? @"" : self.bankDetails[@"AccountNumber"],
                                   kForm_Optional: @(YES),
                                   }];
    
    [bankArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BENEFICIARYIFSC),    //lokalised 30 jan
                            kForm_Type: @(kREGISTRATION_TEXTFIELD),
                            kForm_HasDelegate: @(YES),
                            kForm_Key: kIFSCCode,
                            kForm_Value: [self.bankDetails[@"IFSCCode"] isKindOfClass:[NSNull class]] ? @"" : self.bankDetails[@"IFSCCode"],
                            kForm_HasDelegate : @(IS_COUNTRY(COUNTRYCODE_INDIA)), //textfield delegate for ifsc code
                            kForm_Optional: @(YES),
                            }];
    
    [bankArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BANKNAME),    //lokalised 30 jan
                            kForm_Type: @(kREGISTRATION_DROPDOWN),
                            kForm_Key: kBankID,
                            kForm_Placeholder: LOCALIZATION(C_DROPDOWN_BANKNAME),    //lokalised 30 jan
                            //                            kForm_Value: LOCALIZATION(C_DROPDOWN_BANKNAME),
                            kForm_Enabled: @(NO),
                            kForm_Value: [self.bankDetails[@"BankID"] isKindOfClass:[NSNull class]] ? @"" : [mSession convertToBankName: [[self.bankDetails objectForKey: @"BankID"] intValue]],
                            kForm_Optional: @(YES),
                            }];
    
    
    [bankArray addObject: @{kForm_Type: @(kFORM_PROFILE_IMAGE),
                            kForm_Key: kBeneficiaryImage,
                            kForm_Value: @"cheque_sample",
                            }];
    
    
}

-(void)setupPhotoIDArray:(NSMutableArray*)photoIDArray {
    BOOL allowEditing = YES;
    if(![self.documentIDDetails[@"DrivingLicenseStatus"] isKindOfClass:[NSNull class]] && ![self.documentIDDetails[@"DrivingLicenseStatus"] isEqualToString:@""]) {
        [photoIDArray addObject: @{
                               kForm_Type: @(kFORM_STATUS),
                               kForm_Key: @"FormStatusPhotoID",
                               kForm_Value: self.documentIDDetails[@"DrivingLicenseStatus"],
                               kForm_Optional: @(YES),
        }];
        if(![self.documentIDDetails[@"DrivingLicenseStatus"] isEqualToString:@"4"])
            allowEditing = NO;
    }
    
    if([self.documentIDDetails[@"PhotoId"] isKindOfClass:[NSNull class]] || self.documentIDDetails[@"PhotoId"] == nil || [self.documentIDDetails[@"PhotoId"] isEqualToString:@""])
        self.isPhotoIDEmpty = YES;
    else
        self.isPhotoIDEmpty = NO;
    
    [photoIDArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_PHOTOID),
                              kForm_Type: @(kREGISTRATION_TEXTFIELD),
                              kForm_HasDelegate: @(YES),
                              kForm_Key: kPhotoID,
                              kForm_Placeholder: LOCALIZATION(C_FORM_PHOTOID),
                              kForm_Value: ([self.documentIDDetails[@"PhotoId"] isKindOfClass:[NSNull class]] || self.documentIDDetails[@"PhotoId"] == nil) ? @"" : self.documentIDDetails[@"PhotoId"],
                              kForm_Optional:@(YES),
                              kForm_Enabled: (self.isPhotoIDEmpty) ? @(YES) : @(allowEditing),
           }];
    
    [photoIDArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_NAME),
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_Key: kPhotoIDName,
                               kForm_HasDelegate: @(YES),
                               kForm_Placeholder: LOCALIZATION(C_FORM_NAME_PLACEHOLDER),
                               kForm_Value: ([self.documentIDDetails[@"DrivingLicenseName"] isKindOfClass:[NSNull class]] || self.documentIDDetails[@"DrivingLicenseName"] == nil) ? @"" : self.documentIDDetails[@"DrivingLicenseName"],
                               kForm_Enabled: @(allowEditing),
                               kForm_Optional: @(YES),
        }];
    
    [photoIDArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_ADDRESS),
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_HasDelegate: @(YES),
                               kForm_Key: kPhotoIDAddress,
                               kForm_Placeholder: LOCALIZATION(C_FORM_ADDRESS_PLACEHOLDER),
                               kForm_Value: ([self.documentIDDetails[@"DLicenceAddress"] isKindOfClass:[NSNull class]] || self.documentIDDetails[@"DLicenceAddress"] == nil) ? @"" : self.documentIDDetails[@"DLicenceAddress"],
                               kForm_Enabled: @(allowEditing),
                               kForm_Optional: @(YES),
        }];
    
    [photoIDArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_PHOTOIDFRONT),
                               kForm_Type: @(kFORM_PANCARD_IMAGE),
                               kForm_HasDelegate: @(YES),
                               kForm_Value: ([self.documentIDDetails[@"DrivingLicenseImage"] isKindOfClass:[NSNull class]] || self.documentIDDetails[@"DrivingLicenseImage"] == nil) ? @"" : self.documentIDDetails[@"DrivingLicenseImage"],
                               kForm_Key: kPhotoIDImage,
                               kForm_Optional:@(YES),
                               kForm_Enabled: @(allowEditing),
        }];
    
}

-(void)setupTaxArray:(NSMutableArray*)taxArray {
    BOOL allowEditing = YES;
    if(![self.documentIDDetails[@"TaxStatus"] isKindOfClass:[NSNull class]] && ![self.documentIDDetails[@"TaxStatus"] isEqualToString:@""]) {
        [taxArray addObject: @{
                               kForm_Type: @(kFORM_STATUS),
                               kForm_Key: @"FormStatusTax",
                               kForm_Value: self.documentIDDetails[@"TaxStatus"],
        }];
        if(![self.documentIDDetails[@"TaxStatus"] isEqualToString:@"4"])
            allowEditing = NO;
    }
    
    [taxArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_TAXID),
                           kForm_Type: @(kREGISTRATION_TEXTFIELD),
                           kForm_Key: kTaxNumber,
                           kForm_HasDelegate: @(YES),
                           kForm_Placeholder: LOCALIZATION(C_FORM_TAXID_PLACEHOLDER),
                           kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                           kForm_Value: ([self.documentIDDetails[@"TaxNumber"] isKindOfClass:[NSNull class]] || self.documentIDDetails[@"TaxNumber"] == nil) ? @"" : self.documentIDDetails[@"TaxNumber"],
                           kForm_Optional:@(YES),
                           kForm_Enabled: @(allowEditing),
        }];
    
    [taxArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_TAXPAYERNAME),
                           kForm_Type: @(kREGISTRATION_TEXTFIELD),
                           kForm_HasDelegate: @(YES),
                           kForm_Key: kTaxPayerName,
                           kForm_Placeholder: LOCALIZATION(C_FORM_TAXPAYERNAME_PLACEHOLDER),
                           kForm_Value: ([self.documentIDDetails[@"TaxPayerName"] isKindOfClass:[NSNull class]] || self.documentIDDetails[@"TaxPayerName"] == nil) ? @"" : self.documentIDDetails[@"TaxPayerName"],
                           kForm_Optional:@(YES),
                           kForm_Enabled: @(allowEditing),
        }];
    
    [taxArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_TAXPAYERADDRESS),
                           kForm_Type: @(kREGISTRATION_TEXTFIELD),
                           kForm_HasDelegate: @(YES),
                           kForm_Key: kTaxPayerAddress,
                           kForm_Placeholder: LOCALIZATION(C_FORM_TAXPAYERADDRESS_PLACEHOLDER),
                           kForm_Value: ([self.documentIDDetails[@"TaxPayerAddress"] isKindOfClass:[NSNull class]] || self.documentIDDetails[@"TaxPayerAddress"] == nil) ? @"" : self.documentIDDetails[@"TaxPayerAddress"],
                           kForm_Optional:@(YES),
                           kForm_Enabled: @(allowEditing),
        }];
    
    [taxArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_TAXIDFRONT),
                           kForm_Type: @(kFORM_PANCARD_IMAGE),
                           kForm_Value: ([self.documentIDDetails[@"TaxImage"] isKindOfClass:[NSNull class]] || self.documentIDDetails[@"TaxImage"] == nil) ? @"" : self.documentIDDetails[@"TaxImage"],
                           kForm_Key: kTaxImage,
                           kForm_Optional:@(YES),
                           kForm_Enabled: @(allowEditing),
        }];
    
    [taxArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_COINAGREE),
                           kForm_Type: @(kREGISTRATION_TNCCHECKBOX),
                           kForm_Optional: @(NO),
                           kForm_Key: kBankCheckBox,
                           kForm_CheckboxSelected : @([[self.documentIDDetails objectForKey:@"DocInfoSharing"] boolValue]),
                           kForm_Enabled: [self.documentIDDetails[@"DrivingLicenseStatus"] isEqualToString:@""] || [self.documentIDDetails[@"DrivingLicenseStatus"] isEqualToString:@"4"] || [self.documentIDDetails[@"TaxStatus"] isEqualToString:@""] || [self.documentIDDetails[@"TaxStatus"] isEqualToString:@"4"] ? @(YES) : @(NO),
        }];
    
}


#pragma mark - bank account validation
-(void) setupAccountValidationArray: (NSMutableArray *) cardValidationArray {
    
    NSDictionary *panDetails = self.workshopData[@"REG_PAN"];
    NSDictionary *drivingLicenseDetails = self.workshopData[@"REG_DrivingLicense"];
    NSDictionary *voterDetails = self.workshopData[@"REG_VoterID"];
    
    NSString *checkboxString = @"";
    if(![panDetails[@"PANCardId"] isKindOfClass: [NSNull class]])
        checkboxString = [checkboxString stringByAppendingString:@"0"];
    if(![drivingLicenseDetails[@"DrivingLicenseID"] isKindOfClass: [NSNull class]])
        checkboxString = [checkboxString isEqualToString:@""] ? [checkboxString stringByAppendingString:@"1"] : [checkboxString stringByAppendingString:@",1"];
    if(![voterDetails[@"VoterID"] isKindOfClass: [NSNull class]])
        checkboxString = [checkboxString isEqualToString:@""] ? [checkboxString stringByAppendingString:@"2"] : [checkboxString stringByAppendingString:@",2"];
    
    
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(@"Validate Account by"),  //lokalised 4 feb
                                      kForm_Type: @(kFORM_MULTICHECKBOX),
                                      kForm_Key: kValidateCheckboxes,
                                      kForm_Enabled: @(YES),
                                      kForm_HasDelegate: @(YES),
                                      kForm_CheckboxLabels: @[LOCALIZATION(C_FORM_PANCARD),
                                                              LOCALIZATION(C_FORM_DRIVINGLICENSEID),
                                                              LOCALIZATION(C_FORM_VOTERID),
                                                              ],
                                      kForm_LinkedCellKeys: @[kPANCardId,
                                                              kDrivingLicense,
                                                              kVoterID,
                                                              ],
                                      kForm_Value: checkboxString,
                                      }];
    
    
  /*  [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_PANCARDID), //lokalised 8 Feb
                                      kForm_Type: @(kFORM_PANCARD_ID_NEW),
                                      kForm_Value: panDetails[@"PANCardId"],
                                      kForm_Key: kPANCardId,
                                      kForm_Optional: @(YES),  //controlled in formdelegate level.
                                      }];*/
    
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_PANCARDID), //lokalised 8 Feb
                                      kForm_Type: @(kFORM_PANCARD_ID_NEWSTATUS),
                                      kForm_Value: panDetails[@"PANCardId"],
                                      //  kForm_Value: @"ABCDF",
                                      kForm_Key: kPANCardId,
                                      kForm_StatusVerify: [self.bankDetails[@"BankAccountStatus"] isKindOfClass:[NSNull class]] ? @"" : self.bankDetails[@"BankAccountStatus"],
                                      kForm_Optional: @(YES),  //controlled in formdelegate level.
                                    }];
    
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_PANCARDIMAGE),  //lokalised 8 Feb
                                      kForm_Type: @(
                                          kFORM_PANCARD_IMAGE),
                                      kForm_Value: panDetails[@"PANCardImage"],
                                      kForm_Key: kPANCardImage,
                                      kForm_Placeholder: LOCALIZATION(C_FORM_PANCARDIMAGE_PLACEHOLDER),    //lokalised 8 Feb
                                      kForm_Optional: @(YES),  //controlled in formdelegate level.
                                      }];
    
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_DRIVINGLICENSEID), //lokalised 8 Feb
                                      kForm_Type: @(kFORM_DRIVERLICENSE),
                                      kForm_Value: drivingLicenseDetails[@"DrivingLicenseID"],
                                      kForm_Key: kDrivingLicense,
                                      kForm_Optional: @(YES),  //controlled in formdelegate level.
                                      }];
    
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_DRIVINGLICENSEIMAGE),  //lokalised 8 Feb
                                      kForm_Type: @(kFORM_PANCARD_IMAGE),//share with pan card image
                                      kForm_Value: drivingLicenseDetails[@"DrivingLicenseImage"],
                                      kForm_Key: kDrivingLicenseImage,
                                      kForm_Placeholder: LOCALIZATION(C_FORM_PANCARDIMAGE_PLACEHOLDER),    //lokalised 8 Feb
                                      kForm_Optional: @(YES),  //controlled in formdelegate level.
                                      }];
    
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_VOTERID), //lokalised 8 Feb
                                      kForm_Type: @(kFORM_VOTERID),
                                      kForm_Value: voterDetails[@"VoterID"],
                                      kForm_Key: kVoterID,
                                      kForm_Optional: @(YES),  //controlled in formdelegate level.
                                      }];
    
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_VOTERIDIMAGE),  //lokalised 8 Feb
                                      kForm_Type: @(kFORM_PANCARD_IMAGE), //share with pan card image
                                      kForm_Value: voterDetails[@"VoterIDImage"],
                                      kForm_Key: kVoterIDImage,
                                      kForm_Optional: @(YES),  //controlled in formdelegate level.
                                      }];
    
}



-(NSString *) convertDateForDisplay:(NSString *)monthString day:(NSString *) dayString
{
    if ([monthString isEqualToString: BLANK] || [dayString isEqualToString: BLANK])
        return LOCALIZATION(C_PROFILE_DOB_PLACEHOLDER);    //lokalised 30 jan
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    if ([GET_LOCALIZATION isEqualToString: kThai])
        [dateFormat setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
    else
        [dateFormat setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    [dateFormat setDateFormat:@"MMdd"];
    NSDate *date = [dateFormat dateFromString: [NSString stringWithFormat: @"%@%@", monthString, dayString]];
    
    // Convert date object to desired output format
    [dateFormat setDateFormat:@"dd MMMM"];
    return [dateFormat stringFromDate:date];
}

-(void) didPressWorkingHours
{
    [self.delegate didPressWorkingHours: self.companyHours];
}

-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict
{
    //assign working hours
    self.companyHours = workingHoursDict;
    
    [self.tableViewDelegate updateWorkingHours: self.companyHours];
    
    [self.regFormTableView reloadData];
}

-(void) birthdatePressedWithKey:(NSString *) formKey
{
    [self.delegate birthdatePressedWithKey:formKey];
}
- (void)birthDateSelected:(NSDate *)date
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    
    //   for API
    [formatter setDateFormat:@"dd"];
    self.member.dobDay = [formatter stringFromDate:date];
    
    [formatter setDateFormat:@"MM"];
    self.member.dobMonth = [formatter stringFromDate:date];
    
    [formatter setDateFormat:@"dd MMMM"];
    
    NSString *selectedDateString = [formatter stringFromDate:date];
    
    [self.tableViewDelegate editCell:kDOB withDropdownSelection:selectedDateString];
}

-(void) dropdownPressedWithKey:(NSString *) formKey
{
    if([formKey isEqualToString: kSalutation])
    {
        [self.delegate dropDownSalutationPressedWithKey:formKey];
    }
    else if ([formKey isEqualToString: kCompanyState])
    {
        [self.delegate dropDownStatePressedWithKey:formKey];
    }
    else if ([formKey isEqualToString: kBankID])
    {
        [self.delegate dropDownBankNamePressedWithKey:formKey];
    }
}

-(void)dropDownSelection:(NSArray *)selection withKey:(NSString *)key
{
    [self.tableViewDelegate editCell: key withDropdownSelection: selection.firstObject];
}
-(void) ctaPressedWithKey:(NSString *) ctaKey
{
    if ([ctaKey isEqualToString: kChangePasswordCTA])
    {
        [self.delegate changePasswordPressedWithKey:ctaKey];
    }
    else if ([ctaKey isEqualToString: kUpdateProfileCTA])
    {
        NSMutableDictionary *updateParams;
        
        if (IS_COUNTRY(COUNTRYCODE_INDIA))
        {
            NSDictionary *jsonValuesDict = [self.tableViewDelegate getJsonValues];
            
            updateParams = [[NSMutableDictionary alloc] initWithDictionary: jsonValuesDict];
            
            if (!GET_ISIMPROFILETYPE) //TO
            {
                NSMutableDictionary *regCompanyDict = [NSMutableDictionary new];
                [regCompanyDict setObject: [jsonValuesDict objectForKey: kCompanyEmailAddress] forKey: kCompanyEmailAddress];
                [regCompanyDict setObject: [jsonValuesDict objectForKey: kCompanyName] forKey: kCompanyName];
                [regCompanyDict setObject: [jsonValuesDict objectForKey: kContactNumber] forKey: kContactNumber];
                [regCompanyDict setObject: @"" forKey: @"WorkshopQRCode"];
                
                [regCompanyDict setObject: [jsonValuesDict objectForKey:kOilChangePerMonth] forKey: kOilChangePerMonth];
                [regCompanyDict setObject: [jsonValuesDict objectForKey:kOilChangePercentage] forKey: kOilChangePercentage];
                [regCompanyDict setObject: [jsonValuesDict objectForKey:kHalfSyntheticOilChanges] forKey: kHalfSyntheticOilChanges];
                [regCompanyDict setObject: [jsonValuesDict objectForKey:kShellHelixQty] forKey: kShellHelixQty];
                    
                [updateParams setObject: regCompanyDict forKey: @"REG_Company"];
            }
            
            NSMutableDictionary *regCompanyAddressDict = [NSMutableDictionary new];
            [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kAddress1] forKey: kAddress1];
            [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kAddress2] forKey: kAddress2];
            [regCompanyAddressDict setObject: @"1" forKey: @"AddressType"];
            [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kTown] forKey: kTown];
            NSString *stateString = [jsonValuesDict objectForKey: kCompanyState];
            [regCompanyAddressDict setObject: [mSession convertToStateKeyCode: stateString stateDetailedListArray:GET_LOADSTATE]
                                      forKey: kCompanyState];
            [regCompanyAddressDict setObject: @(self.latitude) forKey: @"Latitude"];
            [regCompanyAddressDict setObject: @(self.longitude) forKey: @"Longitude"];
            [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kPostalCode] forKey: kPostalCode];
            [regCompanyAddressDict setObject:@"" forKey: @"StreetName"];
            
            [updateParams setObject: regCompanyAddressDict forKey: @"REG_CompanyAddress"];
            
            [updateParams setObject: self.companyHours forKey: @"REG_CompanyWorkHours"];
            
            [updateParams setObject: self.member.contactPreference forKey: kContactPreference];
            
            [updateParams setObject:self.member.tradeID forKey:@"TradeID"];
            
            //convert to keycode
            NSString *salutationKeyCodeString = [mSession convertToSalutationKeyCode: [updateParams objectForKey: kSalutation]];
            [updateParams setObject: salutationKeyCodeString forKey: @"Salutation"];
            
            [updateParams removeObjectForKey: kDOB];
            //convert DOBMonth and Day properly
            [updateParams setObject: self.member.dobMonth forKey: @"DOBMonth"];
            [updateParams setObject: self.member.dobDay forKey: @"DOBDay"];

            
        }
        else
        {
            updateParams = [[NSMutableDictionary alloc] initWithDictionary: [self.tableViewDelegate getJsonValues]];
            
            [updateParams setObject:self.member.tradeID forKey:@"TradeID"];
            
            //convert to keycode
            NSString *salutationKeyCodeString = [mSession convertToSalutationKeyCode: [updateParams objectForKey: kSalutation]];
            [updateParams setObject: salutationKeyCodeString forKey: @"Salutation"];
            
            [updateParams removeObjectForKey: kDOB];
            //convert DOBMonth and Day properly
            [updateParams setObject: self.member.dobMonth forKey: @"DOBMonth"];
            [updateParams setObject: self.member.dobDay forKey: @"DOBDay"];
            
            [updateParams setObject: self.member.contactPreference forKey: kContactPreference];
        }
        
        [[WebServiceManager sharedInstance] updateTradeProfile: updateParams
                                                            vc:self];
    }
    else if([ctaKey isEqualToString:@"UpdateBank"])
    {
       // NSMutableDictionary *bankParams = [[NSMutableDictionary alloc] init];
        self.bankParams = [[NSMutableDictionary alloc] init];
        NSDictionary *jsonValuesDict = [self.tableViewDelegate getJsonValues];
        
        if (IS_COUNTRY(COUNTRYCODE_INDIA) &&
            ![[jsonValuesDict objectForKey: kAccountNumber] isEqualToString: [jsonValuesDict objectForKey: kConfirmAccountNumber]])
        {
            [mAlert showErrorAlertWithMessage: LOCALIZATION(C_FORM_BENEFICIARYNUM_NOTMATCH)];
            return;
        }
        
        NSMutableDictionary *regBankDict = [[NSMutableDictionary alloc] init];
        [regBankDict setObject:[jsonValuesDict objectForKey: kBeneficiaryName]  forKey: kBeneficiaryName];
        [regBankDict setObject:[jsonValuesDict objectForKey: kAccountNumber] forKey: kAccountNumber];
        [regBankDict setObject:[jsonValuesDict objectForKey: kIFSCCode] forKey: kIFSCCode];
        [regBankDict setObject: [mSession convertToBankNameKeyCode: [jsonValuesDict objectForKey: kBankID]] forKey: kBankID];
        
        [self.bankParams setObject: regBankDict forKey: @"REG_Bank"];
        
        
//        NSMutableDictionary *regPanDict = [[NSMutableDictionary alloc] init];
//        [regPanDict setObject: [jsonValuesDict objectForKey: kPANCardId] forKey: kPANCardId];
//        if ([jsonValuesDict objectForKey: kPANCardImage])
//            [regPanDict setObject: [jsonValuesDict objectForKey: kPANCardImage] forKey: kPANCardImage];
//
        NSMutableDictionary *regPanDict = [[NSMutableDictionary alloc] init];
        [regPanDict setObject: [jsonValuesDict objectForKey: kPANCardId] forKey: kPANCardId];
        if([jsonValuesDict objectForKey: kPANCardImage])
            [regPanDict setObject: [jsonValuesDict objectForKey: kPANCardImage] forKey: kPANCardImage];
       
        
        NSMutableDictionary *regDrivingLicense = [[NSMutableDictionary alloc] init];
        [regDrivingLicense setObject: [jsonValuesDict objectForKey: kDrivingLicense] forKey: kDrivingLicense];
        if([jsonValuesDict objectForKey: kDrivingLicense])
            [regDrivingLicense setObject: [jsonValuesDict objectForKey: kDrivingLicenseImage] forKey: kDrivingLicenseImage];
        
        NSMutableDictionary *regVoterDict = [[NSMutableDictionary alloc] init];
        [regVoterDict setObject: [jsonValuesDict objectForKey: kVoterID] forKey: kVoterID];
        if([jsonValuesDict objectForKey: kVoterID])
            [regVoterDict setObject: [jsonValuesDict objectForKey: kVoterIDImage] forKey: kVoterIDImage];
        
         BOOL isValidated = TRUE;
        
        NSString *checkboxesString = [jsonValuesDict objectForKey: kValidateCheckboxes];
        NSArray *stringArray = [checkboxesString isEqualToString:@""] ? @[] : [checkboxesString componentsSeparatedByString: @","];
        NSString *idProofTypeString = @"";
        for (NSString *string in stringArray)
        {
            NSString *pancardRegex;
            NSPredicate *pancardTest;
            switch ([string intValue])
            {
                case 0:
                {
                    idProofTypeString = @"1";
                    
                    pancardRegex = REGEX_PANCARD;
                    pancardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pancardRegex];
                    
                    if([[jsonValuesDict objectForKey:kPANCardId] isEqualToString:@""]) {
                        isValidated = FALSE;
                        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_PAN_REQUIRED)];
                        return;
                    }
                    else if(![pancardTest evaluateWithObject: [jsonValuesDict objectForKey:kPANCardId]]){
                        isValidated = FALSE;
                        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_PANFORMAT)];
                        return;
                    }
                    else if([[jsonValuesDict objectForKey:kPANCardImage] isEqualToString:@""]) {
                        isValidated = FALSE;
                        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_PANIMAGE_REQUIRED)];
                        return;
                    }
                    [self.bankParams setObject:regPanDict forKey:@"REG_PAN"];
                }
                    break;
                case 1:
                {
                    if(![idProofTypeString isEqualToString:BLANK])
                        idProofTypeString = [NSString stringWithFormat:@"%@,2",idProofTypeString];
                    else
                        idProofTypeString = @"2";
                    
                    pancardRegex = REGEX_DRIVINGLICENSE;
                    pancardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pancardRegex];
                    
                    if([[jsonValuesDict objectForKey:kDrivingLicense] isEqualToString:@""]) {
                        isValidated = FALSE;
                        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_DRIVINGLICENSE_REQUIRED)];
                        return;
                    }
                    else if(![pancardTest evaluateWithObject: [jsonValuesDict objectForKey:kDrivingLicense]]){
                        isValidated = FALSE;
                        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_DRIVINGLICENSEFORMAT)];
                        return;
                    }
                    else if([[jsonValuesDict objectForKey:kDrivingLicenseImage] isEqualToString:@""]) {
                        isValidated = FALSE;
                        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_DRIVINGLICENSEIMAGE_REQUIRED)];
                        return;
                    }
                    [self.bankParams setObject:regDrivingLicense forKey:@"REG_DrivingLicense"];
                }
                    break;
                case 2:
                {
                    if(![idProofTypeString isEqualToString:BLANK])
                        idProofTypeString = [NSString stringWithFormat:@"%@,4",idProofTypeString];
                    else
                        idProofTypeString = @"4";
                    
                    pancardRegex = REGEX_VOTERID;
                    pancardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pancardRegex];
                    
                    if([[jsonValuesDict objectForKey:kVoterID] isEqualToString:@""]) {
                        isValidated = FALSE;
                        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_VOTERID_REQUIRED)];
                        return;
                    }
                    else if(![pancardTest evaluateWithObject: [jsonValuesDict objectForKey:kVoterID]]){
                        isValidated = FALSE;
                        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_VOTERIDFORMAT)];
                        return;
                    }
                    else if([[jsonValuesDict objectForKey:kVoterIDImage] isEqualToString:@""]) {
                        isValidated = FALSE;
                        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_VOTERIDIMAGE_REQUIRED)];
                        return;
                    }
                    [self.bankParams setObject:regVoterDict forKey:@"REG_VoterID"];
                }
                    break;
            }
        }
        
        [self.bankParams setObject:idProofTypeString forKey:@"IDProofType"];
        [self.bankParams setObject:@"" forKey:@"OTP"];
        
        if ([self.bankParams objectForKey:@"REG_PAN"] == nil) {
            [regPanDict setObject:@"" forKey:kPANCardId];
            [regPanDict setObject:@"" forKey:kPANCardImage];
            
            [self.bankParams setObject:regPanDict forKey:@"REG_PAN"];
        }
        if ([self.bankParams objectForKey:@"REG_DrivingLicense"] == nil) {
            [regDrivingLicense setObject:@"" forKey:kDrivingLicense];
            [regDrivingLicense setObject:@"" forKey:kDrivingLicenseImage];
            
            [self.bankParams setObject:regDrivingLicense forKey:@"REG_DrivingLicense"];
        }
        if ([self.bankParams objectForKey:@"REG_VoterID"] == nil) {
            [regVoterDict setObject:@"" forKey:kVoterID];
            [regVoterDict setObject:@"" forKey:kVoterIDImage];
            
            [self.bankParams setObject:regVoterDict forKey:@"REG_VoterID"];
        }
        
        [self.bankParams setObject: @([[jsonValuesDict objectForKey: kBankCheckBox] boolValue]) forKey: @"BankInfoSharing"];
        if(isValidated)
            [[WebServiceManager sharedInstance] updateBankDetails: self.bankParams
                                                               vc:self];
    } else if([ctaKey isEqualToString:@"UpdateCoin"]) {
        NSMutableDictionary *documentParams = [[NSMutableDictionary alloc] init];
        NSDictionary *jsonValuesDict = [self.tableViewDelegate getJsonValues];
        
        NSString *existingText = [jsonValuesDict objectForKey: kTaxNumber];
        if([existingText length] != 15) {
            [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_TAXNUMBER_VALIDATION)];
            return;
        } else {
            NSMutableDictionary *regDocumentsDict = [[NSMutableDictionary alloc] init];
            [regDocumentsDict setObject:[jsonValuesDict objectForKey: kTaxNumber]  forKey: kTaxNumber];
            [regDocumentsDict setObject:[jsonValuesDict objectForKey: kTaxPayerName] forKey: kTaxPayerName];
            [regDocumentsDict setObject:[jsonValuesDict objectForKey: kTaxPayerAddress] forKey: kTaxPayerAddress];
            [regDocumentsDict setObject:[jsonValuesDict objectForKey: kPhotoID] forKey:kPhotoID];
            [regDocumentsDict setObject:[jsonValuesDict objectForKey: kPhotoIDName] forKey: kPhotoIDName];
            [regDocumentsDict setObject:[jsonValuesDict objectForKey: kPhotoIDAddress] forKey: kPhotoIDAddress];
            
            [documentParams setObject: regDocumentsDict forKey: @"REG_Documents"];
            
            NSMutableDictionary *regTax = [[NSMutableDictionary alloc] init];
            if([jsonValuesDict objectForKey:kTaxImage])
                [regTax setObject: [jsonValuesDict objectForKey: kTaxImage] forKey: kTaxImage];
            else
                [regTax setObject: @"" forKey: kTaxImage];
            [regTax setObject: @"" forKey: @"TaxId"];
            [documentParams setObject: regTax forKey: @"REG_TAX"];
            
            NSMutableDictionary *regPhotoDetails = [[NSMutableDictionary alloc] init];
            if([jsonValuesDict objectForKey:kPhotoIDImage])
                [regPhotoDetails setObject: [jsonValuesDict objectForKey: kPhotoIDImage] forKey: kPhotoIDImage];
            else
                [regPhotoDetails setObject: @"" forKey: kPhotoIDImage];
            [regPhotoDetails setObject: @"" forKey: @"DrivingLicenseID"];
            [documentParams setObject: regPhotoDetails forKey: @"REG_DrivingLicense"];
            
            [documentParams setObject: @([[jsonValuesDict objectForKey: kBankCheckBox] boolValue]) forKey: @"DocInfoSharing"];
               
            [[WebServiceManager sharedInstance] updateDocumentDetails: documentParams vc:self];
        }
        
    }
}

-(void) showPhoto:(UIImage *) image
{
    //popup or push to controller with image
    [mSession pushImagePreview: self.navigationController.parentViewController image: image title: @"Uploaded Image"];
}

-(void) takePhoto: (NSString *) cellKey
{
    if ([cellKey isEqualToString: kPANCardImage])
        [mSession pushPanCardPhotoTakingWithParent: self delegateVc: self  message: LOCALIZATION(C_FORM_PANCARD_PHOTOFORMAT) key: cellKey];
    else if ([cellKey isEqualToString: kDrivingLicenseImage] && !IS_COUNTRY(COUNTRYCODE_INDONESIA))
        [mSession pushPanCardPhotoTakingWithParent: self delegateVc: self  message: LOCALIZATION(C_FORM_DRIVINGLICENSE_PHOTOFORMAT) key: cellKey];
    else if ([cellKey isEqualToString: kPhotoIDImage] && IS_COUNTRY(COUNTRYCODE_INDONESIA))
        [mSession pushPanCardPhotoTakingWithParent: self delegateVc: self  message: LOCALIZATION(C_FORM_PHOTOID_CONTENT) key: cellKey];
    else if ([cellKey isEqualToString: kVoterIDImage])
        [mSession pushPanCardPhotoTakingWithParent: self delegateVc: self  message: LOCALIZATION(C_FORM_VOTERID_PHOTOFORMAT) key: cellKey];
    else if ([cellKey isEqualToString: kTaxImage])
        [mSession pushPanCardPhotoTakingWithParent: self delegateVc: self  message: LOCALIZATION(C_FORM_TAXIDFORMAT_CONTENT) key: cellKey];
    else
        [mSession pushPanCardPhotoTakingWithParent:self delegateVc:self message:@"" key:cellKey];
}

-(void)imageTaken:(UIImage *)image key:(NSString *)key
{
    [self.tableViewDelegate editCell: key withImage: image];
}

-(void)textfieldWithButtonPressedWithKey:(NSString *)buttonKey
{
    [self locationPressed];
}

-(void) attributedLabeldidSelectLinkWithURL:(NSURL *)url {

    if ([[url absoluteString] isEqualToString: @"coinbackInfo"]) {
          NSString *urlAddress = [NSString stringWithFormat:@"%@%@%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX,PAGES_COINSPROMO];
          [mSession pushGenericWebViewWith:LOCALIZATION(C_TITLE_COINBACKPROGRAM) andURL:urlAddress onVC:self.parentViewController];
    }

}

- (void)locationPressed {
    [UpdateHUD addMBProgress: self.view withText: @""];
    switch ([CLLocationManager authorizationStatus])
    {
        case kCLAuthorizationStatusAuthorizedAlways:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusNotDetermined:
            [UpdateHUD removeMBProgress: self.view];
            [self.locationManager requestWhenInUseAuthorization];
            break;
        case kCLAuthorizationStatusDenied:
        {
            [UpdateHUD removeMBProgress: self.view];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_ALERTVIEW_ERROR) message:LOCALIZATION(C_ALERTVIEW_PERMISSIONERRORMSG) preferredStyle:UIAlertControllerStyleAlert];    //lokalised 30 jan
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_CANCEL) style:UIAlertActionStyleCancel handler:nil];    //lokalised 30 jan
            UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_SETTING) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                            UIApplicationOpenSettingsURLString]];    //lokalised 30 jan
            }];
            
            [alertController addAction:cancelAction];
            [alertController addAction:settingsAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
            break;
        case kCLAuthorizationStatusRestricted:
            [UpdateHUD removeMBProgress: self.view];
            break;
    }
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [UpdateHUD removeMBProgress: self.view];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    //most recent location update is at the end of the array.
    CLLocation *location = [locations lastObject];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:6];
    
    NSString *newLatitude   = [formatter stringFromNumber: @(location.coordinate.latitude)];
    NSString *newLongitude  = [formatter stringFromNumber: @(location.coordinate.longitude)];
    
    [self.tableViewDelegate editCell: kWorkshopLocation withTextfieldButtonText: [NSString stringWithFormat: @"%@, %@", newLatitude, newLongitude]];
    
    self.latitude = newLatitude.doubleValue;
    self.longitude = newLongitude.doubleValue;
    
    [self.locationManager stopUpdatingLocation];
    [UpdateHUD removeMBProgress: self.view];
}

-(void)textfieldCellDidBeginEditingWithKey:(NSString *)key
{
    [self.delegate textfieldHandler: YES];
    
    if ([key isEqualToString: kAccountNumber])
        [self.tableViewDelegate editCell: kAccountNumber toSecureField: NO];
}

-(void)textfieldCellDidEndEditingWithKey:(NSString *)key
{
    if ([key isEqualToString: kPostalCode])
    {
        NSString *postalCode = [[self.tableViewDelegate getJsonValues] objectForKey: kPostalCode];
        [mWebServiceManager fetchPostalAddress: postalCode vc:self];
    }
    else if ([key isEqualToString: kIFSCCode])
    {
        [self.tableViewDelegate editCell: kBankID withDropdownSelection: @""];    //Bank Name list
        NSString *ifscCodeString = [[self.tableViewDelegate getJsonValues] objectForKey: kIFSCCode];
        [mWebServiceManager fetchBankDetails: ifscCodeString vc:self];
    }
    else if ([key isEqualToString: kAccountNumber])
    {
        [self.tableViewDelegate editCell: kAccountNumber toSecureField: YES];
    }
    else if([key isEqualToString: kTaxNumber]) {
        NSString *existingText = [[self.tableViewDelegate getJsonValues] objectForKey: kTaxNumber];
        if([existingText length] != 15) {
            [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_TAXNUMBER_VALIDATION)];
        }
    }
    [self.delegate textfieldHandler: NO];
}

-(BOOL) textFieldCell:(NSString *)key shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if([key isEqualToString:kTaxNumber]) {
        NSString *existingText = [[self.tableViewDelegate getJsonValues] objectForKey: kTaxNumber];
        if([existingText length] > 14) {
            if ([string isEqualToString: @""]) //check for backspace first, proceed if yes
                return YES;
            return NO;
        } else {
            return YES;
        }
    }
    
    if ([key isEqualToString:kBeneficiaryName]) {
       // return [self validateStringWithAlphabet:string];
        if(string.length > 0)
        {
            NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "];
            NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];

            BOOL stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
            return stringIsValid;
        }
        return YES;
    }else if ([key isEqualToString:kAccountNumber] || [key isEqualToString:kConfirmAccountNumber]) {
        if(string.length > 0)
        {
            NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
            NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];

            BOOL stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
            return stringIsValid;
        }
        return YES;
    }
    else{
        return YES;
    }
}

- (BOOL)validateStringWithAlphabet:(NSString *)string {
    NSString *stringRegex = @"[A-Za-z ]+";
    NSPredicate *stringPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stringRegex];

    return [stringPredicate evaluateWithObject:string];
}


-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_FETCH_BANKDETAILS:
        {
            [self.tableViewDelegate editCell: kBankID withDropdownSelection: [[response getGenericResponse] objectForKey: @"BankName"]];    //Bank Name list
        }
            break;
        case kWEBSERVICE_UPDATETRADEPROFILE:
            [mAlert showSuccessAlertWithMessage:[response getResponseMessage] onCompletion:^(BOOL finished) {
            }];
            break;
        case kWEBSERVICE_UPDATEBANKDETAILS:
            if ([[response getRequestOTP]boolValue] == 1) {
                  NSLog(@"MOBILE NO GOT:%@",self.member.mobileNumber);
                  NSLog(@"COUNTRY CODE:%@",GET_COUNTRY_CODE);
                  [[WebServiceManager sharedInstance] fetchcountryDial:GET_COUNTRY_CODE mobileNo:self.member.mobileNumber vc:self];
                     // 8420195304
            }else{
                [mAlert showSuccessAlertWithMessage:[response getResponseMessage] onCompletion:^(BOOL finished) {
                }];
            }
            break;
        case kWEBSERVICE_UPDATEDOCUMENTDETAILS:
        {
            [mAlert showSuccessAlertWithMessage:LOCALIZATION(C_POPUP_COINBACK_SUCCESS_CONTENT) onCompletion:^(BOOL finished) {
                LoginInfo *loginInfo = [mSession loadUserProfile];
                [[WebServiceManager sharedInstance] fetchTrade:loginInfo.tradeID vc:self];
            }];
        }
            break;
            
        case kWEBSERVICE_FETCHGENERATEPANOTP:
        {
            NSLog(@"RESPONSE GOT:%@",[response getGenericResponse]);
            NSLog(@"RESPONSE GOT Mobile no:%@",[response getResponseMobileOTP]);
            [mAlert showSuccessAlertWithMessage:[response getResponseMessage] onCompletion:^(BOOL finished) {
//                [mSession pushRegisterPanOTPViewWithMobileNo:[response getResponseMobileOTP] vc:self.navigationController.parentViewController];
                [mSession pushRegisterPanOTPViewWithMobileNo:[response getResponseMobileOTP] dictBankDetail:self.bankParams vc:self.navigationController.parentViewController];
           
            }];
        }
            break;
            
        case kWEBSERVICE_FETCHTRADE:
        {
            self.workshopData = [response getGenericResponse];
            self.member = [[Member alloc] initWithData:[[response getGenericResponse] objectForKey: @"REG_Member"]];
            [self.documentIDDetails removeAllObjects];
            self.documentIDDetails = self.workshopData[@"REG_Document"];
            [self.documentIDDetails addEntriesFromDictionary:self.workshopData[@"REG_TAX"]];
            [self.documentIDDetails addEntriesFromDictionary:self.workshopData[@"REG_DrivingLicense"]];
            [self setupInterface];
        }
            break;
            
        case kWEBSERVICE_FETCH_POSTALADDRESS:
        {
            NSDictionary* responseDictionary = [response getGenericResponse];
            NSDictionary* postalDetails = responseDictionary[@"PostalDetails"];
            
            NSString* stateId = postalDetails[@"StateId"];
            NSString* townName = postalDetails[@"TownName"];
            
            if([stateId isKindOfClass:NSString.class])
            {
                NSString* stateName = [mSession convertToStateName:stateId stateDetailedListArray:GET_LOADSTATE];
                
//                [self.tableViewDelegate editCell: kTown withTextfieldString: townName];
//                [self.tableViewDelegate editCell: kState withTextfieldString: stateName];
            }
        }
            break;
        default:
            break;
    }
}
@end
