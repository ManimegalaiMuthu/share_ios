//
//  ProfileMasterViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 23/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "ProfileMasterViewController.h"
#import "ProfileTabViewController.h"
#import "MyProfileViewController.h"
#import "WorkshopProfileViewController.h"
#import "ProfileTab_NewViewController.h"
#import "CASDatePickerView.h"


#define kSalutation         @"Salutation"

@interface ProfileMasterViewController () <WebServiceManagerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, MyProfileDelegate, WorkshopProfileDelegate, CommonListDelegate, DatePickerDelegate, DatePickerDelegate, profileTabNewDelegate>
{
    ProfileTabViewController *profileTabViewController;
}
@property (weak, nonatomic) IBOutlet UIView *profileTabView;


@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePhoto;

//DSR - company name
@property (weak, nonatomic) IBOutlet UILabel *lblFieldOne;

//DSR - DSR Code
@property (weak, nonatomic) IBOutlet UILabel *lblFieldTwo;

//DSR - ranking
@property (weak, nonatomic) IBOutlet UILabel *lblFieldThree;

@property (weak, nonatomic) IBOutlet UIImageView *imgProgressBubble;
@property (weak, nonatomic) IBOutlet UIView *progressBar;   //grey bar
@property (weak, nonatomic) IBOutlet UILabel *lblProgress;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressBarWidth; //yellow bar width

@property (weak, nonatomic) IBOutlet UILabel *lblCompleteProfile;



@property (weak, nonatomic) IBOutlet UIView *changePasswordView;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UILabel *lblChangePassword;
@property (weak, nonatomic) IBOutlet UITextField *txtOldPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;

@property NSDictionary *workshopData;
//@property NSDictionary *workingHoursDict;
@property Member* member;

@property CASDatePickerView *dateView;
@end

@implementation ProfileMasterViewController
@synthesize listView;
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_MYPROFILE) subtitle: @""];   //lokalised 11 Feb
    
//    [[WebServiceManager sharedInstance] fetchHome: self];
    [self setupInterface];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_COUNTRY)        //lokalised 11 Feb
                                                   list: [mSession getCountryNames]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
    
    dateView = [[CASDatePickerView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_BIRTHDAY)previouslySelected:nil withYearHidden:YES];    //lokalised 11 Feb
    
    dateView.delegate = self;
    
    [self performSelector:@selector(coachmarkShow) withObject:nil afterDelay:1.0f];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_MYPROFILE) screenClass:nil];
}

-(void) setupInterface
{
    self.lblFieldOne.text = [[mSession profileInfo] fullName];
    self.lblFieldTwo.text = [[mSession profileInfo] shareCode];
    //            self.lblFieldThree.text = @([[mSession profileInfo] bonusPoint]).stringValue;
    self.lblFieldThree.text = [[mSession profileInfo] rank];
    
    NSString *urlString = [[[mSession profileInfo] profileImage] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    [self.imgProfilePhoto sd_setImageWithURL:[NSURL URLWithString: urlString]
                            placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_verydarkgrey"]
                                    options:SDWebImageRefreshCached];
    
    CGFloat sliderFillAmt = [[mSession profileInfo] profileCompleteness];
    self.lblProgress.text = [NSString stringWithFormat: @"%3.0f%%", sliderFillAmt];
    self.progressBarWidth.constant = self.progressBar.frame.size.width * (sliderFillAmt / 100);
    
    LoginInfo *loginInfo = [mSession loadUserProfile];
    [[WebServiceManager sharedInstance] fetchTrade:loginInfo.tradeID vc:self];
    self.imgProfilePhoto.layer.cornerRadius = self.imgProfilePhoto.frame.size.width / 2;
    
    self.lblFieldOne.font = FONT_H(25);
    self.lblFieldTwo.font = FONT_B1;
    self.lblFieldThree.font = FONT_B1;
    
    self.lblCompleteProfile.font = FONT_H2;
    self.lblCompleteProfile.text = LOCALIZATION(C_COMPLETE_YOUR_PROFILE);   //lokalised 11 Feb
    
    //format progress bar
    self.lblProgress.font = FONT_B2;
    [self.lblProgress sizeToFit];
    UIImage *bubbleImage = GET_ISADVANCE ? [UIImage imageNamed:@"icon-bubble_blue"] : [UIImage imageNamed:@"icon-bubble"];
    self.imgProgressBubble.image = [bubbleImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 3.5, 0)  resizingMode:UIImageResizingModeStretch];
    
    self.lblChangePassword.font = FONT_H1;
    self.lblChangePassword.text = LOCALIZATION(C_FORM_CHANGEPASSWORD);  //lokalised 11 Feb
    
    self.txtOldPassword.font = FONT_B2;
    self.txtOldPassword.placeholder = LOCALIZATION(C_PLACEHOLDER_CURRENTPASSWORD);  //lokalised 11 Feb
    
    self.txtNewPassword.font = self.txtOldPassword.font;
    self.txtNewPassword.placeholder = LOCALIZATION(C_PLACEHOLDER_NEWPASSWORD);  //lokalised 11 Feb
    
    self.txtConfirmPassword.font = self.txtOldPassword.font;
    self.txtConfirmPassword.placeholder = LOCALIZATION(C_PLACEHOLDER_RE_NEWPASSWORD);   //lokalised 11 Feb
    
    [self.btnSubmit.titleLabel setFont: FONT_BUTTON];
    [self.btnSubmit setBackgroundColor:COLOUR_RED];
    [self.btnSubmit setTitle:LOCALIZATION(C_FORM_SUBMIT) forState:UIControlStateNormal];    //lokalised 11 Feb
    [Helper setCustomFontButtonContentModes: self.btnSubmit];
    
    [self.btnCancel.titleLabel setFont: FONT_BUTTON];
    [self.btnCancel setTitle:LOCALIZATION(C_FORM_CANCEL) forState:UIControlStateNormal];    //lokalised 11 Feb
    [Helper setCustomFontButtonContentModes: self.btnCancel];
    
//    self.workingHoursDict = [self.workshopData objectForKey: @"REG_CompanyWorkHours"];
//
//    if (!self.workingHoursDict ||
//        [self.workingHoursDict isKindOfClass: [NSNull class]])
//        self.workingHoursDict = @{@"WorkHourStart1":   @"0:00",
//                                  @"WorkHourEnd1":     @"0:00",
//                                  @"WeekDays1":        @"",
//                                  @"PublicHoliday":    @"0",
//                                  @"WorkHourStart2":   @"0:00",
//                                  @"WorkHourEnd2":     @"0:00",
//                                  @"WeekDays2":        @"",
//                                  };
}

-(void)changePasswordHandler
{
//    [self.changePasswordView setHidden: NO];
    [self popupChangePassword];
}

-(void)textfieldHandler:(BOOL)isBeginEditing
{
    [UIView animateWithDuration:0.3f animations:^{
        if (isBeginEditing)
        {
            self.profileTabView.transform = CGAffineTransformMakeTranslation(0, -200);
        }
        else
        {
            self.profileTabView.transform = CGAffineTransformIdentity;
        }
    }];
}

//- (IBAction)cancelChangePassword:(id)sender {
//    [self.changePasswordView setHidden: YES];
//    self.txtOldPassword.text = @"";
//    self.txtNewPassword.text = @"";
//    self.txtConfirmPassword.text = @"";
//
//}
//
//- (IBAction)submitChangePasswordPressed:(id)sender {
//    if (![self.txtNewPassword.text isEqualToString: self.txtConfirmPassword.text])
//    {
//        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_CHANGEPASSWORD_MISMATCH_ERRORMSG) title:LOCALIZATION(C_GLOBAL_ERROR) closeButtonTitle: LOCALIZATION(C_POPUPMSG_SUBMITAGAIN)]; //lokalised 11 Feb
//
//        return;
//    }
//
//    [[WebServiceManager sharedInstance] changePasswordPostLogin:self.txtOldPassword.text
//                                                    newPassword:self.txtNewPassword.text
//                                                             vc: self];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)processCompleted:(WebServiceResponse *)response
{
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_FETCHHOME:
        {
            [mSession setProfileInfo: [[ProfileInfo alloc] initWithData: [[response getGenericResponse] objectForKey: @"MyProfile"]]];
            //update to ProfileData
            [[NSNotificationCenter defaultCenter] postNotificationName: @"UpdateProfileInfo" object: nil];
            
            self.lblFieldOne.text = [[mSession profileInfo] fullName];
            self.lblFieldTwo.text = [[mSession profileInfo] shareCode];
            //            self.lblFieldThree.text = @([[mSession profileInfo] bonusPoint]).stringValue;
            self.lblFieldThree.text = [[mSession profileInfo] rank];
            
            NSString *urlString = [[[mSession profileInfo] profileImage] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            [self.imgProfilePhoto sd_setImageWithURL:[NSURL URLWithString: urlString]
                                    placeholderImage:[UIImage imageNamed:@"placeholder_profilepic_verydarkgrey"]
                                             options:SDWebImageRefreshCached];
            
            CGFloat sliderFillAmt = [[mSession profileInfo] profileCompleteness];
            self.lblProgress.text = [NSString stringWithFormat: @"%3.0f%%", sliderFillAmt];
            self.progressBarWidth.constant = self.progressBar.frame.size.width * (sliderFillAmt / 100);
            
            LoginInfo *loginInfo = [mSession loadUserProfile];
            [[WebServiceManager sharedInstance] fetchTrade:loginInfo.tradeID vc:self];
        }
            break;
            
        case kWEBSERVICE_UPDATETRADEPROFILE:
            [mAlert showSuccessAlertWithMessage:[response getResponseMessage] onCompletion:^(BOOL finished) {
            }];
            break;
            
//        case kWEBSERVICE_CHANGEPASSWORD:
//        {
//            [mAlert showSuccessAlertWithMessage:[response getResponseMessage] onCompletion:^(BOOL finished) {
//                [self cancelChangePassword: nil];
//            }];
//        }
//            break;
            
        case kWEBSERVICE_FETCHTRADE:
        {
            self.workshopData = [response getGenericResponse];
            self.member = [[Member alloc] initWithData:[[response getGenericResponse] objectForKey: @"REG_Member"]];
            
            profileTabViewController = [STORYBOARD_PROFILE instantiateViewControllerWithIdentifier: VIEW_PROFILE_TAB];
            profileTabViewController.delegate = self;
            profileTabViewController.member = self.member;
            profileTabViewController.workshopDetailsData = self.workshopData;
            [self addChildViewController: profileTabViewController];
            
            [self.profileTabView addSubview: profileTabViewController.view];
            [profileTabViewController.view setFrame: CGRectMake(0, 0, self.profileTabView.frame.size.width, self.profileTabView.frame.size.height)];
            [profileTabViewController setSelectedIndex:1];
        }
        default:
            break;
    }
}


-(void) dropdownPressedWithKey:(NSString *) formKey
{
    [self.view endEditing: YES];
    listView.formKey = formKey;
    
    //Salutation
    if ([formKey isEqualToString: kSalutation])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO]; //lokalised 11 Feb
    }
    else
    {
        listView.formKey = nil;
        [mAlert showErrorAlertWithMessage: @"Unknown Error"];
        return;
    }
    [self popUpView];
}
-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}
-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        [profileTabViewController dropDownSelection:selection withKey:listView.formKey];
        listView.formKey = nil;
    }
}
-(void) didPressWorkingHours: (NSDictionary *) workingHours
{
    if (!workingHours ||
        [workingHours isKindOfClass: [NSNull class]])
        workingHours = @{@"WorkHourStart1":   @"0:00",
                                  @"WorkHourEnd1":     @"0:00",
                                  @"WeekDays1":        @"",
                                  @"PublicHoliday":    @"0",
                                  @"WorkHourStart2":   @"0:00",
                                  @"WorkHourEnd2":     @"0:00",
                                  @"WeekDays2":        @"",
                                  };
    
    //let tab bar push instead
    [mSession pushWorkingHoursView: self workingHoursData: workingHours];
}

-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict
{
    [profileTabViewController didFinishWorkingHoursSelection: workingHoursDict];
}

-(void) dropDownStatePressedWithKey:(NSString *) formKey
{
    [self.view endEditing: YES];
    listView.formKey = formKey;
    [listView setTitleAndList: LOCALIZATION(C_PROFILE_STATE) list: [mSession getStateNames:GET_LOADSTATE] hasSearchField: NO];  //lokalised 11 Feb
    [self popUpView];
}
-(void) dropDownSalutationPressedWithKey:(NSString *) formKey
{
    [self.view endEditing: YES];
    listView.formKey = formKey;
    [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO]; //lokalised 11 Feb
    [self popUpView];
}
-(void) birthdatePressedWithKey:(NSString *) formKey
{
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}
-(void) dropDownBankNamePressedWithKey:(NSString *) formKey
{
    [self.view endEditing: YES];
    listView.formKey = formKey;
    [listView setTitleAndList: LOCALIZATION(@"Select Bank Name") list: [mSession getBanksDetailsList] hasSearchField: NO];  //later lokalise
    [self popUpView];
}
- (void)selectedDate:(NSDate *)selection
{
    [profileTabViewController birthDateSelected:selection];
}
-(void) changePasswordPressedWithKey:(NSString *) formKey
{
    [self changePasswordHandler];
}

#pragma mark - Coachmark
-(void) coachmarkShow {
    NSMutableDictionary *coachmarkData = GET_COACHMARK_HASSHOWN;
    if(coachmarkData == nil) {
        coachmarkData = [[NSMutableDictionary alloc] initWithDictionary:@{}];
    }
    //For Coachmark
    if(![[coachmarkData objectForKey: COIN_COACHMARK] boolValue] && IS_COUNTRY(COUNTRYCODE_INDONESIA))
    {
        //if coins coachmark has not shown.
        NSMutableArray *coachMarkData = [[NSMutableArray alloc] init];
        [coachMarkData addObject:[Helper createCoachmarkFor:CGRectMake(5.0, [[UIApplication sharedApplication] statusBarFrame].size.height + 240.0 ,SCREEN_WIDTH - 10.0 , 55.0) withCaption:LOCALIZATION(C_COACHMARK_COINS_STEP1) andOnSide:@(kCOACHMARK_UP_LEFTSIDE)]];
        [mSession showCoachMark:coachMarkData withDelegate:nil onVc:self];
        

        [coachmarkData setObject: @(YES) forKey: COIN_COACHMARK];
        SET_COACHMARK_HASSHOWN(coachmarkData);
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
