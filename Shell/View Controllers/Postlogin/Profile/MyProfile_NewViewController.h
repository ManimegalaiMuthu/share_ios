//
//  MyProfile_NewViewController.h
//  Shell
//
//  Created by Jeremy Lua on 22/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"
@protocol MyProfileDelegate <NSObject>
-(void) changePasswordHandler;
-(void) textfieldHandler:(BOOL) isBeginEditing;
@end

NS_ASSUME_NONNULL_BEGIN

@interface MyProfile_NewViewController : BaseVC
@property (weak) id<MyProfileDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
