//
//  ProfileTabViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 23/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "RDVTabBarController.h"
#import <UIKit/UIKit.h>
#import "Helper.h"

#import "ProfileTab_NewViewController.h"

@interface ProfileTabViewController : RDVTabBarController
@property NSMutableDictionary *profileData;
@property NSDictionary *workshopDetailsData;
@property Member* member;
@property (weak, nonatomic) id<profileTabNewDelegate> delegate;

-(void)setWorkshopData:(NSDictionary*)workshopData;
-(void)dropDownSelection:(NSArray *)selection withKey:(NSString*)key;
-(void)birthDateSelected:(NSDate*)date;
-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict;
@end
