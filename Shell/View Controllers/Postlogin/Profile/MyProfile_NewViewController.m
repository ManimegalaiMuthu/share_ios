//
//  MyProfile_NewViewController.m
//  Shell
//
//  Created by Jeremy Lua on 22/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "MyProfile_NewViewController.h"
#import "FormsTableViewDelegate.h"
#import "CASDatePickerView.h"
#import "ProfileInfo.h"

#define kUserID             @"UserID"
#define kDSRCode            @"DSRCode"
#define kUserPassword       @"UserPassword"
#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kEmailAddress       @"EmailAddress"
#define kDOB                @"DOB"
#define kDOBMonth           @"DOBMonth"
#define kDOBDay             @"DOBDay"
#define kCountryCode        @"CountryCode"
#define kContactPreference  @"ContactPreferences"

#define kChangePasswordCTA        @"ChangePasswordCTA"
#define kUpdateProfileCTA        @"UpdateProfileCTA"


#define kWorkshopLocation   @"Location"
#define kCompanyName            @"CompanyName"
#define kContactNumber          @"ContactNumber"
#define kCompanyEmailAddress    @"CompanyEmailAddress"
#define kAddress1       @"Address1"
#define kAddress2       @"Address2"
#define kCompanyCity    @"CompanyCity"
#define kCompanyState   @"CompanyState"
#define kPostalCode     @"PostalCode"
#define kWorkingHours       @"WorkingHours"

#define kOilChangePerMonth      @"OilChangePerMonth"
#define kOilChangePercentage    @"OilChangePercentage"
#define kHalfSyntheticOilChanges    @"HalfSyntheticOilChanges"
#define kShellHelixQty          @"ShellHelixQty"

@interface MyProfile_NewViewController () <FormsTableViewDelegate, WebServiceManagerDelegate, UITextFieldDelegate, CLLocationManagerDelegate>
@property CLLocationManager *locationManager;
@property CGFloat latitude;
@property CGFloat longitude;


@property (weak, nonatomic) IBOutlet UITableView *regFormTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *regFormTableViewHeight;

@property FormsTableViewDelegate *tableViewDelegate;

//change to vc later
@property (weak, nonatomic) IBOutlet UIView *changePasswordView;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UILabel *lblChangePassword;
@property (weak, nonatomic) IBOutlet UITextField *txtOldPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
@property (weak, nonatomic) IBOutlet UIScrollView *backgroundScrollView;
@property (weak, nonatomic) IBOutlet UIView *whiteBackground;


@property Member* member;
@property CompanyInfo *companyInfo;
@property CompanyAddress *companyAddress;
@property NSMutableDictionary *workshopDetailsData;
@property NSDictionary *workingHoursDict;
@property NSArray *stateTable;

@property CASDatePickerView *dateView;
@end

@implementation MyProfile_NewViewController
@synthesize listView;
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager requestWhenInUseAuthorization];
    
    //if india and TO/iM
    if (GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER && IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        
    }
    else
    {
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_MYPROFILE) subtitle: @""];    //lokalise 30 jan
    }
    
//    [[WebServiceManager sharedInstance] fetchHome: self];
    LoginInfo *loginInfo = [mSession loadUserProfile];
    [[WebServiceManager sharedInstance] fetchTrade:loginInfo.tradeID vc:self];
}

//- (void)viewDidLayoutSubviews {
//    self.regFormTableViewHeight.constant = self.regFormTableView.contentSize.height;
//
//    [self.view layoutIfNeeded];
//}

-(void) setupInterface
{
    self.tableViewDelegate = [[FormsTableViewDelegate alloc] init];
    
    //TO combined
    NSMutableArray *topSectionArray = [[NSMutableArray alloc] init];
    [self setupTopSectionArray: topSectionArray];
    [self.tableViewDelegate.headersArray addObject: @""]; //0 char to skip section header
    [self.tableViewDelegate.initializingArray addObject: topSectionArray];
    
    if(GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
    {
        NSMutableArray *workshopProfileArray = [[NSMutableArray alloc] init];
        [self setupWorkshopProfileArray: workshopProfileArray];
        [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_HEADER_WORKSHOPPROFILE)]; //more than 1 char, to show required fields label //lokalised 30 jan
        [self.tableViewDelegate.initializingArray addObject: workshopProfileArray];
    }
    
    
    NSMutableArray *profileArray = [[NSMutableArray alloc] init];
    [self setupProfileArray: profileArray];
    if(GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
    {
        [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_HEADER_MYPROFILE)]; //more than 1 char, to show required fields label
    }
    else
    {
        [self.tableViewDelegate.headersArray addObject: @" "]; //more than 1 char, to show required fields label
    }
    [self.tableViewDelegate.initializingArray addObject: profileArray];
    
    
    if(GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
    {
        NSMutableArray* addressDetailsArray = [NSMutableArray new];
        [self setupAddressArray:addressDetailsArray];
        [self.tableViewDelegate.initializingArray addObject: addressDetailsArray];
        [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_PROFILE_ADDRESS)];   //lokalise 30 jan
    }
    
    NSMutableArray *contactPrefArray = [[NSMutableArray alloc]init];
    [self setupContactPrefArray: contactPrefArray];
    [self.tableViewDelegate.headersArray addObject:@""];
    [self.tableViewDelegate.initializingArray addObject: contactPrefArray];
    
    NSMutableArray *lastSectionArray = [[NSMutableArray alloc] init];
    [self setupLastSectionArray: lastSectionArray];
    [self.tableViewDelegate.headersArray addObject: @""]; //0 char to skip section header
    [self.tableViewDelegate.initializingArray addObject: lastSectionArray];
    
    
    self.regFormTableView.delegate = self.tableViewDelegate;
    self.regFormTableView.dataSource = self.tableViewDelegate;
    
    
    [self.tableViewDelegate registerNib: self.regFormTableView];
    [self.tableViewDelegate setDelegate: self];
    
    if(@available(iOS 11, *))
    {
        self.regFormTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.regFormTableView setContentInset:UIEdgeInsetsMake(-45, 0, -30, 0)];

    
    self.regFormTableView.backgroundColor = [UIColor clearColor];
    self.regFormTableView.backgroundView = nil;
    
    //change to vc later
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc]
                                             initWithTarget:self
                                             action:@selector(cancelChangePassword:)];
    [self.changePasswordView addGestureRecognizer: gesRecognizer];
    self.lblChangePassword.font = FONT_H1;
    self.lblChangePassword.text = LOCALIZATION(C_FORM_CHANGEPASSWORD);  //lokalise 30 jan
    if (kIsRightToLeft)
        [self.lblChangePassword setTextAlignment: NSTextAlignmentRight];
    else
        [self.lblChangePassword setTextAlignment: NSTextAlignmentLeft];
    
    self.txtOldPassword.font = FONT_B2;
    self.txtOldPassword.placeholder = LOCALIZATION(C_PLACEHOLDER_CURRENTPASSWORD);  //lokalise 30 jan
    
    self.txtNewPassword.font = self.txtOldPassword.font;
    self.txtNewPassword.placeholder = LOCALIZATION(C_PLACEHOLDER_NEWPASSWORD);  //lokalise 30 jan
    
    self.txtConfirmPassword.font = self.txtOldPassword.font;
    self.txtConfirmPassword.placeholder = LOCALIZATION(C_PLACEHOLDER_RE_NEWPASSWORD);  //lokalise 30 jan
    
    [self.btnSubmit.titleLabel setFont: FONT_BUTTON];
    [self.btnSubmit setTitle:LOCALIZATION(C_FORM_SUBMIT) forState:UIControlStateNormal];  //lokalise 30 jan
    [Helper setCustomFontButtonContentModes: self.btnSubmit];
    [self.btnSubmit setBackgroundColor:COLOUR_RED];
    
    [self.btnCancel.titleLabel setFont: FONT_BUTTON];
    [self.btnCancel setTitle:LOCALIZATION(C_FORM_CANCEL) forState:UIControlStateNormal];  //lokalise 30 jan
    [Helper setCustomFontButtonContentModes: self.btnCancel];
    
    [self.regFormTableView reloadData];
}

-(void) dropdownPressedWithKey:(NSString *) formKey
{
    [self.view endEditing: YES];
    listView.formKey = formKey;
    
    //Salutation
    if ([formKey isEqualToString: kSalutation])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO]; //lokalise 30 jan
    }
    else if ([formKey isEqualToString: kCompanyState])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_STATE) list: [mSession getStateNames: self.stateTable] hasSearchField: YES]; //lokalise 30 jan
    }
    else
    {
        listView.formKey = nil;
        [mAlert showErrorAlertWithMessage: @"Unknown Error"];
        return;
    }
    [self popUpView];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        [self.tableViewDelegate editCell: listView.formKey withDropdownSelection: selection.firstObject];
        listView.formKey = nil;
    }
}

-(void)birthdatePressedWithKey:(NSString *)formKey
{
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}

- (void)selectedDate:(NSDate *)selectedDate
{
    //do smething
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //   for API
    [formatter setDateFormat:@"dd"];
    self.member.dobDay = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"MM"];
    self.member.dobMonth = [formatter stringFromDate:selectedDate];
    
    if(kIsRightToLeft) {
        [formatter setDateFormat:@"MMMM"];
    } else {
        [formatter setDateFormat:@"dd MMMM"];
    }
    
    if ([GET_LOCALIZATION isEqualToString: kThai])
        [formatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
    else
        [formatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    NSString *selectedDateString = [formatter stringFromDate:selectedDate];
    
    //add formkey to birthday class if needed
    if(kIsRightToLeft) {
        [self.tableViewDelegate editCell: kDOB withDropdownSelection: [NSString stringWithFormat:@"%@ %@",self.member.dobDay,selectedDateString]];
    } else {
        [self.tableViewDelegate editCell: kDOB withDropdownSelection: selectedDateString];
    }
}


-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    //    [self setupInterface];
    
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_COUNTRY) //lokalise 30 jan, not used
                                                   list: [mSession getCountryNames]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
    
    dateView = [[CASDatePickerView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_BIRTHDAY)previouslySelected:nil withYearHidden:YES]; //lokalise 30 jan

    dateView.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_MYPROFILE) screenClass:nil];
}


#pragma mark Setup Profile
-(void) setupTopSectionArray: (NSMutableArray *) topSectionArray
{
    [topSectionArray addObject: @{kForm_Type: @(kFORM_PROFILE_TOPSECTION),
                                  kForm_Key: @"TopSection",
                                  kForm_Value: @{@"FullName": [[mSession profileInfo] fullName],
                                                 @"DSRCode": [[mSession profileInfo] shareCode],
                                                 @"Rank": [[mSession profileInfo] rank],
                                                 }
                                  }];
}

-(void) setupWorkshopProfileArray: (NSMutableArray *) workshopProfileArray
{
    [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_COMPANYNAME), //lokalise 30 jan
                                       kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                       kForm_Key: kCompanyName,
                                       kForm_HasDelegate: @(YES),
                                       kForm_Value: self.companyInfo.companyName,
                                       }]; //company name
    
    [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_CONTACTNUMBER), //lokalise 30 jan
                                       kForm_Placeholder: LOCALIZATION(C_PROFILE_CONTACTNUMBER_PLACEHOLDER), //lokalise 30 jan
                                       kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                       kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                       kForm_Key: kContactNumber,
                                       kForm_HasDelegate: @(YES),
                                       kForm_Value: self.companyInfo.contactNumber,
                                       kForm_Optional: @(YES),
                                       }]; //contact number
    
    [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPEMAILADDRESS), //lokalise 30 jan
                                       kForm_Placeholder: LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER), //lokalise 30 jan
                                       kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                       kForm_HasDelegate: @(YES),
                                       kForm_Keyboard: @(UIKeyboardTypeEmailAddress),
                                       kForm_Key: kCompanyEmailAddress,
                                       kForm_Value: self.companyInfo.companyEmailAddress,
                                       kForm_Optional: @(YES),
                                       }]; //email address
    
    [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKINGHOURS), //lokalise 30 jan
                                       kForm_Type: @(kREGISTRATION_WORKINGHOURS),
                                       kForm_Key: kWorkingHours,
                                       kForm_Value: @"", //using kForm_WorkingHours instead
                                       kForm_WorkingHours: [self.workingHoursDict isKindOfClass:[NSNull class]] ? @"" : self.workingHoursDict,
                                       kForm_Optional: @(YES),
                                       }];
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_OILCHANGEPERDAY), //lokalise 30 jan
                                           kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER), //lokalise 30 jan
                                           kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
                                           kForm_HasDelegate: @(YES),
                                           kForm_Key: kOilChangePerMonth,
                                           kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                           kForm_Value: self.companyInfo.oilChangePerMonth,
                                           }];
        
        [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_OILCHANGEPERCENT), //lokalise 30 jan
                                           kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER), //lokalise 30 jan
                                           kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
                                           kForm_HasDelegate: @(YES),
                                           kForm_Key: kOilChangePercentage,
                                           kForm_Keyboard: @(UIKeyboardTypeDecimalPad),
                                           kForm_Value: self.companyInfo.oilChangePercentage,
                                           }];
        
        [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SEMIOILCHANGEPERCENT), //lokalise 30 jan
                                           kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER), //lokalise 30 jan
                                           kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
                                           kForm_HasDelegate: @(YES),
                                           kForm_Key: kHalfSyntheticOilChanges,
                                           kForm_Keyboard: @(UIKeyboardTypeDecimalPad),
                                           kForm_Value: self.companyInfo.halfSyntheticOilChanges,
                                           }];
        
        [workshopProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SHELLHELIX), //lokalise 30 jan
                                           kForm_Placeholder: LOCALIZATION(C_PROFILE_ANSWER), //lokalise 30 jan
                                           kForm_Type: @(kREGISTRATION_LABELWITHTEXTFIELD),
                                           kForm_HasDelegate: @(YES),
                                           kForm_Key: kShellHelixQty,
                                           kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                           kForm_Value: self.companyInfo.shellHelixQty,
                                           }];

    }
}

-(void) setupProfileArray: (NSMutableArray *) profileArray
{
    [profileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SALUTATION), //lokalise 30 jan
                               kForm_Type: @(kREGISTRATION_DROPDOWN),
                               kForm_Placeholder: LOCALIZATION(C_PROFILE_SALUTATION_PLACEHOLDER), //lokalise 30 jan
                               kForm_Key: kSalutation,
                               kForm_Value: [mSession convertToSalutation: self.member.salutation],
                               kForm_Optional: @(YES),
                               }];
    [profileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_FIRSTNAME), //lokalise 30 jan
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_HasDelegate: @(YES),
                               kForm_Key: kFirstName,
                               kForm_Value: self.member.firstName,
                               }];
    [profileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_LASTNAME), //lokalise 30 jan
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_HasDelegate: @(YES),
                               kForm_Key: kLastName,
                               kForm_Value: self.member.lastName,
                               }];
    [profileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_DOB), //lokalise 30 jan
                               kForm_Type: @(kREGISTRATION_BIRTHDATE),
                               kForm_Key: kDOB,
                               kForm_Value: [self convertDateForDisplay:self.member.dobMonth day:self.member.dobDay],
                               kForm_Optional: @(YES),
                               }];
    [profileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_MOBILENUM), //lokalise 30 jan
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_HasDelegate: @(YES),
                               kForm_Key: kMobileNumber,
                               kForm_Enabled: @(NO),
                               kForm_Value: [NSString stringWithFormat: @"%@%@", [[[mSession convertCountryCodeToCountryDialingCode: GET_COUNTRY_CODE] componentsSeparatedByString: @" "] firstObject], self.member.mobileNumber],
                               }];
    [profileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_EMAILADDRESS),  //lokalise 30 jan
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_HasDelegate: @(YES),
                               kForm_Key: kEmailAddress,
                               kForm_Keyboard: @(UIKeyboardTypeEmailAddress),
                               kForm_Value: self.member.emailAddress,
                               kForm_Optional: @(!IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                               kForm_Placeholder: LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER),  //lokalise 30 jan
                               }];
}

-(void) setupAddressArray: (NSMutableArray *) addressArray
{
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_LOCATION),  //lokalise 30 jan
                               kForm_Type: @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                               kForm_TextfieldEnabled: @(NO),
                               kForm_Icon: @"icon-location",
                               kForm_Key: kWorkshopLocation,
                               kForm_Enabled: @(YES),
                               kForm_Value: [NSString stringWithFormat: @"%f, %f", self.companyAddress.latitude, self.companyAddress.longitude],
                               }];    //location
    
    if(![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_HONGKONG]) {
        [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_POSTALCODE), //lokalise 30 jan
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_HasDelegate: @(YES),
                                   kForm_Key: kPostalCode,
                                   kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                   kForm_HasDelegate : @(IS_COUNTRY(COUNTRYCODE_INDIA)), //textfield delegate for postal code
                                   kForm_Value: self.companyAddress.postalCode,
                                   kForm_Enabled: @([[mSession profileInfo] canEditAddress]),
                                   }]; //postal code
    }
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_STATE), //lokalise 30 jan
                               kForm_Type: @(kREGISTRATION_DROPDOWN),
                               kForm_Placeholder: LOCALIZATION(C_PROFILE_STATE_PLACEHOLDER), //lokalise 30 jan
                               kForm_Key: kCompanyState,
                               kForm_Value: [mSession convertToStateName:self.companyAddress.companyState stateDetailedListArray:self.stateTable],
                               kForm_Enabled: @([[mSession profileInfo] canEditAddress]),
                               }];
    
    if (!IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_TOWN), //lokalise 30 jan
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_HasDelegate: @(YES),
                                   kForm_Key: kCompanyCity, //kCity?
                                   kForm_Value: self.companyAddress.companyCity,
                                   kForm_Optional: @(!(IS_COUNTRY(COUNTRYCODE_RUSSIA) ||
                                                       IS_COUNTRY(COUNTRYCODE_INDIA))),
                                   kForm_Enabled: @([[mSession profileInfo] canEditAddress]),
                                   }];
    }
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_ADDRESSLINE1), //lokalise 30 jan
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_HasDelegate: @(YES),
                               kForm_Key: kAddress1,
                               kForm_Value: self.companyAddress.address1,
                               //                               kForm_Optional: @(!IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                               kForm_Enabled: @([[mSession profileInfo] canEditAddress]),
                               }]; //address line 1
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_ADDRESSLINE2), //lokalise 30 jan
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_HasDelegate: @(YES),
                               kForm_Key: kAddress2,
                               kForm_Value: self.companyAddress.address2,
                               kForm_Optional: @(YES),
                               kForm_Enabled: @([[mSession profileInfo] canEditAddress]),
                               }];
}

-(void) setupContactPrefArray: (NSMutableArray *)contactPrefArray {
    
    [contactPrefArray addObject: @{//kForm_Title: LOCALIZATION(C_PROFILE_CONTACT_PREFERENCE),
                                   kForm_Type: @(kREGISTRATION_CONTACTPREF),
                                   kForm_Key: kContactPreference,
                                   kForm_Value: self.member.contactPreference, //contact pref bitvalue strings
                                   kForm_BackgroundColour: COLOUR_WHITE,
                                   kForm_Optional: @(YES),
                                   }];
}

-(void) setupLastSectionArray: (NSMutableArray *)lastSectionArray
{
    [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_CHANGEPASSWORD), //lokalise 30 jan
                                   kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                   kForm_Key: kChangePasswordCTA,
                                   kForm_Enabled: @(YES),
                                   kForm_TextColour: COLOUR_VERYDARKGREY,
                                   kForm_BackgroundColour: COLOUR_YELLOW,
                                   }];
    
    [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_UPDATEPROFILE), //lokalise 30 jan
                                   kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                   kForm_Key: kUpdateProfileCTA,
                                   kForm_Enabled: @(YES),
                                   kForm_TextColour: COLOUR_WHITE,
                                   kForm_BackgroundColour: COLOUR_RED,
                                   }];
}

-(void) ctaPressedWithKey:(NSString *) ctaKey
{
    if ([ctaKey isEqualToString: kChangePasswordCTA])
    {
        //change to vc later
//        if (GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
//            [self.delegate changePasswordHandler]; //get delegate to load change password instead
//        else
//            [self.changePasswordView setHidden: NO];
        [self popupChangePassword];
    }
    else if ([ctaKey isEqualToString: kUpdateProfileCTA])
    {
        [self updateProfilePressed];
    }
}

-(void) didPressWorkingHours
{
    if (!self.workingHoursDict ||
        [self.workingHoursDict isKindOfClass: [NSNull class]])
        self.workingHoursDict = @{@"WorkHourStart1":   @"0:00",
                                  @"WorkHourEnd1":     @"0:00",
                                  @"WeekDays1":        @"",
                                  @"PublicHoliday":    @"0",
                                  @"WorkHourStart2":   @"0:00",
                                  @"WorkHourEnd2":     @"0:00",
                                  @"WeekDays2":        @"",
                                  };
    
    //let tab bar push instead
    [mSession pushWorkingHoursView: self workingHoursData: self.workingHoursDict];
}

-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict
{
    //assign working hours
    self.workingHoursDict = workingHoursDict;
    
    [self.tableViewDelegate updateWorkingHours: self.workingHoursDict];
    
    [self.regFormTableView reloadData];
}

- (void) updateProfilePressed
{
    NSDictionary *jsonValuesDict = [self.tableViewDelegate getJsonValues];
    NSMutableDictionary *updateParams = [[NSMutableDictionary alloc] init];
    
    NSString *salutationKeyCodeString = [mSession convertToSalutationKeyCode: [jsonValuesDict objectForKey: kSalutation]];
    [updateParams setObject: salutationKeyCodeString forKey: @"Salutation"];

    [updateParams setObject:self.member.tradeID forKey:@"TradeID"];
    
//    //convert DOBMonth and Day properly
    [updateParams setObject: self.member.dobMonth forKey: @"DOBMonth"];
    [updateParams setObject: self.member.dobDay forKey: @"DOBDay"];
    
    [updateParams setObject: [jsonValuesDict objectForKey: kMobileNumber] forKey: kMobileNumber];
    [updateParams setObject: [jsonValuesDict objectForKey: kFirstName] forKey: kFirstName];
    [updateParams setObject: [jsonValuesDict objectForKey: kLastName] forKey: kLastName];
    [updateParams setObject: [jsonValuesDict objectForKey: kEmailAddress] forKey: kEmailAddress];
    
    [updateParams setObject: [jsonValuesDict objectForKey: kContactPreference] forKey: kContactPreference];
    
    
    if (GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
    {
    #pragma mark REG_Company API parameters
        NSMutableDictionary *regCompanyDict = [NSMutableDictionary new];
        if (IS_COUNTRY(COUNTRYCODE_INDIA))
        {
            [regCompanyDict setObject: [NSString stringWithFormat: @"%@ %@", [jsonValuesDict objectForKey:kFirstName], [jsonValuesDict objectForKey:kLastName]]
                               forKey: kCompanyName];
        }
        else
        {
            [regCompanyDict setObject: [jsonValuesDict objectForKey:kCompanyName] forKey: kCompanyName];
        }
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kContactNumber] forKey: kContactNumber];
        
        [regCompanyDict setObject: [jsonValuesDict objectForKey:kCompanyEmailAddress] forKey: kCompanyEmailAddress];
        if (self.companyInfo.workshopQRCode)
            [regCompanyDict setObject: self.companyInfo.workshopQRCode forKey: @"WorkshopQRCode"];
        
        if (IS_COUNTRY(COUNTRYCODE_INDIA))
        {
            
            [regCompanyDict setObject: [jsonValuesDict objectForKey:kOilChangePerMonth] forKey: kOilChangePerMonth];
            [regCompanyDict setObject: [jsonValuesDict objectForKey:kOilChangePercentage] forKey: kOilChangePercentage];
            [regCompanyDict setObject: [jsonValuesDict objectForKey:kHalfSyntheticOilChanges] forKey: kHalfSyntheticOilChanges];
            [regCompanyDict setObject: [jsonValuesDict objectForKey:kShellHelixQty] forKey: kShellHelixQty];
        }
        
        [updateParams setObject: regCompanyDict forKey: @"REG_Company"];
        
    #pragma mark REG_CompanyWorkHours API parameters
        [updateParams setObject: self.workingHoursDict forKey: @"REG_CompanyWorkHours"];
        
    #pragma mark REG_CompanyAddress API parameters
        NSMutableDictionary *regCompanyAddressDict = [NSMutableDictionary new];
        [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kAddress1] forKey: kAddress1];
        [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kAddress2] forKey: kAddress2];
        [regCompanyAddressDict setObject: @"1" forKey: @"AddressType"];
        [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kCompanyCity] forKey: kCompanyCity];
        NSString *stateString = [jsonValuesDict objectForKey: kCompanyState];
        [regCompanyAddressDict setObject: [mSession convertToStateKeyCode: stateString stateDetailedListArray:self.stateTable]
                                  forKey: kCompanyState];
        [regCompanyAddressDict setObject: @(self.latitude) forKey: @"Latitude"];
        [regCompanyAddressDict setObject: @(self.longitude) forKey: @"Longitude"];
        if(![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_HONGKONG]) {
            [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kPostalCode] forKey: kPostalCode];
        } else {
            [regCompanyAddressDict setObject:@"" forKey: kPostalCode];
        }
        [regCompanyAddressDict setObject:@"" forKey: @"StreetName"];
        
        [updateParams setObject: regCompanyAddressDict forKey: @"REG_CompanyAddress"];
        
        //    NSArray *companyService = @[@{@"ServiceID": @"1"},
        //                                @{@"ServiceID": @"2"},
        //                                ];
    }
    else
    {
        NSMutableDictionary *regCompanyDict = [NSMutableDictionary new];
        
        [regCompanyDict setObject: @"" forKey: @"CompanyCategory"];
        [regCompanyDict setObject: @"" forKey: kCompanyName];
        [regCompanyDict setObject: @"" forKey: @"CompanySource"];
        [regCompanyDict setObject: @"" forKey: @"CompanyType"];
        [regCompanyDict setObject: @"" forKey: @"ReferenceCode"];
        [regCompanyDict setObject: @"" forKey: @"WorkshopQRCode"];
        
        [updateParams setObject: regCompanyDict forKey: @"REG_Company"];
        
        
        [updateParams setObject: @(YES) forKey: @"TermCondition"];
        
        [updateParams setObject: @{} forKey: @"REG_CompanyWorkHours"];
        
        [updateParams setObject: @{kCompanyState : @""} forKey: @"REG_CompanyAddress"];
        
        [updateParams setObject: @(NO) forKey: @"EnableCustomerRedemption"];
    }
    [[WebServiceManager sharedInstance] updateTradeProfile: updateParams
                                                        vc:self];
}


-(NSString *) convertDateForDisplay:(NSString *)monthString day:(NSString *) dayString
{
    if ([monthString isEqualToString: BLANK] || [dayString isEqualToString: BLANK])
        return LOCALIZATION(C_PROFILE_DOB_PLACEHOLDER); //lokalise 30 jan
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    if ([GET_LOCALIZATION isEqualToString: kThai])
        [dateFormat setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
    else
        [dateFormat setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    [dateFormat setDateFormat:@"MMdd"];
    NSDate *date = [dateFormat dateFromString: [NSString stringWithFormat: @"%@%@", monthString, dayString]];
    
    // Convert date object to desired output format
    [dateFormat setDateFormat:@"dd MMMM"];
    return [dateFormat stringFromDate:date];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_FETCHTRADE:
            self.member = [[Member alloc] initWithData:[[response getGenericResponse] objectForKey: @"REG_Member"]];
            self.companyInfo = [[CompanyInfo alloc] initWithData: [[response getGenericResponse] objectForKey: @"REG_Company"]];
            self.companyAddress = [[CompanyAddress alloc] initWithData: [[response getGenericResponse] objectForKey: @"REG_CompanyAddress"]];
            
            self.latitude = self.companyAddress.latitude;
            self.longitude = self.companyAddress.longitude;
            
            self.workingHoursDict = [[response getGenericResponse] objectForKey: @"REG_CompanyWorkHours"];
            
            if (!self.workingHoursDict ||
                [self.workingHoursDict isKindOfClass: [NSNull class]]) {
                self.workingHoursDict = @{@"WorkHourStart1":   @"0:00",
                                        @"WorkHourEnd1":     @"0:00",
                                        @"WeekDays1":        @"",
                                        @"PublicHoliday":    @"0",
                                        @"WorkHourStart2":   @"0:00",
                                        @"WorkHourEnd2":     @"0:00",
                                        @"WeekDays2":        @"",
                                        };
            }
            
            if(GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
            {
                //if TO, load.
                //india uses another VC
                [[WebServiceManager sharedInstance] loadState: [[mSession lookupTable] objectForKey: @"LookUpVersion"]
                                                  countryCode: GET_COUNTRY_CODE
                                                           vc: self];
            }
            else
            {
                [self setupInterface];
            }
            break;
        case kWEBSERVICE_FETCHHOME:
        {
            //for DSR/Mechanic. Trade Owner handled in TabView
            ProfileInfo *profileInfo = [[ProfileInfo alloc] initWithData: [[response getGenericResponse] objectForKey: @"MyProfile"]];
            [mSession setProfileInfo: profileInfo];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"Unread Icon Changed" object: nil];
            
//            self.lblFieldOne.text = [[mSession profileInfo] fullName];
//            self.lblFieldTwo.text = [[mSession profileInfo] shareCode];
//            //            self.lblFieldThree.text = @([[mSession profileInfo] bonusPoint]).stringValue;
//            self.lblFieldThree.text = [[mSession profileInfo] rank];
//
//            NSString *urlString = [[[mSession profileInfo] profileImage] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
//
//            [self.imgProfilePhoto sd_setImageWithURL:[NSURL URLWithString: urlString]
//                                    placeholderImage:[UIImage imageNamed:@"icon-profile"]
//                                             options:SDWebImageRefreshCached];
//
//            CGFloat sliderFillAmt = [[mSession profileInfo] profileCompleteness];
//            self.lblProgress.text = [NSString stringWithFormat: @"%3.0f%%", sliderFillAmt];
//            self.progressBarWidth.constant = self.progressBar.frame.size.width * (sliderFillAmt / 100);
            
            LoginInfo *loginInfo = [mSession loadUserProfile];
            [[WebServiceManager sharedInstance] fetchTrade:loginInfo.tradeID vc:self];
        }
            break;
        case kWEBSERVICE_STATE: {
            SET_LOADSTATE([[response getGenericResponse] objectForKey: @"StateDetailsList"]);
            self.stateTable = [[response getGenericResponse] objectForKey: @"StateDetailsList"];
            [self setupInterface];
        }
            break;
        case kWEBSERVICE_UPDATETRADEPROFILE:
        {
            [mAlert showSuccessAlertWithMessage:[response getResponseMessage] onCompletion:^(BOOL finished) {
                LoginInfo *loginInfo = [mSession loadUserProfile];
                [[WebServiceManager sharedInstance] fetchTrade:loginInfo.tradeID vc:self];
            }];
        }
            break;
        case kWEBSERVICE_UPDATEWORKSHOP:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                LoginInfo *loginInfo = [mSession loadUserProfile];
                [[WebServiceManager sharedInstance] fetchTrade:loginInfo.tradeID vc:self];
            }];
            break;
        }
//        case kWEBSERVICE_CHANGEPASSWORD:
//        {
//            [mAlert showSuccessAlertWithMessage:[response getResponseMessage] onCompletion:^(BOOL finished) {
//                [self cancelChangePassword: nil];
//            }];
//        }
//            break;
        default:
            break;
    }
}

#pragma mark - change to popup vc later
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.delegate textfieldHandler: YES];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.delegate textfieldHandler: NO];
}

- (IBAction)changePasswordPressed:(id)sender {
//    if (GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
//        [self.delegate changePasswordHandler]; //get delegate to load change password instead
//    else
    [self.changePasswordView setHidden: NO];
}

- (IBAction)cancelChangePassword:(id)sender {
    [self.changePasswordView setHidden: YES];
    self.txtOldPassword.text = @"";
    self.txtNewPassword.text = @"";
    self.txtConfirmPassword.text = @"";
    
}

- (void)textfieldWithButtonPressedWithKey:(NSString *)buttonKey
{
    if([buttonKey isEqualToString:kWorkshopLocation])
    {
        [self locationPressed];
    }
}

- (IBAction)submitChangePasswordPressed:(id)sender {
    if (![self.txtNewPassword.text isEqualToString: self.txtConfirmPassword.text])
    {
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_CHANGEPASSWORD_MISMATCH_ERRORMSG) title:LOCALIZATION(C_GLOBAL_ERROR) closeButtonTitle: LOCALIZATION(C_POPUPMSG_SUBMITAGAIN)];  //lokalise 30 jan
        
        return;
    }
    [[WebServiceManager sharedInstance] changePasswordPostLogin:self.txtOldPassword.text
                                                    newPassword:self.txtNewPassword.text
                                                             vc: self];
}

- (void)tableViewDidScroll:(UIScrollView *)tableView
{
    [self.backgroundScrollView setContentSize:self.regFormTableView.contentSize];
    [self.backgroundScrollView setContentOffset:CGPointMake(0, tableView.contentOffset.y)];
}

- (void)locationPressed {
    [UpdateHUD addMBProgress: self.view withText: @""];
    switch ([CLLocationManager authorizationStatus])
    {
        case kCLAuthorizationStatusAuthorizedAlways:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusNotDetermined:
            [UpdateHUD removeMBProgress: self];
            [self.locationManager requestWhenInUseAuthorization];
            break;
        case kCLAuthorizationStatusDenied:
        {
            [UpdateHUD removeMBProgress: self];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_ALERTVIEW_ERROR) message:LOCALIZATION(C_ALERTVIEW_PERMISSIONERRORMSG) preferredStyle:UIAlertControllerStyleAlert]; //lokalise 30 jan
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_CANCEL)  style:UIAlertActionStyleCancel handler:nil]; //lokalise 30 jan
            UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_SETTING) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                            UIApplicationOpenSettingsURLString]];  //lokalise 30 jan
            }];
            
            [alertController addAction:cancelAction];
            [alertController addAction:settingsAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
            break;
        case kCLAuthorizationStatusRestricted:
            [UpdateHUD removeMBProgress: self];
            break;
    }
    
}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [UpdateHUD removeMBProgress: self];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    //most recent location update is at the end of the array.
    CLLocation *location = [locations lastObject];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:6];
    
    NSString *newLatitude   = [formatter stringFromNumber: @(location.coordinate.latitude)];
    NSString *newLongitude  = [formatter stringFromNumber: @(location.coordinate.longitude)];
    
    [self.tableViewDelegate editCell:kWorkshopLocation withTextfieldButtonText:[NSString stringWithFormat: @"%@, %@", newLatitude, newLongitude]];
    
    self.latitude = newLatitude.doubleValue;
    self.longitude = newLongitude.doubleValue;
    
    [self.locationManager stopUpdatingLocation];
    [UpdateHUD removeMBProgress: self];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
