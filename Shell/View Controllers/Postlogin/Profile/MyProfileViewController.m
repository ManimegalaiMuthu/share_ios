//
//  MyProfileViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 23/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "MyProfileViewController.h"
#import "CASDatePickerView.h"
#import "ProfileInfo.h"

#define CELL_HEIGHT 72

#define kUserID             @"UserID"
#define kDSRCode            @"DSRCode"
#define kUserPassword       @"UserPassword"
#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kEmailAddress       @"EmailAddress"
#define kDOB                @"DOB"
#define kDOBMonth           @"DOBMonth"
#define kDOBDay             @"DOBDay"
#define kCountryCode        @"CountryCode"

enum kMyProfile_TYPE
{
    kMyProfile_SALUTATION,
    kMyProfile_FIRSTNAME,
    kMyProfile_LASTNAME,
    kMyProfile_DOB,
    kMyProfile_MOBNUM,
    kMyProfile_EMAIL,
};

@interface MyProfileViewController ()<UITextFieldDelegate, WebServiceManagerDelegate, CommonListDelegate, DatePickerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *changePasswordView;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UILabel *lblChangePassword;
@property (weak, nonatomic) IBOutlet UITextField *txtOldPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;

@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePhoto;

//DSR - company name
@property (weak, nonatomic) IBOutlet UILabel *lblFieldOne;

//DSR -
@property (weak, nonatomic) IBOutlet UILabel *lblFieldTwo;

//DSR - points
@property (weak, nonatomic) IBOutlet UILabel *lblFieldThree;

@property (weak, nonatomic) IBOutlet UIImageView *imgProgressBubble;
@property (weak, nonatomic) IBOutlet UIView *progressBar;   //grey bar
@property (weak, nonatomic) IBOutlet UILabel *lblProgress;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressBarWidth; //yellow bar width

@property (weak, nonatomic) IBOutlet UILabel *lblCompleteProfile;


@property (weak, nonatomic) IBOutlet UILabel *lblContactPref;
@property (weak, nonatomic) IBOutlet UIButton *btnChangePassword;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdateWorkshop;
@property CASDatePickerView *dateView;
@property ProfileInfo *profileInfo;

@property (weak, nonatomic) IBOutlet UIView *profileInfoView; //to remove for trade owner
#pragma mark contact Pref
@property (weak, nonatomic) IBOutlet UICollectionView *colView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *colViewHeight; //to adjust according to number of cells <1 + (count / 2)>
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *colViewLayout; //to adjust cell width size according to screen width
//@property NSInteger contactPreferenceBitValue;
@property NSMutableArray *contactPrefArray;

@property NSMutableArray *registrationFormArray; //ui only
@property NSMutableDictionary *regSubmissionDict;  //key as json key, value = cell. retrieve value by using getJson method
@property (weak, nonatomic) IBOutlet UITableView *regFormTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight; //to adjust according number of cells

@property (weak, nonatomic) IBOutlet UILabel *lblRequiredFields;


@end

@implementation MyProfileViewController
@synthesize listView;
@synthesize dateView;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.profileInfo = [mSession profileInfo];
    
    
    [self.colView registerNib: [UINib nibWithNibName: @"ContactPreferencesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ContactPrefCell"];
    
    if (GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
    {
//        LoginInfo *loginInfo = [mSession loadUserProfile];
//        [[WebServiceManager sharedInstance] fetchTrade:loginInfo.tradeID vc:self];
//
        [self.profileInfoView removeFromSuperview];
    }
    else
    {
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_MYPROFILE) subtitle: @""];    //lokalised 11 Feb
        [[WebServiceManager sharedInstance] fetchHome: self];
    }
    [self setupInterface];
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc]
                                             initWithTarget:self
                                             action:@selector(handleTap:)];
    gesRecognizer.delegate = self;
 
    
    NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @"*" attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B3, }];
    
    NSMutableAttributedString *requiredFieldsAttrString = [[NSMutableAttributedString alloc] initWithString: LOCALIZATION(C_PROFILE_REQUIREDFIELDS) attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B3, }];   //lokalised 11 Feb
    
    [asteriskAttrString appendAttributedString:  requiredFieldsAttrString];
    
    //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
    self.lblRequiredFields.attributedText = asteriskAttrString;

    
    // Add Gesture to your view.
    [self.changePasswordView addGestureRecognizer:gesRecognizer];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@",LOCALIZATION_EN(C_SIDEMENU_MYPROFILE)] screenClass:nil];
}

- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer{
    self.changePasswordView.hidden=YES;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    [self setupDelayInterface];
}

-(void) setupDelayInterface
{
    //handle check boxes collection view
    
    CGFloat colviewHeight = (COLVIEW_CELLHEIGHT + COLVIEW_LINESPACING) * (1 + ([[mSession getContactPreference] count] / 2));
    self.colViewHeight.constant = colviewHeight;
    
    CGFloat cellWidth = (self.colView.frame.size.width / 2) - 10; //10 = cell spacing, check IB
    self.colViewLayout.itemSize = CGSizeMake(cellWidth, 50);
}

-(void) setupInterface
{
    self.imgProfilePhoto.layer.cornerRadius = self.imgProfilePhoto.frame.size.width / 2;
    
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_COUNTRY)    //lokalised 11 Feb
                                                   list: [mSession getCountryNames]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
    
    dateView = [[CASDatePickerView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_BIRTHDAY)previouslySelected:nil withYearHidden:YES];    //lokalised 11 Feb
    dateView.delegate = self;
    
    self.lblFieldOne.font = FONT_B1;
    self.lblFieldTwo.font = FONT_B1;
    self.lblFieldThree.font = FONT_B1;
    
    self.lblCompleteProfile.font = FONT_H2;
    self.lblCompleteProfile.text = LOCALIZATION(C_COMPLETE_YOUR_PROFILE);   //lokalised 11 Feb
    
    [self.lblContactPref setFont: FONT_B1];
    self.lblContactPref.text = LOCALIZATION(C_TITLE_CONTACT_PREFERENCE);    //lokalised 11 Feb
    
    //format progress bar
    self.lblProgress.font = FONT_B2;
    [self.lblProgress sizeToFit];
    UIImage *bubbleImage = GET_ISADVANCE ? [UIImage imageNamed:@"icon-bubble_blue"] : [UIImage imageNamed:@"icon-bubble"];
    self.imgProgressBubble.image = [bubbleImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 3.5, 0)  resizingMode:UIImageResizingModeStretch];
    
    //misc.
    [self.btnChangePassword.titleLabel setFont: FONT_BUTTON];
    [self.btnChangePassword setTitle:LOCALIZATION(C_FORM_CHANGEPASSWORD) forState:UIControlStateNormal];    //lokalised 11 Feb
    [Helper setCustomFontButtonContentModes: self.btnChangePassword];
    
    [self.btnUpdateWorkshop.titleLabel setFont: FONT_BUTTON];
    [self.btnUpdateWorkshop setTitle:LOCALIZATION(C_FORM_UPDATEPROFILE) forState:UIControlStateNormal]; //lokalised 11 Feb
    [Helper setCustomFontButtonContentModes: self.btnUpdateWorkshop];
    [self.btnUpdateWorkshop setBackgroundColor:COLOUR_RED];
    
    
    self.lblChangePassword.font = FONT_H1;
    self.lblChangePassword.text = LOCALIZATION(C_FORM_CHANGEPASSWORD);  //lokalised 11 Feb
    
    self.txtOldPassword.font = FONT_B2;
    self.txtOldPassword.placeholder = LOCALIZATION(C_PLACEHOLDER_CURRENTPASSWORD);  //lokalised 11 Feb
    
    self.txtNewPassword.font = self.txtOldPassword.font;
    self.txtNewPassword.placeholder = LOCALIZATION(C_PLACEHOLDER_NEWPASSWORD);  //lokalised 11 Feb
    
    self.txtConfirmPassword.font = self.txtOldPassword.font;
    self.txtConfirmPassword.placeholder = LOCALIZATION(C_PLACEHOLDER_RE_NEWPASSWORD);   //lokalised 11 Feb
    
    [self.btnSubmit.titleLabel setFont: FONT_BUTTON];
    [self.btnSubmit setTitle:LOCALIZATION(C_FORM_SUBMIT) forState:UIControlStateNormal];    //lokalised 11 Feb
    [Helper setCustomFontButtonContentModes: self.btnSubmit];
    [self.btnSubmit setBackgroundColor:COLOUR_RED];
    
    [self.btnCancel.titleLabel setFont: FONT_BUTTON];
    [self.btnCancel setTitle:LOCALIZATION(C_FORM_CANCEL) forState:UIControlStateNormal];    //lokalised 11 Feb
    [Helper setCustomFontButtonContentModes: self.btnCancel];
}

-(void) setupRegistrationForm
{
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormTextfieldCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormTextfieldCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationButtonCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormDropdownCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormDropdownCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationHeaderTableViewCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationLabelTextfieldTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationLabelTextfieldTableViewCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationMobileNumberCell" bundle:nil] forCellReuseIdentifier:@"RegistrationMobileNumberCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationTextfieldWithButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextfieldWithButtonCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationTextViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextViewTableViewCell"];
    
    self.regSubmissionDict = [[NSMutableDictionary alloc] init];
    
    self.registrationFormArray = [[NSMutableArray alloc] initWithArray:
                                  @[
#pragma mark - Owner Profile Form setup
                                    @{@"Title": LOCALIZATION(C_PROFILE_SALUTATION), //lokalised 11 Feb
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key": kSalutation,
                                      @"Value": [mSession convertToSalutation: self.member.salutation],
                                      @"Optional": @(YES),
                                      },  //button
                                    @{@"Title": LOCALIZATION(C_PROFILE_FIRSTNAME),  //lokalised 11 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kFirstName,
                                      @"Value": self.member.firstName,
                                      }, //first name
                                    @{@"Title": LOCALIZATION(C_PROFILE_LASTNAME),   //lokalised 11 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kLastName,
                                      @"Value": self.member.lastName,
                                      }, //last name
                                    @{@"Title": LOCALIZATION(C_PROFILE_DOB),    //lokalised 11 Feb
                                      @"Type": @(kREGISTRATION_BIRTHDATE),
                                      @"Key": kDOB,
                                      @"Value": [self convertDateForDisplay:self.member.dobMonth day:self.member.dobDay],
                                      @"Optional": @(YES),
                                      },   //Date of birth
//                                    @{@"Title": LOCALIZATION(C_PROFILE_MOBILENUM),
//                                      @"Type": @(kREGISTRATION_MOBNUM),
//                                      @"Key": kMobileNumber,
//                                      @"CountryCode": [mSession convertCountryCodeToCountryName: GET_COUNTRY_CODE],
//                                      @"Enabled": @(NO),
//                                      @"MobileNumber": self.member.mobileNumber,
//                                      },   //Mobile number
                                    @{@"Title": LOCALIZATION(C_PROFILE_MOBILENUM),  //lokalised 11 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kMobileNumber,
                                      @"Enabled": @(NO),
                                      @"Value": [NSString stringWithFormat: @"%@%@", [[[mSession convertCountryCodeToCountryDialingCode: GET_COUNTRY_CODE] componentsSeparatedByString: @" "] firstObject], self.member.mobileNumber],
                                      }, //email
                                    @{@"Title": LOCALIZATION(C_PROFILE_EMAILADDRESS),   //lokalised 11 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kEmailAddress,
                                      @"Keyboard": @(UIKeyboardTypeEmailAddress),
                                      @"Value": self.member.emailAddress,
                                      @"Placeholder": LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER), //lokalised 11 Feb
                                      @"Optional": @(YES),
                                      }, //email
                                    ]
                                  ];
    
    //set tableview height. 4 shorter cells
    self.tableViewHeight.constant = CELL_HEIGHT * [self.registrationFormArray count];
    self.regFormTableView.tableFooterView = [UIView new];
    [self.regFormTableView reloadData];
    
    //reset contact pref array before every reload
    self.contactPrefArray = [[NSMutableArray alloc] init];
    [self.colView reloadData];
}

-(NSString *) convertDateForDisplay:(NSString *)monthString day:(NSString *) dayString
{
    if ([monthString isEqualToString: BLANK] || [dayString isEqualToString: BLANK])
        return LOCALIZATION(C_PROFILE_DOB_PLACEHOLDER); //lokalised 11 Feb
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    [dateFormat setDateFormat:@"MMdd"];
    NSDate *date = [dateFormat dateFromString: [NSString stringWithFormat: @"%@%@", monthString, dayString]];
    
    // Convert date object to desired output format
    [dateFormat setDateFormat:@"dd MMMM"];
    return [dateFormat stringFromDate:date];
}

#pragma mark - tableview delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.registrationFormArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *regFieldData = [self.registrationFormArray objectAtIndex: indexPath.row];
    switch ([[regFieldData objectForKey: @"Type"] intValue])
    {
        case kREGISTRATION_TEXTFIELD:
        {
            RegistrationFormTextfieldCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormTextfieldCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell enableCell: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            

            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [textfieldCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [textfieldCell hideTopLabel: NO];
            [textfieldCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
            
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            [textfieldCell setTextfieldText: [regFieldData objectForKey: @"Value"]];
            
            [textfieldCell setTextfieldDelegate: self];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_DROPDOWN:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [dropdownCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [dropdownCell hideTopLabel: NO];
            dropdownCell.button.tag = indexPath.row;
            
            switch (indexPath.row) {
                case kMyProfile_SALUTATION:
                    [dropdownCell.button addTarget: self action: @selector(salutationPressed:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                default:
                    break;
            }
            //set initial value
            if ([regFieldData objectForKey: @"Value"])
                [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            
            //keep a reference of cell to submit json value later
            [self.regSubmissionDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        case kREGISTRATION_BUTTON:
        {
            RegistrationButtonCell *buttonCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationButtonCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [buttonCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [buttonCell setTitle: [regFieldData objectForKey: @"Title"]];
            [buttonCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [buttonCell hideTopLabel: NO];
            
            [self.regSubmissionDict setObject: buttonCell forKey: [regFieldData objectForKey: @"Key"]];
            return buttonCell;
        }
            break;
        case kREGISTRATION_MOBNUM:
        {
            RegistrationMobileNumberCell *mobNumCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationMobileNumberCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [mobNumCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];

            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [mobNumCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [mobNumCell hideTopLabel: NO];
            [mobNumCell setCountryCode: [regFieldData objectForKey: @"CountryCode"]];
            [mobNumCell setMobileNumber: [regFieldData objectForKey: @"MobileNumber"]];
            
            [self.regSubmissionDict setObject: mobNumCell forKey: [regFieldData objectForKey: @"Key"]];
            return mobNumCell;
        }
            break;
        case kREGISTRATION_BIRTHDATE:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            //Salutation
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            [dropdownCell hideTopLabel: NO];
            dropdownCell.button.tag = indexPath.row;
            
            [dropdownCell.button addTarget: self action: @selector(birthdayPressed:) forControlEvents:UIControlEventTouchUpInside];
            [self.regSubmissionDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        default:
            NSLog(@"UNKNOWN CELL TYPE");
            return [UITableViewCell new];
            break;
    }
    return [UITableViewCell new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[mSession getContactPreference] count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ContactPreferencesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"ContactPrefCell" forIndexPath: indexPath];
    
    UIButton *checkboxBtn = [cell viewWithTag: 1]; //tagged in storyboard
    
    UILabel *contactPref = [cell viewWithTag: 2]; //tagged in storyboard
    contactPref.text = [[mSession getContactPreference] objectAtIndex: [indexPath row]];
    contactPref.font = FONT_B1;
    
    cell.bitValue = [[mSession convertToContactPreferenceKeyCode: contactPref.text] intValue];
    
    if ([[self.member.contactPreference componentsSeparatedByString: @","] containsObject: @(cell.bitValue).stringValue])
        checkboxBtn.selected = YES;
    
    if (!self.contactPrefArray)
        self.contactPrefArray = [[NSMutableArray alloc] init];
    [self.contactPrefArray addObject: cell];
    
    return cell;
}

- (IBAction)checkBoxSelected:(UIButton *)sender {
    if ([sender isSelected])
        sender.selected = NO;
    else
        sender.selected = YES;
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        RegistrationFormDropdownCell *cell;
        switch (listView.tag)
        {
            case kMyProfile_SALUTATION:
                cell = [self.regSubmissionDict objectForKey: kSalutation];
                break;
                //            case kPopupSelection_Country:
                //                [self.btnCountryCode setTitle:selection.firstObject forState:UIControlStateNormal];
                //                break;
        }
        [cell.button setTitle:selection.firstObject forState:UIControlStateNormal];
    }
}

- (void)selectedDate:(NSDate *)selectedDate
{
    //do smething
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //   for API
    [formatter setDateFormat:@"dd"];
    self.member.dobDay = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"MM"];
    self.member.dobMonth = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"dd MMMM"];
    
    [formatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    NSString *selectedDateString = [formatter stringFromDate:selectedDate];

    
    RegistrationFormDropdownCell *cell = [self.regSubmissionDict objectForKey: kDOB];
    [cell setSelectionTitle: selectedDateString];
}

- (IBAction)birthdayPressed:(id)sender {
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}
- (IBAction)salutationPressed:(UIButton *)sender {
    
    listView.tag = sender.tag;
    [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO]; //lokalised 11 Feb
    
    [self popUpView];
}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_FETCHTRADE:
            self.member = [[Member alloc] initWithData:[[response getGenericResponse] objectForKey: @"REG_Member"]];
            [self setupRegistrationForm];
            break;
        case kWEBSERVICE_FETCHHOME:
        {
            //for DSR/Mechanic. Trade Owner handled in TabView
            [[NSNotificationCenter defaultCenter] postNotificationName: @"Unread Icon Changed" object: nil];

            self.lblFieldOne.text = [[mSession profileInfo] fullName];
            self.lblFieldTwo.text = [[mSession profileInfo] shareCode];
//            self.lblFieldThree.text = @([[mSession profileInfo] bonusPoint]).stringValue;
            self.lblFieldThree.text = [[mSession profileInfo] rank];
            
            NSString *urlString = [[[mSession profileInfo] profileImage] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            [self.imgProfilePhoto sd_setImageWithURL:[NSURL URLWithString: urlString]
                                    placeholderImage:[UIImage imageNamed:@"icon-profile"]
                                             options:SDWebImageRefreshCached];
            
            CGFloat sliderFillAmt = [[mSession profileInfo] profileCompleteness];
            self.lblProgress.text = [NSString stringWithFormat: @"%3.0f%%", sliderFillAmt];
            self.progressBarWidth.constant = self.progressBar.frame.size.width * (sliderFillAmt / 100);
            
            LoginInfo *loginInfo = [mSession loadUserProfile];
            [[WebServiceManager sharedInstance] fetchTrade:loginInfo.tradeID vc:self];
        }
            break;
        case kWEBSERVICE_UPDATETRADEPROFILE:
            [mAlert showSuccessAlertWithMessage:[response getResponseMessage] onCompletion:^(BOOL finished) {
            }];
            break;
        case kWEBSERVICE_CHANGEPASSWORD:
        {
            [mAlert showSuccessAlertWithMessage:[response getResponseMessage] onCompletion:^(BOOL finished) {
                [self cancelChangePassword: nil];
            }];
        }
            break;
        default:
            break;
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.delegate textfieldHandler: YES];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.delegate textfieldHandler: NO];
}

- (IBAction)changePasswordPressed:(id)sender {
    if (GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
        [self.delegate changePasswordHandler]; //get delegate to load change password instead
    else
        [self.changePasswordView setHidden: NO];
}

- (IBAction)cancelChangePassword:(id)sender {
    [self.changePasswordView setHidden: YES];
    self.txtOldPassword.text = @"";
    self.txtNewPassword.text = @"";
    self.txtConfirmPassword.text = @"";
    
}

- (IBAction)submitChangePasswordPressed:(id)sender {
    if (![self.txtNewPassword.text isEqualToString: self.txtConfirmPassword.text])
    {
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_CHANGEPASSWORD_MISMATCH_ERRORMSG) title:LOCALIZATION(C_GLOBAL_ERROR) closeButtonTitle: LOCALIZATION(C_POPUPMSG_SUBMITAGAIN)]; //lokalised 11 Feb

        return;
    }
    [[WebServiceManager sharedInstance] changePasswordPostLogin:self.txtOldPassword.text
                                                    newPassword:self.txtNewPassword.text
                                                             vc: self];
}

- (IBAction)updateWorkshopPressed:(id)sender {
    
    NSString *contactPrefString = @"";
    for (ContactPreferencesCollectionViewCell* cell in self.contactPrefArray)
    {
        if ([cell.checkboxBtn isSelected])
        {
            if (contactPrefString.length == 0)
                contactPrefString = @(cell.bitValue).stringValue;
            else
                contactPrefString = [contactPrefString stringByAppendingString: [NSString stringWithFormat: @",%@", @(cell.bitValue).stringValue]];
        }
    }
    self.member.contactPreference    = contactPrefString;
    
    self.member.salutation = [[mSession convertToSalutationKeyCode: [[self.regSubmissionDict objectForKey: kSalutation] getJsonValue]] intValue];
    self.member.firstName =  [[self.regSubmissionDict objectForKey:kFirstName] getJsonValue];
    self.member.lastName =  [[self.regSubmissionDict objectForKey:kLastName] getJsonValue];
//    self.member.mobileNumber =  [[self.regSubmissionDict objectForKey:kMobileNumber] getMobileNumberJsonValue];
    self.member.mobileNumber =  [[self.regSubmissionDict objectForKey:kMobileNumber] getJsonValue];
    self.member.emailAddress =  [[self.regSubmissionDict objectForKey:kEmailAddress] getJsonValue];
    
    
    NSDictionary *updateParams = @{@"TradeID":              self.member.tradeID,
                                   @"Salutation":           @(self.member.salutation),
                                   @"FirstName":            self.member.firstName,
                                   @"LastName":             self.member.lastName,
                                   @"MobileNumber":         self.member.mobileNumber,
                                   @"EmailAddress":         self.member.emailAddress,
                                   @"ContactPreferences":   self.member.contactPreference,
                                   @"DOBMonth":             self.member.dobMonth,
                                   @"DOBDay":               self.member.dobDay,
                                   };
    
    [[WebServiceManager sharedInstance] updateTradeProfile: updateParams
                                                        vc:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
