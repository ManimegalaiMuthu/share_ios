//
//  WorkshopProfileViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 23/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "WorkingHoursViewController.h"
#import "WorkshopProfileViewController.h"


#define kCompanyName            @"CompanyName"
#define kContactNumber          @"ContactNumber"
#define kCompanyEmailAddress    @"CompanyEmailAddress"
#define kAddress1       @"Address1"
#define kAddress2       @"Address2"
#define kCompanyCity    @"CompanyCity"
#define kCompanyState   @"CompanyState"
#define kPostalCode     @"PostalCode"

#define kWorkingHours       @"WorkingHours"


enum kManageWorkshopAddNew_TYPE
{
    kWorkshopProfile_WORKSHOPNAME,
    kWorkshopProfile_TELEPHONE,
    kWorkshopProfile_EMAIL,
    kWorkshopProfile_ADDRESS1,
    kWorkshopProfile_ADDRESS2,
    kWorkshopProfile_TOWN,
    kWorkshopProfile_STATE,
    kWorkshopProfile_POSTALCODE,
    kWorkshopProfile_WORKINGHOURS,
};

#define CELL_HEIGHT 72
#define WORKINGHOURS_CELL_HEIGHT 44
@interface WorkshopProfileViewController ()<UITableViewDelegate,UITableViewDataSource, WebServiceManagerDelegate, CommonListDelegate, UITextFieldDelegate, WorkingHoursViewDelegate>

@property Member *member;
@property CompanyInfo *companyInfo;
@property CompanyAddress *companyAddress;
@property NSDictionary *workingHoursDict;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeightLayout;
@property NSMutableArray *registrationFormArray;
@property NSMutableDictionary *regSubmissionDict;  //key as json key, value = cell. retrieve value by using getJson method
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;

@property (weak, nonatomic) IBOutlet UILabel *lblRequiredFields;
@end

@implementation WorkshopProfileViewController
@synthesize listView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    

    
//    LoginInfo *loginInfo = [mSession loadUserProfile];
//    [[WebServiceManager sharedInstance] fetchTrade: loginInfo.tradeID
//                                                vc:self];
    
    // Do any additional setup after loading the view.
}

-(void) loadProfileData
{
    self.member = [[Member alloc] initWithData:[self.workshopDetailsData objectForKey: @"REG_Member"]];
    self.companyInfo = [[CompanyInfo alloc] initWithData: [self.workshopDetailsData objectForKey: @"REG_Company"]];
    self.companyAddress = [[CompanyAddress alloc] initWithData: [self.workshopDetailsData objectForKey: @"REG_CompanyAddress"]];
    
    self.workingHoursDict = [self.workshopDetailsData objectForKey: @"REG_CompanyWorkHours"];
    
    if (!self.workingHoursDict ||
        [self.workingHoursDict isKindOfClass: [NSNull class]])
        self.workingHoursDict = @{@"WorkHourStart1":   @"0:00",
                                  @"WorkHourEnd1":     @"0:00",
                                  @"WeekDays1":        @"",
                                  @"PublicHoliday":    @"0",
                                  @"WorkHourStart2":   @"0:00",
                                  @"WorkHourEnd2":     @"0:00",
                                  @"WeekDays2":        @"",
                                  };
    
    [self setupRegistrationForm];
    [self setupInterface];
    
    NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @"*" attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B3, }];
    
    NSMutableAttributedString *requiredFieldsAttrString = [[NSMutableAttributedString alloc] initWithString: LOCALIZATION(C_PROFILE_REQUIREDFIELDS) attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B3, }];   //lokalised 11 Feb
    
    [asteriskAttrString appendAttributedString:  requiredFieldsAttrString];
    
    //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
    self.lblRequiredFields.attributedText = asteriskAttrString;

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName: LOCALIZATION_EN(C_HEADER_WORKSHOPPROFILE) screenClass:nil];
}

-(void) setupRegistrationForm
{
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationFormTextfieldCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormTextfieldCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationButtonCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationFormDropdownCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormDropdownCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationHeaderTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationLabelTextfieldTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationLabelTextfieldTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationMobileNumberCell" bundle:nil] forCellReuseIdentifier:@"RegistrationMobileNumberCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationTextfieldWithButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextfieldWithButtonCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationTextViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextViewTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationWorkingHoursTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationWorkingHoursTableViewCell"];
    
    self.regSubmissionDict = [[NSMutableDictionary alloc] init];
    
    self.registrationFormArray = [[NSMutableArray alloc] initWithArray:
#pragma mark - Owner Profile Form setup
                                  @[@{@"Title": LOCALIZATION(C_PROFILE_COMPANYNAME),    //lokalised 11 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kCompanyName,
                                      @"Value": [self isNullChecker:[self.companyInfo companyName]],
                                      }, //workshop name
                                    @{@"Title": LOCALIZATION(C_PROFILE_CONTACTNUMBER),  //lokalised 11 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kContactNumber,
                                      @"Keyboard": @(UIKeyboardTypeNumberPad),
                                      @"Value":  [self isNullChecker:[self.companyInfo contactNumber]],
                                      @"Optional": @(YES),
                                      }, //telephone
                                    @{@"Title": LOCALIZATION(C_PROFILE_WORKSHOPEMAILADDRESSOPT),    //lokalised 11 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kCompanyEmailAddress,
                                      @"Keyboard": @(UIKeyboardTypeEmailAddress),
                                      @"Value": [self isNullChecker:[self.companyInfo companyEmailAddress]],
                                      @"Optional": @(YES),
                                      }, //email
                                    @{@"Title": LOCALIZATION(C_PROFILE_ADDRESSLINE1),   //lokalised 11 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kAddress1,
                                      @"Value": [self isNullChecker:[self.companyAddress address1]],
                                      @"Enabled": @([[mSession profileInfo] canEditAddress]),
                                      }, //address 1
                                    @{@"Title": LOCALIZATION(C_PROFILE_ADDRESSLINE2),   //lokalised 11 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kAddress2,
                                      @"Value": [self isNullChecker:[self.companyAddress address2]],
                                      @"Enabled": @([[mSession profileInfo] canEditAddress]),
                                      @"Optional": @(YES),
                                      }, //address 2
                                    @{@"Title": LOCALIZATION(C_PROFILE_TOWN),   //lokalised 11 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kCompanyCity,
                                      @"Value": [self isNullChecker:[self.companyAddress companyCity]],
                                      @"Enabled": @([[mSession profileInfo] canEditAddress]),
                                      @"Optional": @(YES),
                                      }, //town
                                    @{@"Title": LOCALIZATION(C_PROFILE_STATE),  //lokalised 11 Feb
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key":kCompanyState,
                                      @"Value": [mSession convertToStateName:self.companyAddress.companyState stateDetailedListArray:self.stateTable],
                                      @"Enabled": @([[mSession profileInfo] canEditAddress]),
                                      @"Optional": @(YES),
                                      },  //state
                                    @{@"Title": LOCALIZATION(C_PROFILE_POSTALCODE), //lokalised 11 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kPostalCode,
                                      @"Keyboard": @(UIKeyboardTypeNumberPad),
                                      @"Value": [self isNullChecker:[self.companyAddress postalCode]],
                                      @"Enabled": @([[mSession profileInfo] canEditAddress]),
                                      }, //postal
                                    @{@"Title": LOCALIZATION(C_PROFILE_WORKINGHOURS),   //lokalised 11 Feb
                                      @"Type": @(kREGISTRATION_WORKINGHOURS),
//                                      @"Enabled": @(YES),
                                      @"Key": kWorkingHours,
//                                      @"Value": LOCALIZATION(C_PROFILE_WORKINGHOURS),
                                      },
                                    ]
                                  ];
    
    NSArray *fullWorkWeekDays;
    NSArray *halfWorkWeekDays;
    
    if (![[self.workingHoursDict objectForKey: @"WeekDays1"] isEqualToString: @""])
        fullWorkWeekDays = [[self.workingHoursDict objectForKey: @"WeekDays1"] componentsSeparatedByString: @","];
    
    if (![[self.workingHoursDict objectForKey: @"WeekDays2"] isEqualToString: @""])
        halfWorkWeekDays = [[self.workingHoursDict objectForKey: @"WeekDays2"] componentsSeparatedByString: @","];
    
    self.tableHeightLayout.constant = CELL_HEIGHT * [self.registrationFormArray count] + (44 * ([fullWorkWeekDays count] + [halfWorkWeekDays count]));
    
//    self.tableHeightLayout.constant = CELL_HEIGHT * [self.registrationFormArray count] - workingHoursOffset;
    self.tableView.tableFooterView = [UIView new];
    [self.tableView reloadData];
}

-(NSString*)isNullChecker:(NSString *)stringValue
{
    return [stringValue isKindOfClass: [NSNull class]]||stringValue.length==0?@"":stringValue;
}

-(void) setupInterface
{
    [self.btnRegister.titleLabel setFont: FONT_BUTTON];
    [self.btnRegister setTitle:LOCALIZATION(C_FORM_UPDATEWORKSHOPPROFILE) forState:UIControlStateNormal];   //lokalised 11 Feb
    [Helper setCustomFontButtonContentModes: self.btnRegister];
    [self.btnRegister setBackgroundColor:COLOUR_RED];
    
    listView = [[CASCommonListView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_STATE)  //lokalised 11 Feb
                                                   list: [mSession getStateNames: self.stateTable]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
}

#pragma mark - tableview delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.registrationFormArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *regFieldData = [self.registrationFormArray objectAtIndex: indexPath.row];
    switch ([[regFieldData objectForKey: @"Type"] intValue])
    {
        case kREGISTRATION_TEXTFIELD:
        {
            RegistrationFormTextfieldCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormTextfieldCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell enableCell: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];

            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [textfieldCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [textfieldCell hideTopLabel: NO];
            if ([regFieldData objectForKey: @"Value"])
                [textfieldCell setTextfieldText: [regFieldData objectForKey: @"Value"]];
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            [textfieldCell setTextfieldDelegate: self];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_DROPDOWN:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [dropdownCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [dropdownCell hideTopLabel: NO];
            
            //set initial value
            if ([regFieldData objectForKey: @"Value"])
                [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [dropdownCell.button addTarget: self action: @selector(dropdownPressed:) forControlEvents:UIControlEventTouchUpInside];
            dropdownCell.button.tag = indexPath.row;
            
            //keep a reference of cell to submit json value later
            [self.regSubmissionDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        case kREGISTRATION_TEXTFIELDWITHBUTTON:
        {
            RegistrationTextfieldWithButtonCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationTextfieldWithButtonCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            if ([regFieldData objectForKey: @"TextfieldEnabled"])
                [textfieldCell setTextfieldEnabled: [[regFieldData objectForKey: @"TextfieldEnabled"] boolValue]];
            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            [textfieldCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [textfieldCell hideTopLabel: NO];
            
            
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_BUTTON:
        {
            RegistrationButtonCell *buttonCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationButtonCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [buttonCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [buttonCell setTitle: [regFieldData objectForKey: @"Title"]];
            [buttonCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [buttonCell hideTopLabel: NO];
            
            [self.regSubmissionDict setObject: buttonCell forKey: [regFieldData objectForKey: @"Key"]];
            return buttonCell;
        }
            break;
        case kREGISTRATION_MOBNUM:
        {
            RegistrationMobileNumberCell *mobNumCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationMobileNumberCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [mobNumCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            if ([regFieldData objectForKey: @"TextfieldEnabled"])
                [mobNumCell setTextfieldEnabled: [[regFieldData objectForKey: @"TextfieldEnabled"] boolValue]];
            [mobNumCell setMobileCodeEnabled: NO];
            [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [mobNumCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [mobNumCell hideTopLabel: NO];

            [mobNumCell setCountryCode: [regFieldData objectForKey: @"CountryCode"]];
            [mobNumCell setMobileNumber: [regFieldData objectForKey: @"MobileNumber"]];
            
            [self.regSubmissionDict setObject: mobNumCell forKey: [regFieldData objectForKey: @"Key"]];
            return mobNumCell;
        }
            break;
        case kREGISTRATION_LABELWITHTEXTFIELD:
        {
            RegistrationLabelTextfieldTableViewCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationLabelTextfieldTableViewCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [textfieldCell setLeftTitle: [regFieldData objectForKey: @"Title"]];
            [textfieldCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
//            [textfieldCell hideTopLabel: YES];
            
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_BIRTHDATE:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            //Salutation
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            [dropdownCell hideTopLabel: NO];
            dropdownCell.button.tag = indexPath.row;
            
            [dropdownCell.button addTarget: self action: @selector(dropdownPressed:) forControlEvents:UIControlEventTouchUpInside];
            [self.regSubmissionDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        case kREGISTRATION_HEADER:
        {
            RegistrationHeaderTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationHeaderTableViewCell"];
            [headerCell setTitle: [regFieldData objectForKey: @"Title"]];
            return headerCell;
        }
            break;
        case kREGISTRATION_WORKINGHOURS:
        {
            RegistrationWorkingHoursTableViewCell *workingHoursCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationWorkingHoursTableViewCell"];
            
            [workingHoursCell.button addTarget: self action: @selector(workingHoursPressed:) forControlEvents:UIControlEventTouchUpInside];
            workingHoursCell.workingHoursDict = self.workingHoursDict;
            
            [workingHoursCell reloadCellData];
            return workingHoursCell;
        }
            break;
        default:
            NSLog(@"UNKNOWN CELL TYPE");
            return [UITableViewCell new];
            break;
    }
    return [UITableViewCell new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row > kWorkshopProfile_WORKINGHOURS)
    {
        NSArray *fullWorkWeekDays;
        NSArray *halfWorkWeekDays;
        
        if (![[self.workingHoursDict objectForKey: @"WeekDays1"] isEqualToString: @""])
            fullWorkWeekDays = [[self.workingHoursDict objectForKey: @"WeekDays1"] componentsSeparatedByString: @","];
        
        if (![[self.workingHoursDict objectForKey: @"WeekDays2"] isEqualToString: @""])
            halfWorkWeekDays = [[self.workingHoursDict objectForKey: @"WeekDays2"] componentsSeparatedByString: @","];
        
        return 44 + 44 * ([fullWorkWeekDays count] + [halfWorkWeekDays count]);
    }
    else
        return CELL_HEIGHT;
}
-(void) dropdownPressed:(UIButton *)sender
{
    [self.view endEditing: YES];
    listView.tag = sender.tag;
    
    switch (listView.tag)
    {
        case kWorkshopProfile_STATE:
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_STATE) list: [mSession getStateNames: self.stateTable] hasSearchField: YES]; //lokalised 11 Feb
            break;
        case kWorkshopProfile_WORKINGHOURS:
            [self workingHoursPressed: nil];
            return;
            break;
        default:
            break;
    }
    [self popUpView];
}

- (IBAction)workingHoursPressed:(id)sender {
    
    if (!self.workingHoursDict ||
        [self.workingHoursDict isKindOfClass: [NSNull class]])
        self.workingHoursDict = @{@"WorkHourStart1":   @"0:00",
                                  @"WorkHourEnd1":     @"0:00",
                                  @"WeekDays1":        @"",
                                  @"PublicHoliday":    @"0",
                                  @"WorkHourStart2":   @"0:00",
                                  @"WorkHourEnd2":     @"0:00",
                                  @"WeekDays2":        @"",
                                  };
    
    //let tab bar push instead
    [mSession pushWorkingHoursView: self.navigationController.parentViewController workingHoursData: self.workingHoursDict];
}

-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict
{
    //assign working hours
    self.workingHoursDict = workingHoursDict;
    
    [self.tableView reloadRowsAtIndexPaths: @[[NSIndexPath indexPathForRow: kWorkshopProfile_WORKINGHOURS inSection:0]] withRowAnimation: UITableViewRowAnimationNone];
    
    NSArray *fullWorkWeekDays;
    NSArray *halfWorkWeekDays;
    
    if (![[workingHoursDict objectForKey: @"WeekDays1"] isEqualToString: @""])
        fullWorkWeekDays = [[workingHoursDict objectForKey: @"WeekDays1"] componentsSeparatedByString: @","];
    
    if (![[workingHoursDict objectForKey: @"WeekDays2"] isEqualToString: @""])
        halfWorkWeekDays = [[workingHoursDict objectForKey: @"WeekDays2"] componentsSeparatedByString: @","];
    
    self.tableHeightLayout.constant = CELL_HEIGHT * [self.registrationFormArray count] + (44 * ([fullWorkWeekDays count] + [halfWorkWeekDays count]));
//    [self setupRegistrationForm];
}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        RegistrationFormDropdownCell *cell;
        switch (listView.tag)
        {
            case kWorkshopProfile_STATE:
                cell = [self.regSubmissionDict objectForKey: kCompanyState];
                break;
            case kWorkshopProfile_WORKINGHOURS:
                cell = [self.regSubmissionDict objectForKey: kWorkingHours];
                break;
            default:
                return;
                break;
        }
        [cell.button setTitle:selection.firstObject forState:UIControlStateNormal];
    }
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
        {
            SET_LOADSTATE([[response getGenericResponse] objectForKey: @"StateDetailsList"]);
            self.stateTable = [[response getGenericResponse] objectForKey: @"StateDetailsList"];
        }
            break;
        case kWEBSERVICE_FETCHTRADE:
//            self.workshopDetailsData = [response getGenericResponse];
            
            self.member = [[Member alloc] initWithData:[self.workshopDetailsData objectForKey: @"REG_Member"]];
            self.companyInfo = [[CompanyInfo alloc] initWithData: [self.workshopDetailsData objectForKey: @"REG_Company"]];
            self.companyAddress = [[CompanyAddress alloc] initWithData: [self.workshopDetailsData objectForKey: @"REG_CompanyAddress"]];
            
            self.workingHoursDict = [self.workshopDetailsData objectForKey: @"REG_CompanyWorkHours"];
            
            if (!self.workingHoursDict ||
                [self.workingHoursDict isKindOfClass: [NSNull class]])
                self.workingHoursDict = @{@"WorkHourStart1":   @"0:00",
                                          @"WorkHourEnd1":     @"0:00",
                                          @"WeekDays1":        @"",
                                          @"PublicHoliday":    @"0",
                                          @"WorkHourStart2":   @"0:00",
                                          @"WorkHourEnd2":     @"0:00",
                                          @"WeekDays2":        @"",
                                          };
            
            [self setupRegistrationForm];
            [self setupInterface];
            
            break;
        case kWEBSERVICE_UPDATEWORKSHOP:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                
            }];
        }
            break;
        default:
            break;
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress: KEY_WINDOW];
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
            self.stateTable = GET_LOADSTATE;
            
            break;
        default:
            break;
    }
}

- (IBAction)updateButtonPressed:(id)sender
{
    self.companyInfo.companyName            = [[self.regSubmissionDict objectForKey: kCompanyName] getJsonValue];
    
    self.companyInfo.contactNumber          = [[self.regSubmissionDict objectForKey: kContactNumber] getJsonValue];
    
    self.companyInfo.companyEmailAddress    = [[self.regSubmissionDict objectForKey: kCompanyEmailAddress] getJsonValue];
    self.companyAddress.address1        = [[self.regSubmissionDict objectForKey: kAddress1] getJsonValue];
    self.companyAddress.address2        = [[self.regSubmissionDict objectForKey: kAddress2] getJsonValue];
    
    self.companyAddress.companyCity = [[self.regSubmissionDict objectForKey: kCompanyCity] getJsonValue]; //town
    
    NSString *stateString = [[self.regSubmissionDict objectForKey: kCompanyState] getJsonValue];
    self.companyAddress.companyState    = [mSession convertToStateKeyCode: stateString stateDetailedListArray:self.stateTable];
    self.companyAddress.postalCode      = [[self.regSubmissionDict objectForKey: kPostalCode] getJsonValue];
    
    
    NSArray *companyService = @[@{@"ServiceID": @"1"},
                                 @{@"ServiceID": @"2"},
                                 ];
    NSDictionary *updateParams = @{@"REG_Company":            [self.companyInfo getJSON],
                                   @"REG_CompanyAddress":     [self.companyAddress getJSON],
                                   @"REG_CompanyService":     companyService,
                                   @"REG_CompanyWorkHours":   self.workingHoursDict,
                                   @"REG_Member":             [self.member getJSON],
                                   };
    
    [[WebServiceManager sharedInstance] updateApprovedWorkshop: updateParams vc:self];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.delegate textfieldHandler: YES];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.delegate textfieldHandler: NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
