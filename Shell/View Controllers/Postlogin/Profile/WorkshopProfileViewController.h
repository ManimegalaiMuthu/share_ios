//
//  WorkshopProfileViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 23/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"
@protocol WorkshopProfileDelegate <NSObject>
-(void) changePasswordHandler;
-(void) textfieldHandler:(BOOL) isBeginEditing;
@end

@interface WorkshopProfileViewController : BaseVC
@property (weak) id<WorkshopProfileDelegate> delegate;
@property NSDictionary *workshopDetailsData;
@property NSArray *stateTable;

-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict;
-(void) loadProfileData;
@end
