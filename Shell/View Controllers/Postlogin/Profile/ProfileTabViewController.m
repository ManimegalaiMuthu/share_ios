//
//  ProfileTabViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 23/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "RDVTabBarItem.h"
#import "ProfileTabViewController.h"
#import "MyProfileViewController.h"
#import "WorkshopProfileViewController.h"
#import "WorkingHoursViewController.h"
#import "ProfileMasterViewController.h"
#import "ProfileTab_NewViewController.h"

@interface ProfileTabViewController () <WebServiceManagerDelegate, profileTabNewDelegate>
{
    MyProfileViewController *myProfileViewController;
    WorkshopProfileViewController *workshopProfileViewController;
    
    ProfileTab_NewViewController *profileTabNewViewController;
    ProfileTab_NewViewController *bankDetailsViewController;
}
@property NSArray *stateTable;
@end

@implementation ProfileTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.hidden = YES;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!profileTabNewViewController)
        [[WebServiceManager sharedInstance] loadState: [[mSession lookupTable] objectForKey: @"LookUpVersion"]
                                      countryCode: GET_COUNTRY_CODE
                                               vc: self];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    //reload tab bar and contentview UI settings
    //Must be in viewDidAppear
    [self setTabBarHidden: NO animated: NO];
    self.view.hidden = NO;
}

- (void) setupInterface
{
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_MANAGEWORKSHOP) subtitle: @""];  //lokalised 11 Feb
    
    NSArray *title;
    
    if(IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        profileTabNewViewController = [STORYBOARD_PROFILE instantiateViewControllerWithIdentifier: VIEW_PROFILE_TAB_NEW];
        UINavigationController *myProfileNewNav = [[UINavigationController alloc] initWithRootViewController: profileTabNewViewController];
        [myProfileNewNav setNavigationBarHidden:YES];
        profileTabNewViewController.delegate = self;
        profileTabNewViewController.member = self.member;
        profileTabNewViewController.workshopData = self.workshopDetailsData;
        profileTabNewViewController.isBankDetails = false;
        [profileTabNewViewController view];
        
        bankDetailsViewController = [STORYBOARD_PROFILE instantiateViewControllerWithIdentifier: VIEW_PROFILE_TAB_NEW];
        UINavigationController *bankDetailsNav = [[UINavigationController alloc] initWithRootViewController: bankDetailsViewController];
        [bankDetailsNav setNavigationBarHidden:YES];
        bankDetailsViewController.delegate = self;
        bankDetailsViewController.member = self.member;
        bankDetailsViewController.workshopData = self.workshopDetailsData;
        bankDetailsViewController.isBankDetails = true;
        [bankDetailsViewController view];
        
        [self setViewControllers: @[myProfileNewNav,
                                    bankDetailsNav,
                                    ]];
        title = @[LOCALIZATION(C_HEADER_MYPROFILE),     //lokalised 11 Feb
                  LOCALIZATION(C_HEADER_ACCOUNTINFO),
                  ];
    }
    else if(IS_COUNTRY(COUNTRYCODE_INDONESIA))
    {
        profileTabNewViewController = [STORYBOARD_PROFILE instantiateViewControllerWithIdentifier: VIEW_PROFILE_TAB_NEW];
        UINavigationController *myProfileNewNav = [[UINavigationController alloc] initWithRootViewController: profileTabNewViewController];
        [myProfileNewNav setNavigationBarHidden:YES];
        profileTabNewViewController.delegate = self;
        profileTabNewViewController.member = self.member;
        profileTabNewViewController.workshopData = self.workshopDetailsData;
        profileTabNewViewController.isBankDetails = false;
        [profileTabNewViewController view];
        
        bankDetailsViewController = [STORYBOARD_PROFILE instantiateViewControllerWithIdentifier: VIEW_PROFILE_TAB_NEW];
        UINavigationController *bankDetailsNav = [[UINavigationController alloc] initWithRootViewController: bankDetailsViewController];
        [bankDetailsNav setNavigationBarHidden:YES];
        bankDetailsViewController.delegate = self;
        bankDetailsViewController.member = self.member;
        bankDetailsViewController.workshopData = self.workshopDetailsData;
        bankDetailsViewController.isBankDetails = true;
        [bankDetailsViewController view];
        
        [self setViewControllers: @[myProfileNewNav,
                                    bankDetailsNav,
                                    ]];
        title = @[LOCALIZATION(C_HEADER_MYPROFILE),
                  LOCALIZATION(C_TITLE_MOREINFO),
                  ];
    }
    else
    {
        myProfileViewController  = [STORYBOARD_PROFILE instantiateViewControllerWithIdentifier: VIEW_MYPROFILE];
        UINavigationController *myProfileNav = [[UINavigationController alloc] initWithRootViewController: myProfileViewController];
        [myProfileNav setNavigationBarHidden: YES];
        myProfileViewController.delegate = (ProfileMasterViewController *) self.parentViewController;
        myProfileViewController.member = self.member;
        [myProfileViewController view];
        [myProfileViewController setupRegistrationForm];
        
        
        workshopProfileViewController  = [STORYBOARD_PROFILE instantiateViewControllerWithIdentifier: VIEW_WORKSHOPPROFILE];
        UINavigationController *workshopProfileNav = [[UINavigationController alloc] initWithRootViewController: workshopProfileViewController];
        workshopProfileViewController.workshopDetailsData = self.workshopDetailsData;
        workshopProfileViewController.stateTable = self.stateTable;
        [workshopProfileNav setNavigationBarHidden: YES];
        workshopProfileViewController.delegate = (ProfileMasterViewController *) self.parentViewController;
        [workshopProfileViewController view];
        [workshopProfileViewController loadProfileData];
        
        [self setViewControllers: @[myProfileNav,
                                    workshopProfileNav,
                                    ]];
        title = @[LOCALIZATION(C_SIDEMENU_MYPROFILE),       //lokalised 11 Feb
                  LOCALIZATION(C_HEADER_WORKSHOPPROFILE),   //lokalised 11 Feb
                  ];
    }
    
    //same VC but different data.
    //selecttab function have been tweaked locally for this case
    
    
    UIImage *selected = GET_ISADVANCE ? [UIImage imageNamed:@"tab-bg_blue"] : [UIImage imageNamed:@"tab-bg"];
    UIImage *unselected = [UIImage imageNamed:@"tab-bg_grey.png"];
    if (kIsRightToLeft) {
        title = title.reverseObjectEnumerator.allObjects.mutableCopy;
    }
    for (int i = 0; i < [self.tabBar.items count]; i++)
    {
        RDVTabBarItem *item = [self.tabBar.items objectAtIndex: i];
        [item setTitle: [title objectAtIndex: i]];
        
        [item setBackgroundColor: COLOUR_WHITE];
        [item setTitleUnselectedAttributes:FONT_H1 colour:COLOUR_VERYDARKGREY];
        [item setTitleSelectedAttributes:FONT_H1 colour:COLOUR_RED];
        
        [item setBackgroundSelectedImage: selected withUnselectedImage: unselected];
    }
    
    [self setSelectedIndex: self.selectedIndex];
    
    if (kIsRightToLeft) {
        [self tabBar:self.tabBar didSelectItemAtIndex:self.viewControllers.count-1];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict
{
    if(IS_COUNTRY(COUNTRYCODE_INDIA))
        [profileTabNewViewController didFinishWorkingHoursSelection: workingHoursDict];
    else
        [workshopProfileViewController didFinishWorkingHoursSelection: workingHoursDict];
}

#pragma mark RDVTabBarDelegate
- (BOOL)tabBar:(RDVTabBar *)tabBar shouldSelectItemAtIndex:(NSInteger)index
{
    for (RDVTabBarItem *item in [tabBar items])
    {
        
    }
    return ([tabBar selectedItem] != [[tabBar items] objectAtIndex: index]);
}

- (void)tabBar:(RDVTabBar *)tabBar didSelectItemAtIndex:(NSInteger)index
{
    //super method MUST BE PRESENT
    [super tabBar:tabBar didSelectItemAtIndex: index];
    
    if (index == [[tabBar items] count] - 1) //add new to be last object of tab
    {
        
    }
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
            SET_LOADSTATE([[response getGenericResponse] objectForKey: @"StateDetailsList"]);
            self.stateTable = [[response getGenericResponse] objectForKey: @"StateDetailsList"];
            
            [self setupInterface];
            break;
        default:
            break;
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress: KEY_WINDOW];
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
            self.stateTable = GET_LOADSTATE;
            
            [self setupInterface];
            break;
        default:
            break;
    }
}

-(void)dropDownSelection:(NSArray *)selection withKey:(NSString *)key
{
    if (selection.count > 0) {
        [profileTabNewViewController dropDownSelection:selection withKey:key];
        [bankDetailsViewController dropDownSelection:selection withKey:key];
    }
}
-(void) didPressWorkingHours: (NSDictionary *) workingHours
{
    [self.delegate didPressWorkingHours: workingHours];
}
-(void) dropDownStatePressedWithKey:(NSString *) formKey
{
    [self.delegate dropDownStatePressedWithKey:formKey];
}
-(void) dropDownSalutationPressedWithKey:(NSString *) formKey
{
    [self.delegate dropDownSalutationPressedWithKey:formKey];
}
-(void) dropDownBankNamePressedWithKey:(NSString *) formKey;
{
    [self.delegate dropDownBankNamePressedWithKey:formKey];
}
-(void) birthdatePressedWithKey:(NSString *) formKey
{
    [self.delegate birthdatePressedWithKey:formKey];
}
- (void)birthDateSelected:(NSDate *)date
{
    [profileTabNewViewController birthDateSelected:date];
}
-(void) changePasswordPressedWithKey:(NSString *) formKey
{
    [self.delegate changePasswordPressedWithKey:formKey];
}

-(void)textfieldHandler:(BOOL)isBeginEditing
{
    [self.delegate textfieldHandler: isBeginEditing];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
