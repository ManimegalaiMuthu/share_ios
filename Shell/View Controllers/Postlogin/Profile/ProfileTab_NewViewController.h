//
//  ProfileTab_NewViewController.h
//  Shell
//
//  Created by Ben on 3/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"
#import <UIKit/UIKit.h>
#import "Helper.h"

@protocol profileTabNewDelegate <NSObject>
-(void) changePasswordHandler;
-(void)textfieldHandler:(BOOL)isBeginEditing;
-(void) didPressWorkingHours: (NSDictionary *) workingHours;
-(void) dropDownStatePressedWithKey:(NSString *) formKey;
-(void) dropDownSalutationPressedWithKey:(NSString *) formKey;
-(void) dropDownBankNamePressedWithKey:(NSString *) formKey;
-(void) birthdatePressedWithKey:(NSString *) formKey;
-(void) changePasswordPressedWithKey:(NSString *) formKey;
@end

@interface ProfileTab_NewViewController : BaseVC
@property (weak, nonatomic) id<profileTabNewDelegate>delegate;
@property NSDictionary *workshopData;
@property Member* member;
@property BOOL isBankDetails;
@property BOOL shouldUpdateApi;

-(void) dropDownSelection:(NSArray *)selection withKey:(NSString*) key;
-(void) birthDateSelected:(NSDate*)date;
-(void) didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict;
@end
