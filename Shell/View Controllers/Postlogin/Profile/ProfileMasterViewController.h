//
//  ProfileMasterViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 23/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface ProfileMasterViewController : BaseVC

@property (nonatomic) NSUInteger selectedIndex;

@end
