//
//  NotificationsViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 29/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "NotificationsViewController.h"

@interface NotificationsViewController () <UITableViewDataSource, UITableViewDelegate, WebServiceManagerDelegate>
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet TableViewWithEmptyView *notificationTableView;
@property NSMutableArray *notificationArray;
@end

@implementation NotificationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_NOTIFICATION) subtitle: @""];         //lokalise 1 feb
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnEdit];
    [self.btnEdit setTitle: LOCALIZATION(C_NOTIFICATION_EDIT) forState:UIControlStateNormal];         //lokalise 1 feb
    [self.btnEdit setTitleColor:COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    [self.btnEdit.titleLabel setFont: IS_THAI_LANGUAGE ? FONT_B(18) : FONT_B1];
    
     self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    
    PlaceholderView *vwPlaceholder = [[PlaceholderView alloc]initWithView:self.notificationTableView];
    [vwPlaceholder setBackgroundColor:[UIColor clearColor]];
    self.notificationTableView.emptyView = vwPlaceholder;
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
        vwPlaceholder.lblPlaceholderTitle.text = LOCALIZATION(C_RUSSIA_NONOTIFICATIONS);         //lokalise 1 feb
    self.notificationTableView.tableFooterView = [UIView new];
    
    // Initialize the refresh control.
    [self.notificationTableView.customRefreshControl addTarget:self
                                                action:@selector(refreshTable)
                                      forControlEvents:UIControlEventValueChanged];
    
    if (kIsRightToLeft) {
        [self.btnBack setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.btnBack setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
}

-(void) refreshTable
{
    [[WebServiceManager sharedInstance] fetchMessageList: self];
}

- (IBAction)editPressed:(id)sender {
    [mSession pushNotificationEditWithListArray:self.notificationArray vc: self];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];

    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_NOTIFICATION) screenClass:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    [[WebServiceManager sharedInstance] fetchMessageList: self];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    return [self.notificationArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
    
}

enum NOTIFICATIONTYPES
{
    NOTIFICATION_SHAREPROGRAMUPDATES = 1,
    NOTIFICATION_POINTSINCENTIVESTATUS,
    NOTIFICATION_PROMOTIONSACTIVITIES,
    NOTIFICATION_DISTRIBUTOROFFERUPDATES,
    NOTIFICATION_SHELLOFFERUPDATES,
};

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"NotificationMessageCell"];
    
    
    NSDictionary *msgData = [self.notificationArray objectAtIndex: indexPath.row];
    
    UIImageView *msgIcon = [cell viewWithTag: 1]; //read/unread
    switch ([[msgData objectForKey: @"PushType"] intValue])
    {
        case NOTIFICATION_SHAREPROGRAMUPDATES:
            if ([[msgData objectForKey: @"ReadStatus"] boolValue])
                [msgIcon setImage: [UIImage imageNamed: @"sideicon-rewards-notis"]];
            else {
                UIImage *iconImage = GET_ISADVANCE ? [UIImage imageNamed: @"sideicon-rewards-notis_blue"] : [UIImage imageNamed: @"sideicon-rewards-notis-red"];
                [msgIcon setImage: iconImage];
            }
            break;
        case NOTIFICATION_POINTSINCENTIVESTATUS:
            if ([[msgData objectForKey: @"ReadStatus"] boolValue])
                [msgIcon setImage: [UIImage imageNamed: @"icon-msg-notis"]];
            else {
                UIImage *iconImage = GET_ISADVANCE ? [UIImage imageNamed: @"icon-msg-darkgrey_blue"] : [UIImage imageNamed: @"icon-msg-darkgrey-red"];
                [msgIcon setImage: iconImage];
            }
            break;
        case NOTIFICATION_PROMOTIONSACTIVITIES:
            if ([[msgData objectForKey: @"ReadStatus"] boolValue])
                [msgIcon setImage: [UIImage imageNamed: @"sideicon-promo-darkgrey"]];
            else {
                UIImage *iconImage = GET_ISADVANCE ? [UIImage imageNamed: @"sideicon-promo_blue"] : [UIImage imageNamed: @"sideicon-promo-red"];
                [msgIcon setImage: iconImage];
            }
            break;
        case NOTIFICATION_DISTRIBUTOROFFERUPDATES:
            if ([[msgData objectForKey: @"ReadStatus"] boolValue])
                [msgIcon setImage: [UIImage imageNamed: @"icon-notis"]];
            else {
                UIImage *iconImage = GET_ISADVANCE ? [UIImage imageNamed: @"con-notisunread_blue"] : [UIImage imageNamed: @"icon-notisunread"];
                [msgIcon setImage: iconImage];
            }
            break;
        case NOTIFICATION_SHELLOFFERUPDATES:
            if ([[msgData objectForKey: @"ReadStatus"] boolValue])
                [msgIcon setImage: [UIImage imageNamed: @"icon-coin-notis"]];
            else {
                UIImage *iconImage = GET_ISADVANCE ? [UIImage imageNamed: @"icon-coin-notis_blue"] : [UIImage imageNamed: @"icon-coin-notis-red"];
                [msgIcon setImage: iconImage];
            }
            break;
        default:
            if ([[msgData objectForKey: @"ReadStatus"] boolValue])
                [msgIcon setImage: [UIImage imageNamed: @"icon-notis"]];
            else {
                UIImage *iconImage = GET_ISADVANCE ? [UIImage imageNamed: @"con-notisunread_blue"] : [UIImage imageNamed: @"icon-notisunread"];
                [msgIcon setImage: iconImage];
            }
        break;
    }

    UILabel *lblTitle = [cell viewWithTag: 2];
    [lblTitle setFont: FONT_H1];
    lblTitle.text = [msgData objectForKey: @"Header"];
    
    UILabel *lblSubtitle = [cell viewWithTag: 3];
    lblSubtitle.text = [msgData objectForKey: @"ShortDescription"];
    [lblSubtitle setFont: FONT_B1];
    
    UIImageView *imgArrow = [cell viewWithTag: 4];
    if (kIsRightToLeft) {
        imgArrow.image = [UIImage imageNamed:@"icon-arrsingleL.png"];
    } else {
        imgArrow.image = [UIImage imageNamed:@"icon-arrsingleR.png"];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *msgData = [self.notificationArray objectAtIndex: indexPath.row];
    
    //push to detailed message view
//    [mSession pushNotificationsDetailView: [msgData objectForKey: @"PushID"] vc:self];
    [mSession pushNotificationsDetailView: [msgData objectForKey: @"PushID"] vc:self];
    
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_MESSAGELIST:
            [self.notificationTableView.customRefreshControl performSelectorOnMainThread: @selector(endRefreshing) withObject: self.notificationTableView.customRefreshControl waitUntilDone:NO];
            
            //null handler
            if (![[[response getGenericResponse] objectForKey: @"PushMessages"] isKindOfClass: [NSNull class]])
                self.notificationArray = [[response getGenericResponse] objectForKey: @"PushMessages"];
            else
                self.notificationArray = [NSMutableArray arrayWithArray:@[]];
            
            [self.notificationTableView reloadData];

            NSInteger unreadCount = 0;
            for (NSDictionary *notificationData in self.notificationArray)
            {
                if (![[notificationData objectForKey: @"ReadStatus"] boolValue])
                {
                    //increment unread count
                    unreadCount++;
                }
            }
            [[mSession profileInfo] setUnReadPushMessage: unreadCount];
            
            [[NSNotificationCenter defaultCenter] postNotificationName: @"Unread Icon Changed" object: nil];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"UpdateProfileInfo" object: nil];
            break;
        default:
            break;
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress: KEY_WINDOW];
    
    [self.notificationTableView.customRefreshControl performSelectorOnMainThread: @selector(endRefreshing) withObject: self.notificationTableView.customRefreshControl waitUntilDone:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backPressed:(UIButton *)sender {
    if([[self.navigationController childViewControllers] count] == 1) {
        [mSession loadHomeView];
    } else {
        [self popSelf];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
