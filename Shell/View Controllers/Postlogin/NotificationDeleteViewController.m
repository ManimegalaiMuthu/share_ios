//
//  NotificationDeleteViewController.m
//  Shell
//
//  Created by Jeremy Lua on 31/5/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "NotificationDeleteViewController.h"

@interface NotificationDeleteViewController () <WebServiceManagerDelegate,UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectAll;

@property (strong, nonatomic) IBOutlet UIButton *btnDelete;

@property (weak, nonatomic) IBOutlet TableViewWithEmptyView *notificationTableView;
@property NSMutableArray *selectedArray;
@end

@implementation NotificationDeleteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_NOTIFICATION) subtitle: @""];         //lokalise 1 feb
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
//    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_TITLE_NOTIFICATION),  LOCALIZATION(C_REWARDS_BACK)];
    
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnSelectAll];
    [self.btnSelectAll setTitle: LOCALIZATION(C_NOTIFICATION_SELECTALL) forState:UIControlStateNormal];         //lokalise 1 feb
    [self.btnSelectAll.titleLabel setFont: FONT_B1];
    [self.btnSelectAll setTitleColor:COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    
    [self.btnDelete setBackgroundColor:COLOUR_RED];
    [self.btnDelete.titleLabel setFont: FONT_BUTTON];
    [self.btnDelete setTitle: LOCALIZATION(C_DELETE) forState:UIControlStateNormal];         //lokalise 1 feb
    
    //cart dont need refresh
    if(@available(iOS 10, *))
        self.notificationTableView.refreshControl = nil;
    [self.notificationTableView.customRefreshControl removeFromSuperview];
    self.notificationTableView.customRefreshControl = nil;
    
    PlaceholderView *vwPlaceholder = [[PlaceholderView alloc]initWithView:self.notificationTableView];
    [vwPlaceholder setBackgroundColor:[UIColor clearColor]];
    self.notificationTableView.emptyView = vwPlaceholder;
    self.notificationTableView.tableFooterView = [UIView new];

    self.selectedArray = [[NSMutableArray alloc] init];
    for (NSMutableDictionary *cellData in self.notificationArray)
    {
        [self.selectedArray addObject: @(NO)];
    }
    
    // Initialize the refresh control.
//    [self.notificationTableView.customRefreshControl addTarget:self
//                                                        action:@selector(refreshTable)
//                                              forControlEvents:UIControlEventValueChanged];
    if (kIsRightToLeft) {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];

    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / Delete",LOCALIZATION_EN(C_TITLE_NOTIFICATION)] screenClass:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    return [self.notificationArray count];
}

- (IBAction)backPressed:(id)sender {
    //only available prelogin
    [self popSelf];
}

- (IBAction)selectAllPressed:(id)sender {
    //only available prelogin
    
    for (UITableViewCell *cell in self.notificationTableView.subviews)
    {
        NSIndexPath *cellIndexPath = [self.notificationTableView indexPathForCell: cell];
        
        UIButton *btnSelected = [cell viewWithTag: 4];
        btnSelected.selected = YES;
        
    }
    
    for (int i =0; i < [self.selectedArray count]; i++)
    {
//        NSDictionary *cellData = [self.notificationArray objectAtIndex: i];
        
        if (i < [self.selectedArray count])
            [self.selectedArray replaceObjectAtIndex: i withObject: @(YES)];
        else
        {
            NSLog(@"unknown error!");
            [self.selectedArray addObject: @(YES)];
        }
    }
}

- (IBAction)deletePressed:(id)sender {
    [self proceedToDelete];
//    [self popUpYesNoWithTitle: @"Are you sure you want to delete"
//                      content: @""
//                  yesBtnTitle: LOCALIZATION(C_FORM_YES)
//                   noBtnTitle: LOCALIZATION(C_FORM_NO)
//                 onYesPressed:^(BOOL finished) {
//                     [self proceedToDelete];
//                 } onNoPressed: nil];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
    
}

enum NOTIFICATIONTYPES
{
    NOTIFICATION_SHAREPROGRAMUPDATES = 1,
    NOTIFICATION_POINTSINCENTIVESTATUS,
    NOTIFICATION_PROMOTIONSACTIVITIES,
    NOTIFICATION_DISTRIBUTOROFFERUPDATES,
    NOTIFICATION_SHELLOFFERUPDATES,
};

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"NotificationMessageCell"];
    
    
    NSDictionary *msgData = [self.notificationArray objectAtIndex: indexPath.row];
    
    UIImageView *msgIcon = [cell viewWithTag: 1]; //read/unread
    switch ([[msgData objectForKey: @"PushType"] intValue])
    {
        case NOTIFICATION_SHAREPROGRAMUPDATES:
            if ([[msgData objectForKey: @"ReadStatus"] boolValue])
                [msgIcon setImage: [UIImage imageNamed: @"sideicon-rewards-notis"]];
            else {
                UIImage *iconImage = GET_ISADVANCE ? [UIImage imageNamed: @"sideicon-rewards-notis_blue"] : [UIImage imageNamed: @"sideicon-rewards-notis-red"];
                [msgIcon setImage: iconImage];
            }
            break;
        case NOTIFICATION_POINTSINCENTIVESTATUS:
            if ([[msgData objectForKey: @"ReadStatus"] boolValue])
                [msgIcon setImage: [UIImage imageNamed: @"icon-msg-notis"]];
            else {
                UIImage *iconImage = GET_ISADVANCE ? [UIImage imageNamed: @"icon-msg-darkgrey_blue"] : [UIImage imageNamed: @"icon-msg-darkgrey-red"];
                [msgIcon setImage: iconImage];
            }
            break;
        case NOTIFICATION_PROMOTIONSACTIVITIES:
            if ([[msgData objectForKey: @"ReadStatus"] boolValue])
                [msgIcon setImage: [UIImage imageNamed: @"sideicon-promo-darkgrey"]];
            else {
                UIImage *iconImage = GET_ISADVANCE ? [UIImage imageNamed: @"sideicon-promo_blue"] : [UIImage imageNamed: @"sideicon-promo-red"];
                [msgIcon setImage: iconImage];
            }
            break;
        case NOTIFICATION_DISTRIBUTOROFFERUPDATES:
            if ([[msgData objectForKey: @"ReadStatus"] boolValue])
                [msgIcon setImage: [UIImage imageNamed: @"icon-notis"]];
            else {
                UIImage *iconImage = GET_ISADVANCE ? [UIImage imageNamed: @"con-notisunread_blue"] : [UIImage imageNamed: @"icon-notisunread"];
                [msgIcon setImage: iconImage];
            }
            break;
        case NOTIFICATION_SHELLOFFERUPDATES:
            if ([[msgData objectForKey: @"ReadStatus"] boolValue])
                [msgIcon setImage: [UIImage imageNamed: @"icon-coin-notis"]];
            else {
                UIImage *iconImage = GET_ISADVANCE ? [UIImage imageNamed: @"icon-coin-notis_blue"] : [UIImage imageNamed: @"icon-coin-notis-red"];
                [msgIcon setImage: iconImage];
            }
            break;
        default:
            if ([[msgData objectForKey: @"ReadStatus"] boolValue])
                [msgIcon setImage: [UIImage imageNamed: @"icon-notis"]];
            else {
                UIImage *iconImage = GET_ISADVANCE ? [UIImage imageNamed: @"con-notisunread_blue"] : [UIImage imageNamed: @"icon-notisunread"];
                [msgIcon setImage: iconImage];
            }
            break;
    }
    
    UILabel *lblTitle = [cell viewWithTag: 2];
    [lblTitle setFont: FONT_H1];
    lblTitle.text = [msgData objectForKey: @"Header"];
    
    UILabel *lblSubtitle = [cell viewWithTag: 3];
    lblSubtitle.text = [msgData objectForKey: @"ShortDescription"];
    [lblSubtitle setFont: FONT_B1];
    
    UIButton *btnSelected = [cell viewWithTag: 4];
    btnSelected.selected = [[self.selectedArray objectAtIndex: indexPath.row] boolValue];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath: indexPath];
    
    UIButton *btnSelected = [cell viewWithTag: 4];
    btnSelected.selected = !btnSelected.selected;
    [self.selectedArray replaceObjectAtIndex: indexPath.row withObject: @(btnSelected.selected)];
    
//    NSDictionary *msgData = [self.notificationArray objectAtIndex: indexPath.row];
//
//    //push to detailed message view
//    [mSession pushNotificationsDetailView: [msgData objectForKey: @"PushID"] vc:self];
    
}


-(void) proceedToDelete
{
    NSMutableArray *notificationsToDeleteArray = [[NSMutableArray alloc] init];
    
    NSMutableArray *updatedArray = [[NSMutableArray alloc] init];
    
    for (int i =0; i < [self.selectedArray count]; i++)
    {
        NSDictionary *cellData = [self.notificationArray objectAtIndex: i];
        
        if ([[self.selectedArray objectAtIndex: i] boolValue])
        {
            //to delete current entry
            [notificationsToDeleteArray addObject: @{@"PushID": [cellData objectForKey: @"PushID"]}];
        }
        else
        {
            //entry not to delete
            [updatedArray addObject: cellData];
        }
    }
    
    [[WebServiceManager sharedInstance] removePushMessageWithList: notificationsToDeleteArray vc: self];
    
    //replace surviving entries
    self.notificationArray = updatedArray;
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_DELETENOTIFICATION:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                
                [self popSelf];
//                [self.selectedArray removeAllObjects];
//
//                for (NSMutableDictionary *cellData in self.notificationArray)
//                {
//                    [self.selectedArray addObject: @(NO)];
//                }
//
//                [self.notificationTableView reloadData];
            }];
            
        }
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
