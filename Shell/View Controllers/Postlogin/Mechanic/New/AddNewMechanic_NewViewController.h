//
//  AddNewMechanic_NewViewController.h
//  Shell
//
//  Created by Jeremy Lua on 13/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@protocol AddNewMechanic_NewDelegate
-(void)didRegisterSuccess;
@end
@interface AddNewMechanic_NewViewController : BaseVC
@property NSString *workshopTradeID;
@property id<AddNewMechanic_NewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
