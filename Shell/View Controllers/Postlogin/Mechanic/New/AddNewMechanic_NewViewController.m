//
//  AddNewMechanic_NewViewController.m
//  Shell
//
//  Created by Jeremy Lua on 13/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "AddNewMechanic_NewViewController.h"

#import "FormsTableViewDelegate.h"
#import "CASDatePickerView.h"

#import "GalleryUploadViewController.h"
#import "RUCustomerSignatureViewController.h"
#import "PhotoTakingViewController.h"

#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kDOB                @"DateOfBirth"
#define kEmailAddress        @"EmailAddress"

//Bank Details
#define kValidateCheckboxes @"ValidateCheckboxes"
#define kPANCardId          @"PANCardId"
#define kPANCardImage       @"PANCardImage"
#define kBeneficiaryName    @"BeneficiaryName"
#define kBankID             @"BankID"
#define kAccountNumber      @"AccountNumber"
#define kConfirmAccountNumber      @"ConfirmAccountNumber"

#define kIFSCCode           @"IFSCCode"
#define kBeneficiaryImage   @"BeneficiaryImage" //for UI only


//last section
#define kDigitalSignature          @"DigitalSignature"    //Russia, signature
#define kTncCheckBox   @"TncCheckBox" //for checkbox only
#define kBankCheckBox   @"BankCheckBox" //for checkbox only
#define kContactPreference  @"ContactPreferences"       //contact preferences checkboxes
#define kRedemptionCheckBox   @"RedemptionCheckBox" //for checkbox only

#define kRegisterCTA        @"RegisterCTA"

@interface AddNewMechanic_NewViewController () <FormsTableViewDelegate, WebServiceManagerDelegate, CommonListDelegate, DatePickerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *regFormTableView;


@property FormsTableViewDelegate *tableViewDelegate;

@property Member *member;
@property CASDatePickerView *dateView;
@end

@implementation AddNewMechanic_NewViewController
@synthesize listView;
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    if (!self.workshopTradeID)    //use own trade ID
    {
        LoginInfo *loginInfo = [mSession loadUserProfile];
        self.workshopTradeID = loginInfo.tradeID;
    }
    
    [self setupInterface];
}

-(void) setupInterface
{
    self.member = [[Member alloc] init];
    
    self.tableViewDelegate = [[FormsTableViewDelegate alloc] init];
    
//    if(GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
    {
        //DSR
        NSMutableArray *mechanicProfileArray = [[NSMutableArray alloc] init];
        [self setupMechanicProfileArray: mechanicProfileArray];
        [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_HEADER_MECHANICPROFILE)];    //lokalised 8 Feb
        [self.tableViewDelegate.initializingArray addObject: mechanicProfileArray];
    }
    
    if(IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        NSMutableArray *bankDetailsArray = [[NSMutableArray alloc] init];
        [self setupBankDetailsArray: bankDetailsArray];
        [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_HEADER_BANKDETAILS)]; //lokalised 8 Feb
        [self.tableViewDelegate.initializingArray addObject: bankDetailsArray];
    }
    
    NSMutableArray *lastSectionArray = [[NSMutableArray alloc] init];
    [self setupLastSectionArray: lastSectionArray];
    [self.tableViewDelegate.headersArray addObject: @""]; //more than 1 char, to show required fields label
    [self.tableViewDelegate.initializingArray addObject: lastSectionArray];
    
    self.regFormTableView.delegate = self.tableViewDelegate;
    self.regFormTableView.dataSource = self.tableViewDelegate;
    
    
    [self.tableViewDelegate registerNib: self.regFormTableView];
    [self.tableViewDelegate setDelegate: self];
    
    if(@available(iOS 11, *))
    {
        self.regFormTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.regFormTableView setContentInset:UIEdgeInsetsMake(15, 0, 15, 0)];
    
    
    self.regFormTableView.backgroundColor = [UIColor clearColor];
    self.regFormTableView.backgroundView = nil;
}

-(void) setupMechanicProfileArray: (NSMutableArray*) mechanicProfileArray
{
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SALUTATION), //lokalised 8 Feb
                             kForm_Type: @(kREGISTRATION_DROPDOWN),
//                                       kForm_Placeholder: LOCALIZATION(C_PROFILE_SALUTATION_PLACEHOLDER),
                             kForm_Key: kSalutation,
                             kForm_Value: LOCALIZATION(C_PROFILE_SALUTATION_PLACEHOLDER),   //lokalised 8 Feb
                                       kForm_Optional:@(YES),
                             }];
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_FIRSTNAME),  //lokalised 8 Feb
                             kForm_Type: @(kREGISTRATION_TEXTFIELD),
                             kForm_Key: kFirstName,
                             //                                    kForm_Value: ,
                             }]; //first name
    
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_LASTNAME),   //lokalised 8 Feb
                             kForm_Type: @(kREGISTRATION_TEXTFIELD),
                             kForm_Key: kLastName,
                             //                                    kForm_Value: self.member.lastName,
                             }]; //last name
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_MOBILENUM),  //lokalised 8 Feb
                             kForm_Type: @(kREGISTRATION_MOBNUM),
                             kForm_Key: kMobileNumber,
                             kForm_TextfieldEnabled: @(YES),
                             kForm_CountryCode: [mSession convertCountryCodeToCountryName: GET_COUNTRY_CODE],
                             //                                    kForm_MobileNumber: ,
                             }];   //Mobile number
    
    [mechanicProfileArray addObject:@{kForm_Title: LOCALIZATION(C_PROFILE_DOB), //lokalised 8 Feb
                            kForm_Type: @(kREGISTRATION_BIRTHDATE),
                            kForm_Key: kDOB,
                            kForm_Value: LOCALIZATION(C_PROFILE_DOB_PLACEHOLDER),   //lokalised 8 Feb
                            }];
    
    [mechanicProfileArray addObject:@{kForm_Title: LOCALIZATION(C_PROFILE_EMAILADDRESS),    //lokalised 8 Feb
                                      kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                      kForm_Key: kEmailAddress,
                                      kForm_Placeholder: LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER),  //lokalised 8 Feb
                                      kForm_Optional:@(!IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                                      }];
}

-(void) setupBankDetailsArray: (NSMutableArray *) bankDetailsArray
{
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(@"Validate Account by"),  //lokalised 4 feb
                                   kForm_Type: @(kFORM_MULTICHECKBOX),
                                   kForm_Key: kValidateCheckboxes,
                                   kForm_Enabled: @(YES),
                                   }];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_PANCARDID), //lokalised 8 Feb
                                   kForm_Type: @(kFORM_PANCARD_ID),
                                   kForm_Key: kPANCardId,
//                                   kForm_Optional: @(YES),  //controlled in formdelegate level.
                                   }];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_PANCARDIMAGE),  //lokalised 8 Feb
                                   kForm_Type: @(
                                       kFORM_PANCARD_IMAGE),
                                   kForm_Key: kPANCardImage,
                                   kForm_Placeholder: LOCALIZATION(C_FORM_PANCARDIMAGE_PLACEHOLDER),    //lokalised 8 Feb
//                                   kForm_Optional: @(YES),  //controlled in formdelegate level.
                                   }];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BENEFICIARYNAME),   //lokalised 8 Feb
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_Key: kBeneficiaryName,
                                   kForm_Placeholder: LOCALIZATION(C_FORM_BENEFICIARYNAME_PLACEHOLDER), //lokalised 8 Feb
                                   }];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BENEFICIARYACCTNUM),    //lokalised 8 Feb
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                   kForm_Key: kAccountNumber,
                                   kForm_SecureEntry: @(YES),
                                   kForm_HasDelegate : @(IS_COUNTRY(COUNTRYCODE_INDIA)), //textfield delegate for account number
                                   }];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_CONFIRMBENEFICIARYNUM),  //lokalised 4 feb
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                   kForm_Key: kConfirmAccountNumber,
                                   kForm_Value: @"",
                                   }];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BENEFICIARYIFSC),   //lokalised 8 Feb
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_Key: kIFSCCode,
                                   kForm_HasDelegate : @(IS_COUNTRY(COUNTRYCODE_INDIA)), //textfield delegate for ifsc code
                                   }];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BANKNAME),  //lokalised 8 Feb
                                   kForm_Type: @(kREGISTRATION_DROPDOWN),
                                   //                                   kForm_Placeholder: LOCALIZATION(C_DROPDOWN_BANKNAME),
                                   kForm_Key: kBankID,
                                   kForm_Value: LOCALIZATION(C_DROPDOWN_BANKNAME),  //lokalised 8 Feb
                                   kForm_Enabled: @(NO),
                                   }];
    
    [bankDetailsArray addObject: @{kForm_Type: @(kFORM_PROFILE_IMAGE),
                                   kForm_Key: kBeneficiaryImage, //for UI only
                                   kForm_Value: @"cheque_sample",                               }]; //
}

-(void)textfieldCellDidBeginEditingWithKey:(NSString *)key
{
    if ([key isEqualToString: kAccountNumber])
        [self.tableViewDelegate editCell: kAccountNumber toSecureField: NO];
}

-(void)textfieldCellDidEndEditingWithKey:(NSString *)key
{
    if ([key isEqualToString: kIFSCCode])
    {
        [self.tableViewDelegate editCell: kBankID withDropdownSelection: @""];    //Bank Name list
        NSString *ifscCodeString = [[self.tableViewDelegate getJsonValues] objectForKey: kIFSCCode];
        [mWebServiceManager fetchBankDetails: ifscCodeString vc:self];
    }
    else if ([key isEqualToString: kAccountNumber])
        [self.tableViewDelegate editCell: kAccountNumber toSecureField: YES];
}

-(void) setupLastSectionArray: (NSMutableArray *)lastSectionArray
{
    if (IS_COUNTRY(COUNTRYCODE_RUSSIA))
    {
        [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_RUSSIA_CUSTOMERSIGNATURE),   //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_SIGNATURE),
                                       kForm_Key: kDigitalSignature,
                                       kForm_Enabled: @(YES),
                                       }];
    }
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA) || IS_COUNTRY(COUNTRYCODE_RUSSIA))
    {
        [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_TNC_IN),    //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_TNCCHECKBOX),
                                       kForm_Key: kTncCheckBox,
                                       kForm_Hyperlinks: @{LOCALIZATION(C_RU_STEPFOUR_TNCLINK): @"tnc", //lokalised 8 Feb
                                                           LOCALIZATION(C_RU_STEPFOUR_PRIVACYLINK) : @"privacypolicy"   //lokalised 8 Feb
                                                           },
                                       kForm_Enabled: @(YES),
                                       }];
        
    }
    
    if (IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BANKINFO),  //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_TNCCHECKBOX),
                                       kForm_Key: kBankCheckBox,
                                       kForm_Enabled: @(YES),
                                       }];
    }
    
//    if (IS_COUNTRY(COUNTRYCODE_VIETNAM))
//    {
//        [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_CONTACT_PREFERENCE),
//                                       kForm_Type: @(kREGISTRATION_CONTACTPREF),
//                                       kForm_Key: kContactPreference,
//                                       kForm_Enabled: @(YES),
//                                       }];
//    }
    
    if ([[mSession profileInfo] enableCustomerRedemption])
    {
        [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_MECH_REDEEMFLAG),    //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_TNCCHECKBOX),
                                       kForm_Key: kRedemptionCheckBox,
                                       kForm_Enabled: @(YES),
                                       }];
    }
    
    [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_REGISTERNEWSTAFF),  //lokalised 8 Feb
                                   kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                   kForm_Key: kRegisterCTA,
                                   kForm_Enabled: @(YES),
                                   kForm_TextColour: COLOUR_WHITE,
                                   kForm_BackgroundColour: COLOUR_RED,
                                   }];
}

-(void) ctaPressedWithKey:(NSString *) ctaKey
{
    if ([ctaKey isEqualToString: kRegisterCTA])
    {
        [self registerPressed];
    }
}

-(void)hyperlinkPressed:(NSString *) link
{
    //kForm_Hyperlinks: @{LOCALIZATION(C_FORM_TNC_IN): @"tnc",
    //                    LOCALIZATION(C_PROFILE_PRIVACY) : @"privacypolicy"
    if ([link isEqualToString: @"tnc"])
    {
        [mSession pushTNCView: self.navigationController.parentViewController];
    }
    else if ([link isEqualToString: @"privacypolicy"])
    {
        [mSession pushPrivacyView: self.navigationController.parentViewController];
    }
}

-(void) dropdownPressedWithKey:(NSString *) formKey
{
    [self.view endEditing: YES];
    listView.formKey = formKey;
    
    //Salutation
    if ([formKey isEqualToString: kSalutation])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO]; //lokalised 8 Feb
    }
    else if ([formKey isEqualToString: kBankID])
    {
        [listView setTitleAndList: LOCALIZATION(@"Select Bank Name") list: [mSession getBanksDetailsList] hasSearchField: NO];
    }
    else
    {
        listView.formKey = nil;
        [mAlert showErrorAlertWithMessage: @"Unknown Error"];
        return;
    }
    [self popUpView];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        [self.tableViewDelegate editCell: listView.formKey withDropdownSelection: selection.firstObject];
        listView.formKey = nil;
    }
}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}


-(void)textfieldWithButtonPressedWithKey:(NSString *)buttonKey
{
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_COUNTRY)    //lokalised 8 Feb
                                                   list: [mSession getCountryNames]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
    
    //preload dateview
    dateView = [[CASDatePickerView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_BIRTHDAY) previouslySelected:nil withYearHidden:YES];   //lokalised 8 Feb
    
    dateView.delegate = self;
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_MANAGEMECHANIC),LOCALIZATION_EN(C_MANAGEWORKSHOP_ADDNEW)] screenClass:nil];
}

-(void) registerPressed
{
    NSMutableDictionary *jsonValuesDict = [[NSMutableDictionary alloc] initWithDictionary: [self.tableViewDelegate getJsonValues] ];
    //patched for confirm account
    if (IS_COUNTRY(COUNTRYCODE_INDIA) &&
        ![[jsonValuesDict objectForKey: kAccountNumber] isEqualToString: [jsonValuesDict objectForKey: kConfirmAccountNumber]])
    {
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_FORM_BENEFICIARYNUM_NOTMATCH)];
        return;
    }
    
    NSMutableDictionary *submitParams = [[NSMutableDictionary alloc] init];
    
    [submitParams setObject:self.workshopTradeID forKey:@"WorkshopTradeID"];
    [submitParams setObject: @([[mSession convertToSalutationKeyCode: [jsonValuesDict objectForKey: kSalutation]] intValue]) forKey: kSalutation];
    [submitParams setObject: [jsonValuesDict objectForKey:kFirstName] forKey: kFirstName];
    [submitParams setObject: [jsonValuesDict objectForKey:kLastName] forKey: kLastName];
    [submitParams setObject: [jsonValuesDict objectForKey:kMobileNumber] forKey: kMobileNumber];
    [submitParams setObject: [jsonValuesDict objectForKey:kEmailAddress] forKey: kEmailAddress];
    if ([jsonValuesDict objectForKey:kContactPreference])
        [submitParams setObject: [jsonValuesDict objectForKey:kContactPreference] forKey: kContactPreference];
    
    [submitParams setObject:self.member.dobMonth forKey: @"DOBMonth"];
    [submitParams setObject:self.member.dobDay forKey:@"DOBDay"];
    
    if(IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [submitParams setObject: GET_COUNTRY_CODE forKey: @"CountryCode"];
        [submitParams setObject: @([[jsonValuesDict objectForKey: kBankCheckBox] boolValue]) forKey: @"BankInfoSharing"];
        
        NSMutableDictionary *regBankDict = [[NSMutableDictionary alloc] init];
        [regBankDict setObject:[jsonValuesDict objectForKey: kBeneficiaryName]  forKey: kBeneficiaryName];
        [regBankDict setObject:[jsonValuesDict objectForKey: kAccountNumber] forKey: kAccountNumber];
        [regBankDict setObject:[jsonValuesDict objectForKey: kIFSCCode] forKey: kIFSCCode];
        [regBankDict setObject: [mSession convertToBankNameKeyCode: [jsonValuesDict objectForKey: kBankID]] forKey: kBankID];
        
        [submitParams setObject: regBankDict forKey: @"REG_Bank"];
    }
    
    if(IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        NSMutableDictionary *regPanDict = [[NSMutableDictionary alloc] init];
        [regPanDict setObject: [jsonValuesDict objectForKey: kPANCardId] forKey: kPANCardId];
        if ([jsonValuesDict objectForKey: kPANCardImage])
            [regPanDict setObject: [jsonValuesDict objectForKey: kPANCardImage] forKey: kPANCardImage];
        [submitParams setObject: regPanDict forKey: @"REG_PAN"];
    }
    
    
    if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND]) {
        //    LOOKUP VALUE
        //    StaffAccountType =({KeyCode = 1;KeyValue = Mechanic;},
        //                       {KeyCode = 2;KeyValue = "Forecourt Attendant";});
        [submitParams setObject:@(1) forKey:@"StaffType"];
        
    }
    
//    if (self.imgSignature.image)
//    {
//        //set image
//        [registerParams setObject: [UIImageJPEGRepresentation(self.imgSignature.image, 0.5f) base64EncodedStringWithOptions:0]  forKey: @"DigitalSignature"];
//    }
    
    if (IS_COUNTRY(COUNTRYCODE_RUSSIA) || IS_COUNTRY(COUNTRYCODE_INDIA))
    {
        [submitParams setObject: [[jsonValuesDict objectForKey: kTncCheckBox] boolValue]?@(1):@(0) forKey: @"TermCondition"];
    }
    
    [submitParams setObject: @([[jsonValuesDict objectForKey: kRedemptionCheckBox] boolValue]) forKey:@"EnableCustomerRedemption"];
    
    [[WebServiceManager sharedInstance] addMechanic:submitParams vc:self];
}

-(void) showPhoto:(UIImage *) image
{
    //popup or push to controller with image
    NSLog(@"show photo");
    
    [mSession pushImagePreview: self.navigationController.parentViewController image: image title: @"Uploaded Image"];
}

-(void) takePhoto:(NSString *)cellKey
{
    [mSession pushPanCardPhotoTakingWithParent: self.navigationController.parentViewController delegateVc: self  message:@"Please take PAN ID in portrait format" key: cellKey];

}

-(void)imageTaken:(UIImage *)image
{
    [self.tableViewDelegate editCell: kPANCardImage withImage: image];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_ADDMECHANIC:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                [self setupInterface];
                [self.delegate didRegisterSuccess];
            }];
        }
            break;
        case kWEBSERVICE_FETCH_BANKDETAILS:
        {
            [self.tableViewDelegate editCell: kBankID withDropdownSelection: [[response getGenericResponse] objectForKey: @"BankName"]];    //Bank Name list
        }
            break;
        default:
            break;
    }
}

#pragma mark birthdate dropdown delegate methods
-(void)birthdatePressedWithKey:(NSString *)formKey
{
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}

- (void)selectedDate:(NSDate *)selectedDate
{
    //do smething
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //   for API
    [formatter setDateFormat:@"dd"];
    self.member.dobDay = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"MM"];
    self.member.dobMonth = [formatter stringFromDate:selectedDate];
    
    if(kIsRightToLeft) {
        [formatter setDateFormat:@"MMMM"];
    } else {
        [formatter setDateFormat:@"dd MMMM"];
    }
    
    if ([GET_LOCALIZATION isEqualToString: kThai])
        [formatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
    else
        [formatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    NSString *selectedDateString = [formatter stringFromDate:selectedDate];
    
    //add formkey to birthday class if needed
    if(kIsRightToLeft) {
        [self.tableViewDelegate editCell: kDOB withDropdownSelection: [NSString stringWithFormat:@"%@ %@",self.member.dobDay,selectedDateString]];
    } else {
        [self.tableViewDelegate editCell: kDOB withDropdownSelection: selectedDateString];
    }
}


#pragma mark Date methods
-(NSString *) convertDateForDisplay:(NSString *)monthString day:(NSString *) dayString
{
    if ([monthString isEqualToString: BLANK] || [dayString isEqualToString: BLANK])
        return LOCALIZATION(C_PROFILE_DOB_PLACEHOLDER); //lokalised 8 Feb
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    if ([GET_LOCALIZATION isEqualToString: kThai])
        [dateFormat setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
    else
        [dateFormat setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    [dateFormat setDateFormat:@"MMdd"];
    NSDate *date = [dateFormat dateFromString: [NSString stringWithFormat: @"%@%@", monthString, dayString]];
    
    // Convert date object to desired output format
    [dateFormat setDateFormat:@"dd MMMM"];
    return [dateFormat stringFromDate:date];
}

#pragma mark signature delegate
-(void) signaturePressed:(NSString *)key
{
    [mSession pushRUCustomerSignatureWithDelegate: self.navigationController.parentViewController delegate: self];
}
-(void)didFinishSignature:(UIImage *)image
{
    [self.tableViewDelegate editCell: kDigitalSignature withSignatureImage: image];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
