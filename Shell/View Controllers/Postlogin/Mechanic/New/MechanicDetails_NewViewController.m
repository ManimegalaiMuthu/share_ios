//
//  MechanicDetails_NewViewController.m
//  Shell
//
//  Created by Jeremy Lua on 13/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "MechanicDetails_NewViewController.h"

#import "FormsTableViewDelegate.h"
#import "CASDatePickerView.h"

#import "GalleryUploadViewController.h"
#import "RUCustomerSignatureViewController.h"
#import "PhotoTakingViewController.h"

#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kDOB                @"DateOfBirth"
#define kEmailAddress        @"EmailAddress"

#define kRedemptionCheckBox   @"RedemptionCheckBox" //for checkbox only

#define kContactPreference  @"ContactPreferences"       //contact preferences checkboxes

#define kUpdateCTA        @"UpdateCTA"
#define kDeactivateCTA        @"DeactivateCTA"
#define kApproveCTA        @"ApproveCTA"
#define kRejectCTA         @"RejectCTA"

@interface MechanicDetails_NewViewController () <FormsTableViewDelegate, WebServiceManagerDelegate, CommonListDelegate, DatePickerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *regFormTableView;


@property FormsTableViewDelegate *tableViewDelegate;

@property Member *member;
@property CASDatePickerView *dateView;
@property BOOL redemptionFlag;
@property BOOL hasUpdateMechanic;
@end

@implementation MechanicDetails_NewViewController
@synthesize listView;
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_MECHANICDETAILS) subtitle: @""];  //lokalised 8 Feb
    
    [self setNavBackBtn];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_MECHANICDETAILS) screenClass:nil];
}

-(void) setupInterface
{
//    self.member = [[Member alloc] init];
    
    self.tableViewDelegate = [[FormsTableViewDelegate alloc] init];
    
    NSMutableArray *mechanicProfileArray = [[NSMutableArray alloc] init];
    [self setupMechanicProfileArray: mechanicProfileArray];
    [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_HEADER_MECHANICPROFILE)];    //lokalised 8 Feb
    [self.tableViewDelegate.initializingArray addObject: mechanicProfileArray];
    
    NSMutableArray *lastSectionArray = [[NSMutableArray alloc] init];
    [self setupLastSectionArray: lastSectionArray];
    [self.tableViewDelegate.headersArray addObject: @""]; //more than 1 char, to show required fields label
    [self.tableViewDelegate.initializingArray addObject: lastSectionArray];
    
    self.regFormTableView.delegate = self.tableViewDelegate;
    self.regFormTableView.dataSource = self.tableViewDelegate;
    
    
    [self.tableViewDelegate registerNib: self.regFormTableView];
    [self.tableViewDelegate setDelegate: self];
    
    if(@available(iOS 11, *))
    {
        self.regFormTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.regFormTableView setContentInset:UIEdgeInsetsMake(15, 0, 15, 0)];
    
    
    self.regFormTableView.backgroundColor = [UIColor clearColor];
    self.regFormTableView.backgroundView = nil;
}

-(void) setupMechanicProfileArray: (NSMutableArray*) mechanicProfileArray
{
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SALUTATION), //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_DROPDOWN),
                                       kForm_Key: kSalutation,
                                       kForm_Placeholder: LOCALIZATION(C_PROFILE_SALUTATION_PLACEHOLDER),   //lokalised 8 Feb
                                       kForm_Value: [mSession convertToSalutation: self.member.salutation],
                                       kForm_Enabled: @((IS_COUNTRY(COUNTRYCODE_VIETNAM) || IS_COUNTRY(COUNTRYCODE_INDIA))),
                                       kForm_Optional: @((IS_COUNTRY(COUNTRYCODE_VIETNAM) || IS_COUNTRY(COUNTRYCODE_INDIA))),
                                       }];
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_FIRSTNAME),  //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                       kForm_Key: kFirstName,
                                       kForm_Value: self.member.firstName,
                                       kForm_Enabled: @((IS_COUNTRY(COUNTRYCODE_VIETNAM) || IS_COUNTRY(COUNTRYCODE_INDIA))),
                                       }]; //first name
    
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_LASTNAME),   //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                       kForm_Key: kLastName,
                                       kForm_Value: self.member.lastName,
                                       kForm_Enabled: @((IS_COUNTRY(COUNTRYCODE_VIETNAM) || IS_COUNTRY(COUNTRYCODE_INDIA))),
                                       }]; //last name
    
    [mechanicProfileArray addObject:@{kForm_Title: LOCALIZATION(C_PROFILE_DOB), //lokalised 8 Feb
                                      kForm_Type: @(kREGISTRATION_BIRTHDATE),
                                      kForm_Key: kDOB,
                                      kForm_Value: [self convertDateForDisplay: self.member.dobMonth day:self.member.dobDay],
                                      kForm_Enabled: @((IS_COUNTRY(COUNTRYCODE_VIETNAM) || IS_COUNTRY(COUNTRYCODE_INDIA))),
                                      kForm_Optional: @(YES),
                                      }];
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_MOBILENUM),  //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_MOBNUM),
                                       kForm_Key: kMobileNumber,
                                       kForm_TextfieldEnabled: @((IS_COUNTRY(COUNTRYCODE_VIETNAM) || IS_COUNTRY(COUNTRYCODE_INDIA))),
                                       kForm_CountryCode: [mSession convertCountryCodeToCountryName: GET_COUNTRY_CODE],
                                       kForm_MobileNumber: self.member.mobileNumber,
                                       }];   //Mobile number
    
    [mechanicProfileArray addObject:@{kForm_Title: LOCALIZATION(C_PROFILE_EMAILADDRESS),    //lokalised 8 Feb
                                      kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                      kForm_Key: kEmailAddress,
                                      kForm_Placeholder: LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER),  //lokalised 8 Feb
                                      kForm_Value: self.member.emailAddress,
                                      kForm_Enabled: @((IS_COUNTRY(COUNTRYCODE_VIETNAM) || IS_COUNTRY(COUNTRYCODE_INDIA))),
                                      kForm_Optional: @(!IS_COUNTRY(COUNTRYCODE_RUSSIA)),
                                      }];
}

-(void) setupLastSectionArray: (NSMutableArray *)lastSectionArray
{
    if ([[mSession profileInfo] enableCustomerRedemption])
    {
        [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_MECH_REDEEMFLAG),    //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_TNCCHECKBOX),
                                       kForm_Key: kRedemptionCheckBox,
                                       kForm_Enabled: @(self.hasUpdateMechanic),
                                       kForm_CheckboxSelected: @(self.redemptionFlag),
                                       }];
    }
    
    if (self.hasUpdateMechanic)
    {
        [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_MECH_UPDATEDETAILS), //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                       kForm_Key: kUpdateCTA,
                                       kForm_Enabled: @(YES),
                                       kForm_TextColour: COLOUR_VERYDARKGREY,
                                       kForm_BackgroundColour: COLOUR_YELLOW,
                                       }];
    }
    
    if(![self.tradeID isEqualToString:@""]) //approved mechanic
    {
        [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_MECHANICDETAILS_DEACTIVATE), //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                       kForm_Key: kDeactivateCTA,
                                       kForm_Enabled: @(YES),
                                       kForm_TextColour: COLOUR_WHITE,
                                       kForm_BackgroundColour: COLOUR_RED,
                                       }];
    }
    else
    {
        [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_MECHANICDETAILS_REJECT), //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                       kForm_Key: kRejectCTA,
                                       kForm_Enabled: @(YES),
                                       kForm_TextColour: COLOUR_WHITE,
                                       kForm_BackgroundColour: COLOUR_RED,
                                       }];
        [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_MECHANICDETAILS_APPROVE), //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                       kForm_Key: kApproveCTA,
                                       kForm_Enabled: @(YES),
                                       kForm_TextColour: COLOUR_WHITE,
                                       kForm_BackgroundColour: COLOUR_RED,
                                       }];
    }
    

}

-(void) ctaPressedWithKey:(NSString *) ctaKey
{
    if ([ctaKey isEqualToString: kUpdateCTA])
    {
        [self registerPressed];
    }
    else if ([ctaKey isEqualToString: kDeactivateCTA])
    {
        [self deactivatePressed];
    } else if ([ctaKey isEqualToString: kRejectCTA])
    {
        [self rejectPressed];
    } else if ([ctaKey isEqualToString: kApproveCTA])
    {
        [self approvePressed];
    }
}

-(void) deactivatePressed
{
    NSMutableDictionary *jsonValuesDict = [[NSMutableDictionary alloc] initWithDictionary: [self.tableViewDelegate getJsonValues] ];
    
    NSMutableDictionary *submitParams = [[NSMutableDictionary alloc] init];
    
    [submitParams setObject: [jsonValuesDict objectForKey: kSalutation] forKey: kSalutation];
    [submitParams setObject: [jsonValuesDict objectForKey:kFirstName] forKey: kFirstName];
    [submitParams setObject: [jsonValuesDict objectForKey:kLastName] forKey: kLastName];
    [submitParams setObject: [jsonValuesDict objectForKey:kMobileNumber] forKey: kMobileNumber];
    [submitParams setObject: [jsonValuesDict objectForKey:kEmailAddress] forKey: kEmailAddress];
    
    [submitParams setObject:self.member.dobMonth forKey: @"DOBMonth"];
    [submitParams setObject:self.member.dobDay forKey:@"DOBDay"];
    
    [submitParams setObject: @([[jsonValuesDict objectForKey: kRedemptionCheckBox] boolValue]) forKey:@"EnableCustomerRedemption"];
    
    [submitParams setObject: self.workshopTradeID forKey:@"WorkshopTradeID"];
    
    [submitParams setObject: self.member.contactPreference forKey: kContactPreference]; //no contact preferences
    
    [submitParams setObject: @(1) forKey:@"TermCondition"];
    [submitParams setObject: self.tradeID forKey:@"TradeID"];
    [submitParams setObject: @(4) forKey:@"RegistrationStatus"];
    
    if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND]) {
        //    LOOKUP VALUE
        //    StaffAccountType =({KeyCode = 1;KeyValue = Mechanic;},
        //                       {KeyCode = 2;KeyValue = "Forecourt Attendant";});
        [submitParams setObject:@(1) forKey:@"StaffType"];
        
    }
    
    [[WebServiceManager sharedInstance] updateMechanic: submitParams vc:self];
}

-(void)hyperlinkPressed:(NSString *) link
{
    //kForm_Hyperlinks: @{LOCALIZATION(C_FORM_TNC_IN): @"tnc",
    //                    LOCALIZATION(C_PROFILE_PRIVACY) : @"privacypolicy"
    if ([link isEqualToString: @"tnc"])
    {
        [mSession pushTNCView: self.navigationController.parentViewController];
    }
    else if ([link isEqualToString: @"privacypolicy"])
    {
        [mSession pushPrivacyView: self.navigationController.parentViewController];
    }
}

-(void) dropdownPressedWithKey:(NSString *) formKey
{
    [self.view endEditing: YES];
    listView.formKey = formKey;
    
    //Salutation
    if ([formKey isEqualToString: kSalutation])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO]; //lokalised 8 Feb
    }
    else
    {
        listView.formKey = nil;
        [mAlert showErrorAlertWithMessage: @"Unknown Error"];
        return;
    }
    [self popUpView];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        [self.tableViewDelegate editCell: listView.formKey withDropdownSelection: selection.firstObject];
        listView.formKey = nil;
    }
}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

-(void)textfieldCellDidEndEditingWithKey:(NSString *)key
{
    
}

-(void)textfieldWithButtonPressedWithKey:(NSString *)buttonKey
{
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_COUNTRY)    //lokalised 8 Feb
                                                   list: [mSession getCountryNames]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
    
    //preload dateview
    dateView = [[CASDatePickerView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_BIRTHDAY) previouslySelected:nil withYearHidden:YES];   //lokalised 8 Feb
    
    dateView.delegate = self;
    
    
    if(![self.tradeID isEqualToString:@""]) //approved mechanic
    {
        //deactivateCTA
        //update details CTA
//        if([[mSession profileInfo] enableCustomerRedemption])
//        {}
        
        [[WebServiceManager sharedInstance]fetchMechanicDetail:self.tradeID vc:self];
    }
    else
    {
        //set approve cta
        //set reject cta
        //set remarks
        [[WebServiceManager sharedInstance]fetchRegMechanicDetail:self.registrationID vc:self];
    }
}

-(void) registerPressed
{
    NSMutableDictionary *jsonValuesDict = [[NSMutableDictionary alloc] initWithDictionary: [self.tableViewDelegate getJsonValues] ];
    
    NSMutableDictionary *submitParams = [[NSMutableDictionary alloc] init];
    
    [submitParams setObject: @([[mSession convertToSalutationKeyCode: [jsonValuesDict objectForKey: kSalutation]] intValue]) forKey: kSalutation];
    [submitParams setObject: [jsonValuesDict objectForKey:kFirstName] forKey: kFirstName];
    [submitParams setObject: [jsonValuesDict objectForKey:kLastName] forKey: kLastName];
    [submitParams setObject: [jsonValuesDict objectForKey:kMobileNumber] forKey: kMobileNumber];
    [submitParams setObject: [jsonValuesDict objectForKey:kEmailAddress] forKey: kEmailAddress];
    
    [submitParams setObject:self.member.dobMonth forKey: @"DOBMonth"];
    [submitParams setObject:self.member.dobDay forKey:@"DOBDay"];
    [submitParams setObject:self.member.contactPreference forKey: kContactPreference];
    
    [submitParams setObject: @([[jsonValuesDict objectForKey: kRedemptionCheckBox] boolValue]) forKey:@"EnableCustomerRedemption"];
    
    [submitParams setObject: self.tradeID forKey:@"TradeID"];
    [submitParams setObject: @(1) forKey:@"RegistrationStatus"];
    if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND]) {
        //    LOOKUP VALUE
        //    StaffAccountType =({KeyCode = 1;KeyValue = Mechanic;},
        //                       {KeyCode = 2;KeyValue = "Forecourt Attendant";});
        [submitParams setObject:@(1) forKey:@"StaffType"];
        
    }
    [[WebServiceManager sharedInstance] updateMechanic: submitParams vc:self];
}

-(void) approvePressed
{
    NSMutableDictionary *jsonValuesDict = [[NSMutableDictionary alloc] initWithDictionary: [self.tableViewDelegate getJsonValues] ];
    
    NSMutableDictionary *submitParams = [[NSMutableDictionary alloc] init];
    
    [submitParams setObject: @([[mSession convertToSalutationKeyCode: [jsonValuesDict objectForKey: kSalutation]] intValue]) forKey: kSalutation];
    [submitParams setObject: [jsonValuesDict objectForKey:kFirstName] forKey: kFirstName];
    [submitParams setObject: [jsonValuesDict objectForKey:kLastName] forKey: kLastName];
    [submitParams setObject: [jsonValuesDict objectForKey:kMobileNumber] forKey: kMobileNumber];
    [submitParams setObject: [jsonValuesDict objectForKey:kEmailAddress] forKey: kEmailAddress];
    
    [submitParams setObject:self.member.dobMonth forKey: @"DOBMonth"];
    [submitParams setObject:self.member.dobDay forKey:@"DOBDay"];
    [submitParams setObject:self.member.contactPreference forKey: kContactPreference];
    
    [submitParams setObject: self.workshopTradeID forKey:@"WorkshopTradeID"];
    
    [submitParams setObject: @([[jsonValuesDict objectForKey: kRedemptionCheckBox] boolValue]) forKey:@"EnableCustomerRedemption"];
    [submitParams setObject: @(1) forKey:@"TermCondition"];
    [submitParams setObject: self.tradeID forKey:@"TradeID"];
    [submitParams setObject: @(2) forKey:@"RegistrationStatus"];
    [submitParams setObject: self.registrationID forKey:@"RegistrationID"];
    [submitParams setObject: GET_COUNTRY_CODE forKey:COUNTRYCODE_KEY];
    if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND]) {
        //    LOOKUP VALUE
        //    StaffAccountType =({KeyCode = 1;KeyValue = Mechanic;},
        //                       {KeyCode = 2;KeyValue = "Forecourt Attendant";});
        [submitParams setObject:@(1) forKey:@"StaffType"];
        
    }
   [[WebServiceManager sharedInstance] updateMechanicRegistration: submitParams vc:self];
}

-(void) rejectPressed
{
    NSMutableDictionary *jsonValuesDict = [[NSMutableDictionary alloc] initWithDictionary: [self.tableViewDelegate getJsonValues] ];
    
    NSMutableDictionary *submitParams = [[NSMutableDictionary alloc] init];
    
    [submitParams setObject: @([[mSession convertToSalutationKeyCode: [jsonValuesDict objectForKey: kSalutation]] intValue]) forKey: kSalutation];
    [submitParams setObject: [jsonValuesDict objectForKey:kFirstName] forKey: kFirstName];
    [submitParams setObject: [jsonValuesDict objectForKey:kLastName] forKey: kLastName];
    [submitParams setObject: [jsonValuesDict objectForKey:kMobileNumber] forKey: kMobileNumber];
    [submitParams setObject: [jsonValuesDict objectForKey:kEmailAddress] forKey: kEmailAddress];
    
    [submitParams setObject:self.member.dobMonth forKey: @"DOBMonth"];
    [submitParams setObject:self.member.dobDay forKey:@"DOBDay"];
    [submitParams setObject:self.member.contactPreference forKey: kContactPreference];
    
    [submitParams setObject: self.workshopTradeID forKey:@"WorkshopTradeID"];
    
    [submitParams setObject: @([[jsonValuesDict objectForKey: kRedemptionCheckBox] boolValue]) forKey:@"EnableCustomerRedemption"];
    [submitParams setObject: @(1) forKey:@"TermCondition"];
    [submitParams setObject: self.tradeID forKey:@"TradeID"];
    [submitParams setObject: @(4) forKey:@"RegistrationStatus"];
    [submitParams setObject: self.registrationID forKey:@"RegistrationID"];
    [submitParams setObject: GET_COUNTRY_CODE forKey:COUNTRYCODE_KEY];
    if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND]) {
        //    LOOKUP VALUE
        //    StaffAccountType =({KeyCode = 1;KeyValue = Mechanic;},
        //                       {KeyCode = 2;KeyValue = "Forecourt Attendant";});
        [submitParams setObject:@(1) forKey:@"StaffType"];
        
    }
    [[WebServiceManager sharedInstance] updateMechanicRegistration: submitParams vc:self];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_UPDATEREGMECHANIC:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                [self popSelf];
            }];
        }
            break;
        case kWEBSERVICE_UPDATEMECHANIC:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                [self popSelf];
            }];
        }
            break;
        case kWEBSERVICE_FETCHMECHANIC:
        {
            NSDictionary *respData = [response getGenericResponse];
            self.member = [[Member alloc] initWithData: respData];
            
            if (![respData objectForKey: @"EnableCustomerRedemption"] || [[respData objectForKey: @"EnableCustomerRedemption"] isKindOfClass: [NSNull class]])
                    self.redemptionFlag = NO;
            else
                self.redemptionFlag = [[respData objectForKey: @"EnableCustomerRedemption"] boolValue];
            
            if (![respData objectForKey: @"HasUpdateMechanic"] || [[respData objectForKey: @"HasUpdateMechanic"] isKindOfClass: [NSNull class]])
                self.hasUpdateMechanic = NO;
            else
                self.hasUpdateMechanic = [[respData objectForKey: @"HasUpdateMechanic"] boolValue];
            
            [self setupInterface];
            [self.regFormTableView reloadData];
            
            //            if (![[respData objectForKey: @"DigitalSignature"] isKindOfClass: [NSNull class]] &&
            //                [GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
            //            {
            //                NSString *str = [respData objectForKey: @"DigitalSignature"];
            //                NSData *data = [[NSData alloc]  initWithBase64EncodedString: [respData objectForKey: @"DigitalSignature"] options: NSDataBase64DecodingIgnoreUnknownCharacters];
            //                self.imgSignature.image = [[UIImage alloc] initWithData: data];
            //            }
        }
            break;
        case kWEBSERVICE_FETCHREGMECHANIC:
        {
            NSDictionary *respData = [response getGenericResponse];
            self.member = [[Member alloc] initWithData: respData];
            
            
            if (![respData objectForKey: @"EnableCustomerRedemption"] || [[respData objectForKey: @"EnableCustomerRedemption"] isKindOfClass: [NSNull class]])
                self.redemptionFlag = NO;
            else
                self.redemptionFlag = [[respData objectForKey: @"EnableCustomerRedemption"] boolValue];
            
            [self setupInterface];
            [self.regFormTableView reloadData];
            
            //            if (![[respData objectForKey: @"DigitalSignature"] isKindOfClass: [NSNull class]])
            //            {
            //                NSString *str = [respData objectForKey: @"DigitalSignature"];
            //                NSData *data = [[NSData alloc]  initWithBase64EncodedString: [respData objectForKey: @"DigitalSignature"] options: NSDataBase64DecodingIgnoreUnknownCharacters];
            //                self.imgSignature.image = [[UIImage alloc] initWithData: data];
            //            }
        }
            break;
        default:
            break;
    }
}

#pragma mark birthdate dropdown delegate methods
-(void)birthdatePressedWithKey:(NSString *)formKey
{
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}

- (void)selectedDate:(NSDate *)selectedDate
{
    //do smething
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //   for API
    [formatter setDateFormat:@"dd"];
    self.member.dobDay = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"MM"];
    self.member.dobMonth = [formatter stringFromDate:selectedDate];
    
    if(kIsRightToLeft) {
        [formatter setDateFormat:@"MMMM"];
    } else {
        [formatter setDateFormat:@"dd MMMM"];
    }
    
    if ([GET_LOCALIZATION isEqualToString: kThai])
        [formatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
    else
        [formatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    NSString *selectedDateString = [formatter stringFromDate:selectedDate];
    
    //add formkey to birthday class if needed
    if(kIsRightToLeft) {
        [self.tableViewDelegate editCell: kDOB withDropdownSelection: [NSString stringWithFormat:@"%@ %@",self.member.dobDay,selectedDateString]];
    } else {
        [self.tableViewDelegate editCell: kDOB withDropdownSelection: selectedDateString];
    }
}


#pragma mark Date methods
-(NSString *) convertDateForDisplay:(NSString *)monthString day:(NSString *) dayString
{
    if ([monthString isEqualToString: BLANK] || [dayString isEqualToString: BLANK])
        return LOCALIZATION(C_PROFILE_DOB_PLACEHOLDER); //lokalised 8 Feb
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    if ([GET_LOCALIZATION isEqualToString: kThai])
        [dateFormat setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
    else
        [dateFormat setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    if(kIsRightToLeft) {
        [dateFormat setDateFormat:@"MM"];
        NSDate *date = [dateFormat dateFromString: [NSString stringWithFormat: @"%@", monthString]];
        
        // Convert date object to desired output format
        [dateFormat setDateFormat:@"MMMM"];
        return [NSString stringWithFormat:@"%@ %@",dayString,[dateFormat stringFromDate:date]];
    } else {
        [dateFormat setDateFormat:@"MMdd"];
        NSDate *date = [dateFormat dateFromString: [NSString stringWithFormat: @"%@%@", monthString, dayString]];
        
        // Convert date object to desired output format
        [dateFormat setDateFormat:@"dd MMMM"];
        return [dateFormat stringFromDate:date];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
