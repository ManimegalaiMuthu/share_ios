//
//  MechanicDetails_NewViewController.h
//  Shell
//
//  Created by Jeremy Lua on 13/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface MechanicDetails_NewViewController : BaseVC
@property NSString *tradeID;
@property NSString *workshopTradeID;
@property NSString *registrationID;

@end

NS_ASSUME_NONNULL_END
