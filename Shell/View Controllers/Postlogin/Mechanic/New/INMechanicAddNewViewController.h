//
//  INMechanicAddNewViewController.h
//  Shell
//
//  Created by Jeremy Lua on 26/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface INMechanicAddNewViewController : BaseVC

@end

NS_ASSUME_NONNULL_END
