//
//  INMechanicAddNewViewController.m
//  Shell
//
//  Created by Jeremy Lua on 26/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "FormsTableViewDelegate.h"
#import "INMechanicAddNewViewController.h"
#import <MapKit/MapKit.h>
#import "QRCodeScanner.h"

#define kWorkshopQRCode         @"WorkshopQRCode"       //Workshop SHARE Code (for QR Scan)
//#define kMechanicShareCode      @"MechanicShareCode"
//#define kMechanicType           @"MechanicType" //MechanicType use CompanySource
#define kCompanySource           @"CompanySource" //MechanicType use CompanySource
#define kCompanyCategory       @"CompanyCategory"
#define kCompanyType            @"CompanyType"
#define kReferenceCode          @"ReferenceCode"        //Workshop Code
#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"

#define kDOB                @"DateOfBirth"
#define kEmailAddess        @"EmailAddress"

//Address
#define kWorkshopLocation   @"Location"
#define kPostalCode         @"PostalCode"
#define kState              @"State"
#define kTown               @"CompanyCity"
#define kAddress1           @"Address1"
#define kAddress2           @"Address2"

//Bank Details
#define kValidateCheckboxes @"ValidateCheckboxes"
#define kPANCardId              @"PANCardId"
#define kPANCardImage           @"PANCardImage"
#define kDrivingLicense         @"DrivingLicenseID"
#define kDrivingLicenseImage    @"DrivingLicenseImage"
#define kVoterID                @"VoterID"
#define kVoterIDImage           @"VoterIDImage"
#define kBeneficiaryName    @"BeneficiaryName"
#define kBankID             @"BankID"
#define kAccountNumber      @"AccountNumber"
#define kConfirmAccountNumber      @"ConfirmAccountNumber"

#define kIFSCCode           @"IFSCCode"
#define kBeneficiaryImage   @"BeneficiaryImage" //for UI only


//last section
#define kBankCheckBox   @"BankCheckBox" //for checkbox only
#define kTncCheckBox   @"TncCheckBox" //for checkbox only
#define kRegisterCTA        @"RegisterCTA"

@interface INMechanicAddNewViewController () <FormsTableViewDelegate, WebServiceManagerDelegate, CLLocationManagerDelegate, QRCodeScannerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *regFormTableView;

@property NSArray *stateTable;
@property FormsTableViewDelegate *tableViewDelegate;

@property CLLocationManager *locationManager;
@property CGFloat latitude;
@property CGFloat longitude;

@property NSMutableArray *mechanicProfileArray;

@end

@implementation INMechanicAddNewViewController
@synthesize listView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager requestWhenInUseAuthorization];
    
    [[WebServiceManager sharedInstance] loadState: [[mSession lookupTable] objectForKey: @"LookUpVersion"]
                                      countryCode: GET_COUNTRY_CODE
                                               vc: self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_MYNETWORK),LOCALIZATION_EN(C_MANAGEWORKSHOP_ADDNEW)] screenClass:nil];
}

-(void) setupInterface
{
    
    self.tableViewDelegate = [[FormsTableViewDelegate alloc] init];
    
    if(GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
    {
        NSMutableArray *staffProfileArray = [NSMutableArray new];
        [self setupStaffProfileArray: staffProfileArray];
        [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_HEADER_PROFILE)];    //lokalised 8 Feb
        [self.tableViewDelegate.initializingArray addObject: staffProfileArray];
    }
    else
    {
        self.mechanicProfileArray = [[NSMutableArray alloc] init];
        [self setupMechanicProfileArray: self.mechanicProfileArray];
        [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_HEADER_MECHANICPROFILE)];    //lokalised 8 Feb
        [self.tableViewDelegate.initializingArray addObject: self.mechanicProfileArray];
        
        NSMutableArray *addressArray = [[NSMutableArray alloc] init];
        [self setupAddressArray: addressArray];
        [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_PROFILE_ADDRESS)];   //lokalised 8 Feb
        [self.tableViewDelegate.initializingArray addObject: addressArray];
    }
    
    NSMutableArray *bankDetailsArray = [[NSMutableArray alloc] init];
    [self setupBankDetailsArray: bankDetailsArray];
    [self.tableViewDelegate.headersArray addObject: LOCALIZATION(C_HEADER_BANKDETAILS)]; //lokalised 8 Feb
    [self.tableViewDelegate.initializingArray addObject: bankDetailsArray];
    
    NSMutableArray *cardValidationArray = [[NSMutableArray alloc] init];
    [self setupAccountValidationArray: cardValidationArray];
    [self.tableViewDelegate.headersArray addObject: LOCALIZATION(@"Account Verification")];
    [self.tableViewDelegate.initializingArray addObject: cardValidationArray];
    
    NSMutableArray *lastSectionArray = [[NSMutableArray alloc] init];
    [self setupLastSectionArray: lastSectionArray];
    [self.tableViewDelegate.headersArray addObject: @""]; //more than 1 char, to show required fields label
    [self.tableViewDelegate.initializingArray addObject: lastSectionArray];
    
    self.regFormTableView.delegate = self.tableViewDelegate;
    self.regFormTableView.dataSource = self.tableViewDelegate;
    
    
    [self.tableViewDelegate registerNib: self.regFormTableView];
    [self.tableViewDelegate setDelegate: self];
    
    if(@available(iOS 11, *))
    {
        self.regFormTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.regFormTableView setContentInset:UIEdgeInsetsMake(15, 0, 15, 0)];
    
    
    self.regFormTableView.backgroundColor = [UIColor clearColor];
    self.regFormTableView.backgroundView = nil;
}

-(void) setupMechanicProfileArray: (NSMutableArray *) mechanicProfileArray
{
//    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(@"Mechanic Source"),
//                                       kForm_Type: @(kREGISTRATION_DROPDOWN),
//                                       kForm_Key: kCompanySource,
//                                       kForm_Value: LOCALIZATION(@"Select Mechanic Source"),
//                                       }];
//
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_MECHTYPE),   //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_DROPDOWN),
                                       kForm_Key: kCompanyType,
                                       kForm_Value: LOCALIZATION(C_DROPDOWN_MECHTYPE),   //lokalised 8 Feb
                                       }];
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_MECHCATEGORY),   //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_DROPDOWN),
                                       kForm_Key: kCompanyCategory,
                                       kForm_Value: LOCALIZATION(C_DROPDOWN_MECHCATEGORY),   //lokalised 8 Feb
                                       }];
    
//    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_MECHSHARECODE),  //lokalised 8 Feb
//                                       kForm_Type: @(kREGISTRATION_TEXTFIELDWITHBUTTON),
//                                       kForm_Icon: @"icon-code",
//                                       kForm_Key: kWorkshopQRCode,
//                                       kForm_Enabled: @(YES),
//                                       kForm_TextfieldEnabled: @(YES),
//                                       kForm_Optional: @(YES),
//                                       }];   //Workshop SHARE Code
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPCODE),     //lokalised 4 feb
                                          kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                          kForm_Key: kReferenceCode,
                                          kForm_Optional: @(YES),
                                       }];   //Workshop Code
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SALUTATION), //lokalised 8 Feb
                                    kForm_Type: @(kREGISTRATION_DROPDOWN),
                                    kForm_Key: kSalutation,
                                    kForm_Value: LOCALIZATION(C_PROFILE_SALUTATION_PLACEHOLDER),    //lokalised 8 Feb
                                    kForm_Optional: @(YES),
                                    }];
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_FIRSTNAME),  //lokalised 8 Feb
                                    kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                    kForm_Key: kFirstName,
//                                    kForm_Value: ,
                                    }]; //first name
    
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_LASTNAME),   //lokalised 8 Feb
                                    kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                    kForm_Key: kLastName,
//                                    kForm_Value: self.member.lastName,
                                    }]; //last name
    
    [mechanicProfileArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_MOBILENUM),  //lokalised 8 Feb
                                    kForm_Type: @(kREGISTRATION_MOBNUM),
                                    kForm_Key: kMobileNumber,
                                    kForm_TextfieldEnabled: @(YES), //if not approved, allow editing
                                    kForm_CountryCode: [mSession convertCountryCodeToCountryName: GET_COUNTRY_CODE],
//                                    kForm_MobileNumber: ,
                                    }];   //Mobile number
    
    
}

-(void) setupStaffProfileArray: (NSMutableArray*) staffArray
{
    [staffArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_SALUTATION),   //lokalised 8 Feb
                             kForm_Type: @(kREGISTRATION_DROPDOWN),
                             kForm_Key: kSalutation,
                             kForm_Value: LOCALIZATION(C_PROFILE_SALUTATION_PLACEHOLDER),   //lokalised 8 Feb
                             kForm_Optional: @(YES),
                             }];
    
    [staffArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_FIRSTNAME),    //lokalised 8 Feb
                             kForm_Type: @(kREGISTRATION_TEXTFIELD),
                             kForm_Key: kFirstName,
                             //                                    kForm_Value: ,
                             }]; //first name
    
    
    [staffArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_LASTNAME), //lokalised 8 Feb
                             kForm_Type: @(kREGISTRATION_TEXTFIELD),
                             kForm_Key: kLastName,
                             //                                    kForm_Value: self.member.lastName,
                             }]; //last name
    
    [staffArray addObject:@{kForm_Title: LOCALIZATION(C_PROFILE_DOB),   //lokalised 8 Feb
                            kForm_Type: @(kREGISTRATION_BIRTHDATE),
                            kForm_Key: kDOB,
                            kForm_Value: LOCALIZATION(C_PROFILE_DOB_PLACEHOLDER),   //lokalised 8 Feb
                            }];
    
    [staffArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_MOBILENUM),    //lokalised 8 Feb
                             kForm_Type: @(kREGISTRATION_MOBNUM),
                             kForm_Key: kMobileNumber,
                             kForm_TextfieldEnabled: @(YES), //if not approved, allow editing
                             kForm_CountryCode: [mSession convertCountryCodeToCountryName: GET_COUNTRY_CODE],
                             //                                    kForm_MobileNumber: ,
                             }];   //Mobile number
    
    [staffArray addObject:@{kForm_Title: LOCALIZATION(C_PROFILE_EMAILADDRESS),  //lokalised 8 Feb
                            kForm_Type: @(kREGISTRATION_TEXTFIELD),
                            kForm_Key: kEmail,
                            kForm_Placeholder: LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER),    //lokalised 8 Feb
                            }];
}

-(void) setupAddressArray: (NSMutableArray *) addressArray
{
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_LOCATION),   //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                                       kForm_TextfieldEnabled: @(NO),
                                       kForm_Icon: @"icon-location",
                                       kForm_Key: kWorkshopLocation,
                                       kForm_Enabled: @(YES),
//                                       kForm_Value: [NSString stringWithFormat: @"%f, %f", self.companyAddress.latitude, self.companyAddress.longitude],
                                       }];    //location
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_POSTALCODE), //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                       kForm_Key: kPostalCode,
                                       kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                       kForm_HasDelegate : @(YES), //textfield delegate for postal code
                                       }]; //postal code
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_STATE),  //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_DROPDOWN),
                                       kForm_Key: kState,
                                       kForm_Value: LOCALIZATION(C_PROFILE_STATE_PLACEHOLDER),
                                       }];
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_TOWN),   //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                       kForm_Key: kTown, //kCity?
//                                       kForm_Value: self.companyAddress.companyCity,
                                       }];
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_ADDRESSLINE1),   //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                       kForm_Key: kAddress1,
//                                       kForm_Value: self.companyAddress.address1,
                                       }]; //address line 1
    
    [addressArray addObject: @{kForm_Title: LOCALIZATION(C_PROFILE_ADDRESSLINE2),   //lokalised 8 Feb
                                       kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                       kForm_Key: kAddress2,
//                                       kForm_Value: self.companyAddress.address2,
                                       kForm_Optional: @(YES),
                                       }];
}

-(void) setupBankDetailsArray: (NSMutableArray *) bankDetailsArray
{
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BENEFICIARYNAME),   //lokalised 8 Feb
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_Key: kBeneficiaryName,
                               kForm_Placeholder: LOCALIZATION(C_FORM_BENEFICIARYNAME_PLACEHOLDER),
                                   //lokalised 8 Feb
                               kForm_Optional:@(YES),
                               }];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BENEFICIARYACCTNUM),    //lokalised 8 Feb
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                               kForm_Key: kAccountNumber,
                                   kForm_SecureEntry: @(YES),
                                   kForm_HasDelegate : @(IS_COUNTRY(COUNTRYCODE_INDIA)), //textfield delegate for account number
                                   kForm_Optional:@(YES),
                                   
                               }];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_CONFIRMBENEFICIARYNUM),  //lokalised 4 feb
                                   kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                   kForm_Keyboard: @(UIKeyboardTypeNumberPad),
                                   kForm_Key: kConfirmAccountNumber,
                                   kForm_Value: @"",
                                   kForm_Optional:@(YES),
                                   }];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BENEFICIARYIFSC),   //lokalised 8 Feb
                               kForm_Type: @(kREGISTRATION_TEXTFIELD),
                               kForm_Key: kIFSCCode,
                               kForm_HasDelegate : @(IS_COUNTRY(COUNTRYCODE_INDIA)), //textfield delegate for ifsc code
                               kForm_Optional:@(YES),
                               }];
    
    [bankDetailsArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BANKNAME),  //lokalised 8 Feb
                                   kForm_Type: @(kREGISTRATION_DROPDOWN),
                                   kForm_Key: kBankID,
                                   kForm_Value: LOCALIZATION(C_DROPDOWN_BANKNAME),  //lokalised 8 Feb
                                   kForm_Enabled: @(NO),
                                   kForm_Optional:@(YES),
                                   }];
    
    
    [bankDetailsArray addObject: @{kForm_Type: @(kFORM_PROFILE_IMAGE),
                               kForm_Key: kBeneficiaryImage, //for UI only
                                   kForm_Value: @"cheque_sample",                               }]; //
}

#pragma mark - bank account validation
-(void) setupAccountValidationArray: (NSMutableArray *) cardValidationArray {
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(@"Validate Account by"),  //lokalised 4 feb
                                   kForm_Type: @(kFORM_MULTICHECKBOX),
                                   kForm_Key: kValidateCheckboxes,
                                   kForm_Enabled: @(YES),
                                   kForm_HasDelegate: @(YES),
                                      kForm_CheckboxLabels:@[LOCALIZATION(C_FORM_PANCARD),
                                                             LOCALIZATION(C_FORM_DRIVINGLICENSEID),
                                                             LOCALIZATION(C_FORM_VOTERID),
                                                             ],
                                   kForm_LinkedCellKeys: @[kPANCardId,
                                                           kDrivingLicense,
                                                           kVoterID,
                                                           ],
                                   }];
    

    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_PANCARDID), //lokalised 8 Feb
                                   kForm_Type: @(kFORM_PANCARD_ID_NEW),
                                   kForm_Key: kPANCardId,
                                   kForm_Optional: @(YES),  //controlled in formdelegate level.
                                   }];
    
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_PANCARDIMAGE),  //lokalised 8 Feb
                                   kForm_Type: @(
                                       kFORM_PANCARD_IMAGE),
                                   kForm_Key: kPANCardImage,
                                   kForm_Placeholder: LOCALIZATION(C_FORM_PANCARDIMAGE_PLACEHOLDER),    //lokalised 8 Feb
                                   kForm_Optional: @(YES),  //controlled in formdelegate level.
                                   }];
    
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_DRIVINGLICENSEID), //lokalised 8 Feb
                                   kForm_Type: @(kFORM_DRIVERLICENSE),
                                   kForm_Key: kDrivingLicense,
                                   kForm_Optional: @(YES),  //controlled in formdelegate level.
                                   }];
    
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_DRIVINGLICENSEIMAGE),  //lokalised 8 Feb
                                   kForm_Type: @(kFORM_PANCARD_IMAGE),//share with pan card image
                                   kForm_Key: kDrivingLicenseImage,
                                   kForm_Placeholder: LOCALIZATION(C_FORM_PANCARDIMAGE_PLACEHOLDER),    //lokalised 8 Feb
                                   kForm_Optional: @(YES),  //controlled in formdelegate level.
                                   }];
    
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_VOTERID), //lokalised 8 Feb
                                   kForm_Type: @(kFORM_VOTERID),
                                   kForm_Key: kVoterID,
                                   kForm_Optional: @(YES),  //controlled in formdelegate level.
                                   }];
    
    [cardValidationArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_VOTERIDIMAGE),  //lokalised 8 Feb
                                   kForm_Type: @(kFORM_PANCARD_IMAGE), //share with pan card image
                                   kForm_Key: kVoterIDImage,
                                   kForm_Optional: @(YES),  //controlled in formdelegate level.
                                   }];
    
}

-(void) setupLastSectionArray: (NSMutableArray *)lastSectionArray
{
    [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_BANKINFO),  //lokalised 8 Feb
                                   kForm_Type: @(kREGISTRATION_TNCCHECKBOX),
                                   kForm_Key: kBankCheckBox,
                                   kForm_Enabled: @(YES),
                                   }];
    
    [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_TNC_IN),    //lokalised 8 Feb
                                   kForm_Type: @(kREGISTRATION_TNCCHECKBOX),
                                   kForm_Key: kTncCheckBox,
                                   kForm_Hyperlinks: @{LOCALIZATION(C_FORM_TNC_IN): @"tnc", //lokalised 8 Feb
                                                       LOCALIZATION(C_PROFILE_PRIVACY) : @"privacypolicy"   //lokalised 8 Feb
                                                       },
                                   kForm_Enabled: @(YES),
                                   }];
    
    [lastSectionArray addObject: @{kForm_Title: LOCALIZATION(C_FORM_REGISTER),  //lokalised 8 Feb
                                   kForm_Type: @(kREGISTRATION_BUTTONCELL),
                                   kForm_Key: kRegisterCTA,
                                   kForm_Enabled: @(YES),
                                   kForm_TextColour: COLOUR_WHITE,
                                   kForm_BackgroundColour: COLOUR_RED,
                                   }];
}

-(void) ctaPressedWithKey:(NSString *) ctaKey
{
    if ([ctaKey isEqualToString: kRegisterCTA])
    {
        [self registerPressed];
    }
}

-(void)hyperlinkPressed:(NSString *) link
{
//kForm_Hyperlinks: @{LOCALIZATION(C_FORM_TNC_IN): @"tnc",
//                    LOCALIZATION(C_PROFILE_PRIVACY) : @"privacypolicy"
    if ([link isEqualToString: @"tnc"])
    {
        [mSession pushTNCView: self.navigationController.parentViewController];
    }
    else if ([link isEqualToString: @"privacypolicy"])
    {
        [mSession pushPrivacyView: self.navigationController.parentViewController];
    }
}

-(void) dropdownPressedWithKey:(NSString *) formKey
{
    [self.view endEditing: YES];
    listView.formKey = formKey;
    
    //Salutation
    if ([formKey isEqualToString: kSalutation])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO]; //lokalised 8 Feb
    }
    else if ([formKey isEqualToString: kCompanySource])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_MECHSOURCE) list: [mSession getCompanySource] hasSearchField: NO];   //lokalised 8 Feb
    }
    else if ([formKey isEqualToString: kCompanyType])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_MECHTYPE) list: [mSession getCompanyTypes] hasSearchField: NO];  //lokalised 8 Feb
    }
    else if ([formKey isEqualToString: kCompanyCategory])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_MECHCATEGORY) list: [mSession getCompanyCategory] hasSearchField: NO];   //lokalised 8 Feb
    }
    else if ([formKey isEqualToString: kState])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_STATE) list: [mSession getStateNames: self.stateTable] hasSearchField: YES]; //lokalised 8 Feb
    }
    else if ([formKey isEqualToString: kBankID])
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_BANKNAME) list: [mSession getBanksDetailsList] hasSearchField: NO];  //lokalised 8 Feb
    }
    else
    {
        listView.formKey = nil;
        [mAlert showErrorAlertWithMessage: @"Unknown Error"];
        return;
    }
    [self popUpView];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        [self.tableViewDelegate editCell: listView.formKey withDropdownSelection: selection.firstObject];
        if ([listView.formKey isEqualToString: kCompanyType])
        {
            NSString *companyTypeCode = [mSession convertToCompanyTypesKeyCode:selection.firstObject];
            if([companyTypeCode isEqualToString:@"2"] || [companyTypeCode isEqualToString:@"4"]) {
                if([self.mechanicProfileArray count] != 7) {
                    [self.mechanicProfileArray insertObject:@{kForm_Title: LOCALIZATION(C_PROFILE_WORKSHOPCODE),
                                                              kForm_Type: @(kREGISTRATION_TEXTFIELD),
                                                              kForm_Key: kReferenceCode,
                                                              kForm_Optional: @(YES),
                                                              } atIndex:2];   //Workshop Code
                    [self.regFormTableView beginUpdates];
                    [self.regFormTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                    [self.regFormTableView endUpdates];
                    
                    [self.regFormTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
                }
            } else {
                if([self.mechanicProfileArray count] == 7) {
                    [self.mechanicProfileArray removeObjectAtIndex:2];
                    [self.regFormTableView beginUpdates];
                    [self.regFormTableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                    [self.regFormTableView endUpdates];
                    
                    [self.regFormTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
                }
            }
        }
        listView.formKey = nil;
    }
}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

-(void)textfieldCellDidBeginEditingWithKey:(NSString *)key
{
    if ([key isEqualToString: kAccountNumber])
        [self.tableViewDelegate editCell: kAccountNumber toSecureField: NO];
}

-(void)textfieldCellDidEndEditingWithKey:(NSString *)key
{
    if ([key isEqualToString: kPostalCode])
    {
        NSString *postalCode = [[self.tableViewDelegate getJsonValues] objectForKey: kPostalCode];
        [mWebServiceManager fetchPostalAddress: postalCode vc:self];
    }
    else if ([key isEqualToString: kIFSCCode])
    {
        [self.tableViewDelegate editCell: kBankID withDropdownSelection: @""];    //Bank Name list
        NSString *ifscCodeString = [[self.tableViewDelegate getJsonValues] objectForKey: kIFSCCode];
        [mWebServiceManager fetchBankDetails: ifscCodeString vc:self];
    }
    else if ([key isEqualToString: kAccountNumber])
        [self.tableViewDelegate editCell: kAccountNumber toSecureField: YES];
}

-(void)textfieldWithButtonPressedWithKey:(NSString *)buttonKey
{
    if([buttonKey isEqualToString: kWorkshopQRCode])
    {
        mQRCodeScanner.delegate = self;
        [mQRCodeScanner showZBarScannerOnVC: self];
    }
    else if([buttonKey isEqualToString:kWorkshopLocation])
    {
        [self locationPressed];
    }
}

- (void)locationPressed {
    [UpdateHUD addMBProgress: self.view withText: @""];
    switch ([CLLocationManager authorizationStatus])
    {
        case kCLAuthorizationStatusAuthorizedAlways:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusNotDetermined:
            [UpdateHUD removeMBProgress: self];
            [self.locationManager requestWhenInUseAuthorization];
            break;
        case kCLAuthorizationStatusDenied:
        {
            [UpdateHUD removeMBProgress: self];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_ALERTVIEW_ERROR) message:LOCALIZATION(C_ALERTVIEW_PERMISSIONERRORMSG) preferredStyle:UIAlertControllerStyleAlert]; //lokalised 8 Feb
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_CANCEL) style:UIAlertActionStyleCancel handler:nil];  //lokalised 8 Feb
            UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_SETTING) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) { //lokalised 8 Feb
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                            UIApplicationOpenSettingsURLString]];
            }];
            
            [alertController addAction:cancelAction];
            [alertController addAction:settingsAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
            break;
        case kCLAuthorizationStatusRestricted:
            [UpdateHUD removeMBProgress: self];
            break;
    }
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [UpdateHUD removeMBProgress: self];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    //most recent location update is at the end of the array.
    CLLocation *location = [locations lastObject];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:6];
    
    NSString *newLatitude   = [formatter stringFromNumber: @(location.coordinate.latitude)];
    NSString *newLongitude  = [formatter stringFromNumber: @(location.coordinate.longitude)];
    
    [self.tableViewDelegate editCell: kWorkshopLocation withTextfieldButtonText: [NSString stringWithFormat: @"%@, %@", newLatitude, newLongitude]];
    
    self.latitude = newLatitude.doubleValue;
    self.longitude = newLongitude.doubleValue;
    
    [self.locationManager stopUpdatingLocation];
    [UpdateHUD removeMBProgress: self];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    //    [self setupInterface];
    
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_COUNTRY)    //lokalised 8 Feb
                                                   list: [mSession getCountryNames]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
}

-(void) registerPressed
{
    NSMutableDictionary *jsonValuesDict = [[NSMutableDictionary alloc] initWithDictionary: [self.tableViewDelegate getJsonValues] ];
    if (IS_COUNTRY(COUNTRYCODE_INDIA) &&
        ![[jsonValuesDict objectForKey: kAccountNumber] isEqualToString: [jsonValuesDict objectForKey: kConfirmAccountNumber]])
    {
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_FORM_BENEFICIARYNUM_NOTMATCH)];
        return;
    }
    
    NSMutableDictionary *submitParams = [[NSMutableDictionary alloc] init];
    
    [submitParams setObject: @([[jsonValuesDict objectForKey: kBankCheckBox] boolValue]) forKey: @"BankInfoSharing"];
    [submitParams setObject: GET_COUNTRY_CODE forKey: @"CountryCode"];
    
    NSMutableDictionary *regBankDict = [[NSMutableDictionary alloc] init];
    [regBankDict setObject:[jsonValuesDict objectForKey: kBeneficiaryName]  forKey: kBeneficiaryName];
    [regBankDict setObject:[jsonValuesDict objectForKey: kAccountNumber] forKey: kAccountNumber];
    [regBankDict setObject:[jsonValuesDict objectForKey: kIFSCCode] forKey: kIFSCCode];
    [regBankDict setObject: [mSession convertToBankNameKeyCode: [jsonValuesDict objectForKey: kBankID]] forKey: kBankID];
    
    [submitParams setObject: regBankDict forKey: @"REG_Bank"];
    
    NSMutableDictionary *regCompanyDict = [NSMutableDictionary new];
    [regCompanyDict setObject: [mSession convertToCompanyCategoryKeyCode: [jsonValuesDict objectForKey: kCompanyCategory]]
                       forKey: kCompanyCategory]; //customer category
    [regCompanyDict setObject: [NSString stringWithFormat: @"%@ %@", [jsonValuesDict objectForKey:kFirstName], [jsonValuesDict objectForKey:kLastName]]
                       forKey: @"CompanyName"];
    [regCompanyDict setObject:@"1" forKey:kCompanySource]; //CompanySource must be set to 1 , for registering IM
    [regCompanyDict setObject:@"" forKey:kReferenceCode];
    [regCompanyDict setObject: [mSession convertToCompanyTypesKeyCode: [jsonValuesDict objectForKey: kCompanyType]]
                       forKey: kCompanyType];
    [regCompanyDict setObject: [jsonValuesDict objectForKey:kMobileNumber] forKey: @"ContactNumber"];
//    [regCompanyDict setObject: [jsonValuesDict objectForKey: kWorkshopQRCode] forKey: kWorkshopQRCode];
    [regCompanyDict setObject: [jsonValuesDict objectForKey: kReferenceCode] forKey: kReferenceCode];
    
    [submitParams setObject: regCompanyDict forKey: @"REG_Company"];
    [submitParams setObject: @{@"Status": @(0)} forKey: @"REG_CompanyStatus"];
    
    NSMutableDictionary *regCompanyAddressDict = [NSMutableDictionary new];
    [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kAddress1] forKey: kAddress1];
    [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kAddress2] forKey: kAddress2];
    [regCompanyAddressDict setObject: @"1" forKey: @"AddressType"];
    [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kTown] forKey: kTown];
    NSString *stateString = [jsonValuesDict objectForKey: kState];
    [regCompanyAddressDict setObject: [mSession convertToStateKeyCode: stateString stateDetailedListArray:self.stateTable]
                              forKey: @"CompanyState"];
    [regCompanyAddressDict setObject: @(self.latitude) forKey: @"Latitude"];
    [regCompanyAddressDict setObject: @(self.longitude) forKey: @"Longitude"];
    [regCompanyAddressDict setObject: [jsonValuesDict objectForKey: kPostalCode] forKey: kPostalCode];
    [regCompanyAddressDict setObject:@"" forKey: @"StreetName"];
    
    [submitParams setObject: regCompanyAddressDict forKey: @"REG_CompanyAddress"];
    
    [submitParams setObject: @{} forKey: @"REG_CompanyWorkHours"];
    [submitParams setObject: @[] forKey: @"REG_CompanyService"];

    
    NSMutableDictionary *regMemberDict = [NSMutableDictionary new];
    [regMemberDict setObject: [jsonValuesDict objectForKey:kFirstName] forKey: kFirstName];
    [regMemberDict setObject: [jsonValuesDict objectForKey:kLastName] forKey: kLastName];
    [regMemberDict setObject: [jsonValuesDict objectForKey:kMobileNumber] forKey: kMobileNumber];
    [regMemberDict setObject: @"1" forKey: @"RegistrationMode"];
    [regMemberDict setObject: @"1" forKey: @"RegistrationType"];
    [regMemberDict setObject: [mSession convertToSalutationKeyCode: [jsonValuesDict objectForKey: kSalutation]] forKey: kSalutation];
    [regMemberDict setObject: [jsonValuesDict objectForKey:kMobileNumber] forKey: @"UserID"];
    
    [submitParams setObject: regMemberDict forKey: @"REG_Member"];
    
    //Document validation
    
    BOOL isValidated = TRUE;
    
    NSLog(@"kValidateCheckboxes ----- %@", [jsonValuesDict objectForKey: kValidateCheckboxes]);
    NSString *checkboxesString = [jsonValuesDict objectForKey: kValidateCheckboxes];
    NSArray *stringArray = [checkboxesString isEqualToString:@""] ? @[] : [checkboxesString componentsSeparatedByString: @","];
    NSString *idProofTypeString = @"";
    for (NSString *string in stringArray)
    {
        NSString *pancardRegex;
        NSPredicate *pancardTest;
        switch ([string intValue])
        {
            case 0:
            {
                idProofTypeString = @"1";
                
                pancardRegex = REGEX_PANCARD;
                pancardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pancardRegex];
                
                if([[jsonValuesDict objectForKey:kPANCardId] isEqualToString:@""]) {
                    isValidated = FALSE;
                    [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_PAN_REQUIRED)];
                    return;
                }
                else if(![pancardTest evaluateWithObject: [jsonValuesDict objectForKey:kPANCardId]]){
                    isValidated = FALSE;
                    [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_PANFORMAT)];
                    return;
                }
                else if([[jsonValuesDict objectForKey:kPANCardImage] isEqualToString:@""]) {
                    isValidated = FALSE;
                    [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_PANIMAGE_REQUIRED)];
                    return;
                }
                [submitParams setObject:[self getPanDetails:jsonValuesDict] forKey:@"REG_PAN"];
            }
                break;
            case 1:
            {
                if(![idProofTypeString isEqualToString:BLANK])
                    idProofTypeString = [NSString stringWithFormat:@"%@,2",idProofTypeString];
                else
                    idProofTypeString = @"2";
                
                pancardRegex = REGEX_DRIVINGLICENSE;
                pancardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pancardRegex];
                
                if([[jsonValuesDict objectForKey:kDrivingLicense] isEqualToString:@""]) {
                    isValidated = FALSE;
                    [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_DRIVINGLICENSE_REQUIRED)];
                    return;
                }
                else if(![pancardTest evaluateWithObject: [jsonValuesDict objectForKey:kDrivingLicense]]){
                    isValidated = FALSE;
                    [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_DRIVINGLICENSEFORMAT)];
                    return;
                }
                else if([[jsonValuesDict objectForKey:kDrivingLicenseImage] isEqualToString:@""]) {
                    isValidated = FALSE;
                    [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_DRIVINGLICENSEIMAGE_REQUIRED)];
                    return;
                }
                [submitParams setObject:[self getDrivingLicenseDetails:jsonValuesDict] forKey:@"REG_DrivingLicense"];
            }
                break;
            case 2:
            {
                if(![idProofTypeString isEqualToString:BLANK])
                   idProofTypeString = [NSString stringWithFormat:@"%@,4",idProofTypeString];
                else
                    idProofTypeString = @"4";
                
                pancardRegex = REGEX_VOTERID;
                pancardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pancardRegex];
                
                if([[jsonValuesDict objectForKey:kVoterID] isEqualToString:@""]) {
                    isValidated = FALSE;
                    [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_VOTERID_REQUIRED)];
                    return;
                }
                else if(![pancardTest evaluateWithObject: [jsonValuesDict objectForKey:kVoterID]]){
                    isValidated = FALSE;
                    [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_VOTERIDFORMAT)];
                    return;
                }
                else if([[jsonValuesDict objectForKey:kVoterIDImage] isEqualToString:@""]) {
                    isValidated = FALSE;
                    [mAlert showErrorAlertWithMessage: LOCALIZATION(C_POPUP_ERROR_VOTERIDIMAGE_REQUIRED)];
                    return;
                }
                 [submitParams setObject:[self getVoterDetails:jsonValuesDict] forKey:@"REG_VoterID"];
            }
                break;
        }
    }
    
    [submitParams setObject:idProofTypeString forKey:@"IDProofType"];
    [submitParams setObject: @([[jsonValuesDict objectForKey: kTncCheckBox] boolValue]) forKey: @"TermCondition"];
    
    if ([submitParams objectForKey:@"REG_PAN"] == nil) {
        [submitParams setObject:@{
                                  @"PANCardId" : @"",
                                  @"PANCardImage" : @"",
                                  }
                         forKey:@"REG_PAN"];
    }
    if ([submitParams objectForKey:@"REG_DrivingLicense"] == nil) {
        [submitParams setObject:@{
                                  @"DrivingLicenseID" : @"",
                                  @"DrivingLicenseImage" : @"",
                                  }
                         forKey:@"REG_DrivingLicense"];
    }
    if ([submitParams objectForKey:@"REG_VoterID"] == nil) {
        [submitParams setObject:@{
                                  @"VoterID" : @"",
                                  @"VoterIDImage" : @"",
                                  }
                         forKey:@"REG_VoterID"];
    }
    
    if(isValidated)
        [[WebServiceManager sharedInstance] addIM: submitParams vc: self];
}

-(NSMutableDictionary *) getPanDetails: (NSDictionary *)jsonValuesDict {
    NSMutableDictionary *regPanDict = [[NSMutableDictionary alloc] init];
    [regPanDict setObject: [jsonValuesDict objectForKey: kPANCardId] forKey: kPANCardId];
    if([jsonValuesDict objectForKey: kPANCardImage])
        [regPanDict setObject: [jsonValuesDict objectForKey: kPANCardImage] forKey: kPANCardImage];
    return regPanDict;
}

-(NSMutableDictionary *) getDrivingLicenseDetails: (NSDictionary *)jsonValuesDict {
    NSMutableDictionary *regPanDict = [[NSMutableDictionary alloc] init];
    [regPanDict setObject: [jsonValuesDict objectForKey: kDrivingLicense] forKey: kDrivingLicense];
    if([jsonValuesDict objectForKey: kDrivingLicenseImage])
        [regPanDict setObject: [jsonValuesDict objectForKey: kDrivingLicenseImage] forKey: kDrivingLicenseImage];
    return regPanDict;
}

-(NSMutableDictionary *) getVoterDetails: (NSDictionary *)jsonValuesDict {
    NSMutableDictionary *regPanDict = [[NSMutableDictionary alloc] init];
    [regPanDict setObject: [jsonValuesDict objectForKey: kVoterID] forKey: kVoterID];
    if([jsonValuesDict objectForKey: kVoterIDImage])
        [regPanDict setObject: [jsonValuesDict objectForKey: kVoterIDImage] forKey: kVoterIDImage];
    return regPanDict;
}


-(void) showPhoto:(UIImage *) image
{
    //popup or push to controller with image
    NSLog(@"show photo");
    
    [mSession pushImagePreview: self.navigationController.parentViewController image: image title: @"Uploaded Image"];
}

-(void) takePhoto:(NSString *)cellKey
{
    if ([cellKey isEqualToString: kPANCardImage])
        [mSession pushPanCardPhotoTakingWithParent: self.navigationController.parentViewController delegateVc: self  message: LOCALIZATION(C_FORM_PANCARD_PHOTOFORMAT) key: cellKey];
    else if ([cellKey isEqualToString: kDrivingLicenseImage])
        [mSession pushPanCardPhotoTakingWithParent: self.navigationController.parentViewController delegateVc: self  message: LOCALIZATION(C_FORM_DRIVINGLICENSE_PHOTOFORMAT) key: cellKey];
    else if ([cellKey isEqualToString: kVoterIDImage])
        [mSession pushPanCardPhotoTakingWithParent: self.navigationController.parentViewController delegateVc: self  message: LOCALIZATION(C_FORM_VOTERID_PHOTOFORMAT) key: cellKey];

}

-(void)imageTaken:(UIImage *)image key:(NSString *)key
{
    [self.tableViewDelegate editCell: key withImage: image];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_ADDIM:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                [UpdateHUD addMBProgress:self.view withText:@""];
                [self setupInterface];
                [UpdateHUD removeMBProgress: self.view];
            }];
        }
            break;
        case kWEBSERVICE_STATE:
        {
            SET_LOADSTATE([[response getGenericResponse] objectForKey: @"StateDetailsList"]);
            self.stateTable = [[response getGenericResponse] objectForKey: @"StateDetailsList"];
            [self setupInterface];
            [UpdateHUD removeMBProgress: KEY_WINDOW];
        }
            break;
        case kWEBSERVICE_FETCH_POSTALADDRESS:
        {
            NSDictionary* responseDictionary = [response getGenericResponse];
            NSDictionary* postalDetails = responseDictionary[@"PostalDetails"];
            
            NSString* stateId = postalDetails[@"StateId"];
            NSString* townName = postalDetails[@"TownName"];
            
            if([stateId isKindOfClass:NSString.class])
            {
                NSString* stateName = [mSession convertToStateName:stateId stateDetailedListArray:GET_LOADSTATE];
                
                [self.tableViewDelegate editCell: kTown withTextfieldString: townName];
                [self.tableViewDelegate editCell: kState withTextfieldString: stateName];
                
            }
        }
            break;
        case kWEBSERVICE_FETCH_BANKDETAILS:
        {
            [self.tableViewDelegate editCell: kBankID withDropdownSelection: [[response getGenericResponse] objectForKey: @"BankName"]];    //Bank Name list
        }
            break;
        default:
            break;
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress: KEY_WINDOW];
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
            self.stateTable = GET_LOADSTATE;
            
            [self setupInterface];
            break;
        default:
            break;
    }
}

- (void)qrCodeScanSuccess:(NSString *)scanString
{
    [self.tableViewDelegate editCell:kWorkshopQRCode withTextfieldButtonText:[[scanString componentsSeparatedByString:@"="] lastObject]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
