//
//  MechanicAddNewViewController.m
//  Shell
//
//  Created by Admin on 29/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "MechanicAddNewViewController.h"
#import "CASDatePickerView.h"
#import "RUCustomerSignatureViewController.h"
#import "PopupWebviewViewController.h"

@interface MechanicAddNewViewController ()<UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,CommonListDelegate, DatePickerDelegate,WebServiceManagerDelegate, RUCustomerSignatureDelegate>

enum kManageMechanicAddNew_TYPE
{
    kManageMechanicAddNew_HEADER,
    kManageMechanicAddNew_SALUTATION,
    kManageMechanicAddNew_FIRSTNAME,
    kManageMechanicAddNew_LASTNAME,
    kManageMechanicAddNew_MOBNUM,
    kManageMechanicAddNew_EMAIL,
    kManageMechanicAddNew_DOB,
};

#define CELL_HEIGHT 72


#define kUserID             @"UserID"
#define kDSRCode            @"DSRCode"
#define kUserPassword       @"UserPassword"
#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kEmailAddress       @"EmailAddress"
#define kDOB                @"DOB"
#define kDOBMonth           @"DOBMonth"
#define kDOBDay             @"DOBDay"


#pragma mark contact Pref
@property (weak, nonatomic) IBOutlet UILabel *contactPrefLbl;


//colview original height = 131
@property (weak, nonatomic) IBOutlet UICollectionView *colView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *colViewHeight; //to adjust according to number of cells <1 + (count / 2)>
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *colViewLayout; //to adjust cell width size according to screen width

@property Member *member;
@property CASDatePickerView *dateView;

@property NSMutableArray *registrationFormArray; //ui only
@property NSMutableDictionary *regSubmissionDict;  //key as json key, value = cell. retrieve value by using getJson method
@property (weak, nonatomic) IBOutlet UITableView *regFormTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight; //to adjust according number of cells
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;

@property NSMutableArray *contactPrefArray;

@property (weak, nonatomic) IBOutlet UIView *redeemFlagView;
@property (weak, nonatomic) IBOutlet UIButton *btnRedeemFlag;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblRedeemFlag;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *signatureHeight; //0 if not russia
@property (weak, nonatomic) IBOutlet UIView *signatureView;
@property (weak, nonatomic) IBOutlet UILabel *lblSignature;
@property (weak, nonatomic) IBOutlet UIImageView *imgSignature;

@property (weak, nonatomic) IBOutlet UILabel *lblRequiredFields;


@property (weak, nonatomic) IBOutlet UIView *agreeTNC;
@property (weak, nonatomic) IBOutlet UIButton *btnTncCheckbox;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblTnc;

@end

@implementation MechanicAddNewViewController
@synthesize listView;
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupRegistrationForm];

    if (!self.workshopTradeID)    //use own trade ID
    {
        LoginInfo *loginInfo = [mSession loadUserProfile];
        self.workshopTradeID = loginInfo.tradeID;
    }
    
    [self.colView registerNib: [UINib nibWithNibName: @"ContactPreferencesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ContactPrefCell"];
    
    self.contactPrefLbl.hidden=YES;
    self.colView.hidden=YES;
    
    if([[mSession profileInfo] enableCustomerRedemption])
    {
        [self.lblRedeemFlag setFont: FONT_B2];
        self.lblRedeemFlag.text = LOCALIZATION(C_MECH_REDEEMFLAG);  //lokalised 8 Feb
    }
    else
    {
        [self.redeemFlagView removeFromSuperview];
    }
    
    
    NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @"*" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B3, }];
    
    NSMutableAttributedString *requiredFieldsAttrString = [[NSMutableAttributedString alloc] initWithString: LOCALIZATION(C_PROFILE_REQUIREDFIELDS) attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B3, }];   //lokalised 8 Feb
    
    [asteriskAttrString appendAttributedString:  requiredFieldsAttrString];
    
    //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
    self.lblRequiredFields.attributedText = asteriskAttrString;
    
    
    if(![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
    {
        self.signatureHeight.constant = 0;
        [self.agreeTNC removeFromSuperview];
    }
    else
    {
        self.lblSignature.text = LOCALIZATION(C_RUSSIA_CUSTOMERSIGNATURE);  //lokalised 8 Feb
        self.lblTnc.text = LOCALIZATION(C_RU_STEPFOUR_TNC); //lokalised 8 Feb
        [Helper setHyperlinkLabel:self.lblTnc hyperlinkText: LOCALIZATION(C_RU_STEPFOUR_TNCLINK) hyperlinkFont: FONT_B2 bodyText: LOCALIZATION(C_RU_STEPFOUR_TNC) bodyFont:FONT_B2 urlString: @"MechTNC"];  //lokalised 8 Feb
        
//        [Helper addHyperlink:self.lblTnc hyperlinkText: LOCALIZATION(C_RU_STEPFOUR_PRIVACYLINK) hyperlinkFont: FONT_B2 urlString:@"MechPrivacy" bodyText: self.lblTnc.text];
    }
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url absoluteString] isEqualToString: @"MechTNC"])
    {
        NSDictionary *data = GET_WORKSHOP_TNC;
        
        PopupWebviewViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPWEBVIEW];
        
        [popupVc popupInitWithHTMLBody: [data objectForKey: @"TncText"] headerTitle: LOCALIZATION(C_SIDEMENU_TNC) buttonTitle: LOCALIZATION(C_GLOBAL_OK)];  //lokalised 8 Feb
    }
//    else if ([[url absoluteString] isEqualToString: @"MechPrivacy"])
//    {
//        PopupWebviewViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPWEBVIEW];
//        
//        NSString *urlAddress = [NSString stringWithFormat:@"%@%@%@?c=%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_PRIVACY_URL, GET_COUNTRY_CODE];
//        [popupVc popupInitWithURL: urlAddress headerTitle: LOCALIZATION(C_TITLE_PRIVACY) buttonTitle: LOCALIZATION(C_GLOBAL_OK)];
//    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    
    [self setupGoogleAnalytics];
}

-(void) setupGoogleAnalytics
{
    [FIRAnalytics setScreenName:[NSString stringWithFormat: @"%@ / %@", LOCALIZATION_EN(C_TITLE_MANAGEMECHANIC), LOCALIZATION_EN(C_MANAGEMECHANIC_ADDNEW)] screenClass:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    [self setupInterface];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setupRegistrationForm
{
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormTextfieldCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormTextfieldCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationButtonCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormDropdownCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormDropdownCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationHeaderTableViewCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationLabelTextfieldTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationLabelTextfieldTableViewCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationMobileNumberCell" bundle:nil] forCellReuseIdentifier:@"RegistrationMobileNumberCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationTextfieldWithButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextfieldWithButtonCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationTextViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextViewTableViewCell"];
    
    self.regSubmissionDict = [[NSMutableDictionary alloc] init];
    
    self.registrationFormArray = [[NSMutableArray alloc] initWithArray:
                                  
                                  
#pragma mark - Owner Profile Form setup
                                  @[@{@"Title": LOCALIZATION(C_HEADER_PROFILE), //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_HEADER),
                                      },
                                    @{@"Title": LOCALIZATION(C_PROFILE_SALUTATION), //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key": kSalutation,
                                      @"Value": LOCALIZATION(C_PROFILE_SALUTATION), //lokalised 8 Feb
                                      },  //button
                                    @{@"Title": LOCALIZATION(C_PROFILE_FIRSTNAME),  //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kFirstName,
                                      @"Value": @"",
                                      }, //first name
                                    @{@"Title": LOCALIZATION(C_PROFILE_LASTNAME),   //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kLastName,
                                      @"Value": @"",
                                      }, //last name
                                    @{@"Title": LOCALIZATION(C_PROFILE_MOBILENUM),  //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_MOBNUM),
                                      @"Key": kMobileNumber,
                                      @"CountryCode": [mSession convertCountryCodeToCountryName: GET_COUNTRY_CODE],
                                      @"TextfieldEnabled": @(YES),
                                      @"MobileNumber": @"",
                                      },   //Mobile number
                                    @{@"Title": LOCALIZATION(C_PROFILE_EMAILADDRESS),   //lokalised 8 Feb
                                      @"Placeholder": LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER), //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kEmailAddress,
                                      @"Keyboard": @(UIKeyboardTypeEmailAddress),
                                      @"Value": @"",
                                      @"Optional": @(YES),
                                      }, //email
                                    @{@"Title": LOCALIZATION(C_PROFILE_DOB),    //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_BIRTHDATE),
                                      @"Key": kDOB,
                                      @"Value": LOCALIZATION(C_PROFILE_DOB_PLACEHOLDER),    //lokalised 8 Feb
                                      },   //Date of birth
                                    ]
                                  ];
    
    //set tableview height. 4 shorter cells
    self.tableViewHeight.constant = CELL_HEIGHT * [self.registrationFormArray count];
    self.regFormTableView.tableFooterView = [UIView new];
    [self.regFormTableView reloadData];
    
    self.member = [[Member alloc] init];
}

-(void) setupInterface
{
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_DROPDOWN_SALUTATION)    //lokalised 8 Feb
                                                   list: [mSession getSalutations]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
    
    
    dateView = [[CASDatePickerView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_BIRTHDAY) previouslySelected:nil withYearHidden:YES];   //lokalised 8 Feb
    
    dateView.delegate = self;
    
    
    [self.contactPrefLbl setFont: FONT_B1];
    self.contactPrefLbl.text = LOCALIZATION(C_TITLE_CONTACT_PREFERENCE);    //lokalised 8 Feb
    //misc.
    [self.btnRegister.titleLabel setFont: FONT_BUTTON];
    [self.btnRegister setTitle:LOCALIZATION(C_FORM_REGISTERNEWSTAFF) forState:UIControlStateNormal];    //lokalised 8 Feb
    [Helper setCustomFontButtonContentModes: self.btnRegister];
    

    //handle check boxes collection view
    if (!self.contactPrefArray)
        self.contactPrefArray = [[NSMutableArray alloc] init];
//    CGFloat colviewHeight = (COLVIEW_CELLHEIGHT + COLVIEW_LINESPACING) * (1 + ([[mSession getContactPreference] count] / 2));
//    self.colViewHeight.constant = colviewHeight;
    self.colViewHeight.constant = 0;
    
//    CGFloat cellWidth = (self.colView.frame.size.width / 2) - 10; //10 = cell spacing, check IB
//    self.colViewLayout.itemSize = CGSizeMake(cellWidth, 50);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[mSession getContactPreference] count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ContactPreferencesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"ContactPrefCell" forIndexPath: indexPath];
    
    UIButton *checkboxBtn = [cell viewWithTag: 1]; //tagged in storyboard
    checkboxBtn.selected  = YES;
    
    UILabel *contactPref = [cell viewWithTag: 2]; //tagged in storyboard
    contactPref.text = [[mSession getContactPreference] objectAtIndex: [indexPath row]];
    contactPref.font = FONT_B2;
    
    cell.bitValue = [[mSession convertToContactPreferenceKeyCode: contactPref.text] intValue];
    
    if (!self.contactPrefArray)
        self.contactPrefArray = [[NSMutableArray alloc] init];
    [self.contactPrefArray addObject: cell];
    
    return cell;
}

- (IBAction)checkBoxSelected:(UIButton *)sender {
    if ([sender isSelected])
        sender.selected = NO;
    else
        sender.selected = YES;
}

#pragma mark - tableview delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.registrationFormArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *regFieldData = [self.registrationFormArray objectAtIndex: indexPath.row];
    switch ([[regFieldData objectForKey: @"Type"] intValue])
    {
        case kREGISTRATION_TEXTFIELD:
        {
            RegistrationFormTextfieldCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormTextfieldCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [textfieldCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [textfieldCell hideTopLabel: NO];
            
            if ([regFieldData objectForKey: @"Value"])
                [textfieldCell setTextfieldText: [regFieldData objectForKey: @"Value"]];
            if ([regFieldData objectForKey: @"Placeholder"])
                [textfieldCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_DROPDOWN:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [dropdownCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [dropdownCell hideTopLabel: NO];
            dropdownCell.button.tag = indexPath.row;
            
            [dropdownCell.button addTarget: self action: @selector(dropdownPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            //set initial value
            if ([regFieldData objectForKey: @"Value"])
                [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            
            //keep a reference of cell to submit json value later
            [self.regSubmissionDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        case kREGISTRATION_TEXTFIELDWITHBUTTON:
        {
            RegistrationTextfieldWithButtonCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationTextfieldWithButtonCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            if ([regFieldData objectForKey: @"TextfieldEnabled"])
                [textfieldCell setTextfieldEnabled: [[regFieldData objectForKey: @"TextfieldEnabled"] boolValue]];
            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            [textfieldCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [textfieldCell hideTopLabel: NO];

            
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_BUTTON:
        {
            RegistrationButtonCell *buttonCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationButtonCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [buttonCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [buttonCell setTitle: [regFieldData objectForKey: @"Title"]];
            [buttonCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [buttonCell hideTopLabel: NO];
            
            [self.regSubmissionDict setObject: buttonCell forKey: [regFieldData objectForKey: @"Key"]];
            return buttonCell;
        }
            break;
        case kREGISTRATION_MOBNUM:
        {
            RegistrationMobileNumberCell *mobNumCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationMobileNumberCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [mobNumCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            if ([regFieldData objectForKey: @"TextfieldEnabled"])
                [mobNumCell setTextfieldEnabled: [[regFieldData objectForKey: @"TextfieldEnabled"] boolValue]];
            [mobNumCell setMobileCodeEnabled: NO];
            [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [mobNumCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [mobNumCell hideTopLabel: NO];
            
            [mobNumCell setCountryCode: [regFieldData objectForKey: @"CountryCode"]];
            [mobNumCell setMobileNumber: [regFieldData objectForKey: @"MobileNumber"]];
            
            [self.regSubmissionDict setObject: mobNumCell forKey: [regFieldData objectForKey: @"Key"]];
            return mobNumCell;
        }
            break;
        case kREGISTRATION_LABELWITHTEXTFIELD:
        {
            RegistrationLabelTextfieldTableViewCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationLabelTextfieldTableViewCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [textfieldCell setLeftTitle: [regFieldData objectForKey: @"Title"]];
            [textfieldCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
//            [textfieldCell hideTopLabel: YES];
            
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_BIRTHDATE:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            //Salutation
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            [dropdownCell hideTopLabel: NO];
            dropdownCell.button.tag = indexPath.row;
            
            [dropdownCell.button addTarget: self action: @selector(birthdayPressed:) forControlEvents:UIControlEventTouchUpInside];
            [self.regSubmissionDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        case kREGISTRATION_HEADER:
        {
            RegistrationHeaderTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationHeaderTableViewCell"];
            [headerCell setTitle: [regFieldData objectForKey: @"Title"]];
            return headerCell;
        }
            break;
        default:
            NSLog(@"UNKNOWN CELL TYPE");
            return [UITableViewCell new];
            break;
    }
    return [UITableViewCell new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}
-(void) dropdownPressed:(UIButton *)sender
{
    [self.view endEditing: YES];
    listView.tag = sender.tag;
    
    switch (listView.tag)
    {
        case kManageMechanicAddNew_SALUTATION:
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO]; //lokalised 8 Feb
            break;
        default:
            break;
    }
    [self popUpView];
}

- (IBAction)birthdayPressed:(UIButton *)sender {
    [self.view endEditing: YES];
    
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}

- (void)selectedDate:(NSDate *)selectedDate
{
    //do smething
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //   for API
    [formatter setDateFormat:@"dd"];
    self.member.dobDay = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"MM"];
    self.member.dobMonth = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"dd MMMM"];
    
    [formatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    NSString *selectedDateString = [formatter stringFromDate:selectedDate];
    
    
    RegistrationFormDropdownCell *cell = [self.regSubmissionDict objectForKey: kDOB];
    [cell setSelectionTitle: selectedDateString];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        RegistrationFormDropdownCell *cell;
        switch (listView.tag)
        {
            case kManageMechanicAddNew_SALUTATION:
                cell = [self.regSubmissionDict objectForKey: kSalutation];
                break;
            default:
                return;
                break;
        }
        [cell.button setTitle:selection.firstObject forState:UIControlStateNormal];
    }
}
-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

- (IBAction)registerPressed:(id)sender {
    
    self.member.firstName =   [[self.regSubmissionDict objectForKey: kFirstName] getJsonValue];
    self.member.lastName =    [[self.regSubmissionDict objectForKey: kLastName] getJsonValue];
    self.member.mobileNumber = [[self.regSubmissionDict objectForKey: kMobileNumber] getMobileNumberJsonValue];
    self.member.emailAddress = [[self.regSubmissionDict objectForKey: kEmailAddress] getJsonValue];
    self.member.countryCode  = GET_COUNTRY_CODE;
    self.member.salutation   = [[mSession convertToSalutationKeyCode:[[self.regSubmissionDict objectForKey: kSalutation] getJsonValue]] intValue];
    
    
    NSString *contactPrefString = @"";
    for (ContactPreferencesCollectionViewCell* cell in self.contactPrefArray)
    {
        if ([cell.checkboxBtn isSelected])
        {
            if (contactPrefString.length == 0)
                contactPrefString = @(cell.bitValue).stringValue;
            else
                contactPrefString = [contactPrefString stringByAppendingString: [NSString stringWithFormat: @",%@", @(cell.bitValue).stringValue]];
        }
    }
    self.member.contactPreference    = contactPrefString;
    

    NSMutableDictionary *registerParams = [[NSMutableDictionary alloc] init];
    
    [registerParams setObject:self.workshopTradeID forKey:@"WorkshopTradeID"];
    [registerParams setObject: @(self.member.salutation) forKey:@"Salutation"];
    [registerParams setObject:self.member.firstName forKey:@"FirstName"];
    [registerParams setObject:self.member.lastName forKey:@"LastName"];
    [registerParams setObject:self.member.mobileNumber forKey:@"MobileNumber"];
    [registerParams setObject:self.member.emailAddress forKey: @"EmailAddress"];
    [registerParams setObject:contactPrefString forKey: @"ContactPreferences"];
    [registerParams setObject:self.member.dobMonth forKey: @"DOBMonth"];
    [registerParams setObject:self.member.dobDay forKey:@"DOBDay"];
    [registerParams setObject:@(self.btnRedeemFlag.selected) forKey:@"EnableCustomerRedemption"];
    if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND]) {
        //    LOOKUP VALUE
        //    StaffAccountType =({KeyCode = 1;KeyValue = Mechanic;},
        //                       {KeyCode = 2;KeyValue = "Forecourt Attendant";});
        [registerParams setObject:@(1) forKey:@"StaffType"];
        
    }
    
    if (self.imgSignature.image)
    {
        //set image
        [registerParams setObject: [UIImageJPEGRepresentation(self.imgSignature.image, 0.5f) base64EncodedStringWithOptions:0]  forKey: @"DigitalSignature"];
    }
    
    if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA]) //only check for term condition if adding russia mech
    {
        [registerParams setObject:@(self.btnTncCheckbox.selected).stringValue forKey: @"TermCondition"];
    }

    [[WebServiceManager sharedInstance] addMechanic:registerParams vc:self];
    
}

- (IBAction)checkboxPressed:(UIButton *)sender
{
    sender.selected = !sender.selected;
}


- (IBAction)signaturePressed:(id)sender
{
    [mSession pushRUCustomerSignatureWithDelegate: self.parentViewController.parentViewController delegate: self];
}

-(void)didFinishSignature:(UIImage *)image
{
    self.imgSignature.image = image;
}


-(void)processCompleted:(WebServiceResponse *)response
{
    [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
        [self setupRegistrationForm];
        [self.delegate didRegisterSuccess];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
