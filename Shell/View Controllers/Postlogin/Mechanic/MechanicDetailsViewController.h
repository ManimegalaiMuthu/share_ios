//
//  MechanicDetailsViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 30/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "MechanicAddNewViewController.h"

@interface MechanicDetailsViewController : MechanicAddNewViewController
@property NSString *tradeID;
@property NSString *workshopTradeID;
@property NSString *registrationID;

@end
