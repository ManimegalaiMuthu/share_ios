//
//  MechanicAddNewViewController.h
//  Shell
//
//  Created by Admin on 29/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
@protocol MechanicAddNewDelegate
-(void)didRegisterSuccess;
@end
@interface MechanicAddNewViewController :BaseVC
@property NSString *workshopTradeID;
@property id<MechanicAddNewDelegate> delegate;
@end
