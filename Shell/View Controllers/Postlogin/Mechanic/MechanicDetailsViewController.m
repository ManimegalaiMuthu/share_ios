//
//  MechanicDetailsViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 30/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "MechanicDetailsViewController.h"
#import "CASDatePickerView.h"

@interface MechanicDetailsViewController ()<UITableViewDataSource,UITableViewDelegate,CommonListDelegate, DatePickerDelegate, UITextViewDelegate,WebServiceManagerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;

@property Member *member;
@property CASDatePickerView *dateView;
@property (weak, nonatomic) IBOutlet UIButton *btnReject;
@property (weak, nonatomic) IBOutlet UIButton *btnApprove;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeightLayout;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


@property (weak, nonatomic) IBOutlet UILabel *lblPlaceHolder;
@property (weak, nonatomic) IBOutlet UITextView *remarkTextView;
@property NSMutableArray *listFormArray; //ui only
@property NSMutableDictionary *listDict;  //key as json key, value = cell. retrieve value by using getJson method


@property (weak, nonatomic) IBOutlet UIView *redeemFlagView;
@property (weak, nonatomic) IBOutlet UIButton *btnRedeemFlag;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblRedeemFlag;



//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *signatureHeight; //0 if not russia
//@property (weak, nonatomic) IBOutlet UIView *signatureView;
//@property (weak, nonatomic) IBOutlet UILabel *lblSignature;
//@property (weak, nonatomic) IBOutlet UIImageView *imgSignature;

@property (weak, nonatomic) IBOutlet UILabel *lblRequiredFields;

@end

enum kManageMechanicDetails_TYPE
{
    kManageMechanicDetails_HEADER,
    kManageMechanicDetails_SALUTATION,
    kManageMechanicDetails_FIRSTNAME,
    kManageMechanicDetails_LASTNAME,
    kManageMechanicDetails_MOBNUM,
    kManageMechanicDetails_EMAIL,
    kManageMechanicDetails_DOB,
};

#define CELL_HEIGHT 72


#define kUserID             @"UserID"
#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kEmailAddress       @"EmailAddress"
#define kDOB                @"DOB"
#define kDOBMonth           @"DOBMonth"
#define kDOBDay             @"DOBDay"

@implementation MechanicDetailsViewController
@synthesize listView;
@synthesize dateView;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_MECHANICDETAILS) subtitle: @""];  //lokalised 8 Feb
    
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
}

//- (IBAction)signaturePressed:(id)sender
//{
//    [mSession pushRUCustomerSignatureWithDelegate: self delegate: self];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void)viewWillAppear:(BOOL)animated{
//    [super viewWillAppear: animated];
//}

//override from MechanicAddNewViewController
-(void) setupGoogleAnalytics
{
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_MECHANICDETAILS) screenClass:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    [self setupInterface];
}

-(void) setupRegistrationForm
{
    if (!self.member) //avoid superclass viewdidload
        return;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationFormTextfieldCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormTextfieldCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationButtonCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationFormDropdownCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormDropdownCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationHeaderTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationLabelTextfieldTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationLabelTextfieldTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationMobileNumberCell" bundle:nil] forCellReuseIdentifier:@"RegistrationMobileNumberCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationTextfieldWithButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextfieldWithButtonCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationTextViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextViewTableViewCell"];
    
    self.listDict = [[NSMutableDictionary alloc] init];
    
    self.listFormArray = [[NSMutableArray alloc] initWithArray:
                          
                                  @[
                                    @{@"Title": LOCALIZATION(C_PROFILE_STAFF_HEADER),   //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_HEADER),
                                      },
                                    @{@"Title": LOCALIZATION(C_PROFILE_SALUTATION), //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key": kSalutation,
                                      @"Value": [mSession convertToSalutation: self.member.salutation],
                                      },  //button
                                    @{@"Title": LOCALIZATION(C_PROFILE_FIRSTNAME),  //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kFirstName,
                                      @"Value": self.member.firstName,
                                      }, //first name
                                    @{@"Title": LOCALIZATION(C_PROFILE_LASTNAME),   //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kLastName,
                                      @"Value": self.member.lastName,
                                      }, //last name
                                    @{@"Title": LOCALIZATION(C_PROFILE_MOBILENUM),  //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_MOBNUM),
                                      @"Key": kMobileNumber,
                                      @"CountryCode": [mSession convertCountryCodeToCountryName: GET_COUNTRY_CODE],
                                      @"TextfieldEnabled": @(YES),
                                      @"MobileNumber": self.member.mobileNumber,
                                      },   //Mobile number
                                    @{@"Title": LOCALIZATION(C_PROFILE_EMAILADDRESS),   //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kEmailAddress,
                                      @"Keyboard": @(UIKeyboardTypeEmailAddress),
                                      @"Value": self.member.emailAddress,
                                      @"Optional":@(YES),
                                      }, //email
                                    @{@"Title": LOCALIZATION(C_PROFILE_DOB),    //lokalised 8 Feb
                                      @"Type": @(kREGISTRATION_BIRTHDATE),
                                      @"Key": kDOB,
                                      @"Value": [self convertDateForDisplay: self.member.dobMonth day:self.member.dobDay],
                                      },   //Date of birth
                                    ]
                                  ];
    
    
    //set tableview height. 4 shorter cells
    self.tableHeightLayout.constant = CELL_HEIGHT * [self.listFormArray count];
    self.tableView.tableFooterView = [UIView new];
    [self.tableView reloadData];
}

-(void) setupInterface
{
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_DROPDOWN_SALUTATION)    //lokalised 8 Feb
                                                   list: [mSession getSalutations]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
    
    
    dateView = [[CASDatePickerView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_BIRTHDAY) previouslySelected:nil withYearHidden:YES];   //lokalised 8 Feb
    
    dateView.delegate = self;
    
    
    [self.lblPlaceHolder setFont: FONT_B1];
    

    if(![self.tradeID isEqualToString:@""]) //approved mechanic
    {
        [self.btnApprove.titleLabel setFont: FONT_BUTTON];
        [Helper setCustomFontButtonContentModes: self.btnApprove];
        [self.btnApprove setTitle: LOCALIZATION(C_MECHANICDETAILS_DEACTIVATE) forState:UIControlStateNormal]; //swap approve to normal  //lokalised 8 Feb
        
        if ([[mSession profileInfo] enableCustomerRedemption])
        {
            [self.btnReject.titleLabel setFont: FONT_BUTTON];
            [self.btnReject setTitle: LOCALIZATION(C_MECH_UPDATEDETAILS) forState:UIControlStateNormal];    //lokalised 8 Feb
        }
        else
            self.btnReject.hidden = YES;
        
        
        [self.remarkTextView removeFromSuperview];
        [self.lblPlaceHolder removeFromSuperview];
        //self.scrollView.scrollEnabled= NO;
        [[WebServiceManager sharedInstance]fetchMechanicDetail:self.tradeID vc:self];
    }else{
        [self.btnApprove setTitle: LOCALIZATION(C_MECHANICDETAILS_APPROVE) forState:UIControlStateNormal];  //lokalised 8 Feb
        [self.btnReject setTitle: LOCALIZATION(C_MECHANICDETAILS_REJECT) forState:UIControlStateNormal];    //lokalised 8 Feb
        self.lblPlaceHolder.text = LOCALIZATION(C_MECHANICDETAILS_REMARKSPLACEHOLDER);  //lokalised 8 Feb
        
        //misc.
        [self.remarkTextView setFont: FONT_B1];
        [self.btnReject.titleLabel setFont: FONT_BUTTON];
        [Helper setCustomFontButtonContentModes: self.btnReject];
        
        [self.btnApprove.titleLabel setFont: FONT_BUTTON];
        [Helper setCustomFontButtonContentModes: self.btnApprove];
        [[WebServiceManager sharedInstance]fetchRegMechanicDetail:self.registrationID vc:self];
    }
    if([[mSession profileInfo] enableCustomerRedemption])
    {
        [self.lblRedeemFlag setFont: FONT_B2];
        self.lblRedeemFlag.text = LOCALIZATION(C_MECH_REDEEMFLAG);  //lokalised 8 Feb
    }
    else
    {
        [self.redeemFlagView removeFromSuperview];
    }
    [self setupGoogleAnalytics];
}

#pragma mark - tableview delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.listFormArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *regFieldData = [self.listFormArray objectAtIndex: indexPath.row];
    switch ([[regFieldData objectForKey: @"Type"] intValue])
    {
        case kREGISTRATION_TEXTFIELD:
        {
            RegistrationFormTextfieldCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormTextfieldCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [textfieldCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [textfieldCell hideTopLabel: NO];
            if ([regFieldData objectForKey: @"Value"])
                [textfieldCell setTextfieldText: [regFieldData objectForKey: @"Value"]];
            
            [textfieldCell setUserInteractionEnabled:NO];
            [self.listDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            
            return textfieldCell;
        }
            break;
        case kREGISTRATION_DROPDOWN:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [dropdownCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [dropdownCell hideTopLabel: NO];
            
            
            //set initial value
            if ([regFieldData objectForKey: @"Value"])
                [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            
            //keep a reference of cell to submit json value later
            [dropdownCell setUserInteractionEnabled:NO];
            [self.listDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        case kREGISTRATION_TEXTFIELDWITHBUTTON:
        {
            RegistrationTextfieldWithButtonCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationTextfieldWithButtonCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            if ([regFieldData objectForKey: @"TextfieldEnabled"])
                [textfieldCell setTextfieldEnabled: [[regFieldData objectForKey: @"TextfieldEnabled"] boolValue]];
            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            [textfieldCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [textfieldCell hideTopLabel: NO];
            
            [textfieldCell setUserInteractionEnabled:NO];
            [self.listDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_BUTTON:
        {
            RegistrationButtonCell *buttonCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationButtonCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [buttonCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [buttonCell setTitle: [regFieldData objectForKey: @"Title"]];
            [buttonCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [buttonCell hideTopLabel: NO];
            
            [buttonCell setUserInteractionEnabled:NO];
            [self.listDict setObject: buttonCell forKey: [regFieldData objectForKey: @"Key"]];
            return buttonCell;
        }
            break;
        case kREGISTRATION_MOBNUM:
        {
            RegistrationMobileNumberCell *mobNumCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationMobileNumberCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [mobNumCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            if ([regFieldData objectForKey: @"TextfieldEnabled"])
                [mobNumCell setTextfieldEnabled: [[regFieldData objectForKey: @"TextfieldEnabled"] boolValue]];
            [mobNumCell setMobileCodeEnabled: NO];
            [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [mobNumCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [mobNumCell hideTopLabel: NO];

            [mobNumCell setCountryCode: [regFieldData objectForKey: @"CountryCode"]];
            [mobNumCell setMobileNumber: [regFieldData objectForKey: @"MobileNumber"]];
            
            [mobNumCell setTextfieldEnabled:NO];
            [self.listDict setObject: mobNumCell forKey: [regFieldData objectForKey: @"Key"]];
            return mobNumCell;
        }
            break;
//        case kREGISTRATION_LABELWITHTEXTFIELD:
//        {
//            RegistrationLabelTextfieldTableViewCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationLabelTextfieldTableViewCell"];
//            
//            if ([regFieldData objectForKey: @"Keyboard"])
//                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
//            if ([regFieldData objectForKey: @"SecureEntry"])
//                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
//            if ([regFieldData objectForKey: @"Enabled"])
//                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
//            
//            [textfieldCell setLeftTitle: [regFieldData objectForKey: @"Title"]];
//            [textfieldCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
//            [textfieldCell hideTopLabel: NO];
//            
//            [textfieldCell setUserInteractionEnabled:NO];
//            [self.listDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
//            return textfieldCell;
//        }
//            break;
        case kREGISTRATION_BIRTHDATE:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            //Salutation
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            [dropdownCell hideTopLabel: NO];
            dropdownCell.button.tag = indexPath.row;
            
            [dropdownCell.button addTarget: self action: @selector(birthdayPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            [dropdownCell setUserInteractionEnabled:NO];
            [self.listDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        case kREGISTRATION_HEADER:
        {
            RegistrationHeaderTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationHeaderTableViewCell"];
            [headerCell setTitle: [regFieldData objectForKey: @"Title"]];
            return headerCell;
        }
            break;
        default:
            NSLog(@"UNKNOWN CELL TYPE");
            return [UITableViewCell new];
            break;
    }
    return [UITableViewCell new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

-(void) dropdownPressed:(UIButton *)sender
{
    [self.view endEditing: YES];
    listView.tag = sender.tag;
    
    switch (listView.tag)
    {
        case kManageMechanicDetails_SALUTATION:
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO]; //lokalised 8 Feb
            break;
        default:
            break;
    }
    [self popUpView];
}

- (IBAction)birthdayPressed:(UIButton *)sender {
    [self.view endEditing: YES];
    
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}

- (void)selectedDate:(NSDate *)selectedDate
{
    //do smething
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //   for API
    [formatter setDateFormat:@"dd"];
    self.member.dobDay = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"MM"];
    self.member.dobMonth = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"dd MMMM"];
    
    [formatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    NSString *selectedDateString = [formatter stringFromDate:selectedDate];
    
    
    RegistrationFormDropdownCell *cell = [self.listDict objectForKey: kDOB];
    [cell setSelectionTitle: selectedDateString];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        RegistrationFormDropdownCell *cell;
        switch (listView.tag)
        {
            case kManageMechanicDetails_SALUTATION:
                cell = [self.listDict objectForKey: kSalutation];
                break;
            default:
                return;
                break;
        }
        [cell.button setTitle:selection.firstObject forState:UIControlStateNormal];
    }
}
-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}


- (IBAction)backBtnPressed:(id)sender

{
    [self popSelf];
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    self.lblPlaceHolder.hidden = YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView.text.length > 0)
        self.lblPlaceHolder.hidden = YES;
    else
        self.lblPlaceHolder.hidden = NO;
}

-(NSString*)checkForNullInDict:(NSString*)stringValue
{
    return ([stringValue isKindOfClass: [NSNull class]]?@"":stringValue);
}

-(NSString*)checkForBirthdayInput:(NSString*)monthValue day:(NSString*)dayValue
{
    if([monthValue isKindOfClass: [NSNull class]])
        return LOCALIZATION(C_PROFILE_DOB); //lokalised 8 Feb
    else
        return [self convertDateForDisplay:monthValue day:dayValue];
}

-(NSString *) convertDateForDisplay:(NSString *)monthString day:(NSString *) dayString
{
    if ([monthString isEqualToString: BLANK] || [dayString isEqualToString: BLANK])
        return LOCALIZATION(C_PROFILE_DOB_PLACEHOLDER); //lokalised 8 Feb
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    [dateFormat setDateFormat:@"MMdd"];
    NSDate *date = [dateFormat dateFromString: [NSString stringWithFormat: @"%@%@", monthString, dayString]];
    
    // Convert date object to desired output format
    [dateFormat setDateFormat:@"dd MMMM"];
    return [dateFormat stringFromDate:date];
}


- (IBAction)checkBoxSelected:(UIButton *)sender {
    
    //update UI
    if ([sender isSelected])
        sender.selected = NO;
    else
        sender.selected = YES;
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_UPDATEREGMECHANIC:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                [self backBtnPressed: nil];
            }];
        }
            break;
        case kWEBSERVICE_UPDATEMECHANIC:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                [self backBtnPressed: nil];
            }];
        }
            break;
        case kWEBSERVICE_FETCHMECHANIC:
        {
            NSDictionary *respData = [response getGenericResponse];
            self.member = [[Member alloc] initWithData: respData];
            [self setupRegistrationForm];
            
            if (![respData objectForKey: @"EnableCustomerRedemption"] || [[respData objectForKey: @"EnableCustomerRedemption"] isKindOfClass: [NSNull class]])
                [self.btnRedeemFlag setSelected: NO];
            else
                [self.btnRedeemFlag setSelected: [[respData objectForKey: @"EnableCustomerRedemption"] boolValue]];
            [self.tableView reloadData];
            
//            if (![[respData objectForKey: @"DigitalSignature"] isKindOfClass: [NSNull class]] &&
//                [GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
//            {
//                NSString *str = [respData objectForKey: @"DigitalSignature"];
//                NSData *data = [[NSData alloc]  initWithBase64EncodedString: [respData objectForKey: @"DigitalSignature"] options: NSDataBase64DecodingIgnoreUnknownCharacters];
//                self.imgSignature.image = [[UIImage alloc] initWithData: data];
//            }
        }
            break;
        case kWEBSERVICE_FETCHREGMECHANIC:
        {
            NSDictionary *respData = [response getGenericResponse];
            self.member = [[Member alloc] initWithData: respData];
            [self setupRegistrationForm];
            if (![respData objectForKey: @"EnableCustomerRedemption"] || [[respData objectForKey: @"EnableCustomerRedemption"] isKindOfClass: [NSNull class]])
                [self.btnRedeemFlag setSelected: NO];
            else
                [self.btnRedeemFlag setSelected: [[respData objectForKey: @"EnableCustomerRedemption"] boolValue]];
            [self.tableView reloadData];
            
//            if (![[respData objectForKey: @"DigitalSignature"] isKindOfClass: [NSNull class]])
//            {
//                NSString *str = [respData objectForKey: @"DigitalSignature"];
//                NSData *data = [[NSData alloc]  initWithBase64EncodedString: [respData objectForKey: @"DigitalSignature"] options: NSDataBase64DecodingIgnoreUnknownCharacters];
//                self.imgSignature.image = [[UIImage alloc] initWithData: data];
//            }
        }
            break;
        default:
            break;
    }
}

- (IBAction)rejectPressed:(id)sender {
    
    NSMutableDictionary *registerParams = [[NSMutableDictionary alloc] init];
    
    
    [registerParams setObject:@(self.member.salutation) forKey:@"Salutation"];
    [registerParams setObject: self.member.firstName  forKey:@"FirstName"];
    [registerParams setObject: self.member.lastName forKey:@"LastName"];
    [registerParams setObject: self.workshopTradeID forKey:@"WorkshopTradeID"];
    [registerParams setObject: self.member.mobileNumber forKey:@"MobileNumber"];
    [registerParams setObject: self.member.emailAddress forKey:@"EmailAddress"];
    [registerParams setObject: self.member.contactPreference forKey:@"ContactPreferences"];
    [registerParams setObject: self.member.dobMonth forKey:@"DOBMonth"];
    [registerParams setObject: self.member.dobDay forKey:@"DOBDay"];
   
    [registerParams setObject: self.btnRedeemFlag.selected ? @"1" : @"0" forKey:@"EnableCustomerRedemption"];
    [registerParams setObject: @(1) forKey:@"TermCondition"];
    
//    if (self.imgSignature.image)
//    {
//        //set image
//        [registerParams setObject: [UIImageJPEGRepresentation(self.imgSignature.image, 0.5f) base64EncodedStringWithOptions:0]  forKey: @"DigitalSignature"];
//    }
    
    
    if(![self.tradeID isEqualToString:@""]) //update button (for already-approved mechanic)
    {
         [registerParams setObject: self.tradeID forKey:@"TradeID"];
        [registerParams setObject: @(1) forKey:@"RegistrationStatus"];
        if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND]) {
            //    LOOKUP VALUE
            //    StaffAccountType =({KeyCode = 1;KeyValue = Mechanic;},
            //                       {KeyCode = 2;KeyValue = "Forecourt Attendant";});
            [registerParams setObject:@(1) forKey:@"StaffType"];

        }
        [[WebServiceManager sharedInstance] updateMechanic: registerParams vc:self];
    }
    else
    {   //reject for (non-approved mechanics)
        
        NSMutableDictionary *registerParams = [[NSMutableDictionary alloc] init];
        
        [registerParams setObject: GET_COUNTRY_CODE forKey:COUNTRYCODE_KEY];
        [registerParams setObject:@(1) forKey:@"TermCondition"];

        [registerParams setObject: @"" forKey:@"TradeID"];
        [registerParams setObject: self.registrationID forKey:@"RegistrationID"];
        [registerParams setObject: @(4) forKey:@"RegistrationStatus"];
        [registerParams setObject: self.remarkTextView.text forKey:@"Remarks"];
        [registerParams setObject: self.btnRedeemFlag.selected ? @"1" : @"0" forKey:@"EnableCustomerRedemption"];
        
        [[WebServiceManager sharedInstance] updateMechanicRegistration: registerParams vc:self];
    }
}

- (IBAction)approvePressed:(id)sender {
    NSMutableDictionary *registerParams = [[NSMutableDictionary alloc] init];
    
    
    [registerParams setObject:@(self.member.salutation) forKey:@"Salutation"];
    [registerParams setObject: self.member.firstName  forKey:@"FirstName"];
    [registerParams setObject: self.member.lastName forKey:@"LastName"];
    [registerParams setObject: self.workshopTradeID forKey:@"WorkshopTradeID"];
    [registerParams setObject: self.member.mobileNumber forKey:@"MobileNumber"];
    [registerParams setObject: self.member.emailAddress forKey:@"EmailAddress"];
    [registerParams setObject: self.member.contactPreference forKey:@"ContactPreferences"];
    [registerParams setObject: self.member.dobMonth forKey:@"DOBMonth"];
    [registerParams setObject: self.member.dobDay forKey:@"DOBDay"];
    
    [registerParams setObject: self.btnRedeemFlag.selected ? @"1" : @"0" forKey:@"EnableCustomerRedemption"];
    [registerParams setObject: @(1) forKey:@"TermCondition"];
    
//    if (self.imgSignature.image)
//    {
//        //set image
//        [registerParams setObject: [UIImageJPEGRepresentation(self.imgSignature.image, 0.5f) base64EncodedStringWithOptions:0]  forKey: @"DigitalSignature"];
//    }
    
    if(![self.tradeID isEqualToString:@""]) //deactivate button (for already-approved mechanic)
    {
        [registerParams setObject: self.tradeID forKey:@"TradeID"];
        [registerParams setObject: @(4) forKey:@"RegistrationStatus"];
        
        if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND]) {
            //    LOOKUP VALUE
            //    StaffAccountType =({KeyCode = 1;KeyValue = Mechanic;},
            //                       {KeyCode = 2;KeyValue = "Forecourt Attendant";});
            [registerParams setObject:@(1) forKey:@"StaffType"];
            
        }
        
        [[WebServiceManager sharedInstance] updateMechanic: registerParams vc:self];
    }
    else
    {   //approve
        [registerParams setObject: GET_COUNTRY_CODE forKey:COUNTRYCODE_KEY];
        [registerParams setObject:@(1) forKey:@"TermCondition"];
        
        [registerParams setObject: @"" forKey:@"TradeID"];
        [registerParams setObject: self.registrationID forKey:@"RegistrationID"];
        [registerParams setObject: @(2) forKey:@"RegistrationStatus"];
        [registerParams setObject: self.remarkTextView.text forKey:@"Remarks"];
        [registerParams setObject: self.btnRedeemFlag.selected ? @"1" : @"0" forKey:@"EnableCustomerRedemption"];
 

        [[WebServiceManager sharedInstance] updateMechanicRegistration: registerParams vc:self];
    
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
