//
//  ManageMechanicViewController.h
//  Shell
//
//  Created by Admin on 29/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDVTabBarController.h"
#import "Helper.h"

@interface ManageMechanicViewController : RDVTabBarController
@property NSString *tradeID;
-(void) initWithHTMLBody: (NSString *) htmlBody headerTitle: (NSString *)headerTitle buttonTitle: (NSString *) buttonTitle;
@end
