//
//  ManageMechanicViewController.m
//  Shell
//
//  Created by Admin on 29/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//
#import "RDVTabBarItem.h"
#import "INMechanicAddNewViewController.h"
#import "ManageMechanicViewController.h"
#import "MechanicListViewController.h"
#import "MechanicAddNewViewController.h"
#import "AddNewStaffViewController.h"
#import "StaffAdd_NewViewController.h"

#import "AddNewMechanic_NewViewController.h"
@interface ManageMechanicViewController () <MechanicAddNewDelegate, AddNewMechanic_NewDelegate>

{
    MechanicListViewController *listViewController;
    MechanicAddNewViewController *addNewViewController;
    AddNewStaffViewController *addNewStaffViewController;
    StaffAdd_NewViewController *staffAdd_NewViewController;
    
    INMechanicAddNewViewController *inAddNewStaffViewController;
    AddNewMechanic_NewViewController *addNewMechanic_NewViewController;
}
@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@end

@implementation ManageMechanicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInterface];
    // Do any additional setup after loading the view.
}

- (void) setupInterface
{
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
        [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_MANAGEMECHANIC) subtitle: @"" size:15 subtitleSize:0];   //lokalised 8 Feb
    else
        [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_MANAGEMECHANIC) subtitle: @""];  //lokalised 8 Feb
    
    if ([self.navigationController.childViewControllers count] > 1)
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    
    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_TITLE_MANAGEMECHANIC),  LOCALIZATION(C_REWARDS_BACK)];   //lokalised 8 Feb
    
    listViewController  = [STORYBOARD_MANAGEMECHANIC instantiateViewControllerWithIdentifier: VIEW_MECHANICLIST];
    UINavigationController *mechanListNav = [[UINavigationController alloc] initWithRootViewController: listViewController];
    [mechanListNav setNavigationBarHidden: YES];
    listViewController.tradeID = self.tradeID;
    [listViewController view];
    
    addNewViewController  = [STORYBOARD_MANAGEMECHANIC instantiateViewControllerWithIdentifier: VIEW_MECHANICADDNEW];
    UINavigationController *mechanAddNewNav = [[UINavigationController alloc] initWithRootViewController: addNewViewController];
    addNewViewController.workshopTradeID = self.tradeID;
    addNewViewController.delegate = self;
    [mechanAddNewNav setNavigationBarHidden: YES];
    [addNewViewController view];
    
    addNewStaffViewController  = [STORYBOARD_THRetail instantiateViewControllerWithIdentifier: VIEW_ADDSTAFF];
    UINavigationController *addNewStaffNav = [[UINavigationController alloc] initWithRootViewController: addNewStaffViewController];
    addNewStaffViewController.workshopTradeID = self.tradeID;
    [addNewStaffNav setNavigationBarHidden: YES];
    [addNewStaffViewController view];
    
    staffAdd_NewViewController = [STORYBOARD_MANAGEMECHANIC instantiateViewControllerWithIdentifier: VIEW_MANAGE_STAFF];
    UINavigationController *addStaff_NewNav = [[UINavigationController alloc] initWithRootViewController: staffAdd_NewViewController];
    staffAdd_NewViewController.workshopTradeID = self.tradeID;
    staffAdd_NewViewController.delegate = self;
    [addStaff_NewNav setNavigationBarHidden: YES];
    [staffAdd_NewViewController view];
    
    if(IS_COUNTRY(COUNTRYCODE_INDIA) && GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
    {
        inAddNewStaffViewController = [STORYBOARD_MANAGEWORKSHOP instantiateViewControllerWithIdentifier: VIEW_INMECHANIC_ADDNEW];
        UINavigationController *inAddNewStaffNav = [[UINavigationController alloc] initWithRootViewController:inAddNewStaffViewController];
        [inAddNewStaffNav setNavigationBarHidden: YES];
        [inAddNewStaffViewController view];

        [self setViewControllers:@[mechanListNav,
                                       inAddNewStaffNav
                                       ]];
    }
    else if ([[mSession profileInfo]isManageStaffOpt2] && [GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND])
    {
        [self setViewControllers: @[mechanListNav,
                                        addNewStaffNav
                                        ]];
    }
    else //if (IS_COUNTRY(COUNTRYCODE_INDONESIA))
    {
        addNewMechanic_NewViewController  = [STORYBOARD_MANAGEMECHANIC instantiateViewControllerWithIdentifier: VIEW_ADDNEWMECHANIC_NEW];
        UINavigationController *mechanAddNewNav = [[UINavigationController alloc] initWithRootViewController: addNewMechanic_NewViewController];
        addNewMechanic_NewViewController.workshopTradeID = self.tradeID;
        addNewMechanic_NewViewController.delegate = self;
        [mechanAddNewNav setNavigationBarHidden: YES];
        [addNewMechanic_NewViewController view];
        
        [self setViewControllers:@[mechanListNav,
                                   mechanAddNewNav
                                   ]];
    }
//    else
//    {
//        [self setViewControllers:@[mechanListNav,
//                                   addStaff_NewNav
//                                   ]];
//    }
    
    //same VC but different data.
    //selecttab function have been tweaked locally for this case
//    if ([[mSession profileInfo]isManageStaffOpt2] && [GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND]){
//        [self setViewControllers: @[mechanListNav,
//                                    addNewStaffNav
//                                    ]];
//    } else {
//        [self setViewControllers: @[mechanListNav,
//                                    mechanAddNewNav
//                                    ]];
//    }
    

    NSArray *title = @[LOCALIZATION(C_FORM_MANAGEMECHANIC),
                       LOCALIZATION(C_MANAGEMECHANIC_ADDNEW)
                       ];       //lokalised 8 Feb
    
    UIImage *selected = GET_ISADVANCE ? [UIImage imageNamed:@"tab-bg_blue"] : [UIImage imageNamed:@"tab-bg"];
    UIImage *unselected = [UIImage imageNamed:@"tab-bg_grey.png"];
    if (kIsRightToLeft) {
        title = title.reverseObjectEnumerator.allObjects.mutableCopy;
    }
    for (int i = 0; i < [self.tabBar.items count]; i++)
    {
        RDVTabBarItem *item = [self.tabBar.items objectAtIndex: i];
        [item setTitle: [title objectAtIndex: i]];
        
        [item setBackgroundColor: COLOUR_WHITE];
        [item setTitleUnselectedAttributes:FONT_H1 colour:COLOUR_VERYDARKGREY];
        [item setTitleSelectedAttributes:FONT_H1 colour:COLOUR_RED];
        
        [item setBackgroundSelectedImage: selected withUnselectedImage: unselected];
        
    }
    
    if (kIsRightToLeft) {
        [self tabBar:self.tabBar didSelectItemAtIndex: self.viewControllers.count-1];
    }
}

-(void) didRegisterSuccess
{
    self.selectedIndex = 0;
}

-(void)tabBar:(RDVTabBar *)tabBar didSelectItemAtIndex:(NSInteger)index
{
    [super tabBar:tabBar didSelectItemAtIndex: index];
    
}

- (IBAction)backPressed:(id)sender {
    [UpdateHUD removeSpotifyHUD:CURRENT_VIEW];
    
    CATransition *animation = [CATransition animation];
    animation.type = kCATransitionFade;
    animation.duration = 0.4;
    [self.navigationController.view.layer addAnimation:animation forKey:nil];
    
#warning test for popping the only controller
    //    if ([[vc.navigationController childViewControllers] count] > 1)
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
