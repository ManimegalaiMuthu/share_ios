//
//  MechanicListViewController.m
//  Shell
//
//  Created by Admin on 29/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "MechanicListViewController.h"
#import "WorkshopListTableViewCell.h"

@interface MechanicListViewController ()<UITableViewDelegate,UITableViewDataSource, WebServiceManagerDelegate>


@property (weak, nonatomic) IBOutlet TableViewWithEmptyView *tableView;
@property NSMutableArray *mechanicListArray;
@end

@implementation MechanicListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupInterface];
    
    if (!self.tradeID)    //use own trade ID
    {
        LoginInfo *loginInfo = [mSession loadUserProfile];
        self.tradeID = loginInfo.tradeID;
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName:[NSString stringWithFormat: @"%@ / %@", LOCALIZATION_EN(C_TITLE_MANAGEMECHANIC), LOCALIZATION_EN(C_FORM_MANAGEMECHANIC)] screenClass:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    [[WebServiceManager sharedInstance] fetchTrade: self.tradeID
                                                vc: self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setupInterface
{
    if (!self.mechanicListArray)
        self.mechanicListArray = [[NSMutableArray alloc] init];
    
    //tableview setup
    PlaceholderView *vwPlaceholder = [[PlaceholderView alloc]initWithView:self.tableView];
    [vwPlaceholder setBackgroundColor:[UIColor clearColor]];
    self.tableView.emptyView = vwPlaceholder;
    
    // Initialize the refresh control.
    [self.tableView.customRefreshControl addTarget:self
                                            action:@selector(refreshTable)
                                  forControlEvents:UIControlEventValueChanged];
    
//    [self.tableView registerNib:[UINib nibWithNibName:@"WorkshopListTableViewCell" bundle:nil] forCellReuseIdentifier:@"WorkshopListTableViewCell"];
    
    //empty view to eliminate extra separators
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.tableView reloadData];
}

-(void) refreshTable
{
    [[WebServiceManager sharedInstance] fetchTrade: self.tradeID
                                                vc: self];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.mechanicListArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    WorkshopListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"WorkshopListTableViewCell"];
    
//    NSDictionary *cellData = [self.mechanicListArray objectAtIndex: indexPath.row];
//    
//    cell.lblName.text = [cellData objectForKey: @"CompanyName"];
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"MechanicListCell"];
    
    NSDictionary *cellData = [self.mechanicListArray objectAtIndex: indexPath.row];
    
    UILabel *lblName = [cell viewWithTag: 1];
    lblName.text = [cellData objectForKey: @"Name"];
    lblName.font = FONT_H1;
    
    
    UILabel *lblMobile = [cell viewWithTag: 2];
    lblMobile.text = [cellData objectForKey: @"MobileNumber"];
    lblMobile.font = FONT_B1;
    
    if (![[cellData objectForKey:@"StaffTypeDesc"]isKindOfClass:[NSNull class]] & [[mSession profileInfo]isManageStaffOpt2]) {
        UILabel *lblStaffType = [cell viewWithTag: 3];
        lblStaffType.hidden = NO;
        lblStaffType.text = [cellData objectForKey: @"StaffTypeDesc"];
        lblStaffType.font = FONT_B1;
        
    } else {
        UILabel *lblStaffType = [cell viewWithTag: 3];
        lblStaffType.text = @"";
    }
    
    UIImageView *arrowImage = [cell viewWithTag:1001];
    if(kIsRightToLeft)
        arrowImage.image = [UIImage imageNamed:@"icon-arrsingleL.png"];
    else
        arrowImage.image = [UIImage imageNamed:@"icon-arrsingleR.png"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cellData = [self.mechanicListArray objectAtIndex: indexPath.row];
//    NSLog(@"Mechanic detail = %@",cellData);
    if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND] && [[mSession profileInfo]isManageStaffOpt2]) {
        [mSession pushStaffDetailsView:[cellData objectForKey:@"TradeID"] vc:self.navigationController.parentViewController];
    } else {
        [mSession pushMechanicDetailsView:[cellData objectForKey: @"TradeID"]  registrationID:[cellData objectForKey: @"RegistrationID"] workshopTradeID:self.tradeID vc:self.navigationController.parentViewController];
    }
     
     
    //[[WebServiceManager sharedInstance] fetchLeads: [cellData objectForKey: @"LeadID"] vc:self];

}

-(void)processCompleted:(WebServiceResponse *)response
{
    [self.tableView.customRefreshControl performSelectorOnMainThread: @selector(endRefreshing) withObject: self.tableView.customRefreshControl waitUntilDone:NO];
    
    self.mechanicListArray = [[response getGenericResponse] objectForKey:@"MechanicList"];

    [self setupInterface];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
