//
//  PromotionsTabViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 19/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//


#import "RDVTabBarController.h"
#import <UIKit/UIKit.h>
#import "Helper.h"

@interface PromotionsTabViewController : RDVTabBarController

@end
