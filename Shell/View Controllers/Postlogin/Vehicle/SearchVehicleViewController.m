//
//  SearchVehicleViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 29/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "SWGApiClient.h"
#import "SWGConfiguration.h"
// load models
#import "SWGCoordinate.h"
#import "SWGInlineResponse200.h"
#import "SWGInlineResponse200ProcessingTime.h"
#import "SWGInlineResponse400.h"
#import "SWGPlateCandidate.h"
#import "SWGPlateDetails.h"
#import "SWGRegionOfInterest.h"
#import "SWGVehicleCandidate.h"
#import "SWGVehicleDetails.h"
// load API classes for accessing endpoints
#import "SWGDefaultApi.h"


#import "SearchVehicleViewController.h"
#import "QRCodeScanner.h"
#import "PopupTagDecalViewController.h"
#import "PopupTagDecalToVehicleViewController.h"
#import "RegisterCustomerCompleteViewController.h"
#import "PopupCameraDecalViewController.h"

@interface SearchVehicleViewController () <WebServiceManagerDelegate,UITextFieldDelegate, TTTAttributedLabelDelegate, UITableViewDelegate, UITableViewDataSource, QRCodeScannerDelegate, PopupTagDecalDelegate, PopupTagDecalToVehicleDelegate, RegisterCustomerCompleteViewDelegate, PopupCameraDecalDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtSearchField;
@property (weak, nonatomic) IBOutlet UITableView *serviceTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *serviceTableHeight;
@property (weak, nonatomic) IBOutlet UITableView *promoOfferTableView;
@property (weak, nonatomic) IBOutlet UIView *promoTableSeparator;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *promoOfferHeight;
@property (weak, nonatomic) IBOutlet UIButton *scanProductCode;

@property (weak, nonatomic) IBOutlet UIScrollView *searchVehicleScrollView;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblInviteConsumer;

@property (weak, nonatomic) IBOutlet UILabel *lblVehicleId;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleIdData;
@property (weak, nonatomic) IBOutlet UILabel *lblShewStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblShewStatusData;
@property (weak, nonatomic) IBOutlet UILabel *lblShewValidity;
@property (weak, nonatomic) IBOutlet UILabel *lblShewValidityData;
@property (weak, nonatomic) IBOutlet UIView *separatorLine;
@property (weak, nonatomic) IBOutlet UIView *separatorLine2;

@property NSMutableArray *vehicleDetailsArray;
@property NSMutableArray *promotionDetailArray;


@property (strong, nonatomic) IBOutlet UIView *inviteConsumerView;
@property (weak, nonatomic) IBOutlet UILabel *lblInviteConsumerHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

@property (weak, nonatomic) IBOutlet UITextField *txtEmailAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtMobNum;
@property (weak, nonatomic) IBOutlet UITextField *txtVehicleId;


@property (weak, nonatomic) IBOutlet UILabel *lblRedeemPromoHeader;
@property (weak, nonatomic) IBOutlet UITextField *txtPromoCode;
@property (weak, nonatomic) IBOutlet UIButton *btnRedeemSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnRedeemCancel;
@property (weak, nonatomic) IBOutlet UIView *redeemPopup;

@property (weak, nonatomic) IBOutlet UILabel *lblConsumerName;

@property (weak, nonatomic) IBOutlet UIView *stateSelectView;
@property (weak, nonatomic) IBOutlet UIButton *btnState;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchFieldTopSpace;
@property NSArray *stateTable;

@property BOOL shouldLoadTextfieldEndApi;

//Decal
@property (weak, nonatomic) IBOutlet UIImageView *imgDecalIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblShareDecal;
@property (weak, nonatomic) IBOutlet UIButton *btnDecalTag;

#define NO_DECAL_DISTANCE 30 //if have decal
#define HAVE_DECAL_DISTANCE 120 //if not decal, hide decal view as well

#define SEARCHFIELD_DEFAULTOFFSET 5
#define DECAL_OFFSET 45
@property (weak, nonatomic) IBOutlet UIButton *btnCamera;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchFieldTrailingOffset;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vehicleDetailsToTableLayout;
@property (weak, nonatomic) IBOutlet UIView *decalView;
@property BOOL isVehicleTagged;

@property (weak, nonatomic) IBOutlet UILabel *lblServiceHistory;

@property (weak, nonatomic) IBOutlet UIView *consumerDetailsView;
@property (weak, nonatomic) IBOutlet UILabel *lblMemberSince;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consumerDetailViewHeight;


@property NSString *scannedDecalCode;
@end

#define CELL_HEIGHT 78.0

@implementation SearchVehicleViewController
@synthesize listView;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_SEARCHVEHICLE) subtitle: @""];       //lokalise 23 Jan
    
    //QRCode Delegate
    mQRCodeScanner.delegate = self;
    
    //decal default state
    self.decalView.hidden = YES;
    self.vehicleDetailsToTableLayout.constant = NO_DECAL_DISTANCE;
    
//    if ([[mSession profileInfo] hasDecal])
    {
        self.btnCamera.hidden = NO; //btnDecalScan to be renamed to camera
        self.searchFieldTrailingOffset.constant = SEARCHFIELD_DEFAULTOFFSET + DECAL_OFFSET;
    }
//    else
//    {
//        self.btnDecalScan.hidden = YES;
//        self.searchFieldTrailingOffset.constant = SEARCHFIELD_DEFAULTOFFSET;
//    }
    
    //Thai only
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
    {

        [[WebServiceManager sharedInstance] loadState: [[mSession lookupTable] objectForKey: @"LookUpVersion"]
                                          countryCode: GET_COUNTRY_CODE
                                                   vc: self];
        [self.btnState.titleLabel setFont: FONT_B1];
        [self.btnState setTitle: LOCALIZATION(C_THAI_VEHICLE_STATE) forState:UIControlStateNormal];     //lokalise 23 Jan
    }
    else
    {
        [self.stateSelectView removeFromSuperview];
        self.searchFieldTopSpace.constant = 10;
        [self setupInterface];
    }
    
    [self.serviceTableView setTableFooterView:[UIView new]];
    [self.promoOfferTableView setTableFooterView:[UIView new]];
    
    if (kIsRightToLeft) {
        [self.txtSearchField setTextAlignment:NSTextAlignmentRight];
    } else {
        [self.txtSearchField setTextAlignment:NSTextAlignmentLeft];
    }
}

-(void) showDecal:(BOOL)toShow
{
    //if have decal
    if ([[mSession profileInfo] hasDecal] && toShow)
    {
        self.decalView.hidden = NO;
        self.vehicleDetailsToTableLayout.constant = HAVE_DECAL_DISTANCE;
    }
    else
    {
        self.decalView.hidden = YES;
        self.vehicleDetailsToTableLayout.constant = NO_DECAL_DISTANCE;
    }
}

- (void)tagDecal
{
    [mQRCodeScanner showSingleDecalScanner: self];
}



-(void)qrCodeScanSuccess:(NSString *)scanString
{
    
    NSString *scannedCodeString;
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDONESIA])
    {
        scannedCodeString = scanString;
    }
    else
    {
        
        scannedCodeString = [[scanString componentsSeparatedByString:@"="] lastObject];
    }
    
    self.scannedDecalCode = scannedCodeString;
    [[WebServiceManager sharedInstance] searchVehicleWithDecal: self.scannedDecalCode vc: self];
}

-(void) setupInterface
{
    self.txtSearchField.font = FONT_B1;
    self.txtSearchField.placeholder = LOCALIZATION(C_SEARCHFIELD_VEHICLEID);        //lokalise 23 Jan
    
    self.lblVehicleId.font = FONT_B1;
    self.lblShewStatus.font = self.lblVehicleId.font;
    self.lblShewValidity.font = self.lblVehicleId.font;
    
    self.lblVehicleId.text = LOCALIZATION(C_SEARCHVEHICLE_VEHICLEID);               //lokalise 23 Jan
    self.lblShewStatus.text = LOCALIZATION(C_SEARCHVEHICLE_SHEWSTATUS);             //lokalise 23 Jan
    self.lblShewValidity.text = LOCALIZATION(C_SEARCHVEHICLE_SHEWVALIDITY);         //lokalise 23 Jan
    
    self.lblVehicleIdData.font = FONT_H1;
    self.lblShewStatusData.font = self.lblVehicleIdData.font;
    self.lblShewValidityData.font = self.lblVehicleIdData.font;
    
    self.scanProductCode.titleLabel.font = FONT_BUTTON;
    [self.scanProductCode setTitle:LOCALIZATION(C_SEARCHVEHICLE_REGISTERPRODUCTCODE) forState:UIControlStateNormal];        //lokalise 23 Jan
    self.scanProductCode.backgroundColor = COLOUR_RED;

    self.scanProductCode.hidden = [[mSession profileInfo] hasShowScanSearchVehicle]? NO : YES;
    
    self.lblConsumerName.font = FONT_B2;
    self.lblMemberSince.font = FONT_B2;
    
    self.lblInviteConsumerHeader.font = FONT_H1;
    self.lblInviteConsumerHeader.text = LOCALIZATION(C_ALERTHEADER_INVITECONSUMER);     //lokalise 23 Jan
    
    self.txtEmailAddress.font = FONT_B1;
    self.txtEmailAddress.placeholder = LOCALIZATION(C_CONTACTUS_EMAIL);                 //lokalised in contactus
    
    self.txtMobNum.font = FONT_B1;
    self.txtMobNum.placeholder = LOCALIZATION(C_CONTACTUS_MOBILE);                      //lokalised in contactus
    
    self.txtVehicleId.font = FONT_B1;
    self.txtVehicleId.placeholder = LOCALIZATION(C_SEARCHVEHICLE_VEHICLEID);
    
    [self.btnSubmit.titleLabel setFont: FONT_BUTTON];
    [self.btnSubmit setTitle:LOCALIZATION(C_FORM_SUBMIT)  forState:UIControlStateNormal];       //lokalised
    self.btnSubmit.backgroundColor = COLOUR_RED;
    [self.btnCancel.titleLabel setFont: FONT_BUTTON];
    [self.btnCancel setTitle:LOCALIZATION(C_FORM_CANCEL)  forState:UIControlStateNormal];       //lokalised
    self.btnCancel.backgroundColor = COLOUR_YELLOW;
    
    self.lblRedeemPromoHeader.font = FONT_H1;
    self.lblRedeemPromoHeader.text = LOCALIZATION(C_ALERTHEADER_PROMOOFFER);            //lokalise 23 Jan
    self.txtPromoCode.font = FONT_B1;
    self.txtPromoCode.placeholder = LOCALIZATION(C_PLACEHOLDER_PROMO_OFFER);            //lokalise 23 Jan
    
    [self.btnRedeemSubmit.titleLabel setFont: FONT_BUTTON];
    [self.btnRedeemSubmit setTitle:LOCALIZATION(C_FORM_SUBMIT)  forState:UIControlStateNormal];     //loklised
    self.btnRedeemSubmit.backgroundColor = COLOUR_RED;
    [self.btnRedeemCancel.titleLabel setFont: FONT_BUTTON];
    [self.btnRedeemCancel setTitle:LOCALIZATION(C_FORM_CANCEL)  forState:UIControlStateNormal];     //lokalised
    self.btnRedeemCancel.backgroundColor = COLOUR_YELLOW;
    
    self.lblInviteConsumer.delegate = self;
    [Helper setHyperlinkLabel:self.lblInviteConsumer hyperlinkText:LOCALIZATION(C_INVITE_CUSTOMER) bodyText:LOCALIZATION(C_INVITE_CUSTOMER) urlString: @"Invite"];  //lokalise 23 Jan
    if (kIsRightToLeft) {
        [self.lblInviteConsumer setTextAlignment: NSTextAlignmentRight];
        [self.lblShareDecal setTextAlignment:NSTextAlignmentRight];
    }
    else {
        [self.lblInviteConsumer setTextAlignment: NSTextAlignmentLeft];
        [self.lblShareDecal setTextAlignment:NSTextAlignmentLeft];
    }
    
    self.serviceTableView.hidden = YES;
    self.promoOfferTableView.hidden = YES;
    self.promoTableSeparator.hidden = YES;
    
    self.separatorLine2.hidden = YES;
    self.separatorLine.backgroundColor = COLOUR_RED;
    self.separatorLine2.backgroundColor = COLOUR_RED;
    self.promoTableSeparator.backgroundColor = COLOUR_RED;
    
    self.lblShareDecal.text = LOCALIZATION(C_DECAL_SHAREDECAL);         //lokalise 23 Jan
    self.lblShareDecal.font = FONT_H1;
    
    [self.btnDecalTag.titleLabel setFont: FONT_BUTTON];
    [self.btnDecalTag setTitle:LOCALIZATION(C_DECAL_TAGNEW) forState:UIControlStateNormal];     //lokalise 23 Jan
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_SEARCHVEHICLE) screenClass:nil];
    
    self.shouldLoadTextfieldEndApi = YES;
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url absoluteString] isEqualToString: @"Invite"])
        [self inviteConsumer];
}

-(void) inviteConsumer
{
    NSString *stateID = [mSession convertToStateKeyCode:self.btnState.titleLabel.text stateDetailedListArray: self.stateTable];
    [mSession pushRegisterConsumer:@{@"VehicleNumber": self.txtSearchField.text,
                                     @"VehicleStateID": stateID,
                                     @"PromoCode": @"",
                                     }
                                vc: self]; //no promo code
    
//    self.inviteConsumerView.hidden = NO;
//    self.txtVehicleId.text = self.lblVehicleIdData.text;
//
//
//    if(GET_PROFILETYPE == kPROFILETYPE_DSR)
//    {
//        self.btnSubmit.hidden = YES;
//    }
}

- (IBAction)inviteSubmitPressed:(id)sender {
    
    NSDictionary *submitParams = @{@"Name": @"",
                                   @"MobileNumber": self.txtMobNum.text,
                                   @"EmailAddress": self.txtEmailAddress.text,
                                   @"VehicleNumber": self.txtVehicleId.text,
                                   };
    
    [[WebServiceManager sharedInstance] inviteConsumer: submitParams vc:self];
}

- (IBAction)finishPopupPressed:(id)sender {
    self.txtEmailAddress.text = @"";
    self.txtMobNum.text = @"";
    self.txtVehicleId.text = @"";
    
    self.inviteConsumerView.hidden = YES;
    self.redeemPopup.hidden = YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.txtSearchField)
    {
        NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
        NSString *trimmedString = [self.txtSearchField.text stringByTrimmingCharactersInSet:charSet];
        if ([trimmedString isEqualToString:@""]) {
            //if string only whitespace
            
            self.serviceTableView.hidden = YES;
            self.promoOfferTableView.hidden = YES;
            self.promoTableSeparator.hidden = YES;
            self.separatorLine2.hidden = YES;
            
            self.lblConsumerName.text = @"";
            self.lblInviteConsumer.hidden = YES;
            self.consumerDetailsView.hidden = YES;
            self.consumerDetailViewHeight.constant= 25;
            
            self.lblVehicleIdData.text = @"";
            self.lblShewStatusData.text = @"";
            self.lblShewValidityData.text = @"";
        }
        else if (self.shouldLoadTextfieldEndApi)
        {
            //getState
            NSString *stateKeyCode = [mSession convertToStateKeyCode:self.btnState.titleLabel.text stateDetailedListArray: self.stateTable];
            
            [[WebServiceManager sharedInstance] searchVehicle: textField.text state: stateKeyCode vc:self];
        }

    }
}

//delegate method
-(void) completeRegister
{
    //getState
    NSString *stateKeyCode = [mSession convertToStateKeyCode:self.btnState.titleLabel.text stateDetailedListArray: self.stateTable];
    
    [[WebServiceManager sharedInstance] searchVehicle: self.txtSearchField.text state: stateKeyCode vc:self];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.serviceTableView && [self.vehicleDetailsArray count] > 0)
    {
        return [self.vehicleDetailsArray count] + 1; //+1 for header
    }
    else if (tableView == self.promoOfferTableView && [self.promotionDetailArray count] > 0)
    {
        return [self.promotionDetailArray count] + 1; //+1 for header
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.promoOfferTableView || indexPath.row == 0 || IS_COUNTRY(COUNTRYCODE_THAILAND))
        return UITableViewAutomaticDimension;
    else
        return CELL_HEIGHT;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (tableView == self.serviceTableView)
    {
        if (indexPath.row == 0) //header
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceHeader"];
            
//            UILabel *lblServiceHistory = [cell viewWithTag: 5];
//            
//            lblServiceHistory.text = LOCALIZATION(C_SEARCHVEHICLE_SERVICEHISTORY);
//            lblServiceHistory.font = FONT_H1;
            
//            UILabel *lblServiceDate = [cell viewWithTag: 1];
//            lblServiceDate.font = FONT_H2;
//            lblServiceDate.text = [NSString stringWithFormat: @"%@\r%@", LOCALIZATION(C_SEARCHVEHICLE_SERVICEDATE), @"(DD/MM/YY)"];
//
            TTTAttributedLabel *lblServiceDate = [cell viewWithTag: 1];
            NSString *text = [NSString stringWithFormat: @"%@\n%@", LOCALIZATION(C_SEARCHVEHICLE_SERVICEDATE), @"(DD/MM/YY)"];      //lokalise 23 Jan
            lblServiceDate.font = FONT_H1;
            
            [lblServiceDate setText:text afterInheritingLabelAttributesAndConfiguringWithBlock:^(NSMutableAttributedString *mutableAttributedString) {
                NSRange whiteRange = [text rangeOfString:@"(DD/MM/YY)"];
                if (whiteRange.location != NSNotFound) {
                    // Core Text APIs use C functions without a direct bridge to UIFont. See Apple's "Core Text Programming Guide" to learn how to configure string attributes.
                    [mutableAttributedString addAttribute: NSFontAttributeName value: FONT_B2 range:whiteRange];
                }
                
                return mutableAttributedString;
            }];
            
            UILabel *lblLastUsedProduct = [cell viewWithTag: 2];
            lblLastUsedProduct.font = lblServiceDate.font;
            lblLastUsedProduct.text = LOCALIZATION(C_SEARCHVEHICLE_PRODUCTUSED);        //lokalise 23 Jan
            
            UILabel *lblLastMileage = [cell viewWithTag: 3];
            lblLastMileage.font = lblServiceDate.font;
            lblLastMileage.text = LOCALIZATION(C_SEARCHVEHICLE_MILEAGE);                //lokalise 23 Jan
            
            UILabel *lblLastServiceBy = [cell viewWithTag: 4];
            lblLastServiceBy.font = lblServiceDate.font;
            lblLastServiceBy.text = LOCALIZATION(C_SEARCHVEHICLE_SERVICEBY);            //lokalise 23 Jan
            
            if (kIsRightToLeft) {
                lblServiceDate.textAlignment = NSTextAlignmentRight;
            } else {
                lblServiceDate.textAlignment = NSTextAlignmentLeft;
            }
        }
        else
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceCell"];
            NSDictionary *serviceData = [self.vehicleDetailsArray objectAtIndex: indexPath.row - 1];
            UILabel *lblServiceData = [cell viewWithTag: 1];
            lblServiceData.font = FONT_B2;
            lblServiceData.text = [serviceData objectForKey: @"ServiceDate"];
            
            UILabel *lblLastUsedData = [cell viewWithTag: 2];
            lblLastUsedData.font = lblServiceData.font;
            lblLastUsedData.text = [serviceData objectForKey: @"LastUsedProduct"];
            
            UILabel *lblLastMileageData = [cell viewWithTag: 3];
            lblLastMileageData.font = lblServiceData.font;
            lblLastMileageData.text = [serviceData objectForKey: @"LastMileage"];
            
            UILabel *lblLastServiceByData = [cell viewWithTag: 4];
            lblLastServiceByData.font = lblServiceData.font;
            lblLastServiceByData.text = [serviceData objectForKey: @"LastServiceBy"];
        }
    }
    else
    {
        if (indexPath.row == 0) //header
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"PromoHeader"];
            
            UILabel *lblHeader = [cell viewWithTag: 1];
            lblHeader.font = FONT_H1;
            lblHeader.text = LOCALIZATION(C_SEARCHVEHICLE_ENTITLEDOFFER);           //lokalise 23 Jan
            
            UILabel *lblOfferDetailsHeader = [cell viewWithTag: 2];
            lblOfferDetailsHeader.font = FONT_B2;
            lblOfferDetailsHeader.text = LOCALIZATION(C_SEARCHVEHICLE_OFFERDETAILS);        //lokalise 23 Jan
            
            UILabel *lblValidityHeader = [cell viewWithTag: 3];
            lblValidityHeader.font = lblOfferDetailsHeader.font;
            lblValidityHeader.text = LOCALIZATION(C_SEARCHVEHICLE_OFFERVALIDITY);           //lokalise 23 Jan
            
            UIButton *redeemBtn  = [cell viewWithTag: 4];
            [redeemBtn addTarget: self action:@selector(redeemPressed:) forControlEvents:UIControlEventTouchUpInside];
            [redeemBtn.titleLabel setFont: FONT_B2];
            [redeemBtn.titleLabel setNumberOfLines:0];
            [redeemBtn setTitle:LOCALIZATION(C_SEARCHVEHICLE_OFFERREDEEM) forState:UIControlStateNormal];       //lokalise 23 Jan
            if([[mSession profileInfo] hasCustomerRedemptionBtn]){
                [redeemBtn setHidden:NO];
            } else {
                [redeemBtn setHidden:YES];
            }
        }
        else
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"PromoCell"];
            NSDictionary *promoData = [self.promotionDetailArray objectAtIndex: indexPath.row - 1];
            UILabel *lblPromoOffer = [cell viewWithTag: 1];
            lblPromoOffer.font = FONT_B2;
            lblPromoOffer.text = [promoData objectForKey: @"PromotionOffer"];
            
            
            UILabel *lblValidity = [cell viewWithTag: 2];
            lblValidity.font = lblPromoOffer.font;
            lblValidity.text = [promoData objectForKey: @"Validity"];
        }
        
    }
    return cell;
}
- (IBAction)redeemPressed:(UIButton *) sender
{
    self.shouldLoadTextfieldEndApi = NO;
    [mSession pushPromoCodeView: self];
//    UITableViewCell *cell = (UITableViewCell *) [sender superview];
//    UILabel *lblPromoOffer = [cell viewWithTag: 1];
//
//    self.redeemPopup.hidden = NO;
//    self.lblRedeemPromoHeader.text = lblPromoOffer.text;
}

- (IBAction)redeemSubmitPressed:(id)sender {
    
    NSDictionary *submitParams = @{@"Name": @"",
                                   @"MobileNumber": self.txtMobNum.text,
                                   @"EmailAddress": self.txtEmailAddress.text,
                                   @"VehicleNumber": self.txtVehicleId.text,
                                   };
    
    [[WebServiceManager sharedInstance] submitPromoCode: self.txtPromoCode.text
                                          vehicleNumber: self.lblVehicleIdData.text
                                                     vc:self];
}

- (IBAction)tagDecalPressed:(UIButton *)sender {

    [self initTagDecalToVehicleWithVehicleId:self.txtSearchField.text stateName: self.btnState.titleLabel.text];
}

//delegate method
-(void) didTagVehicle
{
    //getState
    NSString *stateKeyCode = [mSession convertToStateKeyCode:self.btnState.titleLabel.text stateDetailedListArray: self.stateTable];
    
    [[WebServiceManager sharedInstance] searchVehicle: self.txtSearchField.text state: stateKeyCode vc:self];
}

//untagged decal code delegate method
-(void)tagDecalWithVehicleId:(NSString *)vehicleId stateName:(NSString *)stateName
{
    self.txtSearchField.text = vehicleId;
    self.btnState.titleLabel.text = stateName;
    
    //getState
    NSString *stateKeyCode = [mSession convertToStateKeyCode:stateName stateDetailedListArray: self.stateTable];
    
    [[WebServiceManager sharedInstance] searchVehicle: self.txtSearchField.text state: stateKeyCode vc:self];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_SEARCHVEHICLE:
        {
            NSDictionary *searchData = [response getGenericResponse];
            
            //can invite = don't hide invite label
            self.lblInviteConsumer.hidden = ![[searchData objectForKey:@"CanInvite"] boolValue];
            self.scanProductCode.hidden = ![[searchData objectForKey:@"ShowRegProductCodeBtn"] boolValue];
            
            if (![[searchData objectForKey:@"ConsumerName"] isKindOfClass: [NSNull class]])
            {
                NSString *GGVdriverNumber = @"";
                if(![[searchData objectForKey:@"GGVDriverNumber"] isKindOfClass:[NSNull class]]) {
                    GGVdriverNumber = [searchData objectForKey:@"GGVDriverNumber"];
                }
                if([GGVdriverNumber isEqualToString:@""]) {
                    self.lblConsumerName.text = [searchData objectForKey:@"ConsumerName"];
                } else {
                    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[searchData objectForKey:@"ConsumerName"] attributes:nil];        //lokalise 23 Jan
                    NSMutableAttributedString *driverNumberString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",GGVdriverNumber] attributes:@{NSForegroundColorAttributeName: COLOUR_DARKGREY,NSFontAttributeName : FONT_H1}];
                    [attrString appendAttributedString:driverNumberString];
                    self.lblConsumerName.attributedText = attrString;
                }
                
                NSString *memberSinceValue = [searchData objectForKey:@"ConsumerJoinDate"];
                NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(C_VEHICLESEARCH_MEMBERSINCE) attributes:nil];        //lokalise 23 Jan
                NSMutableAttributedString *dateString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",memberSinceValue] attributes:@{NSForegroundColorAttributeName: COLOUR_RED,NSFontAttributeName : FONT_H2}];
                [attrString appendAttributedString:dateString];
                self.lblMemberSince.attributedText = attrString;
                self.consumerDetailViewHeight.constant = 55;
                self.consumerDetailsView.hidden = NO;
                [self.view layoutSubviews];
               
                self.lblInviteConsumer.hidden = YES;
                
            }
            else
            {
                self.lblConsumerName.text = @"";
                self.lblInviteConsumer.hidden = NO;
                self.consumerDetailsView.hidden = YES;
                self.consumerDetailViewHeight.constant= 25;

            }
            
            if ([[searchData objectForKey:@"StateVehicleNumber"] isKindOfClass: [NSNull class]] && (
                ![searchData objectForKey:@"VehicleNumber"] ||
                [[searchData objectForKey:@"VehicleNumber"] isKindOfClass: [NSNull class]] ||
                [[searchData objectForKey: @"VehicleNumber"] isEqualToString: @""]))
            {
                
                self.serviceTableView.hidden = YES;
                self.promoOfferTableView.hidden = YES;
                self.promoTableSeparator.hidden = YES;
                self.separatorLine2.hidden = YES;
                
                self.lblVehicleIdData.text = @"";
                [self showDecal: NO];
                
                return;
            }
            else
            {
                self.separatorLine2.hidden = NO;
                
                if ([[searchData objectForKey:@"StateVehicleNumber"] isKindOfClass: [NSNull class]])
                    self.lblVehicleIdData.text = [searchData objectForKey:@"VehicleNumber"];
                else
                    self.lblVehicleIdData.text = [searchData objectForKey:@"StateVehicleNumber"];
                    
                self.lblShewStatusData.text = [searchData objectForKey:@"ShewStatus"];
                self.lblShewValidityData.text = [searchData objectForKey:@"ShewValidity"];
                
                self.vehicleDetailsArray = [searchData objectForKey:@"VehicleDetails"];
                self.promotionDetailArray = [searchData objectForKey:@"PromotionDetail"];
                
                [self.serviceTableView reloadData];
                [self.promoOfferTableView reloadData];
                
                self.serviceTableHeight.constant = CELL_HEIGHT * (self.vehicleDetailsArray.count + 1); //+1 for header;
                self.promoOfferHeight.constant = CELL_HEIGHT * (self.promotionDetailArray.count + 1); //+1 for header;
                
                self.serviceTableView.hidden = [[searchData objectForKey: @"ID"] isEqualToString: @""];
                
                if ([self.promotionDetailArray count] > 0)
                {
                    self.promoOfferTableView.hidden = NO;
                    self.promoTableSeparator.hidden = NO;
                }
                else
                {
                    self.promoOfferTableView.hidden = YES;
                    self.promoTableSeparator.hidden = YES;
                }
                    
            }
            
            if([[mSession profileInfo]hasDecal]) {
                [self showDecal: YES];
            } else {
                [self showDecal:NO];
            }
            
            if ([[searchData objectForKey: @"IsVehicleTaged"] boolValue])
            {
                
                //perfect case, vehicle is tagged
                //decal is definitely valid
                self.imgDecalIcon.image = [UIImage imageNamed: @"icn-decal_yes"];
                self.isVehicleTagged = YES;
                //                [self.btnDecalTag setTitle: LOCALIZATION(C_DECAL_TAGNEW) forState: UIControlStateNormal];
            }
            else
            {
                self.imgDecalIcon.image = [UIImage imageNamed: @"icn-decal_no"];
                self.isVehicleTagged = NO;
                
                if ([[searchData objectForKey: @"IsDecalValid"] boolValue])
                {
                    //                    self.imgDecalIcon.image = [UIImage imageNamed: @"icn-decal_no"];
                }
                else
                {
                    
                    CLSLog(@"unknown error!");
                }
                //                [self.btnDecalTag setTitle: LOCALIZATION(C_DECAL_TAGNEW) forState: UIControlStateNormal]
                
            }
        }
            break;
        case kWEBSERVICE_TAGVEHICLETODECAL:
        {
            
        }
            break;
        case kWEBSERVICE_SEARCHVEHICLEWITHDECAL:
        {
            
            NSDictionary *searchData = [response getGenericResponse];
            if (![[searchData objectForKey:@"IsDecalValid"] boolValue])
            {
                self.serviceTableView.hidden = YES;
                self.promoOfferTableView.hidden = YES;
                self.promoTableSeparator.hidden = YES;
                self.separatorLine2.hidden = YES;
                
                self.txtSearchField.text = @"";
                [self.btnState setTitle: LOCALIZATION(C_THAI_VEHICLE_STATE) forState:UIControlStateNormal];     //lokalise 23 Jan
                
                self.lblVehicleIdData.text = @"";
                [self showDecal: NO];
                
                [mAlert showErrorAlertWithMessage: [response getResponseMessage]];
                return;
            }
            
            if (![[searchData objectForKey:@"IsVehicleTaged"] boolValue])
            {
                //Decal not tagged
                [self initDecalPopupWithDecalCode: self.scannedDecalCode];
                return;
            }
            //clear scanned code if not tagged.
            self.scannedDecalCode = @"";
            
            //can invite = don't hide invite label
            self.lblInviteConsumer.hidden = ![[searchData objectForKey:@"CanInvite"] boolValue];
            
            if (![[searchData objectForKey:@"ConsumerName"] isKindOfClass: [NSNull class]])
            {
                self.lblConsumerName.text = [searchData objectForKey:@"ConsumerName"];
                NSString *memberSinceValue = [searchData objectForKey:@"ConsumerJoinDate"];
                NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(C_VEHICLESEARCH_MEMBERSINCE) attributes:nil];        //lokalise 23 Jan
                NSMutableAttributedString *dateString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",memberSinceValue] attributes:@{NSForegroundColorAttributeName: COLOUR_RED,NSFontAttributeName : FONT_H2}];
                [attrString appendAttributedString:dateString];
                self.lblMemberSince.attributedText = attrString;
                self.consumerDetailsView.hidden = NO;
                self.consumerDetailViewHeight.constant = 55;
                [self.view layoutSubviews];
                
                self.lblInviteConsumer.hidden = YES;
            }
            else
            {
                self.lblConsumerName.text = @"";
                self.lblInviteConsumer.hidden = NO;
                self.consumerDetailsView.hidden = YES;
                self.consumerDetailViewHeight.constant= 25;
            }
            
            if (![searchData objectForKey:@"VehicleNumber"] ||
                [[searchData objectForKey:@"VehicleNumber"] isKindOfClass: [NSNull class]] ||
                [[searchData objectForKey: @"VehicleNumber"] isEqualToString: @""])
            {
                
                self.serviceTableView.hidden = YES;
                self.promoOfferTableView.hidden = YES;
                self.promoTableSeparator.hidden = YES;
                self.separatorLine2.hidden = YES;
                
                self.lblVehicleIdData.text = @"";
                [self showDecal: NO];
                return;
            }
            else
            {
                self.separatorLine2.hidden = NO;
                
                
                self.txtSearchField.text = [searchData objectForKey:@"VehicleNumber"];
                
                if ([self.stateSelectView superview])
                {
                    NSString *stateName = [mSession convertToStateName: [searchData objectForKey:@"StateID"]  stateDetailedListArray:self.stateTable];

                    [self.btnState setTitle: stateName forState: UIControlStateNormal];
                }
                
                self.lblVehicleIdData.text = [searchData objectForKey:@"StateVehicleNumber"];
                self.lblShewStatusData.text = [searchData objectForKey:@"ShewStatus"];
                self.lblShewValidityData.text = [searchData objectForKey:@"ShewValidity"];
                
                self.vehicleDetailsArray = [searchData objectForKey:@"VehicleDetails"];
                self.promotionDetailArray = [searchData objectForKey:@"PromotionDetail"];
                
                [self.serviceTableView reloadData];
                [self.promoOfferTableView reloadData];
                
                self.serviceTableHeight.constant = CELL_HEIGHT * (self.vehicleDetailsArray.count + 1); //+1 for header;
                self.promoOfferHeight.constant = CELL_HEIGHT * (self.promotionDetailArray.count + 1); //+1 for header;
                
                self.serviceTableView.hidden = [[searchData objectForKey: @"ID"] isEqualToString: @""];
                
                if ([[mSession profileInfo] hasCustomerRedemption])
                {
                    if ([self.promotionDetailArray count] > 0)
                    {
                        self.promoOfferTableView.hidden = NO;
                        self.promoTableSeparator.hidden = NO;
                    }
                    else
                    {
                        self.promoOfferTableView.hidden = YES;
                        self.promoTableSeparator.hidden = YES;
                    }
                }
                else
                {
                    self.promoOfferTableView.hidden = YES;
                    self.promoTableSeparator.hidden = YES;
                }
            }

            if ([[searchData objectForKey: @"IsVehicleTaged"] boolValue])
            {
                [self showDecal: YES];
                
                //perfect case, vehicle is tagged
                //decal is definitely valid
                self.imgDecalIcon.image = [UIImage imageNamed: @"icn-decal_yes"];
                self.isVehicleTagged = YES;
//                [self.btnDecalTag setTitle: LOCALIZATION(C_DECAL_TAGNEW) forState: UIControlStateNormal];
            }
            else
            {
                [self showDecal: NO];
                self.imgDecalIcon.image = [UIImage imageNamed: @"icn-decal_no"];
                self.isVehicleTagged = NO;
                
                if ([[searchData objectForKey: @"IsDecalValid"] boolValue])
                {
//                    self.imgDecalIcon.image = [UIImage imageNamed: @"icn-decal_no"];
                }
                else
                {
                    
                    CLSLog(@"unknown error!");
                }
//                [self.btnDecalTag setTitle: LOCALIZATION(C_DECAL_TAGNEW) forState: UIControlStateNormal]
            
            }
        }
            break;
        case kWEBSERVICE_INVITECONSUMER:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                [self finishPopupPressed: nil];
            }];
        }
            break;
        case kWEBSERVICE_PROMOCODE:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                [self finishPopupPressed: nil];
            }];
            break;
        }
        case kWEBSERVICE_STATE:
            SET_LOADSTATE([[response getGenericResponse] objectForKey: @"StateDetailsList"]);
            self.stateTable = [[response getGenericResponse] objectForKey: @"StateDetailsList"];

            //preload listviews
            listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_THAI_VEHICLE_STATE)                     //lokalise 23 Jan
                                                           list: [mSession getStateNames: self.stateTable]
                                                  selectionType:ListSelectionTypeSingle previouslySelected: nil];
            [listView setHasSearchField: YES];
            listView.delegate = self;
            
            [self setupInterface];
            break;
        default:
            break;
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress: KEY_WINDOW];
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
            self.stateTable = GET_LOADSTATE;
            
            //preload listviews
            listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_THAI_VEHICLE_STATE)                 //lokalise 23 Jan
                                                           list: [mSession getStateNames: self.stateTable]
                                                  selectionType:ListSelectionTypeSingle previouslySelected: nil];
            [listView setHasSearchField: YES];
            listView.delegate = self;
            
            [self setupInterface];
            break;
        case kWEBSERVICE_SEARCHVEHICLE: {
            self.scanProductCode.hidden = ![[mSession profileInfo] hasShowScanSearchVehicle];
            self.consumerDetailsView.hidden = YES;
            self.consumerDetailViewHeight.constant = 15;
        }
            break;
        default:
            break;
    }
}

- (IBAction)scanProductCode:(id)sender {
    [mSession pushScanProductView: self.txtSearchField.text previousState:self.btnState.titleLabel.text vc:self];
    
}

- (IBAction)statePressed:(id)sender {
    
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        [self.btnState setTitle: selection.firstObject forState: UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnCameraPressed:(id)sender
{
    if ([self.txtSearchField isFirstResponder])
    {
        [self.txtSearchField resignFirstResponder];
        return;
    }
    
    if ([[mSession profileInfo] hasDecal])
    {
        [self popupSelection];
    }
    else
    {
        [self mainImageButtonPressed: sender];
    }
}

-(void) popupSelection
{
    [self popupCameraDecal];
}

-(void)didSelectDecal
{
    [self tagDecal];
}

-(void) didSelectCarPlate
{
    [self mainImageButtonPressed: nil];
}



#pragma mark - Car Plate Scanning
-(void)imageTaken:(UIImage *)image key:(NSString *)keyString
{
    if (!image)
        return;
    
    UIImage *newImage = [self rotateImage:image];
    
    NSData *imgData = UIImageJPEGRepresentation(newImage, 0.1);
    NSString *base64String = [imgData base64EncodedStringWithOptions: 0];
    
    NSString* secretKey = @"sk_a94baee6f07c7836e8ef292d"; // The secret key used to authenticate your account.  You can view your  secret key by visiting  https://cloud.openalpr.com/
    NSString *country = [self getCountryCode];
    //    NSString* country = @"us"; // Defines the training data used by OpenALPR.  \"us\" analyzes  North-American style plates.  \"eu\" analyzes European-style plates.  This field is required if using the \"plate\" task  You may use multiple datasets by using commas between the country  codes.  For example, 'au,auwide' would analyze using both the  Australian plate styles.  A full list of supported country codes  can be found here https://github.com/openalpr/openalpr/tree/master/runtime_data/config
    //au, auwide, br, br2, eu, fr, gb, in, kr, kr2, mx, sg, us, vn2
    
    if([country length] == 0)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:@"Invalid Country"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {
                                 //BUTTON OK CLICK EVENT
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    NSNumber* recognizeVehicle = @(0); // If set to 1, the vehicle will also be recognized in the image This requires an additional credit per request  (optional) (default to 0)
    NSString* state = @""; // Corresponds to a US state or EU country code used by OpenALPR pattern  recognition.  For example, using \"md\" matches US plates against the  Maryland plate patterns.  Using \"fr\" matches European plates against  the French plate patterns.  (optional) (default to )
    NSNumber* returnImage = @(0); // If set to 1, the image you uploaded will be encoded in base64 and  sent back along with the response  (optional) (default to 0)
    NSNumber* topn = @(5); // The number of results you would like to be returned for plate  candidates and vehicle classifications  (optional) (default to 10)
    NSString* prewarp = @""; // Prewarp configuration is used to calibrate the analyses for the  angle of a particular camera.  More information is available here http://doc.openalpr.com/accuracy_improvements.html#calibration  (optional) (default to )
    
    SWGDefaultApi *apiInstance = [[SWGDefaultApi alloc] init];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [apiInstance recognizeBytesWithImageBytes:base64String
                                    secretKey:secretKey
                                      country:country
                             recognizeVehicle:recognizeVehicle
                                        state:state
                                  returnImage:returnImage
                                         topn:topn
                                      prewarp:prewarp
                            completionHandler: ^(SWGInlineResponse200* output, NSError* error) {
                                if (output)
                                {
                                    if ([output.results count] > 0)
                                    {
                                        SWGPlateDetails *plateDetails = [output.results objectAtIndex: 0];
                                        
                                        self.txtSearchField.text = plateDetails.plate; //String of car plate
                                        [self.txtSearchField becomeFirstResponder];
                                    }
                                    else
                                    {
                                        UIAlertController *alert = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_GLOBAL_ERROR)
                                                                                                       message: LOCALIZATION(C_ERROR_CANNOTRECOGNIZE)
                                                                                                preferredStyle:UIAlertControllerStyleAlert];
                                        
                                        UIAlertAction *ok = [UIAlertAction actionWithTitle: LOCALIZATION(C_BUTTON_OK) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                             {
                                                                 //BUTTON OK CLICK EVENT
                                                             }];
                                        [alert addAction:ok];
                                        [self presentViewController:alert animated:YES completion:nil];
                                        //                                        self.imgView.image = nil;
                                    }
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                }
                                if (error) {
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    
                                    UIAlertController *alert = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_GLOBAL_ERROR)
                                                                                                   message: LOCALIZATION(C_ERROR_UNKNOWNERROR)
                                                                                            preferredStyle:UIAlertControllerStyleAlert];
                                    
                                    UIAlertAction *ok = [UIAlertAction actionWithTitle: LOCALIZATION(C_BUTTON_OK) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                         {
                                                             //BUTTON OK CLICK EVENT
                                                         }];
                                    [alert addAction:ok];
                                    [self presentViewController:alert animated:YES completion:nil];
                                }
                            }];
}

- (IBAction)mainImageButtonPressed:(id)sender
{
    [mSession pushPhotoTakingViewControllerWithParent: self];
}

-(NSString *) getCountryCode
{
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
    {
        return @"th";
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDIA])
    {
        return @"in";
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDONESIA])
    {
        return @"id";
    }
    else if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_SAUDI])
    {
        return @"sa";
    }
    else
    {
        return @"eu";
    }
}

- (UIImage*) rotateImage:(UIImage* )originalImage {
    
    UIImageOrientation orientation = originalImage.imageOrientation;
    
    UIGraphicsBeginImageContext(originalImage.size);
    
    [originalImage drawAtPoint:CGPointMake(0, 0)];
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orientation == UIImageOrientationRight) {
        CGContextRotateCTM (context, [self radians:90]);
    } else if (orientation == UIImageOrientationLeft) {
        CGContextRotateCTM (context, [self radians:90]);
    } else if (orientation == UIImageOrientationDown) {
        // NOTHING
    } else if (orientation == UIImageOrientationUp) {
        CGContextRotateCTM (context, [self radians:0]);
    }
    
    return UIGraphicsGetImageFromCurrentImageContext();
    
}

- (CGFloat) radians:(int)degree {
    return (degree/180)*(22/7);
}


- (UIImage *)imageWithImage:(UIImage *)image
{
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    CGSize newSize = image.size;
    
    CGFloat aspectRatio = newSize.width / newSize.height;
    
    if (newSize.width >= newSize.height)
    {
        //landscape or square, set width to 1280
        newSize.width = 1280;
        newSize.height = newSize.width * aspectRatio;
    }
    else
    {
        //portrait
        newSize.height = 1280;
        newSize.width = newSize.height * aspectRatio;
    }
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
