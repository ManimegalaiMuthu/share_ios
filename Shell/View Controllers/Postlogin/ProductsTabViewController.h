//
//  ProductsTabViewController.h
//  Shell
//
//  Created by Edenred on 27/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "RDVTabBarController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductsTabViewController : RDVTabBarController

@end

NS_ASSUME_NONNULL_END
