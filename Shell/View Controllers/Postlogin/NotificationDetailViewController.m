//
//  NotificationDetailViewController.m
//  Shell
//
//  Created by Admin on 29/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "NotificationDetailViewController.h"
#import <WebKit/WebKit.h>

@interface NotificationDetailViewController () <WebServiceManagerDelegate, WKNavigationDelegate>
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIView *webViewContainer;
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;
@end

@implementation NotificationDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_NOTIFICATION) subtitle: @""];         //lokalise 1 feb
    
    [self setNavBackBtn];

    
    if (kIsRightToLeft) {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / Detail",LOCALIZATION_EN(C_TITLE_NOTIFICATION)] screenClass:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[WebServiceManager sharedInstance] readPushMessage:[self.msgData objectForKey: @"PushID"] vc: self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backPressed:(id)sender {
    //only available prelogin
    [self popSelf];
}

- (IBAction)deletePressed:(id)sender {
    [[WebServiceManager sharedInstance] removePushMessageWithList: @[@{@"PushID": [self.msgData objectForKey: @"PushID"]},] vc: self];
    
//    [self popUpYesNoWithTitle: LOCALIZATION(@"Delete Notifications") content: LOCALIZATION(@"Are you sure you want to delete?") yesBtnTitle: LOCALIZATION(C_FORM_YES)  noBtnTitle: LOCALIZATION(C_FORM_NO) onYesPressed:^(BOOL finished) {
//        [[WebServiceManager sharedInstance] removePushMessageWithList: @[@{@"PushID": self.pushId},] vc: self];
//    } onNoPressed:^(BOOL finished) {
//
//    }];
}

#pragma WKWebView delegate methods
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    [UpdateHUD addMBProgress: self.view withText:LOADING_HUD];
}

-(void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    NSLog(@"failed to load navigation");
    [UpdateHUD removeMBProgress: self.view];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [UpdateHUD removeMBProgress: self.view];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnDelete];
   
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    if(navigationAction.navigationType ==  WKNavigationTypeLinkActivated) {
        if(![[self.msgData objectForKey:@"ReferenceKey"] isKindOfClass:[NSNull class]]) {
            if ([[self.msgData objectForKey:@"ReferenceKey"] isEqualToString: @"IAB"])  // allow navigation only if key is IAB (In App Browser)
            {
                [mSession pushInAppBrowserWithAddress: navigationAction.request.URL.absoluteString vc: self];
                decisionHandler(WKNavigationActionPolicyCancel);
            }
        } else {
           [[UIApplication sharedApplication] openURL: navigationAction.request.URL];
            
            decisionHandler(WKNavigationActionPolicyCancel);
            return;
        }
        return;
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
    return;
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_READNOTIFICATION:
        {
#pragma mark - Adding WKWebView
            self.msgData = [response getGenericResponse];
            NSString *htmlBody = [NSString stringWithFormat: @"<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>%@%@", [self.msgData objectForKey:@"Header"], [self.msgData objectForKey:@"Message"]];
            
            WebViewHelper *webViewHelper = [[WebViewHelper alloc] init];
            [webViewHelper setWebViewWithParentView: self.webViewContainer
                                 navigationDelegate: self
                                           htmlBody: htmlBody];
            //    [webViewHelper setWebConfiguration: WKDataDetectorTypeLink | WKDataDetectorTypePhoneNumber];  //default
        
        }
            break;
        case kWEBSERVICE_DELETENOTIFICATION:
        {
            [self popSelf];
            
        }
            break;
        default:
            break;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
