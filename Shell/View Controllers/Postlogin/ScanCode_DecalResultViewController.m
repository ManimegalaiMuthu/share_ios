//
//  ScanCode_DecalResultViewController.m
//  Shell
//
//  Created by Jeremy Lua on 25/5/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "ScanCode_DecalResultViewController.h"
#define CELL_HEIGHT 70

@interface ScanCode_DecalResultViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnContinueTagging;

@property NSArray *couponArray;

@end

@implementation ScanCode_DecalResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_DECAL_ACTIVATEDECAL) subtitle: @"" size: 15 subtitleSize: 0];  //lokalise 24 Jan
    [self.btnBack setTintColor: COLOUR_RED];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    self.tableView.tableFooterView = [UIView new];
    
    [self.btnContinueTagging.titleLabel setFont: FONT_BUTTON];
    [self.btnContinueTagging setTitle: LOCALIZATION(C_BTN_CONTINUE_TAGGING) forState: UIControlStateNormal];
    [self.btnContinueTagging setBackgroundColor:COLOUR_RED];
    
    if (kIsRightToLeft) {
        [self.btnBack setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.btnBack setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
    
    self.couponArray = [self.resultsDict objectForKey: @"decalCodes"];
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_DECAL_ACTIVATEDECAL) screenClass:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

#pragma mark - tableview delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.couponArray count] > 0)
        return [self.couponArray count] + 1;
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (indexPath.row == 0) //header
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"PendingCodeHeader"];

        
        UILabel *lblSerial = [cell viewWithTag: 1];
        lblSerial.font = FONT_H2;
        lblSerial.text = LOCALIZATION(C_TABLE_NO);      //lokalise 24 Jan
        
        UILabel *lblCode = [cell viewWithTag: 3];
        lblCode.font = FONT_H2;
        lblCode.text = LOCALIZATION(C_TABLE_SCANNEDCODE);   //lokalise 24 Jan
        
        UILabel *lblStatus = [cell viewWithTag: 5];
        lblStatus.font = lblCode.font;
        lblStatus.text = LOCALIZATION(C_RESULT_STATUS);     //lokalise 24 Jan
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"PendingCodeCell"];
        
        NSDictionary *couponData = [self.couponArray objectAtIndex: indexPath.row-1];

        UILabel *lblSerial = [cell viewWithTag: 1];
        lblSerial.font = FONT_B2;
        lblSerial.text = @(indexPath.row).stringValue;
        
        UILabel *lblCode = [cell viewWithTag: 3];
        lblCode.text = [NSString stringWithFormat:@"%@",[couponData objectForKey: @"Codes"]];
        lblCode.font = FONT_B2;
        
        UILabel *lblStatus = [cell viewWithTag: 5];
        lblStatus.font = lblCode.font;
        lblStatus.text = [couponData objectForKey: @"Status"];
        
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
