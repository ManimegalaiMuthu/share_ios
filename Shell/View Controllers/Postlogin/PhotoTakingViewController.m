//
//  PhotoTakingViewController.m
//  Shell
//
//  Created by Ben on 26/11/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "PhotoTakingViewController.h"
#import <Foundation/Foundation.h>
#import "Constants.h"
#import "LocalizationManager.h"
//#import <Photos/Photos.h>   //For requesting from photos later
#import <AVFoundation/AVFoundation.h>

@protocol PhotoOverlayDelegate <NSObject>
-(void)takePicture;
-(void)loadLibrary;
-(void)cancel;
@end

@interface PhotoTakingOverlay: UIView
@property (weak, nonatomic) id<PhotoOverlayDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton* shootButton;
@property (weak, nonatomic) IBOutlet UIButton* libraryButton;
@property (weak, nonatomic) IBOutlet UIButton* closeButton;
@property (weak, nonatomic) IBOutlet UILabel* messageLabel;
@end

@implementation PhotoTakingOverlay

-(IBAction)takePhoto:(id)sender {
    [_delegate takePicture];
}

-(IBAction)loadLibrary:(id)sender {
    [_delegate loadLibrary];
}

-(IBAction)cancel:(id)sender {
    [_delegate cancel];
}
@end

@interface PhotoTakingViewController()<UINavigationControllerDelegate, UIImagePickerControllerDelegate, PhotoOverlayDelegate>
@property CGSize cropSize;
@property UIImagePickerController* cameraPicker;
@property UIImagePickerController* libraryPicker;
@property PhotoTakingOverlay* controlsOverlay;
@property (weak, nonatomic) IBOutlet UIButton* acceptButton;
@property (weak, nonatomic) IBOutlet UIButton* retakeButton;
@property (weak, nonatomic) IBOutlet UIImageView* preview;
@end

@implementation PhotoTakingViewController

- (void)viewDidLoad {
    
    self.navigationItem.hidesBackButton = true;
    self.cropSize = CGSizeMake(SCREEN_WIDTH-100, SCREEN_HEIGHT-200);
    
    
    self.acceptButton.titleLabel.font = FONT_BUTTON;
    self.acceptButton.backgroundColor = COLOUR_RED;
    [self.acceptButton setTitleColor:COLOUR_WHITE forState:UIControlStateNormal];
    self.acceptButton.hidden = YES;
    [self.acceptButton setTitle: LOCALIZATION(C_BTN_ACCEPT) forState: UIControlStateNormal];
    
    self.retakeButton.titleLabel.font = FONT_BUTTON;
    self.retakeButton.backgroundColor = COLOUR_RED;
    [self.retakeButton setTitleColor:COLOUR_WHITE forState:UIControlStateNormal];
    self.retakeButton.hidden = YES;
    [self.retakeButton setTitle: LOCALIZATION(C_BTN_RETAKE) forState: UIControlStateNormal];
    
    switch ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo])
    {
        case AVAuthorizationStatusAuthorized:
        {
            [self showImagePicker];
        }
            break;
            
        default:
        {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
             {
                 if(granted)
                 {
                     [self showImagePicker];
                     
                 }
                 else
                 {
                     UIAlertController *alertController = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_ALERTVIEW_ERROR) message:LOCALIZATION(C_ALERTVIEW_PERMISSIONERRORMSG) preferredStyle:UIAlertControllerStyleAlert];        //lokalised
                     
                     UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_CANCEL) style:UIAlertActionStyleCancel handler:nil];
                     UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_SETTING) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                                     UIApplicationOpenSettingsURLString]];
                     }];            //lokalised
                     
                     [alertController addAction:cancelAction];
                     [alertController addAction:settingsAction];
                     [self presentViewController:alertController animated:YES completion:nil];
                 }
             }];
            break;
        }
    }
}

-(void)showImagePicker {
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.cameraPicker = [[UIImagePickerController alloc] init];
            self.cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            self.cameraPicker.showsCameraControls = false;
            
            float cameraAspectRatio = 4.0/3.0;
            float imageWidth = floorf(SCREEN_WIDTH * cameraAspectRatio);
            float scale = ceilf((SCREEN_HEIGHT / imageWidth) * 10.0) / 10.0;
            self.cameraPicker.cameraViewTransform = CGAffineTransformMakeScale(scale, scale);
            
            self.controlsOverlay = [[NSBundle.mainBundle loadNibNamed:@"PhotoTakingControls" owner:self options:nil] firstObject];
            self.controlsOverlay.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
            self.controlsOverlay.shootButton.backgroundColor = COLOUR_RED;
            self.controlsOverlay.delegate = self;
            
            self.controlsOverlay.closeButton.titleLabel.font = FONT_BUTTON;
            
            
            UIView* overlay = [[UIView alloc]initWithFrame: CGRectMake((SCREEN_WIDTH-self.cropSize.width)/2,
                                                                       (SCREEN_HEIGHT-self.cropSize.height-100)/2,
                                                                       self.cropSize.width, self.cropSize.height)];
            if (self.showBorderOverlay)
            {
                CAShapeLayer *borderOverlay = [CAShapeLayer layer];
                borderOverlay.strokeColor = [UIColor whiteColor].CGColor;
                borderOverlay.fillColor = nil;
                borderOverlay.lineDashPattern = @[@15, @15]; //Line, Blank
                borderOverlay.frame = overlay.bounds;
                borderOverlay.path = [UIBezierPath bezierPathWithRect:overlay.bounds].CGPath;
                [overlay.layer addSublayer:borderOverlay];
            }
            
            if(self.message != nil && ![self.message isEqualToString:@""])
            {
                self.controlsOverlay.messageLabel.textColor = COLOUR_WHITE;
                self.controlsOverlay.messageLabel.font = FONT_B2;
                self.controlsOverlay.messageLabel.text = self.message;
                self.controlsOverlay.messageLabel.hidden = false;
            }
            
            [self.controlsOverlay addSubview:overlay];
            self.cameraPicker.cameraOverlayView = self.controlsOverlay;
            
            self.cameraPicker.delegate = self;
            [self presentViewController:self.cameraPicker animated:YES completion:nil];
            
            //for load library
            self.libraryPicker = [[UIImagePickerController alloc] init];
            self.libraryPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            self.libraryPicker.delegate = self;
            
            //default setting
            UINavigationBar *navigationBarAppearance = [UINavigationBar appearance];
            [navigationBarAppearance setTitleTextAttributes:@{NSFontAttributeName: FONT_H1, NSForegroundColorAttributeName:COLOUR_RED}];
        });
        
    }
    else
    {
        self.libraryPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        self.libraryPicker.delegate = self;
        [self presentViewController: self.libraryPicker animated:YES completion:nil];
        
        //default setting
        UINavigationBar *navigationBarAppearance = [UINavigationBar appearance];
        [navigationBarAppearance setTitleTextAttributes:@{NSFontAttributeName: FONT_H1, NSForegroundColorAttributeName:COLOUR_RED}];
    }
}

-(IBAction)accept:(id)sender {
    if(self.keyString == nil || [self.keyString isKindOfClass:[NSNull class]])
        self.keyString = @"";
    [_delegate imageTaken:_preview.image key: self.keyString];
    [self popSelf];
}

-(IBAction)retake:(id)sender {
    [self showImagePicker];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info {
    
    //assign picker image to memory
    UIImage *chosenImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    _preview.image = chosenImage;
    
    //to show buttons or not in retake vc
    self.acceptButton.hidden = !self.allowRetake;
    self.retakeButton.hidden = !self.allowRetake;
    
    if (!self.allowRetake)
    {
        [self accept: nil]; //send image without retaking
    }
    //dismiss all pickers regardless
    [self.cameraPicker dismissViewControllerAnimated:true completion:nil];
    [self.libraryPicker dismissViewControllerAnimated:true completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    if (picker == self.libraryPicker)
    {
        [self.libraryPicker dismissViewControllerAnimated:true completion:nil];
        
        self.controlsOverlay = nil;
        
        [self presentViewController: self.cameraPicker animated:false completion:^{
            self.controlsOverlay = [[NSBundle.mainBundle loadNibNamed:@"PhotoTakingControls" owner:self options:nil] firstObject];
            self.controlsOverlay.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
            self.controlsOverlay.shootButton.backgroundColor = COLOUR_RED;
            self.controlsOverlay.delegate = self;
            self.cameraPicker.cameraOverlayView = self.controlsOverlay;
        }];
        //        [self showImagePicker];
    }
    else
    {
        [picker dismissViewControllerAnimated:true completion:nil];
        [self popSelf];
    }
}

-(void) takePicture {
    [self.cameraPicker takePicture];
}

-(void) loadLibrary
{
    [self.cameraPicker dismissViewControllerAnimated:false completion:nil];
    [self presentViewController: self.libraryPicker animated:false completion:nil];
}

-(void)cancel {
    [self.cameraPicker dismissViewControllerAnimated:false completion:nil];
    [self.libraryPicker dismissViewControllerAnimated: NO completion: nil];
    [self popSelf];
}

@end
