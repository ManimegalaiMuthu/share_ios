//
//  ContactUsViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 18/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "ContactUsViewController.h"

enum kPopupSelection_Type
{
    kPopupSelection_Salutation = 1,
    kPopupSelection_Country,
    kPopupSelection_Enquiry,
};

enum kContactUs_TYPE
{
    kContactUs_SALUTATION,
    kContactUs_FIRSTNAME,
    kContactUs_LASTNAME,
    kContactUs_MOBNUM,
    kContactUs_EMAIL,
    kContactUs_ENQUIRY,
    kContactUs_MESSAGE,
    kContactUs_IMAGE,
};

#define kUserID             @"UserID"
#define kDSRCode            @"DSRCode"
#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kEmailAddress       @"EmailAddress"
#define kDOB                @"DOB"
#define kDOBMonth           @"DOBMonth"
#define kDOBDay             @"DOBDay"
#define kCountryCode        @"CountryCode"
#define kEnquiryType        @"EnquiryType"
#define kMessage            @"Message"
#define kImage              @"Image"

//#define CELL_HEIGHT          50
#define CELL_HEIGHT          72
#define TEXTVIEW_HEIGHT          150

@interface ContactUsViewController () <WebServiceManagerDelegate, UITableViewDelegate,UITableViewDataSource, CommonListDelegate, UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblContactUs;

@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewLayoutHeight;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnCall;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSMutableDictionary* contactUsDict;
@property NSMutableArray *contactUsArray;
@property NSDictionary* contactUsReloadDict;
@property (weak, nonatomic) IBOutlet UILabel *lblRequiredFields;

@property Member* member;

@end

@implementation ContactUsViewController
@synthesize listView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [UpdateHUD removeMBProgress: KEY_WINDOW];
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_CONTACTUS) subtitle: @""];                    //lokalise 17 Jan
    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_TITLE_CONTACTUS),  LOCALIZATION(C_REWARDS_BACK)];    //lokalise 17 Jan
    
    if ([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK])  //pre_login requires back button
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
        self.contactUsReloadDict = @{
                                     kSalutation:    LOCALIZATION(C_CONTACTUS_SALUTATION),  //lokalise 17 Jan
                                     kFirstName: @"",
                                     kLastName: @"",
                                     kCountryCode:    GET_COUNTRY_CODE,
                                     kMobileNumber: @"",
                                     
                                     kEmailAddress: @"",
                                     kEnquiryType : LOCALIZATION(C_CONTACTUS_ENQUIRY),  //lokalise 17 Jan
                                     kMessage:       @"",
                                     };
    }
    
    [self setupRegistrationForm];
    [self setupInterface];
    
    NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @"*" attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B3, }];
    
    NSMutableAttributedString *requiredFieldsAttrString = [[NSMutableAttributedString alloc] initWithString: LOCALIZATION(C_PROFILE_REQUIREDFIELDS) attributes:  @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B3, }];  //lokalise 17 Jan
    
    [asteriskAttrString appendAttributedString:  requiredFieldsAttrString];
    
    //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
    self.lblRequiredFields.attributedText = asteriskAttrString;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveLanguageChangedNotification:)
                                                 name:kNotificationLanguageChanged
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setupInterface)
                                                 name:@"Reloaded Lookup Table"
                                               object:nil];
    
    if (kIsRightToLeft) {
        self.lblRequiredFields.textAlignment = NSTextAlignmentLeft;
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        self.lblRequiredFields.textAlignment = NSTextAlignmentRight;
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear: animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver: self name:@"Reloaded Lookup Table" object:nil];
    
}

#pragma mark - Localization Methods
- (void) receiveLanguageChangedNotification:(NSNotification *) notification
{
    if ([notification.name isEqualToString:kNotificationLanguageChanged])
    {
        [self reloadLookupTable];
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_CONTACTUS) subtitle: @""];    //lokalise 17 Jan
        [[IQKeyboardManager sharedManager] setToolbarDoneBarButtonItemText: LOCALIZATION(C_FORM_DONE)];
        
        //save pre-filled data if any
        self.contactUsReloadDict = @{
                                     kSalutation:      [[self.contactUsDict objectForKey:kSalutation] getJsonValue],
                                     kFirstName:       [[self.contactUsDict objectForKey:kFirstName] getJsonValue],
                                     kLastName:        [[self.contactUsDict objectForKey:kLastName] getJsonValue],
                                     kCountryCode:     GET_COUNTRY_CODE,
                                     kMobileNumber:    [[self.contactUsDict objectForKey:kMobileNumber] getMobileNumberJsonValue],
                                     
                                     kEmailAddress:    [[self.contactUsDict objectForKey:kEmailAddress] getJsonValue],
                                     kEnquiryType :    [[self.contactUsDict objectForKey:kEnquiryType] getJsonValue],
                                     kMessage:         [[self.contactUsDict objectForKey:kMessage] getJsonValue],
                                     };
        
        [self setupRegistrationForm];
        [self setupInterface];
        [mLookupManager reloadLookup];

        [mSession updateAnalyticsUserProperty];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_CONTACTUS) screenClass:nil];
}

-(void) setupInterface
{
    //all header will have hotline at the back
    //e.g. "Please fill in the form to submit your enquiry. A customer service representative will be in touch with you shortly. Alternative, you can call SHARE hotline at" XXXXXXXXX.
    NSString *contactUsNumber = @"";
    if(!IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        contactUsNumber = [[mSession lookupTable] objectForKey: @"ContactUSCSNumber"];
        if (![contactUsNumber isKindOfClass: [NSNull class]]) {
            self.lblContactUs.text = [LOCALIZATION(C_CONTACTUS_HEADER) stringByReplacingOccurrencesOfString:@"#number#" withString:[[mSession lookupTable] objectForKey: @"ContactUSCSNumber"]];  //lokalise 17 Jan
            if(IS_COUNTRY(COUNTRYCODE_INDONESIA)) {
                NSString *headerString = [LOCALIZATION(C_CONTACTUS_HEADER) stringByReplacingOccurrencesOfString:@"#number#" withString:[[mSession lookupTable] objectForKey: @"ContactUSCSNumber"]];
                self.lblContactUs.text = [NSString stringWithFormat:@"%@ %@ %@",headerString,LOCALIZATION(C_CONTACTUS_REDEMPTION_HOTLINE_HEADER),[[mSession lookupTable] objectForKey: @"ContactUSEVNumber"]];
            }
        }
        else
            self.lblContactUs.text = [LOCALIZATION(C_CONTACTUS_HEADER) stringByReplacingOccurrencesOfString:@"#number#" withString:@""];  //lokalise 17 Jan
    } else {
        switch([[mSession profileInfo] businessType]) {
            case 1:{
                contactUsNumber = [[mSession lookupTable] objectForKey: @"ContactUSB2BCSNumber"];
                self.lblContactUs.text = [LOCALIZATION(C_CONTACTUS_HEADER) stringByReplacingOccurrencesOfString:@"#number#" withString:[[mSession lookupTable] objectForKey: @"ContactUSB2BCSNumber"]];
            }
                break;
            case 2:
            {
                contactUsNumber = [[mSession lookupTable] objectForKey: @"ContactUSB2CCSNumber"];
                self.lblContactUs.text = [LOCALIZATION(C_CONTACTUS_HEADER) stringByReplacingOccurrencesOfString:@"#number#" withString:[[mSession lookupTable] objectForKey: @"ContactUSB2CCSNumber"]];
            }
                break;
            case 3:
            {
                self.lblContactUs.text = [LOCALIZATION(C_CONTACTUS_HEADER) stringByReplacingOccurrencesOfString:@"#number#" withString:[NSString stringWithFormat:@"%@ %@ / %@ %@",LOCALIZATION(C_POPUP_B2B_SUPPORT), [[mSession lookupTable] objectForKey: @"ContactUSB2BCSNumber"],LOCALIZATION(C_POPUP_B2C_SUPPORT), [[mSession lookupTable] objectForKey: @"ContactUSB2CCSNumber"]]];
            }
                break;
            default:
            {
                 if (![contactUsNumber isKindOfClass: [NSNull class]])
                     self.lblContactUs.text = [LOCALIZATION(C_CONTACTUS_HEADER) stringByReplacingOccurrencesOfString:@"#number#" withString:[[mSession lookupTable] objectForKey: @"ContactUSCSNumber"]];
                 else
                     self.lblContactUs.text = [LOCALIZATION(C_CONTACTUS_HEADER) stringByReplacingOccurrencesOfString:@"#number#" withString:@""];
            }
                break;
        }
    }
    self.lblContactUs.font = FONT_B1;
    
    UIImage *callImage = GET_ISADVANCE ? [UIImage imageNamed:@"icon-call_blue"] : [UIImage imageNamed:@"icon-call"];
    [self.btnCall setImage:callImage forState:UIControlStateNormal];
    
    self.btnSubmit.backgroundColor = COLOUR_RED;
    self.btnSubmit.titleLabel.font = FONT_H1;
    [self.btnSubmit setTitle:LOCALIZATION(C_FORM_SUBMIT) forState:UIControlStateNormal];    //lokalise 17 Jan
    
    
    if (![GET_AUTHENTICATIONTOKEN isEqualToString: BLANK] && !IS_COUNTRY(COUNTRYCODE_RUSSIA) && ![contactUsNumber isEqualToString:@""])
    {
        UIBarButtonItem *callIcon = [[UIBarButtonItem alloc] initWithCustomView: self.btnCall];
        self.navigationItem.rightBarButtonItem = callIcon;
        
        if(@available(iOS 11, *)){
            [self.btnCall.widthAnchor constraintEqualToConstant: 30.0].active = YES;
            [self.btnCall.heightAnchor constraintEqualToConstant: 30.0].active = YES;
        }
    } else if(IS_COUNTRY(COUNTRYCODE_RUSSIA)){
        UIBarButtonItem *callIcon = [[UIBarButtonItem alloc] initWithCustomView: self.btnCall];
        self.navigationItem.rightBarButtonItem = callIcon;
        
        if(@available(iOS 11, *)){
            [self.btnCall.widthAnchor constraintEqualToConstant: 30.0].active = YES;
            [self.btnCall.heightAnchor constraintEqualToConstant: 30.0].active = YES;
        }
    }
    
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_COUNTRY)    //lokalise 17 Jan
                                                   list: [mSession getCountryDialingCodes]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    
    listView.delegate = self;

    [self.btnSubmit.titleLabel setFont: FONT_BUTTON];
    
    //get member details for autofill?
}

-(void) setupRegistrationForm
{
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationFormTextfieldCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormTextfieldCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationButtonCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationFormDropdownCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormDropdownCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationHeaderTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationLabelTextfieldTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationLabelTextfieldTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationMobileNumberCell" bundle:nil] forCellReuseIdentifier:@"RegistrationMobileNumberCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationTextfieldWithButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextfieldWithButtonCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RegistrationTextViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextViewTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ContactUsImageTableViewCell" bundle:nil] forCellReuseIdentifier:@"ContactUsImageTableViewCell"];
    
    self.contactUsDict = [[NSMutableDictionary alloc] init];
    
    if (![[mSession profileInfo] salutation] || [[[mSession profileInfo] salutation] isKindOfClass: [NSNull class]])
        [[mSession profileInfo] setSalutation: @" "];
    
    self.contactUsArray = [[NSMutableArray alloc] init];
    if ([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK])
    {
#pragma mark - Owner Profile Form setup
        [self.contactUsArray addObject: @{@"Title": LOCALIZATION(C_CONTACTUS_SALUTATION),   //lokalise 17 Jan
                                          @"Type": @(kREGISTRATION_DROPDOWN),
                                          @"Key": kSalutation,
                                          @"Value": LOCALIZATION(C_CONTACTUS_SALUTATION),   //lokalise 17 Jan
                                          @"Enabled": @([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK]),
                                          }];  //salutation
        if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
        {
            [self.contactUsArray addObject: @{@"Title": LOCALIZATION(C_CONSUMER_FULLNAME),  //lokalise 17 Jan
                                              @"Type": @(kREGISTRATION_TEXTFIELD),
                                              @"Key": kFirstName,
                                              @"Value": [self.contactUsReloadDict objectForKey:kFirstName], //full name use firstname
                                              @"Enabled": @([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK]),
                                              }]; //full name
            [self.contactUsArray addObject: @{@"Title": @"",
                                              @"Type": @(kREGISTRATION_TEXTFIELD),
                                              @"Key": kLastName,
                                              @"Value": @"",
                                              @"Enabled": @(NO),
                                              }]; //last name
        }
        else
        {
            [self.contactUsArray addObject: @{@"Title": LOCALIZATION(C_CONTACTUS_FIRSTNAME),  //lokalise 17 Jan
                                       @"Type": @(kREGISTRATION_TEXTFIELD),
                                       @"Key": kFirstName,
                                       @"Value": [self.contactUsReloadDict objectForKey:kFirstName],
                                       @"Enabled": @([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK]),
                                              }]; //first name
            [self.contactUsArray addObject: @{@"Title": LOCALIZATION(C_CONTACTUS_LASTNAME),  //lokalise 17 Jan
                                   @"Type": @(kREGISTRATION_TEXTFIELD),
                                   @"Key": kLastName,
                                   @"Value": [self.contactUsReloadDict objectForKey:kLastName],
                                   @"Enabled": @([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK]),
                                      }]; //last name
        }
    [self.contactUsArray addObject: @{@"Title": LOCALIZATION(C_CONTACTUS_MOBILE),  //lokalise 17 Jan
                                   @"Type": @(kREGISTRATION_MOBNUM),
                                   @"Key": kMobileNumber,
                                   @"CountryCode": [mSession convertCountryCodeToCountryName: GET_COUNTRY_CODE],
                                   @"Enabled": @([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK]),
                                   @"MobileNumber": [self.contactUsReloadDict objectForKey:kMobileNumber],
                                      }]; //Mobile number
    [self.contactUsArray addObject: @{@"Title": LOCALIZATION(C_CONTACTUS_EMAIL),  //lokalise 17 Jan
                                   @"Type": @(kREGISTRATION_TEXTFIELD),
                                   @"Key": kEmailAddress,
                                   @"Optional": @(YES),
                                   @"Keyboard": @(UIKeyboardTypeEmailAddress),
                                   @"Value": [self.contactUsReloadDict objectForKey:kEmailAddress],
                                   @"Enabled": @([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK]),
                                      }]; //email
    [self.contactUsArray addObject: @{@"Title": LOCALIZATION(C_CONTACTUS_ENQUIRY),  //lokalise 17 Jan
                                   @"Type": @(kREGISTRATION_DROPDOWN),
                                   @"Key": kEnquiryType,
                                   @"Value": [GET_AUTHENTICATIONTOKEN isEqualToString: BLANK]?[self.contactUsReloadDict objectForKey:kEnquiryType] : LOCALIZATION(C_DROPDOWN_ENQUIRY),
                                      }]; //enquiry
    [self.contactUsArray addObject: @{@"Title": LOCALIZATION(C_CONTACTUS_MESSAGE),  //lokalise 17 Jan
                                   @"Type": @(kREGISTRATION_TEXTVIEW),
                                   @"Key": kMessage,
                                   @"Value": [GET_AUTHENTICATIONTOKEN isEqualToString: BLANK] ? [self.contactUsReloadDict objectForKey:kMessage] : @"",
                                      }]; //message
                                 //New Part
      [self.contactUsArray addObject: @{@"Title":  LOCALIZATION(C_CONTACTUS_UPLOADIMAGE),  //lokalise 17 Jan
                                       @"Type": @(kREGISTRATION_IMAGEUPLOAD),
                                       @"Key": kImage,
                                       @"Value": @"",
                                        }];
    }
    else
    {
            self.contactUsArray = [[NSMutableArray alloc] initWithArray:
                                   @[
                                     @{@"Title": LOCALIZATION(C_CONTACTUS_ENQUIRY),  //lokalise 17 Jan
                                       @"Type": @(kREGISTRATION_DROPDOWN),
                                       @"Key": kEnquiryType,
                                       @"Value": [GET_AUTHENTICATIONTOKEN isEqualToString: BLANK]?[self.contactUsReloadDict objectForKey:kEnquiryType] : LOCALIZATION(C_DROPDOWN_ENQUIRY),
                                       }, //enquiry
                                     @{@"Title": LOCALIZATION(C_CONTACTUS_MESSAGE),  //lokalise 17 Jan
                                       @"Type": @(kREGISTRATION_TEXTVIEW),
                                       @"Key": kMessage,
                                       @"Value": [GET_AUTHENTICATIONTOKEN isEqualToString: BLANK] ? [self.contactUsReloadDict objectForKey:kMessage] : @"",
                                       }, //message
                                     //New Part
                                     @{@"Title": LOCALIZATION(C_CONTACTUS_UPLOADIMAGE), //lokalise 17 Jan
                                       @"Type": @(kREGISTRATION_IMAGEUPLOAD),
                                       @"Key": kImage,
                                       @"Value": @"",
                                       },
                                     ]
                                   ];
    }
    
    //set tableview height. 4 shorter cells
//    self.tableViewLayoutHeight.constant = CELL_HEIGHT * [self.contactUsArray count] + (105 - CELL_HEIGHT);
    //New height with Image
    NSInteger cellLength = (SCREEN_WIDTH - 20)/3;
    self.tableViewLayoutHeight.constant = CELL_HEIGHT * [self.contactUsArray count] + (TEXTVIEW_HEIGHT - CELL_HEIGHT) /*textview*/ + cellLength + 50;//50 is the header height
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
        self.tableViewLayoutHeight.constant -= CELL_HEIGHT;
    self.tableView.tableFooterView = [UIView new];
    [self.tableView reloadData];

}


- (IBAction)submitPressed:(id)sender {
    
    NSDictionary *contactFormData;
    
    if ([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK])
    {
        contactFormData            = @{
                                      kSalutation:    [mSession convertToSalutationKeyCode:[[self.contactUsDict objectForKey:kSalutation] getJsonValue]],
                                      kFirstName: [[self.contactUsDict objectForKey:kFirstName] getJsonValue],
                                      kLastName: [[self.contactUsDict objectForKey:kLastName] getJsonValue],
                                      kCountryCode:    GET_COUNTRY_CODE,
                                      kMobileNumber: [[self.contactUsDict objectForKey:kMobileNumber] getMobileNumberJsonValue],
                                      
                                      kEmailAddress: [[self.contactUsDict objectForKey:kEmailAddress] getJsonValue],
                                      kEnquiryType : [mSession convertToEnquiryTypeKeyCode: [[self.contactUsDict objectForKey:kEnquiryType] getJsonValue]],
                                      kMessage:       [[self.contactUsDict objectForKey:kMessage] getJsonValue],
                                      //new code
                                      @"contactImg" :  [[self.contactUsDict objectForKey:kImage] imageArray],
                                      };
    }
    else
    {
        contactFormData            = @{
                                       kSalutation:    [mSession convertToSalutationKeyCode: mSession.profileInfo.salutation],
                                       kFirstName: mSession.profileInfo.firstName,
                                       kLastName: mSession.profileInfo.lastName,
                                       kCountryCode:    GET_COUNTRY_CODE,
                                       kMobileNumber: mSession.profileInfo.mobileNumber,
                                       
                                       kEmailAddress: mSession.profileInfo.emailAddress,
                                       kEnquiryType : [mSession convertToEnquiryTypeKeyCode: [[self.contactUsDict objectForKey:kEnquiryType] getJsonValue]],
                                       kMessage:       [[self.contactUsDict objectForKey:kMessage] getJsonValue],
                                       //new code
                                       @"contactImg" :  [[self.contactUsDict objectForKey:kImage] imageArray],
                                       };
        
    }
    [[WebServiceManager sharedInstance] sendContactForm: contactFormData vc: self];
}

- (IBAction)backPressed:(id)sender {
    //only available prelogin
    [self popSelf];
}

#pragma mark - tableview delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.contactUsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *regFieldData = [self.contactUsArray objectAtIndex: indexPath.row];
    switch ([[regFieldData objectForKey: @"Type"] intValue])
    {
        case kREGISTRATION_TEXTFIELD:
        {
            RegistrationFormTextfieldCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormTextfieldCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [textfieldCell enableCell:[[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [textfieldCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [textfieldCell hideTopLabel: NO];
            
            [self.contactUsDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            [textfieldCell setTextfieldText: [regFieldData objectForKey: @"Value"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_DROPDOWN:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];

            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [dropdownCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [dropdownCell hideTopLabel: NO];

            if([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK])
                dropdownCell.button.tag = indexPath.row;
            else
                dropdownCell.button.tag = kContactUs_ENQUIRY;
            
            switch (dropdownCell.button.tag) {
                case kContactUs_SALUTATION:
                    [dropdownCell.button addTarget: self action: @selector(salutationPressed:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case kContactUs_ENQUIRY:
                    [dropdownCell.button addTarget: self action: @selector(enquiryPressed:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                default:
                    break;
            }
            //set initial value
            if ([regFieldData objectForKey: @"Value"])
                [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            
            //keep a reference of cell to submit json value later
            [self.contactUsDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            
            if (kIsRightToLeft) {
                [dropdownCell.button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
            } else {
                [dropdownCell.button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            }
            
            return dropdownCell;
        }
            break;
        case kREGISTRATION_BUTTON:
        {
            RegistrationButtonCell *buttonCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationButtonCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [buttonCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [buttonCell setTitle: [regFieldData objectForKey: @"Title"]];
            [buttonCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [buttonCell hideTopLabel: YES];
            
            [self.contactUsDict setObject: buttonCell forKey: [regFieldData objectForKey: @"Key"]];
            return buttonCell;
        }
            break;
        case kREGISTRATION_MOBNUM:
        {
            RegistrationMobileNumberCell *mobNumCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationMobileNumberCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [mobNumCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
            
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [mobNumCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [mobNumCell hideTopLabel: NO];
            
            mobNumCell.mobileButton.tag = indexPath.row;
            [mobNumCell.mobileButton addTarget: self action: @selector(countryCodePressed:) forControlEvents:UIControlEventTouchUpInside];
            
            [mobNumCell setCountryCode: [regFieldData objectForKey: @"CountryCode"]];
            [mobNumCell setMobileNumber: [regFieldData objectForKey: @"MobileNumber"]];
            
            [mobNumCell setMobileCodeEnabled: NO];
            [mobNumCell setTextfieldEnabled:[[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [self.contactUsDict setObject: mobNumCell forKey: [regFieldData objectForKey: @"Key"]];
            return mobNumCell;
        }
            break;
        case kREGISTRATION_TEXTVIEW:
        {
            RegistrationTextViewTableViewCell *textViewCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationTextViewTableViewCell"];
            if ([regFieldData objectForKey: @"Keyboard"])
                [textViewCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textViewCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            if ([regFieldData objectForKey: @"Enabled"])
                [textViewCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [textViewCell setTitle: [regFieldData objectForKey: @"Title"]];

            
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [textViewCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_SHELLRED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [textViewCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [textViewCell hideTopLabel: NO];

            [textViewCell setTextviewDelegate: self];
            
            [self.contactUsDict setObject: textViewCell forKey: [regFieldData objectForKey: @"Key"]];
            return textViewCell;
        }
            break;
        case kREGISTRATION_IMAGEUPLOAD:{
            ContactUsImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactUsImageTableViewCell"];
            [self.contactUsDict setObject: cell forKey: [regFieldData objectForKey: @"Key"]];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
            break;
        default:
            NSLog(@"UNKNOWN CELL TYPE");
            return [UITableViewCell new];
            break;
    }
    return [UITableViewCell new];
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    RegistrationTextViewTableViewCell *textViewCell = [self.contactUsDict objectForKey: kMessage];

    [textViewCell didBeginEditing];
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    RegistrationTextViewTableViewCell *textViewCell = [self.contactUsDict objectForKey: kMessage];
    
    [textViewCell didEndEditing];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *fieldDataDict = [self.contactUsArray objectAtIndex: indexPath.row];
    
    if ([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK])
    {
        if([[fieldDataDict objectForKey: @"Key"] isEqualToString: kMessage])
            return CELL_HEIGHT + (TEXTVIEW_HEIGHT - CELL_HEIGHT);
        else if([[fieldDataDict objectForKey: @"Key"] isEqualToString: kImage])
        {
            NSInteger cellLength = (SCREEN_WIDTH -20)/3;
            return cellLength + 50;
        }
        else if([[fieldDataDict objectForKey: @"Key"] isEqualToString: kLastName] &&
                [GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
            return 0;
//        if(indexPath.row == kContactUs_MESSAGE)
//        else if(indexPath.row == kContactUs_IMAGE){
//            NSInteger cellLength = (SCREEN_WIDTH -20)/3;
//            return cellLength + 50;
//    //        return 0;
//        }
    }
    else
    {
        if(indexPath.row == kContactUs_MESSAGE - kContactUs_ENQUIRY)
            return CELL_HEIGHT + (TEXTVIEW_HEIGHT - CELL_HEIGHT);
        else if(indexPath.row == kContactUs_IMAGE - kContactUs_ENQUIRY){
            NSInteger cellLength = (SCREEN_WIDTH -20)/3;
            return cellLength + 50;
            //        return 0;
        }
    }
    return CELL_HEIGHT;
}


-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        RegistrationFormDropdownCell *cell;
        switch (listView.tag)
        {
            case kPopupSelection_Salutation:
                cell = [self.contactUsDict objectForKey: kSalutation];
                break;
                
            case kPopupSelection_Country:
            {
                RegistrationMobileNumberCell *mobileCell =  [self.contactUsDict objectForKey: kMobileNumber];
                
                NSString *countryName = [mSession convertDialCodeToCountryName: selection.firstObject];
                [mobileCell.mobileButton setTitle: countryName forState:UIControlStateNormal];
                
                //have to be before set language or country will not be reloaded properly
                SET_COUNTRY_CODE([mSession convertDialCodeToCountryCode: selection.firstObject]);
                [[NSUserDefaults standardUserDefaults] synchronize];

                [CrashlyticsKit setObjectValue: GET_COUNTRY_CODE forKey: @"Country Code"]; //identifier as country code

                [mSession setDefaultLanguage];
                
                [mLookupManager reloadLookup];
                return;
            }
                break;
            case kPopupSelection_Enquiry:
                cell = [self.contactUsDict objectForKey: kEnquiryType];
                break;
    
        }
        [cell.button setTitle:selection.firstObject forState:UIControlStateNormal];
    }
}

- (IBAction)salutationPressed:(UIButton *)sender {
    
    listView.tag = kPopupSelection_Salutation;
    if ([[mSession getSalutations] count] > 0)
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO];  //lokalise 17 Jan
    
        [self popUpView];
    }
    else
    {
        [self reloadLookupTable];
    }
}

- (IBAction)enquiryPressed:(UIButton *)sender {
    
    listView.tag = kPopupSelection_Enquiry;
    
    if ([[mSession getEnquiryType] count] > 0)
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_ENQUIRY) list: [mSession getEnquiryType] hasSearchField: NO];  //lokalise 17 Jan
        
        [self popUpView];
    }
    else
    {
        [self reloadLookupTable];
    }
}

- (IBAction)countryCodePressed:(UIButton *)sender {
    listView.tag = kPopupSelection_Country;
    
    if ([[mSession getCountryDialingCodes] count] > 0)
    {
        [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_COUNTRY) list: [mSession getCountryDialingCodes] hasSearchField: NO];  //lokalise 17 Jan
        [self popUpView];
    }
    else
    {
        [self reloadLookupTable];
    }
}

-(void) reloadLookupTable
{
    NSString *languageCodeString  = [NSString stringWithFormat: @"%lu", (unsigned long)[kAvailableLanguages indexOfObject: GET_LOCALIZATION]];
    NSDictionary *requestData = @{@"CountryCode": GET_COUNTRY_CODE,
                                  @"LanguageCode": languageCodeString,
                                  @"LookUpVersion": [mSession.lookupTable objectForKey: @"LookUpVersion"] ? [mSession.lookupTable objectForKey: @"LookUpVersion"] : @"1501065350"};
    
    [[WebServiceManager sharedInstance] requestLookupTableInVC: requestData
                                                            vc:self];
}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}



- (IBAction)callPressed:(id)sender {
    NSString *csNumberString =  [[mSession lookupTable] objectForKey: @"ContactUSCSNumber"];
    
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        switch([[mSession profileInfo] businessType]) {
            case 1:{
                csNumberString =  [[mSession lookupTable] objectForKey: @"ContactUSB2BCSNumber"];
                NSString *callUrlString = [NSString stringWithFormat: @"tel:%@",
                                           [csNumberString stringByReplacingOccurrencesOfString: @" " withString: @""]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: callUrlString]];
            }
                break;
            case 2:
            {
                csNumberString =  [[mSession lookupTable] objectForKey: @"ContactUSB2CCSNumber"];
                NSString *callUrlString = [NSString stringWithFormat: @"tel:%@",
                                           [csNumberString stringByReplacingOccurrencesOfString: @" " withString: @""]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: callUrlString]];
            }
                break;
            case 3:
            {
                [self popupCustomerService];
            }
                break;
            default:
                break;
        }
    } else {
        //need to remove space
        NSString *callUrlString = [NSString stringWithFormat: @"tel:%@",
                                   [csNumberString stringByReplacingOccurrencesOfString: @" " withString: @""]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: callUrlString]];
    }
    
}


-(void)processCompleted:(WebServiceResponse *)response
{
    NSLog(@"success");
    switch(response.webserviceCall)
    {
        case kWEBSERVICE_FETCHHOME:
        {
            [self setupRegistrationForm];
            [self setupInterface];
        }
            break;
        case kWEBSERVICE_CONTACTUS:
        {
            [UpdateHUD removeMBProgress:self.view];
            [mAlert showWebserviceAlertWithAlert: response.alert onCompletion:^(BOOL finished) {
                if ([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK])  //pre_login requires back button
                {
                    self.view.userInteractionEnabled = NO;
                    [self performSelector:@selector(backPressed:) withObject:nil afterDelay:0.5f];
                }
                else
                {
                    [mSession loadContactView];
                }
            }];
        }
         break;
        case kWEBSERVICE_LOOKUPTABLE:
        {
            NSDictionary *lookupDict = [response getGenericResponse];
            
            if ([[lookupDict objectForKey: @"LoadData"] boolValue])
            {
                SET_LOOKUP_TABLE(lookupDict);
                mSession.lookupTable = lookupDict;
                SET_LANGUAGE_TABLE([mSession.lookupTable objectForKey: @"LanguageList"]);
                mSession.languageTable = [mSession.lookupTable objectForKey: @"LanguageList"];
                
                [[NSNotificationCenter defaultCenter] postNotificationName: @"Reloaded Lookup Table" object: self];
            }
            
            switch (listView.tag)
            {
                case kPopupSelection_Country:
                    [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_COUNTRY) list: [mSession getCountryDialingCodes] hasSearchField: NO];  //lokalise 17 Jan
                    break;
                case kPopupSelection_Salutation:
                    [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO];  //lokalise 17 Jan
                    break;
                case kPopupSelection_Enquiry:
                    [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_ENQUIRY) list: [mSession getEnquiryType] hasSearchField: NO];  //lokalise 17 Jan
                    break;
                default:
                    break;
            }
            [self popUpView];
        }
            break;
        default:
            break;
    }
}

- (void)processFailed:(WebServiceResponse *)response
{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
