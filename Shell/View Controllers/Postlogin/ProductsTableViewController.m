//
//  ProductsTableViewController.m
//  Shell
//
//  Created by Edenred on 27/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "ProductsTableViewController.h"
#import "ShareProductTableViewCell.h"
#import "ProductTableViewCell.h"
#import "ProductsTabViewController.h"
#import "ProductGroupDetailsModel.h"

#define CELL_HEIGHT 48

static int const kHeaderSectionTag = 6900;


@interface ProductsTableViewController () <WebServiceManagerDelegate,UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

//Footer View - TH
@property (weak, nonatomic) IBOutlet UIView *thFooterView;
@property (weak, nonatomic) IBOutlet UILabel *lblFooter;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *thFooterViewHeight;

@property NSArray *response;
@property (assign) NSInteger expandedSectionHeaderNumber;
@property (assign) UITableViewHeaderFooterView *expandedSectionHeader;

@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@property BOOL isVersion3;

@end

@implementation ProductsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.expandedSectionHeaderNumber = -1;
    
    if(IS_COUNTRY(COUNTRYCODE_THAILAND)) {
        LoginInfo *loginInfo = [mSession loadUserProfile];
        if ([[loginInfo companyType] isEqualToString:@"32768"] || [[loginInfo companyType] isEqualToString:@"65536"]) {
            self.thFooterView.hidden = NO;
            self.thFooterViewHeight.constant = 30.0;

            self.lblFooter.text = LOCALIZATION(C_PARTICIPATINGPRODUCTS_TH_FOOTER);
            self.lblFooter.font = FONT_B3;
            self.lblFooter.textColor = COLOUR_RED;
        } else {
            self.thFooterView.hidden = YES;
            self.thFooterViewHeight.constant = 0;
        }
    } else {
        self.thFooterView.hidden = YES;
        self.thFooterViewHeight.constant = 0;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    self.isVersion3 = NO;
    
    if (![self.parentViewController.parentViewController isKindOfClass: [ProductsTabViewController class]] && [[mSession profileInfo] isParticipatingProductsOpt3])
    {
        [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_PARTICIPATINGPRODUCT) subtitle:@"" size:15 subtitleSize:0];  //lokalised 11 Feb
        if([[mSession profileInfo]hasParticipatingAllProducts]) {
            [[WebServiceManager sharedInstance] fetchAllParticipatingProducts: self];
            [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_PARTICIPATINGPRODUCT),LOCALIZATION_EN(C_TITLE_ALLPRODUCT)] screenClass:nil];
        } else {
            self.fetchShareProducts = YES;
            [[WebServiceManager sharedInstance] fetchShareProducts: self];
            [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_PARTICIPATINGPRODUCT),LOCALIZATION_EN(C_TITLE_SHAREPRODUCT)] screenClass:nil];
        }
        
    } else if([[mSession profileInfo] isParticipatingProductsOpt3]){
        if (self.fetchShareProducts) {
            [[WebServiceManager sharedInstance] fetchShareProducts: self];
            [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_PARTICIPATINGPRODUCT),LOCALIZATION_EN(C_TITLE_SHAREPRODUCT)] screenClass:nil];
        } else {
            [[WebServiceManager sharedInstance] fetchAllParticipatingProducts: self];
            [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_PARTICIPATINGPRODUCT),LOCALIZATION_EN(C_TITLE_ALLPRODUCT)] screenClass:nil];
        }
    } else if([[mSession profileInfo] isParticipatingProductsOpt3]) {
        [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_ALLPRODUCT) subtitle:@"" size:15 subtitleSize:0];
        [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_PARTICIPATINGPRODUCT),LOCALIZATION_EN(C_TITLE_ALLPRODUCT)] screenClass:nil];
        
        if (kIsRightToLeft) {
            [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
        } else {
            [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
        }
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
        
        self.response = self.selectedProductArray;
        self.isVersion3 = YES;
        [self setUpInterface];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) processCompleted:(WebServiceResponse *)response {
    switch (response.webserviceCall) {
        case kWEBSERVICE_FETCHSHAREPRODUCTS :
            _response = [response getGenericResponse];
            [self setUpInterface];
            break;
        case kWEBSERVICE_FETCHALLPARTICIPATINGPRODUCTS:
            _response = [response getGenericResponse];
            [self setUpInterface];
            break;
        default:
            break;
    }
}
- (IBAction)backPressed:(UIButton *)sender {
    [self popSelf];
}

-(void) setUpInterface {
    [self.tableView registerNib:[UINib nibWithNibName:@"ProductTableViewCell" bundle:nil] forCellReuseIdentifier:@"ProductTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ShareProductTableViewCell" bundle:nil] forCellReuseIdentifier:@"ShareProductTableViewCell"];
    
    
    [self.tableView reloadData];

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _response.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.expandedSectionHeaderNumber == section) {
        NSArray *numberOfProducts;
        if(self.isVersion3) {
            ProductGroupDetailsModel *cellData = [_response objectAtIndex:section];
            numberOfProducts = [cellData prodDetails];
        } else {
            numberOfProducts = [[_response objectAtIndex:section] objectForKey:@"ProductGroupDetails"];
        }
        
        if (_fetchShareProducts) {
            return numberOfProducts.count + 1;
        } else {
            return numberOfProducts.count;
        }
        
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_fetchShareProducts) {
        ShareProductTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ShareProductTableViewCell" ];
        if(indexPath.row == 0) {
            cell.productnameLbl.text = LOCALIZATION(C_TITLE_PRODUCTSTITLE); //lokalised 11 Feb
            cell.productnameLbl.font = FONT_H2;
            cell.productnameLbl.textColor = COLOUR_VERYDARKGREY;
            cell.RefPtsLbl.numberOfLines = 0;
            cell.WalkinPtsLbl.numberOfLines = 0;
            cell.RefPtsLbl.text = LOCALIZATION(C_TITLE_PRODUCTSTITLE_REFPTS);   //lokalised 11 Feb
            cell.RefPtsLbl.hidden = YES;
            cell.WalkinPtsLbl.text = LOCALIZATION(C_POINTS_POINTS);
        } else {
            NSArray *productsAtSection = [[_response objectAtIndex:indexPath.section] objectForKey:@"ProductGroupDetails"];
            NSDictionary *productDetails = [productsAtSection objectAtIndex:indexPath.row - 1];
            cell.productnameLbl.font = FONT_B1;
            cell.productnameLbl.textColor = COLOUR_VERYDARKGREY;
            cell.productnameLbl.text = [productDetails objectForKey:@"ProductName"];
            cell.RefPtsLbl.hidden = YES;
            cell.WalkinPtsLbl.text = [NSString stringWithFormat:@"%0.1f",[[productDetails objectForKey:@"Points"]doubleValue]];
            UIView *boxBottomBorderView = [cell viewWithTag:300];
            if (indexPath.row == productsAtSection.count) {
                boxBottomBorderView.hidden = NO;
            } else {
                boxBottomBorderView.hidden = YES;
            }
        }
        cell.RefPtsLbl.font = cell.productnameLbl.font;
        cell.WalkinPtsLbl.font = cell.productnameLbl.font;
        cell.RefPtsLbl.textColor = cell.productnameLbl.textColor;
        cell.WalkinPtsLbl.textColor = cell.productnameLbl.textColor;
        
        
        return cell;
    } else {
        ProductTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ProductTableViewCell"];
        NSArray *productsAtSection;
        if(self.isVersion3) {
            ProductGroupDetailsModel *cellData = [_response objectAtIndex:indexPath.section];
            productsAtSection = [cellData prodDetails];
        } else {
            productsAtSection = [[_response objectAtIndex:indexPath.section] objectForKey:@"ProductGroupDetails"];
        }
        NSDictionary *productDetails = [productsAtSection objectAtIndex:indexPath.row];
        cell.productNameLbl.text = [productDetails objectForKey:@"ProductName"];
        cell.productNameLbl.textColor = COLOUR_VERYDARKGREY;
        
        UIView *boxBottomBorderView = [cell viewWithTag:300];
        if (indexPath.row == (productsAtSection.count - 1)) {
            boxBottomBorderView.hidden = NO;
        } else {
            boxBottomBorderView.hidden = YES;
        }
        
        return cell;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(self.isVersion3) {
        ProductGroupDetailsModel *cellData = [_response objectAtIndex:section];
        return [cellData productGroupName];
    }
     return [[_response objectAtIndex:section] objectForKey:@"ProductGroupName"];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section; {
    return CELL_HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(_expandedSectionHeaderNumber == indexPath.section && indexPath.row == 0 && _fetchShareProducts) {
        return CELL_HEIGHT;
    } else if (!_fetchShareProducts) {
        return 44.0;
    }
    return 44.0;
}


- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.contentView.backgroundColor = COLOUR_BACKGROUNDGREY;
    header.textLabel.textColor = COLOUR_DARKGREY;
    header.textLabel.font = FONT_B1;
    
    header.contentView.layer.borderColor = COLOUR_VERYPALEGREY.CGColor;
    header.contentView.layer.borderWidth = 1.0;
    
    UIImageView *viewWithTag = [self.view viewWithTag:kHeaderSectionTag + section];
    if (viewWithTag) {
        [viewWithTag removeFromSuperview];
    }
    // add the arrow image
    CGSize headerFrame = self.view.frame.size;
    UIImageView *theImageView;
    if(kIsRightToLeft) {
        theImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 13, 18, 18)];
    } else {
        theImageView = [[UIImageView alloc] initWithFrame:CGRectMake(headerFrame.width - 54, 13, 18, 18)];
    }
    theImageView.image = GET_ISADVANCE ? [UIImage imageNamed:@"btn_plus_blue"] : [UIImage imageNamed:@"btn_plus_red"];
    theImageView.tag = kHeaderSectionTag + section;
    [header addSubview:theImageView];

    // make headers touchable
    header.tag = section;
    UITapGestureRecognizer *headerTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderWasTouched:)];
    [header addGestureRecognizer:headerTapGesture];

}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 20.0;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *viewForFooter = [[UIView alloc]init];
    viewForFooter.backgroundColor = COLOUR_WHITE;
    return viewForFooter;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)updateTableViewRowDisplay:(NSArray *)arrayOfIndexPaths {
    [self.tableView beginUpdates];
    [self.tableView deleteRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation: UITableViewRowAnimationFade];
    [self.tableView endUpdates];
}

- (void)sectionHeaderWasTouched:(UITapGestureRecognizer *)sender {
    UITableViewHeaderFooterView *headerView = (UITableViewHeaderFooterView *)sender.view;
    NSInteger section = headerView.tag;
    UIImageView *eImageView = (UIImageView *)[headerView viewWithTag:kHeaderSectionTag + section];
    self.expandedSectionHeader = headerView;
    
    if (self.expandedSectionHeaderNumber == -1) {
        self.expandedSectionHeaderNumber = section;
        [self tableViewExpandSection:section withImage: eImageView];
    } else {
        if (self.expandedSectionHeaderNumber == section) {
            [self tableViewCollapeSection:section withImage: eImageView];
            self.expandedSectionHeader = nil;
        } else {
            UIImageView *cImageView  = (UIImageView *)[self.view viewWithTag:kHeaderSectionTag + self.expandedSectionHeaderNumber];
            [self tableViewCollapeSection:self.expandedSectionHeaderNumber withImage: cImageView];
            [self tableViewExpandSection:section withImage: eImageView];
        }
    }
}

- (void)tableViewCollapeSection:(NSInteger)section withImage:(UIImageView *)imageView {
    NSArray *sectionData;
    if(self.isVersion3) {
        ProductGroupDetailsModel *cellData = [_response objectAtIndex:section];
        sectionData = [cellData prodDetails];
    } else {
        sectionData = [[_response objectAtIndex:section] objectForKey:@"ProductGroupDetails"];
    }
   
    
    self.expandedSectionHeaderNumber = -1;
    if (sectionData.count == 0) {
        return;
    } else {
        [UIView animateWithDuration:0.4 animations:^{
            imageView.transform = CGAffineTransformMakeRotation(M_PI_2);
        }];
        NSMutableArray *arrayOfIndexPaths = [NSMutableArray array];
        for (int i=0; i< sectionData.count; i++) {
            NSIndexPath *index = [NSIndexPath indexPathForRow:i inSection:section];
            [arrayOfIndexPaths addObject:index];
        }
        if(_fetchShareProducts) {
            [arrayOfIndexPaths addObject:[NSIndexPath indexPathForRow:arrayOfIndexPaths.count inSection:section]];
        }
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation: UITableViewRowAnimationFade];
        [self.tableView endUpdates];

    }
}

- (void)tableViewExpandSection:(NSInteger)section withImage:(UIImageView *)imageView {
     NSArray *sectionData;
     if(self.isVersion3) {
         ProductGroupDetailsModel *cellData = [_response objectAtIndex:section];
         sectionData = [cellData prodDetails];
     } else {
         sectionData = [[_response objectAtIndex:section] objectForKey:@"ProductGroupDetails"];
     }
    
    if (sectionData.count == 0) {
        self.expandedSectionHeaderNumber = -1;
        return;
    } else {
        [UIView animateWithDuration:0.4 animations:^{
            imageView.transform = CGAffineTransformMakeRotation(M_PI_4);
            [imageView layoutIfNeeded];
        }];
        NSMutableArray *arrayOfIndexPaths = [NSMutableArray array];
        for (int i=0; i< sectionData.count; i++) {
            NSIndexPath *index = [NSIndexPath indexPathForRow:i inSection:section];
            [arrayOfIndexPaths addObject:index];
        }
        if(_fetchShareProducts) {
            [arrayOfIndexPaths addObject:[NSIndexPath indexPathForRow:arrayOfIndexPaths.count inSection:section]];
        }
        self.expandedSectionHeaderNumber = section;
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation: UITableViewRowAnimationFade];
        [self.tableView endUpdates];
    
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}


@end
