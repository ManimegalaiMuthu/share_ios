//
//  RegisterCodeResultViewController.h
//  Shell
//
//  Created by Admin on 7/9/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface RegisterCodeResultViewController : BaseVC

@property NSDictionary *resultsDict;
@property BOOL isScanBottle;
@property BOOL isConsumer;

@end
