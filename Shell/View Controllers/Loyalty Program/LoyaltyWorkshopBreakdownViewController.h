//
//  LoyaltyWorkshopBreakdownViewController.h
//  Shell
//
//  Created by Nach on 28/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoyaltyWorkshopBreakdownViewController : BaseVC

@property NSDictionary *breakdownDetails;
@property NSArray *workshopList;
@property NSString *contractRef;

@end

NS_ASSUME_NONNULL_END
