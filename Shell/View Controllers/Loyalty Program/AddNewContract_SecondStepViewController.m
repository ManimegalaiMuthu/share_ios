//
//  AddNewContract_SecondStepViewController.m
//  Shell
//
//  Created by Nach on 26/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "AddNewContract_SecondStepViewController.h"
#import "PopupInfoTableViewController.h"

#define PACKAGETIERSELECTIONVIEW_TABLEVIEWCELL @"PackageSelectionTableViewCell"
#define PREMIUMTIERSELECTION_COLLECTIONVIEWCELL @"PremiumTierSelectionCell"
#define PERIODSELECTION_COLLECTIONVIEWCELL @"PeriodSelectionCell"

typedef enum {
    kPACKAGETIERSELECTION_CHECKBOX = 1001,
    kPACKAGETIERSELECTION_CHECKBOX_ON,
    
    kPACKAGETIERSELECTION_LABEL = 1,
    
}kPACKAGETIERSELECTION_TAG;

typedef enum {
    
    kPREMIUMSELECTION_BUTTON1 = 900,
    kPREMIUMSELECTION_BUTTON2,
    kPREMIUMSELECTION_BUTTON3,
    
}kPREMIUMSELECTION_TAG;

typedef enum {
    
    kPERIODSELECTION_BUTTON1 = 1100,
    kPERIODSELECTION_BUTTON2,
    
}kPERIODSELECTION_TAG;

typedef enum
{
    kREWARDS_DELIVERY_OWNERNAME = 0,
    kREWARDS_DELIVERY_ADDRESSLINE1,
    kREWARDS_DELIVERY_ADDRESSLINE2,
    kREWARDS_DELIVERY_TOWN,
    kREWARDS_DELIVERY_STATE,
    kREWARDS_DELIVERY_POSTALCODE,
    kREWARDS_DELIVERY_MOBILENUMBER,
}kREWARDS_DELIVERY_TAG;



@interface AddNewContract_SecondStepViewController ()<WebServiceManagerDelegate,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate,UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet UIView *stepView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stepView_height;

@property (weak, nonatomic) IBOutlet UIView *workshopNameView;
@property (weak, nonatomic) IBOutlet UILabel *lblWorkshopName;
@property (weak, nonatomic) IBOutlet UILabel *lblWorkshopName_Value;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *workshopNameView_Height;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderView_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderView_Content;

@property (weak, nonatomic) IBOutlet UIView *packageTierSelectionView;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageTierSelectionView_Title;
@property (weak, nonatomic) IBOutlet UITableView *packageTierSelection_TableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *packageTierSelection_TableView_Height;

@property (weak, nonatomic) IBOutlet UIView *multiplierSelectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *multiplierSelectionView_Height;
@property (weak, nonatomic) IBOutlet UILabel *lblMultiplierSelectionView_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblSliderMin;
@property (weak, nonatomic) IBOutlet UILabel *lblSliderMax;
@property (weak, nonatomic) IBOutlet UISlider *horizontalSlider;

@property (weak, nonatomic) IBOutlet UIView *currentValueView;
@property (weak, nonatomic) IBOutlet UILabel *currentValueLabel;
@property (weak, nonatomic) IBOutlet UIImageView *currentValueTriangleImage;

@property (weak, nonatomic) IBOutlet UIView *tierViewTopBorder;

@property (weak, nonatomic) IBOutlet UILabel *lblPremiumTier_Title;
@property (weak, nonatomic) IBOutlet UIView *premiumTier_View;
@property (weak, nonatomic) IBOutlet UILabel *lblPeriodSelection_Title;
@property (weak, nonatomic) IBOutlet UIView *periodSelection_View;

@property (weak, nonatomic) IBOutlet UILabel *lblRewardsSelection_Rewards_Title;

@property (weak, nonatomic) IBOutlet UIView *vouchersView;
@property (weak, nonatomic) IBOutlet UIImageView *vouchersView_Image;
@property (weak, nonatomic) IBOutlet UILabel *vouchersView_Title;
@property (weak, nonatomic) IBOutlet UIView *vouchersView_CheckboxView;
@property (weak, nonatomic) IBOutlet UIView *vouchersView_CheckboxOnView;
@property (weak, nonatomic) IBOutlet UILabel *vouchersView_Value;


@property (weak, nonatomic) IBOutlet UIView *goldRewardView;
@property (weak, nonatomic) IBOutlet UIImageView *goldRewardView_Image;
@property (weak, nonatomic) IBOutlet UILabel *goldRewardView_Title;
@property (weak, nonatomic) IBOutlet UIView *goldRewardView_CheckboxView;
@property (weak, nonatomic) IBOutlet UIView *goldRewardView_CheckboxOnView;
@property (weak, nonatomic) IBOutlet UILabel *goldRewardsView_Value;

@property (weak, nonatomic) IBOutlet UILabel *lblVoucherSelection;
@property (weak, nonatomic) IBOutlet UIImageView *img_dropDown;
@property (weak, nonatomic) IBOutlet UIButton *btnVoucherSelection;

@property (weak, nonatomic) IBOutlet UILabel *lblRewardsDelivery;
@property (weak, nonatomic) IBOutlet UITableView *rewardsDelivery_TableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rewardsDelivery_TableView_Height;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnTnC;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblTnC;

@property (weak, nonatomic) IBOutlet UITextField *textMultiplierValue;

//ProgressNUmber Images
@property (weak, nonatomic) IBOutlet UIImageView *progressOneImageView;
@property (weak, nonatomic) IBOutlet UIImageView *progressTwoImageView;
@property (weak, nonatomic) IBOutlet UIView *progressBar_Progress;
@property (weak, nonatomic) IBOutlet UIView *tierAndRewardsView;

@property NSArray *packageTierSelectionArray;
@property NSMutableArray *packageTierSelection_CellHolder;
@property NSDictionary *rewardsDeliveryArray;

@property NSMutableDictionary *rewardsDeliveryArray_EnteredValues;
@property NSMutableArray *rewardsDeliveryFormDict;
@property NSArray *packageTierInfo;
@property NSArray *packageTierRewardInfo;

@property NSIndexPath *selectedPackageIndexPath;
@property int selectedPremiumTier;
@property int selectedPeriod;
@property int multiplier;
@property float multiplierValue;

@property BOOL isGold;
@property NSString *itemCode;
@property NSString *packageID;
@property NSString *packageTierID;
@property NSString *period;
@property NSString *premiumMinTarget;
@property NSString *rewardValue;
@property NSString *goldRewardValue;
@property NSString *voucherRewardValue;

@property BOOL periodSelection;

@property NSNumberFormatter *numberFormatter;
@property NSArray *stateList;

@property BOOL isStateDropDown;
@property BOOL isMultiplierSet;

@property NSDictionary *defaultPackageTier;
@property NSString *stateName;

@property NSArray *catalogueList;
@property BOOL isCatalogueListChanged;
@property NSString *multiplierItemCode;

//Package List
@property NSMutableArray *visiblePackageList;
@property NSMutableArray *nonVisiblePackageList;
@property NSDictionary *defaultPackage;

@property NSString *basePackageId;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *currentViewLeadingConstraint;


@end

@implementation AddNewContract_SecondStepViewController
@synthesize listView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.btnBack setTintColor: COLOUR_VERYDARKGREY];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    if([[GET_LOYALTYTYPE objectForKey:@"CallType"] isEqualToString:@"N"]) {
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_ADDNEWCONTRACT) subtitle:@""];                //lokalise 28 Feb
        
        self.workshopNameView.hidden = YES;
        self.workshopNameView_Height.constant = 0.0;
        
        self.stepView.hidden = NO;
        self.stepView_height.constant = 50.0;
    } else if([[GET_LOYALTYTYPE objectForKey:@"CallType"] isEqualToString:@"U"]) {
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_LOYALTY_UPDATEPROGRAM_TITLE) subtitle:@""];         //lokalise 8 Feb
        
        self.workshopNameView.hidden = NO;
        self.workshopNameView_Height.constant = 80.0;
        
        self.stepView.hidden = YES;
        self.stepView_height.constant = 0.0;
    } else if([[GET_LOYALTYTYPE objectForKey:@"CallType"] isEqualToString:@"E"]) {
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_LOYALTY_EXTENDPROGRAM_TITLE) subtitle:@""];         //lokalise 8 Feb
        
        self.workshopNameView.hidden = NO;
        self.workshopNameView_Height.constant = 80.0;
        
        self.stepView.hidden = YES;
        self.stepView_height.constant = 0.0;
    }
    
    _isStateDropDown = false;
    self.stateName = nil;
    
    _numberFormatter = [[NSNumberFormatter alloc] init];
    [_numberFormatter setGroupingSeparator:@","];
    [_numberFormatter setGroupingSize:3];
    [_numberFormatter setUsesGroupingSeparator:YES];
    
    if([[GET_LOYALTYTYPE objectForKey:@"CallType"] isEqualToString:@"N"]) {
        self.lblHeaderView_Title.text = LOCALIZATION(C_LOYALTY_CONFIRMPRGDETAILS);                  //lokalise 8 Feb
        self.lblHeaderView_Title.font = FONT_H1;
        self.lblHeaderView_Title.textColor = COLOUR_VERYDARKGREY;
        
        self.lblHeaderView_Content.text = LOCALIZATION(C_LOYALTY_CONFIRMDETAILS_CONTENT);
        self.lblHeaderView_Content.font = FONT_B1;
        self.lblHeaderView_Content.textColor = COLOUR_VERYDARKGREY;
    } else {
        self.lblHeaderView_Title.text = [NSString stringWithFormat:@"%@: %@",LOCALIZATION(C_LOYALTY_CONTRACTID),[GET_LOYALTYTYPE objectForKey:@"ContractReference"]];  //lokalise 7 Feb
        self.lblHeaderView_Title.font = FONT_H1;
        self.lblHeaderView_Title.textColor = COLOUR_VERYDARKGREY;
        
        self.lblHeaderView_Content.text = LOCALIZATION(C_LOYALTY_UPDATE_HEADER);                    //lokalise 8 Feb
        self.lblHeaderView_Content.font = FONT_B1;
        self.lblHeaderView_Content.textColor = COLOUR_VERYDARKGREY;
        
        self.lblWorkshopName.text = LOCALIZATION(C_LOYALTY_WORKSHOPNAME);                           //lokalise 8 Feb
        self.lblWorkshopName.textColor = COLOUR_VERYDARKGREY;
        self.lblWorkshopName.font = FONT_B1;
        
        self.lblWorkshopName_Value.text = [NSString stringWithFormat:@"%@",[GET_LOYALTYTYPE objectForKey:@"CompanyName"]];
        self.lblWorkshopName_Value.textColor = COLOUR_VERYDARKGREY;
        self.lblWorkshopName_Value.font = FONT_H1;
        
        self.period = [GET_LOYALTYTYPE objectForKey:@"Period"];
        self.packageID = [GET_LOYALTYTYPE objectForKey:@"PackageTierID"];
    }
    
    self.lblPackageTierSelectionView_Title.text = LOCALIZATION(C_LOYALTY_RECOMMENDEDPACKAGES);      //lokalise 8 feb
    self.lblPackageTierSelectionView_Title.font = FONT_B1;
    self.lblPackageTierSelectionView_Title.textColor = COLOUR_VERYDARKGREY;
    
    self.lblMultiplierSelectionView_Title.font = FONT_B1;
    self.lblMultiplierSelectionView_Title.textColor = COLOUR_VERYDARKGREY;
    
    self.lblSliderMin.textColor = COLOUR_VERYDARKGREY;
    self.lblSliderMin.font = FONT_B3;
    self.lblSliderMax.textColor = COLOUR_VERYDARKGREY;
    self.lblSliderMax.font = FONT_B3;
    self.currentValueLabel.font = FONT_B3;
    self.currentValueLabel.backgroundColor = COLOUR_RED;
    
    self.currentValueTriangleImage.image = GET_ISADVANCE ? [UIImage imageNamed:@"carousel_triangle_blue"] : [UIImage imageNamed:@"carousel_triangle_red"];
    [self.horizontalSlider setTintColor:COLOUR_RED];
    
    self.lblPremiumTier_Title.text = LOCALIZATION(C_LOYALTY_MINIMUMORDER);                           //lokalise 8 feb
    self.lblPremiumTier_Title.font = FONT_B1;
    self.lblPremiumTier_Title.textColor = COLOUR_VERYDARKGREY;
    
    self.lblPeriodSelection_Title.text = LOCALIZATION(C_LOYALTY_PROGRAMPERIOD);                      //lokalised
    self.lblPeriodSelection_Title.font = FONT_B1;
    self.lblPeriodSelection_Title.textColor = COLOUR_VERYDARKGREY;
    
    self.lblRewardsSelection_Rewards_Title.text = LOCALIZATION(C_LOYALTY_ADDNEW_REWARD);
    self.lblRewardsSelection_Rewards_Title.font = FONT_B1;
    self.lblRewardsSelection_Rewards_Title.textColor = COLOUR_VERYDARKGREY;
    
    self.vouchersView_Title.font = FONT_B1;
    self.vouchersView_Title.textColor = COLOUR_VERYDARKGREY;
    self.vouchersView_Value.font = FONT_B1;
    self.vouchersView_Value.textColor = COLOUR_VERYDARKGREY;
    self.goldRewardView_Title.font = FONT_B1;
    self.goldRewardView_Title.textColor = COLOUR_VERYDARKGREY;
    self.goldRewardsView_Value.font = FONT_B1;
    self.goldRewardsView_Value.textColor = COLOUR_VERYDARKGREY;
    
    self.lblRewardsDelivery.text = LOCALIZATION(C_LOYALTY_REWARDSDELIVERY);    //lokalise 8 feb
    self.lblRewardsDelivery.font = FONT_H1;
    self.lblRewardsDelivery.textColor = COLOUR_VERYDARKGREY;
    
    if([[GET_LOYALTYTYPE objectForKey:@"CallType"] isEqualToString:@"N"]) {
         [self.btnSubmit setTitle:LOCALIZATION(C_LOYALTY_SUBMIT) forState:UIControlStateNormal];    //lokalise 8 feb
    } else if([[GET_LOYALTYTYPE objectForKey:@"CallType"] isEqualToString:@"U"]) {
        [self.btnSubmit setTitle:LOCALIZATION(C_LOYALTY_UPDATE) forState:UIControlStateNormal];     //lokalise 8 feb
    } else if([[GET_LOYALTYTYPE objectForKey:@"CallType"] isEqualToString:@"E"]) {
       [self.btnSubmit setTitle:LOCALIZATION(C_LOYALTY_EXTEND) forState:UIControlStateNormal];      //lokalise 8 feb
    }
    self.btnSubmit.titleLabel.font = FONT_BUTTON;
    [self.btnSubmit setBackgroundColor:COLOUR_RED];
    
    self.packageTierInfo = [[NSArray alloc]init];
    self.packageTierRewardInfo = [[NSArray alloc]init];
    
    self.selectedPremiumTier = -1;
    
    self.multiplier = 1;
    self.multiplierValue = 1;
    
    self.textMultiplierValue.font = FONT_B2;
    self.textMultiplierValue.textColor = COLOUR_VERYDARKGREY;
    self.textMultiplierValue.delegate = self;
    self.textMultiplierValue.tag = 1200;
    
//    [[WebServiceManager sharedInstance] loadState: [[mSession lookupTable] objectForKey: @"LookUpVersion"]
//                                      countryCode: GET_COUNTRY_CODE
//                                               vc: self];
    
    self.vouchersView_CheckboxOnView.backgroundColor = COLOUR_RED;
    self.goldRewardView_CheckboxOnView.backgroundColor = COLOUR_RED;
    
    if(GET_ISADVANCE) {
        [self.progressOneImageView setImage: [UIImage imageNamed:@"progressnum-blue-1"]];
        [self.progressTwoImageView setImage: [UIImage imageNamed:@"progressnum-blue-2"]];
    } else {
        [self.progressOneImageView setImage: [UIImage imageNamed:@"progressnum-red-1"]];
        [self.progressTwoImageView setImage: [UIImage imageNamed:@"progressnum-red-2"]];
    }
    self.progressBar_Progress.backgroundColor = COLOUR_RED;
    
//    self.catalogueList = [_catalogueListArray objectAtIndex:0];
    self.isCatalogueListChanged = NO;
    self.itemCode = @"";
    
    [self setupInterface];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / Step 2",LOCALIZATION_EN(C_LOYALTY_ADDNEW_TITLE)] screenClass:nil];
}

-(void) setupInterface {
    
    listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_DROPDOWN_STATE)             //lokalised
                                                   list: [mSession getStateNames:self.stateList]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
    
    self.periodSelection = NO;
    self.isGold = NO;
    
    self.selectedPackageIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    self.packageTierSelection_CellHolder = [[NSMutableArray alloc]init];
    self.packageTierSelection_TableView.allowsSelection = YES;
    self.packageTierSelection_TableView.allowsMultipleSelection = NO;
    
    self.visiblePackageList = [[NSMutableArray alloc] init];
    self.nonVisiblePackageList = [[NSMutableArray alloc] init];
    self.packageTierSelectionArray = [self.responseData objectForKey:@"PackageInfo"];
    
    for(NSDictionary *packageDetails in self.packageTierSelectionArray) {
        if([[packageDetails objectForKey:@"IsVisible"] boolValue]) {
            [self.visiblePackageList addObject:packageDetails];
        } else {
            [self.nonVisiblePackageList addObject:packageDetails];
        }
    }
    
    self.rewardsDeliveryArray = [self.responseData objectForKey:@"RewardContactInfo"];
    self.packageTierInfo = [self.responseData objectForKey:@"PackageTierInfo"];
    
    if(self.packageTierInfo.count >= 1) {
        NSDictionary *firstPackageTier = [self.packageTierInfo objectAtIndex:0];
        if([[firstPackageTier objectForKey:@"PremiumMinTargetQty"] intValue] == 0) {
            self.premiumTier_View.hidden = YES;
            self.lblPremiumTier_Title.hidden = YES;
            [NSLayoutConstraint constraintWithItem:self.lblPeriodSelection_Title attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.lblPremiumTier_Title attribute:NSLayoutAttributeTop multiplier:1.0 constant:0].active = YES;
            [NSLayoutConstraint constraintWithItem:self.tierAndRewardsView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:260].active = YES;
        }
    }
    
    self.packageTierRewardInfo = [self.responseData objectForKey:@"PackageTierRewardInfo"];
    
    self.defaultPackageTier = nil;
    if(self.packageTierRewardInfo.count >= 1) {
        for(NSDictionary *packageTierDict in self.packageTierRewardInfo) {
            if([[packageTierDict objectForKey:@"IsDefault"] boolValue]) {
                self.defaultPackageTier = packageTierDict;
            }
        }
    }
    
    self.packageTierSelection_TableView_Height.constant = self.visiblePackageList.count * 35;
    self.rewardsDelivery_TableView_Height.constant = 0 * 72;
    
    self.packageTierSelection_TableView.tableFooterView = [UIView new];
    self.rewardsDelivery_TableView.tableFooterView = [UIView new];
    
    self.lblTnC.font = FONT_B1;
    self.lblTnC.textColor = COLOUR_VERYDARKGREY;
    
    NSString *tncString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:13; color: #404040;}</style>%@",[self.rewardsDeliveryArray objectForKey:@"TermsNConditions"]];

    self.lblTnC.attributedText = [[NSAttributedString alloc] initWithData: [tncString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil] ;
    
   
    
    
    [self.btnTnC setSelected:NO];
    
//    [self.rewardsDelivery_TableView registerNib:[UINib nibWithNibName:@"RegistrationFormTextfieldCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormTextfieldCell"];
//    [self.rewardsDelivery_TableView registerNib:[UINib nibWithNibName:@"RegistrationMobileNumberCell" bundle:nil] forCellReuseIdentifier:@"RegistrationMobileNumberCell"];
//    [self.rewardsDelivery_TableView registerNib:[UINib nibWithNibName:@"RegistrationFormDropdownCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormDropdownCell"];
    
    [self setupRewardDeliveryForm];
    
    if(self.defaultPackageTier != nil)
    {
        self.periodSelection = YES;
        int count = 0;
        for(NSDictionary *dict in self.packageTierSelectionArray) {
            if([[dict objectForKey:@"PackageID"] isEqualToString: [self.defaultPackageTier objectForKey:@"PackageID"]]){
                self.multiplier = [[dict objectForKey:@"Multiplier"] intValue];
                self.packageID = [self.defaultPackageTier objectForKey:@"PackageID"];
                self.selectedPackageIndexPath = [NSIndexPath indexPathForRow:count inSection:0];
                self.defaultPackage = dict;
                
                int tierInfoCount = 0;
                for(NSDictionary *packageTier in self.packageTierInfo) {
                    if([[packageTier objectForKey:@"PackageID"] isEqualToString:self.packageID]) {
                        if([[packageTier objectForKey:@"PackageTierID"] isEqualToString: [self.defaultPackageTier objectForKey:@"PackageTierID"]]) {
                            
                            self.selectedPremiumTier = 900 + tierInfoCount;
                            self.periodSelection = YES;
                            if([[self.defaultPackageTier objectForKey:@"Period"] isEqualToString:@"3"]) {
                                self.selectedPeriod = 1100;
                            } else {
                                self.selectedPeriod = 1101;
                            }
                            
                            break;
                        }
                        tierInfoCount += 1;
                    }
                }
            }
            count += 1;
        }
        [self packageSelection: self.packageID];
        
        if(![[GET_LOYALTYTYPE objectForKey:@"CallType"] isEqualToString:@"N"]) {
            if([[GET_LOYALTYTYPE objectForKey:@"HasMultiplier"] boolValue]) {
                for(NSDictionary *dict in self.packageTierSelectionArray) {
                    NSString *tierID = [GET_LOYALTYTYPE objectForKey:@"PackageTierID"];
                    if([[dict objectForKey:@"PackageTierID"] isEqualToString:tierID]) {
                        
                    }
                }
            }
        }
    }
}

-(void) setupRewardDeliveryForm {
    self.rewardsDeliveryArray_EnteredValues = [[NSMutableDictionary alloc]init];
    
    [self.rewardsDeliveryArray_EnteredValues setValue:[[self.rewardsDeliveryArray objectForKey:@"OwnerName"] isKindOfClass:[NSNull class]] || [self.rewardsDeliveryArray objectForKey:@"OwnerName"] == nil? @"" : [self.rewardsDeliveryArray objectForKey:@"OwnerName"] forKey:@"OwnerName"];
    
    [self.rewardsDeliveryArray_EnteredValues setValue:[[self.rewardsDeliveryArray objectForKey:@"Address1"] isKindOfClass:[NSNull class]] || [self.rewardsDeliveryArray objectForKey:@"Address1"] == nil? @"" : [self.rewardsDeliveryArray objectForKey:@"Address1"] forKey:@"Address1"];
    
    [self.rewardsDeliveryArray_EnteredValues setValue:[[self.rewardsDeliveryArray objectForKey:@"Address2"] isKindOfClass:[NSNull class]] || [self.rewardsDeliveryArray objectForKey:@"Address2"] == nil? @"" : [self.rewardsDeliveryArray objectForKey:@"Address2"] forKey:@"Address2"];
    
    [self.rewardsDeliveryArray_EnteredValues setValue:[[self.rewardsDeliveryArray objectForKey:@"City"] isKindOfClass:[NSNull class]] || [self.rewardsDeliveryArray objectForKey:@"City"] == nil? @"" : [self.rewardsDeliveryArray objectForKey:@"City"] forKey:@"City"];
    
    [self.rewardsDeliveryArray_EnteredValues setValue:[[self.rewardsDeliveryArray objectForKey:@"State"] isKindOfClass:[NSNull class]] || [self.rewardsDeliveryArray objectForKey:@"State"] == nil? @"" : [self.rewardsDeliveryArray objectForKey:@"State"] forKey:@"State"];
    
    [self.rewardsDeliveryArray_EnteredValues setValue:[[self.rewardsDeliveryArray objectForKey:@"PostalCode"] isKindOfClass:[NSNull class]] || [self.rewardsDeliveryArray objectForKey:@"PostalCode"] == nil? @"" : [self.rewardsDeliveryArray objectForKey:@"PostalCode"] forKey:@"PostalCode"];
    
    [self.rewardsDeliveryArray_EnteredValues setValue:[[self.rewardsDeliveryArray objectForKey:@"ContactNumber"] isKindOfClass:[NSNull class]] || [self.rewardsDeliveryArray objectForKey:@"ContactNumber"] == nil? @"" : [self.rewardsDeliveryArray objectForKey:@"ContactNumber"] forKey:@"ContactNumber"];
    
    
    
//    self.rewardsDeliveryFormDict = [[NSMutableArray alloc]initWithArray:@[
//                                                                          @{@"Title": LOCALIZATION(C_LOYALTY_OWNERNAME),        //lokalise 8 feb
//                                                                            @"Placeholder": LOCALIZATION(C_LOYALTY_OWNERNAME),  //lokalise 8 feb
//                                                                            @"Type": @(kREGISTRATION_TEXTFIELD),
//                                                                            @"Key": @"OwnerName",
//                                                                            @"Value": [[self.rewardsDeliveryArray objectForKey:@"OwnerName"] isKindOfClass:[NSNull class]] || [self.rewardsDeliveryArray objectForKey:@"OwnerName"] == nil? @"" : [self.rewardsDeliveryArray objectForKey:@"OwnerName"],
//                                                                            @"Enabled" : @(NO),
//                                                                            }, //Owner name
//                                                                          @{@"Title": LOCALIZATION(C_LOYALTY_ADDRESSLINE1),     //lokalise 8 feb
//                                                                            @"Placeholder": LOCALIZATION(C_LOYALTY_ADDRESSLINE1),//lokalise 8 feb
//                                                                            @"Type": @(kREGISTRATION_TEXTFIELD),
//                                                                            @"Key": @"Address1",
//                                                                            @"Value":[[self.rewardsDeliveryArray objectForKey:@"Address1"] isKindOfClass:[NSNull class]] || [self.rewardsDeliveryArray objectForKey:@"Address1"] == nil? @"" : [self.rewardsDeliveryArray objectForKey:@"Address1"],
//                                                                            @"Enabled" : @(YES),
//                                                                            }, //AddressLine 1
//                                                                          @{@"Title": LOCALIZATION(C_LOYALTY_ADDRESSLINE2),         //lokalise 8 feb
//                                                                            @"Placeholder": LOCALIZATION(C_LOYALTY_ADDRESSLINE2),   //lokalise 8 feb
//                                                                            @"Type": @(kREGISTRATION_TEXTFIELD),
//                                                                            @"Key": @"Address2",
//                                                                            @"Value": [[self.rewardsDeliveryArray objectForKey:@"Address2"] isKindOfClass:[NSNull class]] || [self.rewardsDeliveryArray objectForKey:@"Address2"] == nil ? @"" : [self.rewardsDeliveryArray objectForKey:@"Address2"],
//                                                                            @"Enabled" : @(YES),
//                                                                            }, //AddressLine2
//                                                                          @{@"Title": LOCALIZATION(C_PROFILE_TOWN),                    //lokalised
//                                                                            @"Placeholder": LOCALIZATION(C_PROFILE_TOWN),              //lokalised
//                                                                            @"Type": @(kREGISTRATION_TEXTFIELD),
//                                                                            @"Key": @"City",
//                                                                            @"Value": [[self.rewardsDeliveryArray objectForKey:@"City"] isKindOfClass:[NSNull class]] || [self.rewardsDeliveryArray objectForKey:@"City"] == nil ? @"" : [self.rewardsDeliveryArray objectForKey:@"City"],
//                                                                            @"Enabled" : @(YES),
//                                                                            }, //Town
//                                                                          @{@"Title": LOCALIZATION(C_PROFILE_STATE),                    //lokalised
//                                                                            @"Placeholder": LOCALIZATION(C_PROFILE_STATE),              //lokalised
//                                                                            @"Type": @(kREGISTRATION_DROPDOWN),
//                                                                            @"Key": @"State",
//                                                                            @"Value": self.stateName == nil ? @"" : self.stateName,
//                                                                            }, //State
//                                                                          @{@"Title": LOCALIZATION(C_PROFILE_POSTALCODE),               //lokalised
//                                                                            @"Placeholder": LOCALIZATION(C_PROFILE_POSTALCODE),         //lokalised
//                                                                            @"Type": @(kREGISTRATION_TEXTFIELD),
//                                                                            @"Key": @"PostalCode",
//                                                                            @"Value": [[self.rewardsDeliveryArray objectForKey:@"PostalCode"] isKindOfClass:[NSNull class]] || [self.rewardsDeliveryArray objectForKey:@"PostalCode"] == nil ? @"" : [self.rewardsDeliveryArray objectForKey:@"PostalCode"],
//                                                                            @"Enabled" : @(YES),
//                                                                            }, //PostalCode
//                                                                          @{@"Title": LOCALIZATION(C_PROFILE_MOBILENUM),                //lokalised
//                                                                            @"Placeholder": LOCALIZATION(C_PROFILE_MOBILENUM),          //lokalised
//                                                                            @"Type": @(kREGISTRATION_MOBNUM),
//                                                                            @"CountryCode": [mSession convertCountryCodeToCountryName: GET_COUNTRY_CODE],
//                                                                            @"Key": @"ContactNumber",
//                                                                            @"MobileNumber": [[self.rewardsDeliveryArray objectForKey:@"ContactNumber"] isKindOfClass:[NSNull class]] || [self.rewardsDeliveryArray objectForKey:@"ContactNumber"] == nil ? @"" : [self.rewardsDeliveryArray objectForKey:@"ContactNumber"],
//                                                                            @"TextfieldEnabled": @(NO),
//                                                                            @"Enabled" : @(YES),
//                                                                            }, //MobileNumber
//                                                                          ]];
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == self.packageTierSelection_TableView) {
        return self.visiblePackageList.count;
    } else {
        return 7;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView == self.packageTierSelection_TableView) {
        return 35.0;
    } else {
        return 72.0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView == self.packageTierSelection_TableView) {
        
        if([self.packageTierSelection_CellHolder count] < (indexPath.row + 1)){
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:PACKAGETIERSELECTIONVIEW_TABLEVIEWCELL forIndexPath:indexPath];
            
            UIView *checkboxView = [cell viewWithTag:kPACKAGETIERSELECTION_CHECKBOX];
            checkboxView.hidden = NO;
            
            UIView *checkboxView_ON = [cell viewWithTag:kPACKAGETIERSELECTION_CHECKBOX_ON];
            checkboxView_ON.backgroundColor = COLOUR_RED;
            
            if(indexPath.row == self.selectedPackageIndexPath.row) {
                checkboxView_ON.hidden = NO;
                self.packageID = [[self.visiblePackageList objectAtIndex:indexPath.row]objectForKey:@"PackageID"];
                if(_defaultPackageTier == nil) {
                    [self packageSelection:self.packageID];
                }
                self.basePackageId = [[self.visiblePackageList objectAtIndex:indexPath.row]objectForKey:@"PackageID"];
                self.multiplier = [[[self.visiblePackageList objectAtIndex:indexPath.row]objectForKey:@"Multiplier"] intValue];
            } else {
                checkboxView_ON.hidden = YES;
                if(self.selectedPackageIndexPath.row >= self.visiblePackageList.count && indexPath.row == (self.visiblePackageList.count - 1)) {
                    checkboxView_ON.hidden = NO;
                    self.packageID = [[self.visiblePackageList objectAtIndex:indexPath.row]objectForKey:@"PackageID"];
                    if(_defaultPackageTier == nil) {
                        [self packageSelection:self.packageID];
                    }
                    self.basePackageId = [[self.visiblePackageList objectAtIndex:indexPath.row]objectForKey:@"PackageID"];
                    self.multiplier = [[[self.visiblePackageList objectAtIndex:indexPath.row]objectForKey:@"Multiplier"] intValue];
                }
            }
            
            UILabel *packageName = [cell viewWithTag:kPACKAGETIERSELECTION_LABEL];
            packageName.text = [[self.visiblePackageList objectAtIndex:indexPath.row]objectForKey:@"PackageName"];
            packageName.font = FONT_B1;
            packageName.textColor = COLOUR_VERYDARKGREY;
            
            [self.packageTierSelection_CellHolder addObject:cell];
        } else {
            UITableViewCell *cell = [self.packageTierSelection_CellHolder objectAtIndex:indexPath.row];
            
            UIView *checkboxView_ON = [cell viewWithTag:kPACKAGETIERSELECTION_CHECKBOX_ON];
            
            if(indexPath.row == self.selectedPackageIndexPath.row) {
                checkboxView_ON.hidden = NO;
                self.packageID = [[self.visiblePackageList objectAtIndex:indexPath.row]objectForKey:@"PackageID"];
                self.multiplier = [[[self.visiblePackageList objectAtIndex:indexPath.row]objectForKey:@"Multiplier"] intValue];
                if(_defaultPackageTier == nil) {
                    [self packageSelection:self.packageID];
                }
            } else {
                checkboxView_ON.hidden = YES;
            }
        }
        
    } else {
        NSDictionary *regFieldData = [self.rewardsDeliveryFormDict objectAtIndex: indexPath.row];
        switch ([[regFieldData objectForKey: @"Type"] intValue]) {
            case kREGISTRATION_TEXTFIELD:
            {
                RegistrationFormTextfieldCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormTextfieldCell"];
                
                if ([regFieldData objectForKey: @"Keyboard"])
                    [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
                if ([regFieldData objectForKey: @"SecureEntry"])
                    [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
                if ([regFieldData objectForKey: @"Enabled"]){
                     [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
                     [textfieldCell enableCell:[[regFieldData objectForKey: @"Enabled"] boolValue]];
                }
                
                [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
                
                [textfieldCell hideTopLabel: NO];
                [textfieldCell setTextfieldDelegate:self];
                [textfieldCell setTextFieldTag:indexPath.row];
                
                if([self.rewardsDeliveryArray_EnteredValues objectForKey:[regFieldData objectForKey:@"Key"]] == nil || [[self.rewardsDeliveryArray_EnteredValues objectForKey:[regFieldData objectForKey:@"Key"]] isEqualToString:@""]) {
                    if ([regFieldData objectForKey: @"Value"])
                        [textfieldCell setTextfieldText: [regFieldData objectForKey: @"Value"]];
                    if ([regFieldData objectForKey: @"Placeholder"])
                        [textfieldCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
                     [self.rewardsDeliveryArray_EnteredValues setObject:[regFieldData objectForKey: @"Value"] forKey:[regFieldData objectForKey:@"Key"]];
                } else {
                    [textfieldCell setTextfieldText:[self.rewardsDeliveryArray_EnteredValues objectForKey:[regFieldData objectForKey:@"Key"]]];
                }
                
                return textfieldCell;
            }
                break;
            case kREGISTRATION_DROPDOWN:
            {
                RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
                if ([regFieldData objectForKey: @"Enabled"])
                    [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
                
                [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
                [dropdownCell hideTopLabel: NO];
                dropdownCell.button.tag = indexPath.row;
                
                [dropdownCell.button addTarget: self action: @selector(dropdownPressed:) forControlEvents:UIControlEventTouchUpInside];
                
                //set initial value
                if ([regFieldData objectForKey: @"Value"])
                    [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
                
                //keep a reference of cell to submit json value later
                if([self.rewardsDeliveryArray_EnteredValues objectForKey:[regFieldData objectForKey:@"Key"]] == nil || [[self.rewardsDeliveryArray_EnteredValues objectForKey:[regFieldData objectForKey:@"Key"]] isEqualToString:@""]) {
                    [self.rewardsDeliveryArray_EnteredValues setObject:[regFieldData objectForKey: @"Value"] forKey:[regFieldData objectForKey:@"Key"]];
                } else {
                    [dropdownCell setSelectionTitle:[self.rewardsDeliveryArray_EnteredValues objectForKey:[regFieldData objectForKey:@"Key"]]];
                }
                return dropdownCell;
            }
                break;
            case kREGISTRATION_MOBNUM: {
                RegistrationMobileNumberCell *mobNumCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationMobileNumberCell"];
                if ([regFieldData objectForKey: @"Enabled"])
                    [mobNumCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
                if ([regFieldData objectForKey: @"TextfieldEnabled"])
                    [mobNumCell setTextfieldEnabled: [[regFieldData objectForKey: @"TextfieldEnabled"] boolValue]];
                [mobNumCell setMobileCodeEnabled: YES];
                [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
                [mobNumCell setTextfieldDelegate:self];
                [mobNumCell setTextFieldTag:indexPath.row];
                [mobNumCell hideTopLabel: NO];
                
                [mobNumCell setCountryCode: [regFieldData objectForKey: @"CountryCode"]];
                [mobNumCell setMobileNumber: [regFieldData objectForKey: @"MobileNumber"]];
                
                if([self.rewardsDeliveryArray_EnteredValues objectForKey:[regFieldData objectForKey:@"Key"]] != nil) {
                    [mobNumCell setMobileNumber:[self.rewardsDeliveryArray_EnteredValues objectForKey:[regFieldData objectForKey:@"Key"]]];
                } else {
                    [self.rewardsDeliveryArray_EnteredValues setObject:[regFieldData objectForKey: @"MobileNumber"] forKey:[regFieldData objectForKey:@"Key"]];
                }
                
                [mobNumCell setUserInteractionEnabled:NO];
                
                return mobNumCell;
            }
                break;
            default:
                break;
        }
    }
    return [UITableViewCell new];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView == self.packageTierSelection_TableView) {
        self.selectedPackageIndexPath = indexPath;
        self.selectedPremiumTier = self.selectedPremiumTier == -1 ? 900 : self.selectedPremiumTier;
        
        if(_defaultPackageTier != nil) {
            _defaultPackageTier = nil;
        }
        
        self.basePackageId = [[self.visiblePackageList objectAtIndex:indexPath.row]objectForKey:@"PackageID"];

        [tableView beginUpdates];
        [tableView reloadRowsAtIndexPaths:[tableView indexPathsForVisibleRows] withRowAnimation:UITableViewRowAnimationNone];
        [tableView endUpdates];
    }
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
}

#pragma mark - View methods

-(void) packageSelection:(NSString*) packageID {
    self.packageID = packageID;
    int count = 900;
    int periodCount = 1100;
    
    [self resetPackageSelection];
     
     for(NSDictionary *dict in self.packageTierInfo) {
        if([[dict objectForKey:@"PackageID"] isEqualToString:packageID]) {
            if([[dict objectForKey:@"HasMultiplier"] boolValue] && !_isMultiplierSet) {
                self.multiplierSelectionView_Height.constant = 130.0;
                self.multiplierSelectionView.hidden = NO;
                self.tierViewTopBorder.hidden = YES;
                
                self.lblSliderMin.text = [dict objectForKey:@"MultiplerMin"];
                self.lblSliderMax.text = [dict objectForKey:@"MultiplerMax"];
                self.lblMultiplierSelectionView_Title.text = [LOCALIZATION(C_LOYALTY_CHOOSEMULTIPLIER) stringByReplacingOccurrencesOfString:@"XX" withString:[NSString stringWithFormat:@"%d",_multiplier]];                //lokalise 8 Feb
                
                [self.horizontalSlider setMinimumValue:[[dict objectForKey:@"MultiplerMin"] floatValue]];
                [self.horizontalSlider setMaximumValue:[[dict objectForKey:@"MultiplerMax"]floatValue]];
                [self.horizontalSlider setValue:[[dict objectForKey:@"MultiplerMin"]floatValue] animated:YES];
                //if not visible package is default
                if(self.selectedPackageIndexPath.row > self.visiblePackageList.count || ![[GET_LOYALTYTYPE objectForKey:@"CallType"] isEqualToString:@"N"]) {
                   [self.horizontalSlider setValue:[[self.defaultPackage objectForKey:@"TargetQty"]floatValue] animated:YES];
                }
                [self sliderValueChanged:self.horizontalSlider];
            } else if(_isMultiplierSet && [[dict objectForKey:@"HasMultiplier"] boolValue]){
                
            }
            else {
                _isMultiplierSet = false;
//                _catalogueList = [_catalogueListArray objectAtIndex:0];
                self.multiplierSelectionView_Height.constant = 0.0;
                self.multiplierSelectionView.hidden = YES;
                self.tierViewTopBorder.hidden = NO;
                self.multiplierValue = 1.0;
                self.multiplier = 1.0;
            }
            switch(count) {
                case kPREMIUMSELECTION_BUTTON1:
                {
                    UIButton *btn1 = [self.premiumTier_View viewWithTag:kPREMIUMSELECTION_BUTTON1];
                    float value = [[dict objectForKey:@"PremiumMinTargetQty"] floatValue];
                    [btn1 setTitle:[NSString stringWithFormat:@"%.0f",value] forState:UIControlStateNormal];
                    btn1.backgroundColor = (_selectedPremiumTier == kPREMIUMSELECTION_BUTTON1) ? COLOUR_YELLOW : COLOUR_PALEGREY;
                    btn1.titleLabel.font = FONT_H2;
                    btn1.titleLabel.textColor = COLOUR_VERYDARKGREY;
                    btn1.hidden = NO;
                    
                    if(_selectedPremiumTier == kPREMIUMSELECTION_BUTTON1) {
                        self.packageTierID = [dict objectForKey:@"PackageTierID"];
                        self.selectedPeriod = _periodSelection ? _selectedPeriod : 1100;
                        self.premiumMinTarget = [dict objectForKey:@"PremiumMinTargetQty"];
                    }
                    
                    count += 1;
                }
                    break;
                case kPREMIUMSELECTION_BUTTON2:
                {
                    UIButton *btn2 = [self.premiumTier_View viewWithTag:kPREMIUMSELECTION_BUTTON2];
                    float value = [[dict objectForKey:@"PremiumMinTargetQty"] floatValue] ;
                    [btn2 setTitle:[NSString stringWithFormat:@"%.0f",value] forState:UIControlStateNormal];
                    btn2.backgroundColor = (_selectedPremiumTier == kPREMIUMSELECTION_BUTTON2) ? COLOUR_YELLOW : COLOUR_PALEGREY;
                    btn2.titleLabel.font = FONT_H2;
                    btn2.titleLabel.textColor = COLOUR_VERYDARKGREY;
                    btn2.hidden = NO;
                    
                    if(_selectedPremiumTier == kPREMIUMSELECTION_BUTTON2) {
                        self.packageTierID = [dict objectForKey:@"PackageTierID"];
                        self.selectedPeriod = _periodSelection ? _selectedPeriod : 1100;
                        self.premiumMinTarget = [dict objectForKey:@"PremiumMinTargetQty"];
                    }
                    
                    count += 1;
                }
                    break;
                case kPREMIUMSELECTION_BUTTON3:
                {
                    UIButton *btn3 = [self.premiumTier_View viewWithTag:kPREMIUMSELECTION_BUTTON3];
                    float value = [[dict objectForKey:@"PremiumMinTargetQty"] floatValue] ;
                    [btn3 setTitle:[NSString stringWithFormat:@"%.0f",value] forState:UIControlStateNormal];
                    btn3.backgroundColor = (_selectedPremiumTier == kPREMIUMSELECTION_BUTTON3) ? COLOUR_YELLOW : COLOUR_PALEGREY;
                    btn3.titleLabel.font = FONT_H2;
                    btn3.titleLabel.textColor = COLOUR_VERYDARKGREY;
                    btn3.hidden = NO;
                    
                    if(_selectedPremiumTier == kPREMIUMSELECTION_BUTTON3) {
                        self.packageTierID = [dict objectForKey:@"PackageTierID"];
                        self.selectedPeriod = _periodSelection ? _selectedPeriod : 1100;
                        self.premiumMinTarget = [dict objectForKey:@"PremiumMinTargetQty"];
                    }
                    
                    count += 1;
                }
                    break;
                default:
                    break;
            }
        }
    }
    
    for(NSDictionary *dict in self.packageTierRewardInfo) {
        if([[dict objectForKey:@"PackageID"] isEqualToString:packageID] && [[dict objectForKey:@"PackageTierID"] isEqualToString:self.packageTierID]) {
            switch(periodCount) {
                case kPERIODSELECTION_BUTTON1:
                {
                    UIButton *btn1 = [self.periodSelection_View viewWithTag:kPERIODSELECTION_BUTTON1];
                    [btn1 setTitle:[dict objectForKey:@"Period"] forState:UIControlStateNormal];
                    btn1.backgroundColor = (_selectedPeriod == kPERIODSELECTION_BUTTON1) ? COLOUR_YELLOW : COLOUR_PALEGREY;
                    btn1.titleLabel.font = FONT_H2;
                    btn1.titleLabel.textColor = COLOUR_VERYDARKGREY;
                    btn1.hidden = NO;
                    
                    if(_selectedPeriod == kPERIODSELECTION_BUTTON1) {
                        self.period = [dict objectForKey:@"Period"];
                        [self setupRewardsView:dict];
                    }
                    
                    periodCount += 1;
                }
                    break;
                case kPERIODSELECTION_BUTTON2:
                {
                    UIButton *btn2 = [self.periodSelection_View viewWithTag:kPERIODSELECTION_BUTTON2];
                    [btn2 setTitle:[dict objectForKey:@"Period"] forState:UIControlStateNormal];
                    btn2.backgroundColor = (_selectedPeriod == kPERIODSELECTION_BUTTON2) ? COLOUR_YELLOW : COLOUR_PALEGREY;
                    btn2.titleLabel.font = FONT_H2;
                    btn2.titleLabel.textColor = COLOUR_VERYDARKGREY;
                    btn2.hidden = NO;
                    
                    if(_selectedPeriod == kPERIODSELECTION_BUTTON2) {
                        self.period = [dict objectForKey:@"Period"];
                        [self setupRewardsView:dict];
                    }
                    
                    periodCount += 1;
                }
                    break;
                default:
                    break;
            }
        }
    }
    
    
}

-(void) resetPackageSelection {
 
    UIButton *btn;
    
    btn = [self.premiumTier_View viewWithTag:kPREMIUMSELECTION_BUTTON1];
    btn.hidden = YES;
    
    btn = [self.premiumTier_View viewWithTag:kPREMIUMSELECTION_BUTTON2];
    btn.hidden = YES;
    
    btn = [self.premiumTier_View viewWithTag:kPREMIUMSELECTION_BUTTON3];
    btn.hidden = YES;
    
    btn = [self.periodSelection_View viewWithTag:kPERIODSELECTION_BUTTON1];
    btn.hidden = YES;
    
    btn = [self.periodSelection_View viewWithTag:kPERIODSELECTION_BUTTON2];
    btn.hidden = YES;
}

-(void)setupRewardsView:(NSDictionary *) rewardData {
    self.lblRewardsSelection_Rewards_Title.text = LOCALIZATION(C_LOYALTY_ADDNEW_REWARD);
    self.lblRewardsSelection_Rewards_Title.textColor = COLOUR_VERYDARKGREY;
    self.lblRewardsSelection_Rewards_Title.font = FONT_B1;
    
    self.vouchersView_Image.image = [UIImage imageNamed:@"icn_coin_yellow.png"];
    self.vouchersView_Image.contentMode = UIViewContentModeScaleAspectFit;
//    self.vouchersView_Title.text = LOCALIZATION(C_LOYALTY_GIFTVOUCHER);                             //lokalise 8 Feb
    self.vouchersView_Title.textColor = COLOUR_VERYDARKGREY;
    self.vouchersView_Title.font = FONT_B1;
    
    float value = [[rewardData objectForKey:@"RewardsValue"]doubleValue];
    //self.vouchersView_Value.text = [NSString stringWithFormat:@"%@ %@",[[[[mSession lookupTable] objectForKey:@"LanguageList"]objectAtIndex:0]objectForKey:@"Currency"],[_numberFormatter stringFromNumber:[NSNumber numberWithFloat:value]]];
    self.vouchersView_Value.text = [NSString stringWithFormat:@"%@ \n%@",[_numberFormatter stringFromNumber:[NSNumber numberWithFloat:value]],LOCALIZATION(C_LOYALTY_COINS)];
    self.vouchersView_Value.textColor = COLOUR_VERYDARKGREY;
    self.vouchersView_Value.font = FONT_B1;
    self.vouchersView_CheckboxOnView.hidden = (_isGold) ? YES : NO ;
    
    self.rewardValue = [NSString stringWithFormat:@"%.0f", value];
    self.voucherRewardValue = [NSString stringWithFormat:@"%.0f", value];
    
//    self.lblVoucherSelection.text = [[_catalogueList objectAtIndex:0] objectForKey:@"ItemName"];
//    self.lblVoucherSelection.textColor = COLOUR_VERYDARKGREY;
//    self.lblVoucherSelection.font = FONT_B1;
    
    BOOL hasGold = false;
    
    for(NSDictionary *dict in _packageTierSelectionArray) {
        if([[dict objectForKey:@"PackageID"] isEqualToString:_packageID]){
            if([[dict objectForKey:@"HasGold"] boolValue]) {
                hasGold = true;
            }
        }
    }
    if(hasGold) {
        self.goldRewardView.hidden = NO;
    
        self.goldRewardView_Image.image = [UIImage imageNamed:@"icn_gold_yellow"];
        self.goldRewardView_Image.contentMode = UIViewContentModeScaleAspectFit;
        self.goldRewardView_Title.text = LOCALIZATION(C_LOYALTY_GOLD);                  //lokalise 8 Feb
        self.goldRewardView_Title.textColor = COLOUR_VERYDARKGREY;
        self.goldRewardView_Title.font = FONT_B1;
    
        self.goldRewardView_CheckboxView.hidden = NO;
        self.goldRewardView_CheckboxOnView.hidden = (_isGold) ? NO : YES;
        
        float value = [[rewardData objectForKey:@"GoldValue"]doubleValue];
        self.goldRewardsView_Value.text = [NSString stringWithFormat:@"%@ g",[_numberFormatter stringFromNumber:[NSNumber numberWithFloat:value]]];
        self.goldRewardsView_Value.textColor = COLOUR_VERYDARKGREY;
        self.goldRewardsView_Value.font = FONT_B1;
        
        self.goldRewardValue = [NSString stringWithFormat:@"%.0f", value];
        if(self.goldRewardView_CheckboxOnView.hidden == NO) {
            self.rewardValue = self.goldRewardValue;
        }
    
    } else {
        self.goldRewardView.hidden = YES;
    }
    
//    if([[GET_LOYALTYTYPE objectForKey:@"IsGold"]boolValue]) {
//        [self goldCheckPressed:nil];
//    }
//    else {
//        NSString *codeString = [self.itemCode isEqualToString:@""] ? [GET_LOYALTYTYPE objectForKey:@"RewardItemID"] : self.itemCode;
//        for(NSDictionary *dict in _catalogueList) {
//            if([[dict objectForKey:@"ItemID"] isEqualToString:codeString]) {
//                self.lblVoucherSelection.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"ItemName"]];
//                self.itemCode = codeString;
//                break;
//            }
//        }
//        if([self.itemCode isEqualToString:@""]) {
//           self.itemCode = [[_catalogueList objectAtIndex:0] objectForKey:@"ItemID"];
//        }
//    }
}

-(NSArray *) vouchersList {
    if(_catalogueList == nil){
        return @[];
    }
    NSMutableArray *vouchersName = [[NSMutableArray alloc]init];
    for(NSDictionary *dict in _catalogueList) {
        [vouchersName addObject: [dict objectForKey:@"ItemName"]];
    }
    return vouchersName;
}

-(void) multiplierValueChanged {
    
//    if(_isMultiplierSet != true) {
//        _catalogueList = ([_catalogueListArray count] > 1)? [_catalogueListArray objectAtIndex:1] : _catalogueList;
//        _isCatalogueListChanged = YES;
//    } else if(_isMultiplierSet && self.multiplierValue <= 1) {
//        _catalogueList = [_catalogueListArray objectAtIndex:0];
//        _isCatalogueListChanged = NO;
//    } else if(_isMultiplierSet && !_isCatalogueListChanged){
//        _catalogueList = ([_catalogueListArray count] > 1)? [_catalogueListArray objectAtIndex:1] : _catalogueList;
//        _isCatalogueListChanged = YES;
//    }
    
    BOOL valueChanged = false;
    _isMultiplierSet = true;
    int count = 900;
    int periodCount = 1100;
    
    for(NSDictionary *packageDict in self.packageTierSelectionArray) {
        if([[packageDict objectForKey:@"TargetQty"] floatValue] == self.horizontalSlider.value) {
            self.packageID = [packageDict objectForKey:@"PackageID"];
            for(NSDictionary *dict in self.packageTierInfo) {
                if([[dict objectForKey:@"PackageID"] isEqualToString:self.packageID]) {
                    if(count == _selectedPremiumTier) {
                        valueChanged = true;
                        self.packageTierID = [dict objectForKey:@"PackageTierID"];
                    }
                    switch(count) {
                        case kPREMIUMSELECTION_BUTTON1:
                        {
                            UIButton *btn1 = [self.premiumTier_View viewWithTag:kPREMIUMSELECTION_BUTTON1];
                            float value = [[dict objectForKey:@"PremiumMinTargetQty"] floatValue];
                            [btn1 setTitle:[NSString stringWithFormat:@"%.0f",value] forState:UIControlStateNormal];
                            count += 1;
                        }
                            break;
                        case kPREMIUMSELECTION_BUTTON2:
                        {
                            UIButton *btn2 = [self.premiumTier_View viewWithTag:kPREMIUMSELECTION_BUTTON2];
                            float value = [[dict objectForKey:@"PremiumMinTargetQty"] floatValue];
                            [btn2 setTitle:[NSString stringWithFormat:@"%.0f",value] forState:UIControlStateNormal];
                            count += 1;
                        }
                            break;
                        case kPREMIUMSELECTION_BUTTON3:
                        {
                            UIButton *btn3 = [self.premiumTier_View viewWithTag:kPREMIUMSELECTION_BUTTON3];
                            float value = [[dict objectForKey:@"PremiumMinTargetQty"] floatValue];
                            [btn3 setTitle:[NSString stringWithFormat:@"%.0f",value] forState:UIControlStateNormal];
                            count += 1;
                        }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
    
    if(!valueChanged) {
        for(NSDictionary *dict in self.packageTierInfo) {
            if([[dict objectForKey:@"PackageID"] isEqualToString:self.basePackageId]) {
                if(count == _selectedPremiumTier) {
                    valueChanged = true;
                    self.packageTierID = [dict objectForKey:@"PackageTierID"];
                }
                switch(count) {
                    case kPREMIUMSELECTION_BUTTON1:
                    {
                        UIButton *btn1 = [self.premiumTier_View viewWithTag:kPREMIUMSELECTION_BUTTON1];
                        float value = [[dict objectForKey:@"PremiumMinTargetQty"] floatValue];
                        [btn1 setTitle:[NSString stringWithFormat:@"%.0f",value] forState:UIControlStateNormal];
                        count += 1;
                    }
                        break;
                    case kPREMIUMSELECTION_BUTTON2:
                    {
                        UIButton *btn2 = [self.premiumTier_View viewWithTag:kPREMIUMSELECTION_BUTTON2];
                        float value = [[dict objectForKey:@"PremiumMinTargetQty"] floatValue];
                        [btn2 setTitle:[NSString stringWithFormat:@"%.0f",value] forState:UIControlStateNormal];
                        count += 1;
                    }
                        break;
                    case kPREMIUMSELECTION_BUTTON3:
                    {
                        UIButton *btn3 = [self.premiumTier_View viewWithTag:kPREMIUMSELECTION_BUTTON3];
                        float value = [[dict objectForKey:@"PremiumMinTargetQty"] floatValue];
                        [btn3 setTitle:[NSString stringWithFormat:@"%.0f",value] forState:UIControlStateNormal];
                        count += 1;
                    }
                        break;
                    default:
                        break;
                }
            }
        }
    }

    valueChanged = false;
    for(NSDictionary *dict in self.packageTierRewardInfo) {
        if([[dict objectForKey:@"PackageID"] isEqualToString:self.packageID] && [[dict objectForKey:@"PackageTierID"] isEqualToString:self.packageTierID]) {
            valueChanged = true;
            switch(periodCount) {
                case kPERIODSELECTION_BUTTON1:
                {
                    if(_selectedPeriod == kPERIODSELECTION_BUTTON1) {
                        [self setupRewardsView:dict];
                    }
                    
                    periodCount += 1;
                }
                    break;
                case kPERIODSELECTION_BUTTON2:
                {
                    if(_selectedPeriod == kPERIODSELECTION_BUTTON2) {
                        [self setupRewardsView:dict];
                    }
                    
                    periodCount += 1;
                }
                    break;
                default:
                    break;
            }
        }
    }
}

- (IBAction)sliderValueChanged:(UISlider *)sender {
    int value = (int)self.horizontalSlider.value - ((int)self.horizontalSlider.value % _multiplier);
    [self.horizontalSlider setValue:(float)value];
    if((self.horizontalSlider.value / self.horizontalSlider.minimumValue) != _multiplierValue) {
        _multiplierValue = self.horizontalSlider.value / self.horizontalSlider.minimumValue;
        [self multiplierValueChanged];
    }
    
    CGRect trackRect = [self.horizontalSlider trackRectForBounds:self.horizontalSlider.frame];
    CGRect thumbRect = [self.horizontalSlider thumbRectForBounds:self.horizontalSlider.frame trackRect:trackRect value:self.horizontalSlider.value];
    self.currentValueLabel.text = [NSString stringWithFormat:@"%.0f",self.horizontalSlider.value];
    self.textMultiplierValue.text = self.currentValueLabel.text;
    [self.currentValueLabel sizeToFit];
    CGFloat width = self.currentValueLabel.frame.size.width < 20 ? 20 : self.currentValueLabel.frame.size.width;
    self.currentValueLabel.frame = CGRectMake(self.currentValueLabel.frame.origin.x, self.currentValueLabel.frame.origin.y, width, 21);
    self.currentValueView.frame = CGRectMake(thumbRect.origin.x + self.multiplierSelectionView.frame.origin.x, self.currentValueView.frame.origin.y, width, 26);
    
    if(@available(iOS 13, *)){
        self.currentViewLeadingConstraint.constant = thumbRect.origin.x - 30;
    }
    
    [self.view layoutSubviews];
}

- (IBAction)catalogueListPressed:(id)sender {
    if(!_isGold) {
        self.isStateDropDown = false;
        self.isPositionRequired = NO;
        [self popUpListViewWithTitle: LOCALIZATION(C_LOYALTY_GIFTVOUCHER) listArray:[self vouchersList] withSearchField: YES];      //lokalise 8 Feb
    }
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        if(_isStateDropDown) {
            RegistrationFormDropdownCell *cell = [self.rewardsDelivery_TableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
            
            [cell setSelectionTitle:selection.firstObject];
            [self.rewardsDeliveryArray_EnteredValues setObject:selection.firstObject forKey:@"State"];
            _isStateDropDown = false;
        } else {
            self.lblVoucherSelection.text = [NSString stringWithFormat:@"%@",selection.firstObject ];
            
            for(NSDictionary *dict in _catalogueList) {
                if([[dict objectForKey:@"ItemName"] isEqualToString:selection.firstObject]) {
                    self.itemCode = [NSString stringWithFormat:@"%@",[dict objectForKey:@"ItemID"]];
                    break;
                }
            }
        }
    }
}

- (IBAction)backBtnPressed:(id)sender {
    [self popSelf];
}

- (IBAction)packageLegendPressed:(UIButton *)sender {
    [self.view endEditing: YES];
    
    PopupInfoTableViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPINFOTABLEVIEW];
    
    [self popUpViewWithPopupVC:popupVc];
    
    [popupVc initPackagesLegend:self.visiblePackageList];
}

- (IBAction)voucherCheckPressed:(UITapGestureRecognizer *)sender {
    if(!_goldRewardView_CheckboxOnView.hidden && _vouchersView_CheckboxOnView.hidden) {
        _vouchersView_CheckboxOnView.hidden = NO;
        _goldRewardView_CheckboxOnView.hidden = YES;
        
        self.lblVoucherSelection.backgroundColor = [UIColor clearColor];
        self.rewardValue = self.voucherRewardValue;
        self.lblVoucherSelection.hidden = NO;
        self.img_dropDown.hidden = NO;
        self.btnVoucherSelection.hidden = NO;
        
        self.isGold = NO;
        
//        SET_LOYALTYTYPE(@{@"IsGold"           :   @(_isGold)});
//        NSMutableDictionary *parameters = GET_LOYALTYTYPE;
//        
//        if([GET_LOYALTYTYPE objectForKey:@"IsGold"] != nil) {
//            parameters[@"IsGold"] = @(_isGold);
//            SET_LOYALTYTYPE(parameters);
//        }
//        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (IBAction)goldCheckPressed:(UITapGestureRecognizer *)sender {
    if(_goldRewardView_CheckboxOnView.hidden && !_vouchersView_CheckboxOnView.hidden) {
        _vouchersView_CheckboxOnView.hidden = YES;
        _goldRewardView_CheckboxOnView.hidden = NO;
        
        self.isGold = YES;
        self.itemCode = @"";
        self.rewardValue = self.goldRewardValue;
        
        self.lblVoucherSelection.backgroundColor = COLOUR_VERYPALEGREY;
        self.lblVoucherSelection.hidden = YES;
        self.img_dropDown.hidden = YES;
        self.btnVoucherSelection.hidden = YES;
    }
}

- (IBAction)packageSelectionBtnPressed:(UIButton *)sender {
    if(sender.superview == self.premiumTier_View) {
        self.periodSelection = self.periodSelection ? self.periodSelection : NO;
        _selectedPremiumTier = sender.tag;
    } else {
        self.periodSelection = YES;
        _selectedPeriod = sender.tag;
    }
    [self packageSelection: _packageID];
}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

-(void) dropdownPressed:(UIButton *)sender
{
    [self.view endEditing: YES];
    listView.tag = sender.tag;
    
    switch (listView.tag)
    {
        case 4:
            _isStateDropDown = true;
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_STATE) list: [mSession getStateNames:self.stateList] hasSearchField: YES];       //lokalised
            break;
        default:
            break;
    }
    [self popUpView];
}

- (IBAction)submitBtnPressed:(UIButton *)sender {
    
    if(_isMultiplierSet) {
        int count = _selectedPremiumTier - 900;
        NSMutableArray *basePackages = [[NSMutableArray alloc] init];
        for(NSDictionary *dict in self.packageTierInfo) {
            if([[dict objectForKey:@"PackageID"] isEqualToString:self.basePackageId]) {
                [basePackages addObject: dict];
            }
        }
        _premiumMinTarget = [[basePackages objectAtIndex:count] objectForKey:@"PremiumMinTargetQty"];
        _packageTierID = [[basePackages objectAtIndex:count] objectForKey:@"PackageTierID"];
    }
    
    [_btnTnC setSelected:YES]; // From Phase 5 there is no t&c checkbox
    
    if(_btnTnC.isSelected) {
        if([[GET_LOYALTYTYPE objectForKey:@"CallType"] isEqualToString:@"N"]) {
            NSDictionary *submitParams = @{
                                           @"TradeID" : _companyTradeID,
                                           @"PackageTierID" : _packageTierID,
                                           @"Period" : _period,
                                           @"PremiumMinTargetQty" : _premiumMinTarget,
                                           @"RewardValue" : _rewardValue,
                                           @"Address1" : [_rewardsDeliveryArray_EnteredValues objectForKey:@"Address1"],
                                           @"Address2" : [_rewardsDeliveryArray_EnteredValues objectForKey:@"Address2"],
                                           @"CompanyCity" : [_rewardsDeliveryArray_EnteredValues objectForKey:@"City"],
                                           @"CompanyState" : [mSession convertToStateKeyCode: [_rewardsDeliveryArray_EnteredValues objectForKey:@"State"] stateDetailedListArray:self.stateList],
                                           @"PostalCode" : [_rewardsDeliveryArray_EnteredValues objectForKey:@"PostalCode"],
                                           @"IsGold"    : @(_isGold),
                                           @"MultiplierValue" : _multiplierValue == 1.0 ? @"" : [NSString stringWithFormat:@"%.2f",_multiplierValue],
                                           @"CallType" : @"N",
                                           @"IsAcceptTC" : @(1),
                                           };
            [[WebServiceManager sharedInstance] submitNewContract:submitParams vc:self];
        } else {
            NSDictionary *submitParams = @{
                                           @"TradeID" : _companyTradeID,
                                           @"PackageTierID" : _packageTierID,
                                           @"Period" : _period,
                                           @"PremiumMinTargetQty" : _premiumMinTarget,
                                           @"RewardValue" : _rewardValue,
                                           @"Address1" : [_rewardsDeliveryArray_EnteredValues objectForKey:@"Address1"],
                                           @"Address2" : [_rewardsDeliveryArray_EnteredValues objectForKey:@"Address2"],
                                           @"CompanyCity" : [_rewardsDeliveryArray_EnteredValues objectForKey:@"City"],
                                           @"CompanyState" :[mSession convertToStateKeyCode: [_rewardsDeliveryArray_EnteredValues objectForKey:@"State"] stateDetailedListArray:self.stateList],
                                           @"PostalCode" : [_rewardsDeliveryArray_EnteredValues objectForKey:@"PostalCode"],
                                           @"IsGold"    : @(_isGold),
                                           @"ContractReference" : [GET_LOYALTYTYPE objectForKey:@"ContractReference"],
                                           @"MultiplierValue" : _multiplierValue == 1.0 ? @"" : [NSString stringWithFormat:@"%.2f",_multiplierValue],
                                           @"CallType" : [NSString stringWithFormat:@"%@",[GET_LOYALTYTYPE objectForKey:@"CallType"]],
                                           @"IsAcceptTC" : @(1),
                                           };
            if([[GET_LOYALTYTYPE objectForKey:@"CallType"] isEqualToString:@"U"]) {
                [[WebServiceManager sharedInstance] updateContract:submitParams vc:self];
            } else if([[GET_LOYALTYTYPE objectForKey:@"CallType"] isEqualToString:@"E"]) {
                [[WebServiceManager sharedInstance] extendContract:submitParams vc:self];
            }
        }
    } else if(!_btnTnC.isSelected){
        [self popUpViewWithTitle:LOCALIZATION(C_LOYALTY_ALERT) content:LOCALIZATION(C_LOYALTY_TNC)];            //lokalise 8 Feb
    }
    
}


#pragma mark - TextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
    switch(textField.tag) {
        case 1: {
            [_rewardsDeliveryArray_EnteredValues setObject:textField.text forKey:@"Address1"];
        }
            break;
        case 2: {
           [_rewardsDeliveryArray_EnteredValues setObject:textField.text forKey:@"Address2"];
        }
            break;
        case 3: {
            [_rewardsDeliveryArray_EnteredValues setObject:textField.text forKey:@"City"];
        }
            break;
        case 5: {
           [_rewardsDeliveryArray_EnteredValues setObject:textField.text forKey:@"PostalCode"];
        }
            break;
        case 1200: {
            int enteredValue = [textField.text intValue];
            if(enteredValue % _multiplier == 0) {
                [self.horizontalSlider setValue:[textField.text floatValue]];
                [self sliderValueChanged:self.horizontalSlider];
            } else {
                [self popUpViewWithTitle:LOCALIZATION(C_ALERTVIEW_ERROR) content:[NSString stringWithFormat:@"%@ %d",LOCALIZATION(C_LOYALTY_ALERT_MULTIPLE),_multiplier]];  //lokalise 8 Feb
                textField.text = [NSString stringWithFormat:@"%.0f",_horizontalSlider.value];
            }
        }
            break;
        default:
            break;
    }
}
- (IBAction)tnCPressed:(id)sender {
    if(self.btnTnC.isSelected) {
        [self.btnTnC setSelected:NO];
    } else {
        [self.btnTnC setSelected:YES];
    }
}

- (IBAction)hyperlinkPressed:(id)sender {
    [mSession pushTNCView:NO tncType: 699
                       vc:self];
}

#pragma mark - WebService
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_STATE:
        {
            SET_LOADSTATE([[response getGenericResponse] objectForKey: @"StateDetailsList"]);
            self.stateList = [[response getGenericResponse] objectForKey: @"StateDetailsList"];
            
            NSString *stateID =  [NSString stringWithFormat:@"%@", [self.rewardsDeliveryArray objectForKey:@"StateID"]];
            
            self.stateName = [[mSession convertToStateName: stateID stateDetailedListArray:self.stateList] isEqualToString:BLANK] ? @"" : [mSession convertToStateName:stateID stateDetailedListArray:self.stateList];
            
            if(![self.stateName isEqualToString:@""]) {
                [self.rewardsDeliveryArray_EnteredValues setObject:self.stateName forKey:@"State"];
                [self.rewardsDelivery_TableView reloadData];
            } else {
                self.stateName = nil;
            }
           
        }
            break;
        case kWEBSERVICE_LOYALTY_SUBMITNEWCONTRACT:{
            
            if([[[response getJSON] objectForKey:@"ResponseCode"] intValue] == 5000) {
                NSDictionary *confirmDict =@{@"title" : LOCALIZATION(C_LOYALTY_CONFIRMATION),       //lokalise 8 Feb
                                             @"header" : LOCALIZATION(C_LOYALTY_THANKYOU),          //lokalise 8 Feb
                                             @"subtitle" : [[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Message"]
                                             };
                
                SET_LOYALTYCONFIRMORDERKEYS(confirmDict);
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [mSession loadContractConfirmView];
            } else {
                [self popUpViewWithTitle:[[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Header"] content:[[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Message"]];
            }
        }
            break;
        case kWEBSERVICE_LOYALTY_UPDATECONTRACT:{
            
            if([[[response getJSON] objectForKey:@"ResponseCode"] intValue] == 5000) {
                NSDictionary *confirmDict =@{@"title" : LOCALIZATION(C_LOYALTY_CONFIRMATION),       //lokalise 8 Feb
                                             @"header" : LOCALIZATION(C_LOYALTY_CONTRACTUPDATED),   //lokalise 8 Feb
                                             @"subtitle" : [[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Message"]
                                             };
                
                SET_LOYALTYCONFIRMORDERKEYS(confirmDict);
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [mSession loadContractConfirmView];
            } else {
                [self popUpViewWithTitle:[[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Header"] content:[[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Message"]];
            }
            
        }
            break;
        case kWEBSERVICE_LOYALTY_EXTENDCONTRACT:{
            
            if([[[response getJSON] objectForKey:@"ResponseCode"] intValue] == 5000) {
                NSDictionary *confirmDict =@{@"title" : LOCALIZATION(C_LOYALTY_CONFIRMATION),       //lokalise 8 Feb
                                             @"header" : LOCALIZATION(C_LOYALTY_CONTRACTUPDATED),   //lokalise 8 Feb
                                             @"subtitle" : [[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Message"]
                                             };
                
                SET_LOYALTYCONFIRMORDERKEYS(confirmDict);
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [mSession loadContractConfirmView];
            } else {
                [self popUpViewWithTitle:[[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Header"] content:[[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Message"]];
            }
            
        }
            break;
        default:
            break;
    }
}
@end
