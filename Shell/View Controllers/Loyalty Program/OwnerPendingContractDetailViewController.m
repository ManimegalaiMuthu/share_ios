//
//  OwnerPendingContractDetailViewController.m
//  Shell
//
//  Created by Nach on 2/4/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "OwnerPendingContractDetailViewController.h"
#import "FormsTableViewDelegate.h"

#define kDigitalSignature @"DigitalSignature"
#define kTNCCheckbox      @"TNCCheckbox"

@interface OwnerPendingContractDetailViewController () <FormsTableViewDelegate,WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UIView *stepView;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet UILabel *lblContractID;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageName;
@property (weak, nonatomic) IBOutlet UILabel *lblProgramPeriod_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblProgramPeriod_Data;
@property (weak, nonatomic) IBOutlet UILabel *lblMinOrder_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblMinOrder_Data;
@property (weak, nonatomic) IBOutlet UILabel *lblProgReward_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblProgReward_Data;

@property (weak, nonatomic) IBOutlet UILabel *lblStartTracking;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@property NSNumberFormatter *numberFormatter;
@property FormsTableViewDelegate *tableViewDelegate;

@end

@implementation OwnerPendingContractDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.btnBack setTintColor: COLOUR_VERYDARKGREY];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_LOYALTY_CONTRACTDETAILS_TITLE) subtitle:@""];
    [self configureStepView];
    
    _numberFormatter = [[NSNumberFormatter alloc] init];
    [_numberFormatter setGroupingSeparator:@","];
    [_numberFormatter setGroupingSize:3];
    [_numberFormatter setUsesGroupingSeparator:YES];
    
    self.lblContractID.text = [NSString stringWithFormat:@"%@: %@",LOCALIZATION(C_LOYALTY_CONTRACTID),[self.contractDetails objectForKey:@"ContractReference"]];
    self.lblContractID.textColor = COLOUR_VERYDARKGREY;
    self.lblContractID.font = FONT_H1;
    
    self.lblPackageName.text = [NSString stringWithFormat:@"%@",[self.contractDetails objectForKey:@"PackageName"]];
    self.lblPackageName.textColor = COLOUR_VERYDARKGREY;
    self.lblPackageName.font = FONT_H1;
    
    self.lblProgramPeriod_Title.text = LOCALIZATION(C_LOYALTY_PROGRAMPERIOD);
    self.lblProgramPeriod_Title.textColor = COLOUR_VERYDARKGREY;
    self.lblProgramPeriod_Title.font = FONT_H1;
    
    self.lblMinOrder_Title.text = LOCALIZATION(C_LOYALTY_MINORDER);
    self.lblMinOrder_Title.textColor = COLOUR_VERYDARKGREY;
    self.lblMinOrder_Title.font = FONT_H1;
    
    self.lblProgReward_Title.text = LOCALIZATION(C_LOYALTY_PROGRAMREWARDS);
    self.lblProgReward_Title.textColor = COLOUR_VERYDARKGREY;
    self.lblProgReward_Title.font = FONT_H1;
    
    self.lblProgramPeriod_Data.text = [NSString stringWithFormat:@"%@ %@",[self.contractDetails objectForKey:@"Period"],LOCALIZATION(C_LUBEMATCH_MONTHS)];
    self.lblProgramPeriod_Data.textColor = COLOUR_VERYDARKGREY;
    self.lblProgramPeriod_Data.font = FONT_B1;
    
    if([[_contractDetails objectForKey:@"HasMultiplier"]boolValue]) {
        self.lblMinOrder_Data.text = [NSString stringWithFormat:@"%.0f %@",[[_contractDetails objectForKey:@"PremiumTarget"] intValue] * [[_contractDetails objectForKey:@"MultipliedValue"] floatValue],LOCALIZATION(C_LOYALTY_CARTONS)];
    } else {
        self.lblMinOrder_Data.text = [NSString stringWithFormat:@"%@ %@",[_contractDetails objectForKey:@"PremiumTarget"],LOCALIZATION(C_LOYALTY_CARTONS)];
    }
    self.lblMinOrder_Data.textColor = COLOUR_VERYDARKGREY;
    self.lblMinOrder_Data.font = FONT_B1;
    
    self.lblProgReward_Data.text = [NSString stringWithFormat:@"%@ %@",[_numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[_contractDetails objectForKey:@"RewardsValue"] intValue]]],LOCALIZATION(C_LOYALTY_COINS)];
    self.lblProgReward_Data.textColor = COLOUR_VERYDARKGREY;
    self.lblProgReward_Data.font = FONT_B1;
    
    self.lblStartTracking.text = LOCALIZATION(C_LOYALTY_STARTTRACKING_CONTENT);
    self.lblStartTracking.textColor = COLOUR_VERYDARKGREY;
    self.lblStartTracking.font = FONT_B1;
    
    [self.btnSubmit setTitle:LOCALIZATION(C_BTN_ACTIVATE) forState:UIControlStateNormal];
    [self.btnSubmit.titleLabel setFont:FONT_BUTTON];
    [self.btnSubmit setBackgroundColor:COLOUR_RED];
    
    [self tableViewSetup];
}

- (IBAction)submitPressed:(UIButton *)sender {
    NSDictionary *jsonValuesDict = [self.tableViewDelegate getJsonValues];
    
    if([[jsonValuesDict objectForKey:kTNCCheckbox] boolValue]) {
        
        NSDictionary *submitParams = @{
                                        @"ContractReference" : [self.contractDetails objectForKey:@"ContractReference"],
                                        @"Period" : [self.contractDetails objectForKey:@"Period"],
                                        @"IsAcceptTC" : @(1),
                                        @"eSignature" : ([jsonValuesDict objectForKey: kDigitalSignature] &&
                                                         ![[jsonValuesDict objectForKey: kDigitalSignature] isEqualToString: @""]) ? [jsonValuesDict objectForKey: kDigitalSignature] : @"" ,
                                    };
        
        [[WebServiceManager sharedInstance] updateSignatureForContract:submitParams vc:self];
        
    } else {
        [self popUpViewWithTitle:LOCALIZATION(C_LOYALTY_ALERT) content:LOCALIZATION(C_LOYALTY_TNC)];
    }
}


- (IBAction)backPressed:(UIButton *)sender {
    [self popSelf];
}


#pragma mark - Helper Methods
-(void) configureStepView {
    UIImageView *imageView = [self.stepView viewWithTag:1];
    UIImageView *nextImageView = [self.stepView viewWithTag:3];
    UILabel *currentStatus = [self.stepView viewWithTag:2];
    UILabel *nextStatus = [self.stepView viewWithTag:4];
    currentStatus.font = FONT_B2;
    nextStatus.font = FONT_B2;
    
    switch([[_contractDetails objectForKey:@"Status"] intValue]) {
        case kCONTRACTSTATUS_PENDING://Pending
            imageView.image = [UIImage imageNamed:@"icn_pending_small_on"];
            nextImageView.image = [UIImage imageNamed:@"icn_processing_small_off"];
                       
            currentStatus.text = [mSession convertToContractStatus:kCONTRACTSTATUS_PENDING];
            nextStatus.text= [mSession convertToContractStatus:kCONTRACTSTATUS_INPROGRESS];
                       
            currentStatus.textColor = COLOUR_VERYDARKGREY;
            nextStatus.textColor = COLOUR_LIGHTGREY;
            break;
        case kCONTRACTSTATUS_INPROGRESS: //In Progress
            imageView.image = [UIImage imageNamed:@"icn_processing_small_on"];
            nextImageView.image = [UIImage imageNamed:@"icn_ongoing_off"];
            
            currentStatus.text = [mSession convertToContractStatus:kCONTRACTSTATUS_INPROGRESS];
            nextStatus.text= [mSession convertToContractStatus:kCONTRACTSTATUS_COMPLETED];
            
            currentStatus.textColor = COLOUR_VERYDARKGREY;
            nextStatus.textColor = COLOUR_LIGHTGREY;
            break;
            
        case kCONTRACTSTATUS_COMPLETED: //Completed
            imageView.image = [UIImage imageNamed:@"icn_processing_small_on"];
            nextImageView.image = [UIImage imageNamed:@"icn_ongoing_on"];
            
            currentStatus.text = [mSession convertToContractStatus:kCONTRACTSTATUS_INPROGRESS];
            nextStatus.text= [mSession convertToContractStatus:kCONTRACTSTATUS_COMPLETED];
            
            currentStatus.textColor = COLOUR_VERYDARKGREY;
            nextStatus.textColor = COLOUR_VERYDARKGREY;
            break;
            
        case kCONTRACTSTATUS_UNDERREVIEW://Under Review
            imageView.image = [UIImage imageNamed:@"icn_underreview_small"];
            nextImageView.image = [UIImage imageNamed:@"icn_cancelled"];
            
            currentStatus.text = [mSession convertToContractStatus:kCONTRACTSTATUS_UNDERREVIEW];
            nextStatus.text= [mSession convertToContractStatus:kCONTRACTSTATUS_INCOMPLETE];
            
            currentStatus.textColor = COLOUR_VERYDARKGREY;
            nextStatus.textColor = COLOUR_LIGHTGREY;
            break;
            
        case kCONTRACTSTATUS_INCOMPLETE://Incompleted
            imageView.image = [UIImage imageNamed:@"icn_underreview_small"];
            nextImageView.image = [UIImage imageNamed:@"icn_cancelled"];
            
            currentStatus.text = [mSession convertToContractStatus:kCONTRACTSTATUS_UNDERREVIEW];
            nextStatus.text= [mSession convertToContractStatus:kCONTRACTSTATUS_INCOMPLETE];
            
            currentStatus.textColor = COLOUR_VERYDARKGREY;
            nextStatus.textColor = COLOUR_VERYDARKGREY;
            break;
            
        case kCONTRACTSTATUS_UNACCEPTED://Not Accepted
            imageView.image = [UIImage imageNamed:@"icn_pending_small_on"];
            nextImageView.image = [UIImage imageNamed:@"icn_cancelled"];
            
            currentStatus.text = [mSession convertToContractStatus:kCONTRACTSTATUS_PENDING];
            nextStatus.text= [mSession convertToContractStatus:kCONTRACTSTATUS_INCOMPLETE];
            
            currentStatus.textColor = COLOUR_VERYDARKGREY;
            nextStatus.textColor = COLOUR_VERYDARKGREY;
            break;
        default:
            break;
    }
}

-(void) tableViewSetup {
    self.tableViewDelegate = [[FormsTableViewDelegate alloc] init];
    NSMutableArray *tableDataArray = [[NSMutableArray alloc] init];
    
    NSString *tncString = [LOCALIZATION(C_LOYALTY_TNCCHECKBOX_CONTENT) stringByReplacingOccurrencesOfString:@"XXX" withString: [self.contractDetails objectForKey:@"OwnerName"]];
    
    [tableDataArray addObject: @{kForm_Title: tncString,
                                 kForm_Type: @(kREGISTRATION_TNCCHECKBOX),
                                 kForm_Key: kTNCCheckbox,
                                 kForm_Enabled: @(YES),
    }];
    
    [tableDataArray addObject: @{kForm_Title: LOCALIZATION(C_LOYALTY_SIGNATURE),
                                                  kForm_Type: @(kREGISTRATION_SIGNATURE),
                                                  kForm_Key: kDigitalSignature,
                                                  kForm_Enabled: @(YES),
    }];
    
    [self.tableViewDelegate.initializingArray addObject: tableDataArray];
    [self.tableViewDelegate.headersArray addObject: @""];
    
    self.tableView.delegate = self.tableViewDelegate;
    self.tableView.dataSource = self.tableViewDelegate;
       
       
    [self.tableViewDelegate registerNib: self.tableView];
    [self.tableViewDelegate setDelegate: self];
    [self.tableView setContentInset:UIEdgeInsetsMake(15, 0, 15, 0)];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.backgroundView = nil;
}

#pragma mark signature delegate
-(void) signaturePressed:(NSString *)key
{
    [mSession pushRUCustomerSignatureWithDelegate: self delegate: self];
}

-(void)didFinishSignature:(UIImage *)image
{
    [self.tableViewDelegate editCell: kDigitalSignature withSignatureImage: image];
}


#pragma mark WebServiceManager
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_LOYALTY_UPDATESIGNATUREFORCONTRACT: {
            if([[[response getJSON] objectForKey:@"ResponseCode"] intValue] == 5000) {
                NSDictionary *confirmDict =@{@"title" : LOCALIZATION(C_LOYALTY_CONFIRMATION),
                                             @"header" : LOCALIZATION(C_LOYALTY_THANKYOU),
                                             @"subtitle" : [[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Message"]
                                            };
                          
                SET_LOYALTYCONFIRMORDERKEYS(confirmDict);
                SET_LOYALTYTYPE(@{@"ContractReference" : [self.contractDetails objectForKey:@"ContractReference"]});
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [mSession loadContractConfirmView];
            } else {
                [self popUpViewWithTitle:[[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Header"] content:[[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Message"]];
            }
        }
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
