//
//  FilterByViewController.h
//  Shell
//
//  Created by Nach on 20/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@protocol FilterByDelegate <NSObject>
-(void) filterBySelection : (NSDictionary *)selection;
@end


@interface FilterByViewController : BaseVC
@property (nonatomic, weak) id<FilterByDelegate> delegate;
@property NSMutableDictionary *filterStatusArray;
@end

NS_ASSUME_NONNULL_END
