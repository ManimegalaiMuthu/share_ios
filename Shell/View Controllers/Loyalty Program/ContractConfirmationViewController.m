//
//  ContractConfirmationViewController.m
//  Shell
//
//  Created by Nach on 27/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "ContractConfirmationViewController.h"

@interface ContractConfirmationViewController ()<WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblThankYouHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblConfirmMsg;
@property (weak, nonatomic) IBOutlet UIButton *btnViewContract;


@end

@implementation ContractConfirmationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.title = [GET_LOYALTYCONFIRMORDERKEYS objectForKey:@"title"];
    [Helper setNavigationBarTitle:self title:[GET_LOYALTYCONFIRMORDERKEYS objectForKey:@"title"] subtitle:@""];
    
    self.lblThankYouHeader.font = FONT_H1;
    self.lblThankYouHeader.textColor = COLOUR_RED;
    self.lblConfirmMsg.font = FONT_B2;
    //    self.lblThankyouHeader.textColor = COLOUR_RED;
    
    self.lblThankYouHeader.text = [[GET_LOYALTYCONFIRMORDERKEYS objectForKey:@"header"] isKindOfClass:[NSNull class]] ? @"" : [GET_LOYALTYCONFIRMORDERKEYS objectForKey:@"header"];
    NSString *tncString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:15; color: #404040;}</style>%@",[GET_LOYALTYCONFIRMORDERKEYS objectForKey:@"subtitle"]];
    self.lblConfirmMsg.attributedText = [[NSAttributedString alloc] initWithData: [tncString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
    
    self.btnViewContract.backgroundColor = COLOUR_RED;
    self.btnViewContract.titleLabel.font = FONT_H1;
    if([[GET_LOYALTYTYPE objectForKey:@"CallType"] isEqualToString:@"N"]) {
        [self.btnViewContract setTitle: LOCALIZATION(C_LOYALTY_VIEWCONTRACTS) forState:UIControlStateNormal];       //lokalise 8 Feb
    } else  {
         [self.btnViewContract setTitle: LOCALIZATION(C_LOYALTY_BACKTODETAILS) forState:UIControlStateNormal];      //lokalise 8 Feb
    }
    [self.btnViewContract setBackgroundColor:COLOUR_RED];
    SET_LOYALTYCONFIRMORDERKEYS(@{});
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_LOYALTY_LOYALTYPROGRAM),LOCALIZATION_EN(C_LOYALTY_CONFIRMATION)] screenClass:nil];
}

- (IBAction)viewContractsPressed:(UIButton *)sender {
    if([[GET_LOYALTYTYPE objectForKey:@"CallType"] isEqualToString:@"N"]) {
        [mSession loadLoyaltyContract];
    } else {
         [[WebServiceManager sharedInstance] loadLoyaltyContractDetails:[GET_LOYALTYTYPE objectForKey:@"ContractReference"] vc:self];
    }
    SET_LOYALTYTYPE(@{});
}

#pragma maek WebserviceManager
-(void)processCompleted:(WebServiceResponse *)response {
    switch (response.webserviceCall) {
        case kWEBSERVICE_LOYALTY_LOADCONTRACTDETAILS:{
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                [mSession loadLoyaltyContractDetails:[response getGenericResponse]];
            }
        }
            break;
        default:
            break;
    }
}

@end
