//
//  OwnerPendingContractDetailViewController.h
//  Shell
//
//  Created by Nach on 2/4/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface OwnerPendingContractDetailViewController : BaseVC

@property NSDictionary* contractDetails;

@end

NS_ASSUME_NONNULL_END
