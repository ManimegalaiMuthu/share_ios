//
//  LoyaltyContractDetailsViewController.m
//  Shell
//
//  Created by Nach on 27/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "LoyaltyContractDetailsViewController.h"
#import "LoyaltyProgramGaugeView.h"

#define UPDATE_BUTTON_TAG 699
#define CONTINUE_BUTTON_TAG 700
#define EXTEND_BUTTON_TAG 701
#define TERMINATE_BUTTON_TAG 702
#define REDEEM_BUTTON_TAG 703

@interface LoyaltyContractDetailsViewController () <WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UIView *stepView;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet UILabel *lblContractID;
@property (weak, nonatomic) IBOutlet UILabel *lblCompanyName;
@property (weak, nonatomic) IBOutlet UILabel *lblCompanyAddress;

@property (weak, nonatomic) IBOutlet UILabel *lblProgramDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageName;
@property (weak, nonatomic) IBOutlet UILabel *lblProgramPeriod_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblProgramPeriod_Data;
@property (weak, nonatomic) IBOutlet UILabel *lblMinOrder_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblMinOrder_Data;
@property (weak, nonatomic) IBOutlet UILabel *lblProgReward_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblProgReward_Data;

@property (weak, nonatomic) IBOutlet UILabel *lblTotalOrdered_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalOrdered_Data;
@property (weak, nonatomic) IBOutlet UILabel *lblPendingOrders_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblPendingOrders_Data;

@property (weak, nonatomic) IBOutlet UIButton *btnViewBreakdown;
@property (weak, nonatomic) IBOutlet UILabel *lblProgramGaugeFooter;
@property (weak, nonatomic) IBOutlet UIView *btnViews;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnViews_Height;

@property (weak, nonatomic) IBOutlet UIButton *btnViews_Button1;
@property (weak, nonatomic) IBOutlet UIButton *btnViews_Button2;

@property (weak, nonatomic) IBOutlet LoyaltyProgramGaugeView *loyaltyProgramGauge;

@property NSMutableArray *buttonArray;
@property NSArray *catalogueList;
@property NSDictionary *requestParams;

@property BOOL isGold;
@property NSNumberFormatter *numberFormatter;

@end

@implementation LoyaltyContractDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if(!_isLoaded) {
        [self.btnBack setTintColor: COLOUR_VERYDARKGREY];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    }
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_LOYALTY_CONTRACTDETAILS_TITLE) subtitle:@""];
    
    [self configureStepView];
    
    _numberFormatter = [[NSNumberFormatter alloc] init];
    [_numberFormatter setGroupingSeparator:@","];
    [_numberFormatter setGroupingSize:3];
    [_numberFormatter setUsesGroupingSeparator:YES];
    
    self.lblContractID.text = [NSString stringWithFormat:@"%@: %@",LOCALIZATION(C_LOYALTY_CONTRACTID),[_contractDetails objectForKey:@"ContractReference"]];        //lokalise 7 Feb
    self.lblContractID.textColor = COLOUR_VERYDARKGREY;
    self.lblContractID.font = FONT_H1;
    
    self.lblCompanyName.text = [NSString stringWithFormat:@"%@",[_contractDetails objectForKey:@"CompanyName"]];
    self.lblCompanyName.textColor = COLOUR_VERYDARKGREY;
    self.lblCompanyName.font = FONT_H2;
    
    self.lblCompanyAddress.text = [NSString stringWithFormat:@"%@ \n%@, %@ \n%@, %@\n%@: %@\n%@: %@",[_contractDetails objectForKey:@"Address1"],[_contractDetails objectForKey:@"CompanyCity"],[_contractDetails objectForKey:@"CompanyStateName"],[_contractDetails objectForKey:@"PostalCode"],[_contractDetails objectForKey:@"CountryName"],LOCALIZATION(C_LOYALTY_ATTN),[_contractDetails objectForKey:@"OwnerName"],LOCALIZATION(C_LOYALTY_MOBILE),[_contractDetails objectForKey:@"MobileNumber"]];                       //lokalise 7 Feb
    self.lblCompanyAddress.textColor = COLOUR_VERYDARKGREY;
    self.lblCompanyAddress.font = FONT_B2;
    
    self.lblProgramDetails.text = LOCALIZATION(C_LOYALTY_PROGRAMDETAILS);           //lokalise 7 Feb
    self.lblProgramDetails.textColor = COLOUR_VERYDARKGREY;
    self.lblProgramDetails.font = FONT_H1;
    
    self.lblPackageName.text = [_contractDetails objectForKey:@"PackageName"];
    self.lblPackageName.textColor = COLOUR_VERYDARKGREY;
    self.lblPackageName.font = FONT_H1;
    
    self.lblProgramPeriod_Title.text = LOCALIZATION(C_LOYALTY_PROGRAMPERIOD);       //lokalise 7 Feb
    self.lblProgramPeriod_Title.textColor = COLOUR_VERYDARKGREY;
    self.lblProgramPeriod_Title.font = FONT_H1;
    
    self.lblMinOrder_Title.text = LOCALIZATION(C_LOYALTY_MINORDER);                 //lokalise 7 Feb
    self.lblMinOrder_Title.textColor = COLOUR_VERYDARKGREY;
    self.lblMinOrder_Title.font = FONT_H1;
    
    self.lblProgReward_Title.text = LOCALIZATION(C_LOYALTY_PROGRAMREWARDS);         //lokalise 7 Feb
    self.lblProgReward_Title.textColor = COLOUR_VERYDARKGREY;
    self.lblProgReward_Title.font = FONT_H1;
    
    [self configureProgramPeriod];
    self.lblProgramPeriod_Data.textColor = COLOUR_VERYDARKGREY;
    self.lblProgramPeriod_Data.font = FONT_B1;
    
    if([[_contractDetails objectForKey:@"HasMultiplier"]boolValue]) {
        self.lblMinOrder_Data.text = [NSString stringWithFormat:@"%.0f %@",[[_contractDetails objectForKey:@"PremiumTarget"] intValue] * [[_contractDetails objectForKey:@"MultipliedValue"] floatValue],LOCALIZATION(C_LOYALTY_CARTONS)];      //lokalise 7 Feb
    } else {
        self.lblMinOrder_Data.text = [NSString stringWithFormat:@"%@ %@",[_contractDetails objectForKey:@"PremiumTarget"],LOCALIZATION(C_LOYALTY_CARTONS)];  //lokalise 7 Feb
    }
    self.lblMinOrder_Data.textColor = COLOUR_VERYDARKGREY;
    self.lblMinOrder_Data.font = FONT_B1;
    
    if([[_contractDetails objectForKey:@"ItemCode"] isEqualToString:@"0"] || [[_contractDetails objectForKey:@"ItemCode"] isEqualToString:@""]) {
        
        NSArray *dateArray = [[NSString stringWithFormat:@"%@",[_contractDetails objectForKey:@"StartDate"]] componentsSeparatedByString:@"/"];
        int year = [dateArray[2] intValue];
        
        if(year > 2019) {
            self.lblProgReward_Data.text = [NSString stringWithFormat:@"%@ %@",[_numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[_contractDetails objectForKey:@"RewardsValue"] intValue]]],LOCALIZATION(C_LOYALTY_COINS)];
            _isGold = false;
        } else {
            self.lblProgReward_Data.text = [NSString stringWithFormat:@"%@ g %@",[_numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[_contractDetails objectForKey:@"RewardsValue"]floatValue]]],LOCALIZATION(C_LOYALTY_GOLD)];          //lokalise 8 Feb
            _isGold = true;
        }
        
    } else {
        self.lblProgReward_Data.text = [NSString stringWithFormat:@"%@ %@ %@ %@",[[[[mSession lookupTable] objectForKey:@"LanguageList"]objectAtIndex:0]objectForKey:@"Currency"],[_numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[_contractDetails objectForKey:@"RewardsValue"]floatValue]]],[_contractDetails objectForKey:@"VoucherName"],LOCALIZATION(C_LOYALTY_VOUCHER)];                   //lokalise 8 Feb
        _isGold = false;
    }
    
    self.lblProgReward_Data.textColor = COLOUR_VERYDARKGREY;
    self.lblProgReward_Data.font = FONT_B1;
    
    self.lblTotalOrdered_Title.text = [_contractDetails objectForKey:@"TextTotalOrdered"];
    self.lblTotalOrdered_Title.font = FONT_H1;
    self.lblTotalOrdered_Title.textColor = COLOUR_RED;
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%.1f ",[[_contractDetails objectForKey:@"NoOfOverallOrdered"] floatValue]] attributes:@{
                                                                                                                                            NSFontAttributeName : FONT_H(18),
                                                                                                                                            }];
    [attrString appendAttributedString: [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(C_LOYALTY_CARTONS) attributes:@{
                                                                                                                         NSFontAttributeName : FONT_B3,
                                                                                                                         }]];           //lokalised
    self.lblTotalOrdered_Data.attributedText = attrString;
    self.lblTotalOrdered_Data.textColor = COLOUR_RED;

    self.lblPendingOrders_Title.text = [NSString stringWithFormat:@"%@*",[_contractDetails objectForKey:@"TextPendingOrders"]];
    self.lblPendingOrders_Title.font = FONT_B2;
    self.lblPendingOrders_Title.textColor = COLOUR_VERYDARKGREY;

    NSMutableAttributedString *textPendingOrders = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%.1f ",[[_contractDetails objectForKey:@"NoOfPendingOrders"] floatValue]] attributes:@{
                                                                                                                        NSFontAttributeName : FONT_H1 ,
                                                                                                                        }];
    [textPendingOrders appendAttributedString: [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(C_LOYALTY_CARTONS) attributes:@{
                                                                                                                                NSFontAttributeName : FONT_B3,
                                                                                                                                }]];        //lokalised
    self.lblPendingOrders_Data.attributedText = textPendingOrders;
    self.lblPendingOrders_Data.textColor = COLOUR_VERYDARKGREY;
    
    [self.btnViewBreakdown setTitle:LOCALIZATION(C_LOYALTY_VIEWBREAKDOWN) forState:UIControlStateNormal];       //lokalise 8 Feb
    
    self.lblProgramGaugeFooter.text = [NSString stringWithFormat:@"%@",[_contractDetails objectForKey:@"FooterText"]];
    self.lblProgramGaugeFooter.textColor = COLOUR_VERYDARKGREY;
    self.lblProgramGaugeFooter.font = FONT_B2;

    _buttonArray = [[NSMutableArray alloc]init];
    if([[_contractDetails objectForKey:@"CanUpdate"]boolValue]) {
        [self createButton:LOCALIZATION(C_LOYALTY_UPDATECONTRACT) withColor:COLOUR_RED tag:UPDATE_BUTTON_TAG];          //lokalise 8 Feb
    }
    if([[_contractDetails objectForKey:@"CanContinue"]boolValue]) {
        [self createButton:LOCALIZATION(C_LOYALTY_CONTINUECONTRACT) withColor:COLOUR_RED tag:CONTINUE_BUTTON_TAG];      //lokalise 8 Feb
    }

    if([[_contractDetails objectForKey:@"CanExtend"]boolValue]) {
        [self createButton:LOCALIZATION(C_LOYALTY_EXTENDCONTRACT) withColor:COLOUR_RED tag:EXTEND_BUTTON_TAG];          //lokalise 8 Feb
    }
    if([[_contractDetails objectForKey:@"CanTerminate"]boolValue]) {
        [self createButton:LOCALIZATION(C_LOYALTY_TERMINATECONTRACT) withColor:COLOUR_YELLOW tag:TERMINATE_BUTTON_TAG];//lokalise 8 Feb
    }
    if([[_contractDetails objectForKey:@"CanRedeem"]boolValue]) {
        [self createButton:LOCALIZATION(C_LOYALTY_COMPLETECONTRACT) withColor:COLOUR_YELLOW tag:REDEEM_BUTTON_TAG];     //lokalise 8 Feb
    }

    self.btnViews_Height.constant = 60 * _buttonArray.count;
    
    if(_buttonArray.count == 1 && _btnViews_Button1.hidden == YES) {
        [NSLayoutConstraint constraintWithItem:self.btnViews_Button1 attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
    }
    
    [self.view setNeedsDisplay];
    NSDictionary *viewData = @{
                               //                               @"TextTotalCarton" : @"Total Carton",
                               @"TextTotalCarton" : [_contractDetails objectForKey:@"TextTotalCarton"] == nil  && [[_contractDetails objectForKey:@"TextTotalCarton"] isKindOfClass:[NSNull class]]? @"" : [_contractDetails objectForKey:@"TextTotalCarton"],
                               //                               @"TextPremiumTier" : @"Premium Tier",
                               @"TextPremiumTier" :  [_contractDetails objectForKey:@"TextPremiumTier"] == nil && [[_contractDetails objectForKey:@"TextPremiumTier"] isKindOfClass:[NSNull class]]? @"" : [_contractDetails objectForKey:@"TextPremiumTier"],
                               @"ColorCodeDefaultOverall" : [_contractDetails objectForKey:@"ColorCodeDefaultOverall"],
                               @"ColorCodeDefaultPremium" : [_contractDetails objectForKey:@"ColorCodeDefaultPremium"],
                               @"NoOfOverallOrdered" : [_contractDetails objectForKey:@"TotalCartonsOrdered"],
                               @"NoOfPremiumOrdered" : [_contractDetails objectForKey:@"TotalPremiumAcheived"],
                               @"TotalPremiumTarget" : [_contractDetails objectForKey:@"TotalPremiumTarget"],
                               @"OverallTarget" : [_contractDetails objectForKey:@"OverallTarget"],
                               @"ColorCodeOverall" : [_contractDetails objectForKey:@"ColorCodeOverall"],
                               @"ColorCodePremium" : [_contractDetails objectForKey:@"ColorCodePremium"],
                               };
    _loyaltyProgramGauge.isAgri = false;
    [_loyaltyProgramGauge loadBar: viewData isCarousel:NO];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_LOYALTY_PROGRAMDETAILS_TITLE) screenClass:nil];
}


#pragma mark - Outlet Actions
- (IBAction)backPressed:(UIButton *)sender {
    [self popSelf];
}

- (IBAction)viewBreakdownPressed:(UIButton *)sender {
    SET_LOYALTYTYPE(@{@"PerformanceCallType" : @"D"});
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[WebServiceManager sharedInstance]fetchLoyaltyPerformaceForContract:@{
                                                                           @"ContractReference": [_contractDetails objectForKey:@"ContractReference"],
                                                                           @"CallType" : @"D",
                                                                           }   vc:self];
}


#pragma mark - View Methods
-(void) configureProgramPeriod {
    switch([[self.contractDetails objectForKey:@"Status"] intValue]) {
        case kCONTRACTSTATUS_PENDING: {
            self.lblProgramPeriod_Data.text = LOCALIZATION(C_LOYALTY_PERIOD_NOTAVAILABLE);
        }
            break;
        case kCONTRACTSTATUS_UNACCEPTED: {
            self.lblProgramPeriod_Data.text = LOCALIZATION(C_LOYALTY_PERIOD_NOTACCEPTED);
        }
            break;
        default: {
            self.lblProgramPeriod_Data.text = [NSString stringWithFormat:@"%@ - %@",[_contractDetails objectForKey:@"StartDate"],[_contractDetails objectForKey:@"EndDate"]];
        }
            break;
    }
}

-(void) configureStepView {
    UIImageView *imageView = [self.stepView viewWithTag:1];
    UIImageView *nextImageView = [self.stepView viewWithTag:3];
    UILabel *currentStatus = [self.stepView viewWithTag:2];
    UILabel *nextStatus = [self.stepView viewWithTag:4];
    currentStatus.font = FONT_B2;
    nextStatus.font = FONT_B2;
    
    switch([[_contractDetails objectForKey:@"Status"] intValue]) {
        case kCONTRACTSTATUS_PENDING://Pending
            imageView.image = [UIImage imageNamed:@"icn_pending_small_on"];
            nextImageView.image = [UIImage imageNamed:@"icn_processing_small_off"];
                       
            currentStatus.text = [mSession convertToContractStatus:kCONTRACTSTATUS_PENDING];
            nextStatus.text= [mSession convertToContractStatus:kCONTRACTSTATUS_INPROGRESS];
                       
            currentStatus.textColor = COLOUR_VERYDARKGREY;
            nextStatus.textColor = COLOUR_LIGHTGREY;
            break;
        case kCONTRACTSTATUS_INPROGRESS: //In Progress
            imageView.image = [UIImage imageNamed:@"icn_processing_small_on"];
            nextImageView.image = [UIImage imageNamed:@"icn_ongoing_off"];
            
            currentStatus.text = [mSession convertToContractStatus:kCONTRACTSTATUS_INPROGRESS];
            nextStatus.text= [mSession convertToContractStatus:kCONTRACTSTATUS_COMPLETED];
            
            currentStatus.textColor = COLOUR_VERYDARKGREY;
            nextStatus.textColor = COLOUR_LIGHTGREY;
            break;
            
        case kCONTRACTSTATUS_COMPLETED: //Completed
            imageView.image = [UIImage imageNamed:@"icn_processing_small_on"];
            nextImageView.image = [UIImage imageNamed:@"icn_ongoing_on"];
            
            currentStatus.text = [mSession convertToContractStatus:kCONTRACTSTATUS_INPROGRESS];
            nextStatus.text= [mSession convertToContractStatus:kCONTRACTSTATUS_COMPLETED];
            
            currentStatus.textColor = COLOUR_VERYDARKGREY;
            nextStatus.textColor = COLOUR_VERYDARKGREY;
            break;
            
        case kCONTRACTSTATUS_UNDERREVIEW://Under Review
            imageView.image = [UIImage imageNamed:@"icn_underreview_small"];
            nextImageView.image = [UIImage imageNamed:@"icn_cancelled"];
            
            currentStatus.text = [mSession convertToContractStatus:kCONTRACTSTATUS_UNDERREVIEW];
            nextStatus.text= [mSession convertToContractStatus:kCONTRACTSTATUS_INCOMPLETE];
            
            currentStatus.textColor = COLOUR_VERYDARKGREY;
            nextStatus.textColor = COLOUR_LIGHTGREY;
            break;
            
        case kCONTRACTSTATUS_INCOMPLETE://Incompleted
            imageView.image = [UIImage imageNamed:@"icn_underreview_small"];
            nextImageView.image = [UIImage imageNamed:@"icn_cancelled"];
            
            currentStatus.text = [mSession convertToContractStatus:kCONTRACTSTATUS_UNDERREVIEW];
            nextStatus.text= [mSession convertToContractStatus:kCONTRACTSTATUS_INCOMPLETE];
            
            currentStatus.textColor = COLOUR_VERYDARKGREY;
            nextStatus.textColor = COLOUR_VERYDARKGREY;
            break;
            
        case kCONTRACTSTATUS_UNACCEPTED://Not Accepted
            imageView.image = [UIImage imageNamed:@"icn_pending_small_on"];
            nextImageView.image = [UIImage imageNamed:@"icn_cancelled"];
            
            currentStatus.text = [mSession convertToContractStatus:kCONTRACTSTATUS_PENDING];
            nextStatus.text= [mSession convertToContractStatus:kCONTRACTSTATUS_INCOMPLETE];
            
            currentStatus.textColor = COLOUR_VERYDARKGREY;
            nextStatus.textColor = COLOUR_VERYDARKGREY;
            break;
        default:
            break;
    }
}

-(void) createButton:(NSString *)buttonName withColor:(UIColor*)color tag:(NSInteger)tag{
    if(tag == UPDATE_BUTTON_TAG || tag == EXTEND_BUTTON_TAG || tag == CONTINUE_BUTTON_TAG) {
        [_btnViews_Button1 setTitle:buttonName forState:UIControlStateNormal];
        _btnViews_Button1.tag = tag;
        [_btnViews_Button1 setHidden:NO];
        [_btnViews_Button1 setUserInteractionEnabled: YES];
        [_btnViews_Button1 setBackgroundColor:color];
        [_buttonArray addObject:_btnViews_Button1];
    } else {
        [_btnViews_Button2 setTitle:buttonName forState:UIControlStateNormal];
        _btnViews_Button2.tag = tag;
        [_btnViews_Button2 setHidden:NO];
        [_btnViews_Button2 setUserInteractionEnabled: YES];
        [_btnViews_Button2 setBackgroundColor:color];
        [_buttonArray addObject:_btnViews_Button2];
    }
}


- (IBAction)button1Pressed:(UIButton *)sender {
     if(sender.tag == UPDATE_BUTTON_TAG) {
//        [[WebServiceManager sharedInstance]fetchLoyaltyCatalogueListWithTradeID:[_contractDetails objectForKey:@"TradeID"] vc:self];
        _requestParams =@{@"CallType"         :   @"U",
                          @"ContractReference": [_contractDetails objectForKey:@"ContractReference"],
                          @"CompanyName"      : [_contractDetails objectForKey:@"CompanyName"],
                          @"Period"           : [_contractDetails objectForKey:@"Period"],
                          @"PackageTierID"    : [_contractDetails objectForKey:@"PackageTierID"],
                          @"PremiumTarget"    : [_contractDetails objectForKey:@"PremiumTarget"],
                          @"MultipliedValue"  : [_contractDetails objectForKey:@"MultipliedValue"],
                          @"HasMultiplier"    : [_contractDetails objectForKey:@"HasMultiplier"],
                          };
        SET_LOYALTYTYPE(_requestParams);
        [[NSUserDefaults standardUserDefaults] synchronize];
         
        [[WebServiceManager sharedInstance] loadLoyaltyPackageList:_requestParams vc:self];
         
    } else if (sender.tag == EXTEND_BUTTON_TAG) {
//        [[WebServiceManager sharedInstance]fetchLoyaltyCatalogueListWithTradeID:[_contractDetails objectForKey:@"TradeID"] vc:self];
        _requestParams =@{@"CallType"         :   @"E",
                          @"ContractReference": [_contractDetails objectForKey:@"ContractReference"],
                          @"CompanyName"      : [_contractDetails objectForKey:@"CompanyName"],
                          @"Period"           : [_contractDetails objectForKey:@"Period"],
                          @"PackageTierID"    : [_contractDetails objectForKey:@"PackageTierID"],
                          @"PremiumTarget"    : [_contractDetails objectForKey:@"PremiumTarget"],
                          @"MultipliedValue"  : [_contractDetails objectForKey:@"MultipliedValue"],
                          @"HasMultiplier"    : [_contractDetails objectForKey:@"HasMultiplier"],
                          };
        SET_LOYALTYTYPE(_requestParams);
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[WebServiceManager sharedInstance] loadLoyaltyPackageList:_requestParams vc:self];
        
    } else if(sender.tag == CONTINUE_BUTTON_TAG) {
        _requestParams =@{@"CallType"         :   @"C",
                          @"ContractReference": [_contractDetails objectForKey:@"ContractReference"],
                          @"CompanyName"      : [_contractDetails objectForKey:@"CompanyName"],
                          @"Period"           : [_contractDetails objectForKey:@"Period"],
                          @"PackageTierID"    : [_contractDetails objectForKey:@"PackageTierID"],
                          @"PremiumTarget"    : [_contractDetails objectForKey:@"PremiumTarget"],
                          @"MultipliedValue"  : [_contractDetails objectForKey:@"MultipliedValue"],
                          @"HasMultiplier"    : [_contractDetails objectForKey:@"HasMultiplier"],
                          };
        SET_LOYALTYTYPE(_requestParams);
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[WebServiceManager sharedInstance] continueContract:@{@"ContractReference" :  [_contractDetails objectForKey:@"ContractReference"]} vc:self];
        
    }
}

- (IBAction)button2Pressed:(UIButton *)sender {
    if(sender.tag == TERMINATE_BUTTON_TAG) {
        _requestParams =@{@"CallType"         :   @"T",
                          @"ContractReference": [_contractDetails objectForKey:@"ContractReference"],
                          };
        SET_LOYALTYTYPE(_requestParams);
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[WebServiceManager sharedInstance] terminateContract:@{@"ContractReference" :  [_contractDetails objectForKey:@"ContractReference"],} vc:self];
    } else if (sender.tag == REDEEM_BUTTON_TAG){
        _requestParams =@{@"CallType"         :   @"R",
                          @"ContractReference": [_contractDetails objectForKey:@"ContractReference"],
                          };
        SET_LOYALTYTYPE(_requestParams);
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[WebServiceManager sharedInstance] completeContract:@{@"ContractReference" :  [_contractDetails objectForKey:@"ContractReference"],} vc:self];
    }
}

#pragma mark - WebService
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_LOYALTY_FETCHCATALOGUELIST:{
            NSDictionary *responseData = [response getGenericResponse];
            if([[responseData objectForKey:@"HasMultiplierCatalogue"]boolValue] && ![[responseData objectForKey:@"CatalogueListMultiplier"] isKindOfClass:[NSNull class]]) {
                _catalogueList = @[
                                   [responseData objectForKey:@"CatalogueList"],
                                   [responseData objectForKey:@"CatalogueListMultiplier"]] ;
            }
            else {
                _catalogueList = @[[responseData objectForKey:@"CatalogueList"]];
            }
        }
            break;
        case kWEBSERVICE_LOYALTY_LOADPACKAGELIST:{
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                if(![[[response getGenericResponse]objectForKey:@"PackageInfo"] isKindOfClass:[NSNull class]])
                {
                    [mSession pushFinalViewInNewContract:self withPreferedDict:[response getGenericResponse] forTradeId:[_contractDetails objectForKey:@"TradeID"] withCatalogue:@[]];
                } else {
                    [self popUpViewWithTitle:LOCALIZATION(C_LOYALTY_ALERT) content:LOCALIZATION(C_LOYALTY_NOPACKAGES)];     //lokalsie 8 Feb
                }
            }
        }
            break;
        case kWEBSERVICE_LOYALTY_TERMINATECONTRACT: {
            if([[[response getJSON] objectForKey:@"ResponseCode"] intValue] == 5000) {
                [mSession loadLoyaltyContract];
            }
        }
            break;
        case kWEBSERVICE_LOYALTY_CONTINUECONTRACT:{
            if([[[response getJSON] objectForKey:@"ResponseCode"] intValue] == 5000) {
                [mSession loadLoyaltyContract];
            } else {
                [self popUpViewWithTitle:LOCALIZATION(C_LOYALTY_ALERT) content:[[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Message"]];  //lokalise 8 Feb
            }
        }
            break;
        case kWEBSERVICE_LOYALTY_COMPLETECONTRACT: {
            if([[[response getJSON] objectForKey:@"ResponseCode"] intValue] == 5000) {
                NSDictionary *confirmDict =@{@"title" : LOCALIZATION(C_LOYALTY_CONFIRMATION),       //lokalise 8 Feb
                                             @"header" : LOCALIZATION(C_LOYALTY_THANKYOU),          //lokalise 8 Feb
                                             @"subtitle" : [[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Message"]
                                             };
                
                SET_LOYALTYCONFIRMORDERKEYS(confirmDict);
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [mSession loadContractConfirmView];
            } else {
                [self popUpViewWithTitle:LOCALIZATION(C_LOYALTY_ALERT) content:[[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Message"]]; //lokalise 8 Feb
            }
        }
            break;
        case kWEBSERVICE_LOYALTY_WORKSHOPPERFORMANCE:
        {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                [mSession pushLoyaltyBreakdown:[response getGenericResponse] withWorkshopList:@[] andContract:[_contractDetails objectForKey:@"ContractReference"] vc:self];
            }
        }
            break;
        default:
            break;
    }
}


@end
