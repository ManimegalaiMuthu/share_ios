//
//  LoyaltyRewardsTabViewController.h
//  Shell
//
//  Created by Nach on 19/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDVTabBarController.h"
#import "Helper.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoyaltyRewardsTabViewController : RDVTabBarController

@end

NS_ASSUME_NONNULL_END
