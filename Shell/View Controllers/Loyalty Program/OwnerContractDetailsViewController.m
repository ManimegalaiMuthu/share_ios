//
//  OwnerContractDetailsViewController.m
//  Shell
//
//  Created by Nach on 6/4/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "OwnerContractDetailsViewController.h"
#import "LoyaltyBreakdownView.h"

@interface OwnerContractDetailsViewController () <UITableViewDelegate,UITableViewDataSource,WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UIView *stepView;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet UILabel *lblContractID;
@property (weak, nonatomic) IBOutlet UILabel *lblPackageName;
@property (weak, nonatomic) IBOutlet UILabel *lblProgramPeriod_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblProgramPeriod_Data;
@property (weak, nonatomic) IBOutlet UILabel *lblMinOrder_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblMinOrder_Data;
@property (weak, nonatomic) IBOutlet UILabel *lblProgReward_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblProgReward_Data;
@property (weak, nonatomic) IBOutlet UILabel *lblContractAchievements;

@property (weak, nonatomic) IBOutlet LoyaltyBreakdownView *loyaltyBreakdownView;

@property (weak, nonatomic) IBOutlet UITableView *productDetailsTableView;
@property (weak, nonatomic) IBOutlet UITableView *productDataTableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewHeight;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *productDataScrollView;
@property (weak, nonatomic) IBOutlet UIView *tableContainerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *productsDetailsTable_Width;

@property NSArray *productGrpList;
@property NSMutableArray *monthBreakdownList;
@property NSDictionary *breakdownDetails;

@property NSMutableArray *productGrpDetailsTotalArray;

@property int currentEndMonth_Table;
@property int swipeCount_Table;

@property NSNumberFormatter *numberFormatter;

@end

@implementation OwnerContractDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if(!_isLoaded) {
           [self.btnBack setTintColor: COLOUR_VERYDARKGREY];
           self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    }
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_LOYALTY_CONTRACTDETAILS_TITLE) subtitle:@""];
    [self configureStepView];
    
    _numberFormatter = [[NSNumberFormatter alloc] init];
    [_numberFormatter setGroupingSeparator:@","];
    [_numberFormatter setGroupingSize:3];
    [_numberFormatter setUsesGroupingSeparator:YES];
    
    self.lblContractID.text = [NSString stringWithFormat:@"%@: %@",LOCALIZATION(C_LOYALTY_CONTRACTID),[self.contractDetails objectForKey:@"ContractReference"]];
    self.lblContractID.textColor = COLOUR_VERYDARKGREY;
    self.lblContractID.font = FONT_H1;
    
    self.lblPackageName.text = [NSString stringWithFormat:@"%@",[self.contractDetails objectForKey:@"PackageName"]];
    self.lblPackageName.textColor = COLOUR_VERYDARKGREY;
    self.lblPackageName.font = FONT_H1;
    
    self.lblProgramPeriod_Title.text = LOCALIZATION(C_LOYALTY_PROGRAMPERIOD);
    self.lblProgramPeriod_Title.textColor = COLOUR_VERYDARKGREY;
    self.lblProgramPeriod_Title.font = FONT_H1;
    
    self.lblMinOrder_Title.text = LOCALIZATION(C_LOYALTY_MINORDER);
    self.lblMinOrder_Title.textColor = COLOUR_VERYDARKGREY;
    self.lblMinOrder_Title.font = FONT_H1;
    
    self.lblProgReward_Title.text = LOCALIZATION(C_LOYALTY_PROGRAMREWARDS);
    self.lblProgReward_Title.textColor = COLOUR_VERYDARKGREY;
    self.lblProgReward_Title.font = FONT_H1;
    
    [self configureProgramPeriod];
    self.lblProgramPeriod_Data.textColor = COLOUR_VERYDARKGREY;
    self.lblProgramPeriod_Data.font = FONT_B1;
    
    if([[_contractDetails objectForKey:@"HasMultiplier"]boolValue]) {
        self.lblMinOrder_Data.text = [NSString stringWithFormat:@"%.0f %@",[[_contractDetails objectForKey:@"PremiumTarget"] intValue] * [[_contractDetails objectForKey:@"MultipliedValue"] floatValue],LOCALIZATION(C_LOYALTY_CARTONS)];
    } else {
        self.lblMinOrder_Data.text = [NSString stringWithFormat:@"%@ %@",[_contractDetails objectForKey:@"PremiumTarget"],LOCALIZATION(C_LOYALTY_CARTONS)];
    }
    
    self.lblMinOrder_Data.textColor = COLOUR_VERYDARKGREY;
    self.lblMinOrder_Data.font = FONT_B1;
    
    self.lblProgReward_Data.text = [NSString stringWithFormat:@"%@ %@",[_numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[_contractDetails objectForKey:@"RewardsValue"] intValue]]],LOCALIZATION(C_LOYALTY_COINS)];
    self.lblProgReward_Data.textColor = COLOUR_VERYDARKGREY;
    self.lblProgReward_Data.font = FONT_B1;
    
    self.lblContractAchievements.text = LOCALIZATION(C_LOYALTY_CONTRACTACHIEVEMENTS);
    self.lblContractAchievements.textColor = COLOUR_VERYDARKGREY;
    self.lblContractAchievements.font = FONT_H1;
    
    SET_LOYALTYTYPE(@{@"PerformanceCallType" : @"D"});
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[WebServiceManager sharedInstance] fetchLoyaltyPerformaceForContract:@{
                                                                           @"ContractReference": [self.contractDetails objectForKey:@"ContractReference"],
                                                                           @"CallType" : @"D",
                                                                           }   vc:self];
    
}

- (IBAction)backPressed:(UIButton *)sender {
    [self popSelf];
}

#pragma amark - Helper Methods
-(void) configureProgramPeriod {
    switch([[self.contractDetails objectForKey:@"Status"] intValue]) {
        case kCONTRACTSTATUS_UNACCEPTED: {
            self.lblProgramPeriod_Data.text = [NSString stringWithFormat:@"%@ %@",[self.contractDetails objectForKey:@"Period"],LOCALIZATION(C_LUBEMATCH_MONTHS)];
        }
            break;
        default: {
            self.lblProgramPeriod_Data.text = [NSString stringWithFormat:@"%@ - %@",[_contractDetails objectForKey:@"StartDate"],[_contractDetails objectForKey:@"EndDate"]];
        }
            break;
    }
}

-(void) configureStepView {
    UIImageView *imageView = [self.stepView viewWithTag:1];
    UIImageView *nextImageView = [self.stepView viewWithTag:3];
    UILabel *currentStatus = [self.stepView viewWithTag:2];
    UILabel *nextStatus = [self.stepView viewWithTag:4];
    currentStatus.font = FONT_B2;
    nextStatus.font = FONT_B2;
    
    switch([[_contractDetails objectForKey:@"Status"] intValue]) {
        case kCONTRACTSTATUS_PENDING://Pending
            imageView.image = [UIImage imageNamed:@"icn_pending_small_on"];
            nextImageView.image = [UIImage imageNamed:@"icn_processing_small_off"];
                       
            currentStatus.text = [mSession convertToContractStatus:kCONTRACTSTATUS_PENDING];
            nextStatus.text= [mSession convertToContractStatus:kCONTRACTSTATUS_INPROGRESS];
                       
            currentStatus.textColor = COLOUR_VERYDARKGREY;
            nextStatus.textColor = COLOUR_LIGHTGREY;
            break;
        case kCONTRACTSTATUS_INPROGRESS: //In Progress
            imageView.image = [UIImage imageNamed:@"icn_processing_small_on"];
            nextImageView.image = [UIImage imageNamed:@"icn_ongoing_off"];
            
            currentStatus.text = [mSession convertToContractStatus:kCONTRACTSTATUS_INPROGRESS];
            nextStatus.text= [mSession convertToContractStatus:kCONTRACTSTATUS_COMPLETED];
            
            currentStatus.textColor = COLOUR_VERYDARKGREY;
            nextStatus.textColor = COLOUR_LIGHTGREY;
            break;
            
        case kCONTRACTSTATUS_COMPLETED: //Completed
            imageView.image = [UIImage imageNamed:@"icn_processing_small_on"];
            nextImageView.image = [UIImage imageNamed:@"icn_ongoing_on"];
            
            currentStatus.text = [mSession convertToContractStatus:kCONTRACTSTATUS_INPROGRESS];
            nextStatus.text= [mSession convertToContractStatus:kCONTRACTSTATUS_COMPLETED];
            
            currentStatus.textColor = COLOUR_VERYDARKGREY;
            nextStatus.textColor = COLOUR_VERYDARKGREY;
            break;
            
        case kCONTRACTSTATUS_UNDERREVIEW://Under Review
            imageView.image = [UIImage imageNamed:@"icn_underreview_small"];
            nextImageView.image = [UIImage imageNamed:@"icn_cancelled"];
            
            currentStatus.text = [mSession convertToContractStatus:kCONTRACTSTATUS_UNDERREVIEW];
            nextStatus.text= [mSession convertToContractStatus:kCONTRACTSTATUS_INCOMPLETE];
            
            currentStatus.textColor = COLOUR_VERYDARKGREY;
            nextStatus.textColor = COLOUR_LIGHTGREY;
            break;
            
        case kCONTRACTSTATUS_INCOMPLETE://Incompleted
            imageView.image = [UIImage imageNamed:@"icn_underreview_small"];
            nextImageView.image = [UIImage imageNamed:@"icn_cancelled"];
            
            currentStatus.text = [mSession convertToContractStatus:kCONTRACTSTATUS_UNDERREVIEW];
            nextStatus.text= [mSession convertToContractStatus:kCONTRACTSTATUS_INCOMPLETE];
            
            currentStatus.textColor = COLOUR_VERYDARKGREY;
            nextStatus.textColor = COLOUR_VERYDARKGREY;
            break;
            
        case kCONTRACTSTATUS_UNACCEPTED://Not Accepted
            imageView.image = [UIImage imageNamed:@"icn_pending_small_on"];
            nextImageView.image = [UIImage imageNamed:@"icn_cancelled"];
            
            currentStatus.text = [mSession convertToContractStatus:kCONTRACTSTATUS_PENDING];
            nextStatus.text= [mSession convertToContractStatus:kCONTRACTSTATUS_INCOMPLETE];
            
            currentStatus.textColor = COLOUR_VERYDARKGREY;
            nextStatus.textColor = COLOUR_VERYDARKGREY;
            break;
        default:
            break;
    }
}

-(void) setBreakdownView {
    [_loyaltyBreakdownView loadBar:_breakdownDetails isCarousel:NO];
    
    if(SCREEN_WIDTH <= 320) {
        _productsDetailsTable_Width.constant = 85;
    }
    else if(SCREEN_WIDTH <= 375) {
        _productsDetailsTable_Width.constant = 100;
    }
    
    _productGrpList = [_breakdownDetails objectForKey:@"ProductGroupList"];
    
    _monthBreakdownList = [[NSMutableArray alloc]init];
    for(NSDictionary *dict in [_breakdownDetails objectForKey:@"MonthList"]) {
        [_monthBreakdownList addObject:dict];
    }
    
    _scrollViewHeight.constant = (_productGrpList.count + 1) * 40;
    _scrollView.contentSize = CGSizeMake(SCREEN_WIDTH - 30, (_productGrpList.count + 1) * 40);
    
    _currentEndMonth_Table = -1;
    _swipeCount_Table = 0;
    
    _productGrpDetailsTotalArray = [_breakdownDetails objectForKey:@"ProductGroupSummaryList"];
    
    [self.monthBreakdownList addObject:@{
                                       @"MonthName" : [NSString stringWithFormat:@"%@",LOCALIZATION(C_LOYALTY_TOTAL)],
                                       @"MonthNumber" : @"999",
                                       }];
    
    [self.productDetailsTableView reloadData];
    [self.productDataTableView reloadData];
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _productGrpList.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    int spaceWidth = 65;
    
    if (SCREEN_WIDTH <= 320) {
        spaceWidth = 55;
    }
    else if(SCREEN_WIDTH <= 375) {
        spaceWidth = 55;
    }
    
    if(tableView == self.productDetailsTableView) {
        if(indexPath.row == 0) {
            return [UITableViewCell new];
        } else {
            cell = [tableView dequeueReusableCellWithIdentifier:@"FixedBreakdownColumnTableViewCell" forIndexPath:indexPath];
            
            NSDictionary *cellData = [_productGrpList objectAtIndex:indexPath.row - 1];
            
            UIView *colourTag = [cell viewWithTag:1];
            colourTag.backgroundColor = [Helper colorFromHex:[cellData objectForKey:@"ProductGroupColorCodes"]];
            
            UILabel *lblName = [cell viewWithTag:2];
            lblName.text = [cellData objectForKey:@"ProductGroupName"];
            lblName.textColor = COLOUR_VERYDARKGREY;
            lblName.font = FONT_B2;
            
            return cell;
        }
    } else {
        cell = [[UITableViewCell alloc]init];
        if(indexPath.row == 0) {
            int count = 0;
            int skipCount = 0;
            for(NSDictionary *month in _monthBreakdownList) {
                if(_swipeCount_Table > skipCount ) {
                    //do nothing
                    skipCount += 1;
                }
                else {
                    UILabel *lblMonth = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x + (count * spaceWidth), cell.frame.origin.y, 60, 40)];
                    
                    if([[month objectForKey:@"MonthNumber"] isEqualToString:@"999"]) {
                        lblMonth.text = LOCALIZATION(C_LOYALTY_TOTAL);                          //lokalise 8 Feb
                        lblMonth.backgroundColor = COLOUR_VERYPALEGREY;
                        lblMonth.font = FONT_H2;
                        lblMonth.textAlignment = NSTextAlignmentCenter;
                    }
                    else {
                        lblMonth.text = [month objectForKey:@"MonthName"];
                        lblMonth.textColor = COLOUR_VERYDARKGREY;
                        lblMonth.font = FONT_H2;
                        lblMonth.numberOfLines = 0;
                    }
                    
                    [cell addSubview:lblMonth];
                    
                    if((lblMonth.frame.origin.x + 10 + _productsDetailsTable_Width.constant) > SCREEN_WIDTH - 30) {
                        _currentEndMonth_Table = count - 1;
                    }
                    
                    count += 1;
                }
            }
           
        } else {
            NSArray *monthDataList = [_breakdownDetails objectForKey:@"ProductGroupDetailsList"];
            NSString *cellProduct = [[_productGrpList objectAtIndex:indexPath.row - 1] objectForKey:@"ProductGroupID"];
            
            int count = 0;
            int skipCount = 0;
            for(NSDictionary *month in _monthBreakdownList) {
                if(_swipeCount_Table > skipCount ) {
                    //do nothing
                    skipCount += 1;
                }
                else {
                    if([[month objectForKey:@"MonthNumber"] isEqualToString:@"999"]) {
                        UILabel *lblTotal = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x + (count * spaceWidth), cell.frame.origin.y, 60, 40)];
                        
                        for(NSDictionary *grpTotalDetail in _productGrpDetailsTotalArray) {
                            if([[grpTotalDetail objectForKey:@"ProductGroupID"] isEqualToString:cellProduct]) {
                                lblTotal.text = [NSString stringWithFormat:@"%@",[grpTotalDetail objectForKey:@"TotalQuantity"]];
                            }
                        }
                        
                        lblTotal.textColor = COLOUR_VERYDARKGREY;
                        lblTotal.backgroundColor = COLOUR_VERYPALEGREY;
                        lblTotal.font = FONT_B2;
                        lblTotal.textAlignment = NSTextAlignmentCenter;
                        
                        [cell addSubview:lblTotal];
                    } else {
                        int monthNumber = [[month objectForKey:@"MonthNumber"] intValue];
                        for(NSDictionary *monthData in monthDataList)
                        {
                            if([[monthData objectForKey:@"MonthNumber"] intValue] == monthNumber && [[monthData objectForKey:@"ProductGroupID"] isEqualToString:cellProduct]) {
                                UILabel *lblMonthData = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x + (count * spaceWidth), cell.frame.origin.y, 60, 40)];
                                
                                lblMonthData.text = [monthData objectForKey:@"TotalQuantity"];
                                lblMonthData.textColor = COLOUR_VERYDARKGREY;
                                lblMonthData.font = FONT_B2;
                                lblMonthData.textAlignment = NSTextAlignmentCenter;
                                
                                [cell addSubview:lblMonthData];
                                
                                count += 1;
                            }
                        }
                    }
                }
            }
        }
    }
    
    return cell;
}

#pragma mark - Swipe Gestures
- (IBAction)tableRightSwipe:(id)sender {
    if(_swipeCount_Table != 0 ) {
        _swipeCount_Table -= 1;
        _currentEndMonth_Table -= 1;
        [self.productDataTableView reloadData];
    }
}

- (IBAction)tableLeftSwipe:(id)sender {
    if(_swipeCount_Table < _monthBreakdownList.count - 3) {
        _currentEndMonth_Table += 1;
        _swipeCount_Table += 1;
        [self.productDataTableView reloadData];
    }
}

#pragma mark - WebserviceManager
-(void) processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
       case kWEBSERVICE_LOYALTY_WORKSHOPPERFORMANCE:
        {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                _breakdownDetails = [response getGenericResponse];
                [self.loyaltyBreakdownView setHidden:NO];
                [self.scrollView setHidden:NO];
                [self setBreakdownView];
            }
        }
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
