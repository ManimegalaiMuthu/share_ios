//
//  LoyaltyWorkshopBreakdownViewController.m
//  Shell
//
//  Created by Nach on 28/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "LoyaltyWorkshopBreakdownViewController.h"
#import "LoyaltyProgramGaugeView.h"
#import "LoyaltyBreakdownView.h"

@interface LoyaltyWorkshopBreakdownViewController() <WebServiceManagerDelegate,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblSelectWorkshop;
@property (weak, nonatomic) IBOutlet UIView *workshopSelectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *workshopSelectionView_Height;

@property (weak, nonatomic) IBOutlet UILabel *lblTotalOrdered_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalOrdered_Data;
@property (weak, nonatomic) IBOutlet UILabel *lblPendingOrders_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblPendingOrders_Data;

@property (weak, nonatomic) IBOutlet UILabel *lblProgramProgress;
@property (weak, nonatomic) IBOutlet LoyaltyProgramGaugeView *loyaltyProgramGauge;
@property (weak, nonatomic) IBOutlet UILabel *lblMonthlyOverview;
@property (weak, nonatomic) IBOutlet LoyaltyBreakdownView *loyaltyBreakdownView;

@property (weak, nonatomic) IBOutlet UITableView *detailedBreakdownTable;
@property (weak, nonatomic) IBOutlet UITableView *productDetailsTableView;
@property (weak, nonatomic) IBOutlet UITableView *productDataTableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewHeight;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *productDataScrollView;
@property (weak, nonatomic) IBOutlet UIView *tablesContainerView;

@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@property NSDictionary *selectedWorkshop;
@property NSArray *productCategoryDetailsList;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailedBreakdown;
@property (weak, nonatomic) IBOutlet UILabel *lblFooter;

@property int productGrpTotal;
@property int productTierTotal;
@property int productCategoryTotal;

@property NSMutableArray *monthDetailsDict;
@property NSArray *productGrpArray;
@property NSArray *productTierArray;
@property NSArray *productCategoryArray;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *productDetailsView_Width;

@property NSArray *productGrpDetailsArray;
@property NSArray *productTierDetailsArray;
@property NSArray *productCategoryDetailsArray;

@property NSMutableArray *productGrpDetailsTotalArray;
@property NSMutableArray *productTierDetailsTotalArray;
@property NSMutableArray *productCategoryDetailsTotalArray;

@property int currentGroupIndex;
@property int currentEndMonth;
@property int swipeCount;
@end

@implementation LoyaltyWorkshopBreakdownViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.btnBack setTintColor: COLOUR_VERYDARKGREY];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_LOYALTY_WORKSHOPBREAKDOWN) subtitle:@""];       //lokalise 8 Feb
    [Helper setNavigationBarAppearanceTitleLabelInTwoLines:self];
    
    if(SCREEN_WIDTH <= 320 || SCREEN_WIDTH <= 375) {
        _productDetailsView_Width.constant = 100;
    }
    
    if([_workshopList isEqualToArray:@[]]) {
        self.workshopSelectionView.hidden = YES;
        self.workshopSelectionView_Height.constant = 0;
    } else {
        self.workshopSelectionView.hidden = NO;
        self.workshopSelectionView_Height.constant = 30;
    }
    
    self.lblSelectWorkshop.textColor = COLOUR_VERYDARKGREY;
    self.lblSelectWorkshop.font = FONT_B1;
    
    self.lblProgramProgress.text = LOCALIZATION(C_LOYALTY_PROGRAMPROGRESS);     //lokalise 8 Feb
    self.lblProgramProgress.textColor = COLOUR_VERYDARKGREY;
    self.lblProgramProgress.font = FONT_H1;
    
    self.lblMonthlyOverview.text = LOCALIZATION(C_LOYALTY_MONTHLYOVERVIEW);     //lokalise 8 Feb
    self.lblMonthlyOverview.textColor = COLOUR_VERYDARKGREY;
    self.lblMonthlyOverview.font = FONT_H1;
    
    self.lblDetailedBreakdown.text = LOCALIZATION(C_LOYALTY_DETAILEDBREAKDOWN); //lokalise 8 Feb
    self.lblDetailedBreakdown.textColor = COLOUR_VERYDARKGREY;
    self.lblDetailedBreakdown.font = FONT_H1;
    
    self.lblFooter.text = [NSString stringWithFormat:@"%@ \n %@",[_breakdownDetails objectForKey:@"TextBDDescription1"],[_breakdownDetails objectForKey:@"TextBDDescription2"]];
    self.lblFooter.textColor = COLOUR_VERYDARKGREY;
    self.lblFooter.font = FONT_B3;
    
    _currentEndMonth = -1;
    _swipeCount = 0;
    
    [self setupInterface];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_LOYALTY_WORKSHOPBREAKDOWN) screenClass:nil];
}

-(void) setupInterface {
    
    self.currentGroupIndex = 0;
    
    
    _productCategoryDetailsList = [_breakdownDetails objectForKey:@"ProductCategoryDetailsList"];
    
    for(NSDictionary *dict in _workshopList) {
        if([[dict objectForKey:@"ContractReference"] isEqualToString:_contractRef]) {
            self.lblSelectWorkshop.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"CompanyName"]];
        }
    }
    
    self.lblTotalOrdered_Title.text = [_breakdownDetails objectForKey:@"TextTotalOrdered"];
    self.lblTotalOrdered_Title.font = FONT_H1;
    self.lblTotalOrdered_Title.textColor = COLOUR_RED;
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%.1f ",[[_breakdownDetails objectForKey:@"TotalOrdered"]floatValue]] attributes:@{
                                                                                                                                                                                          NSFontAttributeName : FONT_H(18),
                                                                                                                                                                                          NSForegroundColorAttributeName : COLOUR_RED,                                                                                                      }];
    [attrString appendAttributedString: [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(C_LOYALTY_CARTONS) attributes:@{
                                                                                                                                NSFontAttributeName : FONT_B3,
                                                                                                                                NSForegroundColorAttributeName: COLOUR_RED,
                                                                                                                                }]];  //lokalised
    self.lblTotalOrdered_Data.attributedText = attrString;
    
    self.lblPendingOrders_Title.text = [NSString stringWithFormat:@"%@*",[_breakdownDetails objectForKey:@"TextPendingOrders"]];
    self.lblPendingOrders_Title.font = FONT_B2;
    self.lblPendingOrders_Title.textColor = COLOUR_VERYDARKGREY;
    
    NSMutableAttributedString *textPendingOrders = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%.1f ",[[
                                                                                                                        _breakdownDetails objectForKey:@"TotalPending"] floatValue] ] attributes:@{
                                                                                                                                                                                                 NSFontAttributeName : FONT_H1 ,
                                                                                                                                                                                                 }];
    [textPendingOrders appendAttributedString: [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(C_LOYALTY_CARTONS) attributes:@{
                                                                                                                                       NSFontAttributeName : FONT_B3,
                                                                                                                                       }]];  //lokalised
    self.lblPendingOrders_Data.attributedText = textPendingOrders;
    self.lblPendingOrders_Data.textColor = COLOUR_VERYDARKGREY;
    
    [self.view setNeedsDisplay];
    NSDictionary *viewData = @{
//                               @"TextTotalCarton" : @"Total Carton",
                                                              @"TextTotalCarton" : [_breakdownDetails objectForKey:@"TextTotalCartons"] == nil  && [[_breakdownDetails objectForKey:@"TextTotalCartons"] isKindOfClass:[NSNull class]]? @"" : [_breakdownDetails objectForKey:@"TextTotalCartons"],
//                               @"TextPremiumTier" : @"Premium Tier",
                                                              @"TextPremiumTier" :  [_breakdownDetails objectForKey:@"TextPremiumTier"] == nil && [[_breakdownDetails objectForKey:@"TextPremiumTier"] isKindOfClass:[NSNull class]]? @"" : [_breakdownDetails objectForKey:@"TextPremiumTier"],
                               @"ColorCodeDefaultOverall" : [_breakdownDetails objectForKey:@"ColorCodeDefaultOverall"],
                               @"ColorCodeDefaultPremium" : [_breakdownDetails objectForKey:@"ColorCodeDefaultPremium"],
                               @"NoOfOverallOrdered" : [_breakdownDetails objectForKey:@"TotalOrdered"],
                               @"NoOfPremiumOrdered" : [_breakdownDetails objectForKey:@"TotalPremiumAcheived"],
                               @"TotalPremiumTarget" : [_breakdownDetails objectForKey:@"TotalPremiumTarget"],
                               @"OverallTarget" : [_breakdownDetails objectForKey:@"TotalTargetQty"],
                               @"ColorCodeOverall" : [_breakdownDetails objectForKey:@"ColorCodeOverall"],
                               @"ColorCodePremium" : [_breakdownDetails objectForKey:@"ColorCodePremium"],
                               };
    _loyaltyProgramGauge.isAgri = false;
    [_loyaltyProgramGauge loadBar: viewData isCarousel:NO];
    
    [self.view setNeedsDisplay];
    [_loyaltyBreakdownView loadBar:_breakdownDetails isCarousel:NO];
    
    _productGrpTotal = 0;
    _productTierTotal = 0;
    _productCategoryTotal = 0;
    
    _monthDetailsDict = [[NSMutableArray alloc]init];
    
    for(NSDictionary *dict in [_breakdownDetails objectForKey:@"MonthList"]) {
        [_monthDetailsDict addObject:dict];
    }
    _productGrpArray = [_breakdownDetails objectForKey:@"ProductGroupList"];
    _productTierArray = [_breakdownDetails objectForKey:@"ProductTierList"];
    _productCategoryArray = [_breakdownDetails objectForKey:@"ProductCategoryList"];
    
    _productGrpDetailsArray = [_breakdownDetails objectForKey:@"ProductGroupDetailsList"];
    _productTierDetailsArray = [_breakdownDetails objectForKey:@"ProductTierDetailsList"];
    _productCategoryDetailsArray = [_breakdownDetails objectForKey:@"ProductCategoryDetailsList"];
    
    _productGrpDetailsTotalArray = [[NSMutableArray alloc]init];
    _productCategoryDetailsTotalArray = [[NSMutableArray alloc]init];
    _productTierDetailsTotalArray = [[NSMutableArray alloc]init];
    
    if(_productGrpArray.count > 0) {
        for(NSDictionary * dict in _productGrpArray) {
            NSString *productGrpId = [dict objectForKey:@"ProductGroupID"];
            for(NSDictionary *grpDetails in [_breakdownDetails objectForKey:@"ProductGroupSummaryList"]) {
                if([productGrpId isEqualToString: [grpDetails objectForKey:@"ProductGroupID"]]) {
                    _productGrpTotal += [[grpDetails objectForKey:@"TotalQuantity"]intValue];
                    [_productGrpDetailsTotalArray addObject:[grpDetails objectForKey:@"TotalQuantity"]];
                }
            }
        }
    }
    
    if(_productTierArray.count > 0) {
        for(NSDictionary * dict in _productTierArray) {
            NSString *productGrpId = [dict objectForKey:@"ProductTierID"];
            for(NSDictionary *grpDetails in [_breakdownDetails objectForKey:@"ProductTierSummaryList"]) {
                if([productGrpId isEqualToString: [grpDetails objectForKey:@"ProductTierID"]]) {
                    _productTierTotal += [[grpDetails objectForKey:@"TotalQuantity"]intValue];
                    [_productTierDetailsTotalArray addObject:[grpDetails objectForKey:@"TotalQuantity"]];
                }
            }
        }
    }
    
    if(_productCategoryArray.count > 0) {
        for(NSDictionary * dict in _productCategoryArray) {
            NSString *productGrpId = [dict objectForKey:@"ProductTypeID"];
            for(NSDictionary *grpDetails in [_breakdownDetails objectForKey:@"ProductCategorySummaryList"]) {
                if([productGrpId isEqualToString: [grpDetails objectForKey:@"ProductTypeID"]]) {
                    _productCategoryTotal += [[grpDetails objectForKey:@"TotalQuantity"]intValue];
                    [_productCategoryDetailsTotalArray addObject:[grpDetails objectForKey:@"TotalQuantity"]];
                }
            }
        }
    }
    
    [self.monthDetailsDict addObject:@{
                            @"MonthName" : [NSString stringWithFormat:@"%@",LOCALIZATION(C_LOYALTY_TOTAL)],     //lokalise 8 Feb
                            @"MonthNumber" : @"999",
                            }];
    
    [self.detailedBreakdownTable reloadData];
    [self.productDetailsTableView reloadData];
    [self.productDataTableView reloadData];

}

#pragma mark - outlet
- (IBAction)workshopSelectionPressed:(id)sender {
    self.isPositionRequired = YES;
    [self popUpListViewWithTitle: LOCALIZATION(C_RU_BREAKDOWN_CHOOSEWORKSHOPS) listArray:[self getWorkshopNames] withSearchField: YES];     //lokalised
}
- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

#pragma mark - WebService
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_LOYALTY_WORKSHOPPERFORMANCE:
        {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                _breakdownDetails = [response getGenericResponse];
                [self setupInterface];
            }
        }
            break;
        default:
            break;
    }
}

#pragma mark - View methods
-(NSArray *)getWorkshopNames {
    NSMutableArray *workshopNames = [[NSMutableArray alloc]init];
    if(_workshopList == nil || [_workshopList isKindOfClass:[NSNull class]]){
        return @[];
    }
    for(NSDictionary *dict in _workshopList) {
        [workshopNames addObject: [dict objectForKey:@"CompanyName"]];
    }
    return workshopNames;
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        NSDictionary *dict = [selection objectAtIndex:0];
        self.lblSelectWorkshop.text = [dict objectForKey:@"Name"];
        
        NSIndexPath *indexPath = [dict objectForKey:@"IndexPath"];
        _contractRef = [[_workshopList objectAtIndex:indexPath.row] objectForKey:@"ContractReference"];
        
        SET_LOYALTYTYPE(@{@"PerformanceCallType" : @"D"});
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[WebServiceManager sharedInstance]fetchLoyaltyPerformaceForContract:@{
                                                                               @"ContractReference": _contractRef,
                                                                               @"CallType" : @"D",
                                                                               }   vc:self];
    }
}

#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == self.detailedBreakdownTable) {
        return 1;
    } else {
        switch(self.currentGroupIndex) {
            case 0: {
                return _productGrpArray.count + 1;
            }
                break;
            case 1:{
                return _productCategoryArray.count + 1;
            }
                break;
            default: {
                return _productTierArray.count + 1;
            }
                break;
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    int spaceWidth = 65;
    
    if (SCREEN_WIDTH <= 320) {
        spaceWidth = 55;
    }
    else if(SCREEN_WIDTH <= 375) {
        spaceWidth = 55;
    }
    
    if(tableView == self.detailedBreakdownTable) {
        cell = [tableView dequeueReusableCellWithIdentifier: @"ChartCell"];
        cell.backgroundColor = COLOUR_WHITE;
        
        UILabel *lblTitle = [cell viewWithTag: 6];
        
        switch (self.currentGroupIndex) {
            case 0:
                [self constructCircle: [cell viewWithTag: 5] breakdownArray:@{
                                                                              @"Total" : @(_productGrpTotal == 0 ? 1: _productGrpTotal),
                                                                              @"List"  : _productGrpArray,
                                                                              @"Details" : _productGrpDetailsTotalArray,
                                                                              @"Key" : @"ProductGroupColorCodes",
                                                                              }];
                lblTitle.text = [NSString stringWithFormat:@"%@",LOCALIZATION(C_LOYALTY_BYPRODUCTGROUP) ];      //lokalise 8 Feb
                [self scrollHeightCalculation];
                break;
            case 1:
                [self constructCircle: [cell viewWithTag: 5] breakdownArray: @{
                                                                               @"Total" : @(_productCategoryTotal == 0 ? 1: _productCategoryTotal),
                                                                               @"List"  : _productCategoryArray,
                                                                               @"Details" : _productCategoryDetailsTotalArray,
                                                                               
                                                                               @"Key" : @"ProductTypeColorCodes",
                                                                               }];
                lblTitle.text = [NSString stringWithFormat:@"%@",LOCALIZATION(C_LOYALTY_BYPRODUCTCATEGORY) ];       //lokalise 8 Feb
                break;
            case 2:
                [self constructCircle: [cell viewWithTag: 5] breakdownArray: @{
                                                                               @"Total" : @(_productTierTotal == 0 ? 1: _productTierTotal),
                                                                               @"List"  : _productTierArray,
                                                                               @"Details" : _productTierDetailsTotalArray,
                                                                               @"Key" : @"ProductTierColorCodes",
                                                                               }];
                lblTitle.text = [NSString stringWithFormat:@"%@",LOCALIZATION(C_LOYALTY_BYPRODUCTTIER) ];           //lokalise 8 Feb
                break;
            default:
                break;
                
        }
        lblTitle.textColor = COLOUR_VERYDARKGREY;
        lblTitle.font = FONT_H1;
        
        UIButton *btnLeft = [cell viewWithTag: 11];
        if (self.currentGroupIndex == 0)
        {
            btnLeft.hidden = YES;
        }
        else
        {
            btnLeft.hidden = NO;
        }
        
        
        UIButton *btnRight = [cell viewWithTag: 12];
        if (self.currentGroupIndex ==  2)
        {
            btnRight.hidden = YES;
        }
        else
        {
            btnRight.hidden = NO;
        }
    } else if(tableView == self.productDetailsTableView) {
        if(indexPath.row == 0) {
            return [UITableViewCell new];
        } else {
            cell = [tableView dequeueReusableCellWithIdentifier:@"FixedBreakdownColumnTableViewCell" forIndexPath:indexPath];
            
            NSDictionary *cellData;
            
            switch(self.currentGroupIndex) {
                case 0 :
                    cellData = [_productGrpArray objectAtIndex:indexPath.row - 1];
                    break;
                case 1:
                    cellData = [_productCategoryArray objectAtIndex:indexPath.row - 1];
                    break;
                case 2:
                    cellData = [_productTierArray objectAtIndex:indexPath.row - 1];
                    break;
            }
            
            UIView *colourTag = [cell viewWithTag:1];
            switch(self.currentGroupIndex) {
                case 0:
                    colourTag.backgroundColor = [Helper colorFromHex:[cellData objectForKey:@"ProductGroupColorCodes"]];
                    break;
                case 1:
                    colourTag.backgroundColor = [Helper colorFromHex:[cellData objectForKey:@"ProductTypeColorCodes"]];
                    break;
                case 2:
                    colourTag.backgroundColor = [Helper colorFromHex:[cellData objectForKey:@"ProductTierColorCodes"]];
                    break;
            }
            
            UILabel *lblName = [cell viewWithTag:2];
            switch(self.currentGroupIndex) {
                case 0:
                    lblName.text = [cellData objectForKey:@"ProductGroupName"];
                    break;
                case 1:
                    lblName.text = [cellData objectForKey:@"ProductType"];
                    break;
                default:
                    lblName.text = [cellData objectForKey:@"ProductTierValue"];
                    break;
            }
            lblName.textColor = COLOUR_VERYDARKGREY;
            lblName.font = FONT_B2;
            
            return cell;
        }
    } else {
        cell = [[UITableViewCell alloc]init];
        if(indexPath.row == 0) {
            int count = 0;
            int skipCount = 0;
            for(NSDictionary *month in _monthDetailsDict) {
                if(_swipeCount > skipCount) {
                    //do nothing
                    skipCount += 1;
                }
                else {
                    UILabel *lblMonth = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x + (count * spaceWidth), cell.frame.origin.y, 60, 40)];
                    
                    if([[month objectForKey:@"MonthNumber"] isEqualToString:@"999"]) {
                        lblMonth.text = LOCALIZATION(C_LOYALTY_TOTAL);      //lokalise 8 Feb
                        lblMonth.backgroundColor = COLOUR_VERYPALEGREY;
                        lblMonth.font = FONT_H2;
                        lblMonth.textAlignment = NSTextAlignmentCenter;
                    }
                    else {
                        lblMonth.text = [month objectForKey:@"MonthName"];
                        lblMonth.textColor = COLOUR_VERYDARKGREY;
                        lblMonth.font = FONT_H2;
                    }
                    
                    [cell addSubview:lblMonth];
                    
                    
                    if((lblMonth.frame.origin.x + 10 + _productDetailsView_Width.constant) > SCREEN_WIDTH - 30) {
                        _currentEndMonth = count - 1;
                    }
                    
                   
                    
                    count += 1;
                }
            }
            
        } else {
            NSArray *monthDataList;
            switch(self.currentGroupIndex) {
                case 0:
                    monthDataList = [_breakdownDetails objectForKey:@"ProductGroupDetailsList"];
                    break;
                case 1:
                    monthDataList = [_breakdownDetails objectForKey:@"ProductCategoryDetailsList"];
                    break;
                default:
                    monthDataList = [_breakdownDetails objectForKey:@"ProductTierDetailsList"];
                    break;
            }
            NSString *cellProduct ;
            switch(self.currentGroupIndex) {
                case 0:
                    cellProduct= [[_productGrpArray objectAtIndex:indexPath.row - 1] objectForKey:@"ProductGroupID"];
                    break;
                case 1:
                    cellProduct= [[_productCategoryArray objectAtIndex:indexPath.row - 1] objectForKey:@"ProductTypeID"];
                    break;
                default:
                   cellProduct= [[_productTierArray objectAtIndex:indexPath.row - 1] objectForKey:@"ProductTierID"];
                    break;
            }
            
            int count = 0;
            int skipCount = 0;
            for(NSDictionary *month in _monthDetailsDict) {
                if(_swipeCount > skipCount) {
                    //do nothing
                    skipCount += 1;
                } else {
                    if([[month objectForKey:@"MonthNumber"] isEqualToString:@"999"]) {
                        UILabel *lblTotal = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x + (count * spaceWidth), cell.frame.origin.y, 60, 40)];
                        
                        switch(self.currentGroupIndex) {
                            case 0:
                            {
                                lblTotal.text = [NSString stringWithFormat:@"%@",[_productGrpDetailsTotalArray objectAtIndex:indexPath.row - 1]];
                            }
                                break;
                            case 1:
                            {
                                lblTotal.text = [NSString stringWithFormat:@"%@",[_productCategoryDetailsTotalArray objectAtIndex:indexPath.row - 1]];
                            }
                                break;
                            case 2:
                            {
                                lblTotal.text = [NSString stringWithFormat:@"%@",[_productTierDetailsTotalArray objectAtIndex:indexPath.row - 1]];
                            }
                                break;
                        }
                        
                        lblTotal.textColor = COLOUR_VERYDARKGREY;
                        lblTotal.backgroundColor = COLOUR_VERYPALEGREY;
                        lblTotal.font = FONT_B2;
                        lblTotal.textAlignment = NSTextAlignmentCenter;
                        
                        [cell addSubview:lblTotal];
                    } else {
                        int monthNumber = [[month objectForKey:@"MonthNumber"] intValue];
                        for(NSDictionary *monthData in monthDataList)
                        {
                            NSString *cellMonthProduct ;
                            switch(self.currentGroupIndex) {
                                case 0:
                                    cellMonthProduct= [monthData objectForKey:@"ProductGroupID"];
                                    break;
                                case 1:
                                    cellMonthProduct= [monthData objectForKey:@"ProductTypeID"];
                                    break;
                                default:
                                    cellMonthProduct= [monthData objectForKey:@"ProductTierID"];
                                    break;
                            }
                            if([[monthData objectForKey:@"MonthNumber"] intValue] == monthNumber && [cellMonthProduct isEqualToString:cellProduct]) {
                                UILabel *lblMonthData = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x + (count * spaceWidth), cell.frame.origin.y, 60, 40)];
                                
                                lblMonthData.text = [monthData objectForKey:@"TotalQuantity"];
                                lblMonthData.textColor = COLOUR_VERYDARKGREY;
                                lblMonthData.font = FONT_B2;
                                
                                [cell addSubview:lblMonthData];
                                
                                count += 1;
                            }
                        }
                    }
                }
            }
        }
    }
    return cell;
}

#define START_ANGLE -90 //12 o clock
#define degreesToRadians(degrees)((M_PI * degrees)/180)
-(void) constructCircle:(UIView *)view breakdownArray:(NSDictionary *)dictionary
{
    //breakdownArray = tier or category. pick out the percent
    CGFloat lastAngleInRadians = degreesToRadians(START_ANGLE);
    CGFloat target = 360.0 / [[dictionary objectForKey:@"Total"] floatValue];
    
    
    int i = 0;
    for (NSDictionary *dict in [dictionary objectForKey:@"List"])
    {
        CGFloat percentage = [[[dictionary objectForKey:@"Details"] objectAtIndex:i]floatValue];
        CGFloat nextAngleInRadians = lastAngleInRadians + (degreesToRadians(target * percentage));
        
        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
        [bezierPath addArcWithCenter: CGPointMake(view.frame.size.width / 2, view.frame.size.width / 2)
                              radius: 70
                          startAngle: lastAngleInRadians
                            endAngle: nextAngleInRadians
                           clockwise:YES];
        
        CAShapeLayer *progressLayer = [[CAShapeLayer alloc] init];
        [progressLayer setPath:bezierPath.CGPath];
        
        UIColor *color = [Helper colorFromHex: [dict objectForKey:[dictionary objectForKey:@"Key"]]];
        [progressLayer setStrokeColor: [color CGColor]];
        
        [progressLayer setFillColor:[UIColor clearColor].CGColor];
        [progressLayer setLineWidth: 30];
        [view.layer addSublayer:progressLayer];
        
        //get ready for the next arc draw
        lastAngleInRadians = nextAngleInRadians;
        i++;
    }
    
    //complete circle if not completed
    if (lastAngleInRadians != degreesToRadians(START_ANGLE))
    {
        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
        [bezierPath addArcWithCenter: CGPointMake(view.frame.size.width / 2, view.frame.size.width / 2)
                              radius: 85
                          startAngle: lastAngleInRadians
                            endAngle: degreesToRadians(START_ANGLE)
                           clockwise:YES];
        
        CAShapeLayer *progressLayer = [[CAShapeLayer alloc] init];
        [progressLayer setPath:bezierPath.CGPath];
        
        UIColor *color = [Helper colorFromHex: @"#D9D9D9"];
        [progressLayer setStrokeColor: [color CGColor]];
        
        [progressLayer setFillColor:[UIColor whiteColor].CGColor];
        [progressLayer setLineWidth: 1];
        [view.layer addSublayer:progressLayer];
        
        UIBezierPath *bezierPath2 = [UIBezierPath bezierPath];
        [bezierPath2 addArcWithCenter: CGPointMake(view.frame.size.width / 2, view.frame.size.width / 2)
                              radius:  55
                          startAngle: lastAngleInRadians
                            endAngle: degreesToRadians(START_ANGLE)
                           clockwise:YES];
        
        CAShapeLayer *progressLayer2 = [[CAShapeLayer alloc] init];
        [progressLayer2 setPath:bezierPath2.CGPath];
        
        [progressLayer2 setStrokeColor: [color CGColor]];
        
        [progressLayer2 setFillColor:[UIColor clearColor].CGColor];
        [progressLayer2 setLineWidth: 1];
        [view.layer addSublayer:progressLayer2];
    }
    
    if(lastAngleInRadians == degreesToRadians(START_ANGLE)) {
        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
        [bezierPath addArcWithCenter: CGPointMake(view.frame.size.width / 2, view.frame.size.width / 2)
                              radius: 85
                          startAngle: degreesToRadians(START_ANGLE)
                            endAngle: degreesToRadians(-90.2)
                           clockwise:YES];
        
        CAShapeLayer *progressLayer = [[CAShapeLayer alloc] init];
        [progressLayer setPath:bezierPath.CGPath];
        
        UIColor *color = [Helper colorFromHex: @"#D9D9D9"];
        [progressLayer setStrokeColor: [color CGColor]];
        
        [progressLayer setFillColor:[UIColor whiteColor].CGColor];
        [progressLayer setLineWidth: 1];
        [view.layer addSublayer:progressLayer];
        
        UIBezierPath *bezierPath2 = [UIBezierPath bezierPath];
        [bezierPath2 addArcWithCenter: CGPointMake(view.frame.size.width / 2, view.frame.size.width / 2)
                               radius: 55
                           startAngle: degreesToRadians(START_ANGLE)
                             endAngle: degreesToRadians(-90.2)
                            clockwise:YES];
        
        CAShapeLayer *progressLayer2 = [[CAShapeLayer alloc] init];
        [progressLayer2 setPath:bezierPath2.CGPath];
        
        [progressLayer2 setStrokeColor: [color CGColor]];
        
        [progressLayer2 setFillColor:[UIColor clearColor].CGColor];
        [progressLayer2 setLineWidth: 1];
        [view.layer addSublayer:progressLayer2];
    }
}

- (IBAction)leftButtonPressed:(UIButton *)sender
{
    self.currentGroupIndex--;
    if (self.currentGroupIndex < 0)
    {
        self.currentGroupIndex = 0;
    }
    [self scrollHeightCalculation];
    [self.detailedBreakdownTable reloadData];
    [self.productDetailsTableView reloadData];
    [self.productDataTableView reloadData];
}

- (IBAction)rightButtonPressed:(UIButton *)sender
{
    self.currentGroupIndex++;
    
    if(self.currentGroupIndex > 2) {
        self.currentGroupIndex = 2;
    }
     [self scrollHeightCalculation];
    [self.detailedBreakdownTable reloadData];
    [self.productDetailsTableView reloadData];
    [self.productDataTableView reloadData];
}

-(void) scrollHeightCalculation {
    switch(self.currentGroupIndex) {
        case 0: {
            _scrollViewHeight.constant = (_productGrpArray.count + 1) * 40;
            _productDataScrollView.contentSize = CGSizeMake((_monthDetailsDict.count * 100), (_productGrpArray.count + 1) * 40);
            _scrollView.contentSize = CGSizeMake(100 + (_monthDetailsDict.count * 100), (_productGrpArray.count + 1) * 40);
        }
            break;
        case 1:{
            _scrollViewHeight.constant = (_productCategoryArray.count + 1) * 40;
            _productDataScrollView.contentSize = CGSizeMake((_monthDetailsDict.count * 100), (_productCategoryArray.count + 1) * 40);
            _scrollView.contentSize = CGSizeMake(100 + (_monthDetailsDict.count * 100), (_productGrpArray.count + 1) * 40);
        }
            break;
        default: {
            _scrollViewHeight.constant = (_productTierArray.count + 1) * 40;
            _productDataScrollView.contentSize = CGSizeMake((_monthDetailsDict.count * 100), (_productTierArray.count + 1) * 40);
            _scrollView.contentSize = CGSizeMake(100 + (_monthDetailsDict.count * 100), (_productGrpArray.count + 1) * 40);
        }
            break;
    }
}

#pragma mark - Swipe Gestures
- (IBAction)tableRightSwipe:(id)sender {
    if(_swipeCount != 0) {
        _swipeCount -= 1;
        _currentEndMonth -= 1;
        [self.productDataTableView reloadData];
    }
}
- (IBAction)tableLeftSwipe:(id)sender {
    if(_swipeCount < _monthDetailsDict.count - 3) {
        _currentEndMonth += 1;
        _swipeCount += 1;
        [self.productDataTableView reloadData];
    }
}

@end
