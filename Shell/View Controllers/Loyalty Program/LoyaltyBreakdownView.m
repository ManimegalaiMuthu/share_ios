//
//  LoyaltyBreakdownView.m
//  Shell
//
//  Created by Nach on 18/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "LoyaltyBreakdownView.h"
#import "Helper.h"
#import "Headers.h"

@implementation LoyaltyBreakdownView

-(void)awakeFromNib
{
    [super awakeFromNib];
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        //https://stackoverflow.com/questions/30335089/reuse-a-uiview-xib-in-storyboard/37668821#37668821
        // 1. load the interface
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        // 2. add as subview
        [self addSubview:self.contentView ];
        _breakdownMonthList = nil;

        [self setNeedsLayout];
    }
    return self;
}

-(void) loadBar:(NSDictionary *) responseData isCarousel:(BOOL)isCarousel{
    
    _currentEndMonth_Bar = -1;
    _swipeCount_Bar = 0;
    
    _viewData = responseData;
    _isCarousel = isCarousel;
    
    _breakdownMonthList = [_viewData objectForKey:@"MonthList"];
    
    [self setupInterface];
    
    [self setupGraph];
}

-(void) setupGraph {
    int spaceWidth = 60;
    int spaceBetweenAxisStartAndBarOne = 20.0;
    int barwidth = 50;
    
    if(SCREEN_WIDTH <= 320) {
        spaceWidth = 50;
        barwidth = 40;
        spaceBetweenAxisStartAndBarOne = 8;
    }
    if(SCREEN_WIDTH <= 375) {
        spaceWidth = 55;
        barwidth = 45;
    }
    
    //clear previously drawn circle
    NSMutableArray *layerToRemoveArray = [[NSMutableArray alloc] init]; //to hold references to actual progress bar
    
    for (CALayer *layer in [self.graphView.layer sublayers])
    {
        if ([layer isKindOfClass: [CAShapeLayer class]])
        {
            [layerToRemoveArray addObject: layer];
        }
    }
    
    if(_labelsAdded == nil) {
        _labelsAdded = [[NSMutableArray alloc]init];
    } else {
        for(UILabel *label in _labelsAdded) {
            [label removeFromSuperview];
        }
    }
    
    //remove referenced layers from superlayer
    for (CALayer *layer in layerToRemoveArray)
    {
        [layer removeFromSuperlayer];
    }
    
    [self layoutSubviews];
    
    NSArray *monthlyOverview = [_viewData objectForKey:@"MonthlyOverviewList"];
    NSDictionary *firstMonthData = [monthlyOverview objectAtIndex:0];
    
    if(_isCarousel) {
         _scaleFactor = 90.0 / [[firstMonthData objectForKey:@"TargetQty"]doubleValue];
    } else {
         _scaleFactor = 100.0 / [[firstMonthData objectForKey:@"TargetQty"]doubleValue];
    }
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    [shapeLayer setFillColor:[[UIColor clearColor] CGColor]];
    [shapeLayer setStrokeColor:[[Helper colorFromHex:[_viewData objectForKey:@"ColorCodeMinimum"]] CGColor]];
    [shapeLayer setLineWidth:2.0f];
    [shapeLayer setLineDashPattern:
     [NSArray arrayWithObjects:[NSNumber numberWithInt:5],
      [NSNumber numberWithInt:5],nil]];
    int y = self.barBottomLine.frame.origin.y - (_scaleFactor * [[firstMonthData objectForKey:@"MonthlyMinTarget"]doubleValue]);
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, self.barBottomLine.bounds.origin.x, y);
    CGPathAddLineToPoint(path, NULL,self.barBottomLine.bounds.origin.x + _graphViewWidth.constant ,y);
    [shapeLayer setPath:path];
    CGPathRelease(path);
    
    UILabel *minTarget = [[UILabel alloc]init];
    minTarget.text =[ NSString stringWithFormat:@"%@ %@",[firstMonthData objectForKey:@"MonthlyMinTarget"],LOCALIZATION(C_LOYALTY_CTNS)];
    minTarget.textColor = [Helper colorFromHex:[_viewData objectForKey:@"ColorCodeMinimum"]];
    minTarget.font = FONT_B(8);
    minTarget.frame = CGRectMake(self.barBottomLine.frame.origin.x, y - 12.0, 30.0, 10.0);
    [minTarget sizeToFit];
    [self.graphView addSubview:minTarget];
    [self.labelsAdded addObject:minTarget];
    [[self.graphView layer] addSublayer:shapeLayer];
    
    if([[firstMonthData objectForKey:@"PremiumQty"]doubleValue] != 0.0) {
        _isPremiumAvailable = true;
        CAShapeLayer *minPremiumShapeLayer = [CAShapeLayer layer];
        [minPremiumShapeLayer setFillColor:[[UIColor clearColor] CGColor]];
        [minPremiumShapeLayer setLineWidth:2.0f];
        [minPremiumShapeLayer setLineDashPattern:
         [NSArray arrayWithObjects:[NSNumber numberWithInt:5],
          [NSNumber numberWithInt:5],nil]];
        [minPremiumShapeLayer setStrokeColor:[[Helper colorFromHex:[_viewData objectForKey:@"ColorCodePremiumTier"]] CGColor]];
        y = self.barBottomLine.frame.origin.y - (_scaleFactor * [[firstMonthData objectForKey:@"PremiumQty"]doubleValue]);
        CGMutablePathRef premiumPath = CGPathCreateMutable();
        CGPathMoveToPoint(premiumPath, NULL, self.barBottomLine.bounds.origin.x, y);
        CGPathAddLineToPoint(premiumPath, NULL,self.barBottomLine.bounds.origin.x + _graphViewWidth.constant, y);
        [minPremiumShapeLayer setPath:premiumPath];
        CGPathRelease(premiumPath);
        UILabel *minPremiumTarget = [[UILabel alloc]init];
        minPremiumTarget.text =[ NSString stringWithFormat:@"%@ %@",[firstMonthData objectForKey:@"PremiumQty"],LOCALIZATION(C_LOYALTY_CTNS)];      //lokalise 8 feb
        minPremiumTarget.textColor = [Helper colorFromHex:[_viewData objectForKey:@"ColorCodePremiumTier"]];
        minPremiumTarget.font = FONT_B(8);
        minPremiumTarget.frame = CGRectMake(self.barBottomLine.frame.origin.x, y - 12.0, 30.0, 10.0);
        [minPremiumTarget sizeToFit];
        [self.graphView addSubview:minPremiumTarget];
        [self.labelsAdded addObject:minPremiumTarget];
        [[self.graphView layer] addSublayer:minPremiumShapeLayer];
    } else {
        _isPremiumAvailable = false;
    }
    
    CAShapeLayer *targetShapeLayer = [CAShapeLayer layer];
    [targetShapeLayer setFillColor:[[UIColor clearColor] CGColor]];
    [targetShapeLayer setLineWidth:2.0f];
    [targetShapeLayer setLineDashPattern:
     [NSArray arrayWithObjects:[NSNumber numberWithInt:5],
      [NSNumber numberWithInt:5],nil]];
    [targetShapeLayer setStrokeColor:[[Helper colorFromHex:[_viewData objectForKey:@"ColorCodeTotal"]] CGColor]];
    y = self.barBottomLine.frame.origin.y - (_scaleFactor * [[firstMonthData objectForKey:@"TargetQty"]doubleValue]);
    CGMutablePathRef totalPath = CGPathCreateMutable();
    CGPathMoveToPoint(totalPath, NULL, self.barBottomLine.bounds.origin.x, y);
    CGPathAddLineToPoint(totalPath, NULL,self.barBottomLine.bounds.origin.x + _graphViewWidth.constant, y);
    [targetShapeLayer setPath:totalPath];
    CGPathRelease(totalPath);
    UILabel *totalTarget = [[UILabel alloc]init];
    totalTarget.text =[ NSString stringWithFormat:@"%@ %@",[firstMonthData objectForKey:@"TargetQty"],LOCALIZATION(C_LOYALTY_CTNS)];            //lokalise 8 feb
    totalTarget.textColor = [Helper colorFromHex:[_viewData objectForKey:@"ColorCodeTotal"]];
    totalTarget.font = FONT_B(8);
    totalTarget.frame = CGRectMake(self.barBottomLine.frame.origin.x, y - 12.0, 30.0, 10.0);
    [totalTarget sizeToFit];
    [self.graphView addSubview:totalTarget];
    [self.labelsAdded addObject:totalTarget];
    [[self.graphView layer] addSublayer:targetShapeLayer];
    
    int count = 1;
    int skipCount = 0;
    
    for(NSDictionary *dict in _breakdownMonthList) {
        if(_isCarousel && count == 4) {
            break;
        }
        if(_swipeCount_Bar > skipCount) {
            //do nothing
            skipCount += 1;
        } else {
            NSDictionary *monthData;
            
            for(NSDictionary *monthDataDict in monthlyOverview) {
                if([[dict objectForKey:@"MonthNumber"] isEqualToString:[monthDataDict objectForKey:@"MonthNumber"]]) {
                    monthData = monthDataDict;
                }
            }
            
            UILabel *lblMonth = [[UILabel alloc]init];
            lblMonth.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"MonthName"]];
            lblMonth.textColor = COLOUR_VERYDARKGREY;
            lblMonth.textAlignment = NSTextAlignmentCenter;
            lblMonth.font = FONT_B(12);
            [lblMonth sizeToFit];
            lblMonth.frame = CGRectMake(self.barBottomLine.frame.origin.x + (spaceBetweenAxisStartAndBarOne * 1.25) + (spaceWidth * count), self.barBottomLine.frame.origin.y + 4.0,lblMonth.frame.size.width, lblMonth.frame.size.height);
            [self.graphView addSubview:lblMonth];
            [self.labelsAdded addObject:lblMonth];
            
            if(lblMonth.frame.origin.x > SCREEN_WIDTH - barwidth) {
                _currentEndMonth_Bar = count - 1;
            }
            
            UILabel *lblOrdered = [[UILabel alloc]init];
            lblOrdered.text = [NSString stringWithFormat:@"%.1f",[[monthData objectForKey:@"TotalAcheived"] floatValue]];
            lblOrdered.textColor = COLOUR_VERYDARKGREY;
            lblOrdered.textAlignment = NSTextAlignmentCenter;
            lblOrdered.font = FONT_B(12);
            [lblOrdered sizeToFit];
            lblOrdered.frame = CGRectMake(self.barBottomLine.frame.origin.x + (spaceBetweenAxisStartAndBarOne * 1.25) + (spaceWidth * count) + 2.0, self.barBottomLine.frame.origin.y + 18.0, lblOrdered.frame.size.width, lblOrdered.frame.size.height);
            [self.graphView addSubview:lblOrdered];
            [self.labelsAdded addObject:lblOrdered];
            
            if([[monthData objectForKey:@"TotalAcheived"]intValue] != 0) {
                
                double overallHeight = _scaleFactor * [[monthData objectForKey:@"TotalAcheived"]doubleValue];
                int y = _scaleFactor * [[monthData objectForKey:@"TargetQty"]doubleValue];

                if(overallHeight > 140) {
                    overallHeight = y + ((_scaleFactor / 7) * ([[monthData objectForKey:@"TotalAcheived"]doubleValue] - [[monthData objectForKey:@"TargetQty"]doubleValue]));
                }
                
                double height = 0;
                
                if(_isPremiumAvailable) {
                    height = _scaleFactor * [[monthData objectForKey:@"TotalPremiumAcheived"]doubleValue];
                    
                    if(height > 140)
                    {
                        height =  y + ((_scaleFactor / 7) * ([[monthData objectForKey:@"TotalPremiumAcheived"]doubleValue] - [[monthData objectForKey:@"TargetQty"]doubleValue]));
                    }
                    
                    UILabel *lblPremiumChart = [[UILabel alloc]init];
                    lblPremiumChart.text = @"";
                    lblPremiumChart.backgroundColor = [Helper colorFromHex:[ _viewData objectForKey:@"ColorCodePremiumTier"]];
                    lblPremiumChart.frame = CGRectMake(self.barBottomLine.frame.origin.x +(spaceWidth * count) + spaceBetweenAxisStartAndBarOne , self.barBottomLine.frame.origin.y - height , barwidth, height);
                    [self.graphView addSubview:lblPremiumChart];
                    [self.labelsAdded addObject:lblPremiumChart];
                    
                    UILabel *lblOrdered_PremiumNumber = [[UILabel alloc]init];
                    lblOrdered_PremiumNumber.text = [NSString stringWithFormat:@"%.1f",[[monthData objectForKey:@"TotalPremiumAcheived"] floatValue]];
                    lblOrdered_PremiumNumber.textColor = COLOUR_WHITE;
                    lblOrdered_PremiumNumber.textAlignment = NSTextAlignmentCenter;
                    lblOrdered_PremiumNumber.font = FONT_B3;
                    if(height < 10) {
                        lblOrdered_PremiumNumber.font = FONT_B(8);
                        lblOrdered_PremiumNumber.frame = CGRectMake(lblPremiumChart.frame.origin.x + 10.0, self.barBottomLine.frame.origin.y - height - 0.5, 40, 10);
                    }
                    else if(height < 40) {
                        lblOrdered_PremiumNumber.frame = CGRectMake(lblPremiumChart.frame.origin.x + 10.0, self.barBottomLine.frame.origin.y - height, 40, 10);
                    }
                    else if(height > 40 && height < 50){
                        lblOrdered_PremiumNumber.frame = CGRectMake(lblPremiumChart.frame.origin.x + 10.0, self.barBottomLine.frame.origin.y - 30, 40, 10);
                    }
                    else {
                        lblOrdered_PremiumNumber.frame = CGRectMake(lblPremiumChart.frame.origin.x + 10.0, self.barBottomLine.frame.origin.y - 60, 40, 10);
                    }
                    [lblOrdered_PremiumNumber sizeToFit];
                    [self.graphView addSubview:lblOrdered_PremiumNumber];
                    [self.labelsAdded addObject:lblOrdered_PremiumNumber];
                }
                
                if(([[monthData objectForKey:@"TotalOthersAcheived"] floatValue] > 0.0 && _isPremiumAvailable) || ([[monthData objectForKey:@"TotalAcheived"] floatValue] > 0.0 && !_isPremiumAvailable))
                {
                    height = overallHeight - height;
                    
                    UILabel *lblOtherChart = [[UILabel alloc]init];
                    lblOtherChart.text = @"";
                    lblOtherChart.backgroundColor = [Helper colorFromHex: [_viewData objectForKey:@"ColorCodeOther"]];
                    lblOtherChart.frame = CGRectMake(self.barBottomLine.frame.origin.x + spaceBetweenAxisStartAndBarOne + (spaceWidth * count), self.barBottomLine.frame.origin.y - overallHeight, barwidth, height);
                    [self.graphView addSubview:lblOtherChart];
                    [self.labelsAdded addObject:lblOtherChart];
                    
                    UILabel *lblOrdered_OtherNumber = [[UILabel alloc]init];
                    if(_isPremiumAvailable) {
                        lblOrdered_OtherNumber.text = [NSString stringWithFormat:@"%.1f",[[monthData objectForKey:@"TotalOthersAcheived"] floatValue]];
                    } else{
                        lblOrdered_OtherNumber.text = [NSString stringWithFormat:@"%.1f",[[monthData objectForKey:@"TotalAcheived"] floatValue]];
                    }
                    lblOrdered_OtherNumber.textColor = COLOUR_WHITE;
                    lblOrdered_OtherNumber.textAlignment = NSTextAlignmentCenter;
                    lblOrdered_OtherNumber.font = FONT_B3;
                    if(height < 10) {
                        lblOrdered_OtherNumber.textColor = COLOUR_VERYDARKGREY;
                        lblOrdered_OtherNumber.frame = CGRectMake(lblOtherChart.frame.origin.x + 10.0, self.barBottomLine.frame.origin.y - overallHeight - 12, 40, 10);
                    }
                    else if(height < 40) {
                        lblOrdered_OtherNumber.frame = CGRectMake(lblOtherChart.frame.origin.x + 10.0, self.barBottomLine.frame.origin.y - overallHeight + 2, 40, 10);
                    }
                    else {
                        lblOrdered_OtherNumber.frame = CGRectMake(lblOtherChart.frame.origin.x + 10.0, self.barBottomLine.frame.origin.y - (overallHeight - 12), 40, 10);
                    }
                    [lblOrdered_OtherNumber sizeToFit];
                    
                    [self.graphView addSubview:lblOrdered_OtherNumber];
                    [self.labelsAdded addObject:lblOrdered_OtherNumber];
                }
            }
            
            count += 1;
        }
    }

}

-(void) setupInterface {
    NSArray *monthlyOverview = [_viewData objectForKey:@"MonthlyOverviewList"];
    NSDictionary *firstMonthData = [monthlyOverview objectAtIndex:0];
    
    if([[firstMonthData objectForKey:@"PremiumQty"]doubleValue] != 0.0) {
        _isPremiumAvailable = true;
    } else {
        _isPremiumAvailable = false;
    }
    
    if(_isCarousel) {
        _legendsFooter.hidden = YES;
        _legendFooterHeight.constant = 0.0;
        _legendsSideView.hidden = NO;
        
        [NSLayoutConstraint constraintWithItem:self.recommendedView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.graphView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0].active = YES;
        
        _graphViewHeight.constant = (SCREEN_HEIGHT * 0.45) - 165 ;
        
        if(SCREEN_WIDTH <= 320) {
            _graphViewWidth.constant = SCREEN_WIDTH - 105;
            _legendsSideViewWidth.constant = 75.0;
            if([GET_LOCALIZATION isEqualToString:kIndo]) {
                _legendsSideViewHeight.constant = _isPremiumAvailable ? 130.0 : 60.0;
            } else {
                _legendsSideViewHeight.constant = _isPremiumAvailable ? 100.0 : 60.0;
            }
        }
        else {
            _graphViewWidth.constant = SCREEN_WIDTH - 130.0;
            _legendsSideViewWidth.constant = 90.0;
            _legendsSideViewHeight.constant = _isPremiumAvailable ? 100.0 : 60.0;
        }
       
        
        _lblLegendsSideView_Title.text = LOCALIZATION(C_LOYALTY_LEGEND);            //lokalise 7 Feb
        _lblLegendsSideView_Title.textColor = COLOUR_VERYDARKGREY;
        _lblLegendsSideView_Title.font = FONT_H2;
        
        
        if(_isPremiumAvailable) {
            UIColor *color = [Helper colorFromHex:[_viewData objectForKey:@"ColorCodePremiumTier"]];
            _lblLegendSideView_ColorCode_One.backgroundColor =  color ;
            color = [Helper colorFromHex:[_viewData objectForKey:@"ColorCodeOther"]];
            _lblLegendsSideView_ColorCode_Two.backgroundColor = color;
            
            _lblLegendSideView_Legend_One.text = [NSString stringWithFormat:@"%@",[_viewData objectForKey:@"TextPremiumTier"]];
            _lblLegendSideView_Legend_One.textColor = COLOUR_VERYDARKGREY;
            _lblLegendSideView_Legend_One.font = FONT_B2;
            _lblLegendSideView_Legend_Two.text = [NSString stringWithFormat:@"%@",[_viewData objectForKey:@"TextOthers"]];
            _lblLegendSideView_Legend_Two.textColor = COLOUR_VERYDARKGREY;
            _lblLegendSideView_Legend_Two.font = FONT_B2;
        } else {
            UIColor *color = [Helper colorFromHex:[_viewData objectForKey:@"ColorCodeOther"]];
            _lblLegendSideView_ColorCode_One.backgroundColor =  color ;
            _lblLegendSideView_Legend_One.text = [NSString stringWithFormat:@"%@",[_viewData objectForKey:@"TextOthers"]];
            _lblLegendSideView_Legend_One.textColor = COLOUR_VERYDARKGREY;
            _lblLegendSideView_Legend_One.font = FONT_B2;
            
            _lblLegendSideView_Legend_Two.hidden = YES;
            _lblLegendsSideView_ColorCode_Two.hidden = YES;
        }
        
        
    } else {
        
        _graphViewWidth.constant = SCREEN_WIDTH - 20.0;
        
        _legendsFooter.hidden = NO;
        _legendFooterHeight.constant = 15.0;
        
        _legendsSideView.hidden = YES;
        _legendsSideViewWidth.constant = 0.0;
        
        _lblLegendsFooter_Title.text =LOCALIZATION(C_LOYALTY_LEGEND);               //lokalise 7 feb
        _lblLegendsFooter_Title.textColor = COLOUR_VERYDARKGREY;
        _lblLegendsFooter_Title.font = FONT_H2;
        
        if(_isPremiumAvailable) {
            UIColor *color = [Helper colorFromHex:[_viewData objectForKey:@"ColorCodePremiumTier"]];
            _lblLegendFooter_ColorCode_One.backgroundColor =  color ;
            color = [Helper colorFromHex:[_viewData objectForKey:@"ColorCodeOther"]];
            _lblLegendsFooter_ColorCode_Two.backgroundColor = color;
            
            _lblLegendFooter_Legend_One.text = [NSString stringWithFormat:@"%@",[_viewData objectForKey:@"TextPremiumTier"]];
            _lblLegendFooter_Legend_Two.text = [NSString stringWithFormat:@"%@",[_viewData objectForKey:@"TextOthers"]];
            _lblLegendFooter_Legend_One.textColor = COLOUR_VERYDARKGREY;
            _lblLegendFooter_Legend_Two.textColor = COLOUR_VERYDARKGREY;
            _lblLegendFooter_Legend_One.font = FONT_B2;
            _lblLegendFooter_Legend_Two.font = FONT_B2;
        } else {
            UIColor *color = [Helper colorFromHex:[_viewData objectForKey:@"ColorCodeOther"]];
            _lblLegendFooter_ColorCode_One.backgroundColor =  color ;
            
            _lblLegendFooter_Legend_One.text = [NSString stringWithFormat:@"%@",[_viewData objectForKey:@"TextOthers"]];
            _lblLegendFooter_Legend_One.textColor = COLOUR_VERYDARKGREY;
            _lblLegendFooter_Legend_One.font = FONT_B2;
            
            _lblLegendFooter_Legend_Two.hidden = YES;
            _lblLegendsFooter_ColorCode_Two.hidden = YES;
        }
    }
    
    _lblRecommendedView_Title.text = LOCALIZATION(C_LOYALTY_RECOMMENDED);               //loaklise 8 feb
    _lblRecommendedView_Title.textColor = COLOUR_VERYDARKGREY;
    if(SCREEN_WIDTH <= 320) {
         _lblRecommendedView_Title.font = FONT_H3;
    } else {
         _lblRecommendedView_Title.font = FONT_H2;
    }
    
    _lbRecommendedView_Legend_One.text = [NSString stringWithFormat:@"%@",[_viewData objectForKey:@"TextMinimum"]];
    _lbRecommendedView_Legend_One.textColor = COLOUR_VERYDARKGREY;
    if(SCREEN_WIDTH <= 320) {
         _lbRecommendedView_Legend_One.font = FONT_B3;
    } else {
         _lbRecommendedView_Legend_One.font = FONT_B2;
    }
    
    [self createDashedLine:[Helper colorFromHex:[_viewData objectForKey:@"ColorCodeMinimum"]] for:_lbRecommendedView_ColorCode_One];
    
    if(_isPremiumAvailable) {
        _lbRecommendedView_Legend_Two.text = [NSString stringWithFormat:@"%@",[_viewData objectForKey:@"TextPremium"]];
        _lbRecommendedView_Legend_Two.textColor = COLOUR_VERYDARKGREY;
        _lbRecommendedView_Legend_Two.font = _lbRecommendedView_Legend_One.font;
        
        _lbRecommendedView_Legend_Three.text = [NSString stringWithFormat:@"%@",[_viewData objectForKey:@"TextTotal"]];
        _lbRecommendedView_Legend_Three.textColor = COLOUR_VERYDARKGREY;
        _lbRecommendedView_Legend_Three.font = _lbRecommendedView_Legend_One.font;
        
        [self createDashedLine:[Helper colorFromHex:[_viewData objectForKey:@"ColorCodePremiumTier"]] for:_lblRecommendedView_ColorCode_Two];
        [self createDashedLine:[Helper colorFromHex:[_viewData objectForKey:@"ColorCodeTotal"]] for:_lblRecommendedView_ColorCode_Three];
    } else {
        _lbRecommendedView_Legend_Two.text = [NSString stringWithFormat:@"%@",[_viewData objectForKey:@"TextTotal"]];
        _lbRecommendedView_Legend_Two.textColor = COLOUR_VERYDARKGREY;
        _lbRecommendedView_Legend_Two.font = _lbRecommendedView_Legend_One.font;
        
        [self createDashedLine:[Helper colorFromHex:[_viewData objectForKey:@"ColorCodeTotal"]] for:_lblRecommendedView_ColorCode_Two];
        
        _lbRecommendedView_Legend_Three.hidden = YES;
        _lblRecommendedView_ColorCode_Three.hidden = YES;
    }
    
}

-(void) createDashedLine:(UIColor*) color for:(UIView*)colorView{
    if ([[[colorView layer] sublayers] objectAtIndex:0])
    {
        colorView.layer.sublayers = nil;
    }
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    [shapeLayer setFillColor:[[UIColor clearColor] CGColor]];
    [shapeLayer setStrokeColor:[color CGColor]];
    [shapeLayer setLineWidth:3.0f];
    [shapeLayer setLineDashPattern:
    [NSArray arrayWithObjects:[NSNumber numberWithInt:2],
      [NSNumber numberWithInt:2],nil]];
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, colorView.bounds.origin.x, colorView.bounds.origin.y + 5.0);
    CGPathAddLineToPoint(path, NULL,colorView.bounds.origin.x + 10.0, colorView.bounds.origin.y + 5.0);
    
    [shapeLayer setPath:path];
    CGPathRelease(path);
    
    [[colorView layer] addSublayer:shapeLayer];
}

#pragma mark - Swipe Gesture

- (IBAction)leftSwipe:(id)sender {
    if(SCREEN_WIDTH <= 320) {
        if(_swipeCount_Bar < _breakdownMonthList.count - 4 && _currentEndMonth_Bar != -1) {
            _currentEndMonth_Bar += 1;
            _swipeCount_Bar += 1;
            [self setupGraph];
        }
    } else {
        if(_swipeCount_Bar < _breakdownMonthList.count - 5 && _currentEndMonth_Bar != -1) {
            _currentEndMonth_Bar += 1;
            _swipeCount_Bar += 1;
            [self setupGraph];
        }
    }
}

- (IBAction)rightSwipe:(id)sender {
    if(_swipeCount_Bar != 0) {
        _swipeCount_Bar -= 1;
        [self setupGraph];
    }
}

@end
