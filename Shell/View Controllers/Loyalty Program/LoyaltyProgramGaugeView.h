//
//  LoyaltyProgramGaugeView.h
//  Shell
//
//  Created by Nach on 13/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoyaltyProgramGaugeView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *currentArrowImage;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *gaugeView;


@property (weak, nonatomic) IBOutlet UIView *legendsView;
@property (weak, nonatomic) IBOutlet UIView *totalCartonLegendView;
@property (weak, nonatomic) IBOutlet UIView *premiumTierLegendView;

@property (weak, nonatomic) IBOutlet UILabel *lblTotalCartonColor;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalCartonTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalCartonData;

@property (weak, nonatomic) IBOutlet UILabel *lblPremiumTierColor;
@property (weak, nonatomic) IBOutlet UILabel *lblPremiumTierTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPremiumTierData;

@property BOOL isPremiumAvailable;
@property BOOL isAgri;

@property NSDictionary *viewData;
@property  UIBezierPath *outerGaugePath;
@property  UIBezierPath *innerGaugePath;

@property double radius;
@property CGPoint arcCenter;

-(void) loadBar:(NSDictionary *) responseData isCarousel:(BOOL) isCarousel;

@end

NS_ASSUME_NONNULL_END
