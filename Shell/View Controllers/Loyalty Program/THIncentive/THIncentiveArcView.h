//
//  THIncentiveArcView.h
//  Shell
//
//  Created by Nach on 24/2/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "THLoyaltyTierPointsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface THIncentiveArcView : UIView

@property (strong, nonatomic) IBOutlet UIView *contentView;

@property NSMutableDictionary *dataSourceForView;
@property  UIBezierPath *gaugePath;
@property double radius;
@property CGPoint arcCenter;

@property BOOL isTierUnlocked;

-(void) loadBar: (THLoyaltyTierPointsModel *) responseData isBreakdown:(BOOL)isBreakdown;

@end

NS_ASSUME_NONNULL_END
