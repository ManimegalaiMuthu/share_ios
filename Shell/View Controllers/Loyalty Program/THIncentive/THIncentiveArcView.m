//
//  THIncentiveArcView.m
//  Shell
//
//  Created by Nach on 24/2/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "THIncentiveArcView.h"
#import "Constants.h"
#import "Helper.h"
#import "Headers.h"
#define degreesToRadians(degrees)   ((M_PI * degrees)/180)
#define BAR_WIDTH                   16.0
#define ANGLE_INTERVAL              18
#define MILLION_CONSTANT            1000000

//Colors
#define COLOUR_TIER0                 @"#DD1D21"
#define COLOUR_TIER1                 @"#D45C2C"
#define COLOUR_TIER2                 @"#EB8705"
#define COLOUR_TIER3                 @"#FBCE07"
#define COLOUR_TIER4                 @"#BED50F"
#define COLOUR_TIER5                 @"#008443"
#define COLOUR_PREMIUM               @"#003C88"
#define COLOUR_OTHER                 @"#7F7F7F"
#define COLOUR_DEFAULT               @"#D9D9D9"

typedef enum {
    MARKING_FOR_START = 0,
    MARKING_FOR_1,
    MARKING_FOR_2,
    MARKING_FOR_3,
    MARKING_FOR_4,
    MARKING_FOR_5,
    MARKING_FOR_END,
}MARKING_FOR_TAG;


@implementation THIncentiveArcView

-(void)awakeFromNib
{
    [super awakeFromNib];
}

-(id) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self xibSetup];
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self xibSetup];
    }
    return self;
}

-(void) xibSetup {
    // 1. load the interface
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    // 2. add as subview
    [self addSubview:self.contentView];
    // 3. allow for autolayout
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    // 4. add constraints to span entire view
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[view]-|" options:0 metrics:nil views:@{@"view":self.contentView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[view]-|" options:0 metrics:nil views:@{@"view":self.contentView}]];
    
    [self setNeedsLayout];
}


-(void) loadBar: (THLoyaltyTierPointsModel *) responseData isBreakdown:(BOOL)isBreakdown
{
    
    NSMutableArray *layerToRemoveArray = [[NSMutableArray alloc] init]; //to hold references to actual progress ba
    for (UILabel *label in self.contentView.subviews){
        [label removeFromSuperview];
    }
       
    for (CALayer *layer in [self.contentView.layer sublayers])
    {
        if ([layer isKindOfClass: [CAShapeLayer class]])
        {
            [layerToRemoveArray addObject: layer];
        }
    }

    //remove referenced layers from superlayer
    for (CALayer *layer in layerToRemoveArray)
    {
        [layer removeFromSuperlayer];
    }
    
    self.dataSourceForView = [NSMutableDictionary new];
    
    self.gaugePath = [UIBezierPath bezierPathWithArcCenter:self.arcCenter radius:self.radius startAngle:degreesToRadians(180) endAngle:degreesToRadians(0) clockwise:YES];
    
    for(int i = 1;i < 6; i++) {
        NSString *targetValue;
        switch(i) {
            case 1:
                targetValue = [responseData target1];
                break;
            case 2:
                targetValue = [responseData target2];
                break;
            case 3:
                targetValue = [responseData target3];
                break;
            case 4:
                targetValue = [responseData target4];
                break;
            case 5:
                targetValue = [responseData target5];
                break;
        }
    
        [self.dataSourceForView setValue:[self convertToMillionBhat:targetValue] forKey:[NSString stringWithFormat:@"Step%d",i]];
    }
    [self.dataSourceForView setValue:[self convertToMillionBhat:[responseData totalOrderValueBaht]] forKey:@"TargetAchieved"];
    
    if(isBreakdown) {
        [self setupDataForBreakdown: responseData];
        [self setupArcForBreakdown: YES];
        [self setBreakdownLegend];
    } else {
        [self setupDataForIncentive: responseData];
        [self setupArcForBreakdown: NO];
        [self setIncentiveLegend];
    }
    
    [self createMarkingValueFor:MARKING_FOR_START atAngle:180];
    [self createMarkingValueFor:MARKING_FOR_END atAngle:0];
    
}

-(void) setupDataForBreakdown:(THLoyaltyTierPointsModel *) responseData {
    
    [self.dataSourceForView setValue:[self convertToMillionBhat:[responseData premiumValueBaht]] forKey:@"PremiumAchieved"];
    [self.dataSourceForView setValue:[self convertToMillionBhat:[responseData otherLubricantsValueBaht]] forKey:@"OtherAchieved"];
    self.isTierUnlocked = NO;
    
}

-(void) setupDataForIncentive:(THLoyaltyTierPointsModel *) responseData {
    
    [self.dataSourceForView setValue:@"0" forKey:@"PremiumAchieved"];
    [self.dataSourceForView setValue:@"0" forKey:@"OtherAchieved"];
    if([responseData tier] > 0) {
        self.isTierUnlocked = YES;
    } else {
        self.isTierUnlocked = NO;
    }
    [self.dataSourceForView setValue:@([responseData tier]) forKey:@"TierUnlocked"];
    [self.dataSourceForView setValue:[responseData tierPoints] forKey:@"TierPoints"];
    
}


-(void) setupArcForBreakdown:(BOOL)isBreakdown {
    
    int count = 0;
    int angleStart = 90;
    UIBezierPath *bezierPath;
    CAShapeLayer *progressLayer;
    NSString *colorHexString;
    
    if(isBreakdown) {
        CAShapeLayer *outerProgressLayer = [[CAShapeLayer alloc] init];
        [outerProgressLayer setPath:self.gaugePath.CGPath];
        [outerProgressLayer setStrokeColor: [[Helper colorFromHex:COLOUR_DEFAULT] CGColor]];
        [outerProgressLayer setFillColor:[UIColor clearColor].CGColor];
        [outerProgressLayer setLineWidth:BAR_WIDTH];
        [self.contentView.layer addSublayer:outerProgressLayer];
    }
    
    //To compare the achieved target with the targets set
    double achievedTarget = [[self.dataSourceForView objectForKey:@"TargetAchieved"] doubleValue];
    double premiumAchieved = [[self.dataSourceForView objectForKey:@"PremiumAchieved"] doubleValue];
    
    BOOL regionAchieved = NO;
    BOOL inBetweenRegion = NO;
    
    double startValue = 0;
    double endValue = 0;
    int startAngle = 0;
    
    while(count < 6) {
        progressLayer = [[CAShapeLayer alloc] init];
        switch(count) {
            case 0:
            {
                bezierPath = [UIBezierPath bezierPathWithArcCenter:self.arcCenter radius:self.radius startAngle:degreesToRadians(180) endAngle:degreesToRadians(-angleStart) clockwise:YES];
                colorHexString = COLOUR_TIER0;
                [self createMarkingValueFor:MARKING_FOR_1 atAngle:angleStart];
                if(achievedTarget > [[self.dataSourceForView objectForKey:@"Step1"] doubleValue]) {
                    regionAchieved = YES;
                } else {
                    if(achievedTarget > 0) {
                        regionAchieved = NO;
                        inBetweenRegion = YES;
                    } else {
                        regionAchieved = NO;
                        inBetweenRegion = NO;
                    }
                }
                startValue = 0;
                endValue = [[self.dataSourceForView objectForKey:@"Step1"] doubleValue];
                startAngle = 180;
            }
                break;
            case 1: {
                bezierPath = [UIBezierPath bezierPathWithArcCenter:self.arcCenter radius:self.radius startAngle:degreesToRadians(-angleStart) endAngle:degreesToRadians(-(angleStart - ANGLE_INTERVAL)) clockwise:YES];
                colorHexString = COLOUR_TIER1;
                if(achievedTarget > [[self.dataSourceForView objectForKey:@"Step2"] doubleValue]) {
                    regionAchieved = YES;
                } else {
                    if(achievedTarget > [[self.dataSourceForView objectForKey:@"Step1"] doubleValue]) {
                        regionAchieved = NO;
                        inBetweenRegion = YES;
                    } else {
                        regionAchieved = NO;
                        inBetweenRegion = NO;
                    }
                }
                startValue = [[self.dataSourceForView objectForKey:@"Step1"] doubleValue];
                endValue = [[self.dataSourceForView objectForKey:@"Step2"] doubleValue];
                startAngle = angleStart;
                angleStart -= ANGLE_INTERVAL;
                [self createMarkingValueFor:MARKING_FOR_2 atAngle:angleStart];
            }
                break;
            case 2: {
                bezierPath = [UIBezierPath bezierPathWithArcCenter:self.arcCenter radius:self.radius startAngle:degreesToRadians(-angleStart) endAngle:degreesToRadians(-(angleStart - ANGLE_INTERVAL)) clockwise:YES];
                colorHexString = COLOUR_TIER2;
                if(achievedTarget > [[self.dataSourceForView objectForKey:@"Step3"] doubleValue]) {
                    regionAchieved = YES;
                } else {
                    if(achievedTarget > [[self.dataSourceForView objectForKey:@"Step2"] doubleValue]) {
                        regionAchieved = NO;
                        inBetweenRegion = YES;
                    } else {
                        regionAchieved = NO;
                        inBetweenRegion = NO;
                    }
                }
                startValue = [[self.dataSourceForView objectForKey:@"Step2"] doubleValue];
                endValue = [[self.dataSourceForView objectForKey:@"Step3"] doubleValue];
                startAngle = angleStart;
                angleStart -= ANGLE_INTERVAL;
                [self createMarkingValueFor:MARKING_FOR_3 atAngle:angleStart];
            }
                break;
            case 3: {
                bezierPath = [UIBezierPath bezierPathWithArcCenter:self.arcCenter radius:self.radius startAngle:degreesToRadians(-angleStart) endAngle:degreesToRadians(-(angleStart - ANGLE_INTERVAL)) clockwise:YES];
                colorHexString = COLOUR_TIER3;
                if(achievedTarget > [[self.dataSourceForView objectForKey:@"Step4"] doubleValue]) {
                    regionAchieved = YES;
                } else {
                    if(achievedTarget > [[self.dataSourceForView objectForKey:@"Step3"] doubleValue]) {
                        regionAchieved = NO;
                        inBetweenRegion = YES;
                    } else {
                        regionAchieved = NO;
                        inBetweenRegion = NO;
                    }
                }
                startValue = [[self.dataSourceForView objectForKey:@"Step3"] doubleValue];
                endValue = [[self.dataSourceForView objectForKey:@"Step4"] doubleValue];
                startAngle = angleStart;
                angleStart -= ANGLE_INTERVAL;
                [self createMarkingValueFor:MARKING_FOR_4 atAngle:angleStart];
            }
                break;
            case 4: {
                bezierPath = [UIBezierPath bezierPathWithArcCenter:self.arcCenter radius:self.radius startAngle:degreesToRadians(-angleStart) endAngle:degreesToRadians(-(angleStart - ANGLE_INTERVAL)) clockwise:YES];
                colorHexString = COLOUR_TIER4;
                if(achievedTarget > [[self.dataSourceForView objectForKey:@"Step5"] doubleValue]) {
                    regionAchieved = YES;
                } else {
                    if(achievedTarget > [[self.dataSourceForView objectForKey:@"Step4"] doubleValue]) {
                        regionAchieved = NO;
                        inBetweenRegion = YES;
                    } else {
                        regionAchieved = NO;
                        inBetweenRegion = NO;
                    }
                }
                startValue = [[self.dataSourceForView objectForKey:@"Step4"] doubleValue];
                endValue = [[self.dataSourceForView objectForKey:@"Step5"] doubleValue];
                startAngle = angleStart;
                angleStart -= ANGLE_INTERVAL;
                [self createMarkingValueFor:MARKING_FOR_5 atAngle:angleStart];
            }
                break;
            case 5: {
                bezierPath = [UIBezierPath bezierPathWithArcCenter:self.arcCenter radius:self.radius startAngle:degreesToRadians(-angleStart) endAngle:degreesToRadians(-(angleStart - ANGLE_INTERVAL)) clockwise:YES];
                colorHexString = COLOUR_TIER5;
                if(achievedTarget > [[self.dataSourceForView objectForKey:@"Step5"] doubleValue]) {
                    regionAchieved = YES;
                } else {
                    if(achievedTarget > [[self.dataSourceForView objectForKey:@"Step5"] doubleValue]) {
                        regionAchieved = NO;
                        inBetweenRegion = YES;
                    } else {
                        regionAchieved = NO;
                        inBetweenRegion = NO;
                    }
                }
                startValue = [[self.dataSourceForView objectForKey:@"Step5"] doubleValue];
                endValue = [[self.dataSourceForView objectForKey:@"Step5"] doubleValue];
                startAngle = angleStart;
            }
                break;
        }
        [progressLayer setPath:bezierPath.CGPath];
        
        if(regionAchieved) {
            if(isBreakdown) {
                if(endValue <= premiumAchieved) {
                    [progressLayer setStrokeColor:[[Helper colorFromHex:COLOUR_PREMIUM withAlpha:1.0] CGColor]];
                } else if(endValue > premiumAchieved && startValue < premiumAchieved) {
                    int angleAchieved = ((startAngle - angleStart) / (endValue - startValue)) * (premiumAchieved - startValue);
                    if(premiumAchieved == achievedTarget) {
                        [self createCurrentMarkingAt:((startAngle - angleAchieved))];
                    }
                    UIBezierPath *bezierPath1 = [UIBezierPath bezierPathWithArcCenter:self.arcCenter radius:self.radius startAngle:degreesToRadians(-startAngle) endAngle:degreesToRadians(-(startAngle - angleAchieved)) clockwise:YES];
                    CAShapeLayer *progressLayer1 = [[CAShapeLayer alloc] init];
                    [progressLayer1 setPath:bezierPath1.CGPath];
                    [progressLayer1 setStrokeColor:[[Helper colorFromHex:COLOUR_PREMIUM withAlpha:1.0] CGColor]];
                    [progressLayer1 setFillColor:[UIColor clearColor].CGColor];
                    [progressLayer1 setLineWidth:BAR_WIDTH];
                    [self.contentView.layer addSublayer:progressLayer1];
                    
                    UIBezierPath *bezierPath2 = [UIBezierPath bezierPathWithArcCenter:self.arcCenter radius:self.radius startAngle:degreesToRadians(-(startAngle - angleAchieved)) endAngle:degreesToRadians(-angleStart) clockwise:YES];
                    CAShapeLayer *progressLayer2 = [[CAShapeLayer alloc] init];
                    [progressLayer2 setPath:bezierPath2.CGPath];
                    [progressLayer2 setStrokeColor:[[Helper colorFromHex:COLOUR_OTHER withAlpha:0.8] CGColor]];
                    [progressLayer2 setFillColor:[UIColor clearColor].CGColor];
                    [progressLayer2 setLineWidth:BAR_WIDTH];
                    [self.contentView.layer addSublayer:progressLayer2];
                }
                else {
                     [progressLayer setStrokeColor:[[Helper colorFromHex:COLOUR_OTHER withAlpha:0.8] CGColor]];
                }
            } else {
                 [progressLayer setStrokeColor:[[Helper colorFromHex:colorHexString withAlpha:1.0] CGColor]];
            }
        } else if(!isBreakdown){
            [progressLayer setStrokeColor:[[Helper colorFromHex:colorHexString withAlpha:0.6] CGColor]];
        }
        [progressLayer setFillColor:[UIColor clearColor].CGColor];
        [progressLayer setLineWidth:BAR_WIDTH];
        [self.contentView.layer addSublayer:progressLayer];
        
        if(inBetweenRegion) {
            if(isBreakdown) {
                if(endValue > achievedTarget && startValue < premiumAchieved) {
                    int angleAchieved = ((startAngle - angleStart) / (endValue - startValue)) * (premiumAchieved - startValue);
                    if(angleAchieved == 0) {
                        angleAchieved = 1;
                    }
                    UIBezierPath *bezierPath1 = [UIBezierPath bezierPathWithArcCenter:self.arcCenter radius:self.radius startAngle:degreesToRadians(-startAngle) endAngle:degreesToRadians(-(startAngle - angleAchieved)) clockwise:YES];
                    CAShapeLayer *progressLayer1 = [[CAShapeLayer alloc] init];
                    [progressLayer1 setPath:bezierPath1.CGPath];
                    [progressLayer1 setStrokeColor:[[Helper colorFromHex:COLOUR_PREMIUM withAlpha:1.0] CGColor]];
                    [progressLayer1 setFillColor:[UIColor clearColor].CGColor];
                    [progressLayer1 setLineWidth:BAR_WIDTH];
                    [self.contentView.layer addSublayer:progressLayer1];
                    
                    int angleAchieved2 = ((startAngle - angleStart) / (endValue - startValue)) * (achievedTarget - startValue);
                    [self createCurrentMarkingAt:((startAngle - angleAchieved2))];
                    UIBezierPath *bezierPath2 = [UIBezierPath bezierPathWithArcCenter:self.arcCenter radius:self.radius startAngle:degreesToRadians(-(startAngle - angleAchieved)) endAngle:degreesToRadians(-(startAngle - angleAchieved2)) clockwise:YES];
                    CAShapeLayer *progressLayer2 = [[CAShapeLayer alloc] init];
                    [progressLayer2 setPath:bezierPath2.CGPath];
                    [progressLayer2 setStrokeColor:[[Helper colorFromHex:COLOUR_OTHER withAlpha:0.8] CGColor]];
                    [progressLayer2 setFillColor:[UIColor clearColor].CGColor];
                    [progressLayer2 setLineWidth:BAR_WIDTH];
                    [self.contentView.layer addSublayer:progressLayer2];
                }
                else {
                     int angleAchieved = ((startAngle - angleStart) / (endValue - startValue)) * (achievedTarget - startValue);
                     [self createCurrentMarkingAt:((startAngle - angleAchieved))];
                     UIBezierPath *bezierPath1 = [UIBezierPath bezierPathWithArcCenter:self.arcCenter radius:self.radius startAngle:degreesToRadians(-startAngle) endAngle:degreesToRadians(-(startAngle - angleAchieved)) clockwise:YES];
                     CAShapeLayer *progressLayer1 = [[CAShapeLayer alloc] init];
                     [progressLayer1 setPath:bezierPath1.CGPath];
                     [progressLayer1 setStrokeColor:[[Helper colorFromHex:COLOUR_OTHER withAlpha:0.8] CGColor]];
                     [progressLayer1 setFillColor:[UIColor clearColor].CGColor];
                     [progressLayer1 setLineWidth:BAR_WIDTH];
                     [self.contentView.layer addSublayer:progressLayer1];
                }
            } else {
                int angleAchieved = ((startAngle - angleStart) / (endValue - startValue)) * (achievedTarget - startValue);
                [self createCurrentMarkingAt:((startAngle - angleAchieved))];
                UIBezierPath *bezierPath1 = [UIBezierPath bezierPathWithArcCenter:self.arcCenter radius:self.radius startAngle:degreesToRadians(-startAngle) endAngle:degreesToRadians(-(startAngle - angleAchieved)) clockwise:YES];
                CAShapeLayer *progressLayer1 = [[CAShapeLayer alloc] init];
                [progressLayer1 setPath:bezierPath1.CGPath];
                [progressLayer1 setStrokeColor:[[Helper colorFromHex:colorHexString withAlpha:1.0] CGColor]];
                [progressLayer1 setFillColor:[UIColor clearColor].CGColor];
                [progressLayer1 setLineWidth:BAR_WIDTH];
                [self.contentView.layer addSublayer:progressLayer1];
            }
            inBetweenRegion = NO;
        }
               
        
        count += 1;
        bezierPath = nil;
        progressLayer = nil;
    }
    
}

-(void) setBreakdownLegend {
    
    UIView *premium_ColorCode = [UIView  new];
    premium_ColorCode.backgroundColor = [Helper colorFromHex:COLOUR_PREMIUM withAlpha:1.0];
    
    UILabel *premium_Title = [UILabel new];
    premium_Title.text = LOCALIZATION(C_HOME_THINCENTIVE_PREMIUMTIER);
    premium_Title.font = FONT_B2;
    premium_Title.textColor = COLOUR_VERYDARKGREY;
    [premium_Title sizeToFit];
    [premium_Title setCenter:CGPointMake(self.arcCenter.x + 5 , self.arcCenter.y - (self.radius * 0.53))];
    premium_ColorCode.frame = CGRectMake(self.arcCenter.x - (premium_Title.frame.size.width / 2) - 10, self.arcCenter.y - (self.radius * 0.55), 8, 8);
    
    NSMutableAttributedString *textPremiumOrders = [[NSMutableAttributedString alloc] initWithString:[self.dataSourceForView objectForKey:@"PremiumAchieved"] attributes:@{
                                                        NSFontAttributeName : FONT_H2 ,
                                                        NSForegroundColorAttributeName : COLOUR_SHELLRED,
                                                    }];
    [textPremiumOrders appendAttributedString: [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" / %@",[self.dataSourceForView objectForKey:@"TargetAchieved"]] attributes:@{
                                                        NSFontAttributeName : FONT_H2,
                                                        NSForegroundColorAttributeName : COLOUR_VERYDARKGREY,
                                                    }]];
    UILabel *premium_Data = [UILabel new];
    premium_Data.attributedText = textPremiumOrders;
    [premium_Data sizeToFit];
    [premium_Data setCenter:CGPointMake(self.arcCenter.x , premium_Title.frame.origin.y + premium_Title.frame.size.height + 8 )];
    
    [self.contentView addSubview:premium_ColorCode];
    [self.contentView addSubview:premium_Title];
    [self.contentView addSubview:premium_Data];
    
    UIView *other_ColorCode = [UIView  new];
    other_ColorCode.backgroundColor = [Helper colorFromHex:COLOUR_OTHER withAlpha:0.8];
    
    UILabel *other_Title = [UILabel new];
    other_Title.text = LOCALIZATION(C_HOME_THINCENTIVE_OTHERPRODUCTS);
    other_Title.font = premium_Title.font;
    other_Title.textColor = premium_Title.textColor;
    [other_Title sizeToFit];
    other_ColorCode.frame = CGRectMake(premium_ColorCode.frame.origin.x, premium_Data.frame.origin.y + premium_Data.frame.size.height + 8, 8, 8);
    other_Title.frame = CGRectMake(premium_Title.frame.origin.x, premium_Data.frame.origin.y + premium_Data.frame.size.height, other_Title.frame.size.width, other_Title.frame.size.height );
    
    NSMutableAttributedString *textOtherOrders = [[NSMutableAttributedString alloc] initWithString:[self.dataSourceForView objectForKey:@"OtherAchieved"] attributes:@{
                                                        NSFontAttributeName : FONT_H2 ,
                                                        NSForegroundColorAttributeName : COLOUR_SHELLRED,
                                                    }];
    [textOtherOrders appendAttributedString: [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" / %@",[self.dataSourceForView objectForKey:@"TargetAchieved"]] attributes:@{
                                                        NSFontAttributeName : FONT_H2,
                                                        NSForegroundColorAttributeName : COLOUR_VERYDARKGREY,
                                                    }]];
    UILabel *other_Data = [UILabel new];
    other_Data.attributedText = textOtherOrders;
    [other_Data sizeToFit];
    [other_Data setCenter:CGPointMake(self.arcCenter.x , other_Title.frame.origin.y + other_Title.frame.size.height + 8 )];
    
    [self.contentView addSubview:other_ColorCode];
    [self.contentView addSubview:other_Title];
    [self.contentView addSubview:other_Data];
}

-(void) setIncentiveLegend {
    
    TTTAttributedLabel *legendLabel = [TTTAttributedLabel new];
    NSString *footerString ;
    legendLabel.textAlignment = NSTextAlignmentCenter;
    legendLabel.numberOfLines = 0;
    [legendLabel sizeToFit];
    
    if(self.isTierUnlocked) {
        UIImageView *confettiImage = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"carousel_partnersofshell_confeti_verydarkgrey"]];
        [confettiImage setFrame:CGRectMake((SCREEN_WIDTH / 2) - 30, self.arcCenter.y - (self.radius * 0.75), 35.0, 35.0) ];
        [self.contentView addSubview:confettiImage];
        
        UILabel *headerLabel = [UILabel new];
        headerLabel.text= [LOCALIZATION(C_HOME_THINCENTIVE_TIERUNLOCKED) stringByReplacingOccurrencesOfString:@"#Tier#" withString:[self.dataSourceForView objectForKey:@"TierUnlocked"]];
        headerLabel.font = FONT_H(14);
        headerLabel.textAlignment = NSTextAlignmentCenter;
        [headerLabel sizeToFit];
        [headerLabel setFrame: CGRectMake(SCREEN_WIDTH / 3, confettiImage.frame.origin.y + 40.0, headerLabel.frame.size.width, headerLabel.frame.size.height)];
        [self.contentView addSubview:headerLabel];
        
        if(IS_THAI_LANGUAGE) {
            NSString *csString = [LOCALIZATION(C_HOME_THINCENTIVE_EARNEDPOINTS) stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"XX %@",LOCALIZATION(C_REWARDS_POINTS_SMALL)] withString:[NSString stringWithFormat:@"<b style=\"color:#DD1D21 ; font-size:14px ; font-family: 'Kittithada Bold 75';\"><br> %@ %@ </b>",[self addCommaThousandSeparatorsWithInputString:[self.dataSourceForView objectForKey:@"TierPoints"]],LOCALIZATION(C_REWARDS_POINTS_SMALL)]];
            footerString = [NSString stringWithFormat:@"<style>body{font-family: 'Kittithada Roman 55'; font-size:13px; color: #404040;}</style>%@",csString];
        } else {
            NSString *csString = [LOCALIZATION(C_HOME_THINCENTIVE_EARNEDPOINTS) stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"XX %@",LOCALIZATION(C_REWARDS_POINTS_SMALL)] withString:[NSString stringWithFormat:@"<b style=\"color:#DD1D21 ; font-size:14px ; font-family: 'ShellFutura-Bold'; \"><br> %@ %@ </b>",[self addCommaThousandSeparatorsWithInputString:[self.dataSourceForView objectForKey:@"TierPoints"]],LOCALIZATION(C_REWARDS_POINTS_SMALL)]];
            footerString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:13px; color: #404040;}</style>%@",csString];
        }
        
        [legendLabel setFrame: CGRectMake(SCREEN_WIDTH / 4, headerLabel.frame.origin.y + headerLabel.frame.size.height - 7.0, self.radius * 1.1, 50)];
    } else {
        if(IS_THAI_LANGUAGE) {
            NSString *csString = [LOCALIZATION(C_HOME_THINCENTIVE_UNLOCKLABEL) stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"XX %@",LOCALIZATION(C_REWARDS_POINTS_SMALL)] withString:[NSString stringWithFormat:@"<b style=\"color:#DD1D21 ; font-size:14px ; font-family: 'Kittithada Bold 75';\"> %@ %@ </b>",[self addCommaThousandSeparatorsWithInputString:[self.dataSourceForView objectForKey:@"TierPoints"]],LOCALIZATION(C_REWARDS_POINTS_SMALL)]];
            footerString = [NSString stringWithFormat:@"<style>body{font-family: 'Kittithada Roman 55'; font-size:13px; color: #404040;}</style>%@",csString];
            [legendLabel setFrame: CGRectMake(SCREEN_WIDTH / 4.2, self.arcCenter.y - (self.radius * 0.5), self.radius * 1.1, 50)];
        } else {
            NSString *csString = [LOCALIZATION(C_HOME_THINCENTIVE_UNLOCKLABEL) stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"XX %@",LOCALIZATION(C_REWARDS_POINTS_SMALL)] withString:[NSString stringWithFormat:@"<b style=\"color:#DD1D21 ; font-size:14px ; font-family : 'ShellFutura-Bold';\"> %@ %@ </b>",[self addCommaThousandSeparatorsWithInputString:[self.dataSourceForView objectForKey:@"TierPoints"]],LOCALIZATION(C_REWARDS_POINTS_SMALL)]];
            footerString = [NSString stringWithFormat:@"<style>body{font-family: 'ShellFutura-Book'; font-size:13px; color: #404040;}</style>%@",csString];
            [legendLabel setFrame: CGRectMake(SCREEN_WIDTH / 4.2, self.arcCenter.y - (self.radius * 0.5), self.radius * 1.1, 50)];
        }
    }
    
    legendLabel.attributedText = [[NSAttributedString alloc] initWithData: [footerString dataUsingEncoding: NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
    [self.contentView addSubview:legendLabel];
}

-(void) createMarkingValueFor:(NSInteger)markingFor atAngle:(int)angle{
    
    UILabel *lblMarking = [UILabel new];
    lblMarking.backgroundColor = COLOUR_DARKGREY;
    
    UIView *lblMarkingExtension = [UIView new];
    lblMarkingExtension.backgroundColor = COLOUR_DARKGREY;
    [lblMarking addSubview:lblMarkingExtension];
    lblMarkingExtension.frame = CGRectMake(0, BAR_WIDTH, 1, BAR_WIDTH/2);
    
    int x = self.arcCenter.x + (self.radius) * cos(degreesToRadians(angle));
    int y = self.arcCenter.y - (self.radius) * sin(degreesToRadians(angle));
    lblMarking.frame = CGRectMake(x, y, 1, BAR_WIDTH);
    [lblMarking setCenter:CGPointMake(x, y)];
    lblMarking.transform =  CGAffineTransformRotate(CGAffineTransformIdentity, -degreesToRadians(angle)+M_PI/2);
    
    UILabel *lblMarkingValue = [UILabel new];
    lblMarkingValue.backgroundColor = [UIColor clearColor];
    lblMarkingValue.font = FONT_B3;
    lblMarkingValue.textColor = COLOUR_VERYDARKGREY;
    lblMarkingValue.numberOfLines = 0;
    
    switch(markingFor) {
        case MARKING_FOR_START : {
            lblMarkingValue.text = @"0";
        }
            break;
        case MARKING_FOR_1 : {
            lblMarkingValue.text = [self.dataSourceForView objectForKey:@"Step1"];
        }
            break;
        case MARKING_FOR_2 : {
            lblMarkingValue.text = [self.dataSourceForView objectForKey:@"Step2"];
        }
            break;
        case MARKING_FOR_3 : {
            lblMarkingValue.text = [self.dataSourceForView objectForKey:@"Step3"];
        }
            break;
        case MARKING_FOR_4 : {
            lblMarkingValue.text = [self.dataSourceForView objectForKey:@"Step4"];
        }
            break;
        case MARKING_FOR_5 : {
            lblMarkingValue.text = [self.dataSourceForView objectForKey:@"Step5"];
        }
            break;
        default : {
            lblMarkingValue.text = @"";
        }
            break;
    }
    
    [lblMarkingValue sizeToFit];
    x = self.arcCenter.x + (self.radius - (BAR_WIDTH * 2) ) * cos(degreesToRadians(angle));
    y = self.arcCenter.y - (self.radius - (BAR_WIDTH * 2) ) * sin(degreesToRadians(angle));
    lblMarkingValue.frame = CGRectMake(x, y, lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
    [lblMarkingValue setCenter:CGPointMake(x, y)];
    
    [self.contentView addSubview:lblMarking];
    [self.contentView addSubview:lblMarkingValue];
    
}

-(void) createCurrentMarkingAt:(int)angle {
    UIImageView *arrowImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"carousel_triangle_red.png"]];
    
    int x = self.arcCenter.x + ( (self.radius + 8) * cos(degreesToRadians(angle)));
    int y = self.arcCenter.y - ( (self.radius + 8) * sin(degreesToRadians(angle)));
    if(angle < 80) {
       x = self.arcCenter.x + ( (self.radius + 12) * cos(degreesToRadians(angle - 7)));
       y = self.arcCenter.y - ( (self.radius + 12) * sin(degreesToRadians(angle - 7)));
    }
    arrowImage.frame = CGRectMake(x,y,5,5);
    
    if(angle > 110) {
        arrowImage.transform =  CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(-50));
    }else if(angle < 75) {
        arrowImage.transform =  CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(50));
    }
    
    UILabel *currentMarking = [UILabel new];
    currentMarking.font = FONT_B3;
    currentMarking.textColor = COLOUR_RED;
    currentMarking.text = [NSString stringWithFormat:@"%@",[self.dataSourceForView objectForKey:@"TargetAchieved"]];
    [currentMarking sizeToFit];
    
    x = self.arcCenter.x + ( (self.radius + 34) * cos(degreesToRadians(angle)));
    y = self.arcCenter.y - ( (self.radius + 34) * sin(degreesToRadians(angle)));
    currentMarking.frame = CGRectMake(x, y, currentMarking.frame.size.width, currentMarking.frame.size.height);
    
    [self addSubview:arrowImage];
    [self addSubview:currentMarking];
}

-(NSString *) convertToMillionBhat:(NSString *) value {
    if(![value isKindOfClass:[NSNull class]]) {
        double netSales = [value doubleValue];
        netSales /= MILLION_CONSTANT;
        return [NSString stringWithFormat:@"%.2f",netSales];
    }
    return 0;
}

-(NSString *) addCommaThousandSeparatorsWithInputString: (NSString *) inputString
{
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setNumberStyle:NSNumberFormatterDecimalStyle]; // to get commas (or locale equivalent)
    [fmt setGroupingSeparator:@","];

    return [fmt stringFromNumber:@([inputString intValue])];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
