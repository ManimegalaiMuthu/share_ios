//
//  ContractListViewController.m
//  Shell
//
//  Created by Nach on 19/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "ContractListViewController.h"
#import "PopupInfoTableViewController.h"
#import "FilterByViewController.h"

@interface ContractListViewController ()<UITableViewDelegate,UITableViewDataSource,WebServiceManagerDelegate,UITextFieldDelegate,FilterByDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textSearchField;
@property (weak, nonatomic) IBOutlet UILabel *lblFilterBy;
@property (weak, nonatomic) IBOutlet UITableView *contractListTableView;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;
@property (weak, nonatomic) IBOutlet UILabel *lblNoDataFound;

@property NSDictionary *responseData;
@property NSArray *contractListArray;
@property NSArray *workshopList;

@property BOOL isDSR;

@property NSMutableDictionary *contractListParameters;

typedef enum {
    kCONTRACTLIST_TABLEVIEWCELL_STATUS_IMAGE = 1,
    kCONTRACTLIST_TABLEVIEWCELL_CONTRACTID,
    kCONTRACTLIST_TABLEVIEWCELL_WORKSHOPIMAGE,
    kCONTRACTLIST_TABLEVIEWCELL_WORKSHOPNAME,
    kCONTRACTLIST_TABLEVIEWCELL_PERIODIMAGE,
    kCONTRACTLIST_TABLEVIEWCELL_PERIODLABEL,
}kCONTRACTLIST_TABLEVIEWCELL_TAG;

@end

@implementation ContractListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.responseData = [[NSMutableDictionary alloc]init];
    
    self.contractListTableView.tableFooterView = [UIView new];
    
    self.contractListParameters = [[NSMutableDictionary alloc]init];
    self.contractListParameters[@"Status"] = @"";
    self.contractListParameters[@"Keyword"] = @"";
    self.contractListParameters[@"Period"] = @"";
    self.contractListParameters[@"WorkshopCategory"] = @"";
    
//    [[WebServiceManager sharedInstance]loadLoyaltyContractList:self.contractListParameters vc:self];
    
    if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
        UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 10)];
        [self.textSearchField setLeftViewMode:UITextFieldViewModeAlways];
        [self.textSearchField setLeftView:spacerView];
           
        self.textSearchField.font = FONT_B1;
        self.textSearchField.placeholder = LOCALIZATION(C_LOYALTY_SEARCHBYWORKSHOPNAME);            //lokalise 7 Feb
        self.textSearchField.textColor = COLOUR_VERYDARKGREY;
        self.textSearchField.delegate = self;
        self.isDSR = YES;
    } else {
        [NSLayoutConstraint constraintWithItem:self.textSearchField attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
        [NSLayoutConstraint constraintWithItem:self.textSearchField attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0].active = YES;
    }
    
    self.lblFilterBy.font = FONT_B1;
    //Space is intentded for UI purposes
    self.lblFilterBy.text = [NSString stringWithFormat:@"  %@",LOCALIZATION(C_LOYALTY_FILTERBY)]; //lokalise 7 Feb
    self.lblFilterBy.textColor = COLOUR_VERYDARKGREY;
    
    self.lblNoDataFound.font = FONT_B1;
    self.lblNoDataFound.text = LOCALIZATION(C_TABLE_NODATAFOUND);       //lokalised
    self.lblNoDataFound.textColor = COLOUR_LIGHTGREY;
    
    self.contractListArray = [[NSArray alloc]init];
    
    UIImage *addImage = GET_ISADVANCE ? [UIImage imageNamed:@"btn_addoffer_blue@3x"] : [UIImage imageNamed:@"btn_addoffer_red"];
    [self.btnAdd setImage:addImage forState:UIControlStateNormal];
    if(self.isDSR) {
        [self.btnAdd setHidden:NO];
        [self.btnAdd setUserInteractionEnabled:YES];
    } else {
        [self.btnAdd setHidden:YES];
        [self.btnAdd setUserInteractionEnabled:NO];
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(self.isDSR) {
        [[WebServiceManager sharedInstance] fetchLoyaltyWorkshopList:self];
    }
    [[WebServiceManager sharedInstance] loadLoyaltyContractList:self.contractListParameters vc:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_LOYALTY_LOYALTYPROGRAM),LOCALIZATION_EN(C_LOYALTY_CONTRACTS_TITLE)] screenClass:nil];
}

- (IBAction)filterByPressed:(UIButton *)sender {
    [mSession pushFilterBy:self.contractListParameters delegate:self vc:self.parentViewController];
}

- (IBAction)legendPressed:(UIButton *)sender {
    [self.view endEditing: YES];
    
    PopupInfoTableViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPINFOTABLEVIEW];
    
    [self popUpViewWithPopupVC:popupVc];
    
    [popupVc initLoyaltyLegendsPopup];
}

- (IBAction)addContractPressed:(id)sender {
    [mSession pushAddNewContract:self.parentViewController];
}

- (IBAction)workshopSelection:(UIButton *)sender {
     [self popUpListViewWithTitle: LOCALIZATION(C_RU_BREAKDOWN_CHOOSEWORKSHOPS) listArray:[self getWorkshopNames] withSearchField: YES];    //lokalised
}


#pragma mark UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.contractListArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    NSDictionary *cellData;
    if(self.isDSR) {
        cell = [self.contractListTableView dequeueReusableCellWithIdentifier:@"DSRLoyaltyContractListTableViewCell" forIndexPath:indexPath];
        cellData = [self.contractListArray objectAtIndex:indexPath.row];
    } else {
        cell = [self.contractListTableView dequeueReusableCellWithIdentifier:@"TOLoyaltyContractListTableViewCell" forIndexPath:indexPath];
        cellData = [self.contractListArray objectAtIndex:indexPath.row];
    }
    
    UIImageView *contractImageView = [cell viewWithTag:1];
    switch([[cellData objectForKey:@"Status"] intValue]) {
        case kCONTRACTSTATUS_INPROGRESS: //In Progress
            contractImageView.image = [UIImage imageNamed:@"icn_processing_small_on"];
            break;
            
        case kCONTRACTSTATUS_COMPLETED: //Completed
            contractImageView.image = [UIImage imageNamed:@"icn_delivered_small_on"];
            break;
            
        case kCONTRACTSTATUS_UNDERREVIEW://Under Review
            contractImageView.image = [UIImage imageNamed:@"icn_underreview_big"];
            break;
            
        case kCONTRACTSTATUS_INCOMPLETE://Incompleted
        case kCONTRACTSTATUS_UNACCEPTED://Unaccepted Contract
            contractImageView.image = [UIImage imageNamed:@"icn_cancelled"];
            break;
            
        case kCONTRACTSTATUS_PENDING:
            contractImageView.image = [UIImage imageNamed:@"icn_pending_small_on"];
            
        default:
            contractImageView.image = [UIImage imageNamed:@"icn_pending_small_on"];
            break;
    }
    contractImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    UILabel *lblContractId = [cell viewWithTag:2];
    lblContractId.text = [NSString stringWithFormat:@"%@: %@",LOCALIZATION(C_LOYALTY_CONTRACTID),[cellData objectForKey:@"ContractReference"] ];  //lokalised 7 Feb
    lblContractId.textColor = COLOUR_VERYDARKGREY;
    lblContractId.font = FONT_H1;
    
    if(self.isDSR) {
        UILabel *lblWorkshopName = [cell viewWithTag:4];
        lblWorkshopName.text = [NSString stringWithFormat:@"%@",[cellData objectForKey:@"CompanyName"] ];
        lblWorkshopName.textColor = COLOUR_VERYDARKGREY;
        lblWorkshopName.font = FONT_B2;
    }
    
    UILabel *lblPeriod = [cell viewWithTag:6];
    lblPeriod.text = [NSString stringWithFormat:@"%@ - %@",[cellData objectForKey:@"StartDate"],[cellData objectForKey:@"EndDate"]];
    lblPeriod.textColor = COLOUR_VERYDARKGREY;
    lblPeriod.font = FONT_B2;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *cellData = [self.contractListArray objectAtIndex:indexPath.row];
    [[WebServiceManager sharedInstance]loadLoyaltyContractDetails:[cellData objectForKey:@"ContractReference"] vc:self];
    
}

#pragma maek WebserviceManager
-(void)processCompleted:(WebServiceResponse *)response {
    switch (response.webserviceCall) {
        case kWEBSERVICE_LOYALTY_FETCHWORKSHOPLIST:{
            _workshopList = [[response getGenericResponse] objectForKey:@"WorkshopList"];
            if(_workshopList == nil || [_workshopList isKindOfClass:[NSNull class]]) {
                _workshopList = @[];
            }
        }
            break;
        case kWEBSERVICE_LOYALTY_LOADCONTRACTLIST:
        {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                self.responseData = [response getGenericResponse];
                
                if([self.responseData objectForKey:@"ContracList"] != nil && ![[self.responseData objectForKey:@"ContracList"] isKindOfClass:[NSNull class]] ){
                    self.contractListArray = [self.responseData objectForKey:@"ContracList"];
                    self.lblNoDataFound.hidden = YES;
                     self.contractListTableView.hidden = NO;
                    if(_contractListArray.count == 0) {
                        self.contractListArray = @[];
                        self.contractListTableView.hidden = YES;
                        self.lblNoDataFound.hidden = NO;
                    }
                } else {
                    self.contractListArray = @[];
                    self.lblNoDataFound.hidden = NO;
                    self.contractListTableView.hidden = YES;
                }
                
                [self.contractListTableView reloadData];
            }
        }
            break;
        case kWEBSERVICE_LOYALTY_LOADCONTRACTDETAILS:{
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
                    [mSession pushContractDetails:[response getGenericResponse] vc:self.parentViewController];
                } else {
                    NSDictionary *responseData = [response getGenericResponse];
                    if(![[responseData objectForKey:@"Status"] isKindOfClass:[NSNull class]]) {
                        switch([[responseData objectForKey:@"Status"] intValue]) {
                            case kCONTRACTSTATUS_PENDING: {
                                [mSession pushPendingContractDetails:[response getGenericResponse] vc:self.navigationController.parentViewController];
                            }
                                break;
                            default:{
                                [mSession pushContractDetailsForOwner:[response getGenericResponse] vc:self.navigationController.parentViewController];
                            }
                                break;
                        }
                    }
                }
            }
        }
            break;
        default:
            break;
    }
}

#pragma mark - Search by workshopName
-(NSArray*) getWorkshopNames {
    if(_workshopList == nil || [_workshopList isKindOfClass:[NSNull class]]){
        return @[];
    }
    NSMutableArray *workshopNames = [[NSMutableArray alloc]init];
    for(NSDictionary *dict in _workshopList) {
        [workshopNames addObject: [dict objectForKey:@"CompanyName"]];
    }
    return workshopNames;
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        self.textSearchField.text = [NSString stringWithFormat:@"  %@",selection.firstObject];
        self.contractListParameters[@"Keyword"] = selection.firstObject;
        
        [[WebServiceManager sharedInstance]loadLoyaltyContractList:self.contractListParameters vc:self];
    }
}

#pragma mark - FilterByDelegate
-(void) filterBySelection:(NSDictionary *)selection {
    self.contractListParameters[@"Status"] = [selection objectForKey:@"Status"];
    self.contractListParameters[@"Period"] = [selection objectForKey:@"Period"];
    self.contractListParameters[@"WorkshopCategory"] = [selection objectForKey:@"WorkshopCategory"];
    
    if(self.isDSR)
        [[WebServiceManager sharedInstance]loadLoyaltyContractList:self.contractListParameters vc:self];
}

#pragma mark - textFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if(textField == self.textSearchField) {
        [textField resignFirstResponder];
        
        self.contractListParameters[@"Keyword"] = textField.text;
        
        [[WebServiceManager sharedInstance]loadLoyaltyContractList:self.contractListParameters vc:self];
    }
}
@end
