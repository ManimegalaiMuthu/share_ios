//
//  LoyaltyContractDetailsViewController.h
//  Shell
//
//  Created by Nach on 27/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoyaltyContractDetailsViewController : BaseVC

@property NSDictionary* contractDetails;
@property BOOL isLoaded;

@end

NS_ASSUME_NONNULL_END
