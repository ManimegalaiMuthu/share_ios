//
//  OwnerContractDetailsViewController.h
//  Shell
//
//  Created by Nach on 6/4/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface OwnerContractDetailsViewController : BaseVC

@property NSDictionary *contractDetails;
@property BOOL isLoaded;

@end

NS_ASSUME_NONNULL_END
