//
//  LoyaltyCartonsBreakdownViewController.m
//  Shell
//
//  Created by Nach on 2/1/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "LoyaltyCartonsBreakdownViewController.h"
#import "LoyaltyBreakdownView.h"

@interface LoyaltyCartonsBreakdownViewController ()<UITableViewDelegate,UITableViewDataSource,WebServiceManagerDelegate>

@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet UIView *emptyStateView;
@property (weak, nonatomic) IBOutlet UIImageView *emptyState_Image;
@property (weak, nonatomic) IBOutlet UILabel *emptyState_Content;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UILabel *lblTotalOrdered_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalOrdered_Data;
@property (weak, nonatomic) IBOutlet UILabel *lblPendingOrders_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblPendingOrders_Data;

@property (weak, nonatomic) IBOutlet UILabel *lblOrdersFooter;

@property (weak, nonatomic) IBOutlet UILabel *lblMonthlyOverview;
@property (weak, nonatomic) IBOutlet LoyaltyBreakdownView *loyaltyBreakdownView;

@property (weak, nonatomic) IBOutlet UITableView *productDetailsTableView;
@property (weak, nonatomic) IBOutlet UITableView *productDataTableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *productBreakdownSuperScrollViewHeight;
@property (weak, nonatomic) IBOutlet UIScrollView *productBreakdownSuperScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *productDataScrollView;
@property (weak, nonatomic) IBOutlet UIView *tableContainerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *productsDetailsTable_Width;

@property NSArray *productGrpList;
@property NSMutableArray *monthBreakdownList;

@property NSMutableArray *productGrpDetailsTotalArray;

@property int currentEndMonth_Table;
@property int swipeCount_Table;

@end

@implementation LoyaltyCartonsBreakdownViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if([self.navigationController.childViewControllers count] > 1) {
        [self.btnBack setTintColor: COLOUR_VERYDARKGREY];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_LOYALTY_PROGRAMBREAKDOWN_TITLE) subtitle:@""];      //lokalise 8 Feb
        [Helper setNavigationBarAppearanceTitleLabelInTwoLines:self];
    }
    
    [self setEmptyView:YES];
//    [[WebServiceManager sharedInstance]fetchLoyaltyPerformaceForContract:@{
//                                                    @"ContractReference": @"",
//                                                    @"CallType" : @"O",
//                                                        }   vc:self];
    self.breakdownDetails = [[NSDictionary alloc] initWithDictionary:GET_PROGRAMBREAKDOWN];
    if(![self.breakdownDetails isEqualToDictionary:@{}])
        [self setEmptyView:NO];
    
    if(SCREEN_WIDTH <= 320) {
        _productsDetailsTable_Width.constant = 85;
    }
    else if(SCREEN_WIDTH <= 375) {
        _productsDetailsTable_Width.constant = 100;
    }
    
    [self setupInterface];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_LOYALTY_PROGRAMBREAKDOWN_TITLE) screenClass:nil];
}

-(void) setEmptyView : (BOOL)isEmpty {
    if(isEmpty) {
        self.emptyStateView.hidden = NO;
        self.scrollView.hidden = YES;
        
        self.emptyState_Content.text = LOCALIZATION(C_LOYALTY_EMPTYSTATE_CONTENT);
        self.emptyState_Content.textColor = COLOUR_LIGHTGREY;
        self.emptyState_Content.font = FONT_B1;
    } else {
        self.emptyStateView.hidden = YES;
        self.scrollView.hidden = NO;
    }
}


- (void) setupInterface {
    
    self.lblMonthlyOverview.text = LOCALIZATION(C_LOYALTY_MONTHLYOVERVIEW);                                     //lokalise 8 Feb
    self.lblMonthlyOverview.textColor = COLOUR_VERYDARKGREY;
    self.lblMonthlyOverview.font = FONT_H1;
    
    self.lblTotalOrdered_Title.text = [_breakdownDetails objectForKey:@"TextTotalOrdered"];
    self.lblTotalOrdered_Title.font = FONT_H1;
    self.lblTotalOrdered_Title.textColor = COLOUR_RED;
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%.1f ",[[_breakdownDetails objectForKey:@"TotalOrdered"] floatValue]] attributes:@{
                                                                                                                 NSForegroundColorAttributeName:COLOUR_RED,                                                                  NSFontAttributeName : FONT_H(18),
                                                                                                                                                                                               }];
    [attrString appendAttributedString: [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(C_LOYALTY_CARTONS) attributes:@{
                                                                                                                                           NSForegroundColorAttributeName:COLOUR_RED,
                                                                                                                 NSFontAttributeName : FONT_B3,
                                                                                                                                   }]];  //lokalise 8 Feb
    self.lblTotalOrdered_Data.attributedText = attrString;
    
    self.lblPendingOrders_Title.text = [NSString stringWithFormat:@"%@*",[_breakdownDetails objectForKey:@"TextPendingOrders"]];
    self.lblPendingOrders_Title.font = FONT_B2;
    self.lblPendingOrders_Title.textColor = COLOUR_VERYDARKGREY;
    
    NSMutableAttributedString *textPendingOrders = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%.1f ",[[
                                                                                                                                             _breakdownDetails objectForKey:@"TotalPending"] floatValue]] attributes:@{
                                                                                                                    NSForegroundColorAttributeName:COLOUR_VERYDARKGREY,                                                         NSFontAttributeName : FONT_H1 ,
                                                                                                                                                                                                       }];
    [textPendingOrders appendAttributedString: [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(C_LOYALTY_CARTONS) attributes:@{
                                                                                                                                                 NSForegroundColorAttributeName:COLOUR_VERYDARKGREY,
                                                                                                                 NSFontAttributeName : FONT_B3,
                                                                                                                                          }]];  //lokalise 8 Feb
    self.lblPendingOrders_Data.attributedText = textPendingOrders;
    self.lblPendingOrders_Data.textColor = COLOUR_VERYDARKGREY;
    
    [self.view setNeedsDisplay];
    [_loyaltyBreakdownView loadBar:_breakdownDetails isCarousel:NO];
    
    self.lblOrdersFooter.text = [NSString stringWithFormat:@"%@",[_breakdownDetails objectForKey:@"TextBDDescription1"] ] ;
    self.lblOrdersFooter.textColor = COLOUR_VERYDARKGREY;
    self.lblOrdersFooter.font = FONT_B2;
    
    _productGrpList = [_breakdownDetails objectForKey:@"ProductGroupList"];
    
    _monthBreakdownList = [[NSMutableArray alloc]init];
    for(NSDictionary *dict in [_breakdownDetails objectForKey:@"MonthList"]) {
        [_monthBreakdownList addObject:dict];
    }
       
    _productBreakdownSuperScrollViewHeight.constant = (_productGrpList.count + 1) * 40;
    _productBreakdownSuperScrollView.contentSize = CGSizeMake(SCREEN_WIDTH - 30, (_productGrpList.count + 1) * 40);
    
    _currentEndMonth_Table = -1;
    _swipeCount_Table = 0;
    
    _productGrpDetailsTotalArray = [_breakdownDetails objectForKey:@"ProductGroupSummaryList"];
    
    [self.monthBreakdownList addObject:@{
                                          @"MonthName" : [NSString stringWithFormat:@"%@",LOCALIZATION(C_LOYALTY_TOTAL)],              //lokalise 8 Feb
                                          @"MonthNumber" : @"999",
                                          }];
}


- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

#pragma mark - WebserviceManager
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_LOYALTY_WORKSHOPPERFORMANCE: {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                self.breakdownDetails = [response getGenericResponse];
                            
                SET_PROGRAMBREAKDOWN(self.breakdownDetails);
                            
                [self setEmptyView:NO];
                [self setupInterface];
            }
        }
            break;
        default:
            break;
    }
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _productGrpList.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    int spaceWidth = 65;
    
    if (SCREEN_WIDTH <= 320) {
        spaceWidth = 55;
    }
    else if(SCREEN_WIDTH <= 375) {
        spaceWidth = 55;
    }
    
    if(tableView == self.productDetailsTableView) {
        if(indexPath.row == 0) {
            return [UITableViewCell new];
        } else {
            cell = [tableView dequeueReusableCellWithIdentifier:@"FixedBreakdownColumnTableViewCell" forIndexPath:indexPath];
            
            NSDictionary *cellData = [_productGrpList objectAtIndex:indexPath.row - 1];
            
            UIView *colourTag = [cell viewWithTag:1];
            colourTag.backgroundColor = [Helper colorFromHex:[cellData objectForKey:@"ProductGroupColorCodes"]];
            
            UILabel *lblName = [cell viewWithTag:2];
            lblName.text = [cellData objectForKey:@"ProductGroupName"];
            lblName.textColor = COLOUR_VERYDARKGREY;
            lblName.font = FONT_B2;
            
            return cell;
        }
    } else {
        cell = [[UITableViewCell alloc]init];
        if(indexPath.row == 0) {
            int count = 0;
            int skipCount = 0;
            for(NSDictionary *month in _monthBreakdownList) {
                if(_swipeCount_Table > skipCount ) {
                    //do nothing
                    skipCount += 1;
                }
                else {
                    UILabel *lblMonth = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x + (count * spaceWidth), cell.frame.origin.y, 60, 40)];
                    
                    if([[month objectForKey:@"MonthNumber"] isEqualToString:@"999"]) {
                        lblMonth.text = LOCALIZATION(C_LOYALTY_TOTAL);                          //lokalise 8 Feb
                        lblMonth.backgroundColor = COLOUR_VERYPALEGREY;
                        lblMonth.font = FONT_H2;
                        lblMonth.textAlignment = NSTextAlignmentCenter;
                    }
                    else {
                        lblMonth.text = [month objectForKey:@"MonthName"];
                        lblMonth.textColor = COLOUR_VERYDARKGREY;
                        lblMonth.font = FONT_H2;
                        lblMonth.numberOfLines = 0;
                    }
                    
                    [cell addSubview:lblMonth];
                    
                    if((lblMonth.frame.origin.x + 10 + _productsDetailsTable_Width.constant) > SCREEN_WIDTH - 30) {
                        _currentEndMonth_Table = count - 1;
                    }
                    
                    count += 1;
                }
            }
           
        } else {
            NSArray *monthDataList = [_breakdownDetails objectForKey:@"ProductGroupDetailsList"];
            NSString *cellProduct = [[_productGrpList objectAtIndex:indexPath.row - 1] objectForKey:@"ProductGroupID"];
            
            int count = 0;
            int skipCount = 0;
            for(NSDictionary *month in _monthBreakdownList) {
                if(_swipeCount_Table > skipCount ) {
                    //do nothing
                    skipCount += 1;
                }
                else {
                    if([[month objectForKey:@"MonthNumber"] isEqualToString:@"999"]) {
                        UILabel *lblTotal = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x + (count * spaceWidth), cell.frame.origin.y, 60, 40)];
                        
                        for(NSDictionary *grpTotalDetail in _productGrpDetailsTotalArray) {
                            if([[grpTotalDetail objectForKey:@"ProductGroupID"] isEqualToString:cellProduct]) {
                                lblTotal.text = [NSString stringWithFormat:@"%@",[grpTotalDetail objectForKey:@"TotalQuantity"]];
                            }
                        }
                        
                        lblTotal.textColor = COLOUR_VERYDARKGREY;
                        lblTotal.backgroundColor = COLOUR_VERYPALEGREY;
                        lblTotal.font = FONT_B2;
                        lblTotal.textAlignment = NSTextAlignmentCenter;
                        
                        [cell addSubview:lblTotal];
                    } else {
                        int monthNumber = [[month objectForKey:@"MonthNumber"] intValue];
                        for(NSDictionary *monthData in monthDataList)
                        {
                            if([[monthData objectForKey:@"MonthNumber"] intValue] == monthNumber && [[monthData objectForKey:@"ProductGroupID"] isEqualToString:cellProduct]) {
                                UILabel *lblMonthData = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x + (count * spaceWidth), cell.frame.origin.y, 60, 40)];
                                
                                lblMonthData.text = [monthData objectForKey:@"TotalQuantity"];
                                lblMonthData.textColor = COLOUR_VERYDARKGREY;
                                lblMonthData.font = FONT_B2;
                                lblMonthData.textAlignment = NSTextAlignmentCenter;
                                
                                [cell addSubview:lblMonthData];
                                
                                count += 1;
                            }
                        }
                    }
                }
            }
        }
    }
    
    return cell;
}

#pragma mark - Swipe Gestures
- (IBAction)tableRightSwipe:(id)sender {
    if(_swipeCount_Table != 0 ) {
        _swipeCount_Table -= 1;
        _currentEndMonth_Table -= 1;
        [self.productDataTableView reloadData];
    }
}

- (IBAction)tableLeftSwipe:(id)sender {
    if(_swipeCount_Table < _monthBreakdownList.count - 3) {
        _currentEndMonth_Table += 1;
        _swipeCount_Table += 1;
        [self.productDataTableView reloadData];
    }
}


@end
