//
//  LoyaltyCartonsBreakdownViewController.h
//  Shell
//
//  Created by Nach on 2/1/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoyaltyCartonsBreakdownViewController : BaseVC

@property NSDictionary *breakdownDetails;

@end

NS_ASSUME_NONNULL_END
