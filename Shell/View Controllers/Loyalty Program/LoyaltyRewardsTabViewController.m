//
//  LoyaltyRewardsTabViewController.m
//  Shell
//
//  Created by Nach on 19/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "LoyaltyRewardsTabViewController.h"
#import "ContractListViewController.h"
#import "LoyaltyProgramPerformanceViewController.h"
#import "LoyaltyCartonsBreakdownViewController.h"

#import "RDVTabBarItem.h"

enum kLoyalty_Type
{
    kLoyalty_ContractList,
    kLoyalty_ProgramPerformance,
    kLoyalty_breakdownPerformance,
};

@interface LoyaltyRewardsTabViewController ()
{
    ContractListViewController *contractListViewController;
    LoyaltyRewardsTabViewController *loyaltyProgramPerformanceViewController;
    LoyaltyCartonsBreakdownViewController *breakdownPerformanceViewController;
}
@end

@implementation LoyaltyRewardsTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupInterface];
    [UpdateHUD removeMBProgress: KEY_WINDOW];
}

-(void) setupInterface {
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_LOYALTY_LOYALTYPROGRAM) subtitle: @""];        //lokalised 7 Feb
    
    [Helper setNavigationBarAppearanceTitleLabelInTwoLines:self];
    
    NSArray *title;
    
    if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
        contractListViewController  = [STORYBOARD_LOYALTYPROGRAM instantiateViewControllerWithIdentifier: VIEW_LOYALTY_CONTRACTLIST];
        UINavigationController *contractListNav = [[UINavigationController alloc] initWithRootViewController: contractListViewController];
        [contractListNav setNavigationBarHidden: YES];
        [contractListViewController view];
        
        loyaltyProgramPerformanceViewController  = [STORYBOARD_LOYALTYPROGRAM instantiateViewControllerWithIdentifier: VIEW_LOYALTY_PROGRAMPERFORMANCE];
        UINavigationController *loyaltyProgramPerformanceNav = [[UINavigationController alloc] initWithRootViewController: loyaltyProgramPerformanceViewController];
        [loyaltyProgramPerformanceNav setNavigationBarHidden: YES];
        [loyaltyProgramPerformanceViewController viewDidLoad];

        
        title = @[LOCALIZATION(C_LOYALTY_CONTRACTS_TITLE),
                           LOCALIZATION(C_LOYALTY_PROGRAMPERFORAMNCE)];                     //lokalised 7 Feb
        
        [self setViewControllers: @[contractListNav,
                                    loyaltyProgramPerformanceNav]];
    } else {
        breakdownPerformanceViewController = [STORYBOARD_LOYALTYPROGRAM instantiateViewControllerWithIdentifier: VIEW_LOYALTY_CARTONBREAKDOWN];
        UINavigationController *breakdownNav = [[UINavigationController alloc] initWithRootViewController: breakdownPerformanceViewController];
        [breakdownNav setNavigationBarHidden: YES];
        [breakdownPerformanceViewController view];
        
        contractListViewController  = [STORYBOARD_LOYALTYPROGRAM instantiateViewControllerWithIdentifier: VIEW_LOYALTY_CONTRACTLIST];
        UINavigationController *contractListNav = [[UINavigationController alloc] initWithRootViewController: contractListViewController];
        [contractListNav setNavigationBarHidden: YES];
        [contractListViewController view];
        
        title = @[LOCALIZATION(C_LOYALTY_BREAKDOWN_TITLE),
                  LOCALIZATION(C_LOYALTY_CONTRACTHISTORY_TITLE)];
        
        [self setViewControllers: @[breakdownNav,
                                    contractListNav]];
    }
    
    UIImage *selected = GET_ISADVANCE ? [UIImage imageNamed:@"tab-bg_blue"] : [UIImage imageNamed:@"tab-bg"];
    UIImage *unselected = [UIImage imageNamed:@"tab-bg_grey.png"];
    if (kIsRightToLeft) {
        title = title.reverseObjectEnumerator.allObjects.mutableCopy;
    }
    for (int i = 0; i < [self.tabBar.items count]; i++)
    {
        RDVTabBarItem *item = [self.tabBar.items objectAtIndex: i];
        [item setTitle: [title objectAtIndex: i]];
        
        
        [item setBackgroundColor: COLOUR_WHITE];
        [item setTitleUnselectedAttributes:FONT_H1 colour:COLOUR_VERYDARKGREY];
        [item setTitleSelectedAttributes:FONT_H1 colour:COLOUR_RED];
        
        [item setBackgroundSelectedImage: selected withUnselectedImage: unselected];
        
    }
    if (kIsRightToLeft) {
        [self tabBar:self.tabBar didSelectItemAtIndex:self.viewControllers.count-1];
    }
}

#pragma mark RDVTabBarDelegate
- (BOOL)tabBar:(RDVTabBar *)tabBar shouldSelectItemAtIndex:(NSInteger)index
{
    return ([tabBar selectedItem] != [[tabBar items] objectAtIndex: index]);
}

-(void)goToAddNewPage
{
    [self tabBar:self.tabBar didSelectItemAtIndex:1];
}

- (void)tabBar:(RDVTabBar *)tabBar didSelectItemAtIndex:(NSInteger)index
{
    //super method MUST BE PRESENT
    [super tabBar:tabBar didSelectItemAtIndex: index];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
