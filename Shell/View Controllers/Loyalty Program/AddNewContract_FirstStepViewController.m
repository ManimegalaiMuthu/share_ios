//
//  AddNewContract_FirstStepViewController.m
//  Shell
//
//  Created by Nach on 23/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "AddNewContract_FirstStepViewController.h"

typedef enum {
    kPOPUP_LOYALTY_WORKSHOP = 1,
    kPOPUP_LOYALTY_VOUCHER,
}kPOPUP_LOYALTY_TAG;

@interface AddNewContract_FirstStepViewController ()<WebServiceManagerDelegate,UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderView_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderView_Content;

@property (weak, nonatomic) IBOutlet UIView *selectWorkshopView;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectWorkshop_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectWorkshop_selection;

@property (weak, nonatomic) IBOutlet UIView *rewardsSelectionView;
@property (weak, nonatomic) IBOutlet UIView *rewardsSelection_RewardsView;
@property (weak, nonatomic) IBOutlet UILabel *lblRewardsSelection_Rewards_Title;

@property (weak, nonatomic) IBOutlet UIView *vouchersView;
@property (weak, nonatomic) IBOutlet UIImageView *vouchersView_Image;
@property (weak, nonatomic) IBOutlet UILabel *vouchersView_Title;
@property (weak, nonatomic) IBOutlet UIView *vouchersView_CheckboxView;
@property (weak, nonatomic) IBOutlet UIView *vouchersView_CheckboxOnView;

@property (weak, nonatomic) IBOutlet UIView *goldRewardView;
@property (weak, nonatomic) IBOutlet UIImageView *goldRewardView_Image;
@property (weak, nonatomic) IBOutlet UILabel *goldRewardView_Title;
@property (weak, nonatomic) IBOutlet UIView *goldRewardView_CheckboxView;
@property (weak, nonatomic) IBOutlet UIView *goldRewardView_CheckboxOnView;

@property (weak, nonatomic) IBOutlet UILabel *lblVoucherSelection;
@property (weak, nonatomic) IBOutlet UIImageView *img_dropDown;
@property (weak, nonatomic) IBOutlet UIButton *btnVoucherSelection;


@property (weak, nonatomic) IBOutlet UIView *rewardsSelection_PreferedValueView;
@property (weak, nonatomic) IBOutlet UILabel *lblRewardsSelection_PreferedValue_Title;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPreferedValue;
@property (weak, nonatomic) IBOutlet UIView *sliderView;
@property (weak, nonatomic) IBOutlet UILabel *lblSliderMin;
@property (weak, nonatomic) IBOutlet UILabel *lblSliderMax;
@property (weak, nonatomic) IBOutlet UISlider *horizontalSlider;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrency;
@property (weak, nonatomic) IBOutlet UILabel *lblGram;

@property (weak, nonatomic) IBOutlet UIView *currentValueView;
@property (weak, nonatomic) IBOutlet UILabel *currentValueLabel;
@property (weak, nonatomic) IBOutlet UIImageView *currentValueTriangleImage;

//ProgressNUmber Images
@property (weak, nonatomic) IBOutlet UIImageView *progressOneImageView;
@property (weak, nonatomic) IBOutlet UIImageView *progressTwoImageView;

@property NSArray *workshopList;
@property NSDictionary *responseData;
@property NSInteger popupTag;
@property NSString *companyTradeID;
@property NSString *rewardItemID;
@property NSString *selectedVoucherID;
@property BOOL isGoldSelected;

@property float minRewardValue;
@property float maxRewardValue;
@property float minGoldValue;
@property float maxGoldValue;

@property NSNumberFormatter *numberFormatter;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *currentValueViewLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *currentValueViewWidth;

@end

@implementation AddNewContract_FirstStepViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view

    [self.btnBack setTintColor: COLOUR_VERYDARKGREY];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_ADDNEWCONTRACT) subtitle:@""];
    
    _numberFormatter = [[NSNumberFormatter alloc] init];
    [_numberFormatter setGroupingSeparator:@","];
    [_numberFormatter setGroupingSize:3];
    [_numberFormatter setUsesGroupingSeparator:YES];
    
    self.lblHeaderView_Title.text = LOCALIZATION(C_LOYALTY_CHOOSEWORKSHOP);
    self.lblHeaderView_Title.font = FONT_H1;
    self.lblHeaderView_Title.textColor = COLOUR_VERYDARKGREY;
    
    self.lblHeaderView_Content.text = LOCALIZATION(C_LOYALTY_CHOOSEWORKSHOP_CONTENT);
    self.lblHeaderView_Content.font = FONT_B1;
    self.lblHeaderView_Content.textColor = COLOUR_VERYDARKGREY;
    
    self.lblSelectWorkshop_Title.text = LOCALIZATION(C_LOYALTY_SELECTWORKSHOP);                         //lokalise 8 Feb
    self.lblSelectWorkshop_Title.font = FONT_B1;
    self.lblSelectWorkshop_Title.textColor = COLOUR_VERYDARKGREY;
    
    self.lblSelectWorkshop_selection.text = LOCALIZATION(C_LOYALTY_SELECTWORKSHOP);                     //lokalise 8 Feb
    self.lblSelectWorkshop_selection.font = FONT_B1;
    self.lblSelectWorkshop_selection.textColor = COLOUR_VERYDARKGREY;
    
    [self.btnNext setTitle:LOCALIZATION(C_LOYALTY_NEXT) forState:UIControlStateNormal];                 //lokalise 8 Feb
    self.btnNext.titleLabel.font = FONT_BUTTON;
    self.btnNext.enabled = NO;
    self.btnNext.userInteractionEnabled = NO;
    [self.btnNext setBackgroundColor:COLOUR_RED];
    
    self.workshopList = [[NSArray alloc]init];
    
    [[WebServiceManager sharedInstance]fetchLoyaltyWorkshopList:self];
    
    self.lblRewardsSelection_Rewards_Title.text = LOCALIZATION(C_LOYALTY_SELECTREWARD);                 //lokalise 8 Feb
    self.lblRewardsSelection_Rewards_Title.textColor = COLOUR_VERYDARKGREY;
    self.lblRewardsSelection_Rewards_Title.font = FONT_B1;
    
    self.lblRewardsSelection_PreferedValue_Title.text = LOCALIZATION(C_LOYALTY_PREFERREDVALUE);          //lokalise 8 Feb
    self.lblRewardsSelection_PreferedValue_Title.textColor = COLOUR_VERYDARKGREY;
    self.lblRewardsSelection_PreferedValue_Title.font = FONT_B1;
    
    self.lblCurrency.text = [[[[mSession lookupTable] objectForKey:@"LanguageList"]objectAtIndex:0]objectForKey:@"Currency"];
    self.lblCurrency.textColor = COLOUR_VERYDARKGREY;
    self.lblCurrency.font = FONT_B2;
    [self.lblCurrency sizeToFit];
    
    self.lblGram.text = @"g";
    self.lblGram.textColor = COLOUR_VERYDARKGREY;
    self.lblGram.font = FONT_B2;
    [self.lblGram sizeToFit];
    
    self.textFieldPreferedValue.textColor = COLOUR_VERYDARKGREY;
    self.textFieldPreferedValue.font = FONT_H2;
    
    self.lblSliderMin.textColor = COLOUR_VERYDARKGREY;
    self.lblSliderMin.font = FONT_B3;
    self.lblSliderMax.textColor = COLOUR_VERYDARKGREY;
    self.lblSliderMax.font = FONT_B3;
    self.currentValueLabel.font = FONT_B3;
    self.currentValueLabel.backgroundColor = COLOUR_RED;
    
    self.currentValueTriangleImage.image = GET_ISADVANCE ? [UIImage imageNamed:@"carousel_triangle_blue"] : [UIImage imageNamed:@"carousel_triangle_red"];

    _responseData = [[NSDictionary alloc]init];
    _rewardItemID = @"";
    _isGoldSelected = NO;
    
    _vouchersView_CheckboxOnView.backgroundColor = COLOUR_RED;
    _goldRewardView_CheckboxOnView.backgroundColor = COLOUR_RED;
    [self.horizontalSlider setTintColor:COLOUR_RED];
    
    if(GET_ISADVANCE) {
        [self.progressOneImageView setImage: [UIImage imageNamed:@"progressnum-blue-1"]];
    } else {
        [self.progressOneImageView setImage: [UIImage imageNamed:@"progressnum-red-1"]];
    }
    [self.progressTwoImageView setImage: [UIImage imageNamed:@"progressnum-grey-2"]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / Step 1",LOCALIZATION_EN(C_LOYALTY_ADDNEW_TITLE)] screenClass:nil];
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

- (IBAction)workshopSelectionPressed:(id)sender {
    if([_workshopList count] > 0) {
        self.popupTag = kPOPUP_LOYALTY_WORKSHOP;
        self.isPositionRequired = YES;
        [self popUpListViewWithTitle: LOCALIZATION(C_RU_BREAKDOWN_CHOOSEWORKSHOPS) listArray:[self getWorkshopNames] withSearchField: YES];     //lokalised
    } else {
        [self popUpViewWithTitle:LOCALIZATION(C_LOYALTY_ALERT) content:LOCALIZATION(C_LOYALTY_NOWORKSHOPAVAILABLE)];                //lokalise 8 Feb
    }
}

- (IBAction)nextPressed:(id)sender {
    /* Used till Phase 3
    NSDictionary *requestParams = @{@"CallType"         :   @"N",
                                    @"TradeID"          :   _companyTradeID,
                                    @"PrefferedValue"   :   _textFieldPreferedValue.text,
                                    @"RewardItemID"     :   _rewardItemID,
                                    @"IsGold"           :   @(_isGoldSelected),
                                    };
     */
    NSDictionary *requestParams = @{@"CallType"         :   @"N",
                                    @"TradeID"          :   _companyTradeID,
                                    };
    
    SET_LOYALTYTYPE(requestParams);
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[WebServiceManager sharedInstance] loadLoyaltyPackageList:requestParams vc:self];
    
}

- (IBAction)voucherSelectionPressed:(id)sender {
    if(!_isGoldSelected) {
        self.popupTag = kPOPUP_LOYALTY_VOUCHER;
        self.isPositionRequired = NO;
        [self popUpListViewWithTitle: LOCALIZATION(C_LOYALTY_GIFTVOUCHER) listArray:[self vouchersList] withSearchField: YES];      //lokalise 8 Feb
    }
}


#pragma mark - WebService
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_LOYALTY_FETCHWORKSHOPLIST:{
            _workshopList = [[response getGenericResponse] objectForKey:@"WorkshopList"];
            if(_workshopList == nil || [_workshopList isKindOfClass:[NSNull class]]) {
               _workshopList = @[];
                
            }
        }
            break;
        case kWEBSERVICE_LOYALTY_FETCHCATALOGUELIST:{
            [self setupRewardsView:[response getGenericResponse]];
        }
            break;
        case kWEBSERVICE_LOYALTY_LOADPACKAGELIST:{
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                if(![[[response getGenericResponse]objectForKey:@"PackageInfo"] isKindOfClass:[NSNull class]])
                {
//                    if([[_responseData objectForKey:@"HasMultiplierCatalogue"]boolValue] && ![[_responseData objectForKey:@"CatalogueListMultiplier"] isKindOfClass:[NSNull class]]) {
//                       [mSession pushFinalViewInNewContract:self withPreferedDict:[response getGenericResponse] forTradeId:self.companyTradeID withCatalogue:@[
//                                                                                                                                                               [_responseData objectForKey:@"CatalogueList"],
//                                                                                                                                                               [_responseData objectForKey:@"CatalogueListMultiplier"]]];
//                    }
//                    else {
//                        [mSession pushFinalViewInNewContract:self withPreferedDict:[response getGenericResponse] forTradeId:self.companyTradeID withCatalogue:@[[_responseData objectForKey:@"CatalogueList"]]];
//                    }
                    [mSession pushFinalViewInNewContract:self withPreferedDict:[response getGenericResponse] forTradeId:self.companyTradeID withCatalogue:@[]];
                } else {
                    [self popUpViewWithTitle:LOCALIZATION(C_LOYALTY_ALERT) content:LOCALIZATION(C_LOYALTY_NOPACKAGES)];         //lokalise 8 feb
                }
            }
        }
            break;
        default:
            break;
    }
}

#pragma mark - Helper Methods

-(NSArray*) getWorkshopNames {
    if(_workshopList == nil || [_workshopList isKindOfClass:[NSNull class]]){
        return @[];
    }
    NSMutableArray *workshopNames = [[NSMutableArray alloc]init];
    for(NSDictionary *dict in _workshopList) {
        [workshopNames addObject: [dict objectForKey:@"CompanyName"]];
    }
    return workshopNames;
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        switch (_popupTag) {
            case kPOPUP_LOYALTY_WORKSHOP:
            {
                NSDictionary *dict = [selection objectAtIndex:0];
                _companyTradeID = @"";
                
                NSIndexPath *indexPath = [dict objectForKey:@"IndexPath"];
                NSDictionary *workshopData = [_workshopList objectAtIndex:indexPath.row];
                if([[dict objectForKey:@"Name"] isEqualToString: [workshopData objectForKey:@"CompanyName"]]) {
                    _companyTradeID = [workshopData objectForKey:@"CompanyTradeID"];
                    self.lblSelectWorkshop_selection.text = [NSString stringWithFormat:@"%@",[workshopData objectForKey:@"CompanyName"]];
//                    [[WebServiceManager sharedInstance]fetchLoyaltyCatalogueListWithTradeID:_companyTradeID vc:self];
                    [self.btnNext setEnabled:YES];
                    [self.btnNext setUserInteractionEnabled:YES];
                } else {
                    self.lblSelectWorkshop_selection.text = LOCALIZATION(C_LOYALTY_SELECTWORKSHOP);
                    [self.btnNext setEnabled:NO];
                    [self.btnNext setUserInteractionEnabled:NO];
                }
            }
                break;
            case kPOPUP_LOYALTY_VOUCHER:
            {
                self.lblVoucherSelection.text = [NSString stringWithFormat:@"   %@",selection.firstObject ];
                
                for(NSDictionary *dict in [_responseData objectForKey:@"CatalogueList"]) {
                    if([[dict objectForKey:@"ItemName"] isEqualToString:selection.firstObject]) {
                        self.rewardItemID = [NSString stringWithFormat:@"%@",[dict objectForKey:@"ItemID"]];
                        self.selectedVoucherID = self.rewardItemID;
                        break;
                    }
                }
                
            }
                break;
            default:
                break;
        }
    }
}

-(void) setupRewardsView:(NSDictionary *)responseData {
    _responseData = responseData;
    self.rewardsSelectionView.hidden = NO;
    
    self.vouchersView_Image.image = [UIImage imageNamed:@"icn_giftvouchers_yellow"];
    self.vouchersView_Image.contentMode = UIViewContentModeScaleAspectFit;
    self.vouchersView_Title.text = LOCALIZATION(C_LOYALTY_GIFTVOUCHER);         //lokalise 8 Feb
    self.vouchersView_Title.textColor = COLOUR_VERYDARKGREY;
    self.vouchersView_Title.font = FONT_B1;
    
    if([[_responseData objectForKey:@"MinRewardsValue"] isKindOfClass:[NSNull class]]) {
        _lblSliderMin.text = @"0";
        self.minRewardValue = 0.0;
        _textFieldPreferedValue.text = @"0";
    } else {
        _lblSliderMin.text = [_numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[_responseData objectForKey:@"MinRewardsValue"] floatValue]]];
        self.minRewardValue = [[_responseData objectForKey:@"MinRewardsValue"]floatValue];
        _textFieldPreferedValue.text = [_responseData objectForKey:@"MinRewardsValue"];
    }
    
    if([[_responseData objectForKey:@"MaxRewardsValue"] isKindOfClass:[NSNull class]]) {
        _lblSliderMax.text = @"0";
        self.maxRewardValue = 0.0;
    } else {
        _lblSliderMax.text = [_numberFormatter stringFromNumber:[NSNumber numberWithFloat: [[_responseData objectForKey:@"MaxRewardsValue"] floatValue]]];
        self.maxRewardValue = [[_responseData objectForKey:@"MaxRewardsValue"]floatValue];
    }
    
    _textFieldPreferedValue.delegate = self;
    
    self.vouchersView_CheckboxOnView.hidden = NO;
    self.lblCurrency.hidden = NO;
    self.lblGram.hidden = YES;
    
    
    [self.horizontalSlider setMinimumValue:self.minRewardValue];
    [self.horizontalSlider setMaximumValue:self.maxRewardValue];
    [self.horizontalSlider setValue:self.minRewardValue animated:YES];
    [self sliderValueChanged:self.horizontalSlider];
   
    
    if([responseData objectForKey:@"CatalogueList"] != nil) {
        self.lblVoucherSelection.text = [NSString stringWithFormat:@"   %@",[[[responseData objectForKey:@"CatalogueList"] objectAtIndex:0] objectForKey:@"ItemName"]];
        self.rewardItemID = [[[responseData objectForKey:@"CatalogueList"] objectAtIndex:0] objectForKey:@"ItemID"];
        self.selectedVoucherID = self.rewardItemID;
        self.lblVoucherSelection.textColor = COLOUR_VERYDARKGREY;
        self.lblVoucherSelection.backgroundColor = [UIColor clearColor];
        self.lblVoucherSelection.font = FONT_B1;
    }
    
    if([[responseData objectForKey:@"HasGold"] boolValue]) {
        self.goldRewardView.hidden = NO;
        
        self.goldRewardView_Image.image = [UIImage imageNamed:@"icn_gold_yellow"];
        self.goldRewardView_Image.contentMode = UIViewContentModeScaleAspectFit;
        self.goldRewardView_Title.text = LOCALIZATION(C_LOYALTY_GOLD);          //lokalise 8 feb
        self.goldRewardView_Title.textColor = COLOUR_VERYDARKGREY;
        self.goldRewardView_Title.font = FONT_B1;
        
        self.goldRewardView_CheckboxView.hidden = NO;
        self.goldRewardView_CheckboxOnView.hidden = YES;
        
        self.minGoldValue = [[_responseData objectForKey:@"MinGoldValue"]floatValue];
        self.maxGoldValue = [[_responseData objectForKey:@"MaxGoldValue"]floatValue];
    } else {
        self.goldRewardView.hidden = YES;
        self.minGoldValue = 0.0;
        self.maxGoldValue = 0.0;
    }
    [self.btnNext setEnabled:YES];
    [self.btnNext setUserInteractionEnabled:YES];
}

- (IBAction)sliderValueChanged:(UISlider *)sender {
    
    CGRect trackRect = [self.horizontalSlider trackRectForBounds:self.horizontalSlider.frame];
    CGRect thumbRect = [self.horizontalSlider thumbRectForBounds:self.horizontalSlider.frame trackRect:trackRect value:self.horizontalSlider.value];
    self.currentValueLabel.text = [_numberFormatter stringFromNumber:[NSNumber numberWithFloat:self.horizontalSlider.value]];
    self.textFieldPreferedValue.text = self.currentValueLabel.text;
    [self.currentValueLabel sizeToFit];
   
    CGFloat width = self.currentValueLabel.frame.size.width < 10 ? 10 : self.currentValueLabel.frame.size.width;
    
    self.currentValueViewLeadingConstraint.constant = thumbRect.origin.x;
    self.currentValueViewWidth.constant = width + 6;

}

- (IBAction)voucherCheckPressed:(UITapGestureRecognizer *)sender {
    if(!_goldRewardView_CheckboxOnView.hidden && _vouchersView_CheckboxOnView.hidden) {
        _vouchersView_CheckboxOnView.hidden = NO;
        _goldRewardView_CheckboxOnView.hidden = YES;
        
        _lblSliderMin.text = [_numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[_responseData objectForKey:@"MinRewardsValue"] floatValue]]];
        _lblSliderMax.text = [_numberFormatter stringFromNumber:[NSNumber numberWithFloat: [[_responseData objectForKey:@"MaxRewardsValue"] floatValue]]];
        
        self.lblCurrency.hidden = NO;
        self.lblGram.hidden = YES;
        
        self.textFieldPreferedValue.text = _lblSliderMin.text;
        
        [self.horizontalSlider setMinimumValue:self.minRewardValue];
        [self.horizontalSlider setMaximumValue:self.maxRewardValue];
        [self.horizontalSlider setValue:self.minRewardValue animated:YES];
        [self sliderValueChanged:self.horizontalSlider];
        
        self.lblVoucherSelection.backgroundColor = [UIColor clearColor];
        self.lblVoucherSelection.hidden = NO;
        self.img_dropDown.hidden = NO;
        self.btnVoucherSelection.hidden = NO;
        
        self.rewardItemID = self.selectedVoucherID;
        
        _isGoldSelected = NO;
    }
}

- (IBAction)goldCheckPressed:(UITapGestureRecognizer *)sender {
    if(_goldRewardView_CheckboxOnView.hidden && !_vouchersView_CheckboxOnView.hidden) {
        _vouchersView_CheckboxOnView.hidden = YES;
        _goldRewardView_CheckboxOnView.hidden = NO;
        
        _lblSliderMin.text = [_numberFormatter stringFromNumber:[NSNumber numberWithFloat:[[_responseData objectForKey:@"MinGoldValue"] floatValue]]];
        _lblSliderMax.text = [_numberFormatter stringFromNumber:[NSNumber numberWithFloat: [[_responseData objectForKey:@"MaxGoldValue"] floatValue]]];
        
        self.lblCurrency.hidden = YES;
        self.lblGram.hidden = NO;
        
        self.textFieldPreferedValue.text = _lblSliderMin.text;
        
        [self.horizontalSlider setMinimumValue:self.minGoldValue];
        [self.horizontalSlider setMaximumValue:self.maxGoldValue];
        [self.horizontalSlider setValue:self.minGoldValue animated:YES];
        [self sliderValueChanged:self.horizontalSlider];
        
        self.lblVoucherSelection.backgroundColor = COLOUR_VERYPALEGREY;
        self.lblVoucherSelection.hidden = YES;
        self.img_dropDown.hidden = YES;
        self.btnVoucherSelection.hidden = YES;
        
        self.isGoldSelected = YES;
        self.rewardItemID = @"";
    }
}

-(NSArray *) vouchersList {
    if([_responseData objectForKey:@"CatalogueList"] == nil){
        return @[];
    }
    NSMutableArray *vouchersName = [[NSMutableArray alloc]init];
    for(NSDictionary *dict in [_responseData objectForKey:@"CatalogueList"]) {
        [vouchersName addObject: [dict objectForKey:@"ItemName"]];
    }
    return vouchersName;
}

#pragma mark - TextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if(_vouchersView_CheckboxOnView.hidden == NO) {
        textField.text = [textField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        if([textField.text floatValue] >= _minRewardValue && [textField.text floatValue] <= _maxRewardValue) {
            [_horizontalSlider setValue:[textField.text floatValue] ];
            [self sliderValueChanged:_horizontalSlider];
            textField.text = [_numberFormatter stringFromNumber:[NSNumber numberWithFloat:_horizontalSlider.value]];
        }
        else {
//            UIAlertController *alertController = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_ALERTVIEW_ERROR) message:LOCALIZATION(@" Enter Value within range") preferredStyle:UIAlertControllerStyleAlert];
//            NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
//            paragraphStyle.alignment = NSTextAlignmentLeft;
//            [alertController setValue:[[NSAttributedString alloc] initWithString:LOCALIZATION(C_ALERTVIEW_ERROR) attributes:@{
//                                                                                                                              NSFontAttributeName : FONT_H1,
//                                                                                                                              NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,
//                                                                                                                              NSParagraphStyleAttributeName : paragraphStyle,
//
//                                                                                                                              }] forKey:@"attributedTitle"];
//
//            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_GLOBAL_OK) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
//                textField.text = [_numberFormatter stringFromNumber:[NSNumber numberWithFloat:_minRewardValue]];
//                [textField becomeFirstResponder];
//            }];
//            [cancelAction setValue:COLOUR_RED forKey:@"_titleTextColor"];
//            [alertController addAction:cancelAction];
//            [self presentViewController:alertController animated:YES completion:nil];
//             alertController.view.tintColor = COLOUR_RED;
            [self popUpViewWithTitle:LOCALIZATION(C_ALERTVIEW_ERROR) content:LOCALIZATION(C_LOYALTY_WITHINRANGE)];      //lokalise 8 Feb
            textField.text = [_numberFormatter stringFromNumber:[NSNumber numberWithFloat:_horizontalSlider.value]];
        }
    } else {
        textField.text = [textField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        if([textField.text floatValue] >= _minGoldValue && [textField.text floatValue] <= _maxGoldValue) {
            [_horizontalSlider setValue:[textField.text floatValue] ];
            [self sliderValueChanged:_horizontalSlider];
            textField.text = [_numberFormatter stringFromNumber:[NSNumber numberWithFloat:_horizontalSlider.value]];
        } else {
//            UIAlertController *alertController = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_ALERTVIEW_ERROR) message:LOCALIZATION(@" Enter Value within range") preferredStyle:UIAlertControllerStyleAlert];
//            NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
//            paragraphStyle.alignment = NSTextAlignmentLeft;
//            [alertController setValue:[[NSAttributedString alloc] initWithString:LOCALIZATION(C_ALERTVIEW_ERROR) attributes:@{
//                                                                                                                              NSFontAttributeName : FONT_H1,
//                                                                                                                              NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,
//                                                                                                                              NSParagraphStyleAttributeName : paragraphStyle,
//
//                                                                                                                              }] forKey:@"attributedTitle"];
//
//            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_GLOBAL_OK) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
//                textField.text = [_numberFormatter stringFromNumber:[NSNumber numberWithFloat:_minGoldValue]];
//                [textField becomeFirstResponder];
//            }];
//            [cancelAction setValue:COLOUR_RED forKey:@"_titleTextColor"];
//
//            [alertController addAction:cancelAction];
//            [self presentViewController:alertController animated:YES completion:nil];
//             alertController.view.tintColor = COLOUR_RED;
            [self popUpViewWithTitle:LOCALIZATION(C_ALERTVIEW_ERROR) content:LOCALIZATION(C_LOYALTY_WITHINRANGE)];          //lokalise 8 Feb
            textField.text = [_numberFormatter stringFromNumber:[NSNumber numberWithFloat:_horizontalSlider.value]];
        }
    }
}

@end
