//
//  LoyaltyProgramGaugeView.m
//  Shell
//
//  Created by Nach on 13/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "LoyaltyProgramGaugeView.h"
#import "Constants.h"
#import "Helper.h"
#import "Headers.h"
#define degreesToRadians(degrees)((M_PI * degrees)/180)
#define LINE_WIDTH 12.0

typedef enum {
    GaugeLegendView_ColorCode = 1,
    GaugeLegendView_Title,
    GaugeLegendView_Value,
} GaugeLegendView_Tag;

@implementation LoyaltyProgramGaugeView

-(void)awakeFromNib
{
    [super awakeFromNib];
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        //https://stackoverflow.com/questions/30335089/reuse-a-uiview-xib-in-storyboard/37668821#37668821
        // 1. load the interface
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        // 2. add as subview
        [self addSubview:self.contentView ];
        
        [self setNeedsLayout];
    }
    return self;
}

-(void) loadBar: (NSDictionary *) responseData isCarousel:(BOOL) isCarousel
{
    _viewData = responseData;
    
    //clear previously drawn circle
    NSMutableArray *layerToRemoveArray = [[NSMutableArray alloc] init]; //to hold references to actual progress ba
    for (UILabel *label in self.gaugeView.subviews){
        [label removeFromSuperview];
    }
    
    for (CALayer *layer in [self.gaugeView.layer sublayers])
    {
        if ([layer isKindOfClass: [CAShapeLayer class]])
        {
            [layerToRemoveArray addObject: layer];
        }
    }

    //remove referenced layers from superlayer
    for (CALayer *layer in layerToRemoveArray)
    { 
        [layer removeFromSuperlayer];
    }
    
    if([[responseData objectForKey:@"TotalPremiumTarget"] intValue] == 0) {
        _isPremiumAvailable = false;
    } else {
        _isPremiumAvailable = true;
    }
    
    if(isCarousel && _isPremiumAvailable) {
        _arcCenter = SCREEN_WIDTH <= 320 ? CGPointMake(SCREEN_WIDTH/2 - 10.0,132) : CGPointMake(SCREEN_WIDTH/2 - 10.0,143);
        if(SCREEN_HEIGHT <= 667) {
           _arcCenter = SCREEN_WIDTH <= 320 ? CGPointMake(SCREEN_WIDTH/2 - 10.0,110) : CGPointMake(SCREEN_WIDTH/2 - 10.0,138);
        }
    } else if(isCarousel && !_isPremiumAvailable) {
         _arcCenter = CGPointMake(SCREEN_WIDTH/2 - 10.0,145);
    }
    else {
        if(SCREEN_WIDTH <= 320) {
            _arcCenter = CGPointMake(SCREEN_WIDTH/2 - 28.0,140);
        } else {
            _arcCenter = CGPointMake(SCREEN_WIDTH/2 - 30.0,145);
        }
    }
   
    if(SCREEN_WIDTH <= 320) {
        _radius = 100;
    } else {
        _radius = 120;
    }
    
    if(self.isAgri && SCREEN_WIDTH <= 320) {
        self.arcCenter = CGPointMake(SCREEN_WIDTH/2 - 7.5,120);
    }
    
    
    _outerGaugePath = [UIBezierPath bezierPathWithArcCenter:_arcCenter radius:_radius startAngle:degreesToRadians(180) endAngle:degreesToRadians(0) clockwise:YES];
    _innerGaugePath = [UIBezierPath bezierPathWithArcCenter:_arcCenter radius:_radius - LINE_WIDTH - 2.0 startAngle:degreesToRadians(180) endAngle:degreesToRadians(0) clockwise:YES];
    
    CAShapeLayer *outerProgressLayer = [[CAShapeLayer alloc] init];
    outerProgressLayer.name = @"OuterGauge";
    [outerProgressLayer setPath:_outerGaugePath.CGPath];
    [outerProgressLayer setStrokeColor: [[Helper colorFromHex:[_viewData objectForKey:@"ColorCodeDefaultOverall"]] CGColor]];
    [outerProgressLayer setFillColor:[UIColor clearColor].CGColor];
    [outerProgressLayer setLineWidth:LINE_WIDTH];
    [self.gaugeView.layer addSublayer:outerProgressLayer];
    
    if(_isPremiumAvailable) {
        CAShapeLayer *innerProgressLayer = [[CAShapeLayer alloc] init];
        innerProgressLayer.name = @"InnerGauge";
        [innerProgressLayer setPath:_innerGaugePath.CGPath];
        [innerProgressLayer setStrokeColor:  [[Helper colorFromHex:[_viewData objectForKey:@"ColorCodeDefaultPremium"]] CGColor]];
        [innerProgressLayer setFillColor:[UIColor clearColor].CGColor];
        [innerProgressLayer setLineWidth:LINE_WIDTH];
        [self.gaugeView.layer addSublayer:innerProgressLayer];
    }

    [self.gaugeView setNeedsDisplay];
    [self.gaugeView setNeedsLayout];
    [self.gaugeView layoutSubviews];
    
    if([[_viewData objectForKey:@"NoOfOverallOrdered"]intValue] != 0) {
        [self colourArcFor:@"OverAll"];
    }
    if([[_viewData objectForKey:@"NoOfPremiumOrdered"]intValue] != 0 && _isPremiumAvailable) {
        [self colourArcFor:@"Premium"];
    }
   
    
    if([[responseData objectForKey:@"TotalPremiumTarget"] intValue] > [[responseData objectForKey:@"NoOfPremiumOrdered"] intValue] && _isPremiumAvailable) {
        [self currentMarkingFor:@"Premium"];
    }
    if([[responseData objectForKey:@"OverallTarget"] intValue] > [[responseData objectForKey:@"NoOfOverallOrdered"] intValue]) {
        [self currentMarkingFor:@"OverAll"];
    }
    
    //Creating Markers
    
    if(_isPremiumAvailable) {
        [self createMarkingViewFor:@"MinPremium"];
        [self createMarkingViewFor:@"FirstTargetPremium"];
        [self createMarkingViewFor:@"SecondTargetPremium"];
        [self createMarkingViewFor:@"MaxPremium"];
    }
    
    [self createMarkingViewFor:@"MinTotal"];
    [self createMarkingViewFor:@"FirstTargetTotal"];
    [self createMarkingViewFor:@"SecondTargetTotal"];
    [self createMarkingViewFor:@"MaxTotal"];
    
    
    self.lblTotalCartonColor.backgroundColor = [Helper colorFromHex:[_viewData objectForKey:@"ColorCodeOverall"]];
    self.lblPremiumTierColor.backgroundColor = [Helper colorFromHex:[_viewData objectForKey:@"ColorCodePremium"]];
    self.lblTotalCartonTitle.text = [_viewData objectForKey:@"TextTotalCarton"];
    self.lblTotalCartonTitle.font = FONT_B2;
    self.lblTotalCartonTitle.textColor = COLOUR_VERYDARKGREY;
    self.lblPremiumTierTitle.text = [_viewData objectForKey:@"TextPremiumTier"];
    self.lblPremiumTierTitle.font = FONT_B2;
    self.lblPremiumTierTitle.textColor = COLOUR_VERYDARKGREY;
    
    NSMutableAttributedString *textTotalCartons ;
    if([[responseData objectForKey:@"OverallTarget"] intValue] <= [[responseData objectForKey:@"NoOfOverallOrdered"] intValue]) {
        textTotalCartons= [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%.1f ",[[
                                                                                                               _viewData objectForKey:@"NoOfOverallOrdered"] floatValue]] attributes:@{
                                                                                                                                                                           NSFontAttributeName : FONT_H(18) , NSForegroundColorAttributeName : COLOUR_DARKGREEN,
                                                                                                                                                                               }];
    } else {
        textTotalCartons= [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%.1f ",[[
                                                                                                               _viewData objectForKey:@"NoOfOverallOrdered"] floatValue]] attributes:@{
                                                                                                                                                                           NSFontAttributeName : FONT_H(18) , NSForegroundColorAttributeName : COLOUR_SHELLRED,
                                                                                                                                                                           }];
    }
    
    [textTotalCartons appendAttributedString: [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"/%@",[
                                                                                                                                     _viewData objectForKey:@"OverallTarget"]] attributes:@{
                                                                                                                                       NSFontAttributeName : FONT_H2,
                                                                                                                                       NSForegroundColorAttributeName : COLOUR_VERYDARKGREY,
                                                                                                                                       }]];
    self.lblTotalCartonData.attributedText = textTotalCartons;

    NSMutableAttributedString *textPremiumCartons ;
    if([[responseData objectForKey:@"TotalPremiumTarget"] intValue] == 0) {
        self.lblPremiumTierData.hidden = YES;
        self.lblPremiumTierColor.hidden = YES;
        self.lblPremiumTierTitle.hidden = YES;
    }
    else {
        if([[responseData objectForKey:@"TotalPremiumTarget"] intValue] <= [[responseData objectForKey:@"NoOfPremiumOrdered"] intValue]) {
            textPremiumCartons= [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%.1f ",[[
                                                                                                                   _viewData objectForKey:@"NoOfPremiumOrdered"] floatValue]] attributes:@{
                                                                                                                                                                                        NSFontAttributeName : FONT_H(18) , NSForegroundColorAttributeName : COLOUR_DARKGREEN,
                                                                                                                                                                                        }];
        } else {
            textPremiumCartons= [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%.1f ",[[
                                                                                                                   _viewData objectForKey:@"NoOfPremiumOrdered"] floatValue]] attributes:@{
                                                                                                                                                                                        NSFontAttributeName : FONT_H(18) , NSForegroundColorAttributeName : COLOUR_SHELLRED,
                                                                                                                                                                                        }];
        }
        
        [textPremiumCartons appendAttributedString: [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"/%@",[
                                                                                                                                        _viewData objectForKey:@"TotalPremiumTarget"]] attributes:@{
                                                                                                                                                                                                    NSFontAttributeName : FONT_H2,
                                                                                                                                                                                                    NSForegroundColorAttributeName : COLOUR_VERYDARKGREY,
                                                                                                                                                                                                    }]];
        self.lblPremiumTierData.attributedText = textPremiumCartons;
    }

        int x,y;
        if(SCREEN_WIDTH <= 320) {
            if(_isPremiumAvailable) {
                x = _arcCenter.x + (_radius - (LINE_WIDTH*3)) * cos(degreesToRadians(135));
                y = _arcCenter.y - (_radius - (LINE_WIDTH*3)) * sin(degreesToRadians(135));
            } else {
                x = _arcCenter.x + (_radius - (LINE_WIDTH*2)) * cos(degreesToRadians(125));
                y = _arcCenter.y - (_radius - (LINE_WIDTH*2)) * sin(degreesToRadians(125));
            }
        } else {
            if(_isPremiumAvailable) {
                x = _arcCenter.x + (_radius - (LINE_WIDTH*3)) * cos(degreesToRadians(125));
                y = _arcCenter.y - (_radius - (LINE_WIDTH*3)) * sin(degreesToRadians(135));
            } else {
                x = _arcCenter.x + (_radius - (LINE_WIDTH*2)) * cos(degreesToRadians(115));
                y = _arcCenter.y - (_radius - (LINE_WIDTH*2)) * sin(degreesToRadians(125));
            }
        }
        
        UIView *totalCarton_ColorCode = [[UIView alloc]init];
        totalCarton_ColorCode.backgroundColor = [Helper colorFromHex:[_viewData objectForKey:@"ColorCodeOverall"]];
    
        
        UILabel *totalCarton_Title = [[UILabel alloc]init];
        totalCarton_Title.text = [_viewData objectForKey:@"TextTotalCarton"];
        totalCarton_Title.font = FONT_B2;
        totalCarton_Title.textColor = COLOUR_VERYDARKGREY;
        [totalCarton_Title sizeToFit];
        [totalCarton_Title setCenter:CGPointMake(self.arcCenter.x + 7, y + 7)];
        if(SCREEN_WIDTH <= 320){
            totalCarton_Title.font = FONT_B(11);
        }
        if(SCREEN_WIDTH <= 320)
            totalCarton_ColorCode.frame = CGRectMake(self.arcCenter.x - (totalCarton_Title.frame.size.width / 2) - 3, y + 8, 8, 8);
        else
            totalCarton_ColorCode.frame = CGRectMake(self.arcCenter.x - (totalCarton_Title.frame.size.width / 2) - 5, y + 7, 10, 10);
    
        [self.gaugeView addSubview:totalCarton_ColorCode];
        [self.gaugeView addSubview:totalCarton_Title];

    
        UILabel *totalCarton_Data = [[UILabel alloc]init];
        totalCarton_Data.attributedText = textTotalCartons;
        [totalCarton_Data sizeToFit];
        [totalCarton_Data setCenter:CGPointMake(self.arcCenter.x, y + 8 + totalCarton_Title.frame.size.height)];
        [self.gaugeView addSubview:totalCarton_Data];

    
        if(_isPremiumAvailable) {
            y = y + 40 ;
            
            UIView *premium_ColorCode = [[UIView alloc]init];
            premium_ColorCode.backgroundColor = [Helper colorFromHex:[_viewData objectForKey:@"ColorCodePremium"]];
            
            UILabel *premium_Title = [[UILabel alloc]init];
            premium_Title.text = [_viewData objectForKey:@"TextPremiumTier"];
            premium_Title.font = FONT_B2;
            premium_Title.textColor = COLOUR_VERYDARKGREY;
            [premium_Title sizeToFit];
            [premium_Title setCenter:CGPointMake(self.arcCenter.x + 7, y + 10)];
            if(SCREEN_WIDTH <= 320){
                premium_Title.font = FONT_B(11);
            }
            if(SCREEN_WIDTH <= 320)
                premium_ColorCode.frame = CGRectMake(self.arcCenter.x - (premium_Title.frame.size.width / 2) - 3, y + 3, 8, 8);
            else
                premium_ColorCode.frame = CGRectMake(self.arcCenter.x - (premium_Title.frame.size.width / 2) - 5, y + 3, 10, 10);
            
            [self.gaugeView addSubview:premium_ColorCode];
            [self.gaugeView addSubview:premium_Title];
            
            UILabel *premium_Data = [[UILabel alloc]init];
            premium_Data.attributedText = textPremiumCartons;
            [premium_Data sizeToFit];
            [premium_Data setCenter:CGPointMake(self.arcCenter.x, y + 8 + premium_Title.frame.size.height)];
            [self.gaugeView addSubview:premium_Data];
            
        }
        
         [self.gaugeView layoutSubviews];
//    }
    
}


-(void) createMarkingViewFor:(NSString *)markingFor {
    
    int overallTarget = [[_viewData objectForKey:@"OverallTarget"]intValue];
    int premiumTarget = [[_viewData objectForKey:@"TotalPremiumTarget"]intValue];

    UILabel *lblMarking = [[UILabel alloc ]init];
    lblMarking.backgroundColor = COLOUR_MIDGREY;
    
    UILabel *lblMarkingValue = [[UILabel alloc]init];
    lblMarkingValue.backgroundColor = [UIColor clearColor];
    lblMarkingValue.font = FONT_B3;
    lblMarkingValue.textColor = COLOUR_VERYDARKGREY;
    
    if([markingFor isEqualToString:@"MinPremium"]) {
        if(SCREEN_WIDTH <= 320) {
            lblMarking.frame = CGRectMake(_arcCenter.x - 92, _arcCenter.y, LINE_WIDTH * 1.5, 1);
        }  else {
            lblMarking.frame = CGRectMake(_arcCenter.x - 112, _arcCenter.y, LINE_WIDTH * 1.5, 1);
        }
        
        lblMarkingValue.text = @"0";
        [lblMarkingValue sizeToFit];
        
        int x = _arcCenter.x + (_radius - (LINE_WIDTH*2.5)) * cos(degreesToRadians(180));
        int y = _arcCenter.y - (_radius - (LINE_WIDTH*2.5)) * sin(degreesToRadians(180));
        lblMarkingValue.frame = CGRectMake(x, y, lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
        
    } else if([markingFor isEqualToString:@"MinTotal"]) {
        if(SCREEN_WIDTH <= 320) {
             lblMarking.frame = CGRectMake(_arcCenter.x - 113, _arcCenter.y, LINE_WIDTH * 1.5, 1);
        } else {
             lblMarking.frame = CGRectMake(_arcCenter.x - 133, _arcCenter.y, LINE_WIDTH * 1.5, 1);
        }
        lblMarkingValue.text = @"0";
        [lblMarkingValue sizeToFit];
        
        int x = _arcCenter.x + ( (_radius + (LINE_WIDTH*2)) * cos(degreesToRadians(180)));
        int y = _arcCenter.y - ( (_radius + (LINE_WIDTH*2)) * sin(degreesToRadians(180)));
        lblMarkingValue.frame = CGRectMake(x, y, lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
        
    } else if([markingFor isEqualToString:@"FirstTargetPremium"]) {
        int x,y;
        x = _arcCenter.x + ( (_radius - LINE_WIDTH + 2.0) * cos(degreesToRadians(121)));
        y = _arcCenter.y - ( (_radius - LINE_WIDTH + 2.0) * sin(degreesToRadians(121)));
        lblMarking.frame = CGRectMake(x, y, 1, LINE_WIDTH * 1.5);
        lblMarking.transform = CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(-25));
        
        if(self.isAgri) {
             lblMarkingValue.text = [NSString stringWithFormat:@"%@",[self.viewData objectForKey:@"PremiumFirstTarget"]];
        } else {
             lblMarkingValue.text = [NSString stringWithFormat:@"%d",premiumTarget * 1/3];
        }
        [lblMarkingValue sizeToFit];
        
        x = _arcCenter.x + ( (_radius - (LINE_WIDTH*2.25)) * cos(degreesToRadians(125)));
        y = _arcCenter.y - ( (_radius - (LINE_WIDTH*2.25)) * sin(degreesToRadians(125)));
        lblMarkingValue.frame = CGRectMake(x, y, lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
        
    } else if([markingFor isEqualToString:@"FirstTargetTotal"]) {
        int x,y;
        x = _arcCenter.x + ( (_radius + LINE_WIDTH - 2.0) * cos(degreesToRadians(120)));
        y = _arcCenter.y - ( (_radius + LINE_WIDTH - 2.0) * sin(degreesToRadians(120)));
        lblMarking.frame = CGRectMake(x, y, 1, LINE_WIDTH * 1.5);
        lblMarking.transform = CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(-25));
        
        if(self.isAgri) {
            lblMarkingValue.text = [NSString stringWithFormat:@"%@",[self.viewData objectForKey:@"TotalFirstTarget"]];
        } else {
            lblMarkingValue.text = [NSString stringWithFormat:@"%d",overallTarget * 1/3];
        }
        [lblMarkingValue sizeToFit];
        
        x = _arcCenter.x + ( (_radius + (LINE_WIDTH*2)) * cos(degreesToRadians(122)));
        y = _arcCenter.y - ( (_radius + (LINE_WIDTH*2)) * sin(degreesToRadians(122)));
        lblMarkingValue.frame = CGRectMake(x, y, lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
        
    } else if([markingFor isEqualToString:@"SecondTargetPremium"]) {
        int x = _arcCenter.x + ( (_radius - LINE_WIDTH + 2.0) * cos(degreesToRadians(59)));
        int y = _arcCenter.y - ( (_radius - LINE_WIDTH + 2.0) * sin(degreesToRadians(59)));
        lblMarking.frame = CGRectMake(x, y, 1, LINE_WIDTH * 1.5);
        lblMarking.transform = CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(25));
        
        if(self.isAgri) {
            lblMarkingValue.text = [NSString stringWithFormat:@"%@",[self.viewData objectForKey:@"PremiumSecondTarget"]];
        } else {
            lblMarkingValue.text = [NSString stringWithFormat:@"%d",premiumTarget * 2/3];
        }
        [lblMarkingValue sizeToFit];
        
        x = _arcCenter.x + ( (_radius - (LINE_WIDTH*2.75)) * cos(degreesToRadians(60)));
        y = _arcCenter.y - ( (_radius - (LINE_WIDTH*2.75)) * sin(degreesToRadians(60)));
        lblMarkingValue.frame = CGRectMake(x, y, lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
        
    } else if([markingFor isEqualToString:@"SecondTargetTotal"]) {
        int x,y;
        x = _arcCenter.x + ( (_radius + LINE_WIDTH - 2.0) * cos(degreesToRadians(60)));
        y = _arcCenter.y - ( (_radius + LINE_WIDTH - 2.0) * sin(degreesToRadians(60)));
        lblMarking.frame = CGRectMake(x, y, 1, LINE_WIDTH * 1.5);
        lblMarking.transform = CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(25));
        
        if(self.isAgri) {
            lblMarkingValue.text = [NSString stringWithFormat:@"%@",[self.viewData objectForKey:@"TotalSecondTarget"]];
        } else {
            lblMarkingValue.text = [NSString stringWithFormat:@"%d",overallTarget * 2/3];
        }
        [lblMarkingValue sizeToFit];
        
        x = _arcCenter.x + ( (_radius + (LINE_WIDTH*1.75)) * cos(degreesToRadians(60)));
        y = _arcCenter.y - ( (_radius + (LINE_WIDTH*1.75)) * sin(degreesToRadians(60)));
        lblMarkingValue.frame = CGRectMake(x, y, lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
        
    } else if([markingFor isEqualToString:@"MaxPremium"]) {
        if(SCREEN_WIDTH <= 320) {
             lblMarking.frame = CGRectMake(_arcCenter.x + 73, _arcCenter.y, LINE_WIDTH * 1.5, 1);
        } else {
             lblMarking.frame = CGRectMake(_arcCenter.x + 93, _arcCenter.y, LINE_WIDTH * 1.5, 1);
        }
        
        lblMarkingValue.text = [NSString stringWithFormat:@"%d",premiumTarget];
        [lblMarkingValue sizeToFit];
        
        int x = _arcCenter.x + ( (_radius - (LINE_WIDTH*3.75)) * cos(degreesToRadians(0)));
        int y = _arcCenter.y - ( (_radius - (LINE_WIDTH*3.75)) * sin(degreesToRadians(0)));
        lblMarkingValue.frame = CGRectMake(x, y, lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
    } else if([markingFor isEqualToString:@"MaxTotal"]) {
        if(SCREEN_WIDTH <= 320) {
             lblMarking.frame = CGRectMake(_arcCenter.x + 94, _arcCenter.y, LINE_WIDTH * 1.5, 1);
        } else {
             lblMarking.frame = CGRectMake(_arcCenter.x + 114, _arcCenter.y, LINE_WIDTH * 1.5, 1);
        }
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icn_rewards"]];
        
        lblMarkingValue.text = [NSString stringWithFormat:@"%d",overallTarget];
        [lblMarkingValue sizeToFit];
        
        int x = _arcCenter.x + ( (_radius + (LINE_WIDTH*1.5)) * cos(degreesToRadians(0)));
        int y = _arcCenter.y - ( (_radius + (LINE_WIDTH*1.5)) * sin(degreesToRadians(0)));
        imageView.frame = CGRectMake(x,y - 5, 15,15);
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.gaugeView addSubview:imageView];
        lblMarkingValue.frame = CGRectMake(x + 15, y, lblMarkingValue.frame.size.width, lblMarkingValue.frame.size.height);
    }
    
    [self.gaugeView addSubview:lblMarkingValue];
    [self.gaugeView addSubview:lblMarking];
    
}

-(void) colourArcFor:(NSString *)gauge{
    if([gauge isEqualToString:@"OverAll"]) {
        CGFloat overAllAngle;
        if([[_viewData objectForKey:@"NoOfOverallOrdered"] intValue] >= [[_viewData objectForKey:@"OverallTarget"] intValue]) {
            overAllAngle = 0;
        } else {
            overAllAngle = (180 * [[_viewData objectForKey:@"NoOfOverallOrdered"] floatValue] / [[_viewData objectForKey:@"OverallTarget"] floatValue]) - 180;
        }
        
        UIBezierPath *overAllPath = [UIBezierPath bezierPathWithArcCenter:_arcCenter radius:_radius startAngle:degreesToRadians(180) endAngle:degreesToRadians(overAllAngle + 4) clockwise:YES];

        CAShapeLayer *progressLayer = [CAShapeLayer layer];
        progressLayer.name = gauge;
        progressLayer.path = overAllPath.CGPath;
        [progressLayer setStrokeColor: [[Helper colorFromHex:[_viewData objectForKey:@"ColorCodeOverall"]] CGColor]];
        [progressLayer setFillColor:[UIColor clearColor].CGColor];
        [progressLayer setLineWidth:LINE_WIDTH];
        [self.gaugeView.layer addSublayer:progressLayer];
    } else if([gauge isEqualToString:@"Premium"]) {
        CGFloat overAllAngle;
        if([[_viewData objectForKey:@"NoOfPremiumOrdered"] intValue] >= [[_viewData objectForKey:@"TotalPremiumTarget"] intValue]) {
            overAllAngle = 0;
        } else {
            overAllAngle = (180 *  [[_viewData objectForKey:@"NoOfPremiumOrdered"] floatValue] / [[_viewData objectForKey:@"TotalPremiumTarget"] floatValue]) - 180;
        }
        
        
        UIBezierPath *overAllPath = [UIBezierPath bezierPathWithArcCenter:_arcCenter radius:_radius - LINE_WIDTH - 2.0 startAngle:degreesToRadians(180) endAngle:degreesToRadians(overAllAngle + 6) clockwise:YES];
        
        CAShapeLayer *progressLayer = [CAShapeLayer layer];
        progressLayer.name = gauge;
        progressLayer.path = overAllPath.CGPath;
        [progressLayer setStrokeColor: [[Helper colorFromHex:[_viewData objectForKey:@"ColorCodePremium"]] CGColor]];
        [progressLayer setFillColor:[UIColor clearColor].CGColor];
        [progressLayer setLineWidth:LINE_WIDTH];
        [self.gaugeView.layer addSublayer:progressLayer];
        
    }
    [self.gaugeView setNeedsDisplay];
    [self.gaugeView setNeedsLayout];
    [self.gaugeView layoutSubviews];
}

-(void)currentMarkingFor:(NSString *)markingFor {
    if([markingFor isEqualToString:@"OverAll"]) {
        int target = [[_viewData objectForKey:@"OverallTarget"]intValue];
        int achieved = [[_viewData objectForKey:@"NoOfOverallOrdered"]intValue];
        UIImageView *arrowImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"carousel_triangle_red.png"]];
        
        if(target * 1/3 != achieved && target * 2/3 != achieved && achieved != 0) {
            CGFloat overAllAngle = 180 - (180 / ([[_viewData objectForKey:@"OverallTarget"] floatValue] / [[_viewData objectForKey:@"NoOfOverallOrdered"] floatValue]));
            
            if(overAllAngle > 90) {
                overAllAngle -= 2;
            }
            
            int x = _arcCenter.x + ( (_radius + LINE_WIDTH +2.0) * cos(degreesToRadians(overAllAngle)));
            int y = _arcCenter.y - ( (_radius + LINE_WIDTH +2.0) * sin(degreesToRadians(overAllAngle)));
            arrowImage.frame = CGRectMake(x,y,5,5);
            
            if(overAllAngle > 110) {
                arrowImage.transform = CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(-50));
            }else if(overAllAngle < 80) {
                arrowImage.transform = CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(50));
            }
            
            UILabel *currentMarking = [[UILabel alloc]init];
            currentMarking.font = FONT_B3;
            currentMarking.textColor = COLOUR_SHELLRED;
            currentMarking.text = [NSString stringWithFormat:@"%.1f",[[_viewData objectForKey:@"NoOfOverallOrdered"] floatValue]];
            [currentMarking sizeToFit];

            x = _arcCenter.x + ( (_radius + LINE_WIDTH + 12.0) * cos(degreesToRadians(overAllAngle)));
            y = _arcCenter.y - ( (_radius + LINE_WIDTH + 12.0) * sin(degreesToRadians(overAllAngle)));

            currentMarking.frame = CGRectMake(x, y, currentMarking.frame.size.width, currentMarking.frame.size.height);
            
            if(overAllAngle > 120)
                currentMarking.frame = CGRectMake(x - 5, y - 5, currentMarking.frame.size.width, currentMarking.frame.size.height);
            
            [self.gaugeView addSubview:arrowImage];
            [self.gaugeView addSubview:currentMarking];
        }
    } else {
        int target = [[_viewData objectForKey:@"TotalPremiumTarget"]intValue];
        int achieved = [[_viewData objectForKey:@"NoOfPremiumOrdered"]intValue];
        UIImageView *arrowImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"carousel_triangle_red.png"]];
        
        if(target * 1/3 != achieved && target * 2/3 != achieved  && achieved != 0) {
            CGFloat overAllAngle = 180 - (180 / ([[_viewData objectForKey:@"TotalPremiumTarget"] floatValue] / [[_viewData objectForKey:@"NoOfPremiumOrdered"] floatValue]));
            
            int x = _arcCenter.x + ( (_radius - (LINE_WIDTH*2.3)) * cos(degreesToRadians(overAllAngle)));
            int y = _arcCenter.y - ( (_radius - (LINE_WIDTH*2.3)) * sin(degreesToRadians(overAllAngle)));
            if(overAllAngle > 110) {
                 arrowImage.frame = CGRectMake(x,y - 7,5,5);
            } else {
                 arrowImage.frame = CGRectMake(x,y,5,5);
            }
            
            if(overAllAngle > 110) {
                arrowImage.transform = CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(-30));
            }else if(overAllAngle < 80) {
                arrowImage.transform = CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(30));
            }

            UILabel *currentMarking = [[UILabel alloc]init];
            currentMarking.font = FONT_B3;
            currentMarking.textColor = COLOUR_SHELLRED;
            currentMarking.text = [NSString stringWithFormat:@"%.1f",[[_viewData objectForKey:@"NoOfPremiumOrdered"] floatValue]];
            [currentMarking sizeToFit];

            x = _arcCenter.x + ( (_radius - (LINE_WIDTH*2.6)) * cos(degreesToRadians(overAllAngle)));
            y = _arcCenter.y - ( (_radius - (LINE_WIDTH*2.6)) * sin(degreesToRadians(overAllAngle)));

            if(overAllAngle > 110) {
                currentMarking.frame = CGRectMake(x, y - 7, currentMarking.frame.size.width, currentMarking.frame.size.height);
            } else if(overAllAngle < 60) {
                currentMarking.frame = CGRectMake(x - 7, y , currentMarking.frame.size.width, currentMarking.frame.size.height);
            } else {
                currentMarking.frame = CGRectMake(x, y , currentMarking.frame.size.width, currentMarking.frame.size.height);
            }
            
            [self.gaugeView addSubview:arrowImage];
            [self.gaugeView addSubview:currentMarking];
        }
    }
}

@end
