//
//  FilterByViewController.m
//  Shell
//
//  Created by Nach on 20/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "FilterByViewController.h"

@interface FilterByViewController () <UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *filterByTableView;
@property (weak, nonatomic) IBOutlet UIButton *btnShowResults;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (strong, nonatomic) IBOutlet UIButton *btnClearAll;

@property NSMutableArray *contentArray;

@end

@implementation FilterByViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_LOYALTY_FILTERBY) subtitle:@"" size:17.0 subtitleSize:0];
    [Helper setNavigationBarAppearanceTitleLabelInTwoLines:self];
    
    _btnClearAll.titleLabel.textColor = COLOUR_VERYDARKGREY;
    _btnClearAll.titleLabel.font = FONT_B1;
    [_btnClearAll.titleLabel setNumberOfLines:0];
    [_btnClearAll setTitle:LOCALIZATION(C_LOYALTY_CLEARALL) forState:UIControlStateNormal];         //lokalise 7 Feb
    
    [self.btnBack setTintColor: COLOUR_VERYDARKGREY];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnClearAll];
    
    int count = 0;
    
    _contentArray = [[NSMutableArray alloc]init];
    if(![[mSession getContractStatus] isKindOfClass:[NSNull class]]) {
        _contentArray[count] = [mSession getContractStatus];
        count += 1;
    }
    if(![[mSession getProgramPeriod] isKindOfClass:[NSNull class]]) {
        _contentArray[count] = [mSession getProgramPeriod];
        count += 1;
    }
    if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
        if(![[mSession getCompanyCategory] isKindOfClass:[NSNull class]]) {
            _contentArray[count] = [mSession getCompanyCategory];
            count += 1;
        }
    }
    
    _filterByTableView.tableFooterView = [UIView new];
    _filterByTableView.allowsSelection = YES;
    
    [_btnShowResults setTitle:LOCALIZATION(C_LOYALTY_FILTER_SHOWRESULTS) forState:UIControlStateNormal];        //lokalise 7 Feb
    _btnShowResults.titleLabel.font = FONT_BUTTON;
    [_btnShowResults setBackgroundColor:COLOUR_RED];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_LOYALTY_FILTERBY) screenClass:nil];
}

- (IBAction)showResultsButton:(id)sender {
    [_delegate filterBySelection:_filterStatusArray];
    [self popSelf];
}

- (IBAction)backButtonPressed:(id)sender {
    [self clearAllPressed:nil];
    [self popSelf];
}
- (IBAction)clearAllPressed:(id)sender {
    _filterStatusArray[@"Status"] = @"";
    _filterStatusArray[@"Period"] = @"";
    _filterStatusArray[@"WorkshopCategory"] = @"";
    
    [_filterByTableView reloadData];
}

#pragma mark UITableViewDelegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FilterByTableViewCell" forIndexPath:indexPath];
    
    NSString *cellText = [[self.contentArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    UILabel *label = [cell viewWithTag:1];
    label.text = cellText;
    label.textColor = COLOUR_VERYDARKGREY;
    label.font = FONT_B1;
    
    UIView *checkboxView = [cell viewWithTag:2];
    UIImageView *checkedImage = [cell viewWithTag:699];
    switch(indexPath.section) {
        case 0:
            if(![[_filterStatusArray objectForKey:@"Status"]isEqualToString:@""]) {
                NSArray *statusSelected = [[_filterStatusArray objectForKey:@"Status"] componentsSeparatedByString:@","];
                if([statusSelected containsObject: [mSession convertToContractStatusKeyCode:cellText]]) {
                    checkboxView.hidden = YES;
                    checkedImage.hidden = NO;
                }
            } else {
                checkboxView.backgroundColor = [UIColor clearColor];
                checkboxView.hidden = NO;
                checkedImage.hidden = YES;
            }
            break;
        case 1:
            if(![[_filterStatusArray objectForKey:@"Period"]isEqualToString:@""]) {
                NSArray *statusSelected = [[_filterStatusArray objectForKey:@"Period"] componentsSeparatedByString:@","];
                if([statusSelected containsObject: [mSession convertToProgramPeriodKeyCode:cellText]]) {
                    checkboxView.hidden = YES;
                    checkedImage.hidden = NO;
                }
            } else {
                checkedImage.hidden = YES;
                checkboxView.hidden = NO;
                checkboxView.backgroundColor = [UIColor clearColor];
            }
            break;
        case 2:
            if(![[_filterStatusArray objectForKey:@"WorkshopCategory"]isEqualToString:@""]) {
                NSArray *statusSelected = [[_filterStatusArray objectForKey:@"WorkshopCategory"] componentsSeparatedByString:@","];
                if([statusSelected containsObject: [mSession convertToCompanyCategoryKeyCode:cellText]]) {
                    checkboxView.hidden = YES;
                    checkedImage.hidden = NO;
                }
            } else {
                checkedImage.hidden = YES;
                checkboxView.hidden = NO;
                checkboxView.backgroundColor = [UIColor clearColor];
            }
            break;
        default:
            break;
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *sectionArray = [_contentArray objectAtIndex:section];
    return sectionArray.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView= [UIView new];
    NSArray *sectionContent = [_contentArray objectAtIndex:section];
    if(sectionContent.count == 0) {
        return headerView;
    }
    UILabel *label = [[UILabel alloc]init];
    label.frame = CGRectMake(headerView.frame.origin.x, headerView.frame.origin.x, 300, 30);
    switch(section) {
        case 0:
            label.text = LOCALIZATION(C_LOYALTY_FILTER_STATUS);             //lokalise 7 Feb
        break;
        case 1:
            label.text = LOCALIZATION(C_LOYALTY_FILTER_POROGRAMPERIOD);     //lokalise 7 Feb
        break;
        case 2:
            label.text = LOCALIZATION(C_LOYALTY_FILTER_WORKSHOPCATEGORY);   //lokalise 7 Feb
        break;
    }
    label.font = FONT_H1;
    label.textColor = COLOUR_VERYDARKGREY;
    [headerView addSubview:label];
    return headerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.contentArray.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    NSString *cellText = [[self.contentArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    UIView *checkboxView = [cell viewWithTag:2];
    UIImageView *checkedImage = [cell viewWithTag:699];
    switch(indexPath.section) {
        case 0:
            if(![[_filterStatusArray objectForKey:@"Status"]isEqualToString:@""]) {
                NSArray *statusSelected = [[_filterStatusArray objectForKey:@"Status"] componentsSeparatedByString:@","];
                if([statusSelected containsObject: [mSession convertToContractStatusKeyCode:cellText]]) {
                    checkboxView.hidden = NO;
                    checkedImage.hidden = YES;
                    NSMutableArray *statusSelected_Mutable = statusSelected.mutableCopy;
                    [statusSelected_Mutable removeObject: [mSession convertToContractStatusKeyCode:cellText]];
                    _filterStatusArray[@"Status"] = [statusSelected_Mutable componentsJoinedByString:@","];
                }
                else {
                    NSMutableArray *statusSelected_Mutable = statusSelected.mutableCopy;
                    [statusSelected_Mutable addObject:[mSession convertToContractStatusKeyCode:cellText]];
                    _filterStatusArray[@"Status"] = [statusSelected_Mutable componentsJoinedByString:@","];
                    checkboxView.hidden = YES;
                    checkedImage.hidden = NO;
                }
            } else {
                checkboxView.hidden = YES;
                checkedImage.hidden = NO;
                _filterStatusArray[@"Status"] = [NSString stringWithFormat:@"%@",[mSession convertToContractStatusKeyCode:cellText]];
            }
            break;
        case 1:
            if(![[_filterStatusArray objectForKey:@"Period"]isEqualToString:@""]) {
                NSArray *statusSelected = [[_filterStatusArray objectForKey:@"Period"] componentsSeparatedByString:@","];
                if([statusSelected containsObject: [mSession convertToProgramPeriodKeyCode:cellText]]) {
                    checkboxView.hidden = NO;
                    checkedImage.hidden = YES;
                    NSMutableArray *statusSelected_Mutable = statusSelected.mutableCopy;
                    [statusSelected_Mutable removeObject: [mSession convertToProgramPeriodKeyCode:cellText]];
                    _filterStatusArray[@"Period"] = [statusSelected_Mutable componentsJoinedByString:@","];
                }
                else {
                    NSMutableArray *statusSelected_Mutable = statusSelected.mutableCopy;
                    [statusSelected_Mutable addObject:[mSession convertToProgramPeriodKeyCode:cellText]];
                    _filterStatusArray[@"Period"] = [statusSelected_Mutable componentsJoinedByString:@","];
                    checkboxView.hidden = YES;
                    checkedImage.hidden = NO;
                }
            } else {
                checkboxView.hidden = YES;
                checkedImage.hidden = NO;
                _filterStatusArray[@"Period"] = [NSString stringWithFormat:@"%@",[mSession convertToProgramPeriodKeyCode:cellText]];
            }
            break;
        case 2:
            if(![[_filterStatusArray objectForKey:@"WorkshopCategory"]isEqualToString:@""]) {
                NSArray *statusSelected = [[_filterStatusArray objectForKey:@"WorkshopCategory"] componentsSeparatedByString:@","];
                if([statusSelected containsObject: [mSession convertToCompanyCategoryKeyCode:cellText]]) {
                    checkboxView.hidden = NO;
                    checkedImage.hidden = YES;
                    NSMutableArray *statusSelected_Mutable = statusSelected.mutableCopy;
                    [statusSelected_Mutable removeObject: [mSession convertToCompanyCategoryKeyCode:cellText]];
                    _filterStatusArray[@"WorkshopCategory"] = [statusSelected_Mutable componentsJoinedByString:@","];
                }
                else {
                    checkboxView.hidden = YES;
                    checkedImage.hidden = NO;
                    NSMutableArray *statusSelected_Mutable = statusSelected.mutableCopy;
                    [statusSelected_Mutable addObject:[mSession convertToCompanyCategoryKeyCode:cellText]];
                    _filterStatusArray[@"WorkshopCategory"] = [statusSelected_Mutable componentsJoinedByString:@","];
                }
            } else {
                checkboxView.hidden = YES;
                checkedImage.hidden = NO;
                _filterStatusArray[@"WorkshopCategory"] = [NSString stringWithFormat:@"%@",[mSession convertToCompanyCategoryKeyCode:cellText]];
            }
            break;
        default:
            break;
    }
    [cell setSelected:NO];
}

@end
