//
//  LoyaltyProgramPerformanceViewController.m
//  Shell
//
//  Created by Nach on 19/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "LoyaltyProgramPerformanceViewController.h"
#import "LoyaltyProgramGaugeView.h"

@interface LoyaltyProgramPerformanceViewController () <WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblTotalContracts;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalContract_Data;
@property (weak, nonatomic) IBOutlet UILabel *lblProgramPerformance;
@property (weak, nonatomic) IBOutlet LoyaltyProgramGaugeView *loyaltyProgramGauge;

@property (weak, nonatomic) IBOutlet UILabel *lblTotalOrdered_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalOrdered_Data;
@property (weak, nonatomic) IBOutlet UILabel *lblPendingOrders_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblPendingOrders_Data;
@property (weak, nonatomic) IBOutlet UILabel *lblBtnWorkshop;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectWorkshop;
@property (weak, nonatomic) IBOutlet UIButton *btnWorkshop;
@property (weak, nonatomic) IBOutlet UIButton *viewBtn;

@property (weak, nonatomic) IBOutlet UILabel *lblFooter;

@property NSDictionary *responseData;
@property NSString *contractRef;
@end

@implementation LoyaltyProgramPerformanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[WebServiceManager sharedInstance] fetchLoyaltyProgramPerf:self];
    
    self.lblProgramPerformance.text = LOCALIZATION(C_LOYALTY_PROGRAMPERFORAMNCE);       //lokalise 7 Feb
    self.lblProgramPerformance.textColor = COLOUR_VERYDARKGREY;
    self.lblProgramPerformance.font = FONT_H1;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_LOYALTY_LOYALTYPROGRAM),LOCALIZATION_EN(C_LOYALTY_PROGRAMPERFORAMNCE)] screenClass:nil];
    [[WebServiceManager sharedInstance] fetchLoyaltyProgramPerf:self];
}

-(void) setupInterface {
    self.lblTotalContracts.text = [NSString stringWithFormat:@"%@",[_responseData objectForKey:@"TextTotalContractInProgress"]];
    self.lblTotalContracts.textColor = COLOUR_VERYDARKGREY;
    self.lblTotalContracts.font = FONT_B1;
    
    self.lblTotalContract_Data.text = [NSString stringWithFormat:@"%@",[_responseData objectForKey:@"TotalContractInProgress"]];
    self.lblTotalContract_Data.font = FONT_H(18);
    self.lblTotalContract_Data.textColor = COLOUR_RED;
    
    [self.view setNeedsDisplay];
    NSDictionary *viewData = @{
//                               @"TextTotalCarton" : @"Total Carton",
                               @"TextTotalCarton" : [_responseData objectForKey:@"TextTotalCartons"] == nil  && [[_responseData objectForKey:@"TextTotalCartons"] isKindOfClass:[NSNull class]]? @"" : [_responseData objectForKey:@"TextTotalCartons"],
//                               @"TextPremiumTier" : @"Premium Tier",
                               @"TextPremiumTier" :  [_responseData objectForKey:@"TextPremiumTier"] == nil && [[_responseData objectForKey:@"TextPremiumTier"] isKindOfClass:[NSNull class]]? @"" : [_responseData objectForKey:@"TextPremiumTier"],
                               @"ColorCodeDefaultOverall" : [_responseData objectForKey:@"ColorCodeDefaultOverall"],
                               @"ColorCodeDefaultPremium" : [_responseData objectForKey:@"ColorCodeDefaultPremium"],
                               @"NoOfOverallOrdered" : [_responseData objectForKey:@"TotalOrdered"],
                               @"NoOfPremiumOrdered" : [_responseData objectForKey:@"TotalPremiumAcheived"],
                               @"TotalPremiumTarget" : [_responseData objectForKey:@"TotalPremiumTarget"],
                               @"OverallTarget" : [_responseData objectForKey:@"TotalTargetQty"],
                               @"ColorCodeOverall" : [_responseData objectForKey:@"ColorCodeOverall"],
                               @"ColorCodePremium" : [_responseData objectForKey:@"ColorCodePremium"],
                               };
    _loyaltyProgramGauge.isAgri = false;
    [_loyaltyProgramGauge loadBar: viewData isCarousel:NO];
    
    self.lblTotalOrdered_Title.text = [_responseData objectForKey:@"TextTotalOrdered"];
    self.lblTotalOrdered_Title.textColor = COLOUR_RED;
    self.lblTotalOrdered_Title.font = FONT_H1;
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%0.1f ",[[_responseData objectForKey:@"TotalOrdered"] floatValue]] attributes:@{
                                                                                                                                                                                                          NSForegroundColorAttributeName: COLOUR_RED,                                                                 NSFontAttributeName : FONT_H(18),
                                                                                                                                                                                                   }];
    [attrString appendAttributedString: [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(C_LOYALTY_CARTONS) attributes:@{
                                                                                                                                       NSForegroundColorAttributeName: COLOUR_RED,
                                                                                                              NSFontAttributeName : FONT_B3,
                                                                                                                                }]];        //lokalised
    self.lblTotalOrdered_Data.attributedText = attrString;
    
    self.lblPendingOrders_Title.text = [NSString stringWithFormat:@"%@*",[_responseData objectForKey:@"TextPendingOrders"]];
    self.lblPendingOrders_Title.font = FONT_B2;
    self.lblPendingOrders_Title.textColor = COLOUR_VERYDARKGREY;
    
    NSMutableAttributedString *textPendingOrders = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%.1f ",[[_responseData objectForKey:@"TotalPending"] floatValue]] attributes:@{
                                                                                                                                                                                                         NSFontAttributeName : FONT_H1 ,
                                                                                                                                                                                                         }];
    [textPendingOrders appendAttributedString: [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(C_LOYALTY_CARTONS) attributes:@{
                                                                                                                                       NSFontAttributeName : FONT_B3,
                                                                                                                                       }]];     //lokalised
    self.lblPendingOrders_Data.attributedText = textPendingOrders;
    self.lblPendingOrders_Data.textColor = COLOUR_VERYDARKGREY;
    
    self.lblFooter.text = [NSString stringWithFormat:@"%@",[_responseData objectForKey:@"TextPerfDescription"] ];
    self.lblFooter.textColor = COLOUR_VERYDARKGREY;
    self.lblFooter.font = FONT_B3;
    
    self.lblSelectWorkshop.text = LOCALIZATION(C_LOYALTY_WORKSHOPBREAKDOWN);            //lokalise 8 Feb
    self.lblSelectWorkshop.textColor = COLOUR_VERYDARKGREY;
    self.lblSelectWorkshop.font = FONT_H1;
    
    [self.viewBtn setTitle:LOCALIZATION(C_LOYALTY_VIEW) forState:UIControlStateNormal]; //lokalise 8 Feb
    self.viewBtn.titleLabel.font = FONT_BUTTON;
    [self.viewBtn setBackgroundColor:COLOUR_RED];
    [self.viewBtn setUserInteractionEnabled:NO];
    
    //[self.btnWorkshop setTitle:LOCALIZATION(@"Select Workshop") forState:UIControlStateNormal];
    self.lblBtnWorkshop.text = LOCALIZATION(C_LOYALTY_SELECTWORKSHOP);                  //lokalise 8 Feb
    self.lblBtnWorkshop.font = FONT_B1;
    self.lblBtnWorkshop.textColor = COLOUR_VERYDARKGREY;
    
    
}

#pragma mark - WebService
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_LOYALTY_LOADDSRPERFORMANCE:
        {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                _responseData = [response getGenericResponse];
                [self setupInterface];
            }
        }
            break;
            
        case kWEBSERVICE_LOYALTY_WORKSHOPPERFORMANCE:
        {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                [mSession pushLoyaltyBreakdown:[response getGenericResponse] withWorkshopList:[_responseData objectForKey:@"WorkshopContractList"] andContract:_contractRef vc:self.parentViewController];
            }
        }
            break;
        default:
            break;
    }
}
- (IBAction)workshopSelectedAction:(UIButton *)sender {
    self.isPositionRequired = YES;
    [self popUpListViewWithTitle: LOCALIZATION(C_RU_BREAKDOWN_CHOOSEWORKSHOPS) listArray:[self getWorkshopNames] withSearchField: YES];     //lokalised
}

- (IBAction)viewBtnPressed:(id)sender {
    SET_LOYALTYTYPE(@{@"PerformanceCallType" : @"D"});
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[WebServiceManager sharedInstance]fetchLoyaltyPerformaceForContract:@{
                                                @"ContractReference": _contractRef,
                                                @"CallType" : @"D",
                                            }   vc:self];
}

-(NSArray *)getWorkshopNames {
    NSArray *workshopList = [_responseData objectForKey:@"WorkshopContractList"];
    if(workshopList == nil || [workshopList isKindOfClass:[NSNull class]]){
        return @[];
    }
    NSMutableArray *workshopNames = [[NSMutableArray alloc]init];
    for(NSDictionary *dict in workshopList) {
        [workshopNames addObject: [dict objectForKey:@"CompanyName"]];
    }
    return workshopNames;
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        NSDictionary *dict = [selection objectAtIndex:0];
        self.lblBtnWorkshop.text = [dict objectForKey:@"Name"];
        
        NSIndexPath *indexPath = [dict objectForKey:@"IndexPath"];
        _contractRef = [[[_responseData objectForKey:@"WorkshopContractList"] objectAtIndex:indexPath.row] objectForKey:@"ContractReference"];
        
        [self.viewBtn setUserInteractionEnabled:YES];
    }
}

@end
