//
//  LoyaltyBreakdownView.h
//  Shell
//
//  Created by Nach on 18/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoyaltyBreakdownView : UIView

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *barView;
@property (weak, nonatomic) IBOutlet UIView *graphView;
@property (weak, nonatomic) IBOutlet UIView *legendsFooter;
@property (weak, nonatomic) IBOutlet UIView *legendsSideView;
@property (weak, nonatomic) IBOutlet UIView *recommendedView;

@property (weak, nonatomic) IBOutlet UILabel *lblLegendsSideView_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblLegendSideView_ColorCode_One;
@property (weak, nonatomic) IBOutlet UILabel *lblLegendSideView_Legend_One;
@property (weak, nonatomic) IBOutlet UILabel *lblLegendsSideView_ColorCode_Two;
@property (weak, nonatomic) IBOutlet UILabel *lblLegendSideView_Legend_Two;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *legendsSideViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *legendsSideViewHeight;

@property (weak, nonatomic) IBOutlet UILabel *lblLegendsFooter_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblLegendFooter_ColorCode_One;
@property (weak, nonatomic) IBOutlet UILabel *lblLegendFooter_Legend_One;
@property (weak, nonatomic) IBOutlet UILabel *lblLegendsFooter_ColorCode_Two;
@property (weak, nonatomic) IBOutlet UILabel *lblLegendFooter_Legend_Two;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *legendFooterHeight;

@property (weak, nonatomic) IBOutlet UILabel *lblRecommendedView_Title;
@property (weak, nonatomic) IBOutlet UIView *lbRecommendedView_ColorCode_One;
@property (weak, nonatomic) IBOutlet UILabel *lbRecommendedView_Legend_One;
@property (weak, nonatomic) IBOutlet UIView *lblRecommendedView_ColorCode_Two;
@property (weak, nonatomic) IBOutlet UILabel *lbRecommendedView_Legend_Two;
@property (weak, nonatomic) IBOutlet UIView *lblRecommendedView_ColorCode_Three;
@property (weak, nonatomic) IBOutlet UILabel *lbRecommendedView_Legend_Three;

@property (weak, nonatomic) IBOutlet UIView *barBottomLine;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *graphViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *graphViewHeight;

@property (weak, nonatomic) IBOutlet UIScrollView *graphView_Scroll;

@property NSDictionary *viewData;
@property BOOL isCarousel;
@property double scaleFactor;
@property NSDictionary *breakdownMonthList;

@property NSMutableArray *labelsAdded;

@property int currentEndMonth_Bar;
@property int swipeCount_Bar;

@property BOOL isPremiumAvailable;

-(void) loadBar:(NSDictionary *) responseData isCarousel:(BOOL)isCarousel;

@end

NS_ASSUME_NONNULL_END
