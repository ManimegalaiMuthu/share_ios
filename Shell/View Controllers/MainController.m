//
//  ViewController.m
//  Syngenta
//
//  Created by Will Choy on 1/10/15.
//  Copyright © 2015 WillChoy. All rights reserved.
//

#import "MainController.h"

#import "RDVTabBarController.h"
#import "RDVTabBarItem.h"
#import "AppDelegate.h"

#import "LocalizationManager.h"

#import "PromotionsViewController.h"

@interface MainController () <RDVTabBarControllerDelegate>
{
    RDVTabBarController *tabBarController;
    
    UIViewController *homeNavigationController;
}
@end

@implementation MainController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self setupViewControllers];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupViewControllers {
    NSMutableArray *navControlArray = [[NSMutableArray alloc] init];
    
    
    homeNavigationController = [[UINavigationController alloc] initWithRootViewController: [self.storyboard instantiateViewControllerWithIdentifier: VIEW_PROMOTION]];
    [navControlArray addObject: homeNavigationController];
    
    tabBarController = [[RDVTabBarController alloc] init];
    [tabBarController setViewControllers: navControlArray];
    
    tabBarController.delegate = self;
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:tabBarController];
    
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(navigateToIndex:)
//                                                 name:kNavigation
//                                               object:nil];
    
//    [self.navigationController pushViewController:tabBarController animated:YES];
//    self.viewController = tabBarController;
    //
    [self customizeTabBarForController:tabBarController];
}

//- (void)navigateToIndex:(NSNotification *)notification {
//    [tabBarController setSelectedIndex:index];
//}

- (void)customizeTabBarForController:(RDVTabBarController *)tabBarController {
//    UIImage *finishedImage = [UIImage imageNamed:@"tab_highlight"];
//    UIImage *unfinishedImage = [UIImage imageNamed:@"tab_normal"];
    
//    NSArray *tabBarItemImages = @[@"home", @"code", @"starconversion", @"report"];
//    NSArray *tabBarTitle = @[LOCALIZATION(C_MENU_HOME),LOCALIZATION(C_MENU_SCAN), LOCALIZATION(C_MENU_STAR_CONVERSION),LOCALIZATION(C_MENU_REPORT)];
    
//    NSInteger index = 0;
    for (RDVTabBarItem *item in [[tabBarController tabBar] items]) {
        [item setTitle: @"title"];
        [item setBackgroundColor: COLOUR_RED];
//        [item setBackgroundSelectedImage:finishedImage withUnselectedImage:unfinishedImage];
//        [item setFinishedSelectedImage:selectedimage withFinishedUnselectedImage:selectedimage];
//        [item setBackgroundSelectedImage:<#(UIImage *)#> withUnselectedImage:<#(UIImage *)#>];
//        item.selectedTitleAttributes = @{NSFontAttributeName: [UIFont systemFontOfSize:12],
//                                         NSForegroundColorAttributeName: [UIColor whiteColor],};
//        item.unselectedTitleAttributes = @{NSFontAttributeName: [UIFont systemFontOfSize:12],
//                                           NSForegroundColorAttributeName: [UIColor whiteColor],};
        
//        index++;
    }
}


#pragma mark RDVTabBarControllerDelegate
- (void)tabBarController:(RDVTabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    
//    if(viewController == homeNavigationController) {
//        [homeViewController reloadPage];
//    }
//    if(viewController == productListNavigationController) {
//        [productListController reloadPage];
//    }
//    if(viewController == reportNavigationController) {
//        [reportController reloadPage];
//    }
}

- (BOOL)tabBarController:(RDVTabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    return YES;
}


@end
