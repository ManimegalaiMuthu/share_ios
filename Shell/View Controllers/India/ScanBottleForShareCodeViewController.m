//
//  ScanBottleForShareCodeViewController.m
//  Shell
//
//  Created by Nach on 12/11/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "ScanBottleForShareCodeViewController.h"
#import "PopupValueEntryViewController.h"
#import "ScanProductTableViewCell.h"
#import "QRCodeScanner.h"

#import <MapKit/MapKit.h>

@interface ScanBottleForShareCodeViewController () <QRCodeScannerDelegate,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,PopupValueEntryDelegate,WebServiceManagerDelegate>

@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblShareCodeViewHeader;
@property (weak, nonatomic) IBOutlet UITextField *textShareCode;
@property (weak, nonatomic) IBOutlet UIButton *btnScanShareCode;
@property (weak, nonatomic) IBOutlet UIButton *btnNotRegistered;
@property (weak, nonatomic) IBOutlet UILabel *lblBottleCodes;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectOption;
@property (weak, nonatomic) IBOutlet UIButton *btnEnterProductCode;
@property (weak, nonatomic) IBOutlet UIButton *btnScanProductCode;
@property (weak, nonatomic) IBOutlet UILabel *lblNo;
@property (weak, nonatomic) IBOutlet UILabel *lblScannedCode;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblDelete;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@property (weak, nonatomic) IBOutlet UIView *offlineScanView;
@property (weak, nonatomic) IBOutlet UILabel *lblOfflineText;
@property (weak, nonatomic) IBOutlet UIButton *btnOfflineYes;
@property (weak, nonatomic) IBOutlet UIButton *btnOfflineNo;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property BOOL isShareCode;

@property CLLocationManager *locationManager;
@property CGPoint currentLocation;
@property BOOL didFindLocation;

@property NSMutableArray *arrayOfScanCode;
@property NSString *currentCodeString;

@end

@implementation ScanBottleForShareCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_QL_SCANBOTTLE) subtitle:@""];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    
    self.arrayOfScanCode = [[NSMutableArray alloc] init];
    self.isShareCode = NO;
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / Workshop",LOCALIZATION_EN(C_QL_SCANBOTTLE)] screenClass:nil];
    
    self.lblHeader.text = LOCALIZATION(C_RU_BREAKDOWN_DETAILS);
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    self.lblHeader.font = FONT_H1;
    
    self.lblShareCodeViewHeader.text = LOCALIZATION(C_SCANBOTTLE_SHARECODE);
    self.lblShareCodeViewHeader.textColor = COLOUR_VERYDARKGREY;
    self.lblShareCodeViewHeader.font = FONT_B2;
    
    self.textShareCode.placeholder = LOCALIZATION(C_SCANBOTTLE_SHARECODE_PLACEHOLDER);
    self.textShareCode.textColor = COLOUR_VERYDARKGREY;
    self.textShareCode.font = FONT_B2;
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [self.textShareCode setLeftViewMode:UITextFieldViewModeAlways];
    [self.textShareCode setLeftView:spacerView];
    
    [Helper setButtonFontWithAttributedString:self.btnNotRegistered font:FONT_B2 colour:COLOUR_VERYDARKGREY underline:YES];
    
    self.lblBottleCodes.text = LOCALIZATION(C_SCANBOTTLE_BOTTLECODES_HEADER);
    self.lblBottleCodes.textColor = COLOUR_VERYDARKGREY;
    self.lblBottleCodes.font = FONT_H1;
    
    self.lblSelectOption.text = LOCALIZATION(C_SCANCODE_SELECTFOLLOWING);
    self.lblSelectOption.textColor = COLOUR_VERYDARKGREY;
    self.lblSelectOption.font = FONT_B2;
    
    [self.btnScanProductCode.titleLabel setFont: FONT_BUTTON];
    [self.btnScanProductCode setBackgroundColor: COLOUR_YELLOW];
    [self.btnScanProductCode setTitle:LOCALIZATION(C_FORM_SCAN) forState:UIControlStateNormal];
    [self.btnScanProductCode setTitleColor: COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    
    [self.btnEnterProductCode.titleLabel setFont: FONT_BUTTON];
    [self.btnEnterProductCode setBackgroundColor: COLOUR_YELLOW];
    [self.btnEnterProductCode setTitle:LOCALIZATION(C_FORM_MANUAL) forState:UIControlStateNormal];
    [self.btnEnterProductCode setTitleColor: COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    
    self.lblNo.font = FONT_H1;
    self.lblNo.textColor = COLOUR_VERYDARKGREY;
    self.lblNo.text = LOCALIZATION(C_TABLE_NO);
    
    self.lblScannedCode.font = self.lblNo.font;
    self.lblScannedCode.textColor = self.lblNo.textColor;
    self.lblScannedCode.text= LOCALIZATION(C_TABLE_SCANNEDCODE);
    if ([GET_LOCALIZATION isEqualToString: kChinese])
        self.lblScannedCode.textAlignment = NSTextAlignmentCenter;
    
    self.lblStatus.font = self.lblNo.font;
    self.lblStatus.textColor = self.lblNo.textColor;
    self.lblStatus.text = LOCALIZATION(C_STATUS);
    
    self.lblDelete.font = self.lblNo.font;
    self.lblDelete.textColor = self.lblNo.textColor;
    self.lblDelete.text = LOCALIZATION(C_DELETE);
    
    self.btnSubmit.titleLabel.font = FONT_BUTTON;
    [self.btnSubmit setTitle:LOCALIZATION(C_FORM_SUBMIT) forState:UIControlStateNormal];
    [self.btnSubmit setBackgroundColor:COLOUR_RED];
    
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:[UINib nibWithNibName:@"ScanProductTableViewCell" bundle:nil] forCellReuseIdentifier:@"ScanProductTableViewCell"];
    mQRCodeScanner.delegate = self;
   
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager requestWhenInUseAuthorization];
    
    if (GET_SCANNEDCODES_IN) {
        NSArray *savedCodes = GET_SCANNEDCODES_IN;
        for(NSDictionary *dict in savedCodes) {
            if(![[dict objectForKey:@"Isconsumer"] boolValue]){
                self.offlineScanView.hidden = NO;
                self.scrollView.hidden = YES;
                self.btnSubmit.hidden = YES;
                break;
            }
        }
    } else {
        self.offlineScanView.hidden = YES;
        self.scrollView.hidden = NO;
        self.btnSubmit.hidden = NO;
    }
    
    self.lblOfflineText.font = FONT_B2;
    self.lblOfflineText.text = LOCALIZATION(C_FORM_OFFLINESCAN);
    
    self.btnOfflineYes.titleLabel.font = FONT_BUTTON;
    [self.btnOfflineYes setTitle:LOCALIZATION(C_FORM_YES) forState:UIControlStateNormal];
    self.btnOfflineNo.titleLabel.font = FONT_BUTTON;
    [self.btnOfflineNo setBackgroundColor: COLOUR_YELLOW];
    [self.btnOfflineNo setTitle:LOCALIZATION(C_FORM_NO) forState:UIControlStateNormal];
    [self.btnOfflineYes setBackgroundColor:COLOUR_RED];
    
}

- (IBAction)backPressed:(UIButton *)sender {
    [self popSelf];
}

- (IBAction)shareCodeScanPressed:(UIButton *)sender {
    if([self.textShareCode isFirstResponder]) {
        [self.textShareCode resignFirstResponder];
    }
    self.isShareCode = YES;
    [mQRCodeScanner showQRCodeScannerOnVC:self withMessage:LOCALIZATION(C_SCANBOTTLE_QR_MESSAGE)];
}

- (IBAction)notRegisteredPressed:(UIButton *)sender {
    [mSession loadAddNewNetwork];
}

- (IBAction)scanProductCodePressed:(UIButton *)sender {
    if([self.textShareCode isFirstResponder]) {
           [self.textShareCode resignFirstResponder];
    }
    self.isShareCode = NO;
    [mQRCodeScanner showQRCodeScannerOnVC:self withMessage:LOCALIZATION(C_REGISTEROILCHANGE_SUBTITLE)];
}

- (IBAction)manualProductCodePressed:(UIButton *)sender {
    [self popupValueEntryVCWithTitle:LOCALIZATION(C_FORM_ENTERCODETITLE) submitTitle:LOCALIZATION(C_FORM_ADDCODE) cancelTitle:LOCALIZATION(C_FORM_CANCEL) placeHolder:LOCALIZATION(C_FORM_ENTERCODETITLE)];
}

- (IBAction)submitPressed:(UIButton *)sender {
    if ([self.arrayOfScanCode count] == 0)
    {
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_ERROR)];
        return;
    }
    NSMutableDictionary *submitCodeDict = [[NSMutableDictionary alloc] init];
    
    [submitCodeDict setObject:@[@{
                @"Coupons"          : self.arrayOfScanCode      ,
                @"VehicleNumber"    : @""                       ,
                @"StateID"          : @""                       ,
                }]
    forKey:@"VehicleListForSellout"];
    [submitCodeDict setObject:@(NO) forKey:@"Isconsumer"];
    [submitCodeDict setObject:self.textShareCode.text forKey:@"Sharecode"];
    
    if ([mSession getReachability]) {
        [[WebServiceManager sharedInstance] submitCodesForSellout:submitCodeDict vc:self];
    } else {
        //save
        NSMutableArray *pendingDict = [[NSMutableArray alloc] initWithArray: GET_SCANNEDCODES_IN copyItems:YES];
        
        if (!pendingDict)
            pendingDict = [[NSMutableArray alloc] init];
        
        for (NSMutableDictionary *dict in self.arrayOfScanCode)
        {
            [dict setObject: self.textShareCode.text forKey: @"SHARECode"];
        }
        
        [pendingDict addObject: @{ @"SHARECode"         : self.textShareCode.text  ,
                                   @"Coupons"           : self.arrayOfScanCode     ,
                                   @"Date"              : [self getTodayDate]      ,
                                   @"VehicleNumber"     : @""                      ,
                                   @"Isconsumer"        : @(NO)                    ,
                                   }];
        
        SET_SCANNEDCODES_IN(pendingDict);
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.arrayOfScanCode removeAllObjects];
        self.textShareCode.text = @"";
    
        [self.tableView reloadData];
        
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_ALERT_SAVEDCODE)];
    }
        
}

- (IBAction)offlineNoPressed:(id)sender {
    self.offlineScanView.hidden = YES;
    self.scrollView.hidden = NO;
    self.btnSubmit.hidden = NO;
}

- (IBAction)offlineYesPressed:(id)sender {
    self.offlineScanView.hidden = YES;
    self.scrollView.hidden = NO;
    self.btnSubmit.hidden = NO;
    //push to pending code view
    [mSession pushPendingCodeViewForScanBottle:YES forConsumer:NO onVc:self];
}

#pragma mark - QRCode Scanner
- (void)qrCodeScanSuccess:(NSString *)scanString {
    
    NSString *scannedValue = [[scanString componentsSeparatedByString:@"="] lastObject];
    
    if(self.isShareCode) {
        self.textShareCode.text = scannedValue;
    } else {
        if(![[mSession profileInfo] hasMultipleSubmissionOilChange]) {
           for (NSDictionary *dict in self.arrayOfScanCode)
           {
               if ([scannedValue isEqualToString: [dict objectForKey: @"Code"]])
               {
                   [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_EXIST)];         //lokalised
                   return;
               }
           }
        }
        
        if(scannedValue.length > 0){
            self.currentCodeString = scannedValue;
            [self updatelocation];
        }
    }
}

#pragma mark - Location
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    if (self.didFindLocation)
        return;
    
    //most recent location update is at the end of the array.
    CLLocation *location = [locations lastObject];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:6];
    
    self.currentLocation = CGPointMake(location.coordinate.latitude, location.coordinate.longitude);
    
    self.didFindLocation = YES;
    [self.locationManager stopUpdatingLocation];
    
    if(![[mSession profileInfo]hasMultipleSubmissionOilChange])
    {
        for(NSDictionary *duplicate in self.arrayOfScanCode)
        {
            if([[duplicate objectForKey:@"Code"] isEqualToString:self.currentCodeString]){
                [UpdateHUD removeMBProgress: self];
                return;
            }
        }
    }
    
    NSMutableDictionary *codeDict = [NSMutableDictionary dictionaryWithDictionary:
                                     @{@"Code"          :self.currentCodeString,
                                       @"Date"          : [self getTodayDate],
                                       @"Latitude"      : @(self.currentLocation.x),
                                       @"Longitude"     : @(self.currentLocation.y),
                                       }];
    
    [self.arrayOfScanCode addObject: codeDict];
    self.currentLocation = CGPointMake(0, 0);
    
    [self.tableView reloadData];
    
    [UpdateHUD removeMBProgress: self];
}

-(void) updatelocation {
    [UpdateHUD addMBProgress: self.view withText: @""];
    self.didFindLocation = NO;
    switch ([CLLocationManager authorizationStatus])
    {
        case kCLAuthorizationStatusAuthorizedAlways:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.locationManager startUpdatingLocation];
            break;
        default:
        {
            //send 0,0
            self.currentLocation = CGPointMake(0, 0);
            
            NSMutableDictionary *codeDict = [NSMutableDictionary dictionaryWithDictionary:
                                             @{@"Code"          :self.currentCodeString,
                                               @"Date"          : [self getTodayDate],
                                               @"Latitude"      : @(self.currentLocation.x),
                                               @"Longitude"     : @(self.currentLocation.y),
                                               }];
            
            [self.arrayOfScanCode addObject: codeDict];
            [UpdateHUD removeMBProgress: self];
            [self.tableView reloadData];
        }
            break;
    }
    
}

#pragma mark - Date
-(NSString *)getTodayDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    return [formatter stringFromDate: [NSDate date]];
}

#pragma mark - PopupValueEntryDelegate
-(void) valueSubmitted:(NSString *)value {
    
    if(![[mSession profileInfo] hasMultipleSubmissionOilChange]) {
       for (NSDictionary *dict in self.arrayOfScanCode)
       {
           if ([value isEqualToString: [dict objectForKey: @"Code"]])
           {
               [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_EXIST)];         //lokalised
               return;
           }
       }
    }
       
    self.currentCodeString = value;
    [self updatelocation];
}

#pragma mark - TableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayOfScanCode.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ScanProductTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ScanProductTableViewCell"];
    NSDictionary *codeStringDict = [self.arrayOfScanCode objectAtIndex:indexPath.row];
    
    [cell setID: @(indexPath.row + 1).stringValue];
    [cell setScannedCode: [codeStringDict objectForKey: @"Code"]];
    [cell setPendingStatus: LOCALIZATION(C_SCANCODE_STATUS_PENDING)];
    
    cell.btnDelete.tag = indexPath.row;
    cell.btnDelete.backgroundColor = COLOUR_RED;
    [cell.btnDelete setTitle: LOCALIZATION(C_DELETE) forState: UIControlStateNormal];
    [cell.btnDelete addTarget: self action: @selector(deletePressed:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(void)deletePressed:(UIButton*)sender
{
    [self.arrayOfScanCode removeObjectAtIndex:sender.tag];
    [self.tableView reloadData];
}

#pragma mark - Webservice Manager
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_SUBMITCODE_SELLOUT: {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                [mSession pushScanBottleResult:[response getGenericResponse] isScanBottle:YES isConsumer:NO vc:self];
                            
                [self.arrayOfScanCode removeAllObjects];
                self.textShareCode.text = @"";
               
                [self.tableView reloadData];
            }
        }
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
