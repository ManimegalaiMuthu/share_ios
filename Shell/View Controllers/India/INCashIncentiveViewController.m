//
//  INCashIncentiveViewController.m
//  Shell
//
//  Created by Ben on 21/5/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INCashIncentiveViewController.h"
#import "PopupInfoTableViewController.h"
#import "PopupViewController.h"
#import "CASMonthYearPicker.h"
#import "ArcView.h"

@implementation CIHeaderCell

@synthesize currentEarningLabel;
@synthesize currentEarningValue;

@synthesize totalEarningLabel;
@synthesize totalEarningValue;

@synthesize totalRemittedLabel;
@synthesize totalRemittedValue;

@synthesize disclaimerFirstLabel;
@synthesize disclaimerSecondLabel;
@synthesize disclaimerUpdateLabel;

@synthesize arcView;

-(void)prepareForReuse {
    [super prepareForReuse];
    
    arcView.datasource = nil;
    arcView.delegate = nil;
    // Then Reset here back to default values that you want.
}

@end


@implementation CIStatusCell

@synthesize infoButton;
@synthesize statusLabel;

@end


@implementation CITransactionCell

@synthesize transactionLabel;

@end


@implementation CIDateRangeCell

@synthesize startDateButton;
@synthesize toJoiningLabel;
@synthesize endDateButton;
@synthesize enterButton;

@end


@implementation CIDetailsCell

@synthesize icon;
@synthesize dateLabel;
@synthesize titleLabel;
@synthesize remarksLabel;
@synthesize valueLabel;

@end


@interface INCashIncentiveViewController () <WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource, PopupListDelegate, MonthYearPickerDelegate, ArcViewDatasource, ArcViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *mainTableView;

@end

@implementation INCashIncentiveViewController

bool isSelectingStatus = false;
bool isSelectingStartDate = false;
NSString *currencyString;

int arcMin = 0;
int arcMax = 250;

NSUInteger balancePoints;
NSUInteger earnedPoints;
NSUInteger redeemedPoints;

CASMonthYearPicker *startDatePicker;
CASMonthYearPicker *endDatePicker;
NSDate *startDate;
NSDate *endDate;
NSDateFormatter * dateFormatter;

NSString *status;
NSString *serviceType;
NSString *updatedDate;

NSMutableArray* resultArray;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_CASHINCENTIVE) subtitle: @""];
    currencyString = [[[[mSession lookupTable] objectForKey:@"LanguageList"]objectAtIndex:0]objectForKey:@"Currency"];
    
    balancePoints = 0;
    earnedPoints = 0;
    redeemedPoints = 0;
    
    status = LOCALIZATION(C_FILTER_BY_STATUS);
    serviceType = LOCALIZATION(C_FILTER_BY_TRANSACTION);
    
    //setup default date
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    //setup the necesary date adjustments
    NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
    [selectedComponent setDay: 1];
    NSDate *selectedDate = [gregorian dateFromComponents: selectedComponent];
    
    [selectedComponent setDay: 0];
    [selectedComponent setMonth: -2];
    [selectedComponent setYear: 0];
    selectedDate = [gregorian dateByAddingComponents: selectedComponent toDate: selectedDate options:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)];
    
    startDate = selectedDate;
    endDate = [NSDate new];
    
    resultArray = [NSMutableArray new];
    
    startDatePicker = [[CASMonthYearPicker alloc] initWithTitle:LOCALIZATION(C_ORDER_STARTDATE) previouslySelected:startDate withYearHidden:false];
    startDatePicker.delegate = self;
    endDatePicker = [[CASMonthYearPicker alloc] initWithTitle:LOCALIZATION(C_ORDER_ENDDATE) previouslySelected:endDate withYearHidden:false];
    endDatePicker.delegate = self;
    
    dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"MMM YYYY";
    [[WebServiceManager sharedInstance] fetchCashHistoryFromDate:@"" toDate:@"" forStatus:@"" forServiceType:@"" vc:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_CASHINCENTIVE) screenClass:nil];
}

-(void)statusDidTap {
    [self popUpListViewWithTitle:LOCALIZATION(C_FILTER_BY_STATUS) listArray:[mSession getCashStatus] withSearchField:false];
    isSelectingStatus = true;
}
-(IBAction)infoButtonDidPress {
    PopupInfoTableViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPINFOTABLEVIEW];
    [self popUpViewWithPopupVC:popupVc];
    [popupVc initCashIncentiveLegend];
}

-(void)transactionDidTap {
    [self popUpListViewWithTitle:LOCALIZATION(C_FILTER_BY_TRANSACTION) listArray:[mSession getCashServiceType] withSearchField:false];
    isSelectingStatus = false;
}

-(IBAction)startDateDidPress {
    
    isSelectingStartDate = true;
    
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: startDatePicker];
    startDatePicker.translatesAutoresizingMaskIntoConstraints = false;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[startDatePicker]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (startDatePicker)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[startDatePicker]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (startDatePicker)]];
}
-(IBAction)endDateDidPress {
    
    isSelectingStartDate = false;
    
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: endDatePicker];
    endDatePicker.translatesAutoresizingMaskIntoConstraints = false;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[endDatePicker]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (endDatePicker)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[endDatePicker]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (endDatePicker)]];
}
-(IBAction)enterButtonDidPress {
    dateFormatter.dateFormat = @"yyyy/MM/dd";
    [[WebServiceManager sharedInstance] fetchCashHistoryFromDate:[dateFormatter stringFromDate:startDate]
                                                          toDate:[dateFormatter stringFromDate:endDate]
                                                       forStatus:[mSession convertToCashStatusKeyCode:status]
                                                  forServiceType:[mSession convertToCashServiceTypeKeyCode:serviceType]
                                                              vc:self];
}

//MARK: PopupView Delegate
- (void)dropDownSelection:(NSArray *)selection {
    
    if (isSelectingStatus) {
        // Status
        status = selection.firstObject;
        [self.mainTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:1], nil] withRowAnimation:UITableViewRowAnimationNone];
        [self enterButtonDidPress];
    } else {
        // Transaction
        serviceType = selection.firstObject;
        [self.mainTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:1 inSection:1], nil] withRowAnimation:UITableViewRowAnimationNone];
        [self enterButtonDidPress];
    }
}

//MARK: Month Year Picker Delegate
- (void)selectedMonthYear:(NSDate *)selection {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    
    //setup the necesary date adjustments
    NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: selection];
    [selectedComponent setDay: 1];
    selection = [gregorian dateFromComponents: selectedComponent];
    
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setMonth: 3];
    [offsetComponents setDay: -1];
    

    if (isSelectingStartDate) {
        //start date
        NSDate *offsetDate = [gregorian dateByAddingComponents:offsetComponents toDate: selection options:0];
        
        //check end date if more than current date
        if ([offsetDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
        {
            //check start date selection if more than current date.
            if ([selection timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
            {
                NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
                [selectedComponent setDay: 1];
                selection = [gregorian dateFromComponents: selectedComponent];
            }
            offsetDate = [NSDate date]; //set end date to current date
            startDate = selection;
            endDate = offsetDate;
        } else {
            startDate = selection;
            endDate = offsetDate;
        }
    } else {
        //end date
        //set date to end of selected month
        NSDateComponents *endOfMonthComponents = [[NSDateComponents alloc] init];
        [endOfMonthComponents setYear: 0];
        [endOfMonthComponents setMonth: 1];
        [endOfMonthComponents setDay: -1];
        selection = [gregorian dateByAddingComponents: endOfMonthComponents toDate: selection options: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)];
        
        //check date from startdate
        NSDate *offsetDate = [gregorian dateByAddingComponents:offsetComponents toDate: startDate options:0];
        
        //if offset date is more than current month, change offset to current month
        if ([offsetDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
        {
            //reset offset date to last day of current month
            NSDateComponents *currentDateComponents = [gregorian components: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
            [currentDateComponents setDay: 1];
            [currentDateComponents setMonth: [currentDateComponents month] + 1];
            offsetDate = [gregorian dateFromComponents: currentDateComponents];
            
            [currentDateComponents setDay: -1];
            [currentDateComponents setMonth: 0];
            [currentDateComponents setYear: 0];
            offsetDate = [gregorian dateByAddingComponents: currentDateComponents toDate: offsetDate options:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) ];
        }
        
        //if selection more than threshold (+2 months from start month)
        if ([selection timeIntervalSinceReferenceDate] > [offsetDate timeIntervalSinceReferenceDate])
        {
            selection = offsetDate;
        }
        else if ([selection timeIntervalSinceReferenceDate] < [startDate timeIntervalSinceReferenceDate])
        {
            //if selection less than start month
            selection = startDate;
        }
        else
        {
            
        }
        endDate = selection;
    }
    [self.mainTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:2 inSection:1], nil] withRowAnimation:UITableViewRowAnimationNone];
}

//MARK: Web Service Manager Delegate
- (void)processCompleted:(WebServiceResponse *)response {
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_CASHINCENTIVE:
        {
            NSDictionary *data = [response getGenericResponse];
            
            balancePoints = [data[@"CurrentEarning"] integerValue];
            earnedPoints = [data[@"TotalEarning"] integerValue];
            redeemedPoints = [data[@"TotalRemitted"] integerValue];
            
            updatedDate = data[@"AsofDate"];
            
            resultArray = data[@"TransactionHistory"];
            [self.mainTableView reloadData];
        }
            break;
        default:
            break;
    }
}

//MARK: Arc View Datasource
- (int)arcView:(ArcView *)arcView minValueFor:(int)view {
    return arcMin;
}
- (int)arcView:(ArcView *)arcView maxValueFor:(int)view {
    return arcMax;
}
-(int)arcView:(ArcView*) arcView progressValueFor:(int) view {
    return (int)balancePoints;
}
- (CGFloat)arcView:(ArcView *)arcView barWidthFor:(int)view {
    return 10;
}
- (NSAttributedString *)arcView:(ArcView *)arcView centerTextForArcView:(int)view {
    
    NSString *remitValueString = [[@"\n" stringByAppendingString: currencyString] stringByAppendingString:[NSString stringWithFormat:@"%d\n", arcMax-(int)balancePoints]];
    
    NSMutableAttributedString *returnString = [[NSMutableAttributedString alloc]initWithString:remitValueString
                                                                                    attributes:@{ NSFontAttributeName : FONT_H1 ,
                                                                                                  NSForegroundColorAttributeName : COLOUR_RED, }];
    [returnString appendAttributedString: [[NSAttributedString alloc]initWithString:LOCALIZATION(C_UNTIL_NEXT_REMIT)
                                                                         attributes:@{ NSFontAttributeName : FONT_B3 ,
                                                                                       NSForegroundColorAttributeName : COLOUR_RED, }]];
    
    return returnString;
}
- (ArcViewCursorType)arcView:(ArcView *)arcView cursorTypeFor:(int)view {
    return ArcViewCursorTypeLine;
}
- (int)arcView:(ArcView *)arcView numberOfMarkersFor:(int)view {
    return 2;
}
- (int)arcView:(ArcView *)arcView valueForMarkerAtIndex:(int)index {
    switch (index) {
        case 0:
            return 0;
        case 1:
            return 250;
        default:
            return 0;
    }
}
- (CGFloat)arcView:(ArcView *)arcView paddingForMarkerLabel:(int)view {
    return 15;
}
- (NSAttributedString *)arcView:(ArcView *)arcView outerTitleForMarkerAtIndex:(int)index {
    switch (index) {
        case 0:
            return [[NSAttributedString alloc]initWithString:@"0"];
        case 1:
            return [[NSAttributedString alloc]initWithString:@"250"];
        default:
            return [[NSAttributedString alloc]initWithString:@""];
    }
}
- (NSAttributedString *)arcView:(ArcView *)arcView innerTitleForMarkerAtIndex:(int)index {
    switch (index) {
        case 0:
            return [[NSAttributedString alloc]initWithString:@""];
        case 1:
            return [[NSAttributedString alloc]initWithString:@""];
        default:
            return [[NSAttributedString alloc]initWithString:@""];
    }
}

//MARK: Table View Datasource Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 1;
        case 1:
            return 3;
        case 2:
            return resultArray.count;
        default:
            return 0;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 0;
        case 1:
            return 0;
        case 2:
            return 50;
        default:
            return 0;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return [UIView new];
        case 1:
            return [UIView new];
        case 2: {
            UIView *returnView = [UIView new];
            
            UIView *containerView = [UIView new];
            containerView.translatesAutoresizingMaskIntoConstraints = false;
            containerView.backgroundColor = UIColor.whiteColor;
            
            UILabel *detailsLabel = [UILabel new];
            detailsLabel.translatesAutoresizingMaskIntoConstraints = false;
            detailsLabel.text = LOCALIZATION(C_MANAGEWORKSHOP_IN_DETAIL_TITLE);
            detailsLabel.font = [UIFont boldSystemFontOfSize:17];
            detailsLabel.font = FONT_H1;
            
            UILabel *dateLabel = [UILabel new];
            dateLabel.translatesAutoresizingMaskIntoConstraints = false;
            dateLabel.text = @"(MM/DD/YY)";
            dateLabel.font = [UIFont systemFontOfSize:14];
            dateLabel.font = FONT_B3;
            
            UILabel *currencyLabel = [UILabel new];
            currencyLabel.translatesAutoresizingMaskIntoConstraints = false;
            currencyLabel.text = LOCALIZATION(C_RUPEE);
            currencyLabel.font = [UIFont boldSystemFontOfSize:17];
            currencyLabel.font = FONT_H1;
            
            [detailsLabel setContentHuggingPriority:UILayoutPriorityDefaultHigh  forAxis:UILayoutConstraintAxisHorizontal];
            [currencyLabel setContentHuggingPriority:UILayoutPriorityDefaultHigh  forAxis:UILayoutConstraintAxisHorizontal];
            
            [returnView addSubview:containerView];
            [containerView addSubview:detailsLabel];
            [containerView addSubview:dateLabel];
            [containerView addSubview:currencyLabel];
            
            NSLayoutConstraint *topToContainer = [[containerView topAnchor]constraintEqualToAnchor:[returnView topAnchor] constant:0];
            NSLayoutConstraint *bottomToContainer = [[containerView bottomAnchor]constraintEqualToAnchor:[returnView bottomAnchor] constant:0];
            NSLayoutConstraint *leadingToContainer = [[containerView leadingAnchor]constraintEqualToAnchor:[returnView leadingAnchor] constant:15];
            NSLayoutConstraint *trailingToContainer = [[returnView trailingAnchor]constraintEqualToAnchor:[containerView trailingAnchor] constant:15];
            
            NSLayoutConstraint *leadingToDetails = [[detailsLabel leadingAnchor]constraintEqualToAnchor:[containerView leadingAnchor] constant:15];
            NSLayoutConstraint *detailsToDate = [[dateLabel leadingAnchor]constraintEqualToAnchor:[detailsLabel trailingAnchor] constant:10];
            NSLayoutConstraint *dateToCurrency = [[currencyLabel leadingAnchor]constraintEqualToAnchor:[dateLabel trailingAnchor] constant:10];
            NSLayoutConstraint *currencyToTrailing = [[containerView trailingAnchor]constraintEqualToAnchor:[currencyLabel trailingAnchor] constant:15];
            
            NSLayoutConstraint *detailsCenterY = [[detailsLabel centerYAnchor]constraintEqualToAnchor:[containerView centerYAnchor] constant:0];
            NSLayoutConstraint *dateCenterY = [[dateLabel centerYAnchor]constraintEqualToAnchor:[containerView centerYAnchor] constant:0];
            NSLayoutConstraint *currencyCenterY = [[currencyLabel centerYAnchor]constraintEqualToAnchor:[containerView centerYAnchor] constant:0];
            
            NSLayoutConstraint *topToDetails = [[detailsLabel topAnchor]constraintEqualToAnchor:[containerView topAnchor] constant:0];
            NSLayoutConstraint *topToDate = [[dateLabel topAnchor]constraintEqualToAnchor:[containerView topAnchor] constant:0];
            NSLayoutConstraint *topToCurrency = [[currencyLabel topAnchor]constraintEqualToAnchor:[containerView topAnchor] constant:0];
            
            [NSLayoutConstraint activateConstraints:[NSArray arrayWithObjects:leadingToDetails, detailsToDate, dateToCurrency, currencyToTrailing, detailsCenterY, dateCenterY, currencyCenterY, topToDetails, topToDate, topToCurrency, topToContainer, bottomToContainer, leadingToContainer, trailingToContainer, nil]];
            
            return returnView;
        }
        default:
            return [UIView new];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    switch (indexPath.section) {
        case 0: {
            CIHeaderCell *cell = (CIHeaderCell*)[tableView dequeueReusableCellWithIdentifier:@"header" forIndexPath:indexPath];
            
            cell.arcView.datasource = self;
            cell.arcView.delegate = self;
            
            cell.currentEarningLabel.text = LOCALIZATION(C_CURRENT_EARNINGS);
            cell.currentEarningLabel.font = FONT_H1;
            cell.currentEarningValue.text = [currencyString stringByAppendingString:[NSString stringWithFormat:@"%d", (int)balancePoints]];
            cell.currentEarningValue.font = FONT_H1;
            
            cell.totalEarningLabel.text = LOCALIZATION(C_TOTAL_EARNINGS);
            cell.totalEarningLabel.font = FONT_B2;
            cell.totalEarningValue.text = [currencyString stringByAppendingString:[NSString stringWithFormat:@"%d", (int)earnedPoints]];
            cell.totalEarningValue.font = FONT_H1;
            
            cell.totalRemittedLabel.text = LOCALIZATION(C_TOTAL_REMITTED);
            cell.totalRemittedLabel.font = FONT_B2;
            cell.totalRemittedValue.text = [currencyString stringByAppendingString:[NSString stringWithFormat:@"%d", (int)redeemedPoints]];
            cell.totalRemittedValue.font = FONT_H1;
            
            cell.disclaimerFirstLabel.text = LOCALIZATION(C_CASHINCENTIVE_DISCLAIMER1);
            cell.disclaimerFirstLabel.font = FONT_B3;
            cell.disclaimerSecondLabel.text = LOCALIZATION(C_CASHINCENTIVE_DISCLAIMER2);
            cell.disclaimerSecondLabel.font = FONT_B3;
            
            dateFormatter.dateFormat = @"dd/MM/YY";
            cell.disclaimerUpdateLabel.text = [NSString stringWithFormat:@"%@ %@",LOCALIZATION(C_UPDATED_AS_OF),updatedDate];
            cell.disclaimerUpdateLabel.font = FONT_B3;
            
            return cell;
        }
            
        case 1:
            switch (indexPath.row) {
                case 0: {
                    CIStatusCell *cell = (CIStatusCell*)[tableView dequeueReusableCellWithIdentifier:@"filterStatus" forIndexPath:indexPath];
                    
                    cell.statusLabel.text = status;
                    for (UIGestureRecognizer *recognizer in cell.statusLabel.superview.gestureRecognizers) {
                        [cell.statusLabel.superview removeGestureRecognizer:recognizer];
                    }
                    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(statusDidTap)];
                    [cell.statusLabel.superview addGestureRecognizer:tapGesture];
                    cell.statusLabel.font = FONT_B1;
                    [cell.infoButton removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
                    [cell.infoButton addTarget:self action:@selector(infoButtonDidPress) forControlEvents:UIControlEventTouchUpInside];
                    
                    return cell;
                }
                    
                case 1: {
                    CITransactionCell *cell = (CITransactionCell*)[tableView dequeueReusableCellWithIdentifier:@"filterTransaction" forIndexPath:indexPath];
                    
                    cell.transactionLabel.text = serviceType;
                    for (UIGestureRecognizer *recognizer in cell.transactionLabel.superview.gestureRecognizers) {
                        [cell.transactionLabel.superview removeGestureRecognizer:recognizer];
                    }
                    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(transactionDidTap)];
                    [cell.transactionLabel.superview addGestureRecognizer:tapGesture];
                    cell.transactionLabel.font = FONT_B1;
                    
                    return cell;
                }
                    
                case 2: {
                    CIDateRangeCell *cell = (CIDateRangeCell*)[tableView dequeueReusableCellWithIdentifier:@"dateRange" forIndexPath:indexPath];
                    
                    dateFormatter.dateFormat = @"MMM YYYY";
                    
                    [cell.startDateButton setTitle:[dateFormatter stringFromDate:startDate] forState:UIControlStateNormal];
                    [cell.startDateButton removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
                    [cell.startDateButton addTarget:self action:@selector(startDateDidPress) forControlEvents:UIControlEventTouchUpInside];
                    cell.startDateButton.titleLabel.font = FONT_B1;
                    
                    [cell.endDateButton setTitle:[dateFormatter stringFromDate:endDate] forState:UIControlStateNormal];
                    [cell.endDateButton removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
                    [cell.endDateButton addTarget:self action:@selector(endDateDidPress) forControlEvents:UIControlEventTouchUpInside];
                    cell.endDateButton.titleLabel.font = FONT_B1;
                    
                    [cell.enterButton setTitle:@"Enter" forState:UIControlStateNormal];
                    [cell.enterButton removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
                    [cell.enterButton addTarget:self action:@selector(enterButtonDidPress) forControlEvents:UIControlEventTouchUpInside];
                    cell.enterButton.titleLabel.font = FONT_H1;
                    
                    return cell;
                }
            }
            
        case 2: {
            CIDetailsCell *cell = (CIDetailsCell*)[tableView dequeueReusableCellWithIdentifier:@"details" forIndexPath:indexPath];
            NSDictionary *info = resultArray[indexPath.row];
            
            switch ([[info objectForKey:@"TypeColor"] intValue]) {
                case 1:
//                    NSLog(@"Plus");
                    [cell.icon setImage:[UIImage imageNamed:@"icn_add_small"]];
                    break;
                    
                case 2:
//                    NSLog(@"Minus");
                    [cell.icon setImage:[UIImage imageNamed:@"icn_minus_small"]];
                    break;
                    
                case 4:
//                    NSLog(@"Pending");
                    [cell.icon setImage:[UIImage imageNamed:@"icn_pending_small"]];
                    break;
                    
                default:
//                    NSLog(@"Default");
                    [cell.icon setImage:[UIImage imageNamed:@"icn_minus_small"]];
                    break;
            }
            dateFormatter.dateFormat = @"dd/MM/YY";
            cell.dateLabel.text = (NSString*) [info objectForKey:@"TransactionDate"];//[dateFormatter stringFromDate:[NSDate new]];
            cell.dateLabel.font = FONT_B2;
            cell.titleLabel.text = (NSString*) [info objectForKey:@"Description"];
            cell.titleLabel.font = FONT_H1;
            if([[info objectForKey:@"Remarks"] isKindOfClass:[NSNull class]])
                cell.remarksLabel.text = @"";
            else
                 cell.remarksLabel.text = (NSString*) [info objectForKey:@"Remarks"];
            cell.remarksLabel.font = FONT_B3;
            cell.valueLabel.text = [currencyString stringByAppendingString:(NSString*) [info objectForKey:@"Amount"]];
            cell.valueLabel.font = FONT_H1;
            
            return cell;
        }
            
        default:
            return [UITableViewCell new];
    }
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0: {
            CIHeaderCell *headerCell = (CIHeaderCell*)cell;
            
            [headerCell.arcView redraw];
        }
            break;
            
        case 1:
            break;
            
        case 2:
            break;
            
        default:
            break;
    }
}


@end

