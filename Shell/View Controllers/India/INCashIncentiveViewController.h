//
//  INCashIncentiveViewController.h
//  Shell
//
//  Created by Ben on 21/5/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "BaseVC.h"
#import "ArcView.h"

@interface INCashIncentiveViewController : BaseVC

@end


@interface CIHeaderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *currentEarningLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentEarningValue;

@property (weak, nonatomic) IBOutlet UILabel *totalEarningLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalEarningValue;

@property (weak, nonatomic) IBOutlet UILabel *totalRemittedLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalRemittedValue;

@property (weak, nonatomic) IBOutlet UILabel *disclaimerFirstLabel;
@property (weak, nonatomic) IBOutlet UILabel *disclaimerSecondLabel;
@property (weak, nonatomic) IBOutlet UILabel *disclaimerUpdateLabel;

@property (weak, nonatomic) IBOutlet ArcView *arcView;

@end


@interface CIStatusCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *infoButton;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@end


@interface CITransactionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *transactionLabel;

@end


@interface CIDateRangeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *startDateButton;
@property (weak, nonatomic) IBOutlet UILabel *toJoiningLabel;
@property (weak, nonatomic) IBOutlet UIButton *endDateButton;
@property (weak, nonatomic) IBOutlet UIButton *enterButton;

@end


@interface CIDetailsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *remarksLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;

@end
