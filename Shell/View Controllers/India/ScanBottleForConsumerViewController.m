//
//  ScanBottleForConsumerViewController.m
//  Shell
//
//  Created by Nach on 12/11/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "ScanBottleForConsumerViewController.h"
#import "PopupValueEntryViewController.h"
#import "ScanProductTableViewCell.h"
#import "QRCodeScanner.h"

#import <Photos/Photos.h>
#import "SWGApiClient.h"
#import "SWGConfiguration.h"
// load models
#import "SWGCoordinate.h"
#import "SWGInlineResponse200.h"
#import "SWGInlineResponse200ProcessingTime.h"
#import "SWGInlineResponse400.h"
#import "SWGPlateCandidate.h"
#import "SWGPlateDetails.h"
#import "SWGRegionOfInterest.h"
#import "SWGVehicleCandidate.h"
#import "SWGVehicleDetails.h"
// load API classes for accessing endpoints
#import "SWGDefaultApi.h"

#import <MapKit/MapKit.h>

@interface ScanBottleForConsumerViewController () <QRCodeScannerDelegate,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,PopupValueEntryDelegate,WebServiceManagerDelegate>

@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblShareCodeViewHeader;
@property (weak, nonatomic) IBOutlet UITextField *textVehicleID;
@property (weak, nonatomic) IBOutlet UIButton *btnScanVehicleID;
@property (weak, nonatomic) IBOutlet UILabel *lblBottleCodes;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectOption;
@property (weak, nonatomic) IBOutlet UIButton *btnEnterProductCode;
@property (weak, nonatomic) IBOutlet UIButton *btnScanProductCode;
@property (weak, nonatomic) IBOutlet UILabel *lblNo;
@property (weak, nonatomic) IBOutlet UILabel *lblScannedCode;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblDelete;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@property (weak, nonatomic) IBOutlet UIView *offlineScanView;
@property (weak, nonatomic) IBOutlet UILabel *lblOfflineText;
@property (weak, nonatomic) IBOutlet UIButton *btnOfflineYes;
@property (weak, nonatomic) IBOutlet UIButton *btnOfflineNo;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property CLLocationManager *locationManager;
@property CGPoint currentLocation;
@property BOOL didFindLocation;

@property NSMutableArray *arrayOfScanCode;
@property NSString *currentCodeString;

@end

@implementation ScanBottleForConsumerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_QL_SCANBOTTLE) subtitle:@""];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    
    self.arrayOfScanCode = [[NSMutableArray alloc] init];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / Customer",LOCALIZATION_EN(C_QL_SCANBOTTLE)] screenClass:nil];
    
    self.lblHeader.text = LOCALIZATION(C_RU_BREAKDOWN_DETAILS);
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    self.lblHeader.font = FONT_H1;
    
    self.lblShareCodeViewHeader.text = LOCALIZATION(C_SCANBOTTLE_VEHICLEID);
    self.lblShareCodeViewHeader.textColor = COLOUR_VERYDARKGREY;
    self.lblShareCodeViewHeader.font = FONT_B2;
    
    self.textVehicleID.placeholder = LOCALIZATION(C_SEARCHVEHICLE_VEHICLEID);
    self.textVehicleID.textColor = COLOUR_VERYDARKGREY;
    self.textVehicleID.font = FONT_B2;
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [self.textVehicleID setLeftViewMode:UITextFieldViewModeAlways];
    [self.textVehicleID setLeftView:spacerView];
    
    self.lblBottleCodes.text = LOCALIZATION(C_SCANBOTTLE_BOTTLECODES_HEADER);
    self.lblBottleCodes.textColor = COLOUR_VERYDARKGREY;
    self.lblBottleCodes.font = FONT_H1;
    
    self.lblSelectOption.text = LOCALIZATION(C_SCANCODE_SELECTFOLLOWING);
    self.lblSelectOption.textColor = COLOUR_VERYDARKGREY;
    self.lblSelectOption.font = FONT_B2;
    
    [self.btnScanProductCode.titleLabel setFont: FONT_BUTTON];
    [self.btnScanProductCode setBackgroundColor: COLOUR_YELLOW];
    [self.btnScanProductCode setTitle:LOCALIZATION(C_FORM_SCAN) forState:UIControlStateNormal];
    [self.btnScanProductCode setTitleColor: COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    
    [self.btnEnterProductCode.titleLabel setFont: FONT_BUTTON];
    [self.btnEnterProductCode setBackgroundColor: COLOUR_YELLOW];
    [self.btnEnterProductCode setTitle:LOCALIZATION(C_FORM_MANUAL) forState:UIControlStateNormal];
    [self.btnEnterProductCode setTitleColor: COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    
    self.lblNo.font = FONT_H1;
    self.lblNo.textColor = COLOUR_VERYDARKGREY;
    self.lblNo.text = LOCALIZATION(C_TABLE_NO);
    
    self.lblScannedCode.font = self.lblNo.font;
    self.lblScannedCode.textColor = self.lblNo.textColor;
    self.lblScannedCode.text= LOCALIZATION(C_TABLE_SCANNEDCODE);
    if ([GET_LOCALIZATION isEqualToString: kChinese])
        self.lblScannedCode.textAlignment = NSTextAlignmentCenter;
    
    self.lblStatus.font = self.lblNo.font;
    self.lblStatus.textColor = self.lblNo.textColor;
    self.lblStatus.text = LOCALIZATION(C_STATUS);
    
    self.lblDelete.font = self.lblNo.font;
    self.lblDelete.textColor = self.lblNo.textColor;
    self.lblDelete.text = LOCALIZATION(C_DELETE);
    
    self.btnSubmit.titleLabel.font = FONT_BUTTON;
    [self.btnSubmit setTitle:LOCALIZATION(C_FORM_SUBMIT) forState:UIControlStateNormal];
    [self.btnSubmit setBackgroundColor:COLOUR_RED];
    
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:[UINib nibWithNibName:@"ScanProductTableViewCell" bundle:nil] forCellReuseIdentifier:@"ScanProductTableViewCell"];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager requestWhenInUseAuthorization];
    
    if (GET_SCANNEDCODES_IN) {
        NSArray *savedCodes = GET_SCANNEDCODES_IN;
        for(NSDictionary *dict in savedCodes) {
            if([[dict objectForKey:@"Isconsumer"] boolValue]){
                self.offlineScanView.hidden = NO;
                self.scrollView.hidden = YES;
                self.btnSubmit.hidden = YES;
                break;
            }
        }
    } else {
        self.offlineScanView.hidden = YES;
        self.scrollView.hidden = NO;
        self.btnSubmit.hidden = NO;
    }
    
    self.lblOfflineText.font = FONT_B2;
    self.lblOfflineText.text = LOCALIZATION(C_FORM_OFFLINESCAN);
    
    self.btnOfflineYes.titleLabel.font = FONT_BUTTON;
    [self.btnOfflineYes setTitle:LOCALIZATION(C_FORM_YES) forState:UIControlStateNormal];
    self.btnOfflineNo.titleLabel.font = FONT_BUTTON;
    [self.btnOfflineNo setBackgroundColor: COLOUR_YELLOW];
    [self.btnOfflineNo setTitle:LOCALIZATION(C_FORM_NO) forState:UIControlStateNormal];
    [self.btnOfflineYes setBackgroundColor:COLOUR_RED];
    
     mQRCodeScanner.delegate = self;
    
}

- (IBAction)backPressed:(UIButton *)sender {
    [self popSelf];
}

- (IBAction) vehicleCameraPressed:(UIButton *)sender {
    if([self.textVehicleID isFirstResponder]) {
        [self.textVehicleID resignFirstResponder];
    }
    
    [mSession pushPhotoTakingViewControllerWithParent: self];
}


- (IBAction)scanProductCodePressed:(UIButton *)sender {
    if([self.textVehicleID isFirstResponder]) {
           [self.textVehicleID resignFirstResponder];
    }
    [mQRCodeScanner showQRCodeScannerOnVC:self withMessage:LOCALIZATION(C_REGISTEROILCHANGE_SUBTITLE)];
}

- (IBAction)manualProductCodePressed:(UIButton *)sender {
    [self popupValueEntryVCWithTitle:LOCALIZATION(C_FORM_ENTERCODETITLE) submitTitle:LOCALIZATION(C_FORM_ADDCODE) cancelTitle:LOCALIZATION(C_FORM_CANCEL) placeHolder:LOCALIZATION(C_FORM_ENTERCODETITLE)];
}

- (IBAction)submitPressed:(UIButton *)sender {
    if ([self.arrayOfScanCode count] == 0)
    {
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_ERROR)];
        return;
    }
    NSMutableDictionary *submitCodeDict = [[NSMutableDictionary alloc] init];
    
    [submitCodeDict setObject:@[@{
                @"Coupons"          : self.arrayOfScanCode      ,
                @"VehicleNumber"    : self.textVehicleID.text                       ,
                @"StateID"          : @""                       ,
                }]
    forKey:@"VehicleListForSellout"];
    [submitCodeDict setObject:@(YES) forKey:@"Isconsumer"];
    [submitCodeDict setObject:@"" forKey:@"Sharecode"];
    
    if ([mSession getReachability]) {
          [[WebServiceManager sharedInstance] submitCodesForSellout:submitCodeDict vc:self];
    } else {
        //save
        NSMutableArray *pendingDict = [[NSMutableArray alloc] initWithArray: GET_SCANNEDCODES_IN copyItems:YES];
        
        if (!pendingDict)
            pendingDict = [[NSMutableArray alloc] init];
        
        for (NSMutableDictionary *dict in self.arrayOfScanCode)
        {
            [dict setObject: self.textVehicleID.text forKey: @"VehicleNumber"];
        }
        
        [pendingDict addObject: @{ @"SHARECode"         : @""                       ,
                                   @"Coupons"           : self.arrayOfScanCode     ,
                                   @"Date"              : [self getTodayDate]      ,
                                   @"VehicleNumber"     :  self.textVehicleID.text                      ,
                                   @"Isconsumer"        : @(YES)                    ,
        }];
        
        SET_SCANNEDCODES_IN(pendingDict);
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.arrayOfScanCode removeAllObjects];
        self.textVehicleID.text = @"";
        
        [self.tableView reloadData];
        
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_ALERT_SAVEDCODE)];
    }
        
}

- (IBAction)offlineNoPressed:(id)sender {
    self.offlineScanView.hidden = YES;
    self.scrollView.hidden = NO;
    self.btnSubmit.hidden = NO;
}

- (IBAction)offlineYesPressed:(id)sender {
    self.offlineScanView.hidden = YES;
    self.scrollView.hidden = NO;
    self.btnSubmit.hidden = NO;
    //push to pending code view
    [mSession pushPendingCodeViewForScanBottle:YES forConsumer:YES onVc:self];
}

#pragma mark - QRCode Scanner
- (void)qrCodeScanSuccess:(NSString *)scanString {
    
    NSString *scannedValue = [[scanString componentsSeparatedByString:@"="] lastObject];
    
    if(![[mSession profileInfo]hasMultipleSubmissionOilChange]) {
        for (NSDictionary *dict in self.arrayOfScanCode)
        {
            if ([scannedValue isEqualToString: [dict objectForKey: @"Code"]])
            {
                [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_EXIST)];         //lokalised
                return;
            }
        }
    }
    
    if(scannedValue.length > 0){
       self.currentCodeString = scannedValue;
       [self updatelocation];
    }
}

#pragma mark - Location
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    if (self.didFindLocation)
        return;
    
    //most recent location update is at the end of the array.
    CLLocation *location = [locations lastObject];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:6];
    
    self.currentLocation = CGPointMake(location.coordinate.latitude, location.coordinate.longitude);
    
    self.didFindLocation = YES;
    [self.locationManager stopUpdatingLocation];
    
    if(![[mSession profileInfo]hasMultipleSubmissionOilChange])
    {
        //ipod touch getting duplicate value. Manually check
        for(NSDictionary *duplicate in self.arrayOfScanCode)
        {
            if([[duplicate objectForKey:@"Code"] isEqualToString:self.currentCodeString]){
                [UpdateHUD removeMBProgress: self];
                return;
            }
        }
    }
    
    NSMutableDictionary *codeDict = [NSMutableDictionary dictionaryWithDictionary:
                                     @{@"Code"          :self.currentCodeString,
                                       @"Date"          : [self getTodayDate],
                                       @"Latitude"      : @(self.currentLocation.x),
                                       @"Longitude"     : @(self.currentLocation.y),
                                       }];
    
    [self.arrayOfScanCode addObject: codeDict];
    self.currentLocation = CGPointMake(0, 0);
    
    [self.tableView reloadData];
    
    [UpdateHUD removeMBProgress: self];
}

-(void) updatelocation {
    [UpdateHUD addMBProgress: self.view withText: @""];
    self.didFindLocation = NO;
    switch ([CLLocationManager authorizationStatus])
    {
        case kCLAuthorizationStatusAuthorizedAlways:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.locationManager startUpdatingLocation];
            break;
        default:
        {
            //send 0,0
            self.currentLocation = CGPointMake(0, 0);
            
            NSMutableDictionary *codeDict = [NSMutableDictionary dictionaryWithDictionary:
                                             @{@"Code"          :self.currentCodeString,
                                               @"Date"          : [self getTodayDate],
                                               @"Latitude"      : @(self.currentLocation.x),
                                               @"Longitude"     : @(self.currentLocation.y),
                                               }];
            
            [self.arrayOfScanCode addObject: codeDict];
            [UpdateHUD removeMBProgress: self];
            [self.tableView reloadData];
        }
            break;
    }
    
}

#pragma mark - Date
-(NSString *)getTodayDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    return [formatter stringFromDate: [NSDate date]];
}

#pragma mark - PopupValueEntryDelegate
-(void) valueSubmitted:(NSString *)value {
    
    if(![[mSession profileInfo] hasMultipleSubmissionOilChange]) {
        for (NSDictionary *dict in self.arrayOfScanCode)
        {
            if ([value isEqualToString: [dict objectForKey: @"Code"]])
            {
                [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_EXIST)];         //lokalised
                return;
            }
        }
    }
    
    self.currentCodeString = value;
    [self updatelocation];
}

#pragma mark - TableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayOfScanCode.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ScanProductTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ScanProductTableViewCell"];
    NSDictionary *codeStringDict = [self.arrayOfScanCode objectAtIndex:indexPath.row];
    
    [cell setID: @(indexPath.row + 1).stringValue];
    [cell setScannedCode: [codeStringDict objectForKey: @"Code"]];
    [cell setPendingStatus: LOCALIZATION(C_SCANCODE_STATUS_PENDING)];
    
    cell.btnDelete.tag = indexPath.row;
    cell.btnDelete.backgroundColor = COLOUR_RED;
    [cell.btnDelete setTitle: LOCALIZATION(C_DELETE) forState: UIControlStateNormal];
    [cell.btnDelete addTarget: self action: @selector(deletePressed:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(void)deletePressed:(UIButton*)sender
{
    [self.arrayOfScanCode removeObjectAtIndex:sender.tag];
    [self.tableView reloadData];
}

#pragma mark - Car Plate Scanning
-(void)imageTaken:(UIImage *)image key:(NSString *)keyString
{
    if (!image)
        return;
    
    UIImage *newImage = [self rotateImage:image];
    
    NSData *imgData = UIImageJPEGRepresentation(newImage, 0.1);
    NSString *base64String = [imgData base64EncodedStringWithOptions: 0];
    
    NSString* secretKey = @"sk_a94baee6f07c7836e8ef292d"; // The secret key used to authenticate your account.  You can view your  secret key by visiting  https://cloud.openalpr.com/
    NSString *country = [self getCountryCode];
    //    NSString* country = @"us"; // Defines the training data used by OpenALPR.  \"us\" analyzes  North-American style plates.  \"eu\" analyzes European-style plates.  This field is required if using the \"plate\" task  You may use multiple datasets by using commas between the country  codes.  For example, 'au,auwide' would analyze using both the  Australian plate styles.  A full list of supported country codes  can be found here https://github.com/openalpr/openalpr/tree/master/runtime_data/config
    //au, auwide, br, br2, eu, fr, gb, in, kr, kr2, mx, sg, us, vn2
    
    if([country length] == 0)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:@"Invalid Country"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {
                                 //BUTTON OK CLICK EVENT
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    NSNumber* recognizeVehicle = @(0); // If set to 1, the vehicle will also be recognized in the image This requires an additional credit per request  (optional) (default to 0)
    NSString* state = @""; // Corresponds to a US state or EU country code used by OpenALPR pattern  recognition.  For example, using \"md\" matches US plates against the  Maryland plate patterns.  Using \"fr\" matches European plates against  the French plate patterns.  (optional) (default to )
    NSNumber* returnImage = @(0); // If set to 1, the image you uploaded will be encoded in base64 and  sent back along with the response  (optional) (default to 0)
    NSNumber* topn = @(5); // The number of results you would like to be returned for plate  candidates and vehicle classifications  (optional) (default to 10)
    NSString* prewarp = @""; // Prewarp configuration is used to calibrate the analyses for the  angle of a particular camera.  More information is available here http://doc.openalpr.com/accuracy_improvements.html#calibration  (optional) (default to )
    
    SWGDefaultApi *apiInstance = [[SWGDefaultApi alloc] init];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [apiInstance recognizeBytesWithImageBytes:base64String
                                    secretKey:secretKey
                                      country:country
                             recognizeVehicle:recognizeVehicle
                                        state:state
                                  returnImage:returnImage
                                         topn:topn
                                      prewarp:prewarp
                            completionHandler: ^(SWGInlineResponse200* output, NSError* error) {
                                if (output)
                                {
                                    if ([output.results count] > 0)
                                    {
                                        SWGPlateDetails *plateDetails = [output.results objectAtIndex: 0];
                                        
                                        self.textVehicleID.text = plateDetails.plate; //String of car plate
                                        [self.textVehicleID becomeFirstResponder];
                                    }
                                    else
                                    {
                                        UIAlertController *alert = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_GLOBAL_ERROR)
                                                                                                       message: LOCALIZATION(C_ERROR_CANNOTRECOGNIZE)
                                                                                                preferredStyle:UIAlertControllerStyleAlert];
                                        
                                        UIAlertAction *ok = [UIAlertAction actionWithTitle: LOCALIZATION(C_BUTTON_OK) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                             {
                                                                 //BUTTON OK CLICK EVENT
                                                             }];
                                        [alert addAction:ok];
                                        [self presentViewController:alert animated:YES completion:nil];
                                        //                                        self.imgView.image = nil;
                                    }
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                }
                                if (error) {
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    
                                    UIAlertController *alert = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_GLOBAL_ERROR)
                                                                                                   message: LOCALIZATION(C_ERROR_UNKNOWNERROR)
                                                                                            preferredStyle:UIAlertControllerStyleAlert];
                                    
                                    UIAlertAction *ok = [UIAlertAction actionWithTitle: LOCALIZATION(C_BUTTON_OK) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                         {
                                                             //BUTTON OK CLICK EVENT
                                                         }];
                                    [alert addAction:ok];
                                    [self presentViewController:alert animated:YES completion:nil];
                                }
                            }];
}


-(NSString *) getCountryCode
{
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
    {
        return @"th";
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDIA])
    {
        return @"in";
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDONESIA])
    {
        return @"id";
    }
    else if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_SAUDI])
    {
        return @"sa";
    }
    else
    {
        return @"eu";
    }
}

- (UIImage*) rotateImage:(UIImage* )originalImage {
    
    UIImageOrientation orientation = originalImage.imageOrientation;
    
    UIGraphicsBeginImageContext(originalImage.size);
    
    [originalImage drawAtPoint:CGPointMake(0, 0)];
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orientation == UIImageOrientationRight) {
        CGContextRotateCTM (context, [self radians:90]);
    } else if (orientation == UIImageOrientationLeft) {
        CGContextRotateCTM (context, [self radians:90]);
    } else if (orientation == UIImageOrientationDown) {
        // NOTHING
    } else if (orientation == UIImageOrientationUp) {
        CGContextRotateCTM (context, [self radians:0]);
    }
    
    return UIGraphicsGetImageFromCurrentImageContext();
    
}

- (CGFloat) radians:(int)degree {
    return (degree/180)*(22/7);
}

- (UIImage *)imageWithImage:(UIImage *)image
{
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    CGSize newSize = image.size;
    
    CGFloat aspectRatio = newSize.width / newSize.height;
    
    if (newSize.width >= newSize.height)
    {
        //landscape or square, set width to 1280
        newSize.width = 1280;
        newSize.height = newSize.width * aspectRatio;
    }
    else
    {
        //portrait
        newSize.height = 1280;
        newSize.width = newSize.height * aspectRatio;
    }
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - Webservice Manager
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_SUBMITCODE_SELLOUT: {
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                [mSession pushScanBottleResult:[response getGenericResponse] isScanBottle:YES isConsumer:YES vc:self];
                            
                [self.arrayOfScanCode removeAllObjects];
                self.textVehicleID.text = @"";
               
                [self.tableView reloadData];
            }
        }
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
