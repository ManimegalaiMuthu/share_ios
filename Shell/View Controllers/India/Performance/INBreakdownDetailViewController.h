//
//  INBreakdownDetailViewController.h
//  Shell
//
//  Created by Nach on 29/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface INBreakdownDetailViewController : BaseVC

-(void)reloadBreakdownData:(nullable NSMutableDictionary *)responseDict;

@property BOOL isProductBreakdown;

@end

NS_ASSUME_NONNULL_END
