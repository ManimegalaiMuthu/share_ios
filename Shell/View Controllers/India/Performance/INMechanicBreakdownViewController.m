//
//  INMechanicBreakdownViewController.m
//  Shell
//
//  Created by Nach on 30/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "INMechanicBreakdownViewController.h"

#import "CASMonthYearPicker.h"

@interface INMechanicBreakdownViewController ()<WebServiceManagerDelegate, MonthYearPickerDelegate, UITableViewDelegate, UITableViewDataSource>

@property NSMutableArray *sectionOpenControlArray; //to control array of BOOL to control sections open/close

@property (weak, nonatomic) IBOutlet UIButton *btnStartDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEndDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property (weak, nonatomic) IBOutlet UIButton *btnEnter;
@property (weak, nonatomic) IBOutlet UITableView *mechTableView;

@property NSDate *startDate;
@property NSDate *endDate;
@property BOOL isStartDate;
@property CASMonthYearPicker *dateView;

@property NSDictionary *breakdownDictionary;

@property NSMutableArray *oilChangeArray;
@property NSString *oilChangeHeaderString;
@property NSMutableArray *customerSignupArray;
@property NSString *customerSignupHeaderString;

@end

@implementation INMechanicBreakdownViewController
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_RU_BREAKDOWN_STAFF_TITLE) subtitle: @""];  //lokalised 28 Jan
    
    [self setNavBackBtn];
    self.lblTo.text = LOCALIZATION(C_RU_STEPONE_TO);    //lokalised 28 Jan
    [self.btnEnter setTitle: LOCALIZATION(C_RU_STEPONE_ENTER) forState: UIControlStateNormal];  //lokalised 28 Jan
    [self.btnEnter setBackgroundColor:COLOUR_RED];
    self.mechTableView.backgroundColor = COLOUR_VERYPALEGREY;
    self.mechTableView.backgroundView = nil;
    
    dateView = [[CASMonthYearPicker alloc] initWithTitle:LOCALIZATION(C_PERFORMANCE_SELECTDATE) previouslySelected:nil withYearHidden:NO];  //lokalised 28 Jan
    
    dateView.delegate = self;
    
    
    //setup default date
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    //setup the necesary date adjustments
    NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
    [selectedComponent setDay: 1];
    NSDate *selectedDate = [gregorian dateFromComponents: selectedComponent];
    
    [selectedComponent setDay: 0];
    [selectedComponent setMonth: -2];
    [selectedComponent setYear: 0];
    selectedDate = [gregorian dateByAddingComponents: selectedComponent toDate: selectedDate options:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)];
    self.isStartDate = YES;
    self.startDate = selectedDate;
    [self selectedMonthYear: selectedDate];
    [self enterPressed: nil];
}

- (IBAction)enterPressed:(id)sender
{
    NSMutableDictionary *tradeDict = [[NSMutableDictionary alloc] init];
    //    LoginInfo *loginInfo = [mSession loadUserProfile];
    //
    //    [tradeDict setObject: loginInfo.tradeID forKey: @"TradeID"];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy/MM/dd"];
    
    [tradeDict setObject: [outputFormatter stringFromDate: self.startDate] forKey:@"StartDate"];
    [tradeDict setObject: [outputFormatter stringFromDate: self.endDate] forKey:@"EndDate"];
    
    [[WebServiceManager sharedInstance] fetchStaffBreakdown: tradeDict vc: self];
}

- (IBAction)datePressed:(UIButton *)sender {
    [self.view endEditing: YES];
    
    if ([sender isEqual: self.btnStartDate])
        self.isStartDate = YES;
    else
        self.isStartDate = NO;
    
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}

#pragma mark - Date Picker delegate method
- (void)selectedMonthYear:(NSDate *)selectedDate
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    
    //setup the necesary date adjustments
    NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: selectedDate];
    [selectedComponent setDay: 1];
    selectedDate = [gregorian dateFromComponents: selectedComponent];
    
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setMonth: 3];
    [offsetComponents setDay: -1];
    
    NSDateFormatter *btnDateFormatter = [[NSDateFormatter alloc] init];
    [btnDateFormatter setDateFormat:@"MMM yyyy"];
    if ([GET_LOCALIZATION isEqualToString: kRussian])
        [btnDateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kRussian]];
    else
        [btnDateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    
    if (self.isStartDate)
    {
        NSDate *offsetDate = [gregorian dateByAddingComponents:offsetComponents toDate: selectedDate options:0];
        
        NSString *selectedDateString;
        NSString *offsetDateString;
        //check end date if more than current date
        if ([offsetDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
        {
            //check start date selection if more than current date.
            if ([selectedDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
            {
                NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
                [selectedComponent setDay: 1];
                selectedDate = [gregorian dateFromComponents: selectedComponent];
            }
            selectedDateString = [btnDateFormatter stringFromDate: selectedDate]; //first of current month
            offsetDateString = [btnDateFormatter stringFromDate: [NSDate date]]; //current date
            offsetDate = [NSDate date]; //set end date to current date
        }
        else
        {
            //start date will never be more than current date if above validation of end date clears.
            selectedDateString = [btnDateFormatter stringFromDate: selectedDate];
            offsetDateString = [btnDateFormatter stringFromDate: offsetDate]; //offset still 2 months later
        }
        
        
        //hardcode to first three character in russian
        if ([GET_LOCALIZATION isEqualToString: kRussian])
        {
            NSMutableArray *selectedDateStringArray = [[NSMutableArray alloc] initWithArray: [selectedDateString componentsSeparatedByString: @" "]];
            NSString *selectedMonth = [selectedDateStringArray objectAtIndex: 0];
            selectedMonth = [selectedMonth substringToIndex: 3];
            selectedDateString = [[NSString stringWithFormat: @"%@ %@", selectedMonth, [selectedDateStringArray objectAtIndex: 1]] capitalizedString];
            
            NSMutableArray *offsetDateStringArray = [[NSMutableArray alloc] initWithArray: [offsetDateString componentsSeparatedByString: @" "]];
            NSString *offsetMonth = [offsetDateStringArray objectAtIndex: 0];
            offsetMonth = [offsetMonth substringToIndex: 3];
            offsetDateString = [[NSString stringWithFormat: @"%@ %@", offsetMonth, [offsetDateStringArray objectAtIndex: 1]] capitalizedString]; //Capitalised, e.g Aug, Sep, instead of aug, sep
        }
        
        //end start is offset by 3 months
        [self.btnStartDate setTitle: selectedDateString forState: UIControlStateNormal];
        [self.btnEndDate setTitle: offsetDateString forState: UIControlStateNormal];
        
        self.startDate = selectedDate;
        self.endDate = offsetDate;
        
    }
    else
    {
        //handle only end date
        
        //set date to end of selected month
        NSDateComponents *endOfMonthComponents = [[NSDateComponents alloc] init];
        [endOfMonthComponents setYear: 0];
        [endOfMonthComponents setMonth: 1];
        [endOfMonthComponents setDay: -1];
        selectedDate = [gregorian dateByAddingComponents: endOfMonthComponents toDate: selectedDate options: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)];
        
        //check date from startdate
        NSDate *offsetDate = [gregorian dateByAddingComponents:offsetComponents toDate: self.startDate options:0];
        
        //if offset date is more than current month, change offset to current month
        if ([offsetDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
        {
            //reset offset date to last day of current month
            NSDateComponents *currentDateComponents = [gregorian components: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
            [currentDateComponents setDay: 1];
            [currentDateComponents setMonth: [currentDateComponents month] + 1];
            offsetDate = [gregorian dateFromComponents: currentDateComponents];
            
            [currentDateComponents setDay: -1];
            [currentDateComponents setMonth: 0];
            [currentDateComponents setYear: 0];
            offsetDate = [gregorian dateByAddingComponents: currentDateComponents toDate: offsetDate options:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) ];
        }
        
        //if selection more than threshold (+2 months from start month)
        if ([selectedDate timeIntervalSinceReferenceDate] > [offsetDate timeIntervalSinceReferenceDate])
        {
            selectedDate = offsetDate;
        }
        else if ([selectedDate timeIntervalSinceReferenceDate] < [self.startDate timeIntervalSinceReferenceDate])
        {
            //if selection less than start month
            selectedDate = self.startDate;
        }
        else
        {
            
        }
        
        //don't need to change start date.
        NSString *selectedDateString = [btnDateFormatter stringFromDate: selectedDate];
        
        //hardcode to first three character in russian
        if ([GET_LOCALIZATION isEqualToString: kRussian])
        {
            NSMutableArray *selectedDateStringArray = [[NSMutableArray alloc] initWithArray: [selectedDateString componentsSeparatedByString: @" "]];
            NSString *selectedMonth = [selectedDateStringArray objectAtIndex: 0];
            selectedMonth = [selectedMonth substringToIndex: 3];
            selectedDateString = [[NSString stringWithFormat: @"%@ %@", selectedMonth, [selectedDateStringArray objectAtIndex: 1]] capitalizedString];
        }
        
        [self.btnEndDate setTitle: selectedDateString forState: UIControlStateNormal];
        self.endDate = selectedDate;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - tableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section > 0)
    {
        if ([[self.sectionOpenControlArray objectAtIndex: section - 1] boolValue])
        {
            if (section == 1)
            {
                return [self.oilChangeArray count] + 1; //for footer
            }
            else if (section == 2)
            {
                return [self.customerSignupArray count];
            }
        }
        else
            return 0;
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return 0;
    return UITableViewAutomaticDimension;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}


enum
{
    kSTAFFBREAKDOWN_TAGS_Plus = 9999,
    kSTAFFBREAKDOWN_TAGS_Button = 10000,
    kSTAFFBREAKDOWN_TAGS_Header = 10001,
    kSTAFFBREAKDOWN_TAGS_StaffName,
    kSTAFFBREAKDOWN_TAGS_MonthOne, //imageview also same tag
    kSTAFFBREAKDOWN_TAGS_MonthTwo,
    kSTAFFBREAKDOWN_TAGS_MonthThree,
    kSTAFFBREAKDOWN_TAGS_Total,
} kSTAFFBREAKDOWN_IN_TAGS_TYPE;

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell;
    if (section == 0)
        return [UIView new];
    if ([[self.sectionOpenControlArray objectAtIndex: section - 1] boolValue])
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"INMechFullHeaderCell"];
        UIImageView *imgPlus = [cell viewWithTag: kSTAFFBREAKDOWN_TAGS_Plus];
        imgPlus.transform = CGAffineTransformRotate(CGAffineTransformIdentity, M_PI_4);
        
        UILabel *lblStaffName = [cell viewWithTag: kSTAFFBREAKDOWN_TAGS_StaffName];
        lblStaffName.text = LOCALIZATION(C_RU_BREAKDOWN_STAFF_STAFFNAME);   //lokalised 28 Jan
        
        UILabel *lblTotal = [cell viewWithTag: kSTAFFBREAKDOWN_TAGS_Total];
        lblTotal.text = LOCALIZATION(C_RU_BREAKDOWN_TOTAL);                 //lokalised 28 Jan
        
        NSArray *monthListArray = [self.breakdownDictionary objectForKey: @"MonthList"];
        UILabel *lblMonthOne = [cell viewWithTag:  kSTAFFBREAKDOWN_TAGS_MonthOne];
        UILabel *lblMonthTwo = [cell viewWithTag:  kSTAFFBREAKDOWN_TAGS_MonthTwo];
        UILabel *lblMonthThree = [cell viewWithTag:  kSTAFFBREAKDOWN_TAGS_MonthThree];
        
        if ([monthListArray count] < 3)
        {
            lblMonthThree.hidden = YES;
        }
        else
        {
            lblMonthThree.text = [[monthListArray objectAtIndex: 2] objectForKey: @"MonthName"];
        }
        
        if ([monthListArray count] < 2)
        {
            lblMonthTwo.hidden = YES;
        }
        else
        {
            lblMonthTwo.text = [[monthListArray objectAtIndex: 1] objectForKey: @"MonthName"];
        }
        
        if ([monthListArray count] < 1)
        {
            lblMonthOne.hidden = YES;
        }
        else
        {
            lblMonthOne.text = [[monthListArray objectAtIndex: 0] objectForKey: @"MonthName"];
        }
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"INMechHeaderCell"];
    }
    UILabel *lblSectionHeader = [cell viewWithTag: kSTAFFBREAKDOWN_TAGS_Header];
    
    if (section == 1)
    {
        lblSectionHeader.text = LOCALIZATION(C_RU_BREAKDOWN_STAFF_OILCHANGE);   //lokalised 28 Jan
    }
    else if (section == 2)
    {
        lblSectionHeader.text = LOCALIZATION(C_RU_BREAKDOWN_STAFF_NEWCUSTOMER); //lokalised 28 Jan
    }
    
    UIButton *btnHeader = [cell viewWithTag: kSTAFFBREAKDOWN_TAGS_Button
                           ]; //10000 set in storyboard
    btnHeader.tag = section;
    
    return cell.contentView;
}

- (IBAction)headerPressed:(UIButton *)sender
{
    BOOL newControl = ![[self.sectionOpenControlArray objectAtIndex: sender.tag - 1] boolValue];
    [self.sectionOpenControlArray replaceObjectAtIndex: (sender.tag - 1) withObject: @(newControl)];
    [self.mechTableView reloadData];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch (indexPath.section) {
        case 0:
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"INMechMainHeaderCell"];
            
            cell.backgroundColor = [UIColor clearColor];
            
            if (!self.breakdownDictionary)
                break;
            
            UILabel *lblStaff = [cell viewWithTag: 1];
            lblStaff.text = LOCALIZATION(C_RU_BREAKDOWN_STAFF_HEADER);  //lokalised 28 Jan
            
            UILabel *lblStaffData = [cell viewWithTag: 2];
            lblStaffData.text = [self.breakdownDictionary objectForKey: @"TotalStaff"];
            
            UILabel *lblUpdated = [cell viewWithTag: 3];
            lblUpdated.text = [NSString stringWithFormat: @"%@ %@", [self.breakdownDictionary objectForKey: @"UpdatedOnText"], [self.breakdownDictionary objectForKey: @"UpdatedOn"] ];
        }
            break;
        default:
        {
            NSMutableArray *sectionArray;
            if (indexPath.section == 1)
            {
                sectionArray = self.oilChangeArray;
                
            }
            else if (indexPath.section == 2)
            {
                sectionArray = self.customerSignupArray;
            }
            
            if (indexPath.row == [sectionArray count])
            {
                cell = [tableView dequeueReusableCellWithIdentifier: @"INMechBreakdownFooterCell"];
                UILabel *lblFooter = [cell viewWithTag: 1];
                lblFooter.text = LOCALIZATION(C_RU_BREAKDOWN_STAFF_FOOTER); //lokalised 28 Jan
            }
            else
            {
                cell = [tableView dequeueReusableCellWithIdentifier: @"INMechBreakdownCell"];
                
                cell.backgroundColor = COLOUR_WHITE;
                
                
                NSDictionary *cellData = [sectionArray objectAtIndex: indexPath.row];
                
                UILabel *lblStaffName = [cell viewWithTag: kSTAFFBREAKDOWN_TAGS_StaffName];
                lblStaffName.text = [cellData objectForKey: @"Name"];
                
                NSArray *monthListArray = [self.breakdownDictionary objectForKey: @"MonthList"];
                
                UILabel *lblMonthOneData = [cell viewWithTag:  kSTAFFBREAKDOWN_TAGS_MonthOne];
                UILabel *lblMonthTwoData = [cell viewWithTag:  kSTAFFBREAKDOWN_TAGS_MonthTwo];
                UILabel *lblMonthThreeData = [cell viewWithTag:  kSTAFFBREAKDOWN_TAGS_MonthThree];
                
                lblMonthOneData.hidden = NO;
                lblMonthTwoData.hidden = NO;
                lblMonthThreeData.hidden = NO;
                
                if ([monthListArray count] < 3)
                    lblMonthThreeData.hidden = YES;
                
                if ([monthListArray count] < 2)
                    lblMonthTwoData.hidden = YES;
                
                if ([monthListArray count] < 1)
                    lblMonthOneData.hidden = YES;
                
                lblMonthOneData.text = [cellData objectForKey: @"MonthValue1"];
                if ([[cellData objectForKey: @"MonthValue2"] isKindOfClass: [NSNull class]])
                    lblMonthTwoData.hidden = YES;
                else
                    lblMonthTwoData.text = [cellData objectForKey: @"MonthValue2"];
                
                if ([[cellData objectForKey: @"MonthValue3"] isKindOfClass: [NSNull class]])
                    lblMonthThreeData.hidden = YES;
                else
                    lblMonthThreeData.text = [cellData objectForKey: @"MonthValue3"];
                UILabel *lblMonthTotal = [cell viewWithTag: kSTAFFBREAKDOWN_TAGS_Total];
                lblMonthTotal.text = [cellData objectForKey: @"Total"];
            }
        }
            break;
    }
    return cell;
}

#pragma mark - Webservice Manager
-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_FETCH_STAFFBREAKDOWN:
        {
            self.breakdownDictionary = [response getGenericResponse];
            self.sectionOpenControlArray = [[NSMutableArray alloc] init];
            
            if ([self.breakdownDictionary objectForKey: @"StaffOilChangeDetails"])
            {
                [self.sectionOpenControlArray addObject: @(NO)];
                self.oilChangeArray = [self.breakdownDictionary objectForKey: @"StaffOilChangeDetails"];
            }
            if ([self.breakdownDictionary objectForKey: @"NewCustomerSignUps"])
            {
                [self.sectionOpenControlArray addObject: @(NO)];
                self.customerSignupArray = [self.breakdownDictionary objectForKey: @"NewCustomerSignUps"];
            }
            self.mechTableView.hidden = NO;
            [self.mechTableView reloadData];
        }
            break;
            
        default:
            break;
    }
}

- (void)processFailed:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_FETCH_STAFFBREAKDOWN:
        {
            self.mechTableView.hidden = YES;
        }
            break;
            
        default:
            break;
    }
}


@end
