//
//  INPerformanceTabViewController.m
//  Shell
//
//  Created by Nach on 29/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "INPerformanceTabViewController.h"
#import "RDVTabBarItem.h"

//#import "INPerformanceDashbordViewController.h"
#import "PerformancePointsViewController.h"
#import "INLeaderboardPerformanceViewController.h"

#import "PerformanceDashboard_NewViewController.h"
//#import "RUTOLeaderboardViewController.h"
#import "LeaderboardWebViewController.h"

#import "MyPerformanceViewController.h"
#import "THRetailMyPerformanceViewController.h"
#import "THPerformancePointsViewController.h"
#import "PerformanceWorkshopViewController.h"
#import "MyCoinsPerformanceViewController.h"

@interface INPerformanceTabViewController (){
//    INPerformanceDashbordViewController *performViewController;
    PerformanceDashboard_NewViewController *dashboardViewController;
    PerformanceWorkshopViewController *performanceWorkshopViewController;

    PerformancePointsViewController *pointViewController;
    INLeaderboardPerformanceViewController *leaderPerformanceViewController;
    
//    RUTOLeaderboardViewController *performanceWorkshopViewController;
    LeaderboardWebViewController *myRankingViewController;
    
    MyPerformanceViewController *myPerformanceViewController;

    THPerformancePointsViewController *thPerformanceViewController;
    THRetailMyPerformanceViewController *thRetailMyPerformanceViewController;
    MyCoinsPerformanceViewController *coinsPerformanceController;
    
}
@end

@implementation INPerformanceTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupInterface];
}

-(void) setupInterface {
//    [Helper setNavigationBarTitle: self title: LOCALIZATION(C_TITLE_WORKSHOPPERFORMANCE) subtitle: @"" size: 15 subtitleSize: 0];

    if (GET_PROFILETYPE == kPROFILETYPE_DSR)
        [Helper setNavigationBarTitle: self title: LOCALIZATION(C_TITLE_ANALYTICS) subtitle: @"" size: 15 subtitleSize: 0];             //lokalised 22/1
    else if (GET_PROFILETYPE == kPROFILETYPE_DMR)
        [Helper setNavigationBarTitle: self title: LOCALIZATION(C_TITLE_IN_MECHPERFORMANCE) subtitle: @"" size: 15 subtitleSize: 0];    //lokalised 22/1
    else if (GET_ISIMPROFILETYPE)
        [Helper setNavigationBarTitle: self title: LOCALIZATION(C_TITLE_MECHPERFORMANCE) subtitle: @"" size: 15 subtitleSize: 0];       //lokalised 22/1
    else if (GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
        [Helper setNavigationBarTitle: self title: LOCALIZATION(C_PERFORMANCE_WORKSHOP) subtitle: @"" size: 15 subtitleSize: 0];        //lokalised 22/1
    else
        [Helper setNavigationBarTitle: self title: LOCALIZATION(C_TITLE_MECHPERFORMANCE) subtitle: @"" size: 15 subtitleSize: 0];       //lokalised 22/1
    
    dashboardViewController = [STORYBOARD_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_PERFORMANCE_DASHBOARD_NEW];
    dashboardViewController.isTab = true;
    UINavigationController *perfNav = [[UINavigationController alloc]initWithRootViewController: dashboardViewController];
    [perfNav setNavigationBarHidden: YES];
    
    leaderPerformanceViewController  = [STORYBOARD_IN_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_IN_PERFORMANCE_LEADERBOARD];
    UINavigationController *leaderPerfNav = [[UINavigationController alloc] initWithRootViewController: leaderPerformanceViewController];
    [leaderPerfNav setNavigationBarHidden: YES];
    
    myRankingViewController  = [STORYBOARD_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_LEADERBOARD];
    UINavigationController *myRankingNav = [[UINavigationController alloc] initWithRootViewController: myRankingViewController];
    [myRankingNav setNavigationBarHidden: YES];
    
    myPerformanceViewController  = [STORYBOARD_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_MYPERFORMANCE];
    thRetailMyPerformanceViewController = [STORYBOARD_THRetail instantiateViewControllerWithIdentifier:VIEW_TH_MYPERFORMANCE];
    
    UINavigationController *myPerformanceNav;
    if(![GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND]) {
        myPerformanceNav = [[UINavigationController alloc] initWithRootViewController: myPerformanceViewController];
    } else {
        myPerformanceNav = [[UINavigationController alloc] initWithRootViewController: thRetailMyPerformanceViewController];
    }
    [myPerformanceNav setNavigationBarHidden: YES];
    
    NSMutableArray *title = [[NSMutableArray alloc] init];
    NSMutableArray *navControllers = [[NSMutableArray alloc] init];
    
    if(GET_PROFILETYPE == kPROFILETYPE_DMR) {
        
        if ([[mSession profileInfo] IsPerfOpt1])
        {
            [title addObject: LOCALIZATION(C_TITLE_PERFORMANCE)];       //lokalised 22/1
            [navControllers addObject: myPerformanceNav];
        }
        else if ([[mSession profileInfo] IsPerfOpt2])
        {
            [title addObject: LOCALIZATION(C_PERFORMANCE_SUMMARY)];     //lokalised 22/1
            [navControllers addObject: perfNav];
        }
        else
        {
            [title addObject: LOCALIZATION(C_TITLE_PERFORMANCE)];       //lokalised 22/1
            [navControllers addObject: myPerformanceNav];
        }
        
//        if ([[mSession profileInfo] hasLeaderBoard])
        {
            [title addObject: LOCALIZATION(C_PERFORMANCE_LEADERBOARD)]; //lokalised 22/1
            [navControllers addObject: leaderPerfNav];
        }
    }
    else
    {
        if(GET_PROFILETYPE == kPROFILETYPE_DSR)
        {
            if ([[mSession profileInfo] IsPerfOpt1])
            {
                [title addObject: LOCALIZATION(C_TITLE_PERFORMANCE)];   //lokalised 22/1
                [navControllers addObject: myPerformanceNav];
            }
            else if ([[mSession profileInfo] IsPerfOpt2])
            {
                [title addObject: LOCALIZATION(C_PERFORMANCE_SUMMARY)]; //lokalised 22/1
                [navControllers addObject: perfNav];
            }
            else
            {
                [title addObject: LOCALIZATION(C_TITLE_PERFORMANCE)];   //lokalised 22/1
                [navControllers addObject: myPerformanceNav];
            }
            
            if ([[mSession profileInfo] hasLeaderBoard])
            {
                [title addObject: LOCALIZATION(C_PERFORMANCE_MYRANKING)];   //lokalised 22/1
                [navControllers addObject: myRankingNav];
            }
            
            if ([[mSession profileInfo] hasWorkshopPerformanceView])
            {
                [title addObject: LOCALIZATION(C_PERFORMANCE_LEADERBOARD)]; //lokalised 22/1
                [navControllers addObject: leaderPerfNav];
            }
        }
        else
        {
            performanceWorkshopViewController  = [STORYBOARD_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_PERFORMANCEWORKSHOP];
            UINavigationController *performanceWorkshopNav = [[UINavigationController alloc] initWithRootViewController: performanceWorkshopViewController];
            if ([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_THAILAND]) {
                thRetailMyPerformanceViewController = [STORYBOARD_THRetail instantiateViewControllerWithIdentifier:VIEW_TH_MYPERFORMANCE];
                performanceWorkshopNav = [[UINavigationController alloc] initWithRootViewController: thRetailMyPerformanceViewController];
            }
            [performanceWorkshopNav setNavigationBarHidden: YES];
            
            if ([[mSession profileInfo] IsPerfOpt1])
            {
                //old performance screen
                [title addObject: LOCALIZATION(C_TITLE_PERFORMANCE)];       //lokalised 22/1
                [navControllers addObject: performanceWorkshopNav];
            }
            else if ([[mSession profileInfo] IsPerfOpt2])
            {
//                if (GET_ISIMPROFILETYPE)
                [title addObject: LOCALIZATION(C_PERFORMANCE_SUMMARY)];     //lokalised 22/1
//                else
//                    [title addObject: LOCALIZATION(C_TITLE_PERFORMANCE)]; //lokalised 22/1
                [navControllers addObject: perfNav];
            }
            else   
            {
                //old performance screen
                [title addObject: LOCALIZATION(C_TITLE_PERFORMANCE)];       //lokalised 22/1
                [navControllers addObject: performanceWorkshopNav];
            }
            
            if ([[mSession profileInfo] hasMYPoints] && !IS_COUNTRY(COUNTRYCODE_INDONESIA))
            {
                pointViewController  = [STORYBOARD_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_PERFORMANCEPOINTS];
                UINavigationController *pointNav = [[UINavigationController alloc] initWithRootViewController: pointViewController];
                [pointNav setNavigationBarHidden: YES];
                [title addObject: LOCALIZATION(C_TITLE_RECENTACTIVITY)];
                [navControllers addObject: pointNav];
            } else {
                coinsPerformanceController  = [STORYBOARD_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_MYCOIN_PERFORMANCE];
                UINavigationController *performancePointsNav = [[UINavigationController alloc] initWithRootViewController: coinsPerformanceController];
                [performancePointsNav setNavigationBarHidden: YES];
                [title addObject: LOCALIZATION(C_TITLE_MYCOINS)];
                [navControllers addObject: performancePointsNav];
            }
        }
    }
    for (UINavigationController *navCtrl in navControllers)
    {
        [navCtrl setNavigationBarHidden: YES];
    }
//        {
//            pointViewController  = [STORYBOARD_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_PERFORMANCEPOINTS];
//            UINavigationController *pointNav = [[UINavigationController alloc] initWithRootViewController: pointViewController];
//            [pointNav setNavigationBarHidden: YES];
//
//            title = @[LOCALIZATION(C_PERFORMANCE_SUMMARY),    //lokalised 22/1
//                      LOCALIZATION(C_TITLE_RECENTACTIVITY),
//                      ];
//
//            [self setViewControllers:@[perfNav,
//                                       pointNav,
//                                       ]];
//        }
//    }

    if (kIsRightToLeft) {
//        navControllers = navControllers.reverseObjectEnumerator.allObjects.mutableCopy;
        title = title.reverseObjectEnumerator.allObjects.mutableCopy;
    }
    [self setViewControllers: navControllers];
    UIImage *selected = GET_ISADVANCE ? [UIImage imageNamed:@"tab-bg_blue"] : [UIImage imageNamed:@"tab-bg"];
    UIImage *unselected = [UIImage imageNamed:@"tab-bg_grey.png"];
    for (int i = 0; i < [self.tabBar.items count]; i++)
    {
        RDVTabBarItem *item = [self.tabBar.items objectAtIndex: i];
        [item setTitle: [title objectAtIndex: i]];
        
        [item setBackgroundColor: COLOUR_WHITE];
        [item setTitleUnselectedAttributes:FONT_H1 colour:COLOUR_VERYDARKGREY];
        [item setTitleSelectedAttributes:FONT_H1 colour:COLOUR_RED];
        
        [item setBackgroundSelectedImage: selected withUnselectedImage: unselected];
    }
    
    if ([navControllers count] == 1)
    {
        
        if ([[[[navControllers firstObject] childViewControllers] firstObject] isKindOfClass: [dashboardViewController class]])
            dashboardViewController.isTab = NO;
        [mSession loadVCWithSidePanelWithVc: [[[navControllers firstObject] childViewControllers] firstObject]];
    }
    if (kIsRightToLeft) {
        [self tabBar:self.tabBar didSelectItemAtIndex:navControllers.count-1];
    }
}

#pragma mark - RDVTabBarDelegate
- (BOOL)tabBar:(RDVTabBar *)tabBar shouldSelectItemAtIndex:(NSInteger)index
{
    return ([tabBar selectedItem] != [[tabBar items] objectAtIndex: index]);
}

- (void)tabBar:(RDVTabBar *)tabBar didSelectItemAtIndex:(NSInteger)index
{
    //super method MUST BE PRESENT
    [super tabBar:tabBar didSelectItemAtIndex: index];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
