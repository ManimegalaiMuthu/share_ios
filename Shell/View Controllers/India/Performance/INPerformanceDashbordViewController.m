//
//  INPerformanceDashbordViewController.m
//  Shell
//
//  Created by Nach on 29/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "INPerformanceDashbordViewController.h"

@interface INPerformanceDashbordViewController ()<WebServiceManagerDelegate,UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *INPerformanceTableView;

@property NSArray *performanceArray;
@end

@implementation INPerformanceDashbordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.INPerformanceTableView.tableFooterView = [UIView new];
    
    [[WebServiceManager sharedInstance] fetchPerformanceV2: self];
}


#pragma mark -  WebService Manager
- (void)processCompleted:(WebServiceResponse *)response {
    switch (response.webserviceCall) {
        case kWEBSERVICE_FETCH_PERFORMANCE_V2:
        {
            NSDictionary *respData = [response getGenericResponse];
            
            self.performanceArray = [respData objectForKey: @"Performance"];
            _INPerformanceTableView.hidden = NO;
            [_INPerformanceTableView reloadData];
        }
            break;
        default:
            break;
    }
    
}

- (void)processFailed:(WebServiceResponse *)response {
    switch (response.webserviceCall) {
        case kWEBSERVICE_FETCH_PERFORMANCE_V2:
        {
            _INPerformanceTableView.hidden = YES;
        }
            break;
        default:
            break;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - tableView
enum
{
    DASHBOARD_TABLECONTENT_IMAGE = 1,
    DASHBOARD_TABLECONTENT_BODY,
    DASHBOARD_TABLECONTENT_DATA,
    DASHBOARD_TABLECONTENT_LINE,
    DASHBOARD_TABLECONTENT_ARROW,
} DASHBOARD_TABLECONTENT_TAG;

enum{
    IN_PERFORMANCE_DASHBOARD_TABLECONTENT_HEADER = 0,
    IN_PERFORMANCE_DASHBOARD_TABLECONTENT_BODY = 1,
    IN_PERFORMANCE_DASHBOARD_TABLECONTENT_FOOTER,
}IN_PERFORMANCE_DASHBOARD_TABLECONTENT_TAG;

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //Content + Header + Footer
    return 3;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch(section) {
        case IN_PERFORMANCE_DASHBOARD_TABLECONTENT_HEADER:
            return 1;
        case IN_PERFORMANCE_DASHBOARD_TABLECONTENT_BODY:
            return _performanceArray.count;
        case IN_PERFORMANCE_DASHBOARD_TABLECONTENT_FOOTER:
            return 1;
        default:
            return 0;
    }
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell;
    switch(indexPath.section) {
        case IN_PERFORMANCE_DASHBOARD_TABLECONTENT_HEADER:
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"INPerformanceDashboardHeaderCell"];
            UILabel *lblHeader = [cell viewWithTag: 1];
            lblHeader.text = LOCALIZATION(C_RU_PERFORMANCE_DASHBOARD_HEADER);   //lokalised 28 Jan
        }
            break;
        case IN_PERFORMANCE_DASHBOARD_TABLECONTENT_BODY:
        {
            NSDictionary *cellData = [self.performanceArray objectAtIndex: indexPath.row];
            
            cell = [tableView dequeueReusableCellWithIdentifier: @"INPerformanceDashboardTableViewCell"];
            
            UIImageView *imgView = [cell viewWithTag: DASHBOARD_TABLECONTENT_IMAGE];
            if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Order BreakDown"])
                imgView.image = [UIImage imageNamed: @"icn_orders_verydarkgrey"];
            else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Points Earned BreakDown"])
                imgView.image = [UIImage imageNamed: @"icn_points_verydarkgrey"];
            else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Customer Break Down"])
                imgView.image = [UIImage imageNamed: @"icn_consumers_verydarkgrey"];
            else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Oil Change BreakDown"])
                imgView.image = [UIImage imageNamed: @"icn_oilchange_verydarkgrey"];
            else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Product Sold Breakdown"])
                           imgView.image = [UIImage imageNamed: @"icn_inventory_verydarkgrey"];
            
            else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"ApprovedWorkshop"])
                                    imgView.image = [UIImage imageNamed: @"icn_totalworkshops_verydarkgrey"];
               
            
         
            UILabel *lblBody = [cell viewWithTag: DASHBOARD_TABLECONTENT_BODY];
            lblBody.text = [cellData objectForKey: @"PerformanceKey"];
            
            UILabel *lblData = [cell viewWithTag: DASHBOARD_TABLECONTENT_DATA];
            lblData.textColor = COLOUR_RED;
            lblData.text = [cellData objectForKey: @"PerformanceValue"];
            
            UIImageView *arrowImgView = [cell viewWithTag: DASHBOARD_TABLECONTENT_ARROW];
            arrowImgView.hidden = ![[cellData objectForKey: @"isClickable"] boolValue];
            if (kIsRightToLeft) {
                arrowImgView.image = [UIImage imageNamed:@"icon-greyarrL.png"];
            } else {
                arrowImgView.image = [UIImage imageNamed:@"icon-greyarrR.png"];
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
            break;
        case IN_PERFORMANCE_DASHBOARD_TABLECONTENT_FOOTER:
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"INPerformanceDashboardFooterCell"];
            UILabel *lblFooter = [cell viewWithTag: 1];
            lblFooter.text = LOCALIZATION(C_RU_PERFORMANCE_DASHBOARD_FOOTER);   //lokalised 28 Jan
            
        }
            break;
        default:
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == IN_PERFORMANCE_DASHBOARD_TABLECONTENT_BODY) {
        NSDictionary *cellData = [self.performanceArray objectAtIndex: indexPath.row];
        if (![[cellData objectForKey: @"isClickable"] boolValue])
            return;
        
        NSString *navKey = [cellData objectForKey: @"OnClickRedirectPath"];
        
        if ([navKey isEqualToString: @"Order BreakDown"])
        {
            [mSession pushOrderBreakdown: self.parentViewController.parentViewController];
        }
        else if ([navKey isEqualToString: @"Staff BreakDown"])
        {
            [mSession pushStaffBreakdown:self.parentViewController.parentViewController];
        }
        else if ([navKey isEqualToString: @"Oil Change BreakDown"])
        {
            [mSession pushINBreakdown:NO vc:self.parentViewController.parentViewController];
        }
        else if ([navKey isEqualToString: @"Customer Break Down"])
        {
            [mSession pushINCustomerBreakdown:self.parentViewController.parentViewController];
        }
        else if ([navKey isEqualToString: @"Offer Redemption BreakDown"])
        {
            [mSession pushOfferRedemptionBreakdown:self.parentViewController.parentViewController];
        }
        else if([navKey isEqualToString:@"Points Earned BreakDown"]) {
            [mSession pushPointsBreakdown: self.parentViewController.parentViewController];
        }
        else if ([navKey isEqualToString: @"Mech Break Down"])
        {
            [mSession pushINMechBreakdown: self.parentViewController.parentViewController];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0;
}

@end
