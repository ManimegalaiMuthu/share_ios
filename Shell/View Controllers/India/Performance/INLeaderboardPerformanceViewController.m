//
//  INLeaderboardPerformanceViewController.m
//  Shell
//
//  Created by Nach on 30/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "INLeaderboardPerformanceViewController.h"

@interface INLeaderboardPerformanceViewController () <UITableViewDelegate, UITableViewDataSource, WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *leaderboardTableView;
@property (weak, nonatomic) IBOutlet UILabel *lblFooter;

@property NSMutableArray *sectionOpenControlArray; //to control array of BOOL to control sections open/close

@property NSMutableArray *tableData;
@property NSMutableArray *tableHeaders;

@end

@implementation INLeaderboardPerformanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lblFooter.text = LOCALIZATION(C_RU_LEADERBOARD_DSRFOOTER); //lokalised 28 Jan
    
    if(@available(iOS 11, *))
    {
        self.leaderboardTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.leaderboardTableView setContentInset:UIEdgeInsetsMake(15, 0, 15, 0)];
    
    self.leaderboardTableView.backgroundColor = [UIColor clearColor];
    self.leaderboardTableView.backgroundView = nil;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
         [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_ANALYTICS),LOCALIZATION_EN(C_PERFORMANCE_LEADERBOARD)] screenClass:nil];
        [[WebServiceManager sharedInstance] fetchLeaderboard: @{@"ViewType": @(1)} vc:self];
    } else {
        [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_IN_MECHPERFORMANCE),LOCALIZATION_EN(C_PERFORMANCE_LEADERBOARD)] screenClass:nil];
        [[WebServiceManager sharedInstance] fetchLeaderboard: @{@"ViewType": @(2)} vc:self];
    }
}


#pragma mark - Webservice Manager
-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_FETCH_LEADERBOARD:
        {
            NSDictionary *respData = [response getGenericResponse];
            self.sectionOpenControlArray = [[NSMutableArray alloc] init];
            self.tableData = [[NSMutableArray alloc] init];
            self.tableHeaders = [[NSMutableArray alloc] init];
            
            if (![[respData objectForKey: @"OrderVolume"]isKindOfClass:[NSNull class]])
            {
                [self.sectionOpenControlArray addObject: @(NO)];
                [self.tableData addObject:[respData objectForKey: @"OrderVolume"]];
                [self.tableHeaders addObject:[respData objectForKey: @"OrderVolumeHeaderText"]];
            }
            
            if (![[respData objectForKey: @"OilChange"] isKindOfClass:[NSNull class]])
            {
                [self.sectionOpenControlArray addObject: @(NO)];
                [self.tableData addObject:[respData objectForKey: @"OilChange"]];
                [self.tableHeaders addObject:[respData objectForKey: @"OilChangeHeaderText"]];
            }
//            if (![[respData objectForKey: @"InvitationSent"] isKindOfClass:[NSNull class]])
//            {
//                [self.sectionOpenControlArray addObject: @(NO)];
//                [self.tableData addObject:[respData objectForKey: @"InvitationSent"]];
//                [self.tableHeaders addObject:[respData objectForKey: @"InvitationSentHeaderText"]];
//            }
            if (![[respData objectForKey: @"CustomerSignUps"] isKindOfClass:[NSNull class]])
            {
                [self.sectionOpenControlArray addObject: @(NO)];
                [self.tableData addObject:[respData objectForKey: @"CustomerSignUps"]];
                [self.tableHeaders addObject:[respData objectForKey: @"CustomerSignupHeaderText"]];
            }
            self.lblFooter.text = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_RU_LEADERBOARD_DSRFOOTER), [respData objectForKey: @"UpdateText"]]; //lokalised 28 Jan
            
            if ([self.sectionOpenControlArray count] == 0)
            {
                self.sectionOpenControlArray = nil;
            }
            
            [self.leaderboardTableView reloadData];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - tableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sectionOpenControlArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[self.sectionOpenControlArray objectAtIndex: section] boolValue])
    {
        NSArray *dataArray = [self.tableData objectAtIndex:section];
        return dataArray.count;
    }
    
    return 0;
}


enum
{
    kLEADERBOARD_TAGS_Plus = 9999,
    kLEADERBOARD_TAGS_Button = 10000,
    kLEADERBOARD_TAGS_Header = 10001,
    kLEADERBOARD_TAGS_Rank,
    kLEADERBOARD_TAGS_Daily, //imageview also same tag
    kLEADERBOARD_TAGS_Mechanic,
    kLEADERBOARD_TAGS_Number,
} kLEADERBOARD_IN_TAGS_TYPE;

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell;
    if ([[self.sectionOpenControlArray objectAtIndex: section] boolValue])
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"FullHeaderCell"];
        UIImageView *imgPlus = [cell viewWithTag: kLEADERBOARD_TAGS_Plus];
        imgPlus.image = GET_ISADVANCE ? [UIImage imageNamed:@"btn_plus_blue"] : [UIImage imageNamed:@"btn_plus_red"];
        imgPlus.transform = CGAffineTransformRotate(CGAffineTransformIdentity, M_PI_4);
        
        UILabel *lblRankHeader = [cell viewWithTag: kLEADERBOARD_TAGS_Rank];
        lblRankHeader.text = LOCALIZATION(C_RU_LEADERBOARD_RANK);               //lokalised 28 Jan
        
        UILabel *lblDailyHeader = [cell viewWithTag: kLEADERBOARD_TAGS_Daily];
        lblDailyHeader.text = LOCALIZATION(C_RU_LEADERBOARD_DAILY);             //lokalised 28 Jan
        
        UILabel *lblMechanicHeader = [cell viewWithTag: kLEADERBOARD_TAGS_Mechanic];
        lblMechanicHeader.text = LOCALIZATION(C_RU_STAFFLEADERBOARD_MECHANIC);  //lokalised 28 Jan
        if(GET_PROFILETYPE == kPROFILETYPE_DSR)
            lblMechanicHeader.text = LOCALIZATION(C_RU_PERFORMANCE_WORKSHOP);
        
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"HeaderCell"];
    }
    UILabel *lblSectionHeader = [cell viewWithTag: kLEADERBOARD_TAGS_Header];
    lblSectionHeader.text = [_tableHeaders objectAtIndex:section];
    
    UIButton *btnHeader = [cell viewWithTag: kLEADERBOARD_TAGS_Button]; //10000 set in storyboard
    btnHeader.tag = section;
    
    return cell.contentView;
}

- (IBAction)headerPressed:(UIButton *)sender
{
    BOOL newControl = ![[self.sectionOpenControlArray objectAtIndex: sender.tag] boolValue];
    [self.sectionOpenControlArray replaceObjectAtIndex: sender.tag withObject: @(newControl)];
    [self.leaderboardTableView reloadData];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"BodyCell"];
    NSArray *dataArray = [self.tableData objectAtIndex:indexPath.section];
    NSDictionary *cellData = [dataArray objectAtIndex: indexPath.row];
    
    UILabel *lblRank = [cell viewWithTag: kLEADERBOARD_TAGS_Rank];
    lblRank.text = [cellData objectForKey: @"Rank"];
    
    UILabel *lblPerformanceArrow = [cell viewWithTag: kLEADERBOARD_TAGS_Daily];
    if ([[cellData objectForKey: @"PerformanceFlag"] isEqualToString: @"1"])
    {
        lblPerformanceArrow.text = @"▲";
        lblPerformanceArrow.textColor = COLOUR_DARKGREEN;
    }
    else
    {
        lblPerformanceArrow.text = @"▼";
        lblPerformanceArrow.textColor = COLOUR_RED;
    }
    
    UILabel *lblMechanic = [cell viewWithTag: kLEADERBOARD_TAGS_Mechanic];
    lblMechanic.text = [cellData objectForKey: @"Name"];
    
    UILabel *lblNumber = [cell viewWithTag: kLEADERBOARD_TAGS_Number];
    lblNumber.text = [cellData objectForKey: @"TotalValue"];
    
    return cell;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
