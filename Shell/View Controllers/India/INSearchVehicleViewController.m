//
//  INSearchVehicleViewController.m
//  Shell
//
//  Created by Nach on 4/12/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <Photos/Photos.h>
#import "SWGApiClient.h"
#import "SWGConfiguration.h"
// load models
#import "SWGCoordinate.h"
#import "SWGInlineResponse200.h"
#import "SWGInlineResponse200ProcessingTime.h"
#import "SWGInlineResponse400.h"
#import "SWGPlateCandidate.h"
#import "SWGPlateDetails.h"
#import "SWGRegionOfInterest.h"
#import "SWGVehicleCandidate.h"
#import "SWGVehicleDetails.h"
// load API classes for accessing endpoints
#import "SWGDefaultApi.h"

#import "INSearchVehicleViewController.h"
#import "RegisterCustomerCompleteViewController.h"

#define CELL_HEIGHT 70

@interface INSearchVehicleViewController () <WebServiceManagerDelegate,UITextFieldDelegate, TTTAttributedLabelDelegate, UITableViewDelegate, UITableViewDataSource, RegisterCustomerCompleteViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtSearchVehicle;
@property (weak, nonatomic) IBOutlet UILabel *lblConsumerName;

@property (weak, nonatomic) IBOutlet UIView *vehicleDetailsView;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleId;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleIDData;

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblInviteConsumer; //Hidden in StoryBoard


@property (weak, nonatomic) IBOutlet UIView *vehicleResultsView;//Hidden in Storyboard
@property (weak, nonatomic) IBOutlet UILabel *lblServicingHistoryHeader;
@property (weak, nonatomic) IBOutlet UITableView *resultsTableView;

@property (weak, nonatomic) IBOutlet UIButton *btnRegisterProductCode;

@property (weak, nonatomic) IBOutlet UIView *consumerDetailsView;
@property (weak, nonatomic) IBOutlet UILabel *lblMemberSince;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consumerDetailViewHeight;

@property (weak, nonatomic) IBOutlet UILabel *btmLine;

@property (weak, nonatomic) IBOutlet UIView *vehicleTypeView;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleType;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleTypeData;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vehicleDetailsViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vehicleTypeViewHeight;

@property NSMutableArray *resultArray;
@property NSMutableArray *offerArray;

@property NSMutableArray *promotionDetailArray;

@end

@implementation INSearchVehicleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_SEARCHVEHICLE) subtitle: @""];       //lokalise 23 Jan
    
    self.resultArray = [[NSMutableArray alloc] init];
    self.offerArray = [[NSMutableArray alloc] init];
    
    self.resultsTableView.tableFooterView = [UIView new];
    
    [self setupInterface];
}


-(void) setupInterface {
    self.txtSearchVehicle.font = FONT_B1;
    self.txtSearchVehicle.placeholder = LOCALIZATION(C_SEARCHFIELD_VEHICLEID);              //lokalise 23 Jan
    if (kIsRightToLeft) {
        [self.txtSearchVehicle setTextAlignment:NSTextAlignmentRight];
    } else {
        [self.txtSearchVehicle setTextAlignment:NSTextAlignmentLeft];
    }
    
    self.btnRegisterProductCode.titleLabel.font = FONT_BUTTON;
    [self.btnRegisterProductCode setBackgroundColor: COLOUR_RED];
    [self.btnRegisterProductCode setTitle:LOCALIZATION(C_SEARCHVEHICLE_REGISTERPRODUCTCODE) forState:UIControlStateNormal];     //lokalise 23 Jan
    
    self.btnRegisterProductCode.hidden = [[mSession profileInfo] hasShowScanSearchVehicle]? NO : YES;
    
    self.lblConsumerName.font = FONT_B1;
    self.lblConsumerName.textColor = COLOUR_VERYDARKGREY;
    self.lblVehicleIDData.font = FONT_B1;
    self.lblVehicleIDData.textColor = COLOUR_VERYDARKGREY;
    
    self.lblVehicleId.font = FONT_H1;
    self.lblVehicleId.textColor = COLOUR_VERYDARKGREY;
    self.lblVehicleId.text = LOCALIZATION(C_SEARCHVEHICLE_VEHICLEID);          //loaklise 23 Jan
    
    self.lblServicingHistoryHeader.font = FONT_H1;
    self.lblServicingHistoryHeader.textColor = COLOUR_VERYDARKGREY;
    self.lblServicingHistoryHeader.text = LOCALIZATION(C_SEARCHVEHICLE_SERVICINGHISTORY);       //lokalise 23 Jan
    
    self.lblInviteConsumer.font = FONT_H1;
    self.lblInviteConsumer.textColor = COLOUR_VERYDARKGREY;
    self.lblInviteConsumer.text = LOCALIZATION(C_ALERTHEADER_INVITECONSUMER);           //lokalise 23 Jan
    
    self.consumerDetailsView.hidden = YES;
    self.consumerDetailViewHeight.constant = 0;
    
    self.lblMemberSince.font = FONT_B2;
    
    self.lblInviteConsumer.delegate = self;
    if (kIsRightToLeft) {
        [self.lblInviteConsumer setTextAlignment:NSTextAlignmentRight];
    } else {
        [self.lblInviteConsumer setTextAlignment:NSTextAlignmentLeft];
    }
    [Helper setHyperlinkLabel:self.lblInviteConsumer hyperlinkText:LOCALIZATION(C_INVITE_CUSTOMER) bodyText:LOCALIZATION(C_INVITE_CUSTOMER) urlString: @"Invite"];      //lokalise 23 Jan
    
    self.btmLine.backgroundColor = COLOUR_RED;
    
    self.vehicleTypeView.hidden = YES;
    self.vehicleDetailsViewHeight.constant = 50.0;
    self.vehicleTypeViewHeight.constant = 0.0;
}

- (IBAction)registerProductCodePressed:(id)sender
{
    [mSession pushScanProductView: self.txtSearchVehicle.text previousState: @"" vc:self];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear: animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_SEARCHVEHICLE) screenClass:nil];

}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url absoluteString] isEqualToString: @"Invite"])
        [self inviteConsumer];
}

-(void) inviteConsumer
{
    [mSession pushRegisterConsumer:@{@"VehicleNumber": self.txtSearchVehicle.text,
                                     @"VehicleStateID": @"",
                                     @"PromoCode": @"",
                                     }
                                vc: self]; //no promo code
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [[WebServiceManager sharedInstance] searchVehicle: self.txtSearchVehicle.text state: @"" vc:self];
}

- (IBAction)redeemPressed:(UIButton *) sender
{
    [mSession pushPromoCodeView: self];
}

#pragma mark - UITableView
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell;
    
    if (section == 0 && [self.resultArray count] > 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"HeaderCell"];
        
        UILabel *lblDate = [cell viewWithTag: 1];
        lblDate.text = LOCALIZATION(C_SEARCHVEHICLE_SERVICEDATE);           //lokalise 23 Jan
        lblDate.font = FONT_H2;
        lblDate.textColor = COLOUR_VERYDARKGREY;
        
        UILabel *lblDateFormat = [cell viewWithTag: 2];
        lblDateFormat.font = FONT_B3;
        lblDateFormat.text = LOCALIZATION(C_RUSSIA_DATEFORMAT);             //lokalise 23 Jan
        lblDateFormat.textColor = COLOUR_VERYDARKGREY;
        
        
        UILabel *lblProductUsed = [cell viewWithTag: 3];
        lblProductUsed.font = FONT_H2;
        lblProductUsed.text = LOCALIZATION(C_SEARCHVEHICLE_PRODUCTUSED);        //lokalise 23 Jan
        lblProductUsed.textColor = COLOUR_VERYDARKGREY;
        
        UILabel *lblMileage = [cell viewWithTag: 4];
        lblMileage.font = FONT_H2;
        lblMileage.text = LOCALIZATION(C_SEARCHVEHICLE_MILEAGE);                //lokalise 23 Jan
        lblMileage.textColor = COLOUR_VERYDARKGREY;
        
        UILabel *lblLastServiceBy = [cell viewWithTag: 5];
        lblLastServiceBy.font = FONT_H2;
        lblLastServiceBy.text = LOCALIZATION(C_SEARCHVEHICLE_SERVICEBY);        //lokalise 23 Jan
        lblLastServiceBy.textColor = COLOUR_VERYDARKGREY;
    }
    else if ((section == 1 && [self.offerArray count] > 0) || (section == 0 && [self.offerArray count] > 0))
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"INOfferHeaderCell"];
        
        UILabel *lblEntitledOffer = [cell viewWithTag: 1];
        lblEntitledOffer.text = LOCALIZATION(C_SEARCHVEHICLE_ENTITLEDOFFER);        //lokalise 23 Jan
        
        UILabel *lblOfferDetails = [cell viewWithTag: 2];
        lblOfferDetails.text = LOCALIZATION(C_SEARCHVEHICLE_OFFERDETAILS);          //lokalise 23 Jan
        
        UILabel *lblValidity = [cell viewWithTag: 3];
        lblValidity.text = LOCALIZATION(C_SEARCHVEHICLE_OFFERVALIDITY);             //lokalise 23 Jan
        
        UIButton *btnRedeem = [cell viewWithTag: 4];
        [btnRedeem.titleLabel setFont: FONT_B2];
        [btnRedeem.titleLabel setNumberOfLines:0];
        [btnRedeem setTitle: LOCALIZATION(C_SEARCHVEHICLE_OFFERREDEEM) forState: UIControlStateNormal];         //lokalise 23 Jan
        [btnRedeem sizeToFit];
        
        UIView *line = [cell viewWithTag: 999];
        line.backgroundColor = COLOUR_RED;
        
        if([[mSession profileInfo] hasCustomerRedemptionBtn]){
            [btnRedeem setHidden:NO];
        } else {
            [btnRedeem setHidden:YES];
        }
        
        UIView *redLineView = [cell viewWithTag:999];
        if(section == 0) {
            redLineView.hidden = YES;
            self.lblServicingHistoryHeader.hidden = YES;
            [NSLayoutConstraint constraintWithItem:self.lblServicingHistoryHeader attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0].active = YES;
        } else {
            redLineView.hidden = NO;
            self.lblServicingHistoryHeader.hidden = NO;
             [NSLayoutConstraint constraintWithItem:self.lblServicingHistoryHeader attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:21.0].active = YES;
        }
            
    }
    return cell.contentView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0 && [self.resultArray count] > 0)
        return [self.resultArray count];
    else if (section == 1 && [self.offerArray count] > 0)
        return [self.offerArray count];
    else if(section == 0 && [self.offerArray count] > 0)
        return [self.offerArray count];
    
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [UITableViewCell new];
    
    if (indexPath.section == 0 && [self.resultArray count] > 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"ResultCell"];
        
        NSDictionary *cellData = [self.resultArray objectAtIndex: indexPath.row];
        
        UILabel *lblDate = [cell viewWithTag: 1];
        lblDate.font = FONT_B2;
        lblDate.textColor = COLOUR_VERYDARKGREY;
        lblDate.text = [cellData objectForKey: @"ServiceDate"];
        
        
        UILabel *lblProductUsed = [cell viewWithTag: 2];
        lblProductUsed.font = FONT_B2;
        lblProductUsed.textColor = COLOUR_VERYDARKGREY;
        lblProductUsed.text = [cellData objectForKey: @"LastUsedProduct"];
        
        UILabel *lblMileage = [cell viewWithTag: 3];
        lblMileage.font = FONT_B2;
        lblMileage.textColor = COLOUR_VERYDARKGREY;
        lblMileage.text = [cellData objectForKey: @"LastMileage"];
        
        UILabel *lblLastServiceByData = [cell viewWithTag: 4];
        lblLastServiceByData.font = FONT_B2;
        lblLastServiceByData.textColor = COLOUR_VERYDARKGREY;
        lblLastServiceByData.text = [cellData objectForKey: @"LastServiceBy"];
    }
    else if ((indexPath.section == 1 && [self.offerArray count] > 0) || (indexPath.section == 0 && [self.offerArray count] > 0))
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"INOfferCell"];
        
        NSDictionary *cellData = [self.offerArray objectAtIndex: indexPath.row];
        
        UILabel *lblPromo = [cell viewWithTag: 1];
        lblPromo.text = [cellData objectForKey: @"PromotionOffer"];
        
        UILabel *lblValidity = [cell viewWithTag: 2];
        lblValidity.text = [cellData objectForKey: @"Validity"];
    }
    
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([self.offerArray count] > 0 && [self.resultArray count] > 0)
        return 2; //show offer table
    else
        return 1;
}

#pragma mark - WebService
-(void)processCompleted:(WebServiceResponse *)response
{
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_SEARCHVEHICLE:
        {
            NSDictionary *searchData = [response getGenericResponse];
            
            self.resultArray = [searchData objectForKey: @"VehicleDetails"];
            
            //consumer name and vehicle MUST be separate
            //vehicle CAN be registered but no records
            self.lblInviteConsumer.hidden = ![[searchData objectForKey:@"CanInvite"] boolValue];
            self.btnRegisterProductCode.hidden = ![[searchData objectForKey:@"ShowRegProductCodeBtn"] boolValue];
            
            //check for null data
            if (![[searchData objectForKey: @"VehicleDetails"] isKindOfClass:[NSNull class]])
            {
                self.vehicleResultsView.hidden = NO;
                
                if(![[searchData objectForKey:@"ConsumerName"] isKindOfClass:[NSNull class]] && [searchData objectForKey:@"ConsumerName"] != nil){
                    NSString *GGVdriverNumber = @"";
                    if(![[searchData objectForKey:@"GGVDriverNumber"] isKindOfClass: [NSNull class]]) {
                        GGVdriverNumber = [searchData objectForKey:@"GGVDriverNumber"];
                    }
                    if([GGVdriverNumber isEqualToString:@""]) {
                        self.lblConsumerName.text = [searchData objectForKey:@"ConsumerName"];
                    } else {
                        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[searchData objectForKey:@"ConsumerName"] attributes:nil];        //lokalise 23 Jan
                        NSMutableAttributedString *driverNumberString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",GGVdriverNumber] attributes:@{NSForegroundColorAttributeName: COLOUR_DARKGREY,NSFontAttributeName : FONT_H1}];
                        [attrString appendAttributedString:driverNumberString];
                        self.lblConsumerName.attributedText = attrString;
                    }
                    
                    NSString *memberSinceValue = [searchData objectForKey:@"ConsumerJoinDate"];
                    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(C_VEHICLESEARCH_MEMBERSINCE) attributes:nil];        //lokalise 23 Jan
                    NSMutableAttributedString *dateString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",memberSinceValue] attributes:@{NSForegroundColorAttributeName: COLOUR_RED,NSFontAttributeName : FONT_H2}];
                    [attrString appendAttributedString:dateString];
                    self.lblMemberSince.attributedText = attrString;
                    
                    if([[mSession profileInfo] hasVehicleType] && self.lblInviteConsumer.isHidden) {
                        self.vehicleTypeView.hidden = NO;
                        self.vehicleDetailsViewHeight.constant = 60.0;
                        self.vehicleTypeViewHeight.constant = 21.0;
                        
                        self.lblVehicleType.text = LOCALIZATION(C_FORM_VEHICLETYPE);
                        self.lblVehicleType.textColor = COLOUR_VERYDARKGREY;
                        self.lblVehicleType.font = FONT_H1;
                        
                        if([[searchData objectForKey:@"vehicleType"] isKindOfClass:[NSNull class]] || [searchData objectForKey:@"VehicleType"] == nil) {
                            self.lblVehicleTypeData.text = @"";
                        } else {
                            self.lblVehicleTypeData.text = [searchData objectForKey:@"VehicleType"];
                        }
                        self.lblVehicleTypeData.textColor = COLOUR_VERYDARKGREY;
                        self.lblVehicleTypeData.font = FONT_B1;
                        
                    } else {
                        self.vehicleTypeView.hidden = YES;
                        self.vehicleDetailsViewHeight.constant = 50.0;
                        self.vehicleTypeViewHeight.constant = 0.0;
                    }
                    
                    self.consumerDetailsView.hidden = NO;
                    self.consumerDetailViewHeight.constant = 45;
                    [self.view layoutSubviews];
                } else {
                    self.consumerDetailsView.hidden = YES;
                    self.consumerDetailViewHeight.constant = 15;
                }
                
                NSDictionary *firstResultData = [self.resultArray firstObject];
                if ([[firstResultData objectForKey: @"LastUsedProduct"] length] > 0)
                {
                    [self.resultsTableView reloadData];
                }
                else
                {
                    [self.resultArray removeAllObjects];
                    self.vehicleResultsView.hidden = YES;
                }
            } else {
                self.vehicleResultsView.hidden = YES;
            }
            
            if(![[searchData objectForKey: @"PromotionDetail"] isKindOfClass:[NSNull class]] && [searchData objectForKey: @"PromotionDetail"] != nil) {
                self.offerArray = [searchData objectForKey: @"PromotionDetail"];
                self.vehicleResultsView.hidden = NO;
            }
            
            
            self.lblVehicleIDData.text = self.txtSearchVehicle.text;
            [self.resultsTableView reloadData];
        }
            break;
        default:
            break;
    }
}

- (void)processFailed:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_SEARCHVEHICLE: {
            self.btnRegisterProductCode.hidden = ![[mSession profileInfo] hasShowScanSearchVehicle];
            self.consumerDetailsView.hidden = YES;
            self.consumerDetailViewHeight.constant = 15;
        }
            break;
        default:
            break;
    }
}

#pragma mark - Car Plate Scanning
- (IBAction)cameraPressed:(id)sender
{
    if ([self.txtSearchVehicle isFirstResponder])
    {
        [self.txtSearchVehicle resignFirstResponder];
        return;
    }

    [mSession pushPhotoTakingViewControllerWithParent: self];
}

-(void)imageTaken:(UIImage *)image key:(NSString *)keyString
{
    if (!image)
        return;
    
    UIImage *newImage = [self rotateImage:image];
    
    NSData *imgData = UIImageJPEGRepresentation(newImage, 0.1);
    NSString *base64String = [imgData base64EncodedStringWithOptions: 0];
    
    NSString* secretKey = @"sk_a94baee6f07c7836e8ef292d"; // The secret key used to authenticate your account.  You can view your  secret key by visiting  https://cloud.openalpr.com/
    NSString *country = [self getCountryCode];
    //    NSString* country = @"us"; // Defines the training data used by OpenALPR.  \"us\" analyzes  North-American style plates.  \"eu\" analyzes European-style plates.  This field is required if using the \"plate\" task  You may use multiple datasets by using commas between the country  codes.  For example, 'au,auwide' would analyze using both the  Australian plate styles.  A full list of supported country codes  can be found here https://github.com/openalpr/openalpr/tree/master/runtime_data/config
    //au, auwide, br, br2, eu, fr, gb, in, kr, kr2, mx, sg, us, vn2
    
    if([country length] == 0)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:@"Invalid Country"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {
                                 //BUTTON OK CLICK EVENT
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    NSNumber* recognizeVehicle = @(0); // If set to 1, the vehicle will also be recognized in the image This requires an additional credit per request  (optional) (default to 0)
    NSString* state = @""; // Corresponds to a US state or EU country code used by OpenALPR pattern  recognition.  For example, using \"md\" matches US plates against the  Maryland plate patterns.  Using \"fr\" matches European plates against  the French plate patterns.  (optional) (default to )
    NSNumber* returnImage = @(0); // If set to 1, the image you uploaded will be encoded in base64 and  sent back along with the response  (optional) (default to 0)
    NSNumber* topn = @(5); // The number of results you would like to be returned for plate  candidates and vehicle classifications  (optional) (default to 10)
    NSString* prewarp = @""; // Prewarp configuration is used to calibrate the analyses for the  angle of a particular camera.  More information is available here http://doc.openalpr.com/accuracy_improvements.html#calibration  (optional) (default to )
    
    SWGDefaultApi *apiInstance = [[SWGDefaultApi alloc] init];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [apiInstance recognizeBytesWithImageBytes:base64String
                                    secretKey:secretKey
                                      country:country
                             recognizeVehicle:recognizeVehicle
                                        state:state
                                  returnImage:returnImage
                                         topn:topn
                                      prewarp:prewarp
                            completionHandler: ^(SWGInlineResponse200* output, NSError* error) {
                                if (output)
                                {
                                    if ([output.results count] > 0)
                                    {
                                        SWGPlateDetails *plateDetails = [output.results objectAtIndex: 0];
                                        
                                        self.txtSearchVehicle.text = plateDetails.plate; //String of car plate
                                        [self.txtSearchVehicle becomeFirstResponder];
                                    }
                                    else
                                    {
                                        UIAlertController *alert = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_GLOBAL_ERROR)
                                                                                                       message: LOCALIZATION(C_ERROR_CANNOTRECOGNIZE)
                                                                                                preferredStyle:UIAlertControllerStyleAlert];
                                        
                                        UIAlertAction *ok = [UIAlertAction actionWithTitle: LOCALIZATION(C_BUTTON_OK) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                             {
                                                                 //BUTTON OK CLICK EVENT
                                                             }];
                                        [alert addAction:ok];
                                        [self presentViewController:alert animated:YES completion:nil];
                                        //                                        self.imgView.image = nil;
                                        
                                    }
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                }
                                if (error) {
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    
                                    UIAlertController *alert = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_GLOBAL_ERROR)
                                                                                                   message: LOCALIZATION(C_ERROR_UNKNOWNERROR)
                                                                                            preferredStyle:UIAlertControllerStyleAlert];
                                    
                                    UIAlertAction *ok = [UIAlertAction actionWithTitle: LOCALIZATION(C_BUTTON_OK) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                         {
                                                             //BUTTON OK CLICK EVENT
                                                         }];
                                    [alert addAction:ok];
                                    [self presentViewController:alert animated:YES completion:nil];
                                }
                            }];
}


-(NSString *) getCountryCode
{
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
    {
        return @"th";
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDIA])
    {
        return @"in";
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDONESIA])
    {
        return @"id";
    }
    else if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_SAUDI])
    {
        return @"sa";
    }
    else
    {
        return @"eu";
    }
}

- (UIImage*) rotateImage:(UIImage* )originalImage {
    
    UIImageOrientation orientation = originalImage.imageOrientation;
    
    UIGraphicsBeginImageContext(originalImage.size);
    
    [originalImage drawAtPoint:CGPointMake(0, 0)];
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orientation == UIImageOrientationRight) {
        CGContextRotateCTM (context, [self radians:90]);
    } else if (orientation == UIImageOrientationLeft) {
        CGContextRotateCTM (context, [self radians:90]);
    } else if (orientation == UIImageOrientationDown) {
        // NOTHING
    } else if (orientation == UIImageOrientationUp) {
        CGContextRotateCTM (context, [self radians:0]);
    }
    
    return UIGraphicsGetImageFromCurrentImageContext();
    
}

- (CGFloat) radians:(int)degree {
    return (degree/180)*(22/7);
}


- (UIImage *)imageWithImage:(UIImage *)image
{
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    CGSize newSize = image.size;
    
    CGFloat aspectRatio = newSize.width / newSize.height;
    
    if (newSize.width >= newSize.height)
    {
        //landscape or square, set width to 1280
        newSize.width = 1280;
        newSize.height = newSize.width * aspectRatio;
    }
    else
    {
        //portrait
        newSize.height = 1280;
        newSize.width = newSize.height * aspectRatio;
    }
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
