//
//  ScanBottleMainViewController.m
//  Shell
//
//  Created by Nach on 12/11/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "ScanBottleMainViewController.h"

@interface ScanBottleMainViewController () <UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *lblSelectCustomerType;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ScanBottleMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_QL_SCANBOTTLE) subtitle:@""];
    
    self.lblSelectCustomerType.text = LOCALIZATION(C_DROPDOWN_SELECTCUSTOMERTYPE);
    self.lblSelectCustomerType.font = FONT_H1;
    self.lblSelectCustomerType.textColor = COLOUR_VERYDARKGREY;
    
    self.tableView.tableFooterView = [UIView new];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / Select Type",LOCALIZATION_EN(C_QL_SCANBOTTLE)] screenClass:nil];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 20.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ScanBottleCustomerTypeTableViewCell" forIndexPath:indexPath];
    
    UIImageView *imgView = [cell viewWithTag:1001];
    UILabel *lblCustomerType = [cell viewWithTag:1002];
    lblCustomerType.textColor = COLOUR_VERYDARKGREY;
    lblCustomerType.font = FONT_B1;
    
    switch(indexPath.section) {
        case 0:
            imgView.image = [UIImage imageNamed:@"icn_addnew_workshop.png"];
            lblCustomerType.text = LOCALIZATION(C_SCANBOTTLE_WORKSHOPORMECHANIC);
            break;
        case 1:
            imgView.image = [UIImage imageNamed:@"icn_consumers_verydarkgrey.png"];
            lblCustomerType.text = LOCALIZATION(C_SCANBOTTLE_CUSTOMER);
            break;
        default:
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch(indexPath.section) {
        case 0:
            [mSession pushScanBottleForShareCodeView:self];
            break;
        case 1:
            [mSession pushScanBottleForVehicleIDView:self];
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
