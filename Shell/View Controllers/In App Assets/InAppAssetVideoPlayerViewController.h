//
//  InAppAssetVideoPlayerViewController.h
//  Shell
//
//  Created by Jeremy Lua on 19/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <AVKit/AVKit.h>

@interface InAppAssetVideoPlayerViewController : AVPlayerViewController

@end
