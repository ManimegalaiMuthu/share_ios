//
//  InAppAssetPDFViewController.m
//  Shell
//
//  Created by Jeremy Lua on 2/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "InAppAssetPDFViewController.h"
#import <WebKit/WebKit.h>

@interface InAppAssetPDFViewController () <WKNavigationDelegate>
@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@end

@implementation InAppAssetPDFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_LIBRARY_TITLE) subtitle: @"" size: 15 subtitleSize:0];  //lokalise 1 feb
    else
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_LIBRARY_TITLE) subtitle: @""];  //lokalise 1 feb
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    
    if (kIsRightToLeft) {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    
#pragma mark - Adding WKWebView
    WebViewHelper *webViewHelper = [[WebViewHelper alloc] init];
    [webViewHelper setWebViewWithParentView: self.view
                         navigationDelegate: self
                                 urlAddress: @""];
    
    [webViewHelper reloadWebView: self.urlString];
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

#pragma WKWebView delegate methods
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    [UpdateHUD addMBProgress: self.view withText:LOADING_HUD];
}

-(void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    NSLog(@"failed to load navigation");
    [UpdateHUD removeMBProgress: self.view];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [UpdateHUD removeMBProgress: self.view];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    if(navigationAction.navigationType ==  WKNavigationTypeLinkActivated) {
        NSMutableURLRequest *newRequest = [NSMutableURLRequest requestWithURL:navigationAction.request.URL];
        [newRequest setAllHTTPHeaderFields: [WebserviceHelper prepPostLoginHeader]];
        [webView performSelector:@selector(loadRequest:) withObject:newRequest afterDelay:0];
        
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
    return;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
