//
//  InAppAssetCategoryViewController.m
//  Shell
//
//  Created by Jeremy Lua on 2/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "UIButton+WebCache.h"

#import "InAppAssetCategoryViewController.h"

#import "InAppAssetPDFViewController.h"

//#import <AVKit/AVKit.h>
#import "InAppAssetVideoPlayerViewController.h"

#define VIDEOCELL_TAG_SUFFIX 100

@interface InAppAssetCategoryViewController () <WebServiceManagerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UITableView *assetTableView;


@property (weak, nonatomic) IBOutlet UIButton *btnFilter;

@property NSArray *assetListArray;

@property AVPlayer *player;
@end

@implementation InAppAssetCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_LIBRARY_TITLE) subtitle: @"" size: 15 subtitleSize:0];  //lokalise 1 feb
    else
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_LIBRARY_TITLE) subtitle: @""];  //lokalise 1 feb
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
//    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_LIBRARY_TITLE),  LOCALIZATION(C_REWARDS_BACK)];
    
    
    [[WebServiceManager sharedInstance] fetchInAppAssetListWithDictionary:@{@"CategoryID": self.categoryId,
                                                                            @"DateRange": @"",
                                                                            } vc: self];
    
    [self.btnFilter.titleLabel setFont: FONT_B1];
    [self.btnFilter setTitle: LOCALIZATION(C_LIBRARY_FILTERDATERANGE) forState: UIControlStateNormal]; //lokalise 1 feb
    
    if (kIsRightToLeft) {
        [self.btnFilter setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.btnFilter setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 15)];
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.btnFilter setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.btnFilter setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
//    self.assetCollectionViewFlowLayout.estimatedItemSize = CGSizeMake(SCREEN_WIDTH - 25, ((SCREEN_WIDTH / 2) + 200 ));
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_LIBRARY_TITLE) screenClass:nil];
}

#pragma mark - UITableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.assetListArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"InAppAssetCategoryCell"];
    
    cell.tag = VIDEOCELL_TAG_SUFFIX + indexPath.row;
    
    NSDictionary *assetData = [self.assetListArray objectAtIndex: indexPath.row];
    
    
    UIButton *btn = [cell viewWithTag: 1];
    [btn sd_setBackgroundImageWithURL: [NSURL URLWithString: [assetData objectForKey: @"ImageTH"]]
                             forState:UIControlStateNormal
                     placeholderImage:[UIImage imageNamed: @"placeholder-media"]
                              options:SDWebImageRefreshCached | SDWebImageRetryFailed];
    
    UIView *indicatorIconView = [cell viewWithTag: 2];
    //0 - Not read, show icon
    //1 - read, hide icon
    indicatorIconView.hidden = [[assetData objectForKey: @"ReadStatus"] boolValue];
    
    UILabel *lblHeader = [cell viewWithTag: 3];
    lblHeader.font = FONT_H2;
    lblHeader.text = [[assetData objectForKey:@"Header"] uppercaseString];
    
    UILabel *lblContent = [cell viewWithTag: 4];
    lblContent.font = FONT_B2;
    lblContent.text = [assetData objectForKey: @"Content"];
    
    switch ([[assetData objectForKey:@"Type"] intValue])
    {
        case kINAPPASSET_VIDEO:
        {
            UICollectionView *cellColView = [cell viewWithTag: 99999];
//            cellColView.tag = COLVIEWCELL_TAG_SUFFIX + indexPath.row;
            UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *) cellColView.collectionViewLayout;
            flowLayout.estimatedItemSize = CGSizeMake(200, 40);
            
        }
            break;
        default:
            break;
    }
    return cell;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

#pragma mark - UICollectionView
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"LanguageCell" forIndexPath:indexPath];
    
    UILabel *lblLanguage = [cell viewWithTag: 1];
    lblLanguage.text = @"Language";
    lblLanguage.font = FONT_B2;
    
    return cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    //cell = btn.superview.superview
    NSDictionary *assetData = [self.assetListArray objectAtIndex: collectionView.superview.superview.tag - VIDEOCELL_TAG_SUFFIX];
//        return [[assetData objectForKey: @"Language"] count];
    return 0;
}

enum
{
    kINAPPASSET_VIDEO = 1,
    kINAPPASSET_PDF,
} kINAPPASSET_TYPE;


- (IBAction)videoSelected:(UIButton *)sender {
    
    //cell = btn.superview.superview
    NSDictionary *assetData = [self.assetListArray objectAtIndex: sender.superview.superview.tag - VIDEOCELL_TAG_SUFFIX];
    
    UITableViewCell *cell = [self.assetTableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow: sender.superview.superview.tag - VIDEOCELL_TAG_SUFFIX inSection:0]];
    
    UIView *indicatorIconView = [cell viewWithTag: 2];
    //0 - Not read, show icon
    //1 - read, hide icon
    indicatorIconView.hidden = YES;
    
    switch ([[assetData objectForKey:@"Type"] intValue])
    {
        case kINAPPASSET_VIDEO:
        {
            InAppAssetVideoPlayerViewController *videoVc = [STORYBOARD_INAPPASSET instantiateViewControllerWithIdentifier: VIEW_INAPPASSETVIDEOVIEW];
            videoVc.player = [AVPlayer playerWithURL:[NSURL URLWithString: [assetData objectForKey:@"URL"]]];
            [CURRENT_VIEWCONTROLLER presentViewController: videoVc animated:YES completion: nil];
        }
            break;
        case kINAPPASSET_PDF:
        {
            [mSession pushInAppAssetPDFView:[assetData objectForKey:@"URL"] vc: self];
        }
            break;
        default:
            break;
    }
    
    [[WebServiceManager sharedInstance] readAssetWithAssetId: [assetData objectForKey:@"AssetID"] vc: nil];
    
}
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UITableViewCell *cell = [tableView cellForRowAtIndexPath: indexPath];
//
//    NSDictionary *assetData = [self.assetListArray objectAtIndex: indexPath.row];
//
//    UIView *indicatorIconView = [cell viewWithTag: 2];
//    //0 - Not read, show icon
//    //1 - read, hide icon
//    indicatorIconView.hidden = YES;
//
//    switch ([[assetData objectForKey:@"Type"] intValue])
//    {
//        case kINAPPASSET_VIDEO:
//        {
//            InAppAssetVideoPlayerViewController *videoVc = [STORYBOARD_INAPPASSET instantiateViewControllerWithIdentifier: VIEW_INAPPASSETVIDEOVIEW];
//            videoVc.player = [AVPlayer playerWithURL:[NSURL URLWithString: [assetData objectForKey:@"URL"]]];
//            [CURRENT_VIEWCONTROLLER presentViewController: videoVc animated:YES completion: nil];
//        }
//            break;
//        case kINAPPASSET_PDF:
//        {
//            [mSession pushInAppAssetPDFView:[assetData objectForKey:@"URL"] vc: self];
//        }
//            break;
//        default:
//            break;
//    }
//
//    [[WebServiceManager sharedInstance] readAssetWithAssetId: [assetData objectForKey:@"AssetID"] vc: nil];
//}

-(void)itemDidFinishPlaying:(NSNotification *) notification {
    [self.player seekToTime: kCMTimeZero];
    [self.player pause];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch(response.webserviceCall)
    {
        case kWEBSERVICE_ASSETLIST:
        {
            
            self.assetListArray = [[response getGenericResponse] objectForKey: @"AssetDetails"];
            
            [self.assetTableView reloadData];
        }
            break;
        default:
            break;
    }
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)filterPressed:(id)sender {
    
    [self popUpListViewWithTitle: LOCALIZATION(C_LIBRARY_FILTERDATERANGE) listArray: [mSession getDateRange]]; //lokalise 1 feb
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        
        [self.btnFilter setTitle:selection.firstObject forState:UIControlStateNormal];
        [[WebServiceManager sharedInstance] fetchInAppAssetListWithDictionary:@{@"CategoryID": self.categoryId,
                                                                                @"DateRange": [mSession convertToDateRangeKeyCode: selection.firstObject],
                                                                                } vc: self];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
