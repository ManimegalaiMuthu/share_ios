//
//  InAppAssetViewController.m
//  Shell
//
//  Created by Jeremy Lua on 2/4/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "InAppAssetViewController.h"

@interface InAppAssetViewController () <WebServiceManagerDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UICollectionView *inAppCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *inAppCollectionFlowLayout;
@property (weak, nonatomic) IBOutlet UIButton *btnFilter;

@property NSArray *categoryArray;

@end

@implementation InAppAssetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_HOME) subtitle: @""];
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_LIBRARY_TITLE) subtitle: @"" size: 15 subtitleSize:0];  //lokalise 1 feb
    else
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_LIBRARY_TITLE) subtitle: @""];  //lokalise 1 feb

    self.inAppCollectionFlowLayout.itemSize = CGSizeMake(SCREEN_WIDTH - 30, 120);
    
    [self.btnFilter.titleLabel setFont: FONT_B1];
    [self.btnFilter setTitle: LOCALIZATION(C_LIBRARY_FILTERDATERANGE) forState: UIControlStateNormal];  //lokalise 1 feb
    
    self.lblHeader.font = FONT_B1;
//    self.lblHeader.text = @"Browse by Category";
    self.lblHeader.text = LOCALIZATION(C_LIBRARY_BROWSECATEGORY);  //lokalise 1 feb
    
    if (kIsRightToLeft) {
        [self.btnFilter setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    } else {
        [self.btnFilter setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    }
    
    [[WebServiceManager sharedInstance] fetchInAppAssetCategoryWithDictionary:@{@"DateRange": @"",} vc: self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_LIBRARY_TITLE) screenClass:nil];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"InAppCategoryCell" forIndexPath: indexPath];
    
    NSDictionary *categoryData = [self.categoryArray objectAtIndex: indexPath.row];
    
    UIView *contentView = [cell viewWithTag: 999];
    [contentView setBackgroundColor:COLOUR_RED];
    
    UIImageView *categoryImgView = [cell viewWithTag: 1];
    NSString *categoryImageUrlString = [categoryData objectForKey: @"CategoryImageURL"];
    
    [categoryImgView sd_setImageWithURL: [NSURL URLWithString: categoryImageUrlString]
                       placeholderImage: [UIImage imageNamed: @"sideicon-media"]
                                options:(SDWebImageRefreshCached  | SDWebImageRetryFailed)];

    NSLog(@"img url:%@",categoryImageUrlString);
    
    UILabel *lblHeader = [cell viewWithTag: 2];
    lblHeader.font = FONT_H1;
    lblHeader.text = [categoryData objectForKey: @"CategoryName"];
    
    
    UILabel *lblContent = [cell viewWithTag: 3];
    lblContent.font = FONT_B1;

    
    lblContent.text = [categoryData objectForKey: @"CategoryDescription"];
    
    UIView *backgroundView = [cell viewWithTag: 101];
    UILabel *lblReadCount = [cell viewWithTag: 102];
    
    NSInteger totalUnreadCount = [[categoryData objectForKey: @"TotalUnRead"] intValue];
        
	if (totalUnreadCount > 0)
    {
        lblReadCount.font = FONT_B2;
        lblReadCount.text = @(totalUnreadCount).stringValue;
        lblReadCount.hidden = NO;
        
        backgroundView.hidden = NO;

        if (totalUnreadCount > 99)
            [lblReadCount setText: @"99+"];
        else
            [lblReadCount setText: @(totalUnreadCount).stringValue];
        
        [lblReadCount sizeToFit];
        [cell layoutIfNeeded];
        [cell setNeedsLayout];


        backgroundView.layer.cornerRadius = backgroundView.frame.size
        .height / 2.0;
    }
    else
    {
        [lblReadCount setHidden: YES];
        [backgroundView setHidden: YES];
    }
    
   
    
    return cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.categoryArray count];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

    NSDictionary *categoryData = [self.categoryArray objectAtIndex: indexPath.row];
    
    [mSession pushInAppAssetCategoryListWithCategoryId: [categoryData objectForKey: @"CategoryID"] vc: self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch(response.webserviceCall)
    {
        case kWEBSERVICE_ASSETCATEGORYLIST:
        {
            self.categoryArray = [[response getGenericResponse] objectForKey: @"AssetCategoryList"];
            
            [self.inAppCollectionView reloadData];
        }
            break;
        default:
            break;
    }
}

-(BOOL)shouldAutorotate
{
    return YES;
}

- (IBAction)filterPressed:(id)sender {
    
    [self popUpListViewWithTitle: LOCALIZATION(C_LIBRARY_FILTERDATERANGE) listArray: [mSession getDateRange]];  //lokalise 1 feb
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        
        [self.btnFilter setTitle:selection.firstObject forState:UIControlStateNormal];
        
        [[WebServiceManager sharedInstance] fetchInAppAssetCategoryWithDictionary:@{@"DateRange": [mSession convertToDateRangeKeyCode: selection.firstObject],} vc: self];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
