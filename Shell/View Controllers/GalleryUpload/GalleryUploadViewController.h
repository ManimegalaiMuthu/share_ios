//
//  GalleryUploadViewController.h
//  Shell
//
//  Created by Admin on 28/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"
#import <UIKit/UIKit.h>

@protocol GalleryUploadViewDelegate <NSObject>
-(void) didFinishUpload: (NSArray *) imageDataArray;
@end

@interface GalleryUploadViewController: BaseVC
@property NSString *tradeID;
@property BOOL isApproved;
@property id<GalleryUploadViewDelegate> delegate;

@end
