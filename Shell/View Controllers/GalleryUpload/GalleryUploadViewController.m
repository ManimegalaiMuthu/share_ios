//
//  GalleryUploadViewController.m
//  Shell
//
//  Created by Admin on 28/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "GalleryUploadViewController.h"
#import <Photos/Photos.h>
#import "ImageCollectionViewCell.h"
#import "Constants.h"

@interface GalleryUploadViewController ()<WebServiceManagerDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
{
    
}
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *btnUpload;
@property (weak, nonatomic) IBOutlet UICollectionView *colView;
@property (weak, nonatomic) IBOutlet UIView *previewView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property NSMutableArray *cellArray;
@property NSIndexPath *cellToBeDeleted;
@end

@implementation GalleryUploadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self.btnUpload.titleLabel setFont: FONT_BUTTON];
    [self.btnUpload setBackgroundColor: COLOUR_RED];
    
    _cellArray = [[NSMutableArray alloc]init];
    [self.colView registerNib: [UINib nibWithNibName: @"ImageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"PhotoImage"];
    
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(@"WORKSHOP IMAGE UPLOAD") subtitle: @"" size:13 subtitleSize:0];
    self.previewView.hidden = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
//    [FIRAnalytics setScreenName:LOCALIZATION_EN(@"WORKSHOP IMAGE UPLOAD") screenClass:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_cellArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoImage" forIndexPath:indexPath];
    
    cell.imageView.image = [UIImage imageWithData: [_cellArray objectAtIndex:indexPath.row]];
    return cell;
}
/*
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.previewView.hidden = NO;
    self.cellToBeDeleted = indexPath;
    self.imageView.image = [UIImage imageWithData:[_cellArray objectAtIndex:indexPath.row]];
}
 */
- (IBAction)libraryPressed:(id)sender
{
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        switch (status) {
            case PHAuthorizationStatusAuthorized:{
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.allowsEditing = YES;
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                
                [self presentViewController:picker animated:YES completion:NULL];
            }
                break;
            case PHAuthorizationStatusDenied:{
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_ALERTVIEW_ERROR) message:LOCALIZATION(C_ALERTVIEW_PERMISSIONERRORMSG) preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_CANCEL) style:UIAlertActionStyleCancel handler:nil];
                UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_SETTING) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                                UIApplicationOpenSettingsURLString]];
                }];
                
                [alertController addAction:cancelAction];
                [alertController addAction:settingsAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
                
                break;
            default:
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_ALERTVIEW_ERROR) message:LOCALIZATION(C_ALERTVIEW_PERMISSIONERRORMSG) preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_CANCEL) style:UIAlertActionStyleCancel handler:nil];
                UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_SETTING) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                                UIApplicationOpenSettingsURLString]];
                }];
                
                [alertController addAction:cancelAction];
                [alertController addAction:settingsAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
                break;
        }
    }];
    
}
- (IBAction)uploadPressed:(id)sender
{
    if ([self.cellArray count] > 0)
    {
        if (!self.tradeID)
        {
            [self.delegate didFinishUpload: _cellArray];
            [self popSelf];
        }
        else
        {
            [[WebServiceManager sharedInstance] uploadGalleryPhotos: self.tradeID isApproved: self.isApproved dataFiles:self.cellArray vc:self];
        }
    }
    else
    {
        [self popSelf];
    }
}


-(void)processCompleted:(WebServiceResponse *)response
{
    switch(response.webserviceCall)
    {
        case kWEBSERVICE_UPLOADGALLERYPHOTOS:
        {
            [self popSelf];
        }
            break;
    }
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

- (IBAction)cameraPressed:(id)sender
{
    switch ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo])
    {
        case AVAuthorizationStatusAuthorized:
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:picker animated:YES completion:NULL];
        }
            break;
        default:
        {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
             {
                 if(granted)
                 {
                     UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                     picker.delegate = self;
                     picker.allowsEditing = YES;
                     picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                     
                     [self presentViewController:picker animated:YES completion:NULL];
                 }
                 else
                 {
                     UIAlertController *alertController = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_ALERTVIEW_ERROR) message:LOCALIZATION(C_ALERTVIEW_PERMISSIONERRORMSG) preferredStyle:UIAlertControllerStyleAlert];
                     
                     UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_CANCEL) style:UIAlertActionStyleCancel handler:nil];
                     UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_SETTING) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                                     UIApplicationOpenSettingsURLString]];
                     }];
                     
                     [alertController addAction:cancelAction];
                     [alertController addAction:settingsAction];
                     [self presentViewController:alertController animated:YES completion:nil];
                 }
             }];
        }
            break;
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    [_cellArray addObject: UIImageJPEGRepresentation(chosenImage, 0.5f)];
    [_colView reloadData];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (IBAction)imageBack:(id)sender {
    self.previewView.hidden = YES;
}

- (IBAction)deletePressed:(id)sender {
    self.previewView.hidden = YES;
    [_cellArray removeObjectAtIndex:_cellToBeDeleted.row];
    [self.colView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
