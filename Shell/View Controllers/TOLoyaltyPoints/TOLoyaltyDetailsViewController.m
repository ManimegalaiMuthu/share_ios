//
//  TOLoyaltyDetailsViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 8/2/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "TOLoyaltyDetailsViewController.h"

@interface TOLoyaltyDetailsViewController () <WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource>
@property NSMutableArray *detailsArray;

@property UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@end

@implementation TOLoyaltyDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.detailsArray = [[NSMutableArray alloc] init];
    
    self.tableView.tableFooterView = [UIView new];
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TOLOYALTY_DETAILS_TITLE) subtitle: @"" size: 15 subtitleSize:15];   //lokalised 11 Feb
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    
    [[WebServiceManager sharedInstance] fetchTOPointsTransactionDetails: self.addressData vc: self];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.detailsArray count] > 0)
        return [self.detailsArray count] + 1;
    else
        return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.row == 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"DetailsHeaderCell"];
        
        UILabel *lblProductName = [cell viewWithTag: 1];
        lblProductName.text = LOCALIZATION(C_TOLOYALTY_DETAILS_PRODUCTNAME);    //lokalised 11 Feb
        lblProductName.font = FONT_H1;
        
        UILabel *lblVolumeHeader = [cell viewWithTag: 2];
        lblVolumeHeader.font = lblProductName.font;
        lblVolumeHeader.text = LOCALIZATION(C_TOLOYALTY_DETAILS_VOLUME);    //lokalised 11 Feb
        
        UILabel *lblPointsHeader = [cell viewWithTag: 3];
        lblPointsHeader.font = lblProductName.font;
        lblPointsHeader.text = LOCALIZATION(C_TOLOYALTY_DETAILS_POINTS);    //lokalised 11 Feb
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"DetailsCell"];
        
//        "TOPointsDetails": [
//                            {
//                                "Product_Group": "550024931 - Spirax S2 A 140_1*209L_A227",
//                                "Points": 50,
//                                "Volume": "7"
//                            },
        NSDictionary *pointsDict = [self.detailsArray objectAtIndex: indexPath.row - 1];
        
        UILabel *lblDate = [cell viewWithTag: 1];
        lblDate.font = FONT_B1;
        lblDate.text = [pointsDict objectForKey: @"Product_Group"];
        
        UILabel *lblDesc = [cell viewWithTag: 2];
        lblDesc.font = lblDate.font;
        lblDesc.text = [[pointsDict objectForKey: @"Points"] stringValue];
        
        UILabel *lblPoints = [cell viewWithTag: 3];
        lblPoints.font = lblDate.font;
        
        lblPoints.text = [[pointsDict objectForKey: @"Volume"] stringValue]; //server returning number value
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
        return;
    else
    {
        NSDictionary *pointsDict = [self.detailsArray objectAtIndex: indexPath.row - 1];
        
        [mSession pushTOLoyaltyDetails:self addressData:[pointsDict objectForKey: @"TransactionDate"]];
    }
}
- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    self.detailsArray = [[response getGenericResponse] objectForKey: @"TOPointsDetails"];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
