//
//  TOLoyaltySummaryViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 8/2/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "TOLoyaltySummaryViewController.h"
#import "CASMonthYearPicker.h"

@interface TOLoyaltySummaryViewController () <WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource, MonthYearPickerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblSearchHeader;

@property (weak, nonatomic) IBOutlet UITableView *tableView;


@property CASMonthYearPicker *dateView;
@property (weak, nonatomic) IBOutlet UIButton *btnStartDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEndDate;

@property (weak, nonatomic) IBOutlet UIButton *goBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;

#warning HEIGHT 0, ADJUST BACK TO 50 WHEN READY
@property (weak, nonatomic) IBOutlet UIButton *btnViewCatalogue;

@property NSMutableArray *summaryArray;
@end

@implementation TOLoyaltySummaryViewController
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.summaryArray = [[NSMutableArray alloc] init];
    
    self.tableView.tableFooterView = [UIView new];
    
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TOLOYALTY_SUMMARY_TITLE) subtitle: @"" size:15 subtitleSize:15];    //lokalised 11 Feb
    
    [self setupInterface];

    [[WebServiceManager sharedInstance] fetchTOPointsTransactionHistory:
                                                        @{@"FromDate":@"",
                                                          @"ToDate":@""}
                                                                     vc:self];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
//    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//    [tracker set:kGAIScreenName value:@"TO Loyalty Summary"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
}

-(void) setupInterface
{
#warning select month year
    dateView = [[CASMonthYearPicker alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_MONTHYEAR)previouslySelected:nil withYearHidden:YES];  //lokalised 11 Feb
    dateView.delegate = self;
    
    self.lblSearchHeader.textColor = COLOUR_VERYDARKGREY;
    self.lblSearchHeader.font = FONT_H2;
    self.lblSearchHeader.text = LOCALIZATION(C_TOLOYALTY_SUMMARY_HEADER);   //lokalised 11 Feb
    
    [Helper setCustomFontButtonContentModes: self.btnStartDate];
    [self.btnStartDate.titleLabel setFont: FONT_B2];
    [self.btnStartDate setTitle: LOCALIZATION(C_TOLOYALTY_SUMMARY_FROMMONTH) forState: UIControlStateNormal];   //lokalised 11 Feb
    
    [Helper setCustomFontButtonContentModes: self.btnEndDate];
    [self.btnEndDate.titleLabel setFont: FONT_B2];
    [self.btnEndDate setTitle: LOCALIZATION(C_TOLOYALTY_SUMMARY_TOMONTH) forState: UIControlStateNormal];   //lokalised 11 Feb
    
    [Helper setCustomFontButtonContentModes: self.goBtn];
    [self.goBtn.titleLabel setFont: FONT_B1];
    [self.goBtn setTitle: LOCALIZATION(C_POINTS_GO) forState: UIControlStateNormal];    //lokalised 11 Feb
    
    [self.lblTo setFont: FONT_B2];
    self.lblTo.text =  LOCALIZATION(C_POINTS_TO);   //lokalised 11 Feb
    
    [Helper setCustomFontButtonContentModes: self.btnViewCatalogue];
    [self.btnViewCatalogue.titleLabel setFont: FONT_BUTTON];
    [self.btnViewCatalogue setTitle: LOCALIZATION(C_TOLOYALTY_SUMMARY_VIEWCATALOGUE) forState: UIControlStateNormal];   //lokalised 11 Feb
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)startDatePress:(id)sender
{
    self.dateView.tag = 1;
    [self showMonthYearPicker];
}

- (IBAction)toDatePressed:(id)sender
{
    self.dateView.tag = 2;
    [self showMonthYearPicker];
}

- (IBAction)goPressed:(id)sender {
    [[WebServiceManager sharedInstance] fetchTOPointsTransactionHistory: @{@"FromDate": self.btnStartDate.titleLabel.text,
                                                                           @"ToDate":self.btnEndDate.titleLabel.text,
                                                                           
                                                                           } vc:self];
//    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
//    if ([GET_LOCALIZATION isEqualToString: kThai])
//    {
//        [inputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
//        [inputFormatter setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierBuddhist]];
//    }
//    [inputFormatter setDateFormat:@"MMM yyyy"];
//
//    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
//    [outputFormatter setDateFormat:@"MMM yyyy"];
//    if ([GET_LOCALIZATION isEqualToString: kThai])
//    {
//        [outputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
//        [outputFormatter setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierGregorian]];
//    }
//
//    NSDate *startDate = [inputFormatter dateFromString: self.btnStartDate.titleLabel.text];
//    NSString *startDateString = @"";
//    if (startDate)
//        startDateString = [outputFormatter stringFromDate: startDate];
//
//    NSDate *endDate = [inputFormatter dateFromString: self.btnEndDate.titleLabel.text];
//    NSString *endDateString = @"";
//    if (endDate)
//        endDateString = [outputFormatter stringFromDate: endDate];
//
//    [[WebServiceManager sharedInstance] fetchTOPointsTransactionHistory: @{@"FromDate":startDateString,
//                                                                   @"ToDate":endDateString,
//
//                                                                   } vc:self];
}

-(void) showMonthYearPicker
{
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}

- (void)selectedMonthYear:(NSDate *)selectedDate
{
    //do smething
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //   for API
//    [formatter setDateFormat:@"dd"];
//    self.member.dobDay = [formatter stringFromDate:selectedDate];
//
//    [formatter setDateFormat:@"MM"];
//    self.member.dobMonth = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"MMM yyyy"];
    
    [formatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    NSString *selectedDateString = [formatter stringFromDate:selectedDate];
    
    if (dateView.tag == 1)
    {
        [self.btnStartDate setTitle: selectedDateString forState: UIControlStateNormal];
    }
    else
    {
        [self.btnEndDate setTitle: selectedDateString forState: UIControlStateNormal];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.summaryArray count] > 0)
        return [self.summaryArray count] + 1;
    else
        return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.row == 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"SummaryHeaderCell"];
        
        UILabel *lblDateHeader = [cell viewWithTag: 1];
        lblDateHeader.text = LOCALIZATION(C_TOLOYALTY_SUMMARY_MONTH);       //lokalised 11 Feb
        lblDateHeader.font = FONT_H1;
        
        UILabel *lblDescHeader = [cell viewWithTag: 2];
        lblDescHeader.font = lblDateHeader.font;
        lblDescHeader.text = LOCALIZATION(C_TOLOYALTY_SUMMARY_VOLUME);      //lokalised 11 Feb
        
        UILabel *lblPointsHeader = [cell viewWithTag: 3];
        lblPointsHeader.font = lblDateHeader.font;
        lblPointsHeader.text = LOCALIZATION(C_TOLOYALTY_SUMMARY_POINTS);    //lokalised 11 Feb
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"SummaryCell"];
     
//        {
//            "TransactionDate": "Jan 2018",
//            "Points": 40,
//            "Volume": "5"
//        }
        NSDictionary *pointsDict = [self.summaryArray objectAtIndex: indexPath.row - 1];
        
        UILabel *lblDate = [cell viewWithTag: 1];
        lblDate.font = FONT_B1;
        lblDate.text = [pointsDict objectForKey: @"TransactionDate"];
        
        UILabel *lblDesc = [cell viewWithTag: 2];
        lblDesc.font = lblDate.font;
        lblDesc.text = [[pointsDict objectForKey: @"Volume"] stringValue];
        
        UILabel *lblPoints = [cell viewWithTag: 3];
        lblPoints.font = lblDate.font;
        
        lblPoints.text = [[pointsDict objectForKey: @"Points"] stringValue]; //server returning number value
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
        return;
    else
    {
        NSDictionary *pointsDict = [self.summaryArray objectAtIndex: indexPath.row - 1];
        
        [mSession pushTOLoyaltyDetails:self
                           addressData: @{@"FromDate": [pointsDict objectForKey: @"TransactionDate"]}];
    }
}

-(void)processCompleted:(WebServiceResponse *)response
{
    self.summaryArray = [[response getGenericResponse] objectForKey: @"TOPointsHistory"];
    
    [self.tableView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
