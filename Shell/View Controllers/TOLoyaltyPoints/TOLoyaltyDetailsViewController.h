//
//  TOLoyaltyDetailsViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 8/2/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface TOLoyaltyDetailsViewController : BaseVC
@property NSDictionary *addressData;
@end
