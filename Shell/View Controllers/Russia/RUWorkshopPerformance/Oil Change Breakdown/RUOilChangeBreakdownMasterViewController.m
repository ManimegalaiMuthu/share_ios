//
//  RUOilChangeBreakdownMasterViewController.m
//  Shell
//
//  Created by Jeremy Lua on 4/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUOilChangeBreakdownMasterViewController.h"
#import "RUOilChangeBreakdownOverviewViewController.h"
#import "RUOilChangeBreakdownDetailsViewController.h"

#import "CASMonthYearPicker.h"

@interface RUOilChangeBreakdownMasterViewController () <WebServiceManagerDelegate, MonthYearPickerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnOverview;
@property (weak, nonatomic) IBOutlet UIButton *btnDetails;

@property (weak, nonatomic) IBOutlet UIButton *btnStartDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEndDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property NSDate *startDate;
@property NSDate *endDate;

@property (weak, nonatomic) IBOutlet UIButton *btnEnter;

@property BOOL isStartDate;
@property CASMonthYearPicker *dateView;

@property (weak, nonatomic) IBOutlet UIView *vcView;
@property RUOilChangeBreakdownOverviewViewController  *overviewVc;
@property RUOilChangeBreakdownDetailsViewController  *detailsVc;


@end

@implementation RUOilChangeBreakdownMasterViewController
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_RU_BREAKDOWN_OILCHANGE_TITLE) subtitle: @""];  //lokalised 28 Jan
    
    [self setNavBackBtn];
    
    [self.btnOverview setTitle: LOCALIZATION(C_RU_BREAKDOWN_OVERVIEW) forState: UIControlStateNormal];  //lokalised 28 Jan
    [self.btnDetails setTitle: LOCALIZATION(C_RU_BREAKDOWN_DETAILS) forState: UIControlStateNormal];    //lokalised 28 Jan
    
    self.lblTo.text = LOCALIZATION(C_RU_STEPONE_TO);    //lokalised 28 Jan
    [self.btnEnter setTitle: LOCALIZATION(C_RU_STEPONE_ENTER) forState: UIControlStateNormal];  //lokalised 28 Jan
    
    
    dateView = [[CASMonthYearPicker alloc] initWithTitle:LOCALIZATION(C_PERFORMANCE_SELECTDATE) previouslySelected:nil withYearHidden:NO];  //lokalised 28 Jan
    
    dateView.delegate = self;
    
    self.overviewVc = [STORYBOARD_RU_WORKSHOPPERFORMANCE instantiateViewControllerWithIdentifier: VIEW_RU_BREAKDOWN_OILCHANGE_OVERVIEW];
    [self addChildViewController: self.overviewVc];
    
    self.detailsVc = [STORYBOARD_RU_WORKSHOPPERFORMANCE instantiateViewControllerWithIdentifier: VIEW_RU_BREAKDOWN_OILCHANGE_DETAILS];
    //    self.detailsVc.delegate = self;
    [self addChildViewController: self.detailsVc];
    
    //setup default date
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    //setup the necesary date adjustments
    NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
    [selectedComponent setDay: 1];
    NSDate *selectedDate = [gregorian dateFromComponents: selectedComponent];
    
    [selectedComponent setDay: 0];
    [selectedComponent setMonth: -2];
    [selectedComponent setYear: 0];
    selectedDate = [gregorian dateByAddingComponents: selectedComponent toDate: selectedDate options:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)];
    self.isStartDate = YES;
    self.startDate = selectedDate;
    [self selectedMonthYear: selectedDate];
    [self btnPressed: self.btnOverview]; //select calendar by default, btnpress will call enterPressed for api
}

- (IBAction)btnPressed:(UIButton *)sender
{
    if (sender == self.btnOverview && !self.btnOverview.selected)
    {
        self.btnOverview.selected = YES;
        [self.btnOverview setTitleColor: COLOUR_WHITE forState: UIControlStateNormal];
        self.btnOverview.backgroundColor = COLOUR_RED;
        
        self.btnDetails.selected = NO;
        [self.btnDetails setTitleColor: COLOUR_VERYDARKGREY forState: UIControlStateNormal];
        self.btnDetails.backgroundColor = COLOUR_WHITE;
        
        [self.detailsVc.view removeFromSuperview];
        [self.vcView addSubview: self.overviewVc.view];
        [self.overviewVc.view setFrame: CGRectMake(0, 0, self.vcView.frame.size.width, self.vcView.frame.size.height)];
        
    }
    else if (sender == self.btnDetails && !self.btnDetails.selected)
    {
        self.btnDetails.selected = YES;
        [self.btnDetails setTitleColor: COLOUR_WHITE forState: UIControlStateNormal];
        self.btnDetails.backgroundColor = COLOUR_RED;
        
        self.btnOverview.selected = NO;
        [self.btnOverview
         setTitleColor: COLOUR_VERYDARKGREY forState: UIControlStateNormal];
        self.btnOverview.backgroundColor = COLOUR_WHITE;
        
        
        
        [self.overviewVc.view removeFromSuperview];
        [self.vcView addSubview: self.detailsVc.view];
        [self.detailsVc.view setFrame: CGRectMake(0, 0, self.vcView.frame.size.width, self.vcView.frame.size.height)];
    }
    
    [self enterPressed: nil];
}

- (IBAction)enterPressed:(id)sender
{
    NSMutableDictionary *tradeDict = [[NSMutableDictionary alloc] init];
    LoginInfo *loginInfo = [mSession loadUserProfile];
    
    [tradeDict setObject: loginInfo.tradeID forKey: @"TradeID"];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy/MM/dd"];
    
    [tradeDict setObject: [outputFormatter stringFromDate: self.startDate] forKey:@"StartDate"];
    [tradeDict setObject: [outputFormatter stringFromDate: self.endDate] forKey:@"EndDate"];
    
    
    if (self.btnOverview.selected)
        [tradeDict setObject: @(1) forKey: @"ViewType"];
    else if (self.btnDetails.selected)
        [tradeDict setObject: @(2) forKey: @"ViewType"];
    
    [[WebServiceManager sharedInstance] fetchOilChangeBreakdown: tradeDict vc: self];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_FETCH_OILCHANGEBREAKDOWN:
        {
            if (self.btnOverview.selected)
                [self.overviewVc reloadBreakdownData: [response getGenericResponse]];
            else if (self.btnDetails.selected)
                [self.detailsVc reloadBreakdownData: [response getGenericResponse]];
        }
            break;
            
        default:
            break;
    }
}

- (void)processFailed:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_FETCH_OILCHANGEBREAKDOWN:
        {
            if (self.btnOverview.selected)
                [self.overviewVc reloadBreakdownData: nil];
            else if (self.btnDetails.selected)
                [self.detailsVc reloadBreakdownData: nil];
        }
            break;
            
        default:
            break;
    }
}

- (IBAction)datePressed:(UIButton *)sender {
    [self.view endEditing: YES];
    
    if ([sender isEqual: self.btnStartDate])
        self.isStartDate = YES;
    else
        self.isStartDate = NO;
    
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}


#pragma Date Picker delegate method
- (void)selectedMonthYear:(NSDate *)selectedDate
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    
    //setup the necesary date adjustments
    NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: selectedDate];
    [selectedComponent setDay: 1];
    selectedDate = [gregorian dateFromComponents: selectedComponent];
    
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setMonth: 3];
    [offsetComponents setDay: -1];
    
    NSDateFormatter *btnDateFormatter = [[NSDateFormatter alloc] init];
    [btnDateFormatter setDateFormat:@"MMM yyyy"];
    if ([GET_LOCALIZATION isEqualToString: kRussian])
        [btnDateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kRussian]];
    else
        [btnDateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    
    if (self.isStartDate)
    {
        NSDate *offsetDate = [gregorian dateByAddingComponents:offsetComponents toDate: selectedDate options:0];
        
        NSString *selectedDateString;
        NSString *offsetDateString;
        //check end date if more than current date
        if ([offsetDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
        {
            //check start date selection if more than current date.
            if ([selectedDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
            {
                NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
                [selectedComponent setDay: 1];
                selectedDate = [gregorian dateFromComponents: selectedComponent];
            }
            selectedDateString = [btnDateFormatter stringFromDate: selectedDate]; //first of current month
            offsetDateString = [btnDateFormatter stringFromDate: [NSDate date]]; //current date
            offsetDate = [NSDate date]; //set end date to current date
        }
        else
        {
            //start date will never be more than current date if above validation of end date clears.
            selectedDateString = [btnDateFormatter stringFromDate: selectedDate];
            offsetDateString = [btnDateFormatter stringFromDate: offsetDate]; //offset still 2 months later
        }
        
        
        //hardcode to first three character in russian
        if ([GET_LOCALIZATION isEqualToString: kRussian])
        {
            NSMutableArray *selectedDateStringArray = [[NSMutableArray alloc] initWithArray: [selectedDateString componentsSeparatedByString: @" "]];
            NSString *selectedMonth = [selectedDateStringArray objectAtIndex: 0];
            selectedMonth = [selectedMonth substringToIndex: 3];
            selectedDateString = [[NSString stringWithFormat: @"%@ %@", selectedMonth, [selectedDateStringArray objectAtIndex: 1]] capitalizedString];
            
            NSMutableArray *offsetDateStringArray = [[NSMutableArray alloc] initWithArray: [offsetDateString componentsSeparatedByString: @" "]];
            NSString *offsetMonth = [offsetDateStringArray objectAtIndex: 0];
            offsetMonth = [offsetMonth substringToIndex: 3];
            offsetDateString = [[NSString stringWithFormat: @"%@ %@", offsetMonth, [offsetDateStringArray objectAtIndex: 1]] capitalizedString]; //Capitalised, e.g Aug, Sep, instead of aug, sep
        }
        
        //end start is offset by 3 months
        [self.btnStartDate setTitle: selectedDateString forState: UIControlStateNormal];
        [self.btnEndDate setTitle: offsetDateString forState: UIControlStateNormal];
        
        self.startDate = selectedDate;
        self.endDate = offsetDate;
        
    }
    else
    {
        //handle only end date
        
        //set date to end of selected month
        NSDateComponents *endOfMonthComponents = [[NSDateComponents alloc] init];
        [endOfMonthComponents setYear: 0];
        [endOfMonthComponents setMonth: 1];
        [endOfMonthComponents setDay: -1];
        selectedDate = [gregorian dateByAddingComponents: endOfMonthComponents toDate: selectedDate options: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)];
        
        //check date from startdate
        NSDate *offsetDate = [gregorian dateByAddingComponents:offsetComponents toDate: self.startDate options:0];
        
        //if offset date is more than current month, change offset to current month
        if ([offsetDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
        {
            //reset offset date to last day of current month
            NSDateComponents *currentDateComponents = [gregorian components: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
            [currentDateComponents setDay: 1];
            [currentDateComponents setMonth: [currentDateComponents month] + 1];
            offsetDate = [gregorian dateFromComponents: currentDateComponents];
            
            [currentDateComponents setDay: -1];
            [currentDateComponents setMonth: 0];
            [currentDateComponents setYear: 0];
            offsetDate = [gregorian dateByAddingComponents: currentDateComponents toDate: offsetDate options:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) ];
        }
        
        //if selection more than threshold (+2 months from start month)
        if ([selectedDate timeIntervalSinceReferenceDate] > [offsetDate timeIntervalSinceReferenceDate])
        {
            selectedDate = offsetDate;
        }
        else if ([selectedDate timeIntervalSinceReferenceDate] < [self.startDate timeIntervalSinceReferenceDate])
        {
            //if selection less than start month
            selectedDate = self.startDate;
        }
        else
        {
            
        }
        
        //don't need to change start date.
        NSString *selectedDateString = [btnDateFormatter stringFromDate: selectedDate];
        
        //hardcode to first three character in russian
        if ([GET_LOCALIZATION isEqualToString: kRussian])
        {
            NSMutableArray *selectedDateStringArray = [[NSMutableArray alloc] initWithArray: [selectedDateString componentsSeparatedByString: @" "]];
            NSString *selectedMonth = [selectedDateStringArray objectAtIndex: 0];
            selectedMonth = [selectedMonth substringToIndex: 3];
            selectedDateString = [[NSString stringWithFormat: @"%@ %@", selectedMonth, [selectedDateStringArray objectAtIndex: 1]] capitalizedString];
        }
        
        [self.btnEndDate setTitle: selectedDateString forState: UIControlStateNormal];
        self.endDate = selectedDate;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
