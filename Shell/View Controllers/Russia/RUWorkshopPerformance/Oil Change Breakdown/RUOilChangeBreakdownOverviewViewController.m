//
//  RUOilChangeBreakdownOverviewViewController.m
//  Shell
//
//  Created by Jeremy Lua on 4/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUOilChangeBreakdownOverviewViewController.h"

@interface RUOilChangeBreakdownOverviewViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *breakdownTableView;
//@property NSMutableArray *breakdownDataArray;
@property NSMutableDictionary *breakdownDictionary;

@end

enum
{
    PRODUCT_BRAND = 1,
    PRODUCT_TIER,
    PRODUCT_CATEGORY,
    PRODUCT_WORKSHOPS,
} PRODUCT_OILCHANGE_TYPES;

@implementation RUOilChangeBreakdownOverviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = COLOUR_VERYPALEGREY;
    self.breakdownTableView.backgroundColor = COLOUR_VERYPALEGREY;
    self.breakdownTableView.backgroundView = nil;
}

#define START_ANGLE -90 //12 o clock
#define degreesToRadians(degrees)((M_PI * degrees)/180)

-(void) constructCircle:(UIView *)view breakdownArray:(NSMutableArray *)breakdownArray chartIndex:(NSInteger)chartIndex
{
    //breakdownArray = tier or category. pick out the percent
    CGFloat lastAngleInRadians = degreesToRadians(START_ANGLE);
    
    int i = 0;
    for (NSDictionary *dict in breakdownArray)
    {
        CGFloat percentage = [[dict objectForKey: @"TotalPercent"] floatValue];
        CGFloat nextAngleInRadians = lastAngleInRadians + (degreesToRadians(360  * percentage * 0.01));
        
        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
        [bezierPath addArcWithCenter: CGPointMake(view.frame.size.width / 2, view.frame.size.width / 2)
                              radius: (view.frame.size.width / 2) + 15
                          startAngle: lastAngleInRadians
                            endAngle: nextAngleInRadians
                           clockwise:YES];
        
        CAShapeLayer *progressLayer = [[CAShapeLayer alloc] init];
        [progressLayer setPath:bezierPath.CGPath];
        
        UIColor *color = [Helper colorFromHex: [dict objectForKey: @"ColorCode"]];
        [progressLayer setStrokeColor: [color CGColor]];
        
        [progressLayer setFillColor:[UIColor clearColor].CGColor];
        [progressLayer setLineWidth: 30];
        [view.layer addSublayer:progressLayer];
        
        //get ready for the next arc draw
        lastAngleInRadians = nextAngleInRadians;
        i++;
    }
}

-(void)reloadBreakdownData:(nullable NSMutableDictionary *)responseDict
{
    if (responseDict)
    {
        self.breakdownTableView.hidden = NO;
        self.breakdownDictionary = responseDict;
        [self.breakdownTableView reloadData];
    }
    else
    {
        self.breakdownTableView.hidden = YES;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //    return [self.breakdownDataArray count] + 1; //+ 1 for header
    return 4;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return 0;
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 15;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section != 0)
    {
        //        return 1; //return array of array count
        NSMutableArray *sectionArray;
        if (section == PRODUCT_BRAND) //product brand
        {
            sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductBrand"];
        }
        else if (section == PRODUCT_TIER) //product tier
        {
            sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductTier"];
        }
        else if (section == PRODUCT_CATEGORY) //product category
        {
            sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductCategory"];
            
        }
        else
        {
            sectionArray = [self.breakdownDictionary objectForKey: @"WorkshopOrderDetails"];
        }
        return [sectionArray count] + 1; //for footer
    }
    return 1;
}


enum
{
    BREAKDOWNHEADERCELL_OILCHANGE = 1,
    BREAKDOWNHEADERCELL_OILCHANGEDATA,
    BREAKDOWNHEADERCELL_SUBHEADER,
} BREAKDOWNHEADERCELL_OILCHANGE_TAG;

enum
{
    BREAKDOWNCELL_COLOURTAG = 1,
    BREAKDOWNCELL_PRODUCT,
    BREAKDOWNCELL_TOTALUNIT,
    BREAKDOWNCELL_TOTALPERCENT,
    BREAKDOWNCELL_CIRCLEVIEW,   //view to hold circle
    BREAKDOWNCELL_BYPRODUCT, //center of circle
} BREAKDOWNCELL_OILCHANGE_TAG;

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return [UIView new];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"BreakdownHeaderCell"];
    
    UIView *circleView = [cell viewWithTag: BREAKDOWNCELL_CIRCLEVIEW];
    
    
    UILabel *lblProductHeader = [cell viewWithTag:  BREAKDOWNCELL_PRODUCT];
    UILabel *lblByProduct = [cell viewWithTag:  BREAKDOWNCELL_BYPRODUCT];
    if (section == PRODUCT_BRAND)
    {
        lblProductHeader.text = LOCALIZATION(C_RU_BREAKDOWN_PRODUCTBRAND);  //lokalised 28 Jan
        lblByProduct.text = LOCALIZATION(C_RU_BREAKDOWN_BYPRODUCTBRAND);    //lokalised 28 Jan
    }
    else if (section == PRODUCT_TIER)
    {
        lblProductHeader.text = LOCALIZATION(C_RU_BREAKDOWN_PRODUCTTIER);   //lokalised 28 Jan
        lblByProduct.text = LOCALIZATION(C_RU_BREAKDOWN_BYPRODUCTTIER);     //lokalised 28 Jan
    }
    else if (section == PRODUCT_CATEGORY)
    {
        //change to category instead for section 2
        lblProductHeader.text = LOCALIZATION(C_RU_BREAKDOWN_PRODUCTCATEGORY);   //lokalised 28 Jan
        lblByProduct.text = LOCALIZATION(C_RU_BREAKDOWN_BYPRODUCTCATEGORY);     //lokalised 28 Jan
    }
    
    UILabel *lblTotalUnitHeader= [cell viewWithTag:  BREAKDOWNCELL_TOTALUNIT];
    lblTotalUnitHeader.text = LOCALIZATION(C_RU_BREAKDOWN_TOTAL);           //lokalised 28 Jan
    
    UILabel *lblTotalPercentHeader = [cell viewWithTag:  BREAKDOWNCELL_TOTALPERCENT];
    lblTotalPercentHeader.text = LOCALIZATION(C_RU_BREAKDOWN_TOTALPERCENT); //lokalised 28 Jan
    
    cell.contentView.backgroundColor = COLOUR_WHITE;
    
    
    //breakdownArray = tier or category. pick out the percent
    NSMutableArray *sectionArray;
    if (section == PRODUCT_BRAND) //product brand
        sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductBrand"];
    else if (section == PRODUCT_TIER) //product tier
        sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductTier"];
    else if (section == PRODUCT_CATEGORY) //product category
        sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductCategory"];
    
    [self constructCircle: [cell viewWithTag: BREAKDOWNCELL_CIRCLEVIEW] breakdownArray:sectionArray chartIndex: section];
    return cell.contentView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch (indexPath.section) {
        case 0:
        {
            //header
            cell = [tableView dequeueReusableCellWithIdentifier: @"MainHeaderCell"];
            
            cell.backgroundColor = [UIColor clearColor];
            
            if (!self.breakdownDictionary)
                break;
            
            UILabel *lblOilChange = [cell viewWithTag: BREAKDOWNHEADERCELL_OILCHANGE];
            lblOilChange.text = LOCALIZATION(C_RU_BREAKDOWN_OILCHANGEBYID); //lokalised 28 Jan
            
            UILabel *lblOilChangeData = [cell viewWithTag: BREAKDOWNHEADERCELL_OILCHANGEDATA];
            lblOilChangeData.text = [self.breakdownDictionary objectForKey: @"TotalOilChanges"];
            
            UILabel *lblSubheader = [cell viewWithTag: BREAKDOWNHEADERCELL_SUBHEADER];
            lblSubheader.text = [NSString stringWithFormat: @"%@ %@", [self.breakdownDictionary objectForKey: @"UpdatedOnText"], [self.breakdownDictionary objectForKey: @"UpdatedOn"] ];
            break;
        }
        default:
        {
            NSMutableArray *sectionArray;
            if (indexPath.section == PRODUCT_BRAND) //product brand
            {
                sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductBrand"];
                
            }
            else if (indexPath.section == PRODUCT_TIER) //product tier
            {
                sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductTier"];
                
            }
            else if (indexPath.section == PRODUCT_CATEGORY) //product category
            {
                sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductCategory"];
            }
            
            if (indexPath.row < [sectionArray count])
            {
                cell = [tableView dequeueReusableCellWithIdentifier: @"BreakdownCell"];
                
                cell.backgroundColor = COLOUR_WHITE;
                
                //            else
                //            {
                //                sectionArray = [self.breakdownDictionary objectForKey: @"WorkshopOrderDetails"];
                //            }
                
                NSDictionary *cellData = [sectionArray objectAtIndex: indexPath.row];
                
                
                UIView *colourTagView = [cell viewWithTag: BREAKDOWNCELL_COLOURTAG];
                colourTagView.backgroundColor = [Helper colorFromHex: [cellData objectForKey: @"ColorCode"]];
                
                UILabel *lblProductData = [cell viewWithTag:  BREAKDOWNCELL_PRODUCT];
                
                //1 = ProductBrand, 2 = ProductTier, 2 = ProductCategory
                if (indexPath.section == PRODUCT_BRAND) //product tier
                {
                    lblProductData.text = [cellData objectForKey: @"ProductBrand"];
                }
                else if (indexPath.section == PRODUCT_TIER) //product tier
                {
                    lblProductData.text = [cellData objectForKey: @"ProductTier"];
                }
                else if (indexPath.section == PRODUCT_CATEGORY)  //product category
                {
                    
                    lblProductData.text = [cellData objectForKey: @"ProductCategory"];
                }
                //            else
                //            {
                //                sectionArray = [self.breakdownDictionary objectForKey: @"WorkshopOrderDetails"];
                //            }
                
                UILabel *lblTotalUnitData = [cell viewWithTag:  BREAKDOWNCELL_TOTALUNIT];
                
                lblTotalUnitData.text = [[cellData objectForKey: @"Total"] stringValue];
                
                UILabel *lblTotalPercentData = [cell viewWithTag:  BREAKDOWNCELL_TOTALPERCENT];
                lblTotalPercentData.text = [[cellData objectForKey: @"TotalPercent"] stringValue];
            }
            else
            {
                cell = [tableView dequeueReusableCellWithIdentifier: @"BreakdownFooterCell"];
                
                UILabel *lblFooter = [cell viewWithTag: 1];
                if (indexPath.section == PRODUCT_BRAND) //product brand
                {
                    lblFooter.text = LOCALIZATION(C_RU_BREAKDOWN_OILCHANGE_BRANDFOOTER);    //lokalised 28 Jan
                }
//                else if (indexPath.section == PRODUCT_TIER) //product tier
//                {
//                    lblFooter.text = LOCALIZATION(@"* Oil changes registered with non-Shell branded engine oil are all grouped under \"Other Brands \"");
//                }
//                else if (indexPath.section == PRODUCT_CATEGORY) //product category
                else
                {
                    lblFooter.text = LOCALIZATION(C_RU_BREAKDOWN_OILCHANGE_FOOTER); //lokalised 28 Jan
                }
            }
        }
            break;
    }
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
