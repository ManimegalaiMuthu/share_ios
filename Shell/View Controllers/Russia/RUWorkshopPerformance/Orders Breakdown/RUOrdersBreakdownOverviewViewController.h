//
//  RUOrdersBreakdownOverviewViewController.h
//  Shell
//
//  Created by Jeremy Lua on 4/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface RUOrdersBreakdownOverviewViewController : BaseVC

-(void)reloadBreakdownData:(nullable NSMutableDictionary *)responseDict;
@end

NS_ASSUME_NONNULL_END
