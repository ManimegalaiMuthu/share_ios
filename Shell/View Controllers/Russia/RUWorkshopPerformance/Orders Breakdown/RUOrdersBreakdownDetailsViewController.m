//
//  RUOrdersBreakdownDetailsViewController.m
//  Shell
//
//  Created by Jeremy Lua on 4/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUOrdersBreakdownDetailsViewController.h"

@interface RUOrdersBreakdownDetailsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *breakdownTableView;
//@property NSMutableArray *breakdownDataArray;
@property NSMutableDictionary *breakdownDictionary;

@end

@implementation RUOrdersBreakdownDetailsViewController
enum
{
    PRODUCT_TIER = 1,
    PRODUCT_CATEGORY,
    PRODUCT_WORKSHOPS,
} PRODUCT_TYPES_ORDERS_DETAILS;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = COLOUR_VERYPALEGREY;
    self.breakdownTableView.backgroundColor = COLOUR_VERYPALEGREY;
    self.breakdownTableView.backgroundView = nil;
}

-(void)reloadBreakdownData:(nullable NSMutableDictionary *)responseDict
{
    if (responseDict)
    {
        self.breakdownTableView.hidden = NO;
        self.breakdownDictionary = responseDict;
        [self.breakdownTableView reloadData];
    }
    else
    {
        self.breakdownTableView.hidden = YES;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //    return [self.breakdownDataArray count] + 1; //+ 1 for header
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return 0;
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 15;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section != 0)
    {
        //        return 1; //return array of array count
        NSMutableArray *sectionArray;
        if (section == PRODUCT_TIER) //product tier
        {
            sectionArray = [self.breakdownDictionary objectForKey: @"ProductTierList"];
        }
        else if (section == PRODUCT_CATEGORY) //product category
        {
            sectionArray = [self.breakdownDictionary objectForKey: @"ProductCategoryList"];
            
        }
        else
        {
            sectionArray = [self.breakdownDictionary objectForKey: @"WorkshopOrderDetails"];
        }
        return [sectionArray count] + 1; //+1 for footer
    }
    return 1;
}

enum
{
    BREAKDOWNCELL_COLOURTAG = 1,
    BREAKDOWNCELL_PRODUCT,
    BREAKDOWNCELL_HEADERCELL_HEADER,
    BREAKDOWNCELL_MONTH_ONE,
    BREAKDOWNCELL_MONTH_TWO,
    BREAKDOWNCELL_MONTH_THREE,
    BREAKDOWNCELL_TOTAL,
} BREAKDOWNCELL_TAG_ORDERS_DETAILS;


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return [UIView new];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"BreakdownHeaderCell"];
    
    UILabel *lblProductHeader = [cell viewWithTag:  BREAKDOWNCELL_HEADERCELL_HEADER];
    
    if (section == PRODUCT_TIER)
    {
        lblProductHeader.text = LOCALIZATION(C_RU_BREAKDOWN_BYPRODUCTTIER); //lokalised 28 Jan
    }
    else if (section == PRODUCT_CATEGORY)
    {
        //change to category instead for section 2
        lblProductHeader.text = LOCALIZATION(C_RU_BREAKDOWN_BYPRODUCTCATEGORY); //lokalised 28 Jan
    }
    
    NSArray *monthListArray = [self.breakdownDictionary objectForKey: @"MonthList"];
    UILabel *lblMonthOne = [cell viewWithTag:  BREAKDOWNCELL_MONTH_ONE];
    UILabel *lblMonthTwo = [cell viewWithTag:  BREAKDOWNCELL_MONTH_TWO];
    UILabel *lblMonthThree = [cell viewWithTag:  BREAKDOWNCELL_MONTH_THREE];
    
    if ([monthListArray count] < 3)
    {
        lblMonthThree.hidden = YES;
    }
    else
    {
        lblMonthThree.text = [[monthListArray objectAtIndex: 2] objectForKey: @"MonthName"];
    }
    
    if ([monthListArray count] < 2)
    {
        lblMonthTwo.hidden = YES;
    }
    else
    {
        lblMonthTwo.text = [[monthListArray objectAtIndex: 1] objectForKey: @"MonthName"];        
    }
    
    if ([monthListArray count] < 1)
    {
        lblMonthOne.hidden = YES;
    }
    else
    {
        lblMonthOne.text = [[monthListArray objectAtIndex: 0] objectForKey: @"MonthName"];
    }
    
    
    UILabel *lblTotalHeader = [cell viewWithTag:  BREAKDOWNCELL_TOTAL];
    lblTotalHeader.text = LOCALIZATION(C_RU_BREAKDOWN_TOTAL);   //lokalised 28 Jan
    
    cell.contentView.backgroundColor = COLOUR_WHITE;
    
    return cell.contentView;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch (indexPath.section) {
        case 0:
        {
            //header
            cell = [tableView dequeueReusableCellWithIdentifier: @"MainHeaderCell"];
            
            cell.backgroundColor = [UIColor clearColor];
            
            if (!self.breakdownDictionary)
                break;
            
            UILabel *lblSubheader = [cell viewWithTag: BREAKDOWNCELL_HEADERCELL_HEADER];
            lblSubheader.text = [NSString stringWithFormat: @"%@ %@", [self.breakdownDictionary objectForKey: @"UpdatedOnText"], [self.breakdownDictionary objectForKey: @"UpdatedOn"] ];
            break;
        }
        default:
        {
            NSMutableArray *sectionArray;
            if (indexPath.section == PRODUCT_TIER) //product tier
            {
                sectionArray = [self.breakdownDictionary objectForKey: @"ProductTierList"];
                
            }
            else if (indexPath.section == PRODUCT_CATEGORY) //product category
            {
                sectionArray = [self.breakdownDictionary objectForKey: @"ProductCategoryList"];
            }
            
            if (indexPath.row < [sectionArray count])
            {
                cell = [tableView dequeueReusableCellWithIdentifier: @"BreakdownCell"];
                //            else
                //            {
                //                sectionArray = [self.breakdownDictionary objectForKey: @"WorkshopOrderDetails"];
                //            }
                
                NSDictionary *cellData = [sectionArray objectAtIndex: indexPath.row];
                
                
                UIView *colourTagView = [cell viewWithTag: BREAKDOWNCELL_COLOURTAG];
                colourTagView.backgroundColor = [Helper colorFromHex: [cellData objectForKey: @"ColorCode"]];

                UILabel *lblProductData = [cell viewWithTag:  BREAKDOWNCELL_PRODUCT];
                //1 = ProductTier, 2 = ProductCategory
                if (indexPath.section == PRODUCT_TIER) //product tier
                {
                    lblProductData.text = [cellData objectForKey: @"ProductTier"];
                }
                else if (indexPath.section == PRODUCT_CATEGORY)  //product category
                {
                    lblProductData.text = [cellData objectForKey: @"ProductCategory"];
                }
    //            else
    //            {
    //                sectionArray = [self.breakdownDictionary objectForKey: @"WorkshopOrderDetails"];
    //            }
                
                NSArray *monthListArray = [self.breakdownDictionary objectForKey: @"MonthList"];
                
                UILabel *lblMonthOneData = [cell viewWithTag:  BREAKDOWNCELL_MONTH_ONE];
                UILabel *lblMonthTwoData = [cell viewWithTag:  BREAKDOWNCELL_MONTH_TWO];
                UILabel *lblMonthThreeData = [cell viewWithTag:  BREAKDOWNCELL_MONTH_THREE];
                
                
                lblMonthOneData.hidden = NO;
                lblMonthTwoData.hidden = NO;
                lblMonthThreeData.hidden = NO;
                if ([monthListArray count] < 3)
                    lblMonthThreeData.hidden = YES;
                
                if ([monthListArray count] < 2)
                    lblMonthTwoData.hidden = YES;
                
                if ([monthListArray count] < 1)
                    lblMonthOneData.hidden = YES;
                
                lblMonthOneData.text = [cellData objectForKey: @"MonthValue1"];
                if ([[cellData objectForKey: @"MonthValue2"] isKindOfClass: [NSNull class]])
                    lblMonthTwoData.hidden = YES;
                else
                    lblMonthTwoData.text = [cellData objectForKey: @"MonthValue2"];
                
                if ([[cellData objectForKey: @"MonthValue3"] isKindOfClass: [NSNull class]])
                    lblMonthThreeData.hidden = YES;
                else
                    lblMonthThreeData.text = [cellData objectForKey: @"MonthValue3"];
            }
            else
            {
                cell = [tableView dequeueReusableCellWithIdentifier: @"BreakdownFooterCell"];
                
                UILabel *lblFooter = [cell viewWithTag: 1];
//                lblFooter.text = LOCALIZATION(@"* Order volume are calculated in litres");
                lblFooter.text = @"";
            }
            
            cell.backgroundColor = COLOUR_WHITE;
        }
            break;
    }
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
