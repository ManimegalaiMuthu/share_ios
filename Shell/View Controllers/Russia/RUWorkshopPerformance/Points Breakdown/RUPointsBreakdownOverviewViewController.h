//
//  RUPointsBreakdownOverviewViewController.h
//  Shell
//
//  Created by Jeremy Lua on 8/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface RUPointsBreakdownOverviewViewController : BaseVC

-(void)reloadBreakdownData:(nullable NSMutableDictionary *)responseDict;
@end

NS_ASSUME_NONNULL_END
