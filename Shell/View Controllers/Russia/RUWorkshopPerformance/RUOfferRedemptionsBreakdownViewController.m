//
//  RUOfferRedemptionsBreakdownViewController.m
//  Shell
//
//  Created by Jeremy Lua on 8/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUOfferRedemptionsBreakdownViewController.h"

#import "CASMonthYearPicker.h"

@interface RUOfferRedemptionsBreakdownViewController () <WebServiceManagerDelegate, MonthYearPickerDelegate, UITableViewDelegate, UITableViewDataSource>
@property NSMutableArray *sectionOpenControlArray; //to control array of BOOL to control sections open/close

@property (weak, nonatomic) IBOutlet UIButton *btnStartDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEndDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property NSDate *startDate;
@property NSDate *endDate;

@property (weak, nonatomic) IBOutlet UIButton *btnEnter;

@property BOOL isStartDate;
@property CASMonthYearPicker *dateView;

@property (weak, nonatomic) IBOutlet UITableView *offersTableView;
@property NSDictionary *breakdownDictionary;

@property NSMutableArray *oilChangeArray;
@property NSString *oilChangeHeaderString;
@property NSMutableArray *customerSignupArray;
@property NSString *customerSignupHeaderString;

@end

@implementation RUOfferRedemptionsBreakdownViewController
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_RU_BREAKDOWN_OFFER_TITLE) subtitle: @""];  //lokalised 28 Jan
    
    [self setNavBackBtn];
    self.lblTo.text = LOCALIZATION(C_RU_STEPONE_TO);    //lokalised 28 Jan
    [self.btnEnter setTitle: LOCALIZATION(C_RU_STEPONE_ENTER) forState: UIControlStateNormal];  //lokalised 28 Jan
    self.offersTableView.backgroundColor = COLOUR_VERYPALEGREY;
    self.offersTableView.backgroundView = nil;
    
    dateView = [[CASMonthYearPicker alloc] initWithTitle:LOCALIZATION(C_PERFORMANCE_SELECTDATE) previouslySelected:nil withYearHidden:NO];  //lokalised 28 Jan
    
    dateView.delegate = self;
    

    //setup default date
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    //setup the necesary date adjustments
    NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
    [selectedComponent setDay: 1];
    NSDate *selectedDate = [gregorian dateFromComponents: selectedComponent];
    
    [selectedComponent setDay: 0];
    [selectedComponent setMonth: -2];
    [selectedComponent setYear: 0];
    selectedDate = [gregorian dateByAddingComponents: selectedComponent toDate: selectedDate options:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)];
    self.isStartDate = YES;
    self.startDate = selectedDate;
    [self selectedMonthYear: selectedDate];
    [self enterPressed: nil];
}

- (IBAction)enterPressed:(id)sender
{
    NSMutableDictionary *tradeDict = [[NSMutableDictionary alloc] init];
    //    LoginInfo *loginInfo = [mSession loadUserProfile];
    //
    //    [tradeDict setObject: loginInfo.tradeID forKey: @"TradeID"];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy/MM/dd"];
    
    [tradeDict setObject: [outputFormatter stringFromDate: self.startDate] forKey:@"StartDate"];
    [tradeDict setObject: [outputFormatter stringFromDate: self.endDate] forKey:@"EndDate"];
    
    [[WebServiceManager sharedInstance] fetchOfferRedemptionBreakdown: tradeDict vc: self];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_FETCH_OFFERREDEMPTIONBREAKDOWN:
        {
            self.breakdownDictionary = [response getGenericResponse];
            
            self.offersTableView.hidden = NO;
            [self.offersTableView reloadData];
        }
            break;
            
        default:
            break;
    }
}
- (void)processFailed:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_FETCH_OFFERREDEMPTIONBREAKDOWN:
        {
            self.offersTableView.hidden = YES;
        }
            break;
            
        default:
            break;
    }
}

- (IBAction)datePressed:(UIButton *)sender {
    [self.view endEditing: YES];
    
    if ([sender isEqual: self.btnStartDate])
        self.isStartDate = YES;
    else
        self.isStartDate = NO;
    
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}

#pragma Date Picker delegate method
- (void)selectedMonthYear:(NSDate *)selectedDate
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    
    //setup the necesary date adjustments
    NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: selectedDate];
    [selectedComponent setDay: 1];
    selectedDate = [gregorian dateFromComponents: selectedComponent];
    
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setMonth: 3];
    [offsetComponents setDay: -1];
    
    NSDateFormatter *btnDateFormatter = [[NSDateFormatter alloc] init];
    [btnDateFormatter setDateFormat:@"MMM yyyy"];
    if ([GET_LOCALIZATION isEqualToString: kRussian])
        [btnDateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kRussian]];
    else
        [btnDateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    
    if (self.isStartDate)
    {
        NSDate *offsetDate = [gregorian dateByAddingComponents:offsetComponents toDate: selectedDate options:0];
        
        NSString *selectedDateString;
        NSString *offsetDateString;
        //check end date if more than current date
        if ([offsetDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
        {
            //check start date selection if more than current date.
            if ([selectedDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
            {
                NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
                [selectedComponent setDay: 1];
                selectedDate = [gregorian dateFromComponents: selectedComponent];
            }
            selectedDateString = [btnDateFormatter stringFromDate: selectedDate]; //first of current month
            offsetDateString = [btnDateFormatter stringFromDate: [NSDate date]]; //current date
            offsetDate = [NSDate date]; //set end date to current date
        }
        else
        {
            //start date will never be more than current date if above validation of end date clears.
            selectedDateString = [btnDateFormatter stringFromDate: selectedDate];
            offsetDateString = [btnDateFormatter stringFromDate: offsetDate]; //offset still 2 months later
        }
        
        
        //hardcode to first three character in russian
        if ([GET_LOCALIZATION isEqualToString: kRussian])
        {
            NSMutableArray *selectedDateStringArray = [[NSMutableArray alloc] initWithArray: [selectedDateString componentsSeparatedByString: @" "]];
            NSString *selectedMonth = [selectedDateStringArray objectAtIndex: 0];
            selectedMonth = [selectedMonth substringToIndex: 3];
            selectedDateString = [[NSString stringWithFormat: @"%@ %@", selectedMonth, [selectedDateStringArray objectAtIndex: 1]] capitalizedString];
            
            NSMutableArray *offsetDateStringArray = [[NSMutableArray alloc] initWithArray: [offsetDateString componentsSeparatedByString: @" "]];
            NSString *offsetMonth = [offsetDateStringArray objectAtIndex: 0];
            offsetMonth = [offsetMonth substringToIndex: 3];
            offsetDateString = [[NSString stringWithFormat: @"%@ %@", offsetMonth, [offsetDateStringArray objectAtIndex: 1]] capitalizedString]; //Capitalised, e.g Aug, Sep, instead of aug, sep
        }
        
        //end start is offset by 3 months
        [self.btnStartDate setTitle: selectedDateString forState: UIControlStateNormal];
        [self.btnEndDate setTitle: offsetDateString forState: UIControlStateNormal];
        
        self.startDate = selectedDate;
        self.endDate = offsetDate;
        
    }
    else
    {
        //handle only end date
        
        //set date to end of selected month
        NSDateComponents *endOfMonthComponents = [[NSDateComponents alloc] init];
        [endOfMonthComponents setYear: 0];
        [endOfMonthComponents setMonth: 1];
        [endOfMonthComponents setDay: -1];
        selectedDate = [gregorian dateByAddingComponents: endOfMonthComponents toDate: selectedDate options: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)];
        
        //check date from startdate
        NSDate *offsetDate = [gregorian dateByAddingComponents:offsetComponents toDate: self.startDate options:0];
        
        //if offset date is more than current month, change offset to current month
        if ([offsetDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
        {
            //reset offset date to last day of current month
            NSDateComponents *currentDateComponents = [gregorian components: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
            [currentDateComponents setDay: 1];
            [currentDateComponents setMonth: [currentDateComponents month] + 1];
            offsetDate = [gregorian dateFromComponents: currentDateComponents];
            
            [currentDateComponents setDay: -1];
            [currentDateComponents setMonth: 0];
            [currentDateComponents setYear: 0];
            offsetDate = [gregorian dateByAddingComponents: currentDateComponents toDate: offsetDate options:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) ];
        }
        
        //if selection more than threshold (+2 months from start month)
        if ([selectedDate timeIntervalSinceReferenceDate] > [offsetDate timeIntervalSinceReferenceDate])
        {
            selectedDate = offsetDate;
        }
        else if ([selectedDate timeIntervalSinceReferenceDate] < [self.startDate timeIntervalSinceReferenceDate])
        {
            //if selection less than start month
            selectedDate = self.startDate;
        }
        else
        {
            
        }
        
        //don't need to change start date.
        NSString *selectedDateString = [btnDateFormatter stringFromDate: selectedDate];
        
        //hardcode to first three character in russian
        if ([GET_LOCALIZATION isEqualToString: kRussian])
        {
            NSMutableArray *selectedDateStringArray = [[NSMutableArray alloc] initWithArray: [selectedDateString componentsSeparatedByString: @" "]];
            NSString *selectedMonth = [selectedDateStringArray objectAtIndex: 0];
            selectedMonth = [selectedMonth substringToIndex: 3];
            selectedDateString = [[NSString stringWithFormat: @"%@ %@", selectedMonth, [selectedDateStringArray objectAtIndex: 1]] capitalizedString];
        }
        
        [self.btnEndDate setTitle: selectedDateString forState: UIControlStateNormal];
        self.endDate = selectedDate;
    }
}


#define START_ANGLE -90 //12 o clock
#define degreesToRadians(degrees)((M_PI * degrees)/180)

-(CGFloat) radianAngleFromConstructCircle:(UIView *)view colourHexString:(NSString *)colourHexString percentage:(CGFloat)percentage lastAngleInRadians:(CGFloat) lastAngleInRadians
{
    CGFloat nextAngleInRadians = lastAngleInRadians + (degreesToRadians(360  * percentage * 0.01));
    
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    [bezierPath addArcWithCenter: CGPointMake(view.frame.size.width / 2, view.frame.size.width / 2)
                          radius: (view.frame.size.width / 2) + 15
                      startAngle: lastAngleInRadians
                        endAngle: nextAngleInRadians
                       clockwise:YES];
    
    CAShapeLayer *progressLayer = [[CAShapeLayer alloc] init];
    [progressLayer setPath:bezierPath.CGPath];
    
    UIColor *color = [Helper colorFromHex: colourHexString];
    [progressLayer setStrokeColor: [color CGColor]];
    
    [progressLayer setFillColor:[UIColor clearColor].CGColor];
    [progressLayer setLineWidth: 30];
    [view.layer addSublayer:progressLayer];
    
    //get ready for the next arc draw
    return nextAngleInRadians;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return 0;
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 15;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section != 0)
    {
        if (section == 1) //total sms sent
        {
            return 2;
        }
        else if (section == 2) //by offer
        {
            NSMutableArray *sectionArray = [self.breakdownDictionary objectForKey: @"OfferDetails"];
            return [sectionArray count];
        }
    }
    return 1;
}

enum
{
    BREAKDOWNCELL_HEADER = 1,
    BREAKDOWNCELL_HEADERDATA,
    BREAKDOWNCELL_CIRCLEVIEW,   //view to hold circle
    BREAKDOWNCELL_REDEMPTIONRATE, //center of circle
    BREAKDOWNCELL_COLOURTAG,
    BREAKDOWNCELL_TEXT,
    BREAKDOWNCELL_TOTAL,
    BREAKDOWNCELL_TOTALPERCENT,
    BREAKDOWNCELL_OFFERNAME = 9,
    BREAKDOWNCELL_SENT,
    BREAKDOWNCELL_SENTNUM,
    BREAKDOWNCELL_REDEEMED,
    BREAKDOWNCELL_REDEEMEDNUM,
    BREAKDOWNCELL_REDEEMEDPERCENT,
} BREAKDOWNCELL_OFFERS_TAG;


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return [UIView new];
    
    UITableViewCell *cell;
    
    if (section == 1)
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"BreakdownHeaderCell"];
        
        UILabel *lblHeader = [cell viewWithTag:  BREAKDOWNCELL_HEADER];
        lblHeader.text = LOCALIZATION(C_RU_BREAKDOWN_OFFER_TOTALSMS);   //lokalised 28 Jan
        
        UILabel *lblHeaderData = [cell viewWithTag:  BREAKDOWNCELL_HEADERDATA];
        lblHeaderData.text = [self.breakdownDictionary objectForKey: @"TotalSMSSent"];
        
        
        UILabel *lblAverageRate = [cell viewWithTag: BREAKDOWNCELL_REDEMPTIONRATE];
        lblAverageRate.text = LOCALIZATION(C_RU_BREAKDOWN_OFFER_AVERAGERATE);   //lokalised 28 Jan
        
        
        //process redeemed first
        UIView *circleView = [cell viewWithTag: BREAKDOWNCELL_CIRCLEVIEW];
        CGFloat nextAngleInRadians = [self radianAngleFromConstructCircle: circleView colourHexString: [self.breakdownDictionary objectForKey: @"RedeemedColorCode"] percentage: [[self.breakdownDictionary objectForKey: @"RedeemedPercent"] floatValue] lastAngleInRadians: degreesToRadians(START_ANGLE)];
        
        //process unredeemed
        [self radianAngleFromConstructCircle: circleView colourHexString: [self.breakdownDictionary objectForKey: @"UnRedeemedColorCode"] percentage: [[self.breakdownDictionary objectForKey: @"UnRedeemedPercent"] floatValue] lastAngleInRadians: nextAngleInRadians];
        
        UILabel *lblTotal = [cell viewWithTag: BREAKDOWNCELL_TOTAL];
        lblTotal.text = LOCALIZATION(C_RU_BREAKDOWN_OFFER_TOTAL);   //lokalised 28 Jan
        
        UILabel *lblTotalPercent = [cell viewWithTag: BREAKDOWNCELL_TOTALPERCENT];
        lblTotalPercent.text = LOCALIZATION(C_RU_BREAKDOWN_OFFER_TOTALPERCENT); //lokalised 28 Jan
    }
    else if (section == 2)
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"FullHeaderCell"];
        
        UILabel *lblHeader = [cell viewWithTag:  BREAKDOWNCELL_HEADER];
        lblHeader.text = LOCALIZATION(C_RU_BREAKDOWN_OFFER_BYOFFER);    //lokalised 28 Jan
        
        UILabel *lblOfferName = [cell viewWithTag: BREAKDOWNCELL_OFFERNAME];
        lblOfferName.text = LOCALIZATION(C_RU_BREAKDOWN_OFFER_OFFERNAME);   //lokalised 28 Jan
        
        UILabel *lblSent = [cell viewWithTag: BREAKDOWNCELL_SENT];
        lblSent.text = LOCALIZATION(C_RU_BREAKDOWN_OFFER_SENT); //lokalised 28 Jan
        
        UILabel *lblSentNum = [cell viewWithTag: BREAKDOWNCELL_SENTNUM];
        lblSentNum.text = LOCALIZATION(C_RU_BREAKDOWN_OFFER_NO);    //lokalised 28 Jan
        
        UILabel *lblRedeemed = [cell viewWithTag: BREAKDOWNCELL_REDEEMED];
        lblRedeemed.text = LOCALIZATION(C_RU_BREAKDOWN_OFFER_REDEEMEDNO);   //lokalised 28 Jan
        
        UILabel *lblRedeemNum = [cell viewWithTag: BREAKDOWNCELL_REDEEMEDNUM];
        lblRedeemNum.text = LOCALIZATION(C_RU_BREAKDOWN_OFFER_NO);  //lokalised 28 Jan
        
        UILabel *lblRedeemedPercent = [cell viewWithTag: BREAKDOWNCELL_REDEEMEDPERCENT];
        lblRedeemedPercent.text = LOCALIZATION(@"%");
    }
    cell.contentView.backgroundColor = COLOUR_WHITE;
    
    return cell.contentView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch (indexPath.section) {
        case 0:
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"MainHeaderCell"];
            
            cell.backgroundColor = [UIColor clearColor];
            
            if (!self.breakdownDictionary)
                break;
            
            UILabel *lblUpdated = [cell viewWithTag: 3];
            lblUpdated.text = [NSString stringWithFormat: @"%@ %@", [self.breakdownDictionary objectForKey: @"UpdatedOnText"], [self.breakdownDictionary objectForKey: @"UpdatedOn"] ];
        }
            break;
        case 1:
        {
            
            cell = [tableView dequeueReusableCellWithIdentifier: @"BreakdownCell"];
            
            UIView *colourTagView = [cell viewWithTag: BREAKDOWNCELL_COLOURTAG];
            
            UILabel *lblText = [cell viewWithTag: BREAKDOWNCELL_TEXT];
            
            UILabel *lblTotal = [cell viewWithTag: BREAKDOWNCELL_TOTAL];
            
            UILabel *lblTotalPercent = [cell viewWithTag: BREAKDOWNCELL_TOTALPERCENT];
            
            if (indexPath.row == 0)
            {
                colourTagView.backgroundColor = [Helper colorFromHex: [self.breakdownDictionary objectForKey: @"RedeemedColorCode"]];
                
                lblText.text = LOCALIZATION(C_RU_BREAKDOWN_OFFER_REDEEMED); //lokalised 28 Jan
                
                lblTotal.text = [[self.breakdownDictionary objectForKey: @"TotalRedeemed"] stringValue];
                lblTotalPercent.text = [self.breakdownDictionary objectForKey: @"RedeemedPercent"];
            }
            else
            {
                colourTagView.backgroundColor = [Helper colorFromHex: [self.breakdownDictionary objectForKey: @"UnRedeemedColorCode"]];
                
                lblText.text = LOCALIZATION(C_RU_BREAKDOWN_OFFER_UNREDEEMED);   //lokalised 28 Jan
                
                lblTotal.text = [[self.breakdownDictionary objectForKey: @"TotalUnRedeemed"] stringValue];
                lblTotalPercent.text = [self.breakdownDictionary objectForKey: @"UnRedeemedPercent"];
            }
        }
            break;
        default:
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"OfferBreakdownCell"];
            
            NSDictionary *cellData = [[self.breakdownDictionary objectForKey: @"OfferDetails"] objectAtIndex: indexPath.row];

            UILabel *lblOfferName = [cell viewWithTag: BREAKDOWNCELL_OFFERNAME];
            lblOfferName.text = [cellData objectForKey: @"OfferTitle"];
            
            UILabel *lblSentNum = [cell viewWithTag: BREAKDOWNCELL_SENTNUM];
            lblSentNum.text = [cellData objectForKey: @"TotalSMSSent"];
            
            UILabel *lblRedeemNum = [cell viewWithTag: BREAKDOWNCELL_REDEEMEDNUM];
            lblRedeemNum.text = [cellData objectForKey: @"TotalRedeemed"];
            
            UILabel *lblRedeemPercent = [cell viewWithTag: BREAKDOWNCELL_REDEEMEDPERCENT];
            lblRedeemPercent.text = [cellData objectForKey: @"RedeemedPercent"];
        }
            break;
    }
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
