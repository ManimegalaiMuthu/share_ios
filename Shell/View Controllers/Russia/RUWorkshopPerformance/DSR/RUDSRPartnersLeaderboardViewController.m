//
//  RUDSRPartnersLeaderboardViewController.m
//  Shell
//
//  Created by Jeremy Lua on 4/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUDSRPartnersLeaderboardViewController.h"

@interface RUDSRPartnersLeaderboardViewController ()<UITableViewDelegate, UITableViewDataSource, WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *leaderBoardTableView;
@property (weak, nonatomic) IBOutlet UILabel *lblFooter;

@property NSMutableArray *sectionOpenControlArray; //to control array of BOOL to control sections open/close

@property NSMutableArray *tableData;
@property NSMutableArray *tableHeaders;

@end

@implementation RUDSRPartnersLeaderboardViewController

enum
{
    kDSRLEADERBOARD_TAGS_Plus = 9999,
    kDSRLEADERBOARD_TAGS_Button = 10000,
    kDSRLEADERBOARD_TAGS_Header = 10001,
    kDSRLEADERBOARD_TAGS_Rank,
    kDSRLEADERBOARD_TAGS_Daily, //imageview also same tag
    kDSRLEADERBOARD_TAGS_Mechanic,
    kDSRLEADERBOARD_TAGS_Number,
} kDSRLEADERBOARD_TAGS_TYPE;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupInterface];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_RU_PERFORMANCE_DASHBOARD_PARTNER),LOCALIZATION_EN(C_PERFORMANCE_LEADERBOARD)] screenClass:nil];
}

-(void) setupInterface {
    self.lblFooter.font = FONT_B1;
//    if(@available(iOS 11, *))
//    {
//        self.leaderBoardTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
//    } else {
//        self.automaticallyAdjustsScrollViewInsets = NO;
//    }

//    [self.leaderBoardTableView setContentInset:UIEdgeInsetsMake(15, 0, 15, 0)];
    
    self.leaderBoardTableView.tableFooterView = [UIView new];
    self.leaderBoardTableView.backgroundColor = [UIColor clearColor];
    self.leaderBoardTableView.backgroundView = nil;
    
    [[WebServiceManager sharedInstance] fetchLeaderboard: @{@"ViewType": @(1)} vc:self];
}

- (void)processCompleted:(WebServiceResponse *)response {
    switch (response.webserviceCall) {
        case kWEBSERVICE_FETCH_LEADERBOARD:
        {
            NSDictionary *respData = [response getGenericResponse];
            self.sectionOpenControlArray = [[NSMutableArray alloc] init];
            self.tableData = [[NSMutableArray alloc] init];
            self.tableHeaders = [[NSMutableArray alloc] init];
            
            if (![[respData objectForKey: @"OrderVolume"]isKindOfClass:[NSNull class]])
            {
                [self.sectionOpenControlArray addObject: @(NO)];
                [self.tableData addObject:[respData objectForKey: @"OrderVolume"]];
                [self.tableHeaders addObject:[respData objectForKey: @"OrderVolumeHeaderText"]];
            }
            if (![[respData objectForKey: @"OilChange"]isKindOfClass:[NSNull class]])
            {
                [self.sectionOpenControlArray addObject: @(NO)];
                [self.tableData addObject:[respData objectForKey: @"OilChange"]];
                [self.tableHeaders addObject:[respData objectForKey: @"OilChangeHeaderText"]];
            }
            if (![[respData objectForKey: @"CustomerSignUps"]isKindOfClass:[NSNull class]])
            {
                [self.sectionOpenControlArray addObject: @(NO)];
                [self.tableData addObject:[respData objectForKey: @"CustomerSignUps"]];
                [self.tableHeaders addObject:[respData objectForKey: @"CustomerSignupHeaderText"]];
            }
            self.lblFooter.text = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_RU_LEADERBOARD_DSRFOOTER), [respData objectForKey: @"UpdateText"]];  //lokalised 28 Jan
            
            [self.leaderBoardTableView reloadData];
        }
            break;
            
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _sectionOpenControlArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([[self.sectionOpenControlArray objectAtIndex: section] boolValue])
    {
         NSArray *dataArray = [self.tableData objectAtIndex:section];
         return dataArray.count;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"RUDSRBodyCell"];
    //    NSDictionary *sectionData = [self.leaderboardArray objectAtIndex: indexPath.section];
   
     NSArray *dataArray = [self.tableData objectAtIndex:indexPath.section];
    //    = [sectionData objectForKey: @"Data"];
    
    NSDictionary *cellData = [dataArray objectAtIndex: indexPath.row];
    
    UILabel *lblRank = [cell viewWithTag: kDSRLEADERBOARD_TAGS_Rank];
    lblRank.text = [cellData objectForKey: @"Rank"];
    
    
    UILabel *lblPerformanceArrow = [cell viewWithTag: kDSRLEADERBOARD_TAGS_Daily];
    if ([[cellData objectForKey: @"PerformanceFlag"] isEqualToString: @"1"])
    {
        lblPerformanceArrow.text = @"▲";
        lblPerformanceArrow.textColor = COLOUR_DARKGREEN;
    }
    else
    {
        lblPerformanceArrow.text = @"▼";
        lblPerformanceArrow.textColor = COLOUR_RED;
    }
    
    
    UILabel *lblMechanic = [cell viewWithTag: kDSRLEADERBOARD_TAGS_Mechanic];
    lblMechanic.text = [cellData objectForKey: @"Name"];
    
    UILabel *lblNumber = [cell viewWithTag: kDSRLEADERBOARD_TAGS_Number];
    lblNumber.text = [cellData objectForKey: @"TotalValue"];
    
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell;
    if ([[self.sectionOpenControlArray objectAtIndex: section] boolValue])
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"RUDSRFullHeaderCell"];
        UIImageView *imgPlus = [cell viewWithTag: kDSRLEADERBOARD_TAGS_Plus];
        imgPlus.transform = CGAffineTransformRotate(CGAffineTransformIdentity, M_PI_4);
        
        UILabel *lblRankHeader = [cell viewWithTag: kDSRLEADERBOARD_TAGS_Rank];
        lblRankHeader.text = LOCALIZATION(C_RU_LEADERBOARD_RANK);   //lokalised 28 Jan
        
        UILabel *lblDailyHeader = [cell viewWithTag: kDSRLEADERBOARD_TAGS_Daily];
        lblDailyHeader.text = LOCALIZATION(C_RU_LEADERBOARD_DAILY); //lokalised 28 Jan
        
        UILabel *lblMechanicHeader = [cell viewWithTag: kDSRLEADERBOARD_TAGS_Mechanic];
        lblMechanicHeader.text = LOCALIZATION(C_RU_STAFFLEADERBOARD_MECHANIC);  //lokalised 28 Jan
        
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"RUDSRHeaderCell"];
    }
    UILabel *lblSectionHeader = [cell viewWithTag: kDSRLEADERBOARD_TAGS_Header];
    //    lblSectionHeader.text = [sectionData objectForKey: @"SectionName"];
    
    lblSectionHeader.text = [_tableHeaders objectAtIndex:section];
    lblSectionHeader.font = FONT_H1;
    
    UIButton *btnHeader = [cell viewWithTag: kDSRLEADERBOARD_TAGS_Button
                           ]; //10000 set in storyboard
    btnHeader.tag = section;
    
    return cell.contentView;
}

- (IBAction)headerPressed:(UIButton *)sender
{
    BOOL newControl = ![[self.sectionOpenControlArray objectAtIndex: sender.tag] boolValue];
    [self.sectionOpenControlArray replaceObjectAtIndex: sender.tag withObject: @(newControl)];
    [self.leaderBoardTableView reloadData];
}


@end
