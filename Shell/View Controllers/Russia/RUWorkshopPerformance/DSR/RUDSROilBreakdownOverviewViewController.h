//
//  RUDSROilBreakdownOverviewViewController.h
//  Shell
//
//  Created by Nach on 18/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface RUDSROilBreakdownOverviewViewController : BaseVC

-(void) reloadBreakdownData:(nullable NSMutableDictionary *) responseDict;

@end

NS_ASSUME_NONNULL_END
