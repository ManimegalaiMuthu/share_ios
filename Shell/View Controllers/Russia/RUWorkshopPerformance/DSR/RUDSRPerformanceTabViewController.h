//
//  RUDSRPerformanceTabViewController.h
//  Shell
//
//  Created by Nach on 16/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDVTabBarController.h"
#import "Helper.h"
#import "RUDSRPartnersWorkshopPerformanceViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RUDSRPerformanceTabViewController : RDVTabBarController
@property NSString *pushNavKey;
@end

NS_ASSUME_NONNULL_END
