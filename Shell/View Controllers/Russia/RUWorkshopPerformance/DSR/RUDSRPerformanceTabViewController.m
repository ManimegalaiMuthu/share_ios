//
//  RUDSRPerformanceTabViewController.m
//  Shell
//
//  Created by Nach on 16/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUDSRPerformanceTabViewController.h"
#import "RDVTabBarItem.h"
#import "INPerformanceDashbordViewController.h"
#import "LocalizationManager.h"
#import "RUDSRPartnersWorkshopPerformanceViewController.h"
#import "RUPartnersPointsEarned_V2ViewController.h"

@implementation RUDSRPerformanceTabViewController

-(void) viewDidLoad {
    [super viewDidLoad];
    
    [self setupInterface];
}

-(void) setupInterface {
//    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_RU_PERFORMANCE_DASHBOARD_PARTNER) subtitle:@"" size: 15 subtitleSize: 0];   //Lokalised 28/1
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_RU_PERFORMANCE_DASHBOARD_PARTNER) subtitle:@"" size: 15 subtitleSize: 0];
    
    NSMutableArray *title = [[NSMutableArray alloc] init];
    NSMutableArray *navControllers = [[NSMutableArray alloc] init];
    
    INPerformanceDashbordViewController *performanceVc  = [STORYBOARD_IN_PERFORMANCE instantiateViewControllerWithIdentifier: VIEW_IN_PERFORMANCE_DASHBOARD];
   // performanceVc.pushNavKey = self.pushNavKey;
    UINavigationController *performanceNav = [[UINavigationController alloc] initWithRootViewController: performanceVc];
    [performanceNav setNavigationBarHidden: YES];
    
    [title addObject:LOCALIZATION(C_TITLE_WORKSHOPPERFORMANCE)];
    [navControllers addObject:performanceNav];
    
   
    if([[mSession profileInfo] hasWorkshopPerformanceView]) {
        UIViewController *leaderboardVc  = [STORYBOARD_RU_DSR_WORKSHOPPERFORMANCE instantiateViewControllerWithIdentifier: VIEW_RU_DSRPERFORMANCE_LEADERBOARD];
        UINavigationController *leaderboardNav = [[UINavigationController alloc] initWithRootViewController: leaderboardVc];
        [leaderboardNav setNavigationBarHidden: YES];
        
        [title addObject:LOCALIZATION(C_PERFORMANCE_LEADERBOARD)];
        [navControllers addObject:leaderboardNav];
    }
   
    
    if([[mSession profileInfo] hasMYPoints]) {
        RUPartnersPointsEarned_V2ViewController *pointsVc = [STORYBOARD_RUSSIA_V2 instantiateViewControllerWithIdentifier: VIEW_PARTNERSOFSHELL_POINTSEARNED_V2];
        UINavigationController *pointsNav = [[UINavigationController alloc] initWithRootViewController: pointsVc];
        [pointsNav setNavigationBarHidden: YES];
        
        [title addObject:LOCALIZATION(C_INVENTORY_POINTHISTORY_TITLE)];
        [navControllers addObject:pointsNav];
    }
    
    [self setViewControllers: navControllers];
    
    UIImage *selected = GET_ISADVANCE ? [UIImage imageNamed:@"tab-bg_blue"] : [UIImage imageNamed:@"tab-bg"];
    UIImage *unselected = [UIImage imageNamed:@"tab-bg_grey.png"];
    
    if (kIsRightToLeft) {
        title = title.reverseObjectEnumerator.allObjects.mutableCopy;
    }
    for (int i = 0; i < [self.tabBar.items count]; i++)
    {
        RDVTabBarItem *item = [self.tabBar.items objectAtIndex: i];
        [item setTitle: [title objectAtIndex: i]];
        
        [item setBackgroundColor: COLOUR_WHITE];
        [item setTitleUnselectedAttributes:FONT_H1 colour:COLOUR_VERYDARKGREY];
        [item setTitleSelectedAttributes:FONT_H1 colour:COLOUR_RED];
        
        [item setBackgroundSelectedImage: selected withUnselectedImage: unselected];
    }
    if (kIsRightToLeft) {
        [self tabBar:self.tabBar didSelectItemAtIndex:self.viewControllers.count-1];
    }
}

#pragma mark RDVTabBarDelegate
- (BOOL)tabBar:(RDVTabBar *)tabBar shouldSelectItemAtIndex:(NSInteger)index
{
    return ([tabBar selectedItem] != [[tabBar items] objectAtIndex: index]);
}

- (void)tabBar:(RDVTabBar *)tabBar didSelectItemAtIndex:(NSInteger)index
{
    //super method MUST BE PRESENT
    [super tabBar:tabBar didSelectItemAtIndex: index];
}

@end

