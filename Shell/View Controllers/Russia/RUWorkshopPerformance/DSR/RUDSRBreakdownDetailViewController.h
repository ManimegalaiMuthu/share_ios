//
//  RUDSRBreakdownDetailViewController.h
//  Shell
//
//  Created by Nach on 16/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface RUDSRBreakdownDetailViewController : BaseVC

-(void) reloadBreakdownData:(nullable NSMutableDictionary *) responseDict;

@end

NS_ASSUME_NONNULL_END
