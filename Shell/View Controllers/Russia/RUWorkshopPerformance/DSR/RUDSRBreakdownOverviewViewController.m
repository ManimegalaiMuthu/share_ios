//
//  RUDSRBreakdownOverviewViewController.m
//  Shell
//
//  Created by Nach on 16/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUDSRBreakdownOverviewViewController.h"

#define START_ANGLE -90 //12 o clock
#define degreesToRadians(degrees)((M_PI * degrees)/180)

@interface RUDSRBreakdownOverviewViewController ()

@property (weak, nonatomic) IBOutlet UITableView *overViewTableView;

@property NSMutableDictionary *breakdownDictionary;
@property NSArray *workshopDetails;

@end

@implementation RUDSRBreakdownOverviewViewController

enum
{
    PRODUCT_TIER = 1,
    PRODUCT_CATEGORY,
    PRODUCT_WORKSHOPS_HEADER,
    PRODUCT_WORKSHOPS,
} PRODUCT_TYPES;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = COLOUR_VERYPALEGREY;
    self.overViewTableView.backgroundColor = COLOUR_VERYPALEGREY;
    self.overViewTableView.backgroundView = nil;
    
}

-(void) constructCircle:(UIView *)view breakdownArray:(NSMutableArray *)breakdownArray chartIndex:(NSInteger)chartIndex
{
    //breakdownArray = tier or category. pick out the percent
    CGFloat lastAngleInRadians = degreesToRadians(START_ANGLE);
    
    int i = 0;
    for (NSDictionary *dict in breakdownArray)
    {
        CGFloat percentage = [[dict objectForKey: @"TotalPercent"] floatValue];
        CGFloat nextAngleInRadians = lastAngleInRadians + (degreesToRadians(360  * percentage * 0.01));
        
        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
        [bezierPath addArcWithCenter: CGPointMake(view.frame.size.width / 2, view.frame.size.width / 2)
                              radius: (view.frame.size.width / 2) + 15
                          startAngle: lastAngleInRadians
                            endAngle: nextAngleInRadians
                           clockwise:YES];
        
        CAShapeLayer *progressLayer = [[CAShapeLayer alloc] init];
        [progressLayer setPath:bezierPath.CGPath];
        
        UIColor *color = [Helper colorFromHex: [dict objectForKey: @"ColorCode"]];
        [progressLayer setStrokeColor: [color CGColor]];
        
        [progressLayer setFillColor:[UIColor clearColor].CGColor];
        [progressLayer setLineWidth: 30];
        [view.layer addSublayer:progressLayer];
        
        //get ready for the next arc draw
        lastAngleInRadians = nextAngleInRadians;
        i++;
    }
}



-(void)reloadBreakdownData:(nullable NSMutableDictionary *)responseDict
{
    if(responseDict) {
        self.breakdownDictionary = responseDict;
        self.overViewTableView.hidden = NO;
        if(_isOrderBreakdown) {
            self.workshopDetails = [responseDict objectForKey:@"WorkshopOrderDetails"];
        }
        [self.overViewTableView reloadData];
    } else {
        self.overViewTableView.hidden = YES;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //    return [self.breakdownDataArray count] + 1; //+ 1 for header
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0 || section == PRODUCT_WORKSHOPS || section == PRODUCT_WORKSHOPS_HEADER)
        return 0;
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
   if(section == PRODUCT_WORKSHOPS_HEADER)
       return 0;
    else
        return 15;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == PRODUCT_WORKSHOPS) {
        return _workshopDetails.count + 2; //+1 for header, + 1 for footer
    }
    else if (section == PRODUCT_TIER || section == PRODUCT_CATEGORY)
    {
        //        return 1; //return array of array count
        NSMutableArray *sectionArray;
        if (section == PRODUCT_TIER) //product tier
        {
            sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductTier"];
        }
        else if (section == PRODUCT_CATEGORY) //product category
        {
            sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductCategory"];
            
        }
        else
        {
            sectionArray = [self.breakdownDictionary objectForKey: @"WorkshopOrderDetails"];
        }
        return [sectionArray count];
    }
    return 1;
}

enum
{
    BREAKDOWNHEADERCELL_ORDERVOLUME = 1,
    BREAKDOWNHEADERCELL_ORDERVOLUMEDATA,
    BREAKDOWNHEADERCELL_SUBHEADER,
} BREAKDOWNHEADERCELL_TAG;

enum
{
    BREAKDOWNCELL_COLOURTAG = 1,
    BREAKDOWNCELL_PRODUCT,
    BREAKDOWNCELL_TOTALUNIT,
    BREAKDOWNCELL_TOTALPERCENT,
    BREAKDOWNCELL_CIRCLEVIEW,   //view to hold circle
    BREAKDOWNCELL_BYPRODUCT, //center of circle
} BREAKDOWNCELL_TAG;

enum
{
    WORKSHOP_BREAKDOWN_CELL_NAME = 1,
    WORKSHOP_BREAKDOWN_CELL_MONTH1,
    WORKSHOP_BREAKDOWN_CELL_MONTH2,
    WORKSHOP_BREAKDOWN_CELL_MONTH3,
    WORKSHOP_BREAKDOWN_CELL_TOTAL,
    WORKSHOP_BREAKDOWN_CELL_LINE,
} WORKSHOP_BREAKDOWN_CELL_TAG;


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
   if (section == 0 || section == PRODUCT_WORKSHOPS || section == PRODUCT_WORKSHOPS_HEADER)
        return [UIView new];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"RUDSRBreakdownHeaderCell"];
    
    UIView *circleView = [cell viewWithTag: BREAKDOWNCELL_CIRCLEVIEW];
    
    
    UILabel *lblProductHeader = [cell viewWithTag:  BREAKDOWNCELL_PRODUCT];
    UILabel *lblByProduct = [cell viewWithTag:  BREAKDOWNCELL_BYPRODUCT];
    if (section == PRODUCT_TIER)
    {
        lblProductHeader.text = LOCALIZATION(C_RU_BREAKDOWN_PRODUCTTIER);   //lokalised 28 Jan
        lblByProduct.text = LOCALIZATION(C_RU_BREAKDOWN_BYPRODUCTTIER);     //lokalised 28 Jan
    }
    else if (section == PRODUCT_CATEGORY)
    {
        //change to category instead for section 2
        lblProductHeader.text = LOCALIZATION(C_RU_BREAKDOWN_PRODUCTCATEGORY);   //lokalised 28 Jan
        lblByProduct.text = LOCALIZATION(C_RU_BREAKDOWN_BYPRODUCTCATEGORY);     //lokalised 28 Jan
    }
    
    UILabel *lblTotalUnitHeader= [cell viewWithTag:  BREAKDOWNCELL_TOTALUNIT];
    lblTotalUnitHeader.text = LOCALIZATION(C_RU_BREAKDOWN_TOTALLITRE);      //lokalised 28 Jan
    
    UILabel *lblTotalPercentHeader = [cell viewWithTag:  BREAKDOWNCELL_TOTALPERCENT];
    lblTotalPercentHeader.text = LOCALIZATION(C_RU_BREAKDOWN_TOTALPERCENT); //lokalised 28 Jan
    
    cell.contentView.backgroundColor = COLOUR_WHITE;
    
    
    //breakdownArray = tier or category. pick out the percent
    NSMutableArray *sectionArray;
    if (section == PRODUCT_TIER) //product tier
        sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductTier"];
    else if (section == PRODUCT_CATEGORY) //product category
        sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductCategory"];
    
    [self constructCircle: [cell viewWithTag: BREAKDOWNCELL_CIRCLEVIEW] breakdownArray:sectionArray chartIndex: section];
    return cell.contentView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch (indexPath.section) {
        case 0:
        {
            //header
            cell = [tableView dequeueReusableCellWithIdentifier: @"RUDSRMainHeaderCell"];
            cell.backgroundColor = [UIColor clearColor];
            
            UIView *bgView = [cell viewWithTag:6900];
            
//            if(_isOrderBreakdown) {
                bgView.backgroundColor = COLOUR_YELLOW;
//            } else {
//                bgView.backgroundColor = COLOUR_YELLOW;
//            }
            
            if (!self.breakdownDictionary)
                break;
            
            UILabel *lblOrderVolume = [cell viewWithTag: BREAKDOWNHEADERCELL_ORDERVOLUME];
            UILabel *lblOrderVolumeData = [cell viewWithTag: BREAKDOWNHEADERCELL_ORDERVOLUMEDATA];
            
            if(_isOrderBreakdown) {
                lblOrderVolumeData.text = [self.breakdownDictionary objectForKey: @"OrderVolume"];
                lblOrderVolume.text = LOCALIZATION(C_RU_LEADERBOARD_ORDERVOLUME);   //lokalised 28 Jan
            } else {
                lblOrderVolumeData.text = [self.breakdownDictionary objectForKey: @"TotalOilChanges"];
                lblOrderVolume.text =  LOCALIZATION(C_RU_BREAKDOWN_OILCHANGEBYID);  //lokalised 28 Jan
            }
            
            UILabel *lblSubheader = [cell viewWithTag: BREAKDOWNHEADERCELL_SUBHEADER];
            lblSubheader.text = [NSString stringWithFormat: @"%@ %@", [self.breakdownDictionary objectForKey: @"UpdatedOnText"], [self.breakdownDictionary objectForKey: @"UpdatedOn"] ];
            lblSubheader.backgroundColor = [UIColor clearColor];
            break;
        }
        case PRODUCT_WORKSHOPS_HEADER:
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"RUDSRWorkshopHeader"];
            UILabel *header = [cell viewWithTag:1001];
            header.font = FONT_H1;
            header.textColor = COLOUR_VERYDARKGREY;
            header.text = LOCALIZATION(C_RU_BREAKDOWN_BYWORKSHOP);  //lokalised 28 Jan
            break;
        }
        case PRODUCT_WORKSHOPS:
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"RUDSRByWorkshopPerformanceCell"];
            UILabel *name = [cell viewWithTag:WORKSHOP_BREAKDOWN_CELL_NAME];
            UILabel *month1 = [cell viewWithTag:WORKSHOP_BREAKDOWN_CELL_MONTH1];
            UILabel *month2 = [cell viewWithTag:WORKSHOP_BREAKDOWN_CELL_MONTH2];
            UILabel *month3 = [cell viewWithTag:WORKSHOP_BREAKDOWN_CELL_MONTH3];
            UILabel *total = [cell viewWithTag:WORKSHOP_BREAKDOWN_CELL_TOTAL];
            
            if(indexPath.row == 0) {
                NSArray *monthList = [_breakdownDictionary objectForKey:@"MonthList"];
                name.font = FONT_H1;
                name.text = LOCALIZATION(C_RU_PERFORMANCE_WORKSHOP);    //lokalised 28 Jan
                name.textColor = COLOUR_VERYDARKGREY;
                month1.text = [[monthList objectAtIndex:0] objectForKey:@"MonthName"];
                month2.text = [[monthList objectAtIndex:1] objectForKey:@"MonthName"];
                month3.text = [[monthList objectAtIndex:2] objectForKey:@"MonthName"];
                total.text = LOCALIZATION(C_RU_BREAKDOWN_TOTAL);        //lokalised 28 Jan
            } else if (indexPath.row < [self.workshopDetails count] + 1){
                name.font = FONT_B1;
                name.textColor = COLOUR_DARKGREY;
                NSDictionary *cellData = [_workshopDetails objectAtIndex:indexPath.row - 1];
                name.text = [cellData objectForKey:@"WorkshopName"];
                month1.text = [cellData objectForKey:@"MonthValue1"];
                month2.text = [cellData objectForKey:@"MonthValue2"];
                month3.text = [cellData objectForKey:@"MonthValue3"];
                total.text = [cellData objectForKey:@"Total"];
                
                UIView *view = [cell viewWithTag:6900];
                view.backgroundColor = COLOUR_VERYPALEGREY;
                
            }
            else
            {
                cell = [tableView dequeueReusableCellWithIdentifier: @"RUDSRBreakdownFooterCell"];
                
                UILabel *lblFooter = [cell viewWithTag: 1];
                lblFooter.text = LOCALIZATION(C_RU_BREAKDOWN_ORDER_FOOTER); //lokalised 28 Jan
            }
            
            month1.font = name.font;
            month2.font = name.font;
            month3.font = name.font;
            total.font = name.font;
            
            month1.textColor = name.textColor;
            month2.textColor = name.textColor;
            month3.textColor = name.textColor;
            total.textColor = name.textColor;
            
            break;
        }
        default:
        {
            NSMutableArray *sectionArray;

            if (indexPath.section == PRODUCT_TIER) //product tier
            {
                sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductTier"];
                
            }
            else if (indexPath.section == PRODUCT_CATEGORY) //product category
            {
                sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductCategory"];
            }
            
            if (indexPath.row < [sectionArray count])
            {
                cell = [tableView dequeueReusableCellWithIdentifier: @"RUDSRBreakdownCell"];
                
                cell.backgroundColor = COLOUR_WHITE;
                
                //            else
                //            {
                //                sectionArray = [self.breakdownDictionary objectForKey: @"WorkshopOrderDetails"];
                //            }
                
                NSDictionary *cellData = [sectionArray objectAtIndex: indexPath.row];
                
                
                UIView *colourTagView = [cell viewWithTag: BREAKDOWNCELL_COLOURTAG];
                colourTagView.backgroundColor = [Helper colorFromHex: [cellData objectForKey: @"ColorCode"]];
                
                UILabel *lblProductData = [cell viewWithTag:  BREAKDOWNCELL_PRODUCT];
                
                //1 = ProductBrand, 2 = ProductTier, 2 = ProductCategory
                if (indexPath.section == PRODUCT_TIER) //product tier
                {
                    lblProductData.text = [cellData objectForKey: @"ProductTier"];
                }
                else if (indexPath.section == PRODUCT_CATEGORY)  //product category
                {
                    
                    lblProductData.text = [cellData objectForKey: @"ProductCategory"];
                }
                //            else
                //            {
                //                sectionArray = [self.breakdownDictionary objectForKey: @"WorkshopOrderDetails"];
                //            }
                
                UILabel *lblTotalUnitData = [cell viewWithTag:  BREAKDOWNCELL_TOTALUNIT];
                
                lblTotalUnitData.text = [[cellData objectForKey: @"Total"] stringValue];
                
                UILabel *lblTotalPercentData = [cell viewWithTag:  BREAKDOWNCELL_TOTALPERCENT];
                lblTotalPercentData.text = [[cellData objectForKey: @"TotalPercent"] stringValue];
            }
            else
            {
                cell = [tableView dequeueReusableCellWithIdentifier: @"RUDSRBreakdownFooterCell"];
                
                UILabel *lblFooter = [cell viewWithTag: 1];
                lblFooter.text = LOCALIZATION(C_RU_BREAKDOWN_OILCHANGE_FOOTER); //lokalised 28 Jan
            }
        }
            break;
    }

    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
