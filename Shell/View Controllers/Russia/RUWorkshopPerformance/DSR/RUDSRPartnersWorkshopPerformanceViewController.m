//
//  RUDSRPartnersWorkshopPerformanceViewController.m
//  Shell
//
//  Created by Jeremy Lua on 4/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUDSRPartnersWorkshopPerformanceViewController.h"

@interface RUDSRPartnersWorkshopPerformanceViewController ()<WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lblFooter;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;

@property NSArray *tableData;

@end

@implementation RUDSRPartnersWorkshopPerformanceViewController

enum
{
    DSRPERFORMANCETABLE_CONTENT_IMAGE = 1,
    DSRPERFORMANCETABLE_CONTENT_BODY,
    DSRPERFORMANCETABLE_CONTENT_DATA,
    DSRPERFORMANCETABLE_CONTENT_LINE,
    DSRPERFORMANCETABLE_CONTENT_ARROW,
} DSRPERFORMANCETABLE_CONTENT_TAG;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupInterface];
}

-(void) setupInterface {
//    [[WebServiceManager sharedInstance]fetchPerformanceV2:self];
    [[WebServiceManager sharedInstance]fetchPerformanceV3:self];
    
    self.tableView.tableFooterView = [UIView new];
    self.lblFooter.font = FONT_B1;
    self.lblFooter.textColor = COLOUR_VERYDARKGREY;
    
    self.lblHeader.text = LOCALIZATION(C_RU_PERFORMANCE_DASHBOARD_HEADER);
    self.lblHeader.font = FONT_B1;
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_RU_PERFORMANCE_DASHBOARD_PARTNER),LOCALIZATION_EN(C_RU_WORKSHOPPERFORMANCE)] screenClass:nil];
}

-(void) processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_MSTRADE_FETCHPERFORMANCE_V3:
        case kWEBSERVICE_FETCH_PERFORMANCE_V2:
        {
            self.tableData = [[response getGenericResponse]objectForKey:@"Performance"];
            NSString *footerText = [[response getGenericResponse] objectForKey:@"UpdatedOnText"];
            self.lblFooter.text = [footerText stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
            [_tableView reloadData];
            
            if ([self.pushNavKey isEqualToString: @"Order BreakDown"])
            {
                //parent is tab controller
                [mSession pushDSRBreakdown:YES vc:self.parentViewController.parentViewController];
            }
            else if ([self.pushNavKey isEqualToString: @"Oil Change BreakDown"])
            {
                [mSession pushDSRBreakdown:NO vc:self.parentViewController.parentViewController];
            }
            else if ([self.pushNavKey isEqualToString: @"Customer Break Down"])
            {
                [mSession pushDSRCustomerBreakdown:self.parentViewController.parentViewController];
            }
        }
            break;
        default:
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *cellData = [self.tableData objectAtIndex: indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RUDSRPerformanceCell"];
    
    UIImageView *imgView = [cell viewWithTag: DSRPERFORMANCETABLE_CONTENT_IMAGE];
    
    if ([[cellData objectForKey: @"IconName"] isEqualToString: @"ApprovedWorkshop"])
        imgView.image = [UIImage imageNamed: @"icn_totalworkshops_verydarkgrey"];
    else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"PendingWorkshop"])
        imgView.image = [UIImage imageNamed: @"icn_pendingworkshops_verydarkgrey"];
    else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Order BreakDown"])
        imgView.image = [UIImage imageNamed: @"icn_orders_verydarkgrey"];
    else  if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Staff BreakDown"])
        imgView.image = [UIImage imageNamed: @"icn_staff_verydarkgrey"];
    else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Oil Change BreakDown"])
        imgView.image = [UIImage imageNamed: @"icn_oilchange_verydarkgrey"];
    else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Customer Break Down"])
        imgView.image = [UIImage imageNamed: @"icn_consumers_verydarkgrey"];
    else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Offer Redemption BreakDown"])
        imgView.image = [UIImage imageNamed: @"icn_redemptions_verydarkgrey"];
   
    else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Product Sold Breakdown"])
                      imgView.image = [UIImage imageNamed: @"icn_inventory_verydarkgrey"];
    
    else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"ApprovedWorkshop"])
                         imgView.image = [UIImage imageNamed: @"icn_totalworkshops_verydarkgrey"];
    

    
    UILabel *lblBody = [cell viewWithTag: DSRPERFORMANCETABLE_CONTENT_BODY];
    lblBody.text = [cellData objectForKey: @"PerformanceKey"];
    lblBody.font = FONT_B1;
    
    UILabel *lblData = [cell viewWithTag: DSRPERFORMANCETABLE_CONTENT_DATA];
    lblData.text = [cellData objectForKey: @"PerformanceValue"];
    
    UIImageView *arrowImgView = [cell viewWithTag: DSRPERFORMANCETABLE_CONTENT_ARROW];
    arrowImgView.hidden = ![[cellData objectForKey: @"isClickable"] boolValue];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *cellData = [_tableData objectAtIndex:indexPath.row];
    
    if (![[cellData objectForKey: @"isClickable"] boolValue])
        return;
    
    NSString *navKey = [cellData objectForKey: @"OnClickRedirectPath"];
    
    if ([navKey isEqualToString: @"Order BreakDown"])
    {
        //parent is tab controller
        [mSession pushDSRBreakdown:YES vc:self.parentViewController.parentViewController];
    }
    else if ([navKey isEqualToString: @"Oil Change BreakDown"])
    {
       [mSession pushDSRBreakdown:NO vc:self.parentViewController.parentViewController];
    }
    else if ([navKey isEqualToString: @"Customer Break Down"])
    {
        [mSession pushDSRCustomerBreakdown:self.parentViewController.parentViewController];
    }
    else if ([navKey isEqualToString: @"Offer Redemption BreakDown"])
    {
        [mSession pushOfferRedemptionBreakdown:self.parentViewController.parentViewController];
    }
    else if([navKey isEqualToString:@"Points Earned BreakDown"]) {
        [mSession pushPointsBreakdown: self.parentViewController.parentViewController];
    } else if([navKey isEqualToString:@"ApprovedWorkshop"]) {
        [mSession loadApprovedWorkshop];
    } else if([navKey isEqualToString:@"PendingWorkshop"]) {
        [mSession loadManageWorkshopView];
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
