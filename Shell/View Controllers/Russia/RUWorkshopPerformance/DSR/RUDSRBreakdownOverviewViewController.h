//
//  RUDSRBreakdownOverviewViewController.h
//  Shell
//
//  Created by Nach on 16/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface RUDSRBreakdownOverviewViewController : BaseVC

-(void) reloadBreakdownData:(nullable NSMutableDictionary *) responseDict;
@property BOOL isOrderBreakdown;

@end

NS_ASSUME_NONNULL_END
