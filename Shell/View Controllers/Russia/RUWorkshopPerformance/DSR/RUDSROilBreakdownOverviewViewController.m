//
//  RUDSROilBreakdownOverviewViewController.m
//  Shell
//
//  Created by Nach on 18/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUDSROilBreakdownOverviewViewController.h"

#define START_ANGLE -90 //12 o clock
#define degreesToRadians(degrees)((M_PI * degrees)/180)

@interface RUDSROilBreakdownOverviewViewController ()

@property (weak, nonatomic) IBOutlet UITableView *overViewTable;
@property NSMutableDictionary *breakdownDictionary;
@property NSArray *workshopDetails;

@end

@implementation RUDSROilBreakdownOverviewViewController

enum
{
    PRODUCT_BRAND = 1,
    PRODUCT_TIER,
    PRODUCT_CATEGORY,
    PRODUCT_WORKSHOPS_HEADER = 4,
    PRODUCT_WORKSHOPS,
} PRODUCT_DSR_OIL_TYPES;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = COLOUR_VERYPALEGREY;
    self.overViewTable.backgroundColor = COLOUR_VERYPALEGREY;
    self.overViewTable.backgroundView = nil;
    
}

-(void) constructCircle:(UIView *)view breakdownArray:(NSMutableArray *)breakdownArray chartIndex:(NSInteger)chartIndex
{
    //breakdownArray = tier or category. pick out the percent
    CGFloat lastAngleInRadians = degreesToRadians(START_ANGLE);
    
    int i = 0;
    for (NSDictionary *dict in breakdownArray)
    {
        CGFloat percentage = [[dict objectForKey: @"TotalPercent"] floatValue];
        CGFloat nextAngleInRadians = lastAngleInRadians + (degreesToRadians(360  * percentage * 0.01));
        
        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
        [bezierPath addArcWithCenter: CGPointMake(view.frame.size.width / 2, view.frame.size.width / 2)
                              radius: (view.frame.size.width / 2) + 15
                          startAngle: lastAngleInRadians
                            endAngle: nextAngleInRadians
                           clockwise:YES];
        
        CAShapeLayer *progressLayer = [[CAShapeLayer alloc] init];
        [progressLayer setPath:bezierPath.CGPath];
        
        UIColor *color = [Helper colorFromHex: [dict objectForKey: @"ColorCode"]];
        [progressLayer setStrokeColor: [color CGColor]];
        
        [progressLayer setFillColor:[UIColor clearColor].CGColor];
        [progressLayer setLineWidth: 30];
        [view.layer addSublayer:progressLayer];
        
        //get ready for the next arc draw
        lastAngleInRadians = nextAngleInRadians;
        i++;
    }
}



-(void)reloadBreakdownData:(nullable NSMutableDictionary *)responseDict
{
    if(responseDict) {
        self.overViewTable.hidden = NO;
        self.breakdownDictionary = responseDict;
        self.workshopDetails = [responseDict objectForKey:@"WorkshopDetails"];
        [self.overViewTable reloadData];
    } else {
        self.overViewTable.hidden = YES;
    }
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //    return [self.breakdownDataArray count] + 1; //+ 1 for header
    return 6;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0 || section == PRODUCT_WORKSHOPS || section == PRODUCT_WORKSHOPS_HEADER)
        return 0;
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section == PRODUCT_WORKSHOPS_HEADER)
        return 0;
    else
        return 15;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == PRODUCT_WORKSHOPS) {
        return _workshopDetails.count + 1;
    }
    else if (section == PRODUCT_TIER || section == PRODUCT_CATEGORY || section == PRODUCT_BRAND)
    {
        //        return 1; //return array of array count
        NSMutableArray *sectionArray;
        if (section == PRODUCT_BRAND) //product brand
        {
            sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductBrand"];
        }
        if (section == PRODUCT_TIER) //product tier
        {
            sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductTier"];
        }
        else if (section == PRODUCT_CATEGORY) //product category
        {
            sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductCategory"];
            
        }
        return [sectionArray count];
    }
    return 1;
}

enum
{
    BREAKDOWNHEADERCELL_ORDERVOLUME = 1,
    BREAKDOWNHEADERCELL_ORDERVOLUMEDATA,
    BREAKDOWNHEADERCELL_SUBHEADER,
} BREAKDOWNHEADERCELL_OIL_TAG;

enum
{
    BREAKDOWNCELL_COLOURTAG = 1,
    BREAKDOWNCELL_PRODUCT,
    BREAKDOWNCELL_TOTALUNIT,
    BREAKDOWNCELL_TOTALPERCENT,
    BREAKDOWNCELL_CIRCLEVIEW,   //view to hold circle
    BREAKDOWNCELL_BYPRODUCT, //center of circle
} BREAKDOWNCELL_OIL_TAG;

enum
{
    WORKSHOP_BREAKDOWN_CELL_NAME = 1,
    WORKSHOP_BREAKDOWN_CELL_MONTH1,
    WORKSHOP_BREAKDOWN_CELL_MONTH2,
    WORKSHOP_BREAKDOWN_CELL_MONTH3,
    WORKSHOP_BREAKDOWN_CELL_TOTAL,
    WORKSHOP_BREAKDOWN_CELL_LINE,
} WORKSHOP_BREAKDOWN_OIL_CELL_TAG;


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0 || section == PRODUCT_WORKSHOPS || section == PRODUCT_WORKSHOPS_HEADER)
        return [UIView new];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"RUDSROilBreakdownHeaderCell"];
    
    UIView *circleView = [cell viewWithTag: BREAKDOWNCELL_CIRCLEVIEW];
    
    
    UILabel *lblProductHeader = [cell viewWithTag:  BREAKDOWNCELL_PRODUCT];
    UILabel *lblByProduct = [cell viewWithTag:  BREAKDOWNCELL_BYPRODUCT];
    if (section == PRODUCT_BRAND)
    {
        lblProductHeader.text = LOCALIZATION(C_RU_BREAKDOWN_PRODUCTBRAND);  //lokalised 28 Jan
        lblByProduct.text = LOCALIZATION(C_RU_BREAKDOWN_BYPRODUCTBRAND);    //lokalised 28 Jan
    }
    if (section == PRODUCT_TIER)
    {
        lblProductHeader.text = LOCALIZATION(C_RU_BREAKDOWN_PRODUCTTIER);   //lokalised 28 Jan
        lblByProduct.text = LOCALIZATION(C_RU_BREAKDOWN_BYPRODUCTTIER);     //lokalised 28 Jan
    }
    else if (section == PRODUCT_CATEGORY)
    {
        //change to category instead for section 2
        lblProductHeader.text = LOCALIZATION(C_RU_BREAKDOWN_PRODUCTCATEGORY);   //lokalised 28 Jan
        lblByProduct.text = LOCALIZATION(C_RU_BREAKDOWN_BYPRODUCTCATEGORY);     //lokalised 28 Jan
    }
    
    UILabel *lblTotalUnitHeader= [cell viewWithTag:  BREAKDOWNCELL_TOTALUNIT];
    lblTotalUnitHeader.text = LOCALIZATION(C_RU_BREAKDOWN_TOTALLITRE);      //lokalised 28 Jan
    
    UILabel *lblTotalPercentHeader = [cell viewWithTag:  BREAKDOWNCELL_TOTALPERCENT];
    lblTotalPercentHeader.text = LOCALIZATION(C_RU_BREAKDOWN_TOTALPERCENT); //lokalised 28 Jan
    
    cell.contentView.backgroundColor = COLOUR_WHITE;
    
    
    //breakdownArray = tier or category. pick out the percent
    NSMutableArray *sectionArray;
    if (section == PRODUCT_BRAND) //product brand
        sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductBrand"];
    if (section == PRODUCT_TIER) //product tier
        sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductTier"];
    else if (section == PRODUCT_CATEGORY) //product category
        sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductCategory"];
    
    [self constructCircle: [cell viewWithTag: BREAKDOWNCELL_CIRCLEVIEW] breakdownArray:sectionArray chartIndex: section];
    return cell.contentView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch (indexPath.section) {
        case 0:
        {
            //header
            cell = [tableView dequeueReusableCellWithIdentifier: @"RUDSROilMainHeaderCell"];
            cell.backgroundColor = [UIColor clearColor];
            UIView *bgView = [cell viewWithTag:6900];
            
            
            bgView.backgroundColor = COLOUR_YELLOW;
            
            
            if (!self.breakdownDictionary)
                break;
            
            UILabel *lblOrderVolume = [cell viewWithTag: BREAKDOWNHEADERCELL_ORDERVOLUME];
            UILabel *lblOrderVolumeData = [cell viewWithTag: BREAKDOWNHEADERCELL_ORDERVOLUMEDATA];
            
            
            lblOrderVolumeData.text = [self.breakdownDictionary objectForKey: @"TotalOilChanges"];
            lblOrderVolume.text =  LOCALIZATION(C_RU_BREAKDOWN_OILCHANGEBYID);  //lokalised 28 Jan
            
            
            UILabel *lblSubheader = [cell viewWithTag: BREAKDOWNHEADERCELL_SUBHEADER];
            lblSubheader.text = [NSString stringWithFormat: @"%@ %@", [self.breakdownDictionary objectForKey: @"UpdatedOnText"], [self.breakdownDictionary objectForKey: @"UpdatedOn"] ];
            lblSubheader.backgroundColor = [UIColor clearColor];
            break;
        }
        case PRODUCT_WORKSHOPS_HEADER:
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"RUDSROilWorkshopHeader"];
            UILabel *header = [cell viewWithTag:1001];
            header.font = FONT_H1;
            header.textColor = COLOUR_VERYDARKGREY;
            header.text = LOCALIZATION(C_RU_BREAKDOWN_BYWORKSHOP);  //lokalised 28 Jan
    
        }
            break;
        case PRODUCT_WORKSHOPS:
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"RUDSROilByWorkshopPerformanceCell"];
            UILabel *name = [cell viewWithTag:WORKSHOP_BREAKDOWN_CELL_NAME];
            UILabel *month1 = [cell viewWithTag:WORKSHOP_BREAKDOWN_CELL_MONTH1];
            UILabel *month2 = [cell viewWithTag:WORKSHOP_BREAKDOWN_CELL_MONTH2];
            UILabel *month3 = [cell viewWithTag:WORKSHOP_BREAKDOWN_CELL_MONTH3];
            UILabel *total = [cell viewWithTag:WORKSHOP_BREAKDOWN_CELL_TOTAL];
            
            NSArray *monthListArray = [self.breakdownDictionary objectForKey: @"MonthList"];
            month1.hidden = NO;
            month2.hidden = NO;
            month3.hidden = NO;
            
            if ([monthListArray count] < 3)
                month3.hidden = YES;
            
            if ([monthListArray count] < 2)
                month2.hidden = YES;
            
            if ([monthListArray count] < 1)
                month1.hidden = YES;
            
            if(indexPath.row == 0) {
                name.font = FONT_H1;
                name.text = LOCALIZATION(C_RU_PERFORMANCE_WORKSHOP);    //lokalised 28 Jan
                name.textColor = COLOUR_VERYDARKGREY;
                if (!month3.hidden)
                    month3.text = [[monthListArray objectAtIndex:2] objectForKey:@"MonthName"];
                if (!month2.hidden)
                    month2.text = [[monthListArray objectAtIndex:1] objectForKey:@"MonthName"];
                if (!month1.hidden)
                    month1.text = [[monthListArray objectAtIndex:0] objectForKey:@"MonthName"];
                total.text = LOCALIZATION(C_RU_BREAKDOWN_TOTAL);    //lokalised 28 Jan
            } else {
                name.font = FONT_B1;
                name.textColor = COLOUR_DARKGREY;
                NSDictionary *cellData = [_workshopDetails objectAtIndex:indexPath.row - 1];
                name.text = [cellData objectForKey:@"WorkshopName"];
                month1.text = [cellData objectForKey:@"MonthValue1"];
                if ([[cellData objectForKey: @"MonthValue2"] isKindOfClass: [NSNull class]])
                    month2.hidden = YES;
                else
                    month2.text = [cellData objectForKey:@"MonthValue2"];
                
                if ([[cellData objectForKey: @"MonthValue3"] isKindOfClass: [NSNull class]])
                    month3.hidden = YES;
                else
                    month3.text = [cellData objectForKey:@"MonthValue3"];
                total.text = [cellData objectForKey:@"Total"];
                
                UIView *view = [cell viewWithTag:6900];
                view.backgroundColor = COLOUR_VERYPALEGREY;
                
            }
            
            month1.font = name.font;
            month2.font = name.font;
            month3.font = name.font;
            total.font = name.font;
            
            month1.textColor = name.textColor;
            month2.textColor = name.textColor;
            month3.textColor = name.textColor;
            total.textColor = name.textColor;
            
            break;
        }
        default:
        {
            NSMutableArray *sectionArray;
            if (indexPath.section == PRODUCT_BRAND) //product brand
            {
                sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductBrand"];
                
            }
            else if (indexPath.section == PRODUCT_TIER) //product tier
            {
                sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductTier"];
                
            }
            else if (indexPath.section == PRODUCT_CATEGORY) //product category
            {
                sectionArray = [self.breakdownDictionary objectForKey: @"ChartProductCategory"];
            }
            
            if (indexPath.row < [sectionArray count])
            {
                cell = [tableView dequeueReusableCellWithIdentifier: @"RUDSROilBreakdownCell"];
                
                cell.backgroundColor = COLOUR_WHITE;
                
                NSDictionary *cellData = [sectionArray objectAtIndex: indexPath.row];
                
                
                UIView *colourTagView = [cell viewWithTag: BREAKDOWNCELL_COLOURTAG];
                colourTagView.backgroundColor = [Helper colorFromHex: [cellData objectForKey: @"ColorCode"]];
                
                UILabel *lblProductData = [cell viewWithTag:  BREAKDOWNCELL_PRODUCT];
                
                
                if (indexPath.section == PRODUCT_BRAND) //product tier
                {
                    lblProductData.text = [cellData objectForKey: @"ProductBrand"];
                }
                else if (indexPath.section == PRODUCT_TIER) //product tier
                {
                    lblProductData.text = [cellData objectForKey: @"ProductTier"];
                }
                else if (indexPath.section == PRODUCT_CATEGORY)  //product category
                {
                    
                    lblProductData.text = [cellData objectForKey: @"ProductCategory"];
                }
                
                UILabel *lblTotalUnitData = [cell viewWithTag:  BREAKDOWNCELL_TOTALUNIT];
                
                lblTotalUnitData.text = [[cellData objectForKey: @"Total"] stringValue];
                
                UILabel *lblTotalPercentData = [cell viewWithTag:  BREAKDOWNCELL_TOTALPERCENT];
                lblTotalPercentData.text = [[cellData objectForKey: @"TotalPercent"] stringValue];
            }
            else
            {
                cell = [tableView dequeueReusableCellWithIdentifier: @"RUDSROilBreakdownFooterCell"];
                
                UILabel *lblFooter = [cell viewWithTag: 1];
                if (indexPath.section == PRODUCT_BRAND) //product brand
                {
                    lblFooter.text = LOCALIZATION(C_RU_BREAKDOWN_OILCHANGE_BRANDFOOTER);    //lokalised 28 Jan
                }
                //                else if (indexPath.section == PRODUCT_TIER) //product tier
                //                {
                //                    lblFooter.text = LOCALIZATION(@"* Oil changes registered with non-Shell branded engine oil are all grouped under \"Other Brands \"");
                //                }
                //                else if (indexPath.section == PRODUCT_CATEGORY) //product category
                else
                {
                    lblFooter.text = LOCALIZATION(C_RU_BREAKDOWN_OILCHANGE_FOOTER); //lokalised 28 Jan
                }
            }
            }
            break;
    }
    
    return cell;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
