//
//  RUCustomersBreakdownViewController.m
//  Shell
//
//  Created by Jeremy Lua on 4/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUCustomersBreakdownViewController.h"

#import "CASMonthYearPicker.h"

@interface RUCustomersBreakdownViewController () <WebServiceManagerDelegate, MonthYearPickerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *btnStartDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEndDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property NSDate *startDate;
@property NSDate *endDate;

@property (weak, nonatomic) IBOutlet UIButton *btnEnter;

@property BOOL isStartDate;
@property CASMonthYearPicker *dateView;

@property (weak, nonatomic) IBOutlet UITableView *customerTableView;
@property NSDictionary *breakdownDictionary;

@property NSMutableArray *trendsArray;
@property NSMutableArray *customerSignupArray;
@end

@implementation RUCustomersBreakdownViewController
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_RU_BREAKDOWN_CUSTOMER_TITLE) subtitle: @""];   //lokalised 28 Jan
    
    [self setNavBackBtn];
    self.lblTo.text = LOCALIZATION(C_RU_STEPONE_TO);    //lokalised 28 Jan
    [self.btnEnter setTitle: LOCALIZATION(C_RU_STEPONE_ENTER) forState: UIControlStateNormal];  //lokalised 28 Jan
    self.view.backgroundColor = COLOUR_VERYPALEGREY;
    self.customerTableView.backgroundColor = COLOUR_VERYPALEGREY;
    self.customerTableView.backgroundView = nil;
    
    dateView = [[CASMonthYearPicker alloc] initWithTitle:LOCALIZATION(C_PERFORMANCE_SELECTDATE) previouslySelected:nil withYearHidden:NO];  //lokalised 28 Jan
    
    dateView.delegate = self;
    

    //setup default date
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    //setup the necesary date adjustments
    NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
    [selectedComponent setDay: 1];
    NSDate *selectedDate = [gregorian dateFromComponents: selectedComponent];
    
    [selectedComponent setDay: 0];
    [selectedComponent setMonth: -2];
    [selectedComponent setYear: 0];
    selectedDate = [gregorian dateByAddingComponents: selectedComponent toDate: selectedDate options:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)];
    self.isStartDate = YES;
    self.startDate = selectedDate;
    [self selectedMonthYear: selectedDate];
    [self enterPressed: nil];
}

- (IBAction)enterPressed:(id)sender
{
    NSMutableDictionary *tradeDict = [[NSMutableDictionary alloc] init];
    //    LoginInfo *loginInfo = [mSession loadUserProfile];
    //
    //    [tradeDict setObject: loginInfo.tradeID forKey: @"TradeID"];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy/MM/dd"];
    
    [tradeDict setObject: [outputFormatter stringFromDate: self.startDate] forKey:@"StartDate"];
    [tradeDict setObject: [outputFormatter stringFromDate: self.endDate] forKey:@"EndDate"];
    
    [[WebServiceManager sharedInstance] fetchCustomerBreakdown: tradeDict vc: self];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_FETCH_CUSTOMERBREAKDOWN:
        {
            self.breakdownDictionary = [response getGenericResponse];
            
            if ([self.breakdownDictionary objectForKey: @"CustomerTrend"])
            {
                self.trendsArray = [self.breakdownDictionary objectForKey: @"CustomerTrend"];
            }
            if ([self.breakdownDictionary objectForKey: @"NewCustomerSignUps"])
            {
                self.customerSignupArray = [self.breakdownDictionary objectForKey: @"NewCustomerSignUps"];
            }
            
            self.customerTableView.hidden = NO;
            [self.customerTableView reloadData];
        }
            break;
            
        default:
            break;
    }
}

- (void)processFailed:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_FETCH_CUSTOMERBREAKDOWN:
        {
            self.customerTableView.hidden = YES;
        }
            break;
            
        default:
            break;
    }
}

- (IBAction)datePressed:(UIButton *)sender {
    [self.view endEditing: YES];
    
    if ([sender isEqual: self.btnStartDate])
        self.isStartDate = YES;
    else
        self.isStartDate = NO;
    
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}



#pragma Date Picker delegate method
- (void)selectedMonthYear:(NSDate *)selectedDate
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    
    //setup the necesary date adjustments
    NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: selectedDate];
    [selectedComponent setDay: 1];
    selectedDate = [gregorian dateFromComponents: selectedComponent];
    
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setMonth: 3];
    [offsetComponents setDay: -1];
    
    NSDateFormatter *btnDateFormatter = [[NSDateFormatter alloc] init];
    [btnDateFormatter setDateFormat:@"MMM yyyy"];
    if ([GET_LOCALIZATION isEqualToString: kRussian])
        [btnDateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kRussian]];
    else
        [btnDateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    
    if (self.isStartDate)
    {
        NSDate *offsetDate = [gregorian dateByAddingComponents:offsetComponents toDate: selectedDate options:0];
        
        NSString *selectedDateString;
        NSString *offsetDateString;
        //check end date if more than current date
        if ([offsetDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
        {
            //check start date selection if more than current date.
            if ([selectedDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
            {
                NSDateComponents *selectedComponent = [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
                [selectedComponent setDay: 1];
                selectedDate = [gregorian dateFromComponents: selectedComponent];
            }
            selectedDateString = [btnDateFormatter stringFromDate: selectedDate]; //first of current month
            offsetDateString = [btnDateFormatter stringFromDate: [NSDate date]]; //current date
            offsetDate = [NSDate date]; //set end date to current date
        }
        else
        {
            //start date will never be more than current date if above validation of end date clears.
            selectedDateString = [btnDateFormatter stringFromDate: selectedDate];
            offsetDateString = [btnDateFormatter stringFromDate: offsetDate]; //offset still 2 months later
        }
        
        
        //hardcode to first three character in russian
        if ([GET_LOCALIZATION isEqualToString: kRussian])
        {
            NSMutableArray *selectedDateStringArray = [[NSMutableArray alloc] initWithArray: [selectedDateString componentsSeparatedByString: @" "]];
            NSString *selectedMonth = [selectedDateStringArray objectAtIndex: 0];
            selectedMonth = [selectedMonth substringToIndex: 3];
            selectedDateString = [[NSString stringWithFormat: @"%@ %@", selectedMonth, [selectedDateStringArray objectAtIndex: 1]] capitalizedString];
            
            NSMutableArray *offsetDateStringArray = [[NSMutableArray alloc] initWithArray: [offsetDateString componentsSeparatedByString: @" "]];
            NSString *offsetMonth = [offsetDateStringArray objectAtIndex: 0];
            offsetMonth = [offsetMonth substringToIndex: 3];
            offsetDateString = [[NSString stringWithFormat: @"%@ %@", offsetMonth, [offsetDateStringArray objectAtIndex: 1]] capitalizedString]; //Capitalised, e.g Aug, Sep, instead of aug, sep
        }
        
        //end start is offset by 3 months
        [self.btnStartDate setTitle: selectedDateString forState: UIControlStateNormal];
        [self.btnEndDate setTitle: offsetDateString forState: UIControlStateNormal];
        
        self.startDate = selectedDate;
        self.endDate = offsetDate;
        
    }
    else
    {
        //handle only end date
        
        //set date to end of selected month
        NSDateComponents *endOfMonthComponents = [[NSDateComponents alloc] init];
        [endOfMonthComponents setYear: 0];
        [endOfMonthComponents setMonth: 1];
        [endOfMonthComponents setDay: -1];
        selectedDate = [gregorian dateByAddingComponents: endOfMonthComponents toDate: selectedDate options: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)];
        
        //check date from startdate
        NSDate *offsetDate = [gregorian dateByAddingComponents:offsetComponents toDate: self.startDate options:0];
        
        //if offset date is more than current month, change offset to current month
        if ([offsetDate timeIntervalSinceReferenceDate] > [[NSDate date] timeIntervalSinceReferenceDate])
        {
            //reset offset date to last day of current month
            NSDateComponents *currentDateComponents = [gregorian components: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]];
            [currentDateComponents setDay: 1];
            [currentDateComponents setMonth: [currentDateComponents month] + 1];
            offsetDate = [gregorian dateFromComponents: currentDateComponents];
            
            [currentDateComponents setDay: -1];
            [currentDateComponents setMonth: 0];
            [currentDateComponents setYear: 0];
            offsetDate = [gregorian dateByAddingComponents: currentDateComponents toDate: offsetDate options:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) ];
        }
        
        //if selection more than threshold (+2 months from start month)
        if ([selectedDate timeIntervalSinceReferenceDate] > [offsetDate timeIntervalSinceReferenceDate])
        {
            selectedDate = offsetDate;
        }
        else if ([selectedDate timeIntervalSinceReferenceDate] < [self.startDate timeIntervalSinceReferenceDate])
        {
            //if selection less than start month
            selectedDate = self.startDate;
        }
        else
        {
            
        }
        
        //don't need to change start date.
        NSString *selectedDateString = [btnDateFormatter stringFromDate: selectedDate];
        
        //hardcode to first three character in russian
        if ([GET_LOCALIZATION isEqualToString: kRussian])
        {
            NSMutableArray *selectedDateStringArray = [[NSMutableArray alloc] initWithArray: [selectedDateString componentsSeparatedByString: @" "]];
            NSString *selectedMonth = [selectedDateStringArray objectAtIndex: 0];
            selectedMonth = [selectedMonth substringToIndex: 3];
            selectedDateString = [[NSString stringWithFormat: @"%@ %@", selectedMonth, [selectedDateStringArray objectAtIndex: 1]] capitalizedString];
        }
        
        [self.btnEndDate setTitle: selectedDateString forState: UIControlStateNormal];
        self.endDate = selectedDate;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1)
    {
        return [self.trendsArray count];
    }
    else if (section == 2)
    {
        return [self.customerSignupArray count];
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return 0;
    return UITableViewAutomaticDimension;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

enum
{
    kCUSTOMERSBREAKDOWN_TAGS_Header = 10001,
    kCUSTOMERSBREAKDOWN_TAGS_StaffName,
    kCUSTOMERSBREAKDOWN_TAGS_MonthOne, //imageview also same tag
    kCUSTOMERSBREAKDOWN_TAGS_MonthTwo,
    kCUSTOMERSBREAKDOWN_TAGS_MonthThree,
    kCUSTOMERSBREAKDOWN_TAGS_Total,
} kCUSTOMERSBREAKDOWN_TAGS_TYPE;

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell;
    if (section == 0)
        return [UIView new];
    
    cell = [tableView dequeueReusableCellWithIdentifier: @"FullHeaderCell"];
    
    UILabel *lblStaffName = [cell viewWithTag: kCUSTOMERSBREAKDOWN_TAGS_StaffName];
    lblStaffName.text = LOCALIZATION(C_RU_BREAKDOWN_STAFF_STAFFNAME);   //lokalised 28 Jan
    
    UILabel *lblTotal = [cell viewWithTag: kCUSTOMERSBREAKDOWN_TAGS_Total];
    lblTotal.text = LOCALIZATION(C_RU_BREAKDOWN_TOTAL);                 //lokalised 28 Jan
    
    NSArray *monthListArray = [self.breakdownDictionary objectForKey: @"MonthList"];
    UILabel *lblMonthOne = [cell viewWithTag:  kCUSTOMERSBREAKDOWN_TAGS_MonthOne];
    UILabel *lblMonthTwo = [cell viewWithTag:  kCUSTOMERSBREAKDOWN_TAGS_MonthTwo];
    UILabel *lblMonthThree = [cell viewWithTag:  kCUSTOMERSBREAKDOWN_TAGS_MonthThree];
    
    if ([monthListArray count] < 3)
    {
        lblMonthThree.hidden = YES;
    }
    else
    {
        lblMonthThree.text = [[monthListArray objectAtIndex: 2] objectForKey: @"MonthName"];
    }
    
    if ([monthListArray count] < 2)
    {
        lblMonthTwo.hidden = YES;
    }
    else
    {
        lblMonthTwo.text = [[monthListArray objectAtIndex: 1] objectForKey: @"MonthName"];
    }
    
    if ([monthListArray count] < 1)
    {
        lblMonthOne.hidden = YES;
    }
    else
    {
        lblMonthOne.text = [[monthListArray objectAtIndex: 0] objectForKey: @"MonthName"];
    }
    
    UILabel *lblSectionHeader = [cell viewWithTag: kCUSTOMERSBREAKDOWN_TAGS_Header];
    
    if (section == 1)
    {
        lblSectionHeader.text = LOCALIZATION(C_RU_BREAKDOWN_CUSTOMERTREND);         //lokalised 28 Jan
    }
    else if (section == 2)
    {
        lblSectionHeader.text = LOCALIZATION(C_RU_BREAKDOWN_NEWCUSTOMERSIGNUPS);    //lokalised 28 Jan
    }
    
    return cell.contentView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch (indexPath.section) {
        case 0:
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"MainHeaderCell"];
            
            cell.backgroundColor = [UIColor clearColor];
            
            if (!self.breakdownDictionary)
                break;
            
            UILabel *lblHeader = [cell viewWithTag: 1];
            lblHeader.text = LOCALIZATION(C_RU_BREAKDOWN_NEWCUSTOMERSIGNUPS);   //lokalised 28 Jan
            
            UILabel *lblHeaderData = [cell viewWithTag: 2];
            lblHeaderData.text = [self.breakdownDictionary objectForKey: @"TotalCustomerSignUp"];
            
            UILabel *lblUpdated = [cell viewWithTag: 3];
            lblUpdated.text = [NSString stringWithFormat: @"%@ %@", [self.breakdownDictionary objectForKey: @"UpdatedOnText"], [self.breakdownDictionary objectForKey: @"UpdatedOn"] ];
        }
            break;
        default:
        {
            NSMutableArray *sectionArray;
            if (indexPath.section == 1)
            {
                sectionArray = self.trendsArray;
                
            }
            else if (indexPath.section == 2)
            {
                sectionArray = self.customerSignupArray;
            }
            
            {
                cell = [tableView dequeueReusableCellWithIdentifier: @"BreakdownCell"];
                
                cell.backgroundColor = COLOUR_WHITE;
                
                
                NSDictionary *cellData = [sectionArray objectAtIndex: indexPath.row];
                if (indexPath.section == 1) //trends
                {
                    UILabel *lblKey = [cell viewWithTag: kCUSTOMERSBREAKDOWN_TAGS_StaffName];
                    lblKey.text = [cellData objectForKey: @"Key"];
                }
                else
                {
                    UILabel *lblName = [cell viewWithTag: kCUSTOMERSBREAKDOWN_TAGS_StaffName];
                    lblName.text = [cellData objectForKey: @"Name"];
                }
                
                UILabel *lblMonthOne = [cell viewWithTag: kCUSTOMERSBREAKDOWN_TAGS_MonthOne];
                lblMonthOne.text = [cellData objectForKey: @"MonthValue1"];
                
                UILabel *lblMonthTwo = [cell viewWithTag: kCUSTOMERSBREAKDOWN_TAGS_MonthTwo];
                lblMonthTwo.text = [cellData objectForKey: @"MonthValue2"];
                
                UILabel *lblMonthThree = [cell viewWithTag: kCUSTOMERSBREAKDOWN_TAGS_MonthThree];
                lblMonthThree.text = [cellData objectForKey: @"MonthValue3"];
                
                UILabel *lblMonthTotal = [cell viewWithTag: kCUSTOMERSBREAKDOWN_TAGS_Total];
                lblMonthTotal.text = [cellData objectForKey: @"Total"];
            }
        }
            break;
    }
    return cell;
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
