//
//  RUTOLeaderboardViewController.m
//  Shell
//
//  Created by Jeremy Lua on 8/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUTOLeaderboardViewController.h"

@interface RUTOLeaderboardViewController () <UITableViewDelegate, UITableViewDataSource, WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *leaderboardTableView;
@property (weak, nonatomic) IBOutlet UILabel *lblFooter;

@property NSMutableArray *sectionOpenControlArray; //to control array of BOOL to control sections open/close

//@property NSMutableArray *leaderboardArray;

@property NSMutableArray *oilChangeArray;
@property NSString *oilChangeHeaderString;
@property NSMutableArray *customerSignupArray;
@property NSString *customerSignupHeaderString;
@end

@implementation RUTOLeaderboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.lblFooter.text = LOCALIZATION(C_RU_LEADERBOARD_DSRFOOTER); //lokalised 28 Jan
    
//    self.leaderboardArray = [[NSMutableArray alloc] init];
    
    
    if(@available(iOS 11, *))
    {
        self.leaderboardTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.leaderboardTableView setContentInset:UIEdgeInsetsMake(15, 0, 15, 0)];

    self.leaderboardTableView.backgroundColor = [UIColor clearColor];
    self.leaderboardTableView.backgroundView = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_WORKSHOPPERFORMANCE),LOCALIZATION_EN(C_RU_PERFORMANCE_STAFFLEADERBOARD)] screenClass:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[WebServiceManager sharedInstance] fetchLeaderboard: @{@"ViewType": @(2)} vc:self];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_FETCH_LEADERBOARD:
            {
                NSDictionary *respData = [response getGenericResponse];
                self.sectionOpenControlArray = [[NSMutableArray alloc] init];
                
                if (![[respData objectForKey: @"OilChange"]isKindOfClass:[NSNull class]])
                {
                    [self.sectionOpenControlArray addObject: @(NO)];
                    self.oilChangeArray = [respData objectForKey: @"OilChange"];
                    self.oilChangeHeaderString = [respData objectForKey: @"OilChangeHeaderText"];
                }
                if (![[respData objectForKey: @"CustomerSignUps"]isKindOfClass:[NSNull class]])
                {
                    [self.sectionOpenControlArray addObject: @(NO)];
                    self.customerSignupArray = [respData objectForKey: @"CustomerSignUps"];
                    self.customerSignupHeaderString = [respData objectForKey: @"CustomerSignupHeaderText"];
                }
                self.lblFooter.text = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_RU_LEADERBOARD_DSRFOOTER), [respData objectForKey: @"UpdateText"]];  //lokalised 28 Jan
                
                [self.leaderboardTableView reloadData];
            }
            break;
            
        default:
            break;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//    return [self.leaderboardArray count];
 
    return [self.sectionOpenControlArray count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    NSDictionary *sectionData = [self.leaderboardArray objectAtIndex: section];
//
    if (!self.sectionOpenControlArray) //nil array
        return 0;
    
    if (section >= [self.sectionOpenControlArray count]) //out of bounds
        return 0;
    
    if ([[self.sectionOpenControlArray objectAtIndex: section] boolValue])
    {
//        NSArray *dataArray = [sectionData objectForKey: @"Data"];
//        return [dataArray count];
        
        if (section == 0)
        {
            if (self.oilChangeArray.count > 0)
                return [self.oilChangeArray count];
            else
                return [self.customerSignupArray count];
        }
        else if (section == 1)
        {
            return [self.customerSignupArray count];
        }
    }
    
    
    return 0;
}


enum
{
    kLEADERBOARD_TAGS_Plus = 9999,
    kLEADERBOARD_TAGS_Button = 10000,
    kLEADERBOARD_TAGS_Header = 10001,
    kLEADERBOARD_TAGS_Rank,
    kLEADERBOARD_TAGS_Daily, //imageview also same tag
    kLEADERBOARD_TAGS_Mechanic,
    kLEADERBOARD_TAGS_Number,
} kLEADERBOARD_TAGS_TYPE;

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell;
    if ([self.sectionOpenControlArray count] == 0)
    {
        return [UITableViewCell new];
    }
    else if ([[self.sectionOpenControlArray objectAtIndex: section] boolValue])
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"FullHeaderCell"];
        UIImageView *imgPlus = [cell viewWithTag: kLEADERBOARD_TAGS_Plus];
        imgPlus.transform = CGAffineTransformRotate(CGAffineTransformIdentity, M_PI_4);
        
        UILabel *lblRankHeader = [cell viewWithTag: kLEADERBOARD_TAGS_Rank];
        lblRankHeader.text = LOCALIZATION(C_RU_LEADERBOARD_RANK);               //lokalised 28 Jan
        
        UILabel *lblDailyHeader = [cell viewWithTag: kLEADERBOARD_TAGS_Daily];
        lblDailyHeader.text = LOCALIZATION(C_RU_LEADERBOARD_DAILY);             //lokalised 28 Jan
        
        UILabel *lblMechanicHeader = [cell viewWithTag: kLEADERBOARD_TAGS_Mechanic];
        lblMechanicHeader.text = LOCALIZATION(C_RU_STAFFLEADERBOARD_MECHANIC);  //lokalised 28 Jan
        
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"HeaderCell"];
    }
    UILabel *lblSectionHeader = [cell viewWithTag: kLEADERBOARD_TAGS_Header];
//    lblSectionHeader.text = [sectionData objectForKey: @"SectionName"];
    
    if (section == 0)
    {
        if (self.oilChangeArray.count > 0)
            lblSectionHeader.text = self.oilChangeHeaderString;
        else
            lblSectionHeader.text = self.customerSignupHeaderString;
    }
    else if (section == 1)
    {
        lblSectionHeader.text = self.customerSignupHeaderString;
    }
    
    UIButton *btnHeader = [cell viewWithTag: kLEADERBOARD_TAGS_Button
                           ]; //10000 set in storyboard
    btnHeader.tag = section;
    
    return cell.contentView;
}

- (IBAction)headerPressed:(UIButton *)sender
{
    BOOL newControl = ![[self.sectionOpenControlArray objectAtIndex: sender.tag] boolValue];
    [self.sectionOpenControlArray replaceObjectAtIndex: sender.tag withObject: @(newControl)];
    [self.leaderboardTableView reloadData];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"BodyCell"];
//    NSDictionary *sectionData = [self.leaderboardArray objectAtIndex: indexPath.section];
    NSArray *dataArray;
    if (indexPath.section == 0)
    {
        if (self.oilChangeArray.count > 0)
            dataArray = self.oilChangeArray;
        else
            dataArray = self.customerSignupArray;
    }
    else if (indexPath.section == 1)
        dataArray = self.customerSignupArray;
        
//    = [sectionData objectForKey: @"Data"];
    
    NSDictionary *cellData = [dataArray objectAtIndex: indexPath.row];
    
    UILabel *lblRank = [cell viewWithTag: kLEADERBOARD_TAGS_Rank];
    lblRank.text = [cellData objectForKey: @"Rank"];
    
    UILabel *lblPerformanceArrow = [cell viewWithTag: kLEADERBOARD_TAGS_Daily];
    if ([[cellData objectForKey: @"PerformanceFlag"] isEqualToString: @"1"])
    {
        lblPerformanceArrow.text = @"▲";
        lblPerformanceArrow.textColor = COLOUR_DARKGREEN;
    }
    else
    {
        lblPerformanceArrow.text = @"▼";
        lblPerformanceArrow.textColor = COLOUR_RED;
    }
        
    UILabel *lblMechanic = [cell viewWithTag: kLEADERBOARD_TAGS_Mechanic];
    lblMechanic.text = [cellData objectForKey: @"Name"];
    
    UILabel *lblNumber = [cell viewWithTag: kLEADERBOARD_TAGS_Number];
    lblNumber.text = [cellData objectForKey: @"TotalValue"];
    
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
