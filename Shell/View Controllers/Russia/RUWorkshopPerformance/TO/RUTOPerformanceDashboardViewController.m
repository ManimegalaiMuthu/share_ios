//
//  RUTOPerformanceDashboardViewController.m
//  Shell
//
//  Created by Jeremy Lua on 8/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUTOPerformanceDashboardViewController.h"

@interface RUTOPerformanceDashboardViewController () <WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *dashboardTableView;

@property NSMutableArray *partnersArray;
@property NSMutableArray *performanceArray;
@end

@implementation RUTOPerformanceDashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.dashboardTableView.backgroundColor = [UIColor clearColor];
    self.dashboardTableView.backgroundView = nil;
    
    [[WebServiceManager sharedInstance] fetchPerformanceV2: self];
  //  [[WebServiceManager sharedInstance] fetchPerformanceV3:self];
    
//    [self setupInterface];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_WORKSHOPPERFORMANCE),LOCALIZATION_EN(C_RU_PERFORMANCE_DASHBOARD)] screenClass:nil];
}
//FetchPerformance_V2
-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_MSTRADE_FETCHPERFORMANCE_V3:
        case kWEBSERVICE_FETCH_PERFORMANCE_V2:
            {
//                [self setupInterface: [response getGenericResponse]];
                NSDictionary *respData = [response getGenericResponse];
                
                if(![[respData objectForKey: @"PartnersOfClub"] isKindOfClass:[NSNull class]]) {
                    self.partnersArray = [respData objectForKey: @"PartnersOfClub"];
                }
                
                self.performanceArray = [respData objectForKey: @"Performance"];
                
                [self.dashboardTableView reloadData];
            }
            break;
            
        default:
            break;
    }
}

enum
{
    PERFORMANCETABLE_CONTENT_IMAGE = 1,
    PERFORMANCETABLE_CONTENT_BODY,
    PERFORMANCETABLE_CONTENT_DATA,
    PERFORMANCETABLE_CONTENT_LINE,
    PERFORMANCETABLE_CONTENT_ARROW,
} PERFORMANCETABLE_CONTENT_TAG;

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(self.partnersArray != nil || [self.partnersArray count] > 0) {
        return 3;
    }
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            //Main Header Cell
            return 1;
            break;
        case 1:
            if(self.partnersArray != nil || [self.partnersArray count] > 0) {
                 return [self.partnersArray count] + 1;
            }
            return [self.performanceArray count] + 1; //+ 1 for button
            break;
        case 2:
            return [self.performanceArray count] + 1; // +1 for footer
            break;
        default:
            break;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch (indexPath.section) {
        case 0:
        {
            cell = [tableView dequeueReusableCellWithIdentifier: @"MainHeaderCell"];
            UILabel *lblHeader = [cell viewWithTag: 1];
            lblHeader.text = LOCALIZATION(C_RU_PERFORMANCE_DASHBOARD_HEADER);   //lokalised 28 Jan
        }
            break;
        case 1:
        {
           if(self.partnersArray != nil || [self.partnersArray count] > 0) {
               cell = [self setPartnersOfShellFor:indexPath.row];
           } else {
               cell = [self setPerformanceCellFor:indexPath.row];
           }
        }
            break;
        case 2:
        {
            cell = [self setPerformanceCellFor:indexPath.row];
        }
            break;
        default:
            break;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cellData;
    switch (indexPath.section) {
        case 1: //partners
        {
            if(self.partnersArray != nil || [self.partnersArray count] > 0) {
                cellData = [self.partnersArray objectAtIndex: indexPath.row];
            } else {
                cellData = [self.performanceArray objectAtIndex: indexPath.row];
            }
        }
            break;
        case 2: // performance
        {
            cellData = [self.performanceArray objectAtIndex: indexPath.row];
        }
            break;
        default:
            return;
            break;
    }
    
    //stop if not clickable
    if (![[cellData objectForKey: @"isClickable"] boolValue])
        return;
    
    NSString *navKey = [cellData objectForKey: @"OnClickRedirectPath"];
    
    if ([navKey isEqualToString: @"Order BreakDown"])
    {
        //parent is tab controller
        [mSession pushOrderBreakdown: self.parentViewController.parentViewController];
    }
    else if ([navKey isEqualToString: @"Staff BreakDown"])
    {
        [mSession pushStaffBreakdown:self.parentViewController.parentViewController];
    }
    else if ([navKey isEqualToString: @"Oil Change BreakDown"])
    {
        [mSession pushOilChangeBreakdown:self.parentViewController.parentViewController];
    }
    else if ([navKey isEqualToString: @"Customer BreakDown"])
    {
        [mSession pushCustomerBreakdown:self.parentViewController.parentViewController];
    }
    else if ([navKey isEqualToString: @"Offer Redemption BreakDown"])
    {
        [mSession pushOfferRedemptionBreakdown:self.parentViewController.parentViewController];
    }
    else if([navKey isEqualToString:@"Points Earned BreakDown"]) {
        [mSession pushPointsBreakdown: self.parentViewController.parentViewController];
    }
}

enum
{
    PARTNERTABLE_HEADER_MAIN = 1,
    PARTNERTABLE_HEADER_SUB,
} PARTNERTABLE_HEADER;

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.partnersArray != nil && section == 1)
        return UITableViewAutomaticDimension;
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.partnersArray != nil && section == 1)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"PartnersHeaderCell"];
        
        UILabel *lblPartner = [cell viewWithTag: PARTNERTABLE_HEADER_MAIN];
        lblPartner.text = LOCALIZATION(C_RU_PERFORMANCE_DASHBOARD_PARTNER); //lokalised 28 Jan
        
        UILabel *lblPartnerNote = [cell viewWithTag: PARTNERTABLE_HEADER_SUB];
        lblPartnerNote.text = LOCALIZATION(C_RU_PERFORMANCE_DASHBOARD_SUBHEADER);   //lokalised 28 Jan
        
        [cell.contentView setBackgroundColor: COLOUR_VERYPALEGREY];
        return cell.contentView;
    }
    return [UIView new];
    
}

- (IBAction)viewSummaryPressed:(id)sender
{
    [mSession loadRUPartners];
}

-(UITableViewCell *) setPartnersOfShellFor:(NSInteger)row {
    UITableViewCell *cell;
    //partners cell
    if (row != [self.partnersArray count])
    {
        NSDictionary *cellData = [self.partnersArray objectAtIndex:row];
                   
        cell = [self.dashboardTableView dequeueReusableCellWithIdentifier: @"PartnersCell"];
                   
        UIImageView *imgView = [cell viewWithTag: PERFORMANCETABLE_CONTENT_IMAGE];
        if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Order BreakDown"])
            imgView.image = [UIImage imageNamed: @"icn_orders_verydarkgrey"];
        else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Points Earned BreakDown"])
            imgView.image = [UIImage imageNamed: @"icn_points_verydarkgrey"];
        else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Product Sold Breakdown"])
        imgView.image = [UIImage imageNamed: @"icn_inventory_verydarkgrey"];
        
        UILabel *lblBody = [cell viewWithTag: PERFORMANCETABLE_CONTENT_BODY];
        lblBody.text = [cellData objectForKey: @"PerformanceKey"];
        
        UILabel *lblData = [cell viewWithTag: PERFORMANCETABLE_CONTENT_DATA];
        lblData.text = [cellData objectForKey: @"PerformanceValue"];
        
        if (row == [self.partnersArray count] - 1)
        {
            UIView *line = [cell viewWithTag: PERFORMANCETABLE_CONTENT_LINE];
            line.hidden = YES;
        }
        
        UIImageView *arrowImgView = [cell viewWithTag: PERFORMANCETABLE_CONTENT_ARROW];
        arrowImgView.hidden = ![[cellData objectForKey: @"isClickable"] boolValue];
    }
    else {
        //button
        cell = [self.dashboardTableView dequeueReusableCellWithIdentifier: @"PartnersButtonCell"];
        
        UIButton *btnViewSummary = [cell viewWithTag: 1];
        [btnViewSummary setTitle: LOCALIZATION(C_RU_PERFORMANCE_VIEWSUMMARY) forState: UIControlStateNormal];   //lokalised 28 Jan
        //target already set in storyboard
    }
    [cell setBackgroundColor: COLOUR_VERYPALEGREY];
    return cell;
}

-(UITableViewCell *) setPerformanceCellFor:(NSInteger)row {
    UITableViewCell *cell;
    if (row != [self.performanceArray count])
    {
        NSDictionary *cellData = [self.performanceArray objectAtIndex: row];
        
        //performance cell
        cell = [self.dashboardTableView dequeueReusableCellWithIdentifier: @"PerformanceCell"];
        
        UIImageView *imgView = [cell viewWithTag: PERFORMANCETABLE_CONTENT_IMAGE];
        
        if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Staff BreakDown"])
            imgView.image = [UIImage imageNamed: @"icn_staff_verydarkgrey"];
        else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Oil Change BreakDown"])
            imgView.image = [UIImage imageNamed: @"icn_oilchange_verydarkgrey"];
        else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Customer BreakDown"])
            imgView.image = [UIImage imageNamed: @"icn_consumers_verydarkgrey"];
        else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Offer Redemption BreakDown"])
            imgView.image = [UIImage imageNamed: @"icn_redemptions_verydarkgrey"];
        else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Order BreakDown"])
            imgView.image = [UIImage imageNamed: @"icn_orders_verydarkgrey"];
        else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Points Earned BreakDown"])
            imgView.image = [UIImage imageNamed: @"icn_points_verydarkgrey"];
        
       else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"Product Sold Breakdown"])
        imgView.image = [UIImage imageNamed: @"icn_inventory_verydarkgrey"];
        
        else if ([[cellData objectForKey: @"IconName"] isEqualToString: @"ApprovedWorkshop"])
                                imgView.image = [UIImage imageNamed: @"icn_totalworkshops_verydarkgrey"];
           
        
        UILabel *lblBody = [cell viewWithTag: PERFORMANCETABLE_CONTENT_BODY];
        lblBody.text = [cellData objectForKey: @"PerformanceKey"];
        
        UILabel *lblData = [cell viewWithTag: PERFORMANCETABLE_CONTENT_DATA];
        lblData.text = [cellData objectForKey: @"PerformanceValue"];
        
        UIImageView *arrowImgView = [cell viewWithTag: PERFORMANCETABLE_CONTENT_ARROW];
        arrowImgView.hidden = ![[cellData objectForKey: @"isClickable"] boolValue];
        
        if (row == [self.performanceArray count] - 1)
        {
            UIView *line = [cell viewWithTag: PERFORMANCETABLE_CONTENT_LINE];
            line.hidden = YES;
        }
    }
    else
    {
        //footer
        cell = [self.dashboardTableView dequeueReusableCellWithIdentifier: @"PerformanceFooterCell"];
        
        UILabel *lblFooter = [cell viewWithTag: 1];
        lblFooter.text = LOCALIZATION(C_RU_PERFORMANCE_DASHBOARD_FOOTER);   //lokalised 28 Jan
    }
    return cell;
}

/*
#pragma mark - Navigation

// I storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
