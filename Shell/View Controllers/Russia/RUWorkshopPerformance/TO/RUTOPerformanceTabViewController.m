//
//  RUTOPerformanceTabViewController.m
//  Shell
//
//  Created by Jeremy Lua on 8/10/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RDVTabBarItem.h"
#import "RUTOPerformanceTabViewController.h"

#import "RUTOPerformanceDashboardViewController.h"
#import "RUTOLeaderboardViewController.h"
#import "RUPartnersPointsEarned_V2ViewController.h"

@interface RUTOPerformanceTabViewController ()

@end

@implementation RUTOPerformanceTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupInterface];
}


- (void) setupInterface
{
    [Helper setNavigationBarTitle: self title: LOCALIZATION(C_TITLE_WORKSHOPPERFORMANCE) subtitle: @"" size: 15 subtitleSize: 0];   //lokalised 28 Jan
    
    NSMutableArray *title = [[NSMutableArray alloc] init];
    NSMutableArray *navControllers = [[NSMutableArray alloc] init];
    
    UIViewController *dashboardVc  = [STORYBOARD_RU_WORKSHOPPERFORMANCE instantiateViewControllerWithIdentifier: VIEW_RU_TOPERFORMANCE_DASHBOARD];
    UINavigationController *dashboardNav = [[UINavigationController alloc] initWithRootViewController: dashboardVc];
    [dashboardNav setNavigationBarHidden: YES];
    
//    [title addObject:LOCALIZATION(C_RU_WORKSHOPPERFORMANCE)];
    [title addObject:LOCALIZATION(C_RU_PERFORMANCE_DASHBOARD)];
    [navControllers addObject:dashboardNav];
    
    if([[mSession profileInfo] hasWorkshopPerformanceView]) {
        UIViewController *leaderboardVc  = [STORYBOARD_RU_WORKSHOPPERFORMANCE instantiateViewControllerWithIdentifier: VIEW_RU_TOPERFORMANCE_LEADERBOARD];
        UINavigationController *leaderboardNav = [[UINavigationController alloc] initWithRootViewController: leaderboardVc];
        [leaderboardNav setNavigationBarHidden: YES];
         
        [title addObject:LOCALIZATION(C_RU_PERFORMANCE_STAFFLEADERBOARD)];
        [navControllers addObject:leaderboardNav];
     }
    
  //  if([[mSession profileInfo] hasMYPoints]) {
         RUPartnersPointsEarned_V2ViewController *pointsVc = [STORYBOARD_RUSSIA_V2 instantiateViewControllerWithIdentifier: VIEW_PARTNERSOFSHELL_POINTSEARNED_V2];
         UINavigationController *pointsNav = [[UINavigationController alloc] initWithRootViewController: pointsVc];
         [pointsNav setNavigationBarHidden: YES];
         
         [title addObject:LOCALIZATION(C_TITLE_MYPOINTS)];
         [navControllers addObject:pointsNav];
  //   }
    
    [self setViewControllers: navControllers];

    UIImage *selected = GET_ISADVANCE ? [UIImage imageNamed:@"tab-bg_blue"] : [UIImage imageNamed:@"tab-bg"];
    UIImage *unselected = [UIImage imageNamed:@"tab-bg_grey.png"];
    
    if (kIsRightToLeft) {
        title = title.reverseObjectEnumerator.allObjects.mutableCopy;
    }
    for (int i = 0; i < [self.tabBar.items count]; i++)
    {
        RDVTabBarItem *item = [self.tabBar.items objectAtIndex: i];
        [item setTitle: [title objectAtIndex: i]];
        
        [item setBackgroundColor: COLOUR_WHITE];
        [item setTitleUnselectedAttributes:FONT_H1 colour:COLOUR_VERYDARKGREY];
        [item setTitleSelectedAttributes:FONT_H1 colour:COLOUR_RED];
        
        [item setBackgroundSelectedImage: selected withUnselectedImage: unselected];
    }
    if (kIsRightToLeft) {
        [self tabBar:self.tabBar didSelectItemAtIndex:self.viewControllers.count-1];
    }
}

#pragma mark RDVTabBarDelegate
- (BOOL)tabBar:(RDVTabBar *)tabBar shouldSelectItemAtIndex:(NSInteger)index
{
    for (RDVTabBarItem *item in [tabBar items])
    {
        
    }
    return ([tabBar selectedItem] != [[tabBar items] objectAtIndex: index]);
}
- (void)tabBar:(RDVTabBar *)tabBar didSelectItemAtIndex:(NSInteger)index
{
    //super method MUST BE PRESENT
    [super tabBar:tabBar didSelectItemAtIndex: index];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
