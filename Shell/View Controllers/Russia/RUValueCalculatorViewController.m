//
//  RUValueCalculatorViewController.m
//  Shell
//
//  Created by Jeremy Lua on 5/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUValueCalculatorViewController.h"
#import "ASValueTrackingSlider.h"
#import "RUValueCalculatorTableViewCell.h"

@interface RUValueCalculatorViewController () <UITableViewDelegate, UITableViewDataSource, ASValueTrackingSliderDelegate, ASValueTrackingSliderDataSource, WebServiceManagerDelegate>

@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPointsData;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property NSMutableArray *pointsArray;
@property NSArray *defaultPointsArray;
@property NSMutableDictionary *sliderDictionary;

@property NSDictionary *partnersLookupTable;
@property NSString *unitString;
@property NSInteger minValue;
@property NSInteger maxValue;

@property BOOL hasPopup;
@property BOOL isSaving;
@end

@implementation RUValueCalculatorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle: self title: LOCALIZATION(LOCALIZATION(C_RUSSIA_VALUE_CALCULATOR)) subtitle: @""];        //lokalised
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    
    self.lblHeader.text = LOCALIZATION(C_RU_VALUECALC_HEADER);          //lokalised
    self.lblTotalPoints.text = LOCALIZATION(C_RUSSIA_TOTAL_POINT);      //lokalised
    
//    self.defaultArray = [[NSMutableArray alloc] initWithArray: [self.calculatorDict objectForKey: @"DefaultGroupPoints"]];
    
//    [self.tableView setContentInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    
    self.sliderDictionary = [[NSMutableDictionary alloc] init];
    self.partnersLookupTable = [[mSession lookupTable] objectForKey: @"PartnersShellClub"];
    self.minValue = [[self.partnersLookupTable objectForKey: @"MinValue"] intValue];
    self.maxValue = [[self.partnersLookupTable objectForKey: @"MaxValue"]  intValue];
    
    self.unitString = [self.partnersLookupTable objectForKey: @"Unit"];
    
    [[WebServiceManager sharedInstance] fetchWaysToEarnPoints: self];
}

-(void) checkForDefault
{
    self.pointsArray = [[NSMutableArray alloc] initWithArray: [self.calculatorDict objectForKey: @"GroupPoints"]];
    
    if ([self.pointsArray count] > 0)
    {
        
    }
    else
    {
        self.pointsArray = [[NSMutableArray alloc] initWithArray:
                            self.defaultPointsArray];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.pointsArray count] + 1; // +1 for button view
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == [self.pointsArray count])
    {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"ButtonCell"];
        
        UIButton *btnReset = [cell viewWithTag: 1];
        [btnReset setTitle: LOCALIZATION(C_RU_VALUECALC_RESET) forState: UIControlStateNormal];         //lokalised
        
        UIButton *btnSave = [cell viewWithTag: 2];
        [btnSave setTitle: LOCALIZATION(C_RU_VALUECALC_SAVE) forState: UIControlStateNormal];           //lokalised
        
        [cell.contentView setBackgroundColor: [UIColor clearColor]];
        
        return cell;
    }
    else
    {
        RUValueCalculatorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"ProductCell"];
        
        if (!cell.isLoaded)
        {
            NSDictionary *cellData = [self.pointsArray objectAtIndex: indexPath.section];
            NSDictionary *defaultCellData = [self.defaultPointsArray objectAtIndex: indexPath.section];
        
            cell.unit = self.unitString;
        
            cell.lblProductPointsName.text = [defaultCellData objectForKey: @"GroupName"];
        
            cell.slider.dataSource = self;
            cell.slider.maximumValue = self.maxValue;
        
            cell.lblProductPoints.text = [NSString stringWithFormat: @"%i %@", ([[cellData objectForKey: @"Points"] intValue] * [[cellData objectForKey: @"Volume"] intValue]), LOCALIZATION(C_REWARDS_PTS)];               //lokalised
        
            cell.lblMinPoints.text = [NSString stringWithFormat: @"%i %@", self.minValue, self.unitString];
        
    //        UILabel *lblMaxPoints = [cell viewWithTag: 4];
            cell.lblMaxPoints.text = [NSString stringWithFormat: @"%i %@", self.maxValue, self.unitString];
        
            //initial setup
            cell.slider.value = [[cellData objectForKey: @"Volume"] intValue];
        
            cell.txtUnits.text = [NSString stringWithFormat: @"%@ %@", [cellData objectForKey: @"Volume"], self.unitString];
        
            [self.sliderDictionary setObject: cell.slider forKey: [defaultCellData objectForKey: @"GroupName"]];
            cell.slider.tag = indexPath.section;
            cell.isLoaded = YES;
        }
        return cell;
    }
    return [UITableViewCell new];
}

// slider has a custom NSNumberFormatter to display temperature in °C
// the dataSource method below returns custom NSStrings for specific values
- (NSString *)slider:(ASValueTrackingSlider *)slider stringForValue:(float)value;
{
    NSInteger otherSliderTotal = 0; //to limit slider from going past max Value
    value = floor(value); //truncate zeros
    for (NSString *key in [self.sliderDictionary allKeys])
    {
        ASValueTrackingSlider *otherSlider = [self.sliderDictionary objectForKey: key];
        if (otherSlider != slider)
        {
            otherSliderTotal += otherSlider.value;
        }
    }
    
    if (value + otherSliderTotal >= self.maxValue)
    {
        value = self.maxValue - otherSliderTotal;
        slider.value = self.maxValue - otherSliderTotal;
        
        //no popup if single slider maxed out.
        //no popup if initially set
        if (!self.hasPopup &&
            otherSliderTotal > 0 &&
            [slider isTracking])
        {
            //cancel longpress gesture so slider doesnt move with popup
            [slider cancelTrackingWithEvent: nil];
            
            self.hasPopup = YES;
            [mAlert showErrorAlertWithMessage: LOCALIZATION(C_RU_VALUECALC_MAXIMUM_UNITS) onCompletion:^(BOOL finished) {
                self.hasPopup = NO;
            }];             //lokalised on 11 Feb
        }
    }
    

    NSInteger index = slider.tag;
    
    RUValueCalculatorTableViewCell *cell = slider.superview.superview;
    NSDictionary *cellData = [self.pointsArray objectAtIndex: index];
    
    //don't edit cell textfield if textfield is the one changing the slider value
    if (![cell.txtUnits isEditing])
        cell.txtUnits.text = [NSString stringWithFormat: @"%.0f %@", value, self.unitString];

    NSMutableAttributedString *prefixString = [[NSMutableAttributedString alloc] initWithString: @(value * [[cellData objectForKey: @"Points"] intValue]).stringValue attributes:@{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_H1, }];
    
    NSAttributedString *selectionString = [[NSAttributedString alloc] initWithString: LOCALIZATION(C_REWARDS_PTS) attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B3, }]; //bold verification number      //lokalised
    
    [prefixString appendAttributedString: [[NSAttributedString alloc] initWithString: @" "]];
    [prefixString appendAttributedString: selectionString];
    cell.lblProductPoints.attributedText = prefixString;
    
//    cell.lblProductPoints.text = [NSString stringWithFormat: @"%.0f %@", (value * [[cellData objectForKey: @"Points"] intValue]), LOCALIZATION(C_REWARDS_PTS)];
    
    
    [self calculateTotal];
    
    NSString *string = [NSString stringWithFormat: @"%.0f %@", value, self.unitString];
    return string;
}

-(void) calculateTotal
{
    NSInteger totalPoints = 0;
    
    for (NSString *key in [self.sliderDictionary allKeys])
    {
        ASValueTrackingSlider *slider = [self.sliderDictionary objectForKey: key];
        
        NSDictionary *cellData = [self.pointsArray objectAtIndex: slider.tag];
        
        totalPoints += (floor(slider.value) * [[cellData objectForKey: @"Points"] intValue]); //calculate total
    }
    self.lblTotalPointsData.text = [NSString stringWithFormat: @"%i %@", totalPoints, LOCALIZATION(C_REWARDS_PTS)];
}

- (IBAction)resetPressed:(id)sender
{
    [self popUpYesNoWithTitle: LOCALIZATION(C_RU_VALUECALC_POPUP_RESET_HEADER)          //lokalised
                      content: LOCALIZATION(C_RU_VALUECALC_POPUP_RESET_BODY)            //lokalised
                  yesBtnTitle: LOCALIZATION(C_RU_VALUECALC_RESET)                       //lokalised
                   noBtnTitle: LOCALIZATION(C_FORM_NO)                                  //lokalised
                 onYesPressed: ^(BOOL finished) {
                     [self resetCalculation];
                 }
                  onNoPressed:^(BOOL finished) {
                      
                  }];
}

-(void) resetCalculation
{
    for (NSString *key in [self.sliderDictionary allKeys])
    {
        ASValueTrackingSlider *slider = [self.sliderDictionary objectForKey: key];
        
        slider.value = 0;
    }

//    [self.tableView reloadData];
}

//-(void) resetCalculation
//{
//
//    NSMutableDictionary *submitParams = [[NSMutableDictionary alloc] init];
//
//    [submitParams setObject: @(YES) forKey: @"isReset"];
//
//    NSMutableArray *submitPointsCalcArray = [[NSMutableArray alloc] init];
//
//    //    NSMutableArray *defaultPointsArray = [[NSMutableArray alloc] initWithArray:[self.calculatorDict objectForKey: @"DefaultGroupPoints"]];
//    //
//    //to send in the same order
//    for (int i = 0; i < [self.defaultPointsArray count]; i++)
//    {
//        NSDictionary *dict = [self.defaultPointsArray objectAtIndex: i]; //group points details
//
//        NSMutableDictionary *submitPointsDict = [[NSMutableDictionary alloc] init];
//        [submitPointsDict setObject: [dict objectForKey: @"GroupID"] forKey: @"ProductGroupID"];
//        [submitPointsDict setObject: [dict objectForKey: @"GroupName"] forKey: @"GroupName"];
//        [submitPointsDict setObject: [dict objectForKey: @"Volume"] forKey: @"Volume"];
//        [submitPointsDict setObject: [dict objectForKey: @"Points"] forKey: @"Points"];
//
//        [submitPointsCalcArray addObject: submitPointsDict];
//    }
//    [submitParams setObject: submitPointsCalcArray forKey: @"PointsCalculator"];
//
//    [[WebServiceManager sharedInstance] submitPointsCalculator: submitParams vc: self];
//}


- (IBAction)savePressed:(id)sender
{
    NSMutableDictionary *submitParams = [[NSMutableDictionary alloc] init];
    
    [submitParams setObject: @(NO) forKey: @"isReset"];

    NSMutableArray *submitPointsCalcArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [self.pointsArray count]; i++)
    {
        NSDictionary *dict = [self.pointsArray objectAtIndex: i]; //group points details
        NSDictionary *defaultDict = [self.defaultPointsArray objectAtIndex: i];
        
        ASValueTrackingSlider *slider =
        [self.sliderDictionary objectForKey: [defaultDict objectForKey: @"GroupName"]];
        
        NSMutableDictionary *submitPointsDict = [[NSMutableDictionary alloc] init];
        [submitPointsDict setObject: [dict objectForKey: @"GroupID"] forKey: @"ProductGroupID"];
        
        if ([[dict objectForKey: @"GroupName"] isKindOfClass: [NSNull class]])
            [submitPointsDict setObject: [defaultDict objectForKey: @"GroupName"] forKey: @"GroupName"];
        else
            [submitPointsDict setObject: [dict objectForKey: @"GroupName"] forKey: @"GroupName"];
        
        [submitPointsDict setObject: @((int) slider.value) forKey: @"Volume"];
        [submitPointsDict setObject: [dict objectForKey: @"Points"] forKey: @"Points"];
        
        [submitPointsCalcArray addObject: submitPointsDict];
    }
    [submitParams setObject: submitPointsCalcArray forKey: @"PointsCalculator"];
    
    self.isSaving = YES;
    [[WebServiceManager sharedInstance] submitPointsCalculator: submitParams vc: self];
}

-(void)processFailed:(WebServiceResponse *)response
{
    self.isSaving = NO;
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_WAYSTOEARN:
            //update points calculator dict
            
            self.calculatorDict = [[response getGenericResponse] objectForKey: @"PointCalculator"];
            self.defaultPointsArray = [[NSMutableArray alloc] initWithArray:[self.calculatorDict objectForKey: @"DefaultGroupPoints"]];
            [self checkForDefault];
            [self.tableView reloadData];
            break;
        case kWEBSERVICE_SUBMITPOINTSCALC:
            //update or reset points
            if (self.isSaving)
            {
                [mAlert showSuccessAlertWithMessage: LOCALIZATION(C_RU_VALUECALC_POPUP_SAVED_BODY) onCompletion:^(BOOL finished) {
                        [[WebServiceManager sharedInstance] fetchWaysToEarnPoints: self];
                }];     //lokalised
            }
            else
            {
                [[WebServiceManager sharedInstance] fetchWaysToEarnPoints: self];
            }
            self.isSaving = NO;
            break;
        default:
            break;
    }
}



- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
