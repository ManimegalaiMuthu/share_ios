//
//  RUValueCalculatorTableViewCell.m
//  Shell
//
//  Created by Jeremy Lua on 13/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "Session.h"
#import "RUValueCalculatorTableViewCell.h"

@interface RUValueCalculatorTableViewCell () <UITextFieldDelegate>

@end


@implementation RUValueCalculatorTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.text = [textField.text stringByReplacingOccurrencesOfString: [NSString stringWithFormat: @" %@", self.unit] withString: @""];
}

- (IBAction)textfieldDidChanged:(UITextField *)sender
{
    self.slider.value = [sender.text intValue];
}

-(void) textFieldDidEndEditing:(UITextField *)textField
{
    self.slider.value = [textField.text intValue];
    
    if ([textField.text intValue] > 0)
        textField.text = [NSString stringWithFormat: @"%@ %@", textField.text, self.unit];
    else if ([textField.text intValue] > self.slider.maximumValue)
        textField.text = [NSString stringWithFormat: @"%.0f %@", self.slider.maximumValue, self.unit];
    else
        textField.text = [NSString stringWithFormat: @"0 %@", self.unit];
}


@end
