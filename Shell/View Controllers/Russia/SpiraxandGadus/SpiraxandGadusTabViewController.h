//
//  SpiraxandGadusTabViewController.h
//  Shell
//
//  Created by MUTHUPANDIYAN Manimegalai on 18/9/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "RDVTabBarController.h"
#import <UIKit/UIKit.h>
#import "Helper.h"

NS_ASSUME_NONNULL_BEGIN

@interface SpiraxandGadusTabViewController : RDVTabBarController

@end

NS_ASSUME_NONNULL_END
