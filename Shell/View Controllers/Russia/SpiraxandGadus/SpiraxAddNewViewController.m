//
//  SpiraxAddNewViewController.m
//  Shell
//
//  Created by MUTHUPANDIYAN Manimegalai on 18/9/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "SpiraxAddNewViewController.h"
#import "CASDatePickerView.h"
#import "QRCodeScanner.h"
#import "WorkingHoursViewController.h"
#import "GalleryUploadViewController.h"
#import "RUCustomerSignatureViewController.h"
#import "PopupWebviewViewController.h"

@interface SpiraxAddNewViewController ()<WorkingHoursViewDelegate, UITextFieldDelegate, WebServiceManagerDelegate, CommonListDelegate, DatePickerDelegate, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, CLLocationManagerDelegate, QRCodeScannerDelegate, GalleryUploadViewDelegate, UITextViewDelegate, RUCustomerSignatureDelegate>

enum kManageWorkshopAddNew_TYPE
{
    kManageWorkshopAddNew_WORKSHOPHEADER,
    kManageWorkshopAddNew_COMPANYSOURCE,
    kManageWorkshopAddNew_ACTIVATIONREMARKS,
    kManageWorkshopAddNew_WORKSHOPTYPE,
    kManageWorkshopAddNew_WORKSHOPCATEGORY,
    kManageWorkshopAddNew_REGWORKSHOPNAME,
    kManageWorkshopAddNew_LOCATION,
    kManageWorkshopAddNew_WORKSHOPQRCODE,
    kManageWorkshopAddNew_WORKSHOPCODE,
    
    kManageWorkshopAddNew_WORKSHOPTELEPHONE,
    kManageWorkshopAddNew_WORKSHOPEMAIL,
    kManageWorkshopAddNew_ADDRESSLINEONE,
    kManageWorkshopAddNew_ADDRESSLINETWO,
    kManageWorkshopAddNew_TOWN,
    kManageWorkshopAddNew_STATE,
    kManageWorkshopAddNew_POSTALCODE,
    kManageWorkshopAddNew_WORKINGHOURS,
    
    kManageWorkshopAddNew_OILCHANGEPERDAY,
    kManageWorkshopAddNew_SYNTHETICPERDAY,
    kManageWorkshopAddNew_SEMISYNTHETICPERDAY,
    kManageWorkshopAddNew_SHELLHELIXPERDAY,
    
    kManageWorkshopAddNew_OWNERHEADER,
    kManageWorkshopAddNew_SALUTATION,
    kManageWorkshopAddNew_FIRSTNAME,
    kManageWorkshopAddNew_LASTNAME,
    kManageWorkshopAddNew_MOBNUM,
    kManageWorkshopAddNew_EMAIL,
    kManageWorkshopAddNew_DOB,
};

#define CELL_HEIGHT 72

#define kUserID             @"UserID"
#define kDSRCode            @"DSRCode"
#define kUserPassword       @"UserPassword"
#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kEmailAddress       @"EmailAddress"
#define kDOB                @"DOB"
#define kDOBMonth           @"DOBMonth"
#define kDOBDay             @"DOBDay"
#define kCountryCode        @"CountryCode"
#define kReferenceCode          @"ReferenceCode"        //Workshop Code
#define kWorkshopQRCode         @"WorkshopQRCode"       //Workshop SHARE Code (for QR Scan)
#define kCompanySource          @"CompanySource"
#define kCompanyType            @"CompanyType"
#define kCompanyCategory            @"CompanyCategory" //to change again?
#define kCompanyName            @"CompanyName"
#define kCompanyEmailAddress    @"CompanyEmailAddress"
#define kCompanyTier            @"CompanyTier"
#define kContactNumber          @"ContactNumber"
#define kOilChangePerMonth      @"OilChangePerMonth"
#define kOilChangePercentage    @"OilChangePercentage"
#define kHalfSyntheticOilChanges    @"HalfSyntheticOilChanges"

#define kShellHelixQty          @"ShellHelixQty"
#define kStatus                 @"Status"
#define kRemarks                @"Remarks"
#define kAddressType    @"AddressType"
#define kAddress1       @"Address1"
#define kAddress2       @"Address2"
#define kStreetName     @"StreetName"
#define kCompanyCity    @"CompanyCity"
#define kCompanyState   @"CompanyState"
#define kPostalCode     @"PostalCode"
#define kLongitude      @"Longitude"
#define kLatitude       @"Latitude"

#define kWorkingHours       @"WorkingHours"
#define kWorkshopLocation   @"Location"

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewToBottom;

@property CASDatePickerView *dateView;

@property (weak, nonatomic) IBOutlet UILabel *lblContactPrefHeader;

@property (weak, nonatomic) IBOutlet UIButton *btnUpload;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;

#pragma mark contact Pref
@property (weak, nonatomic) IBOutlet UILabel *contactPrefLbl;
@property (weak, nonatomic) IBOutlet UICollectionView *colView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *colViewHeight; //to adjust according to number of cells <1 + (count / 2)>
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *colViewLayout; //to adjust cell width size according to screen width
@property NSMutableArray *contactPrefArray;


//@property (strong, nonatomic) IBOutlet MKMapView *mapView;
//@property (weak, nonatomic) IBOutlet UIButton *btnAddCustomer;

@property UIImageView *workshopProfilePicture;

@property NSDictionary *workingHoursDict;
@property NSArray *stateTable;

@property Member *member;
@property CompanyInfo *companyInfo;
@property CompanyAddress *companyAddress;

//@property (weak, nonatomic) IBOutlet UITableView *workingHoursTableView;

@property NSArray *workshopImagesData;

@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@property CLLocationManager *locationManager;

@property NSMutableArray *registrationFormArray; //ui only
@property NSMutableDictionary *regSubmissionDict;  //key as json key, value = cell. retrieve value by using getJson method
@property (weak, nonatomic) IBOutlet UITableView *regFormTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight; //to adjust according number of cells

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *signatureHeight; //0 if not russia
@property (weak, nonatomic) IBOutlet UIView *signatureView;
@property (weak, nonatomic) IBOutlet UILabel *lblSignature;
@property (weak, nonatomic) IBOutlet UIImageView *imgSignature;

@property (weak, nonatomic) IBOutlet UIView *agreeTNC;
@property (weak, nonatomic) IBOutlet UIButton *btnTncCheckbox;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblTnc;

@property (weak, nonatomic) IBOutlet UILabel *lblRequiredFields;

@end

@implementation SpiraxAddNewViewController
@synthesize listView;
@synthesize dateView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

        self.member = [[Member alloc] init];
        self.companyInfo = [[CompanyInfo alloc] init];
        self.companyAddress = [[CompanyAddress alloc] init];
        
        self.member.countryCode = GET_COUNTRY_CODE;
        
        [[WebServiceManager sharedInstance] loadState: [[mSession lookupTable] objectForKey: @"LookUpVersion"]
                                          countryCode: GET_COUNTRY_CODE
                                                   vc: self];
        
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.distanceFilter = kCLDistanceFilterNone;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [self.locationManager requestWhenInUseAuthorization];
        
        NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @"*" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B3, }];
        
        NSMutableAttributedString *requiredFieldsAttrString = [[NSMutableAttributedString alloc] initWithString: LOCALIZATION(C_PROFILE_REQUIREDFIELDS) attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B3, }];   //lokalised
        
        [asteriskAttrString appendAttributedString:  requiredFieldsAttrString];
        
        //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
        self.lblRequiredFields.attributedText = asteriskAttrString;
        
        self.lblSignature.text = LOCALIZATION(C_RUSSIA_CUSTOMERSIGNATURE);          //lokalised
        
        [self.colView registerNib: [UINib nibWithNibName: @"ContactPreferencesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ContactPrefCell"];
        
        if(![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
        {
            self.signatureHeight.constant = 0;
            [self.agreeTNC removeFromSuperview];
        }
        else
        {
            self.lblSignature.text = LOCALIZATION(C_RUSSIA_CUSTOMERSIGNATURE);              //lokalised
            [Helper setHyperlinkLabel:self.lblTnc hyperlinkText: LOCALIZATION(C_RU_STEPFOUR_TNCLINK) hyperlinkFont: FONT_B2 bodyText: LOCALIZATION(C_RU_STEPFOUR_TNC) bodyFont:FONT_B2 urlString: @"WorkshopTNC"];              //lokalised
    //        [Helper addHyperlink:self.lblTnc hyperlinkText: LOCALIZATION(C_RU_STEPFOUR_PRIVACYLINK) hyperlinkFont: FONT_B2 urlString:@"WorkshopPrivacy" bodyText: self.lblTnc.text];
        }
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url absoluteString] isEqualToString: @"WorkshopTNC"])
    {
        NSDictionary *data = GET_WORKSHOP_TNC;
        
        PopupWebviewViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPWEBVIEW];
        
        [popupVc popupInitWithHTMLBody: [data objectForKey: @"TncText"] headerTitle: LOCALIZATION(C_SIDEMENU_TNC) buttonTitle: LOCALIZATION(C_GLOBAL_OK)];  //lokalised
    }
//    else if ([[url absoluteString] isEqualToString: @"WorkshopPrivacy"])
//    {
//        PopupWebviewViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPWEBVIEW];
//
//        NSString *urlAddress = [NSString stringWithFormat:@"%@%@%@?c=%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_PRIVACY_URL, GET_COUNTRY_CODE];
//        [popupVc popupInitWithURL: urlAddress headerTitle: LOCALIZATION(C_TITLE_PRIVACY) buttonTitle: LOCALIZATION(C_GLOBAL_OK)];
//    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    
    self.scrollViewToBottom.constant = 0;
    
    //QRCode Delegate
    mQRCodeScanner.delegate = self;
    [self setupGoogleAnalytics];
}

-(void) setupGoogleAnalytics
{
//    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//    [tracker set: kGAIScreenName value:@"Workshop add new"];
//    [tracker send: [[GAIDictionaryBuilder createScreenView] build]];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    [self setupInterface];
    
//    else
//        self.signatureHeight.constant = 220 * (SCREEN_WIDTH / 320);
//
}

-(void) setupRegistrationForm
{
    self.workshopImagesData = nil;
    
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormTextfieldCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormTextfieldCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationButtonCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormDropdownCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormDropdownCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationHeaderTableViewCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationLabelTextfieldTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationLabelTextfieldTableViewCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationMobileNumberCell" bundle:nil] forCellReuseIdentifier:@"RegistrationMobileNumberCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationTextfieldWithButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextfieldWithButtonCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationTextViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextViewTableViewCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationWorkingHoursTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationWorkingHoursTableViewCell"];
    
    self.regSubmissionDict = [[NSMutableDictionary alloc] init];
    
//    self.registrationFormArray = [[NSMutableArray alloc] initWithArray:
    
#pragma mark - Workshop Profile Setup
    self.registrationFormArray = [[NSMutableArray alloc] init];

    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_HEADER_WORKSHOPPROFILE),      //lokalised
                                      @"Type": @(kREGISTRATION_HEADER),
                                           }];
  
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_COMPANYSOURCE),       //lokalised
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key": kCompanySource,
                                      @"Value": LOCALIZATION(C_PROFILE_COMPANYSOURCE_PLACEHOLDER),  //lokalised
                                      }];
//                                    @{@"Title": LOCALIZATION(C_PROFILE_ACTIVATIONREMARKS),
//                                      @"Placeholder": LOCALIZATION(C_PROFILE_ACTIVATIONREMARKS_PLACEHOLDER),
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_ADDNEWREMARKS),       //lokalised
                                      @"Placeholder": LOCALIZATION(C_PROFILE_ADDNEWREMARKS),        //lokalised
                                      @"Type": @(kREGISTRATION_TEXTVIEW),
                                      @"Key": kRemarks,
                                      @"Value": @"",
                                             @"Optional": @(YES),
                                           }];   //activation remarks
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_WORKSHOPTYPE),        //lokalised
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key": kCompanyType,
                                      @"Value": LOCALIZATION(C_PROFILE_WORKSHOPTYPE_PLACEHOLDER),   //lokalised
                                           }];

    {
      [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_WORKSHOPCATEGORY), //loklaised
                                               @"Type": @(kREGISTRATION_DROPDOWN),
                                               @"Key": kCompanyCategory,
                                               @"Value": LOCALIZATION(C_PROFILE_WORKSHOPCATEGORY), //loklaised
                                               }];
    }
  
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_COMPANYNAME),        //lokalised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kCompanyName,
                                      @"Value": @"",
                                           }]; //company name
    
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_LOCATION),            //lokalised
                                      @"Type": @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                                      @"TextfieldEnabled": @(NO),
                                      @"Icon": @"icon-location",
                                      @"Key": kWorkshopLocation,
                                      @"Value": LOCALIZATION(C_PROFILE_LOCATION),                   //lokalised
//                                             @"Optional": @(YES), //Made Required Globally after VN requirement
                                           }]; //location
    
    if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_VIETNAM]) {
        [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_WORKSHOPSHARECODE), //lokalised
                                                 @"Type": @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                                                 @"Icon": @"icon-code",
                                                 @"Key": kWorkshopQRCode,
                                                 @"Value": self.companyInfo.workshopQRCode,
                                                 @"Enabled": @(YES),
                                                 @"Optional":@(NO),
                                                 }];   //Workshop SHARE Code
    } else {
        [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_WORKSHOPSHARECODE), //lokalised
                                                 @"Type": @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                                                 @"Icon": @"icon-code",
                                                 @"Key": kWorkshopQRCode,
                                                 @"Value": self.companyInfo.workshopQRCode,
                                                 @"Enabled": @(YES),
                                                 @"Optional":@(YES),
                                                 }];   //Workshop SHARE Code
    }
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_WORKSHOPCODE),            //lokalised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kReferenceCode,
                                      @"Value": @"",
                                             @"Optional": @(![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA]),
                                           }];   //Workshop Code
    
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_CONTACTNUMBER),           //lokalised
                                      @"Placeholder": LOCALIZATION(C_PROFILE_CONTACTNUMBER_PLACEHOLDER),//lokalised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Keyboard": @(UIKeyboardTypeNumberPad),
                                      @"Key": kContactNumber,
                                     @"Optional": @(![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA]),
                                      @"Value": @"",
                                           }]; //last name
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_WORKSHOPEMAILADDRESS),    //loklaised
                                      @"Placeholder": LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER), //loklaised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Keyboard": @(UIKeyboardTypeEmailAddress),
                                      @"Key": kCompanyEmailAddress,
                                      @"Value": @"",
                                             @"Optional": @(YES),
                                           }]; //last name
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_ADDRESSLINE1),            //loklaised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kAddress1,
                                      @"Value": @"",
                                           }]; //last name
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_ADDRESSLINE2),            //loklaised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kAddress2,
                                      @"Value": @"",
                                             @"Optional": @(YES),
                                             }];
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_TOWN),                    //loklaised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kCompanyCity,
                                      @"Value": @"",
                                         @"Optional": @(![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA]),
                                             }];
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_STATE),                   //loklaised
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key": kCompanyState,
                                      @"Value": LOCALIZATION(C_PROFILE_STATE_PLACEHOLDER),              //loklaised
                                           }];
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_POSTALCODE),              //loklaised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kPostalCode,
                                      @"Keyboard": @(UIKeyboardTypeNumberPad),
                                      @"Value": @"",
                                             }];
  
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_WORKINGHOURS),            //loklaised
//                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Type": @(kREGISTRATION_WORKINGHOURS),
//                                      @"Enabled": @(YES),
                                      @"Key": kWorkingHours,
//                                      @"Value": LOCALIZATION(C_PROFILE_WORKINGHOURS),
                                             }];
                                    
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_OILCHANGEPERDAY),         //loklaised
                                      @"Placeholder": LOCALIZATION(C_PROFILE_ANSWER),                   //loklaised
                                      @"Type": @(kREGISTRATION_LABELWITHTEXTFIELD),
                                      @"Key": kOilChangePerMonth,
                                      @"Keyboard": @(UIKeyboardTypeNumberPad),
                                      @"Value": @"",
                                             }];
  
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_OILCHANGEPERCENT),        //loklaised
                                      @"Placeholder": LOCALIZATION(C_PROFILE_ANSWER),                   //loklaised
                                      @"Type": @(kREGISTRATION_LABELWITHTEXTFIELD),
                                      @"Key": kOilChangePercentage,
                                      @"Keyboard": @(UIKeyboardTypeDecimalPad),
                                      @"Value": @"",
                                             }];
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_SEMIOILCHANGEPERCENT),     //loklaised
                                      @"Placeholder": LOCALIZATION(C_PROFILE_ANSWER),                    //loklaised
                                      @"Type": @(kREGISTRATION_LABELWITHTEXTFIELD),
                                      @"Key": kHalfSyntheticOilChanges,
                                      @"Keyboard": @(UIKeyboardTypeDecimalPad),
                                      @"Value": @"",
                                             }];
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_SHELLHELIX),               //loklaised
                                      @"Placeholder": LOCALIZATION(C_PROFILE_ANSWER),                    //loklaised
                                      @"Type": @(kREGISTRATION_LABELWITHTEXTFIELD),
                                      @"Key": kShellHelixQty,
                                      @"Keyboard": @(UIKeyboardTypeNumberPad),
                                      @"Value": @"",
                                             }];
  
#pragma mark - Owner Profile Form setup
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_HEADER_OWNERPROFILE),             //loklaised
                                      @"Type": @(kREGISTRATION_HEADER),
                                             }];
  
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_SALUTATION),              //loklaised
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key": kSalutation,
                                      @"Value": LOCALIZATION(C_PROFILE_SALUTATION_PLACEHOLDER),         //loklaised
                                             @"Optional": @(YES),
                                             }];  //button
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_FIRSTNAME),               //loklaised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kFirstName,
                                      @"Value": @"",
                                             }]; //first name
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_LASTNAME),                //loklaised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kLastName,
                                      @"Value": @"",
                                             }]; //last name
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_DOB),                     //loklaised
                                      @"Type": @(kREGISTRATION_BIRTHDATE),
                                      @"Key": kDOB,
                                      @"Value": LOCALIZATION(C_PROFILE_DOB_PLACEHOLDER),                //loklaised
                                             @"Optional": @(YES),
                                             }];   //Date of birth
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_MOBILENUM),               //loklaised
                                      @"Type": @(kREGISTRATION_MOBNUM),
                                      @"Key": kMobileNumber,
                                      @"CountryCode": [mSession convertCountryCodeToCountryName: GET_COUNTRY_CODE],
                                      @"TextfieldEnabled": @(YES),
                                      @"MobileNumber": @"",
                                             }];  //Mobile number
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_OWNEREMAILADDRESSOPT),    //loklaised
                                      @"Placeholder": LOCALIZATION(C_PROFILE_EMAILADDRESS_PLACEHOLDER), //loklaised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kEmailAddress,
                                      @"Keyboard": @(UIKeyboardTypeEmailAddress),
                                      @"Value": @"",
                                             @"Optional": @(YES),
                                             }]; //email
  
    //set tableview height. 5 shorter cells
    self.tableViewHeight.constant = CELL_HEIGHT * [self.registrationFormArray count] - ((CELL_HEIGHT - 50) * 5) + (73);
    self.regFormTableView.tableFooterView = [UIView new];
    [self.regFormTableView reloadData];

    //reset
    
    switch ([CLLocationManager authorizationStatus])
    {
        case kCLAuthorizationStatusNotDetermined:
            [self.locationManager requestWhenInUseAuthorization];
            break;
        default:
            break;
    }
}

-(void) setupInterface
{
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_DROPDOWN_SALUTATION)            //loklaised
                                                   list: [mSession getSalutations]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
    
    dateView = [[CASDatePickerView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_BIRTHDAY) previouslySelected:nil withYearHidden:YES]; //loklaised
    
    dateView.delegate = self;
    
    
    [self.lblContactPrefHeader setFont: FONT_B1];
    self.lblContactPrefHeader.text = LOCALIZATION(C_PROFILE_CONTACT_PREFERENCE);                        //loklaised
    if (!self.workingHoursDict)
        self.workingHoursDict = @{@"WorkHourStart1":   @"00:00",
                                  @"WorkHourEnd1":     @"23:00",
                                  @"WeekDays1":        @"0",
                                  @"PublicHoliday":    @"0",
                                  @"WorkHourStart2":   @"00:00",
                                  @"WorkHourEnd2":     @"23:00",
                                  @"WeekDays2":        @"0",
                                  };
    
    //misc.
    [self.btnUpload.titleLabel setFont: FONT_BUTTON];
    [self.btnUpload setTitle:LOCALIZATION(C_FORM_UPLOADWORKSHOPIMAGE) forState:UIControlStateNormal];   //loklaised
    
    [self.btnRegister.titleLabel setFont: FONT_BUTTON];
    [self.btnRegister setTitle:LOCALIZATION(C_FORM_REGISTER) forState:UIControlStateNormal];            //loklaised
    
    //handle check boxes collection view
    if (!self.contactPrefArray)
        self.contactPrefArray = [[NSMutableArray alloc] init];
    CGFloat colviewHeight = (COLVIEW_CELLHEIGHT + COLVIEW_LINESPACING) * (1 + ([[mSession getContactPreference] count] / 2));
    self.colViewHeight.constant = colviewHeight;
    
    CGFloat cellWidth = (self.colView.frame.size.width / 2) - 10; //10 = cell spacing, check IB
    self.colViewLayout.itemSize = CGSizeMake(cellWidth, 50);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[mSession getContactPreference] count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ContactPreferencesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"ContactPrefCell" forIndexPath: indexPath];
    
    UIButton *checkboxBtn = [cell viewWithTag: 1]; //tagged in storyboard
    checkboxBtn.selected  = YES;
    
    UILabel *contactPref = [cell viewWithTag: 2]; //tagged in storyboard
    contactPref.text = [[mSession getContactPreference] objectAtIndex: [indexPath row]];
    contactPref.font = FONT_B1;
    
    cell.bitValue = [[mSession convertToContactPreferenceKeyCode: contactPref.text] intValue];
    
    if (!self.contactPrefArray)
        self.contactPrefArray = [[NSMutableArray alloc] init];
    [self.contactPrefArray addObject: cell];
    
    return cell;
}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}
- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

- (IBAction)checkBoxSelected:(UIButton *)sender {
    if ([sender isSelected])
        sender.selected = NO;
    else
        sender.selected = YES;
}

- (IBAction)workingHoursPressed:(id)sender {
    
    if (!self.workingHoursDict ||
        [self.workingHoursDict isKindOfClass: [NSNull class]])
        self.workingHoursDict = @{@"WorkHourStart1":   @"0:00",
                                  @"WorkHourEnd1":     @"0:00",
                                  @"WeekDays1":        @"",
                                  @"PublicHoliday":    @"0",
                                  @"WorkHourStart2":   @"0:00",
                                  @"WorkHourEnd2":     @"0:00",
                                  @"WeekDays2":        @"",
                                  };
    
    //let tab bar push instead
    [mSession pushWorkingHoursView: self.navigationController.parentViewController workingHoursData: self.workingHoursDict];
}


-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict
{
    //assign working hours
    self.workingHoursDict = workingHoursDict;
    
    [self.regFormTableView reloadRowsAtIndexPaths: @[[NSIndexPath indexPathForRow: kManageWorkshopAddNew_WORKINGHOURS inSection:0]] withRowAnimation: UITableViewRowAnimationNone];
    
    NSArray *fullWorkWeekDays;
    NSArray *halfWorkWeekDays;
    
    if (![[workingHoursDict objectForKey: @"WeekDays1"] isEqualToString: @""])
        fullWorkWeekDays = [[workingHoursDict objectForKey: @"WeekDays1"] componentsSeparatedByString: @","];
    
    if (![[workingHoursDict objectForKey: @"WeekDays2"] isEqualToString: @""])
        halfWorkWeekDays = [[workingHoursDict objectForKey: @"WeekDays2"] componentsSeparatedByString: @","];
    
    self.tableViewHeight.constant = CELL_HEIGHT * [self.registrationFormArray count] - ((CELL_HEIGHT - 50) * 5) + (73) + (44 * ([fullWorkWeekDays count] + [halfWorkWeekDays count]));
//    [self.workingHoursTableView reloadData];
}

- (IBAction)registerPressed:(id)sender {
    
    self.member.registrationType     = @"1"; //"1" for DSR-assisted trade registration
    
    self.member.firstName =   [[self.regSubmissionDict objectForKey: kFirstName] getJsonValue];
    self.member.lastName =    [[self.regSubmissionDict objectForKey: kLastName] getJsonValue];
    self.member.mobileNumber = [[self.regSubmissionDict objectForKey: kMobileNumber] getMobileNumberJsonValue];
    self.member.emailAddress = [[self.regSubmissionDict objectForKey: kEmailAddress] getJsonValue];
    
    self.member.countryCode  = GET_COUNTRY_CODE;
    self.member.userID       = [[self.regSubmissionDict objectForKey: kMobileNumber] getMobileNumberJsonValue];
    
    self.member.DSRCode      = @"";
    
    NSString *contactPrefString = @"";
    for (ContactPreferencesCollectionViewCell* cell in self.contactPrefArray)
    {
        if ([cell.checkboxBtn isSelected])
        {
            if (contactPrefString.length == 0)
                contactPrefString = @(cell.bitValue).stringValue;
            else
                contactPrefString = [contactPrefString stringByAppendingString: [NSString stringWithFormat: @",%@", @(cell.bitValue).stringValue]];
        }
    }
    self.member.contactPreference    = contactPrefString;
    self.member.registrationMode     = @"1"; //should be defaulted depending on registration method
    self.member.registrationRole    = @"1";
    
    self.companyInfo.referenceCode          = @"";
    self.companyInfo.companySource         = [mSession convertToCompanySourceKeyCode: [[self.regSubmissionDict objectForKey: kCompanySource] getJsonValue]];
    self.companyInfo.workshopQRCode         = [[self.regSubmissionDict objectForKey: kWorkshopQRCode] getJsonValue];
    self.companyInfo.referenceCode         = [[self.regSubmissionDict objectForKey: kReferenceCode] getJsonValue];
    self.companyInfo.companyType            = [[mSession convertToCompanyTypesKeyCode: [[self.regSubmissionDict objectForKey: kCompanyType] getJsonValue]] intValue];
    if ([[[mSession lookupTable] objectForKey: @"IsShowCompanyCategory"] boolValue])
    {
        self.companyInfo.companyCategory            = [[mSession convertToCompanyCategoryKeyCode: [[self.regSubmissionDict objectForKey: kCompanyCategory] getJsonValue]] intValue];
        if (!self.companyInfo.companyCategory)
            self.companyInfo.companyCategory = 0;
    }
    
    self.companyInfo.companyName            = [[self.regSubmissionDict objectForKey: kCompanyName] getJsonValue];
    self.companyInfo.companyEmailAddress    = [[self.regSubmissionDict objectForKey: kCompanyEmailAddress] getJsonValue];
    self.companyInfo.companyTier            = @"1";
    self.companyInfo.contactNumber          = [[self.regSubmissionDict objectForKey: kContactNumber] getJsonValue];
    self.companyInfo.status                 = 4;
    self.companyInfo.remarks                = [[self.regSubmissionDict objectForKey: kRemarks] getJsonValue];
    
    
    self.companyInfo.oilChangePerMonth        = [[self.regSubmissionDict objectForKey: kOilChangePerMonth] getJsonValue];
    self.companyInfo.oilChangePercentage    = [[self.regSubmissionDict objectForKey: kOilChangePercentage] getJsonValue];
    self.companyInfo.halfSyntheticOilChanges    = [[self.regSubmissionDict objectForKey: kHalfSyntheticOilChanges] getJsonValue];
    self.companyInfo.shellHelixQty     = [[self.regSubmissionDict objectForKey: kShellHelixQty] getJsonValue];
    
    
    self.companyAddress.addressType     = 0;
    self.companyAddress.address1        = [[self.regSubmissionDict objectForKey: kAddress1] getJsonValue];
    self.companyAddress.address2        = [[self.regSubmissionDict objectForKey: kAddress2] getJsonValue];
    self.companyAddress.streetName      = @"";
    self.companyAddress.companyCity     = [[self.regSubmissionDict objectForKey: kCompanyCity] getJsonValue];
    
    NSString *stateString = [[self.regSubmissionDict objectForKey: kCompanyState] getJsonValue];
    self.companyAddress.companyState    = [mSession convertToStateKeyCode: stateString stateDetailedListArray:self.stateTable];
    self.companyAddress.postalCode      = [[self.regSubmissionDict objectForKey: kPostalCode] getJsonValue];
    
    NSArray *companyService = @[@{@"ServiceID": @"1"},
                                @{@"ServiceID": @"2"},
                                ];
    
    
//    NSDictionary *registerParams =
//    @{@"CountryCode":            self.member.countryCode,
//                                     @"TermCondition":          @(YES),
//                                     @"REG_Company":            [self.companyInfo getJSON],
//                                     @"REG_CompanyAddress":     [self.companyAddress getJSON],
//                                     @"REG_CompanyService":     companyService,
//                                     @"REG_CompanyWorkHours":   self.workingHoursDict,
//                                     @"REG_Member":             [self.member getJSON],
//                                     };

    NSMutableDictionary *registerParams = [[NSMutableDictionary alloc] init];
    
    [registerParams setObject:self.member.countryCode forKey: @"CountryCode"];
    
    if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
    {
        [registerParams setObject:@(self.btnTncCheckbox.selected) forKey: @"TermCondition"];
    }
    else
        [registerParams setObject:@(YES) forKey: @"TermCondition"];
    
    [registerParams setObject:[self.companyInfo getJSON] forKey: @"REG_Company"];
    [registerParams setObject:[self.companyAddress getJSON] forKey: @"REG_CompanyAddress"];
    [registerParams setObject:companyService forKey: @"REG_CompanyService"];
    [registerParams setObject:self.workingHoursDict forKey: @"REG_CompanyWorkHours"];
    [registerParams setObject:[self.member getJSON] forKey: @"REG_Member"];
    
    if (self.imgSignature.image)
    {
        //set image
        [registerParams setObject: [UIImageJPEGRepresentation(self.imgSignature.image, 0.5f) base64EncodedStringWithOptions:0]  forKey: @"DigitalSignature"];
    }
    
    [[WebServiceManager sharedInstance] addNewWorkshop:registerParams
                                                    vc:self];
}

- (IBAction)signaturePressed:(id)sender
{
    [mSession pushRUCustomerSignatureWithDelegate: self.parentViewController.parentViewController delegate: self];
}

-(void)didFinishSignature:(UIImage *)image
{
    self.imgSignature.image = image;
}



#pragma mark - tableview delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.registrationFormArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *regFieldData = [self.registrationFormArray objectAtIndex: indexPath.row];
    switch ([[regFieldData objectForKey: @"Type"] intValue])
    {
        case kREGISTRATION_TEXTFIELD:
        {
            RegistrationFormTextfieldCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormTextfieldCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];

            
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [textfieldCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [textfieldCell hideTopLabel: NO];
            
            
            
            [textfieldCell  setTextfieldDelegate: self];
            
            if ([regFieldData objectForKey: @"Value"])
                [textfieldCell setTextfieldText: [regFieldData objectForKey: @"Value"]];
            if ([regFieldData objectForKey: @"Placeholder"])
                [textfieldCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_DROPDOWN:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            if (indexPath.row == kManageWorkshopAddNew_WORKINGHOURS)
                [dropdownCell hideTopLabel: YES];
            else
            {
                if ([[regFieldData objectForKey: @"Optional"] boolValue])
                {
                    [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
                }
                else
                {
                    NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                    
                    NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                    
                    [titleAttrString appendAttributedString:  asteriskAttrString];
                    
                    //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                    [dropdownCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
                }
                [dropdownCell hideTopLabel: NO];
            }
            dropdownCell.button.tag = indexPath.row;
            
            [dropdownCell.button addTarget: self action: @selector(dropdownPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            //set initial value
            if ([regFieldData objectForKey: @"Value"])
                [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            
            //keep a reference of cell to submit json value later
            [self.regSubmissionDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        case kREGISTRATION_TEXTFIELDWITHBUTTON:
        {
            RegistrationTextfieldWithButtonCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationTextfieldWithButtonCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            if ([regFieldData objectForKey: @"TextfieldEnabled"])
                [textfieldCell setTextfieldEnabled: [[regFieldData objectForKey: @"TextfieldEnabled"] boolValue]];
            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [textfieldCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            
            [textfieldCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [textfieldCell hideTopLabel: NO];
            [textfieldCell  setTextfieldDelegate: self];
            
            if ([[regFieldData objectForKey: @"Key"] isEqualToString: kWorkshopQRCode])
            {
                [textfieldCell.button addTarget:self action:@selector(btnScanDSRCode:) forControlEvents:UIControlEventTouchUpInside];
            }
            else if ([[regFieldData objectForKey: @"Key"] isEqualToString: kWorkshopLocation])
            {
                
                [textfieldCell.button addTarget:self action:@selector(locationPressed:) forControlEvents:UIControlEventTouchUpInside];
            }
//            switch (indexPath.row)
//            {
//                case kManageWorkshopAddNew_WORKSHOPQRCODE:
//                    [textfieldCell.button addTarget:self action:@selector(btnScanDSRCode:) forControlEvents:UIControlEventTouchUpInside];
//                    break;
//                case kManageWorkshopAddNew_LOCATION:
//                    [textfieldCell.button addTarget:self action:@selector(locationPressed:) forControlEvents:UIControlEventTouchUpInside];
//                    break;
//            }
            
            if ([regFieldData objectForKey: @"Value"])
                [textfieldCell setTextfieldText: [regFieldData objectForKey: @"Value"]];
            
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_BUTTON:
        {
            RegistrationButtonCell *buttonCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationButtonCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [buttonCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [buttonCell setTitle: [regFieldData objectForKey: @"Title"]];
            [buttonCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [buttonCell hideTopLabel: NO];
            
            [self.regSubmissionDict setObject: buttonCell forKey: [regFieldData objectForKey: @"Key"]];
            return buttonCell;
        }
            break;
        case kREGISTRATION_MOBNUM:
        {
            RegistrationMobileNumberCell *mobNumCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationMobileNumberCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [mobNumCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            if ([regFieldData objectForKey: @"TextfieldEnabled"])
                [mobNumCell setTextfieldEnabled: [[regFieldData objectForKey: @"TextfieldEnabled"] boolValue]];
            [mobNumCell setMobileCodeEnabled: NO];
            [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];

            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
                [mobNumCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [mobNumCell hideTopLabel: NO];
            [mobNumCell  setTextfieldDelegate: self];
            
            [mobNumCell setCountryCode: [regFieldData objectForKey: @"CountryCode"]];
            [mobNumCell setMobileNumber: [regFieldData objectForKey: @"MobileNumber"]];
            
            [self.regSubmissionDict setObject: mobNumCell forKey: [regFieldData objectForKey: @"Key"]];
            return mobNumCell;
        }
            break;
        case kREGISTRATION_LABELWITHTEXTFIELD:
        {
            RegistrationLabelTextfieldTableViewCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationLabelTextfieldTableViewCell"];

            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [textfieldCell setLeftTitle: [regFieldData objectForKey: @"Title"]];
            [textfieldCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
//            [textfieldCell hideTopLabel: NO];
            [textfieldCell  setTextfieldDelegate: self];
            
            //set initial value
            if ([regFieldData objectForKey: @"Value"])
                [textfieldCell setTextfieldText: [regFieldData objectForKey: @"Value"]];
            
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_BIRTHDATE:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];

            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            [dropdownCell hideTopLabel: NO];
            dropdownCell.button.tag = indexPath.row;
            
            [dropdownCell.button addTarget: self action: @selector(birthdayPressed:) forControlEvents:UIControlEventTouchUpInside];
            [self.regSubmissionDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        case kREGISTRATION_TEXTVIEW:
        {
            RegistrationTextViewTableViewCell *textviewCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationTextViewTableViewCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textviewCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textviewCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            
            //see if Enabled key is available, if not see if pending or approved
            if ([regFieldData objectForKey: @"Enabled"])
                [textviewCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [textviewCell setTitle: [regFieldData objectForKey: @"Title"]];
            [textviewCell hideTopLabel: NO];
            [textviewCell setTextviewDelegate: self];
            
            if ([regFieldData objectForKey: @"Value"])
                [textviewCell setTextviewText: [regFieldData objectForKey: @"Value"]];
            
            if ([regFieldData objectForKey: @"Placeholder"])
                [textviewCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
            
            [self.regSubmissionDict setObject: textviewCell forKey: [regFieldData objectForKey: @"Key"]];
            return textviewCell;
        }
            break;
        case kREGISTRATION_HEADER:
        {
            RegistrationHeaderTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationHeaderTableViewCell"];
            [headerCell setTitle: [regFieldData objectForKey: @"Title"]];
            return headerCell;
        }
            break;
        case kREGISTRATION_WORKINGHOURS:
        {
            RegistrationWorkingHoursTableViewCell *workingHoursCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationWorkingHoursTableViewCell"];
            
            [workingHoursCell.button addTarget: self action: @selector(workingHoursPressed:) forControlEvents:UIControlEventTouchUpInside];
            workingHoursCell.workingHoursDict = self.workingHoursDict;
            
            [workingHoursCell reloadCellData];
            return workingHoursCell;
        }
            break;
        default:
            NSLog(@"UNKNOWN CELL TYPE");
            return [UITableViewCell new];
            break;
    }
    return [UITableViewCell new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *regFieldData = [self.registrationFormArray objectAtIndex: indexPath.row];
    if(indexPath.row == kManageWorkshopAddNew_OILCHANGEPERDAY ||
       indexPath.row == kManageWorkshopAddNew_SYNTHETICPERDAY ||
       indexPath.row == kManageWorkshopAddNew_SEMISYNTHETICPERDAY ||
       indexPath.row == kManageWorkshopAddNew_SHELLHELIXPERDAY)
        return 71; //special cell
    else if (indexPath.row == kManageWorkshopAddNew_WORKINGHOURS)
    {
        NSArray *fullWorkWeekDays;
        NSArray *halfWorkWeekDays;
        
        if (![[self.workingHoursDict objectForKey: @"WeekDays1"] isEqualToString: @""])
            fullWorkWeekDays = [[self.workingHoursDict objectForKey: @"WeekDays1"] componentsSeparatedByString: @","];
        
        if (![[self.workingHoursDict objectForKey: @"WeekDays2"] isEqualToString: @""])
            halfWorkWeekDays = [[self.workingHoursDict objectForKey: @"WeekDays2"] componentsSeparatedByString: @","];
        
        return 44 + 44 * ([fullWorkWeekDays count] + [halfWorkWeekDays count]);
    }
    else if (indexPath.row == kManageWorkshopAddNew_ACTIVATIONREMARKS)
    {
        return 145;
    }
    else if (indexPath.row == kManageWorkshopAddNew_WORKSHOPCATEGORY)
    {
        if ([[[mSession lookupTable] objectForKey: @"IsShowCompanyCategory"] boolValue])
        {
            return CELL_HEIGHT;
        }
        else
        {
            return 0;
        }
    }
    else
        return CELL_HEIGHT;
}


-(void) dropdownPressed:(UIButton *)sender
{
    [self.view endEditing: YES];
    listView.tag = sender.tag;
    
    switch (listView.tag)
    {
        case kManageWorkshopAddNew_COMPANYSOURCE:
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_COMPANYSOURCE) list: [mSession getCompanySource] hasSearchField: NO];        //loklaised
            break;
        case kManageWorkshopAddNew_WORKSHOPTYPE:
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_WORKSHOPTYPE) list: [mSession getCompanyTypes] hasSearchField: NO];          //loklaised
            break;
        case kManageWorkshopAddNew_WORKSHOPCATEGORY:
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_WORKSHOPCATEGORY) list: [mSession getCompanyCategory] hasSearchField: NO];   //loklaised
            break;
        case kManageWorkshopAddNew_STATE:
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_STATE) list: [mSession getStateNames: self.stateTable] hasSearchField: YES]; //loklaised
            break;
        case kManageWorkshopAddNew_SALUTATION:
            [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO];             //loklaised
            break;
//        case kManageWorkshopAddNew_WORKINGHOURS:
//            [self workingHoursPressed: nil];
//            return;
//            break;
        default:
            break;
    }
    [self popUpView];
}

- (IBAction)birthdayPressed:(UIButton *)sender {
    [self.view endEditing: YES];
    
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}

- (void)selectedDate:(NSDate *)selectedDate
{
    //do smething
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //   for API
    [formatter setDateFormat:@"dd"];
    self.member.dobDay = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"MM"];
    self.member.dobMonth = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"dd MMMM"];
    
    [formatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    NSString *selectedDateString = [formatter stringFromDate:selectedDate];
    
    
    RegistrationFormDropdownCell *cell = [self.regSubmissionDict objectForKey: kDOB];
    [cell setSelectionTitle: selectedDateString];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        RegistrationFormDropdownCell *cell;
        switch (listView.tag)
        {
            case kManageWorkshopAddNew_COMPANYSOURCE:
                cell = [self.regSubmissionDict objectForKey: kCompanySource];
                break;
            case kManageWorkshopAddNew_WORKSHOPTYPE:
                cell = [self.regSubmissionDict objectForKey: kCompanyType];
                break;
            case kManageWorkshopAddNew_STATE:
                cell = [self.regSubmissionDict objectForKey: kCompanyState];
                break;
            case kManageWorkshopAddNew_WORKSHOPCATEGORY:
                cell = [self.regSubmissionDict objectForKey: kCompanyCategory];
                break;
            case kManageWorkshopAddNew_SALUTATION:
                cell = [self.regSubmissionDict objectForKey: kSalutation];
                break;
            default:
                return;
                break;
        }
        [cell.button setTitle:selection.firstObject forState:UIControlStateNormal];
    }
}

- (IBAction)uploadImagePressed:(id)sender {
    [mSession pushGalleryView: nil isApproved: NO vc: self.navigationController.parentViewController delegate: self];
}

-(void)didFinishUpload:(NSArray *)imageDataArray
{
    self.workshopImagesData = imageDataArray;
}


- (IBAction)btnScanDSRCode:(id)sender {
    [mQRCodeScanner showZBarScannerOnVC: self];
}

- (void)qrCodeScanSuccess:(NSString *)scanString
{
    RegistrationTextfieldWithButtonCell *cell = [self.regSubmissionDict objectForKey: kWorkshopQRCode];
    [cell setTextfieldText: [[scanString componentsSeparatedByString:@"="] lastObject]];
    NSLog(@"scan success ---------------------");
}

- (IBAction)checkboxPressed:(UIButton *)sender
{
    sender.selected = !sender.selected;
}

- (IBAction)locationPressed:(id)sender {
    [UpdateHUD addMBProgress: self.view withText: @""];
    switch ([CLLocationManager authorizationStatus])
    {
        case kCLAuthorizationStatusAuthorizedAlways:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.locationManager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusNotDetermined:
            [UpdateHUD removeMBProgress: self];
            [self.locationManager requestWhenInUseAuthorization];
            break;
        case kCLAuthorizationStatusDenied:
        {
            [UpdateHUD removeMBProgress: self];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_ALERTVIEW_ERROR) message:LOCALIZATION(C_ALERTVIEW_PERMISSIONERRORMSG) preferredStyle:UIAlertControllerStyleAlert];         //loklaised
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_CANCEL) style:UIAlertActionStyleCancel handler:nil];
            UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:LOCALIZATION(C_ALERTVIEW_SETTING) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                            UIApplicationOpenSettingsURLString]];
            }];         //loklaised
            
            [alertController addAction:cancelAction];
            [alertController addAction:settingsAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
            break;
        case kCLAuthorizationStatusRestricted:
            [UpdateHUD removeMBProgress: self];
            break;
    }

}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [UpdateHUD removeMBProgress: self];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    //most recent location update is at the end of the array.
    CLLocation *location = [locations lastObject];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:6];
    
    NSString *newLatitude   = [formatter stringFromNumber: @(location.coordinate.latitude)];
    NSString *newLongitude  = [formatter stringFromNumber: @(location.coordinate.longitude)];
    
    RegistrationTextfieldWithButtonCell *cell = [self.regSubmissionDict objectForKey: kWorkshopLocation];
    [cell setTextfieldText: [NSString stringWithFormat: @"%@, %@", newLatitude, newLongitude]];
    
    self.companyAddress.latitude = newLatitude.doubleValue;
    self.companyAddress.longitude = newLongitude.doubleValue;
    
    [self.locationManager stopUpdatingLocation];
    [UpdateHUD removeMBProgress: self];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
            SET_LOADSTATE([[response getGenericResponse] objectForKey: @"StateDetailsList"]);
            self.stateTable = [[response getGenericResponse] objectForKey: @"StateDetailsList"];
            
            //have to get state table before setting up form
            [self setupRegistrationForm];
            break;
        case kWEBSERVICE_ADDNEWWORKSHOP:
        {
            if ([self.workshopImagesData count] > 0)
            {
                [[WebServiceManager sharedInstance] uploadGalleryPhotos: [response getGenericResponse] isApproved:NO dataFiles:self.workshopImagesData vc: self];
            }
            else //if no image, skip uploading and show success
            {
                [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                    [self setupRegistrationForm];
                    self.imgSignature.image = nil;
                }];
            }
        }
            break;
        case kWEBSERVICE_UPLOADGALLERYPHOTOS:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                [self setupRegistrationForm];
            }];
        }
            break;
        default:
            break;
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress: KEY_WINDOW];
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
            self.stateTable = GET_LOADSTATE;
            
            [self setupRegistrationForm];
            break;
        default:
            break;
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.scrollViewToBottom.constant = 0;
}

-(void)keyboardOnScreen:(NSNotification *)notification
{
    [super keyboardOnScreen: notification];
    if (!listView.superview)
        self.scrollViewToBottom.constant = self.keyboardFrame.size.height;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    self.scrollViewToBottom.constant = self.keyboardFrame.size.height;
    RegistrationTextViewTableViewCell *textViewCell = [self.regSubmissionDict objectForKey: kRemarks];
    
    [textViewCell didBeginEditing];
    if (self.mainScrollView.contentOffset.y < textViewCell.frame.origin.y)
        [UIView animateWithDuration:0.25f animations:^{
            self.mainScrollView.contentOffset = CGPointMake(0, textViewCell.frame.origin.y);
        }];
    else
        [self.mainScrollView scrollRectToVisible: textViewCell.frame animated:YES];
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    self.scrollViewToBottom.constant = 0;
    RegistrationTextViewTableViewCell *textViewCell = [self.regSubmissionDict objectForKey: kRemarks];
    
    [textViewCell didEndEditing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
