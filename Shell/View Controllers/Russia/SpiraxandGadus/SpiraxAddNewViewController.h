//
//  SpiraxAddNewViewController.h
//  Shell
//
//  Created by MUTHUPANDIYAN Manimegalai on 18/9/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "TradeRegistrationViewController.h"
#import "BaseVC.h"

@protocol WorkshopAddNewDelegate <NSObject>
-(void)didRegisterWorkshop;
@end

NS_ASSUME_NONNULL_BEGIN

@interface SpiraxAddNewViewController : BaseVC

@property id<WorkshopAddNewDelegate> delegate;
-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict;

@end

NS_ASSUME_NONNULL_END
