//
//  SpiraxListViewController.m
//  Shell
//
//  Created by MUTHUPANDIYAN Manimegalai on 18/9/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "SpiraxListViewController.h"
#import "SpiraxListTableViewCell.h"
#import "PopupInfoTableViewController.h"
#import "ManageWorkshopTabViewController.h"

enum kManageWorkshop_Tab
{
    kManageWorkshop_Pending,
    kManageWorkshop_Approved,
    kManageWorkshop_New,
};

enum kManageWorkshop_Range
{
    kManageWorkshop_All,
    kManageWorkshop_AtoF,
    kManageWorkshop_GtoL,
    kManageWorkshop_MtoZ,
};


enum kManageWorkshop_WorkshopStatus
{
    kManageWorkshop_WorkshopStatus_ACTIVE = 1,
    kManageWorkshop_WorkshopStatus_LAPSED = 1 << 1,
    kManageWorkshop_WorkshopStatus_INACTIVE = 1 << 2,
//    kManageWorkshop_WorkshopStatus_ALL,
};

@interface SpiraxListViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, WebServiceManagerDelegate, CommonListDelegate>


@property IBOutletCollection(UIButton) NSArray *buttonArray;
@property (weak, nonatomic) IBOutlet UIButton *btnAll;


@property NSMutableArray *workshopListArray;
@property NSMutableArray *activeWorkshopArray;
@property NSMutableArray *lapsedWorkshopArray;
@property NSMutableArray *inactiveWorkshopArray;


@property (weak, nonatomic) IBOutlet TableViewWithEmptyView *listTableView;

@property (weak, nonatomic) IBOutlet UITextField *txtKeyword;

@property NSInteger selectedTabIndex;
@property NSInteger selectedRangeIndex;

@property NSInteger selectedFilterIndex;
@property (weak, nonatomic) IBOutlet UIView *statusDropdownView;
@property (weak, nonatomic) IBOutlet UIButton *btnStatusDropdown;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *statusToButtonConstaint;


@property (weak, nonatomic) IBOutlet UIView *legendPopupView;
@property (weak, nonatomic) IBOutlet UILabel *lblLegendsTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnLegend;
@property (weak, nonatomic) IBOutlet UITableView *legendTableView;
@property (weak, nonatomic) IBOutlet UIButton *btnLegendOk;

@property NSString *selectedTradeID;


@end

@implementation SpiraxListViewController
@synthesize listView; //must synthesize list view constraints to work

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInterface];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
}
-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    [self rangeBtnPressed: [self.buttonArray objectAtIndex: kManageWorkshop_All]];
    [self.btnStatusDropdown setTitle:LOCALIZATION(C_FILTER_BY_STATUS) forState: UIControlStateNormal];      //lokalise 28 Jan
    self.selectedFilterIndex = 0;
    
    if(![[mSession profileInfo] hasPendingWorkshopList]) {
        self.selectedTabIndex = 1;
    }
    
    switch (self.selectedTabIndex)
    {
        case kManageWorkshop_Pending:
            if (IS_COUNTRY(COUNTRYCODE_INDIA))
            {
                if (GET_PROFILETYPE == kPROFILETYPE_DSR)
                    [FIRAnalytics setScreenName:[NSString stringWithFormat: @"%@ / %@", LOCALIZATION_EN(C_TITLE_MYNETWORK), LOCALIZATION_EN(C_MANAGEWORKSHOP_PENDING)] screenClass:nil];
                else
                    [FIRAnalytics setScreenName:[NSString stringWithFormat: @"%@  /%@", LOCALIZATION_EN(C_TITLE_IN_MANAGEMYMECHANICS), LOCALIZATION_EN(C_MANAGEWORKSHOP_PENDING)] screenClass:nil];
            }
            else
            {
                [FIRAnalytics setScreenName:[NSString stringWithFormat: @"%@ / %@", LOCALIZATION_EN(C_TITLE_MANAGEWORKSHOP), LOCALIZATION_EN(C_MANAGEWORKSHOP_PENDING)] screenClass:nil];
            }
            
            self.btnLegend.hidden = YES;
            if (IS_COUNTRY(COUNTRYCODE_INDIA) && GET_PROFILETYPE == kPROFILETYPE_DSR)
            {
                self.statusToButtonConstaint.constant = -self.btnLegend.frame.size.width;
                self.statusDropdownView.hidden = NO;
                [self.btnStatusDropdown setTitle:LOCALIZATION(C_FILTER_BY_PROFILETYPE) forState: UIControlStateNormal]; //lokalise 28 Jan
            }
            else
            {
                self.statusDropdownView.hidden = YES;
            }
            break;
        case kManageWorkshop_Approved:
            if (IS_COUNTRY(COUNTRYCODE_INDIA))
            {
                if (GET_PROFILETYPE == kPROFILETYPE_DSR)
                    [FIRAnalytics setScreenName:[NSString stringWithFormat: @"%@ / %@", LOCALIZATION_EN(C_TITLE_MYNETWORK), LOCALIZATION_EN(C_MANAGEWORKSHOP_APPROVE)] screenClass:nil];
                else
                    [FIRAnalytics setScreenName:[NSString stringWithFormat: @"%@ / %@", LOCALIZATION_EN(C_TITLE_IN_MANAGEMYMECHANICS), LOCALIZATION_EN(C_MANAGEWORKSHOP_APPROVE)] screenClass:nil];
            }
            else
            {
                 [FIRAnalytics setScreenName:[NSString stringWithFormat: @"%@ / %@", LOCALIZATION_EN(C_TITLE_MANAGEWORKSHOP), LOCALIZATION_EN(C_MANAGEWORKSHOP_APPROVE)] screenClass:nil];
            }
            self.statusToButtonConstaint.constant = 10;
            self.btnLegend.hidden = NO;
            self.statusDropdownView.hidden = NO;
            break;
        default:
            break;
    }
}

- (void) setupInterface
{
    
    if (![self.parentViewController.parentViewController isKindOfClass: [ManageWorkshopTabViewController class]])
    {
        [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_MANAGEWORKSHOP) subtitle:@"" size:15 subtitleSize:0];
    }
    self.workshopListArray = [[NSMutableArray alloc] init];
    
    //preload listviews
//    listView = [[CASCommonListView alloc] initWithTitle:LOCALIZATION(C_)
    
    listView = [[CASCommonListView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_SELECTWORKSHOPSTATUS)       //lokalise 28 Jan
                                                   list: [mSession getWorkshopStatus]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
    
    //tableview setup
    PlaceholderView *vwPlaceholder = [[PlaceholderView alloc]initWithView:self.listTableView];
    [vwPlaceholder setBackgroundColor:[UIColor clearColor]];
    self.listTableView.emptyView = vwPlaceholder;
    
    // Initialize the refresh control.
    [self.listTableView.customRefreshControl addTarget:self
                                  action:@selector(refreshTable)
                        forControlEvents:UIControlEventValueChanged];

    
    [self.listTableView registerNib:[UINib nibWithNibName:@"SpiraxListTableViewCell" bundle:nil] forCellReuseIdentifier:@"SpiraxListTableViewCell"];
    
    //empty view to eliminate extra separators
    self.listTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    [self.btnStatusDropdown.titleLabel setFont: FONT_B1];
    
    if (kIsRightToLeft)
    {
        [self.btnStatusDropdown setContentHorizontalAlignment: UIControlContentHorizontalAlignmentRight];
    }
    else
    {
        [self.btnStatusDropdown setContentHorizontalAlignment: UIControlContentHorizontalAlignmentLeft];
    }
    
    for (int i = 0; i < [self.buttonArray count]; i++)
    {
        UIButton *btn = [self.buttonArray objectAtIndex: i];
        btn.tag = i;
        [btn.titleLabel setFont: FONT_H1];
        [btn setTitleColor: COLOUR_VERYDARKGREY forState: UIControlStateNormal];
        
        [btn setTitleColor: COLOUR_WHITE forState: UIControlStateSelected];
        
        switch (i)
        {
            case kManageWorkshop_AtoF:
                [btn setTitle: LOCALIZATION(C_KEYWORD_SECONDRANGE) forState:UIControlStateNormal];       //lokalise 28 Jan
                break;
            case kManageWorkshop_GtoL:
                [btn setTitle: LOCALIZATION(C_KEYWORD_THIRDRANGE) forState:UIControlStateNormal];        //lokalise 28 Jan
                break;
            case kManageWorkshop_MtoZ:
                [btn setTitle: LOCALIZATION(C_KEYWORD_FOURTHRANGE) forState:UIControlStateNormal];       //lokalise 28 Jan
                break;
            default:
                break;
        }
    }
    [self.btnAll setTitle: LOCALIZATION(C_KEYWORD_ALL)  forState: UIControlStateNormal];                 //lokalise 28 Jan
    
    UIImageView *imgforLeft=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)]; // Set frame as per space required around icon
    [imgforLeft setImage:[UIImage imageNamed:@"icon-search-lightgray.png"]];

    [imgforLeft setContentMode:UIViewContentModeCenter];// Set content mode centre or fit

    self. txtKeyword.leftView=imgforLeft;
    self. txtKeyword.leftViewMode=UITextFieldViewModeAlways;
    self.txtKeyword.font = FONT_B1;
    self.txtKeyword.placeholder = LOCALIZATION(C_KEYWORD_SEARCH);                                        //lokalise 28 Jan
    self.txtKeyword.delegate = self;
    
//    [self.btnStatusDropdown setTitle:LOCALIZATION(C_) forState: UIControlStateNormal];
    
//    self.lblLegendsTitle.text = LOCALIZATION(C_);
    
//    [self.btnLegendOk setTitle: LOCALIZATION(C_GLOBAL_OK) forState: UIControlStateNormal];
//    [self.btnLegendOk.titleLabel setFont: FONT_BUTTON];
    
    if (kIsRightToLeft) {
        [self.txtKeyword setTextAlignment:NSTextAlignmentRight];
        [self.btnStatusDropdown setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    } else {
        [self.txtKeyword setTextAlignment:NSTextAlignmentLeft];
        [self.btnStatusDropdown setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    }
}

-(void) refreshTable
{
    for (UIButton *btn in self.buttonArray)
    {
        if ([btn isSelected])
        {
            [self rangeBtnPressed: [self.buttonArray objectAtIndex: btn.tag]];
        }
    }
}

-(void) selectedTab: (NSInteger) index
{
    if([self.workshopListArray count] > 0) {
        [self.workshopListArray removeAllObjects];
    }
    [self.listTableView reloadData];
    self.selectedTabIndex = index;
    [self rangeBtnPressed: [self.buttonArray objectAtIndex: kManageWorkshop_All]];
}

- (IBAction)rangeBtnPressed:(UIButton *)sender {

    for (UIButton *btn in self.buttonArray)
    {
        if (btn != sender)
        {
            [btn setSelected: NO];
            [btn setBackgroundColor: COLOUR_WHITE];
        }
        else
        {
            [btn setSelected: YES];
            [btn setBackgroundColor: COLOUR_RED];
        }
    }
    [self prepareToSearchList: sender.tag];
    self.selectedRangeIndex = sender.tag;
}

-(void) prepareToSearchList: (NSInteger) index
{
    switch (index)
    {
        case kManageWorkshop_All:
            [self searchListWithSearchRange: @""];       //lokalise 28 Jan
            break;
        case kManageWorkshop_AtoF:
            [self searchListWithSearchRange: LOCALIZATION(C_KEYWORD_SECONDRANGE)];       //lokalise 28 Jan
            break;
        case kManageWorkshop_GtoL:
            [self searchListWithSearchRange: LOCALIZATION(C_KEYWORD_THIRDRANGE)];       //lokalise 28 Jan
            break;
        case kManageWorkshop_MtoZ:
            [self searchListWithSearchRange: LOCALIZATION(C_KEYWORD_FOURTHRANGE)];       //lokalise 28 Jan
        default:
            break;
    }
}
-(void) searchListWithSearchRange: (NSString *) searchRangeString
{
    if (IS_COUNTRY(COUNTRYCODE_INDIA) &&
        GET_PROFILETYPE == kPROFILETYPE_DSR &&
        self.selectedTabIndex == kManageWorkshop_Pending)
        [[WebServiceManager sharedInstance] fetchINWorkshopList: self.txtKeyword.text searchRange: searchRangeString profileTypeFilter: @(self.selectedFilterIndex).stringValue vc: self];     //lokalise 28 Jan
    else
    {
        if(![[mSession profileInfo] hasPendingWorkshopList]) {
            self.selectedTabIndex = 1;
        }
        if (self.selectedTabIndex == kManageWorkshop_Pending)
        {
            //pending
            [[WebServiceManager sharedInstance] fetchRegisteredPendingWorkshopList: self.txtKeyword.text searchRange: searchRangeString vc: self];
        }
        else
        {
            //approved
            [[WebServiceManager sharedInstance] fetchRegisteredApprovedWorkshopList: self.txtKeyword.text searchRange: searchRangeString vc: self];
        }
        
        NSLog(@"TEXT KEYWORD:%@",self.txtKeyword.text);
        NSLog(@"SEARCHRANGE STRING:%@",searchRangeString);
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self prepareToSearchList: self.selectedRangeIndex];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (IS_COUNTRY(COUNTRYCODE_INDIA) &&
        GET_PROFILETYPE == kPROFILETYPE_DSR &&
        self.selectedTabIndex == kManageWorkshop_Pending)
    {
        return [self.workshopListArray count];
    }
//    if (tableView == self.legendTableView)
//    {
//        NSArray *workshopStatusArray = [[mSession lookupTable] objectForKey: @"WorkshopStatus"];
//        return [workshopStatusArray count];
//    }
//    else
    {
        switch (self.selectedFilterIndex) {
            case kManageWorkshop_WorkshopStatus_ACTIVE:
                return [self.activeWorkshopArray count];
                
                break;
            case kManageWorkshop_WorkshopStatus_LAPSED:
                return [self.lapsedWorkshopArray count];
                
                break;
            case kManageWorkshop_WorkshopStatus_INACTIVE:
                return [self.inactiveWorkshopArray count];
                break;
                
            default:
                return [self.workshopListArray count];
                break;
        }
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    if (tableView == self.legendTableView)
//    {
//        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"LegendsCell"];
//
//        NSDictionary *cellData = [[[mSession lookupTable] objectForKey: @"WorkshopStatus"] objectAtIndex: indexPath.row];
////        @"Status": @(kManageWorkshop_WorkshopStatus_INACTIVE),
////        @"StatusTitle": @"Inactive Workshops:",
////        @"StatusDesc": @"Have not registered any product codes since joined",
//
//        UIImageView *imgStatus = [cell viewWithTag: 1];
//
//        switch ([[cellData objectForKey: @"KeyCode"] intValue]) {
//            case kManageWorkshop_WorkshopStatus_ACTIVE:
//                [imgStatus setImage: [UIImage imageNamed: @"icon-active"]];
//                break;
//            case kManageWorkshop_WorkshopStatus_LAPSED:
//                [imgStatus setImage: [UIImage imageNamed: @"icon-lapsed"]];
//                break;
//            case kManageWorkshop_WorkshopStatus_INACTIVE:
//                [imgStatus setImage: [UIImage imageNamed: @"icon-inactive"]];
//                break;
//            default:
//                [imgStatus setImage: nil];
//                break;
//        }
//
//        UILabel *statusTitle = [cell viewWithTag: 2];
//        statusTitle.font = FONT_B1;
//        statusTitle.text = [cellData objectForKey: @"KeyValue"];
//
//        UILabel *statusDesc = [cell viewWithTag: 3];
//        statusDesc.font = FONT_B1;
//        statusDesc.text = [cellData objectForKey: @"Remarks"];
//        return cell;
//    }
//    else
    {
        NSDictionary *cellData;
        
        switch (self.selectedTabIndex)
        {
            case kManageWorkshop_Pending:
            {
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"PendingCell"];
                cellData = [self.workshopListArray objectAtIndex: indexPath.row];
                
                UILabel *lblName = [cell viewWithTag: 1];
                lblName.text = [cellData objectForKey: @"CompanyName"];
                lblName.font = FONT_B1;
                
                UIImageView *imgArrow = [cell viewWithTag:2];
                if (kIsRightToLeft) {
                    imgArrow.image = [UIImage imageNamed:@"icon-arrsingleL.png"];
                } else {
                    imgArrow.image = [UIImage imageNamed:@"icon-arrsingleR.png"];
                }
                
                return cell;
            }
                break;
            case kManageWorkshop_Approved:
            {
                SpiraxListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"SpiraxListTableViewCell"];
                
                switch (self.selectedFilterIndex) {
                    case kManageWorkshop_WorkshopStatus_ACTIVE:
                        cellData = [self.activeWorkshopArray objectAtIndex: indexPath.row];
                        break;
                    case kManageWorkshop_WorkshopStatus_LAPSED:
                        cellData = [self.lapsedWorkshopArray objectAtIndex: indexPath.row];
                        
                        break;
                    case kManageWorkshop_WorkshopStatus_INACTIVE:
                        cellData = [self.inactiveWorkshopArray objectAtIndex: indexPath.row];
                        break;
                    default:
                        cellData = [self.workshopListArray objectAtIndex: indexPath.row];
                        break;
                }
                
                cell.lblName.text = [cellData objectForKey: @"CompanyName"];
                
                if ([cellData objectForKey: @"Status"])
                {
                    switch ([[cellData objectForKey: @"Status"] intValue]) {
                        case kManageWorkshop_WorkshopStatus_ACTIVE:
                            [cell.imgWorkshopStatus setImage: [UIImage imageNamed: @"icon-active"]];
                            break;
                        case kManageWorkshop_WorkshopStatus_LAPSED:
                            [cell.imgWorkshopStatus setImage: [UIImage imageNamed: @"icon-lapsed"]];
                            
                            break;
                        case kManageWorkshop_WorkshopStatus_INACTIVE:
                            [cell.imgWorkshopStatus setImage: [UIImage imageNamed: @"icon-inactive"]];
                            break;
                        default:
                            [cell.imgWorkshopStatus setImage: [UIImage imageNamed: @"icon-inactive"]];
                            break;
                    }
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                UIImageView *imgArrow = [cell viewWithTag:2];
                if (kIsRightToLeft) {
                    imgArrow.image = [UIImage imageNamed:@"icon-arrsingleL.png"];
                } else {
                    imgArrow.image = [UIImage imageNamed:@"icon-arrsingleR.png"];
                }
                
                return cell;
            }
                break;
            default:
                break;
        }
    }
    return [UITableViewCell new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (tableView == self.legendTableView)
//        return;
//
    NSDictionary *cellData = [self.workshopListArray objectAtIndex: indexPath.row];
    
    switch (self.selectedTabIndex)
    {
        case kManageWorkshop_Pending:
            self.selectedTradeID = [cellData objectForKey:@"LeadID"];
            [[WebServiceManager sharedInstance] fetchLeads: [cellData objectForKey: @"LeadID"] vc:self];
            break;
        default:
            break;
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_FETCHWORKSHOPPENDINGLIST:
        {
            [self.listTableView.customRefreshControl performSelectorOnMainThread: @selector(endRefreshing) withObject: self.listTableView.customRefreshControl waitUntilDone:NO];
            
            NSDictionary *data = [response getGenericResponse];
            
            //reset array
            if ([self.workshopListArray isEqual:[NSNull null]])
                self.workshopListArray = [[NSMutableArray alloc] init];
            
            if(![[data objectForKey:@"PendingWorkShop"] isKindOfClass:[NSNull class]]) {
                self.workshopListArray = [data objectForKey: @"PendingWorkShop"];
            } else {
                self.workshopListArray = @[];
            }
            
            [self.listTableView reloadData];
        }
            break;
        case kWEBSERVICE_FETCHWORKSHOPAPPROVEDLIST:
        {
            [self.listTableView.customRefreshControl performSelectorOnMainThread: @selector(endRefreshing) withObject: self.listTableView.customRefreshControl waitUntilDone:NO];

            NSDictionary *data = [response getGenericResponse];
            
            //reset array
            if ([self.workshopListArray isEqual:[NSNull null]])
                self.workshopListArray = [[NSMutableArray alloc] init];
            
            if(![[data objectForKey:@"OnePlusOneApporvedWorkshop"] isKindOfClass:[NSNull class]]) {
                self.workshopListArray = [data objectForKey: @"OnePlusOneApporvedWorkshop"];
            } else {
                self.workshopListArray = @[];
            }
            
            self.activeWorkshopArray = [[NSMutableArray alloc] init];
            self.lapsedWorkshopArray = [[NSMutableArray alloc] init];
            self.inactiveWorkshopArray = [[NSMutableArray alloc] init];
            
            for (NSDictionary *workshopDict in self.workshopListArray)
            {
                if ([workshopDict objectForKey: @"Status"])
                {
                    switch ([[workshopDict objectForKey: @"Status"] intValue]) {
                        case kManageWorkshop_WorkshopStatus_ACTIVE:
                            [self.activeWorkshopArray addObject: workshopDict];
                            break;
                        case kManageWorkshop_WorkshopStatus_LAPSED:
                            [self.lapsedWorkshopArray addObject: workshopDict];
                            break;
                        case kManageWorkshop_WorkshopStatus_INACTIVE:
                            [self.inactiveWorkshopArray addObject: workshopDict];
                            break;
                        default:
                            break;
                    }
                }
            }
            [self.listTableView reloadData];
        }
            break;
            
        case kWEBSERVICE_FETCHLEADS:
        {
            //push to lead details view. (pending)
            //self > uinavigationcontroller > rdvtabbarcontroller
            NSMutableDictionary *responseDict = [response getGenericResponse];
            responseDict[@"LeadID"] = self.selectedTradeID;
            [mSession pushWorkshopDetailsTabView: [self.parentViewController parentViewController] workshopDetailsData: [response getGenericResponse]];
        }
            break;
        case kWEBSERVICE_FETCHTRADE:
        {
            //push to trade details view. (approved)
            NSMutableDictionary *responseDict = [response getGenericResponse];
            responseDict[@"TradeID"] = self.selectedTradeID;
            [mSession pushWorkshopDetailsTabView: [self.parentViewController parentViewController] workshopDetailsData: responseDict];
        }
            break;
        default:
            break;
            
    }
}

- (IBAction)workshopStatusPressed:(id)sender {
    
    if (self.selectedTabIndex == kManageWorkshop_Pending && IS_COUNTRY(COUNTRYCODE_INDIA) && GET_PROFILETYPE == kPROFILETYPE_DSR)
    {
        [listView setTitleAndList: LOCALIZATION(C_FILTER_BY_PROFILETYPE) list: [mSession getWorkshopProfileTypes] hasSearchField: NO];      //lokalise 28 Jan
    }
    else
    {
        [listView setTitleAndList: LOCALIZATION(C_FILTER_BY_STATUS) list: [mSession getWorkshopStatus] hasSearchField: NO];     //lokalise 28 Jan
    }
    
    [self popUpView];
}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        [self.btnStatusDropdown setTitle: selection.firstObject forState:UIControlStateNormal];
        
        
        if (self.selectedTabIndex == kManageWorkshop_Pending && IS_COUNTRY(COUNTRYCODE_INDIA) && GET_PROFILETYPE == kPROFILETYPE_DSR)
        {
//            [listView setTitleAndList: LOCALIZATION(@"Filter By Profile Type") list: [mSession getWorkshopProfileTypes] hasSearchField: NO];
            
            self.selectedFilterIndex = [[mSession convertToWorkshopProfileTypesKeyCode: selection.firstObject] intValue];
            
            [self prepareToSearchList: self.selectedRangeIndex];
        }
        else
        {
            self.selectedFilterIndex = [[mSession convertToWorkshopStatusKeyCode: selection.firstObject] intValue];
            [self.listTableView reloadData];
        }
    }
}

- (IBAction)showLegend:(id)sender {
//    self.legendPopupView.hidden = NO;
    [self.view endEditing: YES];

    PopupInfoTableViewController *popupVc = [STORYBOARD_POPUPS instantiateViewControllerWithIdentifier: VIEW_POPUPINFOTABLEVIEW];
    
    [self popUpViewWithPopupVC:popupVc];
    
    [popupVc initLegendsPopup];
}

//- (IBAction)hideLegend:(id)sender {
////    self.legendPopupView.hidden = YES;
//}

-(void)processFailed:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress: KEY_WINDOW];
    
    [self.listTableView.customRefreshControl performSelectorOnMainThread: @selector(endRefreshing) withObject: self.listTableView.customRefreshControl waitUntilDone:NO];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
