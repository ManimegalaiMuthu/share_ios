//
//  SpiraxListViewController.h
//  Shell
//
//  Created by MUTHUPANDIYAN Manimegalai on 18/9/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "BaseVC.h"


NS_ASSUME_NONNULL_BEGIN

@interface SpiraxListViewController : BaseVC

-(void) selectedTab: (NSInteger) index;



@end

NS_ASSUME_NONNULL_END
