//
//  SpiraxListTableViewCell.h
//  Shell
//
//  Created by MUTHUPANDIYAN Manimegalai on 18/9/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Constants.h"

NS_ASSUME_NONNULL_BEGIN

@interface SpiraxListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *imgWorkshopStatus;



@end

NS_ASSUME_NONNULL_END
