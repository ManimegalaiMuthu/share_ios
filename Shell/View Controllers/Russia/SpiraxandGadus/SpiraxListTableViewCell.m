//
//  SpiraxListTableViewCell.m
//  Shell
//
//  Created by MUTHUPANDIYAN Manimegalai on 18/9/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "SpiraxListTableViewCell.h"

@interface SpiraxListTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;

@end

@implementation SpiraxListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.lblName.font = FONT_B1;
    
    if (kIsRightToLeft)
    {
        [self.imgArrow setImage: [UIImage imageNamed: @"icon-arrsingleL.png"]];
        self.lblName.textAlignment = NSTextAlignmentRight;
    }
    else
    {
        [self.imgArrow setImage: [UIImage imageNamed: @"icon-arrsingleR.png"]];
        self.lblName.textAlignment = NSTextAlignmentLeft;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
