//
//  RUPartnersBarView.m
//  Shell
//
//  Created by Jeremy Lua on 25/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "Session.h"
#import "RUPartnersBarView.h"
#import "LocalizationManager.h"

@interface RUPartnersBarView ()

@property (weak, nonatomic) IBOutlet UIView *progressBarView;
@property (strong, nonatomic) IBOutlet UILabel *lblTarget;


@property (weak, nonatomic) IBOutlet UIImageView *imgCurrentArrow;
@property (strong, nonatomic) IBOutlet UILabel *lblCurrent;


@property (strong, nonatomic) IBOutlet UILabel *lblZero;
@property (strong, nonatomic) IBOutlet UILabel *lblMax;

@property (weak, nonatomic) IBOutlet UIImageView *imgRemaining;
@property (weak, nonatomic) IBOutlet UILabel *lblRemaining; //Max - Current



@end

@implementation RUPartnersBarView


-(void)awakeFromNib
{
    [super awakeFromNib];
    [self layoutIfNeeded];
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        //https://stackoverflow.com/questions/30335089/reuse-a-uiview-xib-in-storyboard/37668821#37668821
        // 1. load the interface
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        // 2. add as subview
        [self addSubview:self.contentView];
        
        // 3. allow for autolayout
        self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
        // 4. add constraints to span entire view
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[view]-|" options:0 metrics:nil views:@{@"view":self.contentView}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[view]-|" options:0 metrics:nil views:@{@"view":self.contentView}]];
    
        [self setNeedsLayout];
    }
    return self;
}


#define degreesToRadians(degrees)((M_PI * degrees)/180)
-(void) loadBar: (NSDictionary *) responseData
{
    
    //lookup
    NSDictionary *partnersLookupTable;
    if(![responseData objectForKey:@"BusinessType"] || [[responseData objectForKey:@"BusinessType"] isKindOfClass:[NSNull class]]) {
        partnersLookupTable = [[mSession lookupTable] objectForKey: @"PartnersShellClubB2B"];
    } else {
        switch([[responseData objectForKey:@"BusinessType"] intValue]) {
            case 1:
                partnersLookupTable = [[mSession lookupTable] objectForKey: @"PartnersShellClubB2B"];
                break;
            case 2:
                partnersLookupTable = [[mSession lookupTable] objectForKey: @"PartnersShellClubB2C"];
                break;
                
        }
        
    }
    
    NSDictionary *data = [responseData objectForKey:@"Data"];
    
    NSString *unitString = @"";
    
    self.maxPoints = [[data objectForKey: @"MaxPoints"] intValue];
    self.targetPoints = [[data objectForKey: @"FirstTarget"] intValue];
    self.minPoints = [[partnersLookupTable objectForKey: @"MinValue"] intValue];
    self.currentPoints = [[data objectForKey: @"PointsGained"] intValue];

    //clear previously drawn circle
    NSMutableArray *layerToRemoveArray = [[NSMutableArray alloc] init]; //to hold references to actual progress bar
    
    for (CALayer *layer in [self.progressBarView.layer sublayers])
    {
        if ([layer isKindOfClass: [CAShapeLayer class]])
        {
            [layerToRemoveArray addObject: layer];
        }
    }
    
    //remove referenced layers from superlayer
    for (CALayer *layer in layerToRemoveArray)
    {
        [layer removeFromSuperlayer];
    }

    self.imgCurrentArrow.hidden = YES;
    
    self.lblZero.text = [NSString stringWithFormat:@"%@",[self addCommaThousandSeparatorsWithInputString:self.minPoints]];
    self.lblZero.font = FONT_B(11);
    [self.lblZero sizeToFit];
    
    self.lblCurrent.text = [NSString stringWithFormat:@"%@",[self addCommaThousandSeparatorsWithInputString:self.currentPoints]];
    self.lblCurrent.font = FONT_B(11);
    [self.lblCurrent sizeToFit];
    
    self.lblTarget.text = [NSString stringWithFormat:@"%@",[self addCommaThousandSeparatorsWithInputString:self.targetPoints]];
    self.lblTarget.font = FONT_B(11);
    [self.lblTarget sizeToFit];
    
    self.lblMax.text = [NSString stringWithFormat:@"%@",[self addCommaThousandSeparatorsWithInputString:self.maxPoints]];;
    self.lblMax.font = FONT_B(11);
    [self.lblMax sizeToFit];
    
    
    CGFloat zeroAngle = ((self.minPoints * 180) - 180);
    
    //convert all point to angles
    CGFloat currentPointAngle = ((self.currentPoints / self.maxPoints) * 180) + zeroAngle;    //-180 = start angle
    CGFloat targetPointAngle = ((self.targetPoints / self.maxPoints) * 180) + zeroAngle;
    
    if (currentPointAngle < zeroAngle + 5)
    {
        self.lblCurrent.hidden = YES;
    }
    
    if (currentPointAngle - 5 < targetPointAngle &&
        currentPointAngle + 5 > targetPointAngle )
    {
        self.lblCurrent.hidden = YES;
    }
    
    //zero
    if (self.lblZero.superview != self.progressBarView)
        [self.progressBarView addSubview: self.lblZero];
    
    self.lblZero.translatesAutoresizingMaskIntoConstraints = YES;
    self.lblZero.center = [self drawPointsBarWithStartAngle: zeroAngle endAngle: zeroAngle colour: COLOUR_PALEGREY hasMarking: YES layerName:@"Zero"];
    
    [self calculateCenterFromCircle: self.lblZero];
    
    //prepare remaining  views
    //target point
    if (self.lblTarget.superview != self.progressBarView)
    {
        [self.progressBarView addSubview: self.lblTarget];
    }
    self.lblTarget.translatesAutoresizingMaskIntoConstraints = YES;

    //current point
    if (self.lblCurrent.superview != self.progressBarView)
    {
        [self.progressBarView addSubview: self.lblCurrent];
    }
    self.lblCurrent.translatesAutoresizingMaskIntoConstraints = YES;

    //maxPoint
    if (self.lblMax.superview != self.progressBarView)
    {
        [self.progressBarView addSubview: self.lblMax];
    }
    self.lblMax.translatesAutoresizingMaskIntoConstraints = YES;
    
    
    if (self.currentPoints > self.targetPoints &&
        self.maxPoints > self.currentPoints)
    {
        //target points to render first before current
        self.lblTarget.center = [self drawPointsBarWithStartAngle: zeroAngle endAngle:targetPointAngle colour: COLOUR_RED hasMarking: YES layerName: @"Target"];
        [self calculateCenterFromCircle: self.lblTarget];

        self.lblCurrent.center = [self drawPointsBarWithStartAngle: targetPointAngle endAngle: currentPointAngle colour: COLOUR_RED hasMarking: NO layerName: @"Current"];
        [self calculateCenterFromCircle: self.lblCurrent];

        self.lblMax.center = [self drawPointsBarWithStartAngle: currentPointAngle endAngle:0 colour: COLOUR_PALEGREY hasMarking: YES layerName: @"Max"];
        [self calculateCenterFromCircle: self.lblMax];
        
        
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat: @"%@%@\n", [self addCommaThousandSeparatorsWithInputString:self.maxPoints - self.currentPoints], LOCALIZATION(C_REWARDS_PTS)] attributes:@{NSForegroundColorAttributeName: COLOUR_RED, NSFontAttributeName: FONT_H1, }];
        
        NSAttributedString *remainingStringAttr;
        if (![[data objectForKey: @"Text"] isKindOfClass: [NSNull class]])
        {
            remainingStringAttr = [[NSAttributedString alloc] initWithString: [data objectForKey: @"Text"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY, NSFontAttributeName: FONT_B3,}];
        }
        else
        {
            remainingStringAttr = [[NSAttributedString alloc] initWithString: @"" attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY, NSFontAttributeName: FONT_B3,}];
        }
        [attrString appendAttributedString: remainingStringAttr];
        
        self.lblRemaining.attributedText = attrString;
            
        [self.imgRemaining setImage: [UIImage imageNamed: @"carousel_partnersofshell_bottle_verydarkgrey.png"]];
    }
    else if (self.targetPoints > self.currentPoints &&
            self.maxPoints > self.currentPoints)
    {
        //current points to render first before target
        self.lblCurrent.center = [self drawPointsBarWithStartAngle: zeroAngle endAngle: currentPointAngle colour: COLOUR_RED hasMarking: NO layerName: @"Current"];
        [self calculateCenterFromCircle: self.lblCurrent];

        self.lblTarget.center = [self drawPointsBarWithStartAngle: currentPointAngle endAngle:targetPointAngle colour: COLOUR_PALEGREY hasMarking: YES layerName: @"Target"];
        [self calculateCenterFromCircle: self.lblTarget];

        self.lblMax.center = [self drawPointsBarWithStartAngle: targetPointAngle endAngle: 0 colour: COLOUR_PALEGREY hasMarking: YES layerName: @"Max"];
        [self calculateCenterFromCircle: self.lblMax];
        
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat: @"%@%@\n", [self addCommaThousandSeparatorsWithInputString:self.targetPoints - self.currentPoints], LOCALIZATION(C_REWARDS_PTS)] attributes:@{NSForegroundColorAttributeName: COLOUR_RED, NSFontAttributeName: FONT_H1,}];
        
        NSAttributedString *remainingStringAttr;
        if (![[data objectForKey: @"Text"] isKindOfClass: [NSNull class]])
        {
            remainingStringAttr = [[NSAttributedString alloc] initWithString: [data objectForKey: @"Text"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY, NSFontAttributeName: FONT_B3, }];

        }
        else
        {
            remainingStringAttr = [[NSAttributedString alloc] initWithString: @"" attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY, NSFontAttributeName: FONT_B3, }];
        }
        
        [attrString appendAttributedString: remainingStringAttr];
        
        self.lblRemaining.attributedText = attrString;
        [self.imgRemaining setImage: [UIImage imageNamed: @"carousel_partnersofshell_bottle_verydarkgrey.png"]];
    }
    else if (self.currentPoints >= self.maxPoints)
    {
        //no need to render current.
        [self.lblCurrent removeFromSuperview];
        
        self.lblTarget.center = [self drawPointsBarWithStartAngle: zeroAngle endAngle:targetPointAngle colour: COLOUR_RED hasMarking: YES layerName: @"Target"];
        [self calculateCenterFromCircle: self.lblTarget];
    
        self.lblMax.center = [self drawPointsBarWithStartAngle: targetPointAngle endAngle:0 colour: COLOUR_RED hasMarking: YES layerName: @"Max"];
        [self calculateCenterFromCircle: self.lblMax];
        
        //change remaining image and text
        
        if (![[data objectForKey: @"Text"] isKindOfClass: [NSNull class]])
        {
            self.lblRemaining.attributedText = [[NSAttributedString alloc] initWithString: [data objectForKey: @"Text"] attributes: @{NSForegroundColorAttributeName: COLOUR_RED,  NSFontAttributeName: FONT_B3, }];
        }
        else
        {
            self.lblRemaining.attributedText = [[NSAttributedString alloc] initWithString: @"" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,  NSFontAttributeName: FONT_B3, }];
        }
        [self.imgRemaining setImage: [UIImage imageNamed: @"carousel_partnersofshell_confeti_verydarkgrey.png"]];
    }
    
//    [self addIconToLabel: self.lblMax];
    [self addIconToLabel: self.lblTarget];
    
    if(SCREEN_WIDTH <= 320) {
        [NSLayoutConstraint constraintWithItem:self.lblRemaining attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.progressBarView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:-10].active = YES;
        self.lblRemaining.translatesAutoresizingMaskIntoConstraints =  NO;
        self.imgRemaining.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    [self setNeedsLayout];
    
}

-(void) addIconToLabel:(UILabel *) label
{
    UIImageView *imgView = [[UIImageView alloc] initWithFrame: CGRectMake(-20, 0, 20, 20)];
    [imgView setImage: [UIImage imageNamed: @"carousel_partnersofshell_giftbox_verydarkgrey.png"]];
    
    [label addSubview: imgView];
    imgView.center = CGPointMake(imgView.center.x, label.frame.size.height / 2);
    
    //if icon on right side of bar, offset to right to fit icon
    if (label.center.x > self.progressBarView.frame.size.width / 2)
    {
        label.center = CGPointMake(label.center.x + 20, label.center.y);
    }
}

-(void) calculateCenterFromCircle:(UIView *) view
{
    //check which direction of progress point of label should be at.
    if (view.center.x < self.progressBarView.frame.size.width / 2)
    {
        //bottomright of rect
        view.center = CGPointMake(view.center.x - (view.frame.size.width / 2), view.center.y - (view.frame.size.height / 2));
    }
    else
    {
        //bottom left of rect
        view.center = CGPointMake(view.center.x + (view.frame.size.width / 2), view.center.y - (view.frame.size.height / 2));
    }
    
    
}

#define BARWIDTH 15
#define MARKINGWIDTH (BARWIDTH + 5)
-(CGPoint) drawPointsBarWithStartAngle:(CGFloat)startAngle
                              endAngle: (CGFloat)endAngle
                                colour:(UIColor *)colour
                            hasMarking:(BOOL) hasMarking
                             layerName:(NSString *)layerName
{
    CGPoint origin = CGPointMake((SCREEN_WIDTH / 2.1), self.progressBarView.frame.size.height);

    CGFloat baseRadius = self.progressBarView.frame.size.height - 25;
    if(SCREEN_WIDTH <= 320) {
        baseRadius = 91;
        origin = CGPointMake((SCREEN_WIDTH / 2.2), self.progressBarView.frame.size.height - 15);
    }
        
    
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    [bezierPath addArcWithCenter: origin
                          radius: baseRadius
                      startAngle: degreesToRadians(startAngle)
                        endAngle: degreesToRadians(endAngle)
                       clockwise: YES];
    
    CAShapeLayer *progressLayer = [[CAShapeLayer alloc] init];
    progressLayer.name = layerName;
    [progressLayer setPath:bezierPath.CGPath];
    [progressLayer setStrokeColor: [colour CGColor]];
    [progressLayer setFillColor:[UIColor clearColor].CGColor];
    [progressLayer setLineWidth:BARWIDTH];
    [self.progressBarView.layer addSublayer:progressLayer];
    
    
    if (hasMarking)
    {
        //marking
        UIBezierPath *markingPath = [UIBezierPath bezierPath];
        [markingPath addArcWithCenter: origin
                              radius: baseRadius + ((MARKINGWIDTH - BARWIDTH) / 2) //stretching out of red
                          startAngle: degreesToRadians(endAngle - 1)
                            endAngle: degreesToRadians(endAngle)
                           clockwise: YES];
        
        CAShapeLayer *markingLayer = [[CAShapeLayer alloc] init];
        [markingLayer setPath:markingPath.CGPath];
        [markingLayer setStrokeColor: [COLOUR_VERYDARKGREY CGColor]];
        [markingLayer setFillColor:[UIColor clearColor].CGColor];
        [markingLayer setLineWidth:MARKINGWIDTH];
        [self.progressBarView.layer addSublayer:markingLayer];
    }
    else
    {
        //get center point for arrow
        self.imgCurrentArrow.hidden = NO;
        
        UIBezierPath *arrowPointPath = [UIBezierPath bezierPath];
        [arrowPointPath addArcWithCenter: origin
                                   radius: baseRadius + (MARKINGWIDTH / 2) + 2
                               startAngle: degreesToRadians(endAngle)
                                 endAngle: degreesToRadians(endAngle)
                                clockwise: YES];
        
        self.imgCurrentArrow.center = [arrowPointPath currentPoint];
        
        //end angle = arrow angle
        //-90 = -180
        //0 = -90
        //90 = 0
        CGFloat arrowAngle = endAngle + 90;
        
        
        [self.imgCurrentArrow setImage: [UIImage imageNamed: @"carousel_triangle_red.png"]];
        self.imgCurrentArrow.transform = CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(arrowAngle));
//            imgArrow.transform = CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(endAngle));
    }
    
    //get outer point
    UIBezierPath *outerRegionPath = [UIBezierPath bezierPath];
    [outerRegionPath addArcWithCenter: origin
                               radius: baseRadius + MARKINGWIDTH
                           startAngle: degreesToRadians(startAngle)
                             endAngle: degreesToRadians(endAngle)
                            clockwise: YES];
    
    return [outerRegionPath currentPoint];
}

-(NSString *) addCommaThousandSeparatorsWithInputString: (CGFloat) inputValue
{
    
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setNumberStyle:NSNumberFormatterDecimalStyle]; // to get commas (or locale equivalent)
    [fmt setGroupingSeparator:@","];
    return [fmt stringFromNumber:@(inputValue)];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
