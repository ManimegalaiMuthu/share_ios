//
//  RUProgressBarView.m
//  Shell
//
//  Created by Jeremy Lua on 24/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "LocalizationManager.h"
#import "RUProgressBarView.h"
@interface RUProgressBarView ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *thirdWidth;
@property (weak, nonatomic) IBOutlet UILabel *lblBubble;

@end


@implementation RUProgressBarView

-(void)awakeFromNib
{
    [super awakeFromNib];
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        //https://stackoverflow.com/questions/30335089/reuse-a-uiview-xib-in-storyboard/37668821#37668821
        // 1. load the interface
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        // 2. add as subview
        [self addSubview:self.contentView];
        
//        // 3. allow for autolayout
//        self.view.translatesAutoresizingMaskIntoConstraints = NO;
//        // 4. add constraints to span entire view
//        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
//        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
    }
    return self;
}

-(void) reloadBar
{
    self.firstWidth.constant = [self calculateRectWidthWithPoints: self.firstPoints];
    self.secondWidth.constant = [self calculateRectWidthWithPoints: self.secondPoints];
    self.thirdWidth.constant = [self calculateRectWidthWithPoints: self.thirdPoints];
    
    self.lblBubble.text = [NSString stringWithFormat: @"%.0f %@", self.firstPoints + self.secondPoints + self.thirdPoints, LOCALIZATION(C_REWARDS_PTS)];   //lokalised
    
    [self setNeedsLayout];
}

-(CGFloat) calculateRectWidthWithPoints:(CGFloat)points
{
    CGFloat width = (points / self.maxPoints) * self.frame.size.width;
    return width;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
