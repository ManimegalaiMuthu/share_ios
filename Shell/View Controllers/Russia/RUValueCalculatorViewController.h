//
//  RUValueCalculatorViewController.h
//  Shell
//
//  Created by Jeremy Lua on 5/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface RUValueCalculatorViewController : BaseVC
@property NSDictionary *calculatorDict;
@end
