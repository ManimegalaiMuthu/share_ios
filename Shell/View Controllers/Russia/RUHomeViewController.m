//
//  RUHomeViewController.m
//  Shell
//
//  Created by Jeremy Lua on 23/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUHomeViewController.h"
#import "RUProgressBarView.h"
#import "RUPartnersBarView.h"
#import <WebKit/WebKit.h>

@interface RUHomeViewController () <WebServiceManagerDelegate, UIScrollViewDelegate, UICollectionViewDataSource, WKNavigationDelegate/*, QRCodeScannerDelegate*/>

//@property (weak, nonatomic) IBOutlet UIScrollView *carouselScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *carouselPageControl;

@property (strong, nonatomic) IBOutlet UICollectionView *carouselColView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *carouselColViewLayout;

@property NSMutableArray *carouselEnumArray;
@property NSMutableDictionary *carouselProfileDict;
@property NSMutableDictionary *carouselPointsDict;
@property NSMutableDictionary *carouselPartnerDict;

typedef enum
{
    kCAROUSEL_PROFILE,
    kCAROUSEL_POINTS,
    kCAROUSEL_PARTNERS,
    kCAROUSEL_PARTNERSENDED,
    kCAROUSEL_DSRPARTNERS,
} kCAROUSEL_TYPES;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


@property (weak, nonatomic) IBOutlet UILabel *lblQuickLinksHeader;
@property NSMutableArray *quickLinksEnumArray;
@property NSMutableArray *quickLinksArray;
@property (weak, nonatomic) IBOutlet UICollectionView *quickLinksColView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *quickLinkColViewLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *quickLinksColViewHeight;

//survey popup
@property (strong, nonatomic) IBOutlet UIView *surveyView;
@property (weak, nonatomic) IBOutlet UIButton *surveyCloseButton;
@property WebViewHelper *webViewHelper;

//in-appnotification popup
@property (strong, nonatomic) IBOutlet UIView *inAppNotificationView;
@property (weak, nonatomic) IBOutlet UIView *inAppWebViewContainer;
@property (weak, nonatomic) IBOutlet UIButton *btnInAppReadMore;
@property (weak, nonatomic) IBOutlet UIButton *btnInAppClose;
@property NSMutableArray *inAppIdArray;

@property (strong, nonatomic) IBOutlet UIButton *btnCall;

@property NSInteger currentPage_Carousel;
@property NSTimer *autoRotate_Timer;
@property NSTimer *idle_Timer;

@end

@implementation RUHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //    //QRCode Delegate
    //    mQRCodeScanner.delegate = self;
    
    self.carouselColViewLayout.itemSize = CGSizeMake(SCREEN_WIDTH, self.carouselColView.frame.size.height);
    
//    if (GET_CONSUMER_TNC)
//        [[WebServiceManager sharedInstance] requestConsumerTNC: [GET_CONSUMER_TNC objectForKey: @"Version"]
//                                                          view:nil];
//    else
//        [[WebServiceManager sharedInstance] requestConsumerTNC: @""
//                                                          view:nil];
    
    if (GET_WORKSHOP_TNC)
        [[WebServiceManager sharedInstance] requestWorkshopTNC: [GET_WORKSHOP_TNC objectForKey: @"Version"]
                                                          view:nil];
    else
        [[WebServiceManager sharedInstance] requestWorkshopTNC: @""
                                                          view:nil];
    
    if (GET_DSR_TNC)
        [[WebServiceManager sharedInstance] requestDSRTNC: [GET_DSR_TNC objectForKey: @"Version"]
                                                     view:nil];
    else
        [[WebServiceManager sharedInstance] requestDSRTNC: @""
                                                     view:nil];
    //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    [[WebServiceManager sharedInstance] fetchHome: self];
    //    });
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
//    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//    [tracker set:kGAIScreenName value: LOCALIZATION_EN(C_TITLE_HOME)];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_HOME) subtitle: @""];         //lokalised
    
    self.btnCall.hidden = YES;
    
    [self setupInterface];
    [self setupLocalisation];
}

typedef enum
{
    //DSR
    kQUICKLINK_DSRMANAGEWORKSHOP = 1000,
    kQUICKLINK_PARTNERSOFSHELL,
    kQUICKLINK_SPIRAXGADUSONEPLUSONE,
    
//    kQUICKLINK_PARTICIPATINGPRODUCT,
//    kQUICKLINK_DSRMANAGEORDERS,
//    kQUICKLINK_REGISTERPRODUCTCODE,
    kQUICKLINK_MYLIBRARY,
//    kQUICKLINK_OILCHANGEREGISTRATION,
    
    //TO
    kQUICKLINK_MANAGEORDERS,
//    kQUICKLINK_REGISTERPRODUCTCODE,
//    kQUICKLINK_PARTNERSOFSHELL, //already exist
    kQUICKLINK_OILCHANGEREGISTRATION, // same as kQUICKLINK_SEARCHVEHICLEID
    kQUICKLINK_SCANDECAL,
    kQUICKLINK_ACTIVATEDECAL,
//    kQUICKLINK_INVITECUSTOMER,
//    kQUICKLINK_MYLIBRARY, //already exist
    kQUICKLINK_WORKSHOPOFFERS,
    kQUICKLINK_WORKSHOPPERFORMANCE,
    kQUICKLINK_RU_WORKSHOPPERFORMANCE,
    
} kQUICKLINK_TYPES;

-(void) setupInterface
{
    
    self.carouselPageControl.pageIndicatorTintColor = COLOUR_VERYPALEGREY;
    self.carouselPageControl.currentPageIndicatorTintColor = COLOUR_RED;
    
#pragma mark in App notification interface
    
    self.btnInAppReadMore.titleLabel.font = FONT_BUTTON;
    [self.btnInAppReadMore setTitle: LOCALIZATION(C_NOTIFICATION_READMORE) forState: UIControlStateNormal];     //lokalised
    
    self.btnInAppClose.titleLabel.font = FONT_BUTTON;
    [self.btnInAppClose setTitle: LOCALIZATION(C_NOTIFICATION_CLOSE) forState: UIControlStateNormal];            //lokalised
}
-(void) setupNewQuickLinksMenu
{
    self.quickLinksEnumArray = [[NSMutableArray alloc] init];
    self.quickLinksArray = [[NSMutableArray alloc] init];
    
    
    switch (GET_PROFILETYPE)
    {
        case kPROFILETYPE_DSR:
            [self setupDSRQuickLinks];
            break;
        case kPROFILETYPE_TRADEOWNER:
            [self setupTOQuickLinks];
            break;
        case kPROFILETYPE_MECHANIC:
            [self setupMechQuickLinks];
            break;
        default:
            break;
    }
    
    self.quickLinkColViewLayout.itemSize = CGSizeMake((self.quickLinksColView.frame.size.width - 30) / 3, 110);
    self.quickLinksColViewHeight.constant = 110 * ceil([self.quickLinksArray count] / 3.0f);
    [self.quickLinksColView reloadData];
    
    if ([self.quickLinksArray count] == 0)
        self.lblQuickLinksHeader.hidden = YES;
    else
        self.lblQuickLinksHeader.hidden = NO;
}

-(void) setupLocalisation
{
    self.lblQuickLinksHeader.text = LOCALIZATION(C_HOME_QUICKLINKS);                    //lokalised
    self.lblQuickLinksHeader.font = FONT_H1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.quickLinksColView)
        return [self.quickLinksArray count];
    else if (collectionView == self.carouselColView)
        return [self.carouselEnumArray count];
    return 0;
}


typedef enum
{
    kCAROUSEL_PROFILE_HEADER = 1,
    kCAROUSEL_PROFILE_NAME,
    kCAROUSEL_PROFILE_COMPANYNAME,
    
    kCAROUSEL_PROFILE_BUTTON = 999,
} kCAROUSEL_PROFILE_TAG;

typedef enum
{
    kCAROUSEL_PARTNERS_HEADER         = 1,
    kCAROUSEL_PARTNERS_SUBHEADER,
    
    kCAROUSEL_PARTNERS_PROGRESSBAR  = 300,
    
    kCAROUSEL_PARTNERS_BUTTON = 999,
} kCAROUSEL_PARTNERS_TAG;


typedef enum
{
    kCAROUSEL_POINTS_HEADER         = 1,
    kCAROUSEL_POINTS_SUBHEADER,
    kCAROUSEL_POINTS_FOOTER,
    
    kCAROUSEL_POINTS_BAR            = 200,
    
    kCAROUSEL_POINTS_BAR_MIN        = 5,
    kCAROUSEL_POINTS_BAR_MAX,
    
    kCAROUSEL_POINTS_FIRSTPOINTS        = 101,   //0W
    kCAROUSEL_POINTS_SECONDPOINTS,               //ultra
    kCAROUSEL_POINTS_THIRDPOINTS,                //HX8
    
    kCAROUSEL_POINTS_FIRSTLABEL        = 201,   //0W
    kCAROUSEL_POINTS_SECONDLABEL,               //ultra
    kCAROUSEL_POINTS_THIRDLABEL,                //HX8
    
    
    kCAROUSEL_POINTS_BUTTON = 999,
} kCAROUSEL_POINTS_TAG;

typedef enum
{
    kCAROUSEL_PARTNERS_ENDED_HEADER         = 1,
    kCAROUSEL_PARTNERS_ENDED_ICON,
    kCAROUSEL_PARTNERS_ENDED_CONTENT,
    kCAROUSEL_PARTNERS_ENDED_SUBCONTENT,
//    kCAROUSEL_PARTNERS_BUTTON = 999,
} kCAROUSEL_PARTNERS_ENDED_TAG;

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    if (collectionView == self.carouselColView)
    {
        kCAROUSEL_TYPES carouselType =  [[self.carouselEnumArray objectAtIndex: indexPath.row] intValue];
        
        switch (carouselType) {
            case kCAROUSEL_PROFILE:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProfileCell" forIndexPath:indexPath];
                
                UILabel *lblHeader = [cell viewWithTag: kCAROUSEL_PROFILE_HEADER];
                lblHeader.text = LOCALIZATION(C_HEADER_MYPROFILE);                              //lokalised
                lblHeader.font = FONT_H1;
                
                UILabel *lblName = [cell viewWithTag: kCAROUSEL_PROFILE_NAME];
                lblName.text = [[mSession profileInfo] fullName];
                lblName.font = FONT_H(25);
                
                UILabel *lblCompanyName = [cell viewWithTag: kCAROUSEL_PROFILE_COMPANYNAME];
                lblCompanyName.text = [[mSession profileInfo] companyName];
                lblCompanyName.font = FONT_B1;
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_PROFILE_BUTTON];
                [btnView setTitle: LOCALIZATION(C_FORM_VIEWPROFILE) forState:UIControlStateNormal]; //lokalised
                btnView.titleLabel.font = FONT_BUTTON;
            }
                break;
                
            case kCAROUSEL_PARTNERS:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PartnersCell" forIndexPath:indexPath];
                
                UILabel *lblHeader = [cell viewWithTag: kCAROUSEL_PARTNERS_HEADER];
                lblHeader.text = LOCALIZATION(C_SIDEMENU_PARTNERS);                             //lokalised
                
                UILabel *lblSubheader = [cell viewWithTag: kCAROUSEL_PARTNERS_SUBHEADER];
                lblSubheader.text = LOCALIZATION(C_RUSSIA_CAROUSEL_FINALSCORE);                 //lokalised
                RUPartnersBarView *partnersBarView = [cell viewWithTag: kCAROUSEL_PARTNERS_PROGRESSBAR];
//                partnersBarView.currentPoints = [self.carouselPartnerDict objectForKey: @"PointsGained"];
                [partnersBarView.superview setNeedsDisplay];
                
                [partnersBarView loadBar: self.carouselPartnerDict];
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_PARTNERS_BUTTON];
                [btnView setTitle: LOCALIZATION(C_RUSSIA_CAROUSEL_EARNPOINTBTN) forState:UIControlStateNormal];  //loklaised
                btnView.titleLabel.font = FONT_BUTTON;
            }
                break;
              
            case kCAROUSEL_PARTNERSENDED:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PartnersEndedCell" forIndexPath:indexPath];

                UILabel *lblHeader = [cell viewWithTag: 1];
                lblHeader.text = LOCALIZATION(C_SIDEMENU_PARTNERS);                             //lokalised
                
                UILabel *lblThankYou = [cell viewWithTag: 2];
                lblThankYou.text = LOCALIZATION(C_RUSSIA_THANKYOUPARTICIPATION);                //lokalised
                
                UILabel *lblMoreInteresting = [cell viewWithTag: 3];
                lblMoreInteresting.text = LOCALIZATION(C_RUSSIA_MOREINTERESTINGCAMPAIGN);       //lokalised
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_PARTNERS_BUTTON];
                [btnView setTitle: LOCALIZATION(C_RUSSIA_CAROUSEL_EARNPOINTBTN) forState:UIControlStateNormal];     //laoklised
                btnView.titleLabel.font = FONT_BUTTON;
            }
                break;
            case kCAROUSEL_DSRPARTNERS:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DSRPartnersCell" forIndexPath:indexPath];
                
                NSDictionary *cellData = [self.carouselPartnerDict objectForKey:@"PerformanceMetrics"];
                
                UILabel *lblHeader = [cell viewWithTag: kCAROUSEL_POINTS_HEADER];
                lblHeader.text = LOCALIZATION(C_SIDEMENU_PARTNERS);                             //lokalised
                
                UILabel *lblSubheader = [cell viewWithTag: kCAROUSEL_POINTS_SUBHEADER];
                lblSubheader.text = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_HOMECAROUSEL_UPDATEDON), [cellData objectForKey: @"LastUpdateDate"] ];  //lokalised
                
                
                UILabel *lblFirstKey = [cell viewWithTag: kCAROUSEL_POINTS_FIRSTLABEL];
                lblFirstKey.text = [cellData objectForKey: @"PerformanceKeyOne"];
                UILabel *lblFirstValue = [cell viewWithTag: kCAROUSEL_POINTS_FIRSTPOINTS];
                lblFirstValue.text = [[cellData objectForKey: @"PerformanceValueOne"] stringValue];
                
                UILabel *lblSecondKey = [cell viewWithTag: kCAROUSEL_POINTS_SECONDLABEL];
                lblSecondKey.text = [cellData objectForKey: @"PerformanceKeyTwo"];
                UILabel *lblSecondValue = [cell viewWithTag: kCAROUSEL_POINTS_SECONDPOINTS];
                lblSecondValue.text = [[cellData objectForKey: @"PerformanceValueTwo"] stringValue];
                
                UILabel *lblThirdKey = [cell viewWithTag: kCAROUSEL_POINTS_THIRDLABEL];
                lblThirdKey.text = [cellData objectForKey: @"PerformanceKeyThree"];
                UILabel *lblThirdValue = [cell viewWithTag: kCAROUSEL_POINTS_THIRDPOINTS];
                lblThirdValue.text = [[cellData objectForKey: @"PerformanceValueThree"] stringValue];
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_POINTS_BUTTON];
                
                [btnView setTitle: LOCALIZATION(C_RU_CAROUSEL_VIEWPERFORMANCE) forState:UIControlStateNormal];      //lokalised
                btnView.titleLabel.font = FONT_BUTTON;
            }
                break;
            case kCAROUSEL_POINTS:
            {
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PointsCell" forIndexPath:indexPath];
                
                UILabel *lblHeader = [cell viewWithTag: kCAROUSEL_POINTS_HEADER];
                lblHeader.text = LOCALIZATION(C_TITLE_MYPOINTS);                                                    //lokalised
                
                UILabel *lblSubheader = [cell viewWithTag: kCAROUSEL_POINTS_SUBHEADER];
                lblSubheader.text = LOCALIZATION(C_RUSSIA_CAROUSEL_FINALSCORE);                                     //lokalised
                
                RUProgressBarView *progressBarView = [cell viewWithTag: 200];
                
                UILabel *lblMinPoints = [cell viewWithTag: 5];
                
                lblMinPoints.text = [NSString stringWithFormat: @"%@ %@",
                                     @"0", LOCALIZATION(C_REWARDS_PTS)];                                            //lokalised
                
                UILabel *lblMaxPoints = [cell viewWithTag: 6];
                
                lblMaxPoints.text = [NSString stringWithFormat: @"%@ %@", [self.carouselPointsDict objectForKey: @"MaxPoints"], LOCALIZATION(C_REWARDS_PTS)];   //lokalised
                
                progressBarView.maxPoints = [[self.carouselPointsDict objectForKey: @"MaxPoints"] intValue];
                
                if (![[self.carouselPointsDict objectForKey: @"ProductPoints"] isKindOfClass: [NSNull class]])
                {
                    NSArray *productPoints = [self.carouselPointsDict objectForKey: @"ProductPoints"];
                    for (int i = 0; i < [productPoints count]; i++)
                    {
                        NSDictionary *productData = [productPoints objectAtIndex: i];
                        UILabel *lblPoints, *lblPointsData;
                        switch (i)
                        {
                            case 0:
                            {
                                progressBarView.firstPoints = [[productData objectForKey: @"Points"] intValue];
                                lblPoints = [cell viewWithTag: 201];
                                lblPointsData = [cell viewWithTag: 101];

                            }
                                break;
                            case 1:
                            {
                                progressBarView.secondPoints = [[productData objectForKey: @"Points"] intValue];
                                lblPoints = [cell viewWithTag: 202];
                                lblPointsData = [cell viewWithTag: 102];
                            }
                                break;
                            case 2:
                            {
                                progressBarView.thirdPoints = [[productData objectForKey: @"Points"] intValue];
                                lblPoints = [cell viewWithTag: 203];
                                lblPointsData = [cell viewWithTag: 103];
                            }
                                break;
                            default:
                                break;
                        }
                        lblPoints.text = [productData objectForKey: @"GroupName"];
                        lblPointsData.text = [NSString stringWithFormat: @"%@ %@", [productData objectForKey: @"Points"], LOCALIZATION(C_REWARDS_PTS)];     //lokalised
                    }
                }
                else
                {
                    progressBarView.firstPoints = 0;
                    progressBarView.secondPoints = 0;
                    progressBarView.thirdPoints = 0;
                }

                [progressBarView reloadBar];
                
                UILabel *lblFooter = [cell viewWithTag: kCAROUSEL_POINTS_FOOTER];
                
                //lookup
                NSDictionary *partnersLookupTable = [[mSession lookupTable] objectForKey: @"PartnersShellClub"];
                NSString *unitString = [partnersLookupTable objectForKey: @"Unit"];
                
                if ([[self.carouselPointsDict objectForKey: @"PointsReqToReachMileStone"] intValue] > 0)
                {
                    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat: @"%@%@ ", [self.carouselPointsDict objectForKey: @"PointsReqToReachMileStone"], unitString] attributes:@{NSForegroundColorAttributeName: COLOUR_RED, NSFontAttributeName: FONT_H1, }];
                    
                    NSAttributedString *remainingStringAttr = [[NSAttributedString alloc] initWithString: [self.carouselPointsDict objectForKey: @"SummaryText"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY, NSFontAttributeName: FONT_B3,}];
                    
                    [attrString appendAttributedString: remainingStringAttr];
                    
                    lblFooter.attributedText = attrString;
                }
                else
                {
                    lblFooter.attributedText = [[NSAttributedString alloc] initWithString: [self.carouselPointsDict objectForKey: @"SummaryText"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY, NSFontAttributeName: FONT_B3,}];
                }
                
                
                
                UIButton *btnView = [cell viewWithTag: kCAROUSEL_POINTS_BUTTON];
                
                if ([[mSession profileInfo] hasMyRewards])
                    btnView.hidden = NO;
                else
                    btnView.hidden = YES;
                [btnView setTitle: LOCALIZATION(C_HOMECAROUSEL_REDEEMPOINTS) forState:UIControlStateNormal];            //lokalised
                btnView.titleLabel.font = FONT_BUTTON;
            }
                
                break;
            default:
                break;
        }
    }
    else if (collectionView == self.quickLinksColView)
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QuickLinkCell" forIndexPath:indexPath];
        
        cell.tag = [[self.quickLinksEnumArray objectAtIndex: indexPath.row] intValue];
        
        NSDictionary *cellData = [self.quickLinksArray objectAtIndex: indexPath.row];
        
        UIImageView *cellImgView = [cell viewWithTag: 1]; //tagged in storyboard
        cellImgView.image = [UIImage imageNamed: [cellData objectForKey: @"Icon"]];
        
        
        UILabel *cellLbl = [cell viewWithTag: 2]; //tagged in storyboard
        cellLbl.font = FONT_B2;
        cellLbl.text = [cellData objectForKey: @"Label"];
        
        
        //if not last column, show right line
        UIView *rightLine = [cell viewWithTag: 11];
        switch (indexPath.row % 3)
        {
                //        case 0:
                //            break;
                //        case 1:
                //            break;
            case 2: //last row
                rightLine.hidden = YES;
                break;
            default:
                rightLine.hidden = NO;
                break;
        }
        
        //if not last row, show bottom line
        UIView *bottomLine = [cell viewWithTag: 10];
        //check for remainder
        if ([self.quickLinksArray count] % 3 == 0) //perfect multiples of 3 row
        {
            //indexpath starts 0, count must - 1
            //check if last 3, hide if true
            //row index has to be more than count - 3
            if (indexPath.row > abs([self.quickLinksArray count] - 1) - 3)
                bottomLine.hidden = YES;
            else
                bottomLine.hidden = NO;
        }
        else
        {
            //indexpath starts 0, count must - 1
            //check if last row (remainder), hide if true
            //row index has to be more than total count - remainder
            if (indexPath.row >= [self.quickLinksArray count] - ([self.quickLinksArray count] % 3))
            {
                //last row
                bottomLine.hidden = YES;
                
                //total quick link only 1 element OR
                //last element is middle element
                if ([self.quickLinksArray count] == 1)
                    rightLine.hidden = YES;
                else if (indexPath.row % 3 == 1 &&  [self.quickLinksArray count] < 3)
                    rightLine.hidden = YES;
            }
            else
                bottomLine.hidden = NO;
            
            
        }
    }
    return cell;
}

-(NSString *) addCommaThousandSeparatorsWithInputString: (NSString *) inputString
{
    
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setNumberStyle:NSNumberFormatterDecimalStyle]; // to get commas (or locale equivalent)
    //    [fmt setMinimumFractionDigits: 2]; // must have 2 decimal place
    //    [fmt setMaximumFractionDigits: 2];
    
    
    //convert inputString to double value
    //double value to decimal style string
    return [fmt stringFromNumber:@([inputString doubleValue])];
}

-(NSString *) convertDate: (NSString *) inputString
{
    //convert string to date
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat: @"dd-MM-yyyy"];
    [inputFormatter setTimeZone: [NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *inputDate = [inputFormatter dateFromString: inputString];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat: @"MMM yy"];
    [outputFormatter setTimeZone: [NSTimeZone timeZoneWithName:@"GMT"]];
    
    //convert inputString to double value
    //double value to decimal style string
    return [outputFormatter stringFromDate: inputDate];
}


-(void) setupDSRQuickLinks
{
    if ([[mSession profileInfo] hasQLHasManageWorkshop])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_DSRMANAGEWORKSHOP)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_TITLE_MANAGEWORKSHOP),              //lokalised
                                           @"Icon": @"quicklinks_manageworkshops_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasRUPerformance])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_RU_WORKSHOPPERFORMANCE)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_PARTNERS),                 //lokalised
                                           @"Icon": @"quicklinks_partnersofshell_midgrey.png",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasAsset])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYLIBRARY)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_MYLIBRARY),            //lokalised
                                           @"Icon": @"quicklinks_mylibrary_midgrey",
                                           }];
    }
    
    [self.quickLinksColView reloadData];
}

-(void) setupTOQuickLinks
{
    if ([[mSession profileInfo] hasPartnersOfShellClub])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_PARTNERSOFSHELL)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_PARTNERS),             //lokalised
                                           @"Icon": @"quicklinks_partnersofshell_midgrey.png",
                                           }];
    }
    
    if ([[mSession profileInfo]hasOnePlusOne]) {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_SPIRAXGADUSONEPLUSONE)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(TITLE_SPIRAX_GADUS_ONE_PLUS_ONE),             //lokalised
                                           @"Icon": @"quicklinks_partnersofshell_midgrey.png",
                                           }];
    }
    
    if ([[mSession profileInfo] hasSearchVehicleID])
    {
        //oil change to use search vehicle id flag
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_OILCHANGEREGISTRATION)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_RUSSIA_OILCHANGEREGISTRATION),  //lokalised
                                           @"Icon": @"sideicon_searchvehicleid_midgrey.png",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasAsset])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYLIBRARY)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_MYLIBRARY),           //lokalised
                                           @"Icon": @"quicklinks_mylibrary_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasWorkShopOffer])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_WORKSHOPOFFERS)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_TITLE_WORKSHOPOFFER),       //lokalised
                                           @"Icon": @"sideicon_workshopoffers_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasMYPerformance])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_WORKSHOPPERFORMANCE)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_PERFORMANCE_WORKSHOP),      //lokalised
                                           @"Icon": @"sideicon_performance_midgrey",
                                           }];
    }
    
    if ([[mSession profileInfo] hasQLHasRUPerformance])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_RU_WORKSHOPPERFORMANCE)];
        
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_PERFORMANCE_WORKSHOP),      //lokalised
                                       @"Icon": @"sideicon_performance_midgrey",
                                       }];
    }
}
-(void) setupMechQuickLinks
{
    if (![[mSession profileInfo] redeemOnly])//redeem only. add menu if false)
    {
        if ([[mSession profileInfo] hasSearchVehicleID])
        {
            //oil change to use search vehicle id flag
            [self.quickLinksEnumArray addObject: @(kQUICKLINK_OILCHANGEREGISTRATION)];
            [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_RUSSIA_OILCHANGEREGISTRATION),  //lokalised
                                               @"Icon": @"sideicon_searchvehicleid_midgrey.png",
                                               }];
        }
    }
    
    if ([[mSession profileInfo] hasQLHasAsset])
    {
        [self.quickLinksEnumArray addObject: @(kQUICKLINK_MYLIBRARY)];
        [self.quickLinksArray addObject: @{@"Label": LOCALIZATION(C_SIDEMENU_MYLIBRARY),            //lokalised
                                           @"Icon": @"quicklinks_mylibrary_midgrey",
                                           }];
    }
    
}


- (IBAction)viewProfilePressed:(id)sender {
    [UpdateHUD addMBProgress: KEY_WINDOW withText:LOADING_HUD];
    
    if (GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
        [mSession loadProfileTabView];
    else
        [mSession loadMyProfileView];
}

- (IBAction)earnPointsPressed:(id)sender {
//    [mSession loadRUEarnPoints];
    
    [mSession pushRUEarnPoints: self];
}

- (IBAction)viewPointsPressed:(id)sender {
    [mSession loadRewardsView];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath: indexPath];
    if (collectionView == self.carouselColView)
    {
        
    }
    else if (collectionView == self.quickLinksColView)
    {
        switch (cell.tag)
        {
            case kQUICKLINK_WORKSHOPOFFERS:
                [mSession loadWorkshopOfferView];
                break;
            case kQUICKLINK_OILCHANGEREGISTRATION:
                //same as kQUICKLINK_SEARCHVEHICLEID
                [mSession loadSearchVehicleView];
                break;
            case kQUICKLINK_PARTNERSOFSHELL:
                [mSession loadRUPartners];
                break;
            case kQUICKLINK_DSRMANAGEWORKSHOP:
                [mSession loadManageWorkshopView];
                break;
            case kQUICKLINK_MYLIBRARY:
                [mSession loadMyLibraryView];
                break;
            case kQUICKLINK_SCANDECAL:
                [mSession loadSearchVehicleViewWithDecal: nil];
                break;
            case kQUICKLINK_RU_WORKSHOPPERFORMANCE:
                [mSession loadRUWorkshopPerformance];
                break;
            default:
                break;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.carouselColView)
        self.carouselPageControl.currentPage = scrollView.contentOffset.x / scrollView.frame.size.width;
}

- (void) processSurvey
{
    CGSize scrollContentSize = self.scrollView.contentSize;
    self.surveyView.frame = CGRectMake(5, 5, scrollContentSize.width, scrollContentSize.height);
    [self.view addSubview: self.surveyView];
    
#pragma mark - Adding WKWebView
    NSString *urlAddress = [NSString stringWithFormat:@"%@%@%@?sid=%@&ss=1",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_SURVEYFORM_URL, [[mSession profileInfo] surveyId]];
    
    self.webViewHelper = [[WebViewHelper alloc] init];
    [self.webViewHelper setWebViewWithParentView: self.surveyView
                         navigationDelegate: self
                                 urlAddress: urlAddress];
    [self.webViewHelper setWebConfiguration: WKDataDetectorTypeLink];
    
    self.surveyCloseButton.frame = CGRectMake(SCREEN_WIDTH - 60, 0, 20, 20);
    [self.surveyView addSubview:self.surveyCloseButton];
}


- (void) processInAppNotification
{
    [UpdateHUD removeMBProgress: KEY_WINDOW];
    
    if ([self.inAppIdArray count] > 0)
    {
        self.inAppNotificationView.frame = CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview: self.inAppNotificationView];
        
#pragma mark - Adding WKWebView
        NSString *urlAddress = [NSString stringWithFormat:@"%@%@%@?pnid=%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_INAPPNOTIFICATION_URL, [self.inAppIdArray objectAtIndex: 0]];
        
        
        WebViewHelper *webViewHelper = [[WebViewHelper alloc] init];
        [webViewHelper setWebViewWithParentView: self.inAppWebViewContainer
                             navigationDelegate: self
                                     urlAddress: urlAddress];
        self.webViewHelper.webView.frame = CGRectMake(0, 0,self.view.frame.size.width - 50, self.inAppNotificationView.frame.size.height - 160);
        [webViewHelper setWebConfiguration: WKDataDetectorTypeLink];  //default
    }
    else
    {
        if ([[mSession profileInfo] showSurveyPp])
        {
            [self processSurvey];
        }
    }
}

- (IBAction)readMoreInApp:(id)sender {
    //push to notification list > notification details with ID
    [mSession handlePushNotificationWithReferenceKey: @"NL"
                                      referenceValue: [self.inAppIdArray objectAtIndex: 0]];
}

- (IBAction)closeInApp:(id)sender {
    
    //call read api
    [[WebServiceManager sharedInstance] readPushMessage: [self.inAppIdArray objectAtIndex: 0] vc: nil];
    
    //remove first object
    [self.inAppIdArray removeObjectAtIndex: 0];
    
    //"close" notification, proceed to see next notification
    [self.inAppNotificationView removeFromSuperview];
    
    //check for more inapp push, otherwise, survey or close
    [UpdateHUD addMBProgress: KEY_WINDOW withText: @""];
    [self performSelector: @selector(processInAppNotification) withObject:nil afterDelay:0.1f];
}

- (IBAction) callPressed:(id)sender {
//    NSString *csNumberString =  [[mSession lookupTable] objectForKey: @"ContactUSCSNumber"];
//
//    //need to remove space
//    NSString *callUrlString = [NSString stringWithFormat: @"tel:%@",
//                               [csNumberString stringByReplacingOccurrencesOfString: @" " withString: @""]];
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: callUrlString]];
}

- (IBAction)closeSurvey:(id)sender {
    if (![self.webViewHelper.webView.URL.absoluteString containsString: @"SurveyThanks.aspx"])
    {
        [[WebServiceManager sharedInstance] closeSurveyPopup: @{
                                                                @"SurveyID": [[mSession profileInfo] surveyId],
                                                                @"IsShowPP": @(NO),
                                                                }
                                                          vc: nil];
    }
    
    [self.surveyView removeFromSuperview];
}


-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_FETCHHOME:
        {
            SET_HOMEDATA([response getGenericResponse]);
            [self setFetchNewHomeData];
        }
            break;
        default:
            break;
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_FETCHHOME:
        {
            if(GET_HOMEDATA)
            {
                [self setFetchNewHomeData];
            }
        }
            break;
        default:
            break;
    }
}

-(void) setFetchNewHomeData
{
    NSDictionary *respData = GET_HOMEDATA;
    ProfileInfo *profileInfo = [[ProfileInfo alloc] initWithData: [respData objectForKey: @"MyProfile"]];
    [mSession setProfileInfo: profileInfo];
    
    self.carouselPartnerDict = [respData objectForKey: @"MyPartnersClub"];
    self.carouselPointsDict = [respData objectForKey: @"MyPoints"];
    self.carouselEnumArray = [[NSMutableArray alloc] init];

    if ([[mSession profileInfo] hasCSHasMyProfile])
        [self.carouselEnumArray addObject: @(kCAROUSEL_PROFILE)];
    
    if ([[mSession profileInfo ] hasCSHasDSRPartnersOfShellClub])
    {
        [self.carouselEnumArray addObject: @(kCAROUSEL_DSRPARTNERS)];
    }
    
    if ([[mSession profileInfo] hasCSHasPartnersOfShellClub])
    {
        if ([[self.carouselPartnerDict objectForKey: @"IsPromotionActive"] boolValue])
            [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERS)];
        else
            [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERSENDED)];
    }
    
    if ([[mSession profileInfo] hasCSHasInventorySpeedometer])
    {
        if ([[self.carouselPartnerDict objectForKey: @"IsPromotionActive"] boolValue])
            [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERS)];
        else
            [self.carouselEnumArray addObject: @(kCAROUSEL_PARTNERSENDED)];
    }
    
    if ([[mSession profileInfo] hasCSHasMyPoints])
        [self.carouselEnumArray addObject: @(kCAROUSEL_POINTS)];
    
    self.carouselPageControl.numberOfPages = [self.carouselEnumArray count];
    [self.carouselColView reloadData];
    //update to ProfileData
    [[NSNotificationCenter defaultCenter] postNotificationName: @"UpdateProfileInfo" object: nil];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"Unread Icon Changed" object: nil];
    [self setupNewQuickLinksMenu];
    
    //read notification
    [self.inAppIdArray removeAllObjects];
    
    //Add In App Notification below change password and TNC
    if ([[[respData objectForKey: @"MyProfile"] objectForKey: @"HasPushNotif"] boolValue])
    {
        self.inAppIdArray = [[respData objectForKey: @"MyProfile"] objectForKey: @"PushNotifList"];
        [self processInAppNotification];
    }
    //Add survey below change password and TNC
    else if ([[[respData objectForKey: @"MyProfile"] objectForKey: @"ShowSurveyPP"] boolValue])
    {
        [self processSurvey];
    }
    
    //TNC priority first. view hierachy: Home > Change Password > TNC
    //check for change password. show if need.
    if (![[[respData objectForKey: @"MyProfile"] objectForKey: @"ChangePassword"] boolValue])
    {
        [mSession pushChangePasswordView: GET_LOGIN_NAME showBackBtn:YES vc:self];
    }
    
    //    check for TNC. 1/YES = accepted.
    if (![[[respData objectForKey: @"MyProfile"] objectForKey: @"TermCondition"] boolValue])
    {
        if (GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER)
        {
            [mSession pushTNCView:YES tncType: 1 vc:self];
        }
        else
        {
            [mSession pushTNCView:YES tncType: 2 vc:self];
        }
    }
    
    //Timer Setup to auto rotate and idle
    self.autoRotate_Timer = [NSTimer scheduledTimerWithTimeInterval:5
                                                             target:self
                                                           selector:@selector(scrollPages)
                                                           userInfo:Nil
                                                            repeats:YES];
    
    self.idle_Timer = [NSTimer scheduledTimerWithTimeInterval:5
                                                       target:self
                                                     selector:@selector(startTimer)
                                                     userInfo:Nil
                                                      repeats:YES];
    
    [self performSelector:@selector(startTimer) withObject:nil afterDelay:2];
}

- (IBAction)dsrViewPerformancePressed:(id)sender
{
    [mSession loadRUWorkshopPerformance];
}

- (IBAction)firstKeyPressed:(id)sender
{
    [mSession loadRUWorkshopPerformanceFromCarousel: @"Order BreakDown"];
}

- (IBAction)secondKeyPressed:(id)sender
{
    [mSession loadRUWorkshopPerformanceFromCarousel: @"Oil Change BreakDown"];
}

- (IBAction)thirdKeyPressed:(id)sender
{
    [mSession loadRUWorkshopPerformanceFromCarousel: @"Customer Break BreakDown"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - AutoRotation of Carousel

-(void) startTimer {
    [self.idle_Timer invalidate];
    
    self.autoRotate_Timer = [NSTimer scheduledTimerWithTimeInterval:5
                                                             target:self
                                                           selector:@selector(scrollPages)
                                                           userInfo:Nil
                                                            repeats:YES];
    self.currentPage_Carousel = self.carouselPageControl.currentPage;
    [self.autoRotate_Timer fire];
    
}

-(void)scrollPages{
    self.currentPage_Carousel = (self.carouselPageControl.currentPage + 1) % self.carouselPageControl.numberOfPages;
    self.carouselPageControl.currentPage = self.currentPage_Carousel;
    [self.carouselColView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.currentPage_Carousel inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if(self.idle_Timer.isValid) {
        [self.idle_Timer invalidate];
        self.idle_Timer = [NSTimer scheduledTimerWithTimeInterval:5
                                                           target:self
                                                         selector:@selector(startTimer)
                                                         userInfo:Nil
                                                          repeats:YES];
    }
    if(self.autoRotate_Timer.isValid) {
        [self.autoRotate_Timer invalidate];
    }
    
    [self.idle_Timer fire];
}


@end
