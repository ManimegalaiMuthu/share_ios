//
//  RUValueCalculatorTableViewCell.h
//  Shell
//
//  Created by Jeremy Lua on 13/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "ASTableViewCell.h"

@interface RUValueCalculatorTableViewCell : ASTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblProductPointsName;
@property (weak, nonatomic) IBOutlet UILabel *lblProductPoints;

@property (weak, nonatomic) IBOutlet UILabel *lblMinPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblMaxPoints;
@property (weak, nonatomic) IBOutlet UITextField *txtUnits;
@property NSString *unit;
@property BOOL isLoaded; 
@end
