//
//  RUProgressBarView.h
//  Shell
//
//  Created by Jeremy Lua on 24/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RUProgressBarView : UIView

@property (strong, nonatomic) IBOutlet UIView *contentView;

@property CGFloat maxPoints;
@property CGFloat firstPoints;
@property CGFloat secondPoints;
@property CGFloat thirdPoints;

//-(instancetype) initWithTotalPoints:(CGFloat)totalPoints
//                        firstPoints:(CGFloat)firstPoints
//                       secondPoints:(CGFloat)secondPoints
//                        thirdPoints:(CGFloat)thirdPoints;
-(void) reloadBar;

@end
