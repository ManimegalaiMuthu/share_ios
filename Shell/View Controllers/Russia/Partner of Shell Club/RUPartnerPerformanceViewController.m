//
//  RUPartnerPerformanceViewController.m
//  Shell
//
//  Created by Jeremy Lua on 27/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUPartnerPerformanceViewController.h"

@interface RUPartnerPerformanceViewController () <UITableViewDelegate, UITableViewDataSource, WebServiceManagerDelegate>


@property (weak, nonatomic) IBOutlet UITableView *pointsTableView;
@property NSMutableArray *performanceArray;
@end

@implementation RUPartnerPerformanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.performanceArray = [[NSMutableArray alloc] init];
    
    if(@available(iOS 11, *))
    {
        self.pointsTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }

//    [self.pointsTableView setContentInset:UIEdgeInsetsMake(0, 15, 0, 15)];

    [[WebServiceManager sharedInstance] fetchPerformanceMetrics: self];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"HeaderCell"];
    
    NSDictionary *sectionData = [self.performanceArray objectAtIndex: section];
    
    UILabel *lblTitle = [cell viewWithTag: 1];
    lblTitle.text = [sectionData objectForKey: @"GroupName"];
    
    UILabel *lblVolume = [cell viewWithTag: 3];
    lblVolume.text = LOCALIZATION(C_RUSSIA_VOLUME);         //loaklised
    
    
    [cell.contentView setBackgroundColor: COLOUR_VERYPALEGREY];
    return cell.contentView;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.performanceArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"PointsCell"];
    
    NSDictionary *sectionData = [self.performanceArray objectAtIndex: indexPath.section];
    NSArray *sectionProductListArray = [sectionData objectForKey: @"Items"];
    NSDictionary *cellData = [sectionProductListArray objectAtIndex: indexPath.row];
    
    UILabel *lblName = [cell viewWithTag: 1];
    lblName.text = [cellData objectForKey: @"ProductName"];
    
    UILabel *lblPoint = [cell viewWithTag: 2];
    lblPoint.text = [cellData objectForKey: @"ProductPoints"];
    
    if (indexPath.row % 2 == 0)
    {
        [cell.contentView setBackgroundColor: COLOUR_WHITE];
    }
    else
    {
        [cell.contentView setBackgroundColor: COLOUR_VERYPALEGREY];
    }
    
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary *cellData = [self.performanceArray objectAtIndex: section];
    NSArray *productArray = [cellData objectForKey:@"Items"];
    return [productArray count];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_PERFORMANCEMETRICS:
        {
            NSDictionary *respData = [response getGenericResponse];
            self.performanceArray = [respData objectForKey: @"MyPerformance"];
            
            [self.pointsTableView reloadData];
        }
            break;
            
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
