//
//  RUPartnerTabViewController.h
//  Shell
//
//  Created by Jeremy Lua on 27/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RDVTabBarController.h"
#import "Helper.h"

@interface RUPartnerTabViewController : RDVTabBarController

@end
