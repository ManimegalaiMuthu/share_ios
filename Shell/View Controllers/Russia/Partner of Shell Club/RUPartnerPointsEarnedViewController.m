//
//  RUPartnerPointsEarnedViewController.m
//  Shell
//
//  Created by Jeremy Lua on 27/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUPartnerPointsEarnedViewController.h"

@interface RUPartnerPointsEarnedViewController () <UITableViewDelegate, UITableViewDataSource, WebServiceManagerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPointsData;
@property (weak, nonatomic) IBOutlet UILabel *lblFinalScore;
@property (weak, nonatomic) IBOutlet UIButton *btnEarnPoints;

@property (weak, nonatomic) IBOutlet UITableView *pointsTableView;
@property NSMutableArray *pointsArray;
@end

@implementation RUPartnerPointsEarnedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //total points data to change "pts" to FONT_B3
    self.lblTotalPointsData.text = @"0.0";
    
    self.pointsTableView.rowHeight = UITableViewAutomaticDimension;
    self.pointsTableView.estimatedRowHeight = 30;
    self.pointsTableView.tableFooterView = [UIView new];
    
    [self.btnEarnPoints setTitle: LOCALIZATION(C_RUSSIA_CAROUSEL_EARNPOINTBTN) forState: UIControlStateNormal];     //lokalised
    
    self.lblTotalPoints.text = LOCALIZATION(C_RUSSIA_TOTAL_POINT);              //loaklised
    self.lblFinalScore.text = LOCALIZATION(C_RUSSIA_CAROUSEL_FINALSCORE);       //lokalised
    
    [[WebServiceManager sharedInstance] fetchPerformanceMetrics: self];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.pointsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"PointsCell"];
    
    NSDictionary *cellData = [self.pointsArray objectAtIndex: indexPath.row];
    
    UILabel *lblName = [cell viewWithTag: 1];
    lblName.text = [cellData objectForKey: @"ProductName"];
    
    UILabel *lblDate = [cell viewWithTag: 2];
    lblDate.text = [cellData objectForKey: @"EarnedDate"];
    
    
    UILabel *lblPoints = [cell viewWithTag: 3];
    lblPoints.text = [NSString stringWithFormat: @"+%.0f %@", [[cellData objectForKey: @"ProductPoints"] floatValue], LOCALIZATION(C_REWARDS_PTS)];  //lokalised
    
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    return cell;
}

- (IBAction)earnPointsPressed:(id)sender {
    [mSession pushRUEarnPoints: self.parentViewController.parentViewController];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) { 
        case kWEBSERVICE_PERFORMANCEMETRICS:
        {
            NSDictionary *respData = [[response getGenericResponse] objectForKey: @"EarnedPoints"];
            self.pointsArray = [respData objectForKey: @"Points"];
            
            self.lblTotalPointsData.text = [NSString stringWithFormat: @"%.0f %@", [[respData objectForKey: @"TotalPoints"] floatValue], LOCALIZATION(C_REWARDS_PTS)]; //lokalised
            
            
            
            [self.pointsTableView reloadData];
        }
            break;
            
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
