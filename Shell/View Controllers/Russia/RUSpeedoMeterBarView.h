//
//  RUSpeedoMeterBarView.h
//  Shell
//
//  Created by MUTHUPANDIYAN Manimegalai on 24/9/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RUSpeedoMeterBarView : UIView

@property (strong, nonatomic) IBOutlet UIView *contentView;

@property CGFloat percentage;
@property NSString* speedoMeterText;
@property NSString* milestoneOne;
@property NSString* milestoneTwo;
@property NSString* milestoneThree;
@property NSString* milestoneFour;
@property NSString* milestoneFive;


-(void) loadBar: (NSDictionary *) responseData;

@end

NS_ASSUME_NONNULL_END
