//
//  EarnPointsTabViewController.h
//  Shell
//
//  Created by Nach on 11/9/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "RDVTabBarController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RUEarnPoints_V2TabViewController : RDVTabBarController

@end

NS_ASSUME_NONNULL_END
