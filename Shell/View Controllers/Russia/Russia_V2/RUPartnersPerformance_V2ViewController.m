//
//  RUPartnersPerformance_V2ViewController.m
//  Shell
//
//  Created by Nach on 9/9/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "RUPartnersPerformance_V2ViewController.h"
#import "RUPartnersBarView.h"

@interface RUPartnersPerformance_V2ViewController ()   <WebServiceManagerDelegate,UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *collectionViewFlowLayout;

@property NSDictionary *campaignDict;

@end

@implementation RUPartnersPerformance_V2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.lblHeader.text = LOCALIZATION(C_PARTNERSOFSHELL_PERFORMANCE_HEADER);
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    self.lblHeader.font = FONT_B1;
    
    self.campaignDict = GET_HOMEDATA;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION(C_SIDEMENU_PARTNERS),LOCALIZATION(C_TITLE_PERFORMANCE)] screenClass:nil];
    [self.collectionView reloadData];
}

enum {
    kCAMPAIGNCELL_HEADER = 1,
    kCAMPAIGNCELL_PROGRESSVIEW,
    kCAMPAIGNCELL_TOTALORDERED_LBL,
    kCAMPAIGNCELL_TOTALORDERED_DATA,
    kCAMPAIGNCELL_EARNEDPOINTS_LBL,
    kCAMPAIGNCELL_EARNEDPOINTS_DATA,
    kCAMPAIGNCELL_FOOTER,
    
    kCAMPAIGNCELL_BTN = 999,
}kCAMPAIGNCELL_TAG;

#pragma mark - Collection View
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    switch([[mSession profileInfo] businessType]) {
        case 1:
            return 1;
            break;
        case 2:
            return 1;
            break;
        case 3:
            return 2;
            break;
        default:
            break;
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(SCREEN_WIDTH,400);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CampaignCollectionViewCell" forIndexPath:indexPath];
    
    NSDictionary *cellData;
    NSString *totalOrdered = @"";
    NSString *earnedPoints = @"";
    NSString *unitString = @"";
    
    UILabel *lblHeader = [cell viewWithTag:kCAMPAIGNCELL_HEADER];
    lblHeader.textColor = COLOUR_VERYDARKGREY;
    lblHeader.font = FONT_H1;
    
    UIButton *btnViewBreakdown = [cell viewWithTag:kCAMPAIGNCELL_BTN];
    [btnViewBreakdown setBackgroundColor:COLOUR_RED];
    btnViewBreakdown.titleLabel.font = FONT_BUTTON;
    [btnViewBreakdown setTitle:LOCALIZATION(C_LOYALTY_VIEWBREAKDOWN) forState:UIControlStateNormal];
       
    switch([[mSession profileInfo] businessType]) {
        case 1:
        {
            cellData = @{@"BusinessType" : @(1),@"Data" : [self.campaignDict objectForKey: @"MyPartnersClubB2B"]};
            NSDictionary *dict = [self.campaignDict objectForKey:@"MyPointsB2B"];
            totalOrdered = [NSString stringWithFormat:@"%@",[dict objectForKey:@"TotalPoints"]];
            dict = [self.campaignDict objectForKey:@"MySalesB2B"];
            earnedPoints = [NSString stringWithFormat:@"%@",[dict objectForKey:@"TotalVolumeOrdered"]];
            dict = [[mSession lookupTable] objectForKey: @"PartnersShellClubB2B"];
            unitString = [dict objectForKey:@"Unit"];
            
            lblHeader.text = LOCALIZATION(C_RU_B2B_CAMPAIGN);
            [btnViewBreakdown addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(b2bBreakdownPressed:)]];
        }
            break;
        case 2:
        {
            cellData = @{@"BusinessType" : @(2),@"Data" : [self.campaignDict objectForKey: @"MyPartnersClubB2C"]};
            NSDictionary *dict = [self.campaignDict objectForKey:@"MyPointsB2C"];
            totalOrdered = [NSString stringWithFormat:@"%@",[dict objectForKey:@"TotalPoints"]];
            dict = [self.campaignDict objectForKey:@"MySalesB2C"];
            earnedPoints = [NSString stringWithFormat:@"%@",[dict objectForKey:@"TotalVolumeOrdered"]];
            dict = [[mSession lookupTable] objectForKey: @"PartnersShellClubB2C"];
            unitString = [dict objectForKey:@"Unit"];
            
            lblHeader.text = LOCALIZATION(C_RU_B2C_CAMPAIGN);
            [btnViewBreakdown addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(b2cBreakdownPressed:)]];
        }
            break;
        case 3:
        {
            if(indexPath.row == 0) {
                cellData = @{@"BusinessType" : @(1),@"Data" : [self.campaignDict objectForKey: @"MyPartnersClubB2B"]};
                NSDictionary *dict = [self.campaignDict objectForKey:@"MyPointsB2B"];
                totalOrdered = [NSString stringWithFormat:@"%@",[dict objectForKey:@"TotalPoints"]];
                dict = [self.campaignDict objectForKey:@"MySalesB2B"];
                earnedPoints = [NSString stringWithFormat:@"%@",[dict objectForKey:@"TotalVolumeOrdered"]];
                dict = [[mSession lookupTable] objectForKey: @"PartnersShellClubB2B"];
                unitString = [dict objectForKey:@"Unit"];
            } else {
                cellData = @{@"BusinessType" : @(2),@"Data" : [self.campaignDict objectForKey: @"MyPartnersClubB2C"]};
                NSDictionary *dict = [self.campaignDict objectForKey:@"MyPointsB2C"];
                totalOrdered = [NSString stringWithFormat:@"%@",[dict objectForKey:@"TotalPoints"]];
                dict = [self.campaignDict objectForKey:@"MySalesB2C"];
                earnedPoints = [NSString stringWithFormat:@"%@",[dict objectForKey:@"TotalVolumeOrdered"]];
                dict = [[mSession lookupTable] objectForKey: @"PartnersShellClubB2C"];
                unitString = [dict objectForKey:@"Unit"];
            }
            
            if(indexPath.row == 0) {
                lblHeader.text = LOCALIZATION(C_RU_B2B_CAMPAIGN);
                [btnViewBreakdown addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(b2bBreakdownPressed:)]];
            } else {
                lblHeader.text = LOCALIZATION(C_RU_B2C_CAMPAIGN);
                [btnViewBreakdown addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(b2cBreakdownPressed:)]];
            }
        
        }
            break;
        default:
            break;
    }
    
    RUPartnersBarView *partnersBarView = [cell viewWithTag: kCAMPAIGNCELL_PROGRESSVIEW];
    [partnersBarView.superview setNeedsDisplay];
    [partnersBarView loadBar: cellData];
    
    UILabel *lblTotalOrdered = [cell viewWithTag:kCAMPAIGNCELL_TOTALORDERED_LBL];
    lblTotalOrdered.textColor = COLOUR_RED;
    lblTotalOrdered.font = FONT_H1;
    lblTotalOrdered.text = LOCALIZATION(C_PARTNERSOFSHELL_TOTALORDERED);
    
    UILabel *lblTotalOrdered_Data = [cell viewWithTag:kCAMPAIGNCELL_TOTALORDERED_DATA];
    lblTotalOrdered_Data.textColor = COLOUR_RED;
    lblTotalOrdered_Data.font = FONT_H1;
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc]initWithString:totalOrdered attributes:@{
                                                                                                                                                                                                             NSForegroundColorAttributeName:COLOUR_RED,                                                                  NSFontAttributeName : FONT_H(18),
                                                                                                                                                                                                             }];
    [attrString appendAttributedString: [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(C_REWARDS_POINTS) attributes:@{
                                                                                                                                       NSForegroundColorAttributeName:COLOUR_RED,
                                                                                                                                       NSFontAttributeName : FONT_B3,
                                                                                                                                       }]];
    lblTotalOrdered_Data.attributedText = attrString;
    
    UILabel *lblEarnedPoints = [cell viewWithTag:kCAMPAIGNCELL_EARNEDPOINTS_LBL];
    lblEarnedPoints.textColor = COLOUR_VERYDARKGREY;
    lblEarnedPoints.font = FONT_B1;
    lblEarnedPoints.text = LOCALIZATION(C_PARTNERSOFSHELL_EARNEDPOINTS);
    
    UILabel *lblEarnedPoints_Data = [cell viewWithTag:kCAMPAIGNCELL_EARNEDPOINTS_DATA];
    lblEarnedPoints_Data.textColor = COLOUR_VERYDARKGREY;
    lblEarnedPoints_Data.font = FONT_H1;
    
    NSMutableAttributedString *attrEarnedString = [[NSMutableAttributedString alloc]initWithString:earnedPoints attributes:@{
                                                                                                                  NSForegroundColorAttributeName:COLOUR_VERYDARKGREY,                                                                  NSFontAttributeName : FONT_H1,
                                                                                                                  }];
    [attrEarnedString appendAttributedString: [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(C_REWARDS_POINTS) attributes:@{
                                                                                                                                      NSForegroundColorAttributeName:COLOUR_VERYDARKGREY,
                                                                                                                                      NSFontAttributeName : FONT_B3,
                                                                                                                                      }]];
    lblEarnedPoints_Data.attributedText = attrEarnedString;
    
    UILabel *lblFooter = [cell viewWithTag:kCAMPAIGNCELL_FOOTER];
    lblFooter.textColor = COLOUR_VERYDARKGREY;
    lblFooter.font = FONT_B2;
    lblFooter.text = [NSString stringWithFormat:@"* %@",LOCALIZATION(C_PARTNERSOFSHELL_HEADER)];
    
    return cell;
}

- (IBAction) b2cBreakdownPressed:(id)sender {
    [mSession pushRUBreakdown:self.parentViewController fromB2CCampaign:YES];
}

- (IBAction) b2bBreakdownPressed:(id)sender {
    [mSession pushRUBreakdown:self.parentViewController fromB2CCampaign:NO];
}

#pragma mark WebService
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        default:
            break;
    }
}
@end
