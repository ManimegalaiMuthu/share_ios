//
//  EarnPointsTabViewController.m
//  Shell
//
//  Created by Nach on 11/9/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "RUEarnPoints_V2TabViewController.h"
#import "RUEarnPointsTable_V2ViewController.h"
#import "RDVTabBarItem.h"

@interface RUEarnPoints_V2TabViewController () {
    RUEarnPointsTable_V2ViewController *b2cCampaignView;
    RUEarnPointsTable_V2ViewController *b2bCampaignView;
}
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@end

@implementation RUEarnPoints_V2TabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupInterface];
}

-(void) setupInterface {
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_RUSSIA_TITLE_EARNPOINTS) subtitle: @"" size:15 subtitleSize:0];
    
    if ([self.navigationController.childViewControllers count] > 1)
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    
    b2cCampaignView = [STORYBOARD_RUSSIA_V2 instantiateViewControllerWithIdentifier: VIEW_EARNPOINTS_TABLE_V2];
    UINavigationController *b2cCampaignNav = [[UINavigationController alloc] initWithRootViewController: b2cCampaignView];
    [b2cCampaignNav setNavigationBarHidden:YES];
    b2cCampaignView.isB2BCampaign = YES;
    [b2cCampaignView view];
    
    b2bCampaignView = [STORYBOARD_RUSSIA_V2 instantiateViewControllerWithIdentifier: VIEW_EARNPOINTS_TABLE_V2];
    UINavigationController *b2bCampaignNav = [[UINavigationController alloc] initWithRootViewController: b2bCampaignView];
    [b2bCampaignNav setNavigationBarHidden:YES];
    b2bCampaignView.isB2BCampaign = NO;
    [b2bCampaignView view];
    
    NSArray *viewControllers = @[b2cCampaignNav,
                                 b2bCampaignNav
                                 ];
    NSArray *title = @[LOCALIZATION(@"B2B"),
                       LOCALIZATION(@"B2C")
                       ];
    [self setViewControllers: viewControllers];
    
    UIImage *selected = GET_ISADVANCE ? [UIImage imageNamed:@"tab-bg_blue"] : [UIImage imageNamed:@"tab-bg"];
    UIImage *unselected = [UIImage imageNamed:@"tab-bg_grey.png"];
    if (kIsRightToLeft) {
        title = title.reverseObjectEnumerator.allObjects.mutableCopy;
    }
    for (int i = 0; i < [self.tabBar.items count]; i++)
    {
        RDVTabBarItem *item = [self.tabBar.items objectAtIndex: i];
        [item setTitle: [title objectAtIndex: i]];
        
        [item setBackgroundColor: COLOUR_WHITE];
        [item setTitleUnselectedAttributes:FONT_H1 colour:COLOUR_VERYDARKGREY];
        [item setTitleSelectedAttributes:FONT_H1 colour:COLOUR_RED];
        
        [item setBackgroundSelectedImage: selected withUnselectedImage: unselected];
        
    }
    if (kIsRightToLeft) {
        [self tabBar:self.tabBar didSelectItemAtIndex:self.viewControllers.count-1];
    }
}

- (IBAction)backButtonPressed:(UIButton *)sender {
   [self.navigationController popViewControllerAnimated:NO];
}


@end
