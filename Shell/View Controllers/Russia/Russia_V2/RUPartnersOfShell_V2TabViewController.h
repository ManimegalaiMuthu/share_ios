//
//  RUPartnersOfShell_V2TabViewController.h
//  Shell
//
//  Created by Nach on 9/9/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "RDVTabBarController.h"
#import "Helper.h"

NS_ASSUME_NONNULL_BEGIN

@interface RUPartnersOfShell_V2TabViewController : RDVTabBarController

@end

NS_ASSUME_NONNULL_END
