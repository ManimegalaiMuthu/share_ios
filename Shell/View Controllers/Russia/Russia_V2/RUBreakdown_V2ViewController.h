//
//  RUBreakdown_V2ViewController.h
//  Shell
//
//  Created by Nach on 12/9/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface RUBreakdown_V2ViewController : BaseVC

@property BOOL isB2CBreakdown;

@end

NS_ASSUME_NONNULL_END
