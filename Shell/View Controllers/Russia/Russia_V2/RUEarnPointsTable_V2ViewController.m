//
//  EarnPointsTable_V2ViewController.m
//  Shell
//
//  Created by Nach on 11/9/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "RUEarnPointsTable_V2ViewController.h"
#import "RUEarnPoints_V2TabViewController.h"
#import "ShareProductTableViewCell.h"

#define CELL_HEIGHT 48
static int const kHeaderSectionTag = 6900;

@interface RUEarnPointsTable_V2ViewController () <WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblProgramInfo;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@property (assign) NSInteger expandedSectionHeaderNumber;
@property (assign) UITableViewHeaderFooterView *expandedSectionHeader;

@property NSArray *productArray;

@end

@implementation RUEarnPointsTable_V2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (![self.parentViewController.parentViewController isKindOfClass: [RUEarnPoints_V2TabViewController class]])
    {
        [Helper setNavigationBarTitle:self title: LOCALIZATION(C_RUSSIA_TITLE_EARNPOINTS) subtitle:@"" size:15 subtitleSize:0];
    }
    if ([self.parentViewController.childViewControllers count] > 1)
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    }
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.expandedSectionHeaderNumber = -1;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    [self setUpInterface];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_RUSSIA_TITLE_EARNPOINTS),self.isB2BCampaign ? @"B2B":@"B2C"] screenClass:nil];
}

-(void) setUpInterface {
    
    self.lblHeader.font = FONT_B1;
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    
    self.lblProgramInfo.font = FONT_B1;
    self.lblProgramInfo.textColor = COLOUR_VERYDARKGREY;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ShareProductTableViewCell" bundle:nil] forCellReuseIdentifier:@"ShareProductTableViewCell"];
    
    NSDictionary *homeDictionary = GET_HOMEDATA;
    NSDictionary *pointDict;

    if(self.isB2BCampaign) {
        pointDict = [homeDictionary objectForKey:@"MyPartnersClubB2B"];
        self.lblHeader.text =  [LOCALIZATION(C_RU_HOWTOEARNPOINTS_B2B_HEADER) stringByReplacingOccurrencesOfString:@"XXX" withString: [NSString stringWithFormat:@"%@",[pointDict objectForKey:@"FirstTarget"]]];
        self.lblProgramInfo.text = LOCALIZATION(C_RU_HOWTOEARNPOINTS_B2B_PERIOD);
        [[WebServiceManager sharedInstance] fetchWaysToEarnPointsFor:@"1" onVc:self];
    } else {
        pointDict = [homeDictionary objectForKey:@"MyPartnersClubB2C"];
        self.lblHeader.text =  [LOCALIZATION(C_RU_HOWTOEARNPOINTS_B2C_HEADER) stringByReplacingOccurrencesOfString:@"XXX" withString: [NSString stringWithFormat:@"%@",[pointDict objectForKey:@"FirstTarget"]]];
        self.lblProgramInfo.text = LOCALIZATION(C_RU_HOWTOEARNPOINTS_B2C_PERIOD);
        [[WebServiceManager sharedInstance] fetchWaysToEarnPointsFor:@"2" onVc:self];
    }
    
    [self.tableView reloadData];
    
}

- (IBAction)backPressed:(UIButton *)sender {
    [self popSelf];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.productArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.expandedSectionHeaderNumber == section) {
         NSArray *numberOfProducts = [[self.productArray objectAtIndex:section] objectForKey:@"Items"];
        return numberOfProducts.count;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ShareProductTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ShareProductTableViewCell" ];
    NSArray *productsAtSection = [[self.productArray objectAtIndex:indexPath.section] objectForKey:@"Items"];
    NSDictionary *productDetails = [productsAtSection objectAtIndex:indexPath.row];
    cell.productnameLbl.font = FONT_B1;
    cell.productnameLbl.textColor = COLOUR_VERYDARKGREY;
    cell.productnameLbl.text = [productDetails objectForKey:@"ProductName"];
    cell.RefPtsLbl.hidden = YES;
    cell.WalkinPtsLbl.text = [NSString stringWithFormat:@"%@ %@",[productDetails objectForKey:@"ProductPoints"],LOCALIZATION(C_POINTS_POINTS)];
    UIView *boxBottomBorderView = [cell viewWithTag:300];
    if (indexPath.row == productsAtSection.count - 1) {
        boxBottomBorderView.hidden = NO;
    } else {
        boxBottomBorderView.hidden = YES;
    }
    cell.RefPtsLbl.font = cell.productnameLbl.font;
    cell.WalkinPtsLbl.font = cell.productnameLbl.font;
    cell.RefPtsLbl.textColor = cell.productnameLbl.textColor;
    cell.WalkinPtsLbl.textColor = cell.productnameLbl.textColor;
    
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [[self.productArray objectAtIndex:section] objectForKey:@"GroupName"];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section; {
    return CELL_HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
     return CELL_HEIGHT;
}


- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.contentView.backgroundColor = COLOUR_WHITE;
    header.textLabel.textColor = COLOUR_DARKGREY;
    header.textLabel.font = FONT_B1;
    header.layer.borderColor = COLOUR_PALEGREY.CGColor;
    header.layer.borderWidth = 1.0;
    
    UIImageView *viewWithTag = [self.view viewWithTag:kHeaderSectionTag + section];
    if (viewWithTag) {
        [viewWithTag removeFromSuperview];
    }
    // add the arrow image
    CGSize headerFrame = self.view.frame.size;
    UIImageView *theImageView;
    if(kIsRightToLeft) {
        theImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 13, 18, 18)];
    } else {
        theImageView = [[UIImageView alloc] initWithFrame:CGRectMake(headerFrame.width - 69, 13, 18, 18)];
    }
    theImageView.image = GET_ISADVANCE ? [UIImage imageNamed:@"btn_plus_blue"] : [UIImage imageNamed:@"btn_plus_red"];
    theImageView.tag = kHeaderSectionTag + section;
    [header addSubview:theImageView];
    
    // make headers touchable
    header.tag = section;
    UITapGestureRecognizer *headerTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderWasTouched:)];
    [header addGestureRecognizer:headerTapGesture];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 20.0;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *viewForFooter = [[UIView alloc]init];
    viewForFooter.backgroundColor = [UIColor colorWithRed:247/COLOUR_MAXBYTE green:247/COLOUR_MAXBYTE blue:247/COLOUR_MAXBYTE alpha: 1.0f];
    return viewForFooter;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)updateTableViewRowDisplay:(NSArray *)arrayOfIndexPaths {
    [self.tableView beginUpdates];
    [self.tableView deleteRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation: UITableViewRowAnimationFade];
    [self.tableView endUpdates];
}

- (void)sectionHeaderWasTouched:(UITapGestureRecognizer *)sender {
    UITableViewHeaderFooterView *headerView = (UITableViewHeaderFooterView *)sender.view;
    NSInteger section = headerView.tag;
    UIImageView *eImageView = (UIImageView *)[headerView viewWithTag:kHeaderSectionTag + section];
    self.expandedSectionHeader = headerView;
    
    if (self.expandedSectionHeaderNumber == -1) {
        self.expandedSectionHeaderNumber = section;
        [self tableViewExpandSection:section withImage: eImageView];
    } else {
        if (self.expandedSectionHeaderNumber == section) {
            [self tableViewCollapeSection:section withImage: eImageView];
            self.expandedSectionHeader = nil;
        } else {
            UIImageView *cImageView  = (UIImageView *)[self.view viewWithTag:kHeaderSectionTag + self.expandedSectionHeaderNumber];
            [self tableViewCollapeSection:self.expandedSectionHeaderNumber withImage: cImageView];
            [self tableViewExpandSection:section withImage: eImageView];
        }
    }
}

- (void)tableViewCollapeSection:(NSInteger)section withImage:(UIImageView *)imageView {
    NSArray *sectionData = [[self.productArray objectAtIndex:section] objectForKey:@"Items"];
    
    self.expandedSectionHeaderNumber = -1;
    if (sectionData.count == 0) {
        return;
    } else {
        [UIView animateWithDuration:0.4 animations:^{
            imageView.transform = CGAffineTransformMakeRotation(M_PI_2);
        }];
        NSMutableArray *arrayOfIndexPaths = [NSMutableArray array];
        for (int i=0; i< sectionData.count; i++) {
            NSIndexPath *index = [NSIndexPath indexPathForRow:i inSection:section];
            [arrayOfIndexPaths addObject:index];
        }
        
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation: UITableViewRowAnimationFade];
        [self.tableView endUpdates];
        
    }
}

- (void)tableViewExpandSection:(NSInteger)section withImage:(UIImageView *)imageView {
    NSArray *sectionData = [[self.productArray objectAtIndex:section] objectForKey:@"Items"];
    
    if (sectionData.count == 0) {
        self.expandedSectionHeaderNumber = -1;
        return;
    } else {
        [UIView animateWithDuration:0.4 animations:^{
            imageView.transform = CGAffineTransformMakeRotation(M_PI_4);
            [imageView layoutIfNeeded];
        }];
        NSMutableArray *arrayOfIndexPaths = [NSMutableArray array];
        for (int i=0; i< sectionData.count; i++) {
            NSIndexPath *index = [NSIndexPath indexPathForRow:i inSection:section];
            [arrayOfIndexPaths addObject:index];
        }
        
        self.expandedSectionHeaderNumber = section;
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation: UITableViewRowAnimationFade];
        [self.tableView endUpdates];
        
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

#pragma mark Webservice Delegate
-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_FETCHWAYSTOEARNPOINTS:
            if(![[response getGenericResponse] isKindOfClass:[NSNull class]]) {
                self.productArray = [[response getGenericResponse] objectForKey:@"ProductPointsInfo"];
                if([self.productArray isKindOfClass:[NSNull class]]) {
                    self.productArray = @[];
                }
                [self.tableView reloadData];
            }
            break;
            
        default:
            break;
    }
}



@end
