//
//  EarnPointsTable_V2ViewController.h
//  Shell
//
//  Created by Nach on 11/9/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface RUEarnPointsTable_V2ViewController : BaseVC

@property BOOL isB2BCampaign;

@end

NS_ASSUME_NONNULL_END
