//
//  InventoryScanResultViewController.h
//  Shell
//
//  Created by Nach on 16/3/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "BaseVC.h"
#import "InventoryManagementTabViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface InventoryScanResultViewController : BaseVC

@property NSArray *resultData;
@property InventoryManagementTabViewController *inventoryManagementTabView;

@end

NS_ASSUME_NONNULL_END
