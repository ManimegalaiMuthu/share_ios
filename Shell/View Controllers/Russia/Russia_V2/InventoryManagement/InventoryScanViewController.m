//
//  InventoryScanViewController.m
//  Shell
//
//  Created by Nach on 11/3/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "InventoryScanViewController.h"
#import "PopupValueEntryViewController.h"
#import "ScanProductTableViewCell.h"
#import "QRCodeScanner.h"

#define WORKSHOPHEADERVIEW_HEIGHT   45.0
#define SCANNINGLIMIT               150

@interface InventoryScanViewController () <WebServiceManagerDelegate,UITableViewDataSource,UITableViewDelegate,QRCodeScannerDelegate,PopupValueEntryDelegate>

@property (weak, nonatomic) IBOutlet UIView *workshopHeaderView;
@property (weak, nonatomic) IBOutlet UILabel *lblWorkshopName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *workshopHeaderViewHeight;

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnScan;
@property (weak, nonatomic) IBOutlet UIButton *btnManual;

@property (weak, nonatomic) IBOutlet UILabel *lblNo;
@property (weak, nonatomic) IBOutlet UILabel *lblScannedCode;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblDelete;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmitCodes;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@property NSMutableArray *arrayOfCodes;
@property NSString *loginTradeID;

@end

@implementation InventoryScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
        self.workshopHeaderView.hidden = NO;
        self.workshopHeaderViewHeight.constant = WORKSHOPHEADERVIEW_HEIGHT;
    } else {
        self.workshopHeaderView.hidden = YES;
        self.workshopHeaderViewHeight.constant = 0;
    }
    
    if (self.isPushed) {
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_INVENTORY) subtitle: @""];
        [Helper setNavigationBarAppearanceTitleLabelInTwoLines:self];
           
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    }
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(@"Inventory Management"),LOCALIZATION_EN(@"Scan In")] screenClass:nil];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.lblHeader.text = LOCALIZATION(C_INVENTORY_SCANIN_HEADER);
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    self.lblHeader.font = FONT_B1;
    
    [self.btnScan.titleLabel setFont: FONT_BUTTON];
    [self.btnScan setBackgroundColor: COLOUR_YELLOW];
    [self.btnScan setTitle:LOCALIZATION(C_FORM_SCAN) forState:UIControlStateNormal];
    [self.btnScan setTitleColor: COLOUR_VERYDARKGREY forState:UIControlStateNormal];
       
    [self.btnManual.titleLabel setFont: FONT_BUTTON];
    [self.btnManual setBackgroundColor: COLOUR_YELLOW];
    [self.btnManual setTitle:LOCALIZATION(C_FORM_MANUAL) forState:UIControlStateNormal];
    [self.btnManual setTitleColor: COLOUR_VERYDARKGREY forState:UIControlStateNormal];
    
    self.lblNo.font = FONT_H3;
    self.lblNo.textColor = COLOUR_VERYDARKGREY;
    self.lblNo.text = LOCALIZATION(C_TABLE_NO);
    self.lblNo.numberOfLines = 0;
   // [self.lblNo sizeToFit];
    
    self.lblScannedCode.font = self.lblNo.font;
    self.lblScannedCode.textColor = self.lblNo.textColor;
    self.lblScannedCode.text= LOCALIZATION(C_TABLE_SCANNEDCODE);
    if ([GET_LOCALIZATION isEqualToString: kChinese])
        self.lblScannedCode.textAlignment = NSTextAlignmentCenter;
    self.lblScannedCode.numberOfLines = 0;
 //   [self.lblScannedCode sizeToFit];
    
    self.lblStatus.font = self.lblNo.font;
    self.lblStatus.textColor = self.lblNo.textColor;
    self.lblStatus.text = LOCALIZATION(C_STATUS);
    self.lblStatus.numberOfLines = 0;
 //   [self.lblStatus sizeToFit];
    
    self.lblDelete.font = self.lblNo.font;
    self.lblDelete.textColor = self.lblNo.textColor;
    self.lblDelete.text = LOCALIZATION(C_DELETE);
    self.lblDelete.numberOfLines = 0;
//    [self.lblDelete sizeToFit];
    
    self.btnSubmitCodes.titleLabel.font = FONT_BUTTON;
    [self.btnSubmitCodes setTitle:LOCALIZATION(C_INVENTORY_BTN_SUBMITPRODUCTCODES) forState:UIControlStateNormal];
    [self.btnSubmitCodes setBackgroundColor:COLOUR_RED];
    
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView registerNib:[UINib nibWithNibName:@"ScanProductTableViewCell" bundle:nil] forCellReuseIdentifier:@"ScanProductTableViewCell"];
    
    if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
        self.lblWorkshopName.text = GET_INVENTORY_SELECTEDWORKSHOP;
        self.lblWorkshopName.textColor = COLOUR_VERYDARKGREY;
        self.lblWorkshopName.font = FONT_H1;
        
    }
    
    mQRCodeScanner.delegate = self;
    
    self.arrayOfCodes = [[NSMutableArray alloc] init];
    
    if(GET_SCANNED_INVENTORY) {
        NSMutableDictionary *offlineScan = [[NSMutableDictionary alloc] initWithDictionary: GET_SCANNED_INVENTORY];
        NSArray *alreadyScannedCodes;
        if(GET_PROFILETYPE != kPROFILETYPE_DSR) {
            LoginInfo *loginInfo = [mSession loadUserProfile];
            self.loginTradeID = loginInfo.tradeID;
            alreadyScannedCodes = [[NSArray alloc] initWithArray:[offlineScan objectForKey:self.loginTradeID]];
        } else {
            self.loginTradeID = @"";
            alreadyScannedCodes = [[NSArray alloc] initWithArray:[offlineScan objectForKey:GET_INVENTORY_SELECTEDWORKSHOP]];
        }
        
     /*   if([alreadyScannedCodes count] > 0) {
            [self popUpYesNoWithTitle: @"" content: LOCALIZATION(C_FORM_OFFLINESCAN) yesBtnTitle: LOCALIZATION(C_FORM_YES) noBtnTitle: LOCALIZATION(C_FORM_NO) onYesPressed:^(BOOL finished) {
                           [self.arrayOfCodes addObjectsFromArray: alreadyScannedCodes];
                           [self.tableView reloadData];
                       } onNoPressed:^(BOOL finished) {
                           
                   }];
        }*/
    } else {
        if(GET_PROFILETYPE != kPROFILETYPE_DSR) {
            LoginInfo *loginInfo = [mSession loadUserProfile];
            self.loginTradeID = loginInfo.tradeID;
    
        }
    }
    
    [self.tableView reloadData];
}

- (IBAction)scanProductCodePressed:(UIButton *)sender {
    if([self.arrayOfCodes count] < SCANNINGLIMIT) {
        [mQRCodeScanner showQRCodeScannerOnVC:self withMessage:LOCALIZATION(C_REGISTEROILCHANGE_SUBTITLE)];
    } else {
        [self limitCrossedPopup];
    }
}

- (IBAction)manualProductCodePressed:(UIButton *)sender {
    if([self .arrayOfCodes count] < SCANNINGLIMIT) {
        [self popupValueEntryVCWithTitle:LOCALIZATION(C_FORM_ENTERCODETITLE) submitTitle:LOCALIZATION(C_FORM_ADDCODE) cancelTitle:LOCALIZATION(C_FORM_CANCEL) placeHolder:LOCALIZATION(C_FORM_ENTERCODETITLE)];
    } else {
        [self limitCrossedPopup];
    }
}

// modified sept 2020
- (IBAction)submitProductCodesPressed:(UIButton *)sender {
    if(GET_PROFILETYPE != kPROFILETYPE_DSR) {
         [[WebServiceManager sharedInstance] submitCodesMS:
          @{@"DSRTradeID":@"",@"TradeID" : self.loginTradeID,
                                   @"InventoryCoupons" :self.arrayOfCodes,}
        vc: self];
    } else {
    LoginInfo *loginInfo = [mSession loadUserProfile];
        [[WebServiceManager sharedInstance] submitScanOutCodesMS:
                @{@"DSRTradeID":loginInfo.tradeID ,@"TradeID" : GET_INVENTORY_SELECTEDWORKSHOP_TRADEID,
                                         @"InventoryCoupons" :self.arrayOfCodes,}
        vc: self];
    }
}

- (IBAction)backPressed:(UIButton *)sender {
    [self popSelf];
}

#pragma mark - Helper Methods
- (void)addValueToCacheAndView:(NSString *)value {
    
    [self.arrayOfCodes addObject:@{
                                    @"Code" : value,
                                }];
    
    NSMutableDictionary *offlineScan = [[NSMutableDictionary alloc] initWithDictionary: GET_SCANNED_INVENTORY];
    if(offlineScan == nil)
        offlineScan = [[NSMutableDictionary alloc] init];
    if(GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER || GET_PROFILETYPE == kPROFILETYPE_MECHANIC) {
        [offlineScan setValue:self.arrayOfCodes forKey:self.loginTradeID];
    } else {
        [offlineScan setValue:self.arrayOfCodes forKey:GET_INVENTORY_SELECTEDWORKSHOP];
    }
    SET_SCANNED_INVENTORY(offlineScan);
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.tableView reloadData];
    
}

-(void) limitCrossedPopup {
    [mAlert showErrorAlertWithMessage:LOCALIZATION(C_INVENTORY_MAXREACHED_POPUP)];
}

#pragma mark - QRCode Scanner
- (void)qrCodeScanSuccess:(NSString *)scanString {
    NSString *scannedValue = [[scanString componentsSeparatedByString:@"="] lastObject];
    BOOL isDuplicate = false;
    
    for (NSDictionary *dict in self.arrayOfCodes)
    {
        if ([scannedValue isEqualToString: [dict objectForKey: @"Code"]])
        {
            isDuplicate = true;
            [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_EXIST)];
            return;
        }
    }
    
    if(!isDuplicate) {
        [self addValueToCacheAndView:scannedValue];
    }
}

#pragma mark - PopupValueEntryDelegate
-(void) valueSubmitted:(NSString *)value {
    BOOL isDuplicate = false;
    for (NSDictionary *dict in self.arrayOfCodes)
    {
        if ([value isEqualToString: [dict objectForKey: @"Code"]])
        {
             isDuplicate = true;
            [mAlert showErrorAlertWithMessage: LOCALIZATION(C_SCANPRODUCTCODE_CODE_EXIST)];
            return;
        }
    }
    
    if(!isDuplicate) {
         [self addValueToCacheAndView:value];
    }
}

#pragma mark - TableView Delegate & Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.arrayOfCodes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ScanProductTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ScanProductTableViewCell"];
    NSDictionary *codeStringDict = [self.arrayOfCodes objectAtIndex:indexPath.row];
       
    [cell setID: @(indexPath.row + 1).stringValue];
    [cell setScannedCode: [codeStringDict objectForKey: @"Code"]];
    [cell setPendingStatus: LOCALIZATION(C_SCANCODE_STATUS_PENDING)];
       
    cell.btnDelete.tag = indexPath.row;
    [cell.btnDelete setImage:[UIImage imageNamed:@"icon-checkout-bin-dark.png"] forState:UIControlStateNormal];
    [cell.btnDelete setBackgroundColor:COLOUR_WHITE];
    [cell.btnDelete setTitle:@"" forState:UIControlStateNormal];
    
    [cell.btnDelete addTarget: self action: @selector(deletePressed:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(void)deletePressed:(UIButton*)sender
{
    [self.arrayOfCodes removeObjectAtIndex:sender.tag];
    NSMutableDictionary *offlineScan = [[NSMutableDictionary alloc] initWithDictionary: GET_SCANNED_INVENTORY];
    if(GET_PROFILETYPE != kPROFILETYPE_DSR) {
        [offlineScan setValue:self.arrayOfCodes forKey:self.loginTradeID];
    } else {
        [offlineScan setValue:self.arrayOfCodes forKey:GET_INVENTORY_SELECTEDWORKSHOP];
    }
    SET_SCANNED_INVENTORY(offlineScan);
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.tableView reloadData];
}

#pragma mark - WebServiceManager
- (void)processCompleted:(WebServiceResponse *)response {
    NSLog(@"Submit_Code ---------########### %@",[response getJSON]);
    switch (response.webserviceCall) {
        case kWEBSERVICE_MSTRADE_SUBMITCODEMS:
        {
            NSArray *responseData = [[NSArray alloc] initWithArray:[response getGenericResponse]];
            if(![responseData isKindOfClass:[NSNull class]] && [responseData count] > 0) {
                NSMutableDictionary *offlineScan = [[NSMutableDictionary alloc] initWithDictionary: GET_SCANNED_INVENTORY];
                if(GET_PROFILETYPE != kPROFILETYPE_DSR) {
                    [offlineScan removeObjectForKey:self.loginTradeID];
                } else {
                    [offlineScan removeObjectForKey:GET_INVENTORY_SELECTEDWORKSHOP];
                }
                SET_SCANNED_INVENTORY(offlineScan);
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self.arrayOfCodes removeAllObjects];
                [self.tableView reloadData];
                
                [mSession pushInventoryScanResultWith:responseData onVc:(self.isPushed ? self : self.navigationController.parentViewController)];
            }
        }
            break;
            
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
