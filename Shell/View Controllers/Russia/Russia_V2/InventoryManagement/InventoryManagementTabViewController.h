//
//  InventoryManagementTabViewController.h
//  Shell
//
//  Created by Nach on 11/3/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "RDVTabBarController.h"

NS_ASSUME_NONNULL_BEGIN

@interface InventoryManagementTabViewController : RDVTabBarController

@end

NS_ASSUME_NONNULL_END
