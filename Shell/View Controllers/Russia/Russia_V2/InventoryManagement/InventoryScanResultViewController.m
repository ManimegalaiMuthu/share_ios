//
//  InventoryScanResultViewController.m
//  Shell
//
//  Created by Nach on 16/3/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "InventoryScanResultViewController.h"

#define WORKSHOPHEADERVIEW_HEIGHT   45.0

#define CELL_HEIGHT     50.0

@interface InventoryScanResultViewController () <UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *workshopHeaderView;
@property (weak, nonatomic) IBOutlet UILabel *lblWorkshopName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *workshopHeaderViewHeight;

@property (weak, nonatomic) IBOutlet UILabel *lblNo;
@property (weak, nonatomic) IBOutlet UILabel *lblScannedCode;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;

@property (weak, nonatomic) IBOutlet UIButton *btnInventory;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@end

@implementation InventoryScanResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_INVENTORY_SCAIN_RESULT_TITLE) subtitle: @"" size:15 subtitleSize:0];
       
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    
    if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
        self.workshopHeaderView.hidden = NO;
        self.workshopHeaderViewHeight.constant = WORKSHOPHEADERVIEW_HEIGHT;
    } else {
        self.workshopHeaderView.hidden = YES;
        self.workshopHeaderViewHeight.constant = 0;
    }
    
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.lblNo.font = FONT_H1;
    self.lblNo.textColor = COLOUR_VERYDARKGREY;
    self.lblNo.text = LOCALIZATION(C_TABLE_NO);
    
    self.lblScannedCode.font = self.lblNo.font;
    self.lblScannedCode.textColor = self.lblNo.textColor;
    self.lblScannedCode.text= LOCALIZATION(C_TABLE_SCANNEDCODE);
    if ([GET_LOCALIZATION isEqualToString: kChinese])
        self.lblScannedCode.textAlignment = NSTextAlignmentCenter;
       
    self.lblStatus.font = self.lblNo.font;
    self.lblStatus.textColor = self.lblNo.textColor;
    self.lblStatus.text = LOCALIZATION(C_STATUS);
    
    [self.btnInventory setTitle:LOCALIZATION(C_INVENTORY_BTN_SEEINVENTORY) forState:UIControlStateNormal];
    [self.btnInventory setBackgroundColor:COLOUR_RED];
    [self.btnInventory.titleLabel setFont:FONT_BUTTON];
    
    if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@: ",LOCALIZATION(C_LOYALTY_WORKSHOPNAME)] attributes:@{
                                            NSFontAttributeName : FONT_H1 ,
                                            NSForegroundColorAttributeName : COLOUR_VERYDARKGREY,
        }];
        [attrString appendAttributedString: [[NSMutableAttributedString alloc] initWithString:GET_INVENTORY_SELECTEDWORKSHOP attributes:@{
                                            NSFontAttributeName : FONT_B1,
                                            NSForegroundColorAttributeName : COLOUR_VERYDARKGREY,
        }]];
        self.lblWorkshopName.attributedText = attrString;
        self.lblWorkshopName.textColor = COLOUR_VERYDARKGREY;
        
        [self.btnInventory setTitle:LOCALIZATION(C_INVENTORY_BTN_CONTINUEINVENTORY) forState:UIControlStateNormal];
        
    }
    
    [self.tableView setTableFooterView:[UIView new]];
    self.tableViewHeight.constant = CELL_HEIGHT * [self.resultData count];
    [self.tableView reloadData];
}

- (IBAction)seeInventoryPressed:(UIButton *)sender {
    if(GET_PROFILETYPE != kPROFILETYPE_DSR) {
        self.inventoryManagementTabView.selectedIndex = 0;
    }
    [self popSelf];
}

- (IBAction)backPressed:(UIButton *)sender {
    [self popSelf];
}

#pragma mark - TableView
enum {
    INVENTORY_SCANRESULT_ID = 1001,
    INVENTORY_SCANRESULT_CODE,
    INVENTORY_SCANRESULT_STATUS,
} INVENTORY_SCANRESULT_TAG;

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.resultData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return CELL_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InventoryScanResultCell" forIndexPath:indexPath];
    NSDictionary *cellData = [self.resultData objectAtIndex:indexPath.row];
    
    UILabel *lblID = [cell viewWithTag:INVENTORY_SCANRESULT_ID];
    lblID.textColor = COLOUR_VERYDARKGREY;
    lblID.font = FONT_B1;
    lblID.text = [NSString stringWithFormat:@"%d",(indexPath.row + 1)];
    
    UILabel *lblScannedCode = [cell viewWithTag:INVENTORY_SCANRESULT_CODE];
    lblScannedCode.textColor = lblID.textColor;
    lblScannedCode.font = lblID.font;
    lblScannedCode.text = [NSString stringWithFormat:@"%@",[cellData objectForKey:@"Code"]];
    
    UILabel *lblStatus = [cell viewWithTag:INVENTORY_SCANRESULT_STATUS];
    lblStatus.textColor = lblID.textColor;
    lblStatus.font = lblID.font;
    lblStatus.text = [NSString stringWithFormat:@"%@",[cellData objectForKey:@"Status"]];
    
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
