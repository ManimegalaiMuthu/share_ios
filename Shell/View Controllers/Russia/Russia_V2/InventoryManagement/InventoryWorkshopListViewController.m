//
//  InventoryWorkshopListViewController.m
//  Shell
//
//  Created by Nach on 16/3/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "InventoryWorkshopListViewController.h"

#define CELL_HEIGHT     55.0

@interface InventoryWorkshopListViewController () <WebServiceManagerDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UITextField *textFieldToSearch;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property NSMutableArray *defaultWorkshopNameList;
@property NSMutableArray *displayedWorkshopNameList;
@property NSArray *dataArray;

@end

@implementation InventoryWorkshopListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_INVENTORY) subtitle: @""];
    [Helper setNavigationBarAppearanceTitleLabelInTwoLines:self];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // fetchDSRInventoryList
   // [[WebServiceManager sharedInstance] fetchDSRInventoryList:self];
    [[WebServiceManager sharedInstance] fetchDSRWorkshopList:self];
    
    self.lblHeader.text = LOCALIZATION(C_INVENTORY_SELECTWORKSHOP_HEADER);
    self.lblHeader.textColor = COLOUR_VERYDARKGREY;
    self.lblHeader.font = FONT_B1;
    
    self.textFieldToSearch.placeholder = LOCALIZATION(C_INVENTORY_SEARCHWORKSHOP_PLACEHOLDER);
    self.textFieldToSearch.textColor = COLOUR_VERYDARKGREY;
    self.textFieldToSearch.font = FONT_B1;
    self.textFieldToSearch.delegate = self;
    
    UIView *spacerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [self.textFieldToSearch setLeftViewMode:UITextFieldViewModeAlways];
    [self.textFieldToSearch setLeftView:spacerView];
    
    [self.tableView setTableFooterView:[UIView new]];
    self.dataArray = [[NSArray alloc] init];
    self.defaultWorkshopNameList = [[NSMutableArray alloc] init];
    self.displayedWorkshopNameList = [[NSMutableArray alloc] init];
}

#pragma mark - Helper Methods
-(NSString *) getTradeIDFor:(NSString *)name {
    for(NSDictionary *dict in self.dataArray) {
        if([[dict objectForKey:@"CompanyName"] isEqualToString:name]) {
            return [dict objectForKey:@"TradeID"];
        }
    }
    return @"";
}

#pragma mark - TableView
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return CELL_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.displayedWorkshopNameList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InventoryWorkshopListCell" forIndexPath:indexPath];
    
    UILabel *lblName = [cell viewWithTag:1001];
    lblName.text = [self.displayedWorkshopNameList objectAtIndex:indexPath.row];
    lblName.textColor = COLOUR_VERYDARKGREY;
    lblName.font = FONT_B1;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *selectedWorkshop = [self.displayedWorkshopNameList objectAtIndex:indexPath.row];
    SET_INVENTORY_SELECTEDWORKSHOP(selectedWorkshop);
    SET_INVENTORY_SELECTEDWORKSHOP_TRADEID([self getTradeIDFor:selectedWorkshop]);
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [mSession pushInventoryForSelectedWorkshopon:self];
}

#pragma mark - TextViewDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([self.defaultWorkshopNameList count] > 0) {
        NSRange textFieldRange = NSMakeRange(0, [textField.text length]);
        if (NSEqualRanges(range, textFieldRange) && [string length] == 0)
        {
            self.displayedWorkshopNameList = self.defaultWorkshopNameList;
        }
        else
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat: @"SELF contains[cd] %@", [NSString stringWithFormat: @"%@%@", self.textFieldToSearch.text,string]];
            
            self.displayedWorkshopNameList = [NSMutableArray arrayWithArray:[self.defaultWorkshopNameList filteredArrayUsingPredicate:predicate]];
            
        }
        [self.tableView reloadData];
    }
    return YES;
}

-(BOOL) textFieldShouldEndEditing:(UITextField *)textField {
    if([self.defaultWorkshopNameList count] > 0) {
        if ([textField.text isEqualToString:@""])
        {
            self.displayedWorkshopNameList = self.defaultWorkshopNameList;
        }
        else
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat: @"SELF contains[cd] %@",textField.text];
            
            self.displayedWorkshopNameList = [NSMutableArray arrayWithArray:[self.defaultWorkshopNameList filteredArrayUsingPredicate:predicate]];
            
        }
        [self.tableView reloadData];
    }
    return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    return YES;
}


#pragma mark - Webservice Manager
- (void)processCompleted:(WebServiceResponse *)response {
    NSLog(@"RESPONSE ---- %@",[response getJSON]);
    switch(response.webserviceCall) {
        case kWEBSERVICE_MSTRADE_FETCHDSRWORKSHOPLIST:{
            if(![[[response getGenericResponse] objectForKey:@"ApprovedWorkShop"] isKindOfClass:[NSNull class]]) {
                self.dataArray = [[response getGenericResponse] objectForKey:@"ApprovedWorkShop"];
                for(NSDictionary *dict in self.dataArray) {
                    [self.defaultWorkshopNameList addObject:[dict objectForKey:@"CompanyName"]];
                }
                self.displayedWorkshopNameList = self.defaultWorkshopNameList;

                [self.tableView reloadData];
                self.textFieldToSearch.text = @"";
            }
        }
            break;
        default:
            break;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
