//
//  InventoryListViewController.m
//  Shell
//
//  Created by Nach on 11/3/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "InventoryListViewController.h"
#import "ExpandableCollapsibleTableViewController.h"
#import "ExpandableCollapsibleDataModel.h"

#define WORKSHOPHEADERVIEW_HEIGHT   45.0
#define BTNSTARTINVENTORY_HEIGHT     50.0

@interface InventoryListViewController () <WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UIView *workshopHeaderView;
@property (weak, nonatomic) IBOutlet UILabel *lblWorkshopName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *workshopHeaderViewHeight;

@property (weak, nonatomic) IBOutlet UIView *topSectionDetailsView;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentInStock_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentInStock_Data;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentInStock_Bottles;
@property (weak, nonatomic) IBOutlet UILabel *lblProductSold_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblProductSold_Data;
@property (weak, nonatomic) IBOutlet UILabel *lblProductSold_Bottles;
@property (weak, nonatomic) IBOutlet UILabel *lblDisclaimer;

@property (weak, nonatomic) IBOutlet UIView *tableContainerView;

@property (weak, nonatomic) IBOutlet UIButton *btnStartInventory;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnStartInventoryHeight;
@property (retain, nonatomic) IBOutlet UILabel *lblInventoryEarnings;
@property (weak, nonatomic) IBOutlet UIButton *btnEarningPoints;
@property (weak, nonatomic) IBOutlet UIImageView *imgEarningPoint;

@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inventoryLabelHeight;
@property NSDictionary *responseDict;
@property ExpandableCollapsibleDataModel *expandableCollapsibledataModel;
@property ExpandableCollapsibleTableViewController *expandableTableViewController;

@end

@implementation InventoryListViewController



- (IBAction)showTotalEarningsPage:(id)sender {
     [mSession loadTotalEarnings];
}





- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
        self.workshopHeaderView.hidden = NO;
        self.btnStartInventory.hidden = NO;
        
        self.workshopHeaderViewHeight.constant = WORKSHOPHEADERVIEW_HEIGHT;
        self.btnStartInventoryHeight.constant = BTNSTARTINVENTORY_HEIGHT;
    } else {
        self.workshopHeaderView.hidden = YES;
        self.btnStartInventory.hidden = YES;
        
        self.workshopHeaderViewHeight.constant = 0;
        self.btnStartInventoryHeight.constant = 0;
    }
    
    if (self.isPushed) {
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_INVENTORY) subtitle: @""];
        [Helper setNavigationBarAppearanceTitleLabelInTwoLines:self];
           
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    }
    
   

}


-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(@"Inventory Management"),LOCALIZATION_EN(@"My Inventory")] screenClass:nil];
}

-(void) viewWillAppear:(BOOL)animated {
    if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
        [[WebServiceManager sharedInstance] fetchDSRInventoryList: self];
    } else {
        [[WebServiceManager sharedInstance] fetchTOInventoryList: self];
    }
    
    self.lblCurrentInStock_Title.text = LOCALIZATION(C_INVENTORY_CURRENTINSTOCK);
    self.lblCurrentInStock_Title.textColor = COLOUR_RED;
    self.lblCurrentInStock_Title.font = FONT_H2;
    self.lblCurrentInStock_Title.adjustsFontSizeToFitWidth = true;

    self.lblCurrentInStock_Data.text = @"0";
    self.lblCurrentInStock_Data.textColor = COLOUR_RED;
    self.lblCurrentInStock_Data.font = FONT_H1;

    self.lblCurrentInStock_Bottles.text = LOCALIZATION(C_INVENTORY_BOTTLES);
    self.lblCurrentInStock_Bottles.textColor = COLOUR_RED;
    self.lblCurrentInStock_Bottles.font = FONT_B3;

    self.lblProductSold_Title.text = LOCALIZATION(C_INVENTORY_PRODUCTSOLD);
    self.lblProductSold_Title.textColor = COLOUR_VERYDARKGREY;
    self.lblProductSold_Title.font = FONT_B1;

    self.lblProductSold_Data.text = @"0";
    self.lblProductSold_Data.textColor = COLOUR_VERYDARKGREY;
    self.lblProductSold_Data.font = FONT_H1;

    self.lblProductSold_Bottles.text = LOCALIZATION(C_INVENTORY_BOTTLES);
    self.lblProductSold_Bottles.textColor = COLOUR_VERYDARKGREY;
    self.lblProductSold_Bottles.font = FONT_B3;
    
    self.lblDisclaimer.text = LOCALIZATION(C_INVENTORY_HEADER_TO);
    self.lblDisclaimer.textColor = COLOUR_VERYDARKGREY;
    self.lblDisclaimer.font = FONT_B3;
    
   
    if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
        [self.btnStartInventory setTitle:LOCALIZATION(C_INVENTORY_BTN_STARTINVENTORY) forState:UIControlStateNormal];
        [self.btnStartInventory setBackgroundColor:COLOUR_RED];
        [self.btnStartInventory.titleLabel setFont:FONT_BUTTON];

        self.lblWorkshopName.text = GET_INVENTORY_SELECTEDWORKSHOP;
        self.lblWorkshopName.textColor = COLOUR_VERYDARKGREY;
        self.lblWorkshopName.font = FONT_H1;
        
        self.lblInventoryEarnings.hidden = YES;
        self.inventoryLabelHeight.constant = 0;
        self.btnEarningPoints.hidden = YES;
        self.imgEarningPoint.hidden = YES;

    }
    // added sept 2020
    if(GET_PROFILETYPE == kPROFILETYPE_MECHANIC){
        self.lblDisclaimer.hidden = NO;
        self.lblInventoryEarnings.hidden = NO;
     //   self.inventoryLabelHeight.constant = 0;
        self.btnEarningPoints.hidden = YES;
        self.imgEarningPoint.hidden = NO;
    }
    [self addIconToButton];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
       singleTap.numberOfTapsRequired = 1;
    [self.imgEarningPoint setUserInteractionEnabled:YES];
    [self.imgEarningPoint addGestureRecognizer:singleTap];

}

-(void)tapDetected{
  [mSession loadTotalEarnings];
}


-(void)myTapMethod{
    // Treat image tap
}


-(void) addIconToButton {
   self.btnEarningPoints.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.btnEarningPoints.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.btnEarningPoints.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);

}

-(void) setupInterface {
    self.expandableTableViewController = [[ExpandableCollapsibleTableViewController alloc] init];
    [self.expandableTableViewController.view setFrame:self.tableContainerView.bounds];
    [self.expandableTableViewController initialiseTableWith:self.expandableCollapsibledataModel hasPoints:@(TRUE)];
    [self.tableContainerView addSubview:self.expandableTableViewController.view];
    [self.expandableTableViewController.tableView reloadData];    
}

- (IBAction)startInventoryCheckPressed:(UIButton *)sender {
    [mSession pushDSRInventoryScanOn:self];
}

- (IBAction)backPressed:(UIButton *)sender {
    [self popSelf];
}


#pragma mark - Webservice Manager
- (void)processCompleted:(WebServiceResponse *)response {
    switch(response.webserviceCall) {
        case kWEBSERVICE_MSTRADE_FETCHDSRINVENTORY:
        case kWEBSERVICE_MSTRADE_FETCHTOINVENTORY: {
            self.responseDict = [[NSDictionary alloc] initWithDictionary:[response getGenericResponse]];
            if(self.responseDict == nil || [self.responseDict isKindOfClass:[NSNull class]] || [self.responseDict isEqualToDictionary:@{}]) {
            } else {
                
                // modified sept 2020
                self.lblCurrentInStock_Data.text = [NSString stringWithFormat:@"%@",[[self.responseDict objectForKey:@"TotalInStock"] isKindOfClass:[NSNull class]] ? @"0" :[self.responseDict objectForKey:@"TotalInStock"]];
                  
                self.lblProductSold_Data.text = [NSString stringWithFormat:@"%@",[[self.responseDict objectForKey:@"TotalProductSold"] isKindOfClass:[NSNull class]] ? @"0" :[self.responseDict objectForKey:@"TotalProductSold"]];
                  
                NSString * earnings =  [NSString stringWithFormat:@"%@",[[self.responseDict objectForKey:@"InventoryEarnings"] isKindOfClass:[NSNull class]] ? @"0" :[self.responseDict objectForKey:@"InventoryEarnings"]];

                [self.btnEarningPoints setTitle: [earnings stringByAppendingString:LOCALIZATION(C_INVENTORY_EARNPOINTS_BTN)] forState:UIControlStateNormal];

                 self.lblInventoryEarnings.text = LOCALIZATION(C_INVENTORY_EARNINGS_LBL);

               // [self.btnEarningPoints setTitle: [earnings stringByAppendingString:@"pts."] forState:UIControlStateNormal];
             //   [self.btnEarningPoints setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];

                NSArray *productArray = [self.responseDict objectForKey:@"InventoryProdDetails"];
                
                if(![productArray isKindOfClass:[NSNull class]]) {
                    if([productArray count] > 0){
                                      //NSMutableArray *headingsArray = [[NSMutableArray alloc] init];
                                      NSMutableArray *detailsArray = [[NSMutableArray alloc] init];
                                      for(NSDictionary *dict in productArray) {
                                          NSMutableArray *detailsArrayAtIndex = [[NSMutableArray alloc] init];
                                         // [headingsArray addObject: @""];
                                         // NSArray *productDetailsForDict = [[NSArray alloc] initWithArray:[dict objectForKey:@"ProductGroupDetails"]];
                                        //  for(NSDictionary *detailDict in productDetailsForDict) {
                                              [detailsArrayAtIndex addObject:@{
                                                  kExpandable_ProductName : [dict objectForKey:@"ProductName"],
                                                  kExpandable_ProductTotal : [dict objectForKey:@"InStock"],
                                              }];
                                         // }
                                          [detailsArray addObject:detailsArrayAtIndex];
                                      }
                                      self.expandableCollapsibledataModel = [[ExpandableCollapsibleDataModel alloc] initWithData:@{
                                          kExpandable_TableDataArray : detailsArray,
                                         // kExpandable_TableHeadingsArray : headingsArray,
                                      }];
                                      [self setupInterface];
                        
                    }
                
                }
            }
        }
            break;
        default:
            
            break;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
