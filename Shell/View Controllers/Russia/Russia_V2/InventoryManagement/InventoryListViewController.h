//
//  InventoryListViewController.h
//  Shell
//
//  Created by Nach on 11/3/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface InventoryListViewController : BaseVC

@property BOOL isPushed;

@end

NS_ASSUME_NONNULL_END
