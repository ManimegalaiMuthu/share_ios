//
//  InventoryManagementTabViewController.m
//  Shell
//
//  Created by Nach on 11/3/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "InventoryManagementTabViewController.h"
#import "InventoryListViewController.h"
#import "InventoryScanViewController.h"
#import "RDVTabBarItem.h"

@interface InventoryManagementTabViewController ()

@property InventoryListViewController *listViewController;
@property InventoryScanViewController *scanViewController;

@end

@implementation InventoryManagementTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupInterface];
}

-(void) setupInterface {
    
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_INVENTORY) subtitle: @""];
    
    [Helper setNavigationBarAppearanceTitleLabelInTwoLines:self];
    
    self.listViewController  = [STORYBOARD_RUSSIA_V2 instantiateViewControllerWithIdentifier: VIEW_INVENTORY_LISTVIEW];
    UINavigationController *listNav = [[UINavigationController alloc] initWithRootViewController: self.listViewController];
    [listNav setNavigationBarHidden: YES];
    [self.listViewController view];
    
    self.scanViewController  = [STORYBOARD_RUSSIA_V2 instantiateViewControllerWithIdentifier: VIEW_INVENTORY_SCANVIEW];
    UINavigationController *scanNav = [[UINavigationController alloc] initWithRootViewController: self.scanViewController];
    [scanNav setNavigationBarHidden: YES];
    [ self.scanViewController view];

    
    NSArray *title = @[LOCALIZATION(C_INVENTORY_MYINVENTORY_TITLE),
                       LOCALIZATION(C_INVENTORY_SCANIN_TITLE)];
    
    [self setViewControllers: @[listNav,
                                scanNav]];
    
    UIImage *selected = GET_ISADVANCE ? [UIImage imageNamed:@"tab-bg_blue"] : [UIImage imageNamed:@"tab-bg"];
    UIImage *unselected = [UIImage imageNamed:@"tab-bg_grey.png"];
    if (kIsRightToLeft) {
        title = title.reverseObjectEnumerator.allObjects.mutableCopy;
    }
    for (int i = 0; i < [self.tabBar.items count]; i++)
    {
        RDVTabBarItem *item = [self.tabBar.items objectAtIndex: i];
        [item setTitle: [title objectAtIndex: i]];
        
        
        [item setBackgroundColor: COLOUR_WHITE];
        [item setTitleUnselectedAttributes:FONT_H1 colour:COLOUR_VERYDARKGREY];
        [item setTitleSelectedAttributes:FONT_H1 colour:COLOUR_RED];
        
        [item setBackgroundSelectedImage: selected withUnselectedImage: unselected];
        
    }
    if (kIsRightToLeft) {
        [self tabBar:self.tabBar didSelectItemAtIndex:self.viewControllers.count-1];
    }
    
}

#pragma mark RDVTabBarDelegate
- (BOOL)tabBar:(RDVTabBar *)tabBar shouldSelectItemAtIndex:(NSInteger)index
{
    return ([tabBar selectedItem] != [[tabBar items] objectAtIndex: index]);
}

-(void)goToAddNewPage
{
    [self tabBar:self.tabBar didSelectItemAtIndex:1];
}

- (void)tabBar:(RDVTabBar *)tabBar didSelectItemAtIndex:(NSInteger)index
{
    //super method MUST BE PRESENT
    [super tabBar:tabBar didSelectItemAtIndex: index];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
