//
//  RUPartnersOfShell_V2TabViewController.m
//  Shell
//
//  Created by Nach on 9/9/19.
//  Copyright © 2019 Edenred. All rights reserved.
//

#import "RUPartnersOfShell_V2TabViewController.h"
#import "RDVTabBarItem.h"
#import "RUPartnersPerformance_V2ViewController.h"
#import "RUPartnersPointsEarned_V2ViewController.h"

@interface RUPartnersOfShell_V2TabViewController () {
    RUPartnersPerformance_V2ViewController *performanceViewController;
    RUPartnersPointsEarned_V2ViewController *pointsEarnedViewController;
}
@end

@implementation RUPartnersOfShell_V2TabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupInterface];
}

- (void) setupInterface
{
    
    [Helper setNavigationBarTitle: self title: LOCALIZATION(C_SIDEMENU_PARTNERS) subtitle: @""];       //lokalised
    
    pointsEarnedViewController  = [STORYBOARD_RUSSIA_V2 instantiateViewControllerWithIdentifier: VIEW_PARTNERSOFSHELL_POINTSEARNED_V2];
    UINavigationController *pointsEarnedNav = [[UINavigationController alloc] initWithRootViewController: pointsEarnedViewController];
    [pointsEarnedNav setNavigationBarHidden: YES];
    
    performanceViewController  = [STORYBOARD_RUSSIA_V2 instantiateViewControllerWithIdentifier: VIEW_PARTNERSOFSHELL_PERFORMANCE_V2];
    UINavigationController *performanceNav = [[UINavigationController alloc] initWithRootViewController: performanceViewController];
    [performanceNav setNavigationBarHidden: YES];
    
    NSArray *title = @[LOCALIZATION(C_RUSSIA_POINTS_EARNED),        //lokalised
                       LOCALIZATION(C_TITLE_PERFORMANCE),           //lokalised
                       ];
    
    [self setViewControllers: @[pointsEarnedNav,
                                performanceNav,
                                ]];
    UIImage *selected = GET_ISADVANCE ? [UIImage imageNamed:@"tab-bg_blue"] : [UIImage imageNamed:@"tab-bg"];
    UIImage *unselected = [UIImage imageNamed:@"tab-bg_grey.png"];
    
    if (kIsRightToLeft) {
        title = title.reverseObjectEnumerator.allObjects.mutableCopy;
    }
    for (int i = 0; i < [self.tabBar.items count]; i++)
    {
        RDVTabBarItem *item = [self.tabBar.items objectAtIndex: i];
        [item setTitle: [title objectAtIndex: i]];
        
        [item setBackgroundColor: COLOUR_WHITE];
        [item setTitleUnselectedAttributes:FONT_H1 colour:COLOUR_VERYDARKGREY];
        [item setTitleSelectedAttributes:FONT_H1 colour:COLOUR_RED];
        
        [item setBackgroundSelectedImage: selected withUnselectedImage: unselected];
    }
    if (kIsRightToLeft) {
        [self tabBar:self.tabBar didSelectItemAtIndex:self.viewControllers.count-1];
    }
}

#pragma mark RDVTabBarDelegate
- (BOOL)tabBar:(RDVTabBar *)tabBar shouldSelectItemAtIndex:(NSInteger)index
{
    return ([tabBar selectedItem] != [[tabBar items] objectAtIndex: index]);
}

- (void)tabBar:(RDVTabBar *)tabBar didSelectItemAtIndex:(NSInteger)index
{
    //super method MUST BE PRESENT
    [super tabBar:tabBar didSelectItemAtIndex: index];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
