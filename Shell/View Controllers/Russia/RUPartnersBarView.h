//
//  RUPartnersBarView.h
//  Shell
//
//  Created by Jeremy Lua on 25/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RUPartnersBarView : UIView
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property CGFloat minPoints;
@property CGFloat maxPoints;
@property CGFloat currentPoints;
@property CGFloat targetPoints;

-(void) loadBar: (NSDictionary *) responseData;
@end
