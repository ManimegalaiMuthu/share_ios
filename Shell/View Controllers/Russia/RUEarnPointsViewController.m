//
//  RUEarnPointsViewController.m
//  Shell
//
//  Created by Jeremy Lua on 28/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUEarnPointsViewController.h"

@interface RUEarnPointsViewController () <UITableViewDelegate, UITableViewDataSource, WebServiceManagerDelegate>
@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblProducts;
@property (weak, nonatomic) IBOutlet UILabel *lblProductsSubtitle;
@property (weak, nonatomic) IBOutlet UIButton *btnValueCalculator;

@property (weak, nonatomic) IBOutlet UITableView *productsTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *productsTableViewHeight;
@property NSMutableArray *productsArray;

@end

#define HEADER_HEIGHT 50
#define CELL_HEIGHT 40
@implementation RUEarnPointsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle: self title: LOCALIZATION(C_RUSSIA_TITLE_EARNPOINTS) subtitle: @""];      //lokalised
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    
    self.lblHeader.text =  LOCALIZATION(C_RUSSIA_EARNPOINTS_CONTENT);                                       //lokalised
    
    self.lblProducts.text = LOCALIZATION(C_TITLE_PARTICIPATINGPRODUCT);                                     //lokalised
    self.lblProductsSubtitle.text = LOCALIZATION(C_RUSSIA_EARNPOINTS_PACKSIZE);                             //lokalised
    
    [self.btnValueCalculator setTitle: LOCALIZATION(C_RUSSIA_VALUE_CALCULATOR) forState: UIControlStateNormal]; //lokalised on 11 Feb
    
    [[WebServiceManager sharedInstance] fetchWaysToEarnPoints: self];
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"HeaderCell"];
    
    NSDictionary *sectionData = [self.productsArray objectAtIndex: section];
    
    UILabel *lblTitle = [cell viewWithTag: 1];
    lblTitle.text = [sectionData objectForKey: @"GroupName"];
    
    UILabel *lblHighestPoint = [cell viewWithTag: 2];
//    lblHighestPoint.hidden = [[sectionData objectForKey: @"HasHighestPoint"] boolValue];
    
    if (section == 0)
        lblHighestPoint.hidden = NO;
    else
        lblHighestPoint.hidden = YES;
    
    
    if (!lblHighestPoint.hidden)
    {
        lblHighestPoint.text = [sectionData objectForKey: @"HasHighestPointText"];
    }
    [cell.contentView setBackgroundColor: COLOUR_WHITE];
    return cell.contentView;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.productsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"ProductCell"];

    NSDictionary *sectionData = [self.productsArray objectAtIndex: indexPath.section];
    NSArray *sectionProductListArray = [sectionData objectForKey: @"Items"];
    NSDictionary *cellData = [sectionProductListArray objectAtIndex: indexPath.row];
    
    UILabel *lblName = [cell viewWithTag: 1];
    lblName.text = [cellData objectForKey: @"ProductName"];
    
    UILabel *lblPoint = [cell viewWithTag: 2];
    lblPoint.text = [NSString stringWithFormat: @"%.0f %@", [[cellData objectForKey: @"ProductPoints"] floatValue], LOCALIZATION(C_REWARDS_PTS)];
  
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary *cellData = [self.productsArray objectAtIndex: section];
    NSArray *productArray = [cellData objectForKey:@"Items"];
    return [productArray count];
}


- (void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_WAYSTOEARN:
            
            self.productsArray = [[response getGenericResponse] objectForKey: @"ProductPointsInfo"];
            
            NSInteger sectionCount = 0;
            NSInteger productCount = 0;
            for (NSDictionary *dict in self.productsArray)
            {
                sectionCount += 1;
                NSArray *productArray = [dict objectForKey: @"Items"];
                productCount += [productArray count];
            }
            //top space + section height + section spacing + individual cell heights
            self.productsTableViewHeight.constant = 15 + ((HEADER_HEIGHT + 15) * sectionCount) + ((CELL_HEIGHT) * productCount);
            self.productsTableView.tableFooterView = [UIView new];
            
            [self.productsTableView.superview setNeedsDisplay];
            [self.productsTableView reloadData];
            break;
            
        default:
            break;
    }
}

- (IBAction)valueCalculatorPressed:(id)sender
{
    [mSession pushValueCalculatorWithData: nil vc: self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
