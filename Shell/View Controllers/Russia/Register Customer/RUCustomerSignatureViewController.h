//
//  RUCustomerSignatureViewController.h
//  Shell
//
//  Created by Jeremy Lua on 27/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"
@protocol RUCustomerSignatureDelegate <NSObject>
-(void) didFinishSignature:(UIImage *) image;
@end

@interface RUCustomerSignatureViewController : BaseVC
@property id<RUCustomerSignatureDelegate> delegate;
@end
