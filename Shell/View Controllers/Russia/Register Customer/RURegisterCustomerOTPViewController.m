//
//  RURegisterCustomerOTPViewController.m
//  Shell
//
//  Created by Jeremy Lua on 27/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RURegisterCustomerOTPViewController.h"

@interface RURegisterCustomerOTPViewController () <WebServiceManagerDelegate, TTTAttributedLabelDelegate>

@property (strong, nonatomic) IBOutlet UILabel *lblVerifyHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblOTPSent;

@property (strong, nonatomic) IBOutlet UILabel *lblOTP;
@property (strong, nonatomic) IBOutlet UITextField *txtOTP;


@property (strong, nonatomic) IBOutlet UILabel *lblOTPContent;
@property (strong, nonatomic) IBOutlet TTTAttributedLabel *lblOTPResend;

@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;

@property (weak, nonatomic) NSTimer *resendTimer;
@property NSInteger timerCount;

@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@end

@implementation RURegisterCustomerOTPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_CONSUMER_REGISTERCUSTOMER) subtitle: @""];         //lokalised
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    
    [self setupInterface];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / OTP",LOCALIZATION_EN(C_CONSUMER_REGISTERCUSTOMER)] screenClass:nil];
}


-(void)viewDidDisappear:(BOOL)animated
{
    [self.resendTimer invalidate];
    self.resendTimer = nil;
}

-(void) setupInterface
{
    self.lblVerifyHeader.font = FONT_H1;
    self.lblVerifyHeader.textColor = COLOUR_DARKGREY;
    self.lblVerifyHeader.text = LOCALIZATION(C_CONSUMER_VERIFYMOBNUM);                      //lokalise 23 Jan

    self.lblOTPSent.font = FONT_B2;
    self.lblOTPSent.textColor = COLOUR_DARKGREY;
    self.lblOTPSent.text = LOCALIZATION(C_CONSUMER_OTPSENT);                                //lokalise 23 Jan
    
    self.lblOTP.font = FONT_B1;
    self.lblOTP.textColor = COLOUR_DARKGREY;
    self.lblOTP.text = LOCALIZATION(C_CONSUMER_OTP);                                        //lokalise 23 Jan
    
    self.txtOTP.font = FONT_B1;
    self.txtOTP.textColor = COLOUR_DARKGREY;
    self.txtOTP.placeholder = LOCALIZATION(C_OTP_PLACEHOLDER);                              //lokalise 23 Jan
    
    self.lblOTPContent.font = FONT_B2;
    self.lblOTPContent.textColor = COLOUR_DARKGREY;
    
    self.lblOTPContent.text = LOCALIZATION(C_CONSUMER_AUTHMSG);                             //lokalise 23 Jan
    
    self.lblOTPResend.font = FONT_B2;
    self.lblOTPResend.textColor = COLOUR_DARKGREY;
    
    [Helper setHyperlinkLabel: self.lblOTPResend hyperlinkText:self.lblOTPResend.text hyperlinkFont:FONT_B1 bodyText:self.lblOTPResend.text bodyFont:FONT_B1 urlString:@"OTP" bodyColor:COLOUR_VERYDARKGREY];
    
    [self.btnSubmit.titleLabel setFont: FONT_BUTTON];
    [self.btnSubmit setTitle: LOCALIZATION(C_REGISTER_SUBMITOTP) forState:UIControlStateNormal];        //lokalise 23 Jan
    
    
    self.timerCount = [[[mSession lookupTable] objectForKey: @"OTPTimer"] intValue];
    
    self.resendTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(startTimer) userInfo:nil repeats:YES];
    [self setResendOtpEnable:NO timer:self.timerCount];
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url absoluteString] isEqualToString: @"OTP"])
    {
//        [[WebServiceManager sharedInstance] submitMember: self.submitDict vc: self];
        
        NSString *countryCodeString = [mSession convertCountryCodeToCountryDialingCode: GET_COUNTRY_CODE];
        
        NSDictionary *submitParams = @{@"MobileNumber": [self.submitDict objectForKey: @"MobileNumber"],
                                       @"CountryDial":  @(countryCodeString.intValue),
                                       @"ModuleID" : @(1), //consumer OTP
                                       };
        [[WebServiceManager sharedInstance] resendOtpWithDictionary: submitParams vc: self];
    }
    return;
    
}

- (IBAction)submitPressed:(id)sender {
    
    NSMutableDictionary *submitParams = [[NSMutableDictionary alloc] initWithDictionary: self.submitDict];
    [submitParams setObject: self.txtOTP.text forKey: @"OTP"];
    [submitParams setObject: @(3) forKey: @"Step"];
    [[WebServiceManager sharedInstance] submitMember: submitParams vc: self];
}


-(void) setResendOtpEnable :(BOOL) isEnable timer:(NSInteger)timerCounter
{
    NSString *resendString = LOCALIZATION(C_REGISTER_RESENDOTP);                //lokalise 23 Jan
    if(isEnable){
        [Helper setHyperlinkLabel: self.lblOTPResend hyperlinkText:resendString hyperlinkFont: FONT_B2 bodyText:resendString bodyFont:FONT_B2 urlString:@"OTP" bodyColor: COLOUR_VERYDARKGREY];
        self.lblOTPResend.delegate = self;
    }else{
        NSString *stringCountDown = [NSString stringWithFormat:@"%@ (%d)",resendString,timerCounter];
        [Helper setHyperlinkLabel: self.lblOTPResend hyperlinkText:stringCountDown hyperlinkFont:FONT_B2 bodyText:stringCountDown bodyFont:FONT_B2 urlString:@"OTP" bodyColor:COLOUR_LIGHTGREY];
        self.lblOTPResend.delegate = nil;
    }
}

-(void)startTimer{
    self.timerCount -= 1;
    [self setResendOtpEnable:NO timer:self.timerCount];
    
    if(self.timerCount == 0){
        [self setResendOtpEnable:YES timer:self.timerCount];
        //        self.timerCount = MAXTIMEBEFORERESEND;
        self.timerCount = [[[mSession lookupTable] objectForKey: @"OTPTimer"] intValue];
        [self.resendTimer invalidate];
        self.resendTimer = nil;
    }
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_SUBMITMEMBER:
            [mSession loadRURegisterConsumerSuccess];
            break;
        case kWEBSERVICE_SUBMITMEMBER_RESENDOTP:
            //resend OTP response
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished)
             {
                 
             }];
            break;
        default:
            break;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
