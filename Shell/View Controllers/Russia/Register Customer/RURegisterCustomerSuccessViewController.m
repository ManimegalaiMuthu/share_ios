//
//  RURegisterCustomerSuccessViewController.m
//  Shell
//
//  Created by Jeremy Lua on 27/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RURegisterCustomerSuccessViewController.h"

@interface RURegisterCustomerSuccessViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UIButton *btnContinue;

@end

@implementation RURegisterCustomerSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_RUSSIA_REGISTERSUCCESS) subtitle: @""];            //lokalise 24 Jan
    
    // Do any additional setup after loading the view.
    self.lblHeader.font = FONT_H1;
    self.lblHeader.text = LOCALIZATION(C_CONSUMER_SUCCESSFUL);                                                  //lokalise 23 Jan
    
    self.lblContent.font = FONT_B1;
    self.lblContent.text = [NSString stringWithFormat: @"%@",  LOCALIZATION(C_CONSUMER_SUCCESSFULREGISTRATION)];    //lokalsie 23 Jan
    
    
    [self.btnContinue.titleLabel setFont: FONT_BUTTON];
    [self.btnContinue setTitle:LOCALIZATION(C_RUSSIA_BACKTOSEARCHVEH) forState:UIControlStateNormal];               //lokalise 24 Jan
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_RUSSIA_REGISTERSUCCESS) screenClass:nil];
}


- (IBAction)continuePressed:(id)sender
{
    [mSession loadSearchVehicleView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
