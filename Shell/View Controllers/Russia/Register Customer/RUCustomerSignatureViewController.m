//
//  RUCustomerSignatureViewController.m
//  Shell
//
//  Created by Jeremy Lua on 27/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUCustomerSignatureViewController.h"
#import "UviSignatureView.h"

@interface RUCustomerSignatureViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblSignature;
@property (weak, nonatomic) IBOutlet UviSignatureView *signatureView;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet UIButton *btnClear;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@end

@implementation RUCustomerSignatureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_RUSSIA_CUSTOMERSIGNATURE) subtitle: @""];      //lokalised
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    // Do any additional setup after loading the view.
    
    self.lblSignature.text = LOCALIZATION(C_RUSSIA_DRAWSIGNATURE);                                          //lokalised
    [self.btnSubmit setBackgroundColor:COLOUR_RED];
    
    [self.btnClear setTitle: LOCALIZATION(C_RUSSIA_CLEARSIGNATURE) forState: UIControlStateNormal];         //lokalised
    [self.btnSubmit setTitle: LOCALIZATION(C_FORM_SUBMIT) forState: UIControlStateNormal];                  //lokalised
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_RUSSIA_CUSTOMERSIGNATURE) screenClass:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

- (IBAction)clearPressed:(id)sender
{
    [self.signatureView erase];
}

- (IBAction)submitPressed:(id)sender
{
    if (self.signatureView.captureImage)
        [self.delegate didFinishSignature: self.signatureView.captureImage];
    [self popSelf];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
