//
//  RURegisterCustomerViewController.m
//  Shell
//
//  Created by Jeremy Lua on 27/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RURegisterCustomerViewController.h"
#import "RUCustomerSignatureViewController.h"

#import "CASMonthYearPicker.h"

@interface RURegisterCustomerViewController () <WebServiceManagerDelegate, RUCustomerSignatureDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIImageView *imgSignature;
@property (weak, nonatomic) IBOutlet UILabel *lblSignature;

@property (weak, nonatomic) IBOutlet UILabel *lblRequiredFields;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileHeader;

@property (weak, nonatomic) IBOutlet UILabel *lblMarketingCheckbox;
@property (weak, nonatomic) IBOutlet UIButton *btnMarketingCheckbox;

@property (weak, nonatomic) IBOutlet UIButton *btnRegister;



@property NSMutableArray *registrationFormArray; //ui only
@property NSMutableDictionary *regSubmissionDict;  //key as json key, value = cell. retrieve value by using getJson method

@property (weak, nonatomic) IBOutlet UITableView *regFormTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight; //to adjust according number of cells

@property NSInteger popupTag;
@property NSArray *stateTable;

@property Member *member;

@property NSInteger step;

#define CELL_HEIGHT 72

#define kCountryDial    @"CountryDial"
#define kYearOfManufacture             @"YearOfManufacture"
#define kFirstName             @"FirstName"     //fullname
#define kVehicleNumber            @"VehicleNumber"
#define kMobileNumber             @"MobileNumber"
#define kIsSubscribeContent             @"IsSubscribeContent"
#define kIsRegistered             @"IsRegistered"       //default as 1

#define kDigitalSignature            @"DigitalSignature"
#define kStep                        @"Step"            //1 - popup privacy, 2 - finish privacy policy, 3 - submit with OTP

#define kVehicleMake            @"VehicleMake"
#define kVehicleMakeID            @"VehicleMakeID"
#define kIsAcceptTnC                @"IsAcceptTnC"


enum kPopupSelection_Type
{
    kPopupSelection_VehicleMake,
    kPopupSelection_YearOfManufacture,
};
@end

@implementation RURegisterCustomerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_CONSUMER_REGISTERCUSTOMER) subtitle: @""];         //lokalise 23 Jan

    self.step = 1;
    
    self.regSubmissionDict = [[NSMutableDictionary alloc] init];
    [self setupRegistrationForm];

    
    [self.btnRegister.titleLabel setFont: FONT_BUTTON];
    [self.btnRegister setBackgroundColor:COLOUR_RED];
    [self.btnRegister setTitle:LOCALIZATION(C_CONSUMER_SUBMITREGISTRATION) forState:UIControlStateNormal];          //lokalise 23 Jan
    NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @"*" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B3, }];
    
    NSMutableAttributedString *requiredFieldsAttrString = [[NSMutableAttributedString alloc] initWithString: LOCALIZATION(C_PROFILE_REQUIREDFIELDS) attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B3, }];       //lokalised
    
    [asteriskAttrString appendAttributedString:  requiredFieldsAttrString];
    
    //    self.lblConfirmMsg.text = [responseMessageDict objectForKey: @"Message"];
    self.lblRequiredFields.attributedText = asteriskAttrString;
    
    self.lblProfileHeader.text = LOCALIZATION(C_CONSUMER_ENTERDETAILS);             //lokalise 23 Jan
    self.lblMarketingCheckbox.text = LOCALIZATION(C_CONSUMER_MARKETING);            //lokalise 23 Jan
    self.lblSignature.text = LOCALIZATION(C_RUSSIA_CUSTOMERSIGNATURE);              //lokalised
    
    [self.btnRegister setTitle: LOCALIZATION(C_CONSUMER_SUBMITREGISTRATION) forState: UIControlStateNormal];            //lokalise 23 Jan
}

-(void) setupRegistrationForm
{
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormTextfieldCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormTextfieldCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormDropdownCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormDropdownCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationMobileNumberCell" bundle:nil] forCellReuseIdentifier:@"RegistrationMobileNumberCell"];

    
    self.registrationFormArray = [[NSMutableArray alloc] init];
    
    [self.registrationFormArray addObject:@{@"Title": LOCALIZATION(C_CONSUMER_FULLNAME),            //lokalise 23 Jan
                                            @"Type": @(kREGISTRATION_TEXTFIELD),
                                            @"Key": kFirstName,
                                            @"Value": @"",
                                            }]; //first name
    
    //uneditable
    [self.registrationFormArray addObject:@{@"Title": LOCALIZATION(C_CONSUMER_VEHICLEREGPLATE),         //lokalise 23 Jan
                                            @"Type": @(kREGISTRATION_TEXTFIELD),
                                            @"Placeholder": LOCALIZATION(C_INVITECUSTOMER_VEHICLEID_PLACEHOLDER),
                                            @"Key": kVehicleNumber,
                                            @"Value": self.vehicleId,
                                            @"Enabled": @(NO),
                                            }]; //Vehicle registration number
   
    
    [self.registrationFormArray addObject: @{@"Title": LOCALIZATION(C_PROFILE_MOBILENUM),               //lokalise 23 Jan
                                             @"Type": @(kREGISTRATION_MOBNUM),
                                             @"Key": kMobileNumber,
                                             @"CountryCode": [mSession convertCountryCodeToCountryDisplayName: GET_COUNTRY_CODE],
                                             @"TextfieldEnabled": @(YES),
                                             @"MobileNumber": @"",
                                             }];  //Mobile number
    
    [self.registrationFormArray addObject:@{@"Title": LOCALIZATION(C_CONSUMER_YEAROFMANUFACTURE),           //lokalise 23 Jan
                                            @"Type": @(kREGISTRATION_DROPDOWN),
                                            @"Key": kYearOfManufacture,
                                            @"Value": LOCALIZATION(C_CONSUMER_YEAROFMANUFACTURE),
                                            @"Optional": @(YES),
                                            }]; //button for year of manufacture
    
    [self.registrationFormArray addObject:@{@"Title": LOCALIZATION(C_RUSSIA_VEHICLEBRAND),                  //lokalise 24 Jan
                                            @"Type": @(kREGISTRATION_DROPDOWN),
                                            @"Key": kVehicleMake,
                                            @"Value": LOCALIZATION(C_RUSSIA_VEHICLEBRAND),              //lokalise 24 Jan
                                            }]; //button for vehicle Usage

    //set tableview height.
    self.tableViewHeight.constant = CELL_HEIGHT * [self.registrationFormArray count];
    self.regFormTableView.tableFooterView = [UIView new];
    [self.regFormTableView reloadData];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_CONSUMER_REGISTERCUSTOMER) screenClass:nil];
}

#pragma mark - tableview delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.registrationFormArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *regFieldData = [self.registrationFormArray objectAtIndex: indexPath.row];
    switch ([[regFieldData objectForKey: @"Type"] intValue])
    {
        case kREGISTRATION_TEXTFIELD:
        {
            RegistrationFormTextfieldCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormTextfieldCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                [textfieldCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [textfieldCell hideTopLabel: NO];
            
            //see if Enabled key is available, if not see if pending or approved
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell enableCell: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            if ([regFieldData objectForKey: @"Placeholder"])
                [textfieldCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
            
            if ([regFieldData objectForKey: @"Value"])
                [textfieldCell setTextfieldText: [regFieldData objectForKey: @"Value"]];
            
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_MOBNUM:
        {
            RegistrationMobileNumberCell *mobNumCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationMobileNumberCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [mobNumCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            if ([regFieldData objectForKey: @"TextfieldEnabled"])
                [mobNumCell setTextfieldEnabled: [[regFieldData objectForKey: @"TextfieldEnabled"] boolValue]];
            [mobNumCell setMobileCodeEnabled: NO];

            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                [mobNumCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [mobNumCell hideTopLabel: NO];
            
            [mobNumCell setCountryCode: [regFieldData objectForKey: @"CountryCode"]];
            [mobNumCell setMobileNumber: [regFieldData objectForKey: @"MobileNumber"]];
            
            [self.regSubmissionDict setObject: mobNumCell forKey: [regFieldData objectForKey: @"Key"]];
            return mobNumCell;
        }
            break;
        case kREGISTRATION_DROPDOWN:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            
            dropdownCell.button.tag = indexPath.row;
            
            if ([[regFieldData objectForKey: @"Optional"] boolValue])
            {
                [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            }
            else
            {
                NSMutableAttributedString *asteriskAttrString = [[NSMutableAttributedString alloc] initWithString: @" *" attributes: @{NSForegroundColorAttributeName: COLOUR_RED,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                NSMutableAttributedString *titleAttrString = [[NSMutableAttributedString alloc] initWithString: [regFieldData objectForKey: @"Title"] attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
                
                [titleAttrString appendAttributedString:  asteriskAttrString];
                
                [dropdownCell setRequiredTitle:titleAttrString placeholder:[regFieldData objectForKey: @"Title"]];
            }
            [dropdownCell hideTopLabel: NO];
            
            if ([[regFieldData objectForKey: @"Key"] isEqualToString: kYearOfManufacture])
            {
                [dropdownCell.button addTarget: self action: @selector(yearOfManufacturePressed) forControlEvents:UIControlEventTouchUpInside];
            }
            else if ([[regFieldData objectForKey: @"Key"] isEqualToString: kVehicleMake])
            {
                [dropdownCell.button addTarget: self action: @selector(vehicleMakePressed) forControlEvents:UIControlEventTouchUpInside];
            }
            
            //set initial value
            if ([regFieldData objectForKey: @"Value"])
                [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            
            //keep a reference of cell to submit json value later
            [self.regSubmissionDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        default:
            NSLog(@"UNKNOWN CELL TYPE");
            return [UITableViewCell new];
            break;
    }
    return [UITableViewCell new];
}

-(void) yearOfManufacturePressed
{
    self.popupTag = kPopupSelection_YearOfManufacture;
    [self popUpListViewWithTitle: LOCALIZATION(C_CONSUMER_YEAROFMANUFACTURE) listArray:[mSession getYearOfManufacture]];            //lokalise 23 Jan
}

//-(void) vehicleUsagePressed
//{
//    self.popupTag = kPopupSelection_VehicleUsage;
//    [self popUpListViewWithTitle: LOCALIZATION(C_CONSUMER_VEHICLEUSAGE) listArray:[mSession getVehicleUsage]];
//}

-(void) vehicleMakePressed
{
    self.popupTag = kPopupSelection_VehicleMake;
    [self popUpListViewWithTitle: LOCALIZATION(C_RUSSIA_VEHICLEBRAND) listArray:[mSession getVehicleMake]];         //lokaliose 24 Jan
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        RegistrationFormDropdownCell *cell;
        switch (self.popupTag)
        {
            case kPopupSelection_VehicleMake:
            {
                cell = [self.regSubmissionDict objectForKey: kVehicleMake];
            }
                break;
                
            case kPopupSelection_YearOfManufacture:
            {
                cell = [self.regSubmissionDict objectForKey: kYearOfManufacture];
            }
                break;
            default:
                break;
        }
        [cell.button setTitle:selection.firstObject forState:UIControlStateNormal];
    }
}



- (IBAction)checkBoxSelected:(UIButton *)sender {
    if ([sender isSelected])
        sender.selected = NO;
    else
        sender.selected = YES;
}

-(NSMutableDictionary *) createSubmitMemberDictionary
{
    NSMutableDictionary *submitParams = [[NSMutableDictionary alloc] init];
    
    [submitParams setObject: GET_COUNTRY_CODE forKey: kCountryCode];
    
    [submitParams setObject: [[self.regSubmissionDict objectForKey: kFirstName] getJsonValue] forKey: kFirstName];
    
    [submitParams setObject: [[self.regSubmissionDict objectForKey: kVehicleNumber] getJsonValue] forKey: kVehicleNumber];
    
    [submitParams setObject: [[self.regSubmissionDict objectForKey: kMobileNumber] getMobileNumberJsonValue] forKey: kMobileNumber];
    
    //year of manufacture
    [submitParams setObject: @([[mSession convertToYearOfManufactureKeyCode: [[self.regSubmissionDict objectForKey: kYearOfManufacture] getJsonValue]] intValue]) forKey: kYearOfManufacture];
    
    //vehicle make
    [submitParams setObject: [[self.regSubmissionDict objectForKey: kVehicleMake] getJsonValue] forKey: kVehicleMake];
    
    //vehicle make id
    [submitParams setObject: @([[mSession convertToVehicleMakeKeyCode: [[self.regSubmissionDict objectForKey: kVehicleMake] getJsonValue]] intValue]) forKey: kVehicleMakeID];
    
    //registered vehicle by default
    [submitParams setObject: @(YES) forKey: kIsRegistered];
    
    //checkbox
    [submitParams setObject: @(self.btnMarketingCheckbox.selected) forKey: kIsSubscribeContent];
    
    [submitParams setObject: @(0) forKey: @"VehicleUsage"];
    
    if (self.imgSignature.image)
    {
        //set image
        [submitParams setObject: [UIImageJPEGRepresentation(self.imgSignature.image, 0.5f) base64EncodedStringWithOptions:0]  forKey: kDigitalSignature];
    }
    
    return submitParams;
}

- (IBAction)registerPressed:(id)sender
{
    NSMutableDictionary *submitParams = [self createSubmitMemberDictionary];
    
    //register button = step 1
    [submitParams setObject: @(self.step) forKey: kStep];
    
    [[WebServiceManager sharedInstance] submitMember: submitParams vc: self];
    
//    [[WebServiceManager sharedInstance] submitMemberRegistrationStepTwo: submitParams view:self];
    
//    [self initPrivacyPolicyPopup];
}

-(void) didAgreePrivacyPolicy:(BOOL) tncSelected
{
    
    NSMutableDictionary *submitParams = [self createSubmitMemberDictionary];
    
    self.step = 2;
    
    //register button = step 2
    [submitParams setObject: @(self.step) forKey: kStep];
    
    if (tncSelected)
        [submitParams setObject: @"1" forKey: kIsAcceptTnC];
    else
        [submitParams setObject: @"0" forKey: kIsAcceptTnC];

    [[WebServiceManager sharedInstance] submitMember: submitParams vc: self];
}

- (IBAction)signaturePressed:(id)sender
{
    [mSession pushRUCustomerSignature: self];
}

-(void)didFinishSignature:(UIImage *)image
{
    self.imgSignature.image = image;
}

#pragma mark - WebServiceManagerDelegate Methods
- (void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_SUBMITMEMBER:
        {
            switch (self.step)
            {
                case 2:
                {
                    [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished)
                    {
                        NSMutableDictionary *submitParams = [self createSubmitMemberDictionary];
                        
                        self.step = 1;
                        
                        //step 2 successfully submitted,
                        //force OTP side to always send step 2
                        //this VC will reset back to step 1
                        [submitParams setObject: @(2) forKey: kStep];
                        
                        [submitParams setObject: @"1" forKey: kIsAcceptTnC];
                        
                        [mSession pushRURegisterConsumerOTPViewWithDict: submitParams vc: self];
                    }];
                }
                    break;
                default:
                    [self initPrivacyPolicyPopup];
                    break;
            }
//            [mSession pushRegisterConsumerRegisterCompleteWithDict:self.otpDict vc: self];
//            [mSession loadRegisterConsumerRegisterCompleteWithDict: self.otpDict vc:self];
        }
            break;
        default:
            break;
    }
}

-(void) processFailed:(WebServiceResponse *)response
{
    self.step = 1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
