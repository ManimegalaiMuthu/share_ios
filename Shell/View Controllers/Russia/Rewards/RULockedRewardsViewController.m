//
//  RULockedRewardsViewController.m
//  Shell
//
//  Created by Jeremy Lua on 13/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RULockedRewardsViewController.h"
#import "RURewardsCountdownCell.h"
#import "RUBrandsCell.h"

@interface RULockedRewardsViewController () <UICollectionViewDelegate, UICollectionViewDataSource, WebServiceManagerDelegate, RURewardsCountdownCellDelegate>
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@property (weak, nonatomic) IBOutlet UICollectionView *colView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *colViewLayout;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property BOOL shouldShowBack;

@property NSDictionary *rewardsDict;
@end

@implementation RULockedRewardsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[WebServiceManager sharedInstance] fetchRURewardsHome: self];
    
    self.colViewLayout.itemSize = CGSizeMake(320 * (SCREEN_WIDTH / 320), 431 * (SCREEN_WIDTH / 320));
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_REWARDSFETCHREWARDSHOME:
        {
            self.rewardsDict = [[NSDictionary alloc] initWithDictionary: [response getGenericResponse]];
            
            //check rewards start date here
            if ([[_rewardsDict objectForKey:@"RewardsStartDate"]intValue] == 0)
            {
                [self popSelf];
                [mSession loadRewardsViewWithLockFlag: NO];
            } else {
                [self.colView reloadData];
            }
        }
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)didStartCountdown
{
    [self.btnBack setTitle: LOCALIZATION(C_REWARDS_BACK) forState:UIControlStateNormal];    //lokalise 31 jan
    self.shouldShowBack = YES;
}

- (void)didFinishCountdown
{
    [self.btnBack setTitle: LOCALIZATION(C_FORM_UPDATE) forState:UIControlStateNormal];    //lokalise 31 jan
    self.shouldShowBack = NO;
}

- (IBAction)backPressed:(id)sender
{
    if (self.shouldShowBack)
        [mSession loadHomeView];
    else
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: kAppStoreURLString]];
}

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    switch (indexPath.row)
    {
        case 0:
        {
            RURewardsCountdownCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"CountdownCell" forIndexPath: indexPath];
            
            [cell startCountdown: [[self.rewardsDict objectForKey: @"RewardsStartDate"] intValue]];
            cell.delegate = self;
            return cell;
        }
            break;
     
        case 1:
        {
            RUBrandsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"BrandsCell" forIndexPath: indexPath];
            
            [cell loadBrands: [self.rewardsDict objectForKey: @"RewardsImageList"]];
            
            return cell;
        }
            break;
        default:
            break;
    }
    
    return [UICollectionViewCell new];
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    self.pageControl.numberOfPages = 2;
    return self.pageControl.numberOfPages; //fixed 2 items
}

-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.colView)
        self.pageControl.currentPage = scrollView.contentOffset.x / scrollView.frame.size.width;
}

@end
