//
//  RUBrandsCell.h
//  Shell
//
//  Created by Jeremy Lua on 13/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RUBrandsCell : UICollectionViewCell
-(void) loadBrands:(NSArray *) rewardsListArray;
@end
