//
//  RUBrandsCell.m
//  Shell
//
//  Created by Jeremy Lua on 13/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUBrandsCell.h"
#import "LocalizationManager.h"
#import "Session.h"
#import "UIImageView+WebCache.h"

@interface RUBrandsCell () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *lblFeaturedBrands;
@property (weak, nonatomic) IBOutlet UILabel *lblFeaturedBrands2;
@property (weak, nonatomic) IBOutlet UICollectionView *colView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *colViewLayout;

@property NSArray* rewardsListArray;
@end

@implementation RUBrandsCell

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.lblFeaturedBrands.text = LOCALIZATION(C_REWARDS_FEATUREDREWARDS);     //lokalise 31 Jan
    self.lblFeaturedBrands2.text = LOCALIZATION(C_RU_REWARDS_FEATUREDBRANDSTWO);     //lokalise 31 Jan
    CGFloat width = 100;
    self.colViewLayout.itemSize = CGSizeMake(width * (SCREEN_WIDTH / 320), width * (SCREEN_WIDTH / 320));
}

-(void)loadBrands:(NSArray *)rewardsListArray
{
    self.rewardsListArray = rewardsListArray;
    
    [self.colView reloadData];
}


- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"ImageCell" forIndexPath: indexPath];
    
    NSDictionary *cellData = [self.rewardsListArray objectAtIndex: indexPath.row];
    
    UIImageView *imgView = [cell viewWithTag: 1];
    
    [imgView sd_setImageWithURL: [NSURL URLWithString: [cellData objectForKey: @"ImageUrl"]] placeholderImage: nil options: (SDWebImageRetryFailed | SDWebImageRefreshCached)];
    
    return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.rewardsListArray count];
}

@end
