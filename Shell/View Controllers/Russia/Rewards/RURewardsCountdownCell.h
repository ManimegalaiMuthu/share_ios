//
//  RURewardsCountdownCell.h
//  Shell
//
//  Created by Jeremy Lua on 13/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RURewardsCountdownCellDelegate
-(void) didFinishCountdown;
-(void) didStartCountdown;
@end

@interface RURewardsCountdownCell : UICollectionViewCell
@property id<RURewardsCountdownCellDelegate> delegate;

-(void) startCountdown: (NSTimeInterval) secondsToUnlock;
@end
