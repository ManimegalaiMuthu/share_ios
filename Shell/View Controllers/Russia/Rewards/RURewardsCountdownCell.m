//
//  RURewardsCountdownCell.m
//  Shell
//
//  Created by Jeremy Lua on 13/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "Session.h"
#import "LocalizationManager.h"
#import "RURewardsCountdownCell.h"

@interface RURewardsCountdownCell ()
@property (weak, nonatomic) IBOutlet UILabel *lblRedemptionStart;

@property (weak, nonatomic) IBOutlet UILabel *lblDaysData;
@property (weak, nonatomic) IBOutlet UILabel *lblDays;

//" : HH : " format
@property (weak, nonatomic) IBOutlet UILabel *lblHoursData;
@property (weak, nonatomic) IBOutlet UILabel *lblHours;

@property (weak, nonatomic) IBOutlet UILabel *lblMinutesData;
@property (weak, nonatomic) IBOutlet UILabel *lblMinutes;
@property (weak, nonatomic) IBOutlet UIView *countdownView;

@property NSTimeInterval countDownTimer;
@property NSTimer *timer;
@end

@implementation RURewardsCountdownCell

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.countDownTimer = 9999;

    self.lblRedemptionStart.text = LOCALIZATION(C_RU_LOCKEDREWARDS_REDEMPTIONSTART);     //lokalise 31 Jan
    
    self.lblDaysData.text = @"99";
    self.lblDays.text = LOCALIZATION(C_RU_LOCKEDREWARDS_DAYS);     //lokalise 31 Jan
    
    self.lblHoursData.text = @"99";
    self.lblHours.text = LOCALIZATION(C_RU_LOCKEDREWARDS_HOURS);     //lokalise 31 Jan
    
    self.lblMinutesData.text = @"99";
    self.lblMinutes.text = LOCALIZATION(C_RU_LOCKEDREWARDS_MINUTES);     //lokalise 31 Jan
    
}
//https://stackoverflow.com/a/1386746
-(void) startCountdown: (NSTimeInterval) secondsToUnlock
{
    if (self.timer)
        [self.timer invalidate];

    self.countDownTimer = secondsToUnlock;
    
    if (self.countDownTimer > 0)
    {
        [self.delegate didStartCountdown];
        self.lblRedemptionStart.text = LOCALIZATION(C_RU_LOCKEDREWARDS_REDEMPTIONSTART);     //lokalise 31 Jan
        self.countdownView.hidden = NO;
        [self updateCountdown];
        
        self.timer = [NSTimer scheduledTimerWithTimeInterval: 1  target: self selector:@selector(countDown) userInfo:nil repeats: YES];
    }
    else
    {
        [self.timer invalidate];
//        self.countdownView.hidden = YES;
//        self.lblRedemptionStart.text = LOCALIZATION(C_RU_REWARDS_COUNTDOWNFINISHUPDATE);
//        [self.delegate didFinishCountdown];
    }
}

-(void) countDown
{
    if (self.countDownTimer > 0)
    {
        self.countDownTimer--; 
    }
    else
    {
        [self.timer invalidate];
//        self.countdownView.hidden = YES;
//        self.lblRedemptionStart.text = LOCALIZATION(C_RU_REWARDS_COUNTDOWNFINISHUPDATE);
    }
    [self updateCountdown];
}

-(void) updateCountdown
{
    // Get the system calendar
    NSCalendar *sysCalendar = [NSCalendar currentCalendar];
    
    // Create the NSDates
    NSDate *date1 = [[NSDate alloc] init];
    NSDate *date2 = [[NSDate alloc] initWithTimeInterval: self.countDownTimer sinceDate:date1];
    
    NSDateComponents *breakdownInfo = [sysCalendar components: (NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date1  toDate:date2  options:0];
    
    self.lblDaysData.text = @([breakdownInfo day]).stringValue;

    self.lblHoursData.text = [NSString stringWithFormat: @"%i", [breakdownInfo hour]];

    self.lblMinutesData.text = @([breakdownInfo minute]).stringValue;
}

@end

