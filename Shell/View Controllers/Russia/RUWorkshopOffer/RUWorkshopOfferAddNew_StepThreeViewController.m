//
//  RUWorkshopOfferAddNew_StepThreeViewController.m
//  Shell
//
//  Created by Jeremy Lua on 14/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUWorkshopOfferAddNew_StepThreeViewController.h"

@interface RUWorkshopOfferAddNew_StepThreeViewController () <WebServiceManagerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnOfferInfo;


@property (weak, nonatomic) IBOutlet UILabel *lblSelectOffer;
@property (weak, nonatomic) IBOutlet UIButton *btnService;
@property (weak, nonatomic) IBOutlet UIButton *btnProduct;
@property (weak, nonatomic) IBOutlet UILabel *lblSelected;  //attributed string
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property NSString *selectedSelectionString;

@property (weak, nonatomic) IBOutlet UILabel *lblSelectOfferOption;
@property (weak, nonatomic) IBOutlet UIView *selectOfferOptionView;

@property (weak, nonatomic) IBOutlet UIButton *btnPercentDiscount;
@property (weak, nonatomic) IBOutlet UITextField *txtPercentDiscount;
@property (weak, nonatomic) IBOutlet UILabel *lblPercent;
@property (weak, nonatomic) IBOutlet UIView *percentDiscountView;

@property (weak, nonatomic) IBOutlet UIButton *btnDollarDiscount;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrency;
@property (weak, nonatomic) IBOutlet UITextField *txtDollarDiscount;
@property (weak, nonatomic) IBOutlet UIView *dollarDiscountView;

@property (weak, nonatomic) IBOutlet UIButton *btnFree;

@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property NSMutableDictionary *submitParamsStepThree;

//Progress Numbers
@property (weak, nonatomic) IBOutlet UIImageView *progressOneImageView;
@property (weak, nonatomic) IBOutlet UIImageView *progressTwoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *progressThreeImageView;
@property (weak, nonatomic) IBOutlet UIView *progressBar_ProgressView;
@property (weak, nonatomic) IBOutlet UIView *progressBar_ProgressView_Two;

@end

@implementation RUWorkshopOfferAddNew_StepThreeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle: self title: LOCALIZATION(C_RUSSIA_ADDNEWOFFER) subtitle: @""];           //lokalised
    
    self.lblHeader.text = LOCALIZATION(C_RU_STEPTHREE_CHOOSEOFFERTYPE);                                     //loklaised
    self.lblSelectOffer.text = LOCALIZATION(C_RU_STEPTHREE_SELECTOFFER);                                    //lokalised
    
    [self.btnService setTitle: LOCALIZATION(C_RU_STEPTHREE_SERVICE) forState: UIControlStateNormal];        //lokalised
    [self.btnProduct setTitle: LOCALIZATION(C_RU_STEPTHREE_PRODUCT) forState: UIControlStateNormal];        //lokalised
    [self.btnEdit setTitle: LOCALIZATION(C_RUSSIA_EDIT_BTN) forState: UIControlStateNormal];                //lokalised
    
    self.lblSelectOfferOption.text = LOCALIZATION(C_RU_STEPTHREE_SELECTOFFEROPTIONS);                       //lokalised
    [self.btnPercentDiscount setTitle: LOCALIZATION(C_RU_STEPTHREE_PERCENTDISCOUNT) forState: UIControlStateNormal];    //lokalised
    self.txtPercentDiscount.placeholder = LOCALIZATION(C_RU_STEPTHREE_ENTERPERCENT);                        //lokalised
    NSString *dollarDiscountString = [NSString stringWithFormat:@"$ %@",LOCALIZATION(C_RU_STEPTHREE_DOLLARDISCOUNT)];   //lokalised
    [self.btnDollarDiscount setTitle:dollarDiscountString  forState: UIControlStateNormal];
    self.txtDollarDiscount.placeholder = LOCALIZATION(C_RU_STEPTHREE_ENTERAMOUNT);                          //lokalised
    [self.btnFree setTitle: LOCALIZATION(C_RU_STEPTHREE_FREE) forState: UIControlStateNormal];              //lokalised

    [self.btnNext setTitle: LOCALIZATION(C_RUSSIA_NEXT_BTN) forState: UIControlStateNormal];                //lokalised
    [self.btnNext setBackgroundColor:COLOUR_RED];


    
    UIButton *btnBack = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30, 30)];
    [btnBack setImage: [UIImage imageNamed: @"icon-arrsingleL.png"] forState: UIControlStateNormal];
    [btnBack addTarget:self action: @selector(popSelf) forControlEvents: UIControlEventTouchUpInside];
    if(@available(iOS 11, *)){
        [btnBack.widthAnchor constraintEqualToConstant: btnBack.frame.size.width].active = YES;
        [btnBack.heightAnchor constraintEqualToConstant: btnBack.frame.size.height].active = YES;
    }
    
    UIBarButtonItem *backBarBtn = [[UIBarButtonItem alloc] initWithCustomView: btnBack];
    self.navigationItem.leftBarButtonItem = backBarBtn;
    
    UIButton *btnInfo = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30, 30)];
    [btnInfo setImage: [UIImage imageNamed: @"icn_offer_darkgrey_15x15.png"] forState: UIControlStateNormal];
    [btnInfo addTarget:self action: @selector(infoPressed) forControlEvents: UIControlEventTouchUpInside];
    if(@available(iOS 11, *)){
        [btnInfo.widthAnchor constraintEqualToConstant: btnInfo.frame.size.width].active = YES;
        [btnInfo.heightAnchor constraintEqualToConstant: btnInfo.frame.size.height].active = YES;
    }
    UIBarButtonItem *infoBarBtn = [[UIBarButtonItem alloc] initWithCustomView: btnInfo];
    self.navigationItem.rightBarButtonItem = infoBarBtn;
    
    [self.btnEdit setAttributedTitle: [[NSAttributedString alloc] initWithString: LOCALIZATION(C_RUSSIA_EDIT_BTN) attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle), }] forState: UIControlStateNormal];       //lokalised
    
    [self setNavBackBtn];
    if(GET_ISADVANCE) {
        [self.progressOneImageView setImage: [UIImage imageNamed:@"progressnum-blue-1"]];
        [self.progressTwoImageView setImage: [UIImage imageNamed:@"progressnum-blue-2"]];
        [self.progressThreeImageView setImage: [UIImage imageNamed:@"progressnum-blue-3"]];
    } else {
        [self.progressOneImageView setImage: [UIImage imageNamed:@"progressnum-red-1"]];
        [self.progressTwoImageView setImage: [UIImage imageNamed:@"progressnum-red-2"]];
        [self.progressThreeImageView setImage: [UIImage imageNamed:@"progressnum-red-3"]];
    }
    
    self.progressBar_ProgressView.backgroundColor = COLOUR_RED;
    self.progressBar_ProgressView_Two.backgroundColor = COLOUR_RED;
    
    [self checkEditDetails];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / Step 3",LOCALIZATION_EN(C_TITLE_WORKSHOPOFFER)] screenClass:nil];
}

- (IBAction)editPressed:(id)sender
{
    if (self.btnService.selected)
    {
        [self popUpListViewWithTitle: LOCALIZATION(C_RU_STEPTHREE_SELECTSERVICE) listArray: [mSession getWorkshopOfferService]];    //lokalised
    }
    else if (self.btnProduct.selected)
    {
        [self popUpListViewWithTitle: LOCALIZATION(C_RU_STEPTHREE_SELECTPRODUCT) listArray: [mSession getWorkshopOfferProduct]];    //lokalised
    }
    else
    {
        
    }
}


-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        
        NSMutableAttributedString *prefixString = [[NSMutableAttributedString alloc] initWithString: LOCALIZATION(C_RU_STEPTHREE_SELECTED) attributes:@{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_H1, }];                 //lokalised
        
        NSAttributedString *selectionString = [[NSAttributedString alloc] initWithString: selection.firstObject attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
        
        [prefixString appendAttributedString: selectionString];
        
        self.lblSelected.attributedText = prefixString;
        self.selectedSelectionString = selection.firstObject;
        
        self.btnEdit.hidden = NO;
        self.selectOfferOptionView.hidden = NO;
    }
}

- (IBAction)offerTypePressed:(UIButton *)sender
{
    if (sender == self.btnService)
    {
        self.btnService.selected = YES;
        [self.btnService setBackgroundColor: COLOUR_YELLOW];
        
        self.btnProduct.selected = NO;
        [self.btnProduct setBackgroundColor: COLOUR_PALEGREY];
        
        [self popUpListViewWithTitle: LOCALIZATION(C_RU_STEPTHREE_SELECTSERVICE) listArray: [mSession getWorkshopOfferService]];        //lokalised
    }
    else if (sender == self.btnProduct)
    {
        self.btnService.selected = NO;
        [self.btnService setBackgroundColor: COLOUR_PALEGREY];
        
        self.btnProduct.selected = YES;
        [self.btnProduct setBackgroundColor: COLOUR_YELLOW];
        
        [self popUpListViewWithTitle: LOCALIZATION(C_RU_STEPTHREE_SELECTPRODUCT) listArray: [mSession getWorkshopOfferProduct]];        //lokalised
    }
    else
    {
        NSLog(@"unknown error");
    }
}

- (IBAction)offerOptionPressed:(id)sender
{
    [self setDeselectButtons];
    if (sender == self.btnPercentDiscount)
    {
        self.btnPercentDiscount.selected = YES;
        [self.btnPercentDiscount setBackgroundColor: COLOUR_YELLOW];
        
        self.percentDiscountView.hidden = NO;
    }
    else if (sender == self.btnDollarDiscount)
    {
        self.btnDollarDiscount.selected = YES;
        [self.btnDollarDiscount setBackgroundColor: COLOUR_YELLOW];
        
        self.dollarDiscountView.hidden = NO;
    }
    else if (sender == self.btnFree)
    {
        self.btnFree.selected = YES;
        [self.btnFree setBackgroundColor: COLOUR_YELLOW];
        
    }
    else
    {
        NSLog(@"unknown error");
    }
    
    self.btnNext.enabled = YES;
    self.btnNext.alpha = 1.0f;
}

-(void) setDeselectButtons
{
    self.btnPercentDiscount.selected = NO;
    [self.btnPercentDiscount setBackgroundColor: COLOUR_PALEGREY];
    self.txtPercentDiscount.text = @"0";
    self.percentDiscountView.hidden = YES;
    
    self.btnDollarDiscount.selected = NO;
    [self.btnDollarDiscount setBackgroundColor: COLOUR_PALEGREY];
    self.dollarDiscountView.hidden = YES;
    self.txtDollarDiscount.text = @"0";
    
    self.btnFree.selected = NO;
    [self.btnFree setBackgroundColor: COLOUR_PALEGREY];
    
    self.btnNext.enabled = NO;
    self.btnNext.alpha = 0.5f;
}

- (IBAction)offerTypeInfoPressed:(id)sender
{
    [self popUpViewWithTitle: LOCALIZATION(C_RU_WHATISTHIS) content: LOCALIZATION(C_RU_STEPTHREE_HELP)];    //lokalised
}

-(void) infoPressed
{
    [self popUpViewWithTitle: LOCALIZATION(C_RU_WHATISTHIS) content: LOCALIZATION(C_RU_ADDOFFER_HELP)];     //lokalised
}

- (IBAction)nextPressed:(id)sender
{
    self.submitParamsStepThree = [[NSMutableDictionary alloc] initWithDictionary: self.submitParams];
    
    [self.submitParamsStepThree setObject: @(3) forKey:@"Step"];
    
//    [self.submitParamsStepThree setObject: @(1) forKey:@"OfferType"];
    
//    Service ==1, Product ==2
    if (self.btnService.selected)
    {
        [self.submitParamsStepThree setObject: @(1) forKey:@"OfferCategoryType"];
        [self.submitParamsStepThree setObject: [mSession convertToWorkshopServiceKeyCode: self.selectedSelectionString] forKey:@"OfferCategoryCode"];
    }
    else
    {
        [self.submitParamsStepThree setObject: @(2) forKey:@"OfferCategoryType"];
        [self.submitParamsStepThree setObject: [mSession convertToWorkshopProductKeyCode: self.selectedSelectionString] forKey:@"OfferCategoryCode"];
    }
    
//    public static final int PERCENTAGE_DISCOUNT_OPTIONS =1;
//    public static final int CASH_DISCOUNT_OPTIONS =2;
//    public static final int FREE_OPTIONS = 4;
    if (self.btnPercentDiscount.selected)
    {
        [self.submitParamsStepThree setObject: @(1) forKey:@"OfferOption"];
        [self.submitParamsStepThree setObject: @(self.txtPercentDiscount.text.intValue) forKey:@"OfferValue"];
        
    }
    else if (self.btnDollarDiscount.selected)
    {
        [self.submitParamsStepThree setObject: @(2) forKey:@"OfferOption"];
        [self.submitParamsStepThree setObject: @(self.txtDollarDiscount.text.intValue) forKey:@"OfferValue"];
    }
    else
    {
        [self.submitParamsStepThree setObject: @(4) forKey:@"OfferOption"];
    }
    
    
    {
        NSMutableDictionary *stepThreeParams = [[NSMutableDictionary alloc] initWithDictionary: self.submitParamsStepThree];
        [[mSession workshopOfferDict] setObject: stepThreeParams forKey: @"StepThree"];
    }
    
    if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA]) {
        [[WebServiceManager sharedInstance] submitRUAncillaryOfferWithDict: self.submitParamsStepThree vc: self];
    } else {
        [[WebServiceManager sharedInstance] submitAncillaryOfferWithDict: self.submitParamsStepThree vc: self];
    }
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_SUBMITANCILLARY:
            if (![[response getGenericResponse] isKindOfClass: [NSNull class]])
            {
                for (NSString *key in [[response getGenericResponse] allKeys])
                {
                    if([key isEqualToString:@"OfferType"]) {
                        [self.submitParamsStepThree setObject: [[response getGenericResponse] objectForKey: key] forKey: @"OfferTypeText"];
                        [self.submitParamsStepThree setObject:[self.submitParams objectForKey:@"OfferType"] forKey:@"OfferType"];
                    } else {
                        [self.submitParamsStepThree setObject: [[response getGenericResponse] objectForKey: key] forKey: key];
                    }
                }
            }
            
            //just use existing params.
            [mSession pushRUAddNewOfferStepFourWithOffer:self.submitParamsStepThree vc: self];
            break;
            
        default:
            break;
    }
}


-(void) checkEditDetails
{
    NSDictionary *stepThreeParams = [[mSession workshopOfferDict] objectForKey: @"StepThree"];
    if (stepThreeParams)
    {
        
        NSMutableAttributedString *prefixString = [[NSMutableAttributedString alloc] initWithString: LOCALIZATION(C_RU_STEPTHREE_SELECTED) attributes:@{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_H1, }];                 //lokalised
        
        NSAttributedString *selectionString;
        
//        Service ==1
//        Product ==2
        switch ([[stepThreeParams objectForKey: @"OfferCategoryType"] intValue])
        {
            case 1:
            {
                self.btnService.selected = YES;
                [self.btnService setBackgroundColor: COLOUR_YELLOW];
                
                self.btnProduct.selected = NO;
                [self.btnProduct setBackgroundColor: COLOUR_PALEGREY];
                
                NSString *productString = [mSession convertToWorkshopService: [[stepThreeParams objectForKey: @"OfferCategoryCode"] intValue]];
                self.selectedSelectionString = productString;
                
                selectionString = [[NSAttributedString alloc] initWithString: productString  attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
            }
                break;
            case 2:
            {
                self.btnProduct.selected = YES;
                [self.btnProduct setBackgroundColor: COLOUR_YELLOW];
                
                self.btnService.selected = NO;
                [self.btnService setBackgroundColor: COLOUR_PALEGREY];
                
                NSString *productString = [mSession convertToWorkshopProduct: [[stepThreeParams objectForKey: @"OfferCategoryCode"] intValue]];
                self.selectedSelectionString = productString;
                selectionString = [[NSAttributedString alloc] initWithString: productString  attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, }];
            }
                break;
            default:
                break;
        }
                                           
       [prefixString appendAttributedString: selectionString];
       
       self.lblSelected.attributedText = prefixString;
                                           
       self.btnEdit.hidden = NO;
       self.selectOfferOptionView.hidden = NO;
        
//        public static final int PERCENTAGE_DISCOUNT_OPTIONS =1;
//        public static final int CASH_DISCOUNT_OPTIONS =2;
//        public static final int FREE_OPTIONS = 4;
        switch ([[stepThreeParams objectForKey: @"OfferOption"] intValue])
        {
            case 1:
                [self offerOptionPressed: self.btnPercentDiscount];
                self.txtPercentDiscount.text = [[stepThreeParams objectForKey: @"OfferValue"] stringValue];
                break;
            case 2:
                [self offerOptionPressed: self.btnDollarDiscount];
                self.txtDollarDiscount.text = [[stepThreeParams objectForKey: @"OfferValue"] stringValue];
                break;
            case 4:
                [self offerOptionPressed: self.btnFree];
                break;
            default:
                break;
        }
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
