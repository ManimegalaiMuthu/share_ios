//
//  RUWorkshopOfferDetailsViewController.h
//  Shell
//
//  Created by Jeremy Lua on 15/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"
#import "OfferDetailsViewController.h"
@interface RUWorkshopOfferDetailsViewController : OfferDetailsViewController

@end
