
//
//  RUWorkshopOfferAddNew_StepOneViewController.m
//  Shell
//
//  Created by Jeremy Lua on 14/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUWorkshopOfferAddNew_StepOneViewController.h"

@interface RUWorkshopOfferAddNew_StepOneViewController ()
<WebServiceManagerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblSubHeader;

@property (weak, nonatomic) IBOutlet UILabel *lblDateHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnStartDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEndDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;

@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UILabel *datePickerTitle;
@property (weak, nonatomic) IBOutlet UIButton *datePickerDone;


@property (weak, nonatomic) IBOutlet UILabel *lblOilChangeHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnWithOilChange;
@property (weak, nonatomic) IBOutlet UIButton *btnWithoutOilChange;

@property BOOL hasPressedStartDate;
@property BOOL hasPressedEndDate;
@property BOOL hasPressedOilchange;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

//Progress Numbers
@property (weak, nonatomic) IBOutlet UIImageView *progressOneImageView;

@end

@implementation RUWorkshopOfferAddNew_StepOneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle: self title: LOCALIZATION(C_RUSSIA_ADDNEWOFFER) subtitle: @"" size:15 subtitleSize:0];        //lokalised
    
    self.lblHeader.text = LOCALIZATION(C_RU_STEPONE_CHOOSEPROMOTYPE);           //lokalised
    self.lblSubHeader.text = LOCALIZATION(C_RU_STEPONE_RUNWORKSHOP);            //lokalised
    
    self.lblDateHeader.text = LOCALIZATION(C_RU_STEPONE_PROMOPERIOD);           //lokalised
    
    [self.btnStartDate setTitle: LOCALIZATION(C_ORDER_STARTDATE) forState: UIControlStateNormal];       //lokalised
    [self.btnEndDate setTitle: LOCALIZATION(C_ORDER_ENDDATE) forState: UIControlStateNormal];           //lokalised
    self.lblTo.text = LOCALIZATION(C_RU_STEPONE_TO);                            //lokalised
    
    self.lblOilChangeHeader.text = LOCALIZATION(C_RU_STEPONE_SELECTNEEDOILCHANGE);          //lokalised
    
    NSAttributedString *withOilChangeAttrString = [[NSAttributedString alloc] initWithString: LOCALIZATION(C_RU_STEPONE_WITHOILCHANGE) attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_BUTTON, }];            //lokalised
    [self.btnWithOilChange setAttributedTitle:withOilChangeAttrString forState: UIControlStateNormal];
    NSAttributedString *withoutOilChangeAttrString = [[NSAttributedString alloc] initWithString: LOCALIZATION(C_RU_STEPONE_WITHOUTOILCHANGE) attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_BUTTON, }];          //lokalised
    [self.btnWithoutOilChange setAttributedTitle: withoutOilChangeAttrString forState: UIControlStateNormal];
    
    [self.btnNext setTitle: LOCALIZATION(C_RUSSIA_NEXT_BTN) forState: UIControlStateNormal];        //lokalised
    [self.btnNext setBackgroundColor:COLOUR_RED];
    
    UIButton *btnBack = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30, 30)];
    [btnBack setImage: [UIImage imageNamed: @"icon-arrsingleL.png"] forState: UIControlStateNormal];
    [btnBack addTarget:self action: @selector(popSelf) forControlEvents: UIControlEventTouchUpInside];
    if(@available(iOS 11, *)){
        [btnBack.widthAnchor constraintEqualToConstant: btnBack.frame.size.width].active = YES;
        [btnBack.heightAnchor constraintEqualToConstant: btnBack.frame.size.height].active = YES;
    }
    
    UIBarButtonItem *backBarBtn = [[UIBarButtonItem alloc] initWithCustomView: btnBack];
    self.navigationItem.leftBarButtonItem = backBarBtn;
    
    UIButton *btnInfo = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30, 30)];
    [btnInfo setImage: [UIImage imageNamed: @"icn_offer_darkgrey_15x15.png"] forState: UIControlStateNormal];
    [btnInfo addTarget:self action: @selector(infoPressed) forControlEvents: UIControlEventTouchUpInside];
    if(@available(iOS 11, *)){
        [btnInfo.widthAnchor constraintEqualToConstant: btnInfo.frame.size.width].active = YES;
        [btnInfo.heightAnchor constraintEqualToConstant: btnInfo.frame.size.height].active = YES;
    }
    UIBarButtonItem *infoBarBtn = [[UIBarButtonItem alloc] initWithCustomView: btnInfo];
    self.navigationItem.rightBarButtonItem = infoBarBtn;
    
    [self setNavBackBtn];
    if(GET_ISADVANCE) {
        [self.progressOneImageView setImage: [UIImage imageNamed:@"progressnum-blue-1"]];
    } else {
        [self.progressOneImageView setImage: [UIImage imageNamed:@"progressnum-red-1"]];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self checkEditDetails];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / Step 1",LOCALIZATION_EN(C_TITLE_WORKSHOPOFFER)] screenClass:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) infoPressed
{
    [self popUpViewWithTitle: LOCALIZATION(C_RU_WHATISTHIS) content: LOCALIZATION(C_RU_ADDOFFER_HELP)];     //lokalised
}

- (IBAction)startDatePressed:(id)sender {
    //init date 3 days ago as start date
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay: [[[mSession lookupTable] objectForKey: @"AOMinStartDate"] intValue]];
    NSDate *threeDaysLater = [gregorian dateByAddingComponents:offsetComponents toDate: [NSDate date] options:0];
    
    [self.datePicker setLocale: [NSLocale localeWithLocaleIdentifier:GET_LOCALIZATION]];
    self.datePicker.minimumDate = threeDaysLater;
    
    self.datePicker.tag = 1;
    self.datePickerTitle.text = LOCALIZATION(C_ORDER_STARTDATE);        //lokalised
    self.datePickerView.hidden = NO;
    [self.datePickerDone setTitle: LOCALIZATION(C_FORM_DONE) forState: UIControlStateNormal];
}

- (IBAction)endDatePressed:(id)sender {
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat:@"dd MMM yyyy"];
    
    NSDate *date = [inputFormatter dateFromString: self.btnStartDate.titleLabel.text];
    
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay: [[[mSession lookupTable] objectForKey: @"AOMinEndDate"] intValue]];
    NSDate *endDate = [gregorian dateByAddingComponents:offsetComponents toDate: date options:0];
    
    [self.datePicker setLocale: [NSLocale localeWithLocaleIdentifier:GET_LOCALIZATION]];
    self.datePicker.minimumDate = endDate;
    self.datePicker.tag = 2;
    self.datePickerTitle.text = LOCALIZATION(C_ORDER_ENDDATE);          //lokalised
    self.datePickerView.hidden = NO;
    [self.datePickerDone setTitle: LOCALIZATION(C_FORM_DONE) forState: UIControlStateNormal];   //lokalised
}

- (IBAction)datePickerDonePressed:(id)sender {
    self.datePickerView.hidden = YES;
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];
    
    //    if ([GET_LOCALIZATION isEqualToString: kThai])
    //    {
    //        [outputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: kThai]];
    //        [outputFormatter setCalendar: [NSCalendar calendarWithIdentifier: NSCalendarIdentifierBuddhist]];
    //    }
    
    if(self.datePicker.tag == 1)
    {
        [self.btnStartDate setTitle:[outputFormatter stringFromDate:self.datePicker.date] forState:UIControlStateNormal];
        self.hasPressedStartDate = YES;
    }
    else
    {
        [self.btnEndDate setTitle:[outputFormatter stringFromDate:self.datePicker.date] forState:UIControlStateNormal];
        self.hasPressedEndDate = YES;
    }
    [self validateNextButton];
}

-(void) validateNextButton
{
    if (self.hasPressedOilchange &&
        self.hasPressedStartDate &&
        self.hasPressedEndDate)
    {
        self.btnNext.enabled = YES;
        self.btnNext.alpha = 1.0f;
    }
}

- (IBAction)oilChangePressed:(UIButton *)sender
{
    self.hasPressedOilchange = YES;
    [self validateNextButton];
    if (sender == self.btnWithOilChange)
    {
        self.btnWithOilChange.selected = YES;
        self.btnWithoutOilChange.selected = NO;
        
        [self.btnWithOilChange setBackgroundColor: COLOUR_YELLOW];
        [self.btnWithoutOilChange setBackgroundColor: COLOUR_PALEGREY];
    }
    else
    {
        self.btnWithOilChange.selected = NO;
        self.btnWithoutOilChange.selected = YES;
        
        [self.btnWithOilChange setBackgroundColor: COLOUR_PALEGREY];
        [self.btnWithoutOilChange setBackgroundColor: COLOUR_YELLOW];
    }
}


- (IBAction)nextPressed:(id)sender
{
    if (!self.submitParams)
        self.submitParams = [[NSMutableDictionary alloc] init];
    
    [self.submitParams setObject: [self convertDate: self.btnStartDate.titleLabel.text] forKey: @"StartDate"];
    [self.submitParams setObject: [self convertDate: self.btnEndDate.titleLabel.text] forKey: @"EndDate"];
    
    [self.submitParams setObject: @(1) forKey:@"Step"];
    [self.submitParams setObject: @(0.0) forKey:@"OfferValue"];
    
    if (self.btnWithOilChange.selected)
        [self.submitParams setObject: @"1" forKey:@"OfferType"];
    else if (self.btnWithoutOilChange.selected)
        [self.submitParams setObject: @"2" forKey:@"OfferType"];
    else
    {
        [mAlert showErrorAlertWithMessage: @"Unknown Error"];
        NSLog(@"unknown error");
    }
    
//    NSDictionary *stepOneParams = @{@"OfferType": [self.submitParams objectForKey: @"OfferType"],
//                                    @"StartDate": [self convertDate: self.btnStartDate.titleLabel.text],
//                                    @"EndDate": [self convertDate: self.btnEndDate.titleLabel.text],
//                                    };
    NSDictionary *stepOneParams = [[NSDictionary alloc] initWithDictionary: self.submitParams];
    
    [[mSession workshopOfferDict] setObject: stepOneParams  forKey: @"StepOne"];
    
    if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_RUSSIA]) {
        [[WebServiceManager sharedInstance] submitRUAncillaryOfferWithDict: self.submitParams vc: self];
    } else {
        [[WebServiceManager sharedInstance] submitAncillaryOfferWithDict: self.submitParams vc: self];
    }
}

-(void) checkEditDetails
{
    NSDictionary *stepOneParams = [[mSession workshopOfferDict] objectForKey: @"StepOne"];
    if (stepOneParams)
    {
        self.submitParams = [[NSMutableDictionary alloc] initWithDictionary: stepOneParams];
        
        [self.btnStartDate setTitle: [self convertDateFromApi: [stepOneParams objectForKey: @"StartDate"]] forState:UIControlStateNormal];
        [self.btnEndDate setTitle: [self convertDateFromApi: [stepOneParams objectForKey: @"EndDate"]] forState:UIControlStateNormal];
        
        
        if ([stepOneParams objectForKey: @"IsWithOilChange"])
        {
            if ([[stepOneParams objectForKey: @"IsWithOilChange"] intValue] == 1)
            {
                self.btnWithOilChange.selected = YES;
                self.btnWithoutOilChange.selected = NO;
                
                [self.btnWithOilChange setBackgroundColor: COLOUR_YELLOW];
                [self.btnWithoutOilChange setBackgroundColor: COLOUR_PALEGREY];
            }
            else if ([[stepOneParams objectForKey: @"IsWithOilChange"] intValue] == 2)
            {
                
                self.btnWithOilChange.selected = NO;
                self.btnWithoutOilChange.selected = YES;
                
                [self.btnWithOilChange setBackgroundColor: COLOUR_PALEGREY];
                [self.btnWithoutOilChange setBackgroundColor: COLOUR_YELLOW];
            }
        }
        else
        {
            if ([[stepOneParams objectForKey: @"OfferType"] isEqualToString: @"1"])
            {
                self.btnWithOilChange.selected = YES;
                self.btnWithoutOilChange.selected = NO;
                
                [self.btnWithOilChange setBackgroundColor: COLOUR_YELLOW];
                [self.btnWithoutOilChange setBackgroundColor: COLOUR_PALEGREY];
            }
            else if ([[stepOneParams objectForKey: @"OfferType"] isEqualToString: @"2"])
            {
                self.btnWithOilChange.selected = NO;
                self.btnWithoutOilChange.selected = YES;
                
                [self.btnWithOilChange setBackgroundColor: COLOUR_PALEGREY];
                [self.btnWithoutOilChange setBackgroundColor: COLOUR_YELLOW];
            }
        }
        self.hasPressedOilchange = YES;
        self.hasPressedStartDate = YES;
        self.hasPressedEndDate = YES;
        
        [self validateNextButton];
    }
}

-(NSString *)convertDate:(NSString* )dateToConvert
{   
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat:@"dd MMM yyyy"];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy/MM/dd"];
    
    NSDate *date = [inputFormatter dateFromString: dateToConvert];
    NSString *dateString = @"";
    if (date)
        dateString = [outputFormatter stringFromDate: date];
    return dateString;
}

-(NSString *)convertDateFromApi:(NSString* )dateToConvert
{
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat:@"yyyy/MM/dd"];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd MMM yyyy"];
    
    NSDate *date = [inputFormatter dateFromString: dateToConvert];
    NSString *dateString = @"";
    if (date)
        dateString = [outputFormatter stringFromDate: date];
    return dateString;
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_SUBMITANCILLARY:
            if (![[response getGenericResponse] isKindOfClass: [NSNull class]])
            {
                for (NSString *key in [[response getGenericResponse] allKeys])
                {
                    [self.submitParams setObject: [[response getGenericResponse] objectForKey: key] forKey: key];
                }
            }
            [mSession pushRUAddNewOfferStepTwoWithOffer:self.submitParams vc: self];
            break;

        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
