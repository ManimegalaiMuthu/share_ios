//
//  RUWorkshopOfferAddNew_SuccessViewController.m
//  Shell
//
//  Created by Jeremy Lua on 14/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUWorkshopOfferAddNew_SuccessViewController.h"

@interface RUWorkshopOfferAddNew_SuccessViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;

@property (weak, nonatomic) IBOutlet UIButton *btnViewOffers;

@end

@implementation RUWorkshopOfferAddNew_SuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle: self title: LOCALIZATION(C_RU_OFFERSUCCESS_TITLE) subtitle: @""];            //lokalised
 
//    self.lblHeader.text = LOCALIZATION(C_RU_OFFERSUCCESS_HEADER);
    
//    NSDictionary *dictData = 
    
    self.lblHeader.text = [self.submitParams objectForKey: @"Header"];
    self.lblMessage.text = [self.submitParams objectForKey: @"Message"];
    
    [self.btnViewOffers setTitle: LOCALIZATION(C_RU_OFFERSUCCESS_VIEWALLOFFERS) forState: UIControlStateNormal]; //lokalised
    [self.btnViewOffers setBackgroundColor:COLOUR_RED];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_RU_OFFERSUCCESS_TITLE) screenClass:nil];
}

- (IBAction)viewAllOffersPressed:(id)sender {
    [mSession loadWorkshopOfferView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
