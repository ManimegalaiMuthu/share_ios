//
//  RUWorkshopOfferDetailsViewController.m
//  Shell
//
//  Created by Jeremy Lua on 15/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUWorkshopOfferDetailsViewController.h"

@interface RUWorkshopOfferDetailsViewController () <WebServiceManagerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imgPending;
@property (weak, nonatomic) IBOutlet UILabel *lblPending;

@property (weak, nonatomic) IBOutlet UIImageView *imgFirstArrow;

@property (weak, nonatomic) IBOutlet UIImageView *imgScheduled;
@property (weak, nonatomic) IBOutlet UILabel *lblScheduled;

@property (weak, nonatomic) IBOutlet UIImageView *imgSecondArrow;

@property (weak, nonatomic) IBOutlet UIImageView *imgOngoing;
@property (weak, nonatomic) IBOutlet UILabel *lblOngoing;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderText;
@property (weak, nonatomic) IBOutlet UIView *redemptionOverviewView;

@property (weak, nonatomic) IBOutlet UIButton *btnEditDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelOffer;
@property (weak, nonatomic) IBOutlet UIButton *btnReorder;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
//@property BOOL isHidingOverview;
@end

@implementation RUWorkshopOfferDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    self.lblPending.text = LOCALIZATION(C_RUSSIA_OFFER_PENDING);
//    self.lblScheduled.text = LOCALIZATION(C_RUSSIA_OFFER_SCHEDULED);
//    self.lblOngoing.text = LOCALIZATION(C_RUSSIA_OFFER_ONGOING);
    
    self.lblPending.text = [mSession convertToAncilliaryOfferStatus: AncilliaryOfferStatus_PENDING];
    
    self.lblScheduled.text = [mSession convertToAncilliaryOfferStatus: AncilliaryOfferStatus_SCHEDULED];
    self.lblOngoing.text = [mSession convertToAncilliaryOfferStatus: AncilliaryOfferStatus_ONGOING];
    
    
    [self.btnEditDetails setTitle: LOCALIZATION(C_RUSSIA_EDITDETAILS) forState: UIControlStateNormal];      //lokalised
    [self.btnEditDetails setBackgroundColor:COLOUR_YELLOW];
    [self.btnCancelOffer setTitle: LOCALIZATION(C_RUSSIA_CANCELOFFER) forState: UIControlStateNormal];      //lokalised
    [self.btnCancelOffer setBackgroundColor:COLOUR_RED];
    [self.btnReorder setTitle: LOCALIZATION(C_RUSSIA_CREATEOFFERAGAIN) forState: UIControlStateNormal];     //lokalised
    [self.btnReorder setBackgroundColor:COLOUR_RED];
    
    self.redemptionOverviewView.hidden = YES;
    
    if (kIsRightToLeft) {
        self.imgFirstArrow.image = [UIImage imageNamed:@"icon-arrsingleL.png"];
        self.imgSecondArrow.image = [UIImage imageNamed:@"icon-arrsingleL.png"];
    } else {
        self.imgFirstArrow.image = [UIImage imageNamed:@"icon-arrsingleR.png"];
        self.imgSecondArrow.image = [UIImage imageNamed:@"icon-arrsingleR.png"];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_WORKSHOPOFFER) screenClass:nil];
}

enum
{
    AncilliaryOfferStatus_PENDING   = 1 << 0, //1 - pending
    AncilliaryOfferStatus_SCHEDULED = 1 << 1, //2 - Scheduled
    AncilliaryOfferStatus_ONGOING   = 1 << 2, //4 - on going
    AncilliaryOfferStatus_EXPIRED   = 1 << 3, //8 - expired
    AncilliaryOfferStatus_CANCELLED = 1 << 4, //16 - cancelled
    AncilliaryOfferStatus_ALL       = 1 << 5, //32 - all status
} AncilliaryOfferStatus_TYPES;

-(void)setupInterfaceWithData :(NSDictionary *)responseDict
{
    [super setupInterfaceWithData: responseDict];

    //update top bar here
//    NSString *indexString = [responseDict objectForKey: @"OfferStatus"];
    self.redemptionOverviewView.hidden = NO;
    self.btnReorder.hidden = YES;
    self.btnEditDetails.hidden = YES;
    self.btnCancelOffer.hidden = YES;
    
    if (![self.headerView superview])
    {
        [self.scrollView addSubview: self.headerView];
    }
    
    if (![[responseDict objectForKey: @"ShowHeaderText"] boolValue])
    {
        [self.headerView removeFromSuperview]; //remove and bump elements up
    }
    
    switch ([[responseDict objectForKey: @"OfferStatus"] intValue])
    {
//        case AncilliaryOfferStatus_PENDING:
//            break;
        case AncilliaryOfferStatus_SCHEDULED:
            [self.imgScheduled setImage: [UIImage imageNamed: @"icn_scheduled_on.png"]];
            [self.redemptionOverviewView removeFromSuperview];
            if ([[responseDict objectForKey: @"CanEdit"] boolValue])
                self.btnEditDetails.hidden = NO; //if can edit = 1
            
            if ([[responseDict objectForKey: @"CanCancel"] boolValue])
                self.btnCancelOffer.hidden = NO;
            break;
        case AncilliaryOfferStatus_ONGOING:
            [self.imgScheduled setImage: [UIImage imageNamed: @"icn_scheduled_on.png"]];
            [self.imgOngoing setImage: [UIImage imageNamed: @"icn_ongoing_on.png"]];
            break;
        case AncilliaryOfferStatus_EXPIRED:
            self.btnReorder.hidden = NO;
            [self.imgScheduled setImage: [UIImage imageNamed: @"icn_scheduled_on.png"]];
            
            self.lblOngoing.text = [mSession convertToAncilliaryOfferStatus: [[responseDict objectForKey: @"OfferStatus"] intValue]];
            [self.imgOngoing setImage: [UIImage imageNamed: @"icn_cancelled_expired.png"]];
            break;
        case AncilliaryOfferStatus_CANCELLED:
            [self.imgScheduled setImage: [UIImage imageNamed: @"icn_scheduled_on.png"]];
            
            self.lblOngoing.text = [mSession convertToAncilliaryOfferStatus: [[responseDict objectForKey: @"OfferStatus"] intValue]];
            [self.imgOngoing setImage: [UIImage imageNamed: @"icn_cancelled_expired.png"]];
            
            [self.redemptionOverviewView removeFromSuperview];
            self.btnReorder.hidden = NO;
            break;
//        case AncilliaryOfferStatus_ALL:
//            break;
            
        default:
            [self.redemptionOverviewView removeFromSuperview];
            self.btnEditDetails.hidden = NO;
            self.btnCancelOffer.hidden = NO;
            break;
    }
    
    self.lblHeaderText.text = [responseDict objectForKey: @"HeaderText"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelOfferPressed:(id)sender
{
    [self popUpYesNoWithTitle: LOCALIZATION(C_RUSSIA_CANCELOFFERHEADER)     //lokalised
                      content: LOCALIZATION(C_RUSSIA_CANCELOFFERBODY)       //lokalised
                  yesBtnTitle: LOCALIZATION(C_RUSSIA_YESCANCEL)             //lokalised
                   noBtnTitle: LOCALIZATION(C_FORM_NO)                      //lokalised
                 onYesPressed:^(BOOL finished) {
                     NSMutableDictionary *submitParams = [[NSMutableDictionary alloc] init];
//                     [submitParams setObject: [self.offerDict objectForKey: @"OfferStatus"] forKey: @"OfferStatus"];
                     [submitParams setObject: @(AncilliaryOfferStatus_CANCELLED).stringValue forKey: @"OfferStatus"];

                     [submitParams setObject: [self.offerDict objectForKey: @"OfferID"] forKey: @"OfferID"];
                     
                     [[WebServiceManager sharedInstance] cancelAncillaryOfferVcWithOrderDetails: submitParams vc: self];
                 }
                  onNoPressed: nil];
}

- (IBAction)editDetailsPressed:(id)sender
{
    NSMutableDictionary *submitParams = [[NSMutableDictionary alloc] initWithDictionary: self.offerDict];
    
    
    [mSession setWorkshopOfferDict: [[NSMutableDictionary alloc] init]]; //renew dictionary
    [[mSession workshopOfferDict] setObject: submitParams  forKey: @"StepOne"];

    [mSession pushRUAddNewOfferStepOne:self];
//    [mSession pushRUAddNewOfferStepOneWithOffer: submitParams vc: self];
}

- (IBAction)reorderPressed:(id)sender
{
    NSMutableDictionary *submitParams = [[NSMutableDictionary alloc] initWithDictionary: self.offerDict];
    
//    isWithOilChange == offerType
//    IsReCreateOffer = false (It is for recreate new from Cancelled/Expried Offer)
    
    [submitParams setObject: @(YES) forKey: @"IsReCreateOffer"];
    
    [mSession setWorkshopOfferDict: [[NSMutableDictionary alloc] init]]; //renew dictionary
    [[mSession workshopOfferDict] setObject: submitParams  forKey: @"StepOne"];
//    [mSession pushRUAddNewOfferStepOneWithOffer: submitParams vc: self];
    
    [mSession pushRUAddNewOfferStepOne:self];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
