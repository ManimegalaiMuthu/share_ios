//
//  RUWorkshopOfferSelectBrandViewController.m
//  Shell
//
//  Created by Jeremy Lua on 14/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUWorkshopOfferSelectCarBrandViewController.h"
#import "RUWorkshopOfferSelectOilBrandViewController.h"

@interface RUWorkshopOfferSelectCarBrandViewController () <UITableViewDelegate, UITableViewDataSource, WebServiceManagerDelegate, RUWorkshopOfferSelectOilBrandDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UITableView *carBrandTableView;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@property UIButton *btnSelectAll; //reference to header

@property NSMutableArray *selectedArray;
@property NSMutableArray *carBrandArray;
@property NSMutableDictionary *submitParams;
@end

@implementation RUWorkshopOfferSelectCarBrandViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA]) {
        [Helper setNavigationBarTitle: self title: LOCALIZATION(C_RU_SELECTCAR_ALLCARBRANDS) subtitle: @""];        //lokalised
    } else {
        [Helper setNavigationBarTitle: self title: LOCALIZATION(C_RU_SELECTBYPRODUCTUSED) subtitle: @""];
    }

    self.lblHeader.text = LOCALIZATION(C_RU_SELECTCAR_HEADER);                                                  //loklaised
    
    self.carBrandTableView.tableFooterView = [UIView new];
    
    UIButton *btnBack = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30, 30)];
    [btnBack setImage: [UIImage imageNamed: @"icon-arrsingleL.png"] forState: UIControlStateNormal];
    [btnBack addTarget:self action: @selector(popSelf) forControlEvents: UIControlEventTouchUpInside];
    if(@available(iOS 11, *)){
        [btnBack.widthAnchor constraintEqualToConstant: btnBack.frame.size.width].active = YES;
        [btnBack.heightAnchor constraintEqualToConstant: btnBack.frame.size.height].active = YES;
    }
    
    UIBarButtonItem *backBarBtn = [[UIBarButtonItem alloc] initWithCustomView: btnBack];
    self.navigationItem.leftBarButtonItem = backBarBtn;
    
    
    UIButton *btnInfo = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30, 30)];
    [btnInfo setImage: [UIImage imageNamed: @"icn_offer_darkgrey_15x15.png"] forState: UIControlStateNormal];
    [btnInfo addTarget:self action: @selector(infoPressed) forControlEvents: UIControlEventTouchUpInside];
    if(@available(iOS 11, *)){
        [btnInfo.widthAnchor constraintEqualToConstant: btnInfo.frame.size.width].active = YES;
        [btnInfo.heightAnchor constraintEqualToConstant: btnInfo.frame.size.height].active = YES;
    }
    UIBarButtonItem *infoBarBtn = [[UIBarButtonItem alloc] initWithCustomView: btnInfo];
    self.navigationItem.rightBarButtonItem = infoBarBtn;
    
    self.carBrandArray = [[NSArray alloc] initWithArray: [self.carBrandSubmitParams objectForKey: @"SelectedCustomers"]];
    self.selectedArray = [[NSMutableArray alloc] init];
    for (NSMutableDictionary *cellData in self.carBrandArray)
    {
        [self.selectedArray addObject: @(NO)];
    }
    
    [self.btnNext setTitle: LOCALIZATION(C_RUSSIA_NEXT_BTN) forState: UIControlStateNormal];        //lokalised
    [self.btnNext setBackgroundColor:COLOUR_RED];
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_WORKSHOPOFFER),LOCALIZATION_EN(C_RU_SELECTCAR_ALLCARBRANDS)] screenClass:nil];
    }  else {
        [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_WORKSHOPOFFER),LOCALIZATION_EN(C_RU_SELECTBYPRODUCTUSED)] screenClass:nil];
    }
}

-(void) infoPressed
{
    [self popUpViewWithTitle: LOCALIZATION(C_RU_WHATISTHIS) content: LOCALIZATION(C_RU_ADDOFFER_HELP)]; //lokalised
}

- (IBAction)nextPressed:(id)sender
{
    //prepare array of selected elements to be updated
    NSMutableArray *updatedArray = [[NSMutableArray alloc] init];
    for (int i =0; i < [self.selectedArray count]; i++)
    {
        NSDictionary *cellData = [self.carBrandArray objectAtIndex: i];
        
        if ([[self.selectedArray objectAtIndex: i] boolValue])
        {
            //add if selected
            [updatedArray addObject: cellData];
        }
    }
    
    //pass the array to oil brand selection, then pull all to delegate
//    {
//        Count = 1;    //eligible @ step two
//        VehicleBrandID = 12;  //for API
//        VehicleBrandName = GAZ; //car brand append @ step two
//    }
    NSInteger totalCount = 0;
    NSString *vehicleBrandIDString = @"";
    NSString *vehicleBrandNameString = @"";
    self.submitParams = [[NSMutableDictionary alloc] initWithDictionary: self.carBrandSubmitParams];
    
    if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA]) {
        //prepare vehicle brands to update
        for (NSDictionary *dict in updatedArray)
        {
            if ([vehicleBrandIDString length] > 0)
            {
                //append
                vehicleBrandIDString = [vehicleBrandIDString stringByAppendingString: @","];
                NSString *vehicleBrandID = [[dict objectForKey: @"VehicleBrandID"] stringValue];
                vehicleBrandIDString = [vehicleBrandIDString stringByAppendingString: vehicleBrandID];
                
                vehicleBrandNameString = [vehicleBrandNameString stringByAppendingString: @","];
                NSString *vehicleBrandName = [dict objectForKey: @"VehicleBrandName"];
                vehicleBrandNameString = [vehicleBrandNameString stringByAppendingString: vehicleBrandName];
            }
            else
            {
                vehicleBrandIDString = [[dict objectForKey: @"VehicleBrandID"] stringValue];
                vehicleBrandNameString = [dict objectForKey: @"VehicleBrandName"];
            }
            totalCount += [[dict objectForKey: @"Count"] intValue];
        }
        
        if (self.btnSelectAll.selected)
        {
            vehicleBrandNameString = LOCALIZATION(C_RU_SELECTCAR_ALLCARBRANDS);         //lokalised
            //Match with android, all selected = All
            //if manually selected all brands, ignored.
        }
        
        //get oil change
        //set from step two button press
        //    [submitParams setObject: @(2) forKey: @"Step"];
        //    [submitParams setObject: @(1) forKey: @"SubStep"];
    } else {
        for (NSDictionary *dict in updatedArray)
        {
            if ([vehicleBrandIDString length] > 0)
            {
                //append
                vehicleBrandIDString = [vehicleBrandIDString stringByAppendingString: @","];
                NSString *vehicleBrandID = [[dict objectForKey: @"ProductGroupID"] stringValue];
                vehicleBrandIDString = [vehicleBrandIDString stringByAppendingString: vehicleBrandID];
                
                vehicleBrandNameString = [vehicleBrandNameString stringByAppendingString: @","];
                NSString *vehicleBrandName = [dict objectForKey: @"ProductGroupName"];
                vehicleBrandNameString = [vehicleBrandNameString stringByAppendingString: vehicleBrandName];
            }
            else
            {
                vehicleBrandIDString = [[dict objectForKey: @"ProductGroupID"] stringValue];
                vehicleBrandNameString = [dict objectForKey: @"ProductGroupName"];
            }
            totalCount += [[dict objectForKey: @"Count"] intValue];
            
            if (self.btnSelectAll.selected)
            {
                vehicleBrandNameString = LOCALIZATION(C_TITLE_ALLPRODUCT);
            }
        }
        
    }
    
    [self.submitParams setObject: vehicleBrandNameString forKey: @"VehicleBrandsNames"];
    [self.submitParams setObject: vehicleBrandIDString forKey: @"VehicleBrands"]; //string of vehicle IDs
    [self.submitParams setObject: @(totalCount) forKey: @"TotalCustomers"]; //set total customers count
    
    [self.submitParams setObject: @(self.btnSelectAll.selected) forKey: @"IsAllMembers"]; //select all flag

//    if (withOilChange)// to select oil brands if has oilchange
//            [self.submitParams setObject: @"2" forKey:@"OfferType"];
    if ([[self.submitParams objectForKey: @"OfferType"] isEqualToString: @"1"])
    {
        if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_RUSSIA]){
             [[WebServiceManager sharedInstance] submitRUAncillaryOfferWithDict: self.submitParams vc:self];
        } else {
            [self.delegate selectedCustomerDelegate: self.submitParams];
            [self popSelf];
        }
    }
    else if ([[self.submitParams objectForKey: @"OfferType"] isEqualToString: @"2"])
    {
        [self.delegate selectedCustomerDelegate: self.submitParams];
        [self popSelf];
    }
    else //execute delegate and return
    {
        NSLog(@"unknown error!");
        
        [self popSelf];
    }
}

-(void)selectedOilBrandsDelegate:(NSDictionary *)brandDict
{
    //oil change required
    
    //pass brand dict directly.
    [self.delegate selectedCustomerDelegate: brandDict];
    
    NSInteger vcIndex = [[self.parentViewController childViewControllers] indexOfObject: self.delegate];
    [self popToIndex: vcIndex];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_SUBMITANCILLARY:
        {
            //go to oil brand if with oil change
            NSDictionary *respData = [response getGenericResponse];
            
            [self.submitParams setObject: [respData objectForKey: @"SelectedCustomers"]  forKey:@"SelectedCustomers"];
            [mSession pushRUAddNewOfferSelectOilBrand: self.submitParams vc: self];
            
//            {
//                //handle without oilchange here
//                [self.delegate selectedCustomerDelegate: self.submitParams];
//            }
        }
            break;
            
        default:
            break;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell;
    if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_RUSSIA]) {
        cell = [tableView dequeueReusableCellWithIdentifier: @"SelectAllHeaderCell"];
        
        UILabel *lblTitle = [cell viewWithTag: 1];
        lblTitle.text = LOCALIZATION(C_RU_SELECTCAR_ALLCARBRANDS);              //lokalised
        
        UILabel *lblCount = [cell viewWithTag: 2];
        lblCount.text = [NSString stringWithFormat: @"(%@)", [[self.carBrandSubmitParams objectForKey: @"TotalCustomers"] stringValue]];
        
        self.btnSelectAll = [cell viewWithTag: 3];
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier: @"ProductSelectionHeaderCell"];
        
        [cell setBackgroundColor:COLOUR_VERYPALEGREY];
        cell.tintColor = COLOUR_VERYPALEGREY;
        
        UIImageView *imageView = [cell viewWithTag:999];
        imageView.image = [UIImage imageNamed:@"icn_shell"];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        UILabel *lblTitle = [cell viewWithTag: 1];
        lblTitle.text = LOCALIZATION(C_TITLE_ALLPRODUCT);
        
        UILabel *lblCount = [cell viewWithTag: 2];
        lblCount.text = [NSString stringWithFormat: @"(%@)", [[self.carBrandSubmitParams objectForKey: @"TotalCustomers"] stringValue]];
        
        self.btnSelectAll = [cell viewWithTag: 3];
    }
    
    return cell.contentView;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath: indexPath];
    
    UIButton *btnSelected = [cell viewWithTag: 3];
    btnSelected.selected = !btnSelected.selected;
    [self.selectedArray replaceObjectAtIndex: indexPath.row withObject: @(btnSelected.selected)];
    
    //if selected, skip validation
    [self validateNextButton: btnSelected.selected];
    
    //deselect header
    if (!btnSelected.selected)
    {
        self.btnSelectAll.selected = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    UITableViewCell *cell ;
    
    if([GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_RUSSIA]) {
        cell = [tableView dequeueReusableCellWithIdentifier: @"CarBrandCell"];
        
        NSDictionary *cellData = [self.carBrandArray objectAtIndex: indexPath.row];
        
        UILabel *lblBrand = [cell viewWithTag: 1];
        lblBrand.text = [cellData objectForKey: @"VehicleBrandName"];
        
        UILabel *lblCount = [cell viewWithTag: 2];
        lblCount.text = [NSString stringWithFormat: @"(%@)", [[cellData objectForKey: @"Count"] stringValue]];
        
        UIButton *btnSelected = [cell viewWithTag: 3];
        btnSelected.selected = [[self.selectedArray objectAtIndex: indexPath.row] boolValue];
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier: @"ProductSelectionDisplayCell"];
        
        NSDictionary *cellData = [self.carBrandArray objectAtIndex: indexPath.row];
        
        UIImageView *imageView = [cell viewWithTag:999];
        NSString *urlString = [[cellData objectForKey: @"ProductGroupImage"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        [imageView sd_setImageWithURL: [NSURL URLWithString: urlString] placeholderImage: [UIImage imageNamed: @"bottle"] options:
         (SDWebImageRefreshCached | SDWebImageRetryFailed)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        UILabel *lblBrand = [cell viewWithTag: 1];
        lblBrand.text = [cellData objectForKey: @"ProductGroupName"];
        
        UILabel *lblCount = [cell viewWithTag: 2];
        lblCount.text = [NSString stringWithFormat: @"(%@)", [[cellData objectForKey: @"Count"] stringValue]];
        
        UIButton *btnSelected = [cell viewWithTag: 3];
        btnSelected.selected = [[self.selectedArray objectAtIndex: indexPath.row] boolValue];
    }
    
    return cell;
}

- (IBAction)selectAll:(UIButton *)sender
{
    sender.selected = !sender.selected;
    
    for (UITableViewCell *cell in self.carBrandTableView.subviews)
    {
        NSIndexPath *cellIndexPath = [self.carBrandTableView indexPathForCell: cell];
        
        UIButton *btnSelected = [cell viewWithTag: 3];
        btnSelected.selected = sender.selected;
    }
    
    for (int i =0; i < [self.selectedArray count]; i++)
    {
        if (i < [self.selectedArray count])
            [self.selectedArray replaceObjectAtIndex: i withObject: @(sender.selected)];
        else
        {
            NSLog(@"unknown error!");
            [self.selectedArray addObject: @(sender.selected)];
        }
    }
    
    //select all just skip validation
    [self validateNextButton: sender.selected];
}

//YES to skip
-(void) validateNextButton:(BOOL) skipValidation
{
    if (skipValidation)
    {
        self.btnNext.enabled = YES;
        self.btnNext.alpha = 1.0f;
    }
    
    for (id selected in self.selectedArray)
    {
        NSNumber *test = selected;
        if (test.boolValue)
        {
            self.btnNext.enabled = YES;
            self.btnNext.alpha = 1.0f;
            return;
        }
    }
    self.btnNext.enabled = NO;
    self.btnNext.alpha = 0.5f;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.carBrandArray count];
}

@end
