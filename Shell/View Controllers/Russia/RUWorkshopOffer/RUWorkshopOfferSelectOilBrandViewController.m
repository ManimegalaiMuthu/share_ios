//
//  RUWorkshopOfferSelectOilBrandViewController.m
//  Shell
//
//  Created by Jeremy Lua on 14/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUWorkshopOfferSelectOilBrandViewController.h"

@interface RUWorkshopOfferSelectOilBrandViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UITableView *oilBrandTableView;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@property UIButton *btnSelectAll; //reference to header

@property NSMutableArray *selectedArray;
@property NSMutableArray *oilBrandArray;
@property NSMutableDictionary *submitParams;

@end

@implementation RUWorkshopOfferSelectOilBrandViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle: self title: LOCALIZATION(C_RU_SELECTOIL_TITLE) subtitle: @"" size: 15 subtitleSize: 0];      //lokalised
    
    self.lblHeader.text = LOCALIZATION(C_RU_SELECTCAR_HEADER);          //lokalised
    
    UIButton *btnBack = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30, 30)];
    [btnBack setImage: [UIImage imageNamed: @"icon-arrsingleL.png"] forState: UIControlStateNormal];
    [btnBack addTarget:self action: @selector(popSelf) forControlEvents: UIControlEventTouchUpInside];
    if(@available(iOS 11, *)){
        [btnBack.widthAnchor constraintEqualToConstant: btnBack.frame.size.width].active = YES;
        [btnBack.heightAnchor constraintEqualToConstant: btnBack.frame.size.height].active = YES;
    }
    
    UIBarButtonItem *backBarBtn = [[UIBarButtonItem alloc] initWithCustomView: btnBack];
    self.navigationItem.leftBarButtonItem = backBarBtn;
    
    UIButton *btnInfo = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30, 30)];
    [btnInfo setImage: [UIImage imageNamed: @"icn_offer_darkgrey_15x15.png"] forState: UIControlStateNormal];
    [btnInfo addTarget:self action: @selector(infoPressed) forControlEvents: UIControlEventTouchUpInside];
    if(@available(iOS 11, *)){
        [btnInfo.widthAnchor constraintEqualToConstant: btnInfo.frame.size.width].active = YES;
        [btnInfo.heightAnchor constraintEqualToConstant: btnInfo.frame.size.height].active = YES;
    }
    UIBarButtonItem *infoBarBtn = [[UIBarButtonItem alloc] initWithCustomView: btnInfo];
    self.navigationItem.rightBarButtonItem = infoBarBtn;
    
    //oil array
    self.oilBrandArray = [[NSArray alloc] initWithArray: [self.oilBrandSubmitParams objectForKey: @"SelectedCustomers"]];
    self.selectedArray = [[NSMutableArray alloc] init];
    for (NSMutableDictionary *cellData in self.oilBrandArray)
    {
        [self.selectedArray addObject: @(NO)];
    }
    
    [self.btnNext setTitle: LOCALIZATION(C_RUSSIA_NEXT_BTN) forState: UIControlStateNormal];            //lokalised
    [self.btnNext setBackgroundColor:COLOUR_RED];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / %@",LOCALIZATION_EN(C_TITLE_WORKSHOPOFFER),LOCALIZATION_EN(C_RU_SELECTOIL_TITLE)] screenClass:nil];
}

-(void) infoPressed
{
    [self popUpViewWithTitle: LOCALIZATION(C_RU_WHATISTHIS) content: LOCALIZATION(C_RU_ADDOFFER_HELP)];  //lokalised
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)nextPressed:(id)sender
{
    //prepare array of selected elements to be updated
    NSMutableArray *updatedArray = [[NSMutableArray alloc] init];
    for (int i =0; i < [self.selectedArray count]; i++)
    {
        NSDictionary *cellData = [self.oilBrandArray objectAtIndex: i];
        
        if ([[self.selectedArray objectAtIndex: i] boolValue])
        {
            //add if selected
            [updatedArray addObject: cellData];
        }
    }
    
    //pass the array to oil brand selection, then pull all to delegate
//    {
//        Count = 2;
//        OilBrandID = 64300201;
//        OilBrandName = Shell;
//    }
    
    //prepare vehicle brands to update
    NSString *oilBrandIDString = @"";
    NSString *oilBrandNameString = @"";
    NSInteger totalCount = 0;
    for (NSDictionary *dict in updatedArray)
    {
        if ([oilBrandIDString length] > 0)
        {
            //append
            oilBrandIDString = [oilBrandIDString stringByAppendingString: @","];
            
            NSString *vehicleBrandID = [[dict objectForKey: @"OilBrandID"] stringValue];
            oilBrandIDString = [oilBrandIDString stringByAppendingString: vehicleBrandID];
            
            oilBrandNameString = [oilBrandNameString stringByAppendingString: @","];
            NSString *oilBrandName = [dict objectForKey: @"OilBrandName"];
            oilBrandNameString = [oilBrandNameString stringByAppendingString: oilBrandName];
        }
        else
        {
            oilBrandIDString = [[dict objectForKey: @"OilBrandID"] stringValue];
            oilBrandNameString = [dict objectForKey: @"OilBrandName"];
        }
        totalCount += [[dict objectForKey: @"Count"] intValue];
    }
    
    if ([updatedArray count] > 1 && [updatedArray count] == [self.oilBrandArray count])
    {
        oilBrandNameString = LOCALIZATION(C_RU_SELECTOIL_ALLBRANDS);                    //lokalise 11 Feb
        //change to All if all brands selected. Single brands list will be ignored
    }
    
    self.submitParams = [[NSMutableDictionary alloc] initWithDictionary: self.oilBrandSubmitParams];
    
    //get oil change
    //set from step two button press
    //    [submitParams setObject: @(2) forKey: @"Step"];
    //    [submitParams setObject: @(1) forKey: @"SubStep"];
    
    [self.submitParams setObject: @(totalCount) forKey: @"TotalCustomers"]; //set total customers count
                                      
    [self.submitParams setObject: oilBrandIDString forKey: @"ProductBrandID"]; //string of oil brand IDs
    [self.submitParams setObject: oilBrandNameString forKey: @"ProductBrandNames"]; //string of oil brand names
                                      
    [self.submitParams removeObjectForKey: @"SelectedCustomers"]; //not needed anymore
    
    if ([[self.submitParams objectForKey: @"IsAllMembers"] boolValue])  //check flag again if true
        [self.submitParams setObject: @(self.btnSelectAll.selected) forKey: @"IsAllMembers"]; //select all flag

    
    //no API call required. call delegate method.
    //car brand delegate will pop self to return to step two
    [self.delegate selectedOilBrandsDelegate: self.submitParams];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"SelectAllHeaderCell"];
    
    UILabel *lblTitle = [cell viewWithTag: 1];
    lblTitle.text = LOCALIZATION(C_RU_SELECTCAR_ALLCARBRANDS);          //lokalised
    //    lblTitle.text = LOCALIZATION(@"ALL");
    
    self.btnSelectAll = [cell viewWithTag: 3];
    
    return cell.contentView;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath: indexPath];
    
    UIButton *btnSelected = [cell viewWithTag: 3];
    btnSelected.selected = !btnSelected.selected;
    [self.selectedArray replaceObjectAtIndex: indexPath.row withObject: @(btnSelected.selected)];
    
    //if selected, skip validation
    [self validateNextButton: btnSelected.selected];
    
    //deselect header
    if (!btnSelected.selected)
    {
        self.btnSelectAll.selected = NO;
    }
}
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"OilBrandCell"];
    
    NSDictionary *cellData = [self.oilBrandArray objectAtIndex: indexPath.row];
    
    UILabel *lblBrand = [cell viewWithTag: 1];
    lblBrand.text = [cellData objectForKey: @"OilBrandName"];
    
    UILabel *lblCount = [cell viewWithTag: 2];
    lblCount.text = [NSString stringWithFormat: @"(%@)", [[cellData objectForKey: @"Count"] stringValue]];
    
    UIButton *btnSelected = [cell viewWithTag: 3];
    btnSelected.selected = [[self.selectedArray objectAtIndex: indexPath.row] boolValue];
    
    return cell;
}

- (IBAction)selectAll:(UIButton *)sender
{
    sender.selected = !sender.selected;
    
    for (UITableViewCell *cell in self.oilBrandTableView.subviews)
    {
        NSIndexPath *cellIndexPath = [self.oilBrandTableView indexPathForCell: cell];
        
        UIButton *btnSelected = [cell viewWithTag: 3];
        btnSelected.selected = sender.selected;
    }
    
    for (int i =0; i < [self.selectedArray count]; i++)
    {
        if (i < [self.selectedArray count])
            [self.selectedArray replaceObjectAtIndex: i withObject: @(sender.selected)];
        else
        {
            NSLog(@"unknown error!");
            [self.selectedArray addObject: @(sender.selected)];
        }
    }
    self.lblHeader.text = LOCALIZATION(C_RU_SELECTCAR_HEADER);          //lokalised

    
    //select all just skip validation
    [self validateNextButton: sender.selected];
}

//YES to skip
-(void) validateNextButton:(BOOL) skipValidation
{
    if (skipValidation)
    {
        self.btnNext.enabled = YES;
        self.btnNext.alpha = 1.0f;
    }
    
    for (id selected in self.selectedArray)
    {
        NSNumber *test = selected;
        if (test.boolValue)
        {
            self.btnNext.enabled = YES;
            self.btnNext.alpha = 1.0f;
            return;
        }
    }
    self.btnNext.enabled = NO;
    self.btnNext.alpha = 0.5f;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.oilBrandArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}
@end
