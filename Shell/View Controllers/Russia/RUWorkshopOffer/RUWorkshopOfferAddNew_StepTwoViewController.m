//
//  RUWorkshopOfferAddNew_StepTwoViewController.m
//  Shell
//
//  Created by Jeremy Lua on 14/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUWorkshopOfferAddNew_StepTwoViewController.h"
#import "RUWorkshopOfferSelectCarBrandViewController.h"
#import "RUWorkshopOfferSelectOilBrandViewController.h"

@interface RUWorkshopOfferAddNew_StepTwoViewController () <WebServiceManagerDelegate, RUWorkshopOfferSelectCarBrandDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UILabel *lblSelectCustomerGroup;
@property (weak, nonatomic) IBOutlet UIButton *btnCustomerGroupInfo;
@property (weak, nonatomic) IBOutlet UIButton *btnAll;
@property (weak, nonatomic) IBOutlet UIButton *btnSelected;

@property (weak, nonatomic) IBOutlet UILabel *lblCarBrands;
@property (weak, nonatomic) IBOutlet UILabel *lblCarBrandsData;
@property (weak, nonatomic) IBOutlet UILabel *lblOilBrands;
@property (weak, nonatomic) IBOutlet UILabel *lblOilBrandsData;

@property (weak, nonatomic) IBOutlet UIButton *btnEdit;


@property (weak, nonatomic) IBOutlet UIView *customerGroupView;

@property (weak, nonatomic) IBOutlet UILabel *lblEligibleCustomer;
@property (weak, nonatomic) IBOutlet UILabel *lblEligibleCustomerData;

@property NSString *vehicleBrandIDString;
@property NSString *oilBrandIDString;

@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property NSMutableDictionary *submitParamsStepTwo;

//Progress Numbers
@property (weak, nonatomic) IBOutlet UIImageView *progressOneImageView;
@property (weak, nonatomic) IBOutlet UIImageView *progressTwoImageView;
@property (weak, nonatomic) IBOutlet UIView *progressBar_ProgressView;
@end

@implementation RUWorkshopOfferAddNew_StepTwoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle: self title: LOCALIZATION(C_RUSSIA_ADDNEWOFFER) subtitle: @""];       //lokalised
    
    
    self.lblHeader.text = LOCALIZATION(C_RU_STEPTWO_CHOOSEAUDIENCE);                                    //loklaised
    self.lblSelectCustomerGroup.text = LOCALIZATION(C_RU_STEPTWO_SELECTCUSTOMERFGROUP);                 //loklaised
    self.lblEligibleCustomer.text = LOCALIZATION(C_RUSSIA_OFFERS_PROMOELIGIBLE);                        //lokalised
    

    NSAttributedString *allCustomerAttrString = [[NSAttributedString alloc] initWithString: LOCALIZATION(C_RU_STEPTWO_ALLCUSTOMERS) attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_BUTTON, }];                                                           //lokalised
    [self.btnAll setAttributedTitle:allCustomerAttrString forState: UIControlStateNormal];
    
    NSAttributedString *selectedCustomerAttrString = [[NSAttributedString alloc] initWithString: LOCALIZATION(C_RU_STEPTWO_SELECTEDCUSTOMERS) attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_BUTTON, }];                                                            //lokalised
    [self.btnSelected setAttributedTitle:selectedCustomerAttrString forState: UIControlStateNormal];
    
    
    [self.btnNext setTitle: LOCALIZATION(C_RUSSIA_NEXT_BTN) forState: UIControlStateNormal];            //lokalised
    self.btnNext.backgroundColor = COLOUR_RED;
    
    UIButton *btnBack = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30, 30)];
    [btnBack setImage: [UIImage imageNamed: @"icon-arrsingleL.png"] forState: UIControlStateNormal];
    [btnBack addTarget:self action: @selector(popSelf) forControlEvents: UIControlEventTouchUpInside];
    if(@available(iOS 11, *)){
        [btnBack.widthAnchor constraintEqualToConstant: btnBack.frame.size.width].active = YES;
        [btnBack.heightAnchor constraintEqualToConstant: btnBack.frame.size.height].active = YES;
    }
    
    UIBarButtonItem *backBarBtn = [[UIBarButtonItem alloc] initWithCustomView: btnBack];
    self.navigationItem.leftBarButtonItem = backBarBtn;
    
    UIButton *btnInfo = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30, 30)];
    [btnInfo setImage: [UIImage imageNamed: @"icn_offer_darkgrey_15x15.png"] forState: UIControlStateNormal];
    [btnInfo addTarget:self action: @selector(infoPressed) forControlEvents: UIControlEventTouchUpInside];
    if(@available(iOS 11, *)){
        [btnInfo.widthAnchor constraintEqualToConstant: btnInfo.frame.size.width].active = YES;
        [btnInfo.heightAnchor constraintEqualToConstant: btnInfo.frame.size.height].active = YES;
    }
    UIBarButtonItem *infoBarBtn = [[UIBarButtonItem alloc] initWithCustomView: btnInfo];
    self.navigationItem.rightBarButtonItem = infoBarBtn;
    
    [self.btnEdit setAttributedTitle: [[NSAttributedString alloc] initWithString: LOCALIZATION(C_RUSSIA_EDIT_BTN) attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B1, NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle), }] forState: UIControlStateNormal];           //lokalised
    
    [self setNavBackBtn];
    if(GET_ISADVANCE) {
        [self.progressOneImageView setImage: [UIImage imageNamed:@"progressnum-blue-1"]];
        [self.progressTwoImageView setImage: [UIImage imageNamed:@"progressnum-blue-2"]];
    } else {
        [self.progressOneImageView setImage: [UIImage imageNamed:@"progressnum-red-1"]];
        [self.progressTwoImageView setImage: [UIImage imageNamed:@"progressnum-red-2"]];
    }
    self.progressBar_ProgressView.backgroundColor = COLOUR_RED;
    
    [self checkEditDetails];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / Step 2",LOCALIZATION_EN(C_TITLE_WORKSHOPOFFER)] screenClass:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)allCustomerPressed:(id)sender
{
    self.btnAll.selected = YES;
    self.btnSelected.selected = NO;
    [self.btnAll setBackgroundColor: COLOUR_YELLOW];
    [self.btnSelected setBackgroundColor: COLOUR_PALEGREY];
    self.btnNext.enabled = YES;
    self.btnNext.alpha = 1.0f;
    
    self.lblCarBrands.text = @"";
    self.lblCarBrandsData.text = @"";
    self.lblOilBrands.text = @"";
    self.lblOilBrandsData.text = @"";
    
    self.btnEdit.hidden = YES;
    
    self.lblEligibleCustomerData.text = [[self.submitParams objectForKey: @"TotalCustomers"] stringValue];
    self.lblEligibleCustomerData.textColor = COLOUR_RED;
    
    NSString *keyName = @"VehicleBrandID";
    if(![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA]) {
        keyName = @"ProductGroupID";
    }
    
    self.vehicleBrandIDString = @"";
    for (NSDictionary *dict in [self.submitParams objectForKey: @"SelectedCustomers"])
    {
        if ([self.vehicleBrandIDString length] > 0)
        {
            //append
            self.vehicleBrandIDString = [self.vehicleBrandIDString stringByAppendingString: @","];
            
            NSString *vehicleBrandID = [[dict objectForKey: keyName] stringValue];
            self.vehicleBrandIDString = [self.vehicleBrandIDString stringByAppendingString: vehicleBrandID];
        }
        else
        {
            self.vehicleBrandIDString = [[dict objectForKey: keyName] stringValue];
        }
    }
    
    //show number of customers eligible only
    self.customerGroupView.hidden = NO;
}

- (IBAction)selectedCustomerPressed:(id)sender
{
    NSMutableDictionary *selectedCustomerDict = [[NSMutableDictionary alloc] initWithDictionary: self.submitParams];
    
    [selectedCustomerDict setObject: @(2) forKey: @"Step"];
    
    [selectedCustomerDict setObject: @(1) forKey:@"SubStep"]; //additional substep required for selected customer

    [mSession pushRUAddNewOfferSelectCarBrand: selectedCustomerDict vc: self];

}

- (IBAction)customerGroupInfoPressed:(id)sender
{
    //to be replaced
    NSString *contentString = [NSString stringWithFormat: @"%@\n\n%@\n%@\n\n%@\n%@",
                               LOCALIZATION(C_RU_STEPTWO_HELP),
                               LOCALIZATION(C_RU_STEPTWO_ALLCUSTOMERS),
                               LOCALIZATION(C_RU_STEPTWO_HELP_ALLCUST_BODY),
                               LOCALIZATION(C_RU_STEPTWO_SELECTEDCUSTOMERS),
                               LOCALIZATION(C_RU_STEPTWO_HELP_SELCUST_BODY)
                               ];                               //lokalised
    [self popUpViewWithTitle: LOCALIZATION(C_RU_WHATISTHIS) content: contentString];        //lokalised
}

-(void) infoPressed
{
    [self popUpViewWithTitle: LOCALIZATION(C_RU_WHATISTHIS) content: LOCALIZATION(C_RU_ADDOFFER_HELP)];     //lokalised
}

- (IBAction)editPressed:(id)sender
{
    [self selectedCustomerPressed: nil];
}


//Selected Customer delegates
-(void) selectedCustomerDelegate: (NSDictionary *) brandDict
{
    if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA]) {
         self.lblCarBrands.text = LOCALIZATION(C_RU_STEPTWO_CARBRANDS);         //lokalised
    } else {
        self.lblCarBrands.text = LOCALIZATION(C_RUSSIA_PRODUCTUSED);
    }
    self.lblCarBrandsData.text = [brandDict objectForKey: @"VehicleBrandsNames"];
    self.vehicleBrandIDString = [brandDict objectForKey: @"VehicleBrands"];
    
    //check for oil brands length, if more than 0, oil brand selected
    if ([[brandDict objectForKey: @"ProductBrandID"] length] > 0)
    {
        self.lblOilBrands.text =  LOCALIZATION(C_RU_STEPTWO_OILBRANDS);                     //lokalised
        self.lblOilBrandsData.text = [brandDict objectForKey: @"ProductBrandNames"];
        self.oilBrandIDString = [brandDict objectForKey: @"ProductBrandID"];
    }
    else
    {
        [self.submitParamsStepTwo removeObjectForKey: @"ProductBrandID"];
        self.lblOilBrands.text = @"";
        self.lblOilBrandsData.text = @"";
    }
    
    //only change button after delegate is done.
    
    //have to physically count each vehicle brand "Count"
    self.lblEligibleCustomerData.text = [[brandDict objectForKey: @"TotalCustomers"] stringValue];
    
    self.btnEdit.hidden = NO;
    self.customerGroupView.hidden = NO;
    self.btnNext.enabled = YES;
    self.btnNext.alpha = 1.0f;
    
    self.btnAll.selected = NO;
    self.btnSelected.selected = YES;
    [self.btnAll setBackgroundColor: COLOUR_PALEGREY];
    [self.btnSelected setBackgroundColor: COLOUR_YELLOW];
}

- (IBAction)nextPressed:(id)sender
{
    self.submitParamsStepTwo = [[NSMutableDictionary alloc] initWithDictionary: self.submitParams];
    
    if (self.btnAll.selected)
    {
        [self.submitParamsStepTwo setObject: @(0) forKey:@"SubStep"];
        [self.submitParamsStepTwo removeObjectForKey: @"SelectedCustomers"];
        
        [self.submitParamsStepTwo setObject: @(YES) forKey:@"IsAllMembers"]; //selecting all members
    }
    else
        [self.submitParamsStepTwo setObject: @(1) forKey:@"SubStep"];
    
    [self.submitParamsStepTwo setObject: @(2) forKey:@"Step"];
    
    if ([self.vehicleBrandIDString length] > 0)
        [self.submitParamsStepTwo setObject: self.vehicleBrandIDString forKey: @"VehicleBrands"];

    if ([self.oilBrandIDString length] > 0)
        [self.submitParamsStepTwo setObject: self.oilBrandIDString forKey: @"ProductBrandID"];

    
    {
        NSMutableDictionary *stepTwoParams = [[NSMutableDictionary alloc] init];
        [stepTwoParams setObject: @(self.btnAll.selected) forKey: @"isAllPressed"];
        [stepTwoParams setObject: @(self.btnSelected.selected) forKey: @"isSelectedPressed"];
        [stepTwoParams setObject: self.lblCarBrandsData.text forKey: @"VehicleBrandsName"];
        
        if ([self.vehicleBrandIDString length] > 0)
            [stepTwoParams setObject: self.vehicleBrandIDString forKey: @"VehicleBrands"];
        
        if ([self.oilBrandIDString length] > 0)
            [stepTwoParams setObject: self.oilBrandIDString forKey: @"ProductBrandID"];
        
        [stepTwoParams setObject: self.lblOilBrandsData.text forKey: @"ProductBrandName"];
        
        [stepTwoParams setObject: @(self.lblEligibleCustomerData.text.intValue) forKey: @"TotalCustomers"];
        
        [stepTwoParams setObject: @(self.lblEligibleCustomerData.text.intValue) forKey: @"TotalCustomers"];
        
        [[mSession workshopOfferDict] setObject: stepTwoParams  forKey: @"StepTwo"];
    }
    
    if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA]) {
        [[WebServiceManager sharedInstance] submitRUAncillaryOfferWithDict: self.submitParamsStepTwo vc: self];
    } else {
        [[WebServiceManager sharedInstance] submitAncillaryOfferWithDict: self.submitParamsStepTwo vc: self];
    }
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_SUBMITANCILLARY:
//            if (![[response getGenericResponse] isKindOfClass: [NSNull class]])
//            {
//                for (NSString *key in [[response getGenericResponse] allKeys])
//                {
//                    [self.submitParamsStepTwo setObject: [[response getGenericResponse] objectForKey: key] forKey: key];
//                }
//            }
            if ([self.submitParamsStepTwo objectForKey: @"OfferID"])
            {
                NSDictionary *respData = [response getGenericResponse];
                [self.submitParamsStepTwo setObject: [respData objectForKey: @"OfferCategoryType"]   forKey: @"OfferCategoryType"];[self.submitParamsStepTwo setObject: [respData objectForKey: @"OfferCategoryCode"]   forKey: @"OfferCategoryCode"];
                [self.submitParamsStepTwo setObject: [respData objectForKey: @"OfferOption"]   forKey: @"OfferOption"];
                [self.submitParamsStepTwo setObject: [respData objectForKey: @"OfferValue"]   forKey: @"OfferValue"];
                
                [[mSession workshopOfferDict] setObject: self.submitParamsStepTwo forKey: @"StepThree"];
            }
            //just use existing params.
            [mSession pushRUAddNewOfferStepThreeWithOffer:self.submitParamsStepTwo vc: self];
            break;
            
        default:
            break;
    }
}


-(void) checkEditDetails
{
    NSDictionary *stepTwoParams = [[mSession workshopOfferDict] objectForKey: @"StepTwo"];
    if (stepTwoParams)
    {
        //step two can only be created by next btn
        self.btnNext.enabled = YES;
        self.btnNext.alpha = 1.0f;
        self.customerGroupView.hidden = NO;
        
        //one will definitely be selected
        self.btnAll.selected = [[stepTwoParams objectForKey: @"isAllPressed"] boolValue];
        self.btnSelected.selected = [[stepTwoParams objectForKey: @"isSelectedPressed"] boolValue];
        
        self.lblEligibleCustomerData.text = [[stepTwoParams objectForKey: @"TotalCustomers"] stringValue];
        
        if (self.btnAll.selected)
        {
            [self allCustomerPressed: nil];
        }
        else if (self.btnSelected.selected)
        {
            [self.btnSelected setBackgroundColor: COLOUR_YELLOW];
            [self.btnAll setBackgroundColor: COLOUR_PALEGREY];
            
            self.lblCarBrandsData.text = [stepTwoParams objectForKey: @"VehicleBrandsName"];
            self.lblOilBrandsData.text = [stepTwoParams objectForKey: @"ProductBrandName"];
        }
        
        if ([stepTwoParams objectForKey: @"VehicleBrands"])
            self.vehicleBrandIDString = [stepTwoParams objectForKey: @"VehicleBrands"];
        
        if ([stepTwoParams objectForKey: @"ProductBrandID"])
            self.oilBrandIDString = [stepTwoParams objectForKey: @"ProductBrandID"];
        
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
