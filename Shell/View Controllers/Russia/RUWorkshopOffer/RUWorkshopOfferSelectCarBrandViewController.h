//
//  RUWorkshopOfferSelectBrandViewController.h
//  Shell
//
//  Created by Jeremy Lua on 14/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@protocol RUWorkshopOfferSelectCarBrandDelegate
-(void) selectedCustomerDelegate: (NSDictionary *) brandDict;
@end

@interface RUWorkshopOfferSelectCarBrandViewController : BaseVC
@property NSDictionary *carBrandSubmitParams;
@property id<RUWorkshopOfferSelectCarBrandDelegate> delegate;
@end
