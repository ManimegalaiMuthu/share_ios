//
//  RUWorkshopOfferListViewController.m
//  Shell
//
//  Created by Jeremy Lua on 14/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUWorkshopOfferListViewController.h"
#import "WorkshopOfferAllOfferListViewController.h"

#import "CalendarEventViewController.h"

@interface RUWorkshopOfferListViewController () <WorkshopOfferAllOfferDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnCalendarView;
@property (weak, nonatomic) IBOutlet UIButton *btnListView;
@property (weak, nonatomic) IBOutlet UIView *vcView;
@property (weak, nonatomic) CalendarEventViewController *calendarVc;
//@property (weak, nonatomic) ListView *calendarVc;
@property (weak, nonatomic) WorkshopOfferAllOfferListViewController *offerListVc;

@property (weak, nonatomic) IBOutlet UIButton *btnAdd;

@end

@implementation RUWorkshopOfferListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle: self title: LOCALIZATION(C_TITLE_WORKSHOPOFFER) subtitle: @"" size: 15 subtitleSize:0];      //lokalised
    
    [self.btnCalendarView setTitle: LOCALIZATION(C_RUSSIA_OFFERS_CALENDAR) forState: UIControlStateNormal];     //lokalised
    [self.btnListView setTitle: LOCALIZATION(C_RUSSIA_OFFERS_LIST) forState: UIControlStateNormal];     //lokalised
    
    UIButton *btnInfo = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30, 30)];
    [btnInfo setImage: [UIImage imageNamed: @"icn_offer_darkgrey_15x15.png"] forState: UIControlStateNormal];
    [btnInfo addTarget:self action: @selector(infoPressed) forControlEvents: UIControlEventTouchUpInside];
    if(@available(iOS 11, *)){
        [btnInfo.widthAnchor constraintEqualToConstant: btnInfo.frame.size.width].active = YES;
        [btnInfo.heightAnchor constraintEqualToConstant: btnInfo.frame.size.height].active = YES;
    }
    UIBarButtonItem *infoBarBtn = [[UIBarButtonItem alloc] initWithCustomView: btnInfo];
    self.navigationItem.rightBarButtonItem = infoBarBtn;

    [self.btnCalendarView.titleLabel setFont: FONT_B2];
    [self.btnListView.titleLabel setFont: FONT_B2];
    
    self.calendarVc = [STORYBOARD_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_WORKSHOPOFFER_CALENDERVIEW];
    [self addChildViewController: self.calendarVc];
    
    self.offerListVc = [STORYBOARD_WORKSHOPOFFER instantiateViewControllerWithIdentifier: VIEW_WORKSHOPOFFER_ALLLISTOFFER];
    self.offerListVc.delegate = self;
    [self addChildViewController: self.offerListVc];
    
    [self btnPressed: self.btnCalendarView]; //select calendar by default
    
    UIImage *addImage = GET_ISADVANCE ? [UIImage imageNamed:@"btn_addoffer_blue@3x"] : [UIImage imageNamed:@"btn_addoffer_red"];
    [self.btnAdd setImage:addImage forState:UIControlStateNormal];
    
    if (kIsRightToLeft) {
        [self.btnListView setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
        [self.btnCalendarView setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
    } else {
        [self.btnListView setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
        [self.btnCalendarView setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_WORKSHOPOFFER) screenClass:nil];
}

-(void) infoPressed
{
    [self popUpViewWithTitle: LOCALIZATION(C_RU_WHATISTHIS) content: LOCALIZATION(C_RU_ADDOFFER_HELP)];         //loklaised
}

- (IBAction)addOfferPressed:(id)sender
{
    [mSession setWorkshopOfferDict: [[NSMutableDictionary alloc] init]]; //renew dictionary
    [mSession pushRUAddNewOfferStepOne: self];
}

- (IBAction)btnPressed:(UIButton *)sender
{
    if (sender == self.btnCalendarView && !self.btnCalendarView.selected)
    {
        self.btnCalendarView.selected = YES;
        [self.btnCalendarView setTitleColor: COLOUR_WHITE forState: UIControlStateNormal];
        self.btnCalendarView.backgroundColor = COLOUR_RED;
        
        self.btnListView.selected = NO;
        [self.btnListView setTitleColor: COLOUR_VERYDARKGREY forState: UIControlStateNormal];
        self.btnListView.backgroundColor = COLOUR_WHITE;
        
        [self.offerListVc.view removeFromSuperview];
        [self.vcView addSubview: self.calendarVc.view];
        [self.calendarVc.view setFrame: CGRectMake(0, 0, self.vcView.frame.size.width, self.vcView.frame.size.height)];
        
    }
    else if (sender == self.btnListView && !self.btnListView.selected)
    {
        self.btnListView.selected = YES;
        [self.btnListView setTitleColor: COLOUR_WHITE forState: UIControlStateNormal];
        self.btnListView.backgroundColor = COLOUR_RED;
        
        self.btnCalendarView.selected = NO;
        [self.btnCalendarView
         setTitleColor: COLOUR_VERYDARKGREY forState: UIControlStateNormal];
        self.btnCalendarView.backgroundColor = COLOUR_WHITE;
        
        [self.calendarVc.view removeFromSuperview];
        [self.vcView addSubview: self.offerListVc.view];
        [self.offerListVc.view setFrame: CGRectMake(0, 0, self.vcView.frame.size.width, self.vcView.frame.size.height)];
        
    }
}

-(void) reloadAllOffersList:(NSArray *)data
{
    self.calendarVc.offersDataArray = data;
    [self.calendarVc reloadCalendar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
