//
//  RUWorkshopOfferAddNew_StepFourViewController.m
//  Shell
//
//  Created by Jeremy Lua on 14/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUWorkshopOfferAddNew_StepFourViewController.h"

@interface RUWorkshopOfferAddNew_StepFourViewController () <WebServiceManagerDelegate, TTTAttributedLabelDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblOfferName;
@property (weak, nonatomic) IBOutlet UILabel *lblOfferNameData;

@property (weak, nonatomic) IBOutlet UILabel *lblOfferPeriod;
@property (weak, nonatomic) IBOutlet UILabel *lblOfferPeriodData;

@property (weak, nonatomic) IBOutlet UILabel *lblEligibleCustomer;
@property (weak, nonatomic) IBOutlet UILabel *lblEligibleCustomerData;

@property (weak, nonatomic) IBOutlet UILabel *lblOfferType;
@property (weak, nonatomic) IBOutlet UILabel *lblOfferTypeData;

@property (weak, nonatomic) IBOutlet UILabel *lblMessagePreviewHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblMessagePreview;
@property (weak, nonatomic) IBOutlet UIButton *btnMsgInfoBtn;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckbox;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblTnc;

@property (weak, nonatomic) IBOutlet UIButton *btnEditDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;

//Progress Numbers
@property (weak, nonatomic) IBOutlet UIImageView *progressOneImageView;
@property (weak, nonatomic) IBOutlet UIImageView *progressTwoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *progressThreeImageView;
@property (weak, nonatomic) IBOutlet UIImageView *progressFourImageView;

@property (weak, nonatomic) IBOutlet UIView *progressBar_ProgressView;
@property (weak, nonatomic) IBOutlet UIView *progressBar_ProgressView_Two;
@property (weak, nonatomic) IBOutlet UIView *progressBar_ProgressView_Three;

@end

@implementation RUWorkshopOfferAddNew_StepFourViewController

- (void)viewDidLoad {
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle: self title: LOCALIZATION(C_RUSSIA_ADDNEWOFFER) subtitle: @""];           //lokalised
    
    UIButton *btnBack = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30, 30)];
    [btnBack setImage: [UIImage imageNamed: @"icon-arrsingleL.png"] forState: UIControlStateNormal];
    [btnBack addTarget:self action: @selector(popSelf) forControlEvents: UIControlEventTouchUpInside];
    if(@available(iOS 11, *)){
        [btnBack.widthAnchor constraintEqualToConstant: btnBack.frame.size.width].active = YES;
        [btnBack.heightAnchor constraintEqualToConstant: btnBack.frame.size.height].active = YES;
    }
    
    UIBarButtonItem *backBarBtn = [[UIBarButtonItem alloc] initWithCustomView: btnBack];
    self.navigationItem.leftBarButtonItem = backBarBtn;
    
    UIButton *btnInfo = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30, 30)];
    [btnInfo setImage: [UIImage imageNamed: @"icn_offer_darkgrey_15x15.png"] forState: UIControlStateNormal];
    [btnInfo addTarget:self action: @selector(infoPressed) forControlEvents: UIControlEventTouchUpInside];
    if(@available(iOS 11, *)){
        [btnInfo.widthAnchor constraintEqualToConstant: btnInfo.frame.size.width].active = YES;
        [btnInfo.heightAnchor constraintEqualToConstant: btnInfo.frame.size.height].active = YES;
    }
    UIBarButtonItem *infoBarBtn = [[UIBarButtonItem alloc] initWithCustomView: btnInfo];
    self.navigationItem.rightBarButtonItem = infoBarBtn;
    
    
    self.lblOfferName.text = LOCALIZATION(C_RUSSIA_OFFERS_PROMONAME);                       //lokalised
    self.lblOfferNameData.text = [self.submitParams objectForKey: @"OfferTitle"];
    
    self.lblOfferPeriod.text = LOCALIZATION(C_RUSSIA_OFFERS_PROMOPERIOD);                   //lokalised
    self.lblOfferPeriodData.text = [self.submitParams objectForKey: @"OfferPeriod"];
    
    self.lblEligibleCustomer.text = LOCALIZATION(C_RUSSIA_OFFERS_PROMOELIGIBLE);            //lokalised
    self.lblEligibleCustomerData.text = [[self.submitParams objectForKey: @"TargetCustomers"] stringValue];
    
    self.lblOfferType.text = LOCALIZATION(C_RUSSIA_OFFERS_PROMOTYPE);                       //lokalised
    self.lblOfferTypeData.text = [self.submitParams objectForKey: @"OfferTypeText"];
    
    self.lblMessagePreviewHeader.text = LOCALIZATION(C_RU_STEPFOUR_MESSAGEPREVIEW);         //lokalised
    self.lblMessagePreview.text = [self.submitParams objectForKey: @"SMSPreview"];

    [self.btnEditDetails setTitle: LOCALIZATION(C_RUSSIA_EDITDETAILS) forState: UIControlStateNormal];      //lokalised
    [self.btnEditDetails setBackgroundColor:COLOUR_YELLOW];
    [self.btnConfirm setTitle: LOCALIZATION(C_RU_STEPFOUR_CONFIRMPUBLISH) forState: UIControlStateNormal];  //lokalised
    [self.btnConfirm setBackgroundColor:COLOUR_RED];
    
    [Helper setHyperlinkLabel:self.lblTnc hyperlinkText: LOCALIZATION(C_RU_STEPFOUR_TNCLINK) hyperlinkFont: FONT_B2 bodyText: LOCALIZATION(C_RU_STEPFOUR_TNC) bodyFont:FONT_B2 urlString: @"TNC"];  //lokalised
//    [Helper addHyperlink:self.lblTnc hyperlinkText: LOCALIZATION(C_RU_STEPFOUR_PRIVACYLINK) hyperlinkFont: FONT_B2 urlString:@"Privacy" bodyText: self.lblTnc.text];
    
    [self setNavBackBtn];
    if(GET_ISADVANCE) {
        [self.progressOneImageView setImage: [UIImage imageNamed:@"progressnum-blue-1"]];
        [self.progressTwoImageView setImage: [UIImage imageNamed:@"progressnum-blue-2"]];
        [self.progressThreeImageView setImage: [UIImage imageNamed:@"progressnum-blue-3"]];
        [self.progressFourImageView setImage: [UIImage imageNamed:@"progressnum-blue-4"]];
    } else {
        [self.progressOneImageView setImage: [UIImage imageNamed:@"progressnum-red-1"]];
        [self.progressTwoImageView setImage: [UIImage imageNamed:@"progressnum-red-2"]];
        [self.progressThreeImageView setImage: [UIImage imageNamed:@"progressnum-red-3"]];
        [self.progressFourImageView setImage: [UIImage imageNamed:@"progressnum-red-4"]];
    }
    self.progressBar_ProgressView.backgroundColor = COLOUR_RED;
    self.progressBar_ProgressView_Two.backgroundColor = COLOUR_RED;
    self.progressBar_ProgressView_Three.backgroundColor = COLOUR_RED;

}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / Step 4",LOCALIZATION_EN(C_TITLE_WORKSHOPOFFER)] screenClass:nil];
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
//    if ([[url absoluteString] isEqualToString: @"TNC"])
//    {
//        NSDictionary *data = GET_WORKSHOP_TNC;

//        [self popUpViewWithTitle: LOCALIZATION(C_SIDEMENU_TNC) content: [self.submitParams objectForKey: @"TnC"]];
//        [self popupInitWithHTMLBody: [data objectForKey: @"TncText"] headerTitle: LOCALIZATION(C_SIDEMENU_TNC) buttonTitle: LOCALIZATION(C_GLOBAL_OK)];     //lokalised
//    }
//    else if ([[url absoluteString] isEqualToString: @"Privacy"])
//    {
//        NSString *urlAddress = [NSString stringWithFormat:@"%@%@%@?c=%@",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_PRIVACY_URL, GET_COUNTRY_CODE];
//        [self popupInitWithURL: urlAddress headerTitle: LOCALIZATION(C_TITLE_PRIVACY) buttonTitle: LOCALIZATION(C_GLOBAL_OK)];
//    }
    
    if ([[url absoluteString] isEqualToString: @"TNC"])
    {
        NSString *urlAddress =  [NSString stringWithFormat:@"%@%@%@?c=%@&type=%i",[WebServiceManager serverNoApi_Prefix], PAGES_PREFIX, PAGES_TNC_URL, GET_COUNTRY_CODE, 1];

        [self popupInitWithURL: urlAddress headerTitle: LOCALIZATION(C_SIDEMENU_TNC) buttonTitle: LOCALIZATION(C_GLOBAL_OK)];
    }
}


-(void) infoPressed
{
    [self popUpViewWithTitle: LOCALIZATION(C_RU_WHATISTHIS) content: LOCALIZATION(C_RU_ADDOFFER_HELP)];         //lokalised
}

- (IBAction)msgInfoPressed:(id)sender
{
//    [self popUpViewWithTitle: LOCALIZATION(C_RU_WHATISTHIS) content: [[mSession lookupTable] objectForKey: @"SmsPreviewDesc"]];
    [self popUpViewWithTitle: LOCALIZATION(C_RU_WHATISTHIS) content: LOCALIZATION(C_RU_STEPFOUR_HELP)];         //lokalised
}

- (IBAction)checkboxPressed:(id)sender
{
    self.btnCheckbox.selected = !self.btnCheckbox.isSelected;
}

- (IBAction)editDetailsPressed:(id)sender
{
    NSInteger count = [[self.parentViewController childViewControllers] count];
    [self popToIndex: count - 3 - 1];
}

- (IBAction)confirmPressed:(id)sender
{
    NSMutableDictionary *stepFourParams = [[NSMutableDictionary alloc] initWithDictionary: self.submitParams];
    
    [stepFourParams setObject: @(4) forKey: @"Step"];
    [stepFourParams setObject: @(self.btnCheckbox.selected) forKey: @"IsAcceptTerms"];
    
    if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA]) {
        [[WebServiceManager sharedInstance] submitRUAncillaryOfferWithDict: stepFourParams vc: self];
    } else {
        [[WebServiceManager sharedInstance] submitAncillaryOfferWithDict: stepFourParams vc: self];
    }
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_SUBMITANCILLARY:
            [mSession loadRUAddNewOfferSuccessWithOffer: [[response getJSON] objectForKey: @"ResponseMessage"]];
            break;
            
        default:
            break;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
