//
//  RUWorkshopOfferSelectOilBrandViewController.h
//  Shell
//
//  Created by Jeremy Lua on 14/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"
@protocol RUWorkshopOfferSelectOilBrandDelegate
-(void) selectedOilBrandsDelegate: (NSDictionary *) brandDict;
@end

@interface RUWorkshopOfferSelectOilBrandViewController : BaseVC
@property NSDictionary *oilBrandSubmitParams;
@property id<RUWorkshopOfferSelectOilBrandDelegate> delegate;
@end
