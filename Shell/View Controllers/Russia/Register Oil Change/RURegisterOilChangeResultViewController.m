//
//  RURegisterOilChangeResultViewController.m
//  Shell
//
//  Created by Jeremy Lua on 27/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RURegisterOilChangeResultViewController.h"

@interface RURegisterOilChangeResultViewController () <UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet UITableView *resultTableView;
@property NSMutableArray *productArray;

@property BOOL canInvite;
@end

@implementation RURegisterOilChangeResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_RUSSIA_REGISTEROILCHANGE) subtitle: @"" size: 15 subtitleSize:0];              //lokalise 23 Jan
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];

    self.productArray = [self.resultsDict objectForKey: @"FullCoupons"];

    self.canInvite = [[self.resultsDict objectForKey: @"CanInvite"] boolValue];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:[NSString stringWithFormat:@"%@ / Success",LOCALIZATION_EN(C_RUSSIA_REGISTEROILCHANGE)] screenClass:nil];
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.productArray count] + @(self.canInvite).integerValue;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"HeaderCell"];
    
    UILabel *lblVehicleId = [cell viewWithTag: 1];
    lblVehicleId.text = LOCALIZATION(C_SEARCHVEHICLE_VEHICLEID);                                                //lokalise 23 Jan
    
    UILabel *lblVehicleIdData = [cell viewWithTag: 2];
    lblVehicleIdData.text = [self.resultsDict objectForKey: @"VehicleNumber"];
    //set from previous screen instead of taking from cell data
    
    UILabel *lblProductUsed = [cell viewWithTag: 3];
    lblProductUsed.text = LOCALIZATION(C_SEARCHVEHICLE_PRODUCTUSED);                                            //lokalise 23 Jan
    
    UILabel *lblMileage = [cell viewWithTag: 4];
    lblMileage.text = LOCALIZATION(C_SEARCHVEHICLE_MILEAGE);                                                    //lokalise 23 Jan
    
    return cell.contentView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (indexPath.row == [self.productArray count]) //count + 1, caninvite cell
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"InviteCell"];
        

        UILabel *lblHeader = [cell viewWithTag: 1];
        lblHeader.text = LOCALIZATION(C_RUSSIA_OILCHANGERESULT_NOTCONNECTED);                                       //lokalise 24 Jan
        
        UILabel *lblContent = [cell viewWithTag: 2];
        lblContent.text = LOCALIZATION(C_RUSSIA_OILCHANGERESULT_REGISTERCUSTOMER);                                  //lokalise 24 Jan
        
        UIButton *btnRegister = [cell viewWithTag: 3];
        btnRegister.titleLabel.textAlignment = NSTextAlignmentCenter;
        [btnRegister setTitle: LOCALIZATION(C_RUSSIA_OILCHANGERESULT_INVITEBTN) forState: UIControlStateNormal];     //lokalise 24 Jan
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"ProductCell"];
        
        NSDictionary *cellData = [self.productArray objectAtIndex: indexPath.row];
        
        UILabel *lblProductUsed = [cell viewWithTag: 1];
        lblProductUsed.text = [cellData objectForKey: @"Desc"];
        
        UILabel *lblMileage = [cell viewWithTag: 2];
//        lblMileage.text = [self.resultsDict objectForKey: @"Mileage"];
        lblMileage.text = [[cellData objectForKey: @"Mileage"] stringValue];
    }
    
    return cell;
}

- (IBAction)registerPressed:(id)sender
{
    [mSession loadRURegisterCustomer:[self.resultsDict objectForKey: @"VehicleNumber"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
