//
//  RURegisterOilChangeViewController.h
//  Shell
//
//  Created by Jeremy Lua on 27/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface RURegisterOilChangeViewController : BaseVC
@property NSString *vehicleNumberString;
@end
