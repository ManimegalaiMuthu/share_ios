//
//  RUChooseProductBrandViewController.h
//  Shell
//
//  Created by Jeremy Lua on 27/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@protocol RUChooseProductBrandDelegate <NSObject>
-(void) didChooseProduct:(NSDictionary *) productDictionary;
@end

@interface RUChooseProductBrandViewController : BaseVC
@property id<RUChooseProductBrandDelegate> delegate;
@end
