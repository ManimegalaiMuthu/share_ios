//
//  RUCustomerRedemptionSuccessViewController.h
//  Shell
//
//  Created by Jeremy Lua on 17/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface RUCustomerRedemptionSuccessViewController : BaseVC
@property NSDictionary *successDict;
@end
