//
//  RUSearchVehicleViewController.m
//  Shell
//
//  Created by Jeremy Lua on 27/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import <Photos/Photos.h>
#import "SWGApiClient.h"
#import "SWGConfiguration.h"
// load models
#import "SWGCoordinate.h"
#import "SWGInlineResponse200.h"
#import "SWGInlineResponse200ProcessingTime.h"
#import "SWGInlineResponse400.h"
#import "SWGPlateCandidate.h"
#import "SWGPlateDetails.h"
#import "SWGRegionOfInterest.h"
#import "SWGVehicleCandidate.h"
#import "SWGVehicleDetails.h"
// load API classes for accessing endpoints
#import "SWGDefaultApi.h"


#import "RUSearchVehicleViewController.h"

@interface RUSearchVehicleViewController () <WebServiceManagerDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UITextField *txtVehicleNumber;

@property (weak, nonatomic) IBOutlet UITableView *resultTableView;
@property NSMutableArray *resultArray;
@property NSArray *offerArray;

@property (weak, nonatomic) IBOutlet UIButton *btnRegisterOilChange;
@property (weak, nonatomic) IBOutlet UIView *memberDetailsView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *memberDetailsViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblMemberSince;


@property NSMutableArray *promotionDetailArray;

@end

@implementation RUSearchVehicleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_SEARCHVEHICLE) subtitle: @"" size:15 subtitleSize:0];        //lokalise 23 Jan
    
    self.lblHeader.text = LOCALIZATION(C_RUSSIA_SEARCHVEHICLEHEADER);           //lokalise 23 Jan
    
    self.txtVehicleNumber.placeholder = LOCALIZATION(C_RUSSIA_VEHICLE_EXAMPLE);     //lokalise 23 Jsan
    
    [self.btnRegisterOilChange setTitle:LOCALIZATION(C_RUSSIA_REGISTEROILCHANGE) forState:UIControlStateNormal];        //lokalise 23 Jan
    
    self.btnRegisterOilChange.enabled = NO;
    self.resultArray = [[NSMutableArray alloc] init];
    self.offerArray = [[NSArray alloc] init];
    
    self.resultTableView.tableFooterView = [UIView new];
    
     self.lblMemberSince.font = FONT_B2;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.memberDetailsViewHeight.constant = 0;
    self.memberDetailsView.hidden = YES;
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_SEARCHVEHICLE) screenClass:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)registerOilChangePressed:(id)sender {
    [mSession pushRURegisterOilChangeWithVehicleId: self.txtVehicleNumber.text vc: self];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell;
    
    if (section == 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"HeaderCell"];
        
        UILabel *lblDate = [cell viewWithTag: 1];
        lblDate.text = LOCALIZATION(C_SEARCHVEHICLE_SERVICEDATE);       //lokalise 23 Jan
        
        UILabel *lblDateFormat = [cell viewWithTag: 2];
        lblDateFormat.text = LOCALIZATION(C_RUSSIA_DATEFORMAT);         //lokalise 23 Jan
        
        
        UILabel *lblProductUsed = [cell viewWithTag: 3];
        lblProductUsed.text = LOCALIZATION(C_SEARCHVEHICLE_PRODUCTUSED);        //lokalise 23 Jan
        
        UILabel *lblMileage = [cell viewWithTag: 4];
        lblMileage.text = LOCALIZATION(C_SEARCHVEHICLE_MILEAGE);            //lokalise 23 Jan
    }
    else if (section == 1)
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"OfferHeaderCell"];
        
        UILabel *lblEntitledOffer = [cell viewWithTag: 1];
        lblEntitledOffer.text = LOCALIZATION(C_SEARCHVEHICLE_ENTITLEDOFFER);            //lokalise 23 Jan
        
        UILabel *lblOfferDetails = [cell viewWithTag: 2];
        lblOfferDetails.text = LOCALIZATION(C_SEARCHVEHICLE_OFFERDETAILS);              //lokalise 23 Jan
        
        UILabel *lblValidity = [cell viewWithTag: 3];
        lblValidity.text = LOCALIZATION(C_SEARCHVEHICLE_OFFERVALIDITY);                 //lokalise 23 Jan
        
        UIButton *btnRedeem = [cell viewWithTag: 4];
        [btnRedeem setTitle: LOCALIZATION(C_SEARCHVEHICLE_OFFERREDEEM) forState: UIControlStateNormal];     //lokalise 23 Jan
        
        if([[mSession profileInfo] hasCustomerRedemptionBtn]){
           [btnRedeem setHidden:NO];
        } else {
            [btnRedeem setHidden:YES];
        }
    }
    return cell.contentView;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
        return [self.resultArray count];
    else if (section == 1)
        return [self.offerArray count];
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (indexPath.section == 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"ResultCell"];
        
        NSDictionary *cellData = [self.resultArray objectAtIndex: indexPath.row];
        
        UILabel *lblDate = [cell viewWithTag: 1];
        lblDate.text = [cellData objectForKey: @"ServiceDate"];

        
        UILabel *lblProductUsed = [cell viewWithTag: 2];
        lblProductUsed.text = [cellData objectForKey: @"LastUsedProduct"];
        
        UILabel *lblMileage = [cell viewWithTag: 3];
        lblMileage.text = [cellData objectForKey: @"LastMileage"];
    }
    else if (indexPath.section == 1)
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"OfferCell"];
        
        NSDictionary *cellData = [self.offerArray objectAtIndex: indexPath.row];
        
        UILabel *lblPromo = [cell viewWithTag: 1];
        lblPromo.text = [cellData objectForKey: @"PromotionOffer"];
        
        UILabel *lblValidity = [cell viewWithTag: 2];
        lblValidity.text = [cellData objectForKey: @"Validity"];
    }
    
    return cell;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.txtVehicleNumber)
    {        
        NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
        NSString *trimmedString = [self.txtVehicleNumber.text stringByTrimmingCharactersInSet:charSet];
        
        if ([trimmedString isEqualToString:@""]) {
            //if string only whitespace
            
        }
        else
        {
            [[WebServiceManager sharedInstance] searchVehicle: self.txtVehicleNumber.text state: @"" vc:self];
        }
    }
}

//https://stackoverflow.com/questions/5765654/textfield-validation-with-regular-expression
- (BOOL)validateInputWithString:(NSString *)aString
{
    NSDictionary *regexLookup = [[[mSession lookupTable] objectForKey: @"RegexVal"] firstObject];
    
    NSString * const regularExpression = [regexLookup objectForKey: @"KeyValue"];
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regularExpression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    if (error) {
        NSLog(@"error %@", error);
    }
    
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:aString
                                                        options:0
                                                          range:NSMakeRange(0, [aString length])];
    return numberOfMatches > 0;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField.text length] + [string length] < 8)
    {
        self.btnRegisterOilChange.enabled = NO;
        self.btnRegisterOilChange.alpha = 0.5f;
    }
    else
    {
        self.btnRegisterOilChange.enabled = YES;
        self.btnRegisterOilChange.alpha = 1.0f;
    }
    
    //validate with regex
//    if(![self validateInputWithString: string])
//        return NO;
    
    if (textField.text.length >= 9 && ![string isEqualToString: @""])
        return NO;

    return ([string rangeOfCharacterFromSet: [[NSCharacterSet alphanumericCharacterSet] invertedSet]].location == NSNotFound);
}

- (IBAction)redeemPressed:(UIButton *) sender
{
    [mSession loadRUCustomerRedemption];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([self.offerArray count] > 0)
        return 2; //show offer table
    else
        return 1;
}

-(void)processCompleted:(WebServiceResponse *)response
{
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_SEARCHVEHICLE:
        {
            NSDictionary *searchData = [response getGenericResponse];
            
            self.resultArray = [searchData objectForKey: @"VehicleDetails"];
            //check for null data
            if (![[searchData objectForKey: @"VehicleDetails"] isKindOfClass:[NSNull class]])
            {
                NSDictionary *firstResultData = [self.resultArray firstObject];
                if ([[firstResultData objectForKey: @"LastUsedProduct"] length] > 0)
                {
                    [self.resultTableView reloadData];
                    if(![[searchData objectForKey:@"ConsumerJoinDate"] isKindOfClass:[NSNull class]]) {
                        NSString *memberSinceValue = [searchData objectForKey:@"ConsumerJoinDate"];
                        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:LOCALIZATION(C_VEHICLESEARCH_MEMBERSINCE) attributes:nil];        //lokalise 23 Jan
                        NSMutableAttributedString *dateString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",memberSinceValue] attributes:@{NSForegroundColorAttributeName: COLOUR_RED,NSFontAttributeName : FONT_H2}];
                        [attrString appendAttributedString:dateString];
                        self.lblMemberSince.attributedText = attrString;
                        self.memberDetailsView.hidden = NO;
                        self.memberDetailsViewHeight.constant = 35;
                        [self.view layoutSubviews];
                    } else {
                        self.memberDetailsView.hidden = YES;
                        self.memberDetailsViewHeight.constant = 0;
                    }
                }
                else
                {
                    [self.resultArray removeAllObjects];
                    self.memberDetailsView.hidden = YES;
                    self.memberDetailsViewHeight.constant = 0;
                }
            }
            
            if([[searchData objectForKey: @"PromotionDetail"] isKindOfClass:[NSNull class]]) {
                self.offerArray = @[];
            } else {
                self.offerArray = [searchData objectForKey: @"PromotionDetail"];
            }
            
            [self.resultTableView reloadData];
        }
            break;
        default:
            break;
    }
}

#pragma mark - Car Plate Scanning
- (IBAction)cameraPressed:(id)sender
{
    if ([self.txtVehicleNumber isFirstResponder])
    {
        [self.txtVehicleNumber resignFirstResponder];
        return;
    }
    
    [mSession pushPhotoTakingViewControllerWithParent: self];
}

-(void)imageTaken:(UIImage *)image key:(NSString *)keyString
{
    if (!image)
        return;
    
    UIImage *newImage = [self rotateImage:image];
    
    NSData *imgData = UIImageJPEGRepresentation(newImage, 0.1);
    NSString *base64String = [imgData base64EncodedStringWithOptions: 0];
    
    NSString* secretKey = @"sk_a94baee6f07c7836e8ef292d"; // The secret key used to authenticate your account.  You can view your  secret key by visiting  https://cloud.openalpr.com/
    NSString *country = [self getCountryCode];
    //    NSString* country = @"us"; // Defines the training data used by OpenALPR.  \"us\" analyzes  North-American style plates.  \"eu\" analyzes European-style plates.  This field is required if using the \"plate\" task  You may use multiple datasets by using commas between the country  codes.  For example, 'au,auwide' would analyze using both the  Australian plate styles.  A full list of supported country codes  can be found here https://github.com/openalpr/openalpr/tree/master/runtime_data/config
    //au, auwide, br, br2, eu, fr, gb, in, kr, kr2, mx, sg, us, vn2
    
    if([country length] == 0)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:@"Invalid Country"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {
                                 //BUTTON OK CLICK EVENT
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    NSNumber* recognizeVehicle = @(0); // If set to 1, the vehicle will also be recognized in the image This requires an additional credit per request  (optional) (default to 0)
    NSString* state = @""; // Corresponds to a US state or EU country code used by OpenALPR pattern  recognition.  For example, using \"md\" matches US plates against the  Maryland plate patterns.  Using \"fr\" matches European plates against  the French plate patterns.  (optional) (default to )
    NSNumber* returnImage = @(0); // If set to 1, the image you uploaded will be encoded in base64 and  sent back along with the response  (optional) (default to 0)
    NSNumber* topn = @(5); // The number of results you would like to be returned for plate  candidates and vehicle classifications  (optional) (default to 10)
    NSString* prewarp = @""; // Prewarp configuration is used to calibrate the analyses for the  angle of a particular camera.  More information is available here http://doc.openalpr.com/accuracy_improvements.html#calibration  (optional) (default to )
    
    SWGDefaultApi *apiInstance = [[SWGDefaultApi alloc] init];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [apiInstance recognizeBytesWithImageBytes:base64String
                                    secretKey:secretKey
                                      country:country
                             recognizeVehicle:recognizeVehicle
                                        state:state
                                  returnImage:returnImage
                                         topn:topn
                                      prewarp:prewarp
                            completionHandler: ^(SWGInlineResponse200* output, NSError* error) {
                                if (output)
                                {
                                    if ([output.results count] > 0)
                                    {
                                        SWGPlateDetails *plateDetails = [output.results objectAtIndex: 0];
                                        
                                        self.txtVehicleNumber.text = plateDetails.plate; //String of car plate
                                        [self.txtVehicleNumber becomeFirstResponder];
                                        
                                        self.btnRegisterOilChange.enabled = YES;
                                        self.btnRegisterOilChange.alpha = 1.0f;
                                    }
                                    else
                                    {
                                        UIAlertController *alert = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_GLOBAL_ERROR)
                                                                                                       message: LOCALIZATION(C_ERROR_CANNOTRECOGNIZE)
                                                                                                preferredStyle:UIAlertControllerStyleAlert];
                                        
                                        UIAlertAction *ok = [UIAlertAction actionWithTitle: LOCALIZATION(C_BUTTON_OK) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                             {
                                                                 //BUTTON OK CLICK EVENT
                                                             }];
                                        [alert addAction:ok];
                                        [self presentViewController:alert animated:YES completion:nil];
                                        //                                        self.imgView.image = nil;
                                        
                                        self.btnRegisterOilChange.enabled = NO;
                                        self.btnRegisterOilChange.alpha = 0.5f;
                                    }
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                }
                                if (error) {
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    
                                    UIAlertController *alert = [UIAlertController alertControllerWithTitle: LOCALIZATION(C_GLOBAL_ERROR)
                                                                                                   message: LOCALIZATION(C_ERROR_UNKNOWNERROR)
                                                                                            preferredStyle:UIAlertControllerStyleAlert];
                                    
                                    UIAlertAction *ok = [UIAlertAction actionWithTitle: LOCALIZATION(C_BUTTON_OK) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                         {
                                                             //BUTTON OK CLICK EVENT
                                                         }];
                                    [alert addAction:ok];
                                    [self presentViewController:alert animated:YES completion:nil];
                                    
                                    self.btnRegisterOilChange.enabled = NO;
                                    self.btnRegisterOilChange.alpha = 0.5f;
                                }
                            }];
}


-(NSString *) getCountryCode
{
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_THAILAND])
    {
        return @"th";
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDIA])
    {
        return @"in";
    }
    else if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_INDONESIA])
    {
        return @"id";
    }
    else if([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_SAUDI])
    {
        return @"sa";
    }
    else
    {
        return @"eu";
    }
}

- (UIImage*) rotateImage:(UIImage* )originalImage {
    
    UIImageOrientation orientation = originalImage.imageOrientation;
    
    UIGraphicsBeginImageContext(originalImage.size);
    
    [originalImage drawAtPoint:CGPointMake(0, 0)];
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orientation == UIImageOrientationRight) {
        CGContextRotateCTM (context, [self radians:90]);
    } else if (orientation == UIImageOrientationLeft) {
        CGContextRotateCTM (context, [self radians:90]);
    } else if (orientation == UIImageOrientationDown) {
        // NOTHING
    } else if (orientation == UIImageOrientationUp) {
        CGContextRotateCTM (context, [self radians:0]);
    }
    
    return UIGraphicsGetImageFromCurrentImageContext();
    
}

- (CGFloat) radians:(int)degree {
    return (degree/180)*(22/7);
}

- (UIImage *)imageWithImage:(UIImage *)image
{
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    CGSize newSize = image.size;
    
    CGFloat aspectRatio = newSize.width / newSize.height;
    
    if (newSize.width >= newSize.height)
    {
        //landscape or square, set width to 1280
        newSize.width = 1280;
        newSize.height = newSize.width * aspectRatio;
    }
    else
    {
        //portrait
        newSize.height = 1280;
        newSize.width = newSize.height * aspectRatio;
    }
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
