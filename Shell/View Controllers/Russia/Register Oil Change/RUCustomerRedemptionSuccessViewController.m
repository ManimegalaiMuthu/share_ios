//
//  RUCustomerRedemptionSuccessViewController.m
//  Shell
//
//  Created by Jeremy Lua on 17/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUCustomerRedemptionSuccessViewController.h"

@interface RUCustomerRedemptionSuccessViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *lblRedemptionCompletedHeader;

@property (weak, nonatomic) IBOutlet UILabel *lblConfirmationHeader;    //attributed label

@property (weak, nonatomic) IBOutlet UILabel *lblVehicleId;

@property (weak, nonatomic) IBOutlet UILabel *lblVehicleIdData;


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSArray *couponArray;
@end

@implementation RUCustomerRedemptionSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Helper setNavigationBarTitle: self title: LOCALIZATION(C_RU_REDEMPTION_SUCCESSTITLE) subtitle: @"" size: 15 subtitleSize: 0];      //lokalise 24 Jan
    
    self.lblRedemptionCompletedHeader.text = LOCALIZATION(C_RU_REDEMPTION_SUCCESSHEADER);       //lokalise 24 Jan
    
    NSMutableAttributedString *prefixString = [[NSMutableAttributedString alloc] initWithString: LOCALIZATION(C_RU_REDEMPTION_VERIFICATIONID)        attributes:@{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B2, }];           //lokalise 24 Jan
    
    NSString *redemptionVerificationString = [NSString stringWithFormat: @"\n%@", [self.successDict objectForKey: @"OrderReference"]];
    
    NSAttributedString *selectionString = [[NSAttributedString alloc] initWithString: redemptionVerificationString attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_H2, }]; //bold verification number
    
    [prefixString appendAttributedString: selectionString];
    self.lblConfirmationHeader.attributedText = prefixString;
    
    self.lblVehicleId.text = LOCALIZATION(C_PROMO_VEHICLEID);           //lokalise 24 jan
    
    self.lblVehicleIdData.text = [self.successDict objectForKey: @"VehicleNumber"];
    
    
    UIButton *btnBack = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30, 30)];
    [btnBack setImage: [UIImage imageNamed: @"icon-arrsingleL.png"] forState: UIControlStateNormal];
    [btnBack addTarget:self action: @selector(popSelf) forControlEvents: UIControlEventTouchUpInside];
    
    
    
    if(@available(iOS 11, *)){
        [btnBack.widthAnchor constraintEqualToConstant: btnBack.frame.size.width].active = YES;
        [btnBack.heightAnchor constraintEqualToConstant: btnBack.frame.size.height].active = YES;
    }
    
    UIBarButtonItem *backBarBtn = [[UIBarButtonItem alloc] initWithCustomView: btnBack];
    self.navigationItem.leftBarButtonItem = backBarBtn;
    
    if(![[self.successDict objectForKey: @"ProductCodes"] isKindOfClass:[NSNull class]]) {
        self.couponArray = [self.successDict objectForKey: @"ProductCodes"];
        self.tableView.hidden = NO;
    } else {
        self.tableView.hidden = YES;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_RU_REDEMPTION_SUCCESSTITLE) screenClass:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.couponArray count];
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"HeaderCell"];
    
    UILabel *lblProductUsed = [cell viewWithTag: 1];
    lblProductUsed.text = LOCALIZATION(C_SEARCHVEHICLE_PRODUCTUSED);            //lokalise 23 Jan
    
    UILabel *lblMileage = [cell viewWithTag: 2];
    lblMileage.text = LOCALIZATION(C_SEARCHVEHICLE_MILEAGE);                    //lokalise 23 Jan
    
    return cell.contentView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"ProductCell"];
        
        NSDictionary *cellData = [self.couponArray objectAtIndex: indexPath.row];
        
        UILabel *lblProductUsed = [cell viewWithTag: 1];
        lblProductUsed.text = [cellData objectForKey: @"Desc"];
        lblProductUsed.font = FONT_B2;
        
        UILabel *lblMileage = [cell viewWithTag: 2];
        //        lblMileage.text = [self.resultsDict objectForKey: @"Mileage"];
        lblMileage.text = [[self.successDict objectForKey: @"Mileage"] stringValue];
        lblMileage.font = FONT_B2;
    }
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
