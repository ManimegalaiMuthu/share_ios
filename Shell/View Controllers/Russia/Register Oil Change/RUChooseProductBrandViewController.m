//
//  RUChooseProductBrandViewController.m
//  Shell
//
//  Created by Jeremy Lua on 27/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUChooseProductBrandViewController.h"

@interface RUChooseProductBrandViewController () <WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;

@property (weak, nonatomic) IBOutlet UILabel *lblOr;
@property (weak, nonatomic) IBOutlet UILabel *lblProductBrand;

@property (weak, nonatomic) IBOutlet UITableView *brandsTableView;
@property NSMutableArray *brandsArray;
@property NSMutableArray *moreBrandsArray;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@property BOOL moreBrandsPressed;
@end

@implementation RUChooseProductBrandViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_RUSSIA_CHOOSEPRODUCT_TITLE) subtitle: @"" size: 15 subtitleSize:0];            //lokalise 24 Jan
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    
    self.lblOr.text = LOCALIZATION(C_CONSUMER_OR);                          //lokalise 24 Jan
    
    [self.btnSearch setTitle: LOCALIZATION(C_RUSSIA_CHOOSEPRODUCT_SEARCH) forState: UIControlStateNormal];          //lokalise 24 Jan
    
    self.lblProductBrand.text = LOCALIZATION(C_RUSSIA_CHOOSEPRODUCT_PLACEHOLDER);                                   //lokalise 24 Jan
    
    
    [self setupTable];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_RUSSIA_CHOOSEPRODUCT_TITLE) screenClass:nil];
}

-(void) setupTable
{
//    self.brandsArray = [[mSession lookupTable] objectForKey: @"ProductCategory"];
    
//    self.brandsTableView.tableFooterView = [UIView new];
    [[WebServiceManager sharedInstance] fetchRUProductCategory: self];
    
    [self.brandsTableView reloadData];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_FETCHPRODUCTCATEGORY:
            self.brandsArray = [[response getGenericResponse] objectForKey: @"ProductCategory"];
            self.moreBrandsArray = [[response getGenericResponse] objectForKey: @"MoreProductCategory"];
            
            [self.brandsTableView reloadData];
            break;
            
        default:
            break;
    }
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.moreBrandsPressed)
        return [self.brandsArray count] + [self.moreBrandsArray count];
    else
        return [self.brandsArray count] + 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (indexPath.row == 0) //Shell
    {
        NSDictionary *cellData = [self.brandsArray objectAtIndex: indexPath.row];
        cell = [tableView dequeueReusableCellWithIdentifier: @"ShellCell"];
        
//        UIImageView *shellImg = [cell viewWithTag: 1];
        
        UILabel *title = [cell viewWithTag: 2];
        title.text = [cellData objectForKey: @"CategoryName"];
    }
    else if (!self.moreBrandsPressed &&
             indexPath.row == [self.brandsArray count])
    {
        cell = [tableView dequeueReusableCellWithIdentifier: @"MoreBrandCell"];
        
        UILabel *title = [cell viewWithTag: 1];
        title.text = LOCALIZATION(C_RUSSIA_CHOOSEPRODUCT_MOREBRAND);                //lokalise 24 Jan
    }
    else
    {
        NSDictionary *cellData;
        if (indexPath.row < [self.brandsArray count])
            cellData = [self.brandsArray objectAtIndex: indexPath.row];
        else
            cellData = [self.moreBrandsArray objectAtIndex: indexPath.row - [self.brandsArray count]];
        
        cell = [tableView dequeueReusableCellWithIdentifier: @"BrandCell"];
        
        UILabel *title = [cell viewWithTag: 1];
        title.text = [cellData objectForKey: @"CategoryName"];
        
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.moreBrandsPressed && indexPath.row == [self.brandsArray count])
    {
        self.moreBrandsPressed = YES;
        [self.brandsTableView reloadData];
        return;
    }
    
    NSDictionary *cellData;
    if (indexPath.row < [self.brandsArray count])
        cellData = [self.brandsArray objectAtIndex: indexPath.row];
    else
        cellData = [self.moreBrandsArray objectAtIndex: indexPath.row - [self.brandsArray count]];
    
    [mSession pushRUChooseProductWithBrand: cellData vc: self];
}

- (IBAction)searchPressed:(id)sender {
    
    NSMutableArray *allItemsArray = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in self.brandsArray)
    {
        [allItemsArray addObjectsFromArray: [dict objectForKey: @"Item"]];
    }
    for (NSDictionary *dict in self.moreBrandsArray)
    {
        [allItemsArray addObjectsFromArray: [dict objectForKey: @"Item"]];
    }
    
    [mSession pushRUSearchProductWithAllItems:allItemsArray vc: self];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
