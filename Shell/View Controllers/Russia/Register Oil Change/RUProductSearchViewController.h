//
//  RUProductSearchViewController.h
//  Shell
//
//  Created by Jeremy Lua on 30/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUChooseProductBrandViewController.h"

#import "BaseVC.h"

@interface RUProductSearchViewController : BaseVC
@property id<RUChooseProductBrandDelegate> delegate;

@property NSMutableArray *allItemsArray;
@end
