//
//  RUCustomerRedemptionViewController.m
//  Shell
//
//  Created by Jeremy Lua on 17/9/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUCustomerRedemptionViewController.h"

@interface RUCustomerRedemptionViewController () <UITextFieldDelegate, WebServiceManagerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UITextField *txtPromoCode;

@property (weak, nonatomic) IBOutlet UILabel *lblVehicleId;
@property (weak, nonatomic) IBOutlet UILabel *lblOfferDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblQty;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *qtyTopSpace;
@property (weak, nonatomic) IBOutlet UILabel *lblValidity;

@property (weak, nonatomic) IBOutlet UIButton *btnScan;

@property (weak, nonatomic) IBOutlet UIView *offerDetailsView;
@property (weak, nonatomic) IBOutlet UIView *scanCodeView;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;

@property NSMutableArray *redemptionArray;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@property BOOL isPromoCodeQR;
@property NSDictionary *promoOfferDict;

@property (weak, nonatomic) IBOutlet UIView *enterMileageView;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmitOilChange;
@property (weak, nonatomic) IBOutlet UILabel *lblSubmitOilChange;

@property (weak, nonatomic) IBOutlet UITextField *txtEnterMileage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *enterMileageViewHeight;


#define CELL_HEIGHT 50
#define SUBMITOILCHANGE_HEIGHT 85

//@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblInviteConsumer;
@end

@implementation RUCustomerRedemptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_SIDEMENU_SCANCUSTCODE) subtitle: @""];         //lokalise 24 Jan
    
    self.lblHeader.text = LOCALIZATION(C_RU_REDEMPTION_HEADER);                                             //lokalise 24 Jan
    self.txtPromoCode.placeholder = LOCALIZATION(C_RU_REDEMPTION_PLACEHOLDER);                              //lokalise 24 Jan
    
    self.lblSubmitOilChange.text = LOCALIZATION(C_RU_REDEMPTION_CHECKBOXTEXT);                              //lokalise 24 Jan
    self.txtEnterMileage.placeholder = LOCALIZATION(C_RUSSIA_CURRENTMILEAGE_PLACEHOLDER);                   //lokalise 24 Jan
    
    [self.btnScan setTitle: LOCALIZATION(C_RUSSIA_ADDPRODUCTUSED) forState: UIControlStateNormal];          //lokalise 24 Jan
    
    [self.btnSubmitOilChange setTitle: LOCALIZATION(C_SCANPRODUCTCODE_SUBMIT) forState: UIControlStateNormal];//lokalise 23 Jan
    
    self.redemptionArray = [[NSMutableArray alloc] init];
    self.tableView.tableFooterView = [UIView new];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_SIDEMENU_SCANCUSTCODE) screenClass:nil];
}

-(void) validateSubmitOilChange
{
//    if ([self.txtEnterMileage.text length] > 0 && [self.redemptionArray count] > 0)
    if ([self.redemptionArray count] > 0)
    {
        self.btnSubmit.enabled = YES;
        self.btnSubmit.alpha = 1.0f;
    }
    else
    {
        self.btnSubmit.enabled = NO;
        self.btnSubmit.alpha = 0.5f;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [[WebServiceManager sharedInstance] getPromoDetails: self.txtPromoCode.text vc: self];
}

- (IBAction)addProductCodePressed:(id)sender
{
    [mSession pushRUChooseProductBrand: self];
}

-(void) didChooseProduct:(NSDictionary *) productDictionary;
{
    //    [self.resultArray addObject: productDictionary];
    
    //rename key ProductName to productName
    NSMutableDictionary *newProductDictionary = [[NSMutableDictionary alloc] initWithDictionary: productDictionary];
    [newProductDictionary setObject: [productDictionary objectForKey: @"ProductName"] forKey: @"productName"];
    [newProductDictionary removeObjectForKey: @"ProductName"];
    
    [newProductDictionary setObject: [productDictionary objectForKey: @"ProductSKU"] forKey: @"SKU"];
    [newProductDictionary removeObjectForKey: @"ProductSKU"];
    
    [newProductDictionary removeObjectForKey: @"ProductImage"];
    [self.redemptionArray addObject: newProductDictionary];
    
    
    [self validateSubmitOilChange];
    [self.tableView reloadData];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.redemptionArray count];
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"HeaderCell"];
    
    UILabel *lblNo = [cell viewWithTag: 1];
    lblNo.text = LOCALIZATION(C_TABLE_NO);                                          //lokalise 24 Jan
    
    UILabel *lblProductUsed = [cell viewWithTag: 2];
    lblProductUsed.text = LOCALIZATION(C_SEARCHVEHICLE_PRODUCTUSED);                //lokalise 23 Jan
    
    UILabel *lblDelete = [cell viewWithTag: 3];
    lblDelete.text = LOCALIZATION(C_DELETE);                                        //lokalise 24 Jan
    
    
    
    return cell.contentView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"ProductCell"];
    
    NSDictionary *cellData = [self.redemptionArray objectAtIndex: indexPath.row];
    
    UILabel *lblNo = [cell viewWithTag: 1];
    lblNo.text = @(indexPath.row + 1).stringValue;
    
    UILabel *lblProductUsed = [cell viewWithTag: 2];
    //    lblProductUsed.text = [cellData objectForKey: @"ProductName"];
    lblProductUsed.text = [cellData objectForKey: @"productName"];
    return cell;
}

- (IBAction)deletePressed:(UIButton *)sender
{
    UITableViewCell *cell = sender.superview.superview;
    
    NSIndexPath *cellIndexPath = [self.tableView indexPathForCell: cell];
    
    [self.redemptionArray removeObjectAtIndex: cellIndexPath.row];
    
    [self validateSubmitOilChange];
    
    [self.tableView reloadData];
}

- (IBAction)submitPressed:(id)sender
{

    NSDictionary *paramDict = @{@"PromoOfferID": [self.promoOfferDict objectForKey: @"PromoOfferID"],
                                @"PromoCode": [self.promoOfferDict objectForKey: @"PromoCode"],
                                @"Latitude"      : @(0),
                                @"Longitude"     : @(0),
                                @"Codes": self.redemptionArray,
                                @"AddOilChange":    @(self.btnSubmitOilChange.selected),
                                @"Mileage":         @(self.txtEnterMileage.text.intValue),
                                };
    [[WebServiceManager sharedInstance] submitPromoCode: paramDict vc: self];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_PROMOSUBMITCODE:
        {
            if (![[response getGenericResponse] isKindOfClass: [NSNull class]])
            {
//                [mSession pushPromoCodeResultView: [response getGenericResponse] vc:self];
                [mSession pushRUCustomerRedemptionSuccess: [response getGenericResponse] vc: self];
                self.txtPromoCode.text = @"";
                
                self.scanCodeView.hidden = YES;
                self.offerDetailsView.hidden = YES;
                self.btnSubmit.hidden = YES;
                
                self.promoOfferDict = nil;
                [self.redemptionArray removeAllObjects];
                [self.tableView reloadData];
            }
            else {
                [mAlert showSuccessAlertWithMessage: [[[response getJSON] objectForKey:@"ResponseMessage"] objectForKey:@"Message"] onCompletion:^(BOOL finished) {
                    //clear fields?
                    self.txtPromoCode.text = @"";
                    
                    self.scanCodeView.hidden = YES;
                    self.offerDetailsView.hidden = YES;
                    self.btnSubmit.hidden = YES;
                    
                    self.promoOfferDict = nil;
                    [self.redemptionArray removeAllObjects];
                    [self.tableView reloadData];
                }];
            }
        }
            break;
        case kWEBSERVICE_PROMOGETDETAILS:
        {
            self.promoOfferDict = [response getGenericResponse];
            
            self.lblVehicleId.attributedText = [self attributedStringForLabel: LOCALIZATION(C_PROMO_VEHICLEID) content:[self.promoOfferDict objectForKey: @"VehicleNumber"]];  //lokalise 24 jan
            
            self.lblOfferDetails.attributedText = [self attributedStringForLabel:LOCALIZATION(C_PROMO_REDEMPTIONITEM) content:[self.promoOfferDict objectForKey: @"OfferDetails"]];  //lokalise 24 jan
            
            if ([[self.promoOfferDict objectForKey: @"ShowQTY"] boolValue])
            {
                self.lblQty.attributedText = [self attributedStringForLabel: LOCALIZATION(C_PROMO_DETAIL_QTY) content: [self.promoOfferDict objectForKey: @"NumberOfScans"]];  //lokalise 24 jan
                
                self.qtyTopSpace.constant = 10;
            }
            else
            {
                self.qtyTopSpace.constant = 0;
                self.lblQty.text = @"";
            }

            self.lblValidity.attributedText = [self attributedStringForLabel:LOCALIZATION(C_PROMO_VALIDITY) content:[self.promoOfferDict objectForKey: @"Validity"]];  //lokalise 24 Jan
            
            self.txtEnterMileage.text = @"";
            if ([[self.promoOfferDict objectForKey: @"EnableOilChange"] boolValue])
            {
                self.enterMileageViewHeight.constant = SUBMITOILCHANGE_HEIGHT; //default checked
                self.enterMileageView.hidden = NO;
                self.txtEnterMileage.hidden = NO;
                self.btnSubmitOilChange.selected = YES;
                
                if ([[self.promoOfferDict objectForKey: @"ForceOilChange"] boolValue])
                    self.btnSubmitOilChange.userInteractionEnabled = NO;
                else
                    self.btnSubmitOilChange.userInteractionEnabled = YES;
            }
            else
            {
                self.btnSubmitOilChange.selected = NO;
                self.enterMileageViewHeight.constant = 0;
                self.enterMileageView.hidden = YES;
            }
            
            
            if ([[self.promoOfferDict objectForKey: @"AllowQRScan"] boolValue])
                self.scanCodeView.hidden = NO; //show if allow QR scan
            else
                self.scanCodeView.hidden = YES;
            
            self.offerDetailsView.hidden = NO;
            
            //v2 here
            //check for v1 also
            if(GET_PROFILETYPE == kPROFILETYPE_DSR)
                self.btnSubmit.hidden = YES;
            else
                self.btnSubmit.hidden = NO;
            }
            break;
        default:
            break;
    }
}

-(NSAttributedString *) attributedStringForLabel: (NSString *)titleString content:(NSString *) contentString
{
    NSMutableAttributedString *prefixString = [[NSMutableAttributedString alloc] initWithString: titleString attributes:@{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_H2, }];
    
    NSMutableAttributedString *colonString = [[NSMutableAttributedString alloc] initWithString: @": " attributes:@{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_H2, }];
    
    NSAttributedString *selectionString = [[NSAttributedString alloc] initWithString: contentString attributes: @{NSForegroundColorAttributeName: COLOUR_VERYDARKGREY,                                                                                                                                              NSFontAttributeName: FONT_B2, }];
    
    [prefixString appendAttributedString: colonString];
    [prefixString appendAttributedString: selectionString];
    
    return prefixString;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
