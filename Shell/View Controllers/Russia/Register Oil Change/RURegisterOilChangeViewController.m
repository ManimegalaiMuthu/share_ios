//
//  RURegisterOilChangeViewController.m
//  Shell
//
//  Created by Jeremy Lua on 27/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RURegisterOilChangeViewController.h"

@interface RURegisterOilChangeViewController () <WebServiceManagerDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITextField *txtVehicleNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtMileage;
@property (weak, nonatomic) IBOutlet UILabel *lblMileageSuffix;

@property (weak, nonatomic) IBOutlet UITableView *resultTableView;
@property NSMutableArray *resultArray;

@property (weak, nonatomic) IBOutlet UIButton *btnAddProduct;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmitOilChange;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@end

@implementation RURegisterOilChangeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_RUSSIA_REGISTEROILCHANGE) subtitle: @""];          //lokalise 24 Jan
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    
    self.txtVehicleNumber.text = self.vehicleNumberString;
    
    self.txtMileage.placeholder = LOCALIZATION(C_RUSSIA_CURRENTMILEAGE_PLACEHOLDER);                            //lokalise 24 Jan
    
    [self.btnAddProduct setTitle: LOCALIZATION(C_RUSSIA_ADDPRODUCTUSED) forState: UIControlStateNormal];        //lokalise 24 Jan
    
    self.btnSubmitOilChange.enabled = NO;
    self.btnSubmitOilChange.alpha = 0.5f;
    [self.btnSubmitOilChange setTitle: LOCALIZATION(C_SCANPRODUCTCODE_SUBMIT) forState: UIControlStateNormal];  //lokalise 23 Jan
    
    if (GET_PROFILETYPE == kPROFILETYPE_DSR)
        self.btnSubmitOilChange.hidden = YES;
    
    self.resultTableView.tableFooterView = [UIView new];
    self.resultArray = [[NSMutableArray alloc] init];
    
    self.lblMileageSuffix.text = LOCALIZATION(C_RU_MILEAGE_SUFFIX);                                             //lokalise 24 Jan
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_RUSSIA_REGISTEROILCHANGE) screenClass:nil];
}

- (IBAction)backPressed:(id)sender
{
    [self popSelf];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addProductPressed:(id)sender
{
    [mSession pushRUChooseProductBrand: self];
}

-(void) didChooseProduct:(NSDictionary *) productDictionary;
{
//    [self.resultArray addObject: productDictionary];
    
    //rename key ProductName to productName
    NSMutableDictionary *newProductDictionary = [[NSMutableDictionary alloc] initWithDictionary: productDictionary];
    [newProductDictionary setObject: [productDictionary objectForKey: @"ProductName"] forKey: @"productName"];
    [newProductDictionary removeObjectForKey: @"ProductName"];
    
    [newProductDictionary setObject: [productDictionary objectForKey: @"ProductSKU"] forKey: @"SKU"];
    [newProductDictionary removeObjectForKey: @"ProductSKU"];
    
    [newProductDictionary removeObjectForKey: @"ProductImage"];
    [self.resultArray addObject: newProductDictionary];
    
    
    [self validateSubmitOilChange];
    [self.resultTableView reloadData];
}

- (IBAction)submitPressed:(id)sender
{
    NSDictionary *submitParams = @{@"VehicleList": @[@{ @"VehicleNumber": self.txtVehicleNumber.text,
                                                        @"Mileage": @(self.txtMileage.text.integerValue),
                                                        @"Coupons":
                                                            self.resultArray
                                                       
                                                        },
                                                     ]
                                   };
    
    [[WebServiceManager sharedInstance] submitCode: submitParams vc: self];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.resultArray count];
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"HeaderCell"];
    
    UILabel *lblNo = [cell viewWithTag: 1];
    lblNo.text = LOCALIZATION(C_TABLE_NO);                                          //lokalise 24 Jan
    
    UILabel *lblProductUsed = [cell viewWithTag: 2];
    lblProductUsed.text = LOCALIZATION(C_SEARCHVEHICLE_PRODUCTUSED);                //loikalise 23 Jan
    
    UILabel *lblDelete = [cell viewWithTag: 3];
    lblDelete.text = LOCALIZATION(C_DELETE);                                        //lokalise 24 Jan
    
    
    
    return cell.contentView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"ProductCell"];
    
    NSDictionary *cellData = [self.resultArray objectAtIndex: indexPath.row];
    
    UILabel *lblNo = [cell viewWithTag: 1];
    lblNo.text = @(indexPath.row + 1).stringValue;
    
    UILabel *lblProductUsed = [cell viewWithTag: 2];
//    lblProductUsed.text = [cellData objectForKey: @"ProductName"];
    lblProductUsed.text = [cellData objectForKey: @"productName"];
    return cell;
}

- (IBAction)deletePressed:(UIButton *)sender
{
    UITableViewCell *cell = sender.superview.superview;
    
    NSIndexPath *cellIndexPath = [self.resultTableView indexPathForCell: cell];
    
    [self.resultArray removeObjectAtIndex: cellIndexPath.row];
    
    [self validateSubmitOilChange];
    
    [self.resultTableView reloadData];
}

-(void) validateSubmitOilChange
{
    if ([self.txtMileage.text length] > 0 && [self.resultArray count] > 0)
    {
        self.btnSubmitOilChange.enabled = YES;
        self.btnSubmitOilChange.alpha = 1.0f;
    }
    else
    {
        self.btnSubmitOilChange.enabled = NO;
        self.btnSubmitOilChange.alpha = 0.5f;
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self validateSubmitOilChange];
}

//-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//
//    return YES;
//}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall) {
        case kWEBSERVICE_SUBMITCODE:
        {
            NSMutableDictionary *respData = [[NSMutableDictionary alloc] initWithDictionary:[response getGenericResponse]];
            [respData setObject: self.vehicleNumberString forKey: @"VehicleNumber"];
            [respData setObject: self.txtMileage.text forKey: @"Mileage"];
            [mSession pushRURegisterOilChangeResults: respData vc: self];
            self.txtMileage.text = @"";
            [self.resultArray removeAllObjects];
            [self.resultTableView reloadData];
            [self validateSubmitOilChange];
        }
            break;
            
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
