//
//  RUChooseProductViewController.h
//  Shell
//
//  Created by Jeremy Lua on 27/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUChooseProductBrandViewController.h"
#import "BaseVC.h"

@interface RUChooseProductViewController : BaseVC
@property id<RUChooseProductBrandDelegate> delegate;

@property NSDictionary *brandDict;
@end
