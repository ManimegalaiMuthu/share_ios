//
//  RUProductSearchViewController.m
//  Shell
//
//  Created by Jeremy Lua on 30/8/18.
//  Copyright © 2018 Edenred. All rights reserved.
//

#import "RUProductSearchViewController.h"

@interface RUProductSearchViewController () <WebServiceManagerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;

@property (weak, nonatomic) IBOutlet UITableView *itemsTableView;
@property NSMutableArray *itemsArray;

@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@end

@implementation RUProductSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_RUSSIA_CHOOSEPRODUCT_TITLE) subtitle: @""];            //lokalise 24 Jan
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnBack];
    
//    self.allItemsArray = [[NSMutableArray alloc] init];
//    
//    for (NSDictionary *dict in [[mSession lookupTable] objectForKey: @"ProductCategory"])
//    {
//        [self.allItemsArray addObjectsFromArray: [dict objectForKey: @"Item"]];
//    }
    
//    self.itemsArray = [NSMutableArray arrayWithArray: self.allItemsArray];
    
    [self.txtSearch becomeFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_RUSSIA_CHOOSEPRODUCT_TITLE) screenClass:nil];
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.itemsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    NSDictionary *cellData = [self.itemsArray objectAtIndex: indexPath.row];
    
    cell = [tableView dequeueReusableCellWithIdentifier: @"ItemCell"];
    UIImageView *imgView = [cell viewWithTag: 1];
    [imgView sd_setImageWithURL: [NSURL URLWithString: [cellData objectForKey: @"ProductImage"]] placeholderImage: [UIImage imageNamed: @"bottle"] options: (SDWebImageRefreshCached | SDWebImageRetryFailed)];
    
    UILabel *title = [cell viewWithTag: 2];
    title.text = [cellData objectForKey: @"ProductName"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cellData = [self.itemsArray objectAtIndex: indexPath.row];
    
    [self.delegate didChooseProduct: cellData];
    
    NSUInteger delegateIndex = [self.navigationController.childViewControllers indexOfObject: self.delegate];
    [self popToIndex: delegateIndex];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.txtSearch)
    {
        NSRange textFieldRange = NSMakeRange(0, [textField.text length]);
        if (NSEqualRanges(range, textFieldRange) && [string length] == 0)
        {
//            self.itemsArray = [NSMutableArray arrayWithArray: self.allItemsArray];
            [self.itemsArray removeAllObjects];
        }
        else
        {
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat: @"ProductName CONTAINS[cd] %@", [NSString stringWithFormat: @"%@%@", self.txtSearch.text, string]];
            
            self.itemsArray = [NSMutableArray arrayWithArray:[self.allItemsArray filteredArrayUsingPredicate:predicate]];
            
        }
        [self.itemsTableView reloadData];
    }
    return YES;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
