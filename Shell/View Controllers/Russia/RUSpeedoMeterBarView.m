//
//  RUSpeedoMeterBarView.m
//  Shell
//
//  Created by MUTHUPANDIYAN Manimegalai on 24/9/20.
//  Copyright © 2020 Edenred. All rights reserved.
//

#import "Session.h"
#import "RUSpeedoMeterBarView.h"
#import "LocalizationManager.h"

@interface RUSpeedoMeterBarView ()

@property (weak, nonatomic) IBOutlet UIView *progressBarView;
@property (strong, nonatomic) IBOutlet UILabel *lblPercentage;


@property (weak, nonatomic) IBOutlet UIImageView *imgCurrentArrow;


//
//@property (strong, nonatomic) IBOutlet UILabel *lblZero;
//@property (strong, nonatomic) IBOutlet UILabel *lblMax;
@property (weak, nonatomic) IBOutlet UIImageView *imgRemaining;
@property (weak, nonatomic) IBOutlet UILabel *lblRemaining; //Max - Current

@property (strong, nonatomic) IBOutlet UILabel *lblMilestoneOne;
@property (strong, nonatomic) IBOutlet UILabel *lblMilestoneTwo;
@property (strong, nonatomic) IBOutlet UILabel *lblMilestoneThree;
@property (strong, nonatomic) IBOutlet UILabel *lblMilestoneFour;
@property (strong, nonatomic) IBOutlet UILabel *lblMilestoneFive;

@end

@implementation RUSpeedoMeterBarView

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self layoutIfNeeded];
    
    
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        
        //https://stackoverflow.com/questions/30335089/reuse-a-uiview-xib-in-storyboard/37668821#37668821
        // 1. load the interface
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        // 2. add as subview
        [self addSubview:self.contentView];
        
     //   self.contentView.backgroundColor = [UIColor greenColor];
        
        // 3. allow for autolayout
        self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
        // 4. add constraints to span entire view
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[view]-|" options:0 metrics:nil views:@{@"view":self.contentView}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[view]-|" options:0 metrics:nil views:@{@"view":self.contentView}]];
    
        [self setNeedsLayout];
    }
    return self;
}


#define degreesToRadians(degrees)((M_PI * degrees)/180)
-(void) loadBar: (NSDictionary *) responseData
{
    
      NSDictionary* inventorySpeddometer = [responseData objectForKey: @"Data"];
    self.speedoMeterText = [inventorySpeddometer objectForKey: @"InventorySpeedoMeterText"];
    self.percentage = [[inventorySpeddometer objectForKey: @"InventorySpeedometerGraphpercentage"] intValue];
    self.milestoneOne = [inventorySpeddometer objectForKey: @"InventorySpeedometerMileStone1"];
    self.milestoneTwo = [inventorySpeddometer objectForKey: @"InventorySpeedometerMileStone2"];
    self.milestoneThree = [inventorySpeddometer objectForKey: @"InventorySpeedometerMileStone3"];
    self.milestoneFour = [inventorySpeddometer objectForKey: @"InventorySpeedometerMileStone4"];
    self.milestoneFive = [inventorySpeddometer objectForKey: @"InventorySpeedometerMileStone5"];
    
    //clear previously drawn circle
    NSMutableArray *layerToRemoveArray = [[NSMutableArray alloc] init]; //to hold references to actual progress bar
    
    for (CALayer *layer in [self.progressBarView.layer sublayers])
    {
        if ([layer isKindOfClass: [CAShapeLayer class]])
        {
            [layerToRemoveArray addObject: layer];
        }
    }
    
    //remove referenced layers from superlayer
    for (CALayer *layer in layerToRemoveArray)
    {
        [layer removeFromSuperlayer];
    }

    self.imgCurrentArrow.hidden = YES;
    
    self.lblRemaining.text = [NSString stringWithFormat:@"%@", self.speedoMeterText];
    self.lblRemaining.font = FONT_B(10);
    [self.lblRemaining sizeToFit];
    
    self.lblMilestoneOne.text = [NSString stringWithFormat:@"%@", self.milestoneOne];
    self.lblMilestoneOne.font = FONT_B(10);
    [self.lblMilestoneOne sizeToFit];
    
    self.lblMilestoneTwo.text = [NSString stringWithFormat:@"%@", self.milestoneTwo];
      self.lblMilestoneTwo.font = FONT_B(10);
      [self.lblMilestoneTwo sizeToFit];
    
    self.lblMilestoneThree.text = [NSString stringWithFormat:@"%@", self.milestoneThree];
      self.lblMilestoneThree.font = FONT_B(10);
      [self.lblMilestoneThree sizeToFit];
    
      self.lblMilestoneFour.text = [NSString stringWithFormat:@"%@", self.milestoneFour];
      self.lblMilestoneFour.font = FONT_B(10);
      [self.lblMilestoneFour sizeToFit];
    
    self.lblMilestoneFive.text = [NSString stringWithFormat:@"%@", self.milestoneFive];
      self.lblMilestoneFive.font = FONT_B(10);
      [self.lblMilestoneFive sizeToFit];
    
     CGFloat  m1Angle = 180;
     CGFloat m2Angle = 225;
     CGFloat m3Angle = 270;
     CGFloat m4Angle = 315;
     CGFloat m5Angle = 360;
     CGFloat totalPercentage = m1Angle + self.percentage;
    //zero
    if (self.lblMilestoneOne.superview != self.progressBarView)
        [self.progressBarView addSubview: self.lblMilestoneOne];
    
    
    self.lblMilestoneOne.translatesAutoresizingMaskIntoConstraints = YES;
    self.lblMilestoneOne.center = [self drawPointsBarWithStartAngle: m1Angle endAngle: m1Angle colour: COLOUR_PALEGREY hasMarking: YES layerName:@""];
    
    [self calculateCenterFromCircle: self.lblMilestoneOne];
    
    if (self.lblMilestoneTwo.superview != self.progressBarView)
    {
        [self.progressBarView addSubview: self.lblMilestoneTwo];
    }
   
    if (self.lblMilestoneThree.superview != self.progressBarView)
    {
        [self.progressBarView addSubview: self.lblMilestoneThree];
    }
   
    if (self.lblMilestoneFour.superview != self.progressBarView)
    {
        [self.progressBarView addSubview: self.lblMilestoneFour];
    }
  
    if (self.lblMilestoneFive.superview != self.progressBarView)
    {
        [self.progressBarView addSubview: self.lblMilestoneFive];
    }
    if (self.lblPercentage.superview != self.progressBarView)
    {
        [self.progressBarView addSubview: self.lblPercentage];
    }
      self.lblMilestoneTwo.translatesAutoresizingMaskIntoConstraints = YES;
      self.lblMilestoneThree.translatesAutoresizingMaskIntoConstraints = YES;
      self.lblMilestoneFour.translatesAutoresizingMaskIntoConstraints = YES;
      self.lblMilestoneFive.translatesAutoresizingMaskIntoConstraints = YES;
      self.lblPercentage.translatesAutoresizingMaskIntoConstraints = YES;

        self.lblMilestoneTwo.center = [self drawPointsBarWithStartAngle: m1Angle endAngle:m2Angle colour: COLOUR_PALEGREY hasMarking: YES layerName: @""];
        [self calculateCenterFromCircle: self.lblMilestoneTwo];

        self.lblMilestoneThree.center = [self drawPointsBarWithStartAngle: m1Angle endAngle: m3Angle colour: COLOUR_PALEGREY hasMarking: YES layerName: @""];
        [self calculateCenterFromCircle: self.lblMilestoneThree];

        self.lblMilestoneFour.center = [self drawPointsBarWithStartAngle: m1Angle endAngle:m4Angle colour: COLOUR_PALEGREY hasMarking: YES layerName: @""];
        [self calculateCenterFromCircle: self.lblMilestoneFour];

        self.lblMilestoneFive.center = [self drawPointsBarWithStartAngle: m1Angle endAngle:m5Angle colour: COLOUR_PALEGREY hasMarking: YES layerName: @""];
           [self calculateCenterFromCircle: self.lblMilestoneFive];
        
       self.lblPercentage.center = [self drawPointsBarWithStartAngle: m1Angle endAngle:totalPercentage colour: COLOUR_YELLOW hasMarking: NO layerName: @""];
              [self calculateCenterFromCircle: self.lblPercentage];

      [self.imgRemaining setImage: [UIImage imageNamed: @"icn_scanplate_vdarkgrey.png"]];
    if(SCREEN_WIDTH <= 320) {
        [NSLayoutConstraint constraintWithItem:self.lblRemaining attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.progressBarView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:-10].active = YES;
        self.lblRemaining.translatesAutoresizingMaskIntoConstraints =  NO;
        self.imgRemaining.translatesAutoresizingMaskIntoConstraints = NO;
    }
    [self setNeedsLayout];
}
-(void) addIconToLabel:(UILabel *) label
{
    UIImageView *imgView = [[UIImageView alloc] initWithFrame: CGRectMake(-20, 0, 20, 20)];
    [imgView setImage: [UIImage imageNamed: @"carousel_partnersofshell_giftbox_verydarkgrey.png"]];
    
    [label addSubview: imgView];
    imgView.center = CGPointMake(imgView.center.x, label.frame.size.height / 2);
    
    //if icon on right side of bar, offset to right to fit icon
    if (label.center.x > self.progressBarView.frame.size.width / 2)
    {
        label.center = CGPointMake(label.center.x + 20, label.center.y);
    }
}

-(void) calculateCenterFromCircle:(UIView *) view
{
    //check which direction of progress point of label should be at.
    if (view.center.x < self.progressBarView.frame.size.width / 2)
    {
        //bottomright of rect
        view.center = CGPointMake(view.center.x - (view.frame.size.width / 3), view.center.y - (view.frame.size.height / 2));
    }
    else
    {
        //bottom left of rect
        view.center = CGPointMake(view.center.x + (view.frame.size.width / 3), view.center.y - (view.frame.size.height / 2));
    }
    
    
}

#define BARWIDTH 15
#define MARKINGWIDTH (BARWIDTH + 10)
-(CGPoint) drawPointsBarWithStartAngle:(CGFloat)startAngle
                              endAngle: (CGFloat)endAngle
                                colour:(UIColor *)colour
                            hasMarking:(BOOL) hasMarking
                             layerName:(NSString *)layerName
{
    CGPoint origin = CGPointMake((SCREEN_WIDTH / 2.1), self.progressBarView.frame.size.height);

    NSLog(@"SCREEN_WIDTH ___ %f",SCREEN_WIDTH);
    CGFloat baseRadius = self.progressBarView.frame.size.height - 25;
    if(SCREEN_WIDTH <= 450) {
        baseRadius = 91;
        origin = CGPointMake((SCREEN_WIDTH / 2.1), self.progressBarView.frame.size.height - 15);
    }
        
    
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    [bezierPath addArcWithCenter: origin
                          radius: baseRadius
                      startAngle: degreesToRadians(startAngle)
                        endAngle: degreesToRadians(endAngle)
                       clockwise: YES];
    
    CAShapeLayer *progressLayer = [[CAShapeLayer alloc] init];
    progressLayer.name = layerName;
    [progressLayer setPath:bezierPath.CGPath];
    [progressLayer setStrokeColor: [colour CGColor]];
    [progressLayer setFillColor:[UIColor clearColor].CGColor];
    [progressLayer setLineWidth:BARWIDTH];
    [self.progressBarView.layer addSublayer:progressLayer];
    
    
    if (hasMarking)
    {
        //marking
        UIBezierPath *markingPath = [UIBezierPath bezierPath];
        [markingPath addArcWithCenter: origin
                              radius: baseRadius + ((MARKINGWIDTH - BARWIDTH) / 2) //stretching out of red
                          startAngle: degreesToRadians(endAngle - 1)
                            endAngle: degreesToRadians(endAngle)
                           clockwise: YES];
        
        CAShapeLayer *markingLayer = [[CAShapeLayer alloc] init];
        [markingLayer setPath:markingPath.CGPath];
        [markingLayer setStrokeColor: [COLOUR_VERYDARKGREY CGColor]];
        [markingLayer setFillColor:[UIColor clearColor].CGColor];
        [markingLayer setLineWidth:MARKINGWIDTH];
        [self.progressBarView.layer addSublayer:markingLayer];
    }
    else
    {
        //get center point for arrow
        self.imgCurrentArrow.hidden = NO;
        
        UIBezierPath *arrowPointPath = [UIBezierPath bezierPath];
        [arrowPointPath addArcWithCenter: origin
                                   radius: baseRadius + (MARKINGWIDTH / 2) + 2
                               startAngle: degreesToRadians(endAngle)
                                 endAngle: degreesToRadians(endAngle)
                                clockwise: YES];
        
        self.imgCurrentArrow.center = [arrowPointPath currentPoint];
        
        //end angle = arrow angle
        //-90 = -180
        //0 = -90
        //90 = 0
        CGFloat arrowAngle = endAngle + 90;
        
        
        [self.imgCurrentArrow setImage: [UIImage imageNamed: @"carousel_triangle_red.png"]];
        self.imgCurrentArrow.transform = CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(arrowAngle));
//            imgArrow.transform = CGAffineTransformRotate(CGAffineTransformIdentity, degreesToRadians(endAngle));
    }
    
    //get outer point
    UIBezierPath *outerRegionPath = [UIBezierPath bezierPath];
    [outerRegionPath addArcWithCenter: origin
                               radius: baseRadius + MARKINGWIDTH
                           startAngle: degreesToRadians(startAngle )
                             endAngle: degreesToRadians(endAngle )
                            clockwise: YES];
    
    return [outerRegionPath currentPoint];
}

-(NSString *) addCommaThousandSeparatorsWithInputString: (CGFloat) inputValue
{
    
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setNumberStyle:NSNumberFormatterDecimalStyle]; // to get commas (or locale equivalent)
    [fmt setGroupingSeparator:@","];
    return [fmt stringFromNumber:@(inputValue)];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
