//
//  PlaceholderView.m
//  JTIStarPartners
//
//  Created by Shekhar  on 8/4/15.
//  Copyright (c) 2015 Edenred. All rights reserved.
//

#import "PlaceholderView.h"
#import "Constants.h"
#import "LocalizationManager.h"
@interface PlaceholderView()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpace;

@end


@implementation PlaceholderView

- (id)initWithView:(UIView *)view
{
    self = [super initWithFrame:view.frame];
    if (self)
    {
        self = [[[NSBundle mainBundle] loadNibNamed:@"PlaceholderView" owner:self options:nil] objectAtIndex:0];
        self.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
        
        self.lblPlaceholderTitle.font = FONT_B1;
        self.lblPlaceholderTitle.text = LOCALIZATION(C_TABLE_NODATAFOUND);      //lokalised
    }
    return self;
}
- (id)initWithView:(UIView *)view withOffset: (CGFloat) offset
{
    
    self = [super initWithFrame:view.frame];
    if (self)
    {
        self = [[[NSBundle mainBundle] loadNibNamed:@"PlaceholderView" owner:self options:nil] objectAtIndex:0];
        self.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
        
        self.topSpace.constant = offset + 15;
        
        self.lblPlaceholderTitle.font = FONT_B1;
        self.lblPlaceholderTitle.text = LOCALIZATION(C_TABLE_NODATAFOUND);      //lokalised
    }
    return self;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
}

@end
