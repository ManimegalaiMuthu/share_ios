//
//  PlaceholderView.h
//  JTIStarPartners
//
//  Created by Shekhar  on 8/4/15.
//  Copyright (c) 2015 Edenred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaceholderView : UIView

@property (strong, nonatomic) IBOutlet UILabel *lblPlaceholderTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblPlaceholderSubtitle;
@property (assign, nonatomic) BOOL fullView;

- (id)initWithView:(UIView *)view;
- (id)initWithView:(UIView *)view withOffset: (CGFloat) offset;
@end
