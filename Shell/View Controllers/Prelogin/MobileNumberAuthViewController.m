//
//  MobileNumberAuthViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 26/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "MobileNumberAuthViewController.h"


@interface MobileNumberAuthViewController () <WebServiceManagerDelegate, CommonListDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *btnCountryCode;
@property (weak, nonatomic) IBOutlet UITextField *txtMobNum;

@property (weak, nonatomic) IBOutlet UIButton *btnWorkshop;
@property (weak, nonatomic) IBOutlet UILabel *lblWorkshop;
@property (weak, nonatomic) IBOutlet UIButton *btnMechanic;
@property (weak, nonatomic) IBOutlet UILabel *lblMechanic;

@end

@implementation MobileNumberAuthViewController
@synthesize listView; //must synthesize list view constraints to work

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.backBtn setTintColor: COLOUR_RED];
    [self.backBtn.titleLabel setFont: FONT_BUTTON];
    
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveLanguageChangedNotification:)
                                                 name:kNotificationLanguageChanged
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setupInterface)
                                                 name:@"Reloaded Lookup Table"
                                               object:nil];
    
    if (kIsRightToLeft) {
        [self.txtMobNum setTextAlignment:NSTextAlignmentRight];
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
        [self.btnCountryCode setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.btnWorkshop setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.btnMechanic setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    } else {
        [self.txtMobNum setTextAlignment:NSTextAlignmentLeft];
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
        [self.btnCountryCode setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.btnWorkshop setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.btnMechanic setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    }
    
    [self setupInterface];
}

#pragma mark - Localization Methods
- (void) receiveLanguageChangedNotification:(NSNotification *) notification
{
    if ([notification.name isEqualToString:kNotificationLanguageChanged])
    {
        [mLookupManager reloadLookup];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName: LOCALIZATION_EN(C_TITLE_REGISTRATION) screenClass: nil];
}

-(void) setupInterface
{
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_REGISTRATION) subtitle: @""];      //lokalise 11 Feb
//    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_TITLE_MOBAUTH),  LOCALIZATION(C_REWARDS_BACK)];

    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_DROPDOWN_COUNTRY)       //loklaised
                                                   list: [mSession getCountryDialingCodes]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
    
    [self.btnCountryCode setTitle: [mSession convertCountryCodeToCountryName:GET_COUNTRY_CODE] forState: UIControlStateNormal];
    [self.btnCountryCode setUserInteractionEnabled:NO];
    


//    self.btnWorkshop.hidden = NO;
//    self.lblWorkshop.hidden = NO;
    self.lblWorkshop.textColor = COLOUR_VERYDARKGREY;
    self.btnWorkshop.layer.borderColor = [COLOUR_VERYDARKGREY CGColor];
    self.btnWorkshop.selected = NO;
    self.btnWorkshop.userInteractionEnabled = YES;
    self.lblWorkshop.font = FONT_B1;
    self.lblWorkshop.text = LOCALIZATION(C_REGISTER_WORKSHOP);          //lokalised
    
    self.btnWorkshop.accessibilityLabel = self.lblWorkshop.text;
    self.btnWorkshop.accessibilityHint = LOCALIZATION(@"Checkbox");
    self.lblWorkshop.accessibilityElementsHidden = YES;

//    self.btnMechanic.hidden = NO;
//    self.lblMechanic.hidden = NO;
    self.lblMechanic.textColor = COLOUR_VERYDARKGREY;
    self.btnMechanic.layer.borderColor = [COLOUR_VERYDARKGREY CGColor];
    self.btnMechanic.selected = NO;
    self.btnMechanic.userInteractionEnabled = YES;
    self.lblMechanic.font = FONT_B1;
    self.lblMechanic.text = LOCALIZATION(C_REGISTER_MECHANIC);          //lokalised
    
    self.btnMechanic.accessibilityLabel = self.lblMechanic.text;
    self.btnMechanic.accessibilityHint = LOCALIZATION(@"Checkbox");
    self.lblMechanic.accessibilityElementsHidden = YES;
    
    //Assuming at any one time only one will be hidden
    if([[[mSession lookupTable] objectForKey: @"HideWorkshopRegistration"] boolValue])
    {
//        self.btnWorkshop.hidden = YES;
//        self.lblWorkshop.hidden = YES;
//        self.btnWorkshop.selected = NO;
        self.lblWorkshop.textColor = COLOUR_PALEGREY;
        self.btnWorkshop.layer.borderColor = [COLOUR_PALEGREY CGColor];

//        self.btnMechanic.selected = YES;
        [self mechanicPressed: nil];
        self.btnWorkshop.userInteractionEnabled = NO;
        self.btnMechanic.userInteractionEnabled = NO;
    }
    if ([[[mSession lookupTable] objectForKey: @"HideMechRegistration"] boolValue])
    {
//        self.btnMechanic.hidden = YES;
//        self.lblMechanic.hidden = YES;
        
//        self.btnMechanic.selected = NO;
        self.lblMechanic.textColor = COLOUR_PALEGREY;
        self.btnMechanic.layer.borderColor = [COLOUR_PALEGREY CGColor];
        
//        self.btnWorkshop.selected = YES;
        [self workshopPressed: nil];
        self.btnWorkshop.userInteractionEnabled = NO;
        self.btnMechanic.userInteractionEnabled = NO;
    }
    
    self.txtMobNum.placeholder = LOCALIZATION(C_PROFILE_MOBILENUM);         //lokalised
    self.txtMobNum.font = FONT_B1;
    
    self.btnCountryCode.titleLabel.font = self.txtMobNum.font;
    self.btnCountryCode.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    
    
    self.btnSubmit.titleLabel.font = FONT_BUTTON;
    [self.btnSubmit setTitle: LOCALIZATION(C_REGISTER_SUBMIT) forState: UIControlStateNormal];  //lokalised
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)submitPressed:(id)sender {
    if (![self.btnWorkshop isSelected] && ![self.btnMechanic isSelected])
    {
#warning change to lookup table later?
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_REGISTER_TYPE)];              //lokalised
        return;
    }
    
    NSString *countryCodeString = [mSession convertCountryNameToCountryCode: self.btnCountryCode.titleLabel.text];

//    if (IS_STAGING_VERSION)
//    {
//        if (self.btnWorkshop.isSelected)
//            [mSession pushOTPView:self
//                           mobNum:self.txtMobNum.text
//                      countryCode: countryCodeString
//                          regType: kREGISTRATION_WORKSHOP];
//        else
//            [mSession pushOTPView:self
//                           mobNum:self.txtMobNum.text
//                      countryCode: countryCodeString
//                          regType: kREGISTRATION_MECHANIC];
//    }
//    else
    [[WebServiceManager sharedInstance] validateMobileNumber:self.txtMobNum.text
                                                 countryCode: countryCodeString
                                                          vc:self];
}

- (IBAction)dialingCodePressed:(id)sender {
    [self.view endEditing: YES];
    
    if ([[mSession getCountryDialingCodes] count] > 0)
    {
        UIWindow* window = [[UIApplication sharedApplication] keyWindow];
        [window addSubview: listView];
        listView.translatesAutoresizingMaskIntoConstraints = NO;
        [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings (listView)]];
        [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings (listView)]];
    }
    else
    {
        NSString *languageCodeString  = [NSString stringWithFormat: @"%lu", (unsigned long)[kAvailableLanguages indexOfObject: GET_LOCALIZATION]];
        NSDictionary *requestData = @{@"CountryCode": GET_COUNTRY_CODE,
                                      @"LanguageCode": languageCodeString,
                                      @"LookUpVersion": [mSession.lookupTable objectForKey: @"LookUpVersion"] ? [mSession.lookupTable objectForKey: @"LookUpVersion"] : @"1501065350"};
        
        [[WebServiceManager sharedInstance] requestLookupTableInVC: requestData
                                                                vc:self];
    }
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        NSString *countryName = [mSession convertDialCodeToCountryName: selection.firstObject];
        [self.btnCountryCode setTitle: countryName forState:UIControlStateNormal];
        
        SET_COUNTRY_CODE([mSession convertDialCodeToCountryCode: selection.firstObject]);
        [mSession setDefaultLanguage];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [mLookupManager reloadLookup];

        [CrashlyticsKit setObjectValue: GET_COUNTRY_CODE forKey: @"Country Code"]; //identifier as country code
        
    }
}

- (IBAction)workshopPressed:(id)sender {
    self.btnMechanic.selected = NO;
    self.btnWorkshop.selected = YES;
}
- (IBAction)mechanicPressed:(id)sender {
    self.btnMechanic.selected = YES;
    self.btnWorkshop.selected = NO;
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_MOBILEAUTH:
        {
            NSString *countryCodeString = [mSession convertCountryNameToCountryCode: self.btnCountryCode.titleLabel.text];
            
            if (self.btnWorkshop.isSelected)
                [mSession pushOTPView:self
                               mobNum:self.txtMobNum.text
                          countryCode: countryCodeString
                              regType: kREGISTRATION_WORKSHOP];
            else
                [mSession pushOTPView:self
                               mobNum:self.txtMobNum.text
                          countryCode: countryCodeString
                              regType: kREGISTRATION_MECHANIC];
        }
            break;
        case kWEBSERVICE_LOOKUPTABLE:
        {
            NSDictionary *lookupDict = [response getGenericResponse];
            
            if ([[lookupDict objectForKey: @"LoadData"] boolValue])
            {
                SET_LOOKUP_TABLE(lookupDict);
                mSession.lookupTable = lookupDict;
                SET_LANGUAGE_TABLE([mSession.lookupTable objectForKey: @"LanguageList"]);
                mSession.languageTable = [mSession.lookupTable objectForKey: @"LanguageList"];
                
                [[NSNotificationCenter defaultCenter] postNotificationName: @"Reloaded Lookup Table" object: self];
            }
            
            UIWindow* window = [[UIApplication sharedApplication] keyWindow];
            [window addSubview: listView];
            listView.translatesAutoresizingMaskIntoConstraints = NO;
            [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                           options:0
                                                                           metrics:nil
                                                                             views:NSDictionaryOfVariableBindings (listView)]];
            [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                           options:0
                                                                           metrics:nil
                                                                             views:NSDictionaryOfVariableBindings (listView)]];
        }
            break;
        default:
            break;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
