//
//  OTPViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 25/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "OTPViewController.h"
//#define MAXTIMEBEFORERESEND 120 //for disabling OTP.


@interface OTPViewController () <WebServiceManagerDelegate, TTTAttributedLabelDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblOTPSent;
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UITextField *txtOTP;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

//disabled
@property (weak, nonatomic) IBOutlet UIButton *btnCountryCode;
@property (weak, nonatomic) IBOutlet UITextField *txtMobNum;

@property (weak, nonatomic) IBOutlet UIView *progressBarView; //to remove depending on registration type
@property (weak, nonatomic) IBOutlet UIImageView *progressBarIcon; //to remove depending on registration type

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblOTPResend;

@property (weak, nonatomic) NSTimer *resendTimer;
@property NSInteger timerCount;

@end

@implementation OTPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_REGISTRATION) subtitle: @""];      //lokalised
//    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_TITLE_OTP),  LOCALIZATION(C_REWARDS_BACK)];
    [self.backBtn setTintColor: COLOUR_RED];
    [self.backBtn.titleLabel setFont: FONT_BUTTON];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    
    if (kIsRightToLeft) {
        [self.txtMobNum setTextAlignment:NSTextAlignmentRight];
        [self.txtOTP setTextAlignment:NSTextAlignmentRight];
        [self.lblOTPSent setTextAlignment:NSTextAlignmentRight];
        [self.lblOTPResend setTextAlignment:NSTextAlignmentRight];
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
        [self.btnCountryCode setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    } else {
        [self.txtMobNum setTextAlignment:NSTextAlignmentLeft];
        [self.txtOTP setTextAlignment:NSTextAlignmentLeft];
        [self.lblOTPSent setTextAlignment:NSTextAlignmentLeft];
        [self.lblOTPResend setTextAlignment:NSTextAlignmentLeft];
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
        [self.btnCountryCode setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    }
    
    [self setupInterface];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    
    [FIRAnalytics setScreenName: LOCALIZATION_EN(C_TITLE_REGISTRATION) screenClass: nil];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [self.resendTimer invalidate];
    self.resendTimer = nil;
}


-(void) setupInterface
{
    self.txtMobNum.font = FONT_B1;
    self.txtMobNum.text = self.mobNum;
    
    self.btnCountryCode.titleLabel.font = self.txtMobNum.font;
    [self.btnCountryCode setTitle: [mSession convertCountryCodeToCountryName: self.countryCode] forState: UIControlStateNormal];
    self.btnCountryCode.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    
    self.txtOTP.placeholder = LOCALIZATION(C_OTP_PLACEHOLDER);      //lokalised
    self.txtOTP.font = FONT_B1;
    
    self.btnSubmit.titleLabel.font = FONT_BUTTON;
    [self.btnSubmit setTitle:LOCALIZATION(C_REGISTER_SUBMITOTP) forState:UIControlStateNormal];     //lokalised

    switch (self.regType)
    {
        case kREGISTRATION_WORKSHOP:
            break;
        case kREGISTRATION_MECHANIC:
            
            [self.progressBarView removeFromSuperview];
            [self.progressBarIcon removeFromSuperview];
            break;
    }
    
    self.lblOTPSent.font = FONT_B2;
//    self.lblOTPSent.text = LOCALIZATION(C_REGISTER_AUTHMSG);
    self.lblOTPSent.text = [NSString stringWithFormat:@"%@\r\r%@",LOCALIZATION(C_FORM_MSG_OTPSENT),LOCALIZATION(C_FORM_MSG_AVOIDMULTIPLEOTP)];      //lokalised
    
    
    self.lblOTPResend.font = FONT_B1;

    NSString *resendString = LOCALIZATION(C_REGISTER_RESENDOTP);        //lokalised
    self.lblOTPResend.text = resendString;
    
//    self.timerCount = MAXTIMEBEFORERESEND;
    self.timerCount = [[[mSession lookupTable] objectForKey: @"OTPTimer"] intValue];
    
    
//    [Helper setHyperlinkLabel: self.lblOTPResend hyperlinkText: resendString bodyText:resendString urlString: @"OTP"];
    [self setResendOtpEnable:NO timer:self.timerCount];
    self.resendTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(startTimer) userInfo:nil repeats:YES];
    
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url absoluteString] isEqualToString: @"OTP"])
        //resend OTP
        [[WebServiceManager sharedInstance] validateMobileNumber: self.mobNum
                                                     countryCode: self.countryCode
                                                              vc:self];
        return;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

- (IBAction)submitPressed:(id)sender {
//    if (IS_STAGING_VERSION)
//    {
//        switch (self.regType)
//        {
//            case kREGISTRATION_WORKSHOP:
//                [mSession pushTradeRegistrationView:self mobNum: self.mobNum countryCode: self.countryCode];
//                break;
//            case kREGISTRATION_MECHANIC:
//                [mSession pushMechanicRegistrationView:self mobNum: self.mobNum countryCode: self.countryCode];
//                break;
//            default:
//                break;
//        }
//    }
//    else
    [[WebServiceManager sharedInstance] validateOTP: self.txtOTP.text
                                        countryCode: self.countryCode
                                             mobNum: self.mobNum
                                                 vc: self];
}

-(void) setResendOtpEnable :(BOOL) isEnable timer:(NSInteger)timerCounter
{
    NSString *resendString = LOCALIZATION(C_REGISTER_RESENDOTP);            //lokalised
    if(isEnable){
        [Helper setHyperlinkLabel: self.lblOTPResend hyperlinkText:resendString hyperlinkFont:FONT_B2 bodyText:resendString bodyFont:FONT_B2 urlString:@"OTP" bodyColor: COLOUR_VERYDARKGREY];
        self.lblOTPResend.delegate = self;
    }else{
        NSString *stringCountDown = [NSString stringWithFormat:@"%@ (%d)",resendString,timerCounter];
        [Helper setHyperlinkLabel: self.lblOTPResend hyperlinkText:stringCountDown hyperlinkFont:FONT_B2 bodyText:stringCountDown bodyFont:FONT_B2 urlString:@"OTP" bodyColor:COLOUR_LIGHTGREY];
        self.lblOTPResend.delegate = nil;
    }
}

-(void)startTimer{
    self.timerCount -= 1;
    [self setResendOtpEnable:NO timer:self.timerCount];
    
    if(self.timerCount == 0){
        [self setResendOtpEnable:YES timer:self.timerCount];
//        self.timerCount = MAXTIMEBEFORERESEND;
        self.timerCount = [[[mSession lookupTable] objectForKey: @"OTPTimer"] intValue];
        [self.resendTimer invalidate];
        self.resendTimer = nil;
    }
}

-(void)processCompleted:(WebServiceResponse *)response
{
    if (response.webserviceCall == kWEBSERVICE_MOBILEAUTH)
    {
        if ([response getResponseMessage])
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
            }];
        }
        self.resendTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(startTimer) userInfo:nil repeats:YES];
        [self setResendOtpEnable:NO timer:self.timerCount];
        return;
    }
    switch (self.regType)
    {
        case kREGISTRATION_WORKSHOP:
            [mSession pushTradeRegistrationView:self mobNum: self.mobNum countryCode: self.countryCode];
            break;
        case kREGISTRATION_MECHANIC:
            [mSession pushMechanicRegistrationView:self mobNum: self.mobNum countryCode: self.countryCode];
            break;
        default:
            break;
    }
}

- (IBAction)contactPreferencesPressed:(UIButton *)sender {
    [sender setSelected: sender.selected];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
