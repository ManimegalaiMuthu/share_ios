//
//  TradeRegistrationViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 18/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface TradeRegistrationViewController : BaseVC
@property NSString *countryCode;
@property NSString *mobNum;

@property CGFloat longitude;
@property CGFloat latitude;

-(void) setupInterface;
@end
