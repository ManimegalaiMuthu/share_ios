//
//  TradeRegistrationTwoViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 14/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "TradeRegistrationTwoViewController.h"
#import "WorkingHoursViewController.h"

@interface TradeRegistrationTwoViewController () <WorkingHoursViewDelegate, UITextFieldDelegate, WebServiceManagerDelegate, CommonListDelegate, TTTAttributedLabelDelegate, UITableViewDelegate, UITableViewDataSource>

#define CELL_HEIGHT 50

enum kTradeRegFormTwo_TYPE
{
    kTradeRegFormTwo_REGWORKSHOPNAME,
    kTradeRegFormTwo_WORKSHOPTELEPHONE,
    kTradeRegFormTwo_WORKSHOPEMAIL,
    kTradeRegFormTwo_ADDRESSLINEONE,
    kTradeRegFormTwo_ADDRESSLINETWO,
    kTradeRegFormTwo_TOWN,
    kTradeRegFormTwo_STATE,
    kTradeRegFormTwo_POSTALCODE,
    kTradeRegFormTwo_OILCHANGEPERDAY,
    kTradeRegFormTwo_SEMIOILCHANGEPERDAY,
    kTradeRegFormTwo_SYNTHETICPERDAY,
    kTradeRegFormTwo_SHELLHELIXPERDAY,
};

#define kUserID             @"UserID"
#define kDSRCode            @"DSRCode"
#define kUserPassword       @"UserPassword"
#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kEmailAddress       @"EmailAddress"
#define kDOB                @"DOB"
#define kDOBMonth           @"DOBMonth"
#define kDOBDay             @"DOBDay"
#define kCountryCode        @"CountryCode"
#define kPassword           @"Password"
#define kConfirmPassword    @"ConfirmPassword"
#define kReferenceCode          @"ReferenceCode"        //Workshop Code
#define kWorkshopQRCode         @"WorkshopQRCode"       //Workshop SHARE Code (for QR Scan)
#define kCompanyType            @"CompanyType"
#define kCompanyName            @"CompanyName"
#define kCompanyEmailAddress    @"CompanyEmailAddress"
#define kCompanyTier            @"CompanyTier"
#define kContactNumber          @"ContactNumber"
#define kOilChangePerMonth      @"OilChangePerMonth"
#define kOilChangePercentage    @"OilChangePercentage"
#define kSemiOilChangePercentage    @"SemiOilChangePercentage"
#define kShellHelixQty          @"ShellHelixQty"
#define kStatus                 @"Status"
#define kRemarks                @"Remarks"
#define kAddressType    @"AddressType"
#define kAddress1       @"Address1"
#define kAddress2       @"Address2"
#define kStreetName     @"StreetName"
#define kCompanyCity    @"CompanyCity"
#define kCompanyState   @"CompanyState"
#define kPostalCode     @"PostalCode"
#define kLongitude      @"Longitude"
#define kLatitude       @"Latitude"

//IBOutlet Collection for fonts

@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblWorkshopHeader;


@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblAgree;
@property (weak, nonatomic) IBOutlet UIButton *btnAgree;


@property (weak, nonatomic) IBOutlet UIButton *btnRegister;

@property NSDictionary *workingHoursDict;
@property NSArray *stateTable;

@property CompanyInfo *companyInfo;
@property CompanyAddress *companyAddress;

@property (weak, nonatomic) IBOutlet UITableView *workingHoursTableView;


@property NSMutableArray *registrationFormArray; //ui only
@property NSMutableDictionary *regSubmissionDict;  //key as json key, value = cell. retrieve value by using getJson method

@property (weak, nonatomic) IBOutlet UITableView *regFormTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight; //to adjust according number of cells
@end

@implementation TradeRegistrationTwoViewController
@synthesize listView; //must synthesize list view constraints to work

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_REGISTRATION) subtitle: @""];     //lokalised
    
    if (!self.navigationItem.leftBarButtonItem)
    {
        [self.backBtn setTintColor: COLOUR_RED];
        [self.backBtn.titleLabel setFont: FONT_BUTTON];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    }
    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_TITLE_REGISTRATION),  LOCALIZATION(C_REWARDS_BACK)];
    
    if(kIsRightToLeft) {
        [self.lblAgree setTextAlignment:NSTextAlignmentRight];
    } else {
        [self.lblAgree setTextAlignment:NSTextAlignmentLeft];
    }
    
    [[WebServiceManager sharedInstance] loadState: [[mSession lookupTable] objectForKey: @"LookUpVersion"]
                                      countryCode: self.memberInfo.countryCode
                                               vc: self];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [FIRAnalytics setScreenName: LOCALIZATION_EN(C_TITLE_REGISTRATION) screenClass: nil];
}

-(void) setupRegistrationForm
{
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormTextfieldCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormTextfieldCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormDropdownCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormDropdownCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationLabelTextfieldTableViewCell" bundle:nil] forCellReuseIdentifier:@"RegistrationLabelTextfieldTableViewCell"];
    
    self.regSubmissionDict = [[NSMutableDictionary alloc] init];
    
    self.registrationFormArray = [[NSMutableArray alloc] initWithArray:
                                  @[@{@"Title": LOCALIZATION(C_PROFILE_COMPANYNAME),            //lokalised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kCompanyName,
                                      @"Value": @"",
                                      }, //company name
                                    @{@"Title": LOCALIZATION(C_PROFILE_APPROVEDCONTACTNUMBER),  //lokalised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Keyboard": @(UIKeyboardTypeNumberPad),
                                      @"Key": kContactNumber,
                                      @"Value": @"",
                                      }, //last name
                                    @{@"Title": LOCALIZATION(C_PROFILE_WORKSHOPEMAILADDRESS),   //loaklised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kCompanyEmailAddress,
                                      @"Value": @"",
                                      }, //last name
                                    @{@"Title": LOCALIZATION(C_PROFILE_ADDRESSLINE1),           //lokalised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kAddress1,
                                      @"Value": @"",
                                      }, //last name
                                    @{@"Title": LOCALIZATION(C_PROFILE_ADDRESSLINE2),           //lokalised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kAddress2,
                                      @"Value": @"",
                                      },
                                    @{@"Title": LOCALIZATION(C_PROFILE_TOWN),                   //lokalised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kCompanyCity,
                                      @"Value": @"",
                                      },
                                    @{@"Title": LOCALIZATION(C_PROFILE_STATE),                  //lokalised
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key": kCompanyState,
                                      @"Value": LOCALIZATION(C_PROFILE_STATE_PLACEHOLDER),      //lokalised
                                      },
                                    @{@"Title": LOCALIZATION(C_PROFILE_POSTALCODE),             //lokalised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kPostalCode,
                                      @"Keyboard": @(UIKeyboardTypeNumberPad),
                                      @"Value": @"",
                                      },
                                    @{@"Title": LOCALIZATION(C_PROFILE_OILCHANGEPERDAY),        //lokalised
                                      @"Placeholder": LOCALIZATION(C_PROFILE_ANSWER),           //lokalised
                                      @"Type": @(kREGISTRATION_LABELWITHTEXTFIELD),
                                      @"Key": kOilChangePerMonth,
                                      @"Keyboard": @(UIKeyboardTypeNumberPad),
                                      @"Value": @"",
                                      },
                                    @{@"Title": LOCALIZATION(C_PROFILE_OILCHANGEPERCENT),       //lokalised
                                      @"Placeholder": LOCALIZATION(C_PROFILE_ANSWER),           //lokalised
                                      @"Type": @(kREGISTRATION_LABELWITHTEXTFIELD),
                                      @"Key": kOilChangePercentage,
                                      @"Keyboard": @(UIKeyboardTypeDecimalPad),
                                      @"Value": @"",
                                      },
                                    @{@"Title": LOCALIZATION(C_PROFILE_SEMIOILCHANGEPERCENT),   //lokalised
                                      @"Placeholder": LOCALIZATION(C_PROFILE_ANSWER),           //lokalised
                                      @"Type": @(kREGISTRATION_LABELWITHTEXTFIELD),
                                      @"Key":  kSemiOilChangePercentage,
                                      @"Keyboard": @(UIKeyboardTypeDecimalPad),
                                      @"Value": @"",
                                      },
                                    @{@"Title": LOCALIZATION(C_PROFILE_SHELLHELIX),             //lokalised
                                      @"Placeholder": LOCALIZATION(C_PROFILE_ANSWER),           //lokalised
                                      @"Type": @(kREGISTRATION_LABELWITHTEXTFIELD),
                                      @"Key": kShellHelixQty,
                                      @"Keyboard": @(UIKeyboardTypeNumberPad),
                                      @"Value": @"",
                                      },
                                    ]
                                  ];
    
    //set tableview height.
    self.tableViewHeight.constant = CELL_HEIGHT * [self.registrationFormArray count];
    self.regFormTableView.tableFooterView = [UIView new];
    [self.regFormTableView reloadData];
}

#pragma mark - tableview delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.registrationFormArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *regFieldData = [self.registrationFormArray objectAtIndex: indexPath.row];
    switch ([[regFieldData objectForKey: @"Type"] intValue])
    {
        case kREGISTRATION_TEXTFIELD:
        {
            RegistrationFormTextfieldCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormTextfieldCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            
            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            [textfieldCell hideTopLabel: YES];
            
            if ([regFieldData objectForKey: @"Placeholder"])
                [textfieldCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
            
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_DROPDOWN:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            [dropdownCell hideTopLabel: YES];
            dropdownCell.button.tag = indexPath.row;
            
            switch (indexPath.row) {
                case kTradeRegFormTwo_STATE:
                    [dropdownCell.button addTarget: self action: @selector(statePressed:) forControlEvents:UIControlEventTouchUpInside];
                    break;
            }
            //set initial value
            if ([regFieldData objectForKey: @"Value"])
                [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            
            //keep a reference of cell to submit json value later
            [self.regSubmissionDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        case kREGISTRATION_LABELWITHTEXTFIELD:
        {
            RegistrationLabelTextfieldTableViewCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationLabelTextfieldTableViewCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            [textfieldCell setLeftTitle: [regFieldData objectForKey: @"Title"]];
            [textfieldCell setPlaceholder: [regFieldData objectForKey: @"Placeholder"]];
//            [textfieldCell hideTopLabel: YES];
            
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
            break;
        default:
            NSLog(@"UNKNOWN CELL TYPE");
            return [UITableViewCell new];
            break;
    }
    return [UITableViewCell new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}


-(void) setupInterface
{
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_COUNTRY)            //lokalised
                                                   list: [mSession getCountryNames]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
    
    //headers
    [self.lblWorkshopHeader setFont: FONT_H1];
    [self.lblWorkshopHeader setTextColor: COLOUR_VERYDARKGREY];
    [self.lblWorkshopHeader setText: LOCALIZATION(C_HEADER_MYWORKSHOP)];                            //lokalised 11 Feb
    
    self.workingHoursDict = @{@"WorkHourStart1":   @"00:00",
                              @"WorkHourEnd1":     @"23:00",
                              @"WeekDays1":        @"0",
                              @"PublicHoliday":    @"0",
                              @"WorkHourStart2":   @"00:00",
                              @"WorkHourEnd2":     @"23:00",
                              @"WeekDays2":        @"0",
                                       };
    
    //misc.
    [self.btnRegister.titleLabel setFont: FONT_BUTTON];
    [self.btnRegister setTitle:LOCALIZATION(C_FORM_SUBMIT) forState:UIControlStateNormal];              //lokalised
    [Helper setCustomFontButtonContentModes: self.btnRegister];
    
    
    [self.lblAgree setFont: FONT_B2];
    self.lblAgree.text = LOCALIZATION(C_PROFILE_AGREE);                                                 //lokalised
    
    [Helper setHyperlinkLabel:self.lblAgree hyperlinkText: LOCALIZATION(C_PROFILE_TNC) bodyText: LOCALIZATION(C_PROFILE_AGREE) urlString:@"TNC"];   //lokalised

    [Helper addHyperlink: self.lblAgree hyperlinkText: LOCALIZATION(C_PROFILE_PRIVACY) urlString:@"Privacy" bodyText: LOCALIZATION(C_PROFILE_AGREE)]; //lokalised
    
    self.lblAgree.delegate = self;
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url absoluteString] isEqualToString: @"TNC"])
        [mSession pushTNCView:NO tncType: YES vc:self];
    if ([[url absoluteString] isEqualToString: @"Privacy"])
        [mSession pushPrivacyView:self];
}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}

//- (IBAction)countryPressed:(id)sender {
//    
//    listView.tag = kcountry;
//    [listView setTitleAndList:@"Select Country" list:[mSession getCountryNames]];
//    [self popUpView];
//}

- (IBAction)statePressed:(id)sender {
    
    listView.tag = kTradeRegFormTwo_STATE;
    [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_STATE) list: [mSession getStateNames: self.stateTable] hasSearchField: YES];  //lokalised
    [self popUpView];
}

- (IBAction)workingHoursPressed:(id)sender {
    [mSession pushWorkingHoursView: self workingHoursData: self.workingHoursDict];
}

-(void)didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict
{
    //assign working hours
    self.workingHoursDict = workingHoursDict;
    
//    [self.workingHoursTableView reloadData];
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)checkBoxSelected:(UIButton *)sender {
    if ([sender isSelected])
        sender.selected = NO;
    else
        sender.selected = YES;
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        RegistrationFormDropdownCell *cell;
        switch (listView.tag)
        {
            case kTradeRegFormTwo_STATE:
                cell = [self.regSubmissionDict objectForKey: kCompanyState];
                break;
        }
        [cell.button setTitle:selection.firstObject forState:UIControlStateNormal];
    }
}

- (IBAction)registerPressed:(id)sender {
    
    self.companyInfo = [[CompanyInfo alloc] init];
    self.companyInfo.referenceCode          = @"";
    self.companyInfo.workshopQRCode         = @"";
    self.companyInfo.companyType            = 0;
    self.companyInfo.companyName            = [[self.regSubmissionDict objectForKey: kCompanyName] getJsonValue];
    self.companyInfo.companyEmailAddress    = [[self.regSubmissionDict objectForKey: kCompanyEmailAddress ] getJsonValue];
    self.companyInfo.companyTier            = @"1";
    self.companyInfo.contactNumber          = [[self.regSubmissionDict objectForKey: kContactNumber] getJsonValue];
    self.companyInfo.status                 = 1;    //defaulted as pending
    self.companyInfo.remarks                = @"";

    
    self.companyInfo.oilChangePerMonth      = [[self.regSubmissionDict objectForKey: kOilChangePerMonth ] getJsonValue];
    self.companyInfo.halfSyntheticOilChanges = [[self.regSubmissionDict objectForKey: kSemiOilChangePercentage ] getJsonValue];
    self.companyInfo.oilChangePercentage    = [[self.regSubmissionDict objectForKey: kOilChangePercentage ] getJsonValue];
    self.companyInfo.shellHelixQty          = [[self.regSubmissionDict objectForKey: kShellHelixQty] getJsonValue];

    self.companyAddress = [[CompanyAddress alloc] init];

    self.companyAddress.addressType     = @"";
    self.companyAddress.address1        = [[self.regSubmissionDict objectForKey: kAddress1] getJsonValue];
    self.companyAddress.address2        = [[self.regSubmissionDict objectForKey: kAddress2] getJsonValue];
    self.companyAddress.streetName      = @"";
    self.companyAddress.companyCity     = [[self.regSubmissionDict objectForKey: kCompanyCity] getJsonValue];;
    
    NSString *stateString = [[self.regSubmissionDict objectForKey: kCompanyState] getJsonValue];
    self.companyAddress.companyState    = [mSession convertToStateKeyCode: stateString stateDetailedListArray:self.stateTable];
    self.companyAddress.postalCode      = [[self.regSubmissionDict objectForKey: kPostalCode ] getJsonValue];
    self.companyAddress.longitude       = 0;
    self.companyAddress.latitude        = 0;
    
    NSArray *companyService = @[@{@"ServiceID": @"1"},
                                     @{@"ServiceID": @"2"},
                                     ];
    
    NSDictionary *registerParams = @{@"CountryCode":            self.memberInfo.countryCode,
                                     @"TermCondition":          @(self.btnAgree.selected),
                                     @"REG_Company":            [self.companyInfo getJSON],
                                     @"REG_CompanyAddress":     [self.companyAddress getJSON],
                                     @"REG_CompanyService":     companyService,
                                     @"REG_CompanyWorkHours":   self.workingHoursDict,
                                     @"REG_Member":             [self.memberInfo getJSON],
                                     
                                     };
    
    [[WebServiceManager sharedInstance] registerTradePreLoginTwo:registerParams
                                                              vc:self];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
            SET_LOADSTATE([[response getGenericResponse] objectForKey: @"StateDetailsList"]);
            self.stateTable = [[response getGenericResponse] objectForKey: @"StateDetailsList"];
            
            [self setupRegistrationForm];
            [self setupInterface];
            break;
        case kWEBSERVICE_TRADEREGISTERTWO:
            {
                [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                    
                    //non-assisted pop back to root screen
                    [self popToIndex: 0];
                }];
            }
            //if assisted registration?
            break;
        default:
            break;
    }
}

-(void)processFailed:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress: KEY_WINDOW];
    
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_STATE:
            self.stateTable = GET_LOADSTATE;
            
            [self setupRegistrationForm];
            [self setupInterface];
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
