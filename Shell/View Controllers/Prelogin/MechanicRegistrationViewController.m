//
//  MechanicRegistrationViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 14/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "MechanicRegistrationViewController.h"
#import "QRCodeScanner.h"
#import "CASCommonListView.h"
#import "CASDatePickerView.h"
#import "Member.h"

#define CELL_HEIGHT 50

#define COLVIEW_CELLHEIGHT      50
#define COLVIEW_LINESPACING     10


//enum kTradeRegForm_TYPE
//{
//    kTradeRegForm_SALUTATION,
//    kTradeRegForm_FIRSTNAME,
//    kTradeRegForm_LASTNAME,
//    kTradeRegForm_MOBNUM,
//    kTradeRegForm_EMAIL,
//    kTradeRegForm_DOB,
//    kTradeRegForm_QRSCAN,
//    kTradeRegForm_PASSWORD,
//    kTradeRegForm_CONFIRMPASS,
//};

#define kUserID             @"UserID"
//#define kDSRCode            @"DSRCode"
#define kWorkshopQRCode     @"WorkshopQRCode"
#define kUserPassword       @"UserPassword"
#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kEmailAddress       @"EmailAddress"
#define kDOB                @"DOB"
#define kDOBMonth           @"DOBMonth"
#define kDOBDay             @"DOBDay"
#define kCountryCode        @"CountryCode"
#define kPassword           @"Password"
#define kConfirmPassword    @"ConfirmPassword"


@interface MechanicRegistrationViewController () <QRCodeScannerDelegate, UITextFieldDelegate, WebServiceManagerDelegate, CommonListDelegate, DatePickerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, TTTAttributedLabelDelegate>
//IBOutlet Collection for fonts

@property Member *member;

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblAgree;
@property (weak, nonatomic) IBOutlet UIButton *btnAgree;

@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileHeader;

@property (weak, nonatomic) IBOutlet UILabel *contactPrefLbl;
@property (weak, nonatomic) IBOutlet UICollectionView *colView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *colViewHeight; //to adjust according to number of cells <1 + (count / 2)>
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *colViewLayout; //to adjust cell width size according to screen width
@property (strong, nonatomic) NSMutableArray *contactPrefArray;

@property CASDatePickerView *dateView;


@property NSMutableArray *registrationFormArray; //ui only
@property NSMutableDictionary *regSubmissionDict;  //key as json key, value = cell. retrieve value by using getJson method

@property (weak, nonatomic) IBOutlet UITableView *regFormTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight; //to adjust according number of cells
@end

@implementation MechanicRegistrationViewController
@synthesize listView; //must synthesize list view constraints to work
@synthesize dateView; //must synthesize date view constraints to work

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupInterface];
    [self setupRegistrationForm];
}

//use TradeRegistrationViewController viewWillAppear
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];

    [FIRAnalytics setScreenName: LOCALIZATION_EN(C_TITLE_REGISTRATION) screenClass: nil];
}


-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    //reassign button to child VC
    RegistrationTextfieldWithButtonCell *textfieldCell = [self.regSubmissionDict objectForKey: kWorkshopQRCode];
    [textfieldCell.button addTarget:self action:@selector(btnScanWorkshop:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnRegister setTitle:LOCALIZATION(C_FORM_REGISTER) forState:UIControlStateNormal];            //lokalised
}

-(void) setupInterface
{
    [super setupInterface];
//
//    listView = [[CASCommonListView alloc] initWithTitle:@"Select Salutations"
//                                                   list: [mSession getSalutations]
//                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
//    listView.delegate = self;
//    
//    dateView = [[CASDatePickerView alloc] initWithTitle:@"Select Birthday" previouslySelected:nil withYearHidden:YES];
//    
//    dateView.delegate = self;
    
//    //headers
    [self.lblProfileHeader setFont: FONT_H1];
    [self.lblProfileHeader setTextColor: COLOUR_VERYDARKGREY];
    self.lblProfileHeader.text =  LOCALIZATION(C_HEADER_MYPROFILE);         //lokalised
    
    
    [self.lblAgree setFont: FONT_B2];
    self.lblAgree.text = LOCALIZATION(C_PROFILE_AGREE);             //lokalised
    if(kIsRightToLeft) {
        [self.lblAgree setTextAlignment:NSTextAlignmentRight];
    } else {
        [self.lblAgree setTextAlignment:NSTextAlignmentLeft];
    }
    
    [Helper setHyperlinkLabel:self.lblAgree hyperlinkText: LOCALIZATION(C_PROFILE_TNC) bodyText: LOCALIZATION(C_PROFILE_AGREE) urlString:@"TNC"];             //lokalised
    
    [Helper addHyperlink: self.lblAgree hyperlinkText: LOCALIZATION(C_PROFILE_PRIVACY) urlString:@"Privacy" bodyText: LOCALIZATION(C_PROFILE_AGREE)];                     //lokalised
    
    self.lblAgree.delegate = self;
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url absoluteString] isEqualToString: @"TNC"])
        [mSession pushTNCView:NO tncType: 2
                           vc:self];
    if ([[url absoluteString] isEqualToString: @"Privacy"])
        [mSession pushPrivacyView:self];
}

-(void) setupRegistrationForm
{
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormTextfieldCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormTextfieldCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormDropdownCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormDropdownCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationTextfieldWithButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextfieldWithButtonCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationMobileNumberCell" bundle:nil] forCellReuseIdentifier:@"RegistrationMobileNumberCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationButtonCell"];
    
    self.regSubmissionDict = [[NSMutableDictionary alloc] init];
    
    self.registrationFormArray = [[NSMutableArray alloc] initWithArray:
                                  @[@{@"Title": LOCALIZATION(C_PROFILE_SALUTATION),     //lokalised
                                      @"Type": @(kREGISTRATION_DROPDOWN),
                                      @"Key": kSalutation,
                                      @"Value": LOCALIZATION(C_PROFILE_SALUTATION),     //lokalised
                                      },  //button
                                    @{@"Title": LOCALIZATION(C_PROFILE_FIRSTNAME),       //lokalised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kFirstName,
                                      @"Value": @"",
                                      }, //first name
                                    @{@"Title": LOCALIZATION(C_PROFILE_LASTNAME),        //lokalised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kLastName,
                                      @"Value": @"",
                                      }, //last name
                                    @{@"Title": LOCALIZATION(C_PROFILE_MOBILENUM),       //lokalised
                                      @"Type": @(kREGISTRATION_MOBNUM),
                                      @"Key": kMobileNumber,
                                      @"CountryCode": [mSession convertCountryCodeToCountryName: self.countryCode],
                                      @"MobileNumber": self.mobNum,
                                      @"Enabled": @(NO),
                                      },   //Mobile number
                                    @{@"Title": LOCALIZATION(C_PROFILE_EMAILADDRESS),        //lokalised
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"Key": kEmailAddress,
                                      @"Keyboard": @(UIKeyboardTypeEmailAddress),
                                      @"Value": @"",
                                      }, //email
                                    @{@"Title": LOCALIZATION(C_PROFILE_DOB),                 //lokalised
                                      @"Type": @(kREGISTRATION_BIRTHDATE),
                                      @"Key": kDOB,
                                      @"Value": LOCALIZATION(C_PROFILE_DOB),                  //lokalised
                                      },   //Date of birth
                                    
                                    
                                    @{@"Title": LOCALIZATION(C_PROFILE_WORKSHOPSHARECODE),      //lokalised
                                      @"Key": kWorkshopQRCode,
                                      @"Icon": @"icon-code",
                                      @"Type": @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                                      @"Value": @"",
                                      },   //DSR Code
                                    @{@"Title": LOCALIZATION(C_PROFILE_CREATEPASS),             //lokalised
                                      @"Key": kPassword,
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"SecureEntry": @(YES),
                                      @"Value": @"",
                                      }, //create password
                                    @{@"Title": LOCALIZATION(C_PROFILE_CONFPASS),            //lokalised
                                      @"Key": kConfirmPassword,
                                      @"Type": @(kREGISTRATION_TEXTFIELD),
                                      @"SecureEntry": @(YES),
                                      @"Value": @"",
                                      }, //email
                                    ]
                                  ];
    
    //set tableview height.
    self.tableViewHeight.constant = CELL_HEIGHT * [self.registrationFormArray count];
    self.regFormTableView.tableFooterView = [UIView new];
    [self.regFormTableView reloadData];
}

#pragma mark - tableview delegate
- (IBAction)registerPressed:(id)sender {
    if (![[[self.regSubmissionDict objectForKey: kPassword] getJsonValue] isEqualToString:
          [[self.regSubmissionDict objectForKey: kConfirmPassword] getJsonValue]])
    {
        //        [mAlert showErrorAlertWithMessage: LOCALIZATION(<#text#>)];
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_CHANGEPASSWORD_MISMATCH_ERRORMSG)];    //lokalised
        
        return;
    }
    self.member.registrationType     = @"2"; //"2" for non assisted trade registration
    
    self.member.firstName =   [[self.regSubmissionDict objectForKey: kFirstName] getJsonValue];
    self.member.lastName =    [[self.regSubmissionDict objectForKey: kLastName] getJsonValue];
    self.member.mobileNumber = self.mobNum;
    self.member.emailAddress = [[self.regSubmissionDict objectForKey: kEmailAddress] getJsonValue];
    
    self.member.countryCode  = self.countryCode;
    self.member.userID       = self.mobNum;
    self.member.DSRCode      = [[self.regSubmissionDict objectForKey: kWorkshopQRCode] getJsonValue];   //use as Workshop QR
    self.member.userPassword = [[self.regSubmissionDict objectForKey: kPassword] getJsonValue];
    
    self.member.salutation   = [[mSession convertToSalutationKeyCode:[[self.regSubmissionDict objectForKey: kSalutation] getJsonValue]] intValue];
    
    //    self.member.gender               = [mSession convertToGenderKeyCode:self.btnGender.titleLabel.text];
    self.member.gender               = @"0";    //not capturing
    
    NSString *contactPrefString = @"";
    for (ContactPreferencesCollectionViewCell* cell in self.contactPrefArray)
    {
        if ([cell.checkboxBtn isSelected])
        {
            if (contactPrefString.length == 0)
                contactPrefString = @(cell.bitValue).stringValue;
            else
                contactPrefString = [contactPrefString stringByAppendingString: [NSString stringWithFormat: @",%@", @(cell.bitValue).stringValue]];
        }
    }
    self.member.contactPreference    = contactPrefString;
    
    self.member.registrationMode     = @"1"; //should be defaulted depending on registration method
    self.member.registrationRole    = @"1";
    
    NSDictionary *mechanicRegistrationParams = @{COUNTRYCODE_KEY:       self.countryCode,
                                                 @"TermCondition":      @(self.btnAgree.selected).stringValue,
                                                 kFirstName:            self.member.firstName,
                                                 kLastName:             self.member.lastName,
                                                 kMobileNumber:         self.mobNum,
                                                 kEmailAddress:         self.member.emailAddress,
                                                 kUserPassword:         self.member.userPassword,
                                                 @"ContactPreferences": self.member.contactPreference,
                                                 kWorkshopQRCode:       self.member.DSRCode
                                                 };
    
    [[WebServiceManager sharedInstance] registerMechanic: mechanicRegistrationParams
                                                      vc: self];
}

- (IBAction)btnScanWorkshop:(id)sender {
    [mQRCodeScanner showZBarScannerOnVC: self];
}

- (void)qrCodeScanSuccess:(NSString *)scanString
{
    RegistrationTextfieldWithButtonCell *cell = [self.regSubmissionDict objectForKey: kWorkshopQRCode];
    [cell setTextfieldText: [[scanString componentsSeparatedByString:@"="] lastObject]];
}

- (IBAction)checkBoxSelected:(UIButton *)sender {
    if ([sender isSelected])
        sender.selected = NO;
    else
        sender.selected = YES;
}


-(void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_MECHREGISTER:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                
                //non-assisted pop back to root screen
                [self popToIndex: 0];
            }];
        }
            //if assisted registration?
            break;
        default:
            break;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
