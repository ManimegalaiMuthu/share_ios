//
//  OTPViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 25/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface OTPViewController : BaseVC
@property NSString *countryCode;
@property NSString *mobNum;
@property NSInteger regType;
@end
