//
//  TradeRegistrationTwoViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 14/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"
#import "Member.h"
@interface TradeRegistrationTwoViewController : BaseVC
@property Member *memberInfo;
@end
