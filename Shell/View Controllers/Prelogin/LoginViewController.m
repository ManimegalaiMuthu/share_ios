//
//  LoginViewController.m
//  Syngenta
//
//  Created by Will Choy on 6/10/15.
//  Copyright © 2015 WillChoy. All rights reserved.
//

#import "LoginViewController.h"
#import "DatabaseManager.h"
#import "HMSegmentedControl.h"
#import "RegistrationFormTextfieldCell.h"
#import "RegistrationFormDropdownCell.h"
#import "RegistrationTextfieldWithButtonCell.h"
#import "RegistrationMobileNumberCell.h"
#import "RegistrationButtonCell.h"

#import "PopupViewController.h"
#import <Lokalise/Lokalise.h>

#define kAppVersionErrorCode 3001

@interface LoginViewController () <TTTAttributedLabelDelegate, UITextFieldDelegate, WebServiceManagerDelegate, UITableViewDataSource, UITableViewDelegate, PopupListDelegate>
{
    IQKeyboardReturnKeyHandler *returnKeyHandler;
}

@property (weak, nonatomic) IBOutlet UIButton *btnCountryCode;
@property (weak, nonatomic) IBOutlet UITextField *txtMobNum;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblForgetPassword;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblRegister;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblContactUs;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contactUsTopSpace;

@property (weak, nonatomic) IBOutlet UIButton *btnLogin;


@property (weak, nonatomic) IBOutlet UIButton *btnKeepLogin;
@property (weak, nonatomic) IBOutlet UILabel *lblKeepLogin;

@property (strong, nonatomic) IBOutlet UIButton *btnLanguage;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblTNC;

//@property (weak, nonatomic) IBOutlet UIView *vwLocalization;
@property (nonatomic, strong) HMSegmentedControl *segmentedControl;

@property (weak, nonatomic) IBOutlet UILabel *lblVersion;

@property BOOL isReloadingDropdownList;

@property NSInteger popupTag;
@end


enum kPopupSelection_Type
{
    kPopupSelection_Country,
    kPopupSelection_Language,
};

@implementation LoginViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
//    [NSNotificationCenter.defaultCenter addObserver:self
//                                           selector:@selector(receiveLanguageChangedNotification:)
//                                               name:LokaliseDidUpdateLocalizationNotification
//                                             object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveLanguageChangedNotification:)
                                                 name:kNotificationLanguageChanged
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setUpInterface)
                                                 name:@"Reloaded Lookup Table"
                                               object:nil];
    
    returnKeyHandler = [[IQKeyboardReturnKeyHandler alloc] initWithViewController:self];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear: animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name:@"Reloaded Lookup Table"
                                                  object:nil];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    
    [FIRAnalytics setScreenName: LOCALIZATION_EN(C_TITLE_LOGIN) screenClass: nil];
    
    [self.btnCountryCode setTitle: [mSession convertCountryCodeToCountryName:GET_COUNTRY_CODE] forState: UIControlStateNormal];
    NSLog(@"%@",[mSession convertCountryCodeToCountryName:GET_COUNTRY_CODE]);
    
    [self setUpInterface];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([GET_COUNTRY_CODE isEqual: COUNTRYCODE_INDONESIA]) {
        self.lblRegister.alpha = 0;
    } else {
        self.lblRegister.alpha = 1;
    }
    
//    if ([[UIApplication sharedApplication] supportsAlternateIcons]) {
//        if ([GET_COUNTRY_CODE isEqual: COUNTRYCODE_VIETNAM]) {
//            [[UIApplication sharedApplication] setAlternateIconName:@"AppIconViet" completionHandler:^(NSError * _Nullable error) {
//              NSLog(@"%@", error.description);
//            }];
//        } else {
//            [[UIApplication sharedApplication] setAlternateIconName:nil completionHandler:^(NSError * _Nullable error) {
//              NSLog(@"%@", error.description);
//            }];
//        }
//    }
    
    if (kIsRightToLeft) {
        [self.txtMobNum setTextAlignment: NSTextAlignmentRight];
        [self.txtPassword setTextAlignment: NSTextAlignmentRight];
        [self.lblTNC setTextAlignment: NSTextAlignmentRight];
        [self.btnCountryCode setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    } else {
        [self.txtMobNum setTextAlignment: NSTextAlignmentLeft];
        [self.txtPassword setTextAlignment: NSTextAlignmentLeft];
        [self.lblTNC setTextAlignment: NSTextAlignmentLeft];
        [self.btnCountryCode setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    }
    
    NSDictionary *lookupTable = [[NSDictionary alloc] initWithDictionary:GET_LOOKUP_TABLE];
    if(lookupTable == nil)
        [mLookupManager loadLookupTable];
}

-(void)dealloc
{
    returnKeyHandler = nil;
}

- (void)setUpInterface {
    [self updateLanguageButton];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.btnLanguage];
//    [self.btnLanguage sizeToFit];
    
    self.btnCountryCode.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    
    [Helper setCustomFontButtonContentModes: self.btnLogin];
    
//    [self.btnForgetPassword setTitle:LOCALIZATION(C_LOGIN_FORGET) forState:UIControlStateNormal];
    
    [self.btnCountryCode setTitle: [mSession convertCountryCodeToCountryName:GET_COUNTRY_CODE] forState: UIControlStateNormal];
    
//    self.lblForgetPassword.font = FONT_H1;
//    self.lblRegister.font = self.lblForgetPassword.font;
//    self.lblContactUs.font = self.lblForgetPassword.font;
//    self.lblContactUs.font = self.lblTNC.font;
    
    [self setupLocalization];
    
    if (kIsRightToLeft) {
        [self.btnCountryCode setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    } else {
        [self.btnCountryCode setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    }
}

- (void) setupLocalization
{
//    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_LOGIN) subtitle: @""];
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_LOGIN) subtitle: @""]; //lokalised 16 jan
    
    AppInfo *appinfo = [[AppInfo alloc]initWithAppInfo];
    self.lblVersion.font = FONT_B1;
    if (IS_DEVELOPMENT_VERSION || IS_STAGING_VERSION)
        self.lblVersion.text = [NSString stringWithFormat:@"%@ %@ (%@)",LOCALIZATION(C_GLOBAL_VERSION).capitalizedString, appinfo.appVersion, appinfo.appBuild ];   //lokalised 16 jan
    else
        self.lblVersion.text = [NSString stringWithFormat:@"%@ %@",LOCALIZATION(C_GLOBAL_VERSION).capitalizedString, appinfo.appVersion]; //lokalised 16 jan
    
    //language selection button
    [self.btnLanguage.titleLabel setFont: FONT_BUTTON];
    
    if (![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_UNITEDKINGDOM] )//if UK hide menu
        self.btnLanguage.hidden = NO;
    else
        self.btnLanguage.hidden = YES;
        
    self.txtMobNum.font = FONT_B1;
    self.txtPassword.font = self.txtMobNum.font;
    
    self.btnCountryCode.titleLabel.font = self.txtMobNum.font;
    self.lblKeepLogin.font = FONT_B2;
    self.btnLogin.titleLabel.font = FONT_BUTTON;
    self.txtMobNum.placeholder = LOCALIZATION(C_LOGIN_USERNAME_PLACEHOLDER);    //lokalised 16 jan
    self.txtPassword.placeholder = LOCALIZATION(C_LOGIN_PASSWORD_PLACEHOLDER); //lokalised 16 jan
    self.lblKeepLogin.text = LOCALIZATION(C_LOGIN_KEEPLOGIN);               //lokalised 16 jan
//    self.lblKeepLogin.accessibilityElementsHidden = YES;
//    self.btnKeepLogin.accessibilityLabel = self.lblKeepLogin.text;
//    self.btnKeepLogin.accessibilityValue = LOCALIZATION(@"Checkbox");
    
    
    [self.btnLogin setTitle:LOCALIZATION(C_LOGIN_ENTER) forState:UIControlStateNormal]; //lokalised 16 jan
    
    self.lblForgetPassword.text = LOCALIZATION(C_LOGIN_FORGET); //lokalised 16 jan
    self.lblForgetPassword.delegate = self;
//    self.lblForgetPassword.text = LOCALIZATION(C_LOGIN_FORGETLABEL);
    [Helper setHyperlinkLabel:self.lblForgetPassword hyperlinkText: self.lblForgetPassword.text hyperlinkFont: FONT_H1 bodyText: self.lblForgetPassword.text bodyFont: FONT_H1 urlString:@"Forget"];
    
    if (![GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_UNITEDKINGDOM])//if UK hide menu
    {
        self.lblRegister.hidden = YES;
        self.lblRegister.text = LOCALIZATION(C_LOGIN_REGISTER); //Lokalised 16 jan
        [Helper setHyperlinkLabel:self.lblRegister hyperlinkText: self.lblRegister.text  hyperlinkFont: FONT_H1 bodyText: self.lblRegister.text bodyFont: FONT_H1 urlString:@"Register"];
        self.lblRegister.delegate = self;
        
        
        self.lblContactUs.hidden = NO;
        self.lblContactUs.text = LOCALIZATION(C_LOGIN_CONTACT); //Lokalised 16 jan
        [Helper setHyperlinkLabel:self.lblContactUs hyperlinkText: self.lblContactUs.text  hyperlinkFont: FONT_H1 bodyText: self.lblContactUs.text bodyFont: FONT_H1 urlString:@"Contact"];
        
        self.lblContactUs.delegate = self;
    }
    else
    {
        self.lblContactUs.hidden = YES;
        self.lblRegister.hidden = YES;
    }
        
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA] ||
        [GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_HONGKONG] ||
        [GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_INDIA] ||
        [GET_COUNTRY_CODE isEqualToString:COUNTRYCODE_SAUDI])
        //hide registration
    {
        self.lblRegister.hidden = YES;
    }
    
    self.contactUsTopSpace.constant = self.lblRegister.isHidden ? 15.0 : 54.0;

    self.lblTNC.text = LOCALIZATION(C_LOGIN_AGREETNCPRIVACY); //Lokalised 16 jan
    
    {
        [Helper setHyperlinkLabel:self.lblTNC hyperlinkText: LOCALIZATION(C_LOGIN_TNC)  hyperlinkFont: FONT_H2 bodyText: LOCALIZATION(C_LOGIN_AGREETNCPRIVACY) bodyFont: FONT_B2 urlString:@"TNC"]; //Lokalised 16 jan
    }
    
    [Helper addHyperlink:self.lblTNC hyperlinkText: LOCALIZATION(C_LOGIN_PRIVACY) hyperlinkFont: FONT_H2 urlString:@"Privacy" bodyText: self.lblTNC.text]; //Lokalised 16 jan
    
    self.lblTNC.delegate = self;
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if ([[url absoluteString] isEqualToString: @"Forget"])
        [mSession pushForgetPasswordView: self];
    else if ([[url absoluteString] isEqualToString: @"Register"])
        [mSession pushMobileAuthView: self];
    else if ([[url absoluteString] isEqualToString: @"Contact"])
        [mSession pushContactView:self];
    else if ([[url absoluteString] isEqualToString: @"TNC"])
        [mSession pushTNCView: NO tncType: 0 vc:self];
    else if ([[url absoluteString] isEqualToString: @"Privacy"])
        [mSession pushPrivacyView:self];
}

- (IBAction)loginPressed:(id)sender {
    [self.view endEditing: YES];
    //initialise login info data model
    LoginInfo *loginInfo = [[LoginInfo alloc]initWithLoginInfo];
    
    loginInfo.countryCode = [mSession convertCountryNameToCountryCode: self.btnCountryCode.titleLabel.text];
    
    loginInfo.credentials.loginName = self.txtMobNum.text;
    loginInfo.credentials.loginPassword = self.txtPassword.text;
    
    [[WebServiceManager sharedInstance] login:loginInfo vc:self];
}

- (IBAction)countryCodePressed:(id)sender {
    
    self.popupTag = kPopupSelection_Country;
    
    if ([[mSession getCountryDialingCodes] count] > 0)
    {
        [self popUpListViewWithTitle: LOCALIZATION(C_DROPDOWN_COUNTRY) listArray: [mSession getCountryDialingCodes]]; //Lokalised 16 jan
    }
    else
    {
        self.isReloadingDropdownList = YES;

        NSString *languageCodeString  = [NSString stringWithFormat: @"%lu", (unsigned long)[kAvailableLanguages indexOfObject: GET_LOCALIZATION]];
        NSDictionary *requestData = @{@"CountryCode": GET_COUNTRY_CODE,
                                      @"LanguageCode": languageCodeString,
                                      @"LookUpVersion": [mSession.lookupTable objectForKey: @"LookUpVersion"] ? [mSession.lookupTable objectForKey: @"LookUpVersion"] : @"1501065350"};
        
        [mLookupManager loadLookupTable];
    }
}



- (IBAction)keepLoginPressed:(id)sender {
    if ([self.btnKeepLogin isSelected])
        [self.btnKeepLogin setSelected: NO];
    else
        [self.btnKeepLogin setSelected: YES];
   
    
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        switch (self.popupTag)
        {
            case kPopupSelection_Country:
            {
                NSString *countryCode = [mSession convertDialCodeToCountryCode: selection.firstObject];
                [self countrySelected:countryCode];
            }
                break;
            case kPopupSelection_Language:
                {
                    NSString *languageSortCode = [mSession convertLanguageNameToSortCode: selection.firstObject];
                    NSInteger languageIndexCode = [[mSession convertLanguageNameToLanguageKeyCode: selection.firstObject] intValue];
                    
                    if ([kAvailableLanguages objectAtIndex: languageIndexCode])
                    {
//                        [self.btnLanguage setTitle: languageSortCode forState:UIControlStateNormal];
//                        [self.btnLanguage sizeToFit];
                        
                        
                        [self updateLanguageButtonWithSortCode: [languageSortCode uppercaseString]];
                        
                        //for testing only
//                        if ([languageSortCode isEqualToString: @"BH"])
//                        {
//                            [self updateLanguageButtonWithSortCode: @"FIL"];
//
//                            languageIndexCode = 13;
//
//                        }
//                        else
//                        {
//                            //ORIGINAL, remove above if-else later
//                            [self updateLanguageButtonWithSortCode: [languageSortCode uppercaseString]];
//
//                        }
                        
                        [[LocalizationManager sharedInstance] setLanguage: [kAvailableLanguages objectAtIndex: languageIndexCode]];
                        
                        //TODO: Change to Saudi or whatever id as Right To Left
                        if ([[kAvailableLanguages objectAtIndex: languageIndexCode] isEqualToString: kArabic] ||
                            [[kAvailableLanguages objectAtIndex: languageIndexCode] isEqualToString: kUrdu])
                        {
                            if (!(kIsRightToLeft)) {
                                [UIView.appearance setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
                                [mSession loadLoginView];
                                
                            }
                        } else {
                            if (kIsRightToLeft) {
                                [UIView.appearance setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
                                [mSession loadLoginView];
                            }
                        }
                        [mSession configureShortcutItems:NO];
                    }
                    
                }
                break;
            default:
                break;
        }
    }
}

-(void) updateLanguageButton
{
    NSLog(@"%@",kAvailableLanguages);
    NSLog(@"%@",@([kAvailableLanguages indexOfObject: GET_LOCALIZATION]).stringValue);
    
    //mSession's convertLanguageKeyCode don have keycode 5, the array only have keycode 5 when it is TH
    [self updateLanguageButtonWithSortCode: [mSession convertLanguageKeyCodeToSortCode:  @([kAvailableLanguages indexOfObject: GET_LOCALIZATION]).stringValue]];
}

-(void) updateLanguageButtonWithSortCode:(NSString *)sortCode
{
    NSInteger indexCode = [[mSession convertLanguageSortCodeToKeyCode: sortCode] intValue];
    if ([sortCode isEqualToString: [kThai uppercaseString]])
        [self.btnLanguage setTitle: @"ภาษาไทย" forState:UIControlStateNormal];
    else if (indexCode == [kAvailableLanguages indexOfObject: kChineseT_HK])
        [self.btnLanguage setTitle: @"中文" forState:UIControlStateNormal];
    else if (indexCode == [kAvailableLanguages indexOfObject: kFilipino])
        [self.btnLanguage setTitle: @"PH" forState:UIControlStateNormal];
    else
        [self.btnLanguage setTitle: sortCode  forState: UIControlStateNormal];
    [self.btnLanguage sizeToFit];
}

-(UIView *) setupPaddingView: (NSString *) imageName xOffset:(CGFloat) xOffset
{
    UIImageView *imgView = [[UIImageView alloc] initWithImage: [UIImage imageNamed:imageName]];
    imgView.frame = CGRectMake(xOffset, 0, imgView.frame.size.width, imgView.frame.size.height);
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, imgView.frame.size.width + imgView.frame.origin.x, imgView.frame.size.height)];
    [paddingView addSubview:imgView];
    
    return paddingView;
}

-(void) countrySelected:(NSString *) countryCode {
    
    NSString *countryName = [mSession convertCountryCodeToCountryName:countryCode];
    [self.btnCountryCode setTitle: countryName forState:UIControlStateNormal];
    
    SET_COUNTRY_CODE(countryCode);
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if ([GET_COUNTRY_CODE isEqual: COUNTRYCODE_INDONESIA]) {
      self.lblRegister.alpha = 0;
    } else {
      self.lblRegister.alpha = 1;
    }
    
    [CrashlyticsKit setObjectValue: GET_COUNTRY_CODE forKey: @"Country Code"]; //identifier as country code
    
    [mSession setDefaultLanguage];
    
    //TODO: Change to Saudi or whatever id as Right To Left
    if ([GET_LOCALIZATION isEqualToString: kArabic] ||
        [GET_LOCALIZATION isEqualToString: kUrdu])
    {
        if (!(kIsRightToLeft)) {
            [UIView.appearance setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
            [mSession loadLoginView];
            
        }
    } else {
        if (kIsRightToLeft) {
            [UIView.appearance setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
            [mSession loadLoginView];
        }
    }
    
    [mLookupManager reloadLookup];
    
    [self setUpInterface];

    [mSession updateAnalyticsUserProperty];
}

#pragma mark - Localization Methods
- (void) receiveLanguageChangedNotification:(NSNotification *) notification
{
    if ([notification.name isEqualToString:kNotificationLanguageChanged] ||
        [notification.name isEqualToString: LokaliseDidUpdateLocalizationNotification] )
    {
        [self setupLocalization];
        [mLookupManager reloadLookup];
        [[IQKeyboardManager sharedManager] setToolbarDoneBarButtonItemText: LOCALIZATION(C_FORM_DONE)];

        [mSession updateAnalyticsUserProperty];
    }
}

- (IBAction)languageSelectPressed:(id)sender {
    
    self.popupTag = kPopupSelection_Language;
 
    if ( [[mSession getLanguageList] count] > 0)
    {
        [self popUpListViewWithTitle: LOCALIZATION(C_DROPDOWN_LANGUAGE) listArray: [mSession getLanguageList]];
    }
    else
    {
        self.isReloadingDropdownList = YES;
        NSString *languageCodeString  = [NSString stringWithFormat: @"%lu", (unsigned long)[kAvailableLanguages indexOfObject: GET_LOCALIZATION]];
        NSDictionary *requestData = @{@"CountryCode": GET_COUNTRY_CODE,
                                      @"LanguageCode": languageCodeString,
                                      @"LookUpVersion": [mSession.lookupTable objectForKey: @"LookUpVersion"] ? [mSession.lookupTable objectForKey: @"LookUpVersion"] : @"1501065350"};
        
        [[WebServiceManager sharedInstance] requestLookupTableInVC: requestData
                                                                vc:self];
    }
}

#pragma mark - WebServiceManagerDelegate Methods
- (void)processCompleted:(WebServiceResponse *)response
{
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_LOGIN:
        {
            NSLog(@"response -- %@",[response getJSON]);
            
            NSDictionary *userData = [response getGenericResponse];
            
            LoginInfo *loginInfo = [[LoginInfo alloc]initWithLoginInfo];
            
            loginInfo.tradeID = [userData objectForKey: @"TradeID"];
            loginInfo.programID = [userData objectForKey: @"ProgramID"];
            loginInfo.countryCode = [userData objectForKey: @"CountryCode"];
            loginInfo.companyType = [userData objectForKey: @"CompanyType"];
            loginInfo.companyCategory = [userData objectForKey: @"CompanyCategory"];
            
            if ([[userData objectForKey: @"CompanyType"] intValue] == 4096)
                SET_ISIMPROFILETYPE(YES);
            else
                SET_ISIMPROFILETYPE(NO);
            SET_COUNTRY_CODE(loginInfo.countryCode);
            SET_AUTHENTICATIONTOKEN([userData objectForKey: @"AuthToken"]);
            SET_PROFILETYPE([userData objectForKey: @"ProfileType"]);
            
            if([[userData objectForKey: @"IsAdvanced"] boolValue]) {
                SET_ISADVANCE(YES);
            } else {
                SET_ISADVANCE(NO);
            }
            
            SET_KEEPLOGIN(@(self.btnKeepLogin.selected));
            SET_LOGIN_NAME(self.txtMobNum.text);
            [[NSUserDefaults standardUserDefaults] synchronize];
//            [mSession reloadLookupTable: self.view];   //reload due to change in country code
            
            switch (GET_PROFILETYPE)
            {
                case kPROFILETYPE_DSR:
                    [CrashlyticsKit setObjectValue: @"DSR" forKey: @"Profile Type"]; //username as usertype
                    break;
                case kPROFILETYPE_TRADEOWNER:
                    [CrashlyticsKit setObjectValue: @"Trade Owner" forKey: @"Profile Type"]; //username as usertype
                    break;
                case kPROFILETYPE_MECHANIC:
                    [CrashlyticsKit setObjectValue: @"Mechanic" forKey: @"Profile Type"]; //username as usertype
                    break;
                case kPROFILETYPE_FORECOURTATTENDANT:
                    [CrashlyticsKit setObjectValue: @"FA" forKey: @"Profile Type"]; //username as usertype
                    break;
                default:
                    [CrashlyticsKit setObjectValue: @"unknown user" forKey: @"Profile Type"]; //username as usertype
                    break;
            }
            
            [CrashlyticsKit setObjectValue: GET_COUNTRY_CODE forKey: @"Country Code"]; //identifier as country code
            [CrashlyticsKit setObjectValue: [userData objectForKey: @"CompanyType"] forKey: @"Workshop Type"];
            [CrashlyticsKit setObjectValue: [userData objectForKey: @"CompanyCategory"] forKey: @"Workshop Category"];
            
//            NSString *sortCode = [mSession convertLanguageKeyCodeToSortCode:  @([kAvailableLanguages indexOfObject: GET_LOCALIZATION]).stringValue];
            
//            NSDictionary *requestData = @{@"CountryCode": GET_COUNTRY_CODE,
//                                          @"LanguageCode": [mSession convertLanguageSortCodeToKeyCode: sortCode],
//                                          @"LookUpVersion": [[mSession lookupTable] objectForKey: @"LookUpVersion"]};
                   
            [mSession saveUser:loginInfo];
            [mSession configureShortcutItems:YES];
            
            [mLookupManager loadLookupTable];
            [UpdateHUD removeMBProgress:self.view];
            
            [mSession updateAnalyticsUserProperty];
            
            if (self.isReloadingDropdownList)
            {
                if (self.popupTag == kPopupSelection_Country)
                {
                    [self popUpListViewWithTitle: LOCALIZATION(C_DROPDOWN_COUNTRY) listArray: [mSession getCountryDialingCodes]]; //Lokalised 16 jan
                }
                else
                {
                    [self popUpListViewWithTitle: LOCALIZATION(C_DROPDOWN_LANGUAGE) listArray: [mSession getLanguageList]];//Lokalised 16 jan
                }
            }
            else
            {
                [mSession loadSplash: YES]; //reload lookup after login
                [mSession performSelector: @selector(loadHomeView) withObject: nil afterDelay: 4];
            }
            
        }
            break;
        case kWEBSERVICE_LOOKUPTABLE:
            {
                NSDictionary *lookupDict = [response getGenericResponse];
                
                if ([[lookupDict objectForKey: @"LoadData"] boolValue])
                {
                    SET_LOOKUP_TABLE(lookupDict);
                    mSession.lookupTable = lookupDict;
                    SET_LANGUAGE_TABLE([mSession.lookupTable objectForKey: @"LanguageList"]);
                    mSession.languageTable = [mSession.lookupTable objectForKey: @"LanguageList"];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName: @"Reloaded Lookup Table" object: self];
                }
                
                if (self.isReloadingDropdownList)
                {
                    if (self.popupTag == kPopupSelection_Country)
                    {
                        [self popUpListViewWithTitle: LOCALIZATION(C_DROPDOWN_COUNTRY) listArray: [mSession getCountryDialingCodes]]; //Lokalised 16 jan
                    }
                    else
                    {
    //                    self.popupTag = kPopupSelection_Language;
                        [self popUpListViewWithTitle: LOCALIZATION(C_DROPDOWN_LANGUAGE) listArray: [mSession getLanguageList]];//Lokalised 16 jan
                    }
                }
                else
                {
                    [mSession loadSplash: YES]; //reload lookup after login
                    [mSession performSelector: @selector(loadHomeView) withObject: nil afterDelay: 4];
                }
            }
            break;
        default:
            break;
    }
    self.isReloadingDropdownList = NO;
}

-(void) processFailed:(WebServiceResponse *)response
{
    if (response.webserviceCall == kWEBSERVICE_LOOKUPTABLE)
    {
        if (self.isReloadingDropdownList)
        {
            if (self.popupTag == kPopupSelection_Country)
            {
                //count check to be here or at button press?
                //reload lookup only when empty?
                if ([[mSession getCountryDialingCodes] count] > 0)
                    [self popUpListViewWithTitle: LOCALIZATION(C_DROPDOWN_COUNTRY) listArray: [mSession getCountryDialingCodes]]; //Lokalised 16 jan
            }
            else
            {
//                    self.popupTag = kPobpupSelection_Language;
                if ([[mSession getLanguageList] count] > 0)
                    [self popUpListViewWithTitle: LOCALIZATION(C_DROPDOWN_LANGUAGE) listArray: [mSession getLanguageList]]; //Lokalised 16 jan
            }
        }
    }
    
    self.isReloadingDropdownList = NO;
}

@end
