//
//  LoginViewController.h
//  Syngenta
//
//  Created by Will Choy on 6/10/15.
//  Copyright © 2015 WillChoy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Session.h"
#import "BaseVC.h"

@interface LoginViewController : BaseVC

@end
