//
//  WorkingHoursViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 14/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"

@protocol WorkingHoursViewDelegate <NSObject>
-(void) didFinishWorkingHoursSelection: (NSDictionary *) workingHoursDict;
@end

@interface WorkingHoursViewController : BaseVC
@property id<WorkingHoursViewDelegate> delegate;
@property NSDictionary *workingHoursDict;
@end
