//
//  WorkingHoursViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 14/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "WorkingHoursViewController.h"

enum kDay_Type
{
    kDay_Monday     = 1,
    kDay_Tuesday    = 1 << 1,
    kDay_Wednesday  = 1 << 2,
    kDay_Thursday   = 1 << 3,
    kDay_Friday     = 1 << 4,
    kDay_Saturday   = 1 << 5,
    kDay_Sunday     = 1 << 6,
};


@interface WorkingHoursViewController () <UIPickerViewDelegate, UIPickerViewDataSource>
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *timeBtnArray;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *fullDayBtnArray;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *halfDayBtnArray;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labelArray;

@property (weak, nonatomic) IBOutlet UIButton *btnFullDayStartTimeHour;
@property (weak, nonatomic) IBOutlet UIButton *btnFullDayStartTimeMin;
@property (weak, nonatomic) IBOutlet UIButton *btnFullDayEndTimeHour;
@property (weak, nonatomic) IBOutlet UIButton *btnFullDayEndTimeMin;

@property (weak, nonatomic) IBOutlet UIButton *btnHalfDayStartTimeHour;
@property (weak, nonatomic) IBOutlet UIButton *btnHalfDayStartTimeMin;
@property (weak, nonatomic) IBOutlet UIButton *btnHalfDayEndTimeHour;
@property (weak, nonatomic) IBOutlet UIButton *btnHalfDayEndTimeMin;

@property (weak, nonatomic) IBOutlet UILabel *lblFullDayHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblHalfDayHeader;

@property (weak, nonatomic) IBOutlet UILabel *lblPubHoliday;
@property (weak, nonatomic) IBOutlet UIButton *btnPubHoliday;

@property (weak, nonatomic) IBOutlet UIButton *btnDone;
@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UIView *pickerView;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;

@property (weak, nonatomic) IBOutlet UIButton *btnMon;
@property (weak, nonatomic) IBOutlet UIButton *btnTue;
@property (weak, nonatomic) IBOutlet UIButton *btnWed;
@property (weak, nonatomic) IBOutlet UIButton *btnThu;
@property (weak, nonatomic) IBOutlet UIButton *btnFri;
@property (weak, nonatomic) IBOutlet UIButton *btnSat;
@property (weak, nonatomic) IBOutlet UIButton *btnSun;

@property (weak, nonatomic) IBOutlet UIButton *btnMonHalf;
@property (weak, nonatomic) IBOutlet UIButton *btnTueHalf;
@property (weak, nonatomic) IBOutlet UIButton *btnWedHalf;
@property (weak, nonatomic) IBOutlet UIButton *btnThuHalf;
@property (weak, nonatomic) IBOutlet UIButton *btnFriHalf;
@property (weak, nonatomic) IBOutlet UIButton *btnSatHalf;
@property (weak, nonatomic) IBOutlet UIButton *btnSunHalf;

@property (weak, nonatomic) IBOutlet UILabel *lblStartTimeHalf;
@property (weak, nonatomic) IBOutlet UILabel *lblEndTimeHalf;
@property (weak, nonatomic) IBOutlet UILabel *lblStartTime;
@property (weak, nonatomic) IBOutlet UILabel *lblEndTime;

@property (weak, nonatomic) IBOutlet UIView *halfDayView;
@property (weak, nonatomic) IBOutlet UIButton *btnPickerDone;

@property NSMutableArray *hoursArray;
@property NSMutableArray *minutesArray;
@end

@implementation WorkingHoursViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //default value
    if (!self.workingHoursDict)
        self.workingHoursDict = @{@"WorkHourStart1":   @"0:00",
                                  @"WorkHourEnd1":     @"0:00",
                                  @"WeekDays1":        @"",
                                  @"PublicHoliday":    @"0",
                                  @"WorkHourStart2":   @"0:00",
                                  @"WorkHourEnd2":     @"0:00",
                                  @"WeekDays2":        @"",
                                  };
    [self setupInterface];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    
    [FIRAnalytics setScreenName: LOCALIZATION_EN(C_TITLE_WORKINGHOURS) screenClass: nil];
}


-(void) setupInterface
{
    [Helper setNavigationBarTitle:self title: LOCALIZATION(C_TITLE_WORKINGHOURS) subtitle: @""];  //lokalise 30 jan
    
    if (!self.navigationItem.leftBarButtonItem)
    {
//        [self.backBtn setTintColor: COLOUR_RED];
//        [self.backBtn.titleLabel setFont: FONT_BUTTON];
//        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
        
        [self setNavBackBtn];
    }
    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_TITLE_WORKINGHOURS),  LOCALIZATION(C_REWARDS_BACK)];
    
    [self.btnMon setTitle:LOCALIZATION(C_MON)  forState:UIControlStateNormal];  //lokalise 30 jan
    [self.btnTue setTitle:LOCALIZATION(C_TUE)  forState:UIControlStateNormal]; //lokalise 30 jan
    [self.btnWed setTitle:LOCALIZATION(C_WED)  forState:UIControlStateNormal]; //lokalise 30 jan
    [self.btnThu setTitle:LOCALIZATION(C_THU)  forState:UIControlStateNormal]; //lokalise 30 jan
    [self.btnFri setTitle:LOCALIZATION(C_FRI)  forState:UIControlStateNormal]; //lokalise 30 jan
    [self.btnSat setTitle:LOCALIZATION(C_SAT)  forState:UIControlStateNormal]; //lokalise 30 jan
    [self.btnSun setTitle:LOCALIZATION(C_SUN)  forState:UIControlStateNormal]; //lokalise 30 jan
    
    [self.btnMonHalf setTitle:LOCALIZATION(C_MON)  forState:UIControlStateNormal]; //lokalise 30 jan
    [self.btnTueHalf setTitle:LOCALIZATION(C_TUE)  forState:UIControlStateNormal]; //lokalise 30 jan
    [self.btnWedHalf setTitle:LOCALIZATION(C_WED)  forState:UIControlStateNormal]; //lokalise 30 jan
    [self.btnThuHalf setTitle:LOCALIZATION(C_THU)  forState:UIControlStateNormal]; //lokalise 30 jan
    [self.btnFriHalf setTitle:LOCALIZATION(C_FRI)  forState:UIControlStateNormal]; //lokalise 30 jan
    [self.btnSatHalf setTitle:LOCALIZATION(C_SAT)  forState:UIControlStateNormal]; //lokalise 30 jan
    [self.btnSunHalf setTitle:LOCALIZATION(C_SUN)  forState:UIControlStateNormal]; //lokalise 30 jan
    
    self.lblFullDayHeader.text = LOCALIZATION(C_FULLDAY); //lokalise 30 jan
    self.lblHalfDayHeader.text = LOCALIZATION(C_HALFDAY); //lokalise 30 jan
    self.lblStartTime.text = LOCALIZATION(C_STARTTIME); //lokalise 30 jan
    self.lblStartTimeHalf.text = LOCALIZATION(C_STARTTIME); //lokalise 30 jan
    self.lblEndTime.text = LOCALIZATION(C_ENDTIME); //lokalise 30 jan
    self.lblEndTimeHalf.text = LOCALIZATION(C_ENDTIME); //lokalise 30 jan
    
    self.pickerView.tag = 1; //to initialise picker datasource
    
    for (int i = 0; i < [self.timeBtnArray count]; i++)
    {
        [[self.timeBtnArray objectAtIndex: i] setTag: i];
    }
    
    self.hoursArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < 24; i++)
    {
        [self.hoursArray addObject: @(i).stringValue];
    }
    
    self.minutesArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < 60; i++)
    {
        [self.minutesArray addObject: @(i).stringValue];
    }
    
    
    //headers
    [self.lblFullDayHeader setFont: FONT_H1];
    [self.lblFullDayHeader setTextColor: COLOUR_VERYDARKGREY];
    [self.lblHalfDayHeader setFont: FONT_H1];
    [self.lblHalfDayHeader setTextColor: COLOUR_VERYDARKGREY];
    
    
    //buttons and textfields with collection outlsets
    for (UIButton *btn in self.timeBtnArray)
    {
        [btn setTintColor: COLOUR_LIGHTGREY];
        [btn.titleLabel setFont: FONT_B1];
    }
    
    //full day
    NSArray *fullDayStartTime;
    if ([[self.workingHoursDict objectForKey: @"WorkHourStart1"] isKindOfClass: [NSNull class]])
    {
        [self.btnFullDayStartTimeHour setTitle: @"00" forState:UIControlStateNormal];
        [self.btnFullDayStartTimeMin setTitle: @"00" forState:UIControlStateNormal];
    }
    else
    {
        fullDayStartTime = [[self.workingHoursDict objectForKey: @"WorkHourStart1"] componentsSeparatedByString: @":"];
        [self.btnFullDayStartTimeHour setTitle: [fullDayStartTime objectAtIndex: 0] forState:UIControlStateNormal];
        [self.btnFullDayStartTimeMin setTitle: [fullDayStartTime objectAtIndex: 1] forState:UIControlStateNormal];
    }
    
    NSArray *fullDayEndTime;
    if ([[self.workingHoursDict objectForKey: @"WorkHourEnd1"] isKindOfClass: [NSNull class]])
    {
        [self.btnFullDayEndTimeHour setTitle: @"23" forState:UIControlStateNormal];
        [self.btnFullDayEndTimeMin setTitle: @"59" forState:UIControlStateNormal];
    }
    else
    {
        fullDayEndTime = [[self.workingHoursDict objectForKey: @"WorkHourEnd1"] componentsSeparatedByString: @":"];
        [self.btnFullDayEndTimeHour setTitle:[fullDayEndTime objectAtIndex:0] forState:UIControlStateNormal];
        [self.btnFullDayEndTimeMin setTitle:[fullDayEndTime objectAtIndex: 1] forState:UIControlStateNormal];
    }
    
    
    //half day
    NSArray *halfDayStartTime;
    if ([[self.workingHoursDict objectForKey: @"WorkHourStart2"] isKindOfClass: [NSNull class]])
    {
        [self.btnHalfDayStartTimeHour setTitle: @"00" forState:UIControlStateNormal];
        [self.btnHalfDayStartTimeMin setTitle: @"00" forState:UIControlStateNormal];
    }
    else
    {
        halfDayStartTime = [[self.workingHoursDict objectForKey: @"WorkHourStart2"] componentsSeparatedByString: @":"];
        [self.btnHalfDayStartTimeHour setTitle:[halfDayStartTime objectAtIndex:0] forState:UIControlStateNormal];
        [self.btnHalfDayStartTimeMin setTitle:[halfDayStartTime objectAtIndex:1] forState:UIControlStateNormal];
    }
        
        
    NSArray *halfDayEndTime;
    if ([[self.workingHoursDict objectForKey: @"WorkHourEnd2"] isKindOfClass: [NSNull class]])
    {
        [self.btnHalfDayEndTimeHour setTitle: @"00" forState:UIControlStateNormal];
        [self.btnHalfDayEndTimeMin setTitle: @"00" forState:UIControlStateNormal];
    }
    else
    {
        halfDayEndTime = [[self.workingHoursDict objectForKey: @"WorkHourEnd2"] componentsSeparatedByString: @":"];
        [self.btnHalfDayEndTimeHour setTitle:[halfDayEndTime objectAtIndex:0] forState:UIControlStateNormal];
        [self.btnHalfDayEndTimeMin setTitle:[halfDayEndTime objectAtIndex:1] forState:UIControlStateNormal];
    }
    
    
    NSString *fullDaySelectedBitString;
    NSArray *fullDaySelectedArray = [[NSMutableArray alloc] init];
    if ([[self.workingHoursDict objectForKey: @"WeekDays1"] isKindOfClass: [NSNull class]])
    {
        fullDaySelectedBitString = @"";
    }
    else
    {
        fullDaySelectedBitString = [self.workingHoursDict objectForKey: @"WeekDays1"];
        fullDaySelectedArray = [fullDaySelectedBitString componentsSeparatedByString: @","];
    }
    
    
    NSString *halfDaySelectedBitString = [self.workingHoursDict objectForKey: @"WeekDays2"];
    NSArray *halfDaySelectedArray = [[NSMutableArray alloc] init];
    if ([[self.workingHoursDict objectForKey: @"WeekDays2"] isKindOfClass: [NSNull class]])
    {
        halfDaySelectedBitString = @"";
    }
    else
    {
        halfDaySelectedBitString = [self.workingHoursDict objectForKey: @"WeekDays2"];
        halfDaySelectedArray = [halfDaySelectedBitString componentsSeparatedByString: @","];
    }
    
    for (int i = 0; i < 7; i++)
    {
        [[self.fullDayBtnArray objectAtIndex: i] setTag: 1 << i];
        [[self.halfDayBtnArray objectAtIndex: i] setTag: 1 << i];
        [[self.fullDayBtnArray objectAtIndex: i] setFont: FONT_B1];
        [[self.halfDayBtnArray objectAtIndex: i] setFont: FONT_B1];
        
        for (NSString *bitValue in fullDaySelectedArray)
        {
            if (bitValue.integerValue == [[self.fullDayBtnArray objectAtIndex: i] tag])
                [self daySelected:[self.fullDayBtnArray objectAtIndex: i]];
        }
        for (NSString *bitValue in halfDaySelectedArray)
        {
            if (bitValue.integerValue == [[self.halfDayBtnArray objectAtIndex: i] tag])
                [self daySelected:[self.halfDayBtnArray objectAtIndex: i]];
        }
    }
    
    for (UILabel *lbl in self.labelArray)
    {
        [lbl setFont: FONT_B1];
    }
    
    self.lblPubHoliday.text = LOCALIZATION(C_FORM_PUBLICHOLIDAYS);  //lokalise 30 jan
    if ([[self.workingHoursDict objectForKey: @"PublicHoliday"] isKindOfClass: [NSNull class]])
        self.btnPubHoliday.selected = NO;
    else
        self.btnPubHoliday.selected = [[self.workingHoursDict objectForKey: @"PublicHoliday"] intValue];
    
    //misc.
    [self.btnDone.titleLabel setFont: FONT_BUTTON];
    [self.btnDone setBackgroundColor:COLOUR_RED];
    [self.btnDone setTitle:LOCALIZATION(C_FORM_SUBMIT) forState:UIControlStateNormal];  //lokalise 30 jan
    [Helper setCustomFontButtonContentModes: self.btnDone];
    
    [self.btnPickerDone setTitle: LOCALIZATION(C_FORM_DONE) forState: UIControlStateNormal];  //lokalise 30 jan
    
    if ([GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA])
    {
        self.halfDayView.hidden = YES;
    }
}

- (IBAction)hoursPressed:(UIButton *)sender {
    
    for (UIButton *btn in self.timeBtnArray)
    {
        if (btn != sender)
        {
            [btn setBackgroundColor: COLOUR_WHITE];
            [btn setTitleColor: COLOUR_DARKGREY forState: UIControlStateNormal];
        }
        else
        {
            [sender setBackgroundColor: COLOUR_RED];
            [sender setTitleColor: COLOUR_WHITE forState: UIControlStateNormal];
        }
    }
    self.pickerView.tag = 1;
    self.picker.tag = sender.tag;
    [self.picker selectedRowInComponent: 0];
    [self.picker reloadAllComponents];
    self.pickerView.hidden = NO;
}

- (IBAction)minutesPressed:(UIButton *)sender {
    for (UIButton *btn in self.timeBtnArray)
    {
        if (btn != sender)
        {
            [btn setBackgroundColor: COLOUR_WHITE];
            [btn setTitleColor: COLOUR_DARKGREY forState: UIControlStateNormal];
        }
        else
        {
            [sender setBackgroundColor: COLOUR_RED];
            [sender setTitleColor: COLOUR_WHITE forState: UIControlStateNormal];
        }
    }
    self.pickerView.tag = 2;
    self.picker.tag = sender.tag;
    [self.picker selectedRowInComponent: 0];
    [self.picker reloadAllComponents];
    self.pickerView.hidden = NO;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (self.pickerView.tag == 1)
        return [self.hoursArray objectAtIndex: row];
    else
        return [self.minutesArray objectAtIndex: row];
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (self.pickerView.tag == 1)
        return [self.hoursArray count];
    else
        return [self.minutesArray count];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    for (UIButton *btn in self.timeBtnArray)
    {
        if (btn.tag == self.picker.tag)
        {
            if (self.pickerView.tag == 1)
                [btn setTitle: [self.hoursArray objectAtIndex: row] forState: UIControlStateNormal];
            else
                [btn setTitle: [self.minutesArray objectAtIndex: row] forState: UIControlStateNormal];
        }
    }
}

- (IBAction)pickerDonePressed:(id)sender {
    
    for (UIButton *btn in self.timeBtnArray)
    {
        [btn setBackgroundColor: COLOUR_WHITE];
        [btn setTitleColor: COLOUR_DARKGREY forState: UIControlStateNormal];
    }
    [self.picker selectedRowInComponent: 0];
    self.pickerView.hidden = YES;
}

//hide picker view if touched outside of picker view
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event  {
    UITouch *touch = [touches anyObject];
    if(touch.view != self.pickerView){
        [self pickerDonePressed:nil];
    }
}

- (IBAction)submitPressed:(id)sender {
    
    NSString *fullDayWorkingDays = @"";
    for (UIButton *btn in self.fullDayBtnArray)
    {
        if ([btn isSelected])
        {
            if (fullDayWorkingDays.length == 0)
                fullDayWorkingDays = @(btn.tag).stringValue;
            else
                fullDayWorkingDays = [fullDayWorkingDays stringByAppendingString: [NSString stringWithFormat: @",%@", @(btn.tag).stringValue]];
        }
    }
    
    NSString *halfDayWorkingDays = @"";
    for (UIButton *btn in self.halfDayBtnArray)
    {
        if ([btn isSelected])
        {
            if (halfDayWorkingDays.length == 0)
                halfDayWorkingDays = @(btn.tag).stringValue;
            else
                halfDayWorkingDays = [halfDayWorkingDays stringByAppendingString: [NSString stringWithFormat: @",%@", @(btn.tag).stringValue]];
        }
    }
    
    NSString * fullDayStartTime = [NSString stringWithFormat: @"%@:%@", self.btnFullDayStartTimeHour.titleLabel.text, self.btnFullDayStartTimeMin.titleLabel.text];
    NSString * fullDayEndTime = [NSString stringWithFormat: @"%@:%@", self.btnFullDayEndTimeHour.titleLabel.text, self.btnFullDayEndTimeMin.titleLabel.text];
    NSString * halfDayStartTime = [NSString stringWithFormat: @"%@:%@", self.btnHalfDayStartTimeHour.titleLabel.text, self.btnHalfDayStartTimeMin.titleLabel.text];
    NSString * halfDayEndTime = [NSString stringWithFormat: @"%@:%@", self.btnHalfDayEndTimeHour.titleLabel.text, self.btnHalfDayEndTimeMin.titleLabel.text];
    
    NSDictionary *workingHoursDict = @{@"WorkHourStart1":   fullDayStartTime,
                                       @"WorkHourEnd1":     fullDayEndTime,
                                       @"WeekDays1":        fullDayWorkingDays,
                                       @"PublicHoliday":    @(self.btnPubHoliday.isSelected),
                                       @"WorkHourStart2":   halfDayStartTime,
                                       @"WorkHourEnd2":     halfDayEndTime,
                                       @"WeekDays2":        halfDayWorkingDays
                                       };
    
    [self.delegate didFinishWorkingHoursSelection: workingHoursDict];
    [self popSelf];
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

- (IBAction)daySelected:(UIButton *)sender {
    
    if ([sender isSelected])
    {
        [sender setBackgroundColor: COLOUR_WHITE];
        [sender setTintColor: COLOUR_LIGHTGREY];
        
        sender.selected = NO;
    }
    else
    {
        [sender setBackgroundColor: COLOUR_RED];
        [sender setTintColor: COLOUR_WHITE];
        sender.selected = YES;
        
        //check if counterpart is
        if ([self.fullDayBtnArray containsObject: sender])
        {
            //deselect half day if current button is going to be selected
            for (UIButton *btn in self.halfDayBtnArray)
            {
                if (btn.tag == sender.tag)
                {
                    [btn setBackgroundColor: COLOUR_WHITE];
                    [btn setTintColor: COLOUR_LIGHTGREY];
                    btn.selected = NO;
                    return; //finish function once done.
                }
            }
        }
        if ([self.halfDayBtnArray containsObject: sender])
        {
            //deselect full day if current button is going to be selected
            for (UIButton *btn in self.fullDayBtnArray)
            {
                if (btn.tag == sender.tag)
                {
                    [btn setBackgroundColor: COLOUR_WHITE];
                    [btn setTintColor: COLOUR_LIGHTGREY];
                    btn.selected = NO;
                    return; //finish function once done.
                }
            }
        }
    }
}
- (IBAction)checkboxSelected:(UIButton *)sender {
    if ([sender isSelected])
        sender.selected = NO;
    else
        sender.selected = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
