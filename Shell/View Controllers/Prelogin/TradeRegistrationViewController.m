//
//  TradeRegistrationViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 18/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "CASCommonListView.h"
#import "CASDatePickerView.h"

#import "Member.h"
#import "CompanyInfo.h"
#import "CompanyAddress.h"
#import "TradeRegistrationViewController.h"
#import "QRCodeScanner.h"

#define CELL_HEIGHT 50

enum kTradeRegForm_TYPE
{
    kTradeRegForm_SALUTATION,
    kTradeRegForm_FIRSTNAME,
    kTradeRegForm_LASTNAME,
    kTradeRegForm_MOBNUM,
    kTradeRegForm_EMAIL,
    kTradeRegForm_DOB,
    kTradeRegForm_DSRSCAN,
    kTradeRegForm_PASSWORD,
    kTradeRegForm_CONFIRMPASS,
};

#define kUserID             @"UserID"
#define kDSRCode            @"DSRCode"
#define kUserPassword       @"UserPassword"
#define kSalutation         @"Salutation"
#define kFirstName          @"FirstName"
#define kLastName           @"LastName"
#define kMobileNumber       @"MobileNumber"
#define kEmailAddress       @"EmailAddress"
#define kDOB                @"DOB"
#define kDOBMonth           @"DOBMonth"
#define kDOBDay             @"DOBDay"
#define kCountryCode        @"CountryCode"
#define kPassword           @"Password"
#define kConfirmPassword    @"ConfirmPassword"

@interface TradeRegistrationViewController () <QRCodeScannerDelegate, UITextFieldDelegate, WebServiceManagerDelegate, CommonListDelegate, DatePickerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource>

//IBOutlet Collection for fonts
@property (weak, nonatomic) IBOutlet UILabel *lblProfileHeader;
@property (weak, nonatomic) IBOutlet UILabel *contactPrefLbl;

@property Member *member;

@property (strong, nonatomic) IBOutlet UIButton *backBtn;

#pragma mark My Profile


@property (weak, nonatomic) IBOutlet UIButton *btnRegister;

@property (weak, nonatomic) IBOutlet UICollectionView *colView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *colViewHeight; //to adjust according to number of cells <1 + (count / 2)>
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *colViewLayout; //to adjust cell width size according to screen width
//@property NSInteger contactPreferenceBitValue;
@property (strong, nonatomic) NSMutableArray *contactPrefArray;

@property CASDatePickerView *dateView;

@property NSMutableArray *registrationFormArray; //ui only
@property NSMutableDictionary *regSubmissionDict;  //key as json key, value = cell. retrieve value by using getJson method

@property (weak, nonatomic) IBOutlet UITableView *regFormTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight; //to adjust according number of cells

@end

@implementation TradeRegistrationViewController
@synthesize listView; //must synthesize list view constraints to work
@synthesize dateView; //must synthesize date view constraints to work
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //QRCode Delegate
    mQRCodeScanner.delegate = self;
    
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_REGISTRATION) subtitle: @""];     //lokalised
    if (!self.navigationItem.leftBarButtonItem)
    {
        [self.backBtn setTintColor: COLOUR_RED];
        [self.backBtn.titleLabel setFont: FONT_BUTTON];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    }
//    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_TITLE_REGISTRATION),  LOCALIZATION(C_REWARDS_BACK)];
    
    if (!self.contactPrefArray)
        self.contactPrefArray = [[NSMutableArray alloc] init];
    
    self.regSubmissionDict = [[NSMutableDictionary alloc] init];
    [self.btnRegister setTitle:LOCALIZATION(C_REGISTER_CONTINUE) forState:UIControlStateNormal];        //lokalised
    [self setupRegistrationForm];
    
    [self.colView registerNib: [UINib nibWithNibName: @"ContactPreferencesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ContactPrefCell"];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    [self setupInterface];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    [self setupGoogleAnalytics];
}
-(void) setupGoogleAnalytics
{
    [FIRAnalytics setScreenName: LOCALIZATION_EN(C_TITLE_REGISTRATION) screenClass: nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setupRegistrationForm
{
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormTextfieldCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormTextfieldCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationFormDropdownCell" bundle:nil] forCellReuseIdentifier:@"RegistrationFormDropdownCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationTextfieldWithButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationTextfieldWithButtonCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationMobileNumberCell" bundle:nil] forCellReuseIdentifier:@"RegistrationMobileNumberCell"];
    [self.regFormTableView registerNib:[UINib nibWithNibName:@"RegistrationButtonCell" bundle:nil] forCellReuseIdentifier:@"RegistrationButtonCell"];
    
    self.regSubmissionDict = [[NSMutableDictionary alloc] init];
    
    self.registrationFormArray = [[NSMutableArray alloc] initWithArray:
                                  @[@{@"Title": LOCALIZATION(C_PROFILE_SALUTATION),     //lokalised
                                    @"Type": @(kREGISTRATION_DROPDOWN),
                                    @"Key": kSalutation,
                                    @"Value": LOCALIZATION(C_PROFILE_SALUTATION_PLACEHOLDER),   //lokalised
                                     },  //button
                                   @{@"Title": LOCALIZATION(C_PROFILE_FIRSTNAME),           //lokalised
                                     @"Type": @(kREGISTRATION_TEXTFIELD),
                                     @"Key": kFirstName,
                                     @"Value": @"",
                                     }, //first name
                                   @{@"Title": LOCALIZATION(C_PROFILE_LASTNAME),            //lokalised
                                     @"Type": @(kREGISTRATION_TEXTFIELD),
                                     @"Key": kLastName,
                                     @"Value": @"",
                                     }, //last name
                                  @{@"Title": LOCALIZATION(C_PROFILE_MOBILENUM),            //lokalised
                                    @"Type": @(kREGISTRATION_MOBNUM),
                                    @"Key": kMobileNumber,
                                    @"CountryCode": [mSession convertCountryCodeToCountryName: self.countryCode],
                                    @"MobileNumber": self.mobNum,
                                    @"Enabled": @(NO),
                                    },   //Mobile number
                                  @{@"Title": LOCALIZATION(C_PROFILE_OWNEREMAILADDRESSOPT),     //lokalised
                                    @"Type": @(kREGISTRATION_TEXTFIELD),
                                    @"Key": kEmailAddress,
                                    @"Keyboard": @(UIKeyboardTypeEmailAddress),
                                    @"Value": @"",
                                    }, //email
                                  @{@"Title": LOCALIZATION(C_PROFILE_DOB),                  //lokalised
                                    @"Type": @(kREGISTRATION_BIRTHDATE),
                                    @"Key": kDOB,
                                    @"Value": LOCALIZATION(C_PROFILE_DOB_PLACEHOLDER),      //lokalised
                                    },   //Date of birth
                                  @{@"Title": LOCALIZATION(C_PROFILE_DSRCODE),              //loklaised
                                    @"Key": kDSRCode,
                                    @"Icon": @"icon-code",
                                    @"Type": @(kREGISTRATION_TEXTFIELDWITHBUTTON),
                                    @"Value": @"",
                                    },   //DSR Code
                                  @{@"Title": LOCALIZATION(C_PROFILE_CREATEPASS),           //lokalised
                                    @"Key": kPassword,
                                    @"Type": @(kREGISTRATION_TEXTFIELD),
                                    @"SecureEntry": @(YES),
                                    @"Value": @"",
                                    }, //create password
                                  @{@"Title": LOCALIZATION(C_PROFILE_CONFPASS),             //lokalised
                                    @"Key": kConfirmPassword,
                                    @"Type": @(kREGISTRATION_TEXTFIELD),
                                    @"SecureEntry": @(YES),
                                    @"Value": @"",
                                    }, //email
                                  ]
                                  ];
    
    //set tableview height.
    self.tableViewHeight.constant = CELL_HEIGHT * [self.registrationFormArray count];
    self.regFormTableView.tableFooterView = [UIView new];
    [self.regFormTableView reloadData];
}

- (IBAction)backPressed:(id)sender {
    [self popToIndex: [[self.navigationController childViewControllers] count] - 3];
}



-(void) setupInterface
{
    self.member = [[Member alloc] init];
    
    //preload listviews
    listView = [[CASCommonListView alloc] initWithTitle: LOCALIZATION(C_DROPDOWN_SALUTATION)            //lokalised
                                                   list: [mSession getSalutations]
                                          selectionType:ListSelectionTypeSingle previouslySelected: nil];
    listView.delegate = self;
    
    dateView = [[CASDatePickerView alloc] initWithTitle:LOCALIZATION(C_DROPDOWN_BIRTHDAY) previouslySelected:nil withYearHidden:YES]; //lokalised
    
    dateView.delegate = self;
    
    //headers
    [self.lblProfileHeader setFont: FONT_H1];
    [self.lblProfileHeader setTextColor: COLOUR_VERYDARKGREY];
    self.lblProfileHeader.text =  LOCALIZATION(C_HEADER_MYPROFILE);     //lokalised
    [self.contactPrefLbl setFont: FONT_B1];
    self.contactPrefLbl.text = LOCALIZATION(C_PROFILE_CONTACT_PREFERENCE);  //lokalised
    
    //misc.
    [self.btnRegister.titleLabel setFont: FONT_BUTTON];
    [Helper setCustomFontButtonContentModes: self.btnRegister];
    
    //handle check boxes collection view
    CGFloat colviewHeight = (COLVIEW_CELLHEIGHT + COLVIEW_LINESPACING) * (1 + ([[mSession getContactPreference] count] / 2));
    self.colViewHeight.constant = colviewHeight;
    
    CGFloat cellWidth = (self.colView.frame.size.width / 2) - 10; //10 = cell spacing, check IB
    self.colViewLayout.itemSize = CGSizeMake(cellWidth, 50);
    
    if(kIsRightToLeft) {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
    } else {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
    }
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[mSession getContactPreference] count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ContactPreferencesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"ContactPrefCell" forIndexPath: indexPath];
    
    UIButton *checkboxBtn = [cell viewWithTag: 1]; //tagged in storyboard
    checkboxBtn.selected  = YES;
    
    UILabel *contactPref = [cell viewWithTag: 2]; //tagged in storyboard
    contactPref.text = [[mSession getContactPreference] objectAtIndex: [indexPath row]];
    contactPref.font = FONT_B1;
    
    cell.bitValue = [[mSession convertToContactPreferenceKeyCode: contactPref.text] intValue];
    
    if (!self.contactPrefArray)
        self.contactPrefArray = [[NSMutableArray alloc] init];
    [self.contactPrefArray addObject: cell];
    
    return cell;
}

-(void) popUpView
{
    [self.view endEditing: YES];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: listView];
    listView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[listView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (listView)]];
}
- (IBAction)btnScanDSRCode:(id)sender {
    [mQRCodeScanner showZBarScannerOnVC: self];
}

- (void)qrCodeScanSuccess:(NSString *)scanString
{
    RegistrationTextfieldWithButtonCell *cell = [self.regSubmissionDict objectForKey: kDSRCode];
    [cell setTextfieldText: [[scanString componentsSeparatedByString:@"="] lastObject]];

}

- (IBAction)salutationPressed:(UIButton *)sender {
    
    listView.tag = sender.tag;
    [listView setTitleAndList: LOCALIZATION(C_DROPDOWN_SALUTATION) list: [mSession getSalutations] hasSearchField: NO]; //lokalised
    
    [self popUpView];
}
//- (IBAction)genderPressed:(id)sender {
//    listView.tag = kPopupSelection_Gender;
//    [listView setTitleAndList: @"Select Gender" list: [mSession getGender]];
//    
//    [self popUpView];
//}


- (IBAction)registerPressed:(id)sender {
    if (![[[self.regSubmissionDict objectForKey: kPassword] getJsonValue] isEqualToString:
          [[self.regSubmissionDict objectForKey: kConfirmPassword] getJsonValue]])
    {
        //        [mAlert showErrorAlertWithMessage: LOCALIZATION(<#text#>)];
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_CHANGEPASSWORD_MISMATCH_ERRORMSG)];   //lokalised
        
        return;
    }
    self.member.registrationType     = @"2"; //"2" for non assisted trade registration
    
    self.member.firstName =   [[self.regSubmissionDict objectForKey: kFirstName] getJsonValue];
    self.member.lastName =    [[self.regSubmissionDict objectForKey: kLastName] getJsonValue];
    self.member.mobileNumber = self.mobNum;
    self.member.emailAddress = [[self.regSubmissionDict objectForKey: kEmailAddress] getJsonValue];

    self.member.countryCode  = self.countryCode;
    self.member.userID       = self.mobNum;
    self.member.DSRCode      = [[self.regSubmissionDict objectForKey: kDSRCode] getJsonValue];
    self.member.userPassword = [[self.regSubmissionDict objectForKey: kPassword] getJsonValue];
    
    self.member.salutation   = [[mSession convertToSalutationKeyCode:[[self.regSubmissionDict objectForKey: kSalutation] getJsonValue]] intValue];
    
//    self.member.gender               = [mSession convertToGenderKeyCode:self.btnGender.titleLabel.text];
    self.member.gender               = @"0";    //not capturing
    
    NSString *contactPrefString = @"";
    for (ContactPreferencesCollectionViewCell* cell in self.contactPrefArray)
    {
        if ([cell.checkboxBtn isSelected])
        {
            if (contactPrefString.length == 0)
                contactPrefString = @(cell.bitValue).stringValue;
            else
                contactPrefString = [contactPrefString stringByAppendingString: [NSString stringWithFormat: @",%@", @(cell.bitValue).stringValue]];
        }
    }
    self.member.contactPreference    = contactPrefString;
    
    self.member.registrationMode     = @"1"; //should be defaulted depending on registration method
    self.member.registrationRole    = @"1";
    
    NSDictionary *registrationParams = @{COUNTRYCODE_KEY:        self.member.countryCode,
                                         @"REG_Member":          [self.member getJSON]
                                         };
    
    [[WebServiceManager sharedInstance] registerTradePreLoginOne:registrationParams
                                                              vc:self];
}


- (IBAction)birthdayPressed:(UIButton *)sender {
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview: dateView];
    dateView.translatesAutoresizingMaskIntoConstraints = NO;
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
    [window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateView]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings (dateView)]];
}

- (void)selectedDate:(NSDate *)selectedDate
{
    //do smething
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //   for API
    [formatter setDateFormat:@"dd"];
    self.member.dobDay = [formatter stringFromDate:selectedDate];
    
    [formatter setDateFormat:@"MM"];
    self.member.dobMonth = [formatter stringFromDate:selectedDate];
    
    if(kIsRightToLeft) {
         [formatter setDateFormat:@"MMMM"];
    } else {
         [formatter setDateFormat:@"dd MMMM"];
    }
    
    [formatter setLocale: [NSLocale localeWithLocaleIdentifier: GET_LOCALIZATION]];
    
    NSString *selectedDateString = [formatter stringFromDate:selectedDate];
    
    RegistrationFormDropdownCell *cell = [self.regSubmissionDict objectForKey: kDOB];
    if(kIsRightToLeft) {
         [cell setSelectionTitle: [NSString stringWithFormat:@"%@ %@",self.member.dobDay,selectedDateString]];
    } else {
         [cell setSelectionTitle: selectedDateString];
    }
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        RegistrationFormDropdownCell *cell;
        switch (listView.tag)
        {
            case kTradeRegForm_SALUTATION:
                cell = [self.regSubmissionDict objectForKey: kSalutation];
                break;
//            case kPopupSelection_Country:
//                [self.btnCountryCode setTitle:selection.firstObject forState:UIControlStateNormal];
//                break;
        }
        [cell.button setTitle:selection.firstObject forState:UIControlStateNormal];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}



#pragma mark - tableview delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.registrationFormArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *regFieldData = [self.registrationFormArray objectAtIndex: indexPath.row];
    switch ([[regFieldData objectForKey: @"Type"] intValue])
    {
        case kREGISTRATION_TEXTFIELD:
        {
            RegistrationFormTextfieldCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormTextfieldCell"];
            
            if ([regFieldData objectForKey: @"Keyboard"])
                [textfieldCell setKeyboard: [[regFieldData objectForKey: @"Keyboard"] intValue]];
            if ([regFieldData objectForKey: @"SecureEntry"])
                [textfieldCell setSecureTextEntry: [[regFieldData objectForKey: @"SecureEntry"] boolValue]];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            [textfieldCell hideTopLabel: YES];
            
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_DROPDOWN:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            [dropdownCell hideTopLabel: YES];
            dropdownCell.button.tag = indexPath.row;
            
            switch (indexPath.row) {
                case kTradeRegForm_SALUTATION:
                    [dropdownCell.button addTarget: self action: @selector(salutationPressed:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                default:
                    break;
            }
            //set initial value
            if ([regFieldData objectForKey: @"Value"])
                [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            
            if (kIsRightToLeft) {
                [dropdownCell setButtonContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
            } else {
                [dropdownCell setButtonContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            }
            
            //keep a reference of cell to submit json value later
            [self.regSubmissionDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        case kREGISTRATION_TEXTFIELDWITHBUTTON:
        {
            RegistrationTextfieldWithButtonCell *textfieldCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationTextfieldWithButtonCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [textfieldCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [textfieldCell setTitle: [regFieldData objectForKey: @"Title"]];
            [textfieldCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [textfieldCell hideTopLabel: YES];
            
            switch (indexPath.row)
            {
                case kTradeRegForm_DSRSCAN:
                    [textfieldCell.button addTarget:self action:@selector(btnScanDSRCode:) forControlEvents:UIControlEventTouchUpInside];
                    break;
            }
            
            [self.regSubmissionDict setObject: textfieldCell forKey: [regFieldData objectForKey: @"Key"]];
            return textfieldCell;
        }
            break;
        case kREGISTRATION_BUTTON:
        {
            RegistrationButtonCell *buttonCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationButtonCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [buttonCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [buttonCell setTitle: [regFieldData objectForKey: @"Title"]];
            [buttonCell setIconImage: [regFieldData objectForKey: @"Icon"]];
            [buttonCell hideTopLabel: YES];
            
            [self.regSubmissionDict setObject: buttonCell forKey: [regFieldData objectForKey: @"Key"]];
            return buttonCell;
        }
            break;
        case kREGISTRATION_MOBNUM:
        {
            RegistrationMobileNumberCell *mobNumCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationMobileNumberCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [mobNumCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            [mobNumCell setTitle: [regFieldData objectForKey: @"Title"]];
            [mobNumCell hideTopLabel: YES];
            
            [mobNumCell setCountryCode: [regFieldData objectForKey: @"CountryCode"]];
            [mobNumCell setMobileNumber: [regFieldData objectForKey: @"MobileNumber"]];
            
            [self.regSubmissionDict setObject: mobNumCell forKey: [regFieldData objectForKey: @"Key"]];
            return mobNumCell;
        }
            break;
        case kREGISTRATION_BIRTHDATE:
        {
            RegistrationFormDropdownCell *dropdownCell = [tableView dequeueReusableCellWithIdentifier: @"RegistrationFormDropdownCell"];
            if ([regFieldData objectForKey: @"Enabled"])
                [dropdownCell setUserInteractionEnabled: [[regFieldData objectForKey: @"Enabled"] boolValue]];
            //Salutation
            [dropdownCell setTitle: [regFieldData objectForKey: @"Title"]];
            [dropdownCell setSelectionTitle: [regFieldData objectForKey: @"Value"]];
            [dropdownCell hideTopLabel: YES];
            dropdownCell.button.tag = indexPath.row;
            
            if (kIsRightToLeft) {
                [dropdownCell setButtonContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
            } else {
                [dropdownCell setButtonContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            }
            
            [dropdownCell.button addTarget: self action: @selector(birthdayPressed:) forControlEvents:UIControlEventTouchUpInside];
            [self.regSubmissionDict setObject: dropdownCell forKey: [regFieldData objectForKey: @"Key"]];
            return dropdownCell;
        }
            break;
        default:
            NSLog(@"UNKNOWN CELL TYPE");
            return [UITableViewCell new];
            break;
    }
    return [UITableViewCell new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

-(void)processCompleted:(WebServiceResponse *)response
{
    [mSession pushTradeRegistrationTwoView:self memberInfo: self.member];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
