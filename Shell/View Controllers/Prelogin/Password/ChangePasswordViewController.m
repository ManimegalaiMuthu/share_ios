//
//  ChangePasswordViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 21/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "ChangePasswordViewController.h"

@interface ChangePasswordViewController () <WebServiceManagerDelegate>
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UITextField *txtOldPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_CHANGEPASS) subtitle: @""];       //lokalised
    
    if (self.showBackBtn)
    {
        [self.backBtn setTintColor: COLOUR_RED];
        [self.backBtn.titleLabel setFont: FONT_BUTTON];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
    }
    else
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: [UIView new]];
    }
//    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_TITLE_CHANGEPASS),  LOCALIZATION(C_REWARDS_BACK)];
    
    [self setupInterface];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];

    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_CHANGEPASS) screenClass:nil];
}


-(void) setupInterface
{

    self.txtOldPassword.font = FONT_B1;
    self.txtOldPassword.placeholder = LOCALIZATION(C_PLACEHOLDER_CURRENTPASSWORD);      //lokalised
    

    self.txtNewPassword.font = FONT_B1;
    self.txtNewPassword.placeholder = LOCALIZATION(C_PLACEHOLDER_NEWPASSWORD);          //lokalised

    
    self.txtConfirmPassword.font = FONT_B1;
    self.txtConfirmPassword.placeholder = LOCALIZATION(C_PLACEHOLDER_RE_NEWPASSWORD);   //lokalised

    
    self.btnSubmit.titleLabel.font = FONT_BUTTON;
    [self.btnSubmit setTitle:LOCALIZATION(C_FORM_SUBMIT) forState:UIControlStateNormal]; //loklaised
    [self.btnSubmit setBackgroundColor: COLOUR_RED];
    [Helper setCustomFontButtonContentModes: self.btnSubmit];
    
    if(kIsRightToLeft) {
        self.txtOldPassword.textAlignment = NSTextAlignmentRight;
        self.txtNewPassword.textAlignment = NSTextAlignmentRight;
        self.txtConfirmPassword.textAlignment = NSTextAlignmentRight;
    } else {
        self.txtOldPassword.textAlignment = NSTextAlignmentLeft;
        self.txtNewPassword.textAlignment = NSTextAlignmentLeft;
        self.txtConfirmPassword.textAlignment = NSTextAlignmentLeft;
    }
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
}

- (IBAction)submitPressed:(id)sender {
    if (![self.txtNewPassword.text isEqualToString: self.txtConfirmPassword.text])
    {
        [mAlert showErrorAlertWithMessage: LOCALIZATION(C_CHANGEPASSWORD_MISMATCH_ERRORMSG)];  //lokalised
        
        return;
    }
    
    
    [[NSUserDefaults standardUserDefaults]setObject: @(YES) forKey:@"ChangePassword"];
    [[WebServiceManager sharedInstance] changePasswordPostLogin:self.txtOldPassword.text
                                                    newPassword:self.txtNewPassword.text
                                                             vc:self];
}
-(void)processCompleted:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress:self.view];
    
    [mAlert showSuccessAlertWithMessage: [response getResponseMessage]
                           onCompletion:^(BOOL finished) {
                               
                               //popup msg
                               [self popSelf];
                           }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
