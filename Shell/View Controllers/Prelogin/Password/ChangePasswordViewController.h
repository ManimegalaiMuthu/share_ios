//
//  ChangePasswordViewController.h
//  Shell
//
//  Created by Ankita Chhikara on 21/8/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "BaseVC.h"

@interface ChangePasswordViewController : BaseVC
@property NSString* mobNum;
@property BOOL showBackBtn;
@end
