//
//  ForgetPasswordViewController.m
//  Shell
//
//  Created by Ankita Chhikara on 18/7/17.
//  Copyright © 2017 Edenred. All rights reserved.
//

#import "ForgetPasswordViewController.h"

#import "PopupViewController.h"

@interface ForgetPasswordViewController () <WebServiceManagerDelegate, PopupListDelegate>
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UITextField *txtMobNum;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnCountryCode;


@end

@implementation ForgetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.backBtn setTintColor: COLOUR_RED];
    [self.backBtn.titleLabel setFont: FONT_H1];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: self.backBtn];
//    self.backBtn.accessibilityLabel = [NSString stringWithFormat: @"%@ %@", LOCALIZATION(C_TITLE_FORGETPASS),  LOCALIZATION(C_REWARDS_BACK)];
    
    [self setupInterface];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveLanguageChangedNotification:)
                                                 name:kNotificationLanguageChanged
                                               object:nil];
    
    if (kIsRightToLeft) {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleR.png"] forState:UIControlStateNormal];
        [self.btnCountryCode setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    } else {
        [self.backBtn setImage:[UIImage imageNamed:@"icon-arrsingleL.png"] forState:UIControlStateNormal];
        [self.btnCountryCode setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    }
    
    [self.btnCountryCode setUserInteractionEnabled:NO];
}

#pragma mark - Localization Methods
- (void) receiveLanguageChangedNotification:(NSNotification *) notification
{
    if ([notification.name isEqualToString:kNotificationLanguageChanged])
    {
        [self setupInterface];
        [mLookupManager reloadLookup];
        [[IQKeyboardManager sharedManager] setToolbarDoneBarButtonItemText: LOCALIZATION(C_FORM_DONE)];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];

    [FIRAnalytics setScreenName:LOCALIZATION_EN(C_TITLE_FORGETPASS) screenClass:nil];
}

-(void) setupInterface
{
    if ([[GET_LOCALIZATION uppercaseString] isEqualToString: kIndo])
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_FORGETPASS) subtitle: @""];       //lokalise 11 Feb
    else
        [Helper setNavigationBarTitle:self title:LOCALIZATION(C_TITLE_FORGETPASS) subtitle: @"" size:15 subtitleSize:0];   //lokalise 11 Feb
    
    self.txtMobNum.placeholder = LOCALIZATION(C_PROFILE_MOBILENUM);         //lokalise 11 Feb
    self.txtMobNum.font = FONT_B1;
    if (kIsRightToLeft)
    {
        [self.txtMobNum setTextAlignment: NSTextAlignmentRight];
    }
    else
    {
        [self.txtMobNum setTextAlignment: NSTextAlignmentLeft];
    }
    
    self.btnCountryCode.titleLabel.font = self.txtMobNum.font;
    
    self.lblHeader.text = LOCALIZATION(C_LOGIN_FORGETHEADER);               //lokalise 11 Feb
    self.lblHeader.font = FONT_B1;
    
    self.btnSubmit.titleLabel.font = FONT_H1;
    [self.btnSubmit setTitle:LOCALIZATION(C_FORM_SUBMIT) forState:UIControlStateNormal];    //lokalised
    [self.btnSubmit setBackgroundColor:COLOUR_RED];
    [Helper setCustomFontButtonContentModes: self.btnSubmit];
    
    NSString *countryDisplayName = [mSession convertCountryCodeToCountryDisplayName:GET_COUNTRY_CODE];
    NSArray *countryCodeAry = [countryDisplayName componentsSeparatedByString: @"+"];
    
    NSString *countryCode = [NSString stringWithFormat: @"+%@", [countryCodeAry objectAtIndex: 1]];
    
    [self.btnCountryCode setTitle: countryCode forState: UIControlStateNormal];
}

- (IBAction)backPressed:(id)sender {
    [self popSelf];
    //[self showPopUp:YES];
}

- (IBAction)countryCodePressed:(id)sender {
    
    [self.view endEditing: YES];
    
    
    if ([[mSession getCountryDialingCodes] count] > 0)
    {
        [self popUpListViewWithTitle: LOCALIZATION(C_DROPDOWN_COUNTRY) listArray: [mSession getCountryDialingCodes]];   //lokalised
    }
    else
    {
        NSString *languageCodeString  = [NSString stringWithFormat: @"%lu", (unsigned long)[kAvailableLanguages indexOfObject: GET_LOCALIZATION]];
        NSDictionary *requestData = @{@"CountryCode": GET_COUNTRY_CODE,
                                      @"LanguageCode": languageCodeString,
                                      @"LookUpVersion": [mSession.lookupTable objectForKey: @"LookUpVersion"] ? [mSession.lookupTable objectForKey: @"LookUpVersion"] : @"1501065350"};
        
        [[WebServiceManager sharedInstance] requestLookupTableInVC: requestData
                                                                vc:self];
    }
}

-(void)dropDownSelection:(NSArray *)selection
{
    if (selection.count > 0) {
        NSString *countryName = [mSession convertDialCodeToCountryName: selection.firstObject];
        [self.btnCountryCode setTitle: countryName forState:UIControlStateNormal];
        
        SET_COUNTRY_CODE([mSession convertDialCodeToCountryCode: selection.firstObject]);
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [CrashlyticsKit setObjectValue: GET_COUNTRY_CODE forKey: @"Country Code"]; //identifier as country code
        
    
        [mSession setDefaultLanguage];
        
        [mLookupManager reloadLookup];
    }
}
- (IBAction)pressedSubmit:(id)sender {
    
}

- (IBAction)submitPressed:(id)sender {
//    if (![self.txtNewPassword.text isEqualToString: self.txtConfirmPassword.text])
//    {
//        //        [mAlert showErrorAlertWithMessage: LOCALIZATION(<#text#>)];
//        [mAlert showErrorAlertWithMessage: LOCALIZATION(@"Passwords do not match")];
//        
//        return;
//    }
    
    [[WebServiceManager sharedInstance] forgetPassword:self.txtMobNum.text
                                           countryCode: GET_COUNTRY_CODE
                                                    vc:self];
}

-(void)processCompleted:(WebServiceResponse *)response
{
    [UpdateHUD removeMBProgress:self.view];
 
    switch (response.webserviceCall)
    {
        case kWEBSERVICE_FORGOTPASSWORD:
        {
            [mAlert showSuccessAlertWithMessage: [response getResponseMessage]
                                   onCompletion:^(BOOL finished) {
                                       //popup msg
                                       self.view.userInteractionEnabled = NO;
                                       [self performSelector:@selector(backPressed:) withObject:nil afterDelay:0.5f];
                                   }];
        }
            break;
        case kWEBSERVICE_LOOKUPTABLE:
        {
            NSDictionary *lookupDict = [response getGenericResponse];
            
            if ([[lookupDict objectForKey: @"LoadData"] boolValue])
            {
                SET_LOOKUP_TABLE(lookupDict);
                mSession.lookupTable = lookupDict;
                SET_LANGUAGE_TABLE([mSession.lookupTable objectForKey: @"LanguageList"]);
                mSession.languageTable = [mSession.lookupTable objectForKey: @"LanguageList"];
                
                [[NSNotificationCenter defaultCenter] postNotificationName: @"Reloaded Lookup Table" object: self];
            }
            [self popUpListViewWithTitle: LOCALIZATION(C_DROPDOWN_COUNTRY) listArray: [mSession getCountryDialingCodes]];   //lokalised
        }
            break;
        default:
            break;
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
