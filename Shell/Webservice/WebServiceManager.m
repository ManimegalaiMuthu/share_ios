//
//  WebserviceManager.m
//  JTIStarPartners
//
//  Created by Shekhar on 20/11/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import "WebServiceManager.h"
#import "AFNetworking.h"

#import "UpdateHUD.h"
#import "AppDelegate.h"
#import "Session.h"
#import "LocalizationManager.h"

//Datamodels headers
#import "LoginViewController.h"
#import "CompanyInfo.h"
#import "CompanyAddress.h"
#import "Member.h"

#import <Crashlytics/Crashlytics.h>

static NSString *kForbiddenAccess = @"Request failed: forbidden (403)";

    @implementation WebServiceManager

@synthesize delegate=_delegate;
@synthesize requestURL;
@synthesize requestHeaders;
@synthesize requestParams;
@synthesize mutableRequest;
@synthesize serviceReference;
@synthesize imageUploadPath;
@synthesize imageInfoDict;
@synthesize imageData;
@synthesize requestBody;
@synthesize methodType;
@synthesize manager;

+ (WebServiceManager *)sharedInstance {
    
    static WebServiceManager *sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [[WebServiceManager alloc] init];
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    });
    return sharedInstance;
}

+ (NSString *) serverNoApi_Prefix
{
    if (IS_DEVELOPMENT_VERSION)
    {
        return DEVELOPMENT_SERVERNOAPI_PREFIX;
    }
    else if (IS_BETA_VERSION)
    {
        return BETA_SERVERNOAPI_PREFIX;
    }
    else if (IS_STAGING_VERSION)
    {
        return STAGING_SERVERNOAPI_PREFIX;
    }
    else    //live
    {
        return LIVE_SERVERNOAPI_PREFIX;
    }
}

+ (NSString *) secret_token
{
    if (IS_DEVELOPMENT_VERSION)
    {
        return STAGING_SECRET_TOKEN_VALUE;
    }
    else if (IS_BETA_VERSION)
    {
        if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
            return BETA_SECRET_TOKEN_VALUE;
        } else {
            return LIVE_SECRET_TOKEN_VALUE;
        }
    }
    else if (IS_STAGING_VERSION)
    {
        return STAGING_SECRET_TOKEN_VALUE;
    }
    else    //live
    {
        return LIVE_SECRET_TOKEN_VALUE;
    }
}

- (id) init {
    if (self = [super init])
    {
        manager = [AFHTTPSessionManager manager];
        return self;
    }
    return nil;
}

#pragma mark API connectors
//- (void)executeURLRequest
//{
//    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:mutableRequest];
//    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
//     {
//         [UpdateHUD removeMBProgress:self.activeView];
//         //[self successRequest:responseObject request:request withServiceIndentifier:self.serviceReference];
//         
//     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//         
//         
//         //[self failedRequest:error request:request withServiceIndentifier:self.serviceReference];
//         
//         NSLog(@"%@", [error description]);
//     }];
//    
//    [operation start];
//}

#pragma mark - Download Image AFNetwork API
//- (void)downloadImage
//{
//    AFHTTPRequestOperation *postOperation = [[AFHTTPRequestOperation alloc] initWithRequest:self.mutableRequest];
//    postOperation.responseSerializer = [AFImageResponseSerializer serializer];
//    [postOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
//     {
//         //[self successRequest:responseObject request:request withServiceIndentifier:self.serviceReference];
//
//     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
//     {
//         //[self failedRequest:error request:request withServiceIndentifier:self.serviceReference];
//     }];
//
//    [postOperation start];
//}

#pragma mark - Upload Image AFNetwork API
- (void)doRequestWithDataUpload:(WebServiceRequest *)request
{
    //    AFSecurityPolicy *securityPolicy = [[AFSecurityPolicy alloc] init];
    AFSecurityPolicy* securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    [securityPolicy setValidatesDomainName:NO];
    [securityPolicy setAllowInvalidCertificates:YES];
    
    
    if (request.vwHUD != nil) {
        
        if ([request.msgHUD isEqualToString:SPLASHSCREEN_HUD]) {
            [UpdateHUD addSpotifyHUDSplash:request.vwHUD];
        }
        else [UpdateHUD addMBProgress:request.vwHUD withText:request.msgHUD];
        
    }

    manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.requestSerializer.timeoutInterval = 180;
    [request.requestHeaders enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        [manager.requestSerializer setValue:object forHTTPHeaderField:key];
    }];
    [manager setSecurityPolicy:securityPolicy];
    
    if (request.requestMethod == POST)
    {
        [manager POST:request.requestURL parameters:request.requestParams constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            if (request.requestFormDataParams) {
                for (FormDataItem *item in request.requestFormDataParams) {
                    [formData appendPartWithFormData:item.data
                                                name:item.key];
                }
            }
            if (request.requestDataFiles) {
                int i = 1;
                for (NSData *dataFile in request.requestDataFiles) {
                    [formData appendPartWithFileData:dataFile name:[NSString stringWithFormat:@"Attachment%i",i] fileName:[NSString stringWithFormat:@"attachment%i.jpg",i] mimeType:@"image/jpeg"];
                    i++;
                }
            }
        }
             progress:nil
              success:^(NSURLSessionTask *operation, id responseObject) {
                  
                  if (request.vwHUD != nil) {
                      if ([request.msgHUD isEqualToString:SPLASHSCREEN_HUD]) {
                          [UpdateHUD removeSpotifyHUD:request.vwHUD];
                      }
                      else [UpdateHUD removeMBProgress:request.vwHUD];
                  }
                  [self successRequest:responseObject request:request withServiceIndentifier:[NSNumber numberWithInt:request.webserviceCall]];
              }
              failure:^(NSURLSessionTask *operation, NSError *error) {
                  if (request.vwHUD != nil) {
                      if ([request.msgHUD isEqualToString:SPLASHSCREEN_HUD]) {
                          [UpdateHUD removeSpotifyHUD:request.vwHUD];
                      }
                      else [UpdateHUD removeMBProgress:request.vwHUD];                  }
                  [self failedRequest:error request:request withServiceIndentifier:[NSNumber numberWithInt:request.webserviceCall]];
              }];
    }
    else if (request.requestMethod == GET)
    {
        [manager GET:request.requestURL parameters:self.requestHeaders progress:nil success:^(NSURLSessionTask *operation, id responseObject) {
            [self successRequest:responseObject request:request withServiceIndentifier:[NSNumber numberWithInt:request.webserviceCall]];
        }
             failure:^(NSURLSessionTask *operation, NSError *error)
         {
             [self failedRequest:error request:request withServiceIndentifier:[NSNumber numberWithInt:request.webserviceCall]];
         }];
    }
}

- (void)cancelCurrentOperations
{
    //AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

// or just cancel all of them!
    [manager.operationQueue cancelAllOperations];

    [UpdateHUD removeSpotifyHUD:CURRENT_VIEW];
    [UpdateHUD removeMBProgress:CURRENT_VIEW];
}

#pragma mark - Response Methods

- (void)successRequest:(id)responseObj request:(WebServiceRequest*)request withServiceIndentifier:(NSNumber *)reference
{
    if (IS_STAGING_VERSION)
    NSLog(@"Success%@",reference);
    [self processSucceeded:responseObj :reference :request];
}


- (void) failedRequest:(NSError *)error request:(WebServiceRequest*)request withServiceIndentifier:(NSNumber *)reference
{
    if (IS_STAGING_VERSION)
        NSLog(@"Failure");
    [self processFail:error :reference :request];
}

- (NSString *)urlEncodeUsingEncoding:(NSString *)str {
    return [str stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
}


#pragma mark - Prelogin Functions

//- (void)login:(LoginInfo *)loginInfo andView:(UIView *)view withDelegate:(id <WebServiceManagerDelegate> )delegate
- (void)login:(LoginInfo *)loginInfo vc:(UIViewController <WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PRELOGIN_PREFIX
                                               apiURL:PRELOGIN_LOGIN_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepRequestHeader]
              requestParameters:[WebserviceHelper prepLoginParams:loginInfo]
                             vc:vc
                 webServiceCall:kWEBSERVICE_LOGIN
                     msgHUDText:SIGNIN_HUD];
}


//Registration Step 1
- (void)validateMobileNumber: (NSString *)mobNum
                 countryCode:(NSString*)countryCode
                          vc: (UIViewController<WebServiceManagerDelegate> *)vc
{
    
    NSString *sortCode = [mSession convertLanguageKeyCodeToSortCode:  @([kAvailableLanguages indexOfObject: GET_LOCALIZATION]).stringValue];
    
    NSDictionary *mobileAuthParams = @{@"CountryCode": countryCode,
                                       @"LanguageCode":
                                           [mSession convertLanguageSortCodeToKeyCode: sortCode],
                                       @"MobileNumber": mobNum};
    
    NSString *requestURLString = [self setURLToString:PRELOGIN_PREFIX
                                               apiURL:PRELOGIN_MOBILENUM_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepRequestHeader]
              requestParameters:mobileAuthParams
                             vc:vc
                 webServiceCall:kWEBSERVICE_MOBILEAUTH
                     msgHUDText:LOADING_HUD];
}

- (void)validateOTP:(NSString*)otp
        countryCode:(NSString*)otpCountryCode
             mobNum:(NSString *)otpMobNum
                 vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *sortCode = [mSession convertLanguageKeyCodeToSortCode:  @([kAvailableLanguages indexOfObject: GET_LOCALIZATION]).stringValue];
    
    NSDictionary* otpParams = @{@"CountryCode": otpCountryCode,
                                @"LanguageCode":
                                    [mSession convertLanguageSortCodeToKeyCode: sortCode], //change to localization getter
                                @"MobileNumber": otpMobNum,
                                @"OTP": otp};
    NSString *requestURLString = [self setURLToString:PRELOGIN_PREFIX
                                               apiURL:PRELOGIN_VALIDATEOTP_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepRequestHeader]
              requestParameters:otpParams
                             vc:vc
                 webServiceCall:kWEBSERVICE_REGISTEROTP
                     msgHUDText:LOADING_HUD];
}

//update language settings for post notifications
- (void)updateLanguage:(NSString *)languageCode vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PRELOGIN_PREFIX
                                               apiURL:PRELOGIN_UPDATELANGUAGE_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"LanguageCode": languageCode,}
                             vc:vc
                 webServiceCall:kWEBSERVICE_UPDATELANGUAGE
                     msgHUDText:LOADING_HUD];
}

#pragma mark Registration API
//REGISTRATION STEPS METHOD NAMING MAY BE DIFFERENT FROM API
//METHODS ARE NAMED TO ACTUAL STEPS THE SYSTEM FLOW
//API NAMES DO NOT REPRESENT THE STEPS
//DO NOT CHANGE METHOD NAMING ORDER
- (void)registerTradePreLoginOne: (NSDictionary *)registerInfo
                           vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PRELOGIN_PREFIX
                                               apiURL:PRELOGIN_TRADEREGISTRATION2_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepRequestHeader]
              requestParameters:registerInfo
                             vc:vc
                 webServiceCall:kWEBSERVICE_TRADEREGISTER
                     msgHUDText:LOADING_HUD];
}

- (void)registerTradePreLoginTwo: (NSDictionary *)registerInfo
                              vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PRELOGIN_PREFIX
                                               apiURL:PRELOGIN_TRADEREGISTRATION_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepRequestHeader]
              requestParameters:registerInfo
                             vc:vc
                 webServiceCall:kWEBSERVICE_TRADEREGISTERTWO
                     msgHUDText:REGISTRATION_HUD];
}

- (void)registerMechanic: (NSDictionary *)registerInfo
                      vc: (UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PRELOGIN_PREFIX
                                               apiURL:PRELOGIN_MECHANICREGISTRATION_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepRequestHeader]
              requestParameters:registerInfo
                             vc:vc
                 webServiceCall:kWEBSERVICE_MECHREGISTER
                     msgHUDText:LOADING_HUD];
}


- (void)forgetPassword: (NSString *)mobNum
           countryCode:(NSString*)countryCode
                    vc: (UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PRELOGIN_PREFIX
                                               apiURL:PRELOGIN_FORGETPASSWORD_URL];
    NSString *sortCode = [mSession convertLanguageKeyCodeToSortCode:  @([kAvailableLanguages indexOfObject: GET_LOCALIZATION]).stringValue];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepRequestHeader]
              requestParameters: @{@"CountryCode": countryCode,
                                   @"LanguageCode": [mSession convertLanguageSortCodeToKeyCode: sortCode],
                                   @"MobileNumber": mobNum,
                                   }
                             vc:vc
                 webServiceCall:kWEBSERVICE_FORGOTPASSWORD
                     msgHUDText:SENDING_HUD];
}

- (void)changePassword: (NSString *)mobNum
           countryCode:(NSString*)countryCode
              password:(NSString*)password
                    vc: (UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PRELOGIN_PREFIX
                                               apiURL:PRELOGIN_CHANGEPASSWORD_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepRequestHeader]
              requestParameters: @{@"CountryCode": countryCode,
                                   @"MobileNumber": mobNum,
                                   @"UserPassword": password,
                                   }
                             vc:vc
                 webServiceCall:kWEBSERVICE_CHANGEPASSWORD
                     msgHUDText:SENDING_HUD];
}

#pragma mark Contact US
-(void) sendContactForm:(NSDictionary* ) contactFormData
                     vc: (UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PRELOGIN_PREFIX
                                               apiURL:PRELOGIN_CONTACTUS_URL];
    
    NSDictionary *headerData;
    if ([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK]) //use pre-login header
        headerData = [WebserviceHelper prepRequestHeader];
    else
        headerData = [WebserviceHelper prepPostLoginHeader];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:headerData
              requestParameters:contactFormData
                             vc:vc
                 webServiceCall:kWEBSERVICE_CONTACTUS
                     msgHUDText:LOADING_HUD];
}

#pragma mark Get look up table
-(void) requestLookupTable:(NSDictionary* ) lookupInfo
                      view:(UIView<WebServiceManagerDelegate> *)view
{
#warning to do - package lookup info to send
    NSString *requestURLString = [self setURLToString:PRELOGIN_PREFIX
                                               apiURL:PRELOGIN_LOOKUP_URL];
    
    NSMutableDictionary *headerData = [NSMutableDictionary dictionaryWithDictionary:[WebserviceHelper prepRequestHeader]];
    if ([[mSession loadUserProfile] tradeID] != nil) {
        [headerData setValue:[[mSession loadUserProfile] tradeID] forKey:TRADEID_KEY];
    }
    

    if (!view)
    {
        //lookup in background
        [self sendRequestWithParamsInSession: requestURLString
                              requestHeaders: headerData
                           requestParameters: lookupInfo
                                    delegate: mLookupManager
                              webServiceCall: kWEBSERVICE_LOOKUPTABLE
                                  msgHUDText: LOADING_HUD];
    }
    else
    {
        [self sendRequestWithParamsInView: requestURLString
                           requestHeaders:headerData
                        requestParameters:lookupInfo
                                     view:view
                           webServiceCall:kWEBSERVICE_LOOKUPTABLE
                               msgHUDText:LOADING_HUD];
    }
}
-(void) requestLookupTableInVC:(NSDictionary* ) lookupInfo
                      vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PRELOGIN_PREFIX
                                               apiURL:PRELOGIN_LOOKUP_URL];
    NSMutableDictionary *headerData = [NSMutableDictionary dictionaryWithDictionary:[WebserviceHelper prepRequestHeader]];
    if ([[mSession loadUserProfile] tradeID] != nil) {
        [headerData setValue:[[mSession loadUserProfile] tradeID] forKey:TRADEID_KEY];
    }
    
    [self sendRequestWithParams: requestURLString
                 requestHeaders:headerData
              requestParameters:lookupInfo
                             vc:vc
                 webServiceCall:kWEBSERVICE_LOOKUPTABLE
                     msgHUDText:LOADING_HUD];
}

-(void) requestConsumerTNC:(NSString *) versionNumber
                      view:(UIView<WebServiceManagerDelegate> *)view
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_LOADTNCCONSUMER_RU];

    //lookup in background
    [self sendRequestWithParamsInSession: requestURLString
                          requestHeaders: [WebserviceHelper prepPostLoginHeader]
                       requestParameters: @{@"Version": versionNumber,}
                                delegate: mSession
                          webServiceCall: kWEBSERVICE_LOOKUPTNC_CONSUMER_RU
                              msgHUDText:LOADING_HUD];
}

-(void) requestWorkshopTNC:(NSString *) versionNumber
                      view:(UIView<WebServiceManagerDelegate> *)view
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_LOADTNCWORKSHOP_RU];

    //lookup in background
    [self sendRequestWithParamsInSession: requestURLString
                          requestHeaders: [WebserviceHelper prepPostLoginHeader]
                       requestParameters: @{@"Version": versionNumber,}
                                delegate: mSession
                          webServiceCall: kWEBSERVICE_LOOKUPTNC_WORKSHOP_RU                                  msgHUDText:LOADING_HUD];
}

-(void) requestDSRTNC:(NSString *) versionNumber
                      view:(UIView<WebServiceManagerDelegate> *)view
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_LOADTNCDSR_RU];

    //lookup in background
    [self sendRequestWithParamsInSession: requestURLString
                          requestHeaders: [WebserviceHelper prepPostLoginHeader]
                       requestParameters: @{@"Version": versionNumber,}
                                delegate: mSession
                          webServiceCall: kWEBSERVICE_LOOKUPTNC_DSR_RU                                  msgHUDText:LOADING_HUD];
}


-(void) loadLanguage:(NSString *) lookupVersion
                view:(UIView<WebServiceManagerDelegate> *)view
{
    NSString *requestURLString = [self setURLToString:PRELOGIN_PREFIX
                                               apiURL:PRELOGIN_LOADLANGUAGE_URL];
    if (!view)
    {
        //lookup in background
        [self sendRequestWithParamsInSession: requestURLString
                              requestHeaders: [WebserviceHelper prepRequestHeader]
                           requestParameters: @{@"LookUpVersion" : lookupVersion}
                                    delegate: mSession
                              webServiceCall: kWEBSERVICE_LANGUAGE
                                  msgHUDText:LOADING_HUD];
    }
    else
    {
        [self sendRequestWithParamsInView: requestURLString
                           requestHeaders:[WebserviceHelper prepRequestHeader]
                        requestParameters: @{@"LookUpVersion" : lookupVersion}
                                     view:view
                           webServiceCall:kWEBSERVICE_LANGUAGE
                               msgHUDText:LOADING_HUD];
    }
}


-(void) loadState:(NSString *) lookupVersion
      countryCode:(NSString *) countryCode
               vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PRELOGIN_PREFIX
                                               apiURL:PRELOGIN_LOADSTATE_URL];
    
    NSDictionary *headerData;
    if ([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK]) //use pre-login header
        headerData = [WebserviceHelper prepRequestHeader];
    else
        headerData = [WebserviceHelper prepPostLoginHeader];
    
    [self sendRequestWithParams: requestURLString
                 requestHeaders: headerData
              requestParameters: @{@"LookUpVersion" : lookupVersion,
                                   COUNTRYCODE_KEY  : countryCode
                                   }
                             vc:vc
                 webServiceCall:kWEBSERVICE_STATE
                     msgHUDText:LOADING_HUD];
}

#pragma mark - Post Login Functions

#pragma mark Fetch Home Data List
- (void)fetchHome:(UIViewController<WebServiceManagerDelegate> *)vc
{
    //#warning to do - package registration dictionary to send
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHHOME_URL];
    
    NSLog(@"REQUEST URL%@",requestURLString);

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:nil
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHHOME
                     msgHUDText:LOADING_HUD];
}

- (void)fetchHomeSession
{
    //#warning to do - package registration dictionary to send
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHHOME_URL];
    
    [self sendRequestWithParamsInSession:requestURLString
                          requestHeaders:[WebserviceHelper prepPostLoginHeader]
                       requestParameters:nil
                                delegate: mSession
                          webServiceCall: kWEBSERVICE_FETCHHOME
                              msgHUDText:LOADING_HUD];
}

-(void) fetchHomePromotion:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHHOMEPROMOTION_URL];
    
   [self sendRequestWithParams:requestURLString
                requestHeaders:[WebserviceHelper prepPostLoginHeader]
             requestParameters:nil
                            vc:vc
                webServiceCall:kWEBSERVICE_FETCHHOMEPROMOTION
                    msgHUDText:LOADING_HUD];
}

-(void) fetchRewardsBannerForHome:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:REWARDS_PREFIX
                                               apiURL:REWARDS_FETCHREWARDSFORHOME_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:nil
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHREWARDSFORHOME
                     msgHUDText:LOADING_HUD];
}

-(void) fetchTradeSpeedometerDetails:(UIViewController<WebServiceManagerDelegate> *) vc {

  NSString *requestURLString = [self setURLToString:PROMOTION_PREFIX
                                             apiURL:PROMOTION_FETECHTRADESPEEDOMETERDETAILS];

  [self sendRequestWithParams:requestURLString
               requestHeaders:[WebserviceHelper prepPostLoginHeader]
            requestParameters:nil
                           vc:vc
               webServiceCall:kWEBSERVICE_FETECHTRADESPEEDOMETERDETAILS
                   msgHUDText:LOADING_HUD];

}

#pragma mark RegisteredWorkshop

- (void)fetchRegisteredWorkshopList:(NSArray *) registerWorkshopList
                     workShopname:(NSString *) workShopName
                              vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    //#warning to do - package registration dictionary to send
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_REGISTEREDWORKSHOPS_URL];

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"RegisteredWorkshop":       registerWorkshopList,
                                  @"WorkshopName":   workShopName
                                  }
                             vc:vc
                 webServiceCall:kWEBSERVICE_REGISTERWORKSHOPLIST
                     msgHUDText:LOADING_HUD];
}



#pragma mark FetchRegisteredWorkshops
- (void)fetchRegisteredPendingWorkshopList:(NSString *) keyword
              searchRange:(NSString *) searchRange
                       vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    //#warning to do - package registration dictionary to send
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHREGISTEREDWORKSHOPS_URL];

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"Keyword":       keyword,
                                  @"RangeSearch":   searchRange
                                  }
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHWORKSHOPPENDINGLIST
                     msgHUDText:LOADING_HUD];
}

- (void)fetchRegisteredApprovedWorkshopList:(NSString *) keyword
                     searchRange:(NSString *) searchRange
                              vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    //#warning to do - package registration dictionary to send
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHREGISTEREDWORKSHOPS_URL];

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"Keyword":       keyword,
                                  @"RangeSearch":   searchRange
                                  }
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHWORKSHOPAPPROVEDLIST
                     msgHUDText:LOADING_HUD];
}

- (void)fetchINRegisteredWorkshopList:(NSString *) keyword
              searchRange:(NSString *) searchRange
          profileTypeFilter: (NSString *) filterKeyCode
                       vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    //#warning to do - package registration dictionary to send
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHREGISTEREDWORKSHOPS_URL];

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"Keyword":       keyword,
                                  @"RangeSearch":   searchRange,
//                                  @"ProfileTypeFilter": filterKeyCode,
                                  }
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHWORKSHOPPENDINGLIST
                     msgHUDText:LOADING_HUD];
}











#pragma mark Fetch Workshop List
- (void)fetchPendingWorkshopList:(NSString *) keyword
              searchRange:(NSString *) searchRange
                       vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    //#warning to do - package registration dictionary to send
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHWORKSHOPPENDINGLISTV2_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"Keyword":       keyword,
                                  @"RangeSearch":   searchRange
                                  }
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHWORKSHOPPENDINGLIST
                     msgHUDText:LOADING_HUD];
}

- (void)fetchApprovedWorkshopList:(NSString *) keyword
                     searchRange:(NSString *) searchRange
                              vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    //#warning to do - package registration dictionary to send
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHWORKSHOPAPPROVEDLISTV2_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"Keyword":       keyword,
                                  @"RangeSearch":   searchRange
                                  }
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHWORKSHOPAPPROVEDLIST
                     msgHUDText:LOADING_HUD];
}

- (void)fetchINWorkshopList:(NSString *) keyword
              searchRange:(NSString *) searchRange
          profileTypeFilter: (NSString *) filterKeyCode
                       vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    //#warning to do - package registration dictionary to send
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHWORKSHOPPENDINGLISTV2_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"Keyword":       keyword,
                                  @"RangeSearch":   searchRange,
//                                  @"ProfileTypeFilter": filterKeyCode,
                                  }
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHWORKSHOPPENDINGLIST
                     msgHUDText:LOADING_HUD];
}

#pragma - add mechanic
- (void)addMechanic: (NSDictionary *)mechanicInfo
                 vc: (UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_ADDMECHANIC_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:mechanicInfo
                             vc:vc
                 webServiceCall:kWEBSERVICE_ADDMECHANIC
                     msgHUDText:LOADING_HUD];
}

- (void)fetchLeads:(NSString *) leadID
                vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    //#warning to do - package registration dictionary to send
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHLEADDEATILS_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"LeadID": leadID}
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHLEADS
                     msgHUDText:LOADING_HUD];
}

- (void)fetchTrade:(NSString *) tradeID
                vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    //#warning to do - package registration dictionary to send
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHTRADEDEATILS_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"TradeID": tradeID}
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHTRADE
                     msgHUDText:LOADING_HUD];
}


#pragma - Generate OTP

- (void)fetchcountryDial:(NSString*)countryDial
              mobileNo:(NSString*)mobNum
                    vc: (UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHPANBANKOTP_URL];
    NSLog(@"REQEST URL:%@",requestURLString);
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"CountryDial": countryDial,
                                   @"MobileNumber": mobNum,
                                   }
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHGENERATEPANOTP
                     msgHUDText:SENDING_HUD];
}

#pragma - Update OTP

- (void)fetchOtpUpdatecountryDial:(NSString*)countryDial
                         mobileNo:(NSString*)mobNum otp:(NSString*)otp
                    vc: (UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHPANBANKOTPUPDATE_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"CountryDial": countryDial,
                                   @"MobileNumber": mobNum,
                                   @"OTP":otp
                                   }
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHUPDATEPANOTP
                     msgHUDText:SENDING_HUD];
}

#pragma - Request OTP

- (void)fetchRequestOTPForMobile: (NSString *)mobNum
                 countryCode:(NSString*)countryCode
                          vc: (UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
    apiURL:TRADE_RESENDBANKOTP];
    
    NSDictionary *mobileAuthParams = @{@"CountryDial": countryCode,
                                       @"MobileNumber": mobNum};
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:mobileAuthParams
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHREQUESTBANKOTP
                     msgHUDText:LOADING_HUD];
}




#warning api for mechanic details
- (void)fetchMechanicDetail:(NSString *) tradeID
                vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    //#warning to do - package registration dictionary to send
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHMECHANICDETAIL_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"TradeID": tradeID}
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHMECHANIC
                     msgHUDText:LOADING_HUD];
}

- (void)fetchRegMechanicDetail:(NSString *) RegistrationID
                         vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    //#warning to do - package registration dictionary to send
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHREGMECHANICDETAIL_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"RegistrationID": RegistrationID}
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHREGMECHANIC
                     msgHUDText:LOADING_HUD];
}


- (void)updateMechanicRegistration:(NSDictionary *) registrationInfo
                            vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    //#warning to do - package registration dictionary to send
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_UPDATEMECHANICREGISTRATION_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: registrationInfo
                             vc:vc
                 webServiceCall:kWEBSERVICE_UPDATEREGMECHANIC
                     msgHUDText:LOADING_HUD];
}


- (void)updateMechanic:(NSDictionary *) registrationInfo
                    vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    //#warning to do - package registration dictionary to send
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_UPDATEMECHANIC_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: registrationInfo
                             vc:vc
                 webServiceCall:kWEBSERVICE_UPDATEMECHANIC
                     msgHUDText:LOADING_HUD];
}




- (void)addNewWorkshop: (NSDictionary *)workshopInfo
                    vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PRELOGIN_PREFIX
                                               apiURL:PRELOGIN_TRADEREGISTRATION_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:workshopInfo
                             vc:vc
                 webServiceCall:kWEBSERVICE_ADDNEWWORKSHOP
                     msgHUDText:REGISTRATION_HUD];
    
}

- (void)updateWorkshop: (NSDictionary *)workshopInfo
                    vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_UPDATEWORKSHOP_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:workshopInfo
                             vc:vc
                 webServiceCall:kWEBSERVICE_UPDATEWORKSHOP
                     msgHUDText:REGISTRATION_HUD];
}

- (void)uploadGalleryPhotos: (NSString *) tradeID
                 isApproved:(BOOL) isApproved
                  dataFiles: (NSArray *) dataFiles
                         vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setNoApiURLToString:PAGES_PREFIX
                                                    apiURL:PAGES_FILEUPLOAD_URL];
    
    [self sendRequestWithDataUpload:requestURLString
                     requestHeaders:[WebserviceHelper prepPostLoginHeader]
                  requestParameters:@{@"ID": tradeID,
                                      @"PhotoType": @(1), //only workshop can upload multiple images
                                      @"Approved": @(isApproved),
                                      }
                          dataFiles: dataFiles
                                 vc:vc
                     webServiceCall:kWEBSERVICE_UPLOADGALLERYPHOTOS
                         msgHUDText:REGISTRATION_HUD];
}

- (void)fetchTransactionHistory: (NSDictionary *)transactDict
                    vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_TRANSACTIONHISTORY_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:transactDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHTRANSACTION
                     msgHUDText:LOADING_HUD];
}

- (void)updateApprovedWorkshop: (NSDictionary *)workshopInfo
                            vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_UPDATETRADE_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:workshopInfo
                             vc:vc
                 webServiceCall:kWEBSERVICE_UPDATEWORKSHOP
                     msgHUDText:REGISTRATION_HUD];   
    
}

- (void)loadProfiling: (NSString *) idString
            isPending:(BOOL) isPending
                   vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_LOADPROFILING_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"ID": idString,
                                   @"Type": (isPending)? @"1": @"2",
                                   }
                             vc:vc
                 webServiceCall:kWEBSERVICE_LOADPROFILING
                     msgHUDText:LOADING_HUD];
}

#pragma mark Profile
- (void)updateTradeProfile: (NSDictionary *)tradeInfo
                        vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_UPDATETRADEPROFILE_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:tradeInfo
                             vc:vc
                 webServiceCall:kWEBSERVICE_UPDATETRADEPROFILE
                     msgHUDText:REGISTRATION_HUD];
}

- (void)updateBankDetails: (NSDictionary *)tradeInfo
                        vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_UPDATEBANKDETAILS_URL_V2];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:tradeInfo
                             vc:vc
                 webServiceCall:kWEBSERVICE_UPDATEBANKDETAILS
                     msgHUDText:REGISTRATION_HUD];
}

- (void)updateDocumentDetails: (NSDictionary *)tradeInfo
                      vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_UPDATEDOCUMENTDETAILS_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:tradeInfo
                             vc:vc
                 webServiceCall:kWEBSERVICE_UPDATEDOCUMENTDETAILS
                 msgHUDText:REGISTRATION_HUD];
}


#pragma mark Decal
- (void)tagDecalCodeToTradeOwner: (NSArray *)decalCodesArray
                              vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_DECALTRADEOWNER];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"decalCodes": decalCodesArray}
                             vc:vc
                 webServiceCall:kWEBSERVICE_TAGDECALTOTRADEOWNER
                     msgHUDText:LOADING_HUD];
}

- (void)searchVehicleWithDecal:(NSString *) decalCodeString
                            vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETTCHDECALSCAN];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"Codes": decalCodeString}
                             vc:vc
                 webServiceCall:kWEBSERVICE_SEARCHVEHICLEWITHDECAL
                     msgHUDText:LOADING_HUD];
}

- (void)tagVehicleWithDecalCode: (NSDictionary *)decalTagDict
                              vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_DECALTAGVEHICLE];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:decalTagDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_TAGVEHICLETODECAL
                     msgHUDText:LOADING_HUD];
}

#pragma mark Cash Incentive
- (void)fetchCashHistoryFromDate:(NSString*) fromDate
                          toDate:(NSString*) toDate
                       forStatus:(NSString*) status
                  forServiceType:(NSString*) serviceType
                              vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_CASHINCENTIVE_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"FromDate": fromDate,
                                  @"ToDate": toDate,
                                  @"ServiceType": serviceType,
                                  @"Status": status,
                                  }
                             vc:vc
                 webServiceCall:kWEBSERVICE_CASHINCENTIVE
                     msgHUDText:LOADING_HUD];
}

#pragma mark Vehicle
- (void)searchVehicle: (NSString *) keyword
                state: (NSString *) stateKeycode
                   vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_SEARCHVEHICLE_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"Keyword": keyword,
                                   @"StateID": stateKeycode,
                                   }
                             vc:vc
                 webServiceCall:kWEBSERVICE_SEARCHVEHICLE
                     msgHUDText:LOADING_HUD];
}

- (void)inviteConsumer: (NSDictionary *) consumerInfo
                   vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_INVITECONSUMER_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:consumerInfo
                             vc:vc
                 webServiceCall:kWEBSERVICE_INVITECONSUMER
                     msgHUDText:LOADING_HUD];
}
- (void)submitPromoCode:(NSString *)code
          vehicleNumber:(NSString *)vehicleNumber
                     vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_PROMOCODE_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"VehicleNumber": vehicleNumber,
                                   @"Code":code,}
                             vc:vc
                 webServiceCall:kWEBSERVICE_PROMOCODE
                     msgHUDText:LOADING_HUD];
}



- (void)submitCode: (NSDictionary *)submitData
                vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_SUBMITCODES_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:submitData
                             vc:vc
                 webServiceCall:kWEBSERVICE_SUBMITCODE
                     msgHUDText:LOADING_HUD];
    
}

- (void)submitCodesForSellout : (NSDictionary *) submitData
                           vc : (UIViewController<WebServiceManagerDelegate> *) vc {
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_SUBMITCODES_SELLOUT_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:submitData
                             vc:vc
                 webServiceCall:kWEBSERVICE_SUBMITCODE_SELLOUT
                     msgHUDText:LOADING_HUD];
}

//RU SubmitMember
- (void)submitMember: (NSDictionary *)submitData
                vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_SUBMITMEMBER];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:submitData
                             vc:vc
                 webServiceCall:kWEBSERVICE_SUBMITMEMBER
                     msgHUDText:LOADING_HUD];
    
}

- (void)fetchWaysToEarnPoints:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_WAYSTOEARN];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: nil
                             vc:vc
                 webServiceCall:kWEBSERVICE_WAYSTOEARN
                     msgHUDText:LOADING_HUD];
    
}

- (void) fetchShareProducts:(UIViewController<WebServiceManagerDelegate> *)vc {
  /*  NSString *requestURLString = ([[mSession profileInfo] isParticipatingProductsOpt2]) ?
                                            [self setURLToString:TRADE_PREFIX apiURL:TRADE_FETCHSHAREPRODUCTS] :
                                            [self setURLToString:TRADE_PREFIX apiURL:TRADE_FETCHSHAREPRODUCTS_V2];*/
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHSHAREPRODUCTS];
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        requestURLString = [self setURLToString:MSTRADE_PREFIX
                                         apiURL:TRADE_FETCHSHAREPRODUCTS];
    }

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: nil
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHSHAREPRODUCTS
                     msgHUDText:LOADING_HUD];
}

- (void) fetchAllParticipatingProducts:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = ([[mSession profileInfo] isParticipatingProductsOpt2]) ?
                                        [self setURLToString:TRADE_PREFIX apiURL:TRADE_FETCHPARTICIPATINGPRODUCTS] :
                                        [self setURLToString:TRADE_PREFIX apiURL:TRADE_FETCHPARTICIPATINGPRODUCTS_V2];
  //  NSString *requestURLString = [self setURLToString:TRADE_PREFIX apiURL:TRADE_FETCHPARTICIPATINGPRODUCTS];
    if(IS_COUNTRY(COUNTRYCODE_RUSSIA)) {
        requestURLString = [self setURLToString:MSTRADE_PREFIX
                                         apiURL:TRADE_FETCHPARTICIPATINGPRODUCTS];
    }

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: nil
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHALLPARTICIPATINGPRODUCTS
                     msgHUDText:LOADING_HUD];
}


- (void)fetchPerformanceMetrics:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_PERFORMANCEMETRICS];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: nil
                             vc:vc
                 webServiceCall:kWEBSERVICE_PERFORMANCEMETRICS
                     msgHUDText:LOADING_HUD];
}
- (void)fetchRUProductCategory:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL: TRADE_FETCHPRODUCTCATEGORY];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: nil
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHPRODUCTCATEGORY
                     msgHUDText:LOADING_HUD];
}

- (void)uploadImage:(UIImage *)pic type:(ImageUploadType)type andView:(UIView *)view withDelegate:(id <WebServiceManagerDelegate> )delegate
{
//    NSData *data = UIImageJPEGRepresentation(pic,0.5f);
//    
//    WebServiceRequest *request = [[WebServiceRequest alloc] init];
//    [request setRequestURL:[self urlEncodeUsingEncoding:[NSString stringWithFormat:@"%@%@%@",[WebServiceManager serverNoApi_Prefix],PAGES_PREFIX,IMAGEUPLOAD_URL]]];
//    [request setRequestMethod:POST];
//    switch (type) {
//        case kProfilePicUpload:
//            [request setWebserviceCall:kPROFILEPICUPDATE];
//            break;
//        case kMailAttachment:
//            [request setWebserviceCall:kMAILATTACHMENTIMAGE];
//            break;
//            
//        default:
//            break;
//    }
//    [request setRequestParams:[WebserviceHelper prepImageUploadParams:type]];
//    [request setRequestHeaders:[WebserviceHelper prepImageUploadRequestHeader]];
//    [request setRequestDataFiles:[WebserviceHelper prepImageUploadFormDataParams:data]];
//    [request setDataUserImage:data];
//    [request setVwHUD:view];
//    [request setMsgHUD:UPDATING_HUD];
//    [self setWSMDelegate:delegate];
//    [request setDelegate:delegate];
//    [self doRequestWithDataUpload:request];
}

- (void)fetchMessageList:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PUSH_PREFIX
                                               apiURL:PUSH_MESSAGELIST_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:nil
                             vc:vc
                 webServiceCall:kWEBSERVICE_MESSAGELIST
                     msgHUDText:LOADING_HUD];
}

- (void)readPushMessage: (NSString *)pushId
                     vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PRELOGIN_PREFIX
                                               apiURL:PRELOGIN_READPUSHMESSAGE_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"PushID": pushId}
                             vc:vc
                 webServiceCall:kWEBSERVICE_READNOTIFICATION
                     msgHUDText:LOADING_HUD];
}


- (void)removePushMessageWithList:(NSArray *)listArray vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PUSH_PREFIX
                                               apiURL:PUSH_REMOVE_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"list": listArray}
                             vc:vc
                 webServiceCall:kWEBSERVICE_DELETENOTIFICATION
                     msgHUDText:LOADING_HUD];
}

//for push notifications
- (void)openPushMessageInSession: (NSString *)pushId
{
    NSString *requestURLString = [self setURLToString:PRELOGIN_PREFIX
                                               apiURL:PRELOGIN_OPENPUSHMESSAGE_URL];
    
    NSDictionary *headerData;
    if ([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK]) //use pre-login header
        headerData = [WebserviceHelper prepRequestHeader];
    else
        headerData = [WebserviceHelper prepPostLoginHeader];
    
    [self sendRequestWithParamsInSession:requestURLString
                          requestHeaders:headerData
                       requestParameters:@{@"PushID": pushId}
                                delegate: mSession
                          webServiceCall: kWEBSERVICE_OPENNOTIFICATION
                              msgHUDText:LOADING_HUD];
}

- (void)fetchFAQ:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setNoApiURLToString:PAGES_PREFIX
                                                    apiURL:PAGES_FAQ_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPagesHeader]
              requestParameters:nil
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHFAQ
                     msgHUDText:REGISTRATION_HUD];
}


- (void)acceptTNC:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_ACCEPTTC_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"TermCondition": @(1)}
                             vc:vc
                 webServiceCall:kWEBSERVICE_ACCEPTTNC
                     msgHUDText:LOADING_HUD];
}

- (void)acceptPrivacyPolicy:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_ACCEPTPRIVACY];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"AcceptPrivacyPolicy": @(1)}
                             vc:vc
                 webServiceCall:kWEBSERVICE_ACCEPTPRIVACY
                     msgHUDText:LOADING_HUD];
}

- (void)fetchPushStatus:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PUSH_PREFIX
                                               apiURL:PUSH_FETCHPUSHSTATUS_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:nil
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHPUSH
                     msgHUDText:LOADING_HUD];
}

- (void)updatePushStatus:(BOOL) isPushing
                      vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_UPDATEPUSHSTATUS_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"PushToken": GET_PUSHTOKEN,
                                  @"PushStatus": @(isPushing).stringValue}
                             vc:vc
                 webServiceCall:kWEBSERVICE_UPDATEPUSH
                     msgHUDText:LOADING_HUD];
}

- (void)changePasswordPostLogin: (NSString *)oldPassword
                    newPassword: (NSString*)newPassword
                    vc: (UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_CHANGEPASSWORD_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"OldPassword": oldPassword,
                                   @"NewPassword": newPassword,
                                   }
                             vc:vc
                 webServiceCall:kWEBSERVICE_CHANGEPASSWORD
                     msgHUDText:SENDING_HUD];
}

#pragma mark - Register Consumer
- (void) submitMemberRegistrationGetOTP: (NSDictionary *) params
                                  view: (UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_SUBMITMEMBER_OTP];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:params
                             vc:vc
                 webServiceCall:kWEBSERVICE_SUBMITMEMBER_REQUESTOTP
                     msgHUDText:LOADING_HUD];
}

//submit otp
- (void) submitMemberRegistrationStepOne: (NSDictionary *) params
                                             view: (UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_SUBMITMEMBER_STEP1];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:params
                             vc:vc
                 webServiceCall:kWEBSERVICE_SUBMITMEMBER_SUBMITOTP
                     msgHUDText:LOADING_HUD];
}

#pragma mark resend OTP
-(void) resendOtpWithDictionary:(NSDictionary *)dictionary vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_RESENDOTP];
    
//        “ModuleID” : 1,/*Consumer*/
//        “MobileNumber” : “343t”,
//        “CountryDial” : Same as you send in the “API/Trade/SubmitMemberRegistration”

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: dictionary
                             vc:vc
                 webServiceCall:kWEBSERVICE_SUBMITMEMBER_RESENDOTP
                     msgHUDText:LOADING_HUD];    
}

- (void) submitMemberRegistrationStepTwo: (NSDictionary *) params
                                    view: (UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_SUBMITMEMBER_STEP2];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:params
                             vc:vc
                 webServiceCall:kWEBSERVICE_SUBMITMEMBER_FORM
                     msgHUDText:LOADING_HUD];
}

- (void) submitMemberVehicles:(NSDictionary  *) params view:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_SUBMITMEMBERVEHICLES];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:params
                             vc:vc
                 webServiceCall:kWEBSERVICE_SUBMITMEMBER_FORM
                     msgHUDText:LOADING_HUD];
}

#pragma mark - Performance API

- (void) fetchWorkshopPerformanceByDSR: (NSString *)tradeID
                                  view: (UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_WORKSHOPPERFORMANCEBYDSR_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"TradeID" : tradeID}
                             vc:vc
                 webServiceCall:kWEBSERVICE_MYPERFORMANCE
                     msgHUDText:LOADING_HUD];
    
}

- (void) fetchDSRPerformance:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_DSRPERFORMANCE_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:nil
                             vc:vc
                 webServiceCall:kWEBSERVICE_DSRPERFORMANCE
                     msgHUDText:LOADING_HUD];
    
}

- (void) fetchTradeOwnerPerformance:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_WORKSHOPPERFORMANCE_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:nil
                             vc:vc
                 webServiceCall:kWEBSERVICE_TRADEOWNERPERFORMANCE
                     msgHUDText:LOADING_HUD];
    
}

- (void) isValidAuthToken:(UIView<WebServiceManagerDelegate> *)view
{
    NSString *requestURLString = [self setURLToString:PRELOGIN_PREFIX
                                               apiURL:PRELOGIN_VALIDAUTHTOKEN_URL];
    
    [self sendRequestWithParamsInView:requestURLString
                       requestHeaders:[WebserviceHelper prepPostLoginHeader]
                    requestParameters:nil
                                 view:view
                       webServiceCall:kWEBSERVICE_ISVALIDAUTH
                           msgHUDText:LOADING_HUD];
}

#pragma mark Invite Trade
- (void)inviteTrade: (NSString *)companyName
        mobileNumber: (NSString* )mobNum
      companySource: (NSString *)companySource
                 vc: (UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_INVITETRADE_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"CompanyName": companyName,
                                   @"MobileNumber": mobNum,
                                   @"CompanySource": companySource,
                                   }
                             vc:vc
                 webServiceCall:kWEBSERVICE_INVITETRADE
                     msgHUDText:SENDING_HUD];
}


#pragma mark Rewards
- (void) fetchRewardsList:(NSDictionary *) listDict
                       vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:REWARDS_PREFIX
                                               apiURL:REWARDS_FETCHITEMSLIST_URL];
    
    [self sendRequestWithParams:requestURLString
                       requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: listDict
                                 vc:vc
                       webServiceCall:kWEBSERVICE_REWARDSFETCHLIST
                           msgHUDText:LOADING_HUD];
}

- (void) fetchRewardsDetails:(NSString *) categoryCode
                    itemCode:(NSString *) itemCode
                          vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:REWARDS_PREFIX
                                               apiURL:REWARDS_FETCHITEMSDETAILS_URL];
    
    [self sendRequestWithParams:requestURLString
                       requestHeaders:[WebserviceHelper prepPostLoginHeader]
                    requestParameters: @{@"CategoryCode":categoryCode,
                                         @"ItemCode"    :itemCode,
                                         }
                                 vc:vc
                       webServiceCall:kWEBSERVICE_REWARDSFETCHDETAILS
                           msgHUDText:LOADING_HUD];
}
- (void) fetchPointsSummary:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:REWARDS_PREFIX
                                               apiURL:REWARDS_FETCHPOINTSSUMMARY_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:nil
                             vc:vc
                 webServiceCall:kWEBSERVICE_REWARDSPOINTSSUMMARY
                     msgHUDText:LOADING_HUD];
}
- (void) fetchWishList:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:REWARDS_PREFIX
                                               apiURL:REWARDS_FETCHWISHLIST_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:nil
                             vc:vc
                 webServiceCall:kWEBSERVICE_REWARDSFETCHWISHLIST
                     msgHUDText:LOADING_HUD];
}

- (void) addWishItem:(NSString *) categoryCode
            itemCode:(NSString *) itemCode
                  vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:REWARDS_PREFIX
                                               apiURL:REWARDS_ADDWISHITEM_URL];
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"CategoryCode":categoryCode,
                                   @"ItemCode"    :itemCode,
                                   }
                             vc:vc
                 webServiceCall:kWEBSERVICE_REWARDSADDWISHITEM
                     msgHUDText:LOADING_HUD];
}

- (void) removeWishItem:(NSString *) wishID
                     vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:REWARDS_PREFIX
                                               apiURL:REWARDS_REMOVEWISHITEM_URL];
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"WishID": wishID }
                             vc:vc
                 webServiceCall:kWEBSERVICE_REWARDSREMOVEWISHITEM
                     msgHUDText:LOADING_HUD];
}

- (void) confirmOrder:(NSArray *) redemptionCart
       rewardsAddress:(NSDictionary *) rewardsAddress
                     vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:REWARDS_PREFIX
                                               apiURL:REWARDS_CONFIRMORDER_URL];
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"RedemptionCart": redemptionCart,
                                   @"RewardsAddress": rewardsAddress,
                                   }
                             vc:vc
                 webServiceCall:kWEBSERVICE_REWARDSCONFIRMORDER
                     msgHUDText:LOADING_HUD];
}

#pragma Rewards V2
- (void) fetchRewardsHome: (UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:REWARDS_PREFIX
                                               apiURL:REWARDS_FETCHREWARDSHOME_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:nil
                             vc:vc
                 webServiceCall:kWEBSERVICE_REWARDSFETCHREWARDSHOME
                     msgHUDText:LOADING_HUD];
}

- (void) fetchRewardsRedemptionHistoryWithDict:(NSDictionary *)dict vc: (UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:REWARDS_PREFIX
                                               apiURL:REWARDS_FETCHREDEMPTIONHISTORY_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:dict
                             vc:vc
                 webServiceCall:kWEBSERVICE_REWARDSREDEMPTIONHISTORY
                     msgHUDText:LOADING_HUD];
}

- (void) fetchRewardsRedemptionDetailsWithId:(NSString *)redemptionId vc: (UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:REWARDS_PREFIX
                                               apiURL:REWARDS_FETCHREDEMPTIONDETAILS_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"RedemptionId": redemptionId,}
                             vc:vc
                 webServiceCall:kWEBSERVICE_REWARDSREDEMPTIONDETAILS
                     msgHUDText:LOADING_HUD];
}

#pragma mark Promo Code function
- (void) getPromoDetails:(NSString *) promoCode
                   vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PROMOTION_PREFIX
                                               apiURL:PROMOTION_GETPROMODETAILS];
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"PromoCode": promoCode
                                   }
                             vc:vc
                 webServiceCall:kWEBSERVICE_PROMOGETDETAILS
                     msgHUDText:LOADING_HUD];
}

- (void) submitPromoCode: (NSDictionary *) promoCodeDict
                      vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PROMOTION_PREFIX
                                               apiURL:PROMOTION_SUBMITPROMOCODE];
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: promoCodeDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_PROMOSUBMITCODE
                     msgHUDText:LOADING_HUD];
}

- (void)fetchPromoRedemptionHistory:(NSDictionary *) redemptionHistoryDict
                             vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PROMOTION_PREFIX
                                               apiURL:PROMOTION_FETCHREDEMPTIONHISTORY];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:redemptionHistoryDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_PROMOFETCHHISTORY
                     msgHUDText:LOADING_HUD];
}

#pragma mark To Points Function
-(void) fetchTOPointsTransactionHistory: (NSDictionary *)transactDict
                                     vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TOPOINTS_PREFIX
                                               apiURL:TOPOINTS_TRANSACTIONHISTORY];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:transactDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_TOPOINTSHISTORY
                     msgHUDText:LOADING_HUD];
}

-(void) fetchTOPointsTransactionDetails: (NSDictionary *)transactDict
                                     vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TOPOINTS_PREFIX
                                               apiURL:TOPOINTS_TRANSACTIONDETAILS];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:transactDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_TOPOINTSDETAILS
                     msgHUDText:LOADING_HUD];
}

#pragma mark Survey
-(void) closeSurveyPopup: (NSDictionary *) surveyDict
                      vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:SURVEY_PREFIX
                                               apiURL:SURVEY_CLOSESURVEYPOPUP];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:surveyDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_CLOSESURVEY
                     msgHUDText:LOADING_HUD];
}

#pragma mark Workshop offer
-(void) fetchAncillaryOfferVcWithOfferStatus:(NSString *)offerStatus offerType:(NSString *)offerType vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PROMOTION_PREFIX
                                               apiURL:FETCHANCILLARYOFFER];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"OfferStatus": offerStatus,
                                  @"OfferType": offerType,
                                  }
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHANCILLARY
                     msgHUDText:LOADING_HUD];
}

-(void) cancelAncillaryOfferVcWithOrderDetails:(NSDictionary *)offerDetailsDict vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PROMOTION_PREFIX
                                               apiURL:CANCELANCILLARYOFFER];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: offerDetailsDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_CANCELANCILLARY
                     msgHUDText:LOADING_HUD];
}

-(void) fetchCurrentOfferVc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PROMOTION_PREFIX
                                               apiURL:CURRENTOFFER];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{}
                             vc:vc
                 webServiceCall:kWEBSERVICE_CURRENTOFFER
                     msgHUDText:LOADING_HUD];
}

-(void) fetchOfferDetailVc:(UIViewController<WebServiceManagerDelegate> *)vc offerDict :(NSDictionary *)offerDict
{
    NSString *requestURLString = [self setURLToString:PROMOTION_PREFIX
                                               apiURL:OFFERDETAIL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:offerDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_OFFERDETAILS
                     msgHUDText:LOADING_HUD];
}

-(void) submitToGetAncillaryOffer:(UIViewController<WebServiceManagerDelegate> *)vc submitDict :(NSDictionary *)submitDict
{
    NSString *requestURLString = [self setURLToString:PROMOTION_PREFIX
                                               apiURL:SUBMITANCILLARYOFFER];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:submitDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_SUBMITANCILLARY
                     msgHUDText:LOADING_HUD];
}

-(void) fetchPastOfferVc:(UIViewController<WebServiceManagerDelegate> *)vc startDate :(NSString *)startDate endDate:(NSString *)endDate offerType:(NSString *)offerType
{
    NSString *requestURLString = [self setURLToString:PROMOTION_PREFIX
                                               apiURL:PASTOFFER];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"startDate" : startDate,
                                  @"endDate"   : endDate,
                                  @"offerType" : offerType,
                                  }
                             vc:vc
                 webServiceCall:kWEBSERVICE_PASTOFFER
                     msgHUDText:LOADING_HUD];
}

-(void) fetchWorkshopOilDetailVc:(UIViewController<WebServiceManagerDelegate> *)vc input:(NSDictionary *)inputDict
{
    NSString *requestURLString = [self setURLToString:PROMOTION_PREFIX
                                               apiURL:WORKSHOPOIL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:inputDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_WORKSHOPOILDETAIL
                     msgHUDText:LOADING_HUD];
}

#pragma All Orders
-(void) fetchAllOrders:(UIViewController<WebServiceManagerDelegate> *)vc dict:(NSDictionary *)dict
{
    NSString *requestURLString = [self setURLToString:ORDER_PREFIX
                                               apiURL:FETCHALLORDERS];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:dict
                             vc:vc
                 webServiceCall:kWEBSERVICE_ALLORDERS
                     msgHUDText:LOADING_HUD];
}

-(void) fetchOrderDetails:(UIViewController<WebServiceManagerDelegate> *)vc orderID:(NSString *)orderID
{
    NSString *requestURLString = [self setURLToString:ORDER_PREFIX
                                               apiURL:FETCHORDERDETAILS];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"OrderID" : orderID}
                             vc:vc
                 webServiceCall:kWEBSERVICE_ORDERDETAILS
                     msgHUDText:LOADING_HUD];
}

-(void) fetchProductCategoryList:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:ORDER_PREFIX
                                               apiURL:FETCHPRODUCTCATEGORY];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{}
                             vc:vc
                 webServiceCall:kWEBSERVICE_CATEGORYLIST
                     msgHUDText:LOADING_HUD];
}

-(void) fetchAllProductsInCategory:(UIViewController<WebServiceManagerDelegate> *)vc productID:(NSString *)productID
{
    NSString *requestURLString = [self setURLToString:ORDER_PREFIX
                                               apiURL:FETCHALLPRODUCTSINCATEGORY];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"ProductCategoryID" : productID}
                             vc:vc
                 webServiceCall:kWEBSERVICE_ALLPRODUCTSINCATEGORYLIST
                     msgHUDText:LOADING_HUD];
}

-(void) addNewOrder:(UIViewController<WebServiceManagerDelegate> *)vc submitDict:(NSDictionary *)submitDict
{
    NSString *requestURLString = [self setURLToString:ORDER_PREFIX
                                               apiURL:ADDNEWORDER];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:submitDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_ADDNEWORDERS
                     msgHUDText:LOADING_HUD];
}

-(void) updateOrderStatus:(UIViewController<WebServiceManagerDelegate> *)vc submitDict:(NSDictionary *)submitDict
{
    NSString *requestURLString = [self setURLToString:ORDER_PREFIX
                                               apiURL:UPDATESTATUS];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:submitDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_UPDATESTATUS
                     msgHUDText:LOADING_HUD];
}

-(void) fetchPendingOrder:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:ORDER_PREFIX
                                               apiURL:PENDINGORDER];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{}
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHPENDINGORDER
                     msgHUDText:LOADING_HUD];
}


#pragma mark - In App Asset Function
-(void) fetchInAppAssetCategoryWithDictionary: (NSDictionary *) dict vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:ASSET_PREFIX
                                               apiURL:ASSET_FETCHASSETCATEGORYLIST];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:dict
                             vc:vc
                 webServiceCall:kWEBSERVICE_ASSETCATEGORYLIST
                     msgHUDText:LOADING_HUD];
}

-(void) fetchInAppAssetListWithDictionary:(NSDictionary *)dict vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:ASSET_PREFIX
                                               apiURL:ASSET_FETCHASSETLIST];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: dict
                             vc:vc
                 webServiceCall:kWEBSERVICE_ASSETLIST
                     msgHUDText:LOADING_HUD];
}

-(void) readAssetWithAssetId: (NSString *) assetId vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:ASSET_PREFIX
                                               apiURL:ASSET_READ];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"AssetID": assetId}
                             vc:vc
                 webServiceCall:kWEBSERVICE_ASSETREAD
                     msgHUDText:LOADING_HUD];
}

#pragma mark - UK Workshop Profiling Function
-(void) fetchWorkshopProfilingDetailsWithTradeID: (NSString *)tradeID vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHWORKSHOPDETAILSUK];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"TradeID": tradeID}
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHWORKSHOPDETAILSUK
                     msgHUDText:LOADING_HUD];
}

-(void) updateWorkshopProfilingDetailsWithDictionary:(NSDictionary *)dictionary vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_UPDATEWORKSHOPDETAILSUK];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: dictionary
                             vc:vc
                 webServiceCall:kWEBSERVICE_UPDATEWORKSHOPDETAILSUK
                     msgHUDText:LOADING_HUD];
    
}

#pragma mark Russia
-(void) submitPointsCalculator:(NSDictionary *)dictionary vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_SUBMITPOINTSCALC];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: dictionary
                             vc:vc
                 webServiceCall:kWEBSERVICE_SUBMITPOINTSCALC
                     msgHUDText:LOADING_HUD];
    
}
-(void) fetchRURewardsHome:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:REWARDS_PREFIX
                                               apiURL:REWARDS_RUREWARDSHOME];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: nil
                             vc:vc
                 webServiceCall:kWEBSERVICE_REWARDSFETCHREWARDSHOME
                     msgHUDText:LOADING_HUD];
    
}


-(void) submitRUAncillaryOfferWithDict:(NSDictionary *) ancilliaryDict vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PROMOTION_PREFIX
                                               apiURL:PROMOTION_RUSUBMITANCILLARYOFFERS];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: ancilliaryDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_SUBMITANCILLARY
                     msgHUDText:LOADING_HUD];
    
}

-(void) submitAncillaryOfferWithDict:(NSDictionary *) ancilliaryDict vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PROMOTION_PREFIX
                                               apiURL:PROMOTION_SUBMITANCILLARYOFFERS];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: ancilliaryDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_SUBMITANCILLARY
                     msgHUDText:LOADING_HUD];
    
}

#pragma mark Russia Workshop Performance
-(void) fetchPerformanceV2: (UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCH_PERFORMANCE_V2];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: nil
                             vc:vc
                 webServiceCall: kWEBSERVICE_FETCH_PERFORMANCE_V2
                     msgHUDText:LOADING_HUD];
    
}

-(void) fetchOrdersBreakDown:(NSDictionary *) tradeDict vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCH_ORDERSBREAKDOWN];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: tradeDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCH_ORDERSBREAKDOWN
                     msgHUDText:LOADING_HUD];
    
}

-(void) fetchPointsBreakDown:(NSDictionary *) tradeDict vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCH_POINTSBREAKDOWN];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: tradeDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCH_POINTSBREAKDOWN
                     msgHUDText:LOADING_HUD];
    
}

-(void) fetchLeaderboard:(NSDictionary *) tradeDict vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCH_LEADERBOARD];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: tradeDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCH_LEADERBOARD
                     msgHUDText:LOADING_HUD];
    
}

-(void) fetchStaffBreakdown:(NSDictionary *) tradeDict vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCH_STAFFBREAKDOWN];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: tradeDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCH_STAFFBREAKDOWN
                     msgHUDText:LOADING_HUD];
    
}

-(void) fetchOilChangeBreakdown:(NSDictionary *) tradeDict vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCH_OILCHANGEBREAKDOWN];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: tradeDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCH_OILCHANGEBREAKDOWN
                     msgHUDText:LOADING_HUD];
    
}


-(void) fetchCustomerBreakdown:(NSDictionary *) tradeDict vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCH_CUSTOMERBREAKDOWN];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: tradeDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCH_CUSTOMERBREAKDOWN
                     msgHUDText:LOADING_HUD];
    
}


-(void) fetchOfferRedemptionBreakdown:(NSDictionary *) tradeDict vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                            apiURL:TRADE_FETCH_OFFERREDEMPTIONBREAKDOWN];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: tradeDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCH_OFFERREDEMPTIONBREAKDOWN
                     msgHUDText:LOADING_HUD];
    
}

-(void) fetchApprovedWorkshop:(NSDictionary *) tradeDict vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCH_APPROVEDWORKSHOP];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: tradeDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCH_APPROVEDWORKSHOP
                     msgHUDText:LOADING_HUD];
    
}

#pragma mark - IN services
-(void) fetchPostalAddress:(NSString *) postalCodeString vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCH_POSTALADDRESS];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"PostalCode": postalCodeString,}
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCH_POSTALADDRESS
                     msgHUDText:LOADING_HUD];
}
-(void) fetchBankDetails:(NSString *) ifscString vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHBANKDETAILS];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"IFSCCode": ifscString,}
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCH_BANKDETAILS
                     msgHUDText:LOADING_HUD];
}
-(void) addIM:(NSDictionary *) imDict vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_ADDIM];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: imDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_ADDIM
                     msgHUDText:LOADING_HUD];
}

-(void) updateIM:(NSDictionary *) imDict vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_UPDATEIM];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: imDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_UPDATEIM
                     msgHUDText:LOADING_HUD];
}

#pragma mark - Russia V2 For Spirax & Gadus 1+1

-(void) fetchCTODetailsOnePlusOneFor:(NSString *)keywordSearch onVc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHCTODETAILS_ONEPLUSONE];

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"Keyword":keywordSearch}
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHCTO_ONEPLUSONE
                     msgHUDText:LOADING_HUD];
}

#pragma mark - Russia V2
-(void) fetchCTODetailsFor:(NSString *)keywordSearch onVc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHCTODETAILS];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"Keyword":keywordSearch}
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHCTO
                     msgHUDText:LOADING_HUD];
}

-(void) fetchWaysToEarnPointsFor:(NSString *)businessTypeId onVc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHWAYSTOEARNPOINTS];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"BusinessTypeId":businessTypeId}
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHWAYSTOEARNPOINTS
                     msgHUDText:LOADING_HUD];
}

-(void) fetchPerformanceMetricsFor:(NSString *)businessTypeId onVc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCHPERFORMANCEMETRICS];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"BusinessTypeId":businessTypeId}
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHPERFORMANCEMETRICS
                     msgHUDText:LOADING_HUD];
}


#pragma mark Performance New
-(void) fetchPerformanceSummary:(NSDictionary *) filterDataDict  vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCH_PERFORMANCESUMMARY];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: filterDataDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHPERFORMANCESUMMARY
                     msgHUDText:LOADING_HUD];
    
}
-(void) fetchSectionBreakdown:(NSDictionary *) filterDataDict vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX
                                               apiURL:TRADE_FETCH_SECTIONBREAKDOWN];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: filterDataDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHSECTIONBREAKDOWN
                     msgHUDText:LOADING_HUD];
}

//FetchPerformanceSummary
//FetchSectionBreakDown

#pragma mark - Loyalty Program
-(void) loadLoyaltyContractList:(NSDictionary *) searchParameters vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString;
    if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
        requestURLString = [self setURLToString:LOYALTYPROGRAM_PREFIX
                                                      apiURL:LOYALTYPROGRAM_LOADLOYALTYCONTRACTLIST];
    } else {
        requestURLString = [self setURLToString:LOYALTYPROGRAM_PREFIX
                                                      apiURL:LOYALTYPROGRAM_LOADLOYALTYCONTRACTLIST_TO];
    }
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: searchParameters
                             vc:vc
                 webServiceCall:kWEBSERVICE_LOYALTY_LOADCONTRACTLIST
                     msgHUDText:LOADING_HUD];
}

-(void) fetchLoyaltyWorkshopList:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:LOYALTYPROGRAM_PREFIX
                                               apiURL:LOYALTYPROGRAM_FETCHWORKSHOPLIST];
    
    LoginInfo *loginInfo = [mSession loadUserProfile];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:  @{@"TradeID" : loginInfo.tradeID}
                             vc:vc
                 webServiceCall:kWEBSERVICE_LOYALTY_FETCHWORKSHOPLIST
                     msgHUDText:LOADING_HUD];
    
}

-(void) fetchLoyaltyCatalogueListWithTradeID:(NSString *)tradeID vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:LOYALTYPROGRAM_PREFIX
                                               apiURL:LOYALTYPROGRAM_FETCHCATALOGUELIST];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:  @{@"TradeID" : tradeID}
                             vc:vc
                 webServiceCall:kWEBSERVICE_LOYALTY_FETCHCATALOGUELIST
                     msgHUDText:LOADING_HUD];
    
}

-(void) loadLoyaltyPackageList:(NSDictionary *) preferedParams vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:LOYALTYPROGRAM_PREFIX
                                               apiURL:LOYALTYPROGRAM_LOADLOYALTYPACKAGELIST];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: preferedParams
                             vc:vc
                 webServiceCall:kWEBSERVICE_LOYALTY_LOADPACKAGELIST
                     msgHUDText:LOADING_HUD];
}

-(void) submitNewContract:(NSDictionary *) submitParams vc:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:LOYALTYPROGRAM_PREFIX
                                               apiURL:LOYALTYPROGRAM_SUBMITNEWCONTRACT];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: submitParams
                             vc:vc
                 webServiceCall:kWEBSERVICE_LOYALTY_SUBMITNEWCONTRACT
                     msgHUDText:LOADING_HUD];
}

-(void) loadLoyaltyContractDetails:(NSString *)contractRef  vc:(UIViewController<WebServiceManagerDelegate> *)vc {

    NSString *requestURLString;
    if(GET_PROFILETYPE == kPROFILETYPE_DSR) {
        requestURLString = [self setURLToString:LOYALTYPROGRAM_PREFIX
                                            apiURL:LOYALTYPROGRAM_LOADLOYALTYCONTRACTDETAILS];
    } else {
        requestURLString = [self setURLToString:LOYALTYPROGRAM_PREFIX
                                            apiURL:LOYALTYPROGRAM_LOADLOYALTYCONTRACTDETAILS_TO];
    }
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: @{@"ContractReference" : contractRef}
                             vc:vc
                 webServiceCall:kWEBSERVICE_LOYALTY_LOADCONTRACTDETAILS
                     msgHUDText:LOADING_HUD];
}

-(void) fetchLoyaltyProgramPerf:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:LOYALTYPROGRAM_PREFIX
                                               apiURL:LOYALTYPROGRAM_DSRPERFORMANCE];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: nil
                             vc:vc
                 webServiceCall:kWEBSERVICE_LOYALTY_LOADDSRPERFORMANCE
                     msgHUDText:LOADING_HUD];

}

-(void) fetchLoyaltyPerformaceForContract:(NSDictionary *)contractDict vc:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:LOYALTYPROGRAM_PREFIX
                                               apiURL:LOYALTYPROGRAM_WORKSHOPPERFORMANCE];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: contractDict
                             vc:vc
                 webServiceCall:kWEBSERVICE_LOYALTY_WORKSHOPPERFORMANCE
                     msgHUDText:LOADING_HUD];
}

-(void) updateContract:(NSDictionary *) submitParams vc:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:LOYALTYPROGRAM_PREFIX
                                               apiURL:LOYALTYPROGRAM_UPDATECONTRACT];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: submitParams
                             vc:vc
                 webServiceCall:kWEBSERVICE_LOYALTY_UPDATECONTRACT
                     msgHUDText:LOADING_HUD];
}

-(void) extendContract:(NSDictionary *) submitParams vc:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:LOYALTYPROGRAM_PREFIX
                                               apiURL:LOYALTYPROGRAM_EXTENDCONTRACT];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: submitParams
                             vc:vc
                 webServiceCall:kWEBSERVICE_LOYALTY_EXTENDCONTRACT
                     msgHUDText:LOADING_HUD];
}

-(void) continueContract:(NSDictionary *) submitParams vc:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:LOYALTYPROGRAM_PREFIX
                                               apiURL:LOYALTYPROGRAM_CONTINUECONTRACT];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: submitParams
                             vc:vc
                 webServiceCall:kWEBSERVICE_LOYALTY_CONTINUECONTRACT
                     msgHUDText:LOADING_HUD];
}

-(void) terminateContract:(NSDictionary *) submitParams vc:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:LOYALTYPROGRAM_PREFIX
                                               apiURL:LOYALTYPROGRAM_TERMINATECONTRACT];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: submitParams
                             vc:vc
                 webServiceCall:kWEBSERVICE_LOYALTY_TERMINATECONTRACT
                     msgHUDText:LOADING_HUD];
}

-(void) completeContract:(NSDictionary *) submitParams vc:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:LOYALTYPROGRAM_PREFIX
                                               apiURL:LOYALTYPROGRAM_COMPLETECONTRACT];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: submitParams
                             vc:vc
                 webServiceCall:kWEBSERVICE_LOYALTY_COMPLETECONTRACT
                     msgHUDText:LOADING_HUD];
}


-(void) updateSignatureForContract:(NSDictionary *)submitParams vc:(UIViewController <WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:LOYALTYPROGRAM_PREFIX
                                               apiURL:LOYALTYPROGRAM_UPDATEESIGNATURECONTRACT_TO];

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: submitParams
                             vc:vc
                 webServiceCall:kWEBSERVICE_LOYALTY_UPDATESIGNATUREFORCONTRACT
                     msgHUDText:LOADING_HUD];
}

//TH
-(void) loadLoyaltyTierPoints:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:LOYALTYPROGRAM_PREFIX
                                               apiURL:LOYALTYPROGRAM_TH_LOADPOINTS];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{}
                             vc:vc
                 webServiceCall:kWEBSERVICE_LOYALTY_TH_LOADTIERPOINTS
                     msgHUDText:LOADING_HUD];
}

#pragma mark - promotion Redeem
-(void) promotionRedeem:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:PROMOTION_PREFIX apiURL:PROMOTION_REDEEMTRADEPROMOTION];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{}
                             vc:vc
                 webServiceCall:kWEBSERVICE_PROMOTION_REDEEMTRADEPROMOTION
                     msgHUDText:LOADING_HUD];
}

#pragma mark - Company Category
-(void) loadCompanyCategory:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX apiURL:TRADE_LOADCOMPANYCATEGORY];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{}
                             vc:vc
                 webServiceCall:kWEBSERVICE_TRADE_LOADCOMPANYCATEGORY
                     msgHUDText:LOADING_HUD];
}


#pragma mark - LubeMatch
-(void) loadSearchVehicleLubeDetails:(NSDictionary *)dict vc:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX apiURL:TRADE_LUBEMATCH_SEARCHVEHICLE];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:dict
                             vc:vc
                 webServiceCall:kWEBSERVICE_TRADE_LUBEMATCH_SEARCHVEHICLE
                     msgHUDText:LOADING_HUD];
}

-(void) submitLubematchRegistration:(NSDictionary *)dict vc:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX apiURL:TRADE_LUBEMATCH_SUBMITREGISTRATION];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:dict
                             vc:vc
                 webServiceCall:kWEBSERVICE_TRADE_LUBEMATCH_SUBMITREGISTRATION
                     msgHUDText:LOADING_HUD];
    
}


#pragma mark - Earn N Redeem Module
-(void) loadEarnNRedeem:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:TRADE_PREFIX apiURL:TRADE_EARNNREDEEM];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{}
                             vc:vc
                 webServiceCall:kWEBSERVICE_TRADE_EARNNREDEEM
                     msgHUDText:LOADING_HUD];
}

#pragma mark - Moscow
-(void) fetchDSRInventoryList:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:MSTRADE_PREFIX
                                               apiURL:MSTRADE_FETCHDSRINVENTORY];

    NSString *tradeIDSelected = @"";
    if(GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER) {
        LoginInfo *loginInfo = [mSession loadUserProfile];
        tradeIDSelected = loginInfo.tradeID;
    } else {
        tradeIDSelected = GET_INVENTORY_SELECTEDWORKSHOP_TRADEID;
    }

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:  @{@"TradeID" : tradeIDSelected}
                             vc:vc
                 webServiceCall:kWEBSERVICE_MSTRADE_FETCHDSRINVENTORY
                     msgHUDText:LOADING_HUD];
}

-(void) fetchTOInventoryList:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:MSTRADE_PREFIX
                                               apiURL:MSTRADE_FETCHTOINVENTORY];

    NSString *tradeIDSelected = @"";
    // added condition , sept 2020
    if(GET_PROFILETYPE == kPROFILETYPE_TRADEOWNER || GET_PROFILETYPE == kPROFILETYPE_MECHANIC) {
        LoginInfo *loginInfo = [mSession loadUserProfile];
        tradeIDSelected = loginInfo.tradeID;
    } else {
        tradeIDSelected = GET_INVENTORY_SELECTEDWORKSHOP_TRADEID;
    }

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:  @{@"TradeID" : tradeIDSelected}
                             vc:vc
                 webServiceCall:kWEBSERVICE_MSTRADE_FETCHTOINVENTORY
                     msgHUDText:LOADING_HUD];
}

-(void) fetchDSRWorkshopList:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:MSTRADE_WORKSHOP_PREFIX
                                               apiURL:MSTRADE_FETCHDSRWORKSHOPLIST];

    LoginInfo *loginInfo = [mSession loadUserProfile];

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:  @{@"Keyword" :@"",@"RangeSearch":@""}
                             vc:vc
                 webServiceCall:kWEBSERVICE_MSTRADE_FETCHDSRWORKSHOPLIST
                     msgHUDText:LOADING_HUD];
}

//TO be used later for new UI
-(void) fetchMSParticipatingProducts:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:MSTRADE_PREFIX
                                               apiURL:MSTRADE_FETCHPARTICIPATINGPRODUCTS];

    LoginInfo *loginInfo = [mSession loadUserProfile];

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:  @{@"TradeID" : loginInfo.tradeID}
                             vc:vc
                 webServiceCall:kWEBSERVICE_MSTRADE_FETCHPARTICIPATINGPRODUCTS
                     msgHUDText:LOADING_HUD];
}

-(void) submitCodesMS:(NSDictionary *)submitParams vc:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSLog(@"Submit_Code ---------########### %@",submitParams);
    NSString *requestURLString = [self setURLToString:MSTRADE_PREFIX apiURL:MSTRADE_SUBMITCODEMS];
    [self sendRequestWithParams:requestURLString
                requestHeaders:[WebserviceHelper prepPostLoginHeader]
             requestParameters:submitParams
                            vc:vc
                webServiceCall:kWEBSERVICE_MSTRADE_SUBMITCODEMS
                    msgHUDText:LOADING_HUD];
}

-(void) submitScanOutCodesMS:(NSDictionary *)submitParams vc:(UIViewController<WebServiceManagerDelegate> *)vc{
    NSLog(@"Submit_Code scan out ---------########### %@",submitParams);
    NSString *requestURLString = [self setURLToString:MSTRADE_PREFIX apiURL:MSTRADE_SUBMITSCANOUTCODEMS];
    [self sendRequestWithParams:requestURLString
                requestHeaders:[WebserviceHelper prepPostLoginHeader]
             requestParameters:submitParams
                            vc:vc
                webServiceCall:kWEBSERVICE_MSTRADE_SUBMITCODEMS
                    msgHUDText:LOADING_HUD];
}

-(void) fetchPerformanceV3: (UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:MSTRADE_PREFIX
                                               apiURL:MSTRADE_FETCH_PERFORMANCE_V3];

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: nil
                             vc:vc
                 webServiceCall: kWEBSERVICE_MSTRADE_FETCHPERFORMANCE_V3
                     msgHUDText:LOADING_HUD];

}


-(void) fetchScanNRewardHistory:(NSDictionary *)dict onVc:(UIViewController<WebServiceManagerDelegate> *)vc {
    NSString *requestURLString = [self setURLToString:EARNNREDEEMREPORT_PREFIX apiURL:EARNNREDEEMREPORT_FETCHREPORT];

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:dict
                             vc:vc
                 webServiceCall:kWEBSERVICE_TRADE_EARNNREDEEM_REPORT
                     msgHUDText:LOADING_HUD];
}

#pragma mark - Gamification
-(void) viewPromotionDetailsForPromotion:(UIViewController<WebServiceManagerDelegate> *) vc {

    NSString *requestURLString = [self setURLToString:GAMIFICATION_PREFIX apiURL:GAMIFICATION_VIEWPROMOTIONDETAILS];

    LoginInfo *loginInfo = [mSession loadUserProfile];

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{@"CompanyCategory" : loginInfo.companyCategory}
                             vc:vc
                 webServiceCall:kWEBSERVICE_GAMIFICATION_VIEWDETAILS
                     msgHUDText:LOADING_HUD];

}

-(void) showPromotionResultFor:(NSDictionary *)dict onVC:(UIViewController<WebServiceManagerDelegate> *) vc  {

    NSString *requestURLString = [self setURLToString:GAMIFICATION_PREFIX apiURL:GAMIFICATION_SHOWPROMOTIONRESULT];

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters: dict
                             vc:vc
                 webServiceCall:kWEBSERVICE_GAMIFICATION_SHOWPROMOTIONRESULT
                     msgHUDText:LOADING_HUD];

}

-(void) viewGamificationDetails:(UIViewController<WebServiceManagerDelegate> *) vc {

    NSString *requestURLString = [self setURLToString:GAMIFICATION_PREFIX apiURL:GAMIFICATION_CHECKINVIEWDETAILS];

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{}
                             vc:vc
                 webServiceCall:kWEBSERVICE_GAMIFICATION_CHECKIN_VIEWDETAILS
                     msgHUDText:LOADING_HUD];

}

-(void) viewHelixGamificationDetails:(UIViewController<WebServiceManagerDelegate> *) vc {

    NSString *requestURLString = [self setURLToString:GAMIFICATION_PREFIX apiURL:GAMIFICATION_VIEWDETAILS];

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{}
                             vc:vc
                 webServiceCall:kWEBSERVICE_GAMIFICATION_HELIX_VIEWDETAILS
                     msgHUDText:LOADING_HUD];

}

-(void) checkInToday:(NSDictionary *)dict vc:(UIViewController<WebServiceManagerDelegate> *) vc {

    NSString *requestURLString = [self setURLToString:GAMIFICATION_PREFIX                           apiURL:GAMIFICATION_CHECKINTODAY];

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:dict
                             vc:vc
                 webServiceCall:kWEBSERVICE_GAMIFICATION_CHECKIN_TODAY
                     msgHUDText:LOADING_HUD];

}

-(void) playGrandPrizeInGamification:(UIViewController<WebServiceManagerDelegate> *) vc {

    NSString *requestURLString = [self setURLToString:GAMIFICATION_PREFIX                           apiURL:GAMIFICATION_PLAYGRANDPRIZE];

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{}
                             vc:vc
                 webServiceCall:kWEBSERVICE_GAMIFICATION_CHECKIN_PLAYGRANDPRIZE
                     msgHUDText:LOADING_HUD];

}

#pragma mark - Tiering
-(void) fetchCompanyTier:(UIViewController <WebServiceManagerDelegate> *)vc {

    NSString *requestURLString = [self setURLToString:TRADE_PREFIX                                                                            apiURL:TRADE_FETCHCOMPANYTIER];

    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:@{}
                             vc:vc
                 webServiceCall:kWEBSERVICE_FETCHCOMPANYTIER
                     msgHUDText:LOADING_HUD];

}

#pragma mark - AI
-(void) fetchAIConverstaion:(UIViewController <WebServiceManagerDelegate> *) vc {
    NSString *requestURLString = [self setURLToString:AI_PREFIX                                                                                                                                   apiURL:AI_FETCHAICOVERSATIONS];

       [self sendRequestWithParams:requestURLString
                    requestHeaders:[WebserviceHelper prepPostLoginHeader]
                 requestParameters:@{}
                                vc:vc
                    webServiceCall:kWEBSERVICE_AI_FETCHCONVERSATIONS
                        msgHUDText:LOADING_HUD];
}

-(void) fetchAIOilChangeCount:(UIViewController <WebServiceManagerDelegate> *) vc {
    NSString *requestURLString = [self setURLToString:AI_PREFIX                                                                                                                                   apiURL:AI_FETCHAIOILCHANGECOUNT];

       [self sendRequestWithParams:requestURLString
                    requestHeaders:[WebserviceHelper prepPostLoginHeader]
                 requestParameters:@{}
                                vc:vc
                    webServiceCall:kWWEBSERVICE_AI_FETCHOILCHANGECOUNT
                        msgHUDText:LOADING_HUD];
}

#pragma mark - Logout

- (void)logout:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString = [self setURLToString:PRELOGIN_PREFIX
                                               apiURL:PRELOGIN_LOGOUT_URL];
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders:[WebserviceHelper prepPostLoginHeader]
              requestParameters:nil
                             vc:vc
                 webServiceCall:kWEBSERVICE_LOGOUT
                     msgHUDText:SIGNOUT_HUD];
}

#pragma mark - Request functions
//for webservice calls in UIViewController
-(void) sendRequestWithParams: (NSString *) url
               requestHeaders:(NSDictionary *)header
            requestParameters:(NSDictionary *) requestParameters
                           vc:(UIViewController<WebServiceManagerDelegate> *)vc
               webServiceCall:(WebserviceCall) webServiceCallTag
                   msgHUDText:(NSString *)msgHUDText
{
    NSLog(@"logIN ############--------- %@",header);
    [CrashlyticsKit setIntValue: webServiceCallTag forKey: @"Last-called Web Service Call Tag"]; //send current loaded VC
    WebServiceRequest *request = [[WebServiceRequest alloc] init];
    [request setRequestURL:url];
    [request setRequestMethod: POST];
    [request setWebserviceCall: webServiceCallTag];
    [request setRequestParams: requestParameters];
    [request setRequestHeaders: header];
    [request setVwHUD:vc.view];
    [request setMsgHUD:msgHUDText];
    [request setDelegate: vc];
    [self setWSMDelegate:vc];
    [self doProcesswithRequest:request];
}

-(void) sendRequestWithParams: (NSString *) url
               requestHeaders:(NSDictionary *)header
            requestParameters:(NSDictionary *) requestParameters
                requestMethod:(RequestMethod) requestMethod
                           vc:(UIViewController<WebServiceManagerDelegate> *)vc
               webServiceCall:(WebserviceCall) webServiceCallTag
                   msgHUDText:(NSString *)msgHUDText
{
    
    [CrashlyticsKit setIntValue: webServiceCallTag forKey: @"Last-called Web Service Call Tag"]; //send current loaded VC
    WebServiceRequest *request = [[WebServiceRequest alloc] init];
    [request setRequestURL:url];
    [request setRequestMethod: requestMethod];
    [request setWebserviceCall: webServiceCallTag];
    [request setRequestParams: requestParameters];
    [request setRequestHeaders: header];
    [request setVwHUD:vc.view];
    [request setMsgHUD:msgHUDText];
    [request setDelegate: vc];
    [self setWSMDelegate:vc];
    [self doProcesswithRequest:request];
}

//for webservice calls in UIView
-(void) sendRequestWithParamsInView: (NSString *) url
                     requestHeaders:(NSDictionary *)header
                  requestParameters:(NSDictionary *) requestParameters
                               view:(UIView<WebServiceManagerDelegate> *)view
                     webServiceCall:(WebserviceCall) webServiceCallTag
                         msgHUDText:(NSString *)msgHUDText
{
    [CrashlyticsKit setIntValue: webServiceCallTag forKey: @"Last-called Web Service Call Tag"]; //send current loaded VC
    
    WebServiceRequest *request = [[WebServiceRequest alloc] init];
    [request setRequestURL: url];
    [request setRequestMethod:POST];
    [request setWebserviceCall:webServiceCallTag];
    [request setRequestParams: requestParameters];
    [request setRequestHeaders:header];
    [request setVwHUD:view];
    [request setMsgHUD:msgHUDText];
    [request setDelegate:view];
    [self setWSMDelegate:view];
    [self doProcesswithRequest:request];
}

//for webservice calls in Session
-(void) sendRequestWithParamsInSession: (NSString *) url
                     requestHeaders:(NSDictionary *)header
                  requestParameters:(NSDictionary *) requestParameters
                           delegate:(id <WebServiceManagerDelegate>)delegate
                     webServiceCall:(WebserviceCall) webServiceCallTag
                         msgHUDText:(NSString *)msgHUDText
{
    [CrashlyticsKit setIntValue: webServiceCallTag forKey: @"Last-called Web Service Call Tag"]; //send current loaded VC
    
    WebServiceRequest *request = [[WebServiceRequest alloc] init];
    [request setRequestURL: url];
    [request setRequestMethod:POST];
    [request setWebserviceCall:webServiceCallTag];
    [request setRequestParams: requestParameters];
    [request setRequestHeaders:header];
    [request setDelegate: delegate];
    [self setWSMDelegate: delegate];
    [self doProcesswithRequest:request];
}

//for webservice calls with data uploads
-(void) sendRequestWithDataUpload: (NSString *) url
                   requestHeaders:(NSDictionary *)header
                requestParameters:(NSDictionary *) requestParameters
                        dataFiles:(NSArray *) dataFiles
                               vc:(UIViewController<WebServiceManagerDelegate> *)vc
                   webServiceCall:(WebserviceCall) webServiceCallTag
                       msgHUDText:(NSString *)msgHUDText
{
    [CrashlyticsKit setIntValue: webServiceCallTag forKey: @"Last-called Web Service Call Tag"]; //send current loaded VC

    WebServiceRequest *request = [[WebServiceRequest alloc] init];
    [request setRequestURL: url];
    [request setRequestMethod:POST];
    [request setWebserviceCall:webServiceCallTag];
    [request setRequestParams: requestParameters];
    [request setRequestHeaders: [WebserviceHelper prepPostLoginHeader]];
    [request setRequestDataFiles: dataFiles];
//    [request setDataUserImage:data];                
    [request setVwHUD:vc.view];
    [request setMsgHUD:msgHUDText];
    [request setDelegate: vc];
    [self setWSMDelegate:vc];
    [self doRequestWithDataUpload:request];

}

-(NSString *) setURLToString: (NSString *)apiPrefix apiURL:(NSString *)apiURL
{
    NSLog(@"APIPREFIX:%@",apiPrefix);
    NSLog(@"APIURL:%@",apiURL);

    return [self urlEncodeUsingEncoding: [NSString stringWithFormat:@"%@%@%@",SERVER_PREFIX,apiPrefix,apiURL]];
}

-(NSString *) setNoApiURLToString: (NSString *)apiPrefix apiURL:(NSString *)apiURL
{
    return [self urlEncodeUsingEncoding: [NSString stringWithFormat:@"%@%@%@",[WebServiceManager serverNoApi_Prefix],apiPrefix,apiURL]];
}

#pragma mark - Setter method

- (void)setWSMDelegate:(id <WebServiceManagerDelegate> )delegate
{
    self.delegate = delegate;
}
- (BOOL)validateResponse:(WebServiceResponse *)response
{
    if (response.responseCode == kSessionTokenExpired || response.responseCode == KSessionTokenInvalid) {
        [self cancelCurrentOperations];
        [mAlert showWebserviceAlertWithAlert:response.alert onCompletion:^(BOOL completion){
            [self cancelCurrentOperations];
            [mSession reset];
            [mSession loadLoginView];
        }];
        return NO;
    }
    return YES;
}

#pragma mark - check connection
- (BOOL)connected {
    return [AFNetworkReachabilityManager sharedManager].reachable;
}

#pragma mark-
#pragma mark response call backs
- (void)processSucceeded:(id)object :(NSNumber *)ref :(WebServiceRequest *)request{
    WebServiceResponse *response = [[WebServiceResponse alloc] initWithData:object];

    NSLog(@"success FetchHome ------ %@", [response getJSON]);

    if (![self validateResponse:response]) {
        return;
    }
    response.webserviceCall = [ref intValue];
    
    if (response.webserviceCall >= kOATS_SEARCH) //oats api codes are 103000 or more
//        response.webserviceCall == kOATS_EQUIPMENTHREF)
    {
        [UpdateHUD removeMBProgress: (UIViewController *) request.delegate];
        if ([request.delegate respondsToSelector:@selector(processCompleted:)]) {
            
            [request.delegate performSelector:@selector(processCompleted:) withObject:response];
        }
    }
    else if (!response.errorInResponse)
    {
        if ([request.delegate respondsToSelector:@selector(processCompleted:)]) {
            
            [request.delegate performSelector:@selector(processCompleted:) withObject:response];
        }
        
    }
    else
    {
        UIViewController *vc = request.delegate;
        if (request.delegate) {
            if ([vc isKindOfClass: [UIViewController class]])
                [UpdateHUD removeMBProgress: vc.view];
            
            
            if ([request.delegate respondsToSelector:@selector(processFailed:)]) {
                
                CLSLog(@"class %@ failed webservice %i %@", [mSession currentViewControllerString], request.webserviceCall, [response getErrorMessage]);
                [request.delegate performSelector:@selector(processFailed:) withObject:response];
            }
        }
        if ([response getResponseCode] == kAppVersionOldVersionCode) //5002
        {
            [mAlert showErrorAlertWithMessage: [response getResponseMessage] onCompletion:^(BOOL finished) {
                [[UIApplication sharedApplication] openURL: [NSURL URLWithString: kAppStoreURLString]];
                if (![vc isKindOfClass: [LoginViewController class]])
                {
                    [mSession reset];
                    [mLookupManager loadLookupTable];
                    [mSession loadLoginView];
                }
            }];
        }
        if ([response getResponseCode] == kUnAuthorisedAccessCode) //5003
        {
            WebserviceAlert *alert = [[WebserviceAlert alloc]init];
            alert.title = LOCALIZATION(C_GLOBAL_ERROR);
            alert.message = LOCALIZATION(C_GLOBAL_SESSION_EXPIREDLOGINAGAIN);
            alert.btnTextPositive = LOCALIZATION(C_GLOBAL_OK);
            
            [mAlert showWebserviceAlertWithAlert:alert onCompletion:^(BOOL completion){
                [mSession reset];
                [mLookupManager loadLookupTable];
                [mSession loadLoginView];
            }];
        }
        else if ([response getResponseCode] == 8000 ||
                 [response getResponseCode] == 8010 ||
                 [response getResponseCode] == 8111 ||
                 [response getResponseCode] == 8112 ||
                 [response getResponseCode] == 8113 ||
                 [response getResponseCode] == 8115)
        {
            //let processfailed handle
        }
        else
        {
            if (![[response getResponseMessage] isKindOfClass: [NSNull class]])
                [mAlert showErrorAlertWithMessage: [response getResponseMessage]];
        }
    }
}

- (void)processFail:(NSError *)error :(NSNumber *)ref :(WebServiceRequest *)request{
    WebServiceResponse *response = [[WebServiceResponse alloc] init];
    response.error = error;
    response.webserviceCall = [ref intValue];
    
    NSString *networkError = [response getNetworkFailureMsg];
    
    WebserviceAlert *alert = [[WebserviceAlert alloc]init];
    alert.title = LOCALIZATION(C_GLOBAL_ERROR);
    alert.message = LOCALIZATION(C_GLOBAL_SESSION_EXPIREDLOGINAGAIN);
    alert.btnTextPositive = LOCALIZATION(C_GLOBAL_OK);
    
    if (EQUALS(networkError,kForbiddenAccess)) {
        [mAlert showWebserviceAlertWithAlert:alert onCompletion:^(BOOL completion){
            [mSession reset];
            [mLookupManager loadLookupTable];
            [mSession loadLoginView];
    }];
        return;
    }
    
    if ([request.delegate respondsToSelector:@selector(processFailed:)])
    {
        if (request.delegate != mSession)
        {
//            NSLog(@"fail %@", [response getNetworkFailureMsg]);
            CLSLog(@"class %@ failed webservice %i %@", [mSession currentViewControllerString], request.webserviceCall, [response getNetworkFailureMsg]);

            if (response.responseCode != 8000) //invite sms error  code
                [mAlert showErrorAlertWithMessage: response.getNetworkFailureMsg];

//            //Handle timeout in respective failure delegate. TO BE IMPROVED.
//            if (![response.getNetworkFailureMsg isEqualToString: LOCALIZATION(C_GLOBAL_TIMEOUT)])
//                [mAlert showErrorAlertWithMessage: response.getNetworkFailureMsg];
        }
        CLSLog(@"class %@ failed webservice %i %@", [mSession currentViewControllerString], request.webserviceCall, [response getErrorMessage]);
        
        [request.delegate performSelector:@selector(processFailed:) withObject:response];
    }
}


//Send request
- (void)doProcesswithRequest:(WebServiceRequest *)request
{
    //    AFSecurityPolicy *securityPolicy = [[AFSecurityPolicy alloc] init];
    AFSecurityPolicy* securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    [securityPolicy setValidatesDomainName:NO];
    [securityPolicy setAllowInvalidCertificates:YES];
    
    if (request.vwHUD != nil) {
        
        if ([request.msgHUD isEqualToString:SPLASHSCREEN_HUD]) {
            [UpdateHUD addSpotifyHUDSplash:request.vwHUD];
        }
        else if ([request.msgHUD isEqualToString:MYPOINTS_HUD])
        {
            [UpdateHUD addMBProgress:request.vwHUD withText:BLANK];
        }
        else [UpdateHUD addMBProgress:request.vwHUD withText:request.msgHUD];
        
    }
    
    manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.requestSerializer.timeoutInterval = 60;
    
    [request.requestHeaders enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        [manager.requestSerializer setValue:object forHTTPHeaderField:key];
    }];
    [manager setSecurityPolicy:securityPolicy];
    
    if (request.requestMethod == POST)
    {
//        [manager POST:request.requestURL parameters:request.requestParams success:^(NSURLSessionTask *operation, id responseObject) {
        [manager POST:request.requestURL parameters:request.requestParams progress: nil success:^(NSURLSessionDataTask * _Nonnull operation, id  _Nullable responseObject) {
            if (request.vwHUD != nil) {
                if ([request.msgHUD isEqualToString:SPLASHSCREEN_HUD]) {
                    [UpdateHUD removeSpotifyHUD:request.vwHUD];
                }
                else
                {
                    if (request.webserviceCall != kWEBSERVICE_LOGIN) {
                        [UpdateHUD removeMBProgress:request.vwHUD];
                    }
                }
            }


            [self successRequest:responseObject request:request withServiceIndentifier:[NSNumber numberWithInt:request.webserviceCall]];
        }
              failure:^(NSURLSessionTask *operation, NSError *error) {
                  if (request.vwHUD != nil) {
                      if ([request.msgHUD isEqualToString:SPLASHSCREEN_HUD]) {
                          [UpdateHUD removeSpotifyHUD:request.vwHUD];
                      }
                      else [UpdateHUD removeMBProgress:request.vwHUD];                  }
                  [self failedRequest:error request:request withServiceIndentifier:[NSNumber numberWithInt:request.webserviceCall]];
              }];
    }
    else if (request.requestMethod == GET)
    {
        [manager GET:request.requestURL parameters:request.requestParams progress:nil success:^(NSURLSessionTask *operation, id responseObject)
        {
            [self successRequest:responseObject request:request withServiceIndentifier:[NSNumber numberWithInt:request.webserviceCall]];
        }
             failure:^(NSURLSessionTask *operation, NSError *error)
         {
             [self failedRequest:error request:request withServiceIndentifier:[NSNumber numberWithInt:request.webserviceCall]];
         }];
    }
}

- (void)logExceptionError:(UIView *)view withLogInfo:(UncaughtExceptionHandler *)exceptionInfo withDelegate:(id <WebServiceManagerDelegate> )delegate
{
    WebServiceRequest *request = [[WebServiceRequest alloc] init];
    [request setRequestURL:[self urlEncodeUsingEncoding:[NSString stringWithFormat:@"%@%@%@",SERVER_PREFIX,PRELOGIN_PREFIX,LOGEXCEPTIONERROR_URL]]];
    [request setRequestMethod:POST];
    [request setWebserviceCall:kLOGEXCEPTIONERROR];
    if ([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK])
        [request setRequestHeaders:[WebserviceHelper prepRequestHeader]];
    else
        [request setRequestHeaders:[WebserviceHelper prepPostLoginHeader]];
    [request setRequestParams:[WebserviceHelper prepExceptionErrorLogParams:exceptionInfo]];
    [request setVwHUD:nil];
    [request setMsgHUD:SIGNIN_HUD];
    [self setWSMDelegate:delegate];
    [request setDelegate:delegate];
    [self doProcesswithRequest:request];
}


#pragma mark OATS API
#define OATS_INDO_URL @"https://shell-lmc-id.phoenix.earlweb.net/"
#define OATS_INDIA_URL @"https://shell-lmc-in.phoenix.earlweb.net/"
#define OATS_TOKEN_SUFFIX @"token=hE%2A6%24FK5H7ks"

-(void) fetchOatsQueryWithString:(NSString *) queryString vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
    NSString *requestURLString;
//    if(IS_COUNTRY(COUNTRYCODE_INDONESIA))
//        return;
//    else
//        return;
    queryString = [queryString stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLHostAllowedCharacterSet]];
    requestURLString = [NSString stringWithFormat: @"%@search.json?q=%@&%@", OATS_INDO_URL, queryString, OATS_TOKEN_SUFFIX];
//    https://shell-lmc-id.phoenix.earlweb.net/search.json?q=nissan+serena&year=2017&token=hE%2A6%24FK5H7ks
    
    [self sendRequestWithParams:requestURLString
                 requestHeaders: nil
              requestParameters: nil
                  requestMethod: GET
                             vc:vc
                 webServiceCall:kOATS_SEARCH
                     msgHUDText:LOADING_HUD];
}
-(void) fetchOatsEquipmentWithHrefString:(NSString *) hrefString vc:(UIViewController<WebServiceManagerDelegate> *)vc
{
        NSString *requestURLString;
    //    if(IS_COUNTRY(COUNTRYCODE_INDONESIA))
    //        return;
    //    else
    //        return;
    requestURLString = [NSString stringWithFormat: @"%@%@.json?%@", OATS_INDO_URL, hrefString, OATS_TOKEN_SUFFIX];
//    https://shell-lmc-id.phoenix.earlweb.net/equipment/sunny_mk_viii_1_6_saloon_75kw_fwd_FawEdQdjz.json?token=hE%2A6%24FK5H7ks
        
        [self sendRequestWithParams:requestURLString
                     requestHeaders: nil
                  requestParameters: nil
                      requestMethod: GET
                                 vc:vc
                     webServiceCall:kOATS_EQUIPMENTHREF
                         msgHUDText:LOADING_HUD];
    
}

-(void) fetchOatsVehicleType:(UIViewController<WebServiceManagerDelegate> *)vc
{
        NSString *requestURLString;
    //    if(IS_COUNTRY(COUNTRYCODE_INDONESIA))
    //        return;
    //    else
    //        return;
        requestURLString = [NSString stringWithFormat: @"%@browse.json?%@", OATS_INDO_URL, OATS_TOKEN_SUFFIX];
//    https://shell-lmc-id.phoenix.earlweb.net/equipment/sunny_mk_viii_1_6_saloon_75kw_fwd_FawEdQdjz.json?token=hE%2A6%24FK5H7ks
        
        [self sendRequestWithParams:requestURLString
                     requestHeaders: nil
                  requestParameters: nil
                      requestMethod: GET
                                 vc:vc
                     webServiceCall:kOATS_VEHICLETYPES
                         msgHUDText:LOADING_HUD];
    
}

@end
