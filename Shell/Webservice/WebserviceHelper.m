//
//  WebserviceHelper.m
//  JTIStarPartners
//
//  Created by Shekhar  on 11/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import "WebserviceHelper.h"
#import "Session.h"
#import "LocalizationManager.h"

@implementation WebserviceHelper

#pragma mark - Make request header

+ (NSDictionary *)prepRequestHeader
{
    AppInfo *appInfo = [[AppInfo alloc]init];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:SECRET_TOKEN_VALUE  forKey:SECRET_TOKEN_KEY];   //ER_TOKEN
    [dict setObject:CONTENT_TYPE_JSON   forKey:CONTENT_TYPE_KEY];   //Content-Type

    [dict setObject:APPID_VALUE         forKey:APPID_KEY];          //APP_ID
    [dict setObject:appInfo.appVersion  forKey:kAppVersion];        //AppVersion
    
    NSNumber *languageCode = [appInfo localisedLanguage];
    
    [dict setObject:[languageCode stringValue] forKey:APP_LANGUAGE_CODE];   //LanguageCode
    return dict;
}

+ (NSDictionary *) prepPostLoginHeader
{
    AppInfo *appInfo = [[AppInfo alloc]init];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:SECRET_TOKEN_VALUE  forKey:SECRET_TOKEN_KEY];    //ER_TOKEN
    [dict setObject:CONTENT_TYPE_JSON   forKey:CONTENT_TYPE_KEY];    //ContentType
    
    [dict setObject:USER_TOKEN          forKey:AUTH_TOKEN_KEY];      //AUTH_TOKEN
    [dict setObject:APPID_VALUE forKey:APPID_KEY];                  //APPID
    [dict setObject:appInfo.appVersion forKey:kAppVersion];         //AppVersion
    
    NSNumber *languageCode = [appInfo localisedLanguage];
    [dict setObject:[languageCode stringValue] forKey:APP_LANGUAGE_CODE];   //LanguageCode
    
    LoginInfo *loginInfo = [mSession loadUserProfile];
    
    if ([loginInfo.tradeID length] == 0)
    {
        [[WebServiceManager sharedInstance] cancelCurrentOperations];
        
        [mAlert showNetworkErrorAlert: LOCALIZATION(C_GLOBAL_SESSION_EXPIREDLOGINAGAIN) onCompletion:^(BOOL finished) { 
            [mSession reset];
            [mLookupManager loadLookupTable];
            [mSession loadLoginView];
        }];
    }
    
    [dict setObject: loginInfo.tradeID forKey:TRADEID_KEY];     //TradeID
    [dict setObject: loginInfo.programID forKey:PROGRAMID_KEY];   //ProgramID
    [dict setObject: loginInfo.countryCode forKey:COUNTRYCODE_KEY];   //CountryCode

    return dict;
}


+ (NSDictionary *) prepPagesHeader
{
    NSMutableDictionary *dict;
    if ([GET_AUTHENTICATIONTOKEN isEqualToString: BLANK])
        dict = [self prepRequestHeader]; //pre login header
    else
        dict = [self prepPostLoginHeader];
    [dict removeObjectForKey: CONTENT_TYPE_KEY];
    return dict;
}
//+ (NSDictionary *)prepPreLoginRequestHeader
//{
//    
//    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
//    [dict setObject:SECRET_TOKEN_VALUE forKey:SECRET_TOKEN_KEY];
//    [dict setObject:CONTENT_TYPE_JSON forKey:CONTENT_TYPE_KEY];
//    
//    AppInfo *appInfo = [[AppInfo alloc]init];
//    NSNumber *languageCode = [appInfo localisedLanguage];
//    [dict setObject:[languageCode stringValue] forKey:APP_LANGUAGE_CODE];
//    return dict;
//}

#pragma mark - Make session check parameters
+ (NSMutableDictionary *)prepSessionCheckParams:(NSString *)sessionToken
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:sessionToken forKey:kSessionToken];
    return dict;
}

#pragma mark - Make login parameters
+ (NSMutableDictionary *)prepLoginParams:(LoginInfo *)loginInfo
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:loginInfo.countryCode               forKey:kCountryCode];
    [dict setObject:loginInfo.credentials.loginName     forKey:kLoginName];
    [dict setObject:loginInfo.credentials.loginPassword forKey:kPassword];
    [dict setObject:loginInfo.deviceInfo.deviceUUID forKey:kDeviceUUID];
    [dict setObject:[NSString stringWithFormat:@"Hardware- Model-%@ OSVersion-%@",loginInfo.deviceInfo.deviceModel,loginInfo.deviceInfo.iOSVersion]  forKey:kDeviceInformation];
    [dict setObject:loginInfo.pushNotificationInfo.pushToken forKey:kPushToken];
    [dict setObject:loginInfo.deviceInfo.deviceType forKey:kDeviceType];
    [dict setObject:loginInfo.deviceInfo.devicePlatform forKey:kDevicePlatform];
    
    
    return dict;
}

#pragma mark - Change Password params
+ (NSMutableDictionary *)prepChangePasswordParams:(NSString *)oldPW newPassword:(NSString *)newPW
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:oldPW forKey:kChangeOldPassword];
    [dict setObject:newPW forKey:kChangeNewPassword];
    return dict;
}


#pragma mark - Forget Password params
+ (NSMutableDictionary *)prepForgetPasswordParams:(NSString *)email
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:email forKey:kEmailAddress];
    return dict;
}

#pragma mark - Make Exception error log parameters

+ (NSDictionary *)prepExceptionErrorLogParams:(UncaughtExceptionHandler *)exceptionInfo
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:exceptionInfo.logHelper.deviceInfo.devicePlatform forKey:kDevicePlatform];
    [dict setObject:exceptionInfo.logHelper.currentModule forKey:kModule];
    [dict setObject:exceptionInfo.logHelper.deviceInfo.deviceUUID forKey:kDeviceUUID];
    [dict setObject:exceptionInfo.logHelper.deviceInfo.deviceType forKey:kDeviceType];
    [dict setObject:exceptionInfo.logHelper.appInfo.appVersion forKey:kAppVersion];
    [dict setObject:[NSString stringWithFormat:@"Hardware- Model- %@ OSVersion- %@", exceptionInfo.logHelper.deviceInfo.deviceModel, exceptionInfo.logHelper.deviceInfo.iOSVersion]forKey:kDeviceInformation];
    [dict setObject:exceptionInfo.errorDescription forKey:kErrorDesc];
    return dict;
}

@end
