//
//  WebserviceHelper.h
//  JTIStarPartners
//
//  Created by Shekhar  on 11/12/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginInfo.h"
#import "AppInfo.h"
#import "Qrcode.h"
#import "UserCredentials.h"
#import "WebServiceManager.h"
#import "MenuItem.h"

#import "CompanyInfo.h"
#import "CompanyAddress.h"
#import "Member.h"

#import "UncaughtExceptionHandler.h"

@interface WebserviceHelper : NSObject

//HEADERS
+ (NSDictionary *)prepRequestHeader;
+ (NSDictionary *) prepPostLoginHeader;
+ (NSDictionary *) prepPagesHeader;

//+ (NSDictionary *)prepPreLoginRequestHeader;

//PRE_LOGIN PARAMETERS
+ (NSMutableDictionary *)prepSessionCheckParams:(NSString *)sessionToken;
+ (NSMutableDictionary *)prepLoginParams:(LoginInfo *)loginInfo;
+ (NSMutableDictionary *)prepForgetPasswordParams:(NSString *)email;
+ (NSDictionary *)prepRegistrationParams;

//CHANGE PASSWORD
+ (NSMutableDictionary *)prepChangePasswordParams:(NSString *)oldPW newPassword:(NSString *)newPW;

//EXCEPTION ERROR LOG
+ (NSDictionary *)prepExceptionErrorLogParams:(UncaughtExceptionHandler *)exceptionInfo;

@end
