//
//  WebserviceManager.h
//  JTIStarPartners
//
//  Created by Shekhar on 20/11/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

#import "UpdateHUD.h"
#import "Constants.h"
#import "WebServiceManager.h"
#import "WebServiceResponse.h"
#import "WebServiceRequest.h"
#import "WebserviceHelper.h"

@class UserProfile;
@class LoginInfo;

@class UncaughtExceptionHandler;

@class WebServiceManager;
@class WebServiceResponse;

typedef void (^FinishBlock)(BOOL);

#define mWebServiceManager	((WebServiceManager *) [WebServiceManager sharedInstance])

typedef enum {
    kProfilePicUpload         = 1,
} ImageUploadType;


@protocol WebServiceManagerDelegate <NSObject>

@optional

- (void)webserviceManager:(WebServiceManager *)aManager processedData: (WebServiceResponse *) aResponse;
- (void)webserviceManager:(WebServiceManager *)aManager processFailed:(WebServiceResponse *) aResponse;
- (void)processCompleted:(WebServiceResponse *)response;
- (void)processFailed:(WebServiceResponse *)response;

@end

@interface WebServiceManager : NSObject
{
}

@property (nonatomic,strong) AFHTTPSessionManager *manager;
@property (nonatomic,strong) NSString  *requestURL;
@property (nonatomic,strong) NSDictionary  *requestHeaders;
@property (nonatomic,strong) NSDictionary  *requestParams;
@property (nonatomic,strong) NSMutableURLRequest *mutableRequest;
@property (nonatomic,strong) NSNumber *serviceReference;
@property (nonatomic,strong) NSString *imageUploadPath;
@property (nonatomic,strong) NSDictionary *imageInfoDict;
@property (nonatomic,strong) NSData *imageData;
@property (nonatomic,strong) NSString *requestBody;
@property (nonatomic,assign) RequestMethod methodType;
@property (nonatomic,strong) UIView *activeView;
@property (nonatomic,strong) NSString *msgHUD;


- (void)downloadImage;
- (void)executeURLRequest;

- (void)cancelCurrentOperations;

- (void)processSucceeded:(id)object :(NSNumber *)ref;
- (void)processFail:(NSError *)error :(NSNumber *)ref;

- (void)successRequest:(id)responseObj request:(WebServiceRequest*)request withServiceIndentifier:(NSNumber *)reference;
- (void)failedRequest:(NSError *)error request:(WebServiceRequest*)request withServiceIndentifier:(NSNumber *)reference;
+ (WebServiceManager *)sharedInstance;

+ (NSString *) server_Prefix;
+ (NSString *) serverNoApi_Prefix;
+ (NSString *) secret_token;

@property (nonatomic,weak) NSObject  <WebServiceManagerDelegate>  *delegate;

//Webservice Calls




#pragma mark Pre-login
-(void) requestLookupTable:(NSDictionary *) lookupInfo
                      view:(UIView<WebServiceManagerDelegate> *)view;
-(void) requestLookupTableInVC:(NSDictionary* ) lookupInfo
                            vc:(UIViewController<WebServiceManagerDelegate> *)vc;


-(void) requestConsumerTNC:(NSString *) versionNumber
                      view:(UIView<WebServiceManagerDelegate> *)view;
-(void) requestWorkshopTNC:(NSString *) versionNumber
                      view:(UIView<WebServiceManagerDelegate> *)view;
-(void) requestDSRTNC:(NSString *) versionNumber
                 view:(UIView<WebServiceManagerDelegate> *)view;

-(void) loadLanguage:(NSString *) lookupVersion
                      view:(UIView<WebServiceManagerDelegate> *)view;

-(void) loadState:(NSString *) lookupVersion
      countryCode:(NSString *) countryCode
               vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)login:(LoginInfo *)loginInfo vc:(UIViewController <WebServiceManagerDelegate> *)vc;

- (void)validateMobileNumber: (NSString *)mobNum
                 countryCode: (NSString*)countryCode
                          vc: (UIViewController<WebServiceManagerDelegate> *)vc;

- (void)validateOTP:(NSString*)otp
        countryCode:(NSString*)otpCountryCode
             mobNum:(NSString *)otpMobNum
                 vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)updateLanguage:(NSString *)languageCode vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)registerTradePreLoginOne: (NSDictionary *)registerInfo
                              vc: (UIViewController<WebServiceManagerDelegate> *)vc;

- (void)registerTradePreLoginTwo: (NSDictionary *)registerInfo
                              vc: (UIViewController<WebServiceManagerDelegate> *)vc;

- (void)registerMechanic: (NSDictionary *)registerInfo
                      vc: (UIViewController<WebServiceManagerDelegate> *)vc;

- (void)addMechanic: (NSDictionary *)registerInfo
                 vc: (UIViewController<WebServiceManagerDelegate> *)vc;

- (void)forgetPassword: (NSString *)mobNum
           countryCode: (NSString*)countryCode
                    vc: (UIViewController<WebServiceManagerDelegate> *)vc;

- (void)changePassword: (NSString *)mobNum
           countryCode:(NSString*)countryCode
              password:(NSString*)password
                    vc: (UIViewController<WebServiceManagerDelegate> *)vc;

-(void) sendContactForm:(NSDictionary* ) contactFormData
                     vc: (UIViewController<WebServiceManagerDelegate> *)vc;

- (void) isValidAuthToken:(UIView<WebServiceManagerDelegate> *)view;

#pragma mark Trade
- (void)fetchHome:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void) fetchHomeSession;

-(void) fetchHomePromotion:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchRewardsBannerForHome:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchTradeSpeedometerDetails:(UIViewController<WebServiceManagerDelegate> *) vc;

- (void)fetchTransactionHistory: (NSDictionary *)transactDict
                             vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)fetchPendingWorkshopList:(NSString *) keyword
                     searchRange:(NSString *) searchRange
                              vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)fetchApprovedWorkshopList:(NSString *) keyword
                     searchRange:(NSString *) searchRange
                              vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)fetchINWorkshopList:(NSString *) keyword
                searchRange:(NSString *) searchRange
          profileTypeFilter: (NSString *) filterKeyCode
                         vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)fetchRegisteredWorkshopList:(NSArray *) registerWorkshopList
workShopname:(NSString *) workShopName
                                 vc:(UIViewController<WebServiceManagerDelegate> *)vc;


- (void)fetchRegisteredPendingWorkshopList:(NSString *) keyword
                     searchRange:(NSString *) searchRange
                              vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)fetchRegisteredApprovedWorkshopList:(NSString *) keyword
                     searchRange:(NSString *) searchRange
                              vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)fetchINRegisteredWorkshopList:(NSString *) keyword
                searchRange:(NSString *) searchRange
          profileTypeFilter: (NSString *) filterKeyCode
                         vc:(UIViewController<WebServiceManagerDelegate> *)vc;


- (void)fetchLeads:(NSString *) leadID
                vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)fetchTrade:(NSString *) tradeID
                vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)fetchcountryDial:(NSString*)countryDial mobileNo:(NSString*)mobNum
                      vc: (UIViewController<WebServiceManagerDelegate> *)vc;

- (void)fetchOtpUpdatecountryDial:(NSString*)countryDial
     mobileNo:(NSString*)mobNum otp:(NSString*)otp
                               vc: (UIViewController<WebServiceManagerDelegate> *)vc;

- (void)fetchRequestOTPForMobile: (NSString *)mobNum
countryCode:(NSString*)countryCode
                              vc: (UIViewController<WebServiceManagerDelegate> *)vc;


- (void)fetchMechanicDetail:(NSString *) tradeID
                         vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)fetchRegMechanicDetail:(NSString *) RegistrationID
                            vc:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void)updateMechanicRegistration:(NSDictionary *) registrationInfo
                                vc:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void)updateMechanic:(NSDictionary *) registrationInfo
                    vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)addNewWorkshop: (NSDictionary *)workshopInfo
                    vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)updateWorkshop: (NSDictionary *)workshopInfo
                    vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)uploadGalleryPhotos: (NSString *) tradeID
                 isApproved:(BOOL) isApproved
                  dataFiles: (NSArray *) dataFiles
                         vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)updateApprovedWorkshop: (NSDictionary *)workshopInfo
                            vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)loadProfiling: (NSString *) idString
            isPending:(BOOL) isPending
                   vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void) fetchWorkshopPerformanceByDSR: (NSString *)tradeID
                                  view: (UIViewController<WebServiceManagerDelegate> *)vc;

- (void) fetchDSRPerformance:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void) fetchTradeOwnerPerformance:(UIViewController<WebServiceManagerDelegate> *)vc;

#pragma mark To Points
-(void) fetchTOPointsTransactionHistory: (NSDictionary *)transactDict
                                     vc:(UIViewController<WebServiceManagerDelegate> *)vc;

-(void) fetchTOPointsTransactionDetails: (NSDictionary *)transactDict
                                     vc:(UIViewController<WebServiceManagerDelegate> *)vc;

#pragma mark Survey
-(void) closeSurveyPopup: (NSDictionary *) surveyDict
                      vc:(UIViewController<WebServiceManagerDelegate> *)vc;


#pragma mark Profile
- (void)updateTradeProfile: (NSDictionary *)tradeInfo
                        vc:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void)updateBankDetails: (NSDictionary *)tradeInfo
                       vc:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void)updateDocumentDetails: (NSDictionary *)tradeInfo
                       vc:(UIViewController<WebServiceManagerDelegate> *)vc;

#pragma mark Decal
- (void)tagDecalCodeToTradeOwner: (NSArray *)decalCodesArray
                              vc:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void)searchVehicleWithDecal:(NSString *) decalCodeString
                            vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)tagVehicleWithDecalCode: (NSDictionary *)decalTagDict
                             vc:(UIViewController<WebServiceManagerDelegate> *)vc;

#pragma mark Cash Incentive
- (void)fetchCashHistoryFromDate:(NSString*) fromDate
                          toDate:(NSString*) toDate
                       forStatus:(NSString*) status
                  forServiceType:(NSString*) serviceType
                              vc:(UIViewController<WebServiceManagerDelegate> *)vc;

#pragma mark Vehicle
- (void)searchVehicle: (NSString *) keyword
                state: (NSString *) stateKeycode
                   vc:(UIViewController<WebServiceManagerDelegate> *)vc;


- (void)inviteConsumer: (NSDictionary *) consumerInfo
                    vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)submitPromoCode:(NSString *)code
          vehicleNumber:(NSString *)vehicleNumber
                     vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)submitCode: (NSDictionary *)submitData
                vc:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void)submitCodesForSellout : (NSDictionary *) submitData
                        vc : (UIViewController<WebServiceManagerDelegate> *) vc;


- (void)fetchMessageList:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void)removePushMessageWithList:(NSArray *)listArray vc:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void)readPushMessage: (NSString *)pushId
                     vc:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void)openPushMessageInSession: (NSString *)pushId;

- (void)fetchFAQ:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)fetchTNC:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void)acceptTNC:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void)acceptPrivacyPolicy:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)fetchPushStatus:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void)updatePushStatus:(BOOL) isPushing
                      vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)changePasswordPostLogin: (NSString *)oldPassword
                    newPassword: (NSString*)newPassword
                             vc: (UIViewController<WebServiceManagerDelegate> *)vc;


- (void)inviteTrade: (NSString *)companyName
        mobileNumber: (NSString* )mobNum
      companySource: (NSString *)companySource
                 vc: (UIViewController<WebServiceManagerDelegate> *)vc;

- (void) fetchRewardsList:(NSDictionary *) listDict
                       vc:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void) fetchRewardsDetails:(NSString *) categoryCode
                    itemCode:(NSString *) itemCode
                          vc:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void) fetchPointsSummary:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void) fetchWishList:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void) addWishItem:(NSString *) categoryCode
            itemCode:(NSString *) itemCode
                  vc:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void) removeWishItem:(NSString *) wishID
                     vc:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void) confirmOrder:(NSArray *) redemptionCart
       rewardsAddress:(NSDictionary *) rewardsAddress
                   vc:(UIViewController<WebServiceManagerDelegate> *)vc;
//v2
- (void) fetchRewardsHome: (UIViewController<WebServiceManagerDelegate> *)vc;
- (void) fetchRewardsRedemptionHistoryWithDict:(NSDictionary *)dict vc: (UIViewController<WebServiceManagerDelegate> *)vc;
- (void) fetchRewardsRedemptionDetailsWithId:(NSString *)redemptionRefId vc: (UIViewController<WebServiceManagerDelegate> *)vc;
- (void)fetchRUProductCategory:(UIViewController<WebServiceManagerDelegate> *)vc;


- (void) getPromoDetails:(NSString *) promoCode
                      vc:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void) submitPromoCode: (NSDictionary *) promoCodeDict
                      vc:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void)fetchPromoRedemptionHistory:(NSDictionary *) redemptionHistoryDict
                                 vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void) fetchShareProducts:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void) fetchAllParticipatingProducts:(UIViewController<WebServiceManagerDelegate> *)vc;

#pragma mark - In App Asset Function
-(void) fetchInAppAssetCategoryWithDictionary: (NSDictionary *) dict vc: (UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchInAppAssetListWithDictionary: (NSDictionary *) dict vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) readAssetWithAssetId: (NSString *) assetId vc:(UIViewController<WebServiceManagerDelegate> *)vc;


//Workshop offer
-(void) fetchAncillaryOfferVcWithOfferStatus:(NSString *)offerStatus offerType:(NSString *)offerType vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) cancelAncillaryOfferVcWithOrderDetails:(NSDictionary *)offerDetailsDict vc:(UIViewController<WebServiceManagerDelegate> *)vc;

-(void) fetchCurrentOfferVc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchOfferDetailVc:(UIViewController<WebServiceManagerDelegate> *)vc offerDict :(NSDictionary *)offerDict;
-(void) fetchPastOfferVc:(UIViewController<WebServiceManagerDelegate> *)vc startDate :(NSString *)startDate endDate:(NSString *)endDate offerType:(NSString *)offerType;
-(void) fetchWorkshopOilDetailVc:(UIViewController<WebServiceManagerDelegate> *)vc input:(NSDictionary *)inputDict;
-(void) submitToGetAncillaryOffer:(UIViewController<WebServiceManagerDelegate> *)vc submitDict :(NSDictionary *)submitDict;
-(void) fetchPendingOrder:(UIViewController<WebServiceManagerDelegate> *)vc;

//Order
-(void) fetchAllOrders:(UIViewController<WebServiceManagerDelegate> *)vc dict:(NSDictionary *)dict;
-(void) fetchOrderDetails:(UIViewController<WebServiceManagerDelegate> *)vc orderID:(NSString *)orderID;
-(void) fetchProductCategoryList:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchAllProductsInCategory:(UIViewController<WebServiceManagerDelegate> *)vc productID:(NSString *)productID;
-(void) addNewOrder:(UIViewController<WebServiceManagerDelegate> *)vc submitDict:(NSDictionary *)submitDict;
-(void) updateOrderStatus:(UIViewController<WebServiceManagerDelegate> *)vc submitDict:(NSDictionary *)submitDict;

//UK Workshop Profiling
-(void) fetchWorkshopProfilingDetailsWithTradeID: (NSString *)tradeID vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) updateWorkshopProfilingDetailsWithDictionary:(NSDictionary *)dictionary vc:(UIViewController<WebServiceManagerDelegate> *)vc;

#pragma mark RESEND OTP

-(void) resendOtpWithDictionary:(NSDictionary *)dictionary vc:(UIViewController<WebServiceManagerDelegate> *)vc;



//Russia
-(void) submitPointsCalculator:(NSDictionary *)dictionary vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchRURewardsHome:(UIViewController<WebServiceManagerDelegate> *)vc;

//Register Consumer
- (void) submitMemberRegistrationGetOTP: (NSDictionary *) params
                                   view: (UIViewController<WebServiceManagerDelegate> *)vc;
- (void) submitMemberRegistrationStepOne: (NSDictionary *) params
                                    view: (UIViewController<WebServiceManagerDelegate> *)vc;
- (void) submitMemberRegistrationStepTwo: (NSDictionary *) params
                                    view: (UIViewController<WebServiceManagerDelegate> *)vc;
- (void) submitMemberVehicles:(NSDictionary  *) params view:(UIViewController<WebServiceManagerDelegate> *)vc;


//RU submit member
- (void)submitMember: (NSDictionary *)submitData
                  vc:(UIViewController<WebServiceManagerDelegate> *)vc;

- (void)fetchPerformanceMetrics:(UIViewController<WebServiceManagerDelegate> *)vc;
- (void)fetchWaysToEarnPoints:(UIViewController<WebServiceManagerDelegate> *)vc;

//RU Ancilliary
#pragma mark - RU Ancilliary
-(void) submitRUAncillaryOfferWithDict:(NSDictionary *) ancilliaryDict vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) submitAncillaryOfferWithDict:(NSDictionary *) ancilliaryDict vc:(UIViewController<WebServiceManagerDelegate> *)vc;

//RU Workshop Performance
#pragma mark - RU Workshop Performance
-(void) fetchPerformanceV2: (UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchOrdersBreakDown:(NSDictionary *) tradeDict vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchPointsBreakDown:(NSDictionary *) tradeDict vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchLeaderboard:(NSDictionary *) tradeDict vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchStaffBreakdown:(NSDictionary *) tradeDict vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchOilChangeBreakdown:(NSDictionary *) tradeDict vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchCustomerBreakdown:(NSDictionary *) tradeDict vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchOfferRedemptionBreakdown:(NSDictionary *) tradeDict vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchApprovedWorkshop:(NSDictionary *) tradeDict vc:(UIViewController<WebServiceManagerDelegate> *)vc;

//IN fetch address from postal code
-(void) fetchPostalAddress:(NSString *) postalCodeString vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchBankDetails:(NSString *) ifscString vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) addIM:(NSDictionary *) imDict vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) updateIM:(NSDictionary *) imDict vc:(UIViewController<WebServiceManagerDelegate> *)vc;

#pragma mark - Russia V2
-(void) fetchCTODetailsFor:(NSString *)keywordSearch onVc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchWaysToEarnPointsFor:(NSString *)businessTypeId onVc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchPerformanceMetricsFor:(NSString *)businessTypeId onVc:(UIViewController<WebServiceManagerDelegate> *)vc;

-(void) fetchCTODetailsOnePlusOneFor:(NSString *)keywordSearch onVc:(UIViewController<WebServiceManagerDelegate> *)vc;


//new performance
#pragma mark - New Performance
-(void) fetchPerformanceSummary:(NSDictionary *) filterDataDict  vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchSectionBreakdown:(NSDictionary *) filterDataDict vc:(UIViewController<WebServiceManagerDelegate> *)vc;

//LoyaltyProgram
#pragma mark - Loyalty
-(void) loadLoyaltyContractList:(NSDictionary *) searchParameters vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchLoyaltyWorkshopList:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchLoyaltyCatalogueListWithTradeID:(NSString *)tradeID vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) loadLoyaltyPackageList:(NSDictionary *) preferedParams vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) submitNewContract:(NSDictionary *) submitParams vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) loadLoyaltyContractDetails:(NSString *)contractRef  vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchLoyaltyProgramPerf:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchLoyaltyPerformaceForContract:(NSDictionary *)contractDict vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) updateContract:(NSDictionary *) submitParams vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) extendContract:(NSDictionary *) submitParams vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) continueContract:(NSDictionary *) submitParams vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) terminateContract:(NSDictionary *) submitParams vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) completeContract:(NSDictionary *) submitParams vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) updateSignatureForContract:(NSDictionary *)submitParams vc:(UIViewController <WebServiceManagerDelegate> *)vc;
//TH Loyalty
-(void) loadLoyaltyTierPoints:(UIViewController<WebServiceManagerDelegate> *)vc;

//Promotion - Redeem
-(void) promotionRedeem:(UIViewController<WebServiceManagerDelegate> *)vc;

#pragma mark - Company Category
-(void) loadCompanyCategory:(UIViewController<WebServiceManagerDelegate> *)vc;

#pragma mark - LubeMatch
-(void) loadSearchVehicleLubeDetails:(NSDictionary *)dict vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) submitLubematchRegistration:(NSDictionary *)dict vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchOatsQueryWithString:(NSString *) queryString vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchOatsEquipmentWithHrefString:(NSString *) hrefString vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchOatsVehicleType:(UIViewController<WebServiceManagerDelegate> *)vc;

#pragma mark - Earn N Redeem Module
-(void) loadEarnNRedeem:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchScanNRewardHistory:(NSDictionary *)dict onVc:(UIViewController<WebServiceManagerDelegate> *)vc;

#pragma mark - Gamification
-(void) viewPromotionDetailsForPromotion:(UIViewController<WebServiceManagerDelegate> *) vc ;
-(void) showPromotionResultFor:(NSDictionary *)dict onVC:(UIViewController<WebServiceManagerDelegate> *) vc ;
-(void) viewGamificationDetails:(UIViewController<WebServiceManagerDelegate> *) vc;
-(void) viewHelixGamificationDetails:(UIViewController<WebServiceManagerDelegate> *) vc;
-(void) checkInToday:(NSDictionary *)dict vc:(UIViewController<WebServiceManagerDelegate> *) vc;
-(void) playGrandPrizeInGamification:(UIViewController<WebServiceManagerDelegate> *) vc;

#pragma mark - Tiering
-(void) fetchCompanyTier:(UIViewController <WebServiceManagerDelegate> *)vc;

#pragma mark - AI
-(void) fetchAIConverstaion:(UIViewController <WebServiceManagerDelegate> *) vc;
-(void) fetchAIOilChangeCount:(UIViewController <WebServiceManagerDelegate> *) vc;

#pragma mark - Logout
#pragma mark - Moscow
-(void) fetchTOInventoryList:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchDSRInventoryList:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchDSRWorkshopList:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchMSParticipatingProducts:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) submitCodesMS:(NSDictionary *)submitParams vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) submitScanOutCodesMS:(NSDictionary *)submitParams vc:(UIViewController<WebServiceManagerDelegate> *)vc;
-(void) fetchPerformanceV3: (UIViewController<WebServiceManagerDelegate> *)vc;

//LOGOUT
- (void)logout:(UIViewController<WebServiceManagerDelegate> *)vc;


@end
