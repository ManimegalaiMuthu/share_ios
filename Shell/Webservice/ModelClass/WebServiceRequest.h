//
//  WebServiceRequest.h
//  JTIStarPartners
//
//  Created by Shekhar on 20/11/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Constants.h"

typedef enum {
    GET     = 1,
    PUT     = 2,
    POST    = 3,
    DELETE  = 4
}   RequestMethod;


#define DEVELOPMENT_SERVERNOAPI_PREFIX [GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA] ? @"http://shell-ru-dev.surfgold.net/services/" : @"http://shell-dev.ersg-staging.com/services/"
//#define STAGING_SERVERNOAPI_PREFIX [GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA] ? @"http://shell-ru-dev.surfgold.net/services/" : @"http://shell-stg.ersg-staging.com/services/"

#define STAGING_SERVERNOAPI_PREFIX [GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA] ? @"http://shell-ru-dev.ersg-staging.com/services/" : @"http://shell-stg.ersg-staging.com/services/"

#define LIVE_SERVERNOAPI_PREFIX [GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA] ? @"https://share.shell.com.ru/services/" : @"https://share.shell.com/services/"
#define BETA_SERVERNOAPI_PREFIX [GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA] ? @"https://share.shell.com.ru/beta/" : @"https://share.shell.com/services/"

////uat
////#define STAGING_SERVERNOAPI_PREFIX @"http://shell-service.surfgold.net/"
////https://share.shell.com/beta/
////staging
////http://shell-stg.ersg-staging.com/services/
//#define STAGING_SERVERNOAPI_PREFIX [GET_COUNTRY_CODE isEqualToString: COUNTRYCODE_RUSSIA] ? @"http://shell-ru-dev.surfgold.net/services/" : @"http://shell-stg.ersg-staging.com/services/"

#define SERVER_PREFIX [NSString stringWithFormat: @"%@%@", [WebServiceManager serverNoApi_Prefix], @"api/"]

//Prelogin APIs
#define PRELOGIN_PREFIX @"PreLogin/"
#define PRELOGIN_LOGIN_URL @"Login"
#define PRELOGIN_LOOKUP_URL @"LoadLookUp"
#define PRELOGIN_LOADLANGUAGE_URL @"LoadLanguage"
#define PRELOGIN_LOADSTATE_URL @"LoadState"

#define PRELOGIN_MOBILENUM_URL @"RegistrationStep1"
//#define PRELOGINOTP_URL @"RegistrationStep2"
#define PRELOGIN_TRADEREGISTRATION_URL @"TradePreRegistration"
//#define PRELOGINFORGETPASSWORD_URL @"ForgetPassword"
#define PRELOGIN_CONTACTUS_URL @"ContactUS" //use both pre and post login headers
#define PRELOGIN_TRADEREGISTRATION2_URL @"TradePreRegistrationStep2"

#define POSTLOGIN_LOGOUT //use prelogin prefix
#define PRELOGIN_MECHANICREGISTRATION_URL @"MechanicPreRegistration"
#define PRELOGIN_VALIDATEOTP_URL @"ValidateOTP"

#define PRELOGIN_FORGETPASSWORD_URL     @"ForgetPassword"
#define PRELOGIN_CHANGEPASSWORD_URL     @"ChangePassword"
#define PRELOGIN_READPUSHMESSAGE_URL     @"ReadPushMessage"
#define PRELOGIN_OPENPUSHMESSAGE_URL     @"OpenPushMessage"
#define PRELOGIN_VALIDAUTHTOKEN_URL     @"isValidAuthToken"
#define PRELOGIN_UPDATELANGUAGE_URL     @"UpdateLanguage"


#define PRELOGIN_LOGOUT_URL @"Logout"

//Remove Content-Type application/json from Post Login Header
#define PAGES_PREFIX @"Pages/"
#define PAGES_FAQ_URL @"Faq.aspx"
#define PAGES_TNC_URL @"TermCondition.aspx"
#define PAGES_OTHERPRODUCT_URL @"OtherProducts.aspx"
#define PAGES_PARTICIPATINGPRODUCT_URL @"ParticipatingProducts.aspx"
#define PAGES_ADDORDER_URL @"AddOrder.aspx"
#define PAGES_REWARDS_URL @"Rewards.aspx"
#define PAGES_VIEWORDER_URL @"ViewOrder.aspx"
#define PAGES_PENDINGORDER_URL @"PendingOrder.aspx"
#define PAGES_FILEUPLOAD_URL @"FileUpload.aspx"
#define PAGES_PRIVACY_URL @"PrivacyPolicy.aspx"
#define PAGES_LEADERBOARD_URL @"DSRPerformanceView.aspx"
#define PAGES_SURVEYFORM_URL @"SurveyForm.aspx"
#define PAGES_INAPPNOTIFICATION_URL @"PushNotification.aspx"
#define PAGES_TNC_LOYALTY_URL @"TermCondition_LP.aspx"
#define PAGES_SWA_URL @"GoMo.aspx"
#define PAGES_RU_PRIVACYPOPUP_URL @"PrivacyPolicyForCustomerInvite.aspx"
#define PAGES_COINSPROMO    @"coinpromo.aspx"
#define PAGES_JOINCOIN      @"Profile_popup_EN.aspx"
#define PAGES_COINTAX       @"Coinback_popup_EN.aspx"


//Trade APIs
#define TRADE_PREFIX @"Trade/"
#define TRADE_FETCHHOME_URL                     @"FetchHome"
#define TRADE_FETCHHOMEPROMOTION_URL                    @"FetchHomePromotion"
#define TRADE_FETCHWORKSHOPPENDINGLISTV2_URL             @"FetchPendingWorkshopList_V2"
#define TRADE_FETCHWORKSHOPAPPROVEDLISTV2_URL             @"FetchApprovedWorkshopList_V2"

#define TRADE_FETCHREGISTEREDWORKSHOPS_URL             @"FetchRegisteredWorkshops"
#define TRADE_REGISTEREDWORKSHOPS_URL             @"RegisterWorkshop"

#define TRADE_FETCHLEADDEATILS_URL              @"FetchLeadDeatils"
#define TRADE_FETCHTRADEDEATILS_URL             @"FetchTradeDeatils"
#define TRADE_FETCHREGMECHANICDETAIL_URL        @"FetchRegMechanicDeatils"
#define TRADE_FETCHMECHANICDETAIL_URL           @"FetchMechanicDeatils"
#define TRADE_UPDATEMECHANICREGISTRATION_URL    @"UpdateMechanicRegistration"
#define TRADE_UPDATEMECHANIC_URL                @"UpdateMechanic"
#define TRADE_ADDMECHANIC_URL                   @"AddMechanic"
#define TRADE_TRANSACTIONHISTORY_URL            @"FetchTransactionHistory"
#define TRADE_FETCHPARTICIPATINGPRODUCTS        @"FetchParticipatingProducts_V2"
#define TRADE_FETCHSHAREPRODUCTS                @"FetchShareParticipatingProducts_V2"
#define TRADE_FETCHSHAREPRODUCTS_V2             @"FetchShareParticipatingProducts_V2"
#define TRADE_FETCHPARTICIPATINGPRODUCTS_V2     @"FetchParticipatingProducts_V2"
#define TRADE_FETCHBANKDETAILS                  @"FetchBankDetails"
#define TRADE_FETCHCOMPANYTIER                  @"FetchFetchCompanyTier"

#define TRADE_FETCHPANBANKOTP_URL             @"SubmitPANandBankDetailsGetOTP"
#define TRADE_FETCHPANBANKOTPUPDATE_URL             @"SubmitPANandBankUpdateStep1"
#define TRADE_RESENDBANKOTP     @"ResendBankOTP"



#define TRADE_LOADPROFILING_URL         @"LoadProfiling"
#define TRADE_UPDATEWORKSHOP_URL        @"UpdateWorkshop"
#define TRADE_UPDATETRADE_URL           @"UpdateTrade"

#define TRADE_UPDATETRADEPROFILE_URL        @"UpdateTradeProfile"
#define TRADE_UPDATEBANKDETAILS_URL         @"UpdateBankDetails"
#define TRADE_UPDATEBANKDETAILS_URL_V2         @"UpdateBankDetailsV2"
#define TRADE_UPDATEDOCUMENTDETAILS_URL        @"updateDocumentDetails"

#define TRADE_DSRPERFORMANCE_URL            @"DSRPerformance"
#define TRADE_WORKSHOPPERFORMANCE_URL       @"WorkshopPerformance"
#define TRADE_WORKSHOPPERFORMANCEBYDSR_URL  @"WorkshopPerformanceByDSR"

#define TRADE_CASHINCENTIVE_URL         @"FetchCashHistory"
#define TRADE_SEARCHVEHICLE_URL         @"SearchVehicle"
#define TRADE_INVITECONSUMER_URL        @"InviteConsumer"
#define TRADE_PROMOCODE_URL             @"SubmitPromoCode" //for vehicle

#define TRADE_SUBMITCODES_URL           @"SubmitCodes"
#define TRADE_SUBMITCODES_SELLOUT_URL   @"SubmitCodes_SellOut"

#define TRADE_ACCEPTTC_URL              @"AcceptTC"
#define TRADE_ACCEPTPRIVACY             @"AcceptPrivacyPolicy"
#define TRADE_UPDATEPUSHSTATUS_URL      @"UpdatePushStatus"
#define TRADE_CHANGEPASSWORD_URL        @"ChangePassword"

//DECAL
#define TRADE_DECALTAGVEHICLE           @"SubmitDecalCodeToTagVehicle"
#define TRADE_FETTCHDECALSCAN           @"FetchVehicleDetailsScanDecalTag"
#define TRADE_DECALTRADEOWNER             @"SubmitDecalCodeTradeOwner"


//#define TRADE_UPDATEMECHANIC_URL        @"UpdateMechanic"

//#define TRADE_UPDATEOWNER_URL           @"UpdateTrade"
//#define TRADE_FETCHMECHDEATILS          @"FetchMechanicDeatils"       //Input: @"TradeID"
//#define TRADE_FETCHREGMECHDEATILS       @"FetchRegMechanicDeatils"    //Input: @"RegistrationID"
//#define TRADE_UPDATEMECHANICREG         @"UpdateMechanicRegistration" //Input: @"RegistrationID"
//#define TRADE_ADDMECHANIC               @"AddMechanic"
//#define TRADE_UPDATEMECHANIC            @"UpdateMechanic"             //Input: @"TradeID"

#define TRADE_UPDATECOMPANYDETAILS      @"UpdateCompanyDetails"
#define TRADE_UPDATETRADEPROFILE        @"UpdateTradeProfile"

#define TRADE_SUBMITPROMOCODE           @"SubmitPromoCode"  //Input:  @"VehicleNumber", @"Code"
#define TRADE_INVITETRADE_URL           @"InviteTrade"
#define TRADE_FETCHWORKSHOPDETAILSUK     @"FetchWorkshopDeatilsUK"
#define TRADE_UPDATEWORKSHOPDETAILSUK     @"UpdateWorkshopDetailsUK"

#define TRADE_SUBMITMEMBER_OTP @"SubmitMemberRegistrationGetOTP"
#define TRADE_SUBMITMEMBER_STEP1 @"SubmitMemberRegistrationStep1"
#define TRADE_SUBMITMEMBER_STEP2 @"SubmitMemberRegistrationStep2"
#define TRADE_SUBMITMEMBERVEHICLES  @"SubmitMemberVehicles"

#define TRADE_SUBMITMEMBER @"SubmitMemberRegistration"    //Russia
#define TRADE_SUBMITPOINTSCALC @"SubmitPointCalculator"    //Russia


#define TRADE_WAYSTOEARN @"FetchWaysToEarnPoints"
#define TRADE_PERFORMANCEMETRICS @"FetchPerformanceMetrics"
#define TRADE_FETCHPRODUCTCATEGORY @"FetchProductCategory"

#define TRADE_LOADTNCCONSUMER_RU @"LoadTnCPrivacy_Consumer_RU"
#define TRADE_LOADTNCWORKSHOP_RU @"LoadTnCPrivacy_WS_RU"
#define TRADE_LOADTNCDSR_RU @"LoadTnCPrivacy_DSR_RU"

#define TRADE_RESENDOTP @"ResendOTP"
//Russia_V2
#define TRADE_FETCHCTODETAILS           @"FetchCTODetails"
#define TRADE_FETCHWAYSTOEARNPOINTS     @"FetchWaysToEarnPointsV1"
#define TRADE_FETCHPERFORMANCEMETRICS   @"FetchPerformanceMetricsV1"

//Russia_V2 SPIRAX&GADUS
#define TRADE_FETCHCTODETAILS_ONEPLUSONE           @"FetchCTODetails_OnePlusOne"

//Lubematch
#define TRADE_LUBEMATCH_SEARCHVEHICLE               @"SearchVehicleLube"
#define TRADE_LUBEMATCH_SUBMITREGISTRATION          @"SubmitLubeRegistration"


#define PUSH_PREFIX                 @"PushNotification/"
#define PUSH_FETCHPUSHSTATUS_URL    @"FetchPushStatus"
#define PUSH_MESSAGELIST_URL        @"MessageList"
#define PUSH_REMOVE_URL             @"RemovePushNotification"

#define REWARDS_PREFIX                  @"Rewards/"
#define REWARDS_FETCHITEMSLIST_URL      @"FetchItemsList"
#define REWARDS_FETCHITEMSDETAILS_URL   @"FetchItemDetails"
#define REWARDS_FETCHPOINTSSUMMARY_URL  @"FetchPointsSummary"
#define REWARDS_ADDWISHITEM_URL         @"AddWishItem"
#define REWARDS_REMOVEWISHITEM_URL      @"RemoveWishItem"
#define REWARDS_FETCHWISHLIST_URL       @"FetchWishList"
#define REWARDS_CONFIRMORDER_URL        @"Confirmorder"
#define REWARDS_FETCHREWARDSFORHOME_URL                   @"FetchRewardsBannerForHome"
//v2
#define REWARDS_FETCHREWARDSHOME_URL      @"FetchRewards"
#define REWARDS_FETCHREDEMPTIONHISTORY_URL      @"FetchRedemptionHistory"
#define REWARDS_FETCHREDEMPTIONDETAILS_URL      @"FetchRedemptionDetails"
#define REWARDS_RUREWARDSHOME @"FetchRewardsHome_RU"    //Russia


#define PROMOTION_PREFIX                @"Promotion/"
#define PROMOTION_GETPROMODETAILS       @"GetPromoDetails"
#define PROMOTION_SUBMITPROMOCODE       @"SubmitPromoCode"
#define PROMOTION_FETCHREDEMPTIONHISTORY       @"FetchPromotionRedemptionHistory"
#define PROMOTION_REDEEMTRADEPROMOTION  @"RedeemTradePromotion"
#define PROMOTION_FETECHTRADESPEEDOMETERDETAILS @"FetchTradeSpeedometerDetails"


#define TOPOINTS_PREFIX                 @"ToPoints/"
#define TOPOINTS_TRANSACTIONHISTORY     @"FetchTOTransactionHistory"
#define TOPOINTS_TRANSACTIONDETAILS     @"FetchTOTransactionDetails"

#define LOYALTYPROGRAM_PREFIX                   @"LoyaltyContract/"
#define LOYALTYPROGRAM_LOADLOYALTYCONTRACTLIST  @"LoadLoyaltyContractList"
#define LOYALTYPROGRAM_FETCHWORKSHOPLIST        @"FetchWorkshopList"
#define LOYALTYPROGRAM_FETCHCATALOGUELIST        @"FetchCatalogueList"
#define LOYALTYPROGRAM_LOADLOYALTYPACKAGELIST    @"LoadPackageList"
#define LOYALTYPROGRAM_SUBMITNEWCONTRACT        @"SubmitContract"
#define LOYALTYPROGRAM_LOADLOYALTYCONTRACTDETAILS      @"LoadLoyaltyContractDetails"
#define LOYALTYPROGRAM_DSRPERFORMANCE           @"LoadDSRPerformance"
#define LOYALTYPROGRAM_WORKSHOPPERFORMANCE           @"LoadWorkshopBreakdown"
#define LOYALTYPROGRAM_UPDATECONTRACT           @"UpdateContract"
#define LOYALTYPROGRAM_EXTENDCONTRACT           @"ExtendContract"
#define LOYALTYPROGRAM_CONTINUECONTRACT           @"ContinueContract"
#define LOYALTYPROGRAM_TERMINATECONTRACT          @"TerminateContract"
#define LOYALTYPROGRAM_COMPLETECONTRACT          @"CompleteContract"
//TH
#define LOYALTYPROGRAM_TH_LOADPOINTS             @"LoadLoyaltyTierPointsTH"
//ID TO
#define LOYALTYPROGRAM_LOADLOYALTYCONTRACTLIST_TO       @"LoadLoyaltyContractListforTrade"
#define LOYALTYPROGRAM_LOADLOYALTYCONTRACTDETAILS_TO    @"LoadLoyaltyContractDetailsforTrade"
#define LOYALTYPROGRAM_UPDATEESIGNATURECONTRACT_TO      @"UpdateeSignatureContract"



#define SURVEY_PREFIX                    @"Survey/"
#define SURVEY_CLOSESURVEYPOPUP          @"CloseSurveyPopUp"

#define LOGEXCEPTIONERROR_URL @"ErrorLog"

#define LANGUAGE_SUFFIX @"&lang"

#define UPDATEPUSHSTATUS_URL @"UpdatePushStatus"

//orders
#define ORDER_PREFIX @"Order/"
#define FETCHALLORDERS @"FetchAllOrders"
#define FETCHORDERDETAILS @"FetchOrderDetails"
#define FETCHPRODUCTCATEGORY @"LoadProductCategoryList"
#define FETCHALLPRODUCTSINCATEGORY @"LoadProdListWithProdCategoryID"
#define ADDNEWORDER @"AddOrders"
#define UPDATESTATUS @"UpdatePendingOrders"
#define PENDINGORDER @"FetchPendingOrders"

//Workshop offer
#define CURRENTOFFER @"FetchCurrentOffers"
#define OFFERDETAIL @"FetchOfferDetails"
#define PASTOFFER   @"FetchPastOfferDetails"
#define WORKSHOPOIL @"FetchAncillaryProductGroup"
#define SUBMITANCILLARYOFFER @"SubmitToGetAncillaryOffer"
#define FETCHANCILLARYOFFER @"FetchAncilliaryOffers"
#define CANCELANCILLARYOFFER @"CancelAncillaryOffer"


#define PROMOTION_RUSUBMITANCILLARYOFFERS @"SubmitAncillaryOffers_RU" // RUSSIA
#define PROMOTION_SUBMITANCILLARYOFFERS @"SubmitAncillaryOffers"

//Workshop performance
#define TRADE_FETCH_PERFORMANCE_V2 @"FetchPerformance_V2"
#define TRADE_FETCH_LEADERBOARD @"FetchLeaderboard"
#define TRADE_FETCH_ORDERSBREAKDOWN @"FetchOrdersBreakDown"
#define TRADE_FETCH_POINTSBREAKDOWN @"FetchPointsBreakDown"
#define TRADE_FETCH_STAFFBREAKDOWN @"FetchStaffBreakDown"
#define TRADE_FETCH_OILCHANGEBREAKDOWN @"FetchOilChangeBreakDown"
#define TRADE_FETCH_CUSTOMERBREAKDOWN @"FetchCustomerBreakDown"
#define TRADE_FETCH_OFFERREDEMPTIONBREAKDOWN @"FetchOfferRedemptionBreakDown"
#define TRADE_FETCH_APPROVEDWORKSHOP @"FetchApprovedWorkshopList"

//IN services
#define TRADE_FETCH_POSTALADDRESS @"FetchPostalAddress"
#define TRADE_ADDIM @"AddIM"
#define TRADE_UPDATEIM @"UpdateIM"

#define TRADE_FETCH_PERFORMANCESUMMARY      @"FetchPerformanceSummary"
#define TRADE_FETCH_SECTIONBREAKDOWN        @"FetchSectionBreakDown"

//Company Category
#define TRADE_LOADCOMPANYCATEGORY           @"LoadCompanyCategory"

//Earn N Redeem Module
#define TRADE_EARNNREDEEM                   @"EarnandRedeem"
// Inventory/FetchInventoryListForTO
//Moscow modified sept 2020
#define MSTRADE_WORKSHOP_PREFIX             @"Trade/"
#define MSTRADE_PREFIX                     @"Inventory/" //@"MSTrade/"
#define MSTRADE_FETCHDSRINVENTORY          @"FetchInventoryListForDSR"//@"FetchDSRInventory"
#define MSTRADE_FETCHTOINVENTORY            @"FetchInventoryListForTO" //@"FetchTOInventory"
#define MSTRADE_FETCHDSRWORKSHOPLIST        @"FetchApprovedWorkshopList_V2"//@"FetchDSRWorkshoplist"
#define MSTRADE_FETCHPARTICIPATINGPRODUCTS  @"FetchParticipatingProducts"
#define MSTRADE_SUBMITCODEMS                @"SubmitInventoryScanIN" //@"SubmitCodesScanInMS"//
#define MSTRADE_SUBMITSCANOUTCODEMS         @"SubmitInventoryScanOut"
#define MSTRADE_FETCH_PERFORMANCE_V3        @"FetchPerformance_V3"

#define EARNNREDEEMREPORT_PREFIX            @"EarnandRedeemReport/"
#define EARNNREDEEMREPORT_FETCHREPORT       @"FetchEarnandRedeemReport"

#define GAMIFICATION_PREFIX      @"Gamification/"

#define GAMIFICATION_VIEWPROMOTIONDETAILS   @"ViewPromotionDetails"
#define GAMIFICATION_SHOWPROMOTIONRESULT    @"ShowPromotionResult"
#define GAMIFICATION_CHECKINVIEWDETAILS     @"GamificationViewDetails"
#define GAMIFICATION_CHECKINTODAY           @"CheckinToday"
#define GAMIFICATION_PLAYGRANDPRIZE         @"PlayGrandPrize"
#define GAMIFICATION_VIEWDETAILS            @"ViewDetails"

//In app assets
#define ASSET_PREFIX @"Asset/"
#define ASSET_FETCHASSETCATEGORYLIST @"FetchAssetCategoryList"
#define ASSET_FETCHASSETLIST      @"FetchAssetList"
#define ASSET_READ          @"ReadAsset"

//AI
#define AI_PREFIX                   @"AI/"
#define AI_FETCHAICOVERSATIONS      @"FetchAIConversations"
#define AI_FETCHAIOILCHANGECOUNT    @"FetchAIOilChangeCount"

typedef enum {
    kWEBSERVICE_LOGIN               = 1,
    
    kWEBSERVICE_MOBILEAUTH          = 2,    //mobauth
    kWEBSERVICE_REGISTEROTP         ,       //OTP
    kWEBSERVICE_TRADEREGISTER       = 10,
    kWEBSERVICE_TRADEREGISTERTWO    ,
    kWEBSERVICE_MECHREGISTER        ,
    
    kWEBSERVICE_FORGOTPASSWORD      = 40,
    kWEBSERVICE_CHANGEPASSWORD      ,
    
    kWEBSERVICE_FETCHHOME           = 70,
    kWEBSERVICE_FETCHWORKSHOPPENDINGLIST   = 100,
    kWEBSERVICE_FETCHWORKSHOPAPPROVEDLIST,
    kWEBSERVICE_REGISTERWORKSHOPLIST,
    kWEBSERVICE_FETCHLEADS          ,
    kWEBSERVICE_FETCHTRADE          ,
    kWEBSERVICE_FETCHSHAREPRODUCTS  ,
    kWEBSERVICE_FETCHALLPARTICIPATINGPRODUCTS,
    kWEBSERVICE_FETCHHOMEPROMOTION  ,
    kWEBSERVICE_FETCHREWARDSFORHOME ,
    kWEBSERVICE_FETECHTRADESPEEDOMETERDETAILS,
    kWEBSERVICE_FETCHCOMPANYTIER    ,
    kWEBSERVICE_FETCHGENERATEPANOTP ,
    kWEBSERVICE_FETCHUPDATEPANOTP ,
    kWEBSERVICE_FETCHREQUESTBANKOTP,

    kWEBSERVICE_FETCHREGMECHANIC    ,
    kWEBSERVICE_FETCHMECHANIC       ,
    kWEBSERVICE_ADDMECHANIC         ,
    
    kWEBSERVICE_ADDNEWWORKSHOP      ,
    kWEBSERVICE_UPDATEWORKSHOP      ,
    kWEBSERVICE_UPLOADGALLERYPHOTOS ,
    kWEBSERVICE_FETCHTRANSACTION    ,
    
    kWEBSERVICE_CASHINCENTIVE       ,
    
    kWEBSERVICE_LOADPROFILING       ,
    kWEBSERVICE_SEARCHVEHICLE       ,
    kWEBSERVICE_INVITECONSUMER      ,
    kWEBSERVICE_PROMOCODE           ,
    kWEBSERVICE_SUBMITCODE          ,
    kWEBSERVICE_SUBMITCODE_SELLOUT  ,
    kWEBSERVICE_WORKSHOPPERFORMANCE ,
    
    //russia
    kWEBSERVICE_WAYSTOEARN,
    kWEBSERVICE_PERFORMANCEMETRICS,
    kWEBSERVICE_FETCHPRODUCTCATEGORY,
    kWEBSERVICE_SUBMITPOINTSCALC,
    
    //DECAL
    kWEBSERVICE_SEARCHVEHICLEWITHDECAL,
    kWEBSERVICE_TAGDECALTOTRADEOWNER,
    kWEBSERVICE_TAGVEHICLETODECAL,
    
    kWEBSERVICE_UPDATEREGMECHANIC   ,
    kWEBSERVICE_UPDATEMECHANIC      ,
    kWEBSERVICE_ADDIM               ,
    kWEBSERVICE_UPDATEIM               ,
    
    //Russia V2
    kWEBSERVICE_FETCHCTO        ,
    kWEBSERVICE_FETCHWAYSTOEARNPOINTS,
    kWEBSERVICE_FETCHPERFORMANCEMETRICS,
    
    //Russia V2 SPIRAX

    kWEBSERVICE_FETCHCTO_ONEPLUSONE,

    kWEBSERVICE_MYPERFORMANCE       = 200,
    kWEBSERVICE_DSRPERFORMANCE      ,
    kWEBSERVICE_TRADEOWNERPERFORMANCE   ,
    
    kWEBSERVICE_FETCHPERFORMANCESUMMARY,
    kWEBSERVICE_FETCHSECTIONBREAKDOWN,
    
    kWEBSERVICE_UPDATETRADEPROFILE  ,
    kWEBSERVICE_UPDATEBANKDETAILS  ,
    kWEBSERVICE_ISVALIDAUTH         ,
    kWEBSERVICE_UPLOADPROFILEPHOTO  ,
    kWEBSERVICE_UPDATEDOCUMENTDETAILS,
    
    kWEBSERVICE_INVITETRADE         ,
    
    kWEBSERVICE_REWARDSFETCHLIST    = 300,
    kWEBSERVICE_REWARDSFETCHDETAILS ,
    kWEBSERVICE_REWARDSPOINTSSUMMARY,
    kWEBSERVICE_REWARDSFETCHWISHLIST,
    kWEBSERVICE_REWARDSADDWISHITEM  ,
    kWEBSERVICE_REWARDSREMOVEWISHITEM,
    kWEBSERVICE_REWARDSCONFIRMORDER,
    kWEBSERVICE_REWARDSFETCHREWARDSHOME,
    kWEBSERVICE_REWARDSREDEMPTIONHISTORY,
    kWEBSERVICE_REWARDSREDEMPTIONDETAILS,
    
    kWEBSERVICE_PROMOGETDETAILS     = 400,
    kWEBSERVICE_PROMOSUBMITCODE     ,
    kWEBSERVICE_PROMOFETCHHISTORY   ,
    
    kWEBSERVICE_TOPOINTSHISTORY     = 500,
    kWEBSERVICE_TOPOINTSDETAILS     ,
    
    kWEBSERVICE_CURRENTOFFER        = 600,
    kWEBSERVICE_OFFERDETAILS        ,
    kWEBSERVICE_PASTOFFER           ,
    kWEBSERVICE_WORKSHOPOILDETAIL   ,
    kWEBSERVICE_SUBMITANCILLARY   ,
    kWEBSERVICE_FETCHANCILLARY   ,
    kWEBSERVICE_CANCELANCILLARY   ,
    
    kWEBSERVICE_ALLORDERS           = 700,
    kWEBSERVICE_ORDERDETAILS        ,
    kWEBSERVICE_CATEGORYLIST        ,
    kWEBSERVICE_ALLPRODUCTSINCATEGORYLIST        ,
    kWEBSERVICE_ADDNEWORDERS        ,
    kWEBSERVICE_UPDATESTATUS        ,
    kWEBSERVICE_FETCHPENDINGORDER        ,
    
    kWEBSERVICE_CONTACTUS           = 800,
    kWEBSERVICE_FETCHFAQ            ,
    kWEBSERVICE_FETCHTNC            ,
    kWEBSERVICE_ACCEPTTNC           ,
    kWEBSERVICE_ACCEPTPRIVACY       ,
    kWEBSERVICE_MESSAGELIST         ,
    kWEBSERVICE_READNOTIFICATION    ,
    kWEBSERVICE_OPENNOTIFICATION    ,
    kWEBSERVICE_DELETENOTIFICATION  ,
    
    kWEBSERVICE_LOGOUT              = 900,
    kWEBSERVICE_FETCHPUSH           ,
    kWEBSERVICE_UPDATEPUSH          ,
    kWEBSERVICE_UPDATELANGUAGE      ,
    
    kWEBSERVICE_CLOSESURVEY         = 998,
    kWEBSERVICE_LOOKUPTABLE         = 999,
    kWEBSERVICE_LANGUAGE            ,
    kWEBSERVICE_STATE               ,
    kLOGEXCEPTIONERROR              ,
    
    kWEBSERVICE_FETCH_PERFORMANCE_V2 = 1900,
    kWEBSERVICE_FETCH_LEADERBOARD,
    kWEBSERVICE_FETCH_ORDERSBREAKDOWN,
    kWEBSERVICE_FETCH_POINTSBREAKDOWN,
    kWEBSERVICE_FETCH_STAFFBREAKDOWN,
    kWEBSERVICE_FETCH_OILCHANGEBREAKDOWN,
    kWEBSERVICE_FETCH_CUSTOMERBREAKDOWN,
    kWEBSERVICE_FETCH_OFFERREDEMPTIONBREAKDOWN,
    kWEBSERVICE_FETCH_APPROVEDWORKSHOP,
    kWEBSERVICE_FETCH_POSTALADDRESS,
    kWEBSERVICE_FETCH_BANKDETAILS,
    
    kWEBSERVICE_ASSETCATEGORYLIST  = 2000,
    kWEBSERVICE_ASSETLIST            ,
    kWEBSERVICE_ASSETREAD            ,
    
    kWEBSERVICE_FETCHWORKSHOPDETAILSUK  = 2100,
    kWEBSERVICE_UPDATEWORKSHOPDETAILSUK,
    
    kWEBSERVICE_SUBMITMEMBER_REQUESTOTP  = 2200,
    kWEBSERVICE_SUBMITMEMBER_SUBMITOTP,
    kWEBSERVICE_SUBMITMEMBER_FORM,
    kWEBSERVICE_SUBMITMEMBER,
    kWEBSERVICE_SUBMITMEMBER_RESENDOTP,
    kWEBSERVICE_SUBMITMEMBERVEHICLES,
    
    
    kWEBSERVICE_LOOKUPTNC_CONSUMER_RU         = 2300,
    kWEBSERVICE_LOOKUPTNC_WORKSHOP_RU,
    kWEBSERVICE_LOOKUPTNC_DSR_RU,
    
    kWEBSERVICE_LOYALTY_LOADCONTRACTLIST    = 2400,
    kWEBSERVICE_LOYALTY_FETCHWORKSHOPLIST,
    kWEBSERVICE_LOYALTY_FETCHCATALOGUELIST,
    kWEBSERVICE_LOYALTY_LOADPACKAGELIST,
    kWEBSERVICE_LOYALTY_SUBMITNEWCONTRACT,
    kWEBSERVICE_LOYALTY_LOADCONTRACTDETAILS,
    kWEBSERVICE_LOYALTY_LOADDSRPERFORMANCE,
    kWEBSERVICE_LOYALTY_WORKSHOPPERFORMANCE,
    kWEBSERVICE_LOYALTY_UPDATECONTRACT,
    kWEBSERVICE_LOYALTY_EXTENDCONTRACT,
    kWEBSERVICE_LOYALTY_CONTINUECONTRACT,
    kWEBSERVICE_LOYALTY_TERMINATECONTRACT,
    kWEBSERVICE_LOYALTY_COMPLETECONTRACT,
    kWEBSERVICE_LOYALTY_UPDATESIGNATUREFORCONTRACT,
    kWEBSERVICE_PROMOTION_REDEEMTRADEPROMOTION,
    kWEBSERVICE_TRADE_LOADCOMPANYCATEGORY,
    kWEBSERVICE_TRADE_EARNNREDEEM,

    //Moscow
    kWEBSERVICE_MSTRADE_FETCHDSRINVENTORY,
    kWEBSERVICE_MSTRADE_FETCHTOINVENTORY,
    kWEBSERVICE_MSTRADE_FETCHDSRWORKSHOPLIST,
    kWEBSERVICE_MSTRADE_FETCHPARTICIPATINGPRODUCTS,
   // kWEBSERVICE_MSTRADE_SUBMITSCANOUTCODEMS,
    kWEBSERVICE_MSTRADE_SUBMITCODEMS,
    kWEBSERVICE_MSTRADE_FETCHPERFORMANCE_V3,

    kWEBSERVICE_TRADE_EARNNREDEEM_REPORT,
    kWEBSERVICE_LOYALTY_TH_LOADTIERPOINTS,

    kWEBSERVICE_GAMIFICATION_VIEWDETAILS,
    kWEBSERVICE_GAMIFICATION_SHOWPROMOTIONRESULT,
    kWEBSERVICE_GAMIFICATION_CHECKIN_VIEWDETAILS,
    kWEBSERVICE_GAMIFICATION_HELIX_VIEWDETAILS,
    kWEBSERVICE_GAMIFICATION_CHECKIN_TODAY,
    kWEBSERVICE_GAMIFICATION_CHECKIN_PLAYGRANDPRIZE,

    kWEBSERVICE_TRADE_LUBEMATCH_SEARCHVEHICLE,
    kWEBSERVICE_TRADE_LUBEMATCH_SUBMITREGISTRATION,
    
    kWEBSERVICE_AI_FETCHCONVERSATIONS,
    kWWEBSERVICE_AI_FETCHOILCHANGECOUNT,

    kOATS_SEARCH = 103000,
    kOATS_EQUIPMENTHREF,
    kOATS_VEHICLETYPES,

}   WebserviceCall;

@interface FormDataItem : NSObject

@property (nonatomic,strong) NSString  *key;
@property (nonatomic,strong) NSData  *data;

@end

@interface WebServiceRequest : NSObject

@property (nonatomic,strong) NSString  *webserviceAPI;
@property (nonatomic,strong) NSString  *msgHUD;
@property (nonatomic,strong) UIView  *vwHUD;
@property (nonatomic,strong) NSString  *requestURL;
@property (nonatomic,strong) NSDictionary *requestHeaders;
@property (nonatomic,strong) NSDictionary *requestParams;
@property (nonatomic,strong) NSString *savedFileName;
@property (nonatomic,assign) RequestMethod requestMethod;
@property (nonatomic,assign) WebserviceCall webserviceCall;
@property (nonatomic,strong) NSString *imageUploadPath;
@property (nonatomic,strong) NSString *requestBody;
@property (nonatomic,strong) NSData *dataUserImage;
@property (nonatomic,strong) NSArray *requestDataFiles;
@property (nonatomic,strong) NSArray *requestFormDataParams;
@property (assign, nonatomic) id delegate;

@end
