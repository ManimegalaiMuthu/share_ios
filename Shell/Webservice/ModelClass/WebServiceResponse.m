//
//  WebServiceResponse.m
//  JTIStarPartners
//  Created by Shekhar on 20/11/14.
//  Copyright (c) 2014 Edenred. All rights reserved.
//

#import "WebServiceResponse.h"
#import "NSDictionary+JSON.h"
#import "Constants.h"
#import "LocalizationManager.h"
#import <Crashlytics/Crashlytics.h>
#import "Session.h"

@implementation WebserviceAlert

- (id)initWithData:(NSDictionary *)data
{
    self = [super init];
    self.title = [data objectForKey:@"Header"];
    self.message = [data objectForKey:@"Message"];
    self.btnTextPositive = [data objectForKey:@"BtnText1"];
    self.btnTextNegative = [data objectForKey:@"BtnText2"];
    return self;
}

- (NSString *)title {
    if([_title isEqual:[NSNull null]]) {
        return @"";
    }
    return _title;
}

- (NSString *)btnTextPositive {
    if([_btnTextPositive isEqual:[NSNull null]]) {
        return @"";
    }
    return _btnTextPositive;
}

- (NSString *)btnTextNegative {
    if([_btnTextNegative isEqual:[NSNull null]]) {
        return @"";
    }
    return _btnTextNegative;
}

@end

@interface WebServiceResponse()
{
    NSDictionary *jsonDictionary;
}

- (BOOL)isValidJson;

@end

@implementation WebServiceResponse

- (id)initWithData:(id)data
{
    if (self = [super init])
    {
        NSError *error;
        self.data = data;
        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:self.data options: NSJSONReadingMutableContainers error: &error];
        if (jsonObject)
        {
            isValidJson = YES;
            jsonDictionary =  [[NSDictionary alloc ] initWithDictionary:jsonObject];
            self.alert = [[WebserviceAlert alloc]initWithData:[jsonDictionary objectForKey:RESPONSE_MESSAGE_NODE]];
        }
        else
        {
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:[error description] forKey:@"ERROR"];
            [dict setObject:[NSNumber numberWithInt:-1] forKey:@"ResponseCode"];
            jsonDictionary =  [[NSDictionary alloc ] initWithDictionary:dict];
        }
        self.responseData = jsonDictionary;
    }
    return self;
}


- (BOOL)isValidJson
{
    return ([NSJSONSerialization isValidJSONObject:self.data]);
}


- (BOOL)errorInResponse
{
    BOOL error = YES;
    int code = -1;
    if (isValidJson)
    {
        code = [[jsonDictionary objectForKey:RESPONSE_CODE_NODE] intValue];
        if (code == kSuccessCode) error = NO;
    }
    
    return error;
}

- (NSString *)getErrorMessage {
    
    return [jsonDictionary objectForKey:kError];
}

- (id) getGenericResponse {
    
    return [jsonDictionary objectForKey:kData];
}

- (NSDictionary *)getJSON
{
    return jsonDictionary;
}

- (int)getResponseCode
{
    int code = -1;
    if (isValidJson)
    {
       code = [[jsonDictionary objectForKey:RESPONSE_CODE_NODE] intValue];
    }
        
    return code;
}

- (NSString *)getAuthToken
{
    NSString *authToken = nil;
    if (isValidJson)
    {
        authToken = [[jsonDictionary objectForKey:kData] objectForKey:kAuthToken];
    }
    
    return authToken;
}

- (NSString *)getMemberID
{
    NSString *memberID = nil;
    if (isValidJson)
    {
        memberID = [[jsonDictionary objectForKey:kData] objectForKey:kMemberID];
    }
    
    return memberID;
}


- (NSString *)getResponseMessage
{
    NSString *message = nil;
    if (isValidJson)
    {
        message = [[jsonDictionary objectForKey:RESPONSE_MESSAGE_NODE] objectForKey:@"Message"];
    }
    
    return message;
}

- (NSString *)getRequestOTP
{
    NSString *requestOTP = nil;
    if (isValidJson)
    {
        requestOTP = [[jsonDictionary objectForKey:@"Data"] objectForKey:@"RequestOTP"];
    }
    
    return requestOTP;
}




- (NSString *)getResponseMobileOTP
{
    NSString *mobileNo = nil;
    if (isValidJson)
    {
        mobileNo = [[jsonDictionary objectForKey:@"Data"] objectForKey:@"MobileNumber"];
    }
    
    return mobileNo;
}


- (NSString *)getNetworkFailureMsg
{
    NSString *errorCodeString = [[self.error.userInfo objectForKey:@"_kCFStreamErrorCodeKey"] stringValue];
    //The network connection was lost
    if ([errorCodeString isEqualToString: @"60"])
    {
        CLSLog(@"class %@ fail at %@ %@", [mSession currentViewControllerString], errorCodeString, LOCALIZATION(C_GLOBAL_TIMEOUT));
        //The request timed out.
        return LOCALIZATION(C_GLOBAL_TIMEOUT);
    }
    else if([errorCodeString isEqualToString:@"-2102"])
    {
        CLSLog(@"class %@ fail at %@ %@",  [mSession currentViewControllerString], errorCodeString, LOCALIZATION(C_GLOBAL_TIMEOUT));
        //The request timed out.
        return LOCALIZATION(C_GLOBAL_TIMEOUT);
    }
    else if([errorCodeString isEqualToString:@"50"])
    {
        CLSLog(@"class %@ fail at %@ %@",  [mSession currentViewControllerString], errorCodeString, LOCALIZATION(C_GLOBAL_NONETWORK));
        //The Internet connection appears to be offline
        return LOCALIZATION(C_GLOBAL_NONETWORK);
    }
    else
    {
        CLSLog(@"class %@ fail at %@ %@",  [mSession currentViewControllerString], errorCodeString, [self.error.userInfo objectForKey:@"NSLocalizedDescription"]);
        return [self.error.userInfo objectForKey:@"NSLocalizedDescription"];
    }
}

@end
